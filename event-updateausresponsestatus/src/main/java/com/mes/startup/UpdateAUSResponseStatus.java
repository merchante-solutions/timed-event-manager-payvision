package com.mes.startup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;

public class UpdateAUSResponseStatus extends EventBase{

	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(UpdateAUSResponseStatus.class);

	public boolean execute() {
		log.info("Inside UpdateAUSResponseStatus::excute()");
		try {
			connect();
			updateAusOutBoundFileTableWithResponseCodeOne();
			updateAusOutBoundFileTableWithResponseCodeTwo();
			updateAusOutBoundFileTableWithResponseCodeThree();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			logEntry("UpdateAUSResponseStatus::excute()", e.toString());
		} finally {
			cleanUp();
		}
		return true;
	}

	private void updateAusOutBoundFileTableWithResponseCodeOne() {
		log.info("Inside UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeOne()");
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryString = null;
		boolean updateFlag = false;
		try {
			queryString = "SELECT count(*) noofrecords FROM aus_outbound_files aof WHERE EXISTS ( SELECT ob_id FROM aus_inbound_files aif WHERE aif.ob_id = aof.ob_id) AND aof.response_code IS NULL";
			statement = con.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int count = resultSet.getInt("noofrecords");
				log.info("No of records to be updated with response code 1: " + count);
				if (count > 0) {
					updateFlag = true;
				}
			}
			statement.close();
			resultSet.close();
			queryString = null;
			log.info("updateFlag to set response code 1:" + updateFlag);
			if (updateFlag) {
				queryString = "UPDATE AUS_OUTBOUND_FILES AOF SET RESPONSE_CODE = '1' WHERE EXISTS (SELECT OB_ID FROM AUS_INBOUND_FILES AIF WHERE AIF.OB_ID = AOF.OB_ID) AND RESPONSE_CODE IS NULL";
				statement = con.prepareStatement(queryString);
				statement.executeUpdate();
				statement.close();
			}
		} catch (Exception e) {
			log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeOne()" + e.getMessage());
			logEntry("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeOne()", e.toString());
		} finally {
			try {
				statement.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeOne finally block statement.close():" +e.getMessage());
			}
			try {
				resultSet.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeOne finally block resultSet.close():" +e.getMessage());
			}
		}
	}

	private void updateAusOutBoundFileTableWithResponseCodeTwo() {
		log.info("Inside UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeTwo()");
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryString = null;
		boolean updateFlag = false;
		try {
			queryString = "SELECT count(*) noofrecords FROM aus_outbound_files aof WHERE TRUNC(CREATE_TS) >= TRUNC(sysdate)-5 AND NOT EXISTS( SELECT ob_id FROM aus_inbound_files aif WHERE aif.ob_id = aof.ob_id) AND aof.response_code IS NULL";
			statement = con.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int count = resultSet.getInt("noofrecords");
				log.info("No of records to be updated with response code 2: " + count);
				if (count > 0) {
					updateFlag = true;
				}
			}
			statement.close();
			resultSet.close();
			queryString = null;
			log.info("updateFlag to set response code 2:" + updateFlag);
			if (updateFlag) {
				queryString = "UPDATE AUS_OUTBOUND_FILES AOF SET RESPONSE_CODE = '2' WHERE TRUNC(CREATE_TS) >= TRUNC(sysdate)-5 AND NOT EXISTS (SELECT OB_ID FROM AUS_INBOUND_FILES AIF WHERE AIF.OB_ID = AOF.OB_ID) AND RESPONSE_CODE IS NULL";
				statement = con.prepareStatement(queryString);
				statement.executeUpdate();
				statement.close();
			}
		} catch (Exception e) {
			log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeTwo()" + e.getMessage());
			logEntry("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeTwo()", e.toString());
		} finally {
			try {
				statement.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeTwo finally block statement.close():" +e.getMessage());
			}
			try {
				resultSet.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeTwo finally block resultSet.close():" +e.getMessage());
			}
		}
	}


	private void updateAusOutBoundFileTableWithResponseCodeThree() {
		log.info("Inside UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeThree()");
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryString = null;
		boolean updateFlag = false;
		Map<Long,String> obIdSysCodeMap = null;
		try {
			queryString = "SELECT OB_ID,SYS_CODE FROM aus_outbound_files aof WHERE TRUNC(CREATE_TS) < TRUNC(sysdate)-5 AND NOT EXISTS( SELECT ob_id FROM aus_inbound_files aif WHERE aif.ob_id = aof.ob_id) AND aof.response_code IS NULL";
			statement = con.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			obIdSysCodeMap = new HashMap<Long, String>();
			while (resultSet.next()) {
				obIdSysCodeMap.put(resultSet.getLong("OB_ID"), resultSet.getString("SYS_CODE"));
			}
			if(!obIdSysCodeMap.isEmpty()){
				updateFlag = true;
			}
			statement.close();
			resultSet.close();
			queryString = null;
			log.info("updateFlag to set response code 3:" + updateFlag);
			if (updateFlag) {
				queryString = "UPDATE AUS_OUTBOUND_FILES AOF SET RESPONSE_CODE = '3' WHERE TRUNC(CREATE_TS) < TRUNC(sysdate)-5 AND NOT EXISTS(SELECT OB_ID FROM AUS_INBOUND_FILES AIF WHERE AIF.OB_ID = AOF.OB_ID) AND RESPONSE_CODE IS NULL";
				statement = con.prepareStatement(queryString);
				statement.executeUpdate();
				statement.close();
				sendEmailnotification(obIdSysCodeMap);
			}
		} catch (Exception e) {
			log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeThree()" + e.getMessage());
			logEntry("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeThree()", e.toString());
		} finally {
			try {
				statement.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeThree finally block statement.close():" +e.getMessage());
			}
			try {
				resultSet.close();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::updateAusOutBoundFileTableWithResponseCodeThree finally block resultSet.close():" +e.getMessage());
			}
		}
	}

	public void sendEmailnotification(Map<Long,String> obIdSysCodeMap){
		log.info("UpdateAUSResponseStatus::sendEmailnotification():Sending email...");
		StringBuilder sb = null;
		MailMessage msg = null;
		try{
			Iterator<Entry<Long, String>> it = obIdSysCodeMap.entrySet().iterator();
		    while (it.hasNext()) {
		        Entry<Long, String> pair = it.next();
		        sb = getAUSMerchantIdAsString((long)pair.getKey(),(String)pair.getValue());
		        if(sb!=null && !sb.toString().isEmpty()){
					msg = new MailMessage();
					msg.setAddresses(MesEmails.MSG_ADDRS_AUS_RESPONSE_UPDATE);
					msg.setSubject("Missing AUS Response");
					sb.append("\n AUS Response are missing");
					msg.setText(sb.toString());
					msg.send();
				}
				sb = null;
				msg = null;
		        it.remove();
		    }
		}catch(Exception e){
			log.error("UpdateAUSResponseStatus::sendEmailnotification()::Exception while sending email :" + e.getMessage());
			logEntry("UpdateAUSResponseStatus::sendEmailnotification()",e.toString());
		}
		log.info("UpdateAUSResponseStatus::sendEmailnotification():Mail has been sent");
	}

	private StringBuilder getAUSMerchantIdAsString(long obId,String sysCode) {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryString = null;
		StringBuilder merchList = null;
		try {
			queryString = "select merch_num,reqf_id from MES.AUS_REQUEST_files where reqf_id in (select reqf_id from MES.AUS_OUT_RF where ob_id = ?)";
			statement = con.prepareStatement(queryString);
			statement.setLong(1, obId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				if(merchList==null){
					merchList = new StringBuilder("Merchant numbers for ob_id:"+obId+" and card type:"+sysCode+" \n");
					merchList.append("MERCHANT_ID			REQF_ID \n");
					merchList.append("-----------			------- \n");
				}
				merchList.append(resultSet.getLong("merch_num")+"			"+resultSet.getLong("reqf_id"));
				merchList.append("\n");
			}
		} catch (Exception e) {
			log.error("UpdateAUSResponseStatus::getAUSMerchantIdRequestId().Exception occurred:" + e.getMessage());
		}
		return merchList;
	}

	public static void main(String[] args) {
		UpdateAUSResponseStatus updateAUSResponseStatus = null;
		try {
			updateAUSResponseStatus = new UpdateAUSResponseStatus();
			updateAUSResponseStatus.execute();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("UpdateAUSResponseStatus::main():" +e.getMessage());
		} finally {
			try {
				updateAUSResponseStatus.cleanUp();
			} catch (Exception e) {
				log.error("UpdateAUSResponseStatus::finally block exception:" +e.getMessage());
			}
			log.debug("Command line execution of UpdateAUSResponseStatus complete");
			Runtime.getRuntime().exit(0);
		}
	}

}
