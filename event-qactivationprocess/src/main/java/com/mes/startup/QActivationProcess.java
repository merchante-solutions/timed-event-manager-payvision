/*@lineinfo:filename=QActivationProcess*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/QActivationProcess.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/08/04 9:25a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.ops.QueueConstants;
import com.mes.queues.QueueTools;
import sqlj.runtime.ResultSetIterator;

public class QActivationProcess extends EventBase
{
  private int     processSequence = 0;
  private Vector  apps            = new Vector();
  
  private String addToActivationQueue(ActivationApp app)
  {
    String result = "";
    
    try
    {
      // make sure item isn't already in activation queue somewhere
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:45^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(id)
//          
//          from    q_data
//          where   id = :app.appSeqNum and
//                  type between :MesQueues.Q_ACTIVATION_NEW and
//                               :MesQueues.Q_ACTIVATION_NEW + 99
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4521 = MesQueues.Q_ACTIVATION_NEW + 99;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(id)\n         \n        from    q_data\n        where   id =  :1  and\n                type between  :2  and\n                              :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.QActivationProcess",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,app.appSeqNum);
   __sJT_st.setInt(2,MesQueues.Q_ACTIVATION_NEW);
   __sJT_st.setInt(3,__sJT_4521);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:53^7*/
      
      if(recCount == 0)
      {
        // do it to it
        QueueTools.insertQueue(app.appSeqNum, MesQueues.Q_ACTIVATION_NEW, null);
        
        // put into temp queue ...?
        /*@lineinfo:generated-code*//*@lineinfo:61^9*/

//  ************************************************************
//  #sql [Ctx] { insert into temp_activation_queue
//            (
//              q_type,
//              q_stage,
//              request_id,
//              date_in_queue,
//              app_seq_num
//            )
//            values
//            (
//              :QueueConstants.QUEUE_ACTIVATION,
//              :QueueConstants.Q_ACTIVATION_NEW,
//              -1,
//              sysdate,
//              :app.appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into temp_activation_queue\n          (\n            q_type,\n            q_stage,\n            request_id,\n            date_in_queue,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            -1,\n            sysdate,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.QUEUE_ACTIVATION);
   __sJT_st.setInt(2,QueueConstants.Q_ACTIVATION_NEW);
   __sJT_st.setLong(3,app.appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^9*/
        
        // stamp activation in app tracking
        /*@lineinfo:generated-code*//*@lineinfo:82^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     date_received = sysdate,
//                    status_code = 601
//            where   app_seq_num = :app.appSeqNum and
//                    dept_code = :QueueConstants.DEPARTMENT_ACTIVATION and
//                    date_received is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     date_received = sysdate,\n                  status_code = 601\n          where   app_seq_num =  :1  and\n                  dept_code =  :2  and\n                  date_received is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app.appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_ACTIVATION);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^9*/
        
        result = "added";
      }
      else
      {
        result = "already present";
      }
    }
    catch(Exception e)
    {
      logEntry("addToActivationQueue(" + app.appSeqNum + ")", e.toString());
    }
    
    return result;
  }
  
  private String processApp(ActivationApp app)
  {
    String              result      = "";
    boolean             mesTraining = false;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {
      if(app.mesActivation)
      {
        if(app.appType == mesConstants.APP_TYPE_TRANSCOM)
        {
          // transcom_merchant contains phone training value
          /*@lineinfo:generated-code*//*@lineinfo:121^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(phone_training, 'N') phone_training
//              from    transcom_merchant
//              where   app_seq_num = :app.appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(phone_training, 'N') phone_training\n            from    transcom_merchant\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app.appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.QActivationProcess",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^11*/
          
          rs = it.getResultSet();
          
          if(rs.next() && rs.getString("phone_training").equals("M"))
          {
            mesTraining = true;
          }
          
          rs.close();
          it.close();
        }
        else
        {
          // check pos_features table first
          /*@lineinfo:generated-code*//*@lineinfo:141^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(phone_training, 'N') phone_training
//              from    pos_features
//              where   app_seq_num = :app.appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(phone_training, 'N') phone_training\n            from    pos_features\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app.appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.QActivationProcess",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^11*/
        
          rs = it.getResultSet();
        
          if(rs.next() && rs.getString("phone_training").equals("M"))
          {
            mesTraining = true;
          }
        
          rs.close();
          it.close();
          
          // some apps always do phone training
          if(app.appType == mesConstants.APP_TYPE_DISCOVER_IMS)
          {
            mesTraining = true;
          }
        }
      }
      
      // now insert into activation if necessary
      if(mesTraining)
      {
        result = addToActivationQueue(app);
      }
      else
      {
        result = "not needed";
      }
    }
    catch(Exception e)
    {
      logEntry("processApp(" + app.appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return result;
  }
  
  public boolean execute()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    boolean               result  = false;
    
    try
    {
      connect();
      
      // see if there are any apps to process
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:201^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    q_activation_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    q_activation_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.QActivationProcess",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^7*/
      
      if(recCount > 0)
      {
        // get new process sequence
        /*@lineinfo:generated-code*//*@lineinfo:212^9*/

//  ************************************************************
//  #sql [Ctx] { select  process_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.QActivationProcess",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^9*/
        
        // update waiting records with processSequence
        /*@lineinfo:generated-code*//*@lineinfo:220^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_activation_process
//            set     process_sequence = :processSequence
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_activation_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^9*/
        
        // get app seq nums into vector
        /*@lineinfo:generated-code*//*@lineinfo:228^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  qap.app_seq_num,
//                    app.app_type,
//                    nvl(at.mes_activation, 'N') mes_activation
//            from    q_activation_process  qap,
//                    application           app,
//                    app_type              at
//            where   qap.process_sequence = :processSequence and
//                    qap.app_seq_num = app.app_seq_num and
//                    app.app_type = at.app_type_code
//            order by qap.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qap.app_seq_num,\n                  app.app_type,\n                  nvl(at.mes_activation, 'N') mes_activation\n          from    q_activation_process  qap,\n                  application           app,\n                  app_type              at\n          where   qap.process_sequence =  :1  and\n                  qap.app_seq_num = app.app_seq_num and\n                  app.app_type = at.app_type_code\n          order by qap.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.QActivationProcess",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          apps.add(new ActivationApp(rs));
        }
        
        rs.close();
        it.close();
        
        // process apps
        for(int i=0; i<apps.size(); ++i)
        {
          String status = processApp((ActivationApp)(apps.elementAt(i)));
          
          /*@lineinfo:generated-code*//*@lineinfo:257^11*/

//  ************************************************************
//  #sql [Ctx] { update  q_activation_process
//              set     date_processed = sysdate,
//                      status = :status
//              where   process_sequence = :processSequence and
//                      app_seq_num = :((ActivationApp)(apps.elementAt(i))).appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_activation_process\n            set     date_processed = sysdate,\n                    status =  :1 \n            where   process_sequence =  :2  and\n                    app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.QActivationProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,status);
   __sJT_st.setInt(2,processSequence);
   __sJT_st.setLong(3,((ActivationApp)(apps.elementAt(i))).appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^11*/
        }
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public class ActivationApp
  {
    public long     appSeqNum;
    public int      appType;
    public boolean  mesActivation;
    
    public ActivationApp(ResultSet rs)
    {
      try
      {
        appSeqNum = rs.getLong("app_seq_num");
        appType   = rs.getInt("app_type");
        mesActivation = rs.getString("mes_activation").equals("Y");
      }
      catch(Exception e)
      {
        logEntry("constructor", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/