/*@lineinfo:filename=SabreCleanup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/SabreCleanup.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-04-24 17:47:24 -0700 (Tue, 24 Apr 2007) $
  Version            : $Revision: 13694 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.cielo.security.IPasswordHashing;
import com.cielo.security.PasswordHashingFactory;
import com.mes.constants.MesUsers;
import com.mes.ops.AccountSetupBean;
import com.mes.ops.AutoUploadAddressUsage;
import com.mes.ops.AutoUploadBetInfo;
import com.mes.ops.AutoUploadChargeRecords;
import com.mes.ops.AutoUploadIndividualMerchant;
import com.mes.ops.AutoUploadMerchantDiscount;
import com.mes.ops.AutoUploadTransactionDestination;
import sqlj.runtime.ResultSetIterator;

public class SabreCleanup extends EventBase
{
  private Vector  items = new Vector();
  
  private void addChainUser(Item item)
  {
    long    userNode;
    long    userType;
    String  loginName;
    String  password;
    String  userName;
    
    try
    {
      if(item.primaryPcc == null)
      {
        // merchant location
        /*@lineinfo:generated-code*//*@lineinfo:58^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number
//            
//            from    merchant
//            where   app_seq_num = :item.appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,item.appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^9*/
        
        userType = MesUsers.USER_SABRE_MERCHANT;
      }
      else
      {
        // get association hierarchy node
        /*@lineinfo:generated-code*//*@lineinfo:71^9*/

//  ************************************************************
//  #sql [Ctx] { select  to_number('3941'||to_char(ms.association)) association_node
//            
//            from    merchant_sabre ms
//            where   ms.app_seq_num = :item.appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_number('3941'||to_char(ms.association)) association_node\n           \n          from    merchant_sabre ms\n          where   ms.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,item.appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:77^9*/
        
        userType = MesUsers.USER_SABRE_CHAIN;
      }
      
      // get remaining user credentials
      /*@lineinfo:generated-code*//*@lineinfo:83^7*/

//  ************************************************************
//  #sql [Ctx] { select  lower(m.merch_email_address),
//                  upper(ms.pcc),
//                  m.merch_business_name
//          
//          from    merchant        m,
//                  merchant_sabre  ms
//          where   m.app_seq_num = :item.appSeqNum and
//                  m.app_seq_num = ms.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lower(m.merch_email_address),\n                upper(ms.pcc),\n                m.merch_business_name\n         \n        from    merchant        m,\n                merchant_sabre  ms\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = ms.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,item.appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loginName = (String)__sJT_rs.getString(1);
   password = (String)__sJT_rs.getString(2);
   userName = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      
      // make sure this user doesn't already exist
      int userCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//          
//          from    users
//          where   lower(login_name) = lower(:loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n         \n        from    users\n        where   lower(login_name) = lower( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/
      
      if(userCount == 0)
      {
        // create user
        PasswordHashingFactory passwordHashingFactory = PasswordHashingFactory.getInstance();
        IPasswordHashing ipasswordHashing = passwordHashingFactory.getHashingStrategy(PasswordHashingFactory.ALGORITHM_PBKDF2);
        String passwordEnc = ipasswordHashing.createHash(password);
        
        ipasswordHashing = passwordHashingFactory.getHashingStrategy(PasswordHashingFactory.ALGORITHM_MD5);
        String backupPasswordEnc = ipasswordHashing.createHash(password);
        
        
        /*@lineinfo:generated-code*//*@lineinfo:119^9*/

//  ************************************************************
//  #sql [Ctx] { insert into users
//            (
//              user_id,
//              type_id,
//              login_name,
//              name,
//              create_date,
//              pswd_change_date,
//              last_login_date,
//              email,
//              hierarchy_node,
//              password_enc,            
//              password_hash_type,
//              enabled,
//              create_user
//            )
//            values
//            (
//              users_sequence.nextval,
//              :userType,
//              :loginName,
//              :userName,
//              sysdate,
//              sysdate,
//              sysdate,
//              :loginName,
//              :userNode,
//              :passwordEnc,            
//              2,
//              'Y',
//              'SYSTEM'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into users\n          (\n            user_id,\n            type_id,\n            login_name,\n            name,\n            create_date,\n            pswd_change_date,\n            last_login_date,\n            email,\n            hierarchy_node,\n            password_enc,            \n            password_hash_type,\n            enabled,\n            create_user\n          )\n          values\n          (\n            users_sequence.nextval,\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n            sysdate,\n            sysdate,\n             :4 ,\n             :5 ,\n             :6 ,            \n            2,\n            'Y',\n            'SYSTEM'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.SabreCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userType);
   __sJT_st.setString(2,loginName);
   __sJT_st.setString(3,userName);
   __sJT_st.setString(4,loginName);
   __sJT_st.setLong(5,userNode);
   __sJT_st.setString(6,passwordEnc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^9*/
        
        // MailMessage.sendSystemErrorEmail("Sabre user created: " + loginName, loginName + ": " + userName + ", " + userNode);
      }
      else
      {
        // MailMessage.sendSystemErrorEmail("Sabre chain user already exists", loginName);
      }

      // mark user as created no matter what
      /*@lineinfo:generated-code*//*@lineinfo:163^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_sabre
//          set     chain_user_assigned = 'Y'
//          where   app_seq_num = :item.appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_sabre\n        set     chain_user_assigned = 'Y'\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.SabreCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,item.appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^7*/
    }
    catch(Exception e)
    {
      logEntry("addChainUser(" + item.appSeqNum + ")", e.toString());
    }
  }

  private void processApp(Item item)
  {
    String  association;
    try
    {
      // see if primary pcc association has been assigned
      int   recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_sabre
//          where   upper(pcc) = :item.primaryPcc and
//                  upper(pcc) = upper(primary_pcc) and
//                  association is not null and
//                  association != 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_sabre\n        where   upper(pcc) =  :1  and\n                upper(pcc) = upper(primary_pcc) and\n                association is not null and\n                association != 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,item.primaryPcc);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^7*/

      if(recCount > 0)
      {
        // retrieve association
        /*@lineinfo:generated-code*//*@lineinfo:197^9*/

//  ************************************************************
//  #sql [Ctx] { select  association
//            
//            from    merchant_sabre
//            where   upper(pcc) = :item.primaryPcc and
//                    upper(pcc) = upper(primary_pcc)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  association\n           \n          from    merchant_sabre\n          where   upper(pcc) =  :1  and\n                  upper(pcc) = upper(primary_pcc)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.SabreCleanup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,item.primaryPcc);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   association = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:204^9*/

        // update merchant table
        /*@lineinfo:generated-code*//*@lineinfo:207^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     asso_number = :association
//            where   app_seq_num = :item.appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     asso_number =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.SabreCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,association);
   __sJT_st.setInt(2,item.appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:212^9*/

        // do auto upload
        AutoUploadIndividualMerchant       autoUpload1 = new AutoUploadIndividualMerchant    (item.appSeqNum);
        AutoUploadMerchantDiscount         autoUpload2 = new AutoUploadMerchantDiscount      (item.appSeqNum);
        AutoUploadBetInfo                  autoUpload3 = new AutoUploadBetInfo               (item.appSeqNum);
        AutoUploadChargeRecords            autoUpload4 = new AutoUploadChargeRecords         (item.appSeqNum);
        AutoUploadTransactionDestination   autoUpload5 = new AutoUploadTransactionDestination(item.appSeqNum);
        AutoUploadAddressUsage             autoUpload6 = new AutoUploadAddressUsage          (item.appSeqNum);

        //then submit it to the tape and move the account into mms
        AccountSetupBean asb = new AccountSetupBean();
        asb.submitData(null, item.appSeqNum, true);
        asb.cleanUp();
      }
    }
    catch(Exception e)
    {
      logEntry("processApp(" + item.appSeqNum + ")", e.toString());
    }
  }

  public boolean execute()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;

    try
    {
      connect();

      // find completed sabre apps that are missing association numbers
      /*@lineinfo:generated-code*//*@lineinfo:245^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ms.app_seq_num          app_seq_num,
//                  upper(ms.primary_pcc)   primary_pcc,
//                  upper(ms.pcc)           pcc
//          from    merchant_sabre  ms,
//                  merchant        m
//          where   ms.app_seq_num = m.app_seq_num and
//                  m.merch_credit_status is not null and
//                  m.merch_credit_status not in (0, 3, 4) and
//                  ms.association is null and
//                  upper(ms.primary_pcc) != upper(ms.pcc)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ms.app_seq_num          app_seq_num,\n                upper(ms.primary_pcc)   primary_pcc,\n                upper(ms.pcc)           pcc\n        from    merchant_sabre  ms,\n                merchant        m\n        where   ms.app_seq_num = m.app_seq_num and\n                m.merch_credit_status is not null and\n                m.merch_credit_status not in (0, 3, 4) and\n                ms.association is null and\n                upper(ms.primary_pcc) != upper(ms.pcc)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.SabreCleanup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.SabreCleanup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:257^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        items.add(new Item(rs));
      }

      rs.close();
      it.close();

      // process apps
      for(int i=0; i<items.size(); ++i)
      {
        processApp((Item)(items.elementAt(i)));
      }
      
      // now assign users for accounts that need them
      items.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:278^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ms.app_seq_num        app_seq_num,
//                  upper(ms.primary_pcc) primary_pcc,
//                  upper(ms.pcc)         pcc
//          from    merchant_sabre  ms,
//                  merchant        m
//          where   ms.app_seq_num = m.app_seq_num and
//                  m.merch_credit_status is not null and
//                  m.merch_credit_status not in (0, 3, 4) and
//                  ms.association is not null and
//                  nvl(ms.chain_user_assigned, 'N') = 'N' and
//                  (ms.primary_pcc is null or upper(ms.pcc) = upper(ms.primary_pcc))
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ms.app_seq_num        app_seq_num,\n                upper(ms.primary_pcc) primary_pcc,\n                upper(ms.pcc)         pcc\n        from    merchant_sabre  ms,\n                merchant        m\n        where   ms.app_seq_num = m.app_seq_num and\n                m.merch_credit_status is not null and\n                m.merch_credit_status not in (0, 3, 4) and\n                ms.association is not null and\n                nvl(ms.chain_user_assigned, 'N') = 'N' and\n                (ms.primary_pcc is null or upper(ms.pcc) = upper(ms.primary_pcc))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.SabreCleanup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.SabreCleanup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        items.add(new Item(rs));
      }
      
      rs.close();
      it.close();
      
      for(int i=0; i<items.size(); ++i)
      {
        addChainUser((Item)(items.elementAt(i)));
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  public class Item
  {
    int     appSeqNum;
    String  primaryPcc;
    String  pcc;

    public Item(ResultSet rs)
    {
      try
      {
        appSeqNum   = rs.getInt("app_seq_num");
        primaryPcc  = rs.getString("primary_pcc");
        pcc         = rs.getString("pcc");
      }
      catch(Exception e)
      {
        logEntry("constructor", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/