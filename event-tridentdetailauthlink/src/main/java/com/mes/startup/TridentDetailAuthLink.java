/*@lineinfo:filename=TridentDetailAuthLink*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TridentDetailAuthLink.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-01-02 10:39:17 -0800 (Thu, 02 Jan 2014) $
  Version            : $Revision: 22049 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class TridentDetailAuthLink extends EventBase
{
  static Logger log = Logger.getLogger(TridentDetailAuthLink.class);
  
  public static final int   CT_ALL      = 0;
  public static final int   CT_VISA     = 1;
  public static final int   CT_MC       = 2;
  public static final int   CT_NON_VMC  = 3;
  
  public static class AuthLookupEntry
  {
    private   String              AuthCode      = null;
    private   long                BatchRecId    = 0L;
    private   String              CardNumber    = null;
    private   int                 DetailIdx     = 0;
    private   long                MerchantId    = 0L;
    private   Date                TranDate      = null;
    private   String              TranID        = null;
    private   String              debitCreditInd        = null;
  
    public AuthLookupEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthCode      = resultSet.getString("auth_code");
      if (AuthCode == null) AuthCode = "      ";
      if( (AuthCode.trim().length() == 0) && "D".equals(resultSet.getString("dci")) )
        AuthCode      = "000000";
      BatchRecId    = resultSet.getLong("batch_rec_id");
      CardNumber    = resultSet.getString("card_number");
      DetailIdx     = resultSet.getInt("detail_idx");
      MerchantId    = resultSet.getLong("merchant_number");
      TranDate      = resultSet.getDate("tran_date");
      TranID        = resultSet.getString("tran_id");
      debitCreditInd= resultSet.getString("dci");
    }
    
    public String getAuthCode()     { return( AuthCode ); }
    public long getBatchRecId()     { return( BatchRecId ); }
    public String getCardNumber()   { return( CardNumber ); }
    public int getDetailIdx()       { return( DetailIdx ); }
    public long getMerchantId()     { return( MerchantId ); }
    public Date getTranDate()       { return( TranDate ); }
    public String getTranID()       { return( TranID ); }
    public String getDCI()          { return( debitCreditInd ); }

  }
  
  public static class ApiAuthLookupEntry
  {
    private   String              AuthCode      = null;
    private   Date                AuthDateBegin = null;
    private   Date                AuthDateEnd   = null;
    private   String              CardNumber    = null;
    private   int                 CurrencyCode  = 0;
    private   long                MerchantId    = 0L;
    private   long                RecId         = 0L;
    private   double              TranAmount    = 0.0;
    private   String              TridentTranId = null;
    private   String              debitCreditInd = null;
    
    public ApiAuthLookupEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthCode      = resultSet.getString("auth_code");
      AuthDateBegin = resultSet.getDate("begin_date");
      AuthDateEnd   = resultSet.getDate("end_date");
      CardNumber    = resultSet.getString("card_number");
      CurrencyCode  = resultSet.getInt("currency_code");
      MerchantId    = resultSet.getLong("merchant_number");
      RecId         = resultSet.getLong("rec_id");
      TranAmount    = resultSet.getDouble("tran_amount");
      TridentTranId = resultSet.getString("trident_tran_id");
      debitCreditInd= resultSet.getString("dci");
    }
    
    public String getAuthCode()       { return( AuthCode ); }
    public Date   getAuthDateBegin()  { return( AuthDateBegin ); }
    public Date   getAuthDateEnd()    { return( AuthDateEnd ); }
    public String getCardNumber()     { return( CardNumber ); }
    public int    getCurrencyCode()   { return( CurrencyCode ); }
    public long   getMerchantId()     { return( MerchantId ); }
    public long   getRecId()          { return( RecId ); }
    public double getTranAmount()     { return( TranAmount ); }
    public String getTridentTranId()  { return( TridentTranId ); }
    public String getDCI()  { return( debitCreditInd ); }
  }
  
  protected   boolean         Verbose     = false;
  
  public boolean execute()
  {
    boolean       retVal      = false;
    
    try
    {
      connect(true);
      
      switch( Integer.parseInt(getEventArg(0)) )
      {
        case mesConstants.MBS_BT_VISAK:
          linkTridentAuths();
          break;
          
        case mesConstants.MBS_BT_PG:
          linkTridentApiAuths();
          break;
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean isStaticAuthCode(String authCode)
  {
    boolean returnVal = false;
    if(!StringUtils.isEmpty(authCode)){
    	returnVal = authCode.equals("TCR001")||authCode.equals("TCV001") ? true : false ;
    }
    	
    return returnVal;
  }
  
  protected void linkTridentAuths()
  {
    String              authCode      = null;
    long                authRecId     = 0L;
    long                batchRecId    = 0L;
    Calendar            cal           = Calendar.getInstance();
    String              cardNumber    = null;
    int                 cardType      = CT_ALL;
    int                 detailIdx     = 0;
    ResultSetIterator   it            = null;
    long                merchantId    = 0L;
    int                 recCount      = 0;
    ResultSet           resultSet     = null;
    AuthLookupEntry     row           = null;
    Vector              rows          = new Vector();
    Date                tranDate      = null;
    String              tranID        = null;
    String              tridentTranId = null;
    String              iccData       = null;  
    String				cardSeqNumber = null;
    String				posEntryMode  = null;
    
    // monitoring variables
    long                beginTs           = 0L;
    long                endTs             = 0L;
    int                 linkedCount       = 0;
    boolean             logging           = false;
    long                maxQueryTime      = 0L;
    long                processedCount    = 0;
    long                selectQueryTime   = 0L;
    Timestamp           startTime         = null;
    long                totalTime         = 0L;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      rows.removeAllElements();
      cardType = Integer.parseInt(getEventArg(1));
      if ( getEventArgCount() > 2 )
      {
        logging = getEventArg(2).toLowerCase().equals("true");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:196^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentDetailAuthLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   startTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/
      
      // select the items that have not been linked to a DDF entry
      beginTs = System.currentTimeMillis();
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tdl.batch_rec_id                  as batch_rec_id,
//                  tdl.debit_credit_indicator        as dci,
//                  tdl.detail_index                  as detail_idx,
//                  tdl.merchant_number               as merchant_number,
//                  tdl.card_number                   as card_number,
//                  tdl.authorization_code            as auth_code,
//                  decode(tdl.transaction_id,
//                         '000000000000000',null,
//                         '111111111111111',null,
//                         '               ',null,
//                         tdl.transaction_id)        as tran_id,
//                  trunc(tdl.transaction_date)       as tran_date
//          from    trident_detail_lookup   tdl
//          where   tdl.auth_rec_id = decode(:cardType,0,-1,0) and
//                  tdl.mesdb_timestamp > sysdate-1 and
//                  (
//                    (:cardType = 0) or
//                    (:cardType = 1 and substr(tdl.card_number,1,1) = '4') or
//                    (:cardType = 2 and substr(tdl.card_number,1,1) = '5') or
//                    (:cardType = 3 and not substr(tdl.card_number,1,1) in ('4','5'))
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tdl.batch_rec_id                  as batch_rec_id,\n                tdl.debit_credit_indicator        as dci,\n                tdl.detail_index                  as detail_idx,\n                tdl.merchant_number               as merchant_number,\n                tdl.card_number                   as card_number,\n                tdl.authorization_code            as auth_code,\n                decode(tdl.transaction_id,\n                       '000000000000000',null,\n                       '111111111111111',null,\n                       '               ',null,\n                       tdl.transaction_id)        as tran_id,\n                trunc(tdl.transaction_date)       as tran_date\n        from    trident_detail_lookup   tdl\n        where   tdl.auth_rec_id = decode( :1 ,0,-1,0) and\n                tdl.mesdb_timestamp > sysdate-1 and\n                (\n                  ( :2  = 0) or\n                  ( :3  = 1 and substr(tdl.card_number,1,1) = '4') or\n                  ( :4  = 2 and substr(tdl.card_number,1,1) = '5') or\n                  ( :5  = 3 and not substr(tdl.card_number,1,1) in ('4','5'))\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,cardType);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setInt(3,cardType);
   __sJT_st.setInt(4,cardType);
   __sJT_st.setInt(5,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TridentDetailAuthLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^7*/
      endTs = System.currentTimeMillis();
      selectQueryTime = (endTs-beginTs);
      log.debug("query time: " + selectQueryTime);//@
      resultSet = it.getResultSet();
      
      // extract the results to temporary storage
      // to reduce the load on the rollback segments
      while( resultSet.next() )
      {
        rows.addElement( new AuthLookupEntry(resultSet) );
      }
      resultSet.close();
      it.close();
      
      log.debug("records to process: " + rows.size());//@
      
      // process the results from the temporary storage
      for( int i = 0; i < rows.size(); ++i )
      {
        int   attempt         = 1;      // default to "no date in tran id"
        Date  authDateBegin   = null;
        Date  authDateEnd     = null;

        row = (AuthLookupEntry)rows.elementAt(i);
        
        batchRecId    = row.getBatchRecId();
        detailIdx     = row.getDetailIdx();
        tranDate      = row.getTranDate();
        merchantId    = row.getMerchantId() ;
        cardNumber    = row.getCardNumber();
        authCode      = row.getAuthCode();
        tranID        = row.getTranID();
        
        boolean isSimulatorResponse = isStaticAuthCode(authCode) && "C".equalsIgnoreCase(row.getDCI());

        if( tranID != null )
        {
          if( tranID.trim().length() < 12 || isSimulatorResponse)
          {
            tranID = null;
            if(isSimulatorResponse) {
            	authCode = "";	
            }
          }
          else
          {
            switch( cardNumber.charAt(0) )
            {
              case '4':   // VISA
                attempt = -1;     // prepare to ignore bad tran ID
                if( tranID.trim().length() == 15 )
                {
                  java.sql.Date sqlDate = null;

                  try
                  {
                    /*@lineinfo:generated-code*//*@lineinfo:279^21*/

//  ************************************************************
//  #sql [Ctx] { select  yddd_to_date(:tranID.substring(2,6)) 
//                        from    dual
//                       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4884 = tranID.substring(2,6);
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  yddd_to_date( :1 )  \n                      from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TridentDetailAuthLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_4884);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sqlDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^21*/
                  }
                  catch( Exception e )
                  {
                  }

                  if ( sqlDate != null )
                  {
                    cal.setTime(sqlDate);
                    attempt = 0;
                  }
                }
                break;

              case '5':   // MC
                attempt = -1;     // prepare to ignore bad tran ID
                String  bankNetDate = TridentTools.decodeBankNetDate(tranID);
                if( bankNetDate != null )
                {
                  // apparently starts with a good date (so use it first time through the for() loop),
                  //    but is not followed by three letter GCMS product id (so don't require a tranID match)
                  if( !Character.isLetter(tranID.charAt(4)) ||
                      !Character.isLetter(tranID.charAt(5)) ||
                      !Character.isLetter(tranID.charAt(6)) )
                  {
                    tranID = null;
                  }

                  cal.setTime( DateTimeFormatter.parseDate((bankNetDate + cal.get(Calendar.YEAR)),"MMddyyyy") );
                  Calendar cal2 = Calendar.getInstance();
                  cal2.add(Calendar.DAY_OF_MONTH,30);
                  if ( cal.after(cal2) )
                  {
                    cal.add(Calendar.YEAR,-1);
                  }
                  attempt = 0;
                }
                break;
            }

            switch( attempt )
            {
              case -1:
                tranID  = null;
                attempt = 1;    // tran id is invalid
                break;

              case 0:
                cal.add(Calendar.DAY_OF_MONTH,-1);
                authDateBegin = new java.sql.Date(cal.getTime().getTime());
                cal.add(Calendar.DAY_OF_MONTH, 2);
                authDateEnd   = new java.sql.Date(cal.getTime().getTime());

                if( authDateBegin == null ) attempt = 1;    // no valid date in tran id
                break;
            }
          }
        }
        
        // reset the auth rec id and the first tran date end
        authRecId = (authCode.trim().length() == 0) ? -2L : 0L;   // if auth code is all spaces, it won't be found; don't bother looking
        cal.setTime(tranDate);
        cal.add(Calendar.DAY_OF_MONTH,2);   // add 2 days in case pos device has wrong date.
        
        // make several attempts to locate the auth
        //  0. tranIdDate-1 => tranIdDate+1    (v/mc only)
        //  1. tranDate-5   => tranDate+2
        //  2. tranDate-15  => tranDate-5
        //  3. tranDate-25  => tranDate-15
        //  4. tranDate-35  => tranDate-25
        for( ; attempt < 5 && authRecId == 0L; ++attempt )
        {
          if ( attempt != 0 )   
          { 
            // setup the date range
            authDateEnd = new java.sql.Date(cal.getTime().getTime());
            cal.add(Calendar.DAY_OF_MONTH,-((attempt == 1) ? 7 : 10) );
            authDateBegin = new java.sql.Date(cal.getTime().getTime());
          }
          
          beginTs = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:364^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.rec_id, 
//                      auth.trident_transaction_id,
//                      auth.icc_request_data,
//                      auth.pos_entry_mode,
//                      auth.card_sequence_number
//              from    tc33_trident    auth
//              where   auth.merchant_number = :merchantId 
//                      and auth.transaction_date between :authDateBegin and :authDateEnd 
//                      and auth.card_number = :cardNumber 
//                      and upper(auth.authorization_code) = upper(:authCode)
//                      and nvl(auth.transaction_id_alpha,'0') = nvl(:tranID,nvl(auth.transaction_id_alpha,'0'))
//                      and auth.transaction_code != '58'   -- ignore card verify
//                      and (:tranID is not null
//                        or not exists
//                      (
//                        select tdl.auth_rec_id
//                        from   trident_detail_lookup  tdl
//                        where  tdl.auth_rec_id = auth.rec_id
//                      ))
//              order by auth.authorized_amount desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.rec_id, \n                    auth.trident_transaction_id,\n                    auth.icc_request_data,\n                    auth.pos_entry_mode,\n                    auth.card_sequence_number\n            from    tc33_trident    auth\n            where   auth.merchant_number =  :1  \n                    and auth.transaction_date between  :2  and  :3  \n                    and auth.card_number =  :4  \n                    and upper(auth.authorization_code) = upper( :5 )\n                    and nvl(auth.transaction_id_alpha,'0') = nvl( :6 ,nvl(auth.transaction_id_alpha,'0'))\n                    and auth.transaction_code != '58'   -- ignore card verify\n                    and ( :7  is not null\n                      or not exists\n                    (\n                      select tdl.auth_rec_id\n                      from   trident_detail_lookup  tdl\n                      where  tdl.auth_rec_id = auth.rec_id\n                    ))\n            order by auth.authorized_amount desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,authDateBegin);
   __sJT_st.setDate(3,authDateEnd);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setString(5,authCode);
   __sJT_st.setString(6,tranID);
   __sJT_st.setString(7,tranID);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TridentDetailAuthLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:386^11*/
          endTs = System.currentTimeMillis();
          if ( (endTs-beginTs) > maxQueryTime ) { maxQueryTime = (endTs-beginTs); }
          totalTime = (endTs-beginTs);
          resultSet = it.getResultSet();
        
          if ( resultSet.next() )
          {
            authRecId     = resultSet.getLong("rec_id");
            tridentTranId = resultSet.getString("trident_transaction_id");
            iccData       = resultSet.getString("icc_request_data");
            cardSeqNumber = resultSet.getString("card_sequence_number");
            posEntryMode  = resultSet.getString("pos_entry_mode");
          
            /*@lineinfo:generated-code*//*@lineinfo:400^13*/

//  ************************************************************
//  #sql [Ctx] { update  trident_detail_lookup
//                set     auth_rec_id = :authRecId,
//                        trident_transaction_id = :tridentTranId,
//                        icc_data = :iccData,
//                        card_sequence_number = :cardSeqNumber,
//                        pos_entry_mode = :posEntryMode
//                where   batch_rec_id = :batchRecId and
//                        detail_index = :detailIdx
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_detail_lookup\n              set     auth_rec_id =  :1 ,\n                      trident_transaction_id =  :2 ,\n                      icc_data =  :3 ,\n                      card_sequence_number =  :4 ,\n                      pos_entry_mode =  :5 \n              where   batch_rec_id =  :6  and\n                      detail_index =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,authRecId);
   __sJT_st.setString(2,tridentTranId);
   __sJT_st.setString(3,iccData);
   __sJT_st.setString(4,cardSeqNumber);
   __sJT_st.setString(5,posEntryMode);
   __sJT_st.setLong(6,batchRecId);
   __sJT_st.setInt(7,detailIdx);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^13*/
            ++linkedCount;

            // if this auth record already linked to another transaction, send batch through batch scanners
            int authRecCnt = 0;
            /*@lineinfo:generated-code*//*@lineinfo:415^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//                
//                from    trident_detail_lookup  tdl
//                where   tdl.auth_rec_id = :authRecId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n               \n              from    trident_detail_lookup  tdl\n              where   tdl.auth_rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TridentDetailAuthLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,authRecId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   authRecCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:421^13*/
            if( authRecCnt != 1 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:424^15*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_batches   mb
//                  set     mb.load_file_id = -1
//                  where   mb.merchant_batch_date >= trunc(sysdate-2)
//                      and mb.batch_id = :batchRecId
//                      and nvl(mb.load_file_id,0) = 0
//                      and mb.load_filename is null
//                      and mb.response_code = 0    -- only GB's
//                      and mb.test_flag = 'N'      -- only prod batches
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_batches   mb\n                set     mb.load_file_id = -1\n                where   mb.merchant_batch_date >= trunc(sysdate-2)\n                    and mb.batch_id =  :1 \n                    and nvl(mb.load_file_id,0) = 0\n                    and mb.load_filename is null\n                    and mb.response_code = 0    -- only GB's\n                    and mb.test_flag = 'N'      -- only prod batches";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^15*/
            }
          }
          resultSet.close();
          it.close();
        }   // end loop through date ranges -5, -15, -25, -35
        
        // did not find a match, put into the background retry
        if ( authRecId == 0L ) authRecId = -1L;
        if ( authRecId < 0L )
        {
          /*@lineinfo:generated-code*//*@lineinfo:445^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_detail_lookup
//              set     auth_rec_id = :authRecId
//              where   batch_rec_id = :batchRecId and
//                      detail_index = :detailIdx
//                  and (:authRecId <> -1                       -- AND not going into background retry
//                       or mesdb_timestamp < (sysdate-0.003))  --     OR already waiting 5 minutes
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_detail_lookup\n            set     auth_rec_id =  :1 \n            where   batch_rec_id =  :2  and\n                    detail_index =  :3 \n                and ( :4  <> -1                       -- AND not going into background retry\n                     or mesdb_timestamp < (sysdate-0.003))  --     OR already waiting 5 minutes";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,authRecId);
   __sJT_st.setLong(2,batchRecId);
   __sJT_st.setInt(3,detailIdx);
   __sJT_st.setLong(4,authRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:453^11*/
        }
        
        ++processedCount;
        if ( Verbose )
        {
          System.out.print("  record count: " + processedCount + "  max query time: " + maxQueryTime + "  this query time: " + totalTime + "  linked: " + linkedCount + "             \r");
        }          
        
        // commit after every 100 rows
        if ( (i % 100) == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:465^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^11*/
        }            
      }
      
      // boundary condition commit
      /*@lineinfo:generated-code*//*@lineinfo:473^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:476^7*/
      
      if ( logging == true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:480^9*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_detail_lookup_log
//            (
//              start_ts,
//              end_ts,
//              record_count,
//              linked_count,
//              select_query_time,
//              lookup_query_time_max,
//              lookup_query_time_avg
//            )
//            values
//            (
//              :startTime,
//              sysdate,
//              :processedCount,
//              :linkedCount,
//              :selectQueryTime,
//              :maxQueryTime,
//              decode( :processedCount, 0, 0, (:totalTime/:processedCount) )
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_detail_lookup_log\n          (\n            start_ts,\n            end_ts,\n            record_count,\n            linked_count,\n            select_query_time,\n            lookup_query_time_max,\n            lookup_query_time_avg\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n            decode(  :6 , 0, 0, ( :7 / :8 ) )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,startTime);
   __sJT_st.setLong(2,processedCount);
   __sJT_st.setInt(3,linkedCount);
   __sJT_st.setLong(4,selectQueryTime);
   __sJT_st.setLong(5,maxQueryTime);
   __sJT_st.setLong(6,processedCount);
   __sJT_st.setLong(7,totalTime);
   __sJT_st.setLong(8,processedCount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:504^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^9*/
      }        
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:512^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:512^34*/ }catch( Exception ee ) {}
      logEntry("linkTridentAuths()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
      cleanUp();
    }
  }
  
  protected void linkTridentApiAuths( )
  {
    String              authCode      = null;
    Date                authDateBegin = null;
    Date                authDateEnd   = null;
    long                authRecId     = 0L;
    String              cardNumber    = null;
    int                 cardType      = CT_ALL;
    int                 currencyCode  = 0;
    ResultSetIterator   it            = null;
    long                merchantId    = 0L;
    long                recId         = 0L;
    ResultSet           resultSet     = null;
    ApiAuthLookupEntry  row           = null;
    Vector              rows          = new Vector();
    double              tranAmount    = 0.0;
    String              tridentTranId = null;
    
    // monitoring variables
    long                beginTs           = 0L;
    long                endTs             = 0L;
    int                 linkedCount       = 0;
    long                maxQueryTime      = 0L;
    double              percentComplete   = 0.0;
    long                processedCount    = 0;
    long                selectQueryTime   = 0L;
    int                 totalCount        = 0;
    long                totalTime         = 0L;
    
    try
    {
      connect(true);
      setAutoCommit(false);
      
      cardType = Integer.parseInt(getEventArg(1));

      //
      //  if the fraud system was used and the result is deny
      //  then the authorization request will occur when the 
      //  merchant elects to ignore the fraud system recommendation.
      //  because the transaction_id (tapi.trident_tran_id) has
      //  already been communicated to the merchant we do not
      //  want to change the value.  the uuid returned in the 
      //  Visa-D message is saved in tapi.original_trident_tran_id
      //  and should be used when attempting to link the transaction
      //  to the authorization record in tc33_trident.
      //  
      /*@lineinfo:generated-code*//*@lineinfo:570^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(tapi idx_tapi_last_modified_date) */ 
//                  tapi.rec_id                                 as rec_id,
//                  tapi.merchant_number                        as merchant_number,
//                  decode(tapi.auth_ref_num, 
//                          null, tapi.transaction_date-35,
//                          tapi.auth_date-1)                   as begin_date,
//                  decode(tapi.auth_ref_num, 
//                          null, tapi.transaction_date+1,
//                          tapi.auth_date+1)                   as end_date,
//                  decode(tapi.auth_ref_num,
//                          null, null,
//                          case
//                            when  tapi.fraud_result = 'DENY' 
//                                  and tapi.original_trident_tran_id is not null
//                            then
//                              tapi.original_trident_tran_id
//                            else
//                              tapi.trident_tran_id 
//                          end                                         )
//                                                              as trident_tran_id,
//                  tapi.card_number                            as card_number,
//                  tapi.transaction_amount                     as tran_amount,
//                  decode( is_number(tapi.currency_code), 
//                           0, 0, tapi.currency_code )         as currency_code,
//                  upper(tapi.auth_code)                       as auth_code
//          from    trident_capture_api     tapi             
//          where   tapi.last_modified_date > sysdate-1
//                  and tapi.mesdb_timestamp > sysdate-1
//                  and nvl(tapi.auth_rec_id,0) = 0
//                  and tapi.auth_code is not null
//                  and tapi.auth_source_code is not null
//                  and nvl(tapi.load_filename,'ok') not like 'processed%'
//                  and nvl(tapi.test_flag,'Y') = 'N'
//                  and
//                  (
//                    (:cardType = 0) or
//                    (:cardType = 1 and tapi.card_type = 'VS') or
//                    (:cardType = 2 and tapi.card_type = 'MC') or
//                    (:cardType = 3 and not tapi.card_type in ('VS','MC'))
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(tapi idx_tapi_last_modified_date) */ \n                tapi.rec_id                                 as rec_id,\n                tapi.merchant_number                        as merchant_number,\n                decode(tapi.auth_ref_num, \n                        null, tapi.transaction_date-35,\n                        tapi.auth_date-1)                   as begin_date,\n                decode(tapi.auth_ref_num, \n                        null, tapi.transaction_date+1,\n                        tapi.auth_date+1)                   as end_date,\n                decode(tapi.auth_ref_num,\n                        null, null,\n                        case\n                          when  tapi.fraud_result = 'DENY' \n                                and tapi.original_trident_tran_id is not null\n                          then\n                            tapi.original_trident_tran_id\n                          else\n                            tapi.trident_tran_id \n                        end                                         )\n                                                            as trident_tran_id,\n                tapi.card_number                            as card_number,\n                tapi.transaction_amount                     as tran_amount,\ntapi.debit_credit_indicator        as dci,\n                decode( is_number(tapi.currency_code), \n                         0, 0, tapi.currency_code )         as currency_code,\n                upper(tapi.auth_code)                       as auth_code\n        from    trident_capture_api     tapi             \n        where   tapi.last_modified_date > sysdate-1\n                and tapi.mesdb_timestamp > sysdate-1\n                and nvl(tapi.auth_rec_id,0) = 0\n                and tapi.auth_code is not null\n                and tapi.auth_source_code is not null\n                and nvl(tapi.load_filename,'ok') not like 'processed%'\n                and nvl(tapi.test_flag,'Y') = 'N'\n                and\n                (\n                  ( :1  = 0) or\n                  ( :2  = 1 and tapi.card_type = 'VS') or\n                  ( :3  = 2 and tapi.card_type = 'MC') or\n                  ( :4  = 3 and not tapi.card_type in ('VS','MC'))\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,cardType);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setInt(3,cardType);
   __sJT_st.setInt(4,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.TridentDetailAuthLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:612^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        rows.addElement( new ApiAuthLookupEntry(resultSet) );
      }
      resultSet.close();
      it.close();
      
      // process the results from the temporary storage
      totalCount = rows.size();
      for( int i = 0; i < rows.size(); ++i )
      {
        row = (ApiAuthLookupEntry)rows.elementAt(i);
        
        authDateEnd   = row.getAuthDateEnd();
        authDateBegin = row.getAuthDateBegin();
        merchantId    = row.getMerchantId() ;
        recId         = row.getRecId();
        tridentTranId = row.getTridentTranId();
        tranAmount    = row.getTranAmount();
        currencyCode  = row.getCurrencyCode();
        authCode      = row.getAuthCode();
        cardNumber    = row.getCardNumber();
        
        if(isStaticAuthCode(authCode) && "C".equalsIgnoreCase(row.getDCI()))
        {
        	authCode = null;
        }
        
        beginTs = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:639^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(:currencyCode,auth.currency_code,0,1)  as diffCurrency,
//                    abs(auth.authorized_amount - :tranAmount)     as diffAmount,
//                    auth.rec_id
//            from    tc33_trident    auth
//            where   auth.merchant_number = :merchantId 
//                    and auth.transaction_date between :authDateBegin and :authDateEnd 
//                    and auth.trident_transaction_id = nvl(:tridentTranId,auth.trident_transaction_id) 
//                    and auth.card_number = :cardNumber 
//                    and upper(auth.authorization_code) = :authCode 
//            order by diffCurrency, diffAmount, auth.transaction_time desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( :1 ,auth.currency_code,0,1)  as diffCurrency,\n                  abs(auth.authorized_amount -  :2 )     as diffAmount,\n                  auth.rec_id\n          from    tc33_trident    auth\n          where   auth.merchant_number =  :3  \n                  and auth.transaction_date between  :4  and  :5  \n                  and auth.trident_transaction_id = nvl( :6 ,auth.trident_transaction_id) \n                  and auth.card_number =  :7  \n                  and upper(auth.authorization_code) =  :8  \n          order by diffCurrency, diffAmount, auth.transaction_time desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,currencyCode);
   __sJT_st.setDouble(2,tranAmount);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,authDateBegin);
   __sJT_st.setDate(5,authDateEnd);
   __sJT_st.setString(6,tridentTranId);
   __sJT_st.setString(7,cardNumber);
   __sJT_st.setString(8,authCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.TridentDetailAuthLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^9*/
        endTs = System.currentTimeMillis();
        if ( (endTs-beginTs) > maxQueryTime ) { maxQueryTime = (endTs-beginTs); }
        totalTime += (endTs-beginTs);
        resultSet = it.getResultSet();
        
        if ( resultSet.next() )
        {
          authRecId     = resultSet.getLong("rec_id");
          
          /*@lineinfo:generated-code*//*@lineinfo:661^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api   
//              set     auth_rec_id = :authRecId
//              where   rec_id = :recId
//                      and mesdb_timestamp > sysdate-1.05
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api   \n            set     auth_rec_id =  :1 \n            where   rec_id =  :2 \n                    and mesdb_timestamp > sysdate-1.05";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.TridentDetailAuthLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,authRecId);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:667^11*/
          ++linkedCount;
        }
        resultSet.close();
        it.close();
        
        ++processedCount;
        if ( Verbose )
        {
          percentComplete = ((double)processedCount/(double)totalCount);
          System.out.print("  record count: " + processedCount + " of " + totalCount + " (" + NumberFormatter.getPercentString(percentComplete,2) + ")  max query time: " + maxQueryTime + "  avg query time: " + (totalTime/processedCount) + "  linked: " + linkedCount + "             \r");
        }          
        
        // commit after every 100 rows
        if ( (i % 100) == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:683^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:686^11*/
        }            
      }
      
      // boundary condition commit
      /*@lineinfo:generated-code*//*@lineinfo:691^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:694^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:698^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:698^34*/ }catch( Exception ee ) {}
      logEntry("linkTridentApiAuths()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
      cleanUp();
    }
  }
  
  public void setVerbose( boolean value )
  {
    Verbose = value;
  }
  
  public static void main( String[] args )
  {
    TridentDetailAuthLink   bc          = null;
    StringBuffer            eventArgs   = new StringBuffer();
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      bc = new TridentDetailAuthLink();
      bc.setVerbose(true);
      
      eventArgs.append( args[0] );    // mesConstants.MBS_BT_???
      eventArgs.append( "," );
      eventArgs.append( (args.length > 1) ? args[1] : String.valueOf(bc.CT_ALL) );
      eventArgs.append( (args.length > 2) ? "," + args[2] : "" );
      bc.setEventArgs( eventArgs.toString() );
      bc.execute();
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/