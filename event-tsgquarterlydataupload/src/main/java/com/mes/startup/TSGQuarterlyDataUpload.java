/*@lineinfo:filename=TSGQuarterlyDataUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class TSGQuarterlyDataUpload extends EventBase
{
  static Logger log = Logger.getLogger(TSGQuarterlyDataUpload.class);
  
  private static final int TSG_NUM_DATA_FIELDS = 81;
  
  private Date     BeginMonth        = null;
  private Date     EndMonth          = null;
  private Vector   ReportRows        = new Vector();
  
  private boolean  testMode          = false;
  
  public class RowData
  {
    public  String[] MerchActivities       = new String[TSG_NUM_DATA_FIELDS];
    
    public RowData( ResultSet rs )
      throws java.sql.SQLException
    {
      MerchActivities[0]   = processString(rs.getString("merchant_number"));           // #1
      MerchActivities[1]   = processString(rs.getString("sic_code"));                  // #2
      MerchActivities[2]   = processString(rs.getString("date_opened"));               // #3
      MerchActivities[3]   = processString(rs.getString("date_activated"));            // #4
      MerchActivities[4]   = processString(rs.getString("state"));                     // #5
      MerchActivities[5]   = processString(rs.getString("zip_code"));                  // #6
      MerchActivities[6]   = processString(rs.getString("ecomm_flag"));                // #7
      MerchActivities[7]   = processString(rs.getString("bankcard_sales_count"));      // #8
      MerchActivities[8]   = getDoubleString( rs.getDouble("bankcard_sales_volume"));  // #9
      
      MerchActivities[9]   = processString(rs.getString("debit_sales_count"));         // #10
      if (MerchActivities[9] == null || "".equals(MerchActivities[9]))
        MerchActivities[9] = "0";
      MerchActivities[10]  = getDoubleString( rs.getDouble("debit_sales_volume"));     // #11
      MerchActivities[11]  = processString(rs.getString("other_sales_count"));         // #12
      if (MerchActivities[11] == null || "".equals(MerchActivities[11]))
        MerchActivities[11] = "0";
      MerchActivities[12]  = getDoubleString( rs.getDouble("other_sales_volume"));     // #13
      if (MerchActivities[12] == null || "".equals(MerchActivities[12]))
        MerchActivities[12] = "0";
      
      // zero out for now
//      MerchActivities[9] = MerchActivities[10] = MerchActivities[11] = MerchActivities[12] = "0.00";
      
      MerchActivities[13]  = processString(rs.getString("chargeback_number"));         // #14
      MerchActivities[14]  = getDoubleString( rs.getDouble("chargeback_amount") );     // #15
      MerchActivities[15]  = getDoubleString( rs.getDouble("bankcard_revenue_discount") +       // #16 = #53 + #59
                                              rs.getDouble("bankcard_revenue_interchange") );

      MerchActivities[16]  = getDoubleString(rs.getDouble("bankcard_revenue_auth_fees"));       // #17 = #54 + #60
      MerchActivities[17]  = getDoubleString( rs.getDouble("bankcard_revenue_assoc_fees") +     // #18 = #55 + #61
                                          rs.getDouble("bankcard_revenue_misc_fees") + 
                                          rs.getDouble("bankcard_revenue_plan_fees") +
                                          rs.getDouble("bankcard_revenue_batchgsf_fees"));      // BATCH FEE & GSF fee

      MerchActivities[18]  = getDoubleString(rs.getDouble("cos_bankcard_ic_fees"));             // #19 = #56 + #62
      MerchActivities[19]  = getDoubleString(rs.getDouble("cos_bankcard_assoc_fees"));          // #20 = #57 + #63
      MerchActivities[20]  = getDoubleString(rs.getDouble("cos_bankcard_other_fees"));          // #21 = #58 + #64
      MerchActivities[21]  = getDoubleString(rs.getDouble("pin_debit_discount"));      // #22
      MerchActivities[22]  = getDoubleString(rs.getDouble("pin_debit_tran_fees"));     // #23
      MerchActivities[23]  = getDoubleString(rs.getDouble("pin_debit_other_fees"));    // #24
      MerchActivities[24]  = getDoubleString(rs.getDouble("pin_debit_ic_fees"));       // #25
      MerchActivities[25]  = getDoubleString(rs.getDouble("pin_debit_switch_fees"));   // #26
      
      // zero out for now
//      MerchActivities[21] = MerchActivities[22] = MerchActivities[23] = MerchActivities[24] = 
//      MerchActivities[25] = "0.00";
      
      MerchActivities[26]  = getDoubleString(rs.getDouble("monthly_account_fees"));    // #27
      MerchActivities[27]  = getDoubleString(rs.getDouble("annual_account_fees"));     // #28
      MerchActivities[28]  = getDoubleString(rs.getDouble("monthly_pci"));             // #29
      MerchActivities[29]  = getDoubleString(rs.getDouble("annual_pci"));              // #30
      MerchActivities[30]  = getDoubleString(rs.getDouble("monthly_insurance"));       // #31
      MerchActivities[31]  = getDoubleString(rs.getDouble("annual_insurance"));        // #32
      MerchActivities[32]  = getDoubleString(rs.getDouble("monthly_1099_reporting"));  // #33
      MerchActivities[33]  = getDoubleString(rs.getDouble("annual_1099_reporting"));   // #34
      MerchActivities[34]  = getDoubleString(rs.getDouble("equipment_rental"));        // #35
      MerchActivities[35]  = getDoubleString(rs.getDouble("equipment_lease"));         // #36 
      MerchActivities[36]  = getDoubleString(rs.getDouble("equipment_sale"));          // #37
      MerchActivities[37]  = getDoubleString(rs.getDouble("bankcard_revenue_other_fees") -  // #38
                                           rs.getDouble("bankcard_revenue_misc_fees") - 
                                           rs.getDouble("bankcard_revenue_plan_fees") -
                                           rs.getDouble("equipment_rental") -
                                           rs.getDouble("equipment_lease") -
                                           rs.getDouble("equipment_sale") );
      MerchActivities[38]  = getDoubleString(rs.getDouble("other_cos_gateway_fees"));       // #39 
      MerchActivities[39]  = getDoubleString(rs.getDouble("other_cos_processor_fees"));     // #40 
      MerchActivities[40]  = getDoubleString(rs.getDouble("other_cos_sponsor_bank_fees"));  // #41 
      MerchActivities[41]  = processString(rs.getString("residual_1099_agent"));            // #42 
      MerchActivities[42]  = processString(rs.getString("residual_bank"));                  // #43 
      MerchActivities[43]  = processString(rs.getString("residual_refrl_partner"));         // #44 
      MerchActivities[44]  = processString(rs.getString("residual_other_refrl_partners"));  // #45 
      MerchActivities[45]  = processString(rs.getString("sales_model_person_id"));          // #46 
      MerchActivities[46]  = processString(rs.getString("sales_model"));                    // #47 
      MerchActivities[47]  = processString(rs.getString("report_month"));                   // #48
      MerchActivities[48]  = String.valueOf( rs.getLong("bankcard_sales_count") -           // #49
                                           rs.getLong("sig_debit_sales_count") );
      MerchActivities[49]  = getDoubleString( rs.getDouble("bankcard_sales_volume") -       // #50
                                           rs.getDouble("sig_debit_sales_volume") );
      MerchActivities[50]  = processString(rs.getString("sig_debit_sales_count"));          // #51
      MerchActivities[51]  = getDoubleString(rs.getDouble("sig_debit_sales_volume"));       // #52
      MerchActivities[52]  = getDoubleString( rs.getDouble("bankcard_revenue_discount") +   // #53 = #16 - #59
                                           rs.getDouble("bankcard_revenue_interchange") - 
                                           rs.getDouble("sig_debit_revenue_discount") -
                                           rs.getDouble("sig_debit_revenue_interchange") ); // point 1
      MerchActivities[53]  = MerchActivities[16];                                               // #54 = #17
      MerchActivities[54]  = MerchActivities[17];                                               // #55 = #18
      MerchActivities[55]  = getDoubleString( rs.getDouble("cos_bankcard_ic_fees") -            // #56 = #19 - #62
                                              rs.getDouble("cos_sig_debit_ic_fees") );                  // point 2
      MerchActivities[56]  = MerchActivities[19];                                               // #57 = #20
      MerchActivities[57]  = MerchActivities[20];                                               // #58 = #21
      MerchActivities[58]  = getDoubleString( rs.getDouble("sig_debit_revenue_discount") +      // #59
                                           rs.getDouble("sig_debit_revenue_interchange") );  // point 1                                                            
      MerchActivities[59]  = "0.00";                                                            // #60
      MerchActivities[60]  = "0.00";                                                            // #61
      MerchActivities[61]  = getDoubleString(rs.getDouble("cos_sig_debit_ic_fees"));            // #62  // point 2
      if (MerchActivities[61] == null || "".equals(MerchActivities[61]))
        MerchActivities[61] = "0";
      MerchActivities[62]  = "0.00";                                                            // #63
      MerchActivities[63]  = "0.00";                                                            // #64
      
      // zero out for now      
//      MerchActivities[48] = MerchActivities[49] = MerchActivities[50] = MerchActivities[51] =  
//      MerchActivities[52] = MerchActivities[53] = MerchActivities[54] = MerchActivities[55] =
//      MerchActivities[56] = MerchActivities[57] = MerchActivities[58] = MerchActivities[59] =
//      MerchActivities[60] = MerchActivities[61] = MerchActivities[62] = MerchActivities[63] =  "0.00";

      MerchActivities[64]  = "0";                                                               // #65
      MerchActivities[65]  = processString(rs.getString("ecomm_only"));                         // #66
      MerchActivities[66]  = "0.00";                                                            // #67
      
      // zero out 68-81 for now
      MerchActivities[67] = MerchActivities[68] = MerchActivities[69] = MerchActivities[70] =  
      MerchActivities[71] = "0"; 
      MerchActivities[72] = "0";
      MerchActivities[73] = MerchActivities[74] =
      MerchActivities[75] = MerchActivities[76] = MerchActivities[77] = MerchActivities[78] =
      MerchActivities[79] = "0.0";
      MerchActivities[80] =  "0";
    }
    
    public String[] getMerchActivities() { return MerchActivities; }
  }
  
  public TSGQuarterlyDataUpload()
  {
  }
  
  public void setReportBeginMonth( Date beginMonth )
  {
    BeginMonth = beginMonth;
  }
  
  public void setReportEndMonth( Date endMonth )
  {
    EndMonth = endMonth;
  }
  
  private String getDoubleString( double amount )
  {
    String retVal = "0.00";
    try
    {
      retVal = NumberFormatter.getDoubleString(amount, "########0.00");
    }
    catch( Exception ee )
    {
    }
    return retVal;
  }
  
  private String buildDatFile()
  {
    String   delimChar        = "|";
    String   lineEnd          = "\r\n";
    RowData  row              = null;
    String   line             = null;
    StringBuffer buff         = new StringBuffer();
    StringBuffer filename     = new StringBuffer();
    BufferedWriter outFile    = null;
    
    //log.debug("buildDatFile()");
    try
    {
      filename.append("tsg_report_");
      filename.append(DateTimeFormatter.getFormattedDate(BeginMonth, "MMyyyy") );
      filename.append("_to_");
      filename.append(DateTimeFormatter.getFormattedDate(EndMonth, "MMyyyy") );
      filename.append(".dat");

      log.info("BUILD data file: " + filename);
      outFile = new BufferedWriter(new FileWriter(filename.toString(), true));
      for( int i = 0; i < ReportRows.size(); i++ )
      {
        row = (RowData)ReportRows.elementAt(i);
        String[] merData = row.getMerchActivities();
        
        buff.setLength(0);
        for( int j = 0; j < merData.length; j++ )
        {
          buff.append( j == 0 ? "" : delimChar );
          buff.append( merData[j] );
//          if( merData[j] == null || merData[j].equals("null") )
//          {
//            log.debug(merData[0] + " index " + j);
//          }
        }
        
        buff.append(lineEnd);
        outFile.write(buff.toString());
//        outFile.newLine();
      }
    }
    catch( Exception ee )
    {
      ee.printStackTrace();
    }
    finally
    {
      try{ outFile.close(); }catch(Exception e){}
    }
    
    return filename.toString();
  }
  
  protected void setReportDateRange()
  {
    if( BeginMonth != null && EndMonth != null )
    {
      return; 
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:253^7*/

//  ************************************************************
//  #sql [Ctx] { select  add_months(trunc(sysdate,'month'), -1), 
//                  last_day(add_months(trunc(sysdate,'month'), -1))
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  add_months(trunc(sysdate,'month'), -1), \n                last_day(add_months(trunc(sysdate,'month'), -1))\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TSGQuarterlyDataUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   BeginMonth = (java.sql.Date)__sJT_rs.getDate(1);
   EndMonth = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^7*/
    }
    catch(Exception e)
    {
      logEntry("setReportDateRange() ", e.toString());
    }
  }
  
  protected boolean loadData() throws Exception
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;
    //log.debug("loadData()");

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:277^7*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number                                      as merchant_number,
//                 mf.sic_code                                             as sic_code,
//                 to_char(to_date(mf.date_opened,'mmddyy'),'yyyy-mm-dd')  as date_opened,
//                 to_char(mr.date_activated,'yyyy-mm-dd')                 as date_activated,
//                 mf.state1_line_4                                        as state,
//                 mf.zip1_line_4                                          as zip_code, 
//                 decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1 )    as ecomm_flag,
//                 sum( (select sum(pl_number_of_sales - pl_number_of_credits)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS'))
//                 )                                                       as bankcard_sales_count,             --  8
//                 sum( (select sum(pl_sales_amount - pl_credits_amount)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS'))
//                 )                                                       as bankcard_sales_volume,            --  9
//                 sum( (select sum(pl_number_of_sales - pl_number_of_credits)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type = 'DEBC' )
//                 )                                                       as debit_sales_count,                -- 10
//                 sum( (select sum(pl_sales_amount - pl_credits_amount)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type = 'DEBC' )
//                 )                                                       as debit_sales_volume,               -- 11
//                 sum( (select sum(pl_number_of_sales - pl_number_of_credits)
//                       from   monthly_extract_pl
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type not in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS','DEBC')) 
//                 )                                                       as other_sales_count,                -- 12
//                 sum( (select sum(pl_sales_amount - pl_credits_amount)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type not in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS','DEBC')) 
//                 )                                                       as other_sales_volume,               -- 13
//                 sum(sm.chargeback_vol_count)                                 as chargeback_number,           -- 14
//                 sum(sm.chargeback_vol_amount)                                as chargeback_amount,           -- 15
//                 sum( (select sum(pl_correct_disc_amt)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS') )
//                 )                                                       as bankcard_revenue_discount,        -- 16
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'IC')
//                 )                                                       as bankcard_revenue_interchange,     -- 16
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                         and item_category = 'AUTH' 
//                         and st_statement_desc not like '%AMERICAN EXPRESS%'
//                         and st_statement_desc not like '%DEBIT%'
//                         and st_statement_desc not like '%DB AUTHORIZATION FEE%')  
//                 )                                                       as bankcard_revenue_auth_fees,       -- 17
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'ASSOC')
//                 )                                                       as bankcard_revenue_assoc_fees,      -- 18
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'CG-MISC' 
//                              and st_statement_desc like '%VISA%')
//                 )                                                       as bankcard_revenue_misc_fees,       -- 18
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'PLAN' 
//                              and st_statement_desc not like '%AMERICAN EXPRESS%'
//                              and st_statement_desc not like '%DEBIT%')
//                 )                                                       as bankcard_revenue_plan_fees,       -- 18
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'MISC' 
//                              and (st_statement_desc like '%BATCH FEE%' or st_statement_desc like '%GSF%'))
//                 )                                                       as bankcard_revenue_batchgsf_fees,   -- 18
//                 sum( sm.visa_interchange + sm.mc_interchange + sm.disc_interchange )
//                                                                         as cos_bankcard_ic_fees,             -- 19
//                 sum( sm.vmc_assessment_expense )                        as cos_bankcard_assoc_fees,          -- 20 // does it include both assoc fees and assess? Rob S.: yes, it does
//                 0.00                                                    as cos_bankcard_other_fees,          -- 21 all fees are included in assessment fee
//  			   0.00                                                    as pin_debit_discount,				-- 22 PinDebit IC
//  			   sm.tot_inc_debit                                        as pin_debit_tran_fees,              -- 23 already in field 24 // should be able to break out to: auth fee + discount fee
//  			   sum(sm.debit_fees)                                      as pin_debit_other_fees,             -- 24
//  			   dt.pin_debit_ic_fees		                               as pin_debit_ic_fees,                -- 25
//                 0.00                                                    as pin_debit_switch_fees,            -- 26
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'CG-MEM'
//                              and st_statement_desc  not in ('ANNUAL ADMINISTRATIVE MAINTENANCE'))
//                 )                                                       as monthly_account_fees,             -- 27
//                 0.00                                                    as annual_account_fees,              -- 28
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'CG-PCI')
//                 )                                                       as monthly_pci,                      -- 29
//                 0.00                                                    as annual_pci,                       -- 30
//                 0.00                                                    as monthly_insurance,                -- 31
//                 0.00                                                    as annual_insurance,                 -- 32
//                 0.00                                                    as monthly_1099_reporting,           -- 33
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'CG-MEM'
//                              and st_statement_desc = 'ANNUAL ADMINISTRATIVE MAINTENANCE')
//                 )                                                       as annual_1099_reporting,            -- 34
//                 sum(nvl(sm.equip_rental_income,0))                      as equipment_rental,                 -- 35
//                 0.00                                                    as equipment_lease,                  -- 36
//                sum(nvl(sm.equip_sales_income,0))                        as equipment_sale,                   -- 37
//                sum( (select sum(nvl(st_fee_amount,0))
//                      from   monthly_extract_st             
//                      where  hh_load_sec = gn.hh_load_sec
//                             and item_category not in ('AUTH','IC','DEBIT','ASSOC','CG-PCI','CG-MEM'))
//                 )                                                       as bankcard_revenue_other_fees,      -- 38
//                 0.00                                                    as other_cos_gateway_fees,           -- 39
//                 0.00                                                    as other_cos_processor_fees,         -- 40
//                 0.00                                                    as other_cos_sponsor_bank_fees,      -- 41
//                 0.00                                                    as residual_1099_agent,              -- 42
//                 0.00                                                    as residual_bank,                    -- 43
//                 0.00                                                    as residual_refrl_partner,           -- 44
//                 0.00                                                    as residual_other_refrl_partners,    -- 45
//                 '000000'                                                as sales_model_person_id,            -- 46
//                 -- channel name
//                 case
//                   when pn.group_name = 'NetSuite' and mf.merchant_number > 941000091988
//                   then 'C'  --'Inside Sales'
//                   when pn.group_name = 'NetSuite' and mf.merchant_number <= 941000091988
//                   then 'F'  --'National Sales'
//                   when pn.group_name = 'MES E-Commerce Direct' and mf.merchant_number <= 941000091988
//                   then 'C'  --'Inside Sales' 
//                   when pn.group_name = 'MES E-Commerce Direct' and mf.merchant_number > 941000091988
//                   then 'F'  --'National Sales'
//                   when mf.bank_number = 3858
//                   then 'D'  --'SE Sales'
//                   when mf.bank_number || mf.group_2_association = 3943400010
//                   then 'D'  --'Agent Banks'
//                   when mf.bank_number || mf.group_2_association = 3003400010
//                   then 'D'  --'SVB'
//                   when mf.bank_number || mf.group_2_association = 3942400001
//                   then 'D'  --'SSB'
//                   when mf.bank_number || mf.group_2_association = 3941400193
//                   then 'A'  --'ISO and Portfolio Acquisition'
//                   when mf.bank_number || mf.group_2_association = 3941400188
//                   then 'F'  --'National Sales'
//                   when mf.bank_number || mf.group_2_association = 3943400245
//                   then 'C'  --'Inside Sales'
//                   when mf.bank_number || mf.group_2_association in (3941499999, 3941400187, 3941400191, 3941400197, 3941400192)
//                   then 'D'  --'Agent Banks'
//                   when cll.channel_2013 = 'Tele Sales'
//                   then 'C'  --'Inside Sales'
//                   when cll.channel_2013 = 'Direct Sales'
//                   then 'F'  --'National Sales'
//                   when cll.channel_2013 is null and pn.group_name = 'TeleSales Direct'
//                   then 'C'  --'Inside Sales'
//                   when cll.channel_2013 in ('Agent Bank', 'Agent Banks', 'SE Sales')
//                   then 'D'
//                   when cll.channel_2013 in ('Independent Contractors', 'ISO and Portfolio Acquisiton', 'ISO and Portfolio Acquisition')
//                   then 'A'
//                   else 'G' --cll.channel_2013 is null
//                 end                                                     as sales_model,                      -- 47
//                 to_char(sm.active_date,'MMyyyy')                        as report_month,                     -- 48
//                 mds.sig_debit_sales_count                               as sig_debit_sales_count,            -- 49 - 52
//                 mds.sig_debit_sales_volume                              as sig_debit_sales_volume,
//                 mds.sig_debit_ic_expense                                as cos_sig_debit_ic_fees,
//                 sum( (select sum(st_fee_amount)
//                       from   monthly_extract_st             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and item_category = 'IC' 
//                              and (st_statement_desc like '%DEBIT%' or 
//                                   st_statement_desc like '%DB%'))
//                 )                                                       as sig_debit_revenue_interchange,    -- 53 // find better way to id sig debit i/c based on ic rate table
//                 sum( (select sum(pl_correct_disc_amt)
//                       from   monthly_extract_pl             
//                       where  hh_load_sec = gn.hh_load_sec
//                              and pl_plan_type in ('MCDB','VSDB')) )     as sig_debit_revenue_discount,       -- 53
//                 decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1)     as ecomm_only                        -- 66
//          from   mif mf,
//                 merchant mr,
//                 channel_link_2013 cll,
//                 group_names pn,
//                 monthly_extract_summary sm,
//                 monthly_extract_gn gn,
//                 ( -- Signature debit cards: sales count/volume/ic expense
//                   select /*+ parallel(10) */ mbs.merchant_number        as merchant_number,
//                          trunc(mbs.activity_date,'month')               as activity_month,  
//                          sum(mbs.sales_count - mbs.credits_count)       as sig_debit_sales_count,  
//                          sum(mbs.sales_amount - mbs.credits_amount)     as sig_debit_sales_volume,  
//                          sum(mbs.expense)                               as sig_debit_ic_expense  
//                   from   mbs_daily_summary mbs              
//                   where  mbs.activity_date between :BeginMonth and last_day(:EndMonth)  
//                          and (mbs.item_subclass, mbs.ic_cat) in (select card_type, ic_code from sig_debit_ic_codes)
//                   group by mbs.merchant_number,  trunc(mbs.activity_date,'month')
//                 ) mds,
//                 ( -- pin debit ic expense
//                   select /*+ parallel(10) */ merchant_account_number    as merchant_number,
//                          trunc(batch_date,'month')                      as batch_month,
//                          sum(ic_expense)                                as pin_debit_ic_fees
//                   from   daily_detail_file_dt
//                   where  card_type = 'DB'
//                          and batch_date between :BeginMonth and last_day(:EndMonth)
//                   group by merchant_account_number, trunc(batch_date,'month')
//                 ) dt
//          where  mr.merch_number = mf.merchant_number
//                 and mf.bank_number||mf.group_2_association = cll.hierarchy_node(+)
//                 and mf.bank_number||mf.group_2_association = pn.group_number(+)    
//                 and sm.active_date between :BeginMonth and :EndMonth
//                 and sm.merchant_number = mf.merchant_number
//                 and gn.hh_load_sec(+) = sm.hh_load_sec
//                 and mds.merchant_number(+) = sm.merchant_number
//                 and dt.merchant_number(+) = sm.merchant_number
//                 and mds.activity_month(+) = sm.active_date
//                 and dt.batch_month(+) = sm.active_date
//          group by mf.merchant_number, mf.sic_code, to_char(to_date(mf.date_opened,'mmddyy'),'yyyy-mm-dd'),
//                 to_char(mr.date_activated,'yyyy-mm-dd'), mf.state1_line_4, mf.zip1_line_4,
//                 decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1 ),  mf.bank_number, 
//                 mf.group_2_association, pn.group_name, cll.channel_2013, sm.active_date, sm.tot_inc_debit,
//                 mds.sig_debit_sales_count, mds.sig_debit_sales_volume, mds.sig_debit_ic_expense, mf.debit_pass_through,
//                 dt.pin_debit_ic_fees
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number                                      as merchant_number,\n               mf.sic_code                                             as sic_code,\n               to_char(to_date(mf.date_opened,'mmddyy'),'yyyy-mm-dd')  as date_opened,\n               to_char(mr.date_activated,'yyyy-mm-dd')                 as date_activated,\n               mf.state1_line_4                                        as state,\n               mf.zip1_line_4                                          as zip_code, \n               decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1 )    as ecomm_flag,\n               sum( (select sum(pl_number_of_sales - pl_number_of_credits)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS'))\n               )                                                       as bankcard_sales_count,             --  8\n               sum( (select sum(pl_sales_amount - pl_credits_amount)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS'))\n               )                                                       as bankcard_sales_volume,            --  9\n               sum( (select sum(pl_number_of_sales - pl_number_of_credits)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type = 'DEBC' )\n               )                                                       as debit_sales_count,                -- 10\n               sum( (select sum(pl_sales_amount - pl_credits_amount)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type = 'DEBC' )\n               )                                                       as debit_sales_volume,               -- 11\n               sum( (select sum(pl_number_of_sales - pl_number_of_credits)\n                     from   monthly_extract_pl\n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type not in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS','DEBC')) \n               )                                                       as other_sales_count,                -- 12\n               sum( (select sum(pl_sales_amount - pl_credits_amount)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type not in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS','DEBC')) \n               )                                                       as other_sales_volume,               -- 13\n               sum(sm.chargeback_vol_count)                                 as chargeback_number,           -- 14\n               sum(sm.chargeback_vol_amount)                                as chargeback_amount,           -- 15\n               sum( (select sum(pl_correct_disc_amt)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type in ('MC','MCBS','MCDB','VS','VISA','VSBS','VSDB','DS') )\n               )                                                       as bankcard_revenue_discount,        -- 16\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'IC')\n               )                                                       as bankcard_revenue_interchange,     -- 16\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                       and item_category = 'AUTH' \n                       and st_statement_desc not like '%AMERICAN EXPRESS%'\n                       and st_statement_desc not like '%DEBIT%'\n                       and st_statement_desc not like '%DB AUTHORIZATION FEE%')  \n               )                                                       as bankcard_revenue_auth_fees,       -- 17\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'ASSOC')\n               )                                                       as bankcard_revenue_assoc_fees,      -- 18\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'CG-MISC' \n                            and st_statement_desc like '%VISA%')\n               )                                                       as bankcard_revenue_misc_fees,       -- 18\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'PLAN' \n                            and st_statement_desc not like '%AMERICAN EXPRESS%'\n                            and st_statement_desc not like '%DEBIT%')\n               )                                                       as bankcard_revenue_plan_fees,       -- 18\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st\n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'MISC' \n                            and (st_statement_desc like '%BATCH FEE%' or st_statement_desc like '%GSF%'))\n               )                                                       as bankcard_revenue_batchgsf_fees,   -- 18\n               sum( sm.visa_interchange + sm.mc_interchange + sm.disc_interchange )\n                                                                       as cos_bankcard_ic_fees,             -- 19\n               sum( sm.vmc_assessment_expense )                        as cos_bankcard_assoc_fees,          -- 20 // does it include both assoc fees and assess:1 Rob S.: yes, it does\n               0.00                                                    as cos_bankcard_other_fees,          -- 21 all fees are included in assessment fee\n\t\t\t   0.00                                                    as pin_debit_discount,\t\t\t\t-- 22 PinDebit IC\n\t\t\t   sm.tot_inc_debit                                        as pin_debit_tran_fees,              -- 23 already in field 24 // should be able to break out to: auth fee + discount fee\n\t\t\t   sum(sm.debit_fees)                                      as pin_debit_other_fees,             -- 24\n\t\t\t   dt.pin_debit_ic_fees\t\t                               as pin_debit_ic_fees,                -- 25\n               0.00                                                    as pin_debit_switch_fees,            -- 26\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'CG-MEM'\n                            and st_statement_desc  not in ('ANNUAL ADMINISTRATIVE MAINTENANCE'))\n               )                                                       as monthly_account_fees,             -- 27\n               0.00                                                    as annual_account_fees,              -- 28\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'CG-PCI')\n               )                                                       as monthly_pci,                      -- 29\n               0.00                                                    as annual_pci,                       -- 30\n               0.00                                                    as monthly_insurance,                -- 31\n               0.00                                                    as annual_insurance,                 -- 32\n               0.00                                                    as monthly_1099_reporting,           -- 33\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'CG-MEM'\n                            and st_statement_desc = 'ANNUAL ADMINISTRATIVE MAINTENANCE')\n               )                                                       as annual_1099_reporting,            -- 34\n               sum(nvl(sm.equip_rental_income,0))                      as equipment_rental,                 -- 35\n               0.00                                                    as equipment_lease,                  -- 36\n              sum(nvl(sm.equip_sales_income,0))                        as equipment_sale,                   -- 37\n              sum( (select sum(nvl(st_fee_amount,0))\n                    from   monthly_extract_st             \n                    where  hh_load_sec = gn.hh_load_sec\n                           and item_category not in ('AUTH','IC','DEBIT','ASSOC','CG-PCI','CG-MEM'))\n               )                                                       as bankcard_revenue_other_fees,      -- 38\n               0.00                                                    as other_cos_gateway_fees,           -- 39\n               0.00                                                    as other_cos_processor_fees,         -- 40\n               0.00                                                    as other_cos_sponsor_bank_fees,      -- 41\n               0.00                                                    as residual_1099_agent,              -- 42\n               0.00                                                    as residual_bank,                    -- 43\n               0.00                                                    as residual_refrl_partner,           -- 44\n               0.00                                                    as residual_other_refrl_partners,    -- 45\n               '000000'                                                as sales_model_person_id,            -- 46\n               -- channel name\n               case\n                 when pn.group_name = 'NetSuite' and mf.merchant_number > 941000091988\n                 then 'C'  --'Inside Sales'\n                 when pn.group_name = 'NetSuite' and mf.merchant_number <= 941000091988\n                 then 'F'  --'National Sales'\n                 when pn.group_name = 'MES E-Commerce Direct' and mf.merchant_number <= 941000091988\n                 then 'C'  --'Inside Sales' \n                 when pn.group_name = 'MES E-Commerce Direct' and mf.merchant_number > 941000091988\n                 then 'F'  --'National Sales'\n                 when mf.bank_number = 3858\n                 then 'D'  --'SE Sales'\n                 when mf.bank_number || mf.group_2_association = 3943400010\n                 then 'D'  --'Agent Banks'\n                 when mf.bank_number || mf.group_2_association = 3003400010\n                 then 'D'  --'SVB'\n                 when mf.bank_number || mf.group_2_association = 3942400001\n                 then 'D'  --'SSB'\n                 when mf.bank_number || mf.group_2_association = 3941400193\n                 then 'A'  --'ISO and Portfolio Acquisition'\n                 when mf.bank_number || mf.group_2_association = 3941400188\n                 then 'F'  --'National Sales'\n                 when mf.bank_number || mf.group_2_association = 3943400245\n                 then 'C'  --'Inside Sales'\n                 when mf.bank_number || mf.group_2_association in (3941499999, 3941400187, 3941400191, 3941400197, 3941400192)\n                 then 'D'  --'Agent Banks'\n                 when cll.channel_2013 = 'Tele Sales'\n                 then 'C'  --'Inside Sales'\n                 when cll.channel_2013 = 'Direct Sales'\n                 then 'F'  --'National Sales'\n                 when cll.channel_2013 is null and pn.group_name = 'TeleSales Direct'\n                 then 'C'  --'Inside Sales'\n                 when cll.channel_2013 in ('Agent Bank', 'Agent Banks', 'SE Sales')\n                 then 'D'\n                 when cll.channel_2013 in ('Independent Contractors', 'ISO and Portfolio Acquisiton', 'ISO and Portfolio Acquisition')\n                 then 'A'\n                 else 'G' --cll.channel_2013 is null\n               end                                                     as sales_model,                      -- 47\n               to_char(sm.active_date,'MMyyyy')                        as report_month,                     -- 48\n               mds.sig_debit_sales_count                               as sig_debit_sales_count,            -- 49 - 52\n               mds.sig_debit_sales_volume                              as sig_debit_sales_volume,\n               mds.sig_debit_ic_expense                                as cos_sig_debit_ic_fees,\n               sum( (select sum(st_fee_amount)\n                     from   monthly_extract_st             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and item_category = 'IC' \n                            and (st_statement_desc like '%DEBIT%' or \n                                 st_statement_desc like '%DB%'))\n               )                                                       as sig_debit_revenue_interchange,    -- 53 // find better way to id sig debit i/c based on ic rate table\n               sum( (select sum(pl_correct_disc_amt)\n                     from   monthly_extract_pl             \n                     where  hh_load_sec = gn.hh_load_sec\n                            and pl_plan_type in ('MCDB','VSDB')) )     as sig_debit_revenue_discount,       -- 53\n               decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1)     as ecomm_only                        -- 66\n        from   mif mf,\n               merchant mr,\n               channel_link_2013 cll,\n               group_names pn,\n               monthly_extract_summary sm,\n               monthly_extract_gn gn,\n               ( -- Signature debit cards: sales count/volume/ic expense\n                 select /*+ parallel(10) */ mbs.merchant_number        as merchant_number,\n                        trunc(mbs.activity_date,'month')               as activity_month,  \n                        sum(mbs.sales_count - mbs.credits_count)       as sig_debit_sales_count,  \n                        sum(mbs.sales_amount - mbs.credits_amount)     as sig_debit_sales_volume,  \n                        sum(mbs.expense)                               as sig_debit_ic_expense  \n                 from   mbs_daily_summary mbs              \n                 where  mbs.activity_date between  :2  and last_day( :3 )  \n                        and (mbs.item_subclass, mbs.ic_cat) in (select card_type, ic_code from sig_debit_ic_codes)\n                 group by mbs.merchant_number,  trunc(mbs.activity_date,'month')\n               ) mds,\n               ( -- pin debit ic expense\n                 select /*+ parallel(10) */ merchant_account_number    as merchant_number,\n                        trunc(batch_date,'month')                      as batch_month,\n                        sum(ic_expense)                                as pin_debit_ic_fees\n                 from   daily_detail_file_dt\n                 where  card_type = 'DB'\n                        and batch_date between  :4  and last_day( :5 )\n                 group by merchant_account_number, trunc(batch_date,'month')\n               ) dt\n        where  mr.merch_number = mf.merchant_number\n               and mf.bank_number||mf.group_2_association = cll.hierarchy_node(+)\n               and mf.bank_number||mf.group_2_association = pn.group_number(+)    \n               and sm.active_date between  :6  and  :7 \n               and sm.merchant_number = mf.merchant_number\n               and gn.hh_load_sec(+) = sm.hh_load_sec\n               and mds.merchant_number(+) = sm.merchant_number\n               and dt.merchant_number(+) = sm.merchant_number\n               and mds.activity_month(+) = sm.active_date\n               and dt.batch_month(+) = sm.active_date\n        group by mf.merchant_number, mf.sic_code, to_char(to_date(mf.date_opened,'mmddyy'),'yyyy-mm-dd'),\n               to_char(mr.date_activated,'yyyy-mm-dd'), mf.state1_line_4, mf.zip1_line_4,\n               decode( nvl(mf.ecommerce_flag,'N'), 'N', 0, 'Y', 1 ),  mf.bank_number, \n               mf.group_2_association, pn.group_name, cll.channel_2013, sm.active_date, sm.tot_inc_debit,\n               mds.sig_debit_sales_count, mds.sig_debit_sales_volume, mds.sig_debit_ic_expense, mf.debit_pass_through,\n               dt.pin_debit_ic_fees";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TSGQuarterlyDataUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,BeginMonth);
   __sJT_st.setDate(2,EndMonth);
   __sJT_st.setDate(3,BeginMonth);
   __sJT_st.setDate(4,EndMonth);
   __sJT_st.setDate(5,BeginMonth);
   __sJT_st.setDate(6,EndMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TSGQuarterlyDataUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^7*/
      resultSet = it.getResultSet();
    
      //log.debug("ReportRows.addElement");
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      retVal = false;
      logEntry("loadData()", e.toString());
      throw e;
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      //cleanUp();
    }
    
    return( retVal );
  }
  
  public boolean execute()
  {
    try
    {
      connect(true);

      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.MONTH, -1);
      cal.set(Calendar.DATE, 1);
      java.sql.Date firstDateOfPreviousMonth = new java.sql.Date(cal.getTime().getTime());
      cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
      java.sql.Date lastDateOfPreviousMonth = new java.sql.Date(cal.getTime().getTime());

      log.info("Start time: " + cal.getInstance().getTime().toString());
      setReportBeginMonth(firstDateOfPreviousMonth);
      setReportEndMonth(lastDateOfPreviousMonth);
      setReportDateRange();
      loadData();
      String workFilename = buildDatFile();
      if ( !inTestMode() )
      {
        // send file
        log.info("SFTT data file " + workFilename);
        sendDataFile( workFilename,
                      MesDefaults.getString(MesDefaults.DK_TSG_OUTGOING_HOST),
                      MesDefaults.getString(MesDefaults.DK_TSG_OUTGOING_USER),
                      MesDefaults.getString(MesDefaults.DK_TSG_OUTGOING_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_TSG_OUTGOING_PATH),
                      false,
                      false );
        log.info("SFTT data file done");
        
        // archive file
        log.info("ARCHIVE data file " + workFilename);
        archiveMonthlyFile( workFilename );                      
        log.info("ARCHIVE data file done");
      }
      log.info("End time: " + cal.getInstance().getTime().toString());
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return true;
  }
  
  public static void main( String[] args )
  {
    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_TSG_OUTGOING_HOST,
                MesDefaults.DK_TSG_OUTGOING_USER,
                MesDefaults.DK_TSG_OUTGOING_PASSWORD,
                MesDefaults.DK_TSG_OUTGOING_PATH
        });
      }
    } catch (Exception e) {
      System.out.println(e.toString());
    }

    TSGQuarterlyDataUpload tsg = new TSGQuarterlyDataUpload();
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      if ( args.length <= 0 || !args[0].equals("prod") )
      {
        tsg.setTestMode(true);
      }
      
      tsg.execute();
    }
    catch( Exception e )
    {
      e.printStackTrace();
      log.error(e.toString());
    }
    finally
    {
      try{ tsg.cleanUp(); } catch( Exception ee ) {}
      log.info("Command line execution of TSGQuarterlyDataUpload complete");
      Runtime.getRuntime().exit(0);
    }
  }
  
  public boolean inTestMode()
  {
    return testMode;
  }
  
  public void setTestMode(boolean test )
  {
    testMode = test;
  }
}/*@lineinfo:generated-code*/