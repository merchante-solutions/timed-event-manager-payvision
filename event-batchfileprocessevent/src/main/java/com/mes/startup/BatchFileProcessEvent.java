/*@lineinfo:filename=BatchFileProcessEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import com.mes.api.TridentApiConstants;
import com.mes.batchprocessing.BPEmailer;
import com.mes.batchprocessing.BPProfile;
import com.mes.batchprocessing.BPdb;
import com.mes.batchprocessing.ResponseFile;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesEncryption;
import com.mes.support.ThreadPool;
import com.mes.support.TridentTools;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;

public class BatchFileProcessEvent extends EventBase
{
  private  static final String  PRODUCTION_URL  = "https://api.merchante-solutions.com/mes-api/tridentApi";
  private  static final String  TEST_URL        = "https://cert.merchante-solutions.com/mes-api/tridentApi";
  
  private  BPdb     BpDb           = null;
  private  Vector   Records        = new Vector();

  public class FileRecord
  {
    public String  MerchantId      = null;
    public String  ProfileId       = null;
    public long    RecId           = 0L;
    public long    ReqFileId       = 0L;
    public long    RespFileId      = 0L;
    public String  TestFlag        = "";
    public String  ReqFilename     = null;
    public String  RespFilename    = null;
    
    public FileRecord( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      MerchantId     = resultSet.getString("merchant_id");
      ProfileId      = resultSet.getString("profile_id");
      RecId          = resultSet.getLong("rec_id");
      ReqFileId      = resultSet.getLong("req_file_id");
      ReqFilename    = resultSet.getString("req_filename");
      TestFlag       = resultSet.getString("test_flag");
      try{ RespFileId = resultSet.getLong("resp_file_id"); }catch( Exception e ){ }
      try{ RespFilename = resultSet.getString("resp_filename"); }catch( Exception e ){ }
    }
    
    public FileRecord()
    {
    }
    
    public String   getMerchantId(){ return MerchantId; }
    public String   getProfileId(){ return ProfileId; }
    public long     getReqFileId(){ return ReqFileId; }
    public long     getRespFileId(){ return RespFileId; }
    public String   getReqFilename(){ return ReqFilename; }
    public String   getRespFilename(){ return RespFilename; }
    public long     getRecId(){ return RecId; }
    public String   getTestFlag(){ return TestFlag; }
    public boolean  isTest(){ return TestFlag.equals("Y"); }

    public void     setMerchantId( String merchantId ){ MerchantId = merchantId; }
    public void     setProfileId( String profileId ){ ProfileId = profileId; }
    public void     setReqFileId(long reqFileId){ ReqFileId = reqFileId; }
    public void     setRespFileId(long respFileId){ RespFileId = respFileId; }
    public void     setReqFilename(String filename){ ReqFilename = filename; }
    public void     setRespFilename(String filename){ RespFilename = filename; }
    public void     setRecId(long recId){ RecId = recId; }
    public void     setTestFlag(String testFlag){ TestFlag = testFlag; }
  }
  
  protected class FileThread
    extends SQLJConnectionBase
    implements Runnable
  {
    private   FileRecord      Record           = null;
    private   BPProfile       Profile          = null;
    private   BufferedWriter  ReqOutputFile    = null;
    private   BufferedWriter  RspOutputFile    = null;
    
    public FileThread( FileRecord fileRecord )
    {
      Record = fileRecord;
    }
    
    public void run()
    {
      ResultSet         rs     = null;
      ResultSetIterator it     = null;
 
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:104^9*/

//  ************************************************************
//  #sql [Ctx] it = { select tp.merchant_number    as merchant_number,
//                   tp.merchant_name      as merchant_name,
//                   tp.terminal_id        as profile_id,
//                   tp.merchant_key       merchant_key,
//                   nvl(tpa.batch_processing_allowed,'N')
//                                         as batch_processing_allowed,
//                   tpa.batch_processing_email_address 
//                                         as email_addresses
//            from   trident_profile      tp,
//                   trident_profile_api  tpa
//            where  tp.merchant_number = :Record.getMerchantId() and
//                   tp.terminal_id = :Record.getProfileId() and
//                   tpa.terminal_id = tp.terminal_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3781 = Record.getMerchantId();
 String __sJT_3782 = Record.getProfileId();
  try {
   String theSqlTS = "select tp.merchant_number    as merchant_number,\n                 tp.merchant_name      as merchant_name,\n                 tp.terminal_id        as profile_id,\n                 tp.merchant_key       merchant_key,\n                 nvl(tpa.batch_processing_allowed,'N')\n                                       as batch_processing_allowed,\n                 tpa.batch_processing_email_address \n                                       as email_addresses\n          from   trident_profile      tp,\n                 trident_profile_api  tpa\n          where  tp.merchant_number =  :1  and\n                 tp.terminal_id =  :2  and\n                 tpa.terminal_id = tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3781);
   __sJT_st.setString(2,__sJT_3782);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BatchFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^9*/
        
        rs = it.getResultSet();

        while( rs.next() )
        {
          Profile = new BPProfile(rs);
        }
        
        it.close();
        
        if( Profile == null )
        {
          return;
        }
        
        recordTimestampBegin();
        ReqOutputFile = new BufferedWriter(new FileWriter("./" + Record.getReqFilename(), true));
        RspOutputFile = new BufferedWriter(new FileWriter("./" + Record.getRespFilename(), true));
        processRequest();
        recordTimestampEnd();
      }
      catch( Exception e )
      {
        logEntry("run()", e.toString());
      }
      finally
      {
        try{ it.close(); }catch(Exception e){}
        try{ ReqOutputFile.close(); }catch(Exception e){}
        try{ RspOutputFile.close(); }catch(Exception e){}
        cleanUp();
      }
    }
    
    private String processFile()
    {
      String filename       = null;
      byte   fileBody[]     = null;
      byte[] decryptedData  = null;
      BLOB   blob           = null;
      String workFilename   = null;
      FileOutputStream fos  = null;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:165^9*/

//  ************************************************************
//  #sql [Ctx] { select file_data
//            
//            from   batch_request_files
//            where  req_file_id = :Record.getReqFileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3783 = Record.getReqFileId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select file_data\n           \n          from   batch_request_files\n          where  req_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3783);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blob = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^9*/
        
        int dataLen = (int)blob.length();
        int chunkSize = 0;
        int dataRead = 0;
        InputStream is = blob.getBinaryStream();
        fileBody = new byte[dataLen];
        while (chunkSize != -1)
        {
          chunkSize = is.read(fileBody,dataRead,dataLen - dataRead);
          dataRead += chunkSize;
        }
        
        decryptedData  = MesEncryption.decryptLargeData(fileBody);
        workFilename   = "batch-file-" + String.valueOf(Record.getReqFileId());
        fos = new FileOutputStream(workFilename);
        fos.write(decryptedData);
        fos.close();
      }
      catch( Exception e )
      {
        logEntry( "processFile()workFilename:" + workFilename + " ", e.toString());
      }
      finally
      {
      }
      
      return workFilename;
    }
    
    private void processRequest()
    {
      int     rowId            = 0;
      String  line             = null;
      String  requestParams    = null;
      String  workFilename     = null;
      BufferedReader    in     = null;
      
      workFilename = processFile();
      
      if( workFilename != null )
      {
        try
        {
          in = new BufferedReader( new FileReader(workFilename) );
          ThreadPool pool = new ThreadPool(5);        
          while( (line = in.readLine()) != null )
          {
            synchronized(line)
            {
              line = line.trim();   // remove leading and trailing whitespace
              // skip blank lines and comments
              if ( line.trim().equals("") || line.startsWith("--") || 
                   line.startsWith("#") || line.startsWith("//") ) 
              {
                continue;
              }
              ++rowId;
              StringBuffer  buff           = new StringBuffer();
              boolean profileIdIncluded    = line.indexOf("profile_id") != -1;
              boolean profileKeyIncluded   = line.indexOf("profile_key") != -1;
            
              buff.append(line);
              buff.append(!profileIdIncluded ? "&profile_id=" + Profile.getProfileId() : "");
              buff.append(!profileKeyIncluded ? "&profile_key=" + Profile.getMerchantKey() : "");
              requestParams = buff.toString();
              
              if( profileIdIncluded )
              {
                String reqProfileId = getFieldValue("profile_id", line);
                if( !reqProfileId.equals(Profile.getProfileId()) )
                {
                  String oldString = "profile_id=" + reqProfileId;
                  requestParams = buff.toString().replaceAll("profile_id=" + reqProfileId, "profile_id=");
                }
              }
          
              ReqOutputFile.write(String.valueOf(rowId) + "!@#" + truncateRequestParams(line));
              ReqOutputFile.newLine();
            }

            String pgURL;

            if(Record.isTest()) {
                String testURL = MesDefaults.getString(MesDefaults.PG_TEST_URL);
                pgURL = testURL == null ? TEST_URL : testURL;
            } else {
                String prodURL = MesDefaults.getString(MesDefaults.PG_PRODUCTION_URL);
                pgURL = prodURL == null ? PRODUCTION_URL : prodURL;
            }

            Thread thread = pool.getThread( new TranThread(rowId, RspOutputFile, pgURL, requestParams) );
            thread.start();

          }
          pool.waitForAllThreads();
          
          in.close();
          
          // remove temp file
          (new File(workFilename)).delete();
        }
        catch( Exception e )
        {
          logEntry( this.getClass().getName() + "::processRequest()" + workFilename, e.toString());
        }
      }
    }
    
    protected void recordTimestampBegin()
    {
      try
      {
        Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());    
        /*@lineinfo:generated-code*//*@lineinfo:274^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_process
//            set     process_begin_date = :beginTime
//            where   rec_id = :Record.getRecId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3784 = Record.getRecId();
   String theSqlTS = "update  batch_process\n          set     process_begin_date =  :1 \n          where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,__sJT_3784);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampBegin()rec_id=" + Record.getRecId(), e.toString());
      }      
    }
    
    protected void recordTimestampEnd()
    {
      Timestamp     beginTime   = null;
      long          elapsed     = 0L;
      Timestamp     endTime     = null;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:295^9*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//            from    batch_process
//            where   rec_id = :Record.getRecId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3785 = Record.getRecId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n          from    batch_process\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3785);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^9*/
        
        endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
        elapsed = endTime.getTime() - beginTime.getTime();
      
        /*@lineinfo:generated-code*//*@lineinfo:305^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_process
//            set     process_end_date = :endTime,
//                    process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//            where   rec_id = :Record.getRecId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3786 = DateTimeFormatter.getFormattedTimestamp(elapsed);
 long __sJT_3787 = Record.getRecId();
   String theSqlTS = "update  batch_process\n          set     process_end_date =  :1 ,\n                  process_elapsed =  :2 \n          where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_3786);
   __sJT_st.setLong(3,__sJT_3787);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:311^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampEnd()rec_id=" + Record.getRecId(), e.toString());
      }      
    }
  }
  
  protected class TranThread
    extends SQLJConnectionBase
    implements Runnable
  {
    private int             RowId            = 0;
    private BufferedWriter  OutputFile       = null;
    private String          RequestURL       = null;
    private String          RequestParams    = null;
    
    public TranThread( int rowId, BufferedWriter outputFile, String url, String requestParams )
    {
      RowId          = rowId;
      OutputFile     = outputFile;
      RequestURL     = url;
      RequestParams  = requestParams;
    }
    
    public void run()
    {
      String              response      = null;
      URL                 url           = null;
      HttpURLConnection   con           = null;
      try
      {
        // connect to the api
        url = new URL( RequestURL );
        con = (HttpURLConnection)url.openConnection();
        
        // Indicate that you will be doing input and output, 
        // that the method is POST, and that the content
        // length is the length of the request string
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-type", "application/x-www-form-urlencoded" );
        con.setRequestProperty("Content-length", String.valueOf(RequestParams.length()));
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(RequestParams);
        out.flush();
        out.close();
        
        try
        {
          BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
          response = in.readLine();
        }
        catch( Exception ee )
        {
          response = "Internal Error - " + ee.toString();
        }
        
        synchronized(OutputFile)
        {
          OutputFile.write(String.valueOf(RowId) + "!@#" + response);
          OutputFile.newLine();
        }
      }
      catch( Exception e )
      {
        logEntry("run() ", e.toString());
      }
      finally
      {
        try{ con.disconnect(); }catch(Exception e){}
      }
    }
  }
  
  public BatchFileProcessEvent()
  {
  }
  
  private ResponseFile buildResponseFile( FileRecord fileRecord )
  {
    double  approvedAmount      = 0.0;
    int     approvedCount       = 0;
    double  declinedAmount      = 0.0;
    int     declinedCount       = 0;
    StringBuffer      buff      = new StringBuffer();
    ResultSet         rs        = null;
    ResultSetIterator it        = null;
    ResponseFile      respFile  = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:404^7*/

//  ************************************************************
//  #sql [Ctx] it = { select rsp.row_id            as row_id,
//                 req.req_file_id       as req_file_id,
//                 rsp.resp_file_id      as resp_file_id,
//                 req.raw_req_data      as request,
//                 rsp.raw_resp_data     as response
//          from   batch_requests   req,
//                 batch_responses  rsp
//          where  rsp.merchant_id = :fileRecord.getMerchantId() and
//                 rsp.profile_id = :fileRecord.getProfileId() and
//                 rsp.resp_file_id = :fileRecord.getRespFileId() and
//                 req.merchant_id = rsp.merchant_id and
//                 req.profile_id = rsp.profile_id and
//                 req.req_file_id = rsp.req_file_id and
//                 req.row_id = rsp.row_id
//          order by rsp.row_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3788 = fileRecord.getMerchantId();
 String __sJT_3789 = fileRecord.getProfileId();
 long __sJT_3790 = fileRecord.getRespFileId();
  try {
   String theSqlTS = "select rsp.row_id            as row_id,\n               req.req_file_id       as req_file_id,\n               rsp.resp_file_id      as resp_file_id,\n               req.raw_req_data      as request,\n               rsp.raw_resp_data     as response\n        from   batch_requests   req,\n               batch_responses  rsp\n        where  rsp.merchant_id =  :1  and\n               rsp.profile_id =  :2  and\n               rsp.resp_file_id =  :3  and\n               req.merchant_id = rsp.merchant_id and\n               req.profile_id = rsp.profile_id and\n               req.req_file_id = rsp.req_file_id and\n               req.row_id = rsp.row_id\n        order by rsp.row_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3788);
   __sJT_st.setString(2,__sJT_3789);
   __sJT_st.setLong(3,__sJT_3790);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.BatchFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:421^7*/
      
      rs = it.getResultSet();

      while( rs.next() )
      {
        String  rawRequest   = rs.getString("request");
        String  rawResponse  = rs.getString("response");
        String  errorCode    = getFieldValue("error_code", rawResponse);
        String  tranType     = getFieldValue("transaction_type", rawRequest);
        double  tranAmount   = 0.0;
        try{ tranAmount = Double.parseDouble(getFieldValue("transaction_amount", rawRequest)); }catch( Exception e ){ }
        
        if( tranType.equals("C") || tranType.equals("U") )
        {
          tranAmount = (-1) * tranAmount;
        }
        
        if( errorCode.equals("000") || errorCode.equals("085") ) //approved
        {
          ++approvedCount;
          approvedAmount += tranAmount;
        }
        else
        {
          ++declinedCount;
          declinedAmount += tranAmount;
        }
        
        buff.append(buff.length() == 0 ? "" : "\r\n");
        buff.append(rawResponse);
      }
      
      it.close();
      
      byte[] fileData    = buff.toString().getBytes();
      int    fileLength  = (int)fileData.length;
      
      respFile = new ResponseFile();
      respFile.setMerchantId(fileRecord.getMerchantId());
      respFile.setProfileId(fileRecord.getProfileId());
      respFile.setRespFileId(fileRecord.getRespFileId());
      respFile.setReqFileId(fileRecord.getReqFileId());
      respFile.setTestFlag(fileRecord.getTestFlag());
      respFile.setFilename(fileRecord.getRespFilename());
      respFile.setFileSize(fileLength);
      respFile.setFileData(fileData);
      respFile.setApprovedCount(approvedCount);
      respFile.setApprovedAmount(approvedAmount);
      respFile.setDeclinedCount(declinedCount);
      respFile.setDeclinedAmount(declinedAmount);
      respFile.setTotalTranCount(approvedCount + declinedCount);
      respFile.setTotalAmount(approvedAmount + declinedAmount);
    }
    catch( Exception e )
    {
      respFile = null;
      logEntry("buildResponseFile()respFileId:" + fileRecord.getRespFileId(), e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
    
    return respFile;
  }
  
  private ResponseFile buildResponseFile( long respFileId )
  {
    long    reqFileId;
    String  merchantId;
    String  profileId;
    String  testFlag = "Y";
    String  respFilename;
    double  approvedAmount      = 0.0;
    int     approvedCount       = 0;
    double  declinedAmount      = 0.0;
    int     declinedCount       = 0;
    StringBuffer      buff      = new StringBuffer();
    ResultSet         rs        = null;
    ResultSetIterator it        = null;
    ResponseFile      respFile  = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:505^7*/

//  ************************************************************
//  #sql [Ctx] { select merchant_id, 
//                 profile_Id, 
//                 req_file_id, 
//                 file_name, 
//                 test_flag
//          
//          from   batch_response_files br
//          where  br.resp_file_id = :respFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merchant_id, \n               profile_Id, \n               req_file_id, \n               file_name, \n               test_flag\n         \n        from   batch_response_files br\n        where  br.resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = (String)__sJT_rs.getString(1);
   profileId = (String)__sJT_rs.getString(2);
   reqFileId = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   respFilename = (String)__sJT_rs.getString(4);
   testFlag = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:519^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:521^7*/

//  ************************************************************
//  #sql [Ctx] it = { select rsp.merchant_id       as merchant_id,
//                 rsp.profile_id        as profile_id,
//                 rsp.row_id            as row_id,
//                 req.req_file_id       as req_file_id,
//                 rsp.resp_file_id      as resp_file_id,
//                 req.raw_req_data      as request,
//                 rsp.raw_resp_data     as response
//          from   batch_requests   req,
//                 batch_responses  rsp
//          where  rsp.merchant_id = :merchantId and
//                 rsp.profile_id = :profileId and
//                 rsp.resp_file_id = :respFileId and
//                 req.merchant_id = rsp.merchant_id and
//                 req.profile_id = rsp.profile_id and
//                 req.req_file_id = rsp.req_file_id and
//                 req.row_id = rsp.row_id
//          order by rsp.row_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select rsp.merchant_id       as merchant_id,\n               rsp.profile_id        as profile_id,\n               rsp.row_id            as row_id,\n               req.req_file_id       as req_file_id,\n               rsp.resp_file_id      as resp_file_id,\n               req.raw_req_data      as request,\n               rsp.raw_resp_data     as response\n        from   batch_requests   req,\n               batch_responses  rsp\n        where  rsp.merchant_id =  :1  and\n               rsp.profile_id =  :2  and\n               rsp.resp_file_id =  :3  and\n               req.merchant_id = rsp.merchant_id and\n               req.profile_id = rsp.profile_id and\n               req.req_file_id = rsp.req_file_id and\n               req.row_id = rsp.row_id\n        order by rsp.row_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantId);
   __sJT_st.setString(2,profileId);
   __sJT_st.setLong(3,respFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.BatchFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^7*/
      
      rs = it.getResultSet();

      while( rs.next() )
      {
        String  rawRequest   = rs.getString("request");
        String  rawResponse  = rs.getString("response");
        String  errorCode    = getFieldValue("error_code", rawResponse);
        String  tranType     = getFieldValue("transaction_type", rawRequest);
        double  tranAmount   = 0.0;
        try{ tranAmount = Double.parseDouble(getFieldValue("transaction_amount", rawRequest)); }catch( Exception e ){ }
        
        if( tranType.equals("C") || tranType.equals("U") )
        {
          tranAmount = (-1) * tranAmount;
        }
        
        if( errorCode.equals("000") || errorCode.equals("085") ) //approved
        {
          ++approvedCount;
          approvedAmount += tranAmount;
        }
        else
        {
          ++declinedCount;
          declinedAmount += tranAmount;
        }
        
        buff.append(buff.length() == 0 ? "" : "\r\n");
        buff.append(rawResponse);
      }
      
      it.close();
      
      byte[] fileData    = buff.toString().getBytes();
      int    fileLength  = (int)fileData.length;
      
      respFile = new ResponseFile();
      respFile.setMerchantId(merchantId);
      respFile.setProfileId(profileId);
      respFile.setRespFileId(respFileId);
      
      respFile.setReqFileId(reqFileId);
      respFile.setTestFlag(testFlag);
      respFile.setFilename(respFilename);
      respFile.setFileSize(fileLength);
      respFile.setFileData(fileData);
      respFile.setApprovedCount(approvedCount);
      respFile.setApprovedAmount(approvedAmount);
      respFile.setDeclinedCount(declinedCount);
      respFile.setDeclinedAmount(declinedAmount);
      respFile.setTotalTranCount(approvedCount + declinedCount);
      respFile.setTotalAmount(approvedAmount + declinedAmount);
    }
    catch( Exception e )
    {
      respFile = null;
      logEntry("buildResponseFile()respFileId:" + respFileId, e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
    
    return respFile;
  }
  
  public boolean reloadRequestFile( String merchantId, String profileId, long reqFileId )
  {
    boolean retVal       = false;
    int     loopCount    = 0;
    
    try
    {
      connect(true);

      // remove all entries for this file
      log.debug("remove all entries for reqFileId " + reqFileId);
      
      /*@lineinfo:generated-code*//*@lineinfo:620^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   batch_requests
//          where  req_file_id = :reqFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   batch_requests\n        where  req_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^7*/
      
      // extract and store requests
      String workFilename = "req-" + String.valueOf(reqFileId) + ".txt";
      log.debug("workFilename " + workFilename);      
      
      while( !retVal )
      {
        ++loopCount;
        if( loopCount >= 100 )
        {
          break;
        }
        long nextLineNum = getBPdb().getLastReqLineNum(reqFileId) + 1;
        log.debug(workFilename + " rows processed " + (nextLineNum - 1));
        if( nextLineNum >= 0 )
        {
          log.debug("call storeRequests() again");
          retVal = getBPdb().storeRequests( merchantId, profileId, reqFileId, workFilename, nextLineNum);
        }
        else
        {
          break;
        }
      }
      
      log.debug("Total re-call couunt " + loopCount);
      log.debug("total record count " + getBPdb().getLastReqLineNum(reqFileId));
    }
    catch( Exception ee )
    {
      ee.printStackTrace();
      retVal = false;
      logEvent(this.getClass().getName(), "::reloadRequestFile() ", ee.toString());
    }
    finally
    {
     cleanUp();
    }

    return retVal;
  }

  public boolean reloadResponseFile( String merchantId, String profileId, long respFileId ) 
  {
    int     loopCount       = 0;
    long    reqFileId       = 0L;
    boolean retVal          = false;
    try
    {
      connect(true);
      log.debug("delete all entries from batch_responses for respFileId " + respFileId);
      /*@lineinfo:generated-code*//*@lineinfo:677^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   batch_responses
//          where  resp_file_id = :respFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   batch_responses\n        where  resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:682^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] { select req_file_id 
//          from   batch_response_files
//          where  resp_file_id = :respFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select req_file_id  \n        from   batch_response_files\n        where  resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   reqFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:689^7*/

      String workFilename = "rsp-" + String.valueOf(respFileId) + ".txt";
      log.debug("workFilename " + workFilename);      
      while( !retVal )
      {
        ++loopCount;
        if( loopCount >= 100 )
        {
          break;
        }
        long nextLineNum = getBPdb().getLastRespLineNum(respFileId) + 1;
        log.debug(workFilename + " rows processed " + (nextLineNum - 1));
        if( nextLineNum >= 0 )
        {
          log.debug("call storeResponses() again");
          retVal = getBPdb().storeResponses( merchantId, profileId, reqFileId, respFileId, workFilename, nextLineNum );
        }
        else
        {
          break;
        }
      }
      
      log.debug("Total re-call count " + loopCount);
      log.debug("Total record count " + getBPdb().getLastRespLineNum(respFileId));
      
      if( retVal )
      {
        // re-build/store resp file
        log.debug("build the response file " + workFilename);
        ResponseFile respFile = buildResponseFile( respFileId );
        
        log.debug("delete all entries from batch_response_files for respFileId " + respFileId);
        /*@lineinfo:generated-code*//*@lineinfo:723^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from   batch_response_files
//            where  resp_file_id = :respFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from   batch_response_files\n          where  resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:728^9*/
        
        log.debug("create entry in batch_response_files table");
        getBPdb().storeResponseFile( respFile );
      }
    }
    catch( Exception ee )
    {
      ee.printStackTrace();
      logEvent(this.getClass().getName(), "::reloadResponseFile() ", ee.toString());
    }
    finally
    {
      cleanUp();
    }

    return retVal;
  }

  public boolean execute()
  {
    String   workFilename      = null;
    String   emailAddress      = null;
    boolean  retVal            = false;
    long     nextLineNum       = -1L;
    try
    {
      connect(true);
      
      processBatch();

      for( int i = 0; i < Records.size(); ++i )
      {
        FileRecord rec = (FileRecord)Records.elementAt(i);
        
        // store requests
        workFilename = rec.getReqFilename();
        retVal = getBPdb().storeRequests( rec.getMerchantId(), rec.getProfileId(), rec.getReqFileId(), workFilename, 1L);
        int loopCount = 0;
        while( !retVal )
        {
          ++loopCount;
          if( loopCount >= 10 )
          {
            break;
          }
          nextLineNum = getBPdb().getLastReqLineNum(rec.getReqFileId()) + 1;
          log.debug(workFilename + " rows processed " + (nextLineNum - 1) + " rep_file_id " + rec.getReqFileId());
          
          if( nextLineNum >= 0 )
          {
            log.debug("call storeRequests() again");
            retVal = getBPdb().storeRequests( rec.getMerchantId(), rec.getProfileId(), rec.getReqFileId(), workFilename, nextLineNum);
          }
          else
          {
            break;
          }
        }
        
        // remove file
        //(new File(workFilename)).delete();///////////////////////////////////////to debug
        
        // store responses
        workFilename = rec.getRespFilename();
        retVal = getBPdb().storeResponses( rec.getMerchantId(), rec.getProfileId(), rec.getReqFileId(), rec.getRespFileId(), workFilename, 1L );
        while( !retVal )
        {
          nextLineNum = getBPdb().getLastRespLineNum(rec.getRespFileId()) + 1;
          log.debug(workFilename + " row processed " + (nextLineNum - 1));
          if( nextLineNum >= 0 )
          {
            log.debug("call storeResponses() again");
            retVal = getBPdb().storeResponses( rec.getMerchantId(), rec.getProfileId(), rec.getReqFileId(), rec.getRespFileId(), workFilename, nextLineNum );
          }
          else
          {
            break;
          }
        }
        
        // remove file
        //(new File(workFilename)).delete(); ////////////////////////////////////to debug
        
        // store response file
        ResponseFile respFile = buildResponseFile( rec );
        getBPdb().storeResponseFile( respFile );
        
        // send email
        /*@lineinfo:generated-code*//*@lineinfo:817^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(batch_processing_email_address, 'not on file')
//            
//            from    trident_profile_api
//            where   terminal_id = :rec.getProfileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3791 = rec.getProfileId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(batch_processing_email_address, 'not on file')\n           \n          from    trident_profile_api\n          where   terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3791);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   emailAddress = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:823^9*/

        if( !emailAddress.equals("not on file") )
        {
          BPEmailer mail = new BPEmailer();
          mail.addTo(emailAddress);
          mail.sendResponseFile(respFile);
        }

        /*@lineinfo:generated-code*//*@lineinfo:832^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_response_files
//            set     email_address = :emailAddress,
//                    date_email_sent = sysdate
//            where   resp_file_id = :rec.getRespFileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3792 = rec.getRespFileId();
   String theSqlTS = "update  batch_response_files\n          set     email_address =  :1 ,\n                  date_email_sent = sysdate\n          where   resp_file_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,emailAddress);
   __sJT_st.setLong(2,__sJT_3792);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:838^9*/
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "::execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return true;
  }
  
  private BPdb getBPdb()
  {
    if( BpDb == null )
    {
      BpDb = new BPdb();
    }

    return BpDb;
  }
  
  public String getFieldValue( String fieldName, String queryString )
  {
    int           index   = 0;
    StringBuffer  field   = new StringBuffer();
    
    if( queryString != null && queryString.indexOf(fieldName) != -1 )
    {
      index = queryString.indexOf(fieldName)+(fieldName.length()+1);
      while(  index < queryString.length() && queryString.charAt(index) != '&' )
      {
        field.append(queryString.charAt(index++));
      }
    }
    
    return( field.toString() );
  }
  
  private void processBatch()
  {
    int    itemCount         = 0;
    long   recId             = 0L;
    long   sequenceId        = 0L;
    ResultSet         rs     = null;
    ResultSetIterator it     = null;
    ThreadPool    pool       = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:891^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(file_id)  
//          from    batch_process
//          where   process_type = 0 and
//                  process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(file_id)   \n        from    batch_process\n        where   process_type = 0 and\n                process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:897^7*/
      
      if ( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:901^9*/

//  ************************************************************
//  #sql [Ctx] { select  batch_process_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  batch_process_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.BatchFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:905^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:907^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_process
//            set     process_sequence = :sequenceId
//            where   process_type = 0 and
//                    process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  batch_process\n          set     process_sequence =  :1 \n          where   process_type = 0 and\n                  process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:913^9*/
      }

      if( sequenceId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:918^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bp.rec_id            as rec_id,
//                    bpf.merchant_id      as merchant_id,
//                    bpf.profile_id       as profile_id,
//                    bpf.test_flag        as test_flag,
//                    bpf.req_file_id      as req_file_id,
//                    'req-' || bpf.req_file_id || '.txt'
//                                         as req_filename
//            from    batch_process  bp,
//                    batch_request_files bpf
//            where   bp.process_sequence = :sequenceId and
//                    bpf.req_file_id = bp.file_id
//            order by bp.rec_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bp.rec_id            as rec_id,\n                  bpf.merchant_id      as merchant_id,\n                  bpf.profile_id       as profile_id,\n                  bpf.test_flag        as test_flag,\n                  bpf.req_file_id      as req_file_id,\n                  'req-' || bpf.req_file_id || '.txt'\n                                       as req_filename\n          from    batch_process  bp,\n                  batch_request_files bpf\n          where   bp.process_sequence =  :1  and\n                  bpf.req_file_id = bp.file_id\n          order by bp.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.BatchFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.startup.BatchFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:932^9*/
        rs = it.getResultSet();
      
        while( rs.next() )
        {
          long respFileId = getBPdb().getNewFileId();
          String respFilename = "rsp-" + String.valueOf(respFileId) + ".txt";
          FileRecord rec = new FileRecord(rs);
          rec.setRespFileId(respFileId);
          rec.setRespFilename(respFilename);
          Records.addElement( rec );
        }

        it.close();
        
        pool = new ThreadPool(5);
        for( int i = 0; i < Records.size(); ++i )
        {
          Thread thread = pool.getThread( new FileThread((FileRecord)Records.elementAt(i)) );
          thread.start();
        }
        pool.waitForAllThreads();
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processBatch()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  private String truncateRequestParams( String request )
  {
    StringBuffer  buff        = new StringBuffer(request);
    String        tempStr     = null;
    int           endIndex    = 0;
    int           index       = 0;

    if ( (index = buff.indexOf(TridentApiConstants.FN_CARD_NUMBER)) >= 0 )
    {
      index += TridentApiConstants.FN_CARD_NUMBER.length()+1;
      endIndex = index;
      
      while( endIndex < buff.length() && buff.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      if ( (endIndex - index) > 10 )
      {
        tempStr = TridentTools.encodeCardNumber(buff.substring(index,endIndex));
      }
      else if ( (endIndex - index) > 0 )
      {
        tempStr = buff.substring(index,endIndex);
      }
      
      try{ buff.replace( index, endIndex, tempStr ); }catch( Exception ee ){}
    }

    if ( (index = buff.indexOf(TridentApiConstants.FN_CARD_SWIPE)) >= 0 )
    {
      index += TridentApiConstants.FN_CARD_SWIPE.length()+1;
      endIndex = index;
      
      while( endIndex < buff.length() && buff.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      // only display the portion of the card swipe (truncated) used
      // in the authorization request.  all other track data is discarded
      try
      {
        String cardSwipeRaw = URLDecoder.decode(buff.substring(index,endIndex),"UTF-8");
        String cardSwipeTrack = TridentTools.decodeCardSwipeTrackData(cardSwipeRaw);
        buff.replace( index, endIndex, TridentTools.encodeCardSwipe(cardSwipeTrack) );
      }
      catch( Exception e )
      {
        buff.replace( index, endIndex, "invalid-track-data-found" );
      }        
    }
    
    if ( (index = buff.indexOf(TridentApiConstants.FN_CVV2)) >= 0 )
    {
      index += TridentApiConstants.FN_CVV2.length()+1;
      endIndex = index;
      
      while( endIndex < buff.length() && buff.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      buff.replace( index, endIndex, "xxx" );
    }

    return( buff.toString() );
  }
  
  public static void main( String[] args )
  {
    BatchFileProcessEvent test   = null;
    String  cmdLine         = args[0];
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone();
      test = new BatchFileProcessEvent();
      
      // args[1] merchantId 
      // args[2] profileId
      // args[3] reqFileId / respFileId

      if ( "reloadReqFile".equals(cmdLine) )
      {
        test.reloadRequestFile( args[1], args[2], Long.parseLong(args[3]));
      }
      if ( "reloadRspFile".equals(cmdLine) )
      {
        test.reloadResponseFile( args[1], args[2], Long.parseLong(args[3]));
      }
      else
      {
        test.execute();
      }
    }
    finally
    {
      test.cleanUp();
    }
  }
}/*@lineinfo:generated-code*/