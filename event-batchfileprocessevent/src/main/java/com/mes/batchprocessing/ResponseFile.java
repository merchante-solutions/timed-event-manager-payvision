package com.mes.batchprocessing;

import java.sql.ResultSet;
import java.sql.Timestamp;
import com.mes.support.DateTimeFormatter;

public class ResponseFile
{
  private double     ApprovedAmount;
  private int        ApprovedCount;
  private Timestamp  CreateTs;
  private double     DeclinedAmount;
  private int        DeclinedCount;
  private byte[]     FileData;
  private String     Filename;
  private int        FileSize;
  private String     MerchantId;
  private String     ProfileId;
  private long       ReqFileId;
  private long       RespFileId;
  private String     TestFlag;
  private double     TotalAmount;
  private int        TotalTranCount;

  public ResponseFile()
  {
  }

  public ResponseFile( ResultSet rs )
    throws java.sql.SQLException 
  {
    MerchantId       = rs.getString("merchant_id");
    ProfileId        = rs.getString("profile_id");
    ReqFileId        = rs.getLong("req_file_id");
    RespFileId       = rs.getLong("resp_file_id");
    ApprovedAmount   = rs.getDouble("approved_amount");
    ApprovedCount    = rs.getInt("approved_count");
    DeclinedAmount   = rs.getDouble("declined_amount");
    DeclinedCount    = rs.getInt("declined_count");
    TotalTranCount   = ApprovedCount + DeclinedCount;
    TotalAmount      = ApprovedAmount + DeclinedAmount;
  }
  
  public ResponseFile( String merchantId, String profileId, long reqFileId, long respFileId, 
                       String filename, byte[] fileData, int fileSize )
  {
    FileData      = fileData;
    Filename      = filename;
    FileSize      = fileSize;
    MerchantId    = merchantId;
    ProfileId     = profileId;
    ReqFileId     = reqFileId;
    RespFileId    = respFileId;
  }

  public void setApprovedAmount( double approvedAmount ){ ApprovedAmount = approvedAmount; }
  public void setApprovedCount( int approvedCount ){ ApprovedCount = approvedCount; }
  public void setCreateTs( Timestamp createTs ){ CreateTs = createTs; }
  public void setDeclinedAmount( double declinedAmount ){ DeclinedAmount = declinedAmount; }
  public void setDeclinedCount( int declinedCount ){ DeclinedCount = declinedCount; }
  public void setFileData( byte[] fileData ){ FileData = fileData; }
  public void setFilename( String filename ){ Filename = filename; }
  public void setFileSize( int fileSize ){ FileSize = fileSize; }
  public void setMerchantId( String merchantId ){ MerchantId = merchantId; }
  public void setProfileId( String profileId ){ ProfileId = profileId; }
  public void setReqFileId( long reqFileId ){ ReqFileId = reqFileId; }
  public void setRespFileId( long respFileId ){ RespFileId = respFileId; }
  public void setTestFlag( String testFlag ){ TestFlag = testFlag; }
  public void setTotalAmount( double totalAmount ){ TotalAmount = totalAmount; }
  public void setTotalTranCount( int totalTranCount ){ TotalTranCount = totalTranCount; }

  public double    getApprovedAmount(){ return ApprovedAmount; }
  public int       getApprovedCount(){ return ApprovedCount; }
  public Timestamp getCreateTs(){ return CreateTs; }
  public double    getDeclinedAmount(){ return DeclinedAmount; }
  public int       getDeclinedCount(){ return DeclinedCount; }
  public String    getFormattedCreateTs(){ return( DateTimeFormatter.getFormattedDate(CreateTs,"MM/dd/yyyy  h:mm:ss a")); }
  public byte[]    getFileData(){ return FileData; }
  public String    getFilename(){ return Filename; }
  public int       getFileSize(){ return FileSize; }
  public String    getMerchantId(){ return MerchantId; }
  public String    getProfileId(){ return ProfileId; }
  public long      getReqFileId(){ return ReqFileId; }
  public long      getRespFileId(){ return RespFileId; }
  public String    getTestFlag(){ return TestFlag; }
  public double    getTotalAmount(){ return TotalAmount; }
  public int       getTotalTranCount(){ return TotalTranCount; }
  public boolean   isTest(){ return TestFlag.equals("Y"); }
}
