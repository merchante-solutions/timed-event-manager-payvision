/*@lineinfo:filename=BPdb*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.batchprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.MesEncryption;
import com.mes.tools.DropDownItem;
import com.mes.user.UserBean;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;

public class BPdb extends SQLJConnectionBase
{
  static   Logger    log        = Logger.getLogger(BPdb.class);
  private  UserBean  User       = null;

  public BPdb()
  {
  }

  public BPdb( UserBean ub )
  {
    User = ub;
  }
  
  private long getHierarchyNode()
  {
    long retVal = 0L;
    
    if( User != null )
    {
      retVal = User.getHierarchyNode();
    }

    return retVal;
  }

  public long getNewFileId()
  {
    long retVal = 0L;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:51^7*/

//  ************************************************************
//  #sql [Ctx] { select batch_process_id.nextval 
//          from   dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select batch_process_id.nextval  \n        from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:55^7*/
    }
    catch( SQLException sqle )
    {
    }

    return retVal;
  }
  
  public RequestFile getRequestFile( long reqFileId )
  {
    RequestFile         file    = null;
    ResultSet           rs      = null;
    ResultSetIterator   it      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:74^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rf.merchant_id     as merchant_id,
//                  rf.profile_id      as profile_id,
//                  rf.request_ts      as request_ts,
//                  rf.file_name       as filename,
//                  rf.test_flag       as test_flag,
//                  rf.user_name       as user_name
//          from    organization     o,
//                  group_merchant   gm,
//                  batch_request_files rf
//          where   o.org_group = :getHierarchyNode() 
//                  and gm.org_num = o.org_num 
//                  and rf.merchant_id = gm.merchant_number
//                  and rf.req_file_id = :reqFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3597 = getHierarchyNode();
  try {
   String theSqlTS = "select  rf.merchant_id     as merchant_id,\n                rf.profile_id      as profile_id,\n                rf.request_ts      as request_ts,\n                rf.file_name       as filename,\n                rf.test_flag       as test_flag,\n                rf.user_name       as user_name\n        from    organization     o,\n                group_merchant   gm,\n                batch_request_files rf\n        where   o.org_group =  :1  \n                and gm.org_num = o.org_num \n                and rf.merchant_id = gm.merchant_number\n                and rf.req_file_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3597);
   __sJT_st.setLong(2,reqFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.batchprocessing.BPdb",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^7*/
      
      rs = it.getResultSet();

      while( rs.next() )
      {
        int   chunkSize   = 0;
        int   dataRead    = 0;
        int   dataLen     = 0;
        byte  fileBody[]  = null;
        BLOB  blob        = null;
        InputStream is    = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:102^9*/

//  ************************************************************
//  #sql [Ctx] { select  file_data 
//            from    batch_request_files
//            where   req_file_id = :reqFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  file_data  \n          from    batch_request_files\n          where   req_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,reqFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blob = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^9*/
        
        dataLen = (int)blob.length();
        is = blob.getBinaryStream();
        fileBody = new byte[dataLen];
        while (chunkSize != -1)
        {
          chunkSize = is.read(fileBody,dataRead,dataLen - dataRead);
          dataRead += chunkSize;
        }
        
        file = new RequestFile();
        file.setReqFileId(reqFileId);
        file.setCreateTs(rs.getTimestamp("request_ts"));
        file.setMerchantId(rs.getString("merchant_id"));
        file.setProfileId(rs.getString("profile_id"));
        file.setTestFlag(rs.getString("test_flag"));
        file.setUserName(rs.getString("user_name"));
        file.setFilename(rs.getString("filename"));
        file.setFileData(fileBody);
        file.setFileSize(fileBody.length);
      }
      
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
    return( file );
  }
  
  public ResponseFile getResponseFile( long respFileId )
  {
    return getResponseFile(getHierarchyNode(), respFileId );
  }
  
  public long getLastReqLineNum( long reqFileId )
  {
    long retVal = 0L;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:155^7*/

//  ************************************************************
//  #sql [Ctx] { select count(*) 
//          from   batch_requests br
//          where  br.req_file_id = :reqFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)  \n        from   batch_requests br\n        where  br.req_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,reqFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/
    }
    catch( Exception e )
    {
      retVal = 0L;
    }
      log.debug("getLastReqLineNum() reqFileId " + reqFileId);
    return( retVal );
  }

  public long getLastRespLineNum(long respFileId )
  {
    long retVal = 0L;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:178^7*/

//  ************************************************************
//  #sql [Ctx] { select count(*) 
//          from   batch_responses bs
//          where  bs.resp_file_id = :respFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)  \n        from   batch_responses bs\n        where  bs.resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
    }
    catch( Exception e )
    {
      retVal = 0L;
    }
    finally
    {
      cleanUp();
    }

    return( retVal );
  }
  
  public ResponseFile getResponseFile( long nodeId, long respFileId )
  {
    ResponseFile        file    = null;
    ResultSet           rs      = null;
    ResultSetIterator   it      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:207^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rf.req_file_id          as req_file_id,
//                  rf.resp_file_id         as resp_file_id,
//                  rf.merchant_id          as merchant_id,
//                  rf.profile_id           as profile_id,
//                  rf.file_name            as filename,
//                  rf.approved_amount      as approved_amount,
//                  rf.approved_count       as approved_count,
//                  rf.declined_amount      as declined_amount,
//                  rf.declined_count       as declined_count
//          from    organization     o,
//                  group_merchant   gm,
//                  batch_response_files rf
//          where   o.org_group = :nodeId
//                  and gm.org_num = o.org_num 
//                  and rf.merchant_id = gm.merchant_number
//                  and rf.resp_file_id = :respFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rf.req_file_id          as req_file_id,\n                rf.resp_file_id         as resp_file_id,\n                rf.merchant_id          as merchant_id,\n                rf.profile_id           as profile_id,\n                rf.file_name            as filename,\n                rf.approved_amount      as approved_amount,\n                rf.approved_count       as approved_count,\n                rf.declined_amount      as declined_amount,\n                rf.declined_count       as declined_count\n        from    organization     o,\n                group_merchant   gm,\n                batch_response_files rf\n        where   o.org_group =  :1 \n                and gm.org_num = o.org_num \n                and rf.merchant_id = gm.merchant_number\n                and rf.resp_file_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,respFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.batchprocessing.BPdb",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
      
      rs = it.getResultSet();

      while( rs.next() )
      {
        int   chunkSize   = 0;
        int   dataRead    = 0;
        int   dataLen     = 0;
        byte  fileBody[]  = null;
        BLOB  blob        = null;
        InputStream is    = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:238^9*/

//  ************************************************************
//  #sql [Ctx] { select  file_data  
//            from    batch_response_files
//            where   resp_file_id = :respFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  file_data   \n          from    batch_response_files\n          where   resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blob = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^9*/
        
        dataLen = (int)blob.length();
        is = blob.getBinaryStream();
        fileBody = new byte[dataLen];
        while (chunkSize != -1)
        {
          chunkSize = is.read(fileBody,dataRead,dataLen - dataRead);
          dataRead += chunkSize;
        }
        
        file = new ResponseFile( rs );
        file.setFilename(rs.getString("filename"));
        file.setFileData(fileBody);
        file.setFileSize(fileBody.length);
      }
      
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return( file );
  }
  
  public boolean isValidRespFileId( long fileId )
  {
    int      recCount   = 0;
    boolean  retVal     = false;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:283^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rf.resp_file_id) 
//          from    organization     o,
//                  group_merchant   gm,
//                  batch_response_files rf
//          where   o.org_group = :getHierarchyNode() 
//                  and gm.org_num = o.org_num 
//                  and rf.merchant_id = gm.merchant_number
//                  and rf.resp_file_id = :fileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3598 = getHierarchyNode();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rf.resp_file_id)  \n        from    organization     o,\n                group_merchant   gm,\n                batch_response_files rf\n        where   o.org_group =  :1  \n                and gm.org_num = o.org_num \n                and rf.merchant_id = gm.merchant_number\n                and rf.resp_file_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3598);
   __sJT_st.setLong(2,fileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^7*/
    }
    catch( SQLException sqle )
    {
    }

    return (recCount > 0) ;
  }
  
  public BPProfile loadProfile(String profileId)
  {
    BPProfile           profile   = null;
    ResultSet           rs        = null;
    ResultSetIterator   it        = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:312^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.merchant_number     as merchant_number,
//                  tp.merchant_name       as merchant_name,
//                  tp.terminal_id         as profile_id,
//                  tp.merchant_key        as merchant_key,
//                  upper(nvl(tpa.batch_processing_allowed,'N'))
//                                         as batch_processing_allowed,
//                  tpa.batch_processing_email_address 
//                                         as email_addresses
//          from    organization     o,
//                  group_merchant   gm,
//                  trident_profile  tp,
//                  trident_profile_api tpa
//          where   o.org_group = :getHierarchyNode() 
//                  and gm.org_num = o.org_num 
//                  and tp.merchant_number = gm.merchant_number
//                  and tp.terminal_id = :profileId
//                  and tpa.terminal_id(+) = tp.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3599 = getHierarchyNode();
  try {
   String theSqlTS = "select  tp.merchant_number     as merchant_number,\n                tp.merchant_name       as merchant_name,\n                tp.terminal_id         as profile_id,\n                tp.merchant_key        as merchant_key,\n                upper(nvl(tpa.batch_processing_allowed,'N'))\n                                       as batch_processing_allowed,\n                tpa.batch_processing_email_address \n                                       as email_addresses\n        from    organization     o,\n                group_merchant   gm,\n                trident_profile  tp,\n                trident_profile_api tpa\n        where   o.org_group =  :1  \n                and gm.org_num = o.org_num \n                and tp.merchant_number = gm.merchant_number\n                and tp.terminal_id =  :2 \n                and tpa.terminal_id(+) = tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3599);
   __sJT_st.setString(2,profileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.batchprocessing.BPdb",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        profile = new BPProfile(rs);
      }
      
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return( profile );
  }
  
  public List loadActivity( String profileId )
  {
    ResultSet           rs     = null;
    ResultSetIterator   it     = null;
    List                rows   = new ArrayList();

    try
    {      
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  req.merchant_id            as merchant_id,
//                  req.profile_id             as profile_id,
//                  req.req_file_id            as req_file_id,
//                  req.file_name              as req_filename,
//                  req.request_ts             as request_ts,
//                  nvl(req.test_flag,'N')     as test_flag,
//                  req.user_name              as user_name,
//                  resp.resp_file_id          as resp_file_id,
//                  resp.file_name             as resp_filename,
//                  resp.resp_ts               as response_ts
//          from    organization      o,
//                  group_merchant    gm,
//                  trident_profile   tp,
//                  batch_request_files   req,
//                  batch_response_files  resp
//          where   o.org_group =  :getHierarchyNode() and
//                  gm.org_num = o.org_num and
//                  tp.merchant_number = gm.merchant_number and
//                  tp.terminal_id = :profileId and
//                  req.merchant_id = tp.merchant_number and
//                  req.profile_id = tp.terminal_id and
//                  resp.merchant_id(+) = req.merchant_id and
//                  resp.profile_id(+) = req.profile_id and
//                  resp.req_file_id(+) = req.req_file_id
//          order by req.request_ts desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3600 = getHierarchyNode();
  try {
   String theSqlTS = "select  req.merchant_id            as merchant_id,\n                req.profile_id             as profile_id,\n                req.req_file_id            as req_file_id,\n                req.file_name              as req_filename,\n                req.request_ts             as request_ts,\n                nvl(req.test_flag,'N')     as test_flag,\n                req.user_name              as user_name,\n                resp.resp_file_id          as resp_file_id,\n                resp.file_name             as resp_filename,\n                resp.resp_ts               as response_ts\n        from    organization      o,\n                group_merchant    gm,\n                trident_profile   tp,\n                batch_request_files   req,\n                batch_response_files  resp\n        where   o.org_group =   :1  and\n                gm.org_num = o.org_num and\n                tp.merchant_number = gm.merchant_number and\n                tp.terminal_id =  :2  and\n                req.merchant_id = tp.merchant_number and\n                req.profile_id = tp.terminal_id and\n                resp.merchant_id(+) = req.merchant_id and\n                resp.profile_id(+) = req.profile_id and\n                resp.req_file_id(+) = req.req_file_id\n        order by req.request_ts desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3600);
   __sJT_st.setString(2,profileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.batchprocessing.BPdb",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^7*/

      rs = it.getResultSet();

      while( rs.next() )
      {
        rows.add( new BPReportRow(rs) );
      }
      
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return( rows );
  }
  
  public void setUser( UserBean user )
  {
    User = user;
  }
  
  public boolean storeRequestFile( RequestFile requestFile )
  {
    boolean       retVal         = true;
    BLOB          blobData       = null;
    OutputStream  bOut           = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] { insert into batch_request_files
//          (
//            req_file_id,
//            request_ts,
//            merchant_id,
//            profile_id,
//            file_name,
//            file_size,
//            test_flag,
//            user_name
//          )
//          values
//          (
//            :requestFile.getReqFileId(),
//            sysdate,
//            :requestFile.getMerchantId(),
//            :requestFile.getProfileId(),
//            :requestFile.getFilename(),
//            :requestFile.getFileSize(),
//            :requestFile.getTestFlag(),
//            :requestFile.getUserName()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3601 = requestFile.getReqFileId();
 String __sJT_3602 = requestFile.getMerchantId();
 String __sJT_3603 = requestFile.getProfileId();
 String __sJT_3604 = requestFile.getFilename();
 int __sJT_3605 = requestFile.getFileSize();
 String __sJT_3606 = requestFile.getTestFlag();
 String __sJT_3607 = requestFile.getUserName();
   String theSqlTS = "insert into batch_request_files\n        (\n          req_file_id,\n          request_ts,\n          merchant_id,\n          profile_id,\n          file_name,\n          file_size,\n          test_flag,\n          user_name\n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3601);
   __sJT_st.setString(2,__sJT_3602);
   __sJT_st.setString(3,__sJT_3603);
   __sJT_st.setString(4,__sJT_3604);
   __sJT_st.setInt(5,__sJT_3605);
   __sJT_st.setString(6,__sJT_3606);
   __sJT_st.setString(7,__sJT_3607);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:452^7*/

      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] { update  batch_request_files
//          set     file_data = empty_blob()
//          where   req_file_id = :requestFile.getReqFileId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3608 = requestFile.getReqFileId();
   String theSqlTS = "update  batch_request_files\n        set     file_data = empty_blob()\n        where   req_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3608);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/

      /*@lineinfo:generated-code*//*@lineinfo:461^7*/

//  ************************************************************
//  #sql [Ctx] { select  file_data 
//          from    batch_request_files
//          where   req_file_id = :requestFile.getReqFileId()
//          for update
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3609 = requestFile.getReqFileId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  file_data  \n        from    batch_request_files\n        where   req_file_id =  :1 \n        for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3609);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:467^7*/
      
      byte[] encryptedData = MesEncryption.encryptLargeData(requestFile.getFileData());
      bOut = blobData.setBinaryStream(1L);
      bOut.write(encryptedData, 0, encryptedData.length);
      bOut.flush();
      bOut.close();

      /*@lineinfo:generated-code*//*@lineinfo:475^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:478^7*/
    }
    catch( Exception e )
    {
      retVal = false;
      try{ /*@lineinfo:generated-code*//*@lineinfo:483^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:483^34*/ }catch( Exception ee){}
      logEntry("storeRequestFile(" + requestFile.getProfileId() + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return retVal;
  }
  
  public boolean storeRequests( String merchantId, String profileId, long fileId, String workFilename, long beginLineNum )
  {
    long               rowInserted   = 0L;
    long               lineNum   = 0L;
    String            line    = null;
    boolean           retVal  = true;
    BufferedReader    in      = null;
    
    log.debug("storeRequests() workFilename: " + workFilename + " beginLineNum " + beginLineNum);
    if( workFilename != null )
    {
      try
      {
        connect();

        in = new BufferedReader( new FileReader(workFilename) );
        while( (line = in.readLine()) != null )
        {
          line = line.trim();
          if ( line.trim().equals("") ) 
          {
            continue;   
          }
          
          long     rowId    = -1L;
          String  request  = null;
          try
          {
            StringTokenizer tokenizer = new StringTokenizer(line, "!@#");
            while( tokenizer.hasMoreTokens() )
            {
              rowId = Integer.parseInt(tokenizer.nextToken());
              request = tokenizer.nextToken();
              ++lineNum;
            }
          }
          catch( Exception e )
          {
          }
          
          if( rowId != -1L && lineNum >= beginLineNum )
          {
            ++rowInserted;
            /*@lineinfo:generated-code*//*@lineinfo:537^13*/

//  ************************************************************
//  #sql [Ctx] { insert into batch_requests
//                (
//                  req_file_id,
//                  merchant_id,
//                  profile_id,
//                  row_id,
//                  raw_req_data,
//                  mesdb_ts
//                )
//                values
//                (
//                  :fileId,
//                  :merchantId,
//                  :profileId,
//                  :rowId,
//                 :request,
//                  sysdate
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into batch_requests\n              (\n                req_file_id,\n                merchant_id,\n                profile_id,\n                row_id,\n                raw_req_data,\n                mesdb_ts\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                :5 ,\n                sysdate\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,fileId);
   __sJT_st.setString(2,merchantId);
   __sJT_st.setString(3,profileId);
   __sJT_st.setLong(4,rowId);
   __sJT_st.setString(5,request);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:557^13*/
            
            if( rowInserted % 300 == 0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:561^15*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:561^35*/
            }
          }
          else
          {
            continue;
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:570^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:570^29*/
        in.close();
      }
      catch( Exception e )
      {
        try{ /*@lineinfo:generated-code*//*@lineinfo:575^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:575^36*/ }catch( Exception ee){}
        retVal = false;
        logEntry( "storeRequests(" + merchantId + "," + profileId + "," + fileId + "," + workFilename + ")", e.toString());
      }
      finally
      {
        log.debug("storeRequests() total inserted records: " + rowInserted);
        try 
        {
          if( in!= null ) { in.close(); }
        }
        catch( Exception eee ){ }
        cleanUp();
      }
    }
    return retVal;
  }
  
  public boolean storeResponseFile( ResponseFile responseFile )
  {
    boolean       retVal         = true;
    BLOB          blobData       = null;
    OutputStream  bOut           = null;
    
    if( responseFile != null )
    {
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:605^9*/

//  ************************************************************
//  #sql [Ctx] { insert into batch_response_files
//            (
//              resp_file_id,
//              resp_ts,
//              req_file_id,
//              merchant_id,
//              profile_id,
//              file_name,
//              file_size,
//              test_flag,
//              approved_count,
//              approved_amount,
//              declined_count,
//              declined_amount
//            )
//            values
//            (
//              :responseFile.getRespFileId(),
//              sysdate,
//              :responseFile.getReqFileId(),
//              :responseFile.getMerchantId(),
//              :responseFile.getProfileId(),
//              :responseFile.getFilename(),
//              :responseFile.getFileSize(),
//              :responseFile.getTestFlag(),
//              :responseFile.getApprovedCount(),
//              :responseFile.getApprovedAmount(),
//              :responseFile.getDeclinedCount(),
//              :responseFile.getDeclinedAmount()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3610 = responseFile.getRespFileId();
 long __sJT_3611 = responseFile.getReqFileId();
 String __sJT_3612 = responseFile.getMerchantId();
 String __sJT_3613 = responseFile.getProfileId();
 String __sJT_3614 = responseFile.getFilename();
 int __sJT_3615 = responseFile.getFileSize();
 String __sJT_3616 = responseFile.getTestFlag();
 int __sJT_3617 = responseFile.getApprovedCount();
 double __sJT_3618 = responseFile.getApprovedAmount();
 int __sJT_3619 = responseFile.getDeclinedCount();
 double __sJT_3620 = responseFile.getDeclinedAmount();
   String theSqlTS = "insert into batch_response_files\n          (\n            resp_file_id,\n            resp_ts,\n            req_file_id,\n            merchant_id,\n            profile_id,\n            file_name,\n            file_size,\n            test_flag,\n            approved_count,\n            approved_amount,\n            declined_count,\n            declined_amount\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3610);
   __sJT_st.setLong(2,__sJT_3611);
   __sJT_st.setString(3,__sJT_3612);
   __sJT_st.setString(4,__sJT_3613);
   __sJT_st.setString(5,__sJT_3614);
   __sJT_st.setInt(6,__sJT_3615);
   __sJT_st.setString(7,__sJT_3616);
   __sJT_st.setInt(8,__sJT_3617);
   __sJT_st.setDouble(9,__sJT_3618);
   __sJT_st.setInt(10,__sJT_3619);
   __sJT_st.setDouble(11,__sJT_3620);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^9*/

        /*@lineinfo:generated-code*//*@lineinfo:639^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_response_files
//            set     file_data = empty_blob()
//            where   resp_file_id = :responseFile.getRespFileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3621 = responseFile.getRespFileId();
   String theSqlTS = "update  batch_response_files\n          set     file_data = empty_blob()\n          where   resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3621);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:644^9*/

        /*@lineinfo:generated-code*//*@lineinfo:646^9*/

//  ************************************************************
//  #sql [Ctx] { select  file_data 
//            from    batch_response_files
//            where   resp_file_id = :responseFile.getRespFileId()
//            for update
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3622 = responseFile.getRespFileId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  file_data  \n          from    batch_response_files\n          where   resp_file_id =  :1 \n          for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3622);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^9*/
        
        bOut = blobData.setBinaryStream(1L);
        bOut.write( responseFile.getFileData(), 0, responseFile.getFileSize() );
        bOut.flush();
        bOut.close();

        /*@lineinfo:generated-code*//*@lineinfo:659^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:662^9*/
      }
      catch( Exception e )
      {
        retVal = false;
        try{ /*@lineinfo:generated-code*//*@lineinfo:667^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:667^36*/ }catch( Exception ee){}
        logEntry("storeResponseFile(" + responseFile.getProfileId() + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    return retVal;
  }
  
  public boolean storeResponses( String merchantId, String profileId, long reqFileId, long respFileId, String workFilename, long beginLineNum )
  {
    long             rowInserted = 0L;
    long             lineNum   = 0L;
    String            line    = null;
    boolean           retVal  = true;
    BufferedReader    in      = null;
    log.debug("storeResponses() workFilename: " + workFilename + " beginLineNum " + beginLineNum);
    if( workFilename != null )
    {
      try
      {
        connect();
        
        in = new BufferedReader( new FileReader(workFilename) );
        while( (line = in.readLine()) != null )
        {
          line = line.trim();
          if ( line.trim().equals("") ) 
          {
            continue;   
          }
          
          long     rowId       = -1L;
          String  response  = null;
          try
          {
            StringTokenizer tokenizer = new StringTokenizer(line, "!@#");
            while( tokenizer.hasMoreTokens() )
            {
              rowId = Long.parseLong(tokenizer.nextToken());
              response = tokenizer.nextToken();
              ++lineNum;
            }
          }
          catch( Exception e )
          {
            log.debug("line " + line + " ERROR " + e.toString());
          }
          
          if( rowId != -1L && lineNum >= beginLineNum )
          {
            ++rowInserted;
            /*@lineinfo:generated-code*//*@lineinfo:722^13*/

//  ************************************************************
//  #sql [Ctx] { insert into batch_responses
//                (
//                  resp_file_id,
//                  merchant_id,
//                  profile_id,
//                  req_file_id,
//                  row_id,
//                  raw_resp_data,
//                  mesdb_ts
//                )
//                values
//                (
//                  :respFileId,
//                  :merchantId,
//                  :profileId,
//                  :reqFileId,
//                  :rowId,
//                  :response,
//                  sysdate
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into batch_responses\n              (\n                resp_file_id,\n                merchant_id,\n                profile_id,\n                req_file_id,\n                row_id,\n                raw_resp_data,\n                mesdb_ts\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 ,\n                sysdate\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,respFileId);
   __sJT_st.setString(2,merchantId);
   __sJT_st.setString(3,profileId);
   __sJT_st.setLong(4,reqFileId);
   __sJT_st.setLong(5,rowId);
   __sJT_st.setString(6,response);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:744^13*/
            
            if( rowInserted % 200 == 0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:748^15*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:748^35*/
            }
          }
          else
          {
            continue;
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:757^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:757^29*/
        in.close();
      }
      catch( Exception e )
      {
        e.printStackTrace();
        try{ /*@lineinfo:generated-code*//*@lineinfo:763^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:763^36*/ }catch( Exception ee){}
        retVal = false;
        logEntry( "storeResponses(" + merchantId + "," + profileId + "," + reqFileId + "," + respFileId + "," + workFilename + ")", e.toString());
      }
      finally
      {
        log.debug("storeResponses() total inserted records: " + rowInserted);
        try 
        {
          if( in!= null ) { in.close(); }
        }
        catch( Exception eee ){ }
        cleanUp();
      }
    }

    return retVal;
  }
  

  public DropDownItem[] getProfileList()
  {
    ResultSetIterator   it       = null;
    ResultSet           rs       = null;
    List                list     = new ArrayList();

    try
    {      
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:793^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id    as profile_id,
//                  tp.terminal_id || ' - ' || tp.merchant_name
//                                    as profile_desc
//          from    organization     o,
//                  group_merchant   gm,
//                  trident_profile  tp,
//                  trident_profile_api tpa
//          where   o.org_group = :getHierarchyNode()
//                  and gm.org_num = o.org_num 
//                  and tp.merchant_number = gm.merchant_number
//                  and tpa.terminal_id = tp.terminal_id
//                  and upper(nvl(tpa.batch_processing_allowed,'N')) = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3623 = getHierarchyNode();
  try {
   String theSqlTS = "select  tp.terminal_id    as profile_id,\n                tp.terminal_id || ' - ' || tp.merchant_name\n                                  as profile_desc\n        from    organization     o,\n                group_merchant   gm,\n                trident_profile  tp,\n                trident_profile_api tpa\n        where   o.org_group =  :1 \n                and gm.org_num = o.org_num \n                and tp.merchant_number = gm.merchant_number\n                and tpa.terminal_id = tp.terminal_id\n                and upper(nvl(tpa.batch_processing_allowed,'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3623);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.batchprocessing.BPdb",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:807^7*/

      rs = it.getResultSet();

      while( rs.next() )
      {
        list.add(new DropDownItem(rs.getString("profile_id"), rs.getString("profile_desc")));
      }
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return( DropDownItem[])list.toArray(new DropDownItem[]{ } );
  }
  
  public boolean updateResponseData( ResponseFile responseFile )
  {
    boolean       retVal         = true;
    BLOB          blobData       = null;
    OutputStream  bOut           = null;
    
    if( responseFile != null )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:839^9*/

//  ************************************************************
//  #sql [Ctx] { update batch_response_files
//            set    file_size        = :responseFile.getFileSize(),
//                   approved_count   = :responseFile.getApprovedCount(),
//                   approved_amount  = :responseFile.getApprovedAmount(),
//                   declined_count   = :responseFile.getDeclinedCount(),
//                   declined_amount  = :responseFile.getDeclinedAmount()
//            where  merchant_id      = :responseFile.getMerchantId()
//                   and profile_id   = :responseFile.getReqFileId()
//                   and resp_file_id = :responseFile.getRespFileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_3624 = responseFile.getFileSize();
 int __sJT_3625 = responseFile.getApprovedCount();
 double __sJT_3626 = responseFile.getApprovedAmount();
 int __sJT_3627 = responseFile.getDeclinedCount();
 double __sJT_3628 = responseFile.getDeclinedAmount();
 String __sJT_3629 = responseFile.getMerchantId();
 long __sJT_3630 = responseFile.getReqFileId();
 long __sJT_3631 = responseFile.getRespFileId();
   String theSqlTS = "update batch_response_files\n          set    file_size        =  :1 ,\n                 approved_count   =  :2 ,\n                 approved_amount  =  :3 ,\n                 declined_count   =  :4 ,\n                 declined_amount  =  :5 \n          where  merchant_id      =  :6 \n                 and profile_id   =  :7 \n                 and resp_file_id =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_3624);
   __sJT_st.setInt(2,__sJT_3625);
   __sJT_st.setDouble(3,__sJT_3626);
   __sJT_st.setInt(4,__sJT_3627);
   __sJT_st.setDouble(5,__sJT_3628);
   __sJT_st.setString(6,__sJT_3629);
   __sJT_st.setLong(7,__sJT_3630);
   __sJT_st.setLong(8,__sJT_3631);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:850^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:852^9*/

//  ************************************************************
//  #sql [Ctx] { update  batch_response_files
//            set     file_data = empty_blob()
//            where   resp_file_id = :responseFile.getRespFileId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3632 = responseFile.getRespFileId();
   String theSqlTS = "update  batch_response_files\n          set     file_data = empty_blob()\n          where   resp_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.batchprocessing.BPdb",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3632);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:857^9*/

        /*@lineinfo:generated-code*//*@lineinfo:859^9*/

//  ************************************************************
//  #sql [Ctx] { select  file_data 
//            from    batch_response_files
//            where   resp_file_id = :responseFile.getRespFileId()
//            for update
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3633 = responseFile.getRespFileId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  file_data  \n          from    batch_response_files\n          where   resp_file_id =  :1 \n          for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.batchprocessing.BPdb",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3633);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:865^9*/
        
        bOut = blobData.setBinaryStream(1L);
        bOut.write( responseFile.getFileData(), 0, responseFile.getFileSize() );
        bOut.flush();
        bOut.close();

        /*@lineinfo:generated-code*//*@lineinfo:872^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:875^9*/
      }
      catch( Exception e )
      {
        retVal = false;
        try{ /*@lineinfo:generated-code*//*@lineinfo:880^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:880^36*/ }catch( Exception ee){}
        logEntry("updateResponseFile(" + responseFile.getProfileId() + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    return retVal;
  }

}/*@lineinfo:generated-code*/