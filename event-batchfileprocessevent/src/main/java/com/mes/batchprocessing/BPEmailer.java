package com.mes.batchprocessing;

import org.apache.log4j.Logger;
import com.mes.net.MailMessage;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;


public class BPEmailer
{
  public static final String MES_BATCH_UPLOAD_EMAIL    = "mesPGbatchprocessing@merchante-solutions.com";
  
  static   Logger       log      = Logger.getLogger(BPEmailer.class);

  private  MailMessage  Mailer   = null;
  
  public BPEmailer()
  {
  }
  
  public void addTo( String emailAddress )
  {
    try{ getMailer().addTo(emailAddress); }catch( Exception e ){ }
  }
  
  private MailMessage getMailer()
  {
    if( Mailer == null )
    {
      try{ Mailer = new MailMessage(); }catch( Exception e ){ }
    }
    
    return Mailer;
  }
  
  public void sendConfirmationEmail()
  {
    StringBuffer body = new StringBuffer();
    
    try
    {
      body.append("DO NOT REPLY TO THIS MESSAGE - This email has been automatically generated." );
      body.append("\n");
      body.append("\n");
      body.append("MERCHANT E-SOLUTIONS");
      body.append("\n");
      body.append("MeS Payment Gateway batch file processing has received and prepared your file for processing. ");
      body.append("You will be receiving a second email with the response data.");
      body.append("\n");
      body.append("\n");
      body.append("Please keep in mind that the response data is also available via our standard MeS web reporting options.");
      body.append("\n");
      body.append("\n");
      body.append("If you have any questions about this email, your Merchant e-Solutions merchant account or processing procedures, ");
      body.append("please contact our 24-hour Help Desk at 888-288-2692 or help@merchante-solutions.com.");
      body.append("\n");
      body.append("\n");
      body.append("DO NOT REPLY TO THIS MESSAGE - This email has been automatically generated.");
      body.append("\n");

      getMailer().setFrom(MES_BATCH_UPLOAD_EMAIL);
      getMailer().setSubject("MES Payment Gateway Batch File processing was received");
      getMailer().setText(body.toString());
      getMailer().send();
      
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
    }
  }
  
  public void sendResponseFile( ResponseFile respFile )
  {
    int          stringLength  = 3;
    String       format        = "###,###,##0";
    String       totalTran     = "";
    String       approved      = "";
    String       declined      = "";
    StringBuffer body          = new StringBuffer();
    
    try
    {
      totalTran = NumberFormatter.getLongString(Long.parseLong(String.valueOf(respFile.getTotalTranCount())), format);
      approved  = NumberFormatter.getLongString(Long.parseLong(String.valueOf(respFile.getApprovedCount())), format);
      declined  = NumberFormatter.getLongString(Long.parseLong(String.valueOf(respFile.getDeclinedCount())), format);
      stringLength += totalTran.length();
      
      body.append("DO NOT REPLY TO THIS MESSAGE - This email has been automatically generated." );
      body.append("\n");
      body.append("\n");
      body.append("MERCHANT E-SOLUTIONS");
      body.append("\n");
      body.append("MeS Payment Gateway batch file processed your transactions and prepared a response file. ");
      body.append("The attached .txt file contains a response for each of your transactions.");
      body.append("\n");
      body.append("\n");
      body.append("Here is a summary of your file.");
      body.append("\n");
      body.append("Request File - ");
      body.append(respFile.getReqFileId());
      body.append("\n");
      body.append("Respose File - ");
      body.append(respFile.getRespFileId());
      body.append("\n");
      body.append("\n");
      body.append("   # of Transaction: ");
      body.append(StringUtilities.leftJustify(totalTran, stringLength, ' '));
      body.append("$ Amount:   ");
      body.append(MesMath.toCurrency(respFile.getTotalAmount()));
      body.append("\n");
      body.append("   # Approved:       ");
      body.append(StringUtilities.leftJustify(approved, stringLength, ' '));
      body.append("$ Approved: ");
      body.append(MesMath.toCurrency(respFile.getApprovedAmount()));
      body.append("\n");
      body.append("   # Declined:       ");
      body.append(StringUtilities.leftJustify(declined, stringLength, ' '));
      body.append("$ Declined: ");
      body.append(MesMath.toCurrency(respFile.getDeclinedAmount()));
      body.append("\n");
      body.append("\n");
      body.append("Please remember this response data is also available via our standard MeS web reporting options.");
      body.append("\n");
      body.append("\n");
      body.append("If you have any questions about this email, your Merchant e-Solutions merchant account or processing procedures, ");
      body.append("please contact our 24-hour Help Desk at 888-288-2692 or help@merchante-solutions.com.");
      body.append("\n");
      body.append("\n");
      body.append("DO NOT REPLY TO THIS MESSAGE - This email has been automatically generated.");
      body.append("\n");

      getMailer().setFrom(MES_BATCH_UPLOAD_EMAIL);
      getMailer().setSubject("MES Payment Gateway Batch File processing results");
      getMailer().setText(body.toString());
      try{ getMailer().addFile(respFile.getFilename(), respFile.getFileData()); }catch( Exception e ){}
      getMailer().send();
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
    }
  }
}