package com.mes.batchprocessing;

import java.sql.ResultSet;
import java.sql.Timestamp;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;

public class BPReportRow
{
  static Logger log = Logger.getLogger(BPReportRow.class);
  
  private  String     MerchantId       = null;
  private  String     ProfileId        = null;
  private  long       ReqFileId        = 0L;
  private  String     ReqFilename      = null;
  private  Timestamp  RequestTs        = null;
  private  long       RespFileId       = 0L;
  private  String     RespFilename     = null;
  private  Timestamp  ResponseTs       = null;
  private  String     TestFlag         = null;
  private  String     UserName         = null;

  public BPReportRow()
  {
  }
  
  public BPReportRow( ResultSet rs )
   throws java.sql.SQLException
  {
    MerchantId      = rs.getString("merchant_id"); 
    ProfileId       = rs.getString("profile_id");
    ReqFileId       = rs.getLong("req_file_id");
    ReqFilename     = rs.getString("req_filename");
    RequestTs       = rs.getTimestamp("request_ts");
    RespFileId      = rs.getLong("resp_file_id");
    RespFilename    = rs.getString("resp_filename");
    ResponseTs      = rs.getTimestamp("response_ts");
    TestFlag        = rs.getString("test_flag");
    UserName        = rs.getString("user_name");
  }
  
  public void setMerchantId(String merchantId){ MerchantId = merchantId; }
  public void setProfileId(String profileId){ ProfileId = profileId; }
  public void setReqFileId(long reqFileId){ ReqFileId = reqFileId; }
  public void setReqFilename(String reqFilename){ ReqFilename = reqFilename; }
  public void setRequestTs(Timestamp requestTs){ RequestTs = requestTs; }
  public void setRespFileId(long respFileId){ RespFileId = respFileId; }
  public void setRespFilename(String respFilename){ RespFilename = respFilename; }
  public void setResponseTs(Timestamp responseTs){ ResponseTs = responseTs ;}
  public void setTestFlag(String testFlag){ TestFlag = testFlag; }
  public void setUserName(String userName){ UserName = userName; }
  
  public String getMerchantId(){ return MerchantId; }
  public String getProfileId(){ return ProfileId; }
  public long getReqFileId(){ return ReqFileId; }
  public String getReqFilename(){ return ReqFilename; }
  public Timestamp getRequestTs(){ return RequestTs; }
  public String getFormattedRequestTs(){ return ( DateTimeFormatter.getFormattedDate(RequestTs,"MM/dd/yyyy  h:mm:ss a") ); }
  public long getRespFileId(){ return RespFileId; }
  public String getRespFilename(){ return RespFilename; }
  public Timestamp getResponseTs(){ return ResponseTs ;}
  public String getFormattedResponseTs(){ return( DateTimeFormatter.getFormattedDate(ResponseTs,"MM/dd/yyyy  h:mm:ss a") ); }
  public String getTestFlag(){ return TestFlag; }
  public String getUserName(){ return UserName; }
  public boolean isTest(){ return TestFlag.equals("Y"); }
}