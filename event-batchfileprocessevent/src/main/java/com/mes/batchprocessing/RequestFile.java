package com.mes.batchprocessing;

import java.sql.Timestamp;
import com.mes.support.DateTimeFormatter;

public class RequestFile
{
  private Timestamp  CreateTs;
  private byte[]     FileData;
  private String     Filename;
  private int        FileSize;
  private String     MerchantId;
  private String     ProfileId;
  private long       ReqFileId;
  private String     TestFlag;
  private String     UserName;

  public RequestFile()
  {
  }

  public RequestFile( String merchantId, String profileId, long reqFileId, Timestamp createTs,
                      String userName, String filename, byte[] fileData, int fileSize, String testFlag )
  {
    CreateTs      = createTs;
    FileData      = fileData;
    Filename      = filename;
    FileSize      = fileSize;
    MerchantId    = merchantId;
    ProfileId     = profileId;
    ReqFileId     = reqFileId;
    TestFlag      = testFlag;
    UserName      = userName;
  }
  
  public void setCreateTs( Timestamp createTs ){ CreateTs = createTs; }
  public void setFileData( byte[] fileData ){ FileData = fileData; }
  public void setFilename( String filename ){ Filename = filename; }
  public void setFileSize( int fileSize ){ FileSize = fileSize; }
  public void setMerchantId( String merchantId ){ MerchantId = merchantId; }
  public void setProfileId( String profileId ){ ProfileId = profileId; }
  public void setReqFileId( long reqFileId ){ ReqFileId = reqFileId; }
  public void setTestFlag( String testFlag ){ TestFlag = testFlag; }
  public void setUserName( String userName ){ UserName = userName; }

  public String    getFormattedCreateTs(){ return( DateTimeFormatter.getFormattedDate(CreateTs,"MM/dd/yyyy  h:mm:ss a")); }
  public Timestamp getCreateTs(){ return CreateTs; }
  public byte[]    getFileData(){ return FileData; }
  public String    getFilename(){ return Filename; }
  public int       getFileSize(){ return FileSize; }
  public String    getMerchantId(){ return MerchantId; }
  public String    getProfileId(){ return ProfileId; }
  public long      getReqFileId(){ return ReqFileId; }
  public String    getTestFlag(){ return TestFlag; }
  public String    getUserName(){ return UserName; }
}
