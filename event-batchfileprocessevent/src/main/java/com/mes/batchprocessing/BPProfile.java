package com.mes.batchprocessing;

import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class BPProfile
{
  static Logger log = Logger.getLogger(BPReportRow.class);
  
  private  String  MerchantId              = null;
  private  String  MerchantKey             = null;
  private  String  MerchantName            = null;
  private  String  ProfileId               = null;
  private  String  BatchProcessingAllowed  = null;
  private  String  EmailAddresses          = null;

  public BPProfile()
  {
  }
  
  public BPProfile( ResultSet rs )
   throws java.sql.SQLException
  {
    MerchantId              = rs.getString("merchant_number"); 
    MerchantKey             = rs.getString("merchant_key"); 
    MerchantName            = rs.getString("merchant_name"); 
    ProfileId               = rs.getString("profile_id");
    BatchProcessingAllowed  = rs.getString("batch_processing_allowed");
    EmailAddresses          = rs.getString("email_addresses");
  }
  
  public void setMerchantId(String merchantId){ MerchantId = merchantId; }
  public void setMerchantKey(String merchantKey){ MerchantKey = merchantKey; }
  public void setMerchantName(String merchantName){ MerchantName = merchantName; }
  public void setProfileId(String profileId){ ProfileId = profileId; }
  public void setBatchProcessingFlag(String flag){ BatchProcessingAllowed = flag; }
  public void setEmailAddresses(String emailAddresses){ EmailAddresses = emailAddresses; }
  
  public String  getMerchantId(){ return MerchantId; }
  public String  getMerchantKey(){ return MerchantKey; }
  public String  getMerchantName(){ return MerchantName; }
  public String  getProfileId(){ return ProfileId; }
  public String  getEmailAddresses(){ return EmailAddresses; }
  public boolean hasEmailAddresses(){ return( EmailAddresses != null && !EmailAddresses.equals("") ); }
  public boolean isBatchProcessingAllowed(){ return BatchProcessingAllowed.equals("Y"); }
}