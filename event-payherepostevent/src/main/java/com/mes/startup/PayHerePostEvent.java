/*@lineinfo:filename=PayHerePostEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import org.apache.log4j.Logger;
//@import org.apache.commons.codec.binary.Base64;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.ThreadPool;
import com.mes.tools.Base64;
import sqlj.runtime.ResultSetIterator;

public class PayHerePostEvent extends EventBase
{
  static Logger log = Logger.getLogger(PayHerePostEvent.class);
  private long   SequenceId  = -1L;
  
  protected class PayHerePostWorker extends SQLJConnectionBase
    implements Runnable
  {
    private long   RecId     = -1L;
    
    public PayHerePostWorker( long recId )
    {
      RecId   = recId;
    }
    
    public void run()
    {
      long      phRecId           = -1;
      String    data              = null;
      String    response          = null;
      String    responseUrl       = null;
      String    uidpwd            = null;
      URL       url               = null;
      HttpURLConnection  con      = null;
      ResultSet          rs       = null;
      ResultSetIterator  it       = null;
      StringBuffer       buff     = new StringBuffer();
      try
      {
        connect(true);
        recordTimestampBegin(RecId);
        log.debug("retrieving post data");
        /*@lineinfo:generated-code*//*@lineinfo:53^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pd.rec_id             as ph_rec_id,
//                    pd.merchant_number    as merchant_number,
//                    pd.profile_id         as profile_id,
//                    pd.tran_date          as tran_date,
//                    pd.tran_type          as tran_type,
//                    pd.tran_amount        as tran_amount,
//                    pd.card_number        as card_number,
//                    pd.trident_tran_id    as trident_tran_id,
//                    pd.post_data          as post_data,
//                    pd.url                as resp_url,
//                    nvl(md.resp_username,'passall') || ':' ||
//                    nvl(md.resp_password,'passall')
//                                          as resp_uidpwd
//            from    payhere_process pc,
//                    payhere_post_data pd,
//                    payhere_merchant_data md
//            where   pc.rec_id = :RecId and
//                    pd.rec_id = pc.ph_rec_id and
//                    md.profile_id (+) = pd.profile_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pd.rec_id             as ph_rec_id,\n                  pd.merchant_number    as merchant_number,\n                  pd.profile_id         as profile_id,\n                  pd.tran_date          as tran_date,\n                  pd.tran_type          as tran_type,\n                  pd.tran_amount        as tran_amount,\n                  pd.card_number        as card_number,\n                  pd.trident_tran_id    as trident_tran_id,\n                  pd.post_data          as post_data,\n                  pd.url                as resp_url,\n                  nvl(md.resp_username,'passall') || ':' ||\n                  nvl(md.resp_password,'passall')\n                                        as resp_uidpwd\n          from    payhere_process pc,\n                  payhere_post_data pd,\n                  payhere_merchant_data md\n          where   pc.rec_id =  :1  and\n                  pd.rec_id = pc.ph_rec_id and\n                  md.profile_id (+) = pd.profile_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,RecId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.PayHerePostEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^9*/
        rs = it.getResultSet();
      
        while( rs.next() )
        {
          data          = rs.getString("post_data");
          uidpwd        = rs.getString("resp_uidpwd");
          phRecId       = rs.getLong("ph_rec_id");
          responseUrl   = rs.getString("resp_url");
        }
        it.close();
        
        // Encode the user ID and password
        byte[] inputBytes = uidpwd.getBytes();
        String header = Base64.encodeBytes(inputBytes, Base64.DONT_BREAK_LINES);
        Instant startTime = null;
        Instant endTime;
        
        try
        {
          url = new URL( responseUrl );
          log.debug("connecting to: " + responseUrl);
          con = (HttpURLConnection)url.openConnection();
          con.setRequestMethod("POST");
          con.setDoInput(true);
          con.setDoOutput(true);
          con.setReadTimeout(MesDefaults.getInt(MesDefaults.READ_TIMEOUT));
          con.setConnectTimeout(MesDefaults.getInt(MesDefaults.CONNECT_TIMEOUT));
          if( !uidpwd.equals("passall:passall") )
          {
            con.setRequestProperty("Authorization","BASIC " + header);
          }
          con.setRequestProperty("Content-type", "application/x-www-form-urlencoded" );
          con.setRequestProperty("Content-length", String.valueOf(data.length()));
          con.setUseCaches(false);
          
          log.debug("posting data");
          startTime = java.time.Instant.now();
          DataOutputStream wr = new DataOutputStream(con.getOutputStream());
          wr.writeBytes(data);
          wr.flush();
          wr.close();
          endTime = java.time.Instant.now();
          Duration duration = java.time.Duration.between(startTime, endTime);
          log.debug("Posted data in: " + duration);

          log.debug("receiving response");
          startTime = java.time.Instant.now();
          // This line throws an IOException  if response code is not 200
          BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
          response = con.getResponseCode() + "-" + con.getResponseMessage();
          in.close();
          endTime = java.time.Instant.now();
          duration = java.time.Duration.between(startTime, endTime);
          log.debug("Received response after: " + duration);
        }
        catch( Exception e )
        {
          logEntry("postData()",e.toString());
          if (startTime != null) {
              endTime = java.time.Instant.now();
              Duration duration = java.time.Duration.between(startTime, endTime);
              log.debug("Threw an exception after: " + duration);
          }
          logEntry("run()","POST data for the thrown exception");
          logEntry("run(): phRecId = ", "" + phRecId);
          logEntry("run(): data = ", data);
        }
        finally
        {
          try {
              con.disconnect();
          } catch(Exception e) {
              log.debug("HttpURLConnection disconnection failed");
              logEntry("run()", e.getMessage());
          }
        }
        
        log.debug("response: " + response);
        
        /*@lineinfo:generated-code*//*@lineinfo:130^9*/

//  ************************************************************
//  #sql [Ctx] { update  payhere_post_data
//            set     date_posted = sysdate,
//                    response = :response
//            where   rec_id = :phRecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  payhere_post_data\n          set     date_posted = sysdate,\n                  response =  :1 \n          where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,response);
   __sJT_st.setLong(2,phRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/

        recordTimestampEnd(RecId);
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "run()", e.toString() );
      }
      finally
      {
        try { it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
    
    protected void recordTimestampBegin( long recId )
    {
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:157^9*/

//  ************************************************************
//  #sql [Ctx] { update  payhere_process
//            set     process_begin_date = :beginTime
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  payhere_process\n          set     process_begin_date =  :1 \n          where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampBegin()", e.toString());
      }      
    }
    
    protected void recordTimestampEnd( long recId )
    {
      long       elapsed     = 0L;
      Timestamp  beginTime   = null;
      Timestamp  endTime     = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:178^9*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//            from    payhere_process
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n          from    payhere_process\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.PayHerePostEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^9*/
        
        elapsed = (endTime.getTime() - beginTime.getTime());
      
        /*@lineinfo:generated-code*//*@lineinfo:187^9*/

//  ************************************************************
//  #sql [Ctx] { update  payhere_process
//            set     process_end_date = :endTime,
//                    process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4519 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  payhere_process\n          set     process_end_date =  :1 ,\n                  process_elapsed =  :2 \n          where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4519);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^9*/
      }
      catch( java.sql.SQLException sqle )
      {
        logEntry("recordTimestampEnd()", sqle.toString());
      }      
    }
  }
  
  public boolean execute()
  {
    int itemCount      = 0;

    try
    {
      connect(true);

      /*@lineinfo:generated-code*//*@lineinfo:210^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ph_rec_id) 
//          from    payhere_process
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ph_rec_id)  \n        from    payhere_process\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.PayHerePostEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^7*/
      
      if ( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:219^9*/

//  ************************************************************
//  #sql [Ctx] { select  payhere_process_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  payhere_process_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.PayHerePostEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   SequenceId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:223^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:225^9*/

//  ************************************************************
//  #sql [Ctx] { update  payhere_process
//            set     process_sequence = :SequenceId
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  payhere_process\n          set     process_sequence =  :1 \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,SequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^9*/
      }
      
      process();
    }
    catch( Exception e )
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  private void process()
  {
    long              recId             = 0L;
    boolean           processEnded      = false;
    ThreadPool        pool              = null;
    ResultSet         rs                = null;
    ResultSetIterator it                = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:256^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id      as rec_id,
//                  ph_rec_id   as ph_rec_id
//          from    payhere_process 
//          where   process_sequence = :SequenceId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id      as rec_id,\n                ph_rec_id   as ph_rec_id\n        from    payhere_process \n        where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.PayHerePostEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,SequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.PayHerePostEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:262^7*/
      rs = it.getResultSet();
      
      pool = new ThreadPool(1);
      while( rs.next() )
      {
        recId = rs.getLong("rec_id");
        Thread worker = pool.getThread( new PayHerePostWorker(recId) );
        
        worker.start();
        Thread.sleep(20000);
        processEnded = processEnded(recId);
        for ( int i = 0; i < 2 && !processEnded; ++ i )
        {
          Thread.sleep(10000);
          processEnded = processEnded(recId);
        }
        
        if( !processEnded )
        {
            log.debug("As of this message, process has not ended cleanly for thread rec_id=" + recId);
        }
      }
      rs.close();
      pool.waitForAllThreads();
    }
    catch( Exception e )
    {
      logEntry("process() recId " + recId,e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {
          logEntry("process()", ee.getMessage());
      }
    }
  }
  
  private boolean processEnded( long recId )
  {
    Date endDate = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:304^7*/

//  ************************************************************
//  #sql [Ctx] { select process_end_date 
//          from   payhere_process
//          where  rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select process_end_date  \n        from   payhere_process\n        where  rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.PayHerePostEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   endDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:309^7*/
    }
    catch( Exception e )
    {
        logEntry("processEnded(long)", e.getMessage());
    }
    
    return( endDate != null );
  }
  
  public void setSequenceId( long seqId )
  {
    SequenceId = seqId;
  }
  
  public static void main( String[] args )
  {
    PayHerePostEvent ph = null;

    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone();
      
      ph = new PayHerePostEvent();
      
      if ( args.length == 2 && args[0].equals("post") )
      {
        // args[1] : PAYHERE_PROCESS.PROCESS_SEQUENCE
        ph.setSequenceId( Long.parseLong(args[1]) );
        ph.process();
      }
      else
      {
        ph.execute();
      }
    }
    catch ( Exception e )
    {
    }
  }
}/*@lineinfo:generated-code*/