/*@lineinfo:filename=MonthlyBillingSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MonthlyBillingSummary.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $LastChangedDate: 2015-11-12 14:43:38 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23991 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import com.mes.constants.mesConstants;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.mbs.BillingDb;
import com.mes.mbs.IcBillingData;
import com.mes.mbs.IcVolume;
import com.mes.mbs.MbsTools;
import com.mes.mbs.MerchantBilling;
import com.mes.mbs.MerchantBillingElement;
import com.mes.settlement.SettlementDb;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class MonthlyBillingSummary extends EventBase
{
  protected Date                  FileActivityDate      = null;
  protected Date                  TestFileDate          = null;
  protected String                TestFilename          = null;
  protected long                  TestNodeId            = 0L;
  
  private class SummaryJob extends SQLJConnectionBase
    implements Runnable
  {
    protected     String            AccountStatus = null;
    protected     Date              ActiveDate    = null;
    private       MerchantBilling   BillingData   = null;
    protected     long              MerchantId    = 0L;
    protected     Date              MonthEndDate  = null;
    protected     String            LoadFilename  = null;
    protected     long              LoadSec       = 0L;
    
    public SummaryJob()
    {
    }
    
    public SummaryJob( long merchantId, String accountStatus, Date activeDate, Date monthEndDate, long loadSec, String loadFilename )
    {
      init( merchantId, accountStatus, activeDate, monthEndDate, loadSec, loadFilename );
    }
    
    protected void init( long merchantId, String accountStatus, Date activeDate, Date monthEndDate, long loadSec, String loadFilename )
    {
      AccountStatus = accountStatus;
      MerchantId    = merchantId;
      LoadFilename  = loadFilename;
      ActiveDate    = activeDate;
      MonthEndDate  = monthEndDate;
      LoadSec       = loadSec;
      
      BillingData = BillingDb.loadMerchantBilling(MerchantId,MonthEndDate);
    }
    
    public void run( )
    {
      try
      {
        connect(true);
        
        log.debug("processing " + MerchantId + " (" + LoadSec + ")");//@
      
        if ( !"C".equals(AccountStatus) )
        {
          loadCG( LoadFilename, LoadSec, MerchantId, ActiveDate );  // charge records
        }            
        loadPL( LoadFilename, LoadSec, MerchantId );              // plan records
        loadDN( LoadFilename, LoadSec, MerchantId );              // debit networks
        loadAP( LoadFilename, LoadSec, MerchantId );              // auth records
        loadST( LoadFilename, LoadSec, MerchantId );              // statement records
        //@loadC1( loadFilename, loadSec, merchantId );              // capture, obsolete?
        loadIC( LoadFilename, LoadSec, MerchantId, ActiveDate );  // interchange
        loadGN( LoadFilename, LoadSec, MerchantId, ActiveDate );  // merchant records (do last)
      }
      catch( Exception e )
      {
        logEntry("run()", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    protected void addInterchangeDN( String loadFilename, long loadSec, long merchantId )
    {
      String              icPassthrough     = "N";
      ResultSetIterator   it                = null;
      ResultSet           resultSet         = null;
      String trIcPassthrough = "N";
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:136^9*/

//  ************************************************************
//  #sql [Ctx] { select  upper(nvl(mf.debit_pass_through,'N'))
//            
//            from    mif   mf
//            where   mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(nvl(mf.debit_pass_through,'N'))\n           \n          from    mif   mf\n          where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   icPassthrough = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^9*/
        
        try {
          /*@lineinfo:generated-code*//*@lineinfo:145^11*/

//  ************************************************************
//  #sql [Ctx] { select distinct decode(nvl(tr.amex_optblue_pricing_type,-1),1,'Y','N') 
//              
//              from   tranchrg tr, merchant me, mif m
//              where  tr.app_seq_num = me.app_seq_num
//                     and me.merch_number = m.merchant_number
//                     and me.merch_number = :merchantId
//                     and tr.cardtype_code = :mesConstants.APP_CT_AMEX --Amex
//                     and m.damexse in (select amex_optblue_se_number from amex_se_mapping)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select distinct decode(nvl(tr.amex_optblue_pricing_type,-1),1,'Y','N') \n             \n            from   tranchrg tr, merchant me, mif m\n            where  tr.app_seq_num = me.app_seq_num\n                   and me.merch_number = m.merchant_number\n                   and me.merch_number =  :1  \n                   and tr.cardtype_code =  :2   --Amex\n                   and m.damexse in (select amex_optblue_se_number from amex_se_mapping)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   trIcPassthrough = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^11*/
        }
        catch( Exception ee )
        {
          //ignore
        }

        if( "Y".equals(icPassthrough) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:164^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.item_subclass || ' - ' ||
//                      upper(icd.ic_desc)                as smsg,
//                      icd.ic_rate                       as rate,
//                      icd.ic_per_item                   as per_item,
//                      sum(sm.sales_count)               as sales_count,
//                      sum(sm.sales_amount)              as sales_amount,
//                      sum(sm.expense_actual)            as ic_exp,
//                      round( sum((sm.sales_amount*icd.ic_rate*0.01)+(sm.sales_count*icd.ic_per_item)), 2)
//                                                        as ic_due
//              from    mbs_daily_summary           sm,
//                      daily_detail_file_ic_desc   icd
//              where   sm.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                      and sm.merchant_number = :merchantId
//                      and sm.item_type = 111  
//                      and sm.item_subclass in ('DB','EB')
//                      and icd.card_type(+) = sm.item_subclass
//                      and icd.ic_code(+) = sm.ic_cat
//                      and icd.valid_date_begin(+) <= sm.activity_date
//                      and icd.valid_date_end(+)   >= sm.activity_date
//              group by sm.item_subclass,icd.ic_desc,icd.ic_rate,icd.ic_per_item
//              order by upper(icd.ic_desc)          
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.item_subclass || ' - ' ||\n                    upper(icd.ic_desc)                as smsg,\n                    icd.ic_rate                       as rate,\n                    icd.ic_per_item                   as per_item,\n                    sum(sm.sales_count)               as sales_count,\n                    sum(sm.sales_amount)              as sales_amount,\n                    sum(sm.expense_actual)            as ic_exp,\n                    round( sum((sm.sales_amount*icd.ic_rate*0.01)+(sm.sales_count*icd.ic_per_item)), 2)\n                                                      as ic_due\n            from    mbs_daily_summary           sm,\n                    daily_detail_file_ic_desc   icd\n            where   sm.me_load_file_id = load_filename_to_load_file_id( :1  )\n                    and sm.merchant_number =  :2  \n                    and sm.item_type = 111  \n                    and sm.item_subclass in ('DB','EB')\n                    and icd.card_type(+) = sm.item_subclass\n                    and icd.ic_code(+) = sm.ic_cat\n                    and icd.valid_date_begin(+) <= sm.activity_date\n                    and icd.valid_date_end(+)   >= sm.activity_date\n            group by sm.item_subclass,icd.ic_desc,icd.ic_rate,icd.ic_per_item\n            order by upper(icd.ic_desc)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^11*/
          resultSet = it.getResultSet();

          StatementRecord srec = new StatementRecord();
          srec.setLoadSec(loadSec);
          srec.setItemCat("IC");
          srec.setLoadFilename(loadFilename);
          while( resultSet.next() )
          {
            String smsg = resultSet.getString("smsg");

            smsg += "  (" 
                    + NumberFormatter.getPercentString(resultSet.getDouble("rate")/100.0,3) 
                    + " + " 
                    + MesMath.toFractionalCurrency(resultSet.getDouble("per_item")) 
                    + ")";
            srec.setStatementMessage(smsg);
            srec.setItemCount(resultSet.getInt("sales_count"));
            srec.setItemAmount(resultSet.getDouble("sales_amount"));
            srec.setFeeAmount(resultSet.getDouble("ic_due"));
            storeStatementRecord(srec);
          }

          resultSet.close();
          it.close();
        }

        if( "Y".equals(trIcPassthrough) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:216^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.item_subclass || ' - ' ||
//                      upper(icd.ic_desc)                as smsg,
//                      icd.ic_rate                       as rate,
//                      icd.ic_per_item                   as per_item,
//                      sum(sm.sales_count)               as sales_count,
//                      sum(sm.sales_amount)              as sales_amount,
//                      sum(sm.expense_actual)            as ic_exp,
//                      round( sum((sm.sales_amount*icd.ic_rate*0.01)+(sm.sales_count*icd.ic_per_item)), 2)
//                                                        as ic_due
//              from    mbs_daily_summary           sm,
//                      daily_detail_file_ic_desc   icd
//              where   sm.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                      and sm.merchant_number = :merchantId
//                      and sm.item_type = 111  
//                      and sm.item_subclass in ('AM')
//                      and icd.card_type(+) = sm.item_subclass
//                      and icd.ic_code(+) = sm.ic_cat
//                      and icd.valid_date_begin(+) <= sm.activity_date
//                      and icd.valid_date_end(+)   >= sm.activity_date
//              group by sm.item_subclass,icd.ic_desc,icd.ic_rate,icd.ic_per_item
//              order by upper(icd.ic_desc)          
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.item_subclass || ' - ' ||\n                    upper(icd.ic_desc)                as smsg,\n                    icd.ic_rate                       as rate,\n                    icd.ic_per_item                   as per_item,\n                    sum(sm.sales_count)               as sales_count,\n                    sum(sm.sales_amount)              as sales_amount,\n                    sum(sm.expense_actual)            as ic_exp,\n                    round( sum((sm.sales_amount*icd.ic_rate*0.01)+(sm.sales_count*icd.ic_per_item)), 2)\n                                                      as ic_due\n            from    mbs_daily_summary           sm,\n                    daily_detail_file_ic_desc   icd\n            where   sm.me_load_file_id = load_filename_to_load_file_id( :1  )\n                    and sm.merchant_number =  :2  \n                    and sm.item_type = 111  \n                    and sm.item_subclass in ('AM')\n                    and icd.card_type(+) = sm.item_subclass\n                    and icd.ic_code(+) = sm.ic_cat\n                    and icd.valid_date_begin(+) <= sm.activity_date\n                    and icd.valid_date_end(+)   >= sm.activity_date\n            group by sm.item_subclass,icd.ic_desc,icd.ic_rate,icd.ic_per_item\n            order by upper(icd.ic_desc)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^11*/
          resultSet = it.getResultSet();

          StatementRecord srec = new StatementRecord();
          srec.setLoadSec(loadSec);
          srec.setItemCat("IC");
          srec.setLoadFilename(loadFilename);
          while( resultSet.next() )
          {
            String smsg = resultSet.getString("smsg");

            //Added to modify statement message description for Amex Optblue transactions.
            if("Y".equals(trIcPassthrough) && smsg.contains("OPTBLUE")){
              StringBuffer uMsg = new StringBuffer();

              uMsg.append("AM ");

              if(smsg.contains("PREPAID TIER")) // prepaid transactions
                uMsg.append("- PREPAID ");

              if(smsg.contains("TIER1"))
                uMsg.append("- TIER1");
              else if(smsg.contains("TIER2"))
                uMsg.append("- TIER2");
              else if(smsg.contains("TIER3"))
                uMsg.append("- TIER3");
              else if(smsg.contains("MICRO"))
                uMsg.append("- SML TKT");

              smsg = uMsg.toString();
            }

            smsg +=   "  (" 
                      + NumberFormatter.getPercentString(resultSet.getDouble("rate")/100.0,3) 
                      + " + " 
                      + MesMath.toFractionalCurrency(resultSet.getDouble("per_item")) 
                      + ")";
            srec.setStatementMessage(smsg);
            srec.setItemCount(resultSet.getInt("sales_count"));
            srec.setItemAmount(resultSet.getDouble("sales_amount"));
            srec.setFeeAmount(resultSet.getDouble("ic_due"));
            storeStatementRecord(srec);
          }
          resultSet.close();
          it.close();
        }
      }
      catch( Exception e )
      {
        logEntry( "addInterchangeDN( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ){}
      }
    }    
    
    protected void addMonthlyFeesDN( String loadFilename, long loadSec, long merchantId )
    {
      String  acceptsDebit      = "N";
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:302^9*/

//  ************************************************************
//  #sql [Ctx] { select  case when mf.debit_plan like 'D%' then 'Y' else 'N' end
//            
//            from    mif   mf
//            where   mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  case when mf.debit_plan like 'D%' then 'Y' else 'N' end\n           \n          from    mif   mf\n          where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   acceptsDebit = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^9*/
        
        // 17 - Pin Debit Network Participation Fee
        MerchantBillingElement el = BillingData.findItemByType(17,"DB",true);
        if ( "Y".equals(acceptsDebit) && el != null && el.getPerItem() != 0.0 )
        {
          StatementRecord srec = new StatementRecord();
          srec.setLoadSec(loadSec);
          srec.setItemCat("DEBIT");
          srec.setStatementMessage(el.getItemDescription().toUpperCase());
          srec.setFeeAmount(el.getPerItem());
          srec.setLoadFilename(loadFilename);
        
          storeStatementRecord(srec);
        }
      }
      catch( Exception e )
      {
        logEntry( "addMonthlyFeesDN( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
      }
    }    
    
    protected void loadAP( String loadFilename, long loadSec, long merchantId )
    {
      String                  cardType    = null;
      MerchantBillingElement  el          = null;
      double                  feesDue     = 0.0;
      int                     itemCount   = 0;
      ResultSetIterator       it          = null;
      ResultSet               resultSet   = null;
      String                  smsg        = null;
      StatementRecord         srec        = null;
      HashMap                 srecs       = new HashMap();
      String                  srecKey     = null;
      AuthTotals              totals      = null;
      HashMap                 totalsMap   = new HashMap();

      try
      {
        // load the montly_extract_ap table
        /*@lineinfo:generated-code*//*@lineinfo:351^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.item_type                                as item_type,
//                    sm.item_subclass                            as item_subclass,
//                    decode(sm.item_subclass,
//                           'AM','AMERICAN EXPRESS',
//                           'DS','DISCOVER',
//                           'VS','VISA',
//                           'MC','MASTERCARD',
//                           'JC','JCB',
//                           'UK','OTHER',
//                           'ER','OTHER',
//                           sm.item_subclass)                    as card_type,
//                    sum(sm.item_count)                          as item_count
//            from    mbs_daily_summary   sm,
//                    mbs_elements        mbe
//            where   sm.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and mbe.item_category = 'AUTH'
//            group by  sm.item_type, sm.item_subclass
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.item_type                                as item_type,\n                  sm.item_subclass                            as item_subclass,\n                  decode(sm.item_subclass,\n                         'AM','AMERICAN EXPRESS',\n                         'DS','DISCOVER',\n                         'VS','VISA',\n                         'MC','MASTERCARD',\n                         'JC','JCB',\n                         'UK','OTHER',\n                         'ER','OTHER',\n                         sm.item_subclass)                    as card_type,\n                  sum(sm.item_count)                          as item_count\n          from    mbs_daily_summary   sm,\n                  mbs_elements        mbe\n          where   sm.me_load_file_id = load_filename_to_load_file_id( :1  )\n                  and sm.merchant_number =  :2  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and mbe.item_category = 'AUTH'\n          group by  sm.item_type, sm.item_subclass";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          int     itemType    = resultSet.getInt("item_type");
          String  planType    = resultSet.getString("item_subclass");
          boolean useDefault  = !("DB".equals(planType) || "EB".equals(planType));
        
          itemCount   = resultSet.getInt("item_count");
          feesDue     = 0.0;
          
          // set the auth pricing
          el = BillingData.findItemByType(itemType,planType,useDefault);
          if ( el != null )
          {
            feesDue = (itemCount * el.getPerItem());
          }
          
          // store the statement entry
          if ( feesDue != 0.0 )
          {
            cardType  = resultSet.getString("card_type");
            smsg      = ((cardType == null) ? "" : (cardType + " ")) +
                        el.getItemDescription() + " (" + MesMath.toFractionalCurrency(el.getPerItem(),4) + ")";
            srecKey   = (String.valueOf(itemType) + "-" + cardType);
            if(planType != null && ((planType.equalsIgnoreCase("VP")) || (planType.equalsIgnoreCase("MP")))){
               feesDue = 0.0;
            }       
            srec = (StatementRecord)srecs.get(srecKey);
            if ( srec == null )
            {
              srec = new StatementRecord();
              srec.setLoadSec(loadSec);
              srec.setItemCat("AUTH");
              srec.setItemCount(itemCount);
              srec.setItemAmount(0.0);
              srec.setStatementMessage(smsg.toUpperCase());
              srec.setFeeAmount(feesDue);
              srec.setLoadFilename(loadFilename);
              srecs.put(srecKey,srec);
            }                        
            else
            {
              srec.addItemCount(itemCount);
              srec.addFeeAmount(feesDue);
            }
          }
          
          // update the totals
          totals = (AuthTotals)totalsMap.get(planType);
          if ( totals == null ) 
          {
            totals = new AuthTotals(planType);
            totalsMap.put(planType,totals);
          }
          
          switch( itemType )
          {
            case  1:  totals.addAuths   (itemCount,feesDue);  break;  // auth fee
            case  2:  totals.addAvs     (itemCount,feesDue);  break;  // avs fee
            case  3:  totals.addIntlPG  (itemCount,feesDue);  break;  // intl pg fee
            case  4:  totals.add3dSecure(itemCount,feesDue);  break;  // 3D secure
            case  9:  totals.addAru     (itemCount,feesDue);  break;  // aru
            case 12:  totals.addPG      (itemCount,feesDue);  break;  // pg fee
            default:  break;
          }
          totals.addTotals(itemCount,feesDue);    // overall totals
        }
        resultSet.close();
        it.close();
        
        // store statement messages
        for( Iterator i = srecs.keySet().iterator(); i.hasNext(); )
        {
          srec = (StatementRecord)srecs.get( (String)i.next() );
          storeStatementRecord(srec);
        }
        
        // store monthly extract entries
        for( Iterator i = totalsMap.keySet().iterator(); i.hasNext(); )
        {
          totals = (AuthTotals)totalsMap.get((String)i.next());
          if(totals.getPlanType() != null && ((totals.getPlanType().equalsIgnoreCase("VP")) || (totals.getPlanType().equalsIgnoreCase("MP")))){
        	  storeAP("AL",totals);  // load acculynk data into 
          }else{
        	  storeAP("FX",totals);  // load base data into fixed media type
          }
          
          if ( totals.getAruCount() != 0 )
          {
            storeAP("AR",totals); // load telpay auths into ARU media type
          }
        }
      }
      catch( Exception e )
      {
        logEntry( "loadAP( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try { it.close(); } catch( Exception ee ) {}
      }
    }
    
    protected void loadCG( String loadFilename, long loadSec, long merchantId, Date activeDate )
    {
      ResultSetIterator       it          = null;
      ResultSet               resultSet   = null;
      StatementRecord         srec        = new StatementRecord();
  
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:485^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  :loadSec                    as load_sec,
//                    nvl(mcr.charge_type,'MIS')  as ctype,
//                    mcr.dda                     as dda,
//                    mcr.transit_routing         as tr,
//                    9999                        as bank_officer,
//                    mcr.charge_amount           as charge_amount,
//                    'D'                         as debit_remit,
//                    mcr.billing_frequency       as billing_frequency,
//                    mcr.start_date              as start_date,
//                    mcr.end_date                as expiration_date,
//                    mcr.item_count              as num_pos_imprinters,
//                    mcr.statement_message       as statement_msg, 
//                    case
//                      when last_day(:activeDate) between mcr.start_date and mcr.end_date then 1
//                      else 0
//                    end                         as armed,
//                    case
//                      when substr(mcr.billing_frequency,to_char(:activeDate,'mm'),1) = 'Y' then 1
//                      else 0
//                    end                         as active,
//                    (
//                      decode( nvl(mcr.item_count,0),
//                              0, 1, -- unused, default to 1 item
//                              mcr.item_count) *
//                      mcr.charge_amount
//                    )                           as fees_due,
//                    decode( nvl(mcr.item_count,0),
//                            0, 0,
//                            mcr.charge_amount)  as amount,
//                    :loadFilename               as load_filename
//            from    mbs_charge_records    mcr
//            where   mcr.merchant_number = :merchantId
//                    and mcr.end_date >= :activeDate
//                    and upper(nvl(mcr.enabled,'Y')) = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                      as load_sec,\n                  nvl(mcr.charge_type,'MIS')  as ctype,\n                  mcr.dda                     as dda,\n                  mcr.transit_routing         as tr,\n                  9999                        as bank_officer,\n                  mcr.charge_amount           as charge_amount,\n                  'D'                         as debit_remit,\n                  mcr.billing_frequency       as billing_frequency,\n                  mcr.start_date              as start_date,\n                  mcr.end_date                as expiration_date,\n                  mcr.item_count              as num_pos_imprinters,\n                  mcr.statement_message       as statement_msg, \n                  case\n                    when last_day( :2  ) between mcr.start_date and mcr.end_date then 1\n                    else 0\n                  end                         as armed,\n                  case\n                    when substr(mcr.billing_frequency,to_char( :3  ,'mm'),1) = 'Y' then 1\n                    else 0\n                  end                         as active,\n                  (\n                    decode( nvl(mcr.item_count,0),\n                            0, 1, -- unused, default to 1 item\n                            mcr.item_count) *\n                    mcr.charge_amount\n                  )                           as fees_due,\n                  decode( nvl(mcr.item_count,0),\n                          0, 0,\n                          mcr.charge_amount)  as amount,\n                   :4                 as load_filename\n          from    mbs_charge_records    mcr\n          where   mcr.merchant_number =  :5  \n                  and mcr.end_date >=  :6  \n                  and upper(nvl(mcr.enabled,'Y')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setLong(5,merchantId);
   __sJT_st.setDate(6,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:526^11*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_cg  
//              (
//                hh_load_sec               , load_filename       ,
//                cg_charge_record_type     , cg_debit_remit      ,
//                cg_dda_number             , cg_transit_routing  ,
//                cg_billing_freq_mask      , cg_bank_officer     ,
//                cg_number_pos_imprinters  , cg_charge_amount    ,
//                cg_start_date             , cg_expiration_date  ,
//                cg_message_for_stmt
//              )
//              values
//              (
//                :loadSec                                    , :loadFilename                           ,
//                :resultSet.getString("ctype")             , :resultSet.getString("debit_remit")   , 
//                :resultSet.getString("dda")               , :resultSet.getString("tr")            ,
//                :resultSet.getString("billing_frequency") , :resultSet.getString("bank_officer")  ,
//                :resultSet.getInt("num_pos_imprinters")   , :resultSet.getDouble("charge_amount") ,
//                :resultSet.getDate("start_date")          , :resultSet.getDate("expiration_date") ,
//                :resultSet.getString("statement_msg")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_183 = resultSet.getString("ctype");
 String __sJT_184 = resultSet.getString("debit_remit");
 String __sJT_185 = resultSet.getString("dda");
 String __sJT_186 = resultSet.getString("tr");
 String __sJT_187 = resultSet.getString("billing_frequency");
 String __sJT_188 = resultSet.getString("bank_officer");
 int __sJT_189 = resultSet.getInt("num_pos_imprinters");
 double __sJT_190 = resultSet.getDouble("charge_amount");
 java.sql.Date __sJT_191 = resultSet.getDate("start_date");
 java.sql.Date __sJT_192 = resultSet.getDate("expiration_date");
 String __sJT_193 = resultSet.getString("statement_msg");
   String theSqlTS = "insert into monthly_extract_cg  \n            (\n              hh_load_sec               , load_filename       ,\n              cg_charge_record_type     , cg_debit_remit      ,\n              cg_dda_number             , cg_transit_routing  ,\n              cg_billing_freq_mask      , cg_bank_officer     ,\n              cg_number_pos_imprinters  , cg_charge_amount    ,\n              cg_start_date             , cg_expiration_date  ,\n              cg_message_for_stmt\n            )\n            values\n            (\n               :1                                      ,  :2                             ,\n               :3               ,  :4     , \n               :5                 ,  :6              ,\n               :7   ,  :8    ,\n               :9     ,  :10   ,\n               :11            ,  :12   ,\n               :13  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,__sJT_183);
   __sJT_st.setString(4,__sJT_184);
   __sJT_st.setString(5,__sJT_185);
   __sJT_st.setString(6,__sJT_186);
   __sJT_st.setString(7,__sJT_187);
   __sJT_st.setString(8,__sJT_188);
   __sJT_st.setInt(9,__sJT_189);
   __sJT_st.setDouble(10,__sJT_190);
   __sJT_st.setDate(11,__sJT_191);
   __sJT_st.setDate(12,__sJT_192);
   __sJT_st.setString(13,__sJT_193);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^11*/
        
          // armed  == active date between start and end date
          // active == the billing month has the active flag set to Y 
          if ( resultSet.getInt("armed") == 1 && resultSet.getInt("active") == 1 )
          {
            srec.clear(); 
            srec.setLoadSec(loadSec);
            srec.setItemCat("CG-" + resultSet.getString("ctype"));
            srec.setItemCount(resultSet.getInt("num_pos_imprinters"));
            srec.setItemAmount(resultSet.getDouble("amount"));
            srec.setStatementMessage(resultSet.getString("statement_msg"));
            srec.setFeeAmount(resultSet.getDouble("fees_due"));
            srec.setLoadFilename(loadFilename);
          
            storeStatementRecord(srec);
          }
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry( "loadCG( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try { it.close(); } catch( Exception ee ) {}
      }
    }
  
    protected void loadDN( String loadFilename, long loadSec, long merchantId )
    {
      MerchantBillingElement  el          = null;
      ResultSetIterator       it          = null;
      long                    loadFileId  = 0L;
      double                  perItem     = 0.0;
      double                  rate        = 0.0;
      ResultSet               resultSet   = null;
      StatementRecord         srec        = new StatementRecord();

      try
      {
        double  amount                = 0.0;
        int     salesCount            = 0;
        double  salesAmount           = 0.0;
        int     creditsCount          = 0;
        double  creditsAmount         = 0.0;
        double  salesAmountTotal      = 0.0;
        double  creditsAmountTotal    = 0.0;
        double  salesFeesTotal        = 0.0;
        double  creditsFeesTotal      = 0.0;
        int     salesCountTotal       = 0;
        int     creditsCountTotal     = 0;
        int     totalCount            = 0;
        int     volType               = -1;
        boolean isEBT                 = false;

        loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
      
        /*@lineinfo:generated-code*//*@lineinfo:608^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  :loadSec                          as load_sec,
//                    :loadFilename                     as load_filename,
//                    9999                              as bet_number,
//                    sm.item_type                      as item_type,
//                    sm.item_subclass                  as plan_type,
//                    decode( sm.item_subclass,
//                            'DB','DEBIT',
//                            'EB','EBT',
//                            sm.item_subclass )        as card_type,
//                    sum( sm.sales_count )             as sales_count,
//                    sum( sm.sales_amount )            as sales_amount,
//                    sum( sm.credits_count )           as credits_count,
//                    sum( sm.credits_amount )          as credits_amount,
//                    sum( sm.item_count )              as total_count,
//                    sum( sm.sales_amount + sm.credits_amount )  
//                                                      as hash_amount
//            from    mbs_daily_summary       sm,
//                    mbs_elements            mbe
//            where   sm.me_load_file_id = :loadFileId
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and mbe.item_category = 'DEBIT'
//            group by sm.item_type, sm.item_subclass
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                            as load_sec,\n                   :2                       as load_filename,\n                  9999                              as bet_number,\n                  sm.item_type                      as item_type,\n                  sm.item_subclass                  as plan_type,\n                  decode( sm.item_subclass,\n                          'DB','DEBIT',\n                          'EB','EBT',\n                          sm.item_subclass )        as card_type,\n                  sum( sm.sales_count )             as sales_count,\n                  sum( sm.sales_amount )            as sales_amount,\n                  sum( sm.credits_count )           as credits_count,\n                  sum( sm.credits_amount )          as credits_amount,\n                  sum( sm.item_count )              as total_count,\n                  sum( sm.sales_amount + sm.credits_amount )  \n                                                    as hash_amount\n          from    mbs_daily_summary       sm,\n                  mbs_elements            mbe\n          where   sm.me_load_file_id =  :3  \n                  and sm.merchant_number =  :4  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and mbe.item_category = 'DEBIT'\n          group by sm.item_type, sm.item_subclass";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setLong(3,loadFileId);
   __sJT_st.setLong(4,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:633^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          if ( resultSet.getInt("total_count") != 0 )
          {
            el = BillingData.findItemByType(resultSet.getInt("item_type"),resultSet.getString("plan_type"));
          
            perItem = ((el == null) ? 0.0 : el.getPerItem());
            rate    = ((el == null) ? 0.0 : el.getRate());
            volType = ((el == null) ? -1  : el.getVolumeType());
          
            switch( volType )
            {
              case MerchantBillingElement.VT_CREDITS_ONLY:
                amount        = resultSet.getDouble("credits_amount");
                salesAmount   = 0.0;
                creditsAmount = resultSet.getDouble("credits_amount");
                break;
                
              case MerchantBillingElement.VT_SALES_PLUS_CREDITS:
                amount        = resultSet.getDouble("sales_amount") + resultSet.getDouble("credits_amount");
                salesAmount   = resultSet.getDouble("sales_amount");
                creditsAmount = resultSet.getDouble("credits_amount");
                break;
                
              case MerchantBillingElement.VT_SALES_MINUS_CREDITS:
                amount        = resultSet.getDouble("sales_amount") - resultSet.getDouble("credits_amount");
                salesAmount   = resultSet.getDouble("sales_amount");
                creditsAmount = -resultSet.getDouble("credits_amount");
                break;
                
           // case MerchantBillingElement.VT_SALES_ONLY:
              default:    
                amount        = resultSet.getDouble("sales_amount");
                salesAmount   = resultSet.getDouble("sales_amount");
                creditsAmount = 0.0;
                break;
            }
        
            if ( perItem != 0.0 || rate != 0.0 )
            {
              srec.clear(); 
              srec.setLoadSec(loadSec);
              srec.setItemCat("DEBIT");
              srec.setItemCount(resultSet.getInt("total_count"));
              srec.setItemAmount(resultSet.getDouble("hash_amount"));
              srec.setStatementMessage( resultSet.getString("card_type") + " FEES (" 
                                        + ((rate == 0.0)    ? "" : NumberFormatter.getPercentString(rate/100.0,3) + " + ") 
                                        + ((perItem == 0.0) ? "" : MesMath.toFractionalCurrency(perItem,4)) 
                                        + ")" );
              srec.setFeeAmount( (resultSet.getInt("total_count") * perItem) + (amount * rate * 0.01) );
              srec.setLoadFilename(loadFilename);
        
              storeStatementRecord(srec);
            }
            //@ else issue warning about missing pricing?            
            
            // store only the debit totals for the monthly_extract_dn table
            if ( !"EB".equals(resultSet.getString("plan_type")) )
            {
              salesCountTotal     += resultSet.getInt("sales_count");
              salesAmountTotal    += resultSet.getDouble("sales_amount");
              creditsCountTotal   += resultSet.getInt("credits_count");
              creditsAmountTotal  += resultSet.getDouble("credits_amount");
              totalCount          += resultSet.getInt("total_count");
              
              salesFeesTotal      += (salesAmount * rate * 0.01);
              salesFeesTotal      += (resultSet.getInt("sales_count") * perItem);
              creditsFeesTotal    += (creditsAmount * rate * 0.01);
              creditsFeesTotal    += (resultSet.getInt("credits_count") * perItem);
            }
          }
        }
        resultSet.close();
        it.close();        
      
        if ( totalCount != 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:713^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_dn    dn
//              where   dn.hh_load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_dn    dn\n            where   dn.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:718^11*/
        
          // create a single row with the totals
          /*@lineinfo:generated-code*//*@lineinfo:721^11*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_dn
//              ( 
//                hh_load_sec,
//                load_filename,
//                n1_bet_table_number,
//                n1_total_network_sales, n1_total_network_credits,
//                cat_01_sales_count    , cat_01_credits_count    ,  cat_01_total_count,
//                cat_02_sales_count    , cat_02_credits_count    ,  cat_02_total_count,
//                cat_03_sales_count    , cat_03_credits_count    ,  cat_03_total_count,
//                cat_04_sales_count    , cat_04_credits_count    ,  cat_04_total_count,
//                cat_05_sales_count    , cat_05_credits_count    ,  cat_05_total_count,
//                cat_06_sales_count    , cat_06_credits_count    ,  cat_06_total_count,
//                cat_07_sales_count    , cat_07_credits_count    ,  cat_07_total_count,
//                cat_08_sales_count    , cat_08_credits_count    ,  cat_08_total_count,
//                cat_09_sales_count    , cat_09_credits_count    ,  cat_09_total_count,
//                cat_10_sales_count    , cat_10_credits_count    ,  cat_10_total_count,
//                n1_inc_cat_01_sales   , n1_inc_cat_01_credit    ,
//                n1_inc_cat_02_sales   , n1_inc_cat_02_credit    ,
//                n1_inc_cat_03_sales   , n1_inc_cat_03_credit    ,
//                n1_inc_cat_04_sales   , n1_inc_cat_04_credit    ,
//                n1_inc_cat_05_sales   , n1_inc_cat_05_credit    ,
//                n1_inc_cat_06_sales   , n1_inc_cat_06_credit    ,
//                n1_inc_cat_07_sales   , n1_inc_cat_07_credit    ,
//                n1_inc_cat_08_sales   , n1_inc_cat_08_credit    ,
//                n1_inc_cat_09_sales   , n1_inc_cat_09_credit    ,
//                n1_inc_cat_10_sales   , n1_inc_cat_10_credit
//              )
//              values
//              (
//                :loadSec,
//                :loadFilename,
//                9999,
//                :salesAmountTotal ,   :creditsAmountTotal ,                 -- total amounts  
//                :salesCountTotal  ,   :creditsCountTotal  ,   :totalCount , -- counts cat 01
//                0                 ,   0                   ,   0           , -- counts cat 02
//                0                 ,   0                   ,   0           , -- counts cat 03
//                0                 ,   0                   ,   0           , -- counts cat 04
//                0                 ,   0                   ,   0           , -- counts cat 05
//                0                 ,   0                   ,   0           , -- counts cat 06
//                0                 ,   0                   ,   0           , -- counts cat 07
//                0                 ,   0                   ,   0           , -- counts cat 08
//                0                 ,   0                   ,   0           , -- counts cat 09
//                0                 ,   0                   ,   0           , -- counts cat 10
//                :salesFeesTotal   ,   :creditsFeesTotal   ,                 -- inc cat 01
//                0                 ,   0                   ,                 -- inc cat 02 
//                0                 ,   0                   ,                 -- inc cat 03 
//                0                 ,   0                   ,                 -- inc cat 04 
//                0                 ,   0                   ,                 -- inc cat 05 
//                0                 ,   0                   ,                 -- inc cat 06 
//                0                 ,   0                   ,                 -- inc cat 07 
//                0                 ,   0                   ,                 -- inc cat 08 
//                0                 ,   0                   ,                 -- inc cat 09 
//                0                 ,   0                                     -- inc cat 10 
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_dn\n            ( \n              hh_load_sec,\n              load_filename,\n              n1_bet_table_number,\n              n1_total_network_sales, n1_total_network_credits,\n              cat_01_sales_count    , cat_01_credits_count    ,  cat_01_total_count,\n              cat_02_sales_count    , cat_02_credits_count    ,  cat_02_total_count,\n              cat_03_sales_count    , cat_03_credits_count    ,  cat_03_total_count,\n              cat_04_sales_count    , cat_04_credits_count    ,  cat_04_total_count,\n              cat_05_sales_count    , cat_05_credits_count    ,  cat_05_total_count,\n              cat_06_sales_count    , cat_06_credits_count    ,  cat_06_total_count,\n              cat_07_sales_count    , cat_07_credits_count    ,  cat_07_total_count,\n              cat_08_sales_count    , cat_08_credits_count    ,  cat_08_total_count,\n              cat_09_sales_count    , cat_09_credits_count    ,  cat_09_total_count,\n              cat_10_sales_count    , cat_10_credits_count    ,  cat_10_total_count,\n              n1_inc_cat_01_sales   , n1_inc_cat_01_credit    ,\n              n1_inc_cat_02_sales   , n1_inc_cat_02_credit    ,\n              n1_inc_cat_03_sales   , n1_inc_cat_03_credit    ,\n              n1_inc_cat_04_sales   , n1_inc_cat_04_credit    ,\n              n1_inc_cat_05_sales   , n1_inc_cat_05_credit    ,\n              n1_inc_cat_06_sales   , n1_inc_cat_06_credit    ,\n              n1_inc_cat_07_sales   , n1_inc_cat_07_credit    ,\n              n1_inc_cat_08_sales   , n1_inc_cat_08_credit    ,\n              n1_inc_cat_09_sales   , n1_inc_cat_09_credit    ,\n              n1_inc_cat_10_sales   , n1_inc_cat_10_credit\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n              9999,\n               :3   ,    :4   ,                 -- total amounts  \n               :5    ,    :6    ,    :7   , -- counts cat 01\n              0                 ,   0                   ,   0           , -- counts cat 02\n              0                 ,   0                   ,   0           , -- counts cat 03\n              0                 ,   0                   ,   0           , -- counts cat 04\n              0                 ,   0                   ,   0           , -- counts cat 05\n              0                 ,   0                   ,   0           , -- counts cat 06\n              0                 ,   0                   ,   0           , -- counts cat 07\n              0                 ,   0                   ,   0           , -- counts cat 08\n              0                 ,   0                   ,   0           , -- counts cat 09\n              0                 ,   0                   ,   0           , -- counts cat 10\n               :8     ,    :9     ,                 -- inc cat 01\n              0                 ,   0                   ,                 -- inc cat 02 \n              0                 ,   0                   ,                 -- inc cat 03 \n              0                 ,   0                   ,                 -- inc cat 04 \n              0                 ,   0                   ,                 -- inc cat 05 \n              0                 ,   0                   ,                 -- inc cat 06 \n              0                 ,   0                   ,                 -- inc cat 07 \n              0                 ,   0                   ,                 -- inc cat 08 \n              0                 ,   0                   ,                 -- inc cat 09 \n              0                 ,   0                                     -- inc cat 10 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setDouble(3,salesAmountTotal);
   __sJT_st.setDouble(4,creditsAmountTotal);
   __sJT_st.setInt(5,salesCountTotal);
   __sJT_st.setInt(6,creditsCountTotal);
   __sJT_st.setInt(7,totalCount);
   __sJT_st.setDouble(8,salesFeesTotal);
   __sJT_st.setDouble(9,creditsFeesTotal);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:777^11*/
        }
        
        addMonthlyFeesDN(loadFilename, loadSec, merchantId);
        addInterchangeDN(loadFilename, loadSec, merchantId);
      }
      catch( Exception e )
      {
        logEntry( "loadDN( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try { it.close(); } catch( Exception ee ) {}
      }
    }
  
    protected void loadGN( String loadFilename, long loadSec, long merchantId, Date activeDate )
    {
      MerchantBillingElement  el          = null;
      ResultSetIterator       it          = null;
      long                    loadFileId  = 0L;
      ResultSet               resultSet   = null;
  
      try
      {
        loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
    
        // insert the base line merchant data
        /*@lineinfo:generated-code*//*@lineinfo:805^9*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_gn 
//            (
//              hh_load_sec,
//              hh_bank_number,
//              hh_merchant_number,
//              hh_dba_name,
//              hh_curr_date,
//              hh_active_date,
//              load_filename,
//              load_file_id,
//              g1_association_number,
//              g1_sic_code,
//              g1_class_code,
//              g1_dba_name,
//              g1_dba_city,
//              g1_dba_state,
//              g1_dba_zip,
//              g1_phone1,
//              g1_phone2,
//              g1_business_lic_number,
//              g1_bank_officer1,
//              g1_bank_officer2,
//              g1_user_bank_number,
//              g1_user_branch_number,
//              g1_federal_tax_id,
//              g1_state_tax_id,
//              g1_edc_flag,
//              g1_rep_code,
//              g1_merchant_status,
//              g1_user_flag1,
//              g1_user_flag2,
//              g1_user_data1,
//              g1_user_data2,
//              g1_user_data3,
//              g1_user_data4,
//              g1_user_data5,
//              g1_user_account1,
//              g1_user_account2,
//              g1_ach_flag,
//              g1_exception_table,
//              g1_investigator_code,
//              g1_visa_cps2,
//              g1_visa_cps1,
//              g1_visa_super,
//              g1_visa_epds,
//              g1_visa_psrf,
//              g1_visa_eirf,
//              g1_visa_expansion,
//              g1_mc_merit3,
//              g1_mc_merit1,
//              g1_mc_super,
//              g1_mc_petro_cat,
//              g1_mc_wharehouse_club,
//              g1_mc_premier,
//              g1_mc_expansion,
//              g1_club,
//              g1_merchant_type,
//              g1_incorporation_status,
//              g1_member_id,
//              g1_vrs_flag,
//              g2_owner_name,
//              g2_manager_name,
//              g2_asst_manager,
//              g2_other_name,
//              g2_owner_ssn_enc,
//              g2_owner_lic_number,
//              g2_last_stmt_date,
//              g2_date_opened,
//              g2_date_last_credit_ck,
//              g2_date_last_card_req,
//              g2_last_call_date,
//              g2_next_call_date,
//              g2_fin_stmt_due_date,
//              g2_fin_stmt_rec_date,
//              g2_store_ind_flag,
//              g2_number_of_stmts,
//              g2_discount_ind,
//              g2_rcl_list,
//              g2_crb_list,
//              g2_card_mailers,
//              g2_calling_cards,
//              g2_imprinter_rentals,
//              g2_member_fees,
//              g2_pos_terminals,
//              g2_mcs,
//              g2_unique_message,
//              g2_bet1,
//              g2_bet2,
//              g2_bets,
//              g2_intch_net_gross_f$,
//              g2_intch_net_gross_f#,
//              g3_visa_ach_opt,
//              g3_mc_ach_opt,
//              g3_visa_cash_ach_opt,
//              g3_mc_cash_ach_opt,
//              g3_discover_ach_opt,
//              g3_jcb_ach_opt,
//              g3_debit_card_ach_opt,
//              g3_diners_club_ach_opt,
//              g3_amex_ach_opt,
//              g3_plan1_ach_opt,
//              g3_plan2_ach_opt,
//              g3_plan3_ach_opt,
//              g3_visa_lg_ticket_ach_opt,
//              g3_mc_lg_ticket_ach_opt,
//              g3_overall_dest_opt,
//              g3_deposit_opt,
//              g3_adjustment_opt,
//              g3_chargeback_opt,
//              g3_reversal_opt,
//              g3_chargeback_rev_opt,
//              g3_dda_adj_opt,
//              g3_batch_adj_opt,
//              g3_transaction1_opt,
//              g3_transaction2_opt,
//              g3_transaction3_opt,
//              g3_amex_pcid,
//              g3_amex_descriptor_code,
//              g3_amex_settlement_flg,
//              g3_discover_ref_number,
//              g3_amex_account_num,
//              g3_discover_acct_num,
//              g3_jcb_acct_num,
//              g3_diners_club_acct_num,
//              g3_cardholder_detail_flg,
//              g3_confirmation_letter_flg,
//              g3_cfirm_ltr_opt_mbatch,
//              g3_cfirm_ltr_opt_adjust,
//              g3_cfirm_ltr_opt_retrvals,
//              ad_id_code,
//              ad_name_line1,
//              ad_address_line1,
//              ad_address_line2,
//              ad_city,
//              ad_state,
//              ad_zip_code,
//              ad_date_last_maint
//            )
//            select  :loadSec                      as hh_load_sec,
//                    mf.bank_number                as hh_bank_number,
//                    mf.merchant_number            as hh_merchant_number,
//                    substr(mf.dba_name,1,25)      as hh_dba_name,
//                    last_day(:activeDate)         as hh_curr_date,
//                    :activeDate                   as hh_active_date,
//                    :loadFilename                 as load_filename,
//                    :loadFileId                   as load_file_id,
//                    mf.dmagent                    as g1_association_number,
//                    mf.sic_code                   as g1_sic_code,
//                    mf.sic_code                   as g1_class_code,
//                    substr(mf.dba_name,1,25)      as g1_dba_name,
//                    substr(mf.dmcity,1,13)        as g1_dba_city,
//                    mf.dmstate                    as g1_dba_state,
//                    mf.dmzip                      as g1_dba_zip,
//                    mf.phone_1                    as g1_phone1,
//                    mf.phone_2_fax                as g1_phone2,
//                    mf.business_license           as g1_business_lic_number,
//                    mf.officer_1                  as g1_bank_officer1,
//                    mf.officer_2                  as g1_bank_officer2,
//                    mf.bank_number                as g1_user_bank_number,
//                    0                             as g1_user_branch_number,
//                    mf.federal_tax_id             as g1_federal_tax_id,
//                    mf.state_tax_id               as g1_state_tax_id,
//                    mf.edc_flagst_other_3         as g1_edc_flag,
//                    mf.rep_code                   as g1_rep_code,
//                    mf.dmacctst                   as g1_merchant_status,
//                    mf.user_flag_1                as g1_user_flag1,
//                    mf.user_flag_2                as g1_user_flag2,
//                    mf.user_data_1                as g1_user_data1,
//                    mf.user_data_2                as g1_user_data2,
//                    mf.user_data_3                as g1_user_data3,
//                    mf.user_data_4                as g1_user_data4,
//                    mf.user_data_5                as g1_user_data5,
//                    mf.user_acount_1              as g1_user_account1,
//                    mf.user_account_2             as g1_user_account2,
//                    mf.daily_ach                  as g1_ach_flag,
//                    mf.met_table                  as g1_exception_table,
//                    mf.inv_code_investigate       as g1_investigator_code,
//                    null                          as g1_visa_cps2,
//                    mf.dmcpsind                   as g1_visa_cps1,
//                    null                          as g1_visa_super,
//                    null                          as g1_visa_epds,
//                    mf.psrf_eligibility           as g1_visa_psrf,
//                    null                          as g1_visa_eirf,
//                    null                          as g1_visa_expansion,
//                    mf.dmer3ind                   as g1_mc_merit3,
//                    mf.merit_eligibility          as g1_mc_merit1,
//                    null                          as g1_mc_super,
//                    null                          as g1_mc_petro_cat,
//                    null                          as g1_mc_wharehouse_club,
//                    null                          as g1_mc_premier,
//                    null                          as g1_mc_expansion,
//                    mf.club_eligibility           as g1_club,
//                    mf.merchant_type              as g1_merchant_type,
//                    null                          as g1_incorporation_status,
//                    null                          as g1_member_id,
//                    null                          as g1_vrs_flag,
//                    mf.owner_name                 as g2_owner_name,
//                    mf.manager_name               as g2_manager_name,
//                    null                          as g2_asst_manager,
//                    null                          as g2_other_name,
//                    nvl(mf.ssn_enc,
//                    dukpt_encrypt_wrapper(mf.ssn)
//                    )                             as g2_owner_ssn_enc,
//                    mf.license_number             as g2_owner_lic_number,
//                    null                          as g2_last_stmt_date,
//                    mmddyy_to_date(mf.date_opened)as g2_date_opened,
//                    null                          as g2_date_last_credit_ck,
//                    null                          as g2_date_last_card_req,
//                    null                          as g2_last_call_date,
//                    null                          as g2_next_call_date,
//                    null                          as g2_fin_stmt_due_date,
//                    null                          as g2_fin_stmt_rec_date,
//                    null                          as g2_store_ind_flag,
//                    null                          as g2_number_of_stmts,
//                    null                          as g2_discount_ind,
//                    null                          as g2_rcl_list,
//                    null                          as g2_crb_list,
//                    null                          as g2_card_mailers,
//                    null                          as g2_calling_cards,
//                    null                          as g2_imprinter_rentals,
//                    null                          as g2_member_fees,
//                    null                          as g2_pos_terminals,
//                    null                          as g2_mcs,
//                    null                          as g2_unique_message,
//                    null                          as g2_bet1,
//                    null                          as g2_bet2,
//                    null                          as g2_bets,
//                    null                          as g2_intch_net_gross_f$,
//                    null                          as g2_intch_net_gross_f#,
//                    null                          as g3_visa_ach_opt,
//                    null                          as g3_mc_ach_opt,
//                    null                          as g3_visa_cash_ach_opt,
//                    null                          as g3_mc_cash_ach_opt,
//                    null                          as g3_discover_ach_opt,
//                    null                          as g3_jcb_ach_opt,
//                    null                          as g3_debit_card_ach_opt,
//                    null                          as g3_diners_club_ach_opt,
//                    null                          as g3_amex_ach_opt,
//                    null                          as g3_plan1_ach_opt,
//                    null                          as g3_plan2_ach_opt,
//                    null                          as g3_plan3_ach_opt,
//                    null                          as g3_visa_lg_ticket_ach_opt,
//                    null                          as g3_mc_lg_ticket_ach_opt,
//                    mf.trans_dest_global          as g3_overall_dest_opt,
//                    mf.trans_dest_deposit         as g3_deposit_opt,
//                    mf.trans_dest_adj             as g3_adjustment_opt,
//                    mf.trans_dest_chgback         as g3_chargeback_opt,
//                    mf.trans_dest_reversal        as g3_reversal_opt,
//                    mf.trans_dest_cgbk_revrs      as g3_chargeback_rev_opt,
//                    mf.trans_dest_dda_adj         as g3_dda_adj_opt,
//                    mf.trans_dest_batch           as g3_batch_adj_opt,
//                    mf.trans_dest_other_1         as g3_transaction1_opt,
//                    mf.trans_dest_other_2         as g3_transaction2_opt,
//                    mf.trans_dest_other_3         as g3_transaction3_opt,
//                    mf.amex_pc_id                 as g3_amex_pcid,
//                    null                          as g3_amex_descriptor_code,
//                    null                          as g3_amex_settlement_flg,
//                    null                          as g3_discover_ref_number,
//                    ltrim(rtrim(mf.damexse))      as g3_amex_account_num,
//                    ltrim(rtrim(mf.dmdsnum))      as g3_discover_acct_num,
//                    null                          as g3_jcb_acct_num,
//                    mf.diners_club_number         as g3_diners_club_acct_num,
//                    null                          as g3_cardholder_detail_flg,
//                    null                          as g3_confirmation_letter_flg,
//                    null                          as g3_cfirm_ltr_opt_mbatch,
//                    null                          as g3_cfirm_ltr_opt_adjust,
//                    null                          as g3_cfirm_ltr_opt_retrvals,
//                    '00'                          as ad_id_code,
//                    -- 3858 uses custom addresses 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dba_name      ,sa.address_name)    else  
//                      decode(sa.merchant_number, null, mf.name1_line_1, sa.address_name)    end as ad_name_line1, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dmaddr        ,sa.address_1)       else  
//                      decode(sa.merchant_number, null, mf.addr1_line_1, sa.address_1)       end as ad_address_line1, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.address_line_3,sa.address_2)       else  
//                      decode(sa.merchant_number, null, mf.addr1_line_2, sa.address_2)       end as ad_address_line2, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dmcity        ,sa.address_city)    else  
//                      decode(sa.merchant_number, null, mf.city1_line_4, sa.address_city)    end as ad_city, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null,mf.dmstate       ,sa.address_state)    else  
//                      decode(sa.merchant_number, null, mf.state1_line_4, sa.address_state)  end as ad_state, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null,mf.dmzip         ,sa.address_zip)      else  
//                      decode(sa.merchant_number, null, mf.zip1_line_4, sa.address_zip)      end as ad_zip_code,
//                    null                          as ad_date_last_maint
//            from    mif                 mf,
//                    statement_address   sa
//            where   mf.merchant_number = :merchantId
//                    and sa.merchant_number(+) = mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_gn \n          (\n            hh_load_sec,\n            hh_bank_number,\n            hh_merchant_number,\n            hh_dba_name,\n            hh_curr_date,\n            hh_active_date,\n            load_filename,\n            load_file_id,\n            g1_association_number,\n            g1_sic_code,\n            g1_class_code,\n            g1_dba_name,\n            g1_dba_city,\n            g1_dba_state,\n            g1_dba_zip,\n            g1_phone1,\n            g1_phone2,\n            g1_business_lic_number,\n            g1_bank_officer1,\n            g1_bank_officer2,\n            g1_user_bank_number,\n            g1_user_branch_number,\n            g1_federal_tax_id,\n            g1_state_tax_id,\n            g1_edc_flag,\n            g1_rep_code,\n            g1_merchant_status,\n            g1_user_flag1,\n            g1_user_flag2,\n            g1_user_data1,\n            g1_user_data2,\n            g1_user_data3,\n            g1_user_data4,\n            g1_user_data5,\n            g1_user_account1,\n            g1_user_account2,\n            g1_ach_flag,\n            g1_exception_table,\n            g1_investigator_code,\n            g1_visa_cps2,\n            g1_visa_cps1,\n            g1_visa_super,\n            g1_visa_epds,\n            g1_visa_psrf,\n            g1_visa_eirf,\n            g1_visa_expansion,\n            g1_mc_merit3,\n            g1_mc_merit1,\n            g1_mc_super,\n            g1_mc_petro_cat,\n            g1_mc_wharehouse_club,\n            g1_mc_premier,\n            g1_mc_expansion,\n            g1_club,\n            g1_merchant_type,\n            g1_incorporation_status,\n            g1_member_id,\n            g1_vrs_flag,\n            g2_owner_name,\n            g2_manager_name,\n            g2_asst_manager,\n            g2_other_name,\n            g2_owner_ssn_enc,\n            g2_owner_lic_number,\n            g2_last_stmt_date,\n            g2_date_opened,\n            g2_date_last_credit_ck,\n            g2_date_last_card_req,\n            g2_last_call_date,\n            g2_next_call_date,\n            g2_fin_stmt_due_date,\n            g2_fin_stmt_rec_date,\n            g2_store_ind_flag,\n            g2_number_of_stmts,\n            g2_discount_ind,\n            g2_rcl_list,\n            g2_crb_list,\n            g2_card_mailers,\n            g2_calling_cards,\n            g2_imprinter_rentals,\n            g2_member_fees,\n            g2_pos_terminals,\n            g2_mcs,\n            g2_unique_message,\n            g2_bet1,\n            g2_bet2,\n            g2_bets,\n            g2_intch_net_gross_f$,\n            g2_intch_net_gross_f#,\n            g3_visa_ach_opt,\n            g3_mc_ach_opt,\n            g3_visa_cash_ach_opt,\n            g3_mc_cash_ach_opt,\n            g3_discover_ach_opt,\n            g3_jcb_ach_opt,\n            g3_debit_card_ach_opt,\n            g3_diners_club_ach_opt,\n            g3_amex_ach_opt,\n            g3_plan1_ach_opt,\n            g3_plan2_ach_opt,\n            g3_plan3_ach_opt,\n            g3_visa_lg_ticket_ach_opt,\n            g3_mc_lg_ticket_ach_opt,\n            g3_overall_dest_opt,\n            g3_deposit_opt,\n            g3_adjustment_opt,\n            g3_chargeback_opt,\n            g3_reversal_opt,\n            g3_chargeback_rev_opt,\n            g3_dda_adj_opt,\n            g3_batch_adj_opt,\n            g3_transaction1_opt,\n            g3_transaction2_opt,\n            g3_transaction3_opt,\n            g3_amex_pcid,\n            g3_amex_descriptor_code,\n            g3_amex_settlement_flg,\n            g3_discover_ref_number,\n            g3_amex_account_num,\n            g3_discover_acct_num,\n            g3_jcb_acct_num,\n            g3_diners_club_acct_num,\n            g3_cardholder_detail_flg,\n            g3_confirmation_letter_flg,\n            g3_cfirm_ltr_opt_mbatch,\n            g3_cfirm_ltr_opt_adjust,\n            g3_cfirm_ltr_opt_retrvals,\n            ad_id_code,\n            ad_name_line1,\n            ad_address_line1,\n            ad_address_line2,\n            ad_city,\n            ad_state,\n            ad_zip_code,\n            ad_date_last_maint\n          )\n          select   :1                        as hh_load_sec,\n                  mf.bank_number                as hh_bank_number,\n                  mf.merchant_number            as hh_merchant_number,\n                  substr(mf.dba_name,1,25)      as hh_dba_name,\n                  last_day( :2  )         as hh_curr_date,\n                   :3                     as hh_active_date,\n                   :4                   as load_filename,\n                   :5                     as load_file_id,\n                  mf.dmagent                    as g1_association_number,\n                  mf.sic_code                   as g1_sic_code,\n                  mf.sic_code                   as g1_class_code,\n                  substr(mf.dba_name,1,25)      as g1_dba_name,\n                  substr(mf.dmcity,1,13)        as g1_dba_city,\n                  mf.dmstate                    as g1_dba_state,\n                  mf.dmzip                      as g1_dba_zip,\n                  mf.phone_1                    as g1_phone1,\n                  mf.phone_2_fax                as g1_phone2,\n                  mf.business_license           as g1_business_lic_number,\n                  mf.officer_1                  as g1_bank_officer1,\n                  mf.officer_2                  as g1_bank_officer2,\n                  mf.bank_number                as g1_user_bank_number,\n                  0                             as g1_user_branch_number,\n                  mf.federal_tax_id             as g1_federal_tax_id,\n                  mf.state_tax_id               as g1_state_tax_id,\n                  mf.edc_flagst_other_3         as g1_edc_flag,\n                  mf.rep_code                   as g1_rep_code,\n                  mf.dmacctst                   as g1_merchant_status,\n                  mf.user_flag_1                as g1_user_flag1,\n                  mf.user_flag_2                as g1_user_flag2,\n                  mf.user_data_1                as g1_user_data1,\n                  mf.user_data_2                as g1_user_data2,\n                  mf.user_data_3                as g1_user_data3,\n                  mf.user_data_4                as g1_user_data4,\n                  mf.user_data_5                as g1_user_data5,\n                  mf.user_acount_1              as g1_user_account1,\n                  mf.user_account_2             as g1_user_account2,\n                  mf.daily_ach                  as g1_ach_flag,\n                  mf.met_table                  as g1_exception_table,\n                  mf.inv_code_investigate       as g1_investigator_code,\n                  null                          as g1_visa_cps2,\n                  mf.dmcpsind                   as g1_visa_cps1,\n                  null                          as g1_visa_super,\n                  null                          as g1_visa_epds,\n                  mf.psrf_eligibility           as g1_visa_psrf,\n                  null                          as g1_visa_eirf,\n                  null                          as g1_visa_expansion,\n                  mf.dmer3ind                   as g1_mc_merit3,\n                  mf.merit_eligibility          as g1_mc_merit1,\n                  null                          as g1_mc_super,\n                  null                          as g1_mc_petro_cat,\n                  null                          as g1_mc_wharehouse_club,\n                  null                          as g1_mc_premier,\n                  null                          as g1_mc_expansion,\n                  mf.club_eligibility           as g1_club,\n                  mf.merchant_type              as g1_merchant_type,\n                  null                          as g1_incorporation_status,\n                  null                          as g1_member_id,\n                  null                          as g1_vrs_flag,\n                  mf.owner_name                 as g2_owner_name,\n                  mf.manager_name               as g2_manager_name,\n                  null                          as g2_asst_manager,\n                  null                          as g2_other_name,\n                  nvl(mf.ssn_enc,\n                  dukpt_encrypt_wrapper(mf.ssn)\n                  )                             as g2_owner_ssn_enc,\n                  mf.license_number             as g2_owner_lic_number,\n                  null                          as g2_last_stmt_date,\n                  mmddyy_to_date(mf.date_opened)as g2_date_opened,\n                  null                          as g2_date_last_credit_ck,\n                  null                          as g2_date_last_card_req,\n                  null                          as g2_last_call_date,\n                  null                          as g2_next_call_date,\n                  null                          as g2_fin_stmt_due_date,\n                  null                          as g2_fin_stmt_rec_date,\n                  null                          as g2_store_ind_flag,\n                  null                          as g2_number_of_stmts,\n                  null                          as g2_discount_ind,\n                  null                          as g2_rcl_list,\n                  null                          as g2_crb_list,\n                  null                          as g2_card_mailers,\n                  null                          as g2_calling_cards,\n                  null                          as g2_imprinter_rentals,\n                  null                          as g2_member_fees,\n                  null                          as g2_pos_terminals,\n                  null                          as g2_mcs,\n                  null                          as g2_unique_message,\n                  null                          as g2_bet1,\n                  null                          as g2_bet2,\n                  null                          as g2_bets,\n                  null                          as g2_intch_net_gross_f$,\n                  null                          as g2_intch_net_gross_f#,\n                  null                          as g3_visa_ach_opt,\n                  null                          as g3_mc_ach_opt,\n                  null                          as g3_visa_cash_ach_opt,\n                  null                          as g3_mc_cash_ach_opt,\n                  null                          as g3_discover_ach_opt,\n                  null                          as g3_jcb_ach_opt,\n                  null                          as g3_debit_card_ach_opt,\n                  null                          as g3_diners_club_ach_opt,\n                  null                          as g3_amex_ach_opt,\n                  null                          as g3_plan1_ach_opt,\n                  null                          as g3_plan2_ach_opt,\n                  null                          as g3_plan3_ach_opt,\n                  null                          as g3_visa_lg_ticket_ach_opt,\n                  null                          as g3_mc_lg_ticket_ach_opt,\n                  mf.trans_dest_global          as g3_overall_dest_opt,\n                  mf.trans_dest_deposit         as g3_deposit_opt,\n                  mf.trans_dest_adj             as g3_adjustment_opt,\n                  mf.trans_dest_chgback         as g3_chargeback_opt,\n                  mf.trans_dest_reversal        as g3_reversal_opt,\n                  mf.trans_dest_cgbk_revrs      as g3_chargeback_rev_opt,\n                  mf.trans_dest_dda_adj         as g3_dda_adj_opt,\n                  mf.trans_dest_batch           as g3_batch_adj_opt,\n                  mf.trans_dest_other_1         as g3_transaction1_opt,\n                  mf.trans_dest_other_2         as g3_transaction2_opt,\n                  mf.trans_dest_other_3         as g3_transaction3_opt,\n                  mf.amex_pc_id                 as g3_amex_pcid,\n                  null                          as g3_amex_descriptor_code,\n                  null                          as g3_amex_settlement_flg,\n                  null                          as g3_discover_ref_number,\n                  ltrim(rtrim(mf.damexse))      as g3_amex_account_num,\n                  ltrim(rtrim(mf.dmdsnum))      as g3_discover_acct_num,\n                  null                          as g3_jcb_acct_num,\n                  mf.diners_club_number         as g3_diners_club_acct_num,\n                  null                          as g3_cardholder_detail_flg,\n                  null                          as g3_confirmation_letter_flg,\n                  null                          as g3_cfirm_ltr_opt_mbatch,\n                  null                          as g3_cfirm_ltr_opt_adjust,\n                  null                          as g3_cfirm_ltr_opt_retrvals,\n                  '00'                          as ad_id_code,\n                  -- 3858 uses custom addresses \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dba_name      ,sa.address_name)    else  \n                    decode(sa.merchant_number, null, mf.name1_line_1, sa.address_name)    end as ad_name_line1, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dmaddr        ,sa.address_1)       else  \n                    decode(sa.merchant_number, null, mf.addr1_line_1, sa.address_1)       end as ad_address_line1, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.address_line_3,sa.address_2)       else  \n                    decode(sa.merchant_number, null, mf.addr1_line_2, sa.address_2)       end as ad_address_line2, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dmcity        ,sa.address_city)    else  \n                    decode(sa.merchant_number, null, mf.city1_line_4, sa.address_city)    end as ad_city, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null,mf.dmstate       ,sa.address_state)    else  \n                    decode(sa.merchant_number, null, mf.state1_line_4, sa.address_state)  end as ad_state, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null,mf.dmzip         ,sa.address_zip)      else  \n                    decode(sa.merchant_number, null, mf.zip1_line_4, sa.address_zip)      end as ad_zip_code,\n                  null                          as ad_date_last_maint\n          from    mif                 mf,\n                  statement_address   sa\n          where   mf.merchant_number =  :6  \n                  and sa.merchant_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setLong(5,loadFileId);
   __sJT_st.setLong(6,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1099^9*/
      
        // load the totals
        /*@lineinfo:generated-code*//*@lineinfo:1102^9*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_gn
//            set     s1_bet_table_number             = 9999,
//                    s1_imprinter_income_fee         = 0,
//                    s1_terminal_income              = 0,
//                    s1_pos_terminal_income          = 0,
//                    s1_membership_income            = 0,
//                    s1_adjustment_fee_income        = 0,
//                    s1_chargeback_fee_income        = 0,
//                    s1_misc_income1                 = 0,
//                    s1_misc_income2                 = 0,
//                    s1_misc_income3                 = 0,
//                    s1_misc_income4                 = 0,
//                    s1_misc_income5                 = 0,
//                    s1_misc_income6                 = 0,
//                    s1_account_on_file_income       = 0,
//                    s1_processing_fee_income        = 0,
//                    s1_programming_fee_income       = 0,
//                    s1_plastice_income              = 0,
//                    s1_pin_pad_income               = 0,
//                    s1_sale_drft_supply_income      = 0,
//                    s1_credit_voucher_income        = 0,
//                    s1_transmittals_supplied_inc    = 0,
//                    s1_additional_supplies_income   = 0,
//                    s1_merchant_call_income         = 0,
//                    s1_statement_income             = 0,
//                    s1_postage_income               = 0,
//                    s1_setup_fee_income             = 0,
//                    s1_lost_card_income             = 0,
//                    s1_free_checking_income         = 0,
//                    s1_fraud_income                 = 0,
//                    s1_rebate_income                = 0,
//                    s1_proof_income                 = 0,
//                    s1_cost_of_funds_income         = 0,
//                    s1_net_sales_income             = 0,
//                    s1_transmittal_processing_inc   = 0,
//                    s1_num_terminals                = 0,
//                    s1_num_printers                 = 0,
//                    s1_num_imprinters               = 0,
//                    s1_num_pin_pads                 = 0,
//                    s1_num_membership_fee_charges   = 0
//            where   hh_load_sec = :loadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_gn\n          set     s1_bet_table_number             = 9999,\n                  s1_imprinter_income_fee         = 0,\n                  s1_terminal_income              = 0,\n                  s1_pos_terminal_income          = 0,\n                  s1_membership_income            = 0,\n                  s1_adjustment_fee_income        = 0,\n                  s1_chargeback_fee_income        = 0,\n                  s1_misc_income1                 = 0,\n                  s1_misc_income2                 = 0,\n                  s1_misc_income3                 = 0,\n                  s1_misc_income4                 = 0,\n                  s1_misc_income5                 = 0,\n                  s1_misc_income6                 = 0,\n                  s1_account_on_file_income       = 0,\n                  s1_processing_fee_income        = 0,\n                  s1_programming_fee_income       = 0,\n                  s1_plastice_income              = 0,\n                  s1_pin_pad_income               = 0,\n                  s1_sale_drft_supply_income      = 0,\n                  s1_credit_voucher_income        = 0,\n                  s1_transmittals_supplied_inc    = 0,\n                  s1_additional_supplies_income   = 0,\n                  s1_merchant_call_income         = 0,\n                  s1_statement_income             = 0,\n                  s1_postage_income               = 0,\n                  s1_setup_fee_income             = 0,\n                  s1_lost_card_income             = 0,\n                  s1_free_checking_income         = 0,\n                  s1_fraud_income                 = 0,\n                  s1_rebate_income                = 0,\n                  s1_proof_income                 = 0,\n                  s1_cost_of_funds_income         = 0,\n                  s1_net_sales_income             = 0,\n                  s1_transmittal_processing_inc   = 0,\n                  s1_num_terminals                = 0,\n                  s1_num_printers                 = 0,\n                  s1_num_imprinters               = 0,\n                  s1_num_pin_pads                 = 0,\n                  s1_num_membership_fee_charges   = 0\n          where   hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1145^9*/
        
        // update the totals in the GN table
        updateTotals(loadSec,merchantId);
      }
      catch( Exception e )
      {
        logEntry( "loadGN( " + merchantId + "," + activeDate + ")",e.toString());
      }
      finally
      {
      }
    }
  
    protected void loadIC( String loadFilename, long loadSec, long merchantId, Date activeDate )
    {
      HashMap           betData           = null;
      IcBillingData     betItem           = null;
      int               betNumber         = 0;
      int               count             = 0;
      double            amount            = 0.0;
      ResultSetIterator it                = null;
      double            perItem           = 0.0;
      double            rate              = 0.0;
      ResultSet         resultSet         = null;
      boolean           retVal            = false;
      HashMap           betVolume         = new HashMap();
      IcVolume          icVolume          = null;
    
      long              loadFileId    = 0L;
      String            itemCat       = null;
      double            feesDue       = 0.0;
      String            cardType      = null;
      String            smsg          = null;
      StatementRecord   srec          = null;
      double            itemAmount    = 0.0;
      HashMap           srecs         = new HashMap();
      
      try
      {
        // retrieve dt records along with their associated discount, ic billing, and ic expense
        long    startTime       = System.currentTimeMillis();
        int     bankNumber      = Integer.parseInt( getEventArg(0) );
        loadFileId      = SettlementDb.loadFilenameToLoadFileId(loadFilename);
        
        // use the last day of the month as the basis
        // for all interchange rates.  this allows april/october
        // changes to be applied in the first billing month.
        // Calendar cal = Calendar.getInstance();
        // cal.setTime(activeDate);
        // cal.add(Calendar.MONTH,1);
        // cal.add(Calendar.DAY_OF_MONTH,-1);
        // Date betDate = new java.sql.Date(cal.getTime().getTime());
      
        //@log.debug("retrieving ic billing to process " + loadFilename + " (" + loadFileId + ")");
        /*@lineinfo:generated-code*//*@lineinfo:1200^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                          as merchant_number,
//                    mf.ic_bet_visa                              as bet_number,
//                    decode(substr(sm.item_subclass,2,1),'D','Y','N')
//                                                                as is_debit_card,       -- here for "% debit markup to return"; will not catch business debit  
//                    decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass)
//                                                                as card_type,                                                              
//                    ( decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass) || 
//                      lpad(sm.ic_cat,5,'0') || 
//                      '-' || icd.rec_id )                       as ic_label,
//                    sum( sm.sales_count )                       as sales_count,
//                    sum( sm.sales_amount )                      as sales_amount,
//                    sum( sm.credits_count )                     as credits_count,
//                    sum( sm.credits_amount )                    as credits_amount,
//                    sum( sm.sales_count - sm.credits_count )    as net_count,
//                    sum( sm.sales_amount - sm.credits_amount )  as net_amount
//            from    mbs_daily_summary       sm,
//                    mif                     mf,
//                    daily_detail_file_ic_desc icd
//            where   sm.me_load_file_id = :loadFileId
//                    and sm.merchant_number = :merchantId
//                    and not sm.ic_cat is null
//                    and mf.merchant_number = sm.merchant_number
//                    and icd.card_type = sm.item_subclass
//                    and icd.ic_code = sm.ic_cat
//                    and sm.activity_date between icd.valid_date_begin and icd.valid_date_end
//                    and sm.item_subclass not in ('VP','MP')
//            group by  sm.merchant_number, mf.ic_bet_visa,
//                      decode(substr(sm.item_subclass,2,1),'D','Y','N'),
//                      decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass),
//                      lpad(sm.ic_cat,5,'0'), icd.rec_id
//            order by bet_number, merchant_number, ic_label
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                          as merchant_number,\n                  mf.ic_bet_visa                              as bet_number,\n                  decode(substr(sm.item_subclass,2,1),'D','Y','N')\n                                                              as is_debit_card,       -- here for \"% debit markup to return\"; will not catch business debit  \n                  decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass)\n                                                              as card_type,                                                              \n                  ( decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass) || \n                    lpad(sm.ic_cat,5,'0') || \n                    '-' || icd.rec_id )                       as ic_label,\n                  sum( sm.sales_count )                       as sales_count,\n                  sum( sm.sales_amount )                      as sales_amount,\n                  sum( sm.credits_count )                     as credits_count,\n                  sum( sm.credits_amount )                    as credits_amount,\n                  sum( sm.sales_count - sm.credits_count )    as net_count,\n                  sum( sm.sales_amount - sm.credits_amount )  as net_amount\n          from    mbs_daily_summary       sm,\n                  mif                     mf,\n                  daily_detail_file_ic_desc icd\n          where   sm.me_load_file_id =  :1  \n                  and sm.merchant_number =  :2  \n                  and not sm.ic_cat is null\n                  and mf.merchant_number = sm.merchant_number\n                  and icd.card_type = sm.item_subclass\n                  and icd.ic_code = sm.ic_cat\n                  and sm.activity_date between icd.valid_date_begin and icd.valid_date_end\n                  and sm.item_subclass not in ('VP','MP')\n          group by  sm.merchant_number, mf.ic_bet_visa,\n                    decode(substr(sm.item_subclass,2,1),'D','Y','N'),\n                    decode(substr(sm.item_subclass,1,1),'V','VS','M','MC',sm.item_subclass),\n                    lpad(sm.ic_cat,5,'0'), icd.rec_id\n          order by bet_number, merchant_number, ic_label";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1233^9*/
        resultSet = it.getResultSet();
        //@log.debug("  query complete: " + (System.currentTimeMillis() - startTime) + " ms");
      
        while( resultSet.next() )
        {
          betNumber = resultSet.getInt("bet_number");
        
          // load the bet data
          if ( betData == null )
          {
            //@log.debug("  loading BET " + bankNumber + "/" + betNumber );
            betData = com.mes.mbs.BillingDb.loadIcBillingData(bankNumber,betNumber,activeDate);
          }
          betItem = (betData == null) ? null : (IcBillingData)betData.get(resultSet.getString("ic_label"));
        
          if ( betItem == null ) continue;    // skip entries with no billing data
        
          if ( betItem.ApplyToReturns == true )
          {
            count   = resultSet.getInt("net_count");
            amount  = resultSet.getDouble("net_amount");
          }          
          else  // sales only
          {
            count   = resultSet.getInt("sales_count");
            amount  = resultSet.getDouble("sales_amount");
          }
          rate    = betItem.getRate();
          perItem = betItem.getPerItem();
        
          if ( rate != 0.0 || perItem != 0.0 )
          {
            icVolume = (IcVolume)betVolume.get(betItem.StatementMsg);
            if ( icVolume == null )
            {
              icVolume = new IcVolume(merchantId,betItem.StatementMsg);
              betVolume.put(betItem.StatementMsg,icVolume);
            }
            icVolume.addVolume(count,amount,rate,perItem);
          }          
        }
        resultSet.close();
        it.close();
      
        storeIcVolume( loadSec, loadFilename, betVolume );
      
        commit();
        //@log.debug("  DONE: " + (System.currentTimeMillis() - startTime) + " ms");
      
        retVal = true;
      }
      catch(Exception e)
      {
        logEntry("loadIC(" + loadFilename + "," + merchantId + "," + activeDate + ")",e.toString());
      }
      finally
      {
        try { it.close(); } catch(Exception e) {}
      }
      
      //IC calculation for card type VP and MP
      try
      {
        loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
        
         /*@lineinfo:generated-code*//*@lineinfo:1299^10*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                  as merchant_number,
//                    mbe.item_category                           as item_cat,
//                    mbe.item_type 						      as item_type,
//                    concat(concat(sm.item_subclass,' - '),
//                           upper(mbe.statement_msg_default))    as statement_msg,
//                    sm.item_subclass                            as card_type,
//                    sum( sm.sales_count )                       as sales_count,
//                    sum( sm.sales_amount )                      as sales_amount,
//                    sum( sm.credits_count )                     as credits_count,
//                    sum( sm.credits_amount )                    as credits_amount,
//                    sum( sm.fees_due )                          as fees_due
//            from    mbs_daily_summary       sm,
//                    mif                     mf,
//                    mbs_elements            mbe
//             where   sm.me_load_file_id = :loadFileId
//                    and sm.merchant_number = :merchantId
//                    and mf.merchant_number = sm.merchant_number
//                    and sm.item_subclass in ('VP','MP')
//                    and mbe.item_category in ('IC')
//                    and sm.item_type = mbe.item_type
//                    and mbe.item_type  = 228
//            group by  sm.merchant_number, 
//                      sm.item_subclass,item_category,mbe.item_type,mbe.statement_msg_default
//            order  by merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                  as merchant_number,\n                  mbe.item_category                           as item_cat,\n                  mbe.item_type \t\t\t\t\t\t      as item_type,\n                  concat(concat(sm.item_subclass,' - '),\n                         upper(mbe.statement_msg_default))    as statement_msg,\n                  sm.item_subclass                            as card_type,\n                  sum( sm.sales_count )                       as sales_count,\n                  sum( sm.sales_amount )                      as sales_amount,\n                  sum( sm.credits_count )                     as credits_count,\n                  sum( sm.credits_amount )                    as credits_amount,\n                  sum( sm.fees_due )                          as fees_due\n          from    mbs_daily_summary       sm,\n                  mif                     mf,\n                  mbs_elements            mbe\n           where   sm.me_load_file_id =  :1  \n                  and sm.merchant_number =  :2  \n                  and mf.merchant_number = sm.merchant_number\n                  and sm.item_subclass in ('VP','MP')\n                  and mbe.item_category in ('IC')\n                  and sm.item_type = mbe.item_type\n                  and mbe.item_type  = 228\n          group by  sm.merchant_number, \n                    sm.item_subclass,item_category,mbe.item_type,mbe.statement_msg_default\n          order  by merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1325^9*/
        
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          itemCat = resultSet.getString("item_cat");
          count   = resultSet.getInt   ( "sales_count" );
          amount  = resultSet.getDouble( "sales_amount");
          feesDue = resultSet.getDouble("fees_due");
          smsg    = resultSet.getString("statement_msg");     
          cardType    = resultSet.getString("card_type");
          
          if ( feesDue != 0.0 )
          {
            itemAmount  = amount;
            
            srec = (StatementRecord)srecs.get(smsg);
            if ( srec == null )
            {
              srec = new StatementRecord();
              srec.setLoadSec(loadSec);
              srec.setItemCat(itemCat);
              srec.setItemCount(count);
              srec.setItemAmount(itemAmount);
              srec.setStatementMessage(smsg);
              srec.setFeeAmount(feesDue);
              srec.setLoadFilename(loadFilename);
              
              srecs.put(smsg,srec);
            }
            else
            {
              srec.addItemCount(count);
              srec.addItemAmount(itemAmount);
              srec.addFeeAmount(feesDue);
            }
          }
        }
        resultSet.close();
        it.close();
        
        for( Iterator i = srecs.keySet().iterator(); i.hasNext(); )
        {
          srec = (StatementRecord)srecs.get( (String)i.next() );
          storeStatementRecord(srec);
        }
        }
        catch( Exception e )
      {
        logEntry("loadIC(" + loadFilename + "," + merchantId + ")",e.toString());
      }      
      finally
      {
        try { it.close(); } catch( Exception ee ) {}
      }
      
    }
  
    protected void loadPL( String loadFilename, long loadSec, long merchantId )
    {
      MerchantBillingElement  el          = null;
      ResultSetIterator       it          = null;
      long                    loadFileId  = 0L;
      double                  perItem     = 0.0;
      double                  rate        = 0.0;
      int                     recCount    = 0;
      ResultSet               resultSet   = null;

      try
      {
        loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
        
        /*@lineinfo:generated-code*//*@lineinfo:1398^9*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_pl
//            ( 
//              hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,
//              pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,
//              pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,
//              pl_number_of_sales      , pl_sales_amount         ,
//              pl_number_of_credits    , pl_credits_amount       ,
//              pl_number_of_chargeback , pl_chargeback_amount    ,
//              pl_rebate               , pl_rebate_rate_per_item ,
//              pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,
//              pl_min_disc_amount      ,
//              pl_floor_limit          ,
//              pl_bet_table_number     
//            )
//            select  :loadSec                          as load_sec,
//                    :loadFilename                     as load_filename,
//                    decode_mbs_disc_method(mf.merchant_number,sm.item_subclass)
//                                                      as G_or_N_or_B,
//                    'F'                               as rate_table_type,
//                    9999                              as rate_table_number,
//                    decode_mbs_plan_type(mf.sic_code,sm.item_subclass)
//                                                      as plan_type,
//                    sm.rate                           as rate,
//                    sm.per_item                       as per_item,
//                    sum( sm.fees_due            )     as fees_due,
//                    sum( sm.sales_count         )     as sales_count,
//                    sum( sm.sales_amount        )     as sales_amount,
//                    sum( sm.credits_count       )     as credits_count,
//                    sum( sm.credits_amount      )     as credits_amount,
//                    0                                 as cb_count,
//                    0                                 as cb_amount,
//                    0                                 as rebate_rate,
//                    0                                 as rebate_per_item,
//                    mmddyy_to_date(mf.date_opened)    as sign_date,
//                    mf.last_deposit_date              as last_deposit_date,
//                    mf.date_of_1st_deposit            as first_deposit_date,
//                    0                                 as plan_min_disc,
//                    0                                 as floor_limit,
//                    9999                              as bet_number
//            from    mbs_daily_summary       sm,
//                    mbs_elements            mbe,
//                    mif                     mf
//            where   sm.me_load_file_id = :loadFileId
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and mbe.item_category = 'DISC'
//                    and mf.merchant_number = sm.merchant_number                      
//            group by  decode_mbs_disc_method(mf.merchant_number,sm.item_subclass),
//                      decode_mbs_plan_type(mf.sic_code,sm.item_subclass),
//                      sm.rate, sm.per_item,
//                      mf.date_opened,
//                      mf.last_deposit_date,
//                      mf.date_of_1st_deposit
//            order by plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_pl\n          ( \n            hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,\n            pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,\n            pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,\n            pl_number_of_sales      , pl_sales_amount         ,\n            pl_number_of_credits    , pl_credits_amount       ,\n            pl_number_of_chargeback , pl_chargeback_amount    ,\n            pl_rebate               , pl_rebate_rate_per_item ,\n            pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,\n            pl_min_disc_amount      ,\n            pl_floor_limit          ,\n            pl_bet_table_number     \n          )\n          select   :1                            as load_sec,\n                   :2                       as load_filename,\n                  decode_mbs_disc_method(mf.merchant_number,sm.item_subclass)\n                                                    as G_or_N_or_B,\n                  'F'                               as rate_table_type,\n                  9999                              as rate_table_number,\n                  decode_mbs_plan_type(mf.sic_code,sm.item_subclass)\n                                                    as plan_type,\n                  sm.rate                           as rate,\n                  sm.per_item                       as per_item,\n                  sum( sm.fees_due            )     as fees_due,\n                  sum( sm.sales_count         )     as sales_count,\n                  sum( sm.sales_amount        )     as sales_amount,\n                  sum( sm.credits_count       )     as credits_count,\n                  sum( sm.credits_amount      )     as credits_amount,\n                  0                                 as cb_count,\n                  0                                 as cb_amount,\n                  0                                 as rebate_rate,\n                  0                                 as rebate_per_item,\n                  mmddyy_to_date(mf.date_opened)    as sign_date,\n                  mf.last_deposit_date              as last_deposit_date,\n                  mf.date_of_1st_deposit            as first_deposit_date,\n                  0                                 as plan_min_disc,\n                  0                                 as floor_limit,\n                  9999                              as bet_number\n          from    mbs_daily_summary       sm,\n                  mbs_elements            mbe,\n                  mif                     mf\n          where   sm.me_load_file_id =  :3  \n                  and sm.merchant_number =  :4  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and mbe.item_category = 'DISC'\n                  and mf.merchant_number = sm.merchant_number                      \n          group by  decode_mbs_disc_method(mf.merchant_number,sm.item_subclass),\n                    decode_mbs_plan_type(mf.sic_code,sm.item_subclass),\n                    sm.rate, sm.per_item,\n                    mf.date_opened,\n                    mf.last_deposit_date,\n                    mf.date_of_1st_deposit\n          order by plan_type";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setLong(3,loadFileId);
   __sJT_st.setLong(4,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1454^9*/
        
        String[][]  requiredPlanTypes = 
          { 
            { "VS"  , "VISA"  },
            { "VB"  , "VSBS"  },
            { "VD"  , "VSDB"  },
            { "MC"  , "MC"    },
            { "MB"  , "MCBS"  },
            { "MD"  , "MCDB"  },
          };
        for ( int i = 0; i < requiredPlanTypes.length; ++i )
        {
          String cardType = requiredPlanTypes[i][0];
          String planType = requiredPlanTypes[i][1];
          /*@lineinfo:generated-code*//*@lineinfo:1469^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)  
//              from    monthly_extract_pl    pl
//              where   pl.hh_load_sec = :loadSec
//                      and pl.pl_plan_type = :planType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)   \n            from    monthly_extract_pl    pl\n            where   pl.hh_load_sec =  :1  \n                    and pl.pl_plan_type =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,planType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1475^11*/
        
          if ( recCount == 0 )
          {
            el = BillingData.findItemByType(101,cardType);
            if ( el != null )
            {
              perItem = el.getPerItem();
              rate    = el.getRate();
            }
            else
            {
              perItem = 0.0;
              rate    = 0.0;
            }
        
            /*@lineinfo:generated-code*//*@lineinfo:1491^13*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_pl
//                ( 
//                  hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,
//                  pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,
//                  pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,
//                  pl_number_of_sales      , pl_sales_amount         ,
//                  pl_number_of_credits    , pl_credits_amount       ,
//                  pl_number_of_chargeback , pl_chargeback_amount    ,
//                  pl_rebate               , pl_rebate_rate_per_item ,
//                  pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,
//                  pl_min_disc_amount      ,
//                  pl_floor_limit          ,
//                  pl_bet_table_number     
//                )
//                select  :loadSec                          as load_sec,
//                        :loadFilename                     as load_filename,
//                        decode(substr(:planType,1,1),
//                               'M', mf.mastcd_disc_method,
//                               'V', mf.visa_disc_method,
//                               'G')                       as G_or_N_or_B,
//                        'F'                               as rate_table_type,
//                        9999                              as rate_table_number,
//                        :planType                         as plan_type,
//                        :rate                             as rate,
//                        :perItem                          as per_item,
//                        0                                 as fees_due,
//                        0                                 as sales_count,
//                        0                                 as sales_amount,
//                        0                                 as credits_count,
//                        0                                 as credits_amount,
//                        0                                 as cb_count,
//                        0                                 as cb_amount,
//                        0                                 as rebate_rate,
//                        0                                 as rebate_per_item,
//                        mmddyy_to_date(mf.date_opened)    as sign_date,
//                        mf.last_deposit_date              as last_deposit_date,
//                        mf.date_of_1st_deposit            as first_deposit_date,
//                        0                                 as plan_min_disc,
//                        0                                 as floor_limit,
//                        9999                              as bet_number
//                from    mif                     mf
//                where   mf.merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_pl\n              ( \n                hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,\n                pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,\n                pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,\n                pl_number_of_sales      , pl_sales_amount         ,\n                pl_number_of_credits    , pl_credits_amount       ,\n                pl_number_of_chargeback , pl_chargeback_amount    ,\n                pl_rebate               , pl_rebate_rate_per_item ,\n                pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,\n                pl_min_disc_amount      ,\n                pl_floor_limit          ,\n                pl_bet_table_number     \n              )\n              select   :1                            as load_sec,\n                       :2                       as load_filename,\n                      decode(substr( :3  ,1,1),\n                             'M', mf.mastcd_disc_method,\n                             'V', mf.visa_disc_method,\n                             'G')                       as G_or_N_or_B,\n                      'F'                               as rate_table_type,\n                      9999                              as rate_table_number,\n                       :4                           as plan_type,\n                       :5                               as rate,\n                       :6                            as per_item,\n                      0                                 as fees_due,\n                      0                                 as sales_count,\n                      0                                 as sales_amount,\n                      0                                 as credits_count,\n                      0                                 as credits_amount,\n                      0                                 as cb_count,\n                      0                                 as cb_amount,\n                      0                                 as rebate_rate,\n                      0                                 as rebate_per_item,\n                      mmddyy_to_date(mf.date_opened)    as sign_date,\n                      mf.last_deposit_date              as last_deposit_date,\n                      mf.date_of_1st_deposit            as first_deposit_date,\n                      0                                 as plan_min_disc,\n                      0                                 as floor_limit,\n                      9999                              as bet_number\n              from    mif                     mf\n              where   mf.merchant_number =  :7 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,planType);
   __sJT_st.setString(4,planType);
   __sJT_st.setDouble(5,rate);
   __sJT_st.setDouble(6,perItem);
   __sJT_st.setLong(7,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1535^13*/
          }
        }
      
        // add the chargeback data
        /*@lineinfo:generated-code*//*@lineinfo:1540^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(substr(sm.item_subclass,1,2),
//                            --'MC','MC',
//                            'MD','MCDB',
//                            'MB','MCBS',
//                            'VS','VISA',
//                            'VD','VSDB',
//                            'VB','VSBS',
//                            'DB','DEBC',
//                            'EB','EBT',
//                            'BL','BML',
//                            sm.item_subclass)         as plan_type,
//                    sum( sm.fees_due )                as fees_due,
//                    sum( sm.item_count )              as cb_count,
//                    sum( sm.item_amount )             as cb_amount
//            from    mbs_daily_summary       sm,
//                    mbs_elements            mbe
//            where   sm.me_load_file_id = :loadFileId
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and lower(mbe.statement_msg_default) like '%chargeback%'
//            group by sm.item_subclass
//            order by plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(substr(sm.item_subclass,1,2),\n                          --'MC','MC',\n                          'MD','MCDB',\n                          'MB','MCBS',\n                          'VS','VISA',\n                          'VD','VSDB',\n                          'VB','VSBS',\n                          'DB','DEBC',\n                          'EB','EBT',\n                          'BL','BML',\n                          sm.item_subclass)         as plan_type,\n                  sum( sm.fees_due )                as fees_due,\n                  sum( sm.item_count )              as cb_count,\n                  sum( sm.item_amount )             as cb_amount\n          from    mbs_daily_summary       sm,\n                  mbs_elements            mbe\n          where   sm.me_load_file_id =  :1  \n                  and sm.merchant_number =  :2  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and lower(mbe.statement_msg_default) like '%chargeback%'\n          group by sm.item_subclass\n          order by plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1564^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          String  planType  = resultSet.getString("plan_type");
          int     count     = resultSet.getInt("cb_count");
          double  amount    = resultSet.getDouble("cb_amount");
        
          /*@lineinfo:generated-code*//*@lineinfo:1573^11*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_pl  pl
//              set     pl.pl_number_of_chargeback = :count, 
//                      pl.pl_chargeback_amount    = :amount
//              where   pl.hh_load_sec = :loadSec
//                      and pl.pl_plan_type = :planType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_pl  pl\n            set     pl.pl_number_of_chargeback =  :1  , \n                    pl.pl_chargeback_amount    =  :2  \n            where   pl.hh_load_sec =  :3  \n                    and pl.pl_plan_type =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,count);
   __sJT_st.setDouble(2,amount);
   __sJT_st.setLong(3,loadSec);
   __sJT_st.setString(4,planType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1580^11*/
        
          // if there is no record for this plan type, then create a new one
          if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1585^13*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_pl
//                ( 
//                  hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,
//                  pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,
//                  pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,
//                  pl_number_of_sales      , pl_sales_amount         ,
//                  pl_number_of_credits    , pl_credits_amount       ,
//                  pl_number_of_chargeback , pl_chargeback_amount    ,
//                  pl_rebate               , pl_rebate_rate_per_item ,
//                  pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,
//                  pl_min_disc_amount      ,
//                  pl_floor_limit          ,
//                  pl_bet_table_number     
//                )
//                select  :loadSec                          as load_sec,
//                        :loadFilename                     as load_filename,
//                        decode(substr(:planType,1,1),
//                               'M', mf.mastcd_disc_method,
//                               'V', mf.visa_disc_method,
//                               'G')                       as G_or_N_or_B,
//                        'F'                               as rate_table_type,
//                        9999                              as rate_table_number,
//                        :planType                         as plan_type,
//                        0                                 as rate,
//                        0                                 as per_item,
//                        0                                 as fees_due,
//                        0                                 as sales_count,
//                        0                                 as sales_amount,
//                        0                                 as credits_count,
//                        0                                 as credits_amount,
//                        :count                            as cb_count,
//                        :amount                           as cb_amount,
//                        0                                 as rebate_rate,
//                        0                                 as rebate_per_item,
//                        mmddyy_to_date(mf.date_opened)    as sign_date,
//                        mf.last_deposit_date              as last_deposit_date,
//                        mf.date_of_1st_deposit            as first_deposit_date,
//                        0                                 as plan_min_disc,
//                        0                                 as floor_limit,
//                        9999                              as bet_number
//                from    mif                     mf
//                where   mf.merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_pl\n              ( \n                hh_load_sec             , load_filename           , pl_disc_calc_ind_gn ,\n                pl_rate_table_ind_fva   , pl_rate_table_number    , pl_plan_type        ,\n                pl_disc_rate            , pl_disc_rate_per_item   , pl_correct_disc_amt ,\n                pl_number_of_sales      , pl_sales_amount         ,\n                pl_number_of_credits    , pl_credits_amount       ,\n                pl_number_of_chargeback , pl_chargeback_amount    ,\n                pl_rebate               , pl_rebate_rate_per_item ,\n                pl_sign_date            , pl_last_depos_date      , pl_first_depos_date ,\n                pl_min_disc_amount      ,\n                pl_floor_limit          ,\n                pl_bet_table_number     \n              )\n              select   :1                            as load_sec,\n                       :2                       as load_filename,\n                      decode(substr( :3  ,1,1),\n                             'M', mf.mastcd_disc_method,\n                             'V', mf.visa_disc_method,\n                             'G')                       as G_or_N_or_B,\n                      'F'                               as rate_table_type,\n                      9999                              as rate_table_number,\n                       :4                           as plan_type,\n                      0                                 as rate,\n                      0                                 as per_item,\n                      0                                 as fees_due,\n                      0                                 as sales_count,\n                      0                                 as sales_amount,\n                      0                                 as credits_count,\n                      0                                 as credits_amount,\n                       :5                              as cb_count,\n                       :6                             as cb_amount,\n                      0                                 as rebate_rate,\n                      0                                 as rebate_per_item,\n                      mmddyy_to_date(mf.date_opened)    as sign_date,\n                      mf.last_deposit_date              as last_deposit_date,\n                      mf.date_of_1st_deposit            as first_deposit_date,\n                      0                                 as plan_min_disc,\n                      0                                 as floor_limit,\n                      9999                              as bet_number\n              from    mif                     mf\n              where   mf.merchant_number =  :7 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,planType);
   __sJT_st.setString(4,planType);
   __sJT_st.setInt(5,count);
   __sJT_st.setDouble(6,amount);
   __sJT_st.setLong(7,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1629^13*/
          }
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry( "loadPL( " + loadFilename + "," + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
  
 /*
  * The item subclass VP and MP are added by RS with select clause using
  * decode function for LCR implementation
 */
    protected void loadST( String loadFilename, long loadSec, long merchantId )
    {
      double                  amount        = 0.0;
      String                  cardType      = null;
      int                     count         = 0;
      MerchantBillingElement  el            = null;
      ResultSetIterator       it            = null;
      double                  itemAmount    = 0.0;
      String                  itemCat       = null;
      double                  feesDue       = 0.0;
      long                    loadFileId    = 0L;
      double                  perItem       = 0.0;
      double                  rate          = 0.0;
      ResultSet               resultSet     = null;
      String                  smsg          = null;
      StatementRecord         srec          = null;
      HashMap                 srecs         = new HashMap();
      int                     itemType      = 0;
      int                     globalWholesaleTravelTransFeeCount = 0;
      int                     freightAssessmentFeeCount = 0;

  
      try
      {
        loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
      
        /*@lineinfo:generated-code*//*@lineinfo:1671^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mbe.billing_frequency,
//                           'M',mbe.item_type,-1)                as item_type,
//                    mbe.item_category                           as item_cat,
//                    nvl(mbe.consolidate,'N')                    as consolidate,
//                    case 
//                      when sm.item_subclass in ('VB') then 'VS'
//                      when sm.item_subclass in ('MB','MD') then 'MC'
//                      when sm.item_subclass in ('EB','DB') then 'DB'
//                      else sm.item_subclass 
//                    end                                         as item_subclass,
//                    decode( sm.item_subclass,
//                            'MC','MASTERCARD',
//                            'MB','MASTERCARD',
//                            'MD','MASTERCARD',
//                            'VS','VISA',
//                            'VB','VISA',
//                            'VD','VISA',
//                            'DB','DEBIT',
//                            'EB','EBT',
//                            'BL','BML',
//                            'AM','AMERICAN EXPRESS',
//                            'DS','DISCOVER',
//                            'UK','OTHER',
//                            'ER','OTHER',
//                            'AC','ACH',
//                            'VP','VP',
//                            'MP','MP',
//                            sm.item_subclass )                  as card_type,
//                    upper(mbe.statement_msg_default)            as statement_msg,
//                    decode(mbe.billing_frequency,
//                           'M',0,sm.rate)                       as rate,
//                    decode(mbe.billing_frequency,
//                           'M',0,sm.per_item)                   as per_item,
//                    decode(mbe.data_item_type,
//                           null,'item_count',
//                           nvl(mbe.count_column_name,'item_count')
//                         )                                      as count_column_name,
//                    decode(mbe.data_item_type,
//                           null,'item_amount',
//                           nvl(mbe.amount_column_name,'item_amount')
//                         )                                      as amount_column_name,
//                    sum( sm.item_count )                        as item_count,
//                    sum( sm.item_amount )                       as item_amount,
//                    sum( sm.sales_count )                       as sales_count,
//                    sum( sm.sales_amount )                      as sales_amount,
//                    sum( sm.credits_count )                     as credits_count,
//                    sum( sm.credits_amount )                    as credits_amount,
//                    sum( sm.sales_count + sm.credits_count )    as hash_count,
//                    sum( sm.sales_amount + sm.credits_amount )  as hash_amount,
//                    sum( sm.sales_count + sm.credits_count )    as net_count,
//                    sum( sm.sales_amount - sm.credits_amount )  as net_amount,
//                    sum( sm.fees_due )                          as fees_due
//            from    mbs_daily_summary       sm,
//                    mbs_elements            mbe
//            where   sm.me_load_file_id = :loadFileId 
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and mbe.item_category in ('MISC', 'ASSOC','PLAN')
//                    /* credit transaction fee only applies to non-PIN volume */
//                    and not (mbe.item_type = 200 and sm.item_subclass in ('EB','DB'))
//            group by  decode(mbe.billing_frequency,'M',mbe.item_type,-1),
//                      decode(mbe.billing_frequency,'M',0,sm.rate),
//                      decode(mbe.billing_frequency,'M',0,sm.per_item),
//                      mbe.item_category, sm.item_subclass,
//                      nvl(mbe.consolidate,'N'),
//                      decode(mbe.data_item_type,null,'item_count',nvl(mbe.count_column_name,'item_count')),
//                      decode(mbe.data_item_type,null,'item_amount',nvl(mbe.amount_column_name,'item_amount')),
//                      mbe.statement_msg_default
//            order by mbe.item_category, mbe.statement_msg_default, sm.item_subclass
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mbe.billing_frequency,\n                         'M',mbe.item_type,-1)                as item_type,\n                  mbe.item_category                           as item_cat,\n                  nvl(mbe.consolidate,'N')                    as consolidate,\n                  case \n                    when sm.item_subclass in ('VB') then 'VS'\n                    when sm.item_subclass in ('MB','MD') then 'MC'\n                    when sm.item_subclass in ('EB','DB') then 'DB'\n                    else sm.item_subclass \n                  end                                         as item_subclass,\n                  decode( sm.item_subclass,\n                          'MC','MASTERCARD',\n                          'MB','MASTERCARD',\n                          'MD','MASTERCARD',\n                          'VS','VISA',\n                          'VB','VISA',\n                          'VD','VISA',\n                          'DB','DEBIT',\n                          'EB','EBT',\n                          'BL','BML',\n                          'AM','AMERICAN EXPRESS',\n                          'DS','DISCOVER',\n                          'UK','OTHER',\n                          'ER','OTHER',\n                          'AC','ACH',\n                          'VP','VP',\n                          'MP','MP',\n                          sm.item_subclass )                  as card_type,\n                  upper(mbe.statement_msg_default)            as statement_msg,\n                  decode(mbe.billing_frequency,\n                         'M',0,sm.rate)                       as rate,\n                  decode(mbe.billing_frequency,\n                         'M',0,sm.per_item)                   as per_item,\n                  decode(mbe.data_item_type,\n                         null,'item_count',\n                         nvl(mbe.count_column_name,'item_count')\n                       )                                      as count_column_name,\n                  decode(mbe.data_item_type,\n                         null,'item_amount',\n                         nvl(mbe.amount_column_name,'item_amount')\n                       )                                      as amount_column_name,\n                  sum( sm.item_count )                        as item_count,\n                  sum( sm.item_amount )                       as item_amount,\n                  sum( sm.sales_count )                       as sales_count,\n                  sum( sm.sales_amount )                      as sales_amount,\n                  sum( sm.credits_count )                     as credits_count,\n                  sum( sm.credits_amount )                    as credits_amount,\n                  sum( sm.sales_count + sm.credits_count )    as hash_count,\n                  sum( sm.sales_amount + sm.credits_amount )  as hash_amount,\n                  sum( sm.sales_count + sm.credits_count )    as net_count,\n                  sum( sm.sales_amount - sm.credits_amount )  as net_amount,\n                  sum( sm.fees_due )                          as fees_due\n          from    mbs_daily_summary       sm,\n                  mbs_elements            mbe\n          where   sm.me_load_file_id =  :1   \n                  and sm.merchant_number =  :2  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and mbe.item_category in ('MISC', 'ASSOC','PLAN')\n                  /* credit transaction fee only applies to non-PIN volume */\n                  and not (mbe.item_type = 200 and sm.item_subclass in ('EB','DB'))\n          group by  decode(mbe.billing_frequency,'M',mbe.item_type,-1),\n                    decode(mbe.billing_frequency,'M',0,sm.rate),\n                    decode(mbe.billing_frequency,'M',0,sm.per_item),\n                    mbe.item_category, sm.item_subclass,\n                    nvl(mbe.consolidate,'N'),\n                    decode(mbe.data_item_type,null,'item_count',nvl(mbe.count_column_name,'item_count')),\n                    decode(mbe.data_item_type,null,'item_amount',nvl(mbe.amount_column_name,'item_amount')),\n                    mbe.statement_msg_default\n          order by mbe.item_category, mbe.statement_msg_default, sm.item_subclass";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1742^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          itemCat = resultSet.getString("item_cat");
          count   = resultSet.getInt   ( resultSet.getString("count_column_name") );
          amount  = resultSet.getDouble( resultSet.getString("amount_column_name"));
          perItem = resultSet.getDouble("per_item");
          rate    = resultSet.getDouble("rate");
          itemType = resultSet.getInt("item_type");
          feesDue = 0.0;
                   
          // the result set will contain the item type for
          // monthly billing elements and -1 for the daily
          // billing elements.  daily elements are calculated
          // by the daily aggregation and therefore the 
          // fees_due column will contain an accurate billing amount
          if ( itemType > 0 ) 
          {
            el = BillingData.findItemByType(itemType,
                                            resultSet.getString("item_subclass"));
                                                  
            if ( el != null )
            {
              rate    = el.getRate();
              perItem = el.getPerItem();
              
              if ( "PLAN".equals(itemCat) )
              {
                switch( el.getVolumeType() )
                {
                  case MerchantBillingElement.VT_CREDITS_ONLY:
                    count   = resultSet.getInt("credits_count");
                    amount  = resultSet.getDouble("credits_amount");
                    break;
                    
                  case MerchantBillingElement.VT_SALES_PLUS_CREDITS:
                    count   = resultSet.getInt("sales_count")     + resultSet.getInt("credits_count");
                    amount  = resultSet.getDouble("sales_amount") + resultSet.getDouble("credits_amount");
                    break;
                    
                  case MerchantBillingElement.VT_SALES_MINUS_CREDITS:
                    count   = resultSet.getInt("sales_count")     - resultSet.getInt("credits_count");
                    amount  = resultSet.getDouble("sales_amount") - resultSet.getDouble("credits_amount");
                    break;
                    
               // case MerchantBillingElement.VT_SALES_ONLY:
                  default:
                    count   = resultSet.getInt("sales_count");
                    amount  = resultSet.getDouble("sales_amount");
                    break;
                }
              }
              if( itemType == 129)
          	  {
            	    globalWholesaleTravelTransFeeCount = count;
          	  }
              if( itemType == 130)
          	  {
            	    freightAssessmentFeeCount = count;
          	  }
              if(itemType == 230 || itemType == 236){
            	  feesDue = resultSet.getDouble("fees_due");
              } else {
            	  feesDue = ( (count * el.getPerItem()) + 
                          (amount * el.getRate() * 0.01) ); 
              }
            }
            else if( itemType == 209 )
            {
              feesDue = resultSet.getDouble("fees_due");
            }
            //Added by RS for LCR implementation
            if( itemType == 227 || itemType == 229 )
            {
              feesDue = resultSet.getDouble("fees_due");
            }
          }
          else // default is daily, use fees_due
          {
            feesDue = resultSet.getDouble("fees_due");
          }
        
          if ( feesDue != 0.0 )
          {
          
            cardType    = resultSet.getString("card_type");
          
            smsg        = ((cardType == null || "Y".equals(resultSet.getString("consolidate"))) ? "" : ("ACH".equalsIgnoreCase(cardType) ? "" : cardType + " "))  +
                          resultSet.getString("statement_msg") +
                          (!"PLAN".equals(itemCat) ? "" :
                             " (" + 
                                ((rate == 0.0)    ? "" : NumberFormatter.getPercentString(rate/100.0,3)) +
                                ((perItem == 0.0) ? "" : MesMath.toFractionalCurrency(perItem,4)) + 
                             ")");
            itemAmount  = amount;
            
            srec = (StatementRecord)srecs.get(smsg);
            if ( srec == null )
            {
              srec = new StatementRecord();
              srec.setLoadSec(loadSec);
              srec.setItemCat(itemCat);
              srec.setItemCount(count);
              srec.setItemAmount(itemAmount);
              srec.setStatementMessage(smsg);
              srec.setFeeAmount(feesDue);
              srec.setLoadFilename(loadFilename);
              srec.setItemType(itemType);
  
              srecs.put(smsg,srec);
            }
            else
            {
              srec.addItemCount(count);
              srec.addItemAmount(itemAmount);
              srec.addFeeAmount(feesDue);
            }
          }
        }
        resultSet.close();
        it.close();
        
        for( Iterator i = srecs.keySet().iterator(); i.hasNext(); )
        {
          srec = (StatementRecord)srecs.get( (String)i.next() );
          if(srec.getItemType()==102)
          {
              int nabuCount=srec.getItemCount();
              nabuCount=nabuCount - (globalWholesaleTravelTransFeeCount+freightAssessmentFeeCount); 
              MerchantBillingElement ele = BillingData.findItemByType(102,"MC");
	          double nabuFeesDue=nabuCount*ele.getPerItem();
              srec.setItemCount(nabuCount);
              srec.setFeeAmount(nabuFeesDue);
          }
          storeStatementRecord(srec);
        }
      }
      catch( Exception e )
      {
        logEntry("loadST(" + loadFilename + "," + merchantId + ")",e.toString());
      }      
      finally
      {
        try { it.close(); } catch( Exception ee ) {}
      }
    }
      
    public void setBillingData( MerchantBilling billingData )
    {
      BillingData = billingData;
    }
    
    protected void storeAP( String mediaType, AuthTotals totals )
    {
      int[]         counts      = new int[6];
      double[]      fees        = new double[6];
      
      try
      {
        // setup the counts based on the media type
        if ( "AR".equals(mediaType) )
        {
          counts[0] = totals.getAruCount();
          fees[0]   = totals.getAruFeesDue();
        }
        else  
        {
          counts[0] = totals.getAuthCount();
          fees  [0] = totals.getAuthFeesDue();
          counts[1] = totals.getPGCount();
          fees  [1] = totals.getPGFeesDue();
          counts[2] = totals.get3dSecureCount();
          fees  [2] = totals.get3dSecureFeesDue();
          counts[3] = totals.getIntlPGCount();
          fees  [3] = totals.getIntlPGFeesDue();
          counts[4] = totals.getAvsCount();
          fees  [4] = totals.getAvsFeesDue();
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1904^9*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_ap  
//            (
//              hh_load_sec,
//              a1_vendor_code,a1_media_type,a1_plan_type,
//              a1_bet_table_id,
//              a1_number_fixed_auths,    a1_fixed_auths_income,
//              a1_950_number,            a1_950_income,
//              a1_above_floor_number,    a1_above_floor_income,
//              a1_below_floor_number,    a1_below_floor_income,
//              a1_interstate_number1,    a1_interstate_income1, 
//              a1_interstate_number2,    a1_interstate_income2, 
//              a1_local_number,          a1_local_income,
//              a1_other_number,          a1_other_income,
//              a1_switched_number,       a1_switched_income,  
//              auth_count_total,         auth_income_total, 
//              a1_avs_number,            a1_avs_income,  
//              load_filename
//            )
//            values
//            (
//              :LoadSec                        ,   -- hh_load_sec
//              'IH'                            ,   -- vendor_code
//              :mediaType                      ,   -- media_type
//              :totals.getPlanType()         ,   -- plan_type
//              0                               ,   -- bet_number
//              :counts[0]                    ,   -- fixed_auth_count
//              :fees  [0]                    ,   -- fixed_auth_fees
//              0                               ,   -- auth_950_count
//              0                               ,   -- auth_950_fees
//              0                               ,   -- above_count
//              0                               ,   -- above_fees
//              0                               ,   -- below_count
//              0                               ,   -- below_fees
//              0                               ,   -- interstate1_count
//              0                               ,   -- interstate1_fees
//              0                               ,   -- interstate2_count
//              0                               ,   -- interstate2_fees
//              :counts[1]                    ,   -- local_count
//              :fees  [1]                    ,   -- local_fees
//              :counts[2]                    ,   -- other_count
//              :fees  [2]                    ,   -- other_fees
//              :counts[3]                    ,   -- switched_count
//              :fees  [3]                    ,   -- switched_fees
//              :counts[0]                    ,   -- total_count
//              :fees  [0]                    ,   -- total_fees_due
//              :counts[4]                    ,   -- avs_count
//              :fees  [4]                    ,   -- avs_fees
//              :LoadFilename                       -- load_filename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_194 = totals.getPlanType();
 int __sJT_195 = counts[0];
 double __sJT_196 = fees  [0];
 int __sJT_197 = counts[1];
 double __sJT_198 = fees  [1];
 int __sJT_199 = counts[2];
 double __sJT_200 = fees  [2];
 int __sJT_201 = counts[3];
 double __sJT_202 = fees  [3];
 int __sJT_203 = counts[0];
 double __sJT_204 = fees  [0];
 int __sJT_205 = counts[4];
 double __sJT_206 = fees  [4];
   String theSqlTS = "insert into monthly_extract_ap  \n          (\n            hh_load_sec,\n            a1_vendor_code,a1_media_type,a1_plan_type,\n            a1_bet_table_id,\n            a1_number_fixed_auths,    a1_fixed_auths_income,\n            a1_950_number,            a1_950_income,\n            a1_above_floor_number,    a1_above_floor_income,\n            a1_below_floor_number,    a1_below_floor_income,\n            a1_interstate_number1,    a1_interstate_income1, \n            a1_interstate_number2,    a1_interstate_income2, \n            a1_local_number,          a1_local_income,\n            a1_other_number,          a1_other_income,\n            a1_switched_number,       a1_switched_income,  \n            auth_count_total,         auth_income_total, \n            a1_avs_number,            a1_avs_income,  \n            load_filename\n          )\n          values\n          (\n             :1                          ,   -- hh_load_sec\n            'IH'                            ,   -- vendor_code\n             :2                        ,   -- media_type\n             :3           ,   -- plan_type\n            0                               ,   -- bet_number\n             :4                      ,   -- fixed_auth_count\n             :5                      ,   -- fixed_auth_fees\n            0                               ,   -- auth_950_count\n            0                               ,   -- auth_950_fees\n            0                               ,   -- above_count\n            0                               ,   -- above_fees\n            0                               ,   -- below_count\n            0                               ,   -- below_fees\n            0                               ,   -- interstate1_count\n            0                               ,   -- interstate1_fees\n            0                               ,   -- interstate2_count\n            0                               ,   -- interstate2_fees\n             :6                      ,   -- local_count\n             :7                      ,   -- local_fees\n             :8                      ,   -- other_count\n             :9                      ,   -- other_fees\n             :10                      ,   -- switched_count\n             :11                      ,   -- switched_fees\n             :12                      ,   -- total_count\n             :13                      ,   -- total_fees_due\n             :14                      ,   -- avs_count\n             :15                      ,   -- avs_fees\n             :16                         -- load_filename\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   __sJT_st.setString(2,mediaType);
   __sJT_st.setString(3,__sJT_194);
   __sJT_st.setInt(4,__sJT_195);
   __sJT_st.setDouble(5,__sJT_196);
   __sJT_st.setInt(6,__sJT_197);
   __sJT_st.setDouble(7,__sJT_198);
   __sJT_st.setInt(8,__sJT_199);
   __sJT_st.setDouble(9,__sJT_200);
   __sJT_st.setInt(10,__sJT_201);
   __sJT_st.setDouble(11,__sJT_202);
   __sJT_st.setInt(12,__sJT_203);
   __sJT_st.setDouble(13,__sJT_204);
   __sJT_st.setInt(14,__sJT_205);
   __sJT_st.setDouble(15,__sJT_206);
   __sJT_st.setString(16,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1955^9*/
      }
      catch( Exception e )
      {
        logEntry( "storeAP( " + LoadFilename + "," + LoadSec + "," + MerchantId + ")",e.toString());
      }
    }
    
    protected void storeIcVolume( long loadSec, String loadFilename, HashMap betVolume )
    {
      StatementRecord         srec            = new StatementRecord();
      IcVolume                vol             = null;
  
      try
      {
        TreeSet sortedKeys = new TreeSet( betVolume.keySet() );
        for ( Iterator it = sortedKeys.iterator(); it.hasNext(); )
        {
          vol = (IcVolume)betVolume.get( (String)it.next() );
        
          if ( vol.VolumeCount != 0 )
          {
            srec.clear(); 
            srec.setLoadSec( loadSec );
            srec.setItemCat( "IC" );
            srec.setItemCount( vol.VolumeCount );
            srec.setItemAmount( vol.VolumeAmount );
            srec.setStatementMessage( vol.StatementMsg );
            srec.setFeeAmount( vol.FeesDue );
            srec.setLoadFilename( loadFilename );
        
            storeStatementRecord(srec);
          }          
        }        
      }
      catch( Exception e )
      {
        logEntry("storeIcVolume()",e.toString());
      }
    }
  
    protected void storeStatementRecord( StatementRecord rec )
    {
      try
      {
        if ( rec.getFeeAmount() != 0.0 )
        {
          // truncate statement messages longer than 74 characters
          String smsg = rec.getStatementMessage();
          if ( smsg != null && smsg.length() > 74 )
          {
            smsg = smsg.substring(0,74);
          }
      
          /*@lineinfo:generated-code*//*@lineinfo:2009^11*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_st  
//              (
//                hh_load_sec,
//                item_category,
//                st_number_of_items_on_stmt,
//                st_amount_of_item_on_stmt,
//                st_statement_desc,
//                st_fee_amount,
//                load_filename
//              )
//              values
//              (
//                :rec.getLoadSec(),
//                :rec.getItemCat(),
//                :rec.getItemCount(),
//                :rec.getItemAmount(),
//                :smsg,
//                :rec.getFeeAmount(),
//                :rec.getLoadFilename()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_207 = rec.getLoadSec();
 String __sJT_208 = rec.getItemCat();
 int __sJT_209 = rec.getItemCount();
 double __sJT_210 = rec.getItemAmount();
 double __sJT_211 = rec.getFeeAmount();
 String __sJT_212 = rec.getLoadFilename();
   String theSqlTS = "insert into monthly_extract_st  \n            (\n              hh_load_sec,\n              item_category,\n              st_number_of_items_on_stmt,\n              st_amount_of_item_on_stmt,\n              st_statement_desc,\n              st_fee_amount,\n              load_filename\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n               :5  ,\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_207);
   __sJT_st.setString(2,__sJT_208);
   __sJT_st.setInt(3,__sJT_209);
   __sJT_st.setDouble(4,__sJT_210);
   __sJT_st.setString(5,smsg);
   __sJT_st.setDouble(6,__sJT_211);
   __sJT_st.setString(7,__sJT_212);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2031^11*/
        }        
      }
      catch( Exception e )
      {
        logEntry("storeStatementRecord()",e.toString());
      }
    }
    
    public void updateTotals( long loadSec, long merchantId )
    {
      MerchantBillingElement  el          = null;
      ResultSetIterator       it          = null;
      long                    loadFileId  = 0L;
      ResultSet               resultSet   = null;
    
      try
      {
        int         salesCount      = 0;
        double      salesAmount     = 0.0;
        int         creditsCount    = 0;
        double      creditsAmount   = 0.0;
        int         cbCount         = 0;
        double      cbAmount        = 0.0;
        int         cashAdvCount    = 0;
        double      cashAdvAmount   = 0.0;
        double      discInc         = 0.0;
        double      icInc           = 0.0;
        double      authInc         = 0.0;
        double      debitInc        = 0.0;
        double      miscInc         = 0.0;
        double      planInc         = 0.0;
        double      discPaid        = 0.0;
        double      icPaid          = 0.0;
        double      totInc          = 0.0;
        double      totExp          = 0.0;
        double      icExp           = 0.0;
        /* Code modified by RS Generate ACH Income and discount */ 
        double      incAch          = 0.0;
        double      incAchDisc      = 0.0; 
        /* Code modified by RS for LCR changes */ 
        double      incPinlessDebit     = 0.0;
        double      incPinlessDebitPL   = 0.0;
        
        //@log.debug("Loading PL totals");
        /*@lineinfo:generated-code*//*@lineinfo:2076^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(case when pl.pl_plan_type like '%$' then 0 else 1 end
//                        * pl.pl_number_of_sales)        as sales_count,
//                    sum(case when pl.pl_plan_type like '%$' then 0 else 1 end
//                        * pl.pl_sales_amount)           as sales_amount,
//                    sum(pl.pl_number_of_credits)        as credits_count,
//                    sum(pl.pl_credits_amount)           as credits_amount,
//                    sum(pl.pl_number_of_chargeback)     as cb_count,
//                    sum(pl.pl_chargeback_amount)        as cb_amount,
//                    sum(pl.pl_correct_disc_amt)         as discount_inc,
//                    sum(case when pl.pl_plan_type like '%$' then 1 else 0 end
//                        * pl_number_of_sales)           as cash_adv_count,
//                    sum(case when pl.pl_plan_type like '%$' then 1 else 0 end
//                        * pl.pl_sales_amount)           as cash_adv_amount
//            from    monthly_extract_pl    pl
//            where   pl.hh_load_sec = :loadSec and pl.pl_plan_type not in ('VP','MP')                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(case when pl.pl_plan_type like '%$' then 0 else 1 end\n                      * pl.pl_number_of_sales)        as sales_count,\n                  sum(case when pl.pl_plan_type like '%$' then 0 else 1 end\n                      * pl.pl_sales_amount)           as sales_amount,\n                  sum(pl.pl_number_of_credits)        as credits_count,\n                  sum(pl.pl_credits_amount)           as credits_amount,\n                  sum(pl.pl_number_of_chargeback)     as cb_count,\n                  sum(pl.pl_chargeback_amount)        as cb_amount,\n                  sum(pl.pl_correct_disc_amt)         as discount_inc,\n                  sum(case when pl.pl_plan_type like '%$' then 1 else 0 end\n                      * pl_number_of_sales)           as cash_adv_count,\n                  sum(case when pl.pl_plan_type like '%$' then 1 else 0 end\n                      * pl.pl_sales_amount)           as cash_adv_amount\n          from    monthly_extract_pl    pl\n          where   pl.hh_load_sec =  :1   and pl.pl_plan_type not in ('VP','MP')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2093^9*/
        resultSet = it.getResultSet();
      
        if ( resultSet.next() )
        {
          salesCount    = resultSet.getInt   ("sales_count");
          salesAmount   = resultSet.getDouble("sales_amount");
          creditsCount  = resultSet.getInt   ("credits_count");
          creditsAmount = resultSet.getDouble("credits_amount");
          cbCount       = resultSet.getInt   ("cb_count");
          cbAmount      = resultSet.getDouble("cb_amount");
          discInc       = resultSet.getDouble("discount_inc");
          cashAdvCount  = resultSet.getInt   ("cash_adv_count");
          cashAdvAmount = resultSet.getDouble("cash_adv_amount");
        }
        resultSet.close();
        it.close();
      
        // check for minimum discount
        el = BillingData.findItemByType(103,null);
        
        // if the account is *not* closed OR completed at least one sale
        // AND has a minimum discount element then assess minimum discount
        if ( (!"C".equals(AccountStatus) || salesCount > 0) && el != null )
        {
          discInc = (discInc < el.getPerItem()) ? el.getPerItem() : discInc;
        }
      
        String icPassthrough = "N";
        String achCharge1 = mesConstants.ACH_MONTHLY_SERVICE_FEE;
        String achCharge2 = mesConstants.ACH_SMARTPAY_EXP_MONTHLY_FEE;
        String achCharge3 = mesConstants.ACH_HOST_PAY_MONTHLY_FEE;
        String achCharge4 = mesConstants.ACH_SMARTPAY_EXP_SETUP_FEE;
        String achFee1    = mesConstants.ACH_RETURN_REP_FEE;
        String achFee2    = mesConstants.ACH_UNAUTH_RETURN_REP_FEE;
        
        /*@lineinfo:generated-code*//*@lineinfo:2129^9*/

//  ************************************************************
//  #sql [Ctx] { select  upper(nvl(mf.debit_pass_through,'N'))
//                
//              from    mif   mf
//              where   mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(nvl(mf.debit_pass_through,'N'))\n               \n            from    mif   mf\n            where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   icPassthrough = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2135^9*/
             
        if ( "Y".equals(icPassthrough) ) {
            /*@lineinfo:generated-code*//*@lineinfo:2138^13*/

//  ************************************************************
//  #sql [Ctx] it = { select sum( case
//                                  when st.st_statement_desc not like 'DB%' and st.item_category = 'IC' then 1
//                                  else 0
//                              end
//                              * st.st_fee_amount )                 as ic_inc,
//                          sum( decode(st.item_category,
//                                      'AUTH',st.st_fee_amount,0) )  as auth_inc,
//                          sum( case
//                                  when st.st_statement_desc like 'DB%' and st.item_category = 'IC' then 1
//                                  when st.item_category = 'DEBIT' then 1
//                                  else 0
//                              end
//                              * st.st_fee_amount )                 as debit_inc,
//                          sum( decode(st.item_category,
//                                      'PLAN',st.st_fee_amount,0) )  as plan_inc,
//                          sum( case
//                                  when st.item_category like 'CG-%' then 1
//                                  when st.item_category in ('ASSOC','MISC','EQUIP') then 1
//                                  else 0
//                              end
//                              * st.st_fee_amount )                 as sys_gen_inc
//                  from    monthly_extract_st    st
//                  where   st.hh_load_sec = :loadSec
//                    and   st.st_statement_desc not in
//                        (upper(:achFee1) , 
//                        upper(:achFee2),
//                        upper(:achCharge1),
//                        upper(:achCharge2),
//                        upper(:achCharge3),
//                        upper(:achCharge4))
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sum( case\n                                when st.st_statement_desc not like 'DB%' and st.item_category = 'IC' then 1\n                                else 0\n                            end\n                            * st.st_fee_amount )                 as ic_inc,\n                        sum( decode(st.item_category,\n                                    'AUTH',st.st_fee_amount,0) )  as auth_inc,\n                        sum( case\n                                when st.st_statement_desc like 'DB%' and st.item_category = 'IC' then 1\n                                when st.item_category = 'DEBIT' then 1\n                                else 0\n                            end\n                            * st.st_fee_amount )                 as debit_inc,\n                        sum( decode(st.item_category,\n                                    'PLAN',st.st_fee_amount,0) )  as plan_inc,\n                        sum( case\n                                when st.item_category like 'CG-%' then 1\n                                when st.item_category in ('ASSOC','MISC','EQUIP') then 1\n                                else 0\n                            end\n                            * st.st_fee_amount )                 as sys_gen_inc\n                from    monthly_extract_st    st\n                where   st.hh_load_sec =  :1  \n                  and   st.st_statement_desc not in\n                      (upper( :2  ) , \n                      upper( :3  ),\n                      upper( :4  ),\n                      upper( :5  ),\n                      upper( :6  ),\n                      upper( :7  ))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,achFee1);
   __sJT_st.setString(3,achFee2);
   __sJT_st.setString(4,achCharge1);
   __sJT_st.setString(5,achCharge2);
   __sJT_st.setString(6,achCharge3);
   __sJT_st.setString(7,achCharge4);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2170^17*/
        } else {
        //@log.debug("Loading ST totals");
        /*@lineinfo:generated-code*//*@lineinfo:2173^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum( decode(st.item_category,
//                                'IC',st.st_fee_amount,0) )    as ic_inc,
//                    sum( decode(st.item_category,
//                                'AUTH',st.st_fee_amount,0) )  as auth_inc,
//                    sum( decode(st.item_category,
//                                'DEBIT',st.st_fee_amount,0) ) as debit_inc,
//                    sum( decode(st.item_category,
//                                'PLAN',st.st_fee_amount,0) )  as plan_inc,
//                    sum( case 
//                           when st.item_category like 'CG-%' then 1
//                           when st.item_category in ('ASSOC','MISC','EQUIP') then 1
//                           else 0
//                         end
//                         * st.st_fee_amount )                 as sys_gen_inc
//            from    monthly_extract_st    st
//            where   st.hh_load_sec = :loadSec   
//            and     st_statement_desc not in
//                        (upper(:achFee1), 
//                        upper(:achFee2),
//                        upper(:achCharge1),
//                        upper(:achCharge2),
//                        upper(:achCharge3),
//                        upper(:achCharge4))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum( decode(st.item_category,\n                              'IC',st.st_fee_amount,0) )    as ic_inc,\n                  sum( decode(st.item_category,\n                              'AUTH',st.st_fee_amount,0) )  as auth_inc,\n                  sum( decode(st.item_category,\n                              'DEBIT',st.st_fee_amount,0) ) as debit_inc,\n                  sum( decode(st.item_category,\n                              'PLAN',st.st_fee_amount,0) )  as plan_inc,\n                  sum( case \n                         when st.item_category like 'CG-%' then 1\n                         when st.item_category in ('ASSOC','MISC','EQUIP') then 1\n                         else 0\n                       end\n                       * st.st_fee_amount )                 as sys_gen_inc\n          from    monthly_extract_st    st\n          where   st.hh_load_sec =  :1     \n          and     st_statement_desc not in\n                      (upper( :2  ), \n                      upper( :3  ),\n                      upper( :4  ),\n                      upper( :5  ),\n                      upper( :6  ),\n                      upper( :7  ))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,achFee1);
   __sJT_st.setString(3,achFee2);
   __sJT_st.setString(4,achCharge1);
   __sJT_st.setString(5,achCharge2);
   __sJT_st.setString(6,achCharge3);
   __sJT_st.setString(7,achCharge4);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2198^9*/
        }
        resultSet = it.getResultSet();
      
        if ( resultSet.next() )
        {
          icInc     = resultSet.getDouble("ic_inc");
          authInc   = resultSet.getDouble("auth_inc");
          debitInc  = resultSet.getDouble("debit_inc");
          planInc   = resultSet.getDouble("plan_inc");
          miscInc   = resultSet.getDouble("sys_gen_inc");
        }
        resultSet.close();
        it.close();
      
        //@log.debug("Loading discount paid");
        /*@lineinfo:generated-code*//*@lineinfo:2214^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum( decode(mbe.item_category,'DISC',sm.fees_paid,0) )  as discount_paid,
//                    sum( decode(mbe.item_category,'IC'  ,sm.fees_paid,0) )  as ic_paid
//            from    monthly_extract_gn    gn,
//                    mbs_daily_summary     sm,
//                    mbs_elements          mbe
//            where   gn.hh_load_sec = :loadSec
//                    and sm.me_load_file_id = gn.load_file_id
//                    and sm.merchant_number = :merchantId
//                    and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type
//                    and mbe.item_category in ('IC','DISC')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum( decode(mbe.item_category,'DISC',sm.fees_paid,0) )  as discount_paid,\n                  sum( decode(mbe.item_category,'IC'  ,sm.fees_paid,0) )  as ic_paid\n          from    monthly_extract_gn    gn,\n                  mbs_daily_summary     sm,\n                  mbs_elements          mbe\n          where   gn.hh_load_sec =  :1  \n                  and sm.me_load_file_id = gn.load_file_id\n                  and sm.merchant_number =  :2  \n                  and nvl(mbe.data_item_type,mbe.item_type) = sm.item_type\n                  and mbe.item_category in ('IC','DISC')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2226^9*/
        resultSet = it.getResultSet();
      
        if ( resultSet.next() )
        {
          discPaid  = resultSet.getDouble("discount_paid");
          icPaid    = resultSet.getDouble("ic_paid");
        }
        resultSet.close();
        it.close();
      
        //@log.debug("Loading interchange expense");
        /*@lineinfo:generated-code*//*@lineinfo:2238^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum(nvl(sm.expense,0)), 0 )    
//            
//            from    monthly_extract_gn    gn,
//                    mbs_daily_summary     sm
//            where   gn.hh_load_sec = :loadSec
//                    and sm.me_load_file_id = gn.load_file_id
//                    and sm.merchant_number = :merchantId
//                    and sm.item_type in (111,228)
//                    and sm.item_subclass not in ('EB','DB')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum(nvl(sm.expense,0)), 0 )    \n           \n          from    monthly_extract_gn    gn,\n                  mbs_daily_summary     sm\n          where   gn.hh_load_sec =  :1  \n                  and sm.me_load_file_id = gn.load_file_id\n                  and sm.merchant_number =  :2  \n                  and sm.item_type in (111,228)\n                  and sm.item_subclass not in ('EB','DB')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   icExp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2249^9*/

        /*
        *  Code modified by RS for ACHPS partner billing implementation.
        *  We are considering the statement description exactly using the same description as stated in below query.
        *  Joining with PL table is not effective as it is fetching all the rows associated with load sec id
        *  because plan type is missing in monthly_extract_st table. It is filtering from PL table but extracting
        *  all the fees  including 'AC' type from ST table. So we are considering the statement description from ST table.
        */
        /*@lineinfo:generated-code*//*@lineinfo:2258^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(st.st_fee_amount)  as tot_inc_ach
//             from    monthly_extract_st    st
//             where   st.hh_load_sec = :loadSec
//             and   st.st_statement_desc in
//                        (upper(:achFee1), 
//                        upper(:achFee2),
//                        upper(:achCharge1),
//                        upper(:achCharge2),
//                        upper(:achCharge3),
//                        upper(:achCharge4))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(st.st_fee_amount)  as tot_inc_ach\n           from    monthly_extract_st    st\n           where   st.hh_load_sec =  :1  \n           and   st.st_statement_desc in\n                      (upper( :2  ), \n                      upper( :3  ),\n                      upper( :4  ),\n                      upper( :5  ),\n                      upper( :6  ),\n                      upper( :7  ))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,achFee1);
   __sJT_st.setString(3,achFee2);
   __sJT_st.setString(4,achCharge1);
   __sJT_st.setString(5,achCharge2);
   __sJT_st.setString(6,achCharge3);
   __sJT_st.setString(7,achCharge4);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2270^9*/

        resultSet = it.getResultSet();

        if ( resultSet.next() ) {
             incAch = resultSet.getDouble("tot_inc_ach");
        }
        resultSet.close();
        it.close();


        /*@lineinfo:generated-code*//*@lineinfo:2281^9*/

//  ************************************************************
//  #sql [Ctx] it = { select pl.pl_correct_disc_amt  as ach_discount
//              from  monthly_extract_pl    pl
//              where  pl.hh_load_sec = :loadSec    
//              and  pl.pl_plan_type = 'AC'            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select pl.pl_correct_disc_amt  as ach_discount\n            from  monthly_extract_pl    pl\n            where  pl.hh_load_sec =  :1      \n            and  pl.pl_plan_type = 'AC'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2287^9*/

        resultSet = it.getResultSet();

        if ( resultSet.next() ) {
            incAchDisc = resultSet.getDouble("ach_discount");
        }
        resultSet.close();
        it.close();
         
        totInc = discInc + icInc + authInc + debitInc + planInc + miscInc + incAch ;

        /*@lineinfo:generated-code*//*@lineinfo:2299^9*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_gn 
//            set     t1_tot_number_of_sales      = :salesCount,
//                    t1_tot_amount_of_sales      = :salesAmount,
//                    t1_tot_number_of_credits    = :creditsCount,
//                    t1_tot_amount_of_credits    = :creditsAmount,
//                    t1_tot_number_of_chargeback = :cbCount,
//                    t1_tot_amount_of_chargeback = :cbAmount,
//                    t1_tot_number_of_cash_advc  = :cashAdvCount,
//                    t1_tot_amount_of_cash_advc  = :cashAdvAmount,
//                    t1_tot_inc_ind_plans        = :planInc,
//                    t1_tot_inc_interchange      = :icInc,
//                    t1_tot_inc_authorization    = :authInc,
//                    t1_tot_inc_capture          = 0,
//                    t1_tot_inc_sys_generated    = :miscInc,
//                    /* Code modified by RS for ACHPS partner billing implementation.
//                       populate new columns for ACH fees and discount */
//                    t1_tot_inc_ach              = :incAch,
//                    t1_tot_inc_discount_ach     = :incAchDisc,
//                    t1_tot_inc_debit_networks   = :debitInc,
//                    t1_tot_calculated_discount  = :discInc,
//                    t1_tot_discount_paid        = :discPaid,
//                    t1_tot_interchange_paid     = :icPaid,
//                    t1_tot_exp_ind_plans        = :planInc,
//                    t1_tot_exp_interchange      = :icExp,
//                    t1_tot_exp_authorization    = 0,
//                    t1_tot_exp_capture          = 0,
//                    t1_tot_exp_sys_generated    = 0,
//                    t1_tot_exp_debit_networks   = 0,
//                    t1_tot_income               = :totInc,
//                    t1_tot_expense              = :totExp,
//                    t1_tot_net_profit_loss      = (:totInc-:totExp),
//                    t1_tot_num_sales_adj        = 0,
//                    t1_tot_amt_sales_adj        = 0,
//                    t1_tot_num_credits_adj      = 0,
//                    t1_tot_amt_credits_adj      = 0,
//                    t1_tot_num_cash_adj         = 0,
//                    t1_tot_amt_cash_adj         = 0
//            where   hh_load_sec = :loadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_gn \n          set     t1_tot_number_of_sales      =  :1  ,\n                  t1_tot_amount_of_sales      =  :2  ,\n                  t1_tot_number_of_credits    =  :3  ,\n                  t1_tot_amount_of_credits    =  :4  ,\n                  t1_tot_number_of_chargeback =  :5  ,\n                  t1_tot_amount_of_chargeback =  :6  ,\n                  t1_tot_number_of_cash_advc  =  :7  ,\n                  t1_tot_amount_of_cash_advc  =  :8  ,\n                  t1_tot_inc_ind_plans        =  :9  ,\n                  t1_tot_inc_interchange      =  :10  ,\n                  t1_tot_inc_authorization    =  :11  ,\n                  t1_tot_inc_capture          = 0,\n                  t1_tot_inc_sys_generated    =  :12  ,\n                  /* Code modified by RS for ACHPS partner billing implementation.\n                     populate new columns for ACH fees and discount */\n                  t1_tot_inc_ach              =  :13  ,\n                  t1_tot_inc_discount_ach     =  :14  ,\n                  t1_tot_inc_debit_networks   =  :15  ,\n                  t1_tot_calculated_discount  =  :16  ,\n                  t1_tot_discount_paid        =  :17  ,\n                  t1_tot_interchange_paid     =  :18  ,\n                  t1_tot_exp_ind_plans        =  :19  ,\n                  t1_tot_exp_interchange      =  :20  ,\n                  t1_tot_exp_authorization    = 0,\n                  t1_tot_exp_capture          = 0,\n                  t1_tot_exp_sys_generated    = 0,\n                  t1_tot_exp_debit_networks   = 0,\n                  t1_tot_income               =  :21  ,\n                  t1_tot_expense              =  :22  ,\n                  t1_tot_net_profit_loss      = ( :23  - :24  ),\n                  t1_tot_num_sales_adj        = 0,\n                  t1_tot_amt_sales_adj        = 0,\n                  t1_tot_num_credits_adj      = 0,\n                  t1_tot_amt_credits_adj      = 0,\n                  t1_tot_num_cash_adj         = 0,\n                  t1_tot_amt_cash_adj         = 0\n          where   hh_load_sec =  :25 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,salesCount);
   __sJT_st.setDouble(2,salesAmount);
   __sJT_st.setInt(3,creditsCount);
   __sJT_st.setDouble(4,creditsAmount);
   __sJT_st.setInt(5,cbCount);
   __sJT_st.setDouble(6,cbAmount);
   __sJT_st.setInt(7,cashAdvCount);
   __sJT_st.setDouble(8,cashAdvAmount);
   __sJT_st.setDouble(9,planInc);
   __sJT_st.setDouble(10,icInc);
   __sJT_st.setDouble(11,authInc);
   __sJT_st.setDouble(12,miscInc);
   __sJT_st.setDouble(13,incAch);
   __sJT_st.setDouble(14,incAchDisc);
   __sJT_st.setDouble(15,debitInc);
   __sJT_st.setDouble(16,discInc);
   __sJT_st.setDouble(17,discPaid);
   __sJT_st.setDouble(18,icPaid);
   __sJT_st.setDouble(19,planInc);
   __sJT_st.setDouble(20,icExp);
   __sJT_st.setDouble(21,totInc);
   __sJT_st.setDouble(22,totExp);
   __sJT_st.setDouble(23,totInc);
   __sJT_st.setDouble(24,totExp);
   __sJT_st.setLong(25,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2339^9*/
        
        /*
       	 * Code modified by RS for PINLESS DEBIT NETWORKS billing implementation for LCR
       	 */
       	 /*@lineinfo:generated-code*//*@lineinfo:2344^10*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(pl.PL_CORRECT_DISC_AMT)  as PL_CORRECT_DISC_AMT
//            from    monthly_extract_pl  pl
//            where   pl.hh_load_sec = :loadSec
//            and   pl.PL_PLAN_TYPE in('VP','MP')
//         	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(pl.PL_CORRECT_DISC_AMT)  as PL_CORRECT_DISC_AMT\n          from    monthly_extract_pl  pl\n          where   pl.hh_load_sec =  :1  \n          and   pl.PL_PLAN_TYPE in('VP','MP')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2350^9*/

        resultSet = it.getResultSet();

        if ( resultSet.next() )
        {
        	incPinlessDebitPL = resultSet.getDouble("PL_CORRECT_DISC_AMT");
        }
        resultSet.close();
        it.close();
       	
       	/*@lineinfo:generated-code*//*@lineinfo:2361^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(sum(st.st_fee_amount),0)  as tot_inc_pinlessdebit
//            from    monthly_extract_st  st
//            where   st.hh_load_sec = :loadSec
//            and   (st.st_statement_desc like 'VP%' OR st.st_statement_desc like 'MP%')
//         	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(sum(st.st_fee_amount),0)  as tot_inc_pinlessdebit\n          from    monthly_extract_st  st\n          where   st.hh_load_sec =  :1  \n          and   (st.st_statement_desc like 'VP%' OR st.st_statement_desc like 'MP%')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2367^9*/

        resultSet = it.getResultSet();

        if ( resultSet.next() )
        {
        	incPinlessDebit = resultSet.getDouble("tot_inc_pinlessdebit");
        }
        resultSet.close();
        it.close();

        incPinlessDebit = incPinlessDebit+incPinlessDebitPL;
        /*@lineinfo:generated-code*//*@lineinfo:2379^9*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_gn 
//            set     T1_TOT_INC_PINLESSDEBIT      = :incPinlessDebit
//            where   hh_load_sec = :loadSec
//         	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_gn \n          set     T1_TOT_INC_PINLESSDEBIT      =  :1  \n          where   hh_load_sec =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,incPinlessDebit);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2384^9*/

      }
      catch( Exception e )
      {
        logEntry("updateTotals(" + loadSec + "," + merchantId + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }        
    }        
  }
  
  protected class ReloadJob
    extends SQLJConnectionBase
    implements Runnable
  {
    private       long      MerchantId          = 0L;
    private       String    MerchantStatus      = null;
    private       Date      ActiveDate          = null;
    private       Date      MonthEndDate        = null;
    private       long      LoadSec             = 0L;
    private       int       BankNumber          = 0;
    private       String    LoadFilename        = null;
    private       String    DataType            = null;
    
  
    public ReloadJob( String dataType, ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId          = resultSet.getLong("merchant_number");
      MerchantStatus      = resultSet.getString("merchant_status");
      ActiveDate          = resultSet.getDate("active_date");
      MonthEndDate        = resultSet.getDate("month_end_date");
      LoadSec             = resultSet.getLong("load_sec");
      LoadFilename        = resultSet.getString("load_filename");
      BankNumber          = resultSet.getInt("bank_number");
      DataType            = dataType;
    }
    
    public void run()
    {
      SummaryJob          job               = new SummaryJob();
      
      try
      {
        connect(true);
        
        job.connect(true);
    
        // initialize the summary job and run it for this merchant
        job.init(MerchantId,MerchantStatus,ActiveDate,MonthEndDate,LoadSec,LoadFilename);
    
        System.out.println("Reloading " + DataType + " for " + MerchantId + " " + ActiveDate + "(" + LoadSec + ")" );
    
        if ( "NEW".equals(DataType) )
        {
          job.run();
        }
        else if ( "ALL".equals(DataType) )
        {
          // delete all the data in the monthly_extract_xx tables
          // but do not reset the entries in mbs_daily_summary
          MbsTools tools = new MbsTools();
          tools.connect();
          tools.resetMonthEnd(BankNumber,MerchantId,ActiveDate,false); 
          tools.cleanUp();
          job.run();
        }
        else if ( "IC".equals(DataType) )
        {
          // remove existing interchange entries from the statements table
          /*@lineinfo:generated-code*//*@lineinfo:2457^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_st    st
//              where   st.hh_load_sec = :LoadSec
//                      and st.item_category = 'IC'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_st    st\n            where   st.hh_load_sec =  :1  \n                    and st.item_category = 'IC'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2463^11*/
          job.loadIC( LoadFilename, LoadSec, MerchantId, ActiveDate );  // interchange
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }             
        else if ( "PL".equals(DataType) )
        {
          // remove existing interchange entries from the statements table
          /*@lineinfo:generated-code*//*@lineinfo:2470^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_pl    pl
//              where   pl.hh_load_sec = :LoadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_pl    pl\n            where   pl.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2475^11*/
    
          job.setBillingData(BillingDb.loadMerchantBilling(MerchantId,MonthEndDate));
          job.loadPL( LoadFilename, LoadSec, MerchantId );  // plan data
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }
        else if ( "AP".equals(DataType) )
        {
          // remove existing entries from the AP and ST tables
          /*@lineinfo:generated-code*//*@lineinfo:2484^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_ap    ap
//              where   ap.hh_load_sec = :LoadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_ap    ap\n            where   ap.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2489^11*/
      
          /*@lineinfo:generated-code*//*@lineinfo:2491^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_st    st
//              where   st.hh_load_sec = :LoadSec
//                      and st.item_category = 'AUTH'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_st    st\n            where   st.hh_load_sec =  :1  \n                    and st.item_category = 'AUTH'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2497^11*/
      
          job.setBillingData(BillingDb.loadMerchantBilling(MerchantId,MonthEndDate));
          job.loadAP( LoadFilename, LoadSec, MerchantId );  // plan data
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }
        else if ( "DN".equals(DataType) )
        {
          // remove existing entries from the DN and ST tables
          /*@lineinfo:generated-code*//*@lineinfo:2506^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_dn    dn
//              where   dn.hh_load_sec = :LoadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_dn    dn\n            where   dn.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2511^11*/
      
          /*@lineinfo:generated-code*//*@lineinfo:2513^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_st    st
//              where   st.hh_load_sec = :LoadSec
//                      and st.item_category = 'DEBIT'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_st    st\n            where   st.hh_load_sec =  :1  \n                    and st.item_category = 'DEBIT'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2519^11*/
          job.setBillingData(BillingDb.loadMerchantBilling(MerchantId,MonthEndDate));
          job.loadDN( LoadFilename, LoadSec, MerchantId );  // plan data
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }
        else if ( "CG".equals(DataType) || "ST".equals(DataType) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2526^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_st    st
//              where   st.hh_load_sec = :LoadSec
//                      and st.item_category in ( 'MISC', 'ASSOC','PLAN' )                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_st    st\n            where   st.hh_load_sec =  :1  \n                    and st.item_category in ( 'MISC', 'ASSOC','PLAN' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2532^11*/
      
          if ( "CG".equals(DataType) ) 
          {
            // remove the equipment statement records from the ST table
            /*@lineinfo:generated-code*//*@lineinfo:2537^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    monthly_extract_st    st
//                where   st.hh_load_sec = :LoadSec
//                        and ( st.item_category = 'EQUIP'
//                              or st.item_category like 'CG-%' )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    monthly_extract_st    st\n              where   st.hh_load_sec =  :1  \n                      and ( st.item_category = 'EQUIP'\n                            or st.item_category like 'CG-%' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2544^13*/
      
            // clear the equipment charge records
            /*@lineinfo:generated-code*//*@lineinfo:2547^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    monthly_extract_cg    cg
//                where   cg.hh_load_sec = :LoadSec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    monthly_extract_cg    cg\n              where   cg.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2552^13*/
          }
    
          job.setBillingData(BillingDb.loadMerchantBilling(MerchantId,MonthEndDate));
          if ( "CG".equals(DataType) ) 
          {
            job.loadCG( LoadFilename, LoadSec, MerchantId, ActiveDate );  // export charge records again
          }
          job.loadST( LoadFilename, LoadSec, MerchantId );  // misc data
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }
        else if ( "TOTALS".equals(DataType) )
        {
          job.updateTotals(LoadSec,MerchantId); // correct totals in GN table
        }
      }
      catch( Exception e )
      {
        logEntry("reload(" + DataType + "," + MerchantId + "," + ActiveDate + ")",e.toString());
      }
      finally
      {
        cleanUp();
        try{ job.cleanUp(); } catch( Exception ee ) {}
      }
    }
  }
  
  protected static class AuthTotals
  {
    protected   int[]           ItemCount         = new int[6];
    protected   double[]        FeesDue           = new double[6];
    protected   String          PlanType          = null;
    protected   int             TotalItemCount    = 0;
    protected   double          TotalFeesDue      = 0.0;
    
    public AuthTotals( String planType )
    {
      PlanType = planType;
    }
    
    public void add( int type, int count, double feesDue )
    {
      ItemCount[type] += count;
      FeesDue[type]   += feesDue;
    }
    
    public void addTotals( int count, double feesDue )
    {
      TotalItemCount  += count;
      TotalFeesDue    += feesDue;
    }
    
    public void addAuths   (int count, double feesDue) { add(0,count,feesDue); }
    public void addAru     (int count, double feesDue) { add(1,count,feesDue); }
    public void addAvs     (int count, double feesDue) { add(2,count,feesDue); }
    public void addIntlPG  (int count, double feesDue) { add(3,count,feesDue); }
    public void add3dSecure(int count, double feesDue) { add(4,count,feesDue); }
    public void addPG      (int count, double feesDue) { add(5,count,feesDue); }
    
    public int    getItemCount(int type){ return( ItemCount[type] );  }
    public double getFeesDue  (int type){ return( FeesDue[type] );    }
    
    public int    getAuthCount()        { return( getItemCount(0) );  }
    public double getAuthFeesDue()      { return( getFeesDue(0)   );  }
    public int    getAruCount()         { return( getItemCount(1) );  }
    public double getAruFeesDue()       { return( getFeesDue(1)   );  }
    public int    getAvsCount()         { return( getItemCount(2) );  }
    public double getAvsFeesDue()       { return( getFeesDue(2)   );  }
    public int    getIntlPGCount()      { return( getItemCount(3) );  }
    public double getIntlPGFeesDue()    { return( getFeesDue(3)   );  }
    public int    get3dSecureCount()    { return( getItemCount(4) );  }
    public double get3dSecureFeesDue()  { return( getFeesDue(4)   );  }
    public int    getPGCount()          { return( getItemCount(5) );  }
    public double getPGFeesDue()        { return( getFeesDue(5)   );  }
    public String getPlanType()         { return( PlanType        );  }
    
    public int    getTotalItemCount()   { return( TotalItemCount  );  }
    public double getTotalFeesDue()     { return( TotalFeesDue    );  }
  }
  
  public static class StatementRecord
  {
    protected     double      FeeAmount           = 0.0;
    protected     double      ItemAmount          = 0.0;
    protected     int         ItemCount           = 0;
    protected     String      ItemCat             = null;
    protected     String      LoadFilename        = null;
    protected     long        LoadSec             = 0L;
    protected     String      StatementMessage    = null;
    protected     int         ItemType            = 0;
    
    public StatementRecord( )
    {
    }
  
    public StatementRecord( long loadSec )
    {
      setLoadSec(loadSec);
    }
    
    public StatementRecord( long loadSec, String loadFilename )
    {
      setLoadSec(loadSec);
      setLoadFilename(loadFilename);
    }
    
    public long     getLoadSec()                      { return( LoadSec );    }
    public void     setLoadSec(long value)            { LoadSec = value;      }

    public int      getItemCount()                    { return( ItemCount );  }
    public void     setItemCount(int value)           { ItemCount = value;    }

    public double   getItemAmount()                   { return( ItemAmount ); }
    public void     setItemAmount(double value)       { ItemAmount = value;   }
    
    public String   getItemCat()                      { return( ItemCat );    }
    public void     setItemCat(String value)          { ItemCat = value;      }
    
    public String   getStatementMessage()             { return( StatementMessage ); }
    public void     setStatementMessage(String value) { StatementMessage = value;   }

    public double   getFeeAmount()                    { return( FeeAmount );  }
    public void     setFeeAmount(double value)        { FeeAmount = value;    }

    public String   getLoadFilename()                 { return( LoadFilename ); }
    public void     setLoadFilename(String value)     { LoadFilename = value;   }
    
    public void     addItemCount(int value)           { ItemCount   += value; }
    public void     addItemAmount(double value)       { ItemAmount  += value; }
    public void     addFeeAmount(double value)        { FeeAmount   += value; }
    
    public int      getItemType()                     { return ItemType; }
    public void     setItemType(int itemType)         { ItemType = itemType; }
	

	public void clear()
    {
      FeeAmount           = 0.0;    
      ItemAmount          = 0.0;    
      ItemCount           = 0;    
      ItemCat             = null;
      LoadFilename        = null;    
      LoadSec             = 0L;
      ItemType            = 0;
      StatementMessage    = null;    
    }
    
    public String toString() {
    
      return "StatementRecord [ "
        + "LoadSec: " + LoadSec
        + ", LoadFilename: " + LoadFilename
        + ", ItemCat: " + ItemCat
        + ", StatementMessage: " + StatementMessage
        + ", ItemCount: " + ItemCount
        + ", ItemAmount: " + ItemAmount
        + ", FeeAmount: " + FeeAmount
        + " ]";
    }
  }
  
  public MonthlyBillingSummary( )
  {
    PropertiesFilename = "monthly-billing.properties";
  }
  
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    Date                  activeDate      = null;
    Calendar              cal             = Calendar.getInstance();
    ResultSetIterator     it              = null;
    long                  merchantId      = 0L;
    List                  merchantList    = new ArrayList();
    long                  loadFileId      = 0L;
    String                loadFilename    = null;
    Date                  monthEndDate    = null;
    ThreadPool            pool            = null;
    ResultSet             resultSet       = null;
    int                   updateCount     = 0;
    String[]              values          = null;
    int                   sequenceId      = 0;
    long                  recId           = 0;
    long                  lcrRecId           = 0;
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      int bankNumber = Integer.parseInt( getEventArg(0) );
      
      /* RS Modification of ACH PS */
      /* Check the monthly_billing_process table to ensure Billing Summary job is completed */
      /*@lineinfo:generated-code*//*@lineinfo:2744^7*/

//  ************************************************************
//  #sql [Ctx] { select nvl(MAX(rec_id),0)  from monthly_billing_process where process_type=1 and process_sequence=0 and to_number(substr(load_filename,6,4)) = :bankNumber and substr(load_filename,0,3) = 'ach'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(MAX(rec_id),0)   from monthly_billing_process where process_type=1 and process_sequence=0 and to_number(substr(load_filename,6,4)) =  :1   and substr(load_filename,0,3) = 'ach'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2747^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2749^7*/

//  ************************************************************
//  #sql [Ctx] { select nvl(MAX(rec_id),0)  from monthly_billing_process where process_type=1 and process_sequence=0 and to_number(substr(load_filename,4,4)) = :bankNumber and substr(load_filename,0,3) = 'lcr'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(MAX(rec_id),0)   from monthly_billing_process where process_type=1 and process_sequence=0 and to_number(substr(load_filename,4,4)) =  :1   and substr(load_filename,0,3) = 'lcr'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lcrRecId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2752^7*/
      
      if(recId == 0 || lcrRecId==0){
        log.debug("MonthlyBillingSummary Job already executed for this month for the Bank Number :" + bankNumber + " so, exiting MonthlyBillingSummary Job");
        return ( false );
      } else {
        log.debug("Executing MonthlyBillingSummary Job for the Bank Number :" + bankNumber );
      }
      
      //achps sequence update
      /*@lineinfo:generated-code*//*@lineinfo:2762^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_prof_process_sequence.nextval 
//          from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_prof_process_sequence.nextval  \n        from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2766^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:2768^7*/

//  ************************************************************
//  #sql [Ctx] { update monthly_billing_process set process_begin_date = SYSDATE, process_sequence=:sequenceId where rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update monthly_billing_process set process_begin_date = SYSDATE, process_sequence= :1   where rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"48com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2771^7*/
      
      //lcr sequence update
      
      /*@lineinfo:generated-code*//*@lineinfo:2775^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_prof_process_sequence.nextval 
//          from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_prof_process_sequence.nextval  \n        from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2779^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:2781^7*/

//  ************************************************************
//  #sql [Ctx] { update monthly_billing_process set process_begin_date = SYSDATE, process_sequence=:sequenceId where rec_id = :lcrRecId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update monthly_billing_process set process_begin_date = SYSDATE, process_sequence= :1   where rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"50com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   __sJT_st.setLong(2,lcrRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2784^7*/

      if ( TestFileDate == null )
      {
        if ( cal.get(Calendar.DAY_OF_MONTH) <= 15 )
        {
          // default is the previous calendar month
          cal.add(Calendar.MONTH,-1);
        }
        cal.set(Calendar.DAY_OF_MONTH,1);
        activeDate = new java.sql.Date( cal.getTime().getTime() );
      }
      else    // rebuild file using user date
      {
        activeDate    = TestFileDate;
        loadFilename  = TestFilename;
      }
      
      if ( loadFilename == null )
      {
        // if the filename was not provided, generate a new one
        loadFilename = generateFilename("mbs_ext" + bankNumber);
      }
      loadFileId = SettlementDb.loadFilenameToLoadFileId(loadFilename);
      
      // MBS uses calendar month as the billing month
      cal.setTime(activeDate);
      cal.add(Calendar.MONTH,1);
      cal.set(Calendar.DAY_OF_MONTH,1);
      cal.add(Calendar.DAY_OF_MONTH,-1);
      monthEndDate = new java.sql.Date( cal.getTime().getTime() );
      
      log.debug("Loading " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") + " month end");
            
      /*@lineinfo:generated-code*//*@lineinfo:2818^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number || '-' || 
//                  case
//                    when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then
//                      case
//                        when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') > :activeDate then 'O'
//                        else 'C'
//                      end
//                    when nvl(mf.dmacctst,'O') = 'Z'
//                      then
//                        case
//                          when exists
//                            (select 1
//                            from mif_changes
//                            where merchant_number = mf.merchant_number
//                            and change_details like '%merchantStatus [O]=>[Z]%'
//                            and trunc(date_changed, 'MM') = trunc(TO_DATE(:activeDate,'DD-MON-YY'), 'MM')
//                            )
//                          then 'O'
//                          else 'C'
//                      end
//                    else decode( mf.dmacctst,
//                                 null,'O',
//                                 mf.dmacctst )  
//                  end                             as mid_status
//          from    mif           mf
//          where   mf.bank_number = :bankNumber
//                  and nvl(mf.processor_id,0) = 1      -- mes only
//                  --and mf.merchant_number in (select merchant_number from jfirman.may_recover_merchants)
//                  and 
//                  ( 
//                    :TestNodeId = 0 or 
//                    mf.merchant_number in 
//                    ( 
//                      select  gm.merchant_number 
//                      from    organization    o, 
//                              group_merchant  gm 
//                      where   o.org_group = :TestNodeId 
//                              and gm.org_num = o.org_num 
//                    )
//                  )
//          order by mf.merchant_number                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number || '-' || \n                case\n                  when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then\n                    case\n                      when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') >  :1   then 'O'\n                      else 'C'\n                    end\n                  when nvl(mf.dmacctst,'O') = 'Z'\n                    then\n                      case\n                        when exists\n                          (select 1\n                          from mif_changes\n                          where merchant_number = mf.merchant_number\n                          and change_details like '%merchantStatus [O]=>[Z]%'\n                          and trunc(date_changed, 'MM') = trunc(TO_DATE( :2  ,'DD-MON-YY'), 'MM')\n                          )\n                        then 'O'\n                        else 'C'\n                    end\n                  else decode( mf.dmacctst,\n                               null,'O',\n                               mf.dmacctst )  \n                end                             as mid_status\n        from    mif           mf\n        where   mf.bank_number =  :3  \n                and nvl(mf.processor_id,0) = 1      -- mes only\n                --and mf.merchant_number in (select merchant_number from jfirman.may_recover_merchants)\n                and \n                ( \n                   :4   = 0 or \n                  mf.merchant_number in \n                  ( \n                    select  gm.merchant_number \n                    from    organization    o, \n                            group_merchant  gm \n                    where   o.org_group =  :5   \n                            and gm.org_num = o.org_num \n                  )\n                )\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setLong(4,TestNodeId);
   __sJT_st.setLong(5,TestNodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"51com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2861^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        merchantList.add( resultSet.getString("mid_status") );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < merchantList.size(); ++i )
      {
        if ( pool == null )
        {
          pool = new ThreadPool(7);
        }
        
        values      = ((String)merchantList.get(i)).split("-");
        merchantId  = Long.parseLong( values[0] );
        
        // skip masked merchant account numbers
        if ( !includeMid(merchantId,activeDate) ) continue;
        
        /*@lineinfo:generated-code*//*@lineinfo:2884^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_daily_summary   sm
//            set     sm.me_load_filename = :loadFilename,
//                    sm.me_load_file_id  = :loadFileId
//            where   sm.me_load_file_id = 0
//                    and sm.merchant_number = :merchantId 
//                    and sm.activity_date <= :monthEndDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_daily_summary   sm\n          set     sm.me_load_filename =  :1  ,\n                  sm.me_load_file_id  =  :2  \n          where   sm.me_load_file_id = 0\n                  and sm.merchant_number =  :3   \n                  and sm.activity_date <=  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"52com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,loadFileId);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,monthEndDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2892^9*/
        updateCount = Ctx.getExecutionContext().getUpdateCount();
        
        if ( updateCount > 0 || !"C".equals(values[1]) )
        {
          Thread thread = pool.getThread( new SummaryJob(merchantId,values[1],activeDate,monthEndDate,BillingDb.getNewLoadSec(),loadFilename) );
          thread.start();
        }          
      }
      pool.waitForAllThreads();
      
      if ( !inTestMode() )   // if in production mode
      {
        // generate the billing ACH entries
        /*@lineinfo:generated-code*//*@lineinfo:2906^9*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_process 
//            (
//              process_type,process_sequence,load_filename
//            )
//            values
//            (
//              3,0,:loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_process \n          (\n            process_type,process_sequence,load_filename\n          )\n          values\n          (\n            3,0, :1  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"53com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2916^9*/
      
        // initiate the contract process
        /*@lineinfo:generated-code*//*@lineinfo:2919^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process 
//            (
//              process_type,process_sequence,load_filename
//            )
//            values
//            (
//              1,0,:loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process \n          (\n            process_type,process_sequence,load_filename\n          )\n          values\n          (\n            1,0, :1  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"54com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2929^9*/
        
        /*update the monthly_billing_process after Billing Summary job completed */
        //achps update
        /*@lineinfo:generated-code*//*@lineinfo:2933^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_billing_process 
//            set process_end_date = sysdate
//            where rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update monthly_billing_process \n          set process_end_date = sysdate\n          where rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"55com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2938^9*/
        
        //lcr update
        /*@lineinfo:generated-code*//*@lineinfo:2941^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_billing_process 
//            set process_end_date = sysdate
//            where rec_id = :lcrRecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update monthly_billing_process \n          set process_end_date = sysdate\n          where rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"56com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,lcrRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2946^9*/
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( true );
  }
  
  protected boolean includeMid( long merchantId, Date activeDate )
  {
    int       recCount      = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2967^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)    
//          from    mif     mf
//          where   mf.merchant_number = :merchantId
//                  and nvl(mf.processor_id,0) = 1
//                  and nvl(mf.conversion_date,'31-dec-9999') <= :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)     \n        from    mif     mf\n        where   mf.merchant_number =  :1  \n                and nvl(mf.processor_id,0) = 1\n                and nvl(mf.conversion_date,'31-dec-9999') <=  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.startup.MonthlyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2974^7*/
    }
    catch( Exception e )
    {
      logEntry("includeMid(" + merchantId + "," + activeDate + ")",e.toString());
    }
    finally
    {
    }
    return( recCount != 0 );
  }
  
  protected boolean inTestMode()
  {
    return( TestFileDate != null || TestFilename != null || TestNodeId != 0L );
  }
  
  public void reload( String dataType, long nodeId, Date activeDate )
  {
    int                 bankNumber        = 0;
    ResultSetIterator   it                = null;
    ThreadPool          pool              = null;
    ResultSet           resultSet         = null;
    
    try
    {
      if ( "NEW".equals(dataType) )
      {
        long loadSec = BillingDb.getNewLoadSec();
        /*@lineinfo:generated-code*//*@lineinfo:3003^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number            as merchant_number,
//                    :activeDate                   as active_date,
//                    :TestFilename                 as load_filename,
//                    :loadSec                      as load_sec,
//                    case
//                      when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then
//                       case
//                         when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') > :activeDate then 'O'
//                         else 'C'
//                       end
//                      when nvl(mf.dmacctst,'O') = 'Z' then
//                       case
//                         when exists
//                           (select 1
//                           from mif_changes
//                           where merchant_number = mf.merchant_number
//                           and change_details like '%merchantStatus [O]=>[Z]%'
//                           and trunc(date_changed, 'MM') = trunc(TO_DATE(:activeDate,'DD-MON-YY'), 'MM')
//                           )
//                         then 'O'
//                         else 'C'
//                       end
//                      else decode( mf.dmacctst,
//                                   null,'O',
//                                   mf.dmacctst )  
//                    end                           as merchant_status,
//                    last_day(:activeDate)         as month_end_date,
//                    mf.bank_number                as bank_number
//            from    organization        o,
//                    group_merchant      gm,
//                    mif                 mf
//            where   o.org_group = :nodeId 
//                    and gm.org_num = o.org_num
//                    and mf.merchant_number = gm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number            as merchant_number,\n                   :1                     as active_date,\n                   :2                   as load_filename,\n                   :3                        as load_sec,\n                  case\n                    when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then\n                     case\n                       when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') >  :4   then 'O'\n                       else 'C'\n                     end\n                    when nvl(mf.dmacctst,'O') = 'Z' then\n                     case\n                       when exists\n                         (select 1\n                         from mif_changes\n                         where merchant_number = mf.merchant_number\n                         and change_details like '%merchantStatus [O]=>[Z]%'\n                         and trunc(date_changed, 'MM') = trunc(TO_DATE( :5  ,'DD-MON-YY'), 'MM')\n                         )\n                       then 'O'\n                       else 'C'\n                     end\n                    else decode( mf.dmacctst,\n                                 null,'O',\n                                 mf.dmacctst )  \n                  end                           as merchant_status,\n                  last_day( :6  )         as month_end_date,\n                  mf.bank_number                as bank_number\n          from    organization        o,\n                  group_merchant      gm,\n                  mif                 mf\n          where   o.org_group =  :7   \n                  and gm.org_num = o.org_num\n                  and mf.merchant_number = gm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"58com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setString(2,TestFilename);
   __sJT_st.setLong(3,loadSec);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setDate(6,activeDate);
   __sJT_st.setLong(7,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"58com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3039^9*/
      }
      else    // reloading an existing entry, pull data from m/e table
      {
        /*@lineinfo:generated-code*//*@lineinfo:3043^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_merchant_number         as merchant_number,
//                    gn.hh_active_date             as active_date,
//                    gn.load_filename              as load_filename,
//                    gn.hh_load_sec                as load_sec,
//                    case
//                      when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then
//                       case
//                         when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') > :activeDate then 'O'
//                         else 'C'
//                       end
//                      when nvl(mf.dmacctst,'O') = 'Z' then
//                       case
//                         when exists
//                           (select 1
//                           from mif_changes
//                           where merchant_number = mf.merchant_number
//                           and change_details like '%merchantStatus [O]=>[Z]%'
//                           and trunc(date_changed, 'MM') = trunc(TO_DATE(:activeDate,'DD-MON-YY'), 'MM')
//                           )
//                         then 'O'
//                         else 'C'
//                       end
//                      else decode( mf.dmacctst,
//                                   null,'O',
//                                   mf.dmacctst )  
//                    end                           as merchant_status,
//                    last_day(gn.hh_active_date)   as month_end_date,
//                    gn.hh_bank_number             as bank_number                      
//            from    organization        o,
//                    group_merchant      gm,
//                    mif                 mf,
//                    monthly_extract_gn  gn
//            where   o.org_group = :nodeId 
//                    and gm.org_num = o.org_num
//                    and mf.merchant_number = gm.merchant_number
//                    and gn.hh_merchant_number = mf.merchant_number
//                    and gn.hh_active_date = :activeDate
//            order by gn.hh_load_sec                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_merchant_number         as merchant_number,\n                  gn.hh_active_date             as active_date,\n                  gn.load_filename              as load_filename,\n                  gn.hh_load_sec                as load_sec,\n                  case\n                    when nvl(mf.dmacctst,'O') in ( 'D','C','B' ) then\n                     case\n                       when nvl(mf.date_stat_chgd_to_dcb,'31-dec-9999') >  :1   then 'O'\n                       else 'C'\n                     end\n                    when nvl(mf.dmacctst,'O') = 'Z' then\n                     case\n                       when exists\n                         (select 1\n                         from mif_changes\n                         where merchant_number = mf.merchant_number\n                         and change_details like '%merchantStatus [O]=>[Z]%'\n                         and trunc(date_changed, 'MM') = trunc(TO_DATE( :2  ,'DD-MON-YY'), 'MM')\n                         )\n                       then 'O'\n                       else 'C'\n                     end\n                    else decode( mf.dmacctst,\n                                 null,'O',\n                                 mf.dmacctst )  \n                  end                           as merchant_status,\n                  last_day(gn.hh_active_date)   as month_end_date,\n                  gn.hh_bank_number             as bank_number                      \n          from    organization        o,\n                  group_merchant      gm,\n                  mif                 mf,\n                  monthly_extract_gn  gn\n          where   o.org_group =  :3   \n                  and gm.org_num = o.org_num\n                  and mf.merchant_number = gm.merchant_number\n                  and gn.hh_merchant_number = mf.merchant_number\n                  and gn.hh_active_date =  :4  \n          order by gn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"59com.mes.startup.MonthlyBillingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"59com.mes.startup.MonthlyBillingSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3083^9*/
      }
      resultSet = it.getResultSet();
      
      pool = new ThreadPool(7);
      
      while( resultSet.next() )
      {
        bankNumber      = resultSet.getInt("bank_number");
        setEventArgs(String.valueOf(bankNumber));
        
        Thread thread = pool.getThread( new ReloadJob(dataType,resultSet) );
        thread.start();
      }        
      resultSet.close();
      it.close();
      
      pool.waitForAllThreads();
    }
    catch( Exception e )
    {
      logEntry("reload(" + dataType + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }
  
  public void setTestFileDate( Date activeDate )
  {
    TestFileDate = activeDate;
  }
  
  public void setTestFilename( String testFilename )
  {
    TestFilename = testFilename;
  }
  
  public void setTestNodeId( long nodeId )
  {
    TestNodeId = nodeId;
  }
  
  public static void main( String[] args )
  {
    MonthlyBillingSummary        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalonePool();
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      test = new MonthlyBillingSummary();
      test.connect(true);
      
      test.setEventArgs("3941");
      
      if ( args.length > 0 && "loadDN".equals(args[0]) )
      {
        System.out.println("Loading DN data...");
        
        // setup a calendar with the last day of the month passed by user
        Calendar cal = Calendar.getInstance();
        cal.setTime( DateTimeFormatter.parseDate(args[4],"MM/dd/yyyy") );
        cal.add( Calendar.MONTH, 1 );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        cal.add( Calendar.DAY_OF_MONTH, -1 );
        
        Date  monthEndDate  = new java.sql.Date( cal.getTime().getTime() );
        long  merchantId    = Long.parseLong(args[3]);
        
        SummaryJob job = test.new SummaryJob();
        job.connect(true);
        job.setBillingData(BillingDb.loadMerchantBilling(merchantId,monthEndDate));
        job.loadDN( args[1],
                    Long.parseLong(args[2]),
                    merchantId );
        job.cleanUp();
      }
      else if ( args.length > 0 && "loadAP".equals(args[0]) )
      {
        System.out.println("Loading AP data...");
        
        // setup a calendar with the last day of the month passed by user
        Calendar cal = Calendar.getInstance();
        cal.setTime( DateTimeFormatter.parseDate(args[4],"MM/dd/yyyy") );
        cal.add( Calendar.MONTH, 1 );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        cal.add( Calendar.DAY_OF_MONTH, -1 );
        
        Date  monthEndDate  = new java.sql.Date( cal.getTime().getTime() );
        long  merchantId    = Long.parseLong(args[3]);
        
        SummaryJob job = test.new SummaryJob();
        job.connect(true);
        job.setBillingData(BillingDb.loadMerchantBilling(merchantId,monthEndDate));
        job.loadAP( args[1], Long.parseLong(args[2]), merchantId );
        job.cleanUp();
      }
      else if ( args.length > 0 && "loadST".equals(args[0]) )
      {
        System.out.println("Loading ST data...");
        
        // setup a calendar with the last day of the month passed by user
        Calendar cal = Calendar.getInstance();
        cal.setTime( DateTimeFormatter.parseDate(args[4],"MM/dd/yyyy") );
        cal.add( Calendar.MONTH, 1 );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        cal.add( Calendar.DAY_OF_MONTH, -1 );
        
        Date  monthEndDate  = new java.sql.Date( cal.getTime().getTime() );
        long  merchantId    = Long.parseLong(args[3]);
        
        SummaryJob job = test.new SummaryJob();
        job.connect(true);
        job.setBillingData(BillingDb.loadMerchantBilling(merchantId,monthEndDate));
        job.loadST( args[1], Long.parseLong(args[2]), merchantId );
        job.cleanUp();
      }
      else if ( args.length > 0 && "reload".equals(args[0]) )
      {
        System.out.println("Loading " + args[1] + " data...");
        if ( "NEW".equals(args[1]) ) { test.setTestFilename(args[4]); }
        test.reload( args[1], Long.parseLong(args[2]),
                       new java.sql.Date( DateTimeFormatter.parseDate(args[3],"MM/dd/yyyy").getTime() ) 
                      );
      }
      else if ( args.length > 0 && "execute".equals(args[0]) )
      {
        test.setEventArgs(args[1]);
        test.setTestFileDate( (args.length > 2) ?
                               new Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() ) :
                               null );
        test.setTestNodeId( (args.length > 3) ? Long.parseLong(args[3]) : 0L );
        test.setTestFilename( (args.length > 4) ? args[4] : null );
        test.execute();
      }      
      else
      {
        System.out.println("Invalid command");
      }        
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception ee ) {}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/