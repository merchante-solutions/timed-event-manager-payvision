/*@lineinfo:filename=MbsTools*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/MbsTools.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-04-05 16:51:26 -0700 (Tue, 05 Apr 2011) $
  Version            : $Revision: 18670 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class MbsTools
  extends SQLJConnectionBase
{
  private class UpdateSummaryJob extends SQLJConnectionBase
    implements Runnable
  {
    protected     Vector            ResetEntries  = null;
    
    public UpdateSummaryJob( Vector entries )
    {
      ResetEntries = entries;
    }
    
    public void run( )
    {
      try
      {
        connect(true);
        
        for( int i = 0; i < ResetEntries.size(); ++i )
        {
          String values[] = ((String)ResetEntries.elementAt(i)).split("-");

          incrementResetCount();
        
          /*@lineinfo:generated-code*//*@lineinfo:70^11*/

//  ************************************************************
//  #sql [Ctx] { update  /*+ index(sm idx_bds_load_file_id_mid) */
//                      mbs_daily_summary   sm
//              set     me_load_file_id = 0,
//                      me_load_filename = null
//              where   sm.me_load_file_id = :values[1]
//                      and sm.merchant_number = :values[0]
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_123 = values[1];
 String __sJT_124 = values[0];
   String theSqlTS = "update  /*+ index(sm idx_bds_load_file_id_mid) */\n                    mbs_daily_summary   sm\n            set     me_load_file_id = 0,\n                    me_load_filename = null\n            where   sm.me_load_file_id =  :1 \n                    and sm.merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.mbs.MbsTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_123);
   __sJT_st.setString(2,__sJT_124);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:78^11*/
        }
      }
      catch( Exception e )
      {
        logEntry("run()", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }      
  
  private   int             ResetCount        = 0;

  public MbsTools()
  {
  }
  
  public synchronized void incrementResetCount()
  {
    ++ResetCount;
    System.out.print("\r  Resetting 'mbs_daily_summary' (" + ResetCount + ")...");
  }
  
  public void resetDailySummary( String loadFilename )
  {
    long                  beginTs       = 0L;
    long                  endTs         = 0L;
    ResultSetIterator     it            = null;
    ThreadPool            pool          = null;
    ResultSet             resultSet     = null;
  
    try
    {
      pool = new ThreadPool(7);
    
      beginTs = System.currentTimeMillis();
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number || '-' ||
//                  load_filename_to_load_file_id(:loadFilename) as mid_lfi
//          from    mif                         mf
//          where   mf.bank_number = get_file_bank_number(:loadFilename)
//                  and nvl(mf.processor_id,0) = 1
//          order by mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number || '-' ||\n                load_filename_to_load_file_id( :1 ) as mid_lfi\n        from    mif                         mf\n        where   mf.bank_number = get_file_bank_number( :2 )\n                and nvl(mf.processor_id,0) = 1\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.mbs.MbsTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.mbs.MbsTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^7*/
      resultSet = it.getResultSet();
      
      Vector resetEntries = new Vector();
      while( resultSet.next() )
      {
        resetEntries.addElement( resultSet.getString("mid_lfi") );
      
        if ( resetEntries.size() == 100 )
        {
          Thread thread = pool.getThread( new UpdateSummaryJob(resetEntries) );
          thread.start();
          resetEntries = new Vector();
        }
      }
      resultSet.close();
      it.close();
      
      // boundary condition, update the last group
      if ( resetEntries.size() > 0 )
      {
        Thread thread = pool.getThread( new UpdateSummaryJob(resetEntries) );
        thread.start();
      }
      pool.waitForAllThreads();
      endTs = System.currentTimeMillis();
      System.out.println("complete (" + (endTs-beginTs) + ")");
    }
    catch( Exception e )
    {
      logEntry("resetDailySummary(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      
    }
  }
  
  public void resetMonthEnd( int bankNumber, Date activeDate )
  {
    resetMonthEnd(bankNumber,0L,activeDate,true);
  }
  
  public void resetMonthEnd( int bankNumber, long merchantId, Date activeDate, boolean resetActivity )
  {
    long                  beginTs       = 0L;
    long                  endTs         = 0L;
    ResultSetIterator     it            = null;
    String                loadFilename  = null;
    ResultSet             resultSet     = null;
    ThreadPool            pool          = null;
    
    try
    {
      // create a thread pool to reset daily summary entries
      pool = new ThreadPool(7);
    
      String[] tables = { "monthly_extract_ap",
                          "monthly_extract_c1",
                          "monthly_extract_cg",
                          "monthly_extract_dc",
                          "monthly_extract_dn",
                          "monthly_extract_mc",
                          "monthly_extract_pl",
                          "monthly_extract_st",
                          "monthly_extract_visa"
                        };
                        
      for( int i = 0; i < tables.length; ++i )
      {
        String tableName = tables[i];
        
        System.out.print("  Cleaning '" + tableName + "'...");
        beginTs = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:200^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from  :tableName 
//            where hh_load_sec in 
//                  (
//                    select  distinct gn.hh_load_sec
//                    from    mif                         mf,
//                            monthly_extract_gn          gn
//                    where   mf.bank_number = :bankNumber
//                            and :merchantId in ( 0, mf.merchant_number )
//                            and nvl(mf.processor_id,0) = 1
//                            and gn.hh_merchant_number = mf.merchant_number
//                            and gn.hh_active_date = :activeDate
//                            and gn.load_filename like 'mbs%'
//                  )                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete \n          from   ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("  \n          where hh_load_sec in \n                (\n                  select  distinct gn.hh_load_sec\n                  from    mif                         mf,\n                          monthly_extract_gn          gn\n                  where   mf.bank_number =  ? \n                          and  ?  in ( 0, mf.merchant_number )\n                          and nvl(mf.processor_id,0) = 1\n                          and gn.hh_merchant_number = mf.merchant_number\n                          and gn.hh_active_date =  ? \n                          and gn.load_filename like 'mbs%'\n                )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.mbs.MbsTools:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^9*/
        endTs = System.currentTimeMillis();
        System.out.println("complete (" + (endTs-beginTs) + ")");
      }
      
      System.out.print("  Cleaning 'monthly_extract_summary'...");
      beginTs = System.currentTimeMillis();
      /*@lineinfo:generated-code*//*@lineinfo:223^7*/

//  ************************************************************
//  #sql [Ctx] { delete  /*+ index(sm idx_mon_ext_sum_load_file_id) */
//          from    monthly_extract_summary   sm
//          where   sm.load_file_id in 
//                  (
//                    select  distinct load_filename_to_load_file_id(gn.load_filename)
//                    from    mif                         mf,
//                            monthly_extract_gn          gn
//                    where   mf.bank_number = :bankNumber
//                            and nvl(mf.processor_id,0) = 1
//                            and gn.hh_merchant_number = mf.merchant_number
//                            and gn.hh_active_date = :activeDate
//                            and gn.load_filename like 'mbs%'
//                  )
//                  and :merchantId in ( 0, sm.merchant_number )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  /*+ index(sm idx_mon_ext_sum_load_file_id) */\n        from    monthly_extract_summary   sm\n        where   sm.load_file_id in \n                (\n                  select  distinct load_filename_to_load_file_id(gn.load_filename)\n                  from    mif                         mf,\n                          monthly_extract_gn          gn\n                  where   mf.bank_number =  :1 \n                          and nvl(mf.processor_id,0) = 1\n                          and gn.hh_merchant_number = mf.merchant_number\n                          and gn.hh_active_date =  :2 \n                          and gn.load_filename like 'mbs%'\n                )\n                and  :3  in ( 0, sm.merchant_number )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.mbs.MbsTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^7*/
      endTs = System.currentTimeMillis();
      System.out.println("complete (" + (endTs-beginTs) + ")");
        
      //******************************************************************
      //* reset the activity in the mbs_daily_summary table if requested
      //******************************************************************
      if ( resetActivity )
      {
        beginTs = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:249^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number || '-' ||
//                    load_filename_to_load_file_id(gn.load_filename) as mid_lfi
//            from    mif                         mf,
//                    monthly_extract_gn          gn
//            where   mf.bank_number = :bankNumber
//                    and :merchantId in ( 0, mf.merchant_number )
//                    and nvl(mf.processor_id,0) = 1
//                    and gn.hh_merchant_number = mf.merchant_number
//                    and gn.hh_active_date = :activeDate
//                    and gn.load_filename like 'mbs%'
//            order by mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number || '-' ||\n                  load_filename_to_load_file_id(gn.load_filename) as mid_lfi\n          from    mif                         mf,\n                  monthly_extract_gn          gn\n          where   mf.bank_number =  :1 \n                  and  :2  in ( 0, mf.merchant_number )\n                  and nvl(mf.processor_id,0) = 1\n                  and gn.hh_merchant_number = mf.merchant_number\n                  and gn.hh_active_date =  :3 \n                  and gn.load_filename like 'mbs%'\n          order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.mbs.MbsTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.mbs.MbsTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:262^9*/
        resultSet = it.getResultSet();
      
        Vector resetEntries = new Vector();
        while( resultSet.next() )
        {
          resetEntries.addElement( resultSet.getString("mid_lfi") );
        
          if ( resetEntries.size() == 100 )
          {
            Thread thread = pool.getThread( new UpdateSummaryJob(resetEntries) );
            thread.start();
            resetEntries = new Vector();
          }
        }
        resultSet.close();
        it.close();
      
        // boundary condition, update the last group
        if ( resetEntries.size() > 0 )
        {
          Thread thread = pool.getThread( new UpdateSummaryJob(resetEntries) );
          thread.start();
        }
        pool.waitForAllThreads();
        endTs = System.currentTimeMillis();
        System.out.println("complete (" + (endTs-beginTs) + ")");
      }   // end if ( resetActivity )
      
      
      //******************************************************************
      //* Last Step - Remove entries from the GN table
      //******************************************************************
      System.out.print("  Cleaning 'monthly_extract_gn'...");
      beginTs = System.currentTimeMillis();
      /*@lineinfo:generated-code*//*@lineinfo:297^7*/

//  ************************************************************
//  #sql [Ctx] { delete  /*+ index (gn idx_mon_ext_gn_load_file_id) */
//          from    monthly_extract_gn    gn
//          where   gn.load_file_id in 
//                  (
//                    select  distinct load_filename_to_load_file_id(gni.load_filename)
//                    from    mif                         mf,
//                            monthly_extract_gn          gni
//                    where   mf.bank_number = :bankNumber
//                            and nvl(mf.processor_id,0) = 1
//                            and gni.hh_merchant_number = mf.merchant_number
//                            and gni.hh_active_date = :activeDate
//                            and gni.load_filename like 'mbs%'
//                  )
//                  and :merchantId in ( 0, gn.hh_merchant_number )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  /*+ index (gn idx_mon_ext_gn_load_file_id) */\n        from    monthly_extract_gn    gn\n        where   gn.load_file_id in \n                (\n                  select  distinct load_filename_to_load_file_id(gni.load_filename)\n                  from    mif                         mf,\n                          monthly_extract_gn          gni\n                  where   mf.bank_number =  :1 \n                          and nvl(mf.processor_id,0) = 1\n                          and gni.hh_merchant_number = mf.merchant_number\n                          and gni.hh_active_date =  :2 \n                          and gni.load_filename like 'mbs%'\n                )\n                and  :3  in ( 0, gn.hh_merchant_number )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.mbs.MbsTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:313^7*/
      endTs = System.currentTimeMillis();
      System.out.println("complete (" + (endTs-beginTs) + ")");
    }
    catch( Exception e )
    {
      logEntry("resetMonthEnd()",e.toString());
    }
    finally
    {
    }
  }
  
  public static void main( String[] args )
  {
    MbsTools   tools = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      tools = new MbsTools();
      
      tools.connect(true);
      
      if ( args.length > 0 && "resetMonthEnd".equals(args[0]) )
      {
        int bankNumber = Integer.parseInt(args[1]);
        java.sql.Date activeDate = new java.sql.Date(DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime());
        System.out.println("Cleaning all mbs month end data for " + bankNumber + " " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") + "...");
        tools.resetMonthEnd(bankNumber,activeDate);
      }
      else if ( args.length > 0 && "resetDailySummary".equals(args[0]) )
      {
        System.out.println("Resetting mbs daily summary data for " + args[1]);
        tools.resetDailySummary(args[1]);
      }
      else
      {
        System.out.println("Missing command");
      }        
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ tools.cleanUp(); } catch( Exception ee ){}
    }
  }
}/*@lineinfo:generated-code*/