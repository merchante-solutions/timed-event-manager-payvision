/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/IcVolume.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-03-27 18:19:55 -0700 (Sun, 27 Mar 2011) $
  Version            : $Revision: 18623 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

public class IcVolume 
{
  public  double      FeesDue           = 0.0;
  public  long        MerchantId        = 0L;
  public  String      StatementMsg      = null;
  public  double      VolumeAmount      = 0.0;
  public  int         VolumeCount       = 0;
  
  public IcVolume( long merchantId, String smsg )
  {
    MerchantId      = merchantId;
    StatementMsg    = smsg.toUpperCase();
  }
  
  public void addVolume( int count, double amount, double rate, double perItem )
  {
    VolumeAmount  += amount;
    VolumeCount   += count;
    FeesDue       += ((amount * rate * 0.01) + (count * perItem));
  }
}
