/*@lineinfo:filename=MBSDiscountBackfill*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/MBSDiscountBackfill.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-05-24 10:13:10 -0700 (Mon, 24 May 2010) $
  Version            : $Revision: 17322 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.util.Vector;
import org.apache.log4j.Logger;

public class MBSDiscountBackfill extends EventBase
{
  static Logger log = Logger.getLogger(MBSDiscountBackfill.class);
  
  private void backfillDiscount(long merchantNumber)
  {
    try
    {
      log.debug("backfilling: " + merchantNumber);
      /*@lineinfo:generated-code*//*@lineinfo:36^7*/

//  ************************************************************
//  #sql [Ctx] { merge into daily_detail_file_dt dt
//          using (
//            select  mf.merchant_number merchant_number,
//                    mf.visa_disc_method visa_disc_method,
//                    mds.item_subclass card_type,
//                    mp.rate  rate,
//                    mp.per_item per_item,
//                    mds.activity_date,
//                    mds.data_source,
//                    mp.valid_date_begin,
//                    mp.valid_date_end
//            from    mif mf,
//                    mbs_pricing mp,
//                    mbs_daily_summary mds
//            where   mf.merchant_number = :merchantNumber
//                    and mf.merchant_number = mp.merchant_number
//                    and mp.item_type = 101
//                    and mf.merchant_number = mds.merchant_number
//                    and mds.me_load_file_id = 0
//  --                  and substr(mds.data_source,1,4) in ('cdf3', '256s', 'mddf')
//                    and mp.item_type = mds.item_type
//                    and mds.item_subclass = mp.item_subclass
//                    and mp.valid_date_begin < last_day(sysdate)
//                    and mp.valid_date_end >= last_day(sysdate)
//                 ) price
//          on
//          (
//            dt.merchant_account_number = price.merchant_number
//            and dt.batch_date = price.activity_date
//            and dt.load_filename = price.data_source
//            and dt.card_type = price.card_type
//          )
//          when matched then
//          update
//            set dt.discount_amount = 
//              decode(dt.debit_credit_indicator, 
//                'C', decode(price.visa_disc_method,'N', -1, 0),
//                1) * 
//              round(dt.transaction_amount * (nvl(price.rate,0)*.01)+nvl(price.per_item,0),2),
//              dt.discount_rate = price.rate,
//              dt.per_item = price.per_item
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into daily_detail_file_dt dt\n        using (\n          select  mf.merchant_number merchant_number,\n                  mf.visa_disc_method visa_disc_method,\n                  mds.item_subclass card_type,\n                  mp.rate  rate,\n                  mp.per_item per_item,\n                  mds.activity_date,\n                  mds.data_source,\n                  mp.valid_date_begin,\n                  mp.valid_date_end\n          from    mif mf,\n                  mbs_pricing mp,\n                  mbs_daily_summary mds\n          where   mf.merchant_number =  :1 \n                  and mf.merchant_number = mp.merchant_number\n                  and mp.item_type = 101\n                  and mf.merchant_number = mds.merchant_number\n                  and mds.me_load_file_id = 0\n--                  and substr(mds.data_source,1,4) in ('cdf3', '256s', 'mddf')\n                  and mp.item_type = mds.item_type\n                  and mds.item_subclass = mp.item_subclass\n                  and mp.valid_date_begin < last_day(sysdate)\n                  and mp.valid_date_end >= last_day(sysdate)\n               ) price\n        on\n        (\n          dt.merchant_account_number = price.merchant_number\n          and dt.batch_date = price.activity_date\n          and dt.load_filename = price.data_source\n          and dt.card_type = price.card_type\n        )\n        when matched then\n        update\n          set dt.discount_amount = \n            decode(dt.debit_credit_indicator, \n              'C', decode(price.visa_disc_method,'N', -1, 0),\n              1) * \n            round(dt.transaction_amount * (nvl(price.rate,0)*.01)+nvl(price.per_item,0),2),\n            dt.discount_rate = price.rate,\n            dt.per_item = price.per_item";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MBSDiscountBackfill",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^7*/
      
      log.debug("done");
    }
    catch(Exception e)
    {
      logEntry("backfillDiscount(" + merchantNumber + ")", e.toString());
    }
  }
  
  private void resummarizeBilling(long merchantNumber)
  {
    try
    {
      log.debug("resummarizing: " + merchantNumber);
      // add entries into mbs_process to resummarize these files.
      // process_type 4 means daily detail file discount, ic, and debit
      /*@lineinfo:generated-code*//*@lineinfo:96^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//          (
//            process_type,
//            process_sequence,
//            load_filename,
//            hierarchy_node
//          )
//          select  distinct 4,
//                  0,
//                  mds.data_source,
//                  mf.merchant_number
//          from    mif mf,
//                  mbs_pricing mp,
//                  mbs_daily_summary mds
//          where   mf.merchant_number = :merchantNumber
//                  and mf.merchant_number = mp.merchant_number
//                  and mp.item_type = 101
//                  and mf.merchant_number = mds.merchant_number
//                  and mds.me_load_file_id = 0
//                  and mp.item_type = mds.item_type 
//                  and mds.item_subclass = mp.item_subclass
//                  and mp.valid_date_begin < last_day(sysdate)
//                  and mp.valid_date_end >= last_day(sysdate)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n        (\n          process_type,\n          process_sequence,\n          load_filename,\n          hierarchy_node\n        )\n        select  distinct 4,\n                0,\n                mds.data_source,\n                mf.merchant_number\n        from    mif mf,\n                mbs_pricing mp,\n                mbs_daily_summary mds\n        where   mf.merchant_number =  :1 \n                and mf.merchant_number = mp.merchant_number\n                and mp.item_type = 101\n                and mf.merchant_number = mds.merchant_number\n                and mds.me_load_file_id = 0\n                and mp.item_type = mds.item_type \n                and mds.item_subclass = mp.item_subclass\n                and mp.valid_date_begin < last_day(sysdate)\n                and mp.valid_date_end >= last_day(sysdate)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.MBSDiscountBackfill",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^7*/
      
      log.debug("done");
    }
    catch(Exception e)
    {
      logEntry("resummarizeBilling(" + merchantNumber + ")", e.toString());
    }
  }
  
  public boolean execute()
  {
    boolean                         retVal  = false;
    ProcessTable.ProcessTableEntry  entry   = null;
    ProcessTable                    pt      = null;
    int 							DAYS_FROM_BOM_EVT_DISABLED = 1;
    int 							DAYS_TILL_EOM_EVT_DISABLED = 2;
    
    try
    {
      connect();
      
      log.debug("creating process table");
      pt = new ProcessTable("mbs_backfill_process", "mbs_backfill_sequence", ProcessTable.PT_ALL, true);
      
      log.debug("checking for pending entries");
      if ( pt.hasPendingEntries() )
      {
      	try {
      		DAYS_FROM_BOM_EVT_DISABLED = Integer.parseInt( getEventArg(0) );
      	} catch(Exception e) {}
      	 
      	try {
      		DAYS_TILL_EOM_EVT_DISABLED = Integer.parseInt( getEventArg(1) );
      	} catch(Exception e) {}
      	log.debug("DAYS_TILL_EOM_EVT_DISABLED : " + DAYS_TILL_EOM_EVT_DISABLED + ", DAYS_FROM_BOM_EVT_DISABLED : " + DAYS_FROM_BOM_EVT_DISABLED);
      	
      	// This process should run after first day of month as we still have some month end processes running on 1st.
      	// Change being applied as for merchants not marked for month end processing during the 1:00 PM run of this event on 1st, 
      	// the new rates will get applied for previous month. 
        int daysFromBOM = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:163^9*/

//  ************************************************************
//  #sql [Ctx] { select  extract(day from sysdate)
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  extract(day from sysdate)\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MBSDiscountBackfill",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   daysFromBOM = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^9*/
        
        // this process can only run up until two days before the end of the month
        int daysTilEOM = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:173^9*/

//  ************************************************************
//  #sql [Ctx] { select  last_day(sysdate) - sysdate
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  last_day(sysdate) - sysdate\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MBSDiscountBackfill",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   daysTilEOM = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^9*/
        
        if( daysTilEOM > DAYS_TILL_EOM_EVT_DISABLED && daysFromBOM > DAYS_FROM_BOM_EVT_DISABLED )
        {
          pt.preparePendingEntries();
        
          Vector entries = pt.getEntryVector();
          for( int i=0; i < entries.size(); ++i )
          {
            entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
          
            log.debug("backfilling for: " + entry.getNodeId());
          
            entry.recordTimestampBegin();
            backfillDiscount(entry.getNodeId());
            resummarizeBilling(entry.getNodeId());
            entry.recordTimestampEnd();
            commit();
          
            log.debug("done");
          }
        }
        else
        {
          logEntry("execute()", "NOT RUNNING DISCOUNT BACKFILL BECAUSE TOO CLOSE TO EOM OR BOM");
        }
      }
      else
      {
        log.debug("no pending entries");
      }
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( retVal );
  }
  
  public void processSingle(long merchantNumber)
  {
    try
    {
      connect();
      backfillDiscount(merchantNumber);
      resummarizeBilling(merchantNumber);
    }
    catch(Exception e)
    {
      logEntry("processSingle(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static void main(String[] args)
  {
    com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
    
    try
    {
      MBSDiscountBackfill worker = new MBSDiscountBackfill();
      
      if( args.length == 0 )
      {
        worker.execute();
      }
      else
      {
        long merchantNumber = Long.parseLong(args[0]);
      
      
        worker.processSingle(merchantNumber);
      }
    }
    catch(Exception e)
    {
      log.error("main: " + e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/