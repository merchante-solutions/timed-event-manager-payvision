/*@lineinfo:filename=TridentAuthLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TridentAuthLoader.sqlj $

  Description:

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-05-13 13:06:08 -0700 (Wed, 13 May 2015) $
  Version            : $Revision: 23623 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.PropertiesFile;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class TridentAuthLoader extends EventBase
{
  private class FundingSourceWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    private long      AuthRecId         = 0L;
    private String    CardNumberFull    = null;
    
    public FundingSourceWorker( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthRecId       = resultSet.getLong("rec_id");
      CardNumberFull  = resultSet.getString("card_number_full");
    }
    
    public void run()
    {
      String              debitType     = "0";
      String              fundingSource = "C";    // default to credit
      int                 regCount      = 0;
      
      try
      {
        connect(true);
        
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:65^11*/

//  ************************************************************
//  #sql [Ctx] { select  funding_source
//              
//              from    visa_ardef_table    ard
//              where   substr(:CardNumberFull,1,9) between ard.range_low and ard.range_high
//                      and ard.enabled = 'Y'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  funding_source\n             \n            from    visa_ardef_table    ard\n            where   substr( :1 ,1,9) between ard.range_low and ard.range_high\n                    and ard.enabled = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentAuthLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,CardNumberFull);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fundingSource = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:74^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    visa_ardef_table_regulated    ardreg
//              where   rpad(:CardNumberFull,18,'0') between ardreg.range_low and ardreg.range_high
//                      and ardreg.enabled = 'Y'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    visa_ardef_table_regulated    ardreg\n            where   rpad( :1 ,18,'0') between ardreg.range_low and ardreg.range_high\n                    and ardreg.enabled = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentAuthLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,CardNumberFull);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   regCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:81^11*/
          debitType = ((regCount == 0) ? "0" : "1");            
        }
        catch( Exception sqle )
        {
          // ignore, use defaults
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] { update  :TridentAuthTableName
//            set     funding_source  = :fundingSource,
//                    debit_type      = :debitType,
//                    load_sequence   = 0
//            where   rec_id = :AuthRecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(TridentAuthTableName);
   __sjT_sb.append(" \n          set     funding_source  =  ? ,\n                  debit_type      =  ? ,\n                  load_sequence   = 0\n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.startup.TridentAuthLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fundingSource);
   __sJT_st.setString(2,debitType);
   __sJT_st.setLong(3,AuthRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }

  static Logger log = Logger.getLogger(TridentAuthLoader.class);
  
  private   String      TC33AuthTableName         = "tc33_authorization";
  private   String      TridentAuthTableName      = "tc33_trident";
  
  public TridentAuthLoader( )
  {
    PropertiesFilename = "tauth-loader.properties";
  }
  
  public boolean execute()
  {
    String      eventType   = getEventArg(0);
    boolean     retVal      = false;
    
    if ( eventType == null || eventType.equals("0") )
    {
      retVal = loadAuths();
    }
    else if ( eventType.equals("1") )
    {
      retVal = loadVisaFundingSource();
    }
    return( retVal );
  }
  
  protected boolean loadAuths()
  {
    ResultSetIterator   it            = null;
    long                loadFileId    = 0L;
    String              loadFilename  = null;
    long                lastRiskTS    = 0L;
    long                loaderSeq     = 0L;
    int                 loopCount     = 0;
    int                 maxCount      = 0;
    PropertiesFile      propsFile     = null;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;
    long                riskDelay     = 0L;
    int                 updateCount   = 0;
    
    try
    {
      connect(true);
      setAutoCommit(false);
      
      propsFile   = new PropertiesFile( PropertiesFilename );
      maxCount    = propsFile.getInt("tauth.loadCountMax",250);
      riskDelay   = (propsFile.getLong("tauth.riskDelayMinutes",60) * 60 * 1000);   // in miliseconds
      
      lastRiskTS  = propsFile.getLong("tauth.lastRiskLoad",0L);
      if ( lastRiskTS == 0L )
      {
        lastRiskTS = Calendar.getInstance().getTime().getTime();
        propsFile.load( PropertiesFilename );
        propsFile.setProperty("tauth.lastRiskLoad",String.valueOf(lastRiskTS));
        propsFile.store( PropertiesFilename );
      }
      
      loadFilename  = propsFile.getString("tauth.loadFilename", null);
      if ( loadFilename == null )
      {
        loadFilename = generateFilename("tauth3941");
        
        // reload the properties (generateFilename will update file)
        propsFile.load( PropertiesFilename );
        propsFile.setProperty("tauth.loadFilename",loadFilename);
        propsFile.store( PropertiesFilename );
      }
      loadFileId = loadFilenameToLoadFileId(loadFilename);
      
      do
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:184^11*/

//  ************************************************************
//  #sql [Ctx] { select  trident_auth_loader_seq.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_auth_loader_seq.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TridentAuthLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loaderSeq = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:188^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:190^11*/

//  ************************************************************
//  #sql [Ctx] { update  /*+ index (auth idx_tc33_trident_load_seq) */
//                      :TridentAuthTableName   auth
//              set     tc33_load_filename = :loadFilename,
//                      tc33_load_file_id = :loadFileId,
//                      load_sequence  = :loaderSeq
//              where   load_sequence = 0 and
//                      rownum <= :maxCount
//                      and transaction_date >= sysdate-7
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  /*+ index (auth idx_tc33_trident_load_seq) */\n                     ");
   __sjT_sb.append(TridentAuthTableName);
   __sjT_sb.append("    auth\n            set     tc33_load_filename =  ? ,\n                    tc33_load_file_id =  ? ,\n                    load_sequence  =  ? \n            where   load_sequence = 0 and\n                    rownum <=  ? \n                    and transaction_date >= sysdate-7");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.startup.TridentAuthLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,loadFileId);
   __sJT_st.setLong(3,loaderSeq);
   __sJT_st.setInt(4,maxCount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^11*/
          updateCount = Ctx.getExecutionContext().getUpdateCount();
          
          if ( updateCount > 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:205^13*/

//  ************************************************************
//  #sql [Ctx] { insert into :TC33AuthTableName
//                (
//                  merchant_number,
//                  terminal_id,
//                  merchant_category_code,
//                  transaction_date,
//                  transaction_time,
//                  card_type,
//                  card_number,
//                  expiration_date,
//                  expiration_date_incoming,
//                  authorized_amount,
//                  response_code,
//                  authorization_code,
//                  avs_result,
//                  auth_characteristics_ind,
//                  payment_service_transaction_id,
//                  validation_code,
//                  pos_entry_capability,
//                  pos_condition_code,
//                  market_specific_data_indicator,
//                  stnd_in_processing_advice_code,
//                  currency_code,
//                  source_bin,
//                  destination_bin,
//                  mes_ref_num,
//                  cvv_result,
//                  cvv2_result,    
//                  acquirer_bin, 
//                  aquirer_station_id,  
//                  bank_number,              
//                  card_number_md5, 
//                  communication_line_type, 
//                  julian_day, 
//                  pos_entry_mode_code, 
//                  processing_code, 
//                  report_line_sequence_number, 
//                  tc33_application_code, 
//                  terminal_format_code, 
//                  transaction_code, 
//                  transaction_code_qualifier, 
//                  transaction_component_seq_nbr,
//                  time_zone,
//                  retrieval_refrence_number,
//                  requested_aci,
//                  cvv2_request_data,
//                  country_code,
//                  device_code,
//                  moto_ecommerce_indicator,
//                  auth_response_text,
//                  comm_card_request_indicator,
//                  comm_card_response_indicator,
//                  industry_code,
//                  transaction_code_alpha,
//                  card_number_enc,
//                  trident_transaction_id,
//                  avs_zip,
//                  product_level_result_code,
//                  auth_reversed,
//                  secondary_amount,
//                  fee_program_indicator,
//                  spend_qualified_ind,
//                  card_sequence_number,
//                  token_assurance_level,
//                  iso_ecommerce_indicator,
//                  developer_id,
//                  developer_version,
//                  funding_source,
//                  debit_type,
//                  load_filename
//                )
//                select  /*+ index (auth idx_tc33_trident_load_seq) */
//                        auth.merchant_number,
//                        auth.terminal_id,
//                        auth.merchant_category_code,
//                        auth.transaction_date,
//                        auth.transaction_time,
//                        auth.card_type,
//                        auth.card_number,
//                        auth.expiration_date,
//                        auth.expiration_date_incoming,
//                        auth.authorized_amount,
//                        auth.response_code,
//                        auth.authorization_code,
//                        auth.avs_result,
//                        auth.returned_aci,
//                        auth.transaction_id_alpha,
//                        auth.validation_code,
//                        auth.pos_entry_capability,
//                        auth.pos_condition_code,
//                        auth.market_specific_data_indicator,
//                        auth.stnd_in_processing_advice_code,
//                        auth.currency_code,
//                        auth.acquirer_bin,
//                        auth.destination_bin,
//                        auth.mes_ref_num,
//                        auth.cvv_result,
//                        auth.cvv2_result,
//                        auth.acquirer_bin, 
//                        null,  
//                        auth.bank_number,              
//                        null, 
//                        null, 
//                        auth.julian_day, 
//                        auth.pos_entry_mode, 
//                        null, 
//                        null, 
//                        null, 
//                        substr(auth.format_version,1,1), 
//                        decode( is_number(auth.transaction_code), 0, null,auth.transaction_code ), 
//                        null, 
//                        null, 
//                        auth.time_zone,
//                        auth.retrieval_refrence_number,
//                        auth.requested_aci,
//                        auth.cvv2_request_data,
//                        auth.country_code,
//                        auth.device_code,
//                        auth.moto_ecommerce_indicator,
//                        auth.auth_response_text,
//                        auth.comm_card_request_indicator,
//                        auth.comm_card_response_indicator,
//                        auth.industry_code,
//                        auth.transaction_code_alpha,
//                        auth.card_number_enc,
//                        substr(auth.trident_transaction_id,1,32),
//                        auth.avs_zip,
//                        auth.product_level_result_code,
//                        auth.auth_reversed,
//                        auth.secondary_amount,
//                        auth.fee_program_indicator,
//                        auth.spend_qualified_ind,
//                        auth.card_sequence_number,
//                        auth.token_assurance_level,
//                        auth.iso_ecommerce_indicator,
//                        auth.developer_id,
//                        auth.developer_version,
//                        auth.funding_source,
//                        auth.debit_type,
//                        :loadFilename
//                from    :TridentAuthTableName     auth
//                where   auth.load_sequence = :loaderSeq
//                        and auth.transaction_date >= sysdate-7
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into  ");
   __sjT_sb.append(TC33AuthTableName);
   __sjT_sb.append(" \n              (\n                merchant_number,\n                terminal_id,\n                merchant_category_code,\n                transaction_date,\n                transaction_time,\n                card_type,\n                card_number,\n                expiration_date,\n                expiration_date_incoming,\n                authorized_amount,\n                response_code,\n                authorization_code,\n                avs_result,\n                auth_characteristics_ind,\n                payment_service_transaction_id,\n                validation_code,\n                pos_entry_capability,\n                pos_condition_code,\n                market_specific_data_indicator,\n                stnd_in_processing_advice_code,\n                currency_code,\n                source_bin,\n                destination_bin,\n                mes_ref_num,\n                cvv_result,\n                cvv2_result,    \n                acquirer_bin, \n                aquirer_station_id,  \n                bank_number,              \n                card_number_md5, \n                communication_line_type, \n                julian_day, \n                pos_entry_mode_code, \n                processing_code, \n                report_line_sequence_number, \n                tc33_application_code, \n                terminal_format_code, \n                transaction_code, \n                transaction_code_qualifier, \n                transaction_component_seq_nbr,\n                time_zone,\n                retrieval_refrence_number,\n                requested_aci,\n                cvv2_request_data,\n                country_code,\n                device_code,\n                moto_ecommerce_indicator,\n                auth_response_text,\n                comm_card_request_indicator,\n                comm_card_response_indicator,\n                industry_code,\n                transaction_code_alpha,\n                card_number_enc,\n                trident_transaction_id,\n                avs_zip,\n                product_level_result_code,\n                auth_reversed,\n                secondary_amount,\n                fee_program_indicator,\n                spend_qualified_ind,\n                card_sequence_number,\n                token_assurance_level,\n                iso_ecommerce_indicator,\n                developer_id,\n                developer_version,\n                funding_source,\n                debit_type,\n                load_filename\n              )\n              select  /*+ index (auth idx_tc33_trident_load_seq) */\n                      auth.merchant_number,\n                      auth.terminal_id,\n                      auth.merchant_category_code,\n                      auth.transaction_date,\n                      auth.transaction_time,\n                      auth.card_type,\n                      auth.card_number,\n                      auth.expiration_date,\n                      auth.expiration_date_incoming,\n                      auth.authorized_amount,\n                      auth.response_code,\n                      auth.authorization_code,\n                      auth.avs_result,\n                      auth.returned_aci,\n                      auth.transaction_id_alpha,\n                      auth.validation_code,\n                      auth.pos_entry_capability,\n                      auth.pos_condition_code,\n                      auth.market_specific_data_indicator,\n                      auth.stnd_in_processing_advice_code,\n                      auth.currency_code,\n                      auth.acquirer_bin,\n                      auth.destination_bin,\n                      auth.mes_ref_num,\n                      auth.cvv_result,\n                      auth.cvv2_result,\n                      auth.acquirer_bin, \n                      null,  \n                      auth.bank_number,              \n                      null, \n                      null, \n                      auth.julian_day, \n                      auth.pos_entry_mode, \n                      null, \n                      null, \n                      null, \n                      substr(auth.format_version,1,1), \n                      decode( is_number(auth.transaction_code), 0, null,auth.transaction_code ), \n                      null, \n                      null, \n                      auth.time_zone,\n                      auth.retrieval_refrence_number,\n                      auth.requested_aci,\n                      auth.cvv2_request_data,\n                      auth.country_code,\n                      auth.device_code,\n                      auth.moto_ecommerce_indicator,\n                      auth.auth_response_text,\n                      auth.comm_card_request_indicator,\n                      auth.comm_card_response_indicator,\n                      auth.industry_code,\n                      auth.transaction_code_alpha,\n                      auth.card_number_enc,\n                      substr(auth.trident_transaction_id,1,32),\n                      auth.avs_zip,\n                      auth.product_level_result_code,\n                      auth.auth_reversed,\n                      auth.secondary_amount,\n                      auth.fee_program_indicator,\n                      auth.spend_qualified_ind,\n                      auth.card_sequence_number,\n                      auth.token_assurance_level,\n                      auth.iso_ecommerce_indicator,\n                      auth.developer_id,\n                      auth.developer_version,\n                      auth.funding_source,\n                      auth.debit_type,\n                       ? \n              from     ");
   __sjT_sb.append(TridentAuthTableName);
   __sjT_sb.append("      auth\n              where   auth.load_sequence =  ? \n                      and auth.transaction_date >= sysdate-7");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.startup.TridentAuthLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,loaderSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^13*/
            log.debug( Ctx.getExecutionContext().getUpdateCount() + " auths inserted");//@
          }
      
          /*@lineinfo:generated-code*//*@lineinfo:354^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^11*/
        }
        catch( java.sql.SQLException sqle )
        {
          log.error(sqle.toString() + " - rolling back");
          try{ /*@lineinfo:generated-code*//*@lineinfo:362^16*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:362^38*/ }catch( Exception ee ) {}
        }
      } 
      while ( (updateCount > 0) && (++loopCount < 100));
      
      // check to see if a risk scoring entry needs to be made
      long now = Calendar.getInstance().getTime().getTime();
      if ( (now - lastRiskTS) > riskDelay )
      {
        /*@lineinfo:generated-code*//*@lineinfo:371^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//              ( process_type, load_filename )
//            values
//              ( 4, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n            ( process_type, load_filename )\n          values\n            ( 4,  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.TridentAuthLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:377^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:379^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//            (
//              process_sequence,
//              process_type,
//              load_filename
//            )
//            values
//            (
//              0,
//              1,  -- auth file data
//              :loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n          (\n            process_sequence,\n            process_type,\n            load_filename\n          )\n          values\n          (\n            0,\n            1,  -- auth file data\n             :1 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.TridentAuthLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:393^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:395^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:395^29*/
        
        // reset the local filename and timestamp
        loadFilename  = generateFilename( "tauth3941" );
        loadFileId    = loadFilenameToLoadFileId(loadFilename);
        lastRiskTS    = now;
        
        // store the new filename and risk timestamp
        propsFile.load( PropertiesFilename );
        propsFile.setProperty("tauth.lastRiskLoad",String.valueOf(now));
        propsFile.setProperty("tauth.loadFilename",loadFilename);
        propsFile.store( PropertiesFilename );
      }
      retVal = true;
    }
    catch(Exception e)
    {
      rollback();
      logEntry("loadAuths()", e.toString());
      commit();
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean loadVisaFundingSource()
  {
    ResultSetIterator   it            = null;
    int                 loopCount     = 0;
    ThreadPool          pool          = null;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;
    int                 updateCount   = 0;
    
    try
    {
      connect(true);
      
      pool = new ThreadPool(10);
      do 
      {
        updateCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:442^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.rec_id,
//                    dukpt_decrypt_wrapper(card_number_enc)  as card_number_full
//            from    :TridentAuthTableName   auth
//            where   auth.load_sequence = -1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  auth.rec_id,\n                  dukpt_decrypt_wrapper(card_number_enc)  as card_number_full\n          from     ");
   __sjT_sb.append(TridentAuthTableName);
   __sjT_sb.append("    auth\n          where   auth.load_sequence = -1");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "8com.mes.startup.TridentAuthLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          Thread thread = pool.getThread( new FundingSourceWorker(resultSet) );
          thread.start();
          ++updateCount;
        }
        resultSet.close();
        it.close();
        pool.waitForAllThreads();
      }
      while ( (updateCount > 0) && (++loopCount < 100));
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("loadVisaFundingSource()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  protected void resetTest()
    throws Exception
  {
    log.debug("resetting test tables");
    connect();
    /*@lineinfo:generated-code*//*@lineinfo:482^5*/

//  ************************************************************
//  #sql [Ctx] { update tc33_trident_test
//        set load_sequence = 0
//        where load_sequence != 0
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update tc33_trident_test\n      set load_sequence = 0\n      where load_sequence != 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.TridentAuthLoader",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^5*/      


    /*@lineinfo:generated-code*//*@lineinfo:490^5*/

//  ************************************************************
//  #sql [Ctx] { truncate table tc33_authorization_test
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "truncate table tc33_authorization_test";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.TridentAuthLoader",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:493^5*/
    
    /*@lineinfo:generated-code*//*@lineinfo:495^5*/

//  ************************************************************
//  #sql [Ctx] { commit
//       };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:498^5*/      
    cleanUp();
    log.debug("test tables reset");
  }
  
  public void setTestMode()
    throws Exception
  {
    resetTest();    // reset the test tables
  
    // set system to use test tables
    TC33AuthTableName     = "tc33_authorization_test";
    TridentAuthTableName  = "tc33_trident_test";
  }
  
  public static void main( String[] args )
  {
    TridentAuthLoader      test  = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      test = new TridentAuthLoader();
      if ( "execute".equals(args[0]) )
      {
        if ( args.length > 1 && "test".equals(args[1]) ) 
        {
          test.setTestMode();
        }
        test.execute();
      }        
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/