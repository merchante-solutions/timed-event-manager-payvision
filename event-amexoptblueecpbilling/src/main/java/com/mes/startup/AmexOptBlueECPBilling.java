package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;


public class AmexOptBlueECPBilling extends EventBase
{
  static Logger    log             = Logger.getLogger(AmexOptBlueECPBilling.class);

  private Date     ActiveDate      = null;
  private double   ECPFee          = 0.00;
  private double   CBFeePerItem    = 0.00;
  private long     NodeId          = 0L;

  public AmexOptBlueECPBilling()
  {
  }

  protected void readProps() throws Exception
  {
    PropertiesFile props = new PropertiesFile("optblue.properties");

    ECPFee = props.getDouble("ecp_fee_default", ECPFee);
    CBFeePerItem = props.getDouble("cb_fee_per_item", CBFeePerItem);
  }

  public boolean execute()
  {
    int               paramIndex        = 0;
    boolean           lastDayOfMonth    = false;
    String            qs                = null;
    ResultSet         rs                = null;
    PreparedStatement ps                = null;

    try
    {
      connect();

      readProps();
      qs = " select case when trunc(sysdate) = trunc(last_day(sysdate)) then 1  else 0 end " +
           " from   dual ";
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();
      if( rs.next() )
      {
        lastDayOfMonth = (rs.getInt(1) == 1);
      }
      ps.close();
      rs.close();

      if( lastDayOfMonth || (ActiveDate != null && NodeId != 0L) )
      {
        if( NodeId == 0L )
        {
          NodeId = 9999999999L;
        }

        if( ActiveDate == null )
        {
          qs = " select trunc(sysdate,'month') " +
               " from   dual ";
          ps = con.prepareStatement(qs);
          rs = ps.executeQuery();
          if( rs.next() )
          {
            ActiveDate = (rs.getDate(1));
          }
        }
        ps.close();
        rs.close();

        // AM EXCESSIVE CHARGEBACK PROGRAM
        qs = " insert into mbs_charge_records                                                   "+
             " (                                                                                "+
             "   charge_type,                                                                   "+
             "   merchant_number,                                                               "+
             "   billing_frequency,                                                             "+
             "   item_count,                                                                    "+
             "   charge_amount,                                                                 "+
             "   transit_routing,                                                               "+
             "   dda,                                                                           "+
             "   start_date,                                                                    "+
             "   end_date,                                                                      "+
             "   statement_message,                                                             "+
             "   charge_acct_type,                                                              "+
             "   enabled,                                                                       "+ 
             "   created_by                                                                     "+
             "   )                                                                              "+
             "   select 'MIS'                                 as charge_type,                   "+
             "          scd.merchant_number                   as merchant_number,               "+
             "          case                                                                    "+
             "            when to_char(?,'mm') =  1 then 'YNNNNNNNNNNN'                         "+
             "            when to_char(?,'mm') =  2 then 'NYNNNNNNNNNN'                         "+
             "            when to_char(?,'mm') =  3 then 'NNYNNNNNNNNN'                         "+
             "            when to_char(?,'mm') =  4 then 'NNNYNNNNNNNN'                         "+
             "            when to_char(?,'mm') =  5 then 'NNNNYNNNNNNN'                         "+
             "            when to_char(?,'mm') =  6 then 'NNNNNYNNNNNN'                         "+
             "            when to_char(?,'mm') =  7 then 'NNNNNNYNNNNN'                         "+
             "            when to_char(?,'mm') =  8 then 'NNNNNNNYNNNN'                         "+
             "            when to_char(?,'mm') =  9 then 'NNNNNNNNYNNN'                         "+
             "            when to_char(?,'mm') = 10 then 'NNNNNNNNNYNN'                         "+
             "            when to_char(?,'mm') = 11 then 'NNNNNNNNNNYN'                         "+
             "            when to_char(?,'mm') = 12 then 'NNNNNNNNNNNY'                         "+
             "            else                           'NNNNNNNNNNNN'                         "+
             "          end                                   as billing_months,                "+
             "          null                                  as item_count,                    "+
             "          ?                                     as charge_amount,                 "+
             "          decode(mf.discount_routing, null, mf.transit_routng_num, to_char(mf.discount_routing))  "+
             "                                                as transit_routing,               "+
             "          decode(mf.discount_dda, null, mf.dda_num, mf.discount_dda)              "+
             "                                                as dda,                           "+
             "          ?                                     as start_date,                    "+
             "          trunc(last_day(?))   as end_date,                                       "+
             "          'AM EXCESSIVE CHARGEBACK PROGRAM'     as statement_message,             "+
             "          'CK'                                  as charge_acct_type,              "+
             "          'Y'                                   as enabled,                       "+
             "          'SYSTEM'                              as created_by                     "+
             "   from   mif mf,                                                                 "+
             "          organization o,                                                         "+
             "          group_merchant gm,                                                      "+
             "          mes.amex_optblue_scd scd,                                               "+
             "          ( select aos.merchant_number          as merchant_number,               "+
             "                   max(aos.prg_effective_date)  as prg_effective_date             "+
             "            from   mes.amex_optblue_scd aos                                       "+
             "            where  aos.rec_id >= 0                                                "+
             "                   and trim(aos.prg_indicator) = 'ECP'                            "+
             "                   and aos.prg_status_indicator != 'W'                            "+
             "             group by aos.merchant_number                                         "+
             "           ) mcb                                                                  "+
             "   where  o.org_group = ?                                                         "+
             "          and gm.org_num = o.org_num                                              "+
             "          and mf.merchant_number = gm.merchant_number                             "+
             "          and scd.merchant_number = mf.merchant_number                            "+
             "          and scd.merchant_number = mcb.merchant_number                           "+
             "          and scd.prg_effective_date = mcb.prg_effective_date                     "+
             "          and scd.prg_status_indicator = 'E'                                      ";
        paramIndex = 1;
        ps = con.prepareStatement(qs);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDouble(paramIndex++, ECPFee);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setLong(paramIndex++, NodeId);
        int count = ps.executeUpdate();
        commit();
        ps.close();
        rs.close();
        log.info("EXCESSIVE CHARGEBACK PROGRAM - insert total "+ count + " records");

        // AM EXCESSIVE CHARGEBACK FEE
        qs = " insert into mbs_charge_records                                             "+
             " (                                                                          "+
             "   charge_type,                                                             "+
             "   merchant_number,                                                         "+
             "   billing_frequency,                                                       "+
             "   item_count,                                                              "+
             "   charge_amount,                                                           "+
             "   transit_routing,                                                         "+
             "   dda,                                                                     "+
             "   start_date,                                                              "+
             "   end_date,                                                                "+
             "   statement_message,                                                       "+
             "   charge_acct_type,                                                        "+
             "   enabled,                                                                 "+
             "   created_by                                                               "+
             "   )                                                                        "+
             "   select 'MIS'                                 as charge_type,             "+
             "          mcr.merchant_number                   as merchant_number,         "+
             "          mcr.billing_frequency                 as billing_frequency,       "+
             "          count(cbob.case_number)               as item_count,              "+
             "          ?                                     as charge_amount,           "+
             "          mcr.transit_routing                   as transit_routing,         "+
             "          mcr.dda                               as dda,                     "+
             "          mcr.start_date                        as start_date,              "+
             "          mcr.end_date                          as end_date,                "+
             "          'AM EXCESSIVE CHARGEBACK FEE'         as statement_message,       "+
             "          'CK'                                  as charge_acct_type,        "+
             "          'Y'                                   as enabled,                 "+
             "          'SYSTEM'                              as created_by               "+
             "   from   mif mf,                                                           "+
             "          organization o,                                                   "+
             "          group_merchant gm,                                                "+
             "          mbs_charge_records mcr,                                           "+
             "          network_chargeback_amex_optb cbob                                 "+
             "   where  o.org_group = ?                                                   "+
             "          and gm.org_num = o.org_num                                        "+
             "          and mf.merchant_number = gm.merchant_number                       "+
             "          and mcr.merchant_number = mf.merchant_number                      "+
             "          and mcr.start_date = ?                                            "+
             "          and mcr.end_date = trunc(last_day(?))                             "+
             "          and mcr.statement_message like '%EXCESSIVE CHARGEBACK PROGRAM%'   "+
             "          and cbob.incoming_date between ? and trunc(last_day(?))           "+
             "          and cbob.merchant_number = mcr.merchant_number                    "+
             "   group by mcr.merchant_number, mcr.billing_frequency, mcr.dda,            "+
             "          mcr.transit_routing, mcr.start_date, mcr.end_date                 ";
        paramIndex = 1;
        ps = con.prepareStatement(qs);
        ps.setDouble(paramIndex++, CBFeePerItem);
        ps.setLong(paramIndex++, NodeId);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        ps.setDate(paramIndex++, ActiveDate);
        count = ps.executeUpdate();
        commit();
        ps.close();
        rs.close();
        log.info("AM EXCESSIVE CHARGEBACK FEE - insert total "+ count + " records");
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
    return( true );
  }

  public void setActiveDate( Date activeDate )
  {
    ActiveDate = activeDate;
  }

  public void setNodeId( long nodeId )
  {
    NodeId = nodeId;
  }

  public static void main(String[] args)
  {
    AmexOptBlueECPBilling event = null;

    try
    {
      event = new AmexOptBlueECPBilling();
      event.connect();

      if( args.length > 0 && args[0].equals("process") )
      {
        event.setActiveDate( DateTimeFormatter.parseSQLDate( args[1].toString(), "MM/dd/yyyy") );
        event.setNodeId( Long.parseLong(args[2]) );
        log.info("execute() ActiveDate " + args[1].toString() + "   NodeId " + Long.parseLong(args[2]));
        event.execute();
      }
      else
      {
        log.info("invalid arguments");
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      try{ event.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}