package com.mes.startup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.BlobHandler;

public class AmexOptblueDisputeDocsArchiveCreator extends AmexFileBase {

	private static final long serialVersionUID = 1L;	
	static Logger log = Logger.getLogger(AmexOptblueDisputeDocsArchiveCreator.class);

	SimpleDateFormat sdf = new SimpleDateFormat("yyDDDHHmmss"); 
	
	/**
	 *  Initializes SFTP endpoints 
	 */
	public AmexOptblueDisputeDocsArchiveCreator() {
		
		// TODO Auto-generated constructor stub
		DkOutgoingHost = MesDefaults.DK_AMEX_OUTGOING_HOST;
		DkOutgoingUser = MesDefaults.DK_AMEX_OUTGOING_USER;
		DkOutgoingPassword = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
		DkOutgoingPath = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH;
		SettlementAddrsNotify = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
		SettlementAddrsFailure = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
		DkOutgoingUseBinary = false;
		DkOutgoingSendFlagFile = true;
	}
	
	/**
	 * Point of entry
	 * @return
	 */
	protected boolean processTransactions() {
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int colIndex = 1;

		int recsToProcess =  0;
		long disputeImginSequence = 0;
				    
		try {
			connect( true );
			Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());			

			// a. Determine if there are any load file ids / load secs ready to be processed
			pStmt = con.prepareStatement( SQL_GET_DISPUTE_IMGIN_RECORDS_TO_PROCESS.toString() );
			rs = pStmt.executeQuery();
			
			if( rs.next() ) {
				recsToProcess =  rs.getInt( colIndex );
			}
			
			if ( recsToProcess > 0 ) { 
				// b. Get the sequence
				pStmt = con.prepareStatement( SQL_GET_DISPUTE_IMGIN_SEQ.toString() );
				rs = pStmt.executeQuery();
				
				if( rs.next() ) {
					disputeImginSequence = rs.getLong( colIndex );
				}
				
				// c. Update all records to process with sequence
				pStmt = con.prepareStatement( SQL_UPDATE_DISPUTE_IMGIN_PROCESS.toString() );
				pStmt.setLong( paramIndex, disputeImginSequence );
				
				int count = pStmt.executeUpdate();
				
				// d. Read load filenames to process (CBDIS, RESPN)
				pStmt = con.prepareStatement( SQL_GET_DISPUTE_IMGIN_LOADFILENAMES.toString() );
				pStmt.setLong( paramIndex, disputeImginSequence );
				
				rs = pStmt.executeQuery();
				
				// Create and SFTP archives. Record timestamp begin and end
				while ( rs.next() ) {
					colIndex = 1;
					
					long recId = rs.getLong( colIndex++ );
					String loadFilename = rs.getString( colIndex++ );
					int process_type = rs.getInt( colIndex++ );
					long batch_id = rs.getLong( colIndex++ ); 
					
					recordTimestampBegin( recId , beginTime);
					log.debug("Processing " + loadFilename);
					
					createAndSendArchive( loadFilename, process_type, batch_id ); 					
					
					recordTimestampEnd( recId , beginTime);					
				}				
				
				// e. Read load secs to process (IMGIN error)
				pStmt = con.prepareStatement( SQL_GET_DISPUTE_IMGIN_LOADSECS.toString() );
				pStmt.setLong( paramIndex, disputeImginSequence );
				
				rs = pStmt.executeQuery();
				
				HashMap<Long, ArrayList<ErrProcessInfo>> imginErrorBatches = new HashMap(); 
				while( rs.next() ) {
					
					long batch_id = rs.getLong( "batch_id" ); 
					
					ArrayList<ErrProcessInfo> loadSecs = new ArrayList<ErrProcessInfo>();
					if( imginErrorBatches.containsKey( batch_id ) ) {
						loadSecs = imginErrorBatches.get( batch_id );
					} 
					
					loadSecs.add( new ErrProcessInfo(rs) );
					imginErrorBatches.put(batch_id, loadSecs);					
				}
				
				// Create and SFTP archives. Record timestamp begin and end				
				pStmt = con.prepareStatement( SQL_UPDATE_AMEX_OPTB_IMGER.toString() );
				for ( Map.Entry<Long, ArrayList<ErrProcessInfo>> entry : imginErrorBatches.entrySet()) {
					
					long batchId = entry.getKey();
					ArrayList<ErrProcessInfo> loadSecs = entry.getValue();
					
					log.debug("Processing batchId = " + batchId);
					for(ErrProcessInfo epi : loadSecs) {
						log.debug("Processing recId = " + epi.getRecId());
						recordTimestampBegin( epi.getRecId() , beginTime);
					}
					
					String arcName = createAndSendArchive(batchId, loadSecs); 					
					
					for(ErrProcessInfo epi : loadSecs) {
						recordTimestampEnd( epi.getRecId() , beginTime);
						
						try {
							paramIndex = 1;
							pStmt.setString( paramIndex++, arcName );
							pStmt.setString( paramIndex++, arcName );						
							pStmt.setLong( paramIndex++, epi.getLoadSec() );
							pStmt.setString( paramIndex++, batchId+"" );
							
							pStmt.executeUpdate();
							log.debug("Updated arcName = "+ arcName + ", batchId = " + batchId + ", load_sec = "+epi.getLoadSec());
						} catch(Exception e) {
							log.error(e.getMessage());							
						}
					}					
				}				
 
			}
			  	     
		} catch(Exception e) {
			logEvent( this.getClass().getName(), "execute()", e.toString() );
			logEntry( "execute()", e.toString() );
			
		} finally {
			try	{  
				if (rs != null ) rs.close(); 
			} catch( Exception ignore ) {}
			
			try	{  
				if (pStmt != null ) pStmt.close(); 
			} catch( Exception ignore ) {}
		}

		return true;
	}
	
	
	private void createAndSendArchive(String loadFilename, int process_type, long batch_id) {		
		boolean success = false;
		
		disputeDocs.clear();
		initDocsMap(process_type, loadFilename, null);
		
		try{
		if( disputeDocs != null && disputeDocs.size() > 0) {
			List<String> list = CreateIndexFileAndDocList(batch_id, process_type);
			Set <String> filesList = new HashSet<String>();
			filesList.addAll(list);
			for (String fname : filesList) {
				log.info("Sending file " + fname);
				if (!fname.endsWith(".idx")) {
					success = sendDataFile(fname, MesDefaults.getString(DkOutgoingHost),
							MesDefaults.getString(DkOutgoingUser), MesDefaults.getString(DkOutgoingPassword),
							MesDefaults.getString(DkOutgoingPath), true, false);
				} else {
					success = sendDataFile(fname, MesDefaults.getString(DkOutgoingHost),
							MesDefaults.getString(DkOutgoingUser), MesDefaults.getString(DkOutgoingPassword),
							MesDefaults.getString(DkOutgoingPath), false, true);
				}

				if (success == true) {
					log.debug("archiving file");
					archiveDailyFile(fname);
				}
			}

		}
		}catch(Exception e){
			log.error("createAndSendArchive():"+e.getMessage());
		}
	}
	
	private String createAndSendArchive(long batch_id, ArrayList<ErrProcessInfo> loadSecs) {		
		String archiveName = null;
		boolean success = false;

		disputeDocs.clear();
		initDocsMap(PROC_TYPE_IMGINER, null, loadSecs);
		
		try{
		if( disputeDocs != null && disputeDocs.size() > 0) {
			List<String> list = CreateIndexFileAndDocList(batch_id, PROC_TYPE_IMGINER);
			Set <String> filesList = new HashSet<String>();
			filesList.addAll(list);
			for (String fname : filesList) {
				log.info("Sending file " + fname);
				if (!fname.endsWith(".idx")) {
					success = sendDataFile(fname, MesDefaults.getString(DkOutgoingHost),
							MesDefaults.getString(DkOutgoingUser), MesDefaults.getString(DkOutgoingPassword),
							MesDefaults.getString(DkOutgoingPath), true, false);
				} else {
					success = sendDataFile(fname, MesDefaults.getString(DkOutgoingHost),
							MesDefaults.getString(DkOutgoingUser), MesDefaults.getString(DkOutgoingPassword),
							MesDefaults.getString(DkOutgoingPath), false, true);
				}

				if (success == true) {
					log.debug("archiving file");
					archiveDailyFile(fname);
				}
			}
				
		}
		} catch(Exception e){
			log.error("createAndSendArchive():"+e.getMessage());
		}
		
		return archiveName;
	}
	
	private void initDocsMap( int process_type, String loadFilename, ArrayList<ErrProcessInfo> loadSecs ) {		
		  
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		try {
			
			// For CBDIS and RESPN images
			if ( process_type == PROC_TYPE_CBDIS || process_type == PROC_TYPE_RESPN ) {				
				if ( process_type == PROC_TYPE_CBDIS ) {
					pStmt = con.prepareStatement( SQL_GET_CBDIS_DOCS.toString() );
					pStmt.setString(paramIndex++, loadFilename);
					pStmt.setLong(paramIndex++, loadFilenameToLoadFileId( loadFilename ) );
					
					rs = pStmt.executeQuery();
					
				} else if ( process_type == PROC_TYPE_RESPN  ) {
					pStmt = con.prepareStatement( SQL_GET_RESPN_DOCS.toString() );
					pStmt.setString(paramIndex++, loadFilename);
					pStmt.setLong(paramIndex++, loadFilenameToLoadFileId( loadFilename ) );
					
					rs = pStmt.executeQuery();			
				} 
			
				while ( rs.next() ) {
					DocInfo doc = new DocInfo( rs );
					
					List < DocInfo > list = null;
					if( disputeDocs.containsKey( doc.getCaseNumber() ) ) {
						list = disputeDocs.get( doc.getCaseNumber() );
					} else {
						list = new ArrayList<DocInfo>();					
					}
					
					list.add( doc );
					disputeDocs.put( doc.getCaseNumber(), list );
				}
			}
			// For IMGIN ERR 
			else if ( process_type == PROC_TYPE_IMGINER ) {
				pStmt = con.prepareStatement( SQL_GET_IMGINERR_DOCS.toString() );
				
				for(ErrProcessInfo errInfo : loadSecs) {
					paramIndex = 1;
					pStmt.setLong(paramIndex++, errInfo.getLoadSec());
					pStmt.setLong(paramIndex++, errInfo.getLoadSec());
					
					rs = pStmt.executeQuery();
					if( rs.next() ) {
						DocInfo doc = new DocInfo( rs );
						
						List < DocInfo > list = null;
						if( disputeDocs.containsKey( doc.getCaseNumber() ) ) {
							list = disputeDocs.get( doc.getCaseNumber() );
						} else {
							list = new ArrayList<DocInfo>();					
						}
						
						list.add( doc );
						disputeDocs.put( doc.getCaseNumber(), list );
					}
					
				}
			}  	     
		} catch(Exception e) {
			logEntry("getListOfDocs ", e.toString());
			e.printStackTrace();
			
			disputeDocs = null; // Devon
		} finally {
			try	{  
				if (rs != null ) rs.close(); 
			} catch( Exception ignore ) {}
			
			try	{  
				if (pStmt != null ) pStmt.close(); 
			} catch( Exception ignore ) {}
		}
		
		//return disputeDocs;
	}
	
	private List<String> CreateIndexFileAndDocList( long batchId, int process_type ){
		
		List<String> fileNameList = new ArrayList<String>();
		PreparedStatement pStmt = null;
		
		FileWriter indexStream = null;
		
		try{
			long recordCount = 0L;
			int paramIndex = 1;
			recordCount = disputeDocs.size();
			log.debug("record count is :"+recordCount);
			
			String fileNamePrefix = (process_type == PROC_TYPE_IMGINER ? R_ZIP_PREFIX : ZIP_PREFIX) 
					+ DateTimeFormatter.getFormattedDate(new Date(),"MMddyy") + "_" 
					+ String.format("%04d", recordCount) + "_" 
					+ String.format("%07d", batchId);
			
			String indexFileName = (process_type == PROC_TYPE_IMGINER ? R_ZIP_PREFIX : ZIP_PREFIX) 
					+ DateTimeFormatter.getFormattedDate(new Date(),"MMddyy") + "_" 
					+ String.format("%04d", recordCount) + "_" 
					+ String.format("%07d", batchId) //+ "_" 
					//+ sdf.format( new Date() )
					+ ".idx";
			indexStream = new FileWriter(indexFileName);
			BufferedWriter indexOut = new BufferedWriter(indexStream);
			
			// This is more for record
			pStmt = con.prepareStatement( SQL_CREATE_LOAD_FILE_INDEX.toString() );
			pStmt.setString(paramIndex, indexFileName);				
			pStmt.executeUpdate();
			
			int sequenceRecordNumber = 0;
			for(Map.Entry<String, List<DocInfo>> entry : disputeDocs.entrySet()) {
				
				String caseNumber = entry.getKey();				 
				log.debug( caseNumber );
				boolean added = false;
				
				for(DocInfo doc : entry.getValue()) {
					log.debug( "Adding " + doc.getDocId() );
					BlobHandler bh = new BlobHandler();
					BlobHandler.BlobDocData document = bh.getData(doc.getDocId());
					byte[] docBody = document.getBody();
					
					File filename = new File(fileNamePrefix + "_" +document.getFileName().replaceAll("[^a-zA-Z0-9.]+", ""));
					FileOutputStream fop = new FileOutputStream(filename);
					
					fop.write(docBody);
					fop.flush();
					fop.close();
					
					fileNameList.add(filename.toString());
					added = true;
				}
				
				if(added){
					addRecordToIndexFile( entry.getValue(), indexOut, ++sequenceRecordNumber, batchId, fileNamePrefix );
					    fileNameList.add(indexFileName);
				}
				
			}
			
		} catch(Exception e){
			
		}
		
		return fileNameList;
	}
		
	// Examples of Document Records with various file formats (PDF, TIF, MTIF):
	//	000001|BTCH_ID=0522396|DOC_RCV_DT=03/11/2014|DOC_CTGY_CD_1=SRE|DOC_SOURCE_CD=VEND|CM_ACCT_NO=33
	//	3333333333333|CASE_NO=TC363|SE_ACCT_NO=9999999999|~DocType=SMART|AppFormat=PDF|Filename=198219_61634.p
	//	df
	//
	//	000002|BTCH_ID=0522396|DOC_RCV_DT=03/11/2014|DOC_CTGY_CD_1=SRE|DOC_SOURCE_CD=VEND|CM_ACCT_NO=37
	//	8298148611001|CASE_NO=12345678901|SE_ACCT_NO=1234567890|~DocType=SMART|AppFormat=tif|Filename=7632004F.tif
	//	~7632004B.tif
	//	
	//	000003|BTCH_ID=0522396|DOC_RCV_DT=03/11/2014|DOC_CTGY_CD_1=SRE|DOC_SOURCE_CD=VEND||CM_ACCT_NO=37
	//	9460617461008|CASE_NO=12345678901|SE_ACCT_NO=1234567890|~DocType=SMART|AppFormat=mtif|~Pages=3|Filename=
	//	7632004F.tif	
	private void addRecordToIndexFile( List<DocInfo> docs, BufferedWriter out, int sequenceRecordNumber, long batchId, String fileNameprefix ) {
		SimpleDateFormat cdf = new SimpleDateFormat("MM/dd/yyyy");
		String cardNumber = "";
		String caseNumber = "";
		String seNumber = "";
		
		try {
			
			String fileExtension = "";
			StringBuffer fileNames = new StringBuffer("");
			
			for( DocInfo doc : docs ) {
				BlobHandler bh = new BlobHandler();
				BlobHandler.BlobDocData document = bh.getData( doc.getDocId() );
				
				fileExtension = document.getExtension();
				if(!fileNames.toString().equals("")) 
					fileNames.append("~");
				fileNames.append( fileNameprefix +  "_"  + document.getFileName().replaceAll("[^a-zA-Z0-9.]+", ""));
				
				cardNumber = doc.getCardNumber();
				caseNumber = doc.getCaseNumber();
				seNumber = doc.getSeAccountNumber();
			}
			
			// SAMPLE : "000003|BTCH_ID=0522396|DOC_RCV_DT=03/11/2014|DOC_CTGY_CD_1=SRE|DOC_SOURCE_CD=VEND||CM_ACCT_NO=37...";
			out.append(zeroPad(sequenceRecordNumber));
			out.append('|');
			out.append("BTCH_ID=");
			out.append(String.format("%07d", batchId));
			out.append('|');
			out.append("DOC_RCV_DT=");
			out.append(cdf.format( new Date() )); // Current Date MM/DD/YYYY
			out.append('|');
			out.append("DOC_CTGY_CD_1=");
			out.append(DOC_CTGY_CD_1);
			out.append('|');
			out.append("DOC_SOURCE_CD=");
			out.append(DOC_SOURCE_CD);
			out.append('|');
			out.append("CM_ACCT_NO=");
			out.append(cardNumber); // dukpt_decrypt_wrapper(ncao.card_number_enc)
			out.append('|');
			out.append("CASE_NO=");
			out.append(caseNumber); // ncao.case_number
			out.append('|');
			out.append("SE_ACCT_NO=");
			out.append(seNumber); // ncao.se_numb
			out.append('|');
			out.append("~DocType=");
			out.append(DOC_TYPE);
			out.append('|');
			out.append("AppFormat=");
			out.append(fileExtension); 
			
			/* 08/04: Post running tests it is seen that when you attach a tiff file with multiple pages on BP platform it gets converted to pdf.
			Hence for us this scenario will never arise. Thsi can be revisited when we add mtif support.*/			
			// Note: When the mtif [multipage TIF] format is used, the ~Pages= tag between the 
			//       AppFormat and Filename tags should be present.			
			/*if (fileExtension.equalsIgnoreCase("mtif")) {
				out.append('|');
				out.append("~Pages=");
				out.append("3");  // number of pages ... Need to capture this from somewhere
			}*/
			
			out.append('|');
			out.append("Filename=");
			out.append(fileNames);  
			out.newLine();
			
			out.flush();
			
		} catch(IOException ioe) {
			logEntry("addRecordToIndexFile(" + caseNumber + ", " + seNumber +")", ioe.toString());
			ioe.printStackTrace();
			
		} catch( Exception e ) {
			e.printStackTrace();
			
		}
	}

	private static String zeroPad(int num) {
		//	    if (6 < String.valueOf(num).length()) {
		//	        // NOTE : right now we are simply truncating the leading digit if the number is too large.
		//	    } else 
	    if (6 == String.valueOf(num).length()) {
	        return String.valueOf(num);
	    } else {
	        StringBuilder sb = new StringBuilder();
	        for (int i = 0; i < 6; i++) {
	            sb.append("0");
	        }
	        sb.append(String.valueOf(num));
	        return sb.substring(sb.length() - 6, sb.length());
	    }
	}
	
	public void recordTimestampBegin(long recId, Timestamp beginTime) {
		PreparedStatement pStmt = null;
		int paramIndex = 1;
						    
		try {
			pStmt = con.prepareStatement( SQL_UPDATE_DISPUTE_IMGIN_BEGINTIME.toString() );
			pStmt.setTimestamp(paramIndex, beginTime);
			pStmt.setLong(++paramIndex, recId);
			pStmt.executeUpdate();
			  	     
		} catch(Exception e) {
			logEntry("recordTimestampBegin ",e.toString());
			
		} finally {
			try	{  
				if (pStmt != null ) pStmt.close(); 
			} catch( Exception e ) {
				log.error("exception while closing stmnt",e);
			}
		}
		
	}
	
	
	public void recordTimestampEnd(long recId, Timestamp beginTime) {
		PreparedStatement pStmt = null;
		int paramIndex = 1;
		Timestamp endTime = null;
		long elapsed = 0L;
		
		try {
		  
			endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
			elapsed = (endTime.getTime() - beginTime.getTime());
			String elapsedTime = DateTimeFormatter.getFormattedTimestamp(elapsed);
			pStmt = con.prepareStatement(SQL_UPDATE_DISPUTE_IMGIN_ENDTIME.toString());
			pStmt.setString(paramIndex, elapsedTime);
			pStmt.setLong(++paramIndex, recId);
			pStmt.executeUpdate();
			  	     
		} catch(Exception e) {
			logEntry("recordTimestampEnd ",e.toString());
			
		} finally {
			try	{  
				if (pStmt != null ) pStmt.close(); 
			} catch( Exception e ) {
				log.error("exception while closing stmnt",e);
			}
		}
		
	}
	
	
	public static void main(String args[]) {
		AmexOptblueDisputeDocsArchiveCreator disputesDocCreator = null;
		int idx = 0;
		
		try {
			if (args.length > 0 && args[idx].equals("printtestprops")) {
					EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
							MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_INCOMING_PATH });				
			} 

			disputesDocCreator = new AmexOptblueDisputeDocsArchiveCreator();
			disputesDocCreator.connect();
			disputesDocCreator.setEventArgs(args);
			disputesDocCreator.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			try {
				disputesDocCreator.cleanUp();
			} catch (Exception ignore) { }
						
		}
		
		Runtime.getRuntime().exit(0);
	}

	// List of Docs for which archive is to be created
	Map< String, List< DocInfo > > disputeDocs = new HashMap < String, List < DocInfo > > ();
	
	// Index Record Constants
	private static final String DOC_CTGY_CD_1 = "SRE";
	private static final String DOC_SOURCE_CD = "VEND";
	private static final String DOC_TYPE = "SMART";
		
	// Load Types
	public final static int PROC_TYPE_CBDIS = 1;
	public final static int PROC_TYPE_RESPN = 2;
	public final static int PROC_TYPE_IMGINER = 3;
	
	// Zip FileName prefix
	//private final static String ZIP_PREFIX = "MERCH-5000-";
	private final static String ZIP_PREFIX = "amex_img_";
	private final static String R_ZIP_PREFIX = "amex_imgr";

	
	// ************** SQL Queries	
	private static final StringBuffer SQL_GET_DISPUTE_IMGIN_RECORDS_TO_PROCESS  = new StringBuffer("");
	private static final StringBuffer SQL_GET_DISPUTE_IMGIN_SEQ  = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_DISPUTE_IMGIN_PROCESS = new StringBuffer();
	private static final StringBuffer SQL_GET_DISPUTE_IMGIN_LOADFILENAMES = new StringBuffer();
	private static final StringBuffer SQL_GET_DISPUTE_IMGIN_LOADSECS = new StringBuffer();
	private static final StringBuffer SQL_UPDATE_DISPUTE_IMGIN_BEGINTIME = new StringBuffer();
	private static final StringBuffer SQL_UPDATE_DISPUTE_IMGIN_ENDTIME = new StringBuffer();
	private static final StringBuffer SQL_CREATE_LOAD_FILE_INDEX = new StringBuffer(); 
	private static final StringBuffer SQL_UPDATE_AMEX_OPTB_IMGER = new StringBuffer();
	
	private static final StringBuffer SQL_GET_CBDIS_DOCS = new StringBuffer("");
	private static final StringBuffer SQL_GET_RESPN_DOCS = new StringBuffer(""); 
	private static final StringBuffer SQL_GET_IMGINERR_DOCS = new StringBuffer("");
	
	private static final String NL = " \n";
	
	static {
		
		SQL_GET_DISPUTE_IMGIN_RECORDS_TO_PROCESS.append ( 
			"select count(1) " + NL + 
			"from dispute_imgin_process" + NL +
			"where process_sequence is null" + NL );
		
		SQL_GET_DISPUTE_IMGIN_SEQ.append(
			"select  mes.dispute_imgin_process_seq.nextval " + NL +
			"from dual");
		
		SQL_UPDATE_DISPUTE_IMGIN_PROCESS.append (
			"update dispute_imgin_process " + NL +
			"set process_sequence = ? " + NL +
			"where process_sequence is null" + NL );
		
		SQL_GET_DISPUTE_IMGIN_LOADFILENAMES.append (
			"select rec_id, load_filename, process_type, batch_id " + NL +
			"from dispute_imgin_process " + NL +
			"where process_sequence = ? " + NL + 
			"and process_type in (" + PROC_TYPE_CBDIS + ", " + PROC_TYPE_RESPN + ")");
		
		SQL_GET_DISPUTE_IMGIN_LOADSECS.append (
				"select rec_id, batch_id, load_sec " + NL +
				"from dispute_imgin_process " + NL +
				"where process_sequence = ? " + NL + 
				"and process_type = " + PROC_TYPE_IMGINER);
		
		SQL_UPDATE_DISPUTE_IMGIN_BEGINTIME.append (
			"update dispute_imgin_process" + NL +
	        "set process_begin_date = ?" + NL +
	        "where rec_id = ?" + NL );
		
		SQL_UPDATE_DISPUTE_IMGIN_ENDTIME.append (
				"update dispute_imgin_process" + NL +
		        "set process_end_date = sysdate" + NL +
		        ", process_elapsed = ?" + NL +
		        "where rec_id = ?" + NL );
		
		SQL_GET_CBDIS_DOCS.append (
			"select ncao.load_sec load_sec, cd.date_submitted, cd.doc_id doc_id, ncao.case_number case_no, dukpt_decrypt_wrapper(ncao.card_number_enc) cm_acct_no, ncao.se_numb se_acct_no" + NL + 
			"from network_chargeback_amex_optb ncao, network_chargebacks nc, chargeback_docs cd " + NL + 
			"where ncao.output_filename = ?" + NL + 
			"	and ncao.output_file_id = ?" + NL + 
			"   and ncao.load_sec = nc.cb_load_sec" + NL + 
			//"   and nc.review_date is not null" + NL + 		// Debated on this and then took it out as there is no hard check on BP for a doc to be reviewed, its more a process thing. 
			"   and ncao.load_sec = cd.cb_load_sec " + NL + 
			"   and exists (select 1 from document where doc_id = cd.doc_id ) ");
		
		SQL_GET_RESPN_DOCS.append (
			"select nrao.load_sec load_sec, cd.date_submitted, cd.doc_id doc_id, nrao.inquiry_case_number case_no, dukpt_decrypt_wrapper(nrao.card_number_enc) cm_acct_no, nrao.se_number se_acct_no" + NL + 
			"from network_retrieval_amex_optb nrao, network_retrievals nr, chargeback_docs cd " + NL + 
			"where nrao.output_filename = ? " + NL +  
			"	and nrao.output_file_id = ? " + NL +
			" 	and nrao.load_sec = nr.retr_load_sec " + NL + 
			" 	and nrao.load_sec = cd.cb_load_sec " + NL + 
			" 	and exists (select 1 from document where doc_id = cd.doc_id ) " );
		
		SQL_GET_IMGINERR_DOCS.append(
			"select ncao.load_sec load_sec, cd.date_submitted, cd.doc_id doc_id, ncao.case_number case_no, dukpt_decrypt_wrapper(ncao.card_number_enc) cm_acct_no, to_char(ncao.se_numb) se_acct_no" + NL + 
			"from network_chargeback_amex_optb ncao, network_chargebacks nc, chargeback_docs cd " + NL + 
			"where ncao.load_sec = ?" + NL + 
			"	and ncao.load_sec = nc.cb_load_sec" + NL + 
			"   and ncao.load_sec = cd.cb_load_sec " + NL + 
			"   and exists (select 1 from document where doc_id = cd.doc_id )" + NL + 
			"union       " + NL + 
			"select nrao.load_sec load_sec, cd.date_submitted, cd.doc_id doc_id, nrao.inquiry_case_number case_no, dukpt_decrypt_wrapper(nrao.card_number_enc) cm_acct_no, nrao.se_number se_acct_no" + NL + 
			"from network_retrieval_amex_optb nrao, network_retrievals nr, chargeback_docs cd " + NL + 
			"where nrao.load_sec = ?" + NL + 
			"	and nrao.load_sec = nr.retr_load_sec " + NL + 
			"   and nrao.load_sec = cd.cb_load_sec " + NL + 
			"   and exists (select 1 from document where doc_id = cd.doc_id ) " );
		
		SQL_CREATE_LOAD_FILE_INDEX.append(
				"call load_file_index_init(?)");
		
		SQL_UPDATE_AMEX_OPTB_IMGER.append(
				"update amex_optb_imger" + NL + 
				"set output_filename = ?, output_file_id = load_filename_to_load_file_id(?)" + NL + 
				"where load_sec = ?" + NL + 
				"and batch_id = ?");
	}
		
	private class DocInfo {
		long loadSec;
		long docId;
		String caseNumber;
		String cardNumber;
		String seAccountNumber;
				
		public DocInfo(ResultSet rs) {
			try {
				loadSec = rs.getLong("load_sec");
				docId = rs.getLong("doc_id");
				caseNumber = rs.getString("case_no");
				cardNumber = rs.getString("cm_acct_no");
				seAccountNumber = rs.getString("se_acct_no");
			} catch(java.sql.SQLException sqe) {
				
			}
		}
	
		public long getLoadSec() {
			return loadSec;
		}
	
		public long getDocId() {
			return docId;
		}
	
		public String getCaseNumber() {
			return caseNumber;
		}
	
		public String getCardNumber() {
			return cardNumber;
		}
	
		public String getSeAccountNumber() {
			return seAccountNumber;
		}
		
	}
	
	private class ErrProcessInfo {
		long loadSec;
		long recId;
				
		public ErrProcessInfo(ResultSet rs) {
			try {
				loadSec = rs.getLong("load_sec");
				recId = rs.getLong("rec_id");
			} catch(java.sql.SQLException sqe) {
				
			}
		}
		
		public long getLoadSec() {
			return loadSec;
		}
	
		public long getRecId() {
			return recId;
		}
	
	 }

}