/*@lineinfo:filename=CieloActivityDetailEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/CieloActivityDetailEvent.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2012-11-08 11:09:10 -0800 (Thu, 08 Nov 2012) $
  Version            : $Revision: 20687 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class CieloActivityDetailEvent extends EventBase
{
  public class NetRevenueWorker 
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   Date          ActivityDate  = null;
    protected   Date          LastMonth     = null;
    protected   ActivityData  MerchantData  = null;
    protected   long          MerchantId    = 0L;
    protected   Hashtable     BasisPoints   = null;
    protected   int           LastDay       = 0;
    
    public NetRevenueWorker( ActivityData summaryData, Date activityDate, Date lastMonth, Hashtable BP, int lastDay )
      throws Exception
    {
      ActivityDate  = activityDate;
      LastMonth     = lastMonth;
      MerchantId    = summaryData.MerchantNumber;
      MerchantData  = summaryData;
      BasisPoints   = BP;
      LastDay       = lastDay;
    }
    
    public void run()
    {
      double                netRevenue        = 0.0;
      
      try
      {
        connect(true);
        
        // get channel for this merchant
        String channel = "";
        /*@lineinfo:generated-code*//*@lineinfo:72^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct case 
//                      when comm.login_name is not null then 'Tele Sales' 
//                      when cl.channel_2013 is null and mf.bank_number = 3858 then 'SE Sales'
//                      else nvl(cl.channel_2013,'Uncategorized')
//                    end as channel
//            
//            from    mif mf,
//                    channel_link_2013 cl,
//                    (
//                      select  cm.merchant_number  merchant_number,
//                              u.login_name        login_name
//                      from    commissions cm,
//                              users u,
//                              inside_sales_reps isr
//                      where   cm.user_id = u.user_id
//                              and u.login_name = isr.login_name
//                              and cm.merchant_number > 941000091988
//                      group by cm.merchant_number, u.login_name
//                    ) comm
//            where   mf.merchant_number = :MerchantId
//                    and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)
//                    and mf.merchant_number = comm.merchant_number(+)                                             
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct case \n                    when comm.login_name is not null then 'Tele Sales' \n                    when cl.channel_2013 is null and mf.bank_number = 3858 then 'SE Sales'\n                    else nvl(cl.channel_2013,'Uncategorized')\n                  end as channel\n           \n          from    mif mf,\n                  channel_link_2013 cl,\n                  (\n                    select  cm.merchant_number  merchant_number,\n                            u.login_name        login_name\n                    from    commissions cm,\n                            users u,\n                            inside_sales_reps isr\n                    where   cm.user_id = u.user_id\n                            and u.login_name = isr.login_name\n                            and cm.merchant_number > 941000091988\n                    group by cm.merchant_number, u.login_name\n                  ) comm\n          where   mf.merchant_number =  :1 \n                  and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)\n                  and mf.merchant_number = comm.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   channel = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^9*/        
        
        double bp = 0.0;
        
        try
        {
          bp = ((Double)(BasisPoints.get(channel))).doubleValue();
        }
        catch(Exception e)
        {
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:108^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(round((sum(sm.bank_sales_amount
//                                   + sm.nonbank_sales_amount
//                                   - sm.bank_reject_sales_amount
//                                   - sm.nonbank_reject_sales_amount) * :bp),2),0) as net_revenue
//            
//            from    daily_detail_file_summary sm
//            where   sm.merchant_number = :MerchantId
//                    and sm.batch_date = :ActivityDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(round((sum(sm.bank_sales_amount\n                                 + sm.nonbank_sales_amount\n                                 - sm.bank_reject_sales_amount\n                                 - sm.nonbank_reject_sales_amount) *  :1 ),2),0) as net_revenue\n           \n          from    daily_detail_file_summary sm\n          where   sm.merchant_number =  :2 \n                  and sm.batch_date =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,bp);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setDate(3,ActivityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netRevenue = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^9*/

        if ( LastDay == 1 )
        {
          double feeNetRevenue = 0.0;
        
          /*@lineinfo:generated-code*//*@lineinfo:124^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(charge_amount),0)   as net_fees
//              
//              from    mbs_charge_records    mcr,
//                      mif                   mf
//              where   mf.merchant_number = :MerchantId
//                      and mf.date_stat_chgd_to_dcb is null                
//                      and mf.merchant_number = mcr.merchant_number
//                      and substr(mcr.billing_frequency,to_char(:LastMonth,'mm'),1) = 'Y'
//                      and :LastMonth between mcr.start_date and mcr.end_date
//                      and ( mcr.statement_message like '%PCI%'
//                            or mcr.statement_message like '%IRS%'
//                            or mcr.statement_message like '%ADMINISTRATION%'
//                            or mcr.statement_message like '%COMPLIANCE%'
//                            or mcr.statement_message like '%1099 %' )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(charge_amount),0)   as net_fees\n             \n            from    mbs_charge_records    mcr,\n                    mif                   mf\n            where   mf.merchant_number =  :1 \n                    and mf.date_stat_chgd_to_dcb is null                \n                    and mf.merchant_number = mcr.merchant_number\n                    and substr(mcr.billing_frequency,to_char( :2 ,'mm'),1) = 'Y'\n                    and  :3  between mcr.start_date and mcr.end_date\n                    and ( mcr.statement_message like '%PCI%'\n                          or mcr.statement_message like '%IRS%'\n                          or mcr.statement_message like '%ADMINISTRATION%'\n                          or mcr.statement_message like '%COMPLIANCE%'\n                          or mcr.statement_message like '%1099 %' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,LastMonth);
   __sJT_st.setDate(3,LastMonth);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   feeNetRevenue = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^11*/
        
          netRevenue += feeNetRevenue;
        }
        
        MerchantData.setNetRevenue(netRevenue);
      }
      catch( Exception e )
      {
        logEntry("run(" + MerchantId + "," + ActivityDate + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public static class ActivityData
  {
    long          MerchantNumber      = 0L;
    String        Category            = "";
    double        AchAmount           = 0.0;
    int           AchCount            = 0;
    Date          ActivityDate        = null;
    int           BankNumber          = 0;
    double        CbIncomingAmount    = 0.0;
    int           CbIncomingCount     = 0;
    double        CbWorkedAmount      = 0.0;
    int           CbWorkedCount       = 0;
    int           ClosedCount         = 0;
    int           ActivatedCount     = 0;
    double        FxRevenue           = 0.0;
    double        FxProcRevenue       = 0.0;
    int           OpenedCount         = 0;
    double        ProcRevenue         = 0.0;
    double        SalesAmount         = 0.0;
    int           SalesCount          = 0;
    double        NetRevenue          = 0.0;
    Date          OpenDate            = null;
    Date          CloseDate           = null;
    Date          ActivatedDate       = null;
  
    public ActivityData( int bankNumber, Date activityDate )
    {
      BankNumber      = bankNumber;
      ActivityDate    = activityDate; 
    }
    
    public void setMerchantData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantNumber  = resultSet.getLong("merchant_number");
      Category        = resultSet.getString("category");
      OpenDate        = resultSet.getDate("date_opened");
      ActivatedDate   = resultSet.getDate("activation_date");
      CloseDate       = resultSet.getDate("close_date");
      
    }
    
    public void setNetRevenue( double nr )
    {
      NetRevenue = nr;
    }
    
    public void setAch( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AchCount  = resultSet.getInt("ach_count");
      AchAmount = resultSet.getDouble("ach_amount");
    }
    
    public void setFxRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      FxRevenue     = resultSet.getDouble("fx_revenue");
      FxProcRevenue = resultSet.getDouble("fx_proc_revenue");
    }
    
    public void setIncomingChargebacks( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CbIncomingCount  = resultSet.getInt("incoming_count");
      CbIncomingAmount = resultSet.getDouble("incoming_amount");
    }
    
    public void setOpenClosed( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OpenedCount  = resultSet.getInt("opened_count");
      ClosedCount  = resultSet.getInt("closed_count");
    }
    
    public void setActivatedCount(ResultSet resultSet)
      throws java.sql.SQLException
    {
        ActivatedCount = resultSet.getInt("activated_count");
    }

    public void setProcessingRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ProcRevenue  = resultSet.getDouble("proc_revenue");
    }
    
    public void setSales( ResultSet resultSet )
      throws java.sql.SQLException
    {
      SalesCount  = resultSet.getInt("sales_count");
      SalesAmount = resultSet.getDouble("sales_amount");
    }
    
    public void setWorkedChargebacks( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CbWorkedCount  = resultSet.getInt("mchb_count");
      CbWorkedAmount = resultSet.getDouble("mchb_amount");
    }
  }
  
  private double    FxBp            = 0.03900;
  private double    FxProcBp        = 0.01296;
  //private double    NetRevenue      = 0.0;
  private double    ProcBp          = 0.00205;
  
  public boolean execute( )
  {
    boolean     retVal        = false;
  
    try
    {
      connect(true);
      setAutoCommit(false);
    
      Date  activityDate    = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate-1)  
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate-1)  \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^7*/
      
      loadData( activityDate );
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public void loadData( Date activityDate )
  {
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;
    Vector              merchantRecs    = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:305^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                    as merchant_number,
//                  mif_date_opened(date_opened)          as date_opened,
//                  mf.activation_date                    as activation_date,
//                  mf.date_stat_chgd_to_dcb              as close_date,
//                  case
//                    when comm.login_name is not null 
//                      then 'Tele Sales'
//                    when mf.bank_number = 3858
//                      then 'SE Sales'                    
//                    else
//                      nvl(cl.channel_2013, 'Uncategorized')
//                  end                                   as category, 
//                  sum(case when mif_date_opened(mf.date_opened) = :activityDate then 1 else 0 end) as opened_count,
//                  sum(case when mf.date_stat_chgd_to_dcb = :activityDate then 1 else 0 end)        as closed_count,
//                  sum(case when mf.activation_date = :activityDate then 1 else 0 end)        as activated_count,
//                  nvl(ncb.incoming_count,0)   as incoming_count,
//                  nvl(ncb.incoming_amount,0)  as incoming_amount,                     
//                  nvl(mchb.mchb_count,0)      as mchb_count,
//                  nvl(mchb.mchb_amount,0)     as mchb_amount,                                                    
//                  nvl(ach.ach_count,0)        as ach_count,
//                  nvl(ach.ach_amount,0)       as ach_amount,                                                    
//                  nvl(fx.fx_revenue,0)        as fx_revenue,
//                  nvl(fx.fx_proc_revenue,0)   as fx_proc_revenue,                                                    
//                  sum((sm.bank_sales_count
//                      + sm.nonbank_sales_count
//                      - sm.bank_reject_sales_count
//                      - sm.nonbank_reject_sales_count) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end)  as sales_count,
//                  sum((sm.bank_sales_amount
//                      + sm.nonbank_sales_amount
//                      - sm.bank_reject_sales_amount
//                      - sm.nonbank_reject_sales_amount) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end) as sales_amount
//          from    mif mf,
//                  daily_detail_file_summary sm,
//                  channel_link_2013 cl,
//                  (
//                    select  cm.merchant_number  merchant_number,
//                            u.login_name        login_name
//                    from    commissions cm,
//                            users u,
//                            inside_sales_reps isr
//                    where   cm.user_id = u.user_id
//                            and u.login_name = isr.login_name
//                            and cm.merchant_number > 941000091988
//                    group by cm.merchant_number, u.login_name
//                  ) comm,                                     
//                  (
//                    select  cb.merchant_number      as merchant_number,
//                            count(1)                as incoming_count,
//                            nvl(sum(cb.tran_amount),0)     as incoming_amount
//                    from    network_chargebacks   cb
//                    where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)
//                            and cb.incoming_date = :activityDate
//                            and cb.first_time_chargeback = 'Y'
//                    group by cb.merchant_number                
//                  ) ncb,
//                  (
//                    select  cb.merchant_number      as merchant_number,
//                            count(1)                as mchb_count,
//                            nvl(sum(cb.tran_amount),0) as mchb_amount
//                    from    network_chargebacks           cb,
//                            network_chargeback_activity   cba
//                    where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)
//                            and cb.incoming_date between :activityDate-180 and :activityDate
//                            and cba.cb_load_sec = cb.cb_load_sec
//                            and cba.action_date = :activityDate
//                            and cba.action_code = 'D'        
//                    group by cb.merchant_number                
//                  ) mchb,
//                  (
//                    select  ach.merchant_number         as merchant_number,
//                            count(1)                    as ach_count,
//                            nvl(sum(ach.ach_amount),0)       as ach_amount
//                    from    ach_trident_detail      ach
//                    where   ach.merchant_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where owner = 1))
//                            and ach.post_date = :activityDate
//                            and ach.entry_description = 'MERCH DEP'
//                    group by ach.merchant_number                
//                  ) ach,
//                  (
//                    select  /*+ index (tapi idx_tapi_merch_batch_date) */
//                            tapi.merchant_number,
//                            nvl(round((sum(tapi.fx_amount_base) * :FxBp),2),0)     as fx_revenue,
//                            nvl(round((sum(tapi.fx_amount_base) * :FxProcBp),2),0) as fx_proc_revenue
//                    from    trident_capture_api   tapi
//                    where   tapi.merchant_number in 
//                            (
//                              select  mf.merchant_number 
//                              from    mif   mf 
//                              where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                                      and mf.association_node = 3941650500  -- 2CO
//                            )
//                            and tapi.batch_date = :activityDate
//                            and tapi.mesdb_timestamp between :activityDate-30 and :activityDate+1
//                            and tapi.debit_credit_indicator = 'D'
//                            and nvl(tapi.fx_amount_base,0) != 0
//                    group by tapi.merchant_number                
//                  ) fx
//          where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and to_char(mf.merchant_number) not like '9419%'
//                  and nvl(mf.test_account, 'N') != 'Y'
//                  and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)
//                  and mf.merchant_number = comm.merchant_number(+)
//                  and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >= (:activityDate - 180)
//                  and mf.merchant_number = sm.merchant_number(+)
//                  and sm.batch_date(+) = :activityDate
//                  and mf.merchant_number = ncb.merchant_number(+)
//                  and mf.merchant_number = mchb.merchant_number(+)
//                  and mf.merchant_number = ach.merchant_number(+)
//                  and mf.merchant_number = fx.merchant_number(+)
//          group by mf.merchant_number,
//                   mif_date_opened(date_opened),
//                   mf.activation_date,
//                   mf.date_stat_chgd_to_dcb,
//                    case
//                      when comm.login_name is not null 
//                        then 'Tele Sales'
//                      when mf.bank_number = 3858
//                        then 'SE Sales'                    
//                      else
//                        nvl(cl.channel_2013, 'Uncategorized')
//                    end, 
//                    nvl(ncb.incoming_count,0),
//                    nvl(ncb.incoming_amount,0),
//                    nvl(mchb.mchb_count,0),
//                    nvl(mchb.mchb_amount,0),
//                    nvl(ach.ach_count,0),
//                    nvl(ach.ach_amount,0),
//                    nvl(fx.fx_revenue,0),
//                    nvl(fx.fx_proc_revenue,0)                                                    
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                    as merchant_number,\n                mif_date_opened(date_opened)          as date_opened,\n                mf.activation_date                    as activation_date,\n                mf.date_stat_chgd_to_dcb              as close_date,\n                case\n                  when comm.login_name is not null \n                    then 'Tele Sales'\n                  when mf.bank_number = 3858\n                    then 'SE Sales'                    \n                  else\n                    nvl(cl.channel_2013, 'Uncategorized')\n                end                                   as category, \n                sum(case when mif_date_opened(mf.date_opened) =  :1  then 1 else 0 end) as opened_count,\n                sum(case when mf.date_stat_chgd_to_dcb =  :2  then 1 else 0 end)        as closed_count,\n                sum(case when mf.activation_date =  :3  then 1 else 0 end)        as activated_count,\n                nvl(ncb.incoming_count,0)   as incoming_count,\n                nvl(ncb.incoming_amount,0)  as incoming_amount,                     \n                nvl(mchb.mchb_count,0)      as mchb_count,\n                nvl(mchb.mchb_amount,0)     as mchb_amount,                                                    \n                nvl(ach.ach_count,0)        as ach_count,\n                nvl(ach.ach_amount,0)       as ach_amount,                                                    \n                nvl(fx.fx_revenue,0)        as fx_revenue,\n                nvl(fx.fx_proc_revenue,0)   as fx_proc_revenue,                                                    \n                sum((sm.bank_sales_count\n                    + sm.nonbank_sales_count\n                    - sm.bank_reject_sales_count\n                    - sm.nonbank_reject_sales_count) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end)  as sales_count,\n                sum((sm.bank_sales_amount\n                    + sm.nonbank_sales_amount\n                    - sm.bank_reject_sales_amount\n                    - sm.nonbank_reject_sales_amount) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end) as sales_amount\n        from    mif mf,\n                daily_detail_file_summary sm,\n                channel_link_2013 cl,\n                (\n                  select  cm.merchant_number  merchant_number,\n                          u.login_name        login_name\n                  from    commissions cm,\n                          users u,\n                          inside_sales_reps isr\n                  where   cm.user_id = u.user_id\n                          and u.login_name = isr.login_name\n                          and cm.merchant_number > 941000091988\n                  group by cm.merchant_number, u.login_name\n                ) comm,                                     \n                (\n                  select  cb.merchant_number      as merchant_number,\n                          count(1)                as incoming_count,\n                          nvl(sum(cb.tran_amount),0)     as incoming_amount\n                  from    network_chargebacks   cb\n                  where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)\n                          and cb.incoming_date =  :4 \n                          and cb.first_time_chargeback = 'Y'\n                  group by cb.merchant_number                \n                ) ncb,\n                (\n                  select  cb.merchant_number      as merchant_number,\n                          count(1)                as mchb_count,\n                          nvl(sum(cb.tran_amount),0) as mchb_amount\n                  from    network_chargebacks           cb,\n                          network_chargeback_activity   cba\n                  where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)\n                          and cb.incoming_date between  :5 -180 and  :6 \n                          and cba.cb_load_sec = cb.cb_load_sec\n                          and cba.action_date =  :7 \n                          and cba.action_code = 'D'        \n                  group by cb.merchant_number                \n                ) mchb,\n                (\n                  select  ach.merchant_number         as merchant_number,\n                          count(1)                    as ach_count,\n                          nvl(sum(ach.ach_amount),0)       as ach_amount\n                  from    ach_trident_detail      ach\n                  where   ach.merchant_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where owner = 1))\n                          and ach.post_date =  :8 \n                          and ach.entry_description = 'MERCH DEP'\n                  group by ach.merchant_number                \n                ) ach,\n                (\n                  select  /*+ index (tapi idx_tapi_merch_batch_date) */\n                          tapi.merchant_number,\n                          nvl(round((sum(tapi.fx_amount_base) *  :9 ),2),0)     as fx_revenue,\n                          nvl(round((sum(tapi.fx_amount_base) *  :10 ),2),0) as fx_proc_revenue\n                  from    trident_capture_api   tapi\n                  where   tapi.merchant_number in \n                          (\n                            select  mf.merchant_number \n                            from    mif   mf \n                            where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                                    and mf.association_node = 3941650500  -- 2CO\n                          )\n                          and tapi.batch_date =  :11 \n                          and tapi.mesdb_timestamp between  :12 -30 and  :13 +1\n                          and tapi.debit_credit_indicator = 'D'\n                          and nvl(tapi.fx_amount_base,0) != 0\n                  group by tapi.merchant_number                \n                ) fx\n        where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                and to_char(mf.merchant_number) not like '9419%'\n                and nvl(mf.test_account, 'N') != 'Y'\n                and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)\n                and mf.merchant_number = comm.merchant_number(+)\n                and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >= ( :14  - 180)\n                and mf.merchant_number = sm.merchant_number(+)\n                and sm.batch_date(+) =  :15 \n                and mf.merchant_number = ncb.merchant_number(+)\n                and mf.merchant_number = mchb.merchant_number(+)\n                and mf.merchant_number = ach.merchant_number(+)\n                and mf.merchant_number = fx.merchant_number(+)\n        group by mf.merchant_number,\n                 mif_date_opened(date_opened),\n                 mf.activation_date,\n                 mf.date_stat_chgd_to_dcb,\n                  case\n                    when comm.login_name is not null \n                      then 'Tele Sales'\n                    when mf.bank_number = 3858\n                      then 'SE Sales'                    \n                    else\n                      nvl(cl.channel_2013, 'Uncategorized')\n                  end, \n                  nvl(ncb.incoming_count,0),\n                  nvl(ncb.incoming_amount,0),\n                  nvl(mchb.mchb_count,0),\n                  nvl(mchb.mchb_amount,0),\n                  nvl(ach.ach_count,0),\n                  nvl(ach.ach_amount,0),\n                  nvl(fx.fx_revenue,0),\n                  nvl(fx.fx_proc_revenue,0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
   __sJT_st.setDate(6,activityDate);
   __sJT_st.setDate(7,activityDate);
   __sJT_st.setDate(8,activityDate);
   __sJT_st.setDouble(9,FxBp);
   __sJT_st.setDouble(10,FxProcBp);
   __sJT_st.setDate(11,activityDate);
   __sJT_st.setDate(12,activityDate);
   __sJT_st.setDate(13,activityDate);
   __sJT_st.setDate(14,activityDate);
   __sJT_st.setDate(15,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.CieloActivityDetailEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:436^7*/
    
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ActivityData summaryData = new ActivityData(9999,activityDate);
        summaryData.setMerchantData( resultSet );
        summaryData.setSales(resultSet);
        summaryData.setOpenClosed(resultSet);
        summaryData.setActivatedCount(resultSet);
        summaryData.setIncomingChargebacks(resultSet);
        summaryData.setWorkedChargebacks(resultSet);
        summaryData.setAch(resultSet);
        summaryData.setFxRevenue(resultSet);
        
        merchantRecs.add(summaryData);
      }
      
      resultSet.close();
      it.close();
      
      loadNetRevenue(merchantRecs, activityDate);
      
      /*@lineinfo:generated-code*//*@lineinfo:460^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    cielo_activity_details
//          where   activity_date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    cielo_activity_details\n        where   activity_date =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/
      
      commit();
      
      for( int i = 0; i < merchantRecs.size(); ++i )
      {
        ActivityData summaryData = (ActivityData)(merchantRecs.elementAt(i));
        /*@lineinfo:generated-code*//*@lineinfo:472^9*/

//  ************************************************************
//  #sql [Ctx] { insert into cielo_activity_details
//            (
//              merchant_number,
//              open_date,
//              activity_date,
//              category,
//              sales_count,
//              sales_amount,
//              net_revenue,
//              new_account,
//              closed_account,
//              activated_account,
//              closed_date,
//              activation_date,
//              incoming_chargeback_count,
//              incoming_chargeback_amount,
//              adjusted_chargeback_count,
//              adjusted_chargeback_amount,
//              ach_count,
//              ach_amount,
//              fx_revenue,
//              fx_proc_revenue
//            )
//            values
//            (
//              :summaryData.MerchantNumber,
//              :summaryData.OpenDate,
//              :summaryData.ActivityDate,
//              :summaryData.Category,
//              :summaryData.SalesCount,
//              :summaryData.SalesAmount,
//              :summaryData.NetRevenue,
//              :summaryData.OpenedCount,
//              :summaryData.ClosedCount,
//              :summaryData.ActivatedCount,
//              :summaryData.CloseDate,
//              :summaryData.ActivatedDate,
//              :summaryData.CbIncomingCount,
//              :summaryData.CbIncomingAmount,
//              :summaryData.CbWorkedCount,
//              :summaryData.CbWorkedAmount,
//              :summaryData.AchCount,
//              :summaryData.AchAmount,
//              :summaryData.FxRevenue,
//              :summaryData.FxProcRevenue
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into cielo_activity_details\n          (\n            merchant_number,\n            open_date,\n            activity_date,\n            category,\n            sales_count,\n            sales_amount,\n            net_revenue,\n            new_account,\n            closed_account,\n            activated_account,\n            closed_date,\n            activation_date,\n            incoming_chargeback_count,\n            incoming_chargeback_amount,\n            adjusted_chargeback_count,\n            adjusted_chargeback_amount,\n            ach_count,\n            ach_amount,\n            fx_revenue,\n            fx_proc_revenue\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,summaryData.MerchantNumber);
   __sJT_st.setDate(2,summaryData.OpenDate);
   __sJT_st.setDate(3,summaryData.ActivityDate);
   __sJT_st.setString(4,summaryData.Category);
   __sJT_st.setInt(5,summaryData.SalesCount);
   __sJT_st.setDouble(6,summaryData.SalesAmount);
   __sJT_st.setDouble(7,summaryData.NetRevenue);
   __sJT_st.setInt(8,summaryData.OpenedCount);
   __sJT_st.setInt(9,summaryData.ClosedCount);
   __sJT_st.setInt(10,summaryData.ActivatedCount);
   __sJT_st.setDate(11,summaryData.CloseDate);
   __sJT_st.setDate(12,summaryData.ActivatedDate);
   __sJT_st.setInt(13,summaryData.CbIncomingCount);
   __sJT_st.setDouble(14,summaryData.CbIncomingAmount);
   __sJT_st.setInt(15,summaryData.CbWorkedCount);
   __sJT_st.setDouble(16,summaryData.CbWorkedAmount);
   __sJT_st.setInt(17,summaryData.AchCount);
   __sJT_st.setDouble(18,summaryData.AchAmount);
   __sJT_st.setDouble(19,summaryData.FxRevenue);
   __sJT_st.setDouble(20,summaryData.FxProcRevenue);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:520^9*/                               
      }
      commit();
      
      // add processing deal details
      /*@lineinfo:generated-code*//*@lineinfo:525^7*/

//  ************************************************************
//  #sql [Ctx] { insert into cielo_activity_details
//            (
//              merchant_number,
//              activity_date,
//              category,
//              sales_count,
//              sales_amount,
//              net_revenue,
//              new_account,
//              closed_account,
//              activated_account,         
//              incoming_chargeback_count,
//              incoming_chargeback_amount,
//              adjusted_chargeback_count,
//              adjusted_chargeback_amount,
//              ach_count,
//              ach_amount,
//              fx_revenue,
//              fx_proc_revenue,
//              proc_revenue,
//              open_date,
//              closed_date,
//              activation_date
//            )
//            select  mf.merchant_number,
//                    :activityDate,       
//                    'Processing Deals',
//                    0,
//                    0,
//                    0,
//                    0,                
//                    0,
//                    0,
//                    0,
//                    0,                               
//                    0,
//                    0,
//                    0,
//                    0,                
//                    0,
//                    0,
//                    nvl(round((sum((sm.bank_sales_amount
//                      + sm.nonbank_sales_amount
//                      - sm.bank_reject_sales_amount
//                      - sm.nonbank_reject_sales_amount) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end) * :ProcBp),2),0) as proc_revenue,
//                    mif_date_opened(mf.date_opened), mf.date_stat_chgd_to_dcb , mf.activation_date
//            from  mif mf,
//                  daily_detail_file_summary sm
//            where mf.bank_number in (select bank_number from mbs_banks where owner = 2)
//                  and nvl(mf.test_account,'N') != 'Y'
//                  and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >= :activityDate
//                  and to_char(mf.merchant_number) not like '9419%'
//                  and mf.merchant_number = sm.merchant_number
//                  and sm.batch_date = :activityDate
//            group by mf.merchant_number, mif_date_opened(mf.date_opened),mf.date_stat_chgd_to_dcb , mf.activation_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into cielo_activity_details\n          (\n            merchant_number,\n            activity_date,\n            category,\n            sales_count,\n            sales_amount,\n            net_revenue,\n            new_account,\n            closed_account,\n            activated_account,         \n            incoming_chargeback_count,\n            incoming_chargeback_amount,\n            adjusted_chargeback_count,\n            adjusted_chargeback_amount,\n            ach_count,\n            ach_amount,\n            fx_revenue,\n            fx_proc_revenue,\n            proc_revenue,\n            open_date,\n            closed_date,\n            activation_date\n          )\n          select  mf.merchant_number,\n                   :1 ,       \n                  'Processing Deals',\n                  0,\n                  0,\n                  0,\n                  0,                \n                  0,\n                  0,\n                  0,\n                  0,                               \n                  0,\n                  0,\n                  0,\n                  0,                \n                  0,\n                  0,\n                  nvl(round((sum((sm.bank_sales_amount\n                    + sm.nonbank_sales_amount\n                    - sm.bank_reject_sales_amount\n                    - sm.nonbank_reject_sales_amount) * case when mf.sic_code in ('6010', '6011') then 0 else 1 end) *  :2 ),2),0) as proc_revenue,\n                  mif_date_opened(mf.date_opened), mf.date_stat_chgd_to_dcb , mf.activation_date\n          from  mif mf,\n                daily_detail_file_summary sm\n          where mf.bank_number in (select bank_number from mbs_banks where owner = 2)\n                and nvl(mf.test_account,'N') != 'Y'\n                and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >=  :3 \n                and to_char(mf.merchant_number) not like '9419%'\n                and mf.merchant_number = sm.merchant_number\n                and sm.batch_date =  :4 \n          group by mf.merchant_number, mif_date_opened(mf.date_opened),mf.date_stat_chgd_to_dcb , mf.activation_date";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDouble(2,ProcBp);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:582^7*/                               
      
      commit();      
      
      addSummaryData(activityDate);
    }
    catch(Exception e)
    {
      logEntry("loadData(" + activityDate.toString() + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public void addSummaryData( Date activityDate )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:602^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    cielo_activity_channel_summary
//          where   activity_Date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    cielo_activity_channel_summary\n        where   activity_Date =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^7*/
      
      // load summary table
      /*@lineinfo:generated-code*//*@lineinfo:610^7*/

//  ************************************************************
//  #sql [Ctx] { insert into cielo_activity_channel_summary
//          (
//            activity_date,
//            category,
//            sales_count,
//            sales_amount,
//            net_revenue,
//            new_account,
//            closed_account,
//            incoming_chargeback_count,
//            incoming_chargeback_amount,
//            adjusted_chargeback_count,
//            adjusted_chargeback_amount,
//            ach_count,
//            ach_amount,
//            fx_revenue,
//            fx_proc_revenue,
//            proc_revenue
//          )
//          select  activity_date,
//                  category,
//                  sum(nvl(sales_count,0)),
//                  sum(nvl(sales_amount,0)),
//                  sum(nvl(net_revenue,0)),
//                  sum(nvl(new_account,0)),
//                  sum(nvl(closed_account,0)),
//                  sum(nvl(incoming_chargeback_count,0)),
//                  sum(nvl(incoming_chargeback_amount,0)),
//                  sum(nvl(adjusted_chargeback_count,0)),
//                  sum(nvl(adjusted_chargeback_amount,0)),
//                  sum(nvl(ach_count,0)),
//                  sum(nvl(ach_amount,0)),
//                  sum(nvl(fx_revenue,0)),
//                  sum(nvl(fx_proc_revenue,0)),
//                  sum(nvl(proc_revenue,0))
//          from    cielo_activity_details
//          where   activity_date = :activityDate
//          group by activity_date, category
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into cielo_activity_channel_summary\n        (\n          activity_date,\n          category,\n          sales_count,\n          sales_amount,\n          net_revenue,\n          new_account,\n          closed_account,\n          incoming_chargeback_count,\n          incoming_chargeback_amount,\n          adjusted_chargeback_count,\n          adjusted_chargeback_amount,\n          ach_count,\n          ach_amount,\n          fx_revenue,\n          fx_proc_revenue,\n          proc_revenue\n        )\n        select  activity_date,\n                category,\n                sum(nvl(sales_count,0)),\n                sum(nvl(sales_amount,0)),\n                sum(nvl(net_revenue,0)),\n                sum(nvl(new_account,0)),\n                sum(nvl(closed_account,0)),\n                sum(nvl(incoming_chargeback_count,0)),\n                sum(nvl(incoming_chargeback_amount,0)),\n                sum(nvl(adjusted_chargeback_count,0)),\n                sum(nvl(adjusted_chargeback_amount,0)),\n                sum(nvl(ach_count,0)),\n                sum(nvl(ach_amount,0)),\n                sum(nvl(fx_revenue,0)),\n                sum(nvl(fx_proc_revenue,0)),\n                sum(nvl(proc_revenue,0))\n        from    cielo_activity_details\n        where   activity_date =  :1 \n        group by activity_date, category";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:650^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("addSummaryData(" + activityDate.toString() + ")", e.toString());
    }
  }
  
  protected void loadNetRevenue( Vector merchantRecs, Date activityDate )
    throws Exception
  {
    Date          activeDate      = null;
    int           lastDay         = 0;
    ThreadPool    pool            = null;
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:671^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(count(1),0,trunc(:activityDate,'month'),trunc(trunc(:activityDate,'month')-1,'month')) as active_date,
//                  case when last_day(:activityDate) = :activityDate then 1 else 0 end                           as last_day
//          
//          from    monthly_extract_gn  gn
//          where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and gn.hh_active_date = trunc(trunc(:activityDate,'month')-1,'month')
//                  and gn.t1_tot_income != 0      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(count(1),0,trunc( :1 ,'month'),trunc(trunc( :2 ,'month')-1,'month')) as active_date,\n                case when last_day( :3 ) =  :4  then 1 else 0 end                           as last_day\n         \n        from    monthly_extract_gn  gn\n        where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)\n                and gn.hh_active_date = trunc(trunc( :5 ,'month')-1,'month')\n                and gn.t1_tot_income != 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   lastDay = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:680^7*/
      
      // establish total fees from last month by channel for help calculating basis points
      Hashtable TotalFees = new Hashtable();
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  case
//                    when comm.login_name is not null 
//                      then 'Tele Sales'
//                    when mf.bank_number = 3858
//                      then 'SE Sales'
//                    else
//                      nvl(cl.channel_2013, 'Uncategorized')
//                  end                            as category, 
//                  nvl(sum(st.st_fee_amount),0)   as total_fees
//          from    mif mf,
//                  channel_link_2013 cl,
//                  (
//                    select  cm.merchant_number  merchant_number,
//                            u.login_name        login_name
//                    from    commissions cm,
//                            users u,
//                            inside_sales_reps isr
//                    where   cm.user_id = u.user_id
//                            and u.login_name = isr.login_name
//                            and cm.merchant_number > 941000091988
//                    group by cm.merchant_number, u.login_name
//                  ) comm,                                     
//                  monthly_extract_gn    gn,
//                  monthly_extract_st    st
//          where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >= :activeDate-180
//                  and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)
//                  and mf.merchant_number = comm.merchant_number(+)
//                  and mf.merchant_number = gn.hh_merchant_number
//                  and gn.hh_active_date = :activeDate
//                  and st.hh_load_sec = gn.hh_load_sec
//                  and ( st.st_statement_desc like '%PCI%'
//                        or st.st_statement_desc like '%IRS%'
//                        or st.st_statement_desc like '%ADMINISTRATIVE%'
//                        or st.st_statement_desc like '%COMPLIANCE%'
//                        or st.st_statement_desc like '%1099 %' )                
//          group by  case
//                      when comm.login_name is not null 
//                        then 'Tele Sales'
//                      when mf.bank_number = 3858
//                        then 'SE Sales'
//                      else
//                        nvl(cl.channel_2013, 'Uncategorized')
//                    end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  case\n                  when comm.login_name is not null \n                    then 'Tele Sales'\n                  when mf.bank_number = 3858\n                    then 'SE Sales'\n                  else\n                    nvl(cl.channel_2013, 'Uncategorized')\n                end                            as category, \n                nvl(sum(st.st_fee_amount),0)   as total_fees\n        from    mif mf,\n                channel_link_2013 cl,\n                (\n                  select  cm.merchant_number  merchant_number,\n                          u.login_name        login_name\n                  from    commissions cm,\n                          users u,\n                          inside_sales_reps isr\n                  where   cm.user_id = u.user_id\n                          and u.login_name = isr.login_name\n                          and cm.merchant_number > 941000091988\n                  group by cm.merchant_number, u.login_name\n                ) comm,                                     \n                monthly_extract_gn    gn,\n                monthly_extract_st    st\n        where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >=  :1 -180\n                and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)\n                and mf.merchant_number = comm.merchant_number(+)\n                and mf.merchant_number = gn.hh_merchant_number\n                and gn.hh_active_date =  :2 \n                and st.hh_load_sec = gn.hh_load_sec\n                and ( st.st_statement_desc like '%PCI%'\n                      or st.st_statement_desc like '%IRS%'\n                      or st.st_statement_desc like '%ADMINISTRATIVE%'\n                      or st.st_statement_desc like '%COMPLIANCE%'\n                      or st.st_statement_desc like '%1099 %' )                \n        group by  case\n                    when comm.login_name is not null \n                      then 'Tele Sales'\n                    when mf.bank_number = 3858\n                      then 'SE Sales'\n                    else\n                      nvl(cl.channel_2013, 'Uncategorized')\n                  end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.CieloActivityDetailEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:730^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        TotalFees.put(rs.getString("category"), rs.getDouble("total_fees"));
      }
      rs.close();
      it.close();
      
      if( TotalFees.get("Uncategorized") == null )
      {
        TotalFees.put(new String("Uncategorized"), 0.0);
      }

      Hashtable     BasisPoints            = new Hashtable();
      
      for( Enumeration e = TotalFees.keys(); e.hasMoreElements(); )
      {
        String channel = (String)(e.nextElement());
        double totalFees = ((Double)(TotalFees.get(channel))).doubleValue();
        double bp = 0.0;
        
        /*@lineinfo:generated-code*//*@lineinfo:754^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(((sum(gn.t1_tot_income-(sm.interchange_expense_enhanced+sm.vmc_assessment_expense-sm.vmc_fees))-:totalFees)
//                        /sum(gn.t1_tot_amount_of_sales)),0) as bp
//            
//            from    mif mf,
//                    channel_link_2013 cl,
//                    (
//                      select  cm.merchant_number  merchant_number,
//                              u.login_name        login_name
//                      from    commissions cm,
//                              users u,
//                              inside_sales_reps isr
//                      where   cm.user_id = u.user_id
//                              and u.login_name = isr.login_name
//                              and cm.merchant_number > 941000091988
//                      group by cm.merchant_number, u.login_name
//                    ) comm,                                     
//                    monthly_extract_gn        gn,
//                    monthly_extract_summary   sm
//            where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                    and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >= :activeDate-180
//                    and to_char(mf.merchant_number) not like '9419%'
//                    and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)
//                    and mf.merchant_number = comm.merchant_number(+)
//                    and mf.merchant_number = gn.hh_merchant_number       
//                    and gn.hh_active_date = :activeDate
//                    and sm.merchant_number = gn.hh_merchant_number
//                    and sm.active_date = gn.hh_active_date
//                    and nvl(gn.t1_tot_amount_of_sales,0) > 0
//                    and case 
//                          when comm.login_name is not null 
//                            then 'Tele Sales' 
//                          when mf.bank_number = 3858
//                            then 'SE Sales'
//                          else nvl(cl.channel_2013,'Uncategorized') 
//                        end = :channel
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(((sum(gn.t1_tot_income-(sm.interchange_expense_enhanced+sm.vmc_assessment_expense-sm.vmc_fees))- :1 )\n                      /sum(gn.t1_tot_amount_of_sales)),0) as bp\n           \n          from    mif mf,\n                  channel_link_2013 cl,\n                  (\n                    select  cm.merchant_number  merchant_number,\n                            u.login_name        login_name\n                    from    commissions cm,\n                            users u,\n                            inside_sales_reps isr\n                    where   cm.user_id = u.user_id\n                            and u.login_name = isr.login_name\n                            and cm.merchant_number > 941000091988\n                    group by cm.merchant_number, u.login_name\n                  ) comm,                                     \n                  monthly_extract_gn        gn,\n                  monthly_extract_summary   sm\n          where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                  and nvl(mf.date_stat_chgd_to_dcb,'31-Dec-9999') >=  :2 -180\n                  and to_char(mf.merchant_number) not like '9419%'\n                  and mf.bank_number||mf.group_2_association = cl.hierarchy_node(+)\n                  and mf.merchant_number = comm.merchant_number(+)\n                  and mf.merchant_number = gn.hh_merchant_number       \n                  and gn.hh_active_date =  :3 \n                  and sm.merchant_number = gn.hh_merchant_number\n                  and sm.active_date = gn.hh_active_date\n                  and nvl(gn.t1_tot_amount_of_sales,0) > 0\n                  and case \n                        when comm.login_name is not null \n                          then 'Tele Sales' \n                        when mf.bank_number = 3858\n                          then 'SE Sales'\n                        else nvl(cl.channel_2013,'Uncategorized') \n                      end =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.CieloActivityDetailEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,totalFees);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setString(4,channel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:791^9*/
        
        BasisPoints.put(channel, bp);
      }
      
      pool = new ThreadPool(7);
      for( int i = 0; i < merchantRecs.size(); ++i )
      {
        ActivityData summaryData = (ActivityData)(merchantRecs.elementAt(i));
        
        Thread thread = pool.getThread( new NetRevenueWorker(summaryData, activityDate, activeDate, BasisPoints, lastDay) );
        thread.start(); 
      }
      pool.waitForAllThreads();
    }
    catch( Exception e )
    {
      logEntry("loadNetRevenue(" + activityDate + ")",e.toString());
      throw(e);
    }
    finally
    {
    }
  }
  
  public static void main(String[] args)
  {
    CieloActivityDetailEvent         testEvent   = null;

    try
    {
      SQLJConnectionBase.initStandalonePool("ERROR");
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      testEvent = new CieloActivityDetailEvent();
      testEvent.connect(true);

      if ( "execute".equals(args[0]) )
      {
        testEvent.execute();
      }
      else if ( "summarize".equals(args[0]) )
      {
        System.out.println("SUMMARIZING " + args[1] + " -> " + args[2]);
        Calendar        cal           = Calendar.getInstance();
        java.util.Date  beginDate     = DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy");
        
        cal.setTime(DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy"));
        
        while( !cal.getTime().before(beginDate) )
        {
          System.out.print("Summarizing " + DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy") + "           \r");
          testEvent.addSummaryData(new java.sql.Date(cal.getTime().getTime()));
          cal.add(Calendar.DAY_OF_MONTH,-1);
        }
        System.out.println();
      }
      else if ( "backfill".equals(args[0]) )
      {
        Calendar        cal           = Calendar.getInstance();
        java.util.Date  beginDate     = DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy");
        
        System.out.println("beginDate = " + beginDate.toString());
        
        cal.setTime(DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy"));
        
        System.out.println("endDate = " + cal.getTime().toString());
        
        while( !cal.getTime().before(beginDate) )
        {
          System.out.print("Loading " + DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy") + "           \r");
          testEvent.loadData(new java.sql.Date(cal.getTime().getTime()));
          cal.add(Calendar.DAY_OF_MONTH,-1);
        }
        System.out.println();
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ testEvent.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/