package com.mes.startup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mes.constants.CSConstants;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.PropertiesFile;
import com.mes.utils.MesStringUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CSMerchantOnboard extends EventBase{
	Client client;
    WebResource wr;
    ClientResponse cr;
    MultivaluedMap<String, String> formData;
    HashMap<String, String> portfolios ;
    HashMap<Long, String> mlist ;
    String csUrl;
    long mid;
    String api_username,api_password;
    static int testFlag = 0;
    private static final String API_PASSWORD_REGEX = "api_password=\\[*[a-zA-Z0-9]*\\]*[,}]";
    
	public boolean execute(){
		
		PreparedStatement ps;
		ResultSet rs ;
		mlist = new HashMap<Long, String>();
		try{
            connect(true);
            loadPortfolios();
            wr = createConnection();
            //Exclude 3942(Umpqua bank) and sic codes 6010 and 6011
            String query = "select merchant_number, dba_name, name1_line_1, phone_1, dba_name, addr1_line_1, city1_line_4,state1_line_4, zip1_line_4," +
            		"mf.bank_number || mf.dmagent as assn, mf.bank_number || mf.group_1_association as g1," + 
            		"mf.bank_number || mf.group_2_association as g2, mf.bank_number || mf.group_3_association as g3, " +
					"mf.bank_number || mf.group_4_association as g4, mf.bank_number || mf.group_5_association as g5, " +
            		"mf.bank_number || mf.group_6_association as g6, ocs.api_username, ocs.saq_product_id, ocs.breach_protection_product_id " +
            		"from mif mf, org_to_csapi ocs " +
            		"where mif_date_opened(mf.date_opened) = trunc(sysdate -1)  and bank_number <> 3942 " +
            		"and sic_Code not in (6010, 6011) and mf.test_account = 'N' and ocs.organization_id in (" +
            		"mf.bank_number || mf.dmagent, mf.bank_number || mf.group_1_association," +
            		"mf.bank_number || mf.group_2_association, mf.bank_number || mf.group_3_association," +
            		"mf.bank_number || mf.group_4_association, mf.bank_number || mf.group_5_association, mf.bank_number || mf.group_6_association)";
            if(mid>0)
            	query = query + "and merchant_number = " + mid;
//           log.debug(query); 
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
			while(rs.next()){
				if(addMerchant(wr,rs)) {
					mlist.put(rs.getLong("merchant_number"), rs.getString("dba_name") + " - " + rs.getString("api_username"));
					logBoarding(rs.getLong("merchant_number"));
				}
			}
			if(mlist.size() > 0){
				sendEmailnotification();
			}else {
				log.error("No Records found for the criteria");
			}
		}catch(Exception e){
			logEntry("MerchantOnboard.execute()", e.getMessage());
		}
		return false;
	}
	
//Connects to controlscan boardinng API	
	public WebResource createConnection(){
		log.debug("creating connection to Controlscan to " + csUrl);
		try{
			com.mes.net.MesNetTools.disableSSLCertificateValidation();
			client = Client.create();
			wr = client.resource(csUrl);
		}catch(Exception e){
			logEntry("MerchantOnboard.createConnection()", e.getMessage());
		}
		
		return wr;
	}

@SuppressWarnings("unchecked")
public String getProductIds(ResultSet rs, int testFlag) throws JsonProcessingException, SQLException{
	StringBuilder productIds = new StringBuilder();
	String productId;
	JSONObject obj = new JSONObject();
	if(testFlag == 1)
		obj.put("id", portfolios.get(CSConstants.SAQ_PRODUCT_ID));
	else
		obj.put("id", rs.getString(CSConstants.SAQ_PRODUCT_ID));
	obj.put("quantity", 1);
	productIds.append(obj + ",");
	if(testFlag == 1)
		obj.put("id", portfolios.get(CSConstants.BREACH_PROTECTION_PRODUCT_ID));
	else
		obj.put("id", rs.getString(CSConstants.BREACH_PROTECTION_PRODUCT_ID));
	obj.put("quantity", 1);
	productIds.append(obj);
	productId = productIds.toString().replace("}{", ",");
	return productId;
	
}

/*
 * loads Portfolio information like username and password from controlscan.properties file 
 * 
 */
	public void loadPortfolios() {
		PropertiesFile pf = null;
		portfolios = new HashMap<String, String>();
		log.debug("loading portfolios...");
		try {
			pf = new PropertiesFile();
			pf.load("controlscan.properties");
			csUrl = pf.getProperty("ControlScanAPI.Boarding.URL");
			if(pf.containsKey("ControlScanAPI.URL"))
				pf.remove(pf.get("ControlScanAPI.URL"));
			if(pf.containsKey("ControlScanAPI.Boarding.URL"))
				pf.remove(pf.get("ControlScanAPI.Boarding.URL"));
			for (Enumeration e = pf.getPropertyNames(); e.hasMoreElements();) {
				String userName = (String) e.nextElement();
				if (!userName.equals("ControlScanAPI.URL") && !userName.equals("ControlScanAPI.Boarding.URL")) {
					portfolios.put(userName, pf.getString(userName));
				}
			}
		} catch (Exception e) {
			logEntry("MerchantOnboard.loadPortfolios()", e.getMessage());
		}

	}
	
	
	public boolean addMerchant(WebResource wr, ResultSet rs){
		String result = "";
		String productIdRequest;
		
		try{
			formData = new MultivaluedMapImpl();
			//Method for formData request for productIDs
			productIdRequest = getProductIds(rs,testFlag);
			mid = rs.getLong(CSConstants.MERCHANT_NUMBER);
			if(testFlag == 1)
				api_username = CSConstants.SANDBOX_USERNAME;
			else
				api_username = rs.getString(CSConstants.API_USERNAME);
                api_password = portfolios.get(api_username);
			if(api_password != null) {
				log.debug("adding merchant number " + mid + " under " + api_username + " portfolio");
				formData.add(CSConstants.API_USERNAME, api_username);
		        formData.add(CSConstants.API_PASSWORD, api_password );
		        formData.add(CSConstants.MERCHANT_ID,rs.getString(CSConstants.MERCHANT_NUMBER));
		        formData.add(CSConstants.FIRST_NAME,rs.getString(CSConstants.NAME1_LINE_1).split(" ", 2)[0]);
		        
		        if(rs.getString(CSConstants.NAME1_LINE_1).split(" ").length >1){
		        	formData.add(CSConstants.LAST_NAME,rs.getString(CSConstants.NAME1_LINE_1).split(" ", 2)[1]);
		        } else {
		        	formData.add(CSConstants.LAST_NAME,rs.getString(CSConstants.NAME1_LINE_1).split(" ", 2)[0]);
		        }

		        formData.add(CSConstants.PHONE,rs.getString(CSConstants.PHONE_1));
		        formData.add(CSConstants.DBA_NAME,rs.getString(CSConstants.DBA_NAME));
		        formData.add(CSConstants.DBA_ADDRESS,rs.getString(CSConstants.ADDR1_LINE_1));
		        formData.add(CSConstants.DBA_CITY,rs.getString(CSConstants.CITY1_LINE_4));
		        formData.add(CSConstants.DBA_STATE,rs.getString(CSConstants.STATE1_LINE_4));
		        formData.add(CSConstants.DBA_ZIPCODE,rs.getString(CSConstants.ZIP1_LINE_4));
		        formData.add(CSConstants.PRODUCTS, "["+productIdRequest+"]");
		        log.info("JSON Request for ControlScan Boarding:" + MesStringUtil.sanitize(formData.toString(),API_PASSWORD_REGEX));
		        cr = wr.accept(MediaType.APPLICATION_JSON).type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
			    JSONObject jsonObject = (JSONObject) new JSONParser().parse(cr.getEntity(String.class));
			    
			    result = jsonObject.get(CSConstants.T_CODE).toString();
			    if(result.equals("2100"))
			    	log.debug("Account already exists in controlscan");
			    else if(result.equals("0"))
			    	log.info("Account has been boarded into controlscan successfully");
			    else
			    	log.error("Error Code: Refer SmartSync API to resolve the error" + result);
			} else {
				log.debug("no matching portfolio found");
			}
			
		}catch(Exception e){
	
			logEntry("MerchantOnboard.addMerchant(WebResource wr, ResultSet rs)", e.getMessage());
		}
		return (result.equals("0"));
	}
	
//	Adds log entry in controlscan_log table 
	public void logBoarding(long merchant_number){
        PreparedStatement ps = null;
        String sqlText = "insert into controlscan_log (" +
                "merchant_number, log_date, field_changed, new_value, change_type, changed_by)" +
                "values (?,sysdate,?,?,?,?)";
        try{
            ps = con.prepareStatement(sqlText);
            ps.setLong(1, merchant_number);
            ps.setString(2, "Merchant Boarded");
            ps.setLong(3, merchant_number);
            ps.setString(4, "UPDATE");
            ps.setString(5, "BOARDING");
            ps.executeUpdate();
        }catch(Exception e){
        	logEntry("MerchantOnboard.logBoarding(long merchant_number)", e.getMessage());
        }finally{
               try{
                   if (ps != null) {
                       ps.close();
                   }
               } catch( Exception e ) {}
           }
	}
	
//sends an email notification with the list of merchants boarded to Controlscan
	public void sendEmailnotification(){
		log.debug("sending email...");
		Iterator<Long> it = mlist.keySet().iterator();
		 StringBuffer sb = new StringBuffer();
		try{
	          MailMessage msg = new MailMessage();
	          msg.setAddresses(MesEmails.MSG_ADDRS_CONTROLSCAN_NOTIFY);
	          msg.setSubject("Controlscan Boarded merchants");
	          sb.append("Please see below for list of merchants boarded to controlscan:\n");
	          sb.append("Merchant Number - DBA Name - Portfolio \n");
	          while(it.hasNext()){
	        	  long mid = it.next();
	        	  sb.append(mid + " - " + mlist.get(mid) + "\n");
	          }
	          msg.setText(sb.toString());
	          msg.send();
	          mlist.clear();
	    }catch(Exception e){
	        logEntry("sendEmailnotification()",e.toString());
	    }
	    log.debug("mail has been sent");
	}
	

	public static void main(String args[]){
		CSMerchantOnboard mob = new CSMerchantOnboard();
		if(args.length == 1)
			mob.mid = Long.parseLong(args[0]);
		testFlag = 1;
		mob.execute();
		System.exit(0);
	}
}
