package com.mes.constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CSConstants {
	
	// Boarding Request Constants
	public final static String API_USERNAME = "api_username";
	public final static String API_PASSWORD = "api_password";
	public final static String MERCHANT_ID = "merchant_id";
	public final static String FIRST_NAME = "first_name";
	public final static String LAST_NAME = "last_name";
	public final static String PHONE = "phone";
	public final static String DBA_NAME = "dba_name";
	public final static String DBA_ADDRESS = "dba_address";
	public final static String DBA_CITY = "dba_city";
	public final static String DBA_STATE = "dba_state";
	public final static String DBA_ZIPCODE = "dba_zipcode";
	public final static String PRODUCTS = "products[]";
	public final static String SAQ_PRODUCT_ID = "saq_product_id";
	public final static String BREACH_PROTECTION_PRODUCT_ID = "breach_protection_product_id";
	
	// Boarding resultset constants
	public final static String MERCHANT_NUMBER = "merchant_number";
	public final static String NAME1_LINE_1 = "name1_line_1";
	public final static String PHONE_1 = "phone_1";
	public final static String ADDR1_LINE_1 = "addr1_line_1";
	public final static String CITY1_LINE_4 = "city1_line_4";
	public final static String STATE1_LINE_4 = "state1_line_4";
	public final static String ZIP1_LINE_4 = "zip1_line_4";
	
	//SmartSyncAPI Constants
	public final static String RISK_PCI_VENDOR_CODE = "C SCAN";
	public final static String RISK_PCI_COMPLIANCE_YES = "Y";
	public final static String RISK_PCI_COMPLIANCE_NO = "N";
	public final static String RISK_PCI_COMPLIANCE_GRACE = "G";
		
	public final static String SCAN_STATUS_NA = "Scan Requirement Waived";
		
	public final static List<String> SCAN_STATUS_YES = Collections.unmodifiableList(new ArrayList<String>() {{ 
	        add("Scan passed");
            add("Scan(s) compliant");
            add("Quarterly scan compliant, but most recent scan failed");
		}});
		
	public final static List<String> SCAN_STATUS_INPROGRESS = Collections.unmodifiableList(new ArrayList<String>() {{ 
            add("Scan(s) passed, but requires merchant attestation");
            add("Scan(s) passed, but requires ControlScan attestation");
            add("Scan(s) passed within 90 days, but requires ControlScan attestation");
            add("Scan(s) passed within 90 days, but requires merchant attestation");
		}});
		
	public final static List<String> SCAN_STATUS_NO = Collections.unmodifiableList(new ArrayList<String>() {{ 
		    add("Scan(s) required");
		    add("Scan(s) Failed");
		}});
		
	public final static String QUESTIONNAIRE_STATUS = "Questionnaire passed";
	
	//Boarding Response Constants
	public final static String T_CODE = "t_code";
	
	//sandbox username
	public final static String SANDBOX_USERNAME = "mes_api_sandbox";
	
	public final static String EXCEPTION_ORA_12899 = "value too large for column";

	public final static String EXCEPTION_ORA_01461 = "can bind a LONG value only for insert into a LONG column";
	

}
