/*@lineinfo:filename=TridentDebitEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TridentDebitEvent.sqlj $

  Description:

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-10-31 14:23:02 -0700 (Fri, 31 Oct 2014) $
  Version            : $Revision: 23230 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesQueues;
import com.mes.startup.ProcessTable.ProcessTableEntry;
import com.mes.tools.ChargebackTools;
import sqlj.runtime.ResultSetIterator;

public class TridentDebitEvent extends EventBase
{
  static Logger log = Logger.getLogger(TridentDebitEvent.class);
  
  public static class UnpaidFundedItem
  {
    public    String        AuthCode        = null;
    public    int           BankNumber      = 0;
    public    Date          BatchDate       = null;
    public    long          BatchNumber     = 0L;
    public    String        CardNumber      = null;
    public    long          DdfDtId         = 0L;
    public    long          MerchantId      = 0L;
    public    double        TranAmount      = 0.0;
    public    Date          TranDateBegin   = null;
    public    Date          TranDateEnd     = null;
  
    public UnpaidFundedItem( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId      = resultSet.getLong("merchant_number");
      BankNumber      = resultSet.getInt("bank_number");
      BatchDate       = resultSet.getDate("batch_date");
      BatchNumber     = resultSet.getLong("batch_number");
      DdfDtId         = resultSet.getLong("ddf_dt_id");
      TranDateBegin   = resultSet.getDate("tran_date_begin");
      TranDateEnd     = resultSet.getDate("tran_date_end");
      CardNumber      = resultSet.getString("card_number");
      AuthCode        = resultSet.getString("auth_code");
      TranAmount      = resultSet.getDouble("tran_amount");
    }
  }
  
  protected   boolean         Verbose             = false;
  
  // if this is set linkDebitToDDF will only link records with
  // the given loadFilename
  private     String          targetLoadFilename  = null;  
  

  protected boolean addExceptionsToQueue()
  {
    String              bankNumber    = null;
    ResultSetIterator   it            = null;
    int                 queueType     = -1;
    long                recId         = 0L;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;

    log.debug("addExceptionsToQueue");
    
    try
    {
      setAutoCommit(false);
      
      for ( int exceptionType = 0; exceptionType < 2; ++exceptionType )
      {
        switch(exceptionType)
        {
          case 0:   // mes funded by network, merchant *not* funded by mes
            queueType = MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED;
            // select the items that have not been linked to a DDF entry after 5 days
            /*@lineinfo:generated-code*//*@lineinfo:101^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.rec_id                       as rec_id,
//                        dt.settlement_date              as settlement_date,
//                        mf.bank_number                  as bank_number
//                from    trident_debit_financial   dt,
//                        mif                       mf
//                where   dt.balanced = 'N' and
//                        dt.settlement_date between trunc(sysdate-30) and trunc(sysdate-3) and
//                        dt.request_message_type = '0200' and
//                        dt.response_code = '00' and -- only approved items
//                        not exists
//                        (
//                          select  retrieval_reference_number
//                          from    trident_debit_financial dti
//                          where   dti.retrieval_reference_number = dt.retrieval_reference_number and
//                                  dti.request_message_type = '0400' and
//                                  dti.response_code = '00'
//                             and  dti.settlement_date between dt.settlement_date and (dt.settlement_date+1)
//                             and  dti.merchant_number = dt.merchant_number
//                             and  dti.transaction_identifier = dt.transaction_identifier    
//                        ) and
//                        not exists  -- no entry for this record
//                        (
//                          select  id
//                          from    q_data qd
//                          where   qd.id = dt.rec_id and
//                                  qd.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//                        ) and
//                        mf.merchant_number = dt.merchant_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.rec_id                       as rec_id,\n                      dt.settlement_date              as settlement_date,\n                      mf.bank_number                  as bank_number\n              from    trident_debit_financial   dt,\n                      mif                       mf\n              where   dt.balanced = 'N' and\n                      dt.settlement_date between trunc(sysdate-30) and trunc(sysdate-3) and\n                      dt.request_message_type = '0200' and\n                      dt.response_code = '00' and -- only approved items\n                      not exists\n                      (\n                        select  retrieval_reference_number\n                        from    trident_debit_financial dti\n                        where   dti.retrieval_reference_number = dt.retrieval_reference_number and\n                                dti.request_message_type = '0400' and\n                                dti.response_code = '00'\n                           and  dti.settlement_date between dt.settlement_date and (dt.settlement_date+1)\n                           and  dti.merchant_number = dt.merchant_number\n                           and  dti.transaction_identifier = dt.transaction_identifier    \n                      ) and\n                      not exists  -- no entry for this record\n                      (\n                        select  id\n                        from    q_data qd\n                        where   qd.id = dt.rec_id and\n                                qd.item_type =  :1   -- 39\n                      ) and\n                      mf.merchant_number = dt.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^13*/
            break;
            
          case 1:   // mes *not* funded by network, merchant funded by mes
            queueType = MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED;
            // select the items that have been linked to a DDF entry but also have a reversal/0400
            /*@lineinfo:generated-code*//*@lineinfo:137^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.rec_id                       as rec_id,
//                        dt.settlement_date              as settlement_date,
//                        mf.bank_number                  as bank_number
//                from    trident_debit_financial   dt,
//                        mif                       mf
//                where   dt.settlement_date >= trunc(sysdate-30) and
//                        dt.request_message_type = '0200' and
//                        dt.response_code = '00' and -- only approved items
//                        not dt.dt_batch_date is null and
//                        exists
//                        (
//                          select  retrieval_reference_number
//                          from    trident_debit_financial dti
//                          where   dti.retrieval_reference_number = dt.retrieval_reference_number and
//                                  dti.request_message_type = '0400' and
//                                  dti.response_code = '00'
//                             and  dti.settlement_date between dt.settlement_date and (dt.settlement_date+1)
//                             and  dti.merchant_number = dt.merchant_number
//                             and  dti.transaction_identifier = dt.transaction_identifier    
//                        ) and
//                        not exists  -- no entry for this record
//                        (
//                          select  id
//                          from    q_data qd
//                          where   qd.id = dt.rec_id and
//                                  qd.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//                        ) and
//                        mf.merchant_number = dt.merchant_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.rec_id                       as rec_id,\n                      dt.settlement_date              as settlement_date,\n                      mf.bank_number                  as bank_number\n              from    trident_debit_financial   dt,\n                      mif                       mf\n              where   dt.settlement_date >= trunc(sysdate-30) and\n                      dt.request_message_type = '0200' and\n                      dt.response_code = '00' and -- only approved items\n                      not dt.dt_batch_date is null and\n                      exists\n                      (\n                        select  retrieval_reference_number\n                        from    trident_debit_financial dti\n                        where   dti.retrieval_reference_number = dt.retrieval_reference_number and\n                                dti.request_message_type = '0400' and\n                                dti.response_code = '00'\n                           and  dti.settlement_date between dt.settlement_date and (dt.settlement_date+1)\n                           and  dti.merchant_number = dt.merchant_number\n                           and  dti.transaction_identifier = dt.transaction_identifier    \n                      ) and\n                      not exists  -- no entry for this record\n                      (\n                        select  id\n                        from    q_data qd\n                        where   qd.id = dt.rec_id and\n                                qd.item_type =  :1   -- 39\n                      ) and\n                      mf.merchant_number = dt.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^13*/
            break;
            
          default:
            continue;
        }
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          recId         = resultSet.getLong("rec_id");
          bankNumber    = resultSet.getString("bank_number");
        
          /*@lineinfo:generated-code*//*@lineinfo:180^11*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//              (
//                id, 
//                type,
//                item_type,
//                owner,
//                date_created,
//                source,
//                affiliate
//              )
//              values
//              (
//                :recId,
//                :queueType,  
//                :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS,  -- 39
//                1,                                          -- mes
//                :resultSet.getDate("settlement_date"),
//                decode(:exceptionType,0,'visa',1,'mbs','Unknown'),
//                :bankNumber
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_202 = resultSet.getDate("settlement_date");
   String theSqlTS = "insert into q_data\n            (\n              id, \n              type,\n              item_type,\n              owner,\n              date_created,\n              source,\n              affiliate\n            )\n            values\n            (\n               :1  ,\n               :2  ,  \n               :3  ,  -- 39\n              1,                                          -- mes\n               :4  ,\n              decode( :5  ,0,'visa',1,'mbs','Unknown'),\n               :6  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setInt(2,queueType);
   __sJT_st.setInt(3,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   __sJT_st.setDate(4,__sJT_202);
   __sJT_st.setInt(5,exceptionType);
   __sJT_st.setString(6,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:204^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^11*/
        }
        resultSet.close();
      }        
      
      retVal = true;
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:216^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^34*/ }catch( Exception ee ) {}
      logEntry("addExceptionsToQueue()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
    }
    return( retVal );
  }
  
  public void addUnpaidFundedExceptions( String loadFilename )
  {
    UnpaidFundedItem        unpaidItem    = null;
    Vector                  unpaidItems   = new Vector();
    ResultSetIterator       it            = null;
    long                    recId         = 0L;
    ResultSet               resultSet     = null;
    
    log.debug("addUnpaidFundedExceptions");
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index( dt idx_ddf_dt_loadfilename )
//                      ordered 
//                   */
//                  mf.bank_number                  as bank_number,  
//                  dt.merchant_account_number      as merchant_number,
//                  dt.batch_date                   as batch_date,
//                  dt.batch_number                 as batch_number,
//                  dt.ddf_dt_id                    as ddf_dt_id,
//  	  	          dt.transaction_date             as tran_date_begin,
//                  (dt.transaction_date+2)         as tran_date_end,
//  				        dt.cardholder_account_number    as card_number,
//  				        dt.authorization_num            as auth_code,
//  				        dt.transaction_amount           as tran_amount
//          from   	daily_detail_file_dt    dt,
//                  mif                     mf
//          where	  dt.load_filename = :loadFilename 
//  		            and dt.card_type in ('DB','EB')
//  		            and exists
//  		            (
//  		              select  tdbt.rec_id
//  		              from    trident_debit_financial tdbt
//    		            where   tdbt.merchant_number = dt.merchant_account_number 
//  	  	                    and tdbt.settlement_date between dt.transaction_date and (dt.transaction_date+2)
//  				                  and tdbt.request_message_type = '0200'
//                            and tdbt.balanced = 'Y'
//                            and nvl(tdbt.reversed, decode(tdbt.dt_batch_date,null,'Y','N')) = 'Y'
//  				                  and tdbt.card_number = dt.cardholder_account_number
//  				                  and tdbt.authorization_id_resp_code = dt.authorization_num
//  				                  and tdbt.transaction_amount = dt.transaction_amount
//  		            )
//                  and not exists
//                  (
//                    select  tdbti.rec_id
//                    from    trident_debit_financial tdbti
//                    where   tdbti.dt_batch_date = dt.batch_date
//                            and tdbti.dt_batch_number = dt.batch_number
//                            and tdbti.dt_ddf_dt_id = dt.ddf_dt_id
//                  )
//                  and mf.merchant_number = dt.merchant_account_number
//          order by dt.merchant_account_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index( dt idx_ddf_dt_loadfilename )\n                    ordered \n                 */\n                mf.bank_number                  as bank_number,  \n                dt.merchant_account_number      as merchant_number,\n                dt.batch_date                   as batch_date,\n                dt.batch_number                 as batch_number,\n                dt.ddf_dt_id                    as ddf_dt_id,\n\t  \t          dt.transaction_date             as tran_date_begin,\n                (dt.transaction_date+2)         as tran_date_end,\n\t\t\t\t        dt.cardholder_account_number    as card_number,\n\t\t\t\t        dt.authorization_num            as auth_code,\n\t\t\t\t        dt.transaction_amount           as tran_amount\n        from   \tdaily_detail_file_dt    dt,\n                mif                     mf\n        where\t  dt.load_filename =  :1   \n\t\t            and dt.card_type in ('DB','EB')\n\t\t            and exists\n\t\t            (\n\t\t              select  tdbt.rec_id\n\t\t              from    trident_debit_financial tdbt\n  \t\t            where   tdbt.merchant_number = dt.merchant_account_number \n\t  \t                    and tdbt.settlement_date between dt.transaction_date and (dt.transaction_date+2)\n\t\t\t\t                  and tdbt.request_message_type = '0200'\n                          and tdbt.balanced = 'Y'\n                          and nvl(tdbt.reversed, decode(tdbt.dt_batch_date,null,'Y','N')) = 'Y'\n\t\t\t\t                  and tdbt.card_number = dt.cardholder_account_number\n\t\t\t\t                  and tdbt.authorization_id_resp_code = dt.authorization_num\n\t\t\t\t                  and tdbt.transaction_amount = dt.transaction_amount\n\t\t            )\n                and not exists\n                (\n                  select  tdbti.rec_id\n                  from    trident_debit_financial tdbti\n                  where   tdbti.dt_batch_date = dt.batch_date\n                          and tdbti.dt_batch_number = dt.batch_number\n                          and tdbti.dt_ddf_dt_id = dt.ddf_dt_id\n                )\n                and mf.merchant_number = dt.merchant_account_number\n        order by dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:282^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        unpaidItems.add( new UnpaidFundedItem( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < unpaidItems.size(); ++i )
      {
        unpaidItem = (UnpaidFundedItem)unpaidItems.elementAt(i);
        
        /*@lineinfo:generated-code*//*@lineinfo:296^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tdbt.rec_id
//  		      from    trident_debit_financial tdbt
//  		      where   tdbt.merchant_number = :unpaidItem.MerchantId
//  		              and tdbt.settlement_date between :unpaidItem.TranDateBegin and :unpaidItem.TranDateEnd
//  				          and tdbt.request_message_type = '0200'
//                    and tdbt.balanced = 'Y'
//                    and nvl(tdbt.reversed, decode(tdbt.dt_batch_date,null,'Y','N')) = 'Y'
//  				          and tdbt.card_number = :unpaidItem.CardNumber
//  				          and tdbt.authorization_id_resp_code = :unpaidItem.AuthCode
//  				          and tdbt.transaction_amount = :unpaidItem.TranAmount
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tdbt.rec_id\n\t\t      from    trident_debit_financial tdbt\n\t\t      where   tdbt.merchant_number =  :1  \n\t\t              and tdbt.settlement_date between  :2   and  :3  \n\t\t\t\t          and tdbt.request_message_type = '0200'\n                  and tdbt.balanced = 'Y'\n                  and nvl(tdbt.reversed, decode(tdbt.dt_batch_date,null,'Y','N')) = 'Y'\n\t\t\t\t          and tdbt.card_number =  :4  \n\t\t\t\t          and tdbt.authorization_id_resp_code =  :5  \n\t\t\t\t          and tdbt.transaction_amount =  :6 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,unpaidItem.MerchantId);
   __sJT_st.setDate(2,unpaidItem.TranDateBegin);
   __sJT_st.setDate(3,unpaidItem.TranDateEnd);
   __sJT_st.setString(4,unpaidItem.CardNumber);
   __sJT_st.setString(5,unpaidItem.AuthCode);
   __sJT_st.setDouble(6,unpaidItem.TranAmount);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^9*/
        resultSet = it.getResultSet();
        
        while ( resultSet.next() )
        {
          recId = resultSet.getLong("rec_id");
          /*@lineinfo:generated-code*//*@lineinfo:314^11*/

//  ************************************************************
//  #sql [Ctx] { update trident_debit_financial
//              set     dt_batch_date   = :unpaidItem.BatchDate,
//                      dt_batch_number = :unpaidItem.BatchNumber,
//                      dt_ddf_dt_id    = :unpaidItem.DdfDtId
//              where   rec_id = :recId                    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update trident_debit_financial\n            set     dt_batch_date   =  :1  ,\n                    dt_batch_number =  :2  ,\n                    dt_ddf_dt_id    =  :3  \n            where   rec_id =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,unpaidItem.BatchDate);
   __sJT_st.setLong(2,unpaidItem.BatchNumber);
   __sJT_st.setLong(3,unpaidItem.DdfDtId);
   __sJT_st.setLong(4,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^11*/
          
          log.debug("adding exception for " + recId);//@
          /*@lineinfo:generated-code*//*@lineinfo:324^11*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//              (
//                id, 
//                type,
//                item_type,
//                owner,
//                date_created,
//                source,
//                affiliate
//              )
//              values
//              (
//                :recId,
//                :MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED,  -- 1802
//                :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS,      -- 39
//                1,                                              -- mes
//                :unpaidItem.BatchDate,
//                'tsys',
//                :unpaidItem.BankNumber
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n            (\n              id, \n              type,\n              item_type,\n              owner,\n              date_created,\n              source,\n              affiliate\n            )\n            values\n            (\n               :1  ,\n               :2  ,  -- 1802\n               :3  ,      -- 39\n              1,                                              -- mes\n               :4  ,\n              'tsys',\n               :5  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setInt(2,MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED);
   __sJT_st.setInt(3,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   __sJT_st.setDate(4,unpaidItem.BatchDate);
   __sJT_st.setInt(5,unpaidItem.BankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:348^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:351^11*/
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry("addUnpaidFundedExceptions()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }    
  
  protected void closeVexExceptions( )
  {
    ResultSetIterator   it            = null;
    long                queueId       = 0L;
    ResultSet           resultSet     = null;
    
    log.debug("closeVexExceptions");
    
    try
    {
      setAutoCommit(false);
      
      // select the items that are waiting for progress in the debit exception queue and an 0220 msg has been received
      /*@lineinfo:generated-code*//*@lineinfo:380^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.rec_id                       as rec_id,
//                  dt.retrieval_reference_number   as rrn,
//                  dt_org.rec_id                   as queue_id,
//                  dt_org.settlement_date          as settled_date
//          from    trident_debit_financial   dt,
//                  trident_debit_financial   dt_org
//          where   dt.settlement_date >= (sysdate-30) and
//                  dt.request_message_type = '0220' and
//                  dt.response_code = '00' and -- only approved items
//                  exists  -- entry found in incoming queue for this exception
//                  (
//                    select  id
//                    from    q_data qd
//                    where   qd.id = dt_org.rec_id and
//                            qd.type in 
//                            (
//                              :MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED, -- 1801
//                              :MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED    -- 1802
//                            )
//                  )
//                  and dt_org.retrieval_reference_number = dt.retrieval_reference_number 
//                  and dt_org.settlement_date between (dt.settlement_date-30) and (dt.settlement_date + 7)
//                  and dt_org.merchant_number = dt.merchant_number
//                  and dt_org.transaction_identifier = dt.transaction_identifier    
//                  and dt_org.request_message_type = '0200'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.rec_id                       as rec_id,\n                dt.retrieval_reference_number   as rrn,\n                dt_org.rec_id                   as queue_id,\n                dt_org.settlement_date          as settled_date\n        from    trident_debit_financial   dt,\n                trident_debit_financial   dt_org\n        where   dt.settlement_date >= (sysdate-30) and\n                dt.request_message_type = '0220' and\n                dt.response_code = '00' and -- only approved items\n                exists  -- entry found in incoming queue for this exception\n                (\n                  select  id\n                  from    q_data qd\n                  where   qd.id = dt_org.rec_id and\n                          qd.type in \n                          (\n                             :1  , -- 1801\n                             :2      -- 1802\n                          )\n                )\n                and dt_org.retrieval_reference_number = dt.retrieval_reference_number \n                and dt_org.settlement_date between (dt.settlement_date-30) and (dt.settlement_date + 7)\n                and dt_org.merchant_number = dt.merchant_number\n                and dt_org.transaction_identifier = dt.transaction_identifier    \n                and dt_org.request_message_type = '0200'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED);
   __sJT_st.setInt(2,MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:407^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        queueId = resultSet.getLong("queue_id");
        Date settledDate = resultSet.getDate("settled_date");
      
        /*@lineinfo:generated-code*//*@lineinfo:415^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_data  qd
//            set     type = decode( qd.type,
//                                   :MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED,:MesQueues.Q_DEBIT_EXCEPTION_CH_CREDIT,
//                                   :MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED,:MesQueues.Q_DEBIT_EXCEPTION_CH_DEBIT,
//                                   qd.type ),
//                    last_user = 'vex-incoming',
//                    last_changed = :settledDate 
//            where   id = :queueId and
//                    item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data  qd\n          set     type = decode( qd.type,\n                                  :1  , :2  ,\n                                  :3  , :4  ,\n                                 qd.type ),\n                  last_user = 'vex-incoming',\n                  last_changed =  :5   \n          where   id =  :6   and\n                  item_type =  :7   -- 39";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED);
   __sJT_st.setInt(2,MesQueues.Q_DEBIT_EXCEPTION_CH_CREDIT);
   __sJT_st.setInt(3,MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED);
   __sJT_st.setInt(4,MesQueues.Q_DEBIT_EXCEPTION_CH_DEBIT);
   __sJT_st.setDate(5,settledDate);
   __sJT_st.setLong(6,queueId);
   __sJT_st.setInt(7,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:426^9*/
        
        if ( Ctx.getExecutionContext().getUpdateCount() > 1 )
        {
          logEntry("closeVexExceptions()","WARNING: " + queueId + " has multiple exception entries");
          /*@lineinfo:generated-code*//*@lineinfo:431^11*/

//  ************************************************************
//  #sql [Ctx] { rollback
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:438^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:441^11*/
        }          
      }
      resultSet.close();
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:448^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^34*/ }catch( Exception ee ) {}
      logEntry("closeVexExceptions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
    }
  }

  public boolean execute()
  {
    boolean             retVal        = false;
    
    log.debug("execute");
    
    try
    {
      connect(true);
      
      linkDebitToDDF();             // link unbalanced debit entries to DDF
      addExceptionsToQueue();       // add exceptions to queues
      closeVexExceptions();         // complete process 
      handleProcessTableEntries();  // process entries in trident_debit_process
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
      log.debug("execute complete");
    }
    return( retVal );
  }

  protected boolean handleProcessTableEntries( )
  {
    ProcessTableEntry   entry         = null;
    int                 recCount      = 0;
    boolean             retVal        = false;
    long                seqNum        = 0L;
    
    log.debug("handleProcessTableEntries");
    
    try
    {
      ProcessTable pt = new ProcessTable( "trident_debit_process", "trident_debit_process_sequence", ProcessTable.PT_ALL );
      
      if ( pt.hasPendingEntries() )
      {
        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry) entries.elementAt(i);
          
          entry.recordTimestampBegin();
          switch( entry.getProcessType() )
          {
            case 0:   // process unpaid funded exceptions
              addUnpaidFundedExceptions( entry.getLoadFilename() );
              break;
              
            case 1:   // process chargebacks
              loadChargebackSystem(entry.getLoadFilename());
              break;
              
            default:
              break;
          }
          entry.recordTimestampEnd();
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("handleProcessTableEntries()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean isBaseIIChargeback( long recId )
  {
    int               count         = 0;
    boolean           retVal        = false;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:547^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(td.acquirer_business_id,'10049494',1,0)
//          
//          from    trident_debit_financial   td
//          where   td.rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(td.acquirer_business_id,'10049494',1,0)\n         \n        from    trident_debit_financial   td\n        where   td.rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TridentDebitEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:553^7*/
      retVal = (count != 0);
    }
    catch( Exception e )
    {
      logEntry("isBaseIIChargeback(" + recId + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  protected boolean linkDebitToDDF()
  {
    String              authCode      = null;
    Date                batchDate     = null;
    String              bin           = null;
    String              cardNumber    = null;
    ResultSetIterator   dt_it         = null;
    ResultSet           dt_rs         = null;
    ResultSetIterator   sdIt          = null; // settlment date iteration
    ResultSet           sdRs          = null;
    ResultSetIterator   it            = null;
    long                merchantId    = 0L;
    int                 networkId     = 0;
    long                recId         = 0L;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;
    double              tranAmount    = 0.0;
    String              tranId        = null;
    
    // monitoring variables
    long                beginTs           = 0L;
    long                endTs             = 0L;
    int                 linkedCount       = 0;
    boolean             logging           = false;
    long                maxQueryTime      = 0L;
    long                processedCount    = 0;
    long                selectQueryTime   = 0L;
    Timestamp           startTime         = null;
    long                totalTime         = 0L;
    
    boolean             linksFound = false;
    
    log.debug("linkDebitToDDF");
    
    try
    {
      setAutoCommit(false);
      
      if (targetLoadFilename != null) {
        log.debug("Targeting " + targetLoadFilename);
      }
      
      int unlinkCount = -1;
      /*@lineinfo:generated-code*//*@lineinfo:609^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          from    trident_debit_financial   dt
//          where   (dt.balanced = 'N' or 
//                    (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and
//                  dt.settlement_date >= (sysdate-30) and
//                  dt.request_message_type = '0200' and
//                  dt.response_code = '00' -- only approved items
//                  and ( :targetLoadFilename is null or load_filename = :targetLoadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        from    trident_debit_financial   dt\n        where   (dt.balanced = 'N' or \n                  (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and\n                dt.settlement_date >= (sysdate-30) and\n                dt.request_message_type = '0200' and\n                dt.response_code = '00' -- only approved items\n                and (  :1   is null or load_filename =  :2   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.TridentDebitEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,targetLoadFilename);
   __sJT_st.setString(2,targetLoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   unlinkCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:619^7*/
      log.debug(""+unlinkCount + " unlinked records found in trident_debit_financial");
      
      // find unique settlement dates
      /*@lineinfo:generated-code*//*@lineinfo:623^7*/

//  ************************************************************
//  #sql [Ctx] sdIt = { select  settlement_date, count(0)
//          from    trident_debit_financial   dt
//          where   (dt.balanced = 'N' or 
//                    (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and
//                  dt.settlement_date >= (sysdate-30) and
//                  dt.request_message_type = '0200' and
//                  dt.response_code = '00' -- only approved items
//                  and ( :targetLoadFilename is null or load_filename = :targetLoadFilename )
//          group by settlement_date
//          order by settlement_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  settlement_date, count(0)\n        from    trident_debit_financial   dt\n        where   (dt.balanced = 'N' or \n                  (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and\n                dt.settlement_date >= (sysdate-30) and\n                dt.request_message_type = '0200' and\n                dt.response_code = '00' -- only approved items\n                and (  :1   is null or load_filename =  :2   )\n        group by settlement_date\n        order by settlement_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,targetLoadFilename);
   __sJT_st.setString(2,targetLoadFilename);
   // execute query
   sdIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:634^7*/
      sdRs = sdIt.getResultSet();
      
      // link each found settlement day 
      while (sdRs.next()) {
      
      // select the items that have not been linked to a DDF entry
        Date settleDate = sdRs.getDate(1);
        /*@lineinfo:generated-code*//*@lineinfo:642^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.rec_id                       as rec_id,
//                    dt.merchant_number              as merchant_number,
//                    dt.settlement_date              as batch_date,
//                    nvl(dt.reversed,'N')            as reversed,
//                    dt.card_number                  as card_number,
//                    dt.transaction_amount           as tran_amount,
//                    dt.authorization_id_resp_code   as auth_code,
//                    nvl(dt.network_id,0)            as network_id,
//                    dt.transaction_identifier       as tran_id,
//                    dt.acquiring_institution_id     as bin_number
//            from    trident_debit_financial   dt
//            where   (dt.balanced = 'N' or 
//                      (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and
//                    dt.settlement_date = :settleDate and
//                    dt.request_message_type = '0200' and
//                    dt.response_code = '00' -- only approved items
//                    and ( :targetLoadFilename is null or load_filename = :targetLoadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.rec_id                       as rec_id,\n                  dt.merchant_number              as merchant_number,\n                  dt.settlement_date              as batch_date,\n                  nvl(dt.reversed,'N')            as reversed,\n                  dt.card_number                  as card_number,\n                  dt.transaction_amount           as tran_amount,\n                  dt.authorization_id_resp_code   as auth_code,\n                  nvl(dt.network_id,0)            as network_id,\n                  dt.transaction_identifier       as tran_id,\n                  dt.acquiring_institution_id     as bin_number\n          from    trident_debit_financial   dt\n          where   (dt.balanced = 'N' or \n                    (dt.settlement_date < trunc(sysdate) and dt.reversed = 'Y' and dt.dt_batch_date is null) ) and\n                  dt.settlement_date =  :1   and\n                  dt.request_message_type = '0200' and\n                  dt.response_code = '00' -- only approved items\n                  and (  :2   is null or load_filename =  :3   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,settleDate);
   __sJT_st.setString(2,targetLoadFilename);
   __sJT_st.setString(3,targetLoadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^9*/
        resultSet = it.getResultSet();
    
        // reset monitoring variables
        beginTs           = 0L;
        endTs             = 0L;
        linkedCount       = 0;
        logging           = false;
        maxQueryTime      = 0L;
        processedCount    = 0;
        selectQueryTime   = 0L;
        startTime         = null;
        totalTime         = 0L;
    
        while( resultSet.next() )
        {
          recId         = resultSet.getLong("rec_id");
          merchantId    = resultSet.getLong("merchant_number");
          batchDate     = resultSet.getDate("batch_date");
          cardNumber    = resultSet.getString("card_number");
          tranAmount    = resultSet.getDouble("tran_amount");
          authCode      = resultSet.getString("auth_code");
          networkId     = resultSet.getInt("network_id");
          tranId        = resultSet.getString("tran_id");
          bin           = resultSet.getString("bin_number");
      
          if( "N".equals(resultSet.getString("reversed")) )
          {
            // if find a debit reversal OR get db exception, skip this rec id this time
            if( linkDebitToReversal(recId) == true ) continue;
          }

          // *******************************************************************
          //  BIN 440000 is AccuLynk
          //  The incoming settlement data from AccuLynk does not include
          //  the original authorization code received by the POS.  When
          //  linking transactions from this source ignore the auth code.
          // *******************************************************************
          beginTs = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:700^11*/

//  ************************************************************
//  #sql [Ctx] dt_it = { select  batch_date, batch_number, ddf_dt_id
//              from    daily_detail_file_dt    dt
//              where   dt.merchant_account_number = :merchantId 
//                      and dt.batch_date between (:batchDate-7) and (:batchDate+30) 
//                      and dt.cardholder_account_number = :cardNumber 
//                      and dt.card_type in ( 'DB','EB' ) 
//                      and decode(nvl(dt.auth_amt,0),0,dt.transaction_amount,dt.auth_amt) = :tranAmount
//                      and ( :bin = '440000' or nvl(dt.authorization_num,'000000') = nvl(:authCode,'000000') )
//                      and not exists
//                      (
//                        select  tdbt.rec_id
//                        from    trident_debit_financial tdbt
//                        where   tdbt.dt_batch_date = dt.batch_date and
//                                tdbt.dt_batch_number = dt.batch_number and
//                                tdbt.dt_ddf_dt_id = dt.ddf_dt_id
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  batch_date, batch_number, ddf_dt_id\n            from    daily_detail_file_dt    dt\n            where   dt.merchant_account_number =  :1   \n                    and dt.batch_date between ( :2  -7) and ( :3  +30) \n                    and dt.cardholder_account_number =  :4   \n                    and dt.card_type in ( 'DB','EB' ) \n                    and decode(nvl(dt.auth_amt,0),0,dt.transaction_amount,dt.auth_amt) =  :5  \n                    and (  :6   = '440000' or nvl(dt.authorization_num,'000000') = nvl( :7  ,'000000') )\n                    and not exists\n                    (\n                      select  tdbt.rec_id\n                      from    trident_debit_financial tdbt\n                      where   tdbt.dt_batch_date = dt.batch_date and\n                              tdbt.dt_batch_number = dt.batch_number and\n                              tdbt.dt_ddf_dt_id = dt.ddf_dt_id\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setDouble(5,tranAmount);
   __sJT_st.setString(6,bin);
   __sJT_st.setString(7,authCode);
   // execute query
   dt_it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:718^11*/
          endTs = System.currentTimeMillis();
          if ( (endTs-beginTs) > maxQueryTime ) { maxQueryTime = (endTs-beginTs); }
          totalTime += (endTs-beginTs);
          dt_rs = dt_it.getResultSet();
    
          if ( dt_rs.next() )
          {
            /*@lineinfo:generated-code*//*@lineinfo:726^13*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_financial
//                set     dt_batch_date = :dt_rs.getDate("batch_date"),
//                        dt_batch_number = :dt_rs.getLong("batch_number"),
//                        dt_ddf_dt_id = :dt_rs.getLong("ddf_dt_id"),
//                        balanced = 'Y',
//                        balanced_date = :dt_rs.getDate("batch_date")
//                where   rec_id = :recId                    
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_203 = dt_rs.getDate("batch_date");
 long __sJT_204 = dt_rs.getLong("batch_number");
 long __sJT_205 = dt_rs.getLong("ddf_dt_id");
 java.sql.Date __sJT_206 = dt_rs.getDate("batch_date");
   String theSqlTS = "update  trident_debit_financial\n              set     dt_batch_date =  :1  ,\n                      dt_batch_number =  :2  ,\n                      dt_ddf_dt_id =  :3  ,\n                      balanced = 'Y',\n                      balanced_date =  :4  \n              where   rec_id =  :5 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_203);
   __sJT_st.setLong(2,__sJT_204);
   __sJT_st.setLong(3,__sJT_205);
   __sJT_st.setDate(4,__sJT_206);
   __sJT_st.setLong(5,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^13*/
      
            // remove any exceptions in the pend exception queue
            /*@lineinfo:generated-code*//*@lineinfo:738^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    q_data
//                where   type = :MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED and
//                        id = :recId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    q_data\n              where   type =  :1   and\n                      id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:744^13*/
      
            /*@lineinfo:generated-code*//*@lineinfo:746^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:749^13*/
        
            ++linkedCount;
          }
          dt_rs.close();
          dt_it.close();
          
          ++processedCount;
          
          if ( Verbose )
          {
            System.out.print("  record count: " + processedCount + "  max query time: " + maxQueryTime + "  avg query time: " + (totalTime/processedCount) + "  linked: " + linkedCount + "             \r");
          }          
          
          if (processedCount % 1000 == 0) {
            log.debug("processing " + settleDate 
              + ", record: "        + processedCount 
              + ", max query ms: "  + maxQueryTime 
              + ", avg query ms: "  + (totalTime/processedCount) 
              + ", linked:  "       + linkedCount);
          }
        }
        if ( Verbose )
        {
          System.out.print("\n");
        }          
        resultSet.close();
        it.close();
        
        log.debug("completed " + settleDate 
          + ", record: "        + processedCount 
          + ", max query ms: "  + maxQueryTime 
          + ", avg query ms: "  + (totalTime/processedCount) 
          + ", linked:  "       + linkedCount);
        
      } // settlement date loop
      
      sdRs.close();
      sdIt.close();
    
      retVal = true;
    }
    catch(Exception e)
    {
      log.error("linkDebitToDDF error",e);
      try{ /*@lineinfo:generated-code*//*@lineinfo:794^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:794^34*/ }catch( Exception ee ) {}
      logEntry("linkDebitToDDF()", e.toString());
      
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      try{ dt_it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
    }
    return( retVal );
  }
  
  protected boolean linkDebitToReversal( long recId)
  {
    Date        adjDate       = null;
    int         recCount      = 0;
    boolean     retVal        = true;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:815^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(dt.rec_id),
//                  max(dt.settlement_date)
//          
//          from    trident_debit_financial   dt
//          where   exists
//                  (
//                    select  retrieval_reference_number
//                    from    trident_debit_financial dt_org
//                    where   dt_org.rec_id = :recId
//                      and   dt.retrieval_reference_number = dt_org.retrieval_reference_number
//                      and   dt.settlement_date between dt_org.settlement_date and (dt_org.settlement_date+30)
//                      and   dt.merchant_number = dt_org.merchant_number
//                      and   dt.transaction_identifier = dt_org.transaction_identifier
//                  ) and
//                  dt.request_message_type in ('0220','0400') and
//                  dt.response_code = '00' -- only approved items
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(dt.rec_id),\n                max(dt.settlement_date)\n         \n        from    trident_debit_financial   dt\n        where   exists\n                (\n                  select  retrieval_reference_number\n                  from    trident_debit_financial dt_org\n                  where   dt_org.rec_id =  :1  \n                    and   dt.retrieval_reference_number = dt_org.retrieval_reference_number\n                    and   dt.settlement_date between dt_org.settlement_date and (dt_org.settlement_date+30)\n                    and   dt.merchant_number = dt_org.merchant_number\n                    and   dt.transaction_identifier = dt_org.transaction_identifier\n                ) and\n                dt.request_message_type in ('0220','0400') and\n                dt.response_code = '00' -- only approved items";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.TridentDebitEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   adjDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:833^7*/
      
      if ( recCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:837^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_financial
//            set     balanced = 'Y',
//                    reversed = 'Y',
//                    balanced_date = :adjDate
//            where   rec_id = :recId                    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_debit_financial\n          set     balanced = 'Y',\n                  reversed = 'Y',\n                  balanced_date =  :1  \n          where   rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,adjDate);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:844^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:846^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:849^9*/
      }
      else
      {
        retVal = false;
      }
    }
    catch(Exception e)
    {
      logEntry("linkDebitToReversal()", e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  public void loadChargebackSystem( String loadFilename )
  {
    boolean             autoCommit      = getAutoCommit();
    int                 bankNumber      = getFileBankNumber(loadFilename);
    String              cbFilename      = null;
    ResultSetIterator   it              = null;
    long                loadSec         = 0L;
    long                recId           = 0L;
    Vector              recIds          = new Vector();
    ResultSet           resultSet       = null;
    
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:881^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dbt.rec_id                            as rec_id
//          from    trident_debit_financial   dbt
//          where   dbt.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  and dbt.request_message_type = '0422'   -- chargebacks
//                  and dbt.response_code in ( '00','21' )  -- approved or unable to back out
//                  and dbt.giv_flag = '1' 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dbt.rec_id                            as rec_id\n        from    trident_debit_financial   dbt\n        where   dbt.load_file_id = load_filename_to_load_file_id( :1  )\n                and dbt.request_message_type = '0422'   -- chargebacks\n                and dbt.response_code in ( '00','21' )  -- approved or unable to back out\n                and dbt.giv_flag = '1'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.TridentDebitEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:889^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        recIds.addElement(resultSet.getLong("rec_id"));
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < recIds.size(); ++i )
      {
        recId   = ((Long)recIds.elementAt(i)).longValue();
        loadSec = ChargebackTools.getNewLoadSec();
        
        if ( isBaseIIChargeback(recId) )
        {
          if ( cbFilename == null )
          {
            cbFilename = generateFilename("vs_chgbk" + bankNumber);
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:911^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_visa
//              (
//                load_sec,
//                chargeback_ref_num,
//                transaction_code,
//                account_number,
//                transaction_date,
//                debit_credit_ind,
//                transaction_amount,
//                currency_code,
//                original_amt,
//                original_currency_code,
//                merchant_account_number,
//                merchant_name,
//                merchant_city,
//                merchant_state,
//                reference_number,
//                issuer_bin,
//                authorization_number,
//                reason_code,
//                class_code,
//                merchant_country,
//                usage_code,
//                acquirer_member_id,
//                cardholder_act_term_ind,
//                documentation_ind,
//                trans_id,
//                request_payment_service,
//                authorization_source,
//                pos_terminal_capablities,
//                terminal_entry_mode,
//                cardholder_id_method,
//                multiple_clearing_seq_num,
//                mail_phone_order_ind,
//                reimbursement_attr_code,
//                merchant_volume_ind,
//                cashback_amount,
//                special_condition_ind,
//                fee_program_ind,
//                central_processing_date,
//                bank_number,
//                actual_incoming_date,
//                dispute_condition, 
//                vrol_case_number, 
//                dispute_status,             
//                load_filename
//              )
//              select  :loadSec                                as load_sec,
//                      td.chargeback_reference_number          as cb_ref_num,
//                      decode(vs.cash_disbursement,
//                             'Y',17,
//                             decode(vs.debit_credit_indicator,'C',16,15)
//                            )                                 as tran_code,
//                      substr(dukpt_decrypt_wrapper(td.card_number_enc),1,16)
//                                                              as account_number,
//                      vs.transaction_date                     as tran_date,        
//                      vs.debit_credit_indicator               as debit_credit_ind,
//                      ( decode(vs.debit_credit_indicator,'C',-1,1) *
//                        td.transaction_amount )               as dest_amount,
//                      td.currency_code                        as currency_code,
//                      nvl( td.settlement_amount, td.transaction_amount )
//                                                              as original_amount,
//                      nvl( td.settlement_currency_code, td.currency_code )
//                                                              as original_currency_code,
//                      nvl(td.merchant_number,0)               as merchant_number,                                  
//                      nvl(td.card_acceptor_name,
//                          vs.dba_name)                        as dba_name,
//                      nvl(td.card_acceptor_city,
//                          vs.dba_city)                        as dba_city,
//                      vs.dba_state                            as dba_state,
//                      vs.reference_number                     as ref_num,
//                      substr(td.affiliate_bin,1,6)            as issuer_bin,
//                      vs.auth_code                            as auth_code,
//                      td.message_reason_code                  as reason_code,
//                      nvl(td.merchant_fs_type,vs.sic_code)    as sic_code,
//                      nvl(td.card_acceptor_country,
//                          vs.country_code)                    as country_code,
//                      1                                       as usage_code,
//                      nvl(td.acquirer_business_id,
//                          vs.acquirer_business_id)            as acquirer_id,
//                      null                                    as card_act_term_ind,
//                      td.documentation_indicator              as doc_ind,
//                      lpad(td.transaction_identifier,15,'0')  as tran_id,
//                      nvl( td.requested_payment_service,
//                           vs.requested_payment_service )     as rps,
//                      vs.auth_source_code                     as auth_source,
//                      nvl(td.pos_terminal_entry_capability,
//                          vs.pos_term_cap)                    as pos_term_cap,
//                      vs.pos_entry_mode                       as entry_mode,
//                      vs.cardholder_id_method                 as cardholder_id_method,
//                      td.multiple_clearing_seq_num            as multiple_clearing_seq_num,
//                      td.moto_ecommerce_ind                   as moto_ecomm_ind,
//                      nvl( td.reimbursement_attribute,
//                           vs.reimbursement_attribute )       as ra,
//                      nvl( td.merchant_volume_indicator,
//                           vs.merchant_vol_indicator )        as merch_vol_ind,
//                      0                                       as cashback_amount,
//                      vs.special_conditions_ind               as special_conditions_ind,
//                      td.fee_program_indicator                as fpi,
//                      td.settlement_date                      as cpd,
//                      td.bank_number                          as bank_number,
//                      td.settlement_date                      as incoming_date,
//                      td.dispute_condition                    as dispute_condition,
//                      td.vrol_case_number                     as vrol_case_number,
//                      td.dispute_status                       as dispute_status,
//                      :cbFilename                             as load_filename
//              from    trident_debit_financial   td,
//                      visa_settlement           vs
//              where   td.rec_id = :recId
//                      and vs.merchant_number in
//                      ( 
//                        select  mf.merchant_number 
//                        from    mif     mf
//                        where   mf.bank_number = td.bank_number 
//                                and ( nvl(td.merchant_number,0) = 0 
//                                      or mf.merchant_number = td.merchant_number
//                                    ) 
//                      )
//                      and vs.batch_date between td.local_transaction_date and (td.local_transaction_date+45)
//                      and vs.transaction_date = td.local_transaction_date
//                      and vs.card_number = td.card_number
//                      and vs.debit_credit_indicator = decode(td.debit_credit_indicator,'C','D','D','C',td.debit_credit_indicator)
//                      and nvl(vs.auth_tran_id,lpad(td.transaction_identifier,15,'0')) = lpad(td.transaction_identifier,15,'0')
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_visa\n            (\n              load_sec,\n              chargeback_ref_num,\n              transaction_code,\n              account_number,\n              transaction_date,\n              debit_credit_ind,\n              transaction_amount,\n              currency_code,\n              original_amt,\n              original_currency_code,\n              merchant_account_number,\n              merchant_name,\n              merchant_city,\n              merchant_state,\n              reference_number,\n              issuer_bin,\n              authorization_number,\n              reason_code,\n              class_code,\n              merchant_country,\n              usage_code,\n              acquirer_member_id,\n              cardholder_act_term_ind,\n              documentation_ind,\n              trans_id,\n              request_payment_service,\n              authorization_source,\n              pos_terminal_capablities,\n              terminal_entry_mode,\n              cardholder_id_method,\n              multiple_clearing_seq_num,\n              mail_phone_order_ind,\n              reimbursement_attr_code,\n              merchant_volume_ind,\n              cashback_amount,\n              special_condition_ind,\n              fee_program_ind,\n              central_processing_date,\n              bank_number,\n              actual_incoming_date,\n              dispute_condition, \n              vrol_case_number, \n              dispute_status,             \n              load_filename\n            )\n            select   :1                                  as load_sec,\n                    td.chargeback_reference_number          as cb_ref_num,\n                    decode(vs.cash_disbursement,\n                           'Y',17,\n                           decode(vs.debit_credit_indicator,'C',16,15)\n                          )                                 as tran_code,\n                    substr(dukpt_decrypt_wrapper(td.card_number_enc),1,16)\n                                                            as account_number,\n                    vs.transaction_date                     as tran_date,        \n                    vs.debit_credit_indicator               as debit_credit_ind,\n                    ( decode(vs.debit_credit_indicator,'C',-1,1) *\n                      td.transaction_amount )               as dest_amount,\n                    td.currency_code                        as currency_code,\n                    nvl( td.settlement_amount, td.transaction_amount )\n                                                            as original_amount,\n                    nvl( td.settlement_currency_code, td.currency_code )\n                                                            as original_currency_code,\n                    nvl(td.merchant_number,0)               as merchant_number,                                  \n                    nvl(td.card_acceptor_name,\n                        vs.dba_name)                        as dba_name,\n                    nvl(td.card_acceptor_city,\n                        vs.dba_city)                        as dba_city,\n                    vs.dba_state                            as dba_state,\n                    vs.reference_number                     as ref_num,\n                    substr(td.affiliate_bin,1,6)            as issuer_bin,\n                    vs.auth_code                            as auth_code,\n                    td.message_reason_code                  as reason_code,\n                    nvl(td.merchant_fs_type,vs.sic_code)    as sic_code,\n                    nvl(td.card_acceptor_country,\n                        vs.country_code)                    as country_code,\n                    1                                       as usage_code,\n                    nvl(td.acquirer_business_id,\n                        vs.acquirer_business_id)            as acquirer_id,\n                    null                                    as card_act_term_ind,\n                    td.documentation_indicator              as doc_ind,\n                    lpad(td.transaction_identifier,15,'0')  as tran_id,\n                    nvl( td.requested_payment_service,\n                         vs.requested_payment_service )     as rps,\n                    vs.auth_source_code                     as auth_source,\n                    nvl(td.pos_terminal_entry_capability,\n                        vs.pos_term_cap)                    as pos_term_cap,\n                    vs.pos_entry_mode                       as entry_mode,\n                    vs.cardholder_id_method                 as cardholder_id_method,\n                    td.multiple_clearing_seq_num            as multiple_clearing_seq_num,\n                    td.moto_ecommerce_ind                   as moto_ecomm_ind,\n                    nvl( td.reimbursement_attribute,\n                         vs.reimbursement_attribute )       as ra,\n                    nvl( td.merchant_volume_indicator,\n                         vs.merchant_vol_indicator )        as merch_vol_ind,\n                    0                                       as cashback_amount,\n                    vs.special_conditions_ind               as special_conditions_ind,\n                    td.fee_program_indicator                as fpi,\n                    td.settlement_date                      as cpd,\n                    td.bank_number                          as bank_number,\n                    td.settlement_date                      as incoming_date,\n                    td.dispute_condition                    as dispute_condition,\n                    td.vrol_case_number                     as vrol_case_number,\n                    td.dispute_status                       as dispute_status,\n                     :2                               as load_filename\n            from    trident_debit_financial   td,\n                    visa_settlement           vs\n            where   td.rec_id =  :3  \n                    and vs.merchant_number in\n                    ( \n                      select  mf.merchant_number \n                      from    mif     mf\n                      where   mf.bank_number = td.bank_number \n                              and ( nvl(td.merchant_number,0) = 0 \n                                    or mf.merchant_number = td.merchant_number\n                                  ) \n                    )\n                    and vs.batch_date between td.local_transaction_date and (td.local_transaction_date+45)\n                    and vs.transaction_date = td.local_transaction_date\n                    and vs.card_number = td.card_number\n                    and vs.debit_credit_indicator = decode(td.debit_credit_indicator,'C','D','D','C',td.debit_credit_indicator)\n                    and nvl(vs.auth_tran_id,lpad(td.transaction_identifier,15,'0')) = lpad(td.transaction_identifier,15,'0')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,cbFilename);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1036^11*/
          
          if ( Ctx.getExecutionContext().getUpdateCount() != 1 )
          {
            logEntry("loadChargebackSystem(" + recId + ")", "Failed to create Base II chargeback entry");
          }
        }
        else    // pin debit chargeback
        {
          /*@lineinfo:generated-code*//*@lineinfo:1045^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargebacks
//              (
//                cb_load_sec,
//                cb_ref_num,
//                merchant_number,
//                bank_number,
//                incoming_date,
//                reference_number,
//                card_number,
//                card_type,
//                tran_date,
//                tran_amount,
//                merchant_name,
//                reason_code,
//                load_filename,
//                first_time_chargeback,
//                mes_ref_num,
//                debit_credit_ind,
//                tran_date_missing,
//                card_number_enc
//              )
//              select  :loadSec                              as load_sec,
//                      lpad(dbt.transaction_identifier,15,0) as cb_ref_num,
//                      nvl(dbt.merchant_number,0)            as merchant_number,
//                      nvl(dbt.bank_number,3941)             as bank_number,
//                      dbt.settlement_date                   as incoming_date,
//                      dbt.retrieval_reference_number        as reference_number,
//                      dbt.card_number                       as card_number,
//                      'DB'                                  as card_type,
//                      dbt.local_transaction_date            as tran_date,
//                      ( decode(dbt.debit_credit_indicator,'C',1,-1) *
//                        dbt.transaction_amount )            as tran_amount,
//                      dbt.card_acceptor_name                as merchant_name,
//                      CASE
//                      WHEN dbt.message_reason_code IN (10, 11, 12, 13)
//                      THEN
//                          CASE
//                          WHEN dbt.dispute_condition IS NOT NULL
//                          THEN
//                              CASE
//                              WHEN LENGTH(TRIM (dbt.dispute_condition)) = 3 AND SUBSTR (dbt.dispute_condition, 2,1)='.'              
//                              THEN dbt.message_reason_code || '.' || SUBSTR (dbt.dispute_condition, 1,1)
//                              WHEN LENGTH(RTRIM(dbt.dispute_condition)) = 1
//                              THEN dbt.message_reason_code || '.' || SUBSTR (RTRIM(dbt.dispute_condition), 1,1)
//                              ELSE dbt.message_reason_code || ''
//                              END                               -- not length 3 or 1 after trim
//                          ELSE dbt.message_reason_code || ''        --dispute_condition is null
//                          END
//                      ELSE dbt.message_reason_code || ''        -- dispute_condition <> in (10, 11, 12, 13)
//                      END                                   as reason_code,
//                      dbt.load_filename                     as load_filename,
//                      'Y'                                   as first_time,
//                      null                                  as mes_ref_num,
//                      decode( dbt.debit_credit_indicator,  
//                              'C','D', -- logic reversed for chargeback reporting
//                              'D','C',
//                              dbt.debit_credit_indicator )  as debit_credit_ind,
//                      'N'                                   as tran_date_missing,
//                      dbt.card_number_enc                   as card_number_enc
//              from    trident_debit_financial   dbt
//              where   dbt.rec_id = :recId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargebacks\n            (\n              cb_load_sec,\n              cb_ref_num,\n              merchant_number,\n              bank_number,\n              incoming_date,\n              reference_number,\n              card_number,\n              card_type,\n              tran_date,\n              tran_amount,\n              merchant_name,\n              reason_code,\n              load_filename,\n              first_time_chargeback,\n              mes_ref_num,\n              debit_credit_ind,\n              tran_date_missing,\n              card_number_enc\n            )\n            select   :1                                as load_sec,\n                    lpad(dbt.transaction_identifier,15,0) as cb_ref_num,\n                    nvl(dbt.merchant_number,0)            as merchant_number,\n                    nvl(dbt.bank_number,3941)             as bank_number,\n                    dbt.settlement_date                   as incoming_date,\n                    dbt.retrieval_reference_number        as reference_number,\n                    dbt.card_number                       as card_number,\n                    'DB'                                  as card_type,\n                    dbt.local_transaction_date            as tran_date,\n                    ( decode(dbt.debit_credit_indicator,'C',1,-1) *\n                      dbt.transaction_amount )            as tran_amount,\n                    dbt.card_acceptor_name                as merchant_name,\n                    CASE\n                    WHEN dbt.message_reason_code IN (10, 11, 12, 13)\n                    THEN\n                        CASE\n                        WHEN dbt.dispute_condition IS NOT NULL\n                        THEN\n                            CASE\n                            WHEN LENGTH(TRIM (dbt.dispute_condition)) = 3 AND SUBSTR (dbt.dispute_condition, 2,1)='.'              \n                            THEN dbt.message_reason_code || '.' || SUBSTR (dbt.dispute_condition, 1,1)\n                            WHEN LENGTH(RTRIM(dbt.dispute_condition)) = 1\n                            THEN dbt.message_reason_code || '.' || SUBSTR (RTRIM(dbt.dispute_condition), 1,1)\n                            ELSE dbt.message_reason_code || ''\n                            END                               -- not length 3 or 1 after trim\n                        ELSE dbt.message_reason_code || ''        --dispute_condition is null\n                        END\n                    ELSE dbt.message_reason_code || ''        -- dispute_condition <> in (10, 11, 12, 13)\n                    END                                   as reason_code,\n                    dbt.load_filename                     as load_filename,\n                    'Y'                                   as first_time,\n                    null                                  as mes_ref_num,\n                    decode( dbt.debit_credit_indicator,  \n                            'C','D', -- logic reversed for chargeback reporting\n                            'D','C',\n                            dbt.debit_credit_indicator )  as debit_credit_ind,\n                    'N'                                   as tran_date_missing,\n                    dbt.card_number_enc                   as card_number_enc\n            from    trident_debit_financial   dbt\n            where   dbt.rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1108^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:1110^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//              (
//                cb_load_sec,
//                file_load_sec,
//                action_code,
//                action_date,
//                user_message,
//                action_source,
//                user_login
//              )
//              values
//              (
//                :loadSec,
//                1,
//                'D',
//                trunc(sysdate),
//                'Auto Action - Debit',
//                10,     -- auto debit
//                'system'
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_activity\n            (\n              cb_load_sec,\n              file_load_sec,\n              action_code,\n              action_date,\n              user_message,\n              action_source,\n              user_login\n            )\n            values\n            (\n               :1  ,\n              1,\n              'D',\n              trunc(sysdate),\n              'Auto Action - Debit',\n              10,     -- auto debit\n              'system'\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1132^11*/
      
          /*@lineinfo:generated-code*//*@lineinfo:1134^11*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//              (
//                id,
//                type,
//                item_type,
//                owner,
//                date_created,
//                source,
//                affiliate,
//                last_changed,
//                last_user
//              )
//              select  :loadSec                        as id,
//                      :MesQueues.Q_CHARGEBACKS_MCHB as type,      -- 1763
//                      18                              as item_type, -- chargeback
//                      1                               as owner,
//                      dbt.settlement_date             as date_created,
//                      'tdbt'                          as source,
//                      to_char(nvl(dbt.bank_number,3941))         
//                                                      as affiliate,
//                      sysdate                         as last_changed,
//                      'system'                        as last_user
//              from    trident_debit_financial   dbt
//              where   dbt.rec_id = :recId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n            (\n              id,\n              type,\n              item_type,\n              owner,\n              date_created,\n              source,\n              affiliate,\n              last_changed,\n              last_user\n            )\n            select   :1                          as id,\n                     :2   as type,      -- 1763\n                    18                              as item_type, -- chargeback\n                    1                               as owner,\n                    dbt.settlement_date             as date_created,\n                    'tdbt'                          as source,\n                    to_char(nvl(dbt.bank_number,3941))         \n                                                    as affiliate,\n                    sysdate                         as last_changed,\n                    'system'                        as last_user\n            from    trident_debit_financial   dbt\n            where   dbt.rec_id =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_MCHB);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1160^11*/
        }
      }
      
      // queue the loading of chargeback risk points
      /*@lineinfo:generated-code*//*@lineinfo:1165^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//          (
//            process_type,
//            load_filename
//          )
//          values
//          (
//            1,
//            :loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n        (\n          process_type,\n          load_filename\n        )\n        values\n        (\n          1,\n           :1  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1177^7*/
      
      if ( cbFilename != null )   // have base II chargebacks
      {
        // enter it into the risk_process table to ensure it
        // gets loaded into the proper places/queues
        /*@lineinfo:generated-code*//*@lineinfo:1183^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//            (
//              process_type,
//              process_sequence,
//              load_filename
//            )
//            values
//            (
//              8,
//              0,
//              :cbFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n          (\n            process_type,\n            process_sequence,\n            load_filename\n          )\n          values\n          (\n            8,\n            0,\n             :1  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.TridentDebitEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cbFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1197^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1200^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1203^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:1207^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1207^34*/ } catch( Exception sqe ) {}
      logEntry("loadChargebackSystem(" + loadFilename + ")",e.toString());
    }
    finally
    {
      setAutoCommit(autoCommit);
    }
  }
  
  public void setVerbose( boolean value )
  {
    Verbose = value;
  }
  
  public void setTargetLoadFilename(String targetLoadFilename) {
    this.targetLoadFilename = targetLoadFilename;
  }
  
  public static void main( String[] args )
  {
      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.CHARGEBACK_TOOLS_FTPDB
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    TridentDebitEvent       bc  = null;
    
    try
    {
      log.debug("Starting command line execution of TridentDebitEvent");
      
      bc = new TridentDebitEvent();
      bc.connect(true);
      bc.setVerbose(true);
      
      if ( args.length > 0 && args[0].equals("linkDebitToDDF") )
      {
        log.debug("linkDebitToDDF");
        bc.linkDebitToDDF();
      }
      else if ( args.length > 0 && args[0].equals("addUnpaidFunded") )
      {
        log.debug("addUnpaidFunded");
        bc.addUnpaidFundedExceptions(args[1]);
      }
      else if ( args.length > 0 && args[0].equals("handleProcessTable") )
      {
        log.debug("handleProcessTable");
        bc.handleProcessTableEntries();
      }
      else if ( args.length > 0 && args[0].equals("loadChargebackSystem") )
      {
        log.debug("loadChargebackSystem");
        bc.loadChargebackSystem(args[1]);
      }
      else if ( args.length > 0 && args[0].equals("addExceptionsToQueue") )
      {
        log.debug("addExceptionsToQueue");
        bc.addExceptionsToQueue();
      }
      else
      {
        if (args.length > 0) {
          log.debug("executing with target file " + args[0]);
          bc.setTargetLoadFilename(args[0]);
        }
        else {
          log.debug("execute");
        }
        bc.execute();
      }        
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bc.cleanUp(); } catch( Exception ee ) {}
      log.debug("Command line execution of TridentDebitEvent complete");
    }
  }
}/*@lineinfo:generated-code*/