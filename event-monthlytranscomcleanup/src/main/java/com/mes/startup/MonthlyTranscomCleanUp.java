package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import com.mes.support.DateTimeFormatter;

/**
 * Cleaning up the transcom_extract_summary in accordance with rules defined by Vikram Katamareddy.   
 * @author rmehta
 *
 */
public class MonthlyTranscomCleanUp extends EventBase {
  
	public MonthlyTranscomCleanUp()  {
	}
  
	public boolean execute() {
	  
		Date activeDate = null;
		String loadFileName = "";
		
		// Set activeDate to 1st of previous month
		Calendar cal = Calendar.getInstance();
		cal.add( Calendar.MONTH, -1 );
        cal.set(Calendar.DAY_OF_MONTH, 1);
        activeDate = new Date(cal.getTime().getTime());
        
        connect(true);
	  
        // cleanup the monthly_extract_summary per rules defined
		runCleanUp(activeDate);
		
		// insert into transcom_process
		triggerFileRegeneration(activeDate);
		
		return true;
	}
		
	private void runCleanUp(Date activeDate) {
		PreparedStatement ps = null;
		int paramIndex = 1;
		
		try {
			log.info("Clean up for Date = " + DateTimeFormatter.getFormattedDate(activeDate, "MM/dd/yyyy"));
			
			ps = getPreparedStatement(SQL_EXACT_DUPLICATES.toString());
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);
			
			int count = ps.executeUpdate();
			log.info("STEP 1 a) EXACT duplicates removed : count = " + count);			
			// This re-execution is DELIBERATE.
			count = ps.executeUpdate();
			
			log.info("STEP 1 b) EXACT duplicates removed : count = " + count);
			
			
			ps = getPreparedStatement(SQL_VISA_AUTH_DUPLICATES.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);			
			count = ps.executeUpdate();			
			
			log.info("STEP 2) Duplicate VISA Authorization Income records removed : count = " + count);
						
			ps = getPreparedStatement(SQL_AMEX_AUTH_DUPLICATES.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);			
			count = ps.executeUpdate();
			
			log.info("STEP 3) Duplicate Amex Authorization Income records removed : count = " + count);
			
			ps = getPreparedStatement(SQL_DISCOVER_AUTH_DUPLICATES.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);
			count = ps.executeUpdate();
			
			log.info("STEP 4) Duplicate Discover Authorization Income records removed : count = " + count);
			
			ps = getPreparedStatement(SQL_MC_AUTH_DUPLICATES.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);
			count = ps.executeUpdate();
			
			log.info("STEP 5) Duplicate MC Authorization Income records removed : count = " + count);
			
			ps = getPreparedStatement(SQL_DEL_CARDTYPE_CHARGETYPE_1.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);
			count = ps.executeUpdate();
			
			log.info("STEP 6) Delete rows with Cardtype=30 and chargetype=2321 : count = " + count);
			
			ps = getPreparedStatement(SQL_DEL_CARDTYPE_CHARGETYPE_2.toString());
			paramIndex = 1;
			ps.setDate(paramIndex++, activeDate);					
			count = ps.executeUpdate();
			
			log.info("STEP 7) Delete rows with Cardtype=32 and chargetype=2321 : count = " + count);
			
		} catch(SQLException sqe) {
			log.error("runCleanUp.. ", sqe);
		} finally {
			try {
				if(ps != null) {
					ps.close();
				}
			} catch (SQLException ignore) {}
		}
	}

	private void triggerFileRegeneration(Date activeDate) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		String loadFilename = "";
		
		try {
			ps = getPreparedStatement(SQL_GET_LOADFILENAME.toString());
			ps.setDate(paramIndex++, activeDate);
			ps.setDate(paramIndex++, activeDate);
			
			rs = ps.executeQuery();
			if(rs.next()) {
				loadFilename = rs.getString("loadFileName");
				log.info("STEP 8) Get load file name = " + loadFilename);
			}
			
			rs.close();
			ps.close();
			paramIndex = 1;
			
			if(loadFilename != null && !"".equals(loadFilename)) {
				ps = getPreparedStatement(SQL_MES_TRANSCOM_PROCESS_INSERT.toString());
				ps.setString(paramIndex, loadFilename);
				
				int count = ps.executeUpdate(); 
				log.info("Transcom Process Inser Count = " + count);
			}
			
		} catch(SQLException sqe) {
			log.error("triggerFileRegeneration.. ", sqe);
		} finally {
			try {
				if(ps != null) {
					ps.close();
				}
			} catch (SQLException ignore) {}
		}
		
	}

	public static void main(String args[]) {		
		MonthlyTranscomCleanUp mtc = new MonthlyTranscomCleanUp();
		
		Date activeDate = null;
		Calendar cal = Calendar.getInstance();
		if(args.length > 0) {
			cal.setTime( DateTimeFormatter.parseDate(args[0],"MM/dd/yyyy") );
			activeDate = new Date(cal.getTime().getTime());
			
			mtc.runCleanUp(activeDate);
			mtc.triggerFileRegeneration(activeDate);
		} else {
			mtc.execute();
		}
		System.exit(0);
	}
	
	
	static StringBuffer SQL_EXACT_DUPLICATES 			= new StringBuffer(); 		// STEP 1) Identify the EXACT duplicates and remove those
	static StringBuffer SQL_VISA_AUTH_DUPLICATES 		= new StringBuffer(); 		// STEP 2) Identify the VISA Authorization Income records that are considered duplicates and delete those 
	static StringBuffer SQL_AMEX_AUTH_DUPLICATES 		= new StringBuffer(); 		// STEP 3) Identify the Amex the Authorization Income records that are considered duplicates and delete those
	static StringBuffer SQL_DISCOVER_AUTH_DUPLICATES 	= new StringBuffer(); 		// STEP 4) Identify the Discover the Authorization Income records that are considered duplicates and delete those
	static StringBuffer SQL_MC_AUTH_DUPLICATES 			= new StringBuffer(); 		// STEP 5) Identify the Discover the Authorization Income records that are considered duplicates and delete those
	static StringBuffer SQL_DEL_CARDTYPE_CHARGETYPE_1 	= new StringBuffer(); 		// STEP 6) Identify rows with Cardtype=30 and chargetype=2321 and delete those
	static StringBuffer SQL_DEL_CARDTYPE_CHARGETYPE_2 	= new StringBuffer(); 		// STEP 7) Identify rows with Cardtype=32 and chargetype=2321 and delete those
	
	static StringBuffer SQL_MES_TRANSCOM_PROCESS_INSERT = new StringBuffer();
		
	static StringBuffer SQL_GET_LOADFILENAME			= new StringBuffer();
	
	static {	
		SQL_EXACT_DUPLICATES.append("delete																						");				  
		SQL_EXACT_DUPLICATES.append("from transcom_extract_summary tes_to_del													");			
		SQL_EXACT_DUPLICATES.append("where tes_to_del.active_date = ?															");
		SQL_EXACT_DUPLICATES.append("and (merchant_number, load_sec) in (														");
		SQL_EXACT_DUPLICATES.append("    select mn, ls from (																	");								
		SQL_EXACT_DUPLICATES.append("        select merchant_number mn, description descr, min(load_sec) ls						");
		SQL_EXACT_DUPLICATES.append("        from transcom_extract_summary mes 													");
		SQL_EXACT_DUPLICATES.append("	     where mes.active_date = ?															");
		SQL_EXACT_DUPLICATES.append("        and (merchant_number, description) in (											");
		SQL_EXACT_DUPLICATES.append("            select merchant_number, description from (										");
		SQL_EXACT_DUPLICATES.append("                select ticket_count,surcharge_revenue,surcharge_per_item_revenue,sales_vol_count,sales_vol_amount,mtd_inc,misc_revenue,merchant_number,		");
		SQL_EXACT_DUPLICATES.append("                    load_filename,item_fee,item_count,interchange_revenue,interchange_expense,equip_revenue,disc_rate_revenue,disc_per_item_revenue,			");
		SQL_EXACT_DUPLICATES.append("                    description,credits_vol_count,credits_vol_amount,charge_type,card_type,batch_count,auth_per_item,auth_count,assessment_expense,active_date,count(*)	");
		SQL_EXACT_DUPLICATES.append("                from transcom_extract_summary mes 											");
		SQL_EXACT_DUPLICATES.append("                where mes.active_date = ?													");
		SQL_EXACT_DUPLICATES.append("                group by  ticket_count,surcharge_revenue,surcharge_per_item_revenue,sales_vol_count,sales_vol_amount,mtd_inc,misc_revenue,merchant_number,		");
		SQL_EXACT_DUPLICATES.append("                    load_filename,item_fee,item_count,interchange_revenue,interchange_expense,equip_revenue,disc_rate_revenue,disc_per_item_revenue,			");
		SQL_EXACT_DUPLICATES.append("                    description,credits_vol_count,credits_vol_amount,charge_type,card_type,batch_count,auth_per_item,auth_count,assessment_expense,active_date	");
		SQL_EXACT_DUPLICATES.append("                having count(*) > 1														");
		SQL_EXACT_DUPLICATES.append("                )																			");
		SQL_EXACT_DUPLICATES.append("            )																				");
		SQL_EXACT_DUPLICATES.append("            group by merchant_number, description											");
		SQL_EXACT_DUPLICATES.append("        )																					");
		SQL_EXACT_DUPLICATES.append("    )																						");
		
		
		SQL_VISA_AUTH_DUPLICATES.append("delete  																				");
		SQL_VISA_AUTH_DUPLICATES.append("from transcom_extract_summary tes 														");
		SQL_VISA_AUTH_DUPLICATES.append("where tes.active_date = ?													");	
		SQL_VISA_AUTH_DUPLICATES.append("and tes.card_type = '40'																");
		SQL_VISA_AUTH_DUPLICATES.append("and tes.charge_type = 99																");
		SQL_VISA_AUTH_DUPLICATES.append("and (tes.load_sec, tes.merchant_number) in (											");
		SQL_VISA_AUTH_DUPLICATES.append("	select min(tes_99.load_sec), tes_99.merchant_number									");
		SQL_VISA_AUTH_DUPLICATES.append("	from transcom_extract_summary tes_2321, transcom_extract_summary tes_2314, transcom_extract_summary tes_99");
		SQL_VISA_AUTH_DUPLICATES.append("	where tes_2321.active_date = ?											");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2321.card_type = '40'														");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2321.charge_type = 2321														");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2314.merchant_number = tes_2321.merchant_number 							");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2314.active_date = tes_2321.active_date  									");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2314.card_type = tes_2321.card_type  										");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2314.charge_type = 2314														");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2314.description = 'Visa Authorizations'									");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2321.merchant_number = tes_99.merchant_number 								");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2321.active_date = tes_99.active_date  										");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_2321.card_type = tes_99.card_type											");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_99.charge_type = 99															");
		SQL_VISA_AUTH_DUPLICATES.append("	and tes_99.auth_count = tes_2321.auth_count											");
		SQL_VISA_AUTH_DUPLICATES.append("	and exists (select 1 																");
		SQL_VISA_AUTH_DUPLICATES.append("		from transcom_extract_summary tes_dup 											");
		SQL_VISA_AUTH_DUPLICATES.append("		where tes_dup.active_date = tes_99.active_date									");
		SQL_VISA_AUTH_DUPLICATES.append("		and tes_dup.card_type = tes_99.card_type										");
		SQL_VISA_AUTH_DUPLICATES.append("		and tes_dup.charge_type = tes_99.charge_type									");
		SQL_VISA_AUTH_DUPLICATES.append("		and tes_99.merchant_number = tes_dup.merchant_number							");	
		SQL_VISA_AUTH_DUPLICATES.append("		having count(*) > 1)            												");
		SQL_VISA_AUTH_DUPLICATES.append("	group by tes_99.merchant_number)													");									
		
		
		SQL_AMEX_AUTH_DUPLICATES.append("delete																					");  
		SQL_AMEX_AUTH_DUPLICATES.append("from transcom_extract_summary tes 														");
		SQL_AMEX_AUTH_DUPLICATES.append("where tes.active_date = ?																");
		SQL_AMEX_AUTH_DUPLICATES.append("and tes.card_type = '30'																");
		SQL_AMEX_AUTH_DUPLICATES.append("and tes.charge_type = 2900																");	
		SQL_AMEX_AUTH_DUPLICATES.append("and (tes.load_sec, tes.merchant_number) in (											");
		SQL_AMEX_AUTH_DUPLICATES.append("	select min(tes_99.load_sec), tes_99.merchant_number									");
		SQL_AMEX_AUTH_DUPLICATES.append("	from transcom_extract_summary tes_2321, transcom_extract_summary tes_2314, transcom_extract_summary tes_99 ");
		SQL_AMEX_AUTH_DUPLICATES.append("	where tes_2321.active_date = ?														");	
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2321.card_type = '30'														");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2321.charge_type = 2321														");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2314.merchant_number = tes_2321.merchant_number 							");		
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2314.active_date = tes_2321.active_date  									");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2314.card_type = tes_2321.card_type  										");	
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2314.charge_type = 2314														");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2314.description = 'American Express Authorizations'						");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2321.merchant_number = tes_99.merchant_number 								");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2321.active_date = tes_99.active_date  										");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_2321.card_type = tes_99.card_type											");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_99.charge_type = 2900														");
		SQL_AMEX_AUTH_DUPLICATES.append("	and tes_99.auth_count = tes_2321.auth_count											");
		SQL_AMEX_AUTH_DUPLICATES.append("	and exists (select 1 																");
		SQL_AMEX_AUTH_DUPLICATES.append("		from transcom_extract_summary tes_dup 											");
		SQL_AMEX_AUTH_DUPLICATES.append("		where tes_dup.active_date = tes_99.active_date									");
		SQL_AMEX_AUTH_DUPLICATES.append("		and tes_dup.card_type = tes_99.card_type										");
		SQL_AMEX_AUTH_DUPLICATES.append("		and tes_dup.charge_type = tes_99.charge_type									");
		SQL_AMEX_AUTH_DUPLICATES.append("		and tes_99.merchant_number = tes_dup.merchant_number							");
		SQL_AMEX_AUTH_DUPLICATES.append("	having count(*) > 1)																");
		SQL_AMEX_AUTH_DUPLICATES.append("group by tes_99.merchant_number)														");
			
	
		SQL_DISCOVER_AUTH_DUPLICATES.append("delete																				");
		SQL_DISCOVER_AUTH_DUPLICATES.append("from transcom_extract_summary tes 													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("where tes.active_date = ?															");
		SQL_DISCOVER_AUTH_DUPLICATES.append("and tes.card_type = '32'															");
		SQL_DISCOVER_AUTH_DUPLICATES.append("and tes.charge_type = 2902															");
		SQL_DISCOVER_AUTH_DUPLICATES.append("and (tes.load_sec, tes.merchant_number) in (										");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	select min(tes_99.load_sec), tes_99.merchant_number								");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	from transcom_extract_summary tes_2321, transcom_extract_summary tes_2314, transcom_extract_summary tes_99 ");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	where tes_2321.active_date = ?													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2321.card_type = '32'													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2321.charge_type = 2321													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2314.merchant_number = tes_2321.merchant_number 						");	
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2314.active_date = tes_2321.active_date  								");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2314.card_type = tes_2321.card_type  									");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2314.charge_type = 2314													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2314.description = 'Discover Authorizations'							");		
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2321.merchant_number = tes_99.merchant_number 							");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2321.active_date = tes_99.active_date  									");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_2321.card_type = tes_99.card_type										");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_99.charge_type = 2902													");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and tes_99.auth_count = tes_2321.auth_count										");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	and exists (select 1 															");	
		SQL_DISCOVER_AUTH_DUPLICATES.append("		from transcom_extract_summary tes_dup 										");
		SQL_DISCOVER_AUTH_DUPLICATES.append("		where tes_dup.active_date = tes_99.active_date								");				
		SQL_DISCOVER_AUTH_DUPLICATES.append("		and tes_dup.card_type = tes_99.card_type									");
		SQL_DISCOVER_AUTH_DUPLICATES.append("		and tes_dup.charge_type = tes_99.charge_type								");
		SQL_DISCOVER_AUTH_DUPLICATES.append("		and tes_99.merchant_number = tes_dup.merchant_number						");	
		SQL_DISCOVER_AUTH_DUPLICATES.append("		having count(*) > 1)														");
		SQL_DISCOVER_AUTH_DUPLICATES.append("	group by tes_99.merchant_number)												");								


		SQL_MC_AUTH_DUPLICATES.append("delete																					");
		SQL_MC_AUTH_DUPLICATES.append("from transcom_extract_summary tes 														");
		SQL_MC_AUTH_DUPLICATES.append("where tes.active_date = ?																");
		SQL_MC_AUTH_DUPLICATES.append("and tes.card_type = '50'																	");
		SQL_MC_AUTH_DUPLICATES.append("and tes.charge_type = 99																	");
		SQL_MC_AUTH_DUPLICATES.append("and (tes.load_sec, tes.merchant_number) in (												");	
		SQL_MC_AUTH_DUPLICATES.append("		select min(tes_99.load_sec), tes_99.merchant_number									");
		SQL_MC_AUTH_DUPLICATES.append("		from transcom_extract_summary tes_2321, transcom_extract_summary tes_2314, transcom_extract_summary tes_99 ");
		SQL_MC_AUTH_DUPLICATES.append("		where tes_2321.active_date = ?														");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2321.card_type = '50'														");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2321.charge_type = 2321														");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2314.merchant_number = tes_2321.merchant_number 							");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2314.active_date = tes_2321.active_date  									");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2314.card_type = tes_2321.card_type  										");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2314.charge_type = 2314														");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2314.description = 'MasterCard Authorizations'								");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2321.merchant_number = tes_99.merchant_number 								");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2321.active_date = tes_99.active_date  										");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_2321.card_type = tes_99.card_type											");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_99.charge_type = 99															");
		SQL_MC_AUTH_DUPLICATES.append("		and tes_99.auth_count = tes_2321.auth_count											");
		SQL_MC_AUTH_DUPLICATES.append("		and exists (select 1 																");
		SQL_MC_AUTH_DUPLICATES.append("			from transcom_extract_summary tes_dup 											");
		SQL_MC_AUTH_DUPLICATES.append("			where tes_dup.active_date = tes_99.active_date									");	
		SQL_MC_AUTH_DUPLICATES.append("			and tes_dup.card_type = tes_99.card_type										");
		SQL_MC_AUTH_DUPLICATES.append("			and tes_dup.charge_type = tes_99.charge_type									");
		SQL_MC_AUTH_DUPLICATES.append("			and tes_99.merchant_number = tes_dup.merchant_number							");
		SQL_MC_AUTH_DUPLICATES.append("			having count(*) > 1)															");
		SQL_MC_AUTH_DUPLICATES.append("		group by tes_99.merchant_number)													");


		SQL_DEL_CARDTYPE_CHARGETYPE_1.append("delete  																			");
		SQL_DEL_CARDTYPE_CHARGETYPE_1.append("from transcom_extract_summary tes 												");
		SQL_DEL_CARDTYPE_CHARGETYPE_1.append("where tes.active_date = ?															");
		SQL_DEL_CARDTYPE_CHARGETYPE_1.append("and tes.card_type = '30'															");
		SQL_DEL_CARDTYPE_CHARGETYPE_1.append("and tes.charge_type = 2321														");
		
		
		SQL_DEL_CARDTYPE_CHARGETYPE_2.append("delete  																			");
		SQL_DEL_CARDTYPE_CHARGETYPE_2.append("from transcom_extract_summary tes													"); 
		SQL_DEL_CARDTYPE_CHARGETYPE_2.append("where tes.active_date = ?															");
		SQL_DEL_CARDTYPE_CHARGETYPE_2.append("and tes.card_type = '32'															");
		SQL_DEL_CARDTYPE_CHARGETYPE_2.append("and tes.charge_type = 2321														");
		
		
		SQL_MES_TRANSCOM_PROCESS_INSERT.append("insert into mes.transcom_process (file_type, load_filename)						");
		SQL_MES_TRANSCOM_PROCESS_INSERT.append("values (2, ?)																	");
		
		
		SQL_GET_LOADFILENAME.append("select max(load_filename)	loadFileName																");
		SQL_GET_LOADFILENAME.append("from load_file_index																		");	
		SQL_GET_LOADFILENAME.append("where creation_date between add_months(?,1) and last_day(add_months(? ,1) )				");
		SQL_GET_LOADFILENAME.append("and load_filename like 'mbs_ext3941%' 														");
	}
}
