BUG-888: Sales Activity Report not working
BUG-949: MESP OLA on Sales Activity Report
BUG-961: Create a Batch Feature not working for CB&T merchants
BUG-963: PCI Quest V 1.2
BUG-964: Verifi prefix change to auto laod of chargeback and retrieval docs
BUG-965: PCI Summery For Level Three Merchants- Last Questionnaire
BUG-967: CB&T Var forms are missing zero's from merchant ID numbers.
BUG-968: Update sort order on daily IC summary report
PRF-1070: Web based commission system: Tracie Tran
PRF-1219: Add pricing option to Mountain West Bank for PCI
PRF-1238: Add a Batch Close function to the Payment Gateway
PRF-1241: Additional Queue Needed for Non Bankcard
PRF-1246: CB&T ACR Address
PRF-1247: Create system to send e-mail informing of auth linkage failure
PRF-1250: MeS gateway enhancement to allow routing of USD transaction to Payvision
PRF-1254: Create OLA for American West Bank
PRF-1256: Modify Offline Adjustment report to reflect change from BankServ to JPMC
PRF-1260: Update ACH extraction to allow individual entry descriptions to be enabled or disabled.
PRF-1261: Create a BML payment task
PRF-1265: Add PG Request Log to Reporting API
PRF-1266: Add BET table to Banner Bank OLA
PRF-1267: Please add a new transit routing number to the drop down of the Banner Bank OLA (125107820)
