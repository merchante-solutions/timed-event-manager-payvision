package com.mesapps.john;

import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueTools;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.ops.QueueConstants;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;

public class FixExports extends SQLJConnectionBase
{
  private int appSeqNum = 0;
  
  public FixExports (int appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  
  private void fixTimeStamps(long primaryKey, String loginName)
  {
    try 
    {
      //updates tsys department to completed status and time stamps completed
      #sql [Ctx]
      {
        update  app_tracking
        set     date_completed = sysdate,
                status_code = 202,
                contact_name = :loginName
        where   app_seq_num = :primaryKey and
                dept_code = :(QueueConstants.DEPARTMENT_TSYS)
                
      };

      //updates mms department to received status and time stamps received
      #sql [Ctx]
      {
        update  app_tracking
        set     date_received = sysdate,
                status_code = 301
        where   app_seq_num = :primaryKey and
                dept_code = :(QueueConstants.DEPARTMENT_MMS)
      };
    }
    catch(Exception e)
    {
      System.out.println("fixTimeStamps(): " + e.toString());
    }
  }
  
  protected void execute()
  {
    try
    {
      connect();
      
      // move to MMS Queues        
      int appType = 0;
      
      #sql [Ctx]
      {
        select  app_type
        into    :appType
        from    application
        where   app_seq_num = :appSeqNum
      };
      
      System.out.println("  getting profile generator");
      // determine the profile generator (i.e. mms, vericentre, termmaster ...)
      EquipProfileGeneratorDictator epgd = new EquipProfileGeneratorDictator();
      int appLevelProfGen = epgd.getAppLevelProfileGeneratorCode(appSeqNum);

      // determine the appropriate mms setup queue as a function of the app level profile generator
      int qType = MesQueues.Q_NONE;
      
      System.out.println("  profile generator = " + appLevelProfGen);
      
      switch(appLevelProfGen) 
      {
        case mesConstants.PROFGEN_VCTM:
          System.out.println("  moving to VCTM Queue");
          qType = MesQueues.Q_MMS_VCTM_NEW;
          break;
          
        default:
          if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
          {
            System.out.println("  moving to CBT POS New Queue");
            qType = MesQueues.Q_CBT_MMS_NEW;
          }
          else
          {
            System.out.println("  moving to standard POS New Queue");
            qType = MesQueues.Q_MMS_NEW;
          }
          break;
      }
    
      // put in new mms setup queue
      System.out.println("  executing move queue");
      QueueTools.insertQueue(appSeqNum, qType, null);

      // fix time stamps so app tracking is correct
      System.out.println("  fixing time stamps");
      fixTimeStamps(appSeqNum, "SYSTEM");
      System.out.println("  execute() COMPLETE");
    }
    catch(Exception e)
    {
      System.out.println("execute(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      int appSeqNum = Integer.parseInt(args[0]);
      
      System.out.println("\nProcessing: " + appSeqNum);
      FixExports fe = new FixExports(appSeqNum);
      
      fe.execute();
      System.out.println("done");
      
      System.exit(0);
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}