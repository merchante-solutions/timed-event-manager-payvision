package com.mesapps.john;

import com.mes.user.UserBean;
import com.mes.database.SQLJConnectionBase;

public class HasRight
{
  public static void main(String[] args)
  {
    try
    {
      UserBean user = new UserBean();
      
      if(user.forceValidate("jfirman"))
      {
        if(user.hasRight(Long.parseLong(args[0])))
        {
          System.out.println("User DOES have access to right: " + args[0]);
        }
        else
        {
          System.out.println("User DOES NOT have access to rigth: " + args[0]);
        }
      }
      else
      {
        System.out.println("Unable to force validate jfirman");
      }
    }
    catch(Exception e)
    {
      System.out.println("ERROR: e.toString()");
    }
    
    Runtime.getRuntime().exit(0);
  }
}