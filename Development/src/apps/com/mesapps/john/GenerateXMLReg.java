package com.mesapps.john;

import com.mes.net.*;
import com.mes.database.SQLJConnectionBase;

public class GenerateXMLReg
{
  public static void main (String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      long appSeqNum = Long.parseLong(args[0]);
      
      // generate a message for each status type
      VsXMLRegRequest compReq = new VsXMLRegStatusUpdate(appSeqNum, "Complete");
      VsXMLRegRequest declReq = new VsXMLRegStatusUpdate(appSeqNum, "Terminated");
      VsXMLRegRequest cancReq = new VsXMLRegStatusUpdate(appSeqNum, "Canceled");
      VsXMLRegRequest setupReq = new VsXMLRegProcessorInfo(appSeqNum);
      VsXMLRegRequest cardReq = new VsXMLRegAddCard(appSeqNum, 14);
      VsXMLRegRequest cardDelReq = new VsXMLRegRemoveCard(appSeqNum, 14);
      
      // output each of these
      System.out.println("<!-- STATUS COMPLETE -->\n");
      System.out.println(compReq.toString());
      
      System.out.println("\n\n<!-- STATUS DECLINED -->\n");
      System.out.println(declReq.toString());
      
      System.out.println("\n\n<!-- STATUS CANCELLED -->\n");
      System.out.println(cancReq.toString());
      
      System.out.println("\n\n<!-- STATUS SETUP COMPLETE -->\n");
      System.out.println(setupReq.toString());
      
      System.out.println("\n\n<!-- ADD CARD (Discover) -->\n");
      System.out.println(cardReq.toString());
      
      System.out.println("\n\n<!-- REMOVE CARD (Discover) -->\n");
      System.out.println(cardDelReq.toString());
    }
    catch(Exception e)
    {
      System.out.println("main: " + e.toString());
    }
  }
}
