package com.mesapps.john;

import com.mes.net.*;
import com.mes.database.SQLJConnectionBase;

public class VSRegTest
{
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      long appSeqNum = Long.parseLong(args[0]);
      
      VsXMLRegRequest msg = null;
      
      System.out.println("\n\nSTATUS COMPLETE");
      msg = new VsXMLRegStatusUpdate(appSeqNum, "Complete");
      System.out.println(msg.toString());
      
      System.out.println("\n\nSTATUS DECLINED");
      msg = new VsXMLRegStatusUpdate(appSeqNum, "Terminated");
      System.out.println(msg.toString());
      
      System.out.println("\n\nSTATUS CANCELED");
      msg = new VsXMLRegStatusUpdate(appSeqNum, "Canceled");
      System.out.println(msg.toString());
      
      System.out.println("\n\nSTATUS SETUP COMPLETE");
      msg = new VsXMLRegProcessorInfo(appSeqNum);
      System.out.println(msg.toString());
      
      System.out.println("\n\nSTATUS ADDED CARD");
      msg = new VsXMLRegAddCard(appSeqNum, 14);
      System.out.println(msg.toString());
      
      System.out.println("\n\nSTATUS REMOVED CARD");
      msg = new VsXMLRegRemoveCard(appSeqNum, 14);
      System.out.println(msg.toString());
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
  }
}