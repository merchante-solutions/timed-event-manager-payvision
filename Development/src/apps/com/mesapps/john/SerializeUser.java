package com.mesapps.john;

import java.io.*;

import com.mes.user.UserBean;

import com.mes.database.SQLJConnectionBase;

public class SerializeUser
{
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      // instantiate class based on args
      System.out.println("Instantiating User Bean");
      UserBean ub = new UserBean();
      
      // try to serialize
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(bos);
    
      System.out.println("Serializing UserBean PRE-Validation");
      oos.writeObject(ub);
      
      // now validate user
      if(ub.validate("jfirman", "1liabf", null))
      {
        // try to serialize validated object
        System.out.println("\nSerializing UserBean POST-Validation");
        oos.writeObject(ub);
      }
      else
      {
        System.out.println("\nValidation failed, serializing anyway");
        oos.writeObject(ub);
      }
      
      System.out.println("Serialize test complete");
    }
    catch(Exception e)
    {
      System.out.println("Failed to Serialize: " + e.toString());
    }
  }
}