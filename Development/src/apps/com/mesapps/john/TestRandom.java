package com.mesapps.john;

import java.util.Random;

public class TestRandom
{
  public static void main(String[] args)
  {
    try
    {
      int divMod = Integer.parseInt(args[0]);
      
      Random generator = new Random(System.currentTimeMillis());
      
      int randNum = 0;
      int newNum = 0;
      for(int i=0; i<100; ++i)
      {
        randNum = generator.nextInt();
      
        randNum = (randNum < 0 ? randNum * -1 : randNum);
      
        newNum = randNum % divMod;
        
        System.out.println(newNum);
      }
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
  }
}
