package com.mesapps.john;

import java.lang.StringBuffer;
import java.util.StringTokenizer;

import com.mes.support.HTMLDecoder;

public class LongString
{
  public static void main (String[] args)
  {
    String tempString = "Thank you for choosing BB&amp;T as your Merchant Services provider.  BB&amp;T has been providing payment processing solutions to businesses like yours for more than 30 years and has been consistently ranked as one of the top small business-friendly banks in the nation.  We look forward to servicing your credit card acceptance needs with a host of products and services that will help your business increase sales and customer loyalty.";
    String longString = tempString;
    
    try
    {
      longString = HTMLDecoder.decode(tempString);
    }
    catch(Exception e)
    {
    }
    
    StringTokenizer tokenizer = new StringTokenizer(longString, " ");
    
    StringBuffer curString = new StringBuffer("");
    
    while(tokenizer.hasMoreTokens())
    {
      String nextWord = tokenizer.nextToken();
      
      if(curString.length() + nextWord.length() > 74)
      {
        System.out.println(curString.toString());
        curString.setLength(0);
      }
      
      curString.append(nextWord);
      curString.append(" ");
      
      switch(nextWord.charAt(nextWord.length()-1))
      {
        case '.':
        case '!':
        case '?':
          // add another space
          curString.append(" ");
          break;
        
        default:
          // do nothing
          break;
      }
    }
    
    // output string if non-empty
    if(curString.length() > 0)
    {
      System.out.println(curString.toString());
    }
  }
}