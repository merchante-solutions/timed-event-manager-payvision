package com.mesapps.john;

import com.mes.database.SQLJConnectionBase;
import com.mes.database.OracleConnectionPool;

import com.mes.constants.mesConstants;

import com.mes.forms.*;

import com.mes.app.cbt.CBTApplicationWrapper;

import com.mes.config.MesDefaults;

import com.mes.ops.AssignMidBean;

import com.mes.support.HttpHelper;

import java.sql.ResultSet;

import sqlj.runtime.ResultSetIterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class NBSCtoCBT extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(NBSCtoCBT.class);

  private static final int  ARG_COUNT = 1;

  public long                   merchantNumber    = 0L;
  public long                   cbtMerchantNumber = 0L;
  public long                   appSeqNum         = 0L;
  private CBTApplicationWrapper cbt               = null;

  public NBSCtoCBT(String[] args)
  {
    try
    {
      merchantNumber = Long.parseLong(args[0]);
    }
    catch(Exception e)
    {
      System.out.println("Constructor: " + e.toString());
    }
  }

  private boolean loadTranVolume()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    boolean result = false;

    try
    {
      // calculate average ticket and annual volume
      log.debug("retrieving tran volume");
      #sql [Ctx] it =
      {
        select  round(sum(sm.vmc_sales_amount) / count(sm.active_date), 2) monthly_sales,
                round(sum(sm.vmc_sales_amount) / count(sm.active_date), 2) monthly_vmc_sales,
                round(sum(sm.vmc_avg_ticket) / count(sm.active_date), 2) average_ticket
        from    monthly_extract_summary sm
        where   sm.merchant_number = :merchantNumber and
                sm.active_date between trunc(trunc(sysdate, 'MM')-365, 'MM') and trunc(trunc(sysdate, 'MM')-1, 'MM')
      };

      rs = it.getResultSet();

      log.debug("setting tran volume fields");
      cbt.setBusinessFields(rs);

      result = true;
    }
    catch(Exception e)
    {
      logEntry("loadTranVolume(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  private boolean loadBusinessInfo()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    boolean result = false;

    try
    {
      log.debug("loading business info");
      #sql [Ctx] it =
      {
        select  mf.dba_name                       business_name,
                mf.addr1_line_1                   business_address_1,
                mf.addr1_line_2                   business_address_2,
                mf.city1_line_4                   business_csz_city,
                mf.state1_line_4                  business_csz_state,
                mf.zip1_line_4                    business_csz_zip,
                mf.name2_line_1                   business_legal_name,
                mf.federal_tax_id                 taxpayer_id,
                mf.name1_line_1                   mailing_name,
                mf.dmaddr                         mailing_address_1,
                mf.address_line_3                 mailing_address_2,
                mf.dmcity                         mailing_csz_city,
                mf.dmstate                        mailing_csz_state,
                mf.dmzip                          mailing_csz_zip,
                mf.phone_1                        business_phone,
                decode(mf.phone_2_fax,
                  0, '',
                  mf.phone_2_fax)                 business_fax,
                mr.merch_email_address            business_email,
                nvl(mc.merchcont_prim_phone,
                  mf.phone_1)                     contact_phone,
                mc.merchcont_prim_phone_ext       contact_phone_ext,
                decode(mc.merchcont_prim_first_name,
                  null, gn.g2_owner_name,
                  mc.merchcont_prim_first_name||' '||
                    mc.merchcont_prim_last_name)  contact_name,
                mc.merchcont_prim_email           contact_email,
                mr.bustype_code                   business_type,
                decode(mr.industype_code,
                  null, '1',
                  mr.industype_code)              industry_type,
                decode(mr.loctype_code,
                  null, decode(pos_type, 1, '1', ''),
                  mr.loctype_code)                location_type,
                round(
                  (sysdate - mif_date_opened(mf.date_opened))/365,
                  0)                              location_years,
                nvl(mr.merch_business_establ_month,
                  to_char(mif_date_opened(mf.date_opened),
                    'MM'))                        established_date_month,
                nvl(mr.merch_business_establ_year,
                  to_char(mif_date_opened(mf.date_opened),
                    'YY'))                        established_date_year,
                nvl(substr(mr.merch_busgoodserv_descr,1,16),
                  substr(nvl(sic.merchant_type,'UNKNOWN'),1,16)) business_desc,
                '1'                               application_type,
                '1'                               num_locations,
                '4'                               refund_policy,
                decode(ncm.pos_type,
                  2,  '100',
                  '10')                           moto_percentage,
                'C'                               account_type,
                mf.sic_code                       sic_code,
                'Y'                               have_processed,
                'Merchant e-Solutions'            previous_processor,
                'Y'                               statements_provided,
                'N'                               have_canceled,
                gn.g2_owner_name                  owner_1_name,
                mf.addr1_line_1                   owner_1_address_1,
                mf.city1_line_4                   owner_1_csz_city,
                mf.state1_line_4                  owner_1_csz_state,
                mf.zip1_line_4                    owner_1_csz_zip,
                gn.g2_owner_ssn                   owner_1_ssn,
                '100'                             owner_1_percent,
                mr.merch_mail_phone_sales         moto_percentage,
                mr.refundtype_code                refund_policy,
                mf.dda_num                        checking_account,
                mf.dda_num                        confirm_checking_account,
                lpad(mf.transit_routng_num,
                  9, '0')                         transit_routing,
                lpad(mf.transit_routng_num,
                  9, '0')                         confirm_transit_routing,
                raba.bank_name                    bank_name,
                raba.bank_address                 bank_address,
                raba.bank_city                    bank_csz_city,
                raba.bank_state                   bank_csz_state,
                raba.bank_zip                     bank_csz_zip,
                mb.merchbank_info_source          source_of_info,
                bankaddr.address_line1            bank_address,
                bankaddr.address_city             bank_csz_city,
                bankaddr.countrystate_code        bank_csz_state,
                bankaddr.address_zip              bank_csz_zip,
                bankaddr.address_phone            bank_phone,
                round(
                  (sysdate - mif_date_opened(mf.date_opened))/365,
                  0)                              years_open,
                mr.merch_referring_bank           referring_bank,
                nvl(mb.bankacc_type,2)            type_of_acct,
                nvl(mb.billing_method,'Debit')    billing_method,
                'Y'                               vmc_accepted,
                decode(mf.debit_plan,
                  null, 'N',
                  'Y')                            debit_accepted,
                decode(mf.amex_plan,
                  null, 'N',
                  'Y')                            amex_accepted,
                decode(mf.amex_plan,
                  null, '',
                  rpad(
                    rtrim(ltrim(mf.user_account_2, '0'), '0'),
                    10, 0))                       amex_acct_num,
                decode(mf.discover_plan,
                  null, 'N',
                  'Y')                            discover_accepted,
                decode(mf.discover_plan,
                  null, '',
                  rpad(
                    rtrim(ltrim(mf.user_acount_1, '0'), '0'),
                    15, 0))                       discover_acct_num,
                decode(mf.diners_plan,
                  null, 'N',
                  'Y')                            diners_accepted,
                decode(mf.jcb_plan,
                  null, 'N',
                  'Y')                            jcb_accepted,
                decode(ncm.pos_type,
                  1, '1',
                  2, 5,
                  '')                             product_type,
                'Original NBSC Merchant Number: '||
                  mf.merchant_number              comments
        from    mif mf,
                merchant mr,
                merchcontact mc,
                monthly_extract_gn gn,
                merchbank mb,
                address bankaddr,
                nbsc_conversion_merchants ncm,
                sic_codes sic,
                rap_app_bank_aba raba
        where   mf.merchant_number = :merchantNumber and
                mf.merchant_number = ncm.merchant_number and
                mf.sic_code = sic.sic_code(+) and
                mf.merchant_number = mr.merch_number(+) and
                mr.app_seq_num = mc.app_seq_num(+) and
                mr.app_seq_num = mb.app_seq_num(+) and
                mb.merchbank_acct_srnum(+) = 1 and
                mr.app_seq_num = bankaddr.app_seq_num(+) and
                bankaddr.addresstype_code(+) = 9 and
                mf.merchant_number = gn.hh_merchant_number and
                gn.hh_active_date = trunc(trunc(sysdate, 'MM') - 1, 'MM') and
                mf.transit_routng_num = raba.transit_routing_num(+)
      };
      log.debug("finished business info query");

      rs = it.getResultSet();

      log.debug("setting business info fields");
      cbt.setBusinessFields(rs);

      result = true;
    }
    catch(Exception e)
    {
      logEntry("loadBusinessInfo(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  private boolean loadEquipmentInfo()
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      log.debug("loading equipment info");
      shippingBusinessName = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_BUSINESS_NAME); 
      shippingAddr1        = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_ADDR_1);              
      shippingAddr2        = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_ADDR_2);             
      shippingCsz          = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_CSZ);               
      shippingContactName  = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_CONTACT_NAME);                    
      shippingPhone        = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_SHIPPING_PHONE);                      
      equipmentComments    = MesDefaults.getString(MesDefaults.NBSC_TO_CBT_EQUIPMENT_COMMENTS);         

      #sql [Ctx] it =
      {
        select  :shippingBusinessName      shipping_business_name,
                :shippingAddr1             shipping_addr_1,
                :shippingAddr2             shipping_addr_2,
                :shippingCsz               shipping_csz,
                :shippingContactName       shipping_contact_name,
                :shippingPhone             shipping_phone,
                :equipmentComments         equipment_comments
        from    dual
      };

      rs = it.getResultSet();

      log.debug("setting equipment info fields");
      cbt.setEquipmentFields(rs);

      result = true;
    }
    catch(Exception e)
    {
      logEntry("loadEquipmentData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return( result );
  }

  private boolean loadData()
  {
    boolean result = false;
    try
    {
      result = (loadTranVolume() && 
                loadBusinessInfo() && 
                loadEquipmentInfo());
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }

    return( result );
  }
  
  private boolean storePricingData(long appSeqNum)
  {
    boolean result    = false;
    int     recCount  = 0;
    
    try
    {
      // make sure everything is committed so that triggers won't bail
      commit();
      
      log.debug("Storing Pricing Data");
      // check system bet
      int sysBet = 0;
      
      log.debug("  system generated bet");
      #sql [Ctx]
      {
        select  gn.s1_bet_table_number
        into    :sysBet
        from    monthly_extract_gn gn
        where   gn.hh_merchant_number = :merchantNumber and
                gn.hh_active_date = trunc((trunc(sysdate, 'MON') - 1), 'MON')
      };
      
      double stmtFee = 0.0;
      
      switch(sysBet)
      {
        case 1001:
          stmtFee = 10.0;
          break;
          
        case 1002:
          stmtFee = 5.0;
          break;
          
        default:
          break;
      }
      
      // insert statement fee charge record if necessary
      log.debug("  inserting statement fee");
      if(stmtFee > 0.0)
      {
        #sql [Ctx]
        {
          insert into miscchrg
          (
            app_seq_num,
            misc_code,
            misc_chrg_amount
          )
          values
          (
            :appSeqNum,
            :(mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE),
            :stmtFee
          )
        };
      }
      
      // all accounts get a $25 chargeback fee
      log.debug("  insertinng chargeback fee");
      #sql [Ctx]
      {
        insert into miscchrg
        (
          app_seq_num,
          misc_code,
          misc_chrg_amount
        )
        values
        (
          :appSeqNum,
          :(mesConstants.APP_MISC_CHARGE_CHARGEBACK),
          25
        )
      };

      // set up interchange bets
      int   icFeeType = 0;
      int   icFeeFee  = 0;
      double discRate = 0.0;
      double perItem  = 0.0;
      double perAuth  = 0.0;
      double mminChrg = 0.0;
      
      log.debug("  getting ic data");
      #sql [Ctx]
      {
        select  aib.bet_type              ic_fee_type,
                aib.combo_id              ic_fee_fee,
                mf.visa_disc_rate/1000    disc_rate,
                decode(sm.visa_ic_bet_number,
                  4010, 0,
                  mf.visa_per_item/1000)  per_item,
                decode(sm.visa_ic_bet_number,
                  4010, 0.17,
                  0.0)                    per_auth,
                cg.cg_charge_amount       mmin_chrg
        into    :icFeeType,
                :icFeeFee,
                :discRate,
                :perItem,
                :perAuth,
                :mminChrg
        from    mif   mf,
                appo_ic_bets  aib,
                nbsc_conversion_ic_bets ncib,
                monthly_extract_summary sm,
                monthly_extract_cg cg
        where   mf.merchant_number = :merchantNumber and
                mf.merchant_number = sm.merchant_number and
                sm.active_date = trunc(trunc(sysdate, 'MM') - 1, 'MM') and
                sm.visa_ic_bet_number = ncib.nbsc_visa_bet and
                ncib.cbt_visa_bet = aib.vs_bet and
                aib.app_type = 31 and
                sm.hh_load_sec = cg.hh_load_sec and
                cg.cg_charge_record_type = 'DIS'
      };
      
      // insert visa record
      log.debug("  inserting visa record");
      #sql [Ctx]
      {
        insert into tranchrg
        (
          app_seq_num,
          cardtype_code,
          tranchrg_discrate_type,
          tranchrg_disc_rate,
          tranchrg_pass_thru,
          tranchrg_interchangefee_type,
          tranchrg_interchangefee_fee,
          tranchrg_per_auth,
          tranchrg_mmin_chrg
        )
        values
        (
          :appSeqNum,
          :(mesConstants.APP_CT_VISA),
          :(mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN),
          :discRate,
          :perItem,
          :icFeeType,
          :icFeeFee,
          :perAuth,
          :mminChrg
        )
      };
      
      // insert mc record
      log.debug("  inserting mc record");
      #sql [Ctx]
      {
        insert into tranchrg
        (
          app_seq_num,
          cardtype_code,
          tranchrg_discrate_type,
          tranchrg_disc_rate,
          tranchrg_pass_thru,
          tranchrg_interchangefee_type,
          tranchrg_interchangefee_fee,
          tranchrg_per_auth,
          tranchrg_mmin_chrg
        )
        values
        (
          :appSeqNum,
          :(mesConstants.APP_CT_MC),
          :(mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN),
          :discRate,
          :perItem,
          :icFeeType,
          :icFeeFee,
          :perAuth,
          :mminChrg
        )
      };
      
      // insert siteinspection record which contains supply billing info
      log.debug("  inserting supply billing");
      #sql [Ctx]
      {
        insert into siteinspection
        (
          app_seq_num,
          siteinsp_supply_billing
        )
        values
        (
          :appSeqNum,
          'NCLUB'
        )
      };
      
      // insert appo_fee_apps entry to make sure feeset matches what it should be
      #sql [Ctx]
      {
        select  count(app_seq_num)
        into    :recCount
        from    appo_fee_apps
        where   app_seq_num = :appSeqNum
      };
      
      if(recCount > 0)
      {
        #sql [Ctx]
        {
          update  appo_fee_apps
          set     set_id = 1
          where   app_seq_num = :appSeqNum
        };
      }
      else
      {
        #sql [Ctx]
        {
          insert into appo_fee_apps
          (
            app_seq_num,
            set_id
          )
          values
          (
            :appSeqNum,
            1
          )
        };
      }
      
      result = true;      
      log.debug("done storing pricing data");
    }
    catch(Exception e)
    {
      logEntry("storePricingData(" + merchantNumber + ")", e.toString());
    }
    
    return result;
  }
  
  private boolean assignMid(long appSeqNum)
  {
    boolean result = false;
    try
    {
      // assign new mid
      log.debug("assigning MID");
      AssignMidBean amb = new AssignMidBean();
      
      if(amb.assignMid(appSeqNum))
      {
        // retrieve new cbt merchant number
        cbtMerchantNumber = amb.getMerchantNumber();
      }
      else
      {
        log.error("UNABLE TO ASSIGN MID");
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("assignMid(" + merchantNumber + ")", e.toString());
    }
    
    return result;
  }
  
  private void markAsComplete(long appSeqNum)
    throws Exception
  {
    // mark item in nbsc_conversion_merchants as complete so that it doesn't get built again
    log.debug("Marking as complete");
    #sql [Ctx]
    {
      update  nbsc_conversion_merchants
      set     app_seq_num = :appSeqNum,
              cbt_merchant_number = :cbtMerchantNumber,
              date_processed = sysdate
      where   merchant_number = :merchantNumber
    };
    
    // add service call for this merchant number with old nbsc merchant number
    log.debug("adding service call for: " + cbtMerchantNumber);
    String note = "Old NBSC Merchant Number: " + merchantNumber;
    #sql [Ctx]
    {
      call add_sys_service_call(:cbtMerchantNumber, 83, :note)
    };
    
    // make sure screen progress shows what we want (i.e. nothing)
    #sql [Ctx]
    {
      update  screen_progress
      set     screen_complete = 0
      where   screen_pk = :appSeqNum and
              screen_sequence_id = 1008
    };
  }

  private void execute()
  {
    try
    {
      connect();
      
      // make sure this account exists in conversion table and hasn't been processed
      int recCount = 0;
      
      #sql [Ctx]
      {
        select  count(merchant_number)
        into    :recCount
        from    nbsc_conversion_merchants
        where   merchant_number = :merchantNumber and
                date_processed is null
      };
      
      if(recCount > 0)
      {
        // initialize application
        cbt = new CBTApplicationWrapper();
        cbt.initializeApp();

        // load data from database
        if(loadData())
        {
          // submit data to database
          if( cbt.storeBusinessData() && 
              cbt.storeEquipmentData() &&
              storePricingData(cbt.appSeqNum))
          {
            log.debug("COMMITTING phase 1");
            commit();
          
            // assign mid and mark as complete
            if(assignMid(cbt.appSeqNum))
            {
              markAsComplete(cbt.appSeqNum);
            }
          }
          else
          {
            log.error("PROCESS FAILED!!!");
          }
        }

        // TEMPORARY -- remove application from database for now
        //cbt.deleteApplication();
      }
      else
      {
        log.error("ACCOUNT NOT PRESENT OR ALREADY PROCESSED (" + merchantNumber + ")");
      }
    }
    catch(Exception e)
    {
      System.out.println("execute(" + merchantNumber + "): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public static void main(String[] args)
  {
    try
    {
      if(args.length != ARG_COUNT)
      {
        System.out.println("USAGE: NBSCtoCBT mno");
        System.out.println("  mno: NBSC merchant to convert");
      }
      else
      {
        SQLJConnectionBase.initStandalonePool();

        NBSCtoCBT grunt = new NBSCtoCBT(args);

        grunt.execute();
      }
    }
    catch(Exception e)
    {
      System.out.println("main: " + e.toString());
    }
    finally
    {
      OracleConnectionPool.destroy();
      MesDefaults.destroy();
      HttpHelper.destroy();
    }

    System.exit(0);
  }
}