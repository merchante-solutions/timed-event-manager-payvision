package com.mesapps.john;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;
import java.util.Vector;

import com.mes.database.SQLJConnectionBase;

public class SetFlatFilePositions extends SQLJConnectionBase
{
  private void setPositions(int defType)
  {
    Vector rows = new Vector();
    ResultSet rs = null;
    ResultSetIterator it = null;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      #sql [Ctx] it =
      {
        select  field_order,
                length
        from    flat_file_def
        where   def_type = :defType
        order by field_order
      };
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        rows.add(new FieldDef(rs));
      }
      
      rs.close();
      it.close();
      
      int curPos = 1;
      
      for( int i=0; i<rows.size(); ++i )
      {
        FieldDef field = (FieldDef)(rows.elementAt(i));
        
        String newVal = "";
        
        #sql [Ctx]
        {
          select  lpad(to_char(:curPos),4,'0')||'-'||lpad(to_char(:curPos+:(field.fieldLength)-1),4,'0')
          into    :newVal
          from    dual
        };
        
        #sql [Ctx]
        {
          update  flat_file_def         
          set     info = :newVal
          where   def_type = :defType and
                  field_order = :(field.fieldOrder)
        };
        
        curPos += field.fieldLength;
      }
      commit();
    }
    catch(Exception e)
    {
      System.out.println("setPositions(" + defType + "): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    
    try
    {
      SetFlatFilePositions grunt = new SetFlatFilePositions();
      
      grunt.setPositions(Integer.parseInt(args[0]));
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
  
  public class FieldDef
  {
    int fieldOrder;
    int fieldLength;
    
    public FieldDef(ResultSet rs)
    {
      try
      {
        fieldOrder    = rs.getInt("field_order");
        fieldLength   = rs.getInt("length");
      }
      catch(Exception e)
      {
        System.out.println("FieldDef constructor: " + e.toString());
      }
    }
  }
}
