package com.mesapps.john;

import com.mes.database.SQLJConnectionBase;
import com.mes.startup.BankServTransmit;

public class BankServResend
{
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      int processSequence = Integer.parseInt(args[0]);
      
      BankServTransmit bst = new BankServTransmit();
      
      System.out.println("Resending email...");
      bst.resendEmail(processSequence);
      System.out.println("done");
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}