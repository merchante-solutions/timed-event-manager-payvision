package com.mesapps.john;

import com.mes.net.MailMessage;
import com.mes.database.SQLJConnectionBase;

public class TestEmail
{
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    try
    {
      int addrType = Integer.parseInt(args[0]);
      
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(addrType);
      
      msg.setSubject("TEST EMAIL PLEASE IGNORE");
      msg.setText("This email is just a test to validate the email process for Merchant e-Solutions.");
      msg.send();
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
  }
}