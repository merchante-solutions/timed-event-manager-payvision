package com.mesapps.john;

import java.util.Properties;
import com.tricipher.B2F.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

public class TriCipherTest
{
  private static Log log = LogFactory.getLog(TriCipherTest.class);
  
  public TriCipherTest()
  {
  }
  
  protected void run(String[] args)
  {
    // set up B2F Configuration
    B2FConfiguration config = new B2FConfiguration();
    
    try
    {
      B2FCryptoProvider crypto = new B2FCryptoProviderImplJDK14();
      B2FRandomNumberGenerator rng = new B2FRandomNumberGeneratorImpl();
      
      String[] appliances = { "192.168.10.6" };
      
      config.setAppliance(appliances);
      config.setManagerPort((short)6035);
      config.setUserPort((short)6035);
      config.setB2fManagerLogin("b2fmanager");
      config.setB2fManagerPassword("Tricipher3");
      config.setB2fFieldName("B2F");
      config.setB2fUserFieldName("B2fUser");
      config.setCookiePrefix("MESB2F_");
    
      config.setB2fFieldSigningKey("B2fSigningKey");
      config.setB2fFieldSigningCert("B2fSigningCert");
      config.setEncryption(crypto);
      config.setRng(rng);
      config.setCsrManagerUserId("supercsr");
      config.setCsrManagerPassword("Tricipher3");
      config.setMaintainB2fManagerSession(true);
    }
    catch(Exception e)
    {
      log.error("Exception configuring B2F: " + e.toString());
    }
    catch(Error er)
    {
      log.error("ERROR configuring B2F: " + er.toString());
    }
    
    try
    {
      // instantiate B2F
      B2F b2fInstance = new B2F(config);
    }
    catch(Exception e)
    {
      log.error("Exception instantiating B2F: " + e.toString());
    }
    catch(Error er)
    {
      log.error("ERROR instantiating B2F: " + er.toString());
    }
  }
  
  public static void main(String[] args)
  {
    // set up log4j
    Properties lp = new Properties();
    
    lp.setProperty("log4j.rootCategory", "debut, stdout");
    lp.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
    lp.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
    lp.setProperty("log4j.appender.stdout.layout.ConversionPattern", "%-5p (%F:%L) - %m%n");
    
    PropertyConfigurator.configure(lp);
    
    TriCipherTest test = new TriCipherTest();
    
    test.run(args);
  }
}