package com.mesapps.john;

import java.io.File;
import java.util.Arrays;

public class DirList
{
  private static void printListOfFiles(java.util.ArrayList onlyFiles,java.io.File rootdir) 
  {
    File[] list = rootdir.listFiles();
    for(int i = 0; i < list.length;i++)
    {
      File eachFile = (java.io.File)list[i];
      if(eachFile.isDirectory())
      {
        printListOfFiles(onlyFiles,eachFile);
      } 
      else if(eachFile.isFile()) 
      {
        onlyFiles.add(eachFile);
      }
    }
  }

  public static File[] printListOfFiles(java.io.File rootdir)
  {
    java.util.ArrayList blah = new java.util.ArrayList();
    printListOfFiles(blah,rootdir);
    return (java.io.File[])blah.toArray(new File[blah.size()]);
}

  public static void main(String[] args) 
  {
    java.io.File[] onlyFiles = printListOfFiles(new java.io.File("C:/mconline/mfe"));
    for(int i = 0;i < onlyFiles.length; i++){
    System.out.println(onlyFiles[i].getAbsolutePath());
  }
}}