package com.mesapps.john;

import java.io.*;

public class SerializeTest
{
  public static void main(String[] args)
  {
    try
    {
      // instantiate class based on args
      System.out.println("Instantiating object of type: " + args[0]);
      Object serObj = (Object)(Class.forName(args[0]).newInstance());
    
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(bos);
    
      System.out.println("Serializing object...");
      oos.writeObject(serObj);
      
      System.out.println("Object in serialized form:");
      System.out.println(bos.toString());
    }
    catch(Exception e)
    {
      System.out.println("Failed to Serialize: " + e.toString());
    }
  }
}