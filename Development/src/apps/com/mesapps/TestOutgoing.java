import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TestOutgoing
{
  public static void main(String[] args)
  {
    try
    {
      if(args.length == 0)
      {
        System.out.println("You must supply an address to connect to");
      }
      else
      {
        System.out.println("creating connection");
        URL url = new URL("http", args[0], "/index.html");
        
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                url.openStream()));
        
        String line1;
        
        System.out.println("getting response...");
        while((line1 = in.readLine()) !=null) 
        {
          System.out.println(line1);
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("TestOutgoing: " + e.toString());
    }
  }
}