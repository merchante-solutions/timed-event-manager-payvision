package com.mesapps.vs;

import com.mes.virtualapp.VirtualAppProcessor;
import com.mes.database.SQLJConnectionBase;

public class GetXMLApp
{
  long appSeqNum = 0;
  public GetXMLApp(String _appSeqNum)
  {
    try
    {
      appSeqNum = Long.parseLong(_appSeqNum);
    }
    catch(Exception e)
    {
      System.out.println("GetXMLApp constructor: " + e.toString());
    }
  }
  
  public void shitFire()
  {
    try
    {
      // retrieve xml app from database and print the fucker out
      VirtualAppProcessor vap = new VirtualAppProcessor();
    
      System.out.println("Retrieving: " + VirtualAppProcessor.VAPP_XML_REQUEST + ", " + appSeqNum);
      System.out.println(vap.getXML(VirtualAppProcessor.VAPP_XML_REQUEST, appSeqNum));
    }
    catch(Exception e)
    {
      System.out.println("shitFire(): " + e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.setConnectString(SQLJConnectionBase.PROD_DIRECT_CONNECTION_STRING);
      
      System.out.println(VirtualAppProcessor.getLoggedRequest(Long.parseLong(args[0])));
      
//      GetXMLApp grunt = new GetXMLApp(args[0]);
      
//      grunt.shitFire();
    }
    catch(Exception e)
    {
      System.out.println("main: " + e.toString());
    }
  }
}