package com.mesapps.stmt;

import java.io.*;
import java.util.Properties;
import java.util.Calendar;
import java.util.Vector;
import java.text.SimpleDateFormat;

import com.mes.database.SQLJConnectionBase;

import com.mes.reports.*;

import org.apache.log4j.Logger;

public abstract class GenericStatementParser extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(GenericStatementParser.class);

  protected Properties            appProperties;

  protected File                  inputFile;
  protected File                  logFile;
  protected String                loadFilename;
  protected String                logPath;
  protected boolean               unixWorld;

  protected FileWriter            logOut                  = null;
  protected SimpleDateFormat      fmt                     = null;

  public int                   statementCount          = 0;
  public int                   errorCount              = 0;

  protected abstract void doParse() throws ParserException;
  
  /*
  ** protected void logResults()
  **
  ** logs results of parsing to log file to allow file loading statistics
  */
  protected void logResults()
  {
    // do shugang's formatting
    String filler           = "          ";
    String recordsRead      = Long.toString(statementCount - errorCount);
    String recordsRejected  = Long.toString(errorCount);
    recordsRead             = filler.substring(recordsRead.length())
                              + recordsRead;
    recordsRejected         = filler.substring(recordsRejected.length())
                              + recordsRejected;

    // log end date and time and some stats
    logLine(logOut,"Run ended on " +
      fmt.format(Calendar.getInstance().getTime()));
    logLine(logOut,"\nTotal logical records skipped:          0");
    logLine(logOut,  "Total logical records read:    " + recordsRead);
    logLine(logOut,  "Total logical records rejected:" + recordsRejected);
    logLine(logOut,  "Total logical records discarded:        0");

    // flush and close log file
    try { logOut.flush(); logOut.close(); } catch (Exception e) {}
    
  }

  /*
  ** protected void run(String[] args) throws ParserException
  **
  ** Top level parsing routine.  Parses a statement input file and generates
  ** a sql loader compatible statement output file.
  */
  protected void run(String[] args) throws ParserException
  {

    // load parser app properties
    loadProperties();

    // get input/ouput files
    processArgs(args);
    
    // open log file
    logOut = openLogFile(logFile);

    // log the start date and time
    fmt = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
    logLine(logOut,"Run began on " +
      fmt.format(Calendar.getInstance().getTime()));

    if (unixWorld)
    {
      System.out.println("Statement file upload beginning for file "  +
        inputFile.getAbsolutePath());
    }
    else
    {
      // cosmetic
      System.out.println("");
    }
    
    try
    {
      doParse();
    }
    catch(Exception e)
    {
      System.out.println("Error during doParse(): " + e.toString());
    }

    System.out.println("File upload complete.");
  }

  /*
  ** private void logLine(FileWriter logOut, String line)
  **
  ** Writes a String line to the log file pointed to by logOut.  Appends
  ** a carriage return to the line.
  */
  protected void logLine(FileWriter logOut, String line)
  {
    if(line != null && logOut != null)
    {
      try
      {
        logOut.write(line + "\n");
      }
      catch (Exception e)
      {
        System.out.println("Error writing to log file: " + e.toString());
        System.out.println("  Log line not written: " + line);
      }
    }
  }

  /*
  ** class ParserException
  **
  ** Internal application exception type.
  */
  public class ParserException extends Exception
  {
    public ParserException(String details)
    {
      super(details);
    }

    public String toString()
    {
      return getMessage();
    }
  };

  /*
  ** protected void loadProperties() throws ParserException
  **
  ** Loads app properties.
  */
  protected void loadProperties() throws ParserException
  {
    try
    {
      // load the app properties
      appProperties = new Properties();
      FileInputStream in = new FileInputStream("parser.properties");
      appProperties.load(in);
      in.close();

      unixWorld       = appProperties.getProperty("unix.world").equals("true");
      logPath         = appProperties.getProperty("log.path");
    }
    catch (Exception e)
    {
      throw new ParserException(e.toString());
    }
  }

  /*
  ** protected void processArgs(String[] args)
  **
  ** Checks for missing command line argument, creates input and log files.
  */
  protected void processArgs(String[] args) throws ParserException
  {
    if (args.length < 1)
    {
      throw new ParserException("No statement input file specified.");
    }
    else if (args.length > 2)
    {
      StringBuffer extraArgs = new StringBuffer();
      for (int i = 1, len = args.length; i < len; ++i)
      {
        extraArgs.append("(" + args[i] + ")");
      }
      throw new ParserException("Extra arguments in command line: " + extraArgs);
    }
    else
    {
      // arg 0 contains the input filename
      inputFile = new File(args[0]);
      loadFilename = inputFile.getName();
      //ADD IN FOR PROD
      // construct the log file from the input filename
      StringBuffer logFilename = new StringBuffer(logPath);
      char endCh = logPath.charAt(logPath.length() - 1);
      if (endCh != File.separatorChar)
      {
        logFilename.append(File.separatorChar);
      }
      int dotIdx = loadFilename.lastIndexOf('.');
      if (dotIdx > -1)
      {
        logFilename.append(loadFilename.substring(0,dotIdx));
      }
      else
      {
        logFilename.append(loadFilename);
      }
      logFilename.append(".log");
      logFile = new File(logFilename.toString());
    }
  }

  /*
  ** private FileWriter openLogFile(File logFile)
  **
  ** Creates a FileWriter pointed to logFile.
  **
  ** RETURNS: FileWriter reference.
  */
  protected FileWriter openLogFile(File logFile)
    throws ParserException
  {
    FileWriter fw = null;

    try
    {
      fw = new FileWriter(logFile);
    }
    catch (Exception e)
    {
      throw new ParserException("Unable to open log file: " + logFile.getAbsolutePath());
    }

    return fw;
  }

}

