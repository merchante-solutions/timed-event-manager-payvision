/*************************************************************************

  FILE: $Archive: /Java/apps/statements/StatementFileParser.java $

  Description:

    XMLStatementFileParser

    Parses XML statement file from TSYS.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-01-29 10:29:53 -0800 (Mon, 29 Jan 2007) $
  Version            : $Revision: 13353 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mesapps.stmt;

import com.mes.database.SQLJConnectionBase;
import com.mes.reports.XMLStatementFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Blob;

import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

//DOM4J
import org.dom4j.*;
import org.dom4j.io.*;

import org.apache.log4j.Logger;

public class XMLStatementFileParser extends GenericStatementParser
{
  public static final String    STATEMENT_START_TAG   = "CUSTOMER";
  public static final String    MERCHANT_PATH         = "MERCHANT_NUM";

  static Logger log = Logger.getLogger(XMLStatementFileParser.class);

  protected XMLStatementFile  xFile;
  private   long              counter = 0L;

  public XMLStatementFileParser()
  {
  }

  /* doParse
  ** abstract method from super
  ** meat and potatoes - reads the file
  ** processing as it goes via the _ElementHandler
  */
  protected void doParse() throws ParserException
  {
    xFile = new XMLStatementFile();
    
    xFile.openConnection();
    
    log.debug("starting XMLStatementFileParser doParse()...");
    try
    {
      SAXReader reader = new SAXReader();
      reader.setStripWhitespaceText(true);
      reader.addHandler(appProperties.getProperty("root_xpath"), new _ElementHandler(this));
      Document document = reader.read(inputFile);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ParserException(e.toString());
    }
    
    xFile.closeConnection();
    
    logResults();
    log.debug("ending XMLStatementFileParser doParse()");
  }


  //convenience method
  public void setElement(Element e)
    throws Exception
  {
    xFile.setElement(e,appProperties);
  }

  /* submitData
  ** advances the counter (for REF_NUM)
  ** sets the loadfilename
  ** and triggers the DB insertion
  */
  public boolean submitData()
    throws Exception
  {
    boolean result = false;
    
    //counter to be used for rec num in DB... will work
    //only if xml stays constant, which it should
    ++counter;
    
    xFile.setRecNum(counter);
    xFile.setLoadFilename(loadFilename);

    //TODO - where do we go if this
    //fails due to invalid XML info?
    if(appProperties.getProperty("production.insert").equals("true"))
    {
      result = xFile.submitData();
    }
    else
    {
      result = true;
    }
    
    if(! result)
    {
      ++errorCount;
    }
    
    return result;
  }

  private void logError(Exception e, Element element, String progress)
  {
    logEntry(progress + element.valueOf(MERCHANT_PATH).trim(), e.toString());
    //logLine(logOut, e.toString() + "\n" + element.asXML());
  }

  /* class _ElementHandler
  ** paramount to the process, this handler
  ** triggers the collection of the individual
  ** XML segments as defined in the properties file (CUSTOMER)
  */
  class _ElementHandler implements ElementHandler
  {

    private XMLStatementFileParser parser;

    //need this reference to allow for the processing
    //of the individual segments during onEnd
    public _ElementHandler (XMLStatementFileParser parser)
    {
      log.debug("Instantiating _ElementHandler()");
      this.parser = parser;
    }

    public void onStart(ElementPath path)
    {
      // do nothing here...
    }

    public void onEnd(ElementPath path)
    {
      long in = System.currentTimeMillis();

      // process a CUSTOMER element
      Element customer = path.getCurrent();
      String progress = "";
      try
      {
        ++parser.statementCount;
        
        progress = "setting element to customer: ";
        parser.setElement(customer);
        progress = "submitting data: ";
        parser.submitData();

        // prune the tree
        progress = "pruning tree: ";
        customer.detach();
      }
      catch (Exception e)
      {
        ++parser.errorCount;
        parser.logError(e, customer, progress);
      }

      long out = System.currentTimeMillis() - in;
      log.debug("Time elapsed = " + out/1000);
    }
  }

  public static void main(String[] args)
  {
    try
    {
      String errorLevel = "DEBUG";
      
      try
      {
        errorLevel = args[1];
      }
      catch(Exception e)
      {
      }
      
      SQLJConnectionBase.initStandalone(errorLevel);

      System.out.println("starting parser");

      XMLStatementFileParser parser = new XMLStatementFileParser();
      long in = System.currentTimeMillis();
      parser.run(args);
      long out = System.currentTimeMillis() - in;
      
      System.out.println("end parser (time elapsed = " + out/1000 + ")");
    }
    catch(Exception e)
    {
      System.out.println("\n*** ERROR ***\n");
      System.out.println(e + "\n");
      System.out.println("Usage: parse <input file>");
    }
  }

}