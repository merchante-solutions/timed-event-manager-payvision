package com.mesapps.stmt;

import java.io.*;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;

import com.mes.reports.*;
import com.mes.database.*;

public class PdfGenerator
{
  public static void main(String[] args)
  {
//    if (args.length == 4)
    {
      try
      {
        StatementRecord rec = new StatementRecord();
        rec.setConnectString("direct;jdbc:oracle:thin:@cablecar:1521:FTPDB;oracle.jdbc.driver.OracleDriver;user=mes;password=mes");
        rec.getData("0601000052",1,200209);
        System.out.println(rec.getFees());
        //args[0],args[1],Integer.parseInt(args[2]));
        //"0601000052","1",200209);
        Document doc = new Document(PageSize.A4,18,18,18,18);
        FileOutputStream fos = new FileOutputStream("out.pdf");
        PdfWriter.getInstance(doc,fos);
        doc.open();
        Image logo = Image.getInstance("meslogo66.png");
        Statement s = rec.getStatement();
        for (int i = 0, pgCnt = s.getPageCount(); i < pgCnt; ++i)
        {
          doc.add(logo);
          StatementPage p = s.getPage(i);
          for (int j = 0, lnCnt = p.getLineCount(); j < lnCnt; ++j)
          {
            Paragraph para =
              new Paragraph(11,p.getLine(j).toString(),new Font(Font.COURIER,8));
            para.setAlignment(Element.ALIGN_CENTER);
            doc.add(para);
          }
    
          if (i < pgCnt - 1)
          {
            doc.newPage();
          }
        }
        doc.close();
        
        System.out.println("\nOUT.PDF generated.");
      }
      catch (Exception e)
      {
        System.out.println("PDF file generation exception: " + e.toString());
      }
    }
    /*
    else
    {
      System.out.println("\nUsage: pdf <merch num> <dda> <transit rtg> <year month>");
    }
    */
  }
}
