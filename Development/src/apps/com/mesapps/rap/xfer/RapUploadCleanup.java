/*************************************************************************

  FILE: $Archive: /Java/apps/Disc Transfer/RapUploadCleanup.java $

  Description:
  
  RapUploadCleanup

  Archives a RAP upload file and upload result file.  Evaluates the
  result file and sends email notices of success or failure to system
  admin.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/01/02 2:26p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueueItem;
import com.mes.net.EmailQueue;

public class RapUploadCleanup extends SQLJConnectionBase
{
  
  private String uploadPath   = "c:\\xfer\\upload\\";
  private String archivePath  = "c:\\xfer\\upload\\arc\\";
  private String logFile      = "c:\\xfer\\upload\\rap_upload.log";
  
  /*
  ** public static void main(String[] args)
  **
  ** Main entry point for the RapUploadGenerator application.  Creates an 
  ** instance of this class and calls the run method.
  */
  public static void main(String[] args)
  {
    try
    {
      RapUploadCleanup rug = new RapUploadCleanup();
      rug.run();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
      com.mes.database.OracleConnectionPool.getInstance().cleanUp();
    }
  }

  private File    uploadFile;
  private File    resultFile;
  private File    arcUploadFile;
  private File    arcResultFile;
  private String  dateTime;
  
  /*
  ** public void run()
  **
  ** Generates the RAP application upload file to be uploaded to Discover
  ** (via IBM Global Services).
  */
  public void run()
  {
    try
    {
      loadProperties();
      logEvent("START");
      initialize();
      evaluateResults();
      doCleanup();
    }
    catch (Exception e)
    {
      logEvent("ERROR: " + e);
    }
    finally
    {
      logEvent("END");
    }
  }
  
  /*
  ** private void loadProperties() throws Exception
  **
  ** Loads app properties.
  */
	private void loadProperties() throws Exception
  {
		// load the app properties
    Properties appProperties = new Properties();
		FileInputStream in = new FileInputStream("rapuploadcleanup.properties");
		appProperties.load(in);
		in.close();

    uploadPath  = checkPath(appProperties.getProperty("upload.path"));
    archivePath = checkPath(appProperties.getProperty("archive.path"));
    logFile     = checkPath(appProperties.getProperty("log.file"));
	}
  
  /*
  ** private String checkPath(String path)
  **
  ** Makes sure a path string ends correctly (with a '/').
  **
  ** RETURNS: the path, corrected if necessary.
  */
  private String checkPath(String path)
  {
    if (path.lastIndexOf("/") != (path.length() - 1))
    {
      path = path + "/";
    }
    return path;
  }
  
  /*
  ** private void logEvent(String logData)
  **
  ** Appends a timestamped line to the log file in the upload directory.
  */
  private void logEvent(String logData)
  {
    try
    {
      DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
      String logTime = df.format(Calendar.getInstance().getTime());
    
      FileWriter fw = null;
      try
      {
        fw = new FileWriter(logFile,true);
      }
      catch (Exception e)
      {
        fw = new FileWriter(logFile);
      }
      
      String logLine = logTime + ": (RUC) " + logData;
      fw.write(logLine + "\n");
      fw.close();
    
      System.out.println(logLine);
    }
    catch (Exception e)
    {
      System.out.println("logEvent exception: " + e);
    }
  }
  
  /*
  ** private void initialize()
  **
  ** Initializes some internal members such as upload file objects.
  */
  private void initialize()
  {
    // date time stamp to use for archive file naming
    DateFormat df   = new SimpleDateFormat("MMddyy_HHmm");
    dateTime = df.format(Calendar.getInstance().getTime());
    
    // create some file objects
    uploadFile    = new File(uploadPath + "RAP_UPLOAD.TXT");
    resultFile    = new File(uploadPath + "RAP_UPLOAD.RES");
    arcUploadFile = new File(archivePath + "RAP_" + dateTime + ".TXT");
    arcResultFile = new File(archivePath + "RAP_" + dateTime + ".RES");
  }
  
  /*
  ** private void moveFile(File oldFile, File newFile)
  **
  ** Moves a file.
  */
  private void moveFile(File oldFile, File newFile)
  {
    if (oldFile.renameTo(newFile))
    {
      logEvent("Moved " + oldFile.getPath() + " to " + newFile.getPath());
    }
    else
    {
      logEvent("ERROR: Failed to move " + oldFile.getPath() + " to " 
        + newFile.getPath());
    }
  }
  
  /*
  ** private void evaluateResults()
  **
  ** Examines the result file (if it exists) to determine if the upload
  ** was successful.
  */
  private void evaluateResults()
  {
    try
    {
      if (resultFile.exists())
      {
        // check for success result
        String result = getResultText(resultFile);
        if (result.equals("SUCCESS"))
        {
          logEvent("SUCCESS: " + uploadFile.getPath() + " uploaded");
          Notifier.send("SUCCESS");
        }
        // check for no apps to upload result
        else if (result.equals("NO APPS TO UPLOAD"))
        {
          logEvent("NO APPLICATIONS TO UPLOAD");
          Notifier.send("NO APPS");
        }
        // check for other (error) results
        else
        {
          logEvent("ERROR: " + result);
          Notifier.send("ERROR",result);
        }
      }
      else
      {
        logEvent("ERROR: " + resultFile.getPath() + " does not exist");
        Notifier.send("ERROR",resultFile.getPath() + " does not exist");
      }
    }
    catch (Exception e)
    {
      logEvent("ERROR: failed to send notification - " + e);
    }
  }
  
  /*
  ** public class Notifier
  **
  ** Helper class that creates and sends a notification email describing
  ** the results of the last upload attempt.
  */
  public static class Notifier
  {
    private String result;
    private String description = "";
    private Notifier(String result)
    {
      this.result = result;
    }
    private Notifier(String result,String description)
    {
      this.result = result;
      this.description = description;
    }
    
    private void send() throws Exception
    {
      DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
      String dateTime = df.format(Calendar.getInstance().getTime());
    
      EmailQueueItem qi
        = new EmailQueueItem("RapUploadCleanup.Notifier","send"/*,18*/);
    
      // manually set addresses until email queue item is fixed
      qi.setFrom("rapupload@merchante-solutions.com");
      qi.addTo("tbaker@merchante-solutions.com");
    
      qi.setSubject("RAP UL " + result + " (" + dateTime + ")");
      
      StringBuffer text = new StringBuffer();
      text.append("RAP Upload " + result);
      if (description != null && description.length() > 0)
      {
        text.append(": " + description);
      }
      qi.setText(text.toString());
      
      EmailQueue.push(qi);
    }
    
    public static void send(String result) throws Exception
    {
      Notifier n = new Notifier(result);
      n.send();
    }
    public static void send(String result,String description)
      throws Exception
    {
      Notifier n = new Notifier(result,description);
      n.send();
    }
  }

  /*
  ** private String getResultText(File resultFile)
  **
  ** Reads the first text line from resultFile and returns it.
  **
  ** RETURNS: first line of the file as a String, null if error.
  */
  private String getResultText(File resultFile)
  {
    String resultText = null;
    try
    {
      FileReader fr = new FileReader(resultFile);
      LineNumberReader lnr = new LineNumberReader(fr);
      resultText = lnr.readLine();
      lnr.close();
    }
    catch (Exception e)
    {
      logEvent("ERROR: failed to read line from result file "
        + resultFile.getPath());
    }
    return resultText;
  }
  
  /*
  ** private void doCleanup()
  **
  ** Archives upload files.
  */
  private void doCleanup()
  {
    if (uploadFile.exists())
    {
      moveFile(uploadFile,arcUploadFile);
    }
    if (resultFile.exists())
    {
      moveFile(resultFile,arcResultFile);
    }
  }
}
