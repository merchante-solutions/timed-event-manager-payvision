/*************************************************************************

  FILE: $Archive: /Java/apps/rap/xfer/RapUploadGenerator.sqlj $

  Description:
  
  RapUploadGenerator

  Generates an upload file to be sent to IBM Global Services containing
  RAP applications, updates database to indicate applications have been
  sent to Discover.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 5/22/03 12:18p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

import sqlj.runtime.ResultSetIterator;

import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueueItem;
import com.mes.net.EmailQueue;
import com.mes.support.SyncLog;

public class RapUploadGenerator extends SQLJConnectionBase
{
  private String uploadPath   = "c:\\xfer\\upload\\";
  private String archivePath  = "c:\\xfer\\upload\\arc\\";
  private String logFile      = "c:\\xfer\\upload\\rap_upload.log";
  
  /*
  ** public RapUploadGenerator()
  **
  ** Constructor.
  **
  ** Uses direct connection string for database connections, disables
  ** autocommit.
  */
  public RapUploadGenerator()
  {
    super(PROD_DIRECT_CONNECTION_STRING,false);
  }
  
  /*
  ** public static void main(String[] args)
  **
  ** Main entry point for the RapUploadGenerator application.  Creates an 
  ** instance of this class and calls the run method.
  */
  public static void main(String[] args)
  {
    try
    {
      RapUploadGenerator rug = new RapUploadGenerator();
      rug.run();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
      com.mes.database.OracleConnectionPool.getInstance().cleanUp();
    }
  }

  private File    rapUploadFile;
  private String  dateTime;
  
  /*
  ** public void run()
  **
  ** Generates the RAP application upload file to be uploaded to Discover
  ** (via IBM Global Services).
  */
  public void run()
  {
    try
    {
      loadProperties();
      logEvent("START");
      initialize();
      if (generateFile(rapUploadFile))
      {
        logEvent("SUCCESS: " + rapUploadFile.getPath() + " generated");
      }
      else
      {
        doErrorNotification();
        doErrorCleanUp();
        logEvent("ERROR: " + rapUploadFile.getPath() + " not generated");
      }
    }
    catch (Exception e)
    {
      logEvent("ERROR: " + e);
    }
    finally
    {
      logEvent("END");
    }
  }
  
  /*
  ** private void loadProperties() throws Exception
  **
  ** Loads app properties.
  */
	private void loadProperties() throws Exception
  {
		// load the app properties
    Properties appProperties = new Properties();
		FileInputStream in = new FileInputStream("rapuploadgenerator.properties");
		appProperties.load(in);
		in.close();

    uploadPath  = checkPath(appProperties.getProperty("upload.path"));
    archivePath = checkPath(appProperties.getProperty("archive.path"));
    logFile     = checkPath(appProperties.getProperty("log.file"));
	}
  
  /*
  ** private String checkPath(String path)
  **
  ** Makes sure a path string ends correctly (with a '/').
  **
  ** RETURNS: the path, corrected if necessary.
  */
  private String checkPath(String path)
  {
    if (path.lastIndexOf("/") != (path.length() - 1))
    {
      path = path + "/";
    }
    return path;
  }
  
  /*
  ** private void logEvent(String logData)
  **
  ** Appends a timestamped line to the log file in the upload directory.
  */
  private void logEvent(String logData)
  {
    try
    {
      DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
      String logTime = df.format(Calendar.getInstance().getTime());
    
      FileWriter fw = null;
      try
      {
        fw = new FileWriter(logFile,true);
      }
      catch (Exception e)
      {
        fw = new FileWriter(logFile);
      }
      
      String logLine = logTime + ": (RUG) " + logData;
      fw.write(logLine + "\n");
      fw.close();
    
      System.out.println(logLine);
    }
    catch (Exception e)
    {
      System.out.println("logEvent exception: " + e);
    }
  }
  
  /*
  ** private void initialize()
  **
  ** Initializes some internal members such as upload file objects.
  */
  private void initialize()
  {
    // date time stamp to use for archive file naming
    DateFormat df   = new SimpleDateFormat("MMddyy_HHmm");
    dateTime = df.format(Calendar.getInstance().getTime());
    
    // create some file objects
    rapUploadFile  = new File(uploadPath + "RAP_UPLOAD.TXT");

    // clear up old files from previous upload attempts
    doInitialCleanUp();
  }
  
  /*
  ** private void doInitialCleanUp
  **
  ** Checks for the existence of files that will be in the way of the
  ** generation of new upload files, archives such found files.
  */
  private void doInitialCleanUp()
  {
    // archive old upload file if present
    if (rapUploadFile.exists())
    {
      File archiveFile = new File(archivePath + "OLD_" + dateTime + ".TXT");
      moveFile(rapUploadFile,archiveFile);
    }
  }
  
  /*
  ** private void moveFile(File oldFile, File newFile)
  **
  ** Moves a file.
  */
  private void moveFile(File oldFile, File newFile)
  {
    if (oldFile.renameTo(newFile))
    {
      logEvent("Moved " + oldFile.getPath() + " to " + newFile.getPath());
    }
    else
    {
      logEvent("ERROR: Failed to move " + oldFile.getPath() + " to " 
        + newFile.getPath());
    }
  }

  /*
  ** private void doErrorNotification()
  **
  ** Sends an email to system administrator indicating an error has occured
  ** in the file upload generation.
  */
  private void doErrorNotification()
  {
    try
    {
      // create an email queue item
      EmailQueueItem qi = new EmailQueueItem("RapUploadGenerator",
        "doErrorNotification",true);
    
      // manually set addresses until email queue item is fixed
      qi.setFrom("rapupload@merchante-solutions.com");
      qi.addTo("tbaker@merchante-solutions.com");
      
      // subject
      StringBuffer subject = new StringBuffer();
      subject.append("RAP UL ERROR");
      DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
      String errorTime = df.format(Calendar.getInstance().getTime());
      subject.append(" (" + errorTime + ")");
      qi.setSubject(subject.toString());
    
      // text
      qi.setText("Error in RAP upload file generator");
    
      // send the email
      EmailQueue.push(qi);
    }
    catch (Exception e)
    {
      logEvent("ERROR: unable to perform error notification - " + e);
    }
  }
  
  /*
  ** private void doErrorCleanUp()
  **
  ** Archives upload file, if it exists.
  */
  private void doErrorCleanUp()
  {
    // archive upload file if present
    if (rapUploadFile.exists())
    {
      File archiveFile = new File(archivePath + "ERR_" + dateTime + ".TXT");
      moveFile(rapUploadFile,archiveFile);
    }
  }
  
  /*
  ** Members and methods taken from com.mes.rap.application.RapFileUpload
  */

  private static final int      ZERO                = 0;
  private static final int      BLANK               = 1;
  private static final int      RIGHT_JUST          = 0;
  private static final int      LEFT_JUST           = 1;

  private static final int      NO_FORMATTING       = 1;
  private static final int      NUM_LET_SPC         = 2;
  private static final int      LET_SPC             = 3;
  private static final int      NUMS_ONLY           = 4;
  private static final int      MMDDYY              = 5;
  private static final int      YYMMDD              = 6;
  private static final int      MMYY                = 7;
  private static final int      IMPLIED2            = 8;
  private static final int      IMPLIED3            = 9;
  private static final int      NUMS_PAD_SPACE      = 10;
  private static final int      TAX_ID              = 11;

  private static final String   PARTNER_MES         = "9966";
  private static final String   PARTNER_DISCOVER    = "9968";
  private static final String   PARTNER_RAP         = "9990";


  private ResultSetIterator     batch_it            = null;
  private int                   idx                 = 0;
  private String[]              fileDataLine        = null;

  private int                   batchSize           = 0;
  private int                   currentRecNum       = 0;
  private String                mesIdentifierCode   = "0000";

  private String                currentPartnerNum   = "";
  
  private File                  uploadFile          = null;
  private FileWriter            fwriter             = null;
  private String                fileName            = null;

  private String                dateString          = null;
  private String                timeString          = null;
  private String                headerDateStr       = null;
  private String                headerTimeStr       = null;
  private String                cntrlNumStr         = null;

  private int                   formatCode          = NO_FORMATTING;  //temp solution
  private String                dbName              = null;

  private boolean               ftpSuccessful       = false;

  private HashMap               fileNames           = new HashMap();

  private void reset()
  {
    batch_it            = null;
    idx                 = 0;
    fileDataLine        = null;
    batchSize           = 0;
    mesIdentifierCode   = "0000";
    currentPartnerNum   = "";
    uploadFile          = null;
    fwriter             = null;
    fileName            = null;
    dateString          = null;
    timeString          = null;
    headerDateStr       = null;
    headerTimeStr       = null;
    formatCode          = NO_FORMATTING;  //temp solution
    dbName              = null;
    ftpSuccessful       = false;
  }

  /*
  ** METHOD private ResultSet getBatchRecords() throws Exception
  **
  ** Timestamps all unprocessed accounts and returns a result set 
  ** containing all available NSI batch elements.
  **
  ** RETURNS: ResultSet containing the batch data.
  */

  private ResultSet getBatchRecords() throws Exception
  {
    // get a timestamp value
    java.sql.Timestamp today = null;
    #sql [Ctx]
    {
      select    sysdate into :today
      from      dual
    };
    
    // timestamp all unprocessed accounts
    #sql [Ctx]
    {
      update    RAP_APP_COMPLETED acc
      set       acc.date_processed = :today
      where     acc.date_processed is null 
    };
  
    // gather todays freshly stamped records
    #sql [Ctx] batch_it =
    {
      select    vtf.*
              
      from      RAP_APP_INFO vtf,
                RAP_APP_COMPLETED acc
                              
      where     vtf.app_seq_num = acc.app_seq_num and 
                acc.date_processed = :today
                                
      order by  acc.date_completed asc
    };
    
    return batch_it.getResultSet();
  }
  
  private void submitControlNum(long appSeqNum, String controlNum)
  {
    try
    {
      #sql [Ctx]
      {
        update    RAP_APP_INFO 
        set       control_number = :controlNum
        where     app_seq_num = :appSeqNum
      };
    }
    catch(Exception e)
    {
    }

  }

  private void addField(ResultSet rs, String dbName, int fieldLength, String defaultData, int formatCode) throws Exception
  {
    String fieldData = null;
    if (dbName != null && rs != null)
    {
      fieldData = rs.getString(dbName);
      fieldData = isBlank(fieldData) ? defaultData : fieldData.trim();
    }
    else if (defaultData != null)
    {
      fieldData = defaultData.trim();
    }
    fieldData = specialFormatting(fieldData, fieldLength, formatCode);
    fileDataLine[idx] = fieldData.toUpperCase();
    idx++;
    //System.out.println("index = " + idx);
  }

  private String specialFormatting(String data, int fieldLength, int formatCode) throws Exception
  {
    StringBuffer      dataString  = null;
    SimpleDateFormat  df          = null;
    DateFormat        fmt         = null;
    DecimalFormat     nf          = null;
    String            appendage   = " ";
    int               strLength   = 0;
    int               padLength   = 0;
    double            tempDouble  = 0.0;

    if (data == null)
    {
      data = "";
    }

    switch(formatCode)
    {
      case NO_FORMATTING:
        dataString = new StringBuffer(data);   
      break;
      
      case TAX_ID:  // all blanks if empty, left zero padded otherwise, numbers only
        dataString = new StringBuffer();
        try
        {
          if (data.length() > 0)
          {
            // filter out non-numeric
            for(int i=0; i<data.length(); ++i)
            {
              if(Character.isDigit(data.charAt(i)))
              {
                dataString.append(data.charAt(i));
              }
            }
            while (dataString.length() < fieldLength)
            {
              dataString.insert(0,'0');
            }
          }
        }
        catch (Exception e) {}
      break;
      
      case NUM_LET_SPC:    //only numbers letters and whitespaces.. removes punctuation
        dataString = new StringBuffer();   
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isLetterOrDigit(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
      break;
      
      case LET_SPC: //only letters and whitespaces
        dataString = new StringBuffer();   
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isLetter(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
      break;

      case NUMS_ONLY: //only numbers
        
        dataString = new StringBuffer();   

        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isDigit(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
        appendage = "0";
      break;
      
      case NUMS_PAD_SPACE:
        
        dataString = new StringBuffer();   
        
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isDigit(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
        appendage = " ";
      break;

      case MMDDYY:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }

        df            = new SimpleDateFormat("MMddyy");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;

      case YYMMDD:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }
        
        df            = new SimpleDateFormat("yyMMdd");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;
      
      case MMYY:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }
        df            = new SimpleDateFormat("MMyy");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;

      case IMPLIED2: //implied 2 spaces
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          appendage = "0";
          break;
        }
        
        try
        {
          tempDouble = Double.parseDouble(data);
        }
        catch(Exception e)
        {
          dataString = new StringBuffer();   
          break;
        }

        if(fieldLength == 5)
        {
          nf = new DecimalFormat("000.00");
        }
        else if(fieldLength == 6)
        {
          nf = new DecimalFormat("0000.00");
        }
        else if(fieldLength == 7)
        {
          nf = new DecimalFormat("00000.00");
        }
        
        dataString = new StringBuffer(nf.format(tempDouble));
        if(fieldLength == 5)
        {
          dataString.deleteCharAt(3);
        }
        else if(fieldLength == 6)
        {
          dataString.deleteCharAt(4);
        }
        else if(fieldLength == 7)
        {
          dataString.deleteCharAt(5);
        }
        appendage = "0";
      break;
      
      case IMPLIED3: //implied 3 spaces
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          appendage = "0";
          break;
        }

        try
        {
          tempDouble = Double.parseDouble(data);
        }
        catch(Exception e)
        {
          dataString = new StringBuffer();   
          break;
        }

        if(fieldLength == 5)
        {
          nf = new DecimalFormat("00.000");
        }
        else if(fieldLength == 6)
        {
          nf = new DecimalFormat("000.000");
        }
        dataString = new StringBuffer(nf.format(tempDouble));
        if(fieldLength == 5)
        {
          dataString.deleteCharAt(2);
        }
        else if(fieldLength == 6)
        {
          dataString.deleteCharAt(3);
        }
        appendage = "0";
      break;
      
      default:
      break;
    }

    strLength = dataString.length();
    padLength = fieldLength - strLength;

    if(padLength > 0 && strLength == 0)
    {
      while(padLength != 0)
      {
        dataString.append(appendage);
        padLength--;
      }
    }
    else if(padLength > 0)
    {
      while(padLength != 0)
      {
        if(appendage.equals("0"))
        {
          dataString.insert(0,appendage);
        }
        else if(appendage.equals(" "))
        {
          dataString.append(appendage);
        }
        padLength--;
      }
    }
    else if(padLength < 0)
    {
      dataString.delete(fieldLength, strLength);
    }

    return dataString.toString();
  }

 
  /*
  ** METHOD private String getRecordData(ResultSet rs) throws Exception
  **
  ** Builds an NSI batch record from the current row in the given ResultSet.
  **
  ** RETURNS: the batch record in a String.
  */
  private boolean getRecordData(ResultSet rs) throws Exception
  {
    
    fileDataLine            = new String[43];

    boolean errorOccurred   = false;
    currentRecNum   = 0;

    //starts at zero gets incremented as we go used in control number custruction
    batchSize++;
    currentRecNum           = batchSize;
   
    
    idx = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "01", NUM_LET_SPC);
    
    // 2 version no.
    addField(null, null, 2, "01", NUM_LET_SPC);
    
    // 3 control no.
    
    //gets control number using time.. 200 per upload limitation..
    //String tempControlNum = ("R" + currentPartnerNum + specialFormatting(dateString, 6, YYMMDD) + specialFormatting(Integer.toString(getIntTime(cntrlNumStr) + currentRecNum), 4, NUMS_ONLY));
    
    //gets control number using sequence.. 10000 per day upload limitation..
    String tempControlNum = ("R" + currentPartnerNum + specialFormatting(dateString, 6, YYMMDD) + specialFormatting(Integer.toString(getNextSeqVal()), 4, NUMS_ONLY));

    addField(null, null, 15, tempControlNum, NUM_LET_SPC);
    
    submitControlNum(rs.getLong("app_seq_num"), tempControlNum);

    // 4 hq control no.
    addField(null, null, 15, "", NUM_LET_SPC);

    // 5 dba name
    addField(rs, "dba_name", 30, "", NUM_LET_SPC); 

    // 6 dba address
    addField(rs, "dba_address", 30, "", NUM_LET_SPC); 

    // 7 dba city
    addField(rs, "dba_city", 13, "", NUM_LET_SPC); 

    // 8 dba state
    addField(rs, "dba_state", 2, "", NUM_LET_SPC); 

    // 9 dba zip
    addField(rs, "dba_zip", 5, "", NUMS_ONLY); 

    // 10 dba phone
    addField(rs, "dba_phone", 10, "", NUMS_ONLY); 

    // 11 business principal 
    addField(rs, "principal_name", 45, "", NUM_LET_SPC); 

    // 12 principal title
    addField(rs, "principal_title", 15, "", NUM_LET_SPC); 

    // 13 principal address
    addField(rs, "principal_address", 30, "", NUM_LET_SPC); 

    // 14 principal city
    addField(rs, "principal_city", 13, "", NUM_LET_SPC); 

    // 15 principal state
    addField(rs, "principal_state", 2, "", NUM_LET_SPC); 

    // 16 principal zip
    addField(rs, "principal_zip", 5, "", NUM_LET_SPC); 

    // 17 federal tax id
    addField(rs, "federal_tax_id", 9, "", TAX_ID); 

    // 18 corporate name 
    addField(rs, "corporate_name", 30, "", NUM_LET_SPC); 

    // 19 principal address
    addField(rs, "corporate_address", 30, "", NUM_LET_SPC); 

    // 20 principal address line 2
    addField(rs, "corporate_address2", 30, "", NUM_LET_SPC); 
    
    // 21 principal city
    addField(rs, "corporate_city", 13, "", NUM_LET_SPC); 

    // 22 principal state
    addField(rs, "corporate_state", 2, "", NUM_LET_SPC); 

    // 23 principal zip
    
    if(isBlank(rs.getString("corporate_zip")))
    {
      addField(null, null, 5, "", NUM_LET_SPC); 
    }
    else
    {
      addField(rs, "corporate_zip", 5, "", NUMS_ONLY); 
    }

    // 24 merchant category code
    addField(rs, "merchant_cat_code", 4, "", NUMS_ONLY); 


     // 25 moto merchant flag
    addField(rs, "moto_flag", 1, "", NUM_LET_SPC); 
    String motoFlag = isBlank(rs.getString("moto_flag")) ? "" : rs.getString("moto_flag");
    if(!isBlank(motoFlag))
    {
      motoFlag = motoFlag.toUpperCase();
    }

     // 26 non profit flag
    addField(rs, "non_profit_flag", 1, "", NUM_LET_SPC); 

    // 27 average sale
    addField(rs, "average_sale", 3, "", NUMS_ONLY); 
 
    // 28 annual sales
    addField(rs, "annual_sales", 8, "", NUMS_ONLY); 

    // 29 business type
    addField(rs, "business_type", 1, "", NUM_LET_SPC); 

    // 30 principal ssn#
    addField(rs, "principal_ssn", 9, "", TAX_ID); 

    // 31 transit routing num

    addField(rs, "transit_routing_num", 9, "", NUMS_ONLY); 

    // 32 bank account dda
    addField(rs, "account_dda", 17, "", NUMS_PAD_SPACE); 

    // 33 discount rate
    addField(rs, "discover_disc_rate", 5, "", IMPLIED3); 

    // 34 membership fee
    addField(rs, "membership_fee", 3, "", NUMS_ONLY); 

    // 35 Years in business
    addField(rs, "years_in_business", 2, "", NUMS_ONLY); 

    // 36 franchise code
    addField(rs, "franchise_code", 4, "", NUM_LET_SPC); 

    // 37 easi partner number
    addField(null, null, 4, currentPartnerNum, NUM_LET_SPC); 

    //38 Rap sales rep
    addField(rs, "rap_sales_rep_name", 15, "", NUM_LET_SPC); 

    //39 Rap member number
    String tempMemberNum = isBlank(rs.getString("rap_member_num")) ? "" : ("60110" + rs.getString("rap_member_num"));
    addField(null, null, 15, tempMemberNum, NUM_LET_SPC); 

    //40 processor name
    addField(rs, "acquirer_name", 15, "", NUM_LET_SPC); 

    //41 acquirer merchant number
    //addField(rs, "acquirer_merchant_num", 20, "", NUM_LET_SPC); 
    addField(null, null, 20, tempControlNum, NUM_LET_SPC);

    //42 website address
    if(motoFlag.equals("Y"))
    {
      addField(rs, "dba_url", 45, "", NO_FORMATTING); 
    }
    else
    {
      addField(null, null, 45, "", NUM_LET_SPC);
    }

    //43 email address
    addField(rs, "dba_email", 30, "", NO_FORMATTING); 

    return errorOccurred;
  }


  private int getIntTime(String str)
  {
    int tempInt = getRandomNum(); 

    try
    {
      tempInt = Integer.parseInt(str);  
    }
    catch(Exception e)
    {
    }

    return tempInt;
  }
  
  private int getNextSeqVal()
  {
    //set result with default control number... using old scheme...
    int result = getIntTime(cntrlNumStr) + currentRecNum;

    try
    {
      #sql [Ctx]
      {
        select   RAP_CNTRL_NUM_SEQ.nextval into :result
        from     dual
      };
    }
    catch(Exception e)
    {
    }

    return result;
  }


  private int getRandomNum()
  {
    int result = 123;
    
    double tempRand = java.lang.Math.random();
    
    tempRand = tempRand * 100;

    int tempInt = java.lang.Math.round((float)tempRand);
    
    switch(tempInt)
    {
      case 0:
        result = 111;
      break;
      case 1:
        result = 222;
      break;
      case 2:
        result = 333;
      break;
      case 3:
        result = 444;
      break;
      case 4:
        result = 555;
      break;
      case 5:
        result = 6661;
      break;
      case 6:
        result = 777;
      break;
      case 7:
        result = 888;
      break;
      case 8:
        result = 22;
      break;
      case 9:
        result = 0;
      break;
    }
    return result;
  }

  private void createTrailer() throws Exception
  {
    fileDataLine = new String[4];
    idx          = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "99",   NO_FORMATTING); 

    // 2 easi partner number
    addField(null, null, 4, currentPartnerNum, NO_FORMATTING); 

    // 3 number of records in the file
    addField(null, null, 5, Integer.toString(batchSize), NUMS_ONLY);

    // 4 filler
    addField(null, null, 553, "", NO_FORMATTING);
  }

  private void createHeader() throws Exception
  {
    fileDataLine = new String[5];
    idx          = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "00",   NO_FORMATTING); 

    // 2 easi partner number
    addField(null, null, 4, currentPartnerNum, NO_FORMATTING); 

    // 3 version number
    addField(null, null, 2, "01", NO_FORMATTING);

    // 4 date timestamp
    addField(null, null, 24, (headerDateStr + "." + headerTimeStr + ".0000"), NO_FORMATTING);

    // 5 filler
    addField(null, null, 532, "", NO_FORMATTING);
  }

  private void createPreHeader() throws Exception
  {
    fileDataLine = new String[1];
    idx          = 0;   //reset the array index

    // 1 preheader
    addField(null, null, 28, "+DATA MERCHE,MBP0P,000000001", NO_FORMATTING); //set to send to production ..test mail box.. is MBP0T
  }

  private void createPostTrailer() throws Exception
  {
    fileDataLine = new String[1];
    idx          = 0;   //reset the array index

    // 1 post trailer
    addField(null, null, 8, "+ENDDATA",   NO_FORMATTING); 
  }

  /*
  ** METHOD private boolean loadBatchData()
  **
  ** Timestamps today's batch and fills a StringBuffer with the data formatted
  ** according to the NSI batch specs.
  **
  ** RETURNS: true if successful, else false if an error occured (exception
  **          was thrown
  */
  private boolean loadFileData()
  {
    // initialize batch storage
    batchSize          = 0;
    
    boolean errorOccurred = false;
    
    ResultSet rs = null;

    try
    {
      // get batch data
      rs      = getBatchRecords();
      fwriter = new FileWriter(uploadFile);

      createPreHeader();
      writeLineToFile();      

      createHeader();
      writeLineToFile();      
      
      while (rs.next())
      {
        //System.out.println("got rs.. going to getrecorddata");
        
        errorOccurred = getRecordData(rs);
        
        //System.out.println("back from getrecorddata erroroccured = " + errorOccurred);
        
        if(errorOccurred)
        {
          break;
        }
        else
        {
          //System.out.println("writing line to file");
          
          writeLineToFile(); //will throw exception
          
          //System.out.println("wrote line to file");
        }
      }
      
      if(batchSize > 0)
      {
        createTrailer();
        writeLineToFile();      

        createPostTrailer();
        writeLineToFile();      
      }

      fwriter.close(); //close the file...
      
      if(errorOccurred)
      {
        // rollback the timestamping
        con.rollback(); 
      }
      else
      {
        // commit the timestamping      
        con.commit();
      }
    }
    catch (Exception e)
    {
      // rollback the timestamping
      try 
      {
        con.rollback(); 
        fwriter.close(); //close the file...
      }
      catch (Exception re) 
      {
        System.out.println(this.getClass().getName() + "::loadFileData(con.rollback()): " + re);
        logEntry("loadFileData(con.rollback())", re.toString());
      }
      
      System.out.println(this.getClass().getName() + "::loadFileData(): " + e);
      logEntry("loadFileData()", e.toString());
      errorOccurred = true;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { batch_it.close(); } catch (Exception e) {}
    }
    
    return errorOccurred;
  }
  

  public boolean generateFile(File uploadFile)
  {
    this.uploadFile = uploadFile;
    return getData(PARTNER_RAP);
  }

  /*
  ** METHOD public boolean execute()
  **
  ** EventBase entry point.
  ** creates tape file, generates a tape file status email message and sends it.
  ** RETURNS: true if no errors occurred....
  */
  public boolean getData(String partnerNum)
  {
    this.currentPartnerNum  = partnerNum;

    boolean errorOccurred   = false;
    boolean fileCreated     = false;

    try
    {
      connect();

      // get current date and time
      Calendar   cal  = Calendar.getInstance();

      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      DateFormat tf   = new SimpleDateFormat("hh:mm:ss");
      dateString      = df.format(cal.getTime());
      timeString      = tf.format(cal.getTime());

      DateFormat hdf  = new SimpleDateFormat("yyyy-MM-dd");
      DateFormat htf  = new SimpleDateFormat("hh.mm.ss");
      headerDateStr   = hdf.format(cal.getTime());
      headerTimeStr   = htf.format(cal.getTime());
      
      DateFormat cnf  = new SimpleDateFormat("HHmm");
      cntrlNumStr     = cnf.format(cal.getTime());


      //create file here
      fileCreated     = createFile(); 
     
      
      if(fileCreated) // generate the file data.. and write it to the file....
      {
        fileName      = uploadFile.getName();
      
        errorOccurred = loadFileData();
      }
      else
      {
        errorOccurred = true;
      }

      // if errors occurred. then delete the file if created
      if (errorOccurred)
      {
        if(fileCreated)
        {
          fileCreated = !deleteFile();
          fileName    = "DELETED";
        }
        
        logEntry("execute()","File Upload failed.");
      }

      //if no records delete appropriate file...
      if(batchSize == 0 && fileCreated)
      {
        fileCreated = !deleteFile();
        fileName    = "DELETED";
      }

      if(fileCreated)
      {
        //put file names in this map so we can access them later
        fileNames.put(this.currentPartnerNum,fileName);
        this.ftpSuccessful = true;//ftp(fileName);
      }
    }
    catch(Exception e)
    {

      System.out.println(this.getClass().getName() + "::execute(): " + e);
      logEntry("execute()",e.toString());

      errorOccurred = true;

      if(fileCreated)
      {
        fileCreated = !deleteFile();
        fileName    = "DELETED";
      }
    }
    finally
    {
      cleanUp();
      reset();  //reset variable after method is completed
    }

    return (!errorOccurred);
  }

  public String getFileName(String parNum)
  {
    String result = "";

    if(isBlank(parNum))
    {
      return "";
    }
    
    try
    {
      result = (String)fileNames.get(parNum);
    }
    catch(Exception e)
    {
    }
    
    return result;
  }


  private void writeLineToFile() throws Exception
  {
    for(int i=0; i < fileDataLine.length; i++)
    {
      //System.out.println("writing  filedataline[" + i + "] = " + fileDataLine[i]);
      fwriter.write(fileDataLine[i]);
    }
    fwriter.write("\r\n");
  }
  
  private boolean createFile()
  {
    return true;
    /*
    boolean fileCreated = false;
    try
    {
      Calendar     cal      = Calendar.getInstance();
      DateFormat   df       = new SimpleDateFormat("MMddyy");
      String       fileName = df.format(cal.getTime());

          fileName       = com.mes.net.MesDefaults.getString(com.mes.net.MesDefaults.DK_TAPEUPLOAD_RELATIVE_PATH) + "DISCOVER_TAPEFILE_" + currentPartnerNum  + "_" + fileName + ".txt";
          
          System.out.println(fileName);

          uploadFile  = new File(fileName);
          
          System.out.println(currentPartnerNum + " new file(filename)");
          
          fileCreated    = uploadFile.createNewFile();

          System.out.println(currentPartnerNum + " new file created");
    }
    catch(Exception e)
    {
      System.out.println(currentPartnerNum + " Upload File could not be created.");
      logEntry("createFile()",e.toString());
    }
    return fileCreated;
    */
  }

  private boolean deleteFile()
  {
    boolean fileDeleted = false;

    try
    {
      fileDeleted = uploadFile.delete();
    }
    catch(Exception e)
    {
      System.out.println(currentPartnerNum + " Upload File could not be deleted.");
      logEntry("deleteFile()",e.toString());
    }
    return fileDeleted;
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;

    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }

    return pass;
  }
}
