/*************************************************************************

  FILE: $Archive: /Java/apps/rap/xfer/RapResponseHandler.sqlj $

  Description:
  
  RapResponseHandler

  Processes the downloaded Discover RAP response and error files.  Updates
  the database with the data contained in the downloaded file(s).
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/27/03 11:05a $
  Version            : $Revision: 15 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueueItem;
import com.mes.net.EmailQueue;

import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ResultSetIterator;

public class RapResponseHandler extends SQLJConnectionBase
{
  private String downloadPath       = "\\xfer\\download\\";
  private String archivePath        = "\\xfer\\download\\arc\\";
  private String logFile            = "\\xfer\\download\\rap_download.log";
    
  private File errFile;
  private File errArcFile;
  private File errResultFile;
  private File errArcResultFile;
  private File respFile;
  private File respArcFile;
  private File respResultFile;
  private File respArcResultFile;
  
  private Vector errFileErrors  = new Vector();
  private Vector respFileErrors = new Vector();
  
  private boolean errorParseOk      = false;
  private boolean errorResultOk     = false;
  private boolean responseParseOk   = false;
  private boolean responseResultOk  = false;
  private String errorResult;
  private String responseResult;
  
  private DefaultContext myCtx;
  
  /*
  ** public RapResponseHandler()
  **
  ** Constructor.
  **
  ** Sets up database with direct connect string.
  */
  public RapResponseHandler()
  {
    super(PROD_DIRECT_CONNECTION_STRING);
  }
  
  /*
  ** public boolean connect()
  **
  ** Overrides base method to support use of myCtx.
  **
  ** RETURNS: result of base method connect().
  */
  public boolean connect()
  {
    boolean connectOk = super.connect();
    myCtx = Ctx;
    return connectOk;
  }
  
  /*
  ** public void celanUp()
  **
  ** Overrides base method to support use of myCtx.
  */
  public void cleanUp()
  {
    myCtx = null;
    super.cleanUp();
  }
  
  /*
  ** private void loadProperties() throws Exception
  **
  ** Loads app properties.
  */
  private void loadProperties() throws Exception
  {
    // load the app properties
    Properties appProperties = new Properties();
    FileInputStream in = new FileInputStream("rapresponsehandler.properties");
    appProperties.load(in);
    in.close();

    downloadPath  = appProperties.getProperty("download.path");
    archivePath   = appProperties.getProperty("archive.path");
    logFile       = appProperties.getProperty("log.file");
  }
  
  /*
  ** private void logEvent(String logData)
  **
  ** Appends a timestamped line to DOWNLOAD.LOG in the download directory.
  */
  private void logEvent(String logData)
  {
    try
    {
      DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
      String timeStamp = df.format(Calendar.getInstance().getTime());
    
      FileWriter fw = null;
      try
      {
        fw = new FileWriter(logFile,true);
      }
      catch (Exception e)
      {
        fw = new FileWriter(logFile);
      }
      
      String logLine = timeStamp + ": (RRH) " + logData;
      fw.write(logLine + "\n");
      fw.close();
    
      System.out.println(logLine);
    }
    catch (Exception e)
    {
      System.out.println("logEvent exception: " + e);
    }
  }
  
  /*
  ** private String getResultText(File resultFile)
  **
  ** Reads the first text line from resultFile and returns it.
  **
  ** RETURNS: first line of the file as a String, null if error.
  */
  private String getResultText(File resultFile)
  {
    String resultText = null;
    try
    {
      FileReader fr = new FileReader(resultFile);
      LineNumberReader lnr = new LineNumberReader(fr);
      resultText = lnr.readLine();
      lnr.close();
    }
    catch (Exception e)
    {
      logEvent("ERROR: failed to read line from result file "
        + resultFile.getPath());
    }
    return resultText;
  }
  
  /*
  ** private void archiveFile(File originalFile, File archiveFile)
  **
  ** Moves the original file to the archive file.
  */
  private void archiveFile(File originalFile, File archiveFile)
  {
    try
    {
      if (originalFile.renameTo(archiveFile))
      {
        logEvent("Archived " + originalFile.getPath() 
          + " as " + archiveFile.getPath());
      }
      else
      {
        logEvent("ERROR: Failed to archive " + originalFile.getPath() 
          + " as " + archiveFile.getPath());
      }
    }
    catch (Exception e)
    {
      logEvent("ERROR: Exception in archiveFile() - " + e);
    }
  }
  
  /*
  ** private void initialize()
  **
  ** Initialize some handler file objects and result flags.
  */
  private void initialize()
  {
    // create a date time stamp to use for archive file naming
    DateFormat df   = new SimpleDateFormat("MMddyy_HHmm");
    String dateTime = df.format(Calendar.getInstance().getTime());
      
    errFile           = new File(downloadPath + "RAP_ERROR.TXT");
    errArcFile        = new File(archivePath  + "RAP_ERR_" + dateTime + ".TXT");
    errResultFile     = new File(downloadPath + "RAP_ERROR.RES");
    errArcResultFile  = new File(archivePath  + "RAP_ERR_" + dateTime + ".RES");
    respFile          = new File(downloadPath + "RAP_RESPONSE.TXT");
    respArcFile       = new File(archivePath  + "RAP_RSP_" + dateTime + ".TXT");
    respResultFile    = new File(downloadPath + "RAP_RESPONSE.RES");
    respArcResultFile = new File(archivePath  + "RAP_RSP_" + dateTime + ".RES");
      
    errorParseOk      = false;
    errorResultOk     = false;
    responseParseOk   = false;
    responseResultOk  = false;
  }
  
  /*
  ** public long getXferSeqNum()
  **
  ** If first time called this method will get the next discover file transfer 
  ** sequence number.  Subsequent calls will return the value originally 
  ** retrieved.
  **
  ** RETURNS: file transfer sequence number.
  */
  private long xferSeqNum = -1L;
  public long getXferSeqNum()
  {
    boolean externalConnection = !isConnectionStale();
    if (xferSeqNum == -1L)
    {
      try
      {
        if (!externalConnection)
        {
          connect();
        }
      
        #sql [Ctx]
        {
          select  discover_file_xfer_sequence.nextval 
          into    :xferSeqNum
          from    dual
        };
      }
      catch (Exception e)
      {
        String method   = this.getClass().getName() + "::getXferSeqNum()";
        String message  = e.toString();
        logEntry(method,message);
        System.out.println(method + ": " + message);
      }
      finally
      {
        if (!externalConnection)
        {
          cleanUp();
        }
      }
    }
    
    return xferSeqNum;
  }
  
  /*
  ** public abstract class Record
  **
  ** Base record class
  */
  public abstract class Record
  {
    protected String recLine;
    protected int lineNum;

    public Record(String recLine, int lineNum) throws Exception
    {
      this.recLine = recLine;
      this.lineNum = lineNum;
      parse();
    }
    
    public String getRecLine()  { return recLine; }
    public int getLineNum()     { return lineNum; }

    protected abstract void parse() throws Exception;
    
    public abstract Vector submit();
    
    public String toString()
    {
      return recLine;
    }
    
    protected boolean isBlank(String testString)
    {
      if (testString == null || testString.equals(""))
      {
        return true;
      }
      return false;
    }
  }

  /*
  ** public class HeaderRecord
  **
  ** Header record
  */
  public class HeaderRecord extends Record
  {
    private String partner;
    private String version;
    private String timestamp;
    
    public HeaderRecord(String recLine, int lineNum) throws Exception
    {
      super(recLine,lineNum);
    }
    
    protected void parse() throws Exception
    {
      partner   = recLine.substring(2,6);
      version   = recLine.substring(6,8);
      timestamp = recLine.substring(8,32);
    }
    
    public String getPartner()    { return partner; }
    public String getVersion()    { return version; }
    public String getTimestamp()  { return timestamp; }
    
    public String toString()
    {
      return "Header [ partner: " + partner + ", version: " + version
        + ", timestamp: " + timestamp + ", lineNum: " + lineNum + " ]";
    }
    
    public Vector submit()
    {
      // currently no submission needed to database
      return new Vector();
    }
  }

  /*
  ** public class ResponseRecord
  **
  ** Response record
  */
  public class ResponseRecord extends Record
  {
    private String discoverNum;
    private String acquirerNum;
    private String decisionStatus;
    private String dispositionCode;
    private String receivedDate;
    private String decisionDate;
    private Timestamp receivedTs;
    private Timestamp decisionTs;
    
    public ResponseRecord(String recLine, int lineNum) throws Exception
    {
      super(recLine,lineNum);
    }
    
    protected void parse() throws Exception
    {
      discoverNum     = recLine.substring(2,17).trim();
      acquirerNum     = recLine.substring(17,37).trim();
      decisionStatus  = recLine.substring(52,53).trim();
      dispositionCode = recLine.substring(53,55).trim();
      receivedDate    = recLine.substring(55,65).trim();
      decisionDate    = recLine.substring(65,75).trim();
      
      if(decisionStatus.equals("P") && isBlank(dispositionCode))
      {
        this.dispositionCode = "PM";
      }

      if(decisionStatus.equals("D") && dispositionCode.equals("RD"))
      {
        this.decisionStatus  = "A";
        this.dispositionCode = "AE";
      }
      
      if (!isBlank(receivedDate))
      {
        receivedTs = Timestamp.valueOf(receivedDate + " 00:00:00.000000000");
      }
      
      if (!isBlank(decisionDate))
      {
        decisionTs = Timestamp.valueOf(decisionDate + " 00:00:00.000000000");
      }
      
      if (isBlank(decisionStatus))
      {
        throw new Exception("Decision status missing");
      }
      
      if (isBlank(dispositionCode))
      {
        throw new Exception("Disposition code missing");
      }
    }
    
    public String getDiscoverNum()      { return discoverNum; }
    public String getAcquirerNum()      { return acquirerNum; }
    public String getDecisionStatus()   { return decisionStatus; }
    public String getDispositionCode()  { return dispositionCode; }
    public String getReceivedDate()     { return receivedDate; }
    public String getDecisionDate()     { return decisionDate; }
    
    public String toString()
    {
      StringBuffer recDesc = new StringBuffer();
      recDesc.append("Response [ ");
      recDesc.append(" discoverNum: " + discoverNum + ", ");
      recDesc.append(" acquirerNum: " + acquirerNum + ", ");
      recDesc.append(" decision: " + decisionStatus + ", ");
      recDesc.append(" code: " + dispositionCode + ", ");
      recDesc.append(" received: " + receivedDate + ", ");
      recDesc.append(" decided: " + decisionDate + ", ");
      recDesc.append(" lineNum: " + lineNum + " ]");
      return recDesc.toString();
    }
    
    /*
    ** public Vector submit()
    **
    ** Submits this response record data to the database.  Data is stored
    ** in rap_app_response.  Pending responses will not overwrite other
    ** responses if they already exist.
    **
    ** RETURNS: Vector containing any errors that occured.
    */
    public Vector submit()
    {
      Vector errors = new Vector();
      ResultSetIterator it = null;
      ResultSet rs = null;
      
      try
      {
        long appSeqNum;
        
        // look up the app seq num of the response
        #sql [myCtx]
        {
          select  app_seq_num
          into    :appSeqNum
          from    rap_app_info
          where   control_number = :acquirerNum
        };
        
        // determine if a response already exists
        // and if so what its decision status is
        #sql [myCtx] it = 
        {
          select  decision_status,
                  error_flags
          from    rap_app_response
          where   app_seq_num = :appSeqNum
        };
        rs = it.getResultSet();
        boolean isUpdate = false;
        String oldDecisionStatus = "";
        String oldErrorFlags = "";
        if (rs.next())
        {
          isUpdate = true;
          oldDecisionStatus = rs.getString("decision_status");
          oldErrorFlags     = rs.getString("error_flags");
          if (oldErrorFlags == null)
          {
            oldErrorFlags = "";
          }
        }
        
        // updates to existing responses
        if (isUpdate)
        {
          // if old decision is pending or new decision is
          // approval or decline then update (this prevents
          // old pending responses from overwriting newer
          // final decisions)
          if (  oldDecisionStatus.equals("P")
                || decisionStatus.equals("A")
                || decisionStatus.equals("D")
                || oldErrorFlags.equals("D1*DUP RCD") )
          {
            #sql [myCtx]
            {
              update  rap_app_response
              set     decision_status         = :decisionStatus,
                      disposition_reason_code = :dispositionCode,
                      discover_merchant_num   = :discoverNum,
                      received_date           = :receivedTs,
                      decision_date           = :decisionTs,
                      date_last_updated       = sysdate,
                      error_flags             = null,
                      xfer_seq_num            = :(getXferSeqNum())
              where   app_seq_num             = :appSeqNum
            };
          }
        }
        // no prior response received for this record, insert
        // new response
        else
        {
          #sql [myCtx]
          {
            insert into rap_app_response
            ( app_seq_num,
              acquirer_merchant_num,
              decision_status,
              disposition_reason_code,
              discover_merchant_num,
              received_date,
              decision_date,
              date_last_updated,
              error_flags,
              xfer_seq_num )
            values
            ( :appSeqNum,
              :acquirerNum,
              :decisionStatus,
              :dispositionCode,
              :discoverNum,
              :receivedTs,
              :decisionTs,
              sysdate,
              null,
              :(getXferSeqNum()) )
          };
        }
      }
      catch (Exception e)
      {
        String errorDesc = "ERROR: ResponseRecord submit failed - " + e
          + ", Record: " + toString();
        errors.add(errorDesc);
        logEvent(errorDesc);
      }
      finally
      {
        try { rs.close(); }catch (Exception e) {}
        try { it.close(); }catch (Exception e) {}
      }
      
      return errors;
    }
  }

  /*
  ** public class ErrorRecord
  **
  ** Error record
  */
  public class ErrorRecord extends Record
  {
    private String acquirerNum;
    private String errorFields;
    
    public ErrorRecord(String recLine, int lineNum) throws Exception
    {
      super(recLine,lineNum);
    }
    
    protected void parse() throws Exception
    {
      acquirerNum = recLine.substring(469,489).trim();
      errorFields = recLine.substring(564).trim();
      
      if (isBlank(errorFields))
      {
        throw new Exception("Error fields missing");
      }
      
      if (isBlank(acquirerNum))
      {
        throw new Exception("Acquirer number missing");
      }
    }
    
    public String getAcquirerNum()  { return acquirerNum; }
    public String getErrorFields()  { return errorFields; }
    
    public String toString()
    {
      return "Error Response [ acquirerNum: " + acquirerNum + ", errors: "
        + errorFields + ", lineNum: " + lineNum + " ]";
    }

    /*
    ** public Vector submit()
    **
    ** Submits this error record data to the database.  Data is stored
    ** in rap_app_response.  
    **
    ** RETURNS: Vector containing any errors that occured.
    */
    public Vector submit()
    {
      Vector errors = new Vector();
      ResultSetIterator it = null;
      ResultSet rs = null;
      
      try
      {
        long appSeqNum;
        
        // look up the app seq num of the response
        #sql [myCtx]
        {
          select  app_seq_num
          into    :appSeqNum
          from    rap_app_info
          where   control_number = :acquirerNum
        };
        
        // determine if a response already exists
        #sql [myCtx] it = 
        {
          select  decision_status
          from    rap_app_response
          where   app_seq_num = :appSeqNum
        };
        rs = it.getResultSet();
        boolean isUpdate = false;
        if (rs.next())
        {
          isUpdate = true;
        }

        // try to update the response record if it already exists
        if (isUpdate)
        {
          // ignore duplicate record errors updating previously 
          // existing responses 
          if (!errorFields.equals("D1*DUP RCD"))
          {                
            #sql [myCtx]
            {
              update  rap_app_response
              set     decision_status     = 'E',
                      date_last_updated   = sysdate,
                      error_flags         = :errorFields,
                      xfer_seq_num        = :(getXferSeqNum())
              where   app_seq_num         = :appSeqNum
            };
          }
        }
        // else insert as new response
        else
        {
          #sql [myCtx]
          {
            insert into rap_app_response
            ( app_seq_num,
              acquirer_merchant_num,
              decision_status,
              date_last_updated,
              error_flags,
              xfer_seq_num )
            values
            ( :appSeqNum,
              :acquirerNum,
              'E',
              sysdate,
              :errorFields,
              :(getXferSeqNum()) )
          };
        }
      }
      catch (Exception e)
      {
        String errorDesc = "ERROR: ErrorRecord submit failed - " + e
          + ", Record: " + toString();
        errors.add(errorDesc);
        logEvent(errorDesc);
      }
      finally
      {
        try { rs.close(); }catch (Exception e) {}
        try { it.close(); }catch (Exception e) {}
      }
      
      
      return errors;
    }
  }

  /*
  ** public class TrailerRecord
  **
  ** Trailer record
  */
  public class TrailerRecord extends Record
  {
    private String partner;
    private int recordCount;
    
    public TrailerRecord(String recLine, int lineNum) throws Exception
    {
      super(recLine,lineNum);
    }
    
    protected void parse() throws Exception
    {
      partner     = recLine.substring(2,6).trim();
      recordCount = Integer.parseInt(recLine.substring(6,11).trim());
    }
    
    public String getPartner()    { return partner; }
    public int getRecordCount()   { return recordCount; }
    
    public String toString()
    {
      return "Trailer [ partner: " + partner + ", recordCount: "
        + recordCount + ", lineNum: " + lineNum + " ]";
    }
    
    public Vector submit()
    {
      // currently no submission needed to database
      return new Vector();
    }
  }
  
  /*
  ** public class Batch
  **
  ** Batch of records.  Keeps track of header and trailer of a batch,
  ** allows submission of records.
  */
  public class Batch extends Vector
  {
    private HeaderRecord header;
    private TrailerRecord trailer;
    
    /*
    ** public boolean add(Object o)
    **
    ** Checks to see if add object is header or trailer, and if so stores
    ** a reference to it in the appropriate header or trailer member.
    **
    ** RETURNS: the result of the Vector add.
    */
    public boolean add(Object o)
    {
      if (o instanceof HeaderRecord)
      {
        header = (HeaderRecord)o;
      }
      else if (o instanceof TrailerRecord)
      {
        trailer = (TrailerRecord)o;
      }
      
      return super.add(o);
    }
    
    /*
    ** public Vector submit()
    **
    ** Causes all contained records to submit.
    **
    ** RETURNS: Vector of error descriptions.
    */
    public Vector submit()
    {
      Vector errors = new Vector();
      
      for (Iterator i = iterator(); i.hasNext();)
      {
        errors.addAll(((Record)i.next()).submit());
      }
      
      return errors;
    }
    
    /*
    ** public HeaderRecord getHeader()
    **
    ** RETURNS: batch header record
    */
    public HeaderRecord getHeader()
    {
      return header;
    }

    /*
    ** public TrailerRecord getTrailer()
    **
    ** RETURNS: batch trailer record.
    */
    public TrailerRecord getTrailer()
    {
      return trailer;
    }
  }

  private static final int RT_PLUS            = 0;
  private static final int RT_HEADER          = 1;
  private static final int RT_RESPONSE_RECORD = 2;
  private static final int RT_ERROR_RECORD_01 = 3;
  private static final int RT_ERROR_RECORD_02 = 4;
  private static final int RT_ERROR_RECORD_03 = 5;
  private static final int RT_TRAILER         = 6;
  private static final int RT_INVALID         = -1;

  /*
  ** private int getRecordType(String line)
  **
  ** Determines the record type contained in a line.
  **
  ** RETURNS: the record type code of the record.
  */
  private int getRecordType(String line)
  {
    int recordType;

    try
    {
      if (line.startsWith("+"))
      {
        recordType = RT_PLUS;
      }
      else
      {
        String recTypeStr = line.substring(0,2);
        if (recTypeStr.equals("00"))
        {
          recordType = RT_HEADER;
        }
        else if (recTypeStr.equals("05"))
        {
          recordType = RT_RESPONSE_RECORD;
        }
        else if (recTypeStr.equals("01"))
        {
          recordType = RT_ERROR_RECORD_01;
        }
        else if (recTypeStr.equals("02"))
        {
          recordType = RT_ERROR_RECORD_02;
        }
        else if (recTypeStr.equals("03"))
        {
          recordType = RT_ERROR_RECORD_03;
        }
        else if (recTypeStr.equals("99"))
        {
          recordType = RT_TRAILER;
        }
        else
        {
          throw new Exception("Invalid record type");
        }
      }
    }
    catch (Exception e)
    {
      recordType = RT_INVALID;
    }

    return recordType;
  }

  private Vector batches = null;

  private static final int S_NEW              = 1;
  private static final int S_BATCH_STARTED    = 2;
  private static final int S_BATCH_FINISHED   = 3;

  /*
  ** private Vector loadRecords(File fileToParse)
  **
  ** Opens the file and loads the records into batches.
  **
  ** RETURNS: Vector containing any errors that occured.
  */
  private Vector loadRecords(File fileToParse)
  {
    Vector errors = new Vector();
    int lineCount = 0;
    String errorDesc;
    
    try
    {
      // error batch to store out of batch records
      Batch currentBatch = null;
      Batch errorBatch = new Batch();
      batches = new Vector();
      batches.add(errorBatch);
      
      String line = null;
      int state = S_NEW;

      // read records from the file until end of file
      LineNumberReader in = new LineNumberReader(new FileReader(fileToParse));
      while ((line = in.readLine()) != null)
      {
        ++lineCount;

        // determine record type
        int recordType = getRecordType(line);

        // generate and store the record
        Record record = null;
        switch (recordType)
        {
          case RT_PLUS:
            // ignore these records for now (file transfer related)
            break;

          case RT_HEADER:
            // notice if in the midst of another batch
            if (state != S_NEW && state != S_BATCH_FINISHED)
            {
              // store previous batch if not already stored (missing trailer?)
              if (currentBatch != null)
              {
                batches.add(currentBatch);
              }
              errorDesc = "Header record at line " + lineCount
                + " found before end of previous batch";
              errors.add(errorDesc);
              logEvent(errorDesc);
            }

            // create new batch, add the header
            currentBatch = new Batch();
            record = new HeaderRecord(line,lineCount);
            currentBatch.add(record);
            
            // change state to indicate in a batch
            state = S_BATCH_STARTED;
            break;

          case RT_RESPONSE_RECORD:
            // create response record
            record = new ResponseRecord(line,lineCount);

            // notice if not in a batch
            if (state != S_BATCH_STARTED)
            {
              errorDesc = "Response record at line " + lineCount
                + " found outside of batch";
              errors.add(errorDesc);
              logEvent(errorDesc);
            }

            // store the record
            if (currentBatch == null)
            {
              errorBatch.add(record);
            }
            else
            {
              currentBatch.add(record);
            }
            break;

          case RT_ERROR_RECORD_01:
          case RT_ERROR_RECORD_02:
          case RT_ERROR_RECORD_03:
            // create error response record
            record = new ErrorRecord(line,lineCount);

            // notice if not in a batch
            if (state != S_BATCH_STARTED)
            {
              errorDesc = "Response error record at line " + lineCount
                + " found outside of batch";
              errors.add(errorDesc);
              logEvent(errorDesc);
            }

            // store the record
            if (currentBatch == null)
            {
              errorBatch.add(record);
            }
            else
            {
              currentBatch.add(record);
            }
            break;

          case RT_TRAILER:
            // create trailer record
            record = new TrailerRecord(line,lineCount);

            // notice if not in a batch
            if (state != S_BATCH_STARTED)
            {
              errorDesc = "Trailer record at line " + lineCount
                + " found outside of batch";
              errors.add(errorDesc);
              logEvent(errorDesc);
            }

            // store the trailer, add the batch
            if (currentBatch == null)
            {
              errorBatch.add(record);
            }
            else
            {
              currentBatch.add(record);
              batches.add(currentBatch);
              currentBatch = null;
            }
            
            // change state to indicate a batch was completed
            state = S_BATCH_FINISHED;
            break;

          case RT_INVALID:
            // log the invalid record
            errorDesc = "Invalid record at line " + lineCount + ": " +  line;
            errors.add(errorDesc);
            logEvent(errorDesc);
            break;
        }
      }
      
      // close the reader
      in.close();

      // notice incomplete batch (partial file downloaded?)
      if (state != S_BATCH_FINISHED)
      {
        errorDesc = "Incomplete batch, possible partial file download";
        errors.add(errorDesc);
        logEvent(errorDesc);
      }
    }
    catch (Exception e)
    {
      errorDesc = "ERROR: parsing failed at line " + lineCount + " - " + e;
      errors.add(errorDesc);
      logEvent(errorDesc);
    }
    
    // return true only if error count is zero
    return errors;
  }

  /*
  ** private Vector submitResponses()
  **
  ** Submit batches of responses to the database.  Connect() is called
  ** here, autocommit is turned off so that if errors occur during submit
  ** a rollback may be used to reverse any updates caused by this file.
  **
  ** RETURNS: vector containing any error decscription strings that occured.
  */
  private Vector submitResponses()
  {
    Vector errors = new Vector();
    boolean autoCommit = true;
    
    try
    {
      autoCommit = con.getAutoCommit();
    }
    catch (Exception e) { }

    try
    {
      // connect, turn off auto commit
      connect();
      setAutoCommit(false);
      
      // submit each batch
      for (Iterator i = batches.iterator(); i.hasNext();)
      {
        Batch batch = (Batch)i.next();
        errors.addAll(batch.submit());
        /*
        
        // disable the rollback for now
        
        if (errors.size() > 0)
        {
          throw new Exception("Batch submit failed in batch with "
            + batch.getHeader().toString());
        }
        */
      }
      
      #sql [Ctx]
      {
        commit
      };
    }
    catch (Exception e)
    {
      logEvent("ERROR: " + e);
      errors.add(e.toString());
      
      try
      {
        #sql [Ctx]
        {
          rollback
        };
      }
      catch (Exception re)
      {
        logEvent("ERROR: unable to execute rollback - " + re);
        errors.add(re.toString());
      }
    }
    finally
    {
      // disconnect, restore former auto commit setting
      cleanUp();
      setAutoCommit(autoCommit);
    }
    
    return errors;
  }
      
  /*
  ** private Vector processFile(File fileToProcess)
  **
  ** Parse a file, update database with responses in file.
  **
  ** RETURNS: a Vector containing any error strings that were returned
  **          by loadRecords or submitResponses.
  */
  private Vector processFile(File fileToProcess)
  {
    Vector errors = new Vector();
    
    // load the records
    logEvent("Parsing " + fileToProcess.getPath());
    errors.addAll(loadRecords(fileToProcess));
    if (errors.size() == 0)
    {
      // update the database, return result of update
      logEvent("Submitting " + fileToProcess.getPath());
      errors.addAll(submitResponses());
    }
    
    // load records had errors
    return errors;
  }
      
  /*
  ** private void handleFiles()
  **
  ** Parses text files, evaluates result files.
  */
  private void handleFiles()
  {
    // process error file
    if (errFile.exists())
    {
      // process the file
      errFileErrors.addAll(processFile(errFile));
      errorParseOk = (errFileErrors.size() == 0);
      if (errorParseOk)
      {
        logEvent("SUCCESS: Processed " + errFile.getPath());
      }
      // error processing the file
      else
      {
        logEvent("ERROR: errors occured while processing " + errFile.getPath());
      }
        
      // move the file to archive
      archiveFile(errFile,errArcFile);
    }
  
    // process error result file
    if (errResultFile.exists())
    {
      // store the error results text
      errorResult = getResultText(errResultFile);
      
      // flag error results as ok
      errorResultOk = errorResult.equals("SUCCESS")
                      || errorResult.equals("NO FILE TO DOWNLOAD");
    
      // move the file to archive
      archiveFile(errResultFile,errArcResultFile);
    }
    // no error result file
    else
    {
      logEvent("ERROR: " + errResultFile.getPath() + " does not exist");
     
      // note the missing file in error results text
      errorResult = errResultFile.getName() + " not found";
    }

    // process response file
    if (respFile.exists())
    {
      // process the file
      respFileErrors.addAll(processFile(respFile));
      responseParseOk = (respFileErrors.size() == 0);
      if (responseParseOk)
      {
        logEvent("SUCCESS: Processed " + respFile.getPath());
      }
      else
      {
        logEvent("ERROR: errors occured while processing " + respFile.getPath());
      }

      // move the file to archive
      archiveFile(respFile,respArcFile);
    }
    
    // process response result file
    if (respResultFile.exists())
    {
      // store the error results text
      responseResult = getResultText(respResultFile);
      
      // flag response results as ok
      responseResultOk = responseResult.equals("SUCCESS")
                         || responseResult.equals("NO FILE TO DOWNLOAD");
    
      // move the file to archive
      archiveFile(respResultFile,respArcResultFile);
    }
    // no response result file
    else
    {
      logEvent("ERROR: " + respResultFile.getPath() + " does not exist");
      
      // note the missing file in response results text
      responseResult = respResultFile.getName() + " not found";
    }
  }
  
  /*
  ** private void notifyResults() throws Exception
  **
  ** Notifies administrator of results of download processing.
  */
  private void notifyResults() throws Exception
  {
    // determine if an error occured somewhere
    boolean errorCondition = ((errFile.exists() || errArcFile.exists()) 
      && !errorParseOk) || ((respFile.exists() || respArcFile.exists()) 
      && !responseParseOk) || !errorResultOk || !responseResultOk;
    
    // build an email item descibing the results
    EmailQueueItem qi = new EmailQueueItem("RapResponseHandler","run",true);
    
    // manually set addresses until email queue item is fixed
    qi.setFrom("rapdownload@merchante-solutions.com");
    qi.addTo("tbaker@merchante-solutions.com");

    // subject line (error/success + timestamp)
    StringBuffer subject = new StringBuffer();
    subject.append(errorCondition ? "RAP DL ERROR" 
      : "RAP DL SUCCESS");
    DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
    String dateTime = df.format(Calendar.getInstance().getTime());
    subject.append(" (" + dateTime + ")");
    qi.setSubject(subject.toString());
    
    // generate descriptions of the parsing jobs
    String responseParse = "N/A";
    if (responseResult.equals("SUCCESS"))
    {
      responseParse = (responseParseOk ? "OK" : "ERROR");
    }
    String errorParse = "N/A";
    if (errorResult.equals("SUCCESS"))
    {
      errorParse = (errorParseOk ? "OK" : "ERROR");
    }
    
    // describe results of response download and parsing
    StringBuffer text = new StringBuffer();
    text.append("Main Responses\n\n");
    if (respArcResultFile.exists())
    {
      text.append("  " + respArcResultFile.getName() + ": " 
        + responseResult + "\n");
      if (respArcFile.exists())
      {
        text.append("  " + respArcFile.getName() + ": " + responseParse + "\n");
        for (Iterator i = respFileErrors.iterator(); i.hasNext();)
        {
          String error = (String)i.next();
          text.append("  * " + error + "\n");
        }
      }
      text.append("\n");
    }
    else
    {
      text.append("  ERROR - No result found for response file download\n\n");
    }
    
    // describe results of error download and parsing
    text.append("Error Responses\n\n");
    if (errArcResultFile.exists())
    {
      text.append("  " + errArcResultFile.getName() + ": " 
        + errorResult + "\n");
      if (errArcFile.exists())
      {
        text.append("  " + errArcFile.getName() + ": " + errorParse + "\n");
        for (Iterator i = errFileErrors.iterator(); i.hasNext();)
        {
          String error = (String)i.next();
          text.append("  * " + error + "\n");
        }
      }
      text.append("\n");
    }
    else
    {
      text.append("  ERROR - No result found for error file download\n\n");
    }

    qi.setText(text.toString());
    
    // send the email
    EmailQueue.push(qi);
  }
  
  /*
  ** public void run()
  **
  ** Processes the error and response files downloaded from Discover for the
  ** RAP program.  After processing the files are processed along with each
  ** file's accompanying result file.
  **
  ** Email will be sent to the system administrator when a result file
  ** is missing or the result is other than success or the parsing
  ** of a file fails.
  */
  public void run()
  {
    try
    {
      loadProperties();
      logEvent("START");
      initialize();
      handleFiles();
      notifyResults();
    }
    catch (Exception e)
    {
      logEvent("ERROR: " + e);
    }
    finally
    {
      logEvent("END");
    }
  }
  
  /*
  ** private File locateFile(String filename)
  **
  ** Tries to locate a file given a filename.  Looks first in the local
  ** directory, then tries the download archives.
  **
  ** RETURNS: File object of found file, or null if file is not found.
  */
  private File locateFile(String filename)
  {
    // attempt to create file with filename given
    File file = new File(filename);
    
    // if not found look for file in download archives
    if (!file.exists())
    {
      file = new File(archivePath + filename);
    }
    
    // if not found look for file in download
    if (!file.exists())
    {
      file = new File(downloadPath + filename);
    }
    
    // give up
    if (!file.exists())
    {
      logEvent("ERROR: " + filename + " not found.");
      return null;
    }
    
    return file;
  }
  
  /*
  ** public void processSingleFile(String filename)
  **
  ** Processes a single response or error file.
  */
  public void processSingleFile(String filename)
  {
    try
    {
      loadProperties();
      logEvent("START");
      File file = locateFile(filename);
      if (file != null)
      {
        processFile(file);
      }
    }
    catch (Exception e)
    {
      logEvent("ERROR: " + e);
    }
    finally
    {
      logEvent("END");
    }
  }

  /*
  ** public static void main(String[] args)
  **
  ** Main entry point for the RapResponseHandler application.  Creates an instance
  ** of this class and calls the run method.
  */
  public static void main(String[] args)
  {
    try
    {
      RapResponseHandler engine = new RapResponseHandler();
      
      if (args.length > 0)
      {
        engine.processSingleFile(args[0]);
      }
      else
      {
        engine.run();
      }
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
      com.mes.database.OracleConnectionPool.getInstance().cleanUp();
    }
  }
}
