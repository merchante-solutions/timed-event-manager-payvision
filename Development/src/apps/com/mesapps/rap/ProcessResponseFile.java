package com.mesapps.rap;

import java.io.File;
import com.mes.ops.EasiDownloadResponse;

public class ProcessResponseFile
{
  public static void main(String[] args)
  {
    try
    {
      if(args.length == 1)
      {
        (new EasiDownloadResponse()).getData(new File(args[0]));
      }
      else
      {
        System.out.println("you must pass a filename to this class");
      }
    }
    catch(Exception e)
    {
      System.out.println("ProcessResponseFile::main(): " + e.toString());
    }
  }
}