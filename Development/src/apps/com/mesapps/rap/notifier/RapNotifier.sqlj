/*************************************************************************

  FILE: $Archive: /Java/apps/rap/RapNotifier.sqlj $

  Description:  
  
    RapNotifier
    
    A quick and dirty process that sends email notifications to rap
    members when a response has been returned on an application.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/03/03 12:20p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

import java.net.*;
import java.io.*;
import java.sql.ResultSet;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import sqlj.runtime.ResultSetIterator;

import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueueItem;
import com.mes.net.EmailQueue;

public class RapNotifier extends SQLJConnectionBase
{
  private void processResponses()
  {
    try
    {
      connect();
      
      ResultSetIterator it = null;
      #sql [Ctx] it = 
      {
        select  i.dba_name,
                i.app_seq_num,
                i.app_date,
                r.decision_status,
                r.error_flags,
                r.received_date,
                m.email
                
        from    rap_app_info i,
                rap_app_response r,
                rap_members m
                
        where   r.decision_status in ('A','D','E')
                and r.app_seq_num = i.app_seq_num
                and i.needs_notify = 'Y'
                and ( '60110' || i.rap_member_num ) = m.member_id
                and m.email is not null
      };
      ResultSet rs = it.getResultSet();
      DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
      while (rs.next())
      {
        long    appSeqNum     = rs.getLong("app_seq_num");
        String  emailTo       = rs.getString("email");
        String  dbaName       = rs.getString("dba_name");
        Date    submitDate    = rs.getDate("app_date");
        String  decision      = rs.getString("decision_status");
        String  errorFlags    = rs.getString("error_flags");
        String  status        = "";
        String  statusPhrase  = "";
        boolean statusOk      = true;
        if (decision.equals("A"))
        {
          status = "Approved";
          statusPhrase = "was approved";
        }
        else if (decision.equals("D"))
        {
          status = "Declined";
          statusPhrase = "was declined";
        }
        else if (decision.equals("E") && !errorFlags.equals("D1*DUP RCD")
                  && !errorFlags.equals("D2*DUP RCD"))
        {
          status = "Error";
          statusPhrase = "was rejected";
        }
        else
        {
          statusOk = false;
        }
        
        if (statusOk)
        {
          EmailQueueItem qi = new EmailQueueItem("AutoNotify","rapnotifier",appSeqNum);
          qi.setFrom("rap@discoverfinancial.com");
          qi.addTo(emailTo);
          qi.setSubject("Discover Merchant Referral Status Update: " + dbaName + " - " + status);
          StringBuffer text = new StringBuffer();
          text.append("Please click on the link below to receive updated ");
          text.append("status information for the merchant referral ");
          text.append("submitted to Discover Business Services using ");
          text.append("the Referral Acquisition Program (RAP).\n\n");
          text.append("Merchant: " + dbaName + ", submitted on ");
          text.append(df.format(submitDate) + ", " + statusPhrase + ".\n\n");
          text.append("For more details about this referral's status click the following link:\n\n");
          text.append("http://www.discoverrap.com/rap/status/lookup.jsp\n\n");
          text.append("For assistance, please feel free to reply to this message ");
          text.append("via email for a response within 24 hours.  You may also ");
          text.append("call and speak to a Discover Portfolio Manager at ");
          text.append("1-800-347-3062 during the hours of 8:30 am - 6:00 pm EST.");
          qi.setText(text.toString());
          System.out.println("QUEUE ITEM PUSHED: \n" + qi.toString());
          EmailQueue.push(qi);
        
          #sql [Ctx]
          {
            update  rap_app_info
            set     needs_notify = 'N'
            where   app_seq_num = :appSeqNum
          };
        }
        else
        {
          System.out.println("*** ERROR: Invalid Status ('" + decision + "')");
        }
      }
    }
    catch (Exception e)
    {
      System.out.println("Exception in processResponses(): " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
  }
  
  private synchronized void run()
  {
    while (true)
    {
      try
      {
        processResponses();
        wait(5000);
      }
      catch (Exception e)
      {
        System.out.println("Exception in run(): " + e);
        e.printStackTrace();
      }
    }
  }
  
  public static void main(String[] args) throws IOException
  {
    try
    {
      System.out.println("RapNotifier running...");
      (new RapNotifier()).run();
    }
    catch (Exception e)
    {
      System.out.println("Exception in main(): " + e);
      e.printStackTrace();
    }
    OracleConnectionPool.getInstance().cleanUp();
  }
}
