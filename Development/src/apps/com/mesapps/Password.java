package com.mesapps;

import com.mes.crypt.MD5;

public class Password
{
  public static void main(String[] args)
  {
    try
    {
      String clearPassword = args[0];
      
      MD5 md5 = new MD5();
      
      md5.Update(clearPassword);
      
      String passwordEnc = md5.asHex();
      
      System.out.println("clear: " + clearPassword);
      System.out.println("enc:   " + passwordEnc);
    }
    catch(Exception e)
    {
      System.out.println("main: " + e.toString());
    }
  }
}
