import com.mes.net.EmailQueue;

public class EmailQueueProcess
{
  public synchronized void run() throws Exception
  {
    long waitInterval = 15000L;
      
    while (true)
    {
      try
      {
        EmailQueue.send(true);
        wait(waitInterval);
      }
      catch (Exception e)
      {
        System.out.println("error: " + e);
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args)
  {
    try
    {
      (new EmailQueueProcess()).run();
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
  }
}
