import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.BankServAchFile;

import java.io.FileOutputStream;

import bankserv.hermes.api.FileProcess;

public class Test
{
  public static void main(String args[])
  {
    try
    {
      String fileName = args[0];
      /*
      System.out.println("building ACH file");
      SQLJConnectionBase.setConnectString(SQLJConnectionBase.PROD_DIRECT_CONNECTION_STRING);
      BankServAchFile file = new BankServAchFile();

      file.buildACHFile(66666, null);

      // output file to an actual file on the file system
      StringBuffer  fileName = new StringBuffer("inbound/mes_ach_");
      fileName.append(file.getFileBase());
      fileName.append(".inbound");

      FileOutputStream  out = new FileOutputStream(fileName.toString());
      out.write(file.getFile().getBytes());
      out.close();
      */

      // now send file to BankServ
      FileProcess fp = new FileProcess();

      // send file to BankServ
      fp.uploadFile(fileName);

      System.out.println("DONE");
    }
    catch(Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
  }
}
