import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.BankServAchFile;

import java.io.FileOutputStream;

import bankserv.hermes.api.FileProcess;

public class getAllExtension
{
  public static void main(String args[])
  {
    try
    {
      if(args.length == 0)
      {
        System.out.println("you must pass the extension you want to download");
      }
      else
      {
        String extension = args[0];
      
        System.out.println("Attempting to download all cofirm files with extension: " + extension);
      
        // now send file to BankServ
        FileProcess fp = new FileProcess();
      
        // retrieve files from bankserv
        fp.downloadFileList(extension);
        
        // move file to SNA server for processing
      }
    }
    catch(Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
  }
}
