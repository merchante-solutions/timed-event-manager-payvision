
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.BankServAchFile;

import java.io.FileOutputStream;

import bankserv.hermes.api.FileProcess;

public class getConfirm
{
  public static void main(String args[])
  {
    try
    {
      if(args.length == 0)
      {
        System.out.println("You must pass in the base filename you want to retrieve");
      }
      else
      {
        StringBuffer  fName = new StringBuffer(args[0]);
        fName.append(".confirm");
        System.out.println("Attempting to download file: " + fName.toString());
      
        // now send file to BankServ
        FileProcess fp = new FileProcess();
      
        // retrieve files from bankserv
        fp.downloadFile(fName.toString());
      }
    }
    catch(Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
  }
}
