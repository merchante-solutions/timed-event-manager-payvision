/*************************************************************************

  FILE: $Archive: /Java/apps/redirector/XmlSpoofer.java $

  Description:  
  
    XmlSpoofer
    
    A quick and dirty server that sits on a port and forwards requests
    sent to it and returns the results to the requesting server.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/03/03 12:24p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.redir;

import java.net.*;
import java.io.*;
import java.util.Properties;

public class XmlSpoofer
{
	private static Properties properties = null;
  
	private static void loadProperties() throws IOException
  {
		// create and load current application properties
		properties = new Properties();
		FileInputStream in = new FileInputStream("xmlspoofer.properties");
		properties.load(in);
		in.close();
	}
  
  public static void main(String[] args) throws IOException
  {
    ServerSocket serverSocket = null;
    boolean listening = true;
    
    loadProperties();
    int xmlSpooferPort = Integer.parseInt(properties.getProperty("xmlspoofer.port"));
    int spoofType = Integer.parseInt(properties.getProperty("spoof.type"));

    try
    {
      serverSocket = new ServerSocket(xmlSpooferPort);
    }
    catch (IOException e)
    {
      System.out.println("Could not listen on port " + xmlSpooferPort);
      System.exit(-1);
    }

    System.out.println("XmlSpoofer running on port " + xmlSpooferPort);

    while (listening)
    {
      new XmlSpooferThread(serverSocket.accept(),spoofType).start();
    }

    serverSocket.close();
  }
}
