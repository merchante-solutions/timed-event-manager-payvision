/*************************************************************************

  FILE: $Archive: /Java/apps/redirector/Redirector.java $

  Description:  
  
    Redirector
    
    A quick and dirty server that sits on a port and forwards requests
    sent to it and returns the results to the requesting server.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/05/04 9:53a $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.redir;

import java.io.*;
import java.net.*;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Redirector
{
  /*
  ** class MySSLSocketFactory extends javax.net.ssl.SSLSocketFactory
  **
  ** This factory wrapper class uses the system's factory to generate sockets,
  ** but then sets a read timeout in the sockets.
  */
  public class MySSLSocketFactory extends SSLSocketFactory
  {
    SSLSocketFactory sslSocketFactory;

    public MySSLSocketFactory(SSLSocketFactory sslSocketFactory)
    {
      this.sslSocketFactory = sslSocketFactory;
    }

    void setTimeOut(Socket socket) throws IOException
    {
      socket.setSoTimeout(socketTimeout);
    }

    public Socket createSocket(String host,int port)
      throws IOException, UnknownHostException
    {
      Socket socket = sslSocketFactory.createSocket(host, port);
      setTimeOut(socket);
      return socket;
    }

    public Socket createSocket(String host, int port, InetAddress clientHost,
      int clientPort)
      throws IOException, UnknownHostException
    {
      Socket socket = 
        sslSocketFactory.createSocket(host,port,clientHost,clientPort);
      setTimeOut(socket);
      return socket;
    }

    public Socket createSocket(InetAddress host,int port) 
      throws IOException, UnknownHostException
    {
      Socket socket = sslSocketFactory.createSocket(host, port);
      setTimeOut(socket);
      return socket;
    }

    public Socket createSocket(InetAddress address,int port,
      InetAddress clientAddress,int clientPort)
      throws IOException
    {
      Socket socket = 
        sslSocketFactory.createSocket(address,port,clientAddress,clientPort);
      setTimeOut(socket);
      return socket;
    }

    public Socket createSocket(Socket socket,String host,int port,boolean autoClose)
      throws IOException
    {
      Socket socket2 = sslSocketFactory.createSocket(socket,host,port,autoClose);
      setTimeOut(socket2);
      return socket2;
    }

    public String[] getSupportedCipherSuites()
    {
      return sslSocketFactory.getSupportedCipherSuites();
    }

    public String[] getDefaultCipherSuites()
    {
      return sslSocketFactory.getDefaultCipherSuites();
    } 
  }

  private static long       threadCount       = 0;

  public static String      END_POST_DATA     = "end_post_data";
  
  private int               redirectorPort    = 0;
  private int               socketTimeout     = 0;
  private boolean           isVerbose         = false;
  private long              idleTimeout       = 0L;
  
  private String            logFilename       = null;
  private FileWriter        logOut            = null;
  private SimpleDateFormat  logDf             = null;
  
  private List              threads           = new ArrayList();
  private Properties        properties        = null;

  /*
  ** private void consoleOut(String output)
  **
  ** If verbose flag is on the output string is printed to the console, otherwise
  ** this does nothing.
  */
  private void consoleOut(String output)
  {
    if (isVerbose)
    {
      System.out.println(output);
    }
  }
  
  private void openLogFile()
  {
    try
    {
      logOut  = new FileWriter(logFilename);
      logDf   = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".openLogFile(), " + e.toString());
    }
  }

  public synchronized void log(String logOutput)
  {
    try
    {
      String logLine = logDf.format(new Date()) + " " + logOutput + "\n";
      logOut.write(logLine);
      logOut.flush();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".log(), " + e.toString());
    }
      
    consoleOut(logOutput);
  }
    
  /*
  ** private void loadProperties() throws IOException
  ** 
  ** Loads application properties.
  */
  private void loadProperties() throws IOException
  {
    // create and load current application properties
    properties = new Properties();
    FileInputStream in = new FileInputStream("redirector.properties");
    properties.load(in);
    in.close();

    redirectorPort
          = Integer.parseInt(properties.getProperty("redirector.port"));
    socketTimeout
          = Integer.parseInt(properties.getProperty("socket.timeout"));
    isVerbose       
          = Boolean.valueOf(properties.getProperty("is.verbose")).booleanValue();
    idleTimeout
          = Long.parseLong(properties.getProperty("idle.thread.timeout"));
    logFilename
          = properties.getProperty("log.filename");
  }
  
  /**
   ** public class MyX509TrustManager
   **
   ** Wrapper around a X509TrustManager that trusts all certificates so that
   ** certificate authentication can be ignored.
   */
  public class MyX509TrustManager implements X509TrustManager
  {
    public X509Certificate[] getAcceptedIssuers() 
    {
        return null;
    }
    public boolean isClientTrusted(X509Certificate[] chain) 
    {
      return true;
    }
    public void checkClientTrusted(X509Certificate[] certs, String authType)
    {
    }
    public void checkServerTrusted(X509Certificate[] certs, String authType)
    {
    }
    public boolean isServerTrusted(X509Certificate[] chain)
    {
      return true;
    }
  }

  /*
  ** private void run() throws Exception
  **
  ** Application main body.
  */
  private void run() throws Exception
  {
    // load application config info, open log file
    try
    {
      loadProperties();
      openLogFile();
    }
    catch (Exception e)
    {
      throw new Exception("Failure loading properties: " + e);
    }

    // setup ssl socket factory wrapper 
    try
    {
      // create a trust manager that does not validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{ new MyX509TrustManager() };
      SSLContext context = SSLContext.getInstance("SSLv3");
      context.init(null,trustAllCerts,new SecureRandom());
      SSLSocketFactory factory = context.getSocketFactory();
      HttpsURLConnection.setDefaultSSLSocketFactory(new MySSLSocketFactory(factory));
    }
    catch (Exception e)
    {
      throw new Exception("Failure during setup of default SSLSocketFactory: " + e);
    }

    // create server socket to listen for redirection requests
    ServerSocket serverSocket = null;
    try
    {
      serverSocket = new ServerSocket(redirectorPort);
    }
    catch (Exception e)
    {
      throw new Exception("Failure creating server socket: " + e);
    }
    
    // start monitor thread
    (new RedirectorMonitor(this,threads,idleTimeout)).start();

    System.out.println("Redirector running on port " + redirectorPort);

    // accept redirection requests
    boolean listening = true;
    while (listening)
    {
      try
      {
        Thread senderThread = new RedirectorSender(this,serverSocket.accept());
        senderThread.start();
        threads.add(senderThread);
      }
      catch (Exception e)
      {
        System.out.println("Error accepting serverSocket: " + e);
      }
    }

    serverSocket.close();
  }
  
  /*
  ** public static void main(String[] args)
  **
  ** Main entry point for execution.
  */
  public static void main(String[] args)
  {
    try
    {
      (new Redirector()).run();
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
  }
}
