package com.mesapps.redir;

import java.text.*;
import java.util.*;

/*
** public class ConnectData
**
** Stores information about a connection and events that occur during the life
** of the connection.  Methods are provided for noting timestamps for specific
** events and for assisting during the logging of the connection data.
*/
public class ConnectData
{
  public class ConnectEvent
  {
    private long    ts            = -1L;
    private String  name          = null;
    private String  data          = null;
    
    public ConnectEvent(String name)
    {
      this.name = name;
      setTimestamp();
    }

    private void setTimestamp()
    {
      ts = (new Date()).getTime();
    }
    public long getTimestamp()
    {
      return ts;
    }
    public Date getTimestampDate()
    {
      if (ts != -1L)
      {
        return new Date(ts);
      }
      return null;
    }

    public String getData()
    {
      return data;
    }
    public void setData(String data)
    {
      this.data = data;
    }
    
    public String getName()
    {
      return name;
    }
  }
  
  private Vector events = new Vector();
  
  public ConnectData()
  {
    logEvent("created");
  }

  public ConnectEvent logEvent(String eventName)
  {
    ConnectEvent event = new ConnectEvent(eventName);
    events.add(event);
    return event;
  }
  
  public ConnectEvent logEvent(String eventName, String eventData)
  {
    ConnectEvent event = logEvent(eventName);
    event.setData(eventData);
    return event;
  }
    
  public long getLastTimestamp()
  {
    ConnectEvent event = (ConnectEvent)events.lastElement();
    if (event != null)
    {
      return event.getTimestamp();
    }
    return -1L;
  }
  
  public Date getLastTimestampDate()
  {
    long ts = getLastTimestamp();
    if (ts != -1L)
    {
      return new Date(ts);
    }
    return null;
  }
  
  public long getIdleTime()
  {
    long ts = getLastTimestamp();
    if (ts != -1L)
    {
      return (new Date()).getTime() - getLastTimestamp();
    }
    return 0;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer("*** ConnectData ***\n");
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
    for (Iterator i = events.iterator(); i.hasNext();)
    {
      ConnectEvent event = (ConnectEvent)i.next();
      buf.append(df.format(event.getTimestampDate()));
      buf.append(" '" + event.getName() + "'");
      String eventData = event.getData();
      if (eventData != null)
      {
        buf.append(" ('");
        buf.append(eventData);
        buf.append("')");
      }
      buf.append("\n");
    }
    return buf.toString();
  }
}
  
