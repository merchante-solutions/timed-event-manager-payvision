/*
** public class ThreadMonitor extends Thread
**
** This thread periodically scans the threads vector to see how many active
** threads are present.  Inactive threads are removed and garbage collection
** is initiated.
*/

package com.mesapps.redir;

import java.util.*;

public class RedirectorMonitor extends Thread
{
  private Redirector redir;
  private Collection threads;
  private long idleTimeout;

  public RedirectorMonitor(Redirector redir, Collection threads, long idleTimeout)
  {
    this.redir = redir;
    this.threads = threads;
    this.idleTimeout = idleTimeout;
  }

  public void run()
  {
    Runtime rt = Runtime.getRuntime();
    while (true)
    {
      try
      {
        // monitor active threads at intervals equal to idle timeout setting
        sleep(idleTimeout);
      
        int threadCount = 0;
        boolean threadRemoved = false;
        long memCheckTs = -1L;
        for (Iterator i = threads.iterator();i.hasNext();)
        {
          RedirectorSender sender = (RedirectorSender)i.next();
          if (sender.getIdleTime() > idleTimeout)
          {
            // log this to a file 
            redir.log("Interrupting thread " + sender.getId() + ".");
            sender.interrupt();
            redir.log("Message thread timeout due to idleness:");
            redir.log(sender.getConnectData().toString());
          }
          if (sender.isAlive())
          {
            ++threadCount;
          }
          else
          {
            redir.log("Removing dead thread " + sender.getId());
            sender.closeIo();
            i.remove();
            threadRemoved = true;
          }
        }
        
        if (threadRemoved)
        {
          redir.log("garbage collecting...");
          rt.gc();
        }
        
        // log memory and thread count every five minutes
        long curTs = (new Date()).getTime();
        if (curTs - memCheckTs > 300000L || memCheckTs < 0)
        {
          redir.log("free memory: " + rt.freeMemory());
          redir.log(threadCount + " active threads detected.");
          memCheckTs = curTs;
        }
      }
      catch (Exception e)
      {
        System.out.println("MonitorThread error: " + e.toString());
      }
    }
  }
}
  
