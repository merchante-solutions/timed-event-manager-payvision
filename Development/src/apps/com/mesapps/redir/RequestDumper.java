/*************************************************************************

  FILE: $Archive: /Java/apps/redirector/RequestDumper.java $

  Description:  
  
    RequestDumper
    
    Takes a request and echoes it to the console.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/28/04 4:17p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.redir;

import java.lang.Runnable;
import java.net.*;
import java.io.*;
import javax.net.ServerSocketFactory;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import javax.net.ssl.*;

public class RequestDumper
{
  /*
  ** public class ConnectData
  **
  ** Stores information about a connection and events that occur during the life
  ** of the connection.  Methods are provided for noting timestamps for specific
  ** events and for assisting during the logging of the connection data.
  */
  public class ConnectData
  {
    public class ConnectEvent
    {
      private long    ts            = -1L;
      private String  name          = null;
      private String  data          = null;
      
      public ConnectEvent(String name)
      {
        this.name = name;
        setTimestamp();
      }

      private void setTimestamp()
      {
        ts = (new Date()).getTime();
      }
      public long getTimestamp()
      {
        return ts;
      }
      public Date getTimestampDate()
      {
        if (ts != -1L)
        {
          return new Date(ts);
        }
        return null;
      }

      public String getData()
      {
        return data;
      }
      public void setData(String data)
      {
        this.data = data;
      }
      
      public String getName()
      {
        return name;
      }
    }
    
    private Vector events = new Vector();
    
    public ConnectData()
    {
      logEvent("created");
    }

    public ConnectEvent logEvent(String eventName)
    {
      ConnectEvent event = new ConnectEvent(eventName);
      events.add(event);
      return event;
    }
    
    public ConnectEvent logEvent(String eventName, String eventData)
    {
      ConnectEvent event = logEvent(eventName);
      event.setData(eventData);
      return event;
    }
      
    public long getLastTimestamp()
    {
      ConnectEvent event = (ConnectEvent)events.lastElement();
      if (event != null)
      {
        return event.getTimestamp();
      }
      return -1L;
    }
    
    public Date getLastTimestampDate()
    {
      long ts = getLastTimestamp();
      if (ts != -1L)
      {
        return new Date(ts);
      }
      return null;
    }
    
    public long getIdleTime()
    {
      long ts = getLastTimestamp();
      if (ts != -1L)
      {
        return (new Date()).getTime() - getLastTimestamp();
      }
      return 0;
    }

    public String toString()
    {
      StringBuffer buf = new StringBuffer("*** ConnectData ***\n");
      SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
      for (Iterator i = events.iterator(); i.hasNext();)
      {
        ConnectEvent event = (ConnectEvent)i.next();
        buf.append(df.format(event.getTimestampDate()));
        buf.append(" '" + event.getName() + "'");
        String eventData = event.getData();
        if (eventData != null)
        {
          buf.append("\nData: '");
          buf.append(eventData);
          buf.append("'");
        }
        buf.append("\n");
      }
      return buf.toString();
    }
  }
  
  /*
  ** public class MonitorThread extends Thread
  **
  ** This thread periodically scans the threads vector to see how many active
  ** threads are present.  Inactive threads are removed and garbage collection
  ** is initiated.
  */
  public class MonitorThread extends Thread
  {
    public void run()
    {
      Runtime rt = Runtime.getRuntime();
      while (true)
      {
        try
        {
          // monitor active threads at intervals equal to idle timeout setting
          sleep(idleThreadTimeout);
        
          int threadCount = 0;
          boolean threadRemoved = false;
          long memCheckTs = -1L;
          for (Iterator i = threads.iterator();i.hasNext();)
          {
            RequestReceiver ms = (RequestReceiver)i.next();
            if (ms.getIdleTime() > idleThreadTimeout)
            {
              // log this to a file 
              log("Interrupting thread " + ms.getId() + ".");
              ms.interrupt();
              log("Message thread timeout due to idleness:");
              log(ms.getConnectData().toString());
            }
            if (ms.isAlive())
            {
              ++threadCount;
            }
            else
            {
              log("Removing dead thread " + ms.getId());
              ms.closeIo();
              i.remove();
              threadRemoved = true;
            }
          }
          
          if (threadRemoved)
          {
            log("garbage collecting...");
            rt.gc();
          }
          
          // log memory and thread count every five minutes
          long curTs = (new Date()).getTime();
          if (curTs - memCheckTs > 300000L || memCheckTs < 0)
          {
            log("free memory: " + rt.freeMemory());
            log(threadCount + " active threads detected.");
            memCheckTs = curTs;
          }
        }
        catch (Exception e)
        {
          System.out.println("MonitorThread error: " + e.toString());
        }
      }
    }
  }
  
  /*
  ** public static class RequestReceiver implements Runnable
  **
  ** Redirector message handler.
  */
  public class RequestReceiver extends Thread
  {
    private static final int  CLIENT_TIMEOUT_MS = 10000;
    
    private Socket            socket            = null;
    private BufferedReader    srcBr             = null;
    private DataOutputStream  srcDos            = null;
    //private OutputStream      srcOs             = null;
    
    private StringBuffer      reqData           = null;
      
    private ConnectData       conData           = new ConnectData();
    private long              id                = 0L;
    
    private boolean           ioClosed          = false;

    /*
    ** public RequestReceiver(Socket socket)
    **
    ** Constructor.
    **
    ** Assigns an id to self and stores ref to client socket.
    */
    public RequestReceiver(Socket socket)
    {
      this.socket = socket;
      this.id = getThreadId();
    }
  
    /*
    ** public long getIdleTime()
    **
    ** RETURNS: how long in milliseconds the thread has been idle.
    */
    public long getIdleTime()
    {
      return conData.getIdleTime();
    }
    
    /*
    ** public ConnectData getConnectData()
    **
    ** RETURNS: reference to internal ConnectData object.
    */
    public ConnectData getConnectData()
    {
      return conData;
    }
    
    /*
    ** public long getId()
    **
    ** RETURNS: id assigned to this thread.
    */
    public long getId()
    {
      return id;
    }
    
    /*
    ** private void getRequestData() throws Exception
    **
    ** Opens some io streams, configures the socket connection to the 
    ** requesting client to timeout, reads all request data, echoes it to
    ** console out.
    */
    private void getRequestData() throws Exception
    {
      // get some io streams to the originator socket
      log("getting io streams");
      socket.setSoTimeout(CLIENT_TIMEOUT_MS);
      srcBr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      log("got io streams ok");

      // get request data
      log("reading request data");
      reqData = new StringBuffer();

      String inLine = null;
      while ((inLine = srcBr.readLine()) != null)
      {
        reqData.append(inLine);
        log(inLine);
      }

      conData.logEvent("received request: " + reqData);
      System.out.println("received request: " + reqData);
      System.out.print("\n *** end of data ***\n");
      conData.logEvent("read request data ok").setData(reqData.toString());
    }
    
    /*
    ** private void sendResponseToClient() throws Exception
    **
    ** Sends contents of postResponse to client.
    */
    private void sendResponseToClient() throws Exception
    {
      srcDos = new DataOutputStream(socket.getOutputStream());
      srcDos.writeBytes("HTTP/1.0 200 OK\r\n");
      srcDos.writeBytes("Content-Length: 10\r\n");
      srcDos.writeBytes("Content-Type: text/html\r\n\r\n");
      srcDos.writeBytes("0123456789");
      srcDos.flush();

      /*
      String responseStr = "+++ RequestDumper dummy response received request '" 
        + reqData + "' +++";

      // send the generic response indicator back to the requester
      conData.logEvent("sending response");
      srcOs.write(responseStr.getBytes(),0,responseStr.length());
      conData.logEvent("response sent");
      */
    }
    
    /*
    ** public void closeIo()
    **
    ** Attempts to close io io objects that may be open, sets ioClosed to true.
    */
    public void closeIo()
    {
      if (!ioClosed)
      {
        try { srcBr.close(); }        catch (Exception e) {}
        try { srcDos.close(); }        catch (Exception e) {}
        try { socket.close(); }       catch (Exception e) {}
        ioClosed = true;
      }
    }
      
    /*
    ** public void run()
    **
    ** Given a socket, this needs to establish an HttpURLConnection with the target
    ** host, post the data, get the response from the post and return it via the
    ** socket to the originating server.
    */
    public void run()
    {
      try
      {
        getRequestData();
        sendResponseToClient();
      }
      catch (Exception e)
      {
        conData.logEvent("exception thrown",e.toString());
        System.out.println(this.getClass().getName() + 
          ".run, " + e.toString());
      }
      finally
      {
        closeIo();
      }
      conData.logEvent("finished");
    }
  }
  
  /*
  ** private void consoleOut(String output)
  **
  ** If verbose flag is on the output string is printed to the console, otherwise
  ** this does nothing.
  */
  private void consoleOut(String output)
  {
    if (isVerbose)
    {
      System.out.println(output);
    }
  }
  
  private void openLogFile()
  {
    try
    {
      logOut  = new FileWriter(logFilename);
      logDf   = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".openLogFile(), " + e.toString());
    }
  }

  private void log(String logOutput)
  {
    try
    {
      logOut.write(logDf.format(new Date()) + " " + logOutput + "\n");
      logOut.flush();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".log(), " + e.toString());
    }
      
    consoleOut(logOutput);
  }
    
  private static long       threadCount       = 0;
  
  private int               dumperPort        = 0;
  private int               socketTimeout     = 0;
  private boolean           isVerbose         = false;
  private long              idleThreadTimeout = 0L;
  
  private String            logFilename       = null;
  private FileWriter        logOut            = null;
  private SimpleDateFormat  logDf             = null;
  
  private Vector            threads           = new Vector();
  private Properties        properties        = null;

  /*
  ** private void loadProperties() throws IOException
  ** 
  ** Loads application properties.
  */
  private void loadProperties() throws IOException
  {
    // create and load current application properties
    properties = new Properties();
    FileInputStream in = new FileInputStream("dumper.properties");
    properties.load(in);
    in.close();

    dumperPort
          = Integer.parseInt(properties.getProperty("dumper.port"));
    socketTimeout
          = Integer.parseInt(properties.getProperty("socket.timeout"));
    isVerbose       
          = Boolean.valueOf(properties.getProperty("is.verbose")).booleanValue();
    idleThreadTimeout
          = Long.parseLong(properties.getProperty("idle.thread.timeout"));
    logFilename
          = properties.getProperty("log.filename");
  }
  
  /*
  ** private synchronized long getThreadId()
  **
  ** Returns next number in the thread id sequence, used to give each
  ** thread a unique id.
  **
  ** RETURNS: next id in the sequence.
  */
  private synchronized long getThreadId()
  {
    return ++threadCount;
  }

  /*
  ** private void run() throws Exception
  **
  ** Application main body.
  */
  private void run() throws Exception
  {
    // load application config info, open log file
    try
    {
      loadProperties();
      openLogFile();
    }
    catch (Exception e)
    {
      throw new Exception("Failure loading properties: " + e);
    }

    // setup ssl handling
    try
    {
      //String handlers = System.getProperty("java.protocol.handler.pkgs");
      //System.setProperty("java.protocol.handler.pkgs",
      //                    "com.sun.net.ssl.internal.www.protocol|" + handlers);
      //java.security.Security.addProvider(new Provider());
    }
    catch (Exception e)
    {
      throw new Exception("Failure during setup of ssl handling: " + e);
    }
    
    // create server socket to listen for redirection requests
    //ServerSocket serverSocket = new ServerSocket(7004);
    ServerSocket serverSocket = null;
    try
    {
      char password[] = "password".toCharArray();
      KeyStore keyStore = KeyStore.getInstance("JKS");
      keyStore.load(new FileInputStream("serverkeys"),password);
      KeyManagerFactory keyMgrFactory = KeyManagerFactory.getInstance("SunX509");
      keyMgrFactory.init(keyStore,password);

      SSLContext context = SSLContext.getInstance("SSLv3");
      context.init(keyMgrFactory.getKeyManagers(),null,null);
      ServerSocketFactory factory = context.getServerSocketFactory();
      serverSocket = factory.createServerSocket(dumperPort);
    }
    catch (Exception e)
    {
      throw new Exception("Failure creating server socket: " + e);
    }
    
    // start monitor thread
    (new MonitorThread()).start();

    System.out.println("RequestDumper running on port " + dumperPort);

    // accept redirection requests
    boolean listening = true;
    while (listening)
    {
      try
      {
        Thread senderThread = new RequestReceiver(serverSocket.accept());
        senderThread.start();
        threads.add(senderThread);
      }
      catch (Exception e)
      {
        System.out.println("Error accepting serverSocket: " + e);
      }
    }

    serverSocket.close();
  }
  
  /*
  ** public static void main(String[] args)
  **
  ** Main entry point for execution.
  */
  public static void main(String[] args)
  {
    try
    {
      (new RequestDumper()).run();
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
  }
}
