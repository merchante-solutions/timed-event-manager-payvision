package com.mesapps.redir;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.net.ssl.*;

public class RedirectorSender extends Thread
{
  private static final int  CLIENT_TIMEOUT_MS = 10000;
  
  private Redirector        redir             = null;
  private Socket            socket            = null;
  private BufferedReader    srcBr             = null;
  private OutputStream      srcOs             = null;
  private HttpURLConnection destCon           = null;
  
  private String            targetUrl         = null;
  private String            contentType       = null;
  private StringBuffer      postData          = null;
  private StringBuffer      postResponse      = null;
    
  private ConnectData       conData           = new ConnectData();
  private long              id                = 0L;
  
  private boolean           ioClosed          = false;

  private static long       threadCount       = 0;

  /*
  ** private class NullHostnameVerifier implements com.sun.net.ssl.HostnameVerifier
  **
  ** This will be used to override the default behavior in ssl connections and
  ** allow connections where the certificate hostname doesn't match it's host's
  ** name.
  */
  private class NullHostnameVerifier implements HostnameVerifier
  {
    public boolean verify(String urlName, SSLSession session)
    {
      return true;
    }
  }

  /*
  ** public MessageSender(Socket socket)
  **
  ** Constructor.
  **
  ** Assigns an id to self and stores ref to client socket.
  */
  public RedirectorSender(Redirector redir, Socket socket)
  {
    this.redir = redir;
    this.socket = socket;
    this.id = ++threadCount;
  }

  /*
  ** public long getIdleTime()
  **
  ** RETURNS: how long in milliseconds the thread has been idle.
  */
  public long getIdleTime()
  {
    return conData.getIdleTime();
  }
  
  /*
  ** public ConnectData getConnectData()
  **
  ** RETURNS: reference to internal ConnectData object.
  */
  public ConnectData getConnectData()
  {
    return conData;
  }
  
  /*
  ** public long getId()
  **
  ** RETURNS: id assigned to this thread.
  */
  public long getId()
  {
    return id;
  }
  
  /*
  ** private void getRequestData() throws Exception
  **
  ** Opens some io streams, configures the socket connection to the requesting
  ** client to timeout, reads the target url and content type from the client.
  */
  private void getRequestData() throws Exception
  {
    // get some io streams to the originator socket
    conData.logEvent("getting io streams");
    socket.setSoTimeout(CLIENT_TIMEOUT_MS);
    srcBr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    conData.logEvent("got io streams ok");

    // get the target url and content type
    conData.logEvent("getting targetUrl and contentType");
    targetUrl = srcBr.readLine();
    contentType = srcBr.readLine();
    conData.logEvent("got targetUrl and contentType");

    // get the post data
    conData.logEvent("reading post data");
    postData = new StringBuffer();
    String inputData;
    while ((inputData = srcBr.readLine()) != null && 
            !inputData.equals(Redirector.END_POST_DATA))
    {
      postData.append(inputData);
    }
    conData.logEvent("read post data ok").setData(postData.toString());
    
  }
  
  /*
  ** private HttpURLConnection getConnection()
  ** 
  ** Opens an http or https connection to the host specified by urlString.
  **
  ** RETURNS: HttpURLConnection reference.
  */
  private HttpURLConnection getConnection()
  {
    java.net.HttpURLConnection con = null;

    try
    {
      // create URL object for the URL string
      URL url = new URL(targetUrl);
  
      // get a connection object open on the url object
      conData.logEvent("connecting to target",targetUrl);
      con = (HttpURLConnection)url.openConnection();
      if (con instanceof HttpsURLConnection)
      {
        conData.logEvent("ssl connection opened");
        ((HttpsURLConnection)con).
          setHostnameVerifier(new NullHostnameVerifier());
      }
      else
      {
        conData.logEvent("connection opened");
      }
  
      // configure the connection for input, output and to do an http post
      con.setRequestMethod("POST");
      con.setDoOutput(true);
      con.setUseCaches(false);
      con.setRequestProperty("Content-Type",contentType);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".getConnection, " + e.toString());
      conData.logEvent("failed to open connection",e.toString());
    }
  
    return con;
  }
  
  /*
  ** private void sendRequestToTarget() throws Exception
  **
  ** Opens connection to target, posts request data to it.
  */
  private void sendRequestToTarget() throws Exception
  {
    redir.log("\n*** SENDING REQUEST ***\n");
    redir.log("targetUrl:   " + targetUrl);
    redir.log("contentType: " + contentType);
    redir.log("postData:    " + postData);
    
    // open connection to target
    destCon = getConnection();
    if (destCon != null)
    {
      try
      {
        conData.logEvent("sending post data");
        redir.log("about to send post data...");
        PrintWriter pw = new PrintWriter(destCon.getOutputStream());
        pw.println(postData.toString());
        pw.flush();
        pw.close();
        conData.logEvent("sent post data ok");
        redir.log("post data sent.");
      }
      catch (Exception e)
      {
        throw new Exception("Failed to send request: " + e.toString());
      }
    }
    else
    {
      throw new Exception("Failed to open connection to " + targetUrl);
    }

    redir.log("\n*** REQUEST SUCCESSFULLY SENT ***\n");
  }
  
  /*
  ** private void receiveResponseFromTarget() throws Exception
  **
  ** Receives a response from the target, the response is placed into 
  ** postResponse.
  */
  private void receiveResponseFromTarget() throws Exception
  {
    redir.log("\n*** RECEIVING RESPONSE ***\n");
    
    postResponse = new StringBuffer();
    try
    {
      // receive response
      conData.logEvent("receiving post response");
      System.out.println("Response code: " + destCon.getResponseCode());
      InputStream is = destCon.getInputStream();
      int inChar;
      while ((inChar = is.read()) != -1)
      {
        postResponse.append((char)inChar);
      }
      redir.log("response: " + postResponse);
      conData.logEvent("received post response",postResponse.toString());
    }
    catch (Exception e)
    {
      throw new Exception("Failed to receive response: " + e.toString());
    }
    
    redir.log("\n*** RESPONSE SUCCESSFULLY RECEIVED ***\n");
  }
  
  /*
  ** private void sendResponseToClient() throws Exception
  **
  ** Sends contents of postResponse to client.
  */
  private void sendResponseToClient() throws Exception
  {
    srcOs = socket.getOutputStream();

    // send the post response back to the originator
    conData.logEvent("forwarding post response");
    srcOs.write(postResponse.toString().getBytes(),0,postResponse.length());
    conData.logEvent("forwarded post response");
  }
  
  /*
  ** public void closeIo()
  **
  ** Attempts to close io io objects that may be open, sets ioClosed to true.
  */
  public void closeIo()
  {
    if (!ioClosed)
    {
      try { srcBr.close(); }        catch (Exception e) {}
      try { srcOs.close(); }        catch (Exception e) {}
      try { socket.close(); }       catch (Exception e) {}
      try { destCon.disconnect(); } catch (Exception e) {}
      ioClosed = true;
    }
  }
    
  /*
  ** public void run()
  **
  ** Given a socket, this needs to establish an HttpURLConnection with the target
  ** host, post the data, get the response from the post and return it via the
  ** socket to the originating server.
  */
  public void run()
  {
    try
    {
      getRequestData();
      sendRequestToTarget();
      receiveResponseFromTarget();
      sendResponseToClient();
      conData.logEvent("finished");
    }
    catch (Exception e)
    {
      conData.logEvent("exception thrown",e.toString());
      System.out.println(this.getClass().getName() + 
        ".run, " + e.toString());
      System.out.println("" + conData);
    }
    finally
    {
      closeIo();
      //redir.log(conData.toString());
    }
  }
}
  
