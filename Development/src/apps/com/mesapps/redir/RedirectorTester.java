package com.mesapps.redir;

import java.io.*;
import java.net.*;
import java.util.Properties;

public class RedirectorTester
{
  private String  redirectorHost;
  private int     redirectorPort;
  private String  targetUrl;
  private String  contentType;
  private int     connections;
  
  private void loadProperties() throws Exception
  {
    // load properties
    Properties properties = new Properties();
    FileInputStream in = new FileInputStream("redirectortester.properties");
    properties.load(in);
    in.close();
    
    // extract properties
    redirectorHost  = properties.getProperty("redirector.host");
    redirectorPort  = Integer.parseInt(properties.getProperty("redirector.port"));
    targetUrl       = properties.getProperty("targeturl");
    contentType     = properties.getProperty("contenttype");
    connections     = Integer.parseInt(properties.getProperty("connections"));
  }
  
  private void run() throws Exception
  {
    // load config
    loadProperties();
    
    // create connections
    System.out.println("host: " + redirectorHost + " port: " + redirectorPort);
    Socket[] rSockets = new Socket[connections];
    for (int i = 0; i < connections; ++i)
    {
      rSockets[i] = new Socket(redirectorHost,redirectorPort);
    }
  
    // send target url and content type
    for (int i = 0; i < connections; ++i)
    {
      PrintWriter rpw = new PrintWriter(rSockets[i].getOutputStream(),true);

      // send the target url and content type
      rpw.println(targetUrl);
      rpw.println(contentType);
    }
    
    // send message data
    for (int i = 0; i < connections; ++i)
    {
      PrintWriter rpw = new PrintWriter(rSockets[i].getOutputStream(),true);
      rpw.println("dummy data");
      rpw.println("end_post_data");
      System.out.println("message sent on connection " + i);
    }
  
    // receive responses
    for (int i = 0; i < connections; ++i)
    {
      BufferedReader br = 
        new BufferedReader(new InputStreamReader(rSockets[i].getInputStream()));
      String line = null;
      System.out.println("response received:");
      while ((line = br.readLine()) != null)
      {
        System.out.println(line);
      }
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      (new RedirectorTester()).run();
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
  }
}