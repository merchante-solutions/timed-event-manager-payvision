package com.mesapps.redir;

import java.io.*;
import java.net.*;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Message
{
  static Logger log = Logger.getLogger(Message.class);

  private String  redirectorHost;
  private int     redirectorPort;
  private String  contentType;

  private String  targetUrl;
  private String  data;
  
  public Message()
  {
    loadProperties();
  }
  public Message(String targetUrl, String contentType)
  {
    loadProperties();
    this.targetUrl = targetUrl;
    this.contentType = contentType;
  }

  private void loadProperties()
  {
    FileInputStream in = null;

    try
    {
      // load properties
      Properties properties = new Properties();
      in = new FileInputStream("message.properties");
      properties.load(in);
    
      // extract properties
      redirectorHost  = properties.getProperty("redirector.host");
      redirectorPort  = Integer.parseInt(properties.getProperty("redirector.port"));
      contentType     = properties.getProperty("default.contenttype");
    }
    catch (Exception e)
    {
      String errorMsg = "Error loading properties: " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
  
  public void setTargetUrl(String targetUrl)
  {
    this.targetUrl = targetUrl;
  }
  public String getTargetUrl()
  {
    return targetUrl;
  }

  public void setContentType(String contentType)
  {
    this.contentType = contentType;
  }
  public String getContentType()
  {
    return contentType;
  }

  public String send(String data)
  {
    PrintWriter out = null;
    BufferedReader in = null;
    String responseData = null;

    try
    {
      Socket socket = new Socket(redirectorHost,redirectorPort);

      out = new PrintWriter(socket.getOutputStream(),true);
      out.println(targetUrl);
      out.println(contentType);
      out.println(data);
      out.println(Redirector.END_POST_DATA);

      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      responseData = in.readLine();
    }
    catch (Exception e)
    {
      String errorMsg = "Failed to send message: " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    finally
    {
      try { out.close(); } catch (Exception e) { }
      try { in.close(); } catch (Exception e) { }
    }

    return responseData;
  }
  
  public static String send(String targetUrl, String contentType, String data)
  {
    Message message = new Message(targetUrl,contentType);
    return message.send(data);
  }
}
