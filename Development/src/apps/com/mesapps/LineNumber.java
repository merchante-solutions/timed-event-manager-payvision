package com.mesapps;
import java.io.*;

public class LineNumber
{
  public LineNumber()
  {
  }
  
  private void number(String filename)
  {
    try
    {
      BufferedReader in = new BufferedReader(new FileReader(filename));
      FileOutputStream out = new FileOutputStream(filename+".line");
      
      String buf = in.readLine();
      
      int counter = 0;
      while(buf != null)
      {
        out.write(new String(counter + " " + buf + "\n").getBytes());
        ++counter;
        buf = in.readLine();
      }
      
      out.close();
      in.close();
    }
    catch(Exception e)
    {
      System.out.println("number(" + filename + "): " + e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    if(args.length == 1)
    {
      LineNumber ln = new LineNumber();
      
      ln.number(args[0]);
    }
    else
    {
      System.out.println("USAGE: LineNumber <filename>");
    }
  }
}