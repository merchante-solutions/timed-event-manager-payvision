/*************************************************************************

  FILE: $Archive: /Java/apps/events/TimedEventThread.java $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/30/02 11:46a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

import java.lang.Thread;

import com.mes.support.HttpHelper;
import com.mes.startup.TimedEventManager;
import com.mes.database.SQLJConnectionBase;

public class TimedEventThread
{
  public static void main(String args[])
  {
    try
    {
      String hostName = HttpHelper.getServerName(null);
      
      // configure SQLJConnectionBase to always get direct connections
      SQLJConnectionBase.setConnectString(SQLJConnectionBase.PROD_DIRECT_CONNECTION_STRING);
      
      // instantiate a TimedEventManager object that will run in standalone mode
      //@System.out.println("Instantiating TimedEventManager");
      TimedEventManager manager = new TimedEventManager(true);
      
      // instantiate a Thread object which will encapsulate the TimedEventManager
      //@System.out.println("Instantiating TimedEventThread");
      Thread managerThread = new Thread(manager, "TimedEventThread");
      
      manager.setThreadWrapper(managerThread);
      
      System.out.println("\nStarting " + hostName);
      managerThread.start();
      
      while(managerThread.isAlive())
      {
        //@System.out.println("Still alive, sleeping for 30 seconds");
        Thread.sleep(30000);
      }
    }
    catch(Exception e)
    {
      System.out.println("Error: " + e.toString());
    }
    finally
    {
    }
  }
}
