/*************************************************************************

  FILE: $Archive: /Java/apps/Server/Service.java $

  Description:
  
    Service
    
    Abstract base class for Thread-based service classes.
    
    Each service starts it's own CommandPortListener.  The listener object
    monitors a designated command port and upon receipt of a command causes
    a command request to be made to the service by the listener thread 
    via various waitRequest() wrapper methods.  Once the service has 
    processed the command, acknowledgeRequest() is called to notify the 
    listener that the command was processed.
  
    All Service child classes must implement several abtract methods: 
    serviceStart(), serviceRun() and service Stop().  serviceStart() is
    called once when the service is first started.  serviceRun() is called
    repeatedly throughout the life of the service (unless the service is
    suspended) and should perform the actual service tasks.  serviceStop()
    is called when the service is shutdown and provides an oppurtunity for
    cleanup, etc. in the subclass.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/18/01 4:34p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;
import java.util.Properties;

public abstract class Service extends Thread {
	// service request types
	private final static int RQ_SHUTDOWN				= 1;
	private final static int RQ_STATUS					= 2;
	private final static int RQ_SUSPEND					= 3;
	private final static int RQ_RESUME					= 4;

	// service request processing state
	private final static int RQ_ST_IDLE					= 0;
	private final static int RQ_ST_STATUS_REQUESTED		= 1;
	private final static int RQ_ST_SUSPEND_REQUESTED	= 2;
	private final static int RQ_ST_AWAITING_SUSPEND		= 3;
	private final static int RQ_ST_RESUME_REQUESTED		= 4;
	private final static int RQ_ST_AWAITING_RESUME		= 5;
	private final static int RQ_ST_SHUTDOWN_REQUESTED	= 6;
	private final static int RQ_ST_AWAITING_SHUTDOWN	= 7;
	private int requestState = RQ_ST_IDLE;
	private String requestResults;

	// service states
	private final static int SRVC_ST_START_REQUESTED	= 0;
	private final static int SRVC_ST_RUNNING			= 1;
	private final static int SRVC_ST_STOP_REQUESTED		= 2;
	private final static int SRVC_ST_STOPPED			= 3;

	protected int serviceState = SRVC_ST_START_REQUESTED;

	// service command port listener
	private CommandPortListener commandListener = null;

	// thread running flag
	private boolean running = false;

	abstract boolean serviceStart();
	abstract void serviceStop();
	abstract void serviceRun();

	public Service(int port) {
		commandListener = new CommandPortListener(port, this);
		commandListener.start();
	}

	public String toString() {
		return getClass().getName() + 
			" Service (" + super.toString() + ")";
	}

	/*
	** waitRequest() and each of the specific request entry
	** functions are called from another thread to make
	** requests to this service thread.  waitRequest()
	** performs a wait() at the end that pauses requesting
	** thread until the service thread does a notify() in
	** acknowledgeRequest().  The service's own commandListener
	** thread calls these functions after having successfully
	** parsed requests received on the command port.
	*/

	private final static int FC_OK = 0;
	private final static int FC_BUSY = 1;
	private final static int FC_INVALID = 2;
	private synchronized String waitRequest(int request) {
		int failureCode = FC_OK;

		switch (request) {
			case RQ_SHUTDOWN:
				if (requestState != RQ_ST_IDLE) {
					failureCode = FC_BUSY;
				} else {
					requestState = RQ_ST_SHUTDOWN_REQUESTED;
				}
				break;

			case RQ_SUSPEND:
				if (serviceState != SRVC_ST_RUNNING) {
					failureCode = FC_INVALID;
				} else if (requestState != RQ_ST_IDLE) {
					failureCode = FC_BUSY;
				} else {
					requestState = RQ_ST_SUSPEND_REQUESTED;
				}
				break;

			case RQ_RESUME:
				if (requestState != RQ_ST_IDLE || 
					serviceState != SRVC_ST_STOPPED) {
					failureCode = FC_INVALID;
				} else {
					requestState = RQ_ST_RESUME_REQUESTED;
				}
				break;

			case RQ_STATUS:
				if (requestState != RQ_ST_IDLE) {
					failureCode = FC_BUSY;
				} else {
					requestState = RQ_ST_STATUS_REQUESTED;
				}
				break;
			
			default:
				failureCode = FC_INVALID;
				break;
		}

		switch (failureCode) {
			case FC_OK:
				break;

			case FC_BUSY:
				return "Service busy.";

			default:
				return "Invalid request.";
		}

		try {
			wait();
		} catch(Exception e) {
			System.err.println(
				"Exception thrown waiting for listener thread to notify.");
			System.err.println("Request type: " + request);
		}

		return requestResults;
	}

	public String getStatus() {
		return waitRequest(RQ_STATUS);
	}

	public String startService() {
		start();
		return getStatus();
	}

	public String endService() {
		return waitRequest(RQ_SHUTDOWN);
	}

	public String suspendService() {
		return waitRequest(RQ_SUSPEND);
	}

	public String resumeService() {
		return waitRequest(RQ_RESUME);
	}

	private void checkServiceState() {
		switch (serviceState)
		{
			case SRVC_ST_START_REQUESTED:
				if (serviceStart()) {
					serviceState = SRVC_ST_RUNNING;
				} else {
					serviceState = SRVC_ST_STOPPED;
				}
				break;

			case SRVC_ST_STOP_REQUESTED:
				serviceStop();
				serviceState = SRVC_ST_STOPPED;
				break;

			case SRVC_ST_RUNNING:
				serviceRun();
				break;

			case SRVC_ST_STOPPED:
			default:
				try {
					sleep(500);
				} catch (InterruptedException ie) { }
				break;
		}
	}

	private String getStatusString() {
		String status;
		switch (serviceState) {
			case SRVC_ST_START_REQUESTED:
				status = toString() + " starting...";
				break;

			case SRVC_ST_RUNNING:
				status = toString() + " running.";
				break;

			case SRVC_ST_STOP_REQUESTED:
				status = toString() + " stopping...";
				break;

			case SRVC_ST_STOPPED:
				status = toString() + " stopped.";
				break;

			default:
				status = toString() + " in unknown state! (" + 
						serviceState + ")";
				break;
		}

		return status;
	}

	/*
	** acknowledgeRequest() is called by the service thread
	** when it has completed a detected request from another
	** thread.  It sets the requestResults string to show
	** the status of the service and then does a notify() to
	** signal the requesting thread that the request has been
	** processed.
	*/

	private synchronized void acknowledgeRequest() {
		// set the status string
		requestResults = getStatusString();

		// notify all waiting threads
		try {
			notifyAll();
		} catch (Exception e) {
			System.err.println(
				"Exception thrown notifying waiting thread.");
		}
	}

	private void checkRequestState() {
		switch (requestState) {
			// status request
			case RQ_ST_STATUS_REQUESTED:
				System.out.println("Status requested.");
				requestState = RQ_ST_IDLE;
				acknowledgeRequest();
				break;

			// service suspend requested
			case RQ_ST_SUSPEND_REQUESTED:
				System.out.println("Suspend requested.");
				if (serviceState == SRVC_ST_RUNNING) {
					serviceState = SRVC_ST_STOP_REQUESTED;
					requestState = RQ_ST_AWAITING_SUSPEND;
				} else {
					requestState = RQ_ST_IDLE;
					acknowledgeRequest();
				}
				break;

			case RQ_ST_AWAITING_SUSPEND:
				if (serviceState == SRVC_ST_STOPPED) {
					requestState = RQ_ST_IDLE;
					acknowledgeRequest();
				} else { /* do timing checks here */ }
				break;

			// service resume requested
			case RQ_ST_RESUME_REQUESTED:
				System.out.println("Resume requested.");
				if (serviceState == SRVC_ST_STOPPED) {
					serviceState = SRVC_ST_START_REQUESTED;
					requestState = RQ_ST_AWAITING_RESUME;
				} else {
					requestState = RQ_ST_IDLE;
					acknowledgeRequest();
				}
				break;

			case RQ_ST_AWAITING_RESUME:
				if (serviceState == SRVC_ST_RUNNING) {
					requestState = RQ_ST_IDLE;
					acknowledgeRequest();
				} else { /* do timing checks here */ }
				break;
			
			// shutdown requested
			case RQ_ST_SHUTDOWN_REQUESTED:
				System.out.println("Shutdown requested.");
				serviceState = SRVC_ST_STOP_REQUESTED;
				requestState = RQ_ST_AWAITING_SHUTDOWN;
				break;

			case RQ_ST_AWAITING_SHUTDOWN:
				if (serviceState == SRVC_ST_STOPPED) {
					acknowledgeRequest();
					requestState = RQ_ST_IDLE;
					running = false;
				} else { /* do timing checks here */ }
				break;

			// no requests to process
			case RQ_ST_IDLE:
			default:
				break;
		}
	}

  /*
  ** public void run()
  **
  ** Main execution loop of the service thread.  Until running flag is turned
  ** off this routine repeatedly cycles through checking the current service
  ** state (and reacting accordingly) and checking for any new requests that
  ** may have arrived on the command port.
  */
	public void run() {
		running = true;
		while (running) {
			checkServiceState();
			checkRequestState();
		}

		if (commandListener.isAlive()) {
			commandListener.stopRunning();
		}

		running = false;
	}
}
