/*************************************************************************

  FILE: $Archive: /Java/apps/Server/CommandPortListener.java $

  Description:
  
    CommandPortListener
    
    This Thread based class works in conjunction with Service based
    classes.  It monitors a specified port for connection requests.
    Once connected the client may send various Service command requests.
    Only one command connection at a time is allowed per service.  Commands
    understood by this class are status, suspend, resume, shutdown and exit.
    Commands received cause the corresponding service routines to be run.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/18/01 4:34p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;
import java.util.Properties;

public class CommandPortListener extends Thread {
	private final int STATE_DONE				= -1;
	private final int STATE_NEW					= 0;
	private final int STATE_RECEIVE_COMMAND		= 1;
	private final int STATE_COMMAND_RECEIVED	= 2;
	private final int STATE_END_CONNECTION		= 3;

	private int commandPortNumber = 23;
	private boolean running = false;
	private Service myService = null;

	public CommandPortListener(int portNumber, Service service) {
		commandPortNumber = portNumber;
		myService = service;
	}

	private void commandConsole(Socket socket) {
		PrintWriter out = null;
		BufferedReader in = null;
		String command = null;
		String commandResponse = null;
		int currentState = STATE_NEW;

		// set up socket input and output streams 
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
			currentState = STATE_RECEIVE_COMMAND;
		} catch(IOException ie) {
			System.err.println(
				"Failed to open in/out stream on client socket.");
			currentState = STATE_END_CONNECTION;
		}

		// notify connection established
		out.println("Ready for commands.");
		out.write('>');
		out.flush();
		while (running && currentState != STATE_DONE) {
			switch (currentState) {
				case STATE_NEW:
				case STATE_RECEIVE_COMMAND:
					try {
						if (in.ready()) {
							command = in.readLine();
							if (command != null) {
								currentState = STATE_COMMAND_RECEIVED;
							}
						} else {
							yield();
						}
					} catch (IOException ie) { 
						System.err.println("Failed to read command.");
					}

					// do idle timeout checking here
					break;

				case STATE_COMMAND_RECEIVED:
					if (command != null) {
						if (command.equalsIgnoreCase("status")) {
							commandResponse = myService.getStatus();
							currentState = STATE_RECEIVE_COMMAND;
						} else if (command.equalsIgnoreCase("suspend")) {
							commandResponse = myService.suspendService();
							currentState = STATE_RECEIVE_COMMAND;
						} else if (command.equalsIgnoreCase("resume")) {
							commandResponse = myService.resumeService();
							currentState = STATE_RECEIVE_COMMAND;
						} else if (command.equalsIgnoreCase("shutdown")) {
							commandResponse = myService.endService();
							currentState = STATE_END_CONNECTION;
						} else if (command.equalsIgnoreCase("exit")) {
							commandResponse = "Ending connection...";
							currentState = STATE_END_CONNECTION;
						} else {
							commandResponse = "Invalid command.";
							currentState = STATE_RECEIVE_COMMAND;
						}
					}
					out.println(commandResponse);
					out.write('>');
					out.flush();
					break;

				case STATE_END_CONNECTION:
					try {
						in.close();
						out.close();
						socket.close();
					} catch (IOException ie) { }
					currentState = STATE_DONE;
					break;

				case STATE_DONE:
				default:
					break;
			}
		}
	}

	public void run() {
		ServerSocket listenerSocket = null;

		try {
			listenerSocket = new ServerSocket(commandPortNumber);
			listenerSocket.setSoTimeout(500);
			running = true;
		} catch (IOException e) {
			System.err.println(
			"IOException creating server socket on " + 
			commandPortNumber + ".");
		}

		while (running)
		{
			try {
				commandConsole(listenerSocket.accept());
			} catch (IOException e) { }
		}
	}

	public void stopRunning() {
		running = false;
	}
}
