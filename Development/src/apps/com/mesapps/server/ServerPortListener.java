/*************************************************************************

  FILE: $Archive: /Java/apps/Server/ServerPortListener.java $

  Description:
  
    ServerPortListener
    
    This port listener is given a port number to monitor at creation.
    When receiving a request for connection it creates an Fsm based
    object and places it on the execute queue.  The PortServer service
    may then pop the connection handlers off the queue and execute them.
    
    NOTE: this class currently is hard-coded to create HttpFsm connection
    objects.  Eventually this class should allow the Fsm type to be
    specified as a configurable option.
        
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/19/04 3:16p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;
import java.util.Properties;

public class ServerPortListener extends Thread {
  private Properties appProps = null;
  private ServerSocket listenerSocket = null;
  private Queue executeQueue = null;
  private int listenerPortNumber = 6666;
  private boolean running = true;

  public ServerPortListener(int portNumber, Queue queue) {
    listenerPortNumber = portNumber;
    executeQueue = queue;
  }

  public void run() {
    try {
      listenerSocket = new ServerSocket(listenerPortNumber);
      listenerSocket.setSoTimeout(500);
    } catch (IOException e) {
      System.err.println(
        "IOException creating server socket on " + 
        listenerPortNumber + ".");
    }

    while (running)
    {
      try {
        executeQueue.push(new HttpFsm(listenerSocket.accept()));
      } catch (IOException e) { }
    }

    try {
      listenerSocket.close();
    } catch (IOException e) {
      System.err.println(
        "IOException closing server socket on " + 
        listenerPortNumber + ".");
    }
  }

  public void stopRunning() {
    running = false;
  }
}
