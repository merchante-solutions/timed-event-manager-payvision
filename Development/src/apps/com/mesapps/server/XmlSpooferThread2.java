package com.mesapps.server;

import java.net.*;
import java.io.*;

import org.jdom.*;
import org.jdom.output.*;
import org.jdom.input.*;

public class XmlSpooferThread2 extends Thread
{
  private Socket socket = null;

  public static final int     ST_ALL_GOOD           = 1;
  public static final int     ST_COMM_GOOD_RESP_2   = 2;
  public static final int     ST_COMM_GOOD_RESP_3   = 3;
  public static final int     ST_COMM_GOOD_RESP_4   = 4;
  public static final int     ST_COMM_GOOD_RESP_5   = 5;
  public static final int     ST_COMM_GOOD_RESP_6   = 6;
  public static final int     ST_COMM_GOOD_RESP_7   = 7;
  public static final int     ST_COMM_GOOD_RESP_8   = 8;
  public static final int     ST_COMM_GOOD_RESP_9   = 9;
  public static final int     ST_COMM_DROP          = 10;
  public static final int     ST_COMM_TIMEOUT       = 11;
  public static final int     ST_COMM_IGNORE        = 12;
  
  private static final int    CR_OK                 = 1;
  private static final int    CR_DROP               = 2;
  private static final int    CR_TIMEOUT            = 3;
  private static final int    CR_IGNORE             = 4;


  private static final int    RR_OK                 = 1;  
  private static final int    RR_2                  = 2;
  private static final int    RR_3                  = 3;
  private static final int    RR_4                  = 4;
  private static final int    RR_5                  = 5;
  private static final int    RR_6                  = 6;
  private static final int    RR_7                  = 7;
  private static final int    RR_8                  = 8;
  private static final int    RR_9                  = 9;
    
  private int     spoofType   = ST_ALL_GOOD;
  private int     commResult  = CR_OK;
  private int     respResult  = RR_OK;
  private String  respMsgs[]  =
  {
    "",
    "Success",
    "Error type 2",
    "Error type 3",
    "Error type 4",
    "Error type 5",
    "Error type 6",
    "Error type 7",
    "Error type 8",
    "Error type 9"
  };

  public XmlSpooferThread2(Socket socket,int spoofType)
  {
    super("XmlSpooferThread2");
    this.socket = socket;
    switch (spoofType)
    {
      case ST_ALL_GOOD:
        commResult = CR_OK;
        respResult = RR_OK;
        break;
        
      case ST_COMM_GOOD_RESP_2:
        commResult = CR_OK;
        respResult = RR_2;
        break;
        
      case ST_COMM_GOOD_RESP_3:
        commResult = CR_OK;
        respResult = RR_3;
        break;
        
      case ST_COMM_GOOD_RESP_4:
        commResult = CR_OK;
        respResult = RR_4;
        break;
        
      case ST_COMM_GOOD_RESP_5:
        commResult = CR_OK;
        respResult = RR_5;
        break;
        
      case ST_COMM_GOOD_RESP_6:
        commResult = CR_OK;
        respResult = RR_6;
        break;
        
      case ST_COMM_GOOD_RESP_7:
        commResult = CR_OK;
        respResult = RR_7;
        break;
        
      case ST_COMM_GOOD_RESP_8:
        commResult = CR_OK;
        respResult = RR_8;
        break;
        
      case ST_COMM_GOOD_RESP_9:
        commResult = CR_OK;
        respResult = RR_9;
        break;
        
      case ST_COMM_DROP:
        commResult = CR_DROP;
        respResult = RR_OK;
        break;
        
      case ST_COMM_TIMEOUT:
        commResult = CR_TIMEOUT;
        respResult = RR_OK;
        break;

      case ST_COMM_IGNORE:
        commResult = CR_IGNORE;
        respResult = RR_OK;
        break;
    }
        
    this.spoofType = spoofType;
  }
  
  private void log(String msg)
  {
    // implement logging here
    System.out.println("Log message: " + msg);
  }

  /*
  ** Given a socket this pretends to open a connection with a target url, post the
  ** data and get a response from the post.  In fact, it takes the data (assuming
  ** it's an xml doc) and generates a response to it.  Settings in the future will
  ** allow it to simulate various error conditions...like communications errors with
  ** the target server, or error responses in the returned xml response message.
  */
  public void run()
  {
    if (commResult != CR_IGNORE)
    {
      OutputStream socketos = null;
      BufferedReader socketbr = null;
      try
      {
        // get some io streams to the originator socket
        socketos = socket.getOutputStream();
        socketbr = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        // get the target url
        String targetUrl = socketbr.readLine();
        System.out.println("\n-- targetUrl: " + targetUrl);
      
        // get the post data
        StringBuffer postData = new StringBuffer();
        String inputData;
        while ((inputData = socketbr.readLine()) != null && 
                !inputData.equals("end_post_data"))
        {
          postData.append(inputData);
        }
        System.out.println("-- postData: " + postData);
        
        if (commResult != CR_DROP)
        {
          // load the post data into an xml document
          SAXBuilder builder = new SAXBuilder();
          Document doc = builder.build(new StringReader(postData.toString()));
      
          // modify the document to look like a response
      
          // get root and old body
          Element root = doc.getRootElement();
          Element oldBody = (Element)(root.getChildren().get(1));
      
          // create the new response body, add the old body childrent to it
          String oldBodyName = oldBody.getName();
          Element newBody = 
            new Element(oldBodyName.substring(0,oldBodyName.length() - 7) + "Response");
          newBody.setContent(oldBody.getContent());
      
          // add result code and msg text to the new body
          Element result = new Element("Result");
          Element msg = new Element("Msg");
          result.setText(Integer.toString(respResult));
          msg.setText(respMsgs[respResult]);
          newBody.addContent(result);
          newBody.addContent(msg);

          // add the new body, remove the old body
          root.removeContent(oldBody);
          root.addContent(newBody);
      
          // send the post response back to the originator
          XMLOutputter xmlOut = new XMLOutputter(Format.getRawFormat());
          xmlOut.output(doc,socketos);

          // echo the xml response doc       
          System.out.println("\n-- response:");
          xmlOut = new XMLOutputter(Format.getPrettyFormat());
          xmlOut.output(doc,System.out);
        }
        else
        {
          System.out.println("-- dropping connection");
        }
      }
      catch (Exception e)
      {
        log(e.toString());
        System.out.println(e.toString());
      }
      finally
      {
        try { socketbr.close(); } catch (Exception e) {}
        try { socketos.close(); } catch (Exception e) {}
        try { socket.close();   } catch (Exception e) {}
      }
    }
    else
    {
      System.out.println("-- ignoring connection request");
    }
  }
}
