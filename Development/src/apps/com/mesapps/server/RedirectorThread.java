package com.mesapps.server;

import java.net.*;
import java.io.*;

import com.sun.net.ssl.*;

public class RedirectorThread extends Thread
{
  private Socket socket = null;

  /*
  ** This will be used to override the default behavior in ssl connections and
  ** allow connections where the certificate hostname doesn't match it's host's
  ** name.
  */
  static class NullHostnameVerifier implements com.sun.net.ssl.HostnameVerifier
  {
    public boolean verify(String urlName, String certName)
    {
      return true;
    }
  }

  public RedirectorThread(Socket socket)
  {
    super("RedirectorThread");
    this.socket = socket;
  }
  
  private void log(String msg)
  {
    // implement logging here
    System.out.println("Log message: " + msg);
  }

  /*
  ** Opens an http or https connection to the host specified by urlString.
  */
  private HttpURLConnection getConnection(String urlString,String contentType)
  {
    java.net.HttpURLConnection con = null;

    try
    {
      // set up ssl handling
      String handlers = System.getProperty("java.protocol.handler.pkgs");
      System.setProperty("java.protocol.handler.pkgs",
                          "com.sun.net.ssl.internal.www.protocol|" + handlers);
      java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    
      // create URL object for the URL string
      URL url = new URL(urlString);
    
      // get a connection object open on the url object
      con = (java.net.HttpURLConnection)url.openConnection();
      if (con instanceof com.sun.net.ssl.HttpsURLConnection)
      {
        ((com.sun.net.ssl.HttpsURLConnection)con).
          setHostnameVerifier(new NullHostnameVerifier());
      }
    
      // configure the connection for input, output and to do an http post
      con.setDoOutput(true);
      con.setDoInput(true);
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type",contentType);
    }
    catch (Exception e)
    {
      log(e.toString());
      System.out.println(this.getClass().getName() + 
        ".getConnection, " + e.toString());
    }
    
    return con;
  }

  /*
  ** Given a socket, this needs to establish an HttpURLConnection with the target
  ** host, post the data, get the response from the post and return it via the
  ** socket to the originating server.
  */
  public void run()
  {
    OutputStream socketos = null;
    BufferedReader socketbr = null;
    try
    {
      // get some io streams to the originator socket
      socketos = socket.getOutputStream();
      socketbr = new BufferedReader(new InputStreamReader(socket.getInputStream()));

      // get the target url
      String targetUrl = socketbr.readLine();
      System.out.println("targetUrl: " + targetUrl);
      String contentType = socketbr.readLine();
      System.out.println("contentType: " + contentType);
      
      // get the post data
      StringBuffer postData = new StringBuffer();
      String inputData;
      while ((inputData = socketbr.readLine()) != null && !inputData.equals("end_post_data"))
      {
        postData.append(inputData);
      }
      System.out.println("postData: " + postData);
      
      // open a connection to the target and post the data
      HttpURLConnection con = getConnection(targetUrl,contentType);
      StringBuffer postResponse = new StringBuffer();
      if (con != null)
      {
        try
        {
          // post the data
          con.getOutputStream().write(postData.toString().getBytes(),0,postData.length());
          log("Request sent");
        }
        catch (Exception e)
        {
          throw new Exception("Failed to send request: " + e.toString());
        }
    
        try
        {
          // receive response
          InputStream is = con.getInputStream();
          int inChar;
          while ((inChar = is.read()) != -1)
          {
            postResponse.append((char)inChar);
          }
          log("Response received");
          //System.out.println(postResponse);
        }
        catch (Exception e)
        {
          throw new Exception("Failed to receive response: " + e.toString());
        }
      }
      
      // send the post response back to the originator
      socketos.write(postResponse.toString().getBytes(),0,postResponse.length());
    }
    catch (Exception e)
    {
      log(e.toString());
      System.out.println(e.toString());
    }
    finally
    {
      try { socketbr.close(); } catch (Exception e) {}
      try { socketos.close(); } catch (Exception e) {}
      try { socket.close();   } catch (Exception e) {}
    }
  }
}
