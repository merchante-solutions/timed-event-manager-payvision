/*************************************************************************

  FILE: $Archive: /Java/apps/Server/HttpFsm.java $

  Description:
  
    HttpFsm
    
    Each instance of this class is a finite-state machine that implements
    HTTP (or some crude semblance of it) over a socket connection.  On
    creation it is given an open socket connecting it to the client.
    Incoming data is processed and a response is returned.
    
    NOTE: currently this is being used to test XML messaging.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/11/01 6:32p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class HttpFsm extends Fsm {
  public final int        FSM_ST_PROCESS_HEADER_LINE  = 1;
  public final int        FSM_ST_PROCESS_BODY_LINE    = 2;
  public final int        FSM_ST_SEND_RESPONSE        = 3;
  public final int        FSM_ST_DISCONNECT           = 4;

  private Socket          socket                      = null;
  private PrintWriter     out                         = null;
  private BufferedReader  in                          = null;
  private PrintStream     so                          = System.out;
  
  private String          inLine                      = null;
  
  private StringBuffer    requestData                 = null;
  private int             contentLength               = 0;
  private int             receivedLength              = 0;

  public HttpFsm(Socket connectSocket) {
    socket = connectSocket;
    try {
      out = new PrintWriter(socket.getOutputStream(), true);
      in = new BufferedReader(
        new InputStreamReader(socket.getInputStream()));
    } catch(IOException ie) {
      System.err.println(
        "Failed to open in/out stream on client socket.");
      currentState = FSM_ST_DISCONNECT;
    }

    so.println("Connection established with client: " +
      socket.getPort());
  }
          
  public int getState() {
    return currentState;
  }

  public boolean isDone() {
    return currentState == FSM_ST_DONE;
  }
  
  private boolean inputReceived() {
    boolean getOk = false;
    try {
      if (in.ready()) {
        inLine = in.readLine();
        if (inLine != null) {
          so.println("inLine: " + inLine);
          getOk = true;
        }
        else
        {
          so.println("inLine is null");
        }
      }
    }
    catch (Exception e) { so.println("inputReceived exception"); }
    return getOk;
  }
  
  private void processHeaderLine() {
    if (inLine.indexOf("Content-Length") == 0) {
      int off = inLine.indexOf(':');
      contentLength = Integer.parseInt(inLine.substring(off + 2));
    }
  }
  
  private boolean endOfHeader() {
    return currentState == FSM_ST_PROCESS_HEADER_LINE &&
           inLine.equals("");
  }
  
  private void processBodyLine() {
    requestData.append(inLine + "\n");
    receivedLength += inLine.length() + 2;
  }
  
  private boolean endOfBody() {
    return currentState == FSM_ST_PROCESS_BODY_LINE &&
           receivedLength >= contentLength;
  }
  
  private void sendOutput(String output) {
    out.print(output);
    so.print(output);
  }
  
  private void sendOutputCRLF() {
    sendOutput("\n");
  }
  
  private void sendOutputLine(String output) {
    sendOutput(output);
    sendOutputCRLF();
  }
  
  private void sendResponse() {
    try
    {
      SAXBuilder builder = new SAXBuilder();
      Document doc = builder.build(new StringReader(requestData.toString()));
      doc.getRootElement().addContent(new Element("Result").setText("1"));
      XMLOutputter fmt = new XMLOutputter();
    
      so.println("*** Response ***");
      sendOutputLine("HTTP/1.0 200 OK");
      if (requestData != null) {
        StringWriter docBuffer = new StringWriter();
        fmt.output(doc,docBuffer);
        sendOutputLine("Content-Length: " + docBuffer.toString().length());
        sendOutputCRLF();
        sendOutput(docBuffer.toString());
      }
      else {
        sendOutputCRLF();
      }
    }
    catch (Exception e)
    {
      so.println("");
      so.println("sendResponse(), Error sending response: " + e.toString());
      e.printStackTrace(so);
    }
  }
    
  public void doState() {
    try {
       switch (currentState) {
        case FSM_ST_NEW:
          so.println("*** Request ***");
          currentState = FSM_ST_PROCESS_HEADER_LINE;
          
        case FSM_ST_PROCESS_HEADER_LINE:
          if (inputReceived()) {
            processHeaderLine();
            if (endOfHeader()) {
              if (contentLength > 0) {
                requestData = new StringBuffer();
                currentState = FSM_ST_PROCESS_BODY_LINE;
              }
              else {
                currentState = FSM_ST_SEND_RESPONSE;
              }
            }
          }
          break;
            
        case FSM_ST_PROCESS_BODY_LINE:
          if (inputReceived()) {
            processBodyLine();
            if (endOfBody()) {
              currentState = FSM_ST_SEND_RESPONSE;
            }
          }
          break;

        case FSM_ST_SEND_RESPONSE:
          sendResponse();
          currentState = FSM_ST_DISCONNECT;
          break;

        case FSM_ST_DISCONNECT:
          out.flush();
          out.close();
          in.close();
          socket.close();
          so.println("*** Disconnected ***");
          currentState = FSM_ST_DONE;
          break;

        case FSM_ST_DONE:
        default:
          break;
      }
    } catch (IOException ie) {
      System.err.println("IOException in doState().");
      currentState = FSM_ST_DISCONNECT;
    }
  }

  public void kill() {
    try {
      in.close();
      out.close();
      socket.close();
    } catch (IOException e) {
      System.err.println("IOException in kill().");
    }
      
    currentState = FSM_ST_DONE;
  }
}
