/*************************************************************************

  FILE: $Archive: /Java/apps/Server/Redirector.java $

  Description:  
  
    Redirector
    
    A quick and dirty server that sits on a port and forwards requests
    sent to it and returns the results to the requesting server.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/17/01 5:40p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;
import java.util.Properties;

public class Redirector2
{
	private static Properties properties = null;
  
	private static void loadProperties() throws IOException
  {
		// create and load current application properties
		properties = new Properties();
		FileInputStream in = new FileInputStream("redirector.properties");
		properties.load(in);
		in.close();
	}
  
  public static void main(String[] args) throws IOException
  {
    ServerSocket serverSocket = null;
    boolean listening = true;
    
    loadProperties();
    int redirectorPort = Integer.parseInt(properties.getProperty("redirector.port"));

    try
    {
      serverSocket = new ServerSocket(redirectorPort);
    }
    catch (IOException e)
    {
      System.out.println("Could not listen on port " + redirectorPort);
      System.exit(-1);
    }

    System.out.println("Redirector running on port " + redirectorPort);

    while (listening)
    {
      new RedirectorThread(serverSocket.accept()).start();
    }

    serverSocket.close();
  }
}
