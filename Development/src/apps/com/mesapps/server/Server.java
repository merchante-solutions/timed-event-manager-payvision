/*************************************************************************

  FILE: $Archive: /Java/apps/Server/Server.java $

  Description:  
  
    Server
    
    Server application.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/19/04 3:16p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.io.*;
import java.util.Properties;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class Server {
  public static Properties appProps = null;

  private static void loadProperties() throws IOException {

    // load the default properties
    Properties defaultProps = new Properties();
    FileInputStream in = new FileInputStream("default.props");
    defaultProps.load(in);
    in.close();

    // create and load current application properties
    appProps = new Properties(defaultProps);
    in = new FileInputStream("app.props");
    appProps.load(in);
    in.close();
  }

  private static void saveProperties() throws IOException {

    // save the current
    FileOutputStream out = new FileOutputStream("app.props");
    appProps.store(out,"Application Settings");
    out.close();
  }

  public static void main(String[] args) throws IOException {

    // create some input and output handles
    PrintWriter sysOut = new PrintWriter(System.out, true);
    BufferedReader sysIn = new BufferedReader(
      new InputStreamReader(System.in));

    // load server properties
    sysOut.println("Loading server properties...");
    loadProperties();

    // start server services
    sysOut.println("Starting port server service...");
    PortServer portServer = new PortServer(appProps);
    portServer.startService();

    sysOut.println();
    sysOut.println("Server started.");
    sysOut.println();

    // wait for the port server to get shutdown
    try {
      portServer.join();
    } catch (Exception e) { }

    // store current property settings
    saveProperties();

    sysOut.println("Server shutdown.");
    sysOut.close();
    sysIn.close();

    //System.exit(-1);
    }
}

