/*************************************************************************

  FILE: $Archive: /Java/apps/Server/ConnectionFsm.java $

  Description:
  
    ConnectionFsm
    
    This Fsm is a very basic "echo-server."  It echoes back any data that
    it receives from the connection.  It holds the connection open until
    receiving a line consisting of "exit".
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/18/01 4:34p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;

public class ConnectionFsm extends Fsm {
	public final int FSM_ST_RECEIVE_LINE	  = 1;
	public final int FSM_ST_PROCESS_LINE	  = 2;
	public final int FSM_ST_END_CONNECTION	= 3;

	private Socket socket = null;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private String input = null;

	public ConnectionFsm(Socket connectSocket) {
		socket = connectSocket;
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
			currentState = FSM_ST_RECEIVE_LINE;
		} catch(IOException ie) {
			System.err.println(
				"Failed to open in/out stream on client socket.");
			currentState = FSM_ST_END_CONNECTION;
		}

		out.println("Connected to server.");
		System.out.println("Connection established with client: " +
			socket.getPort());
	}
					
	public int getState() {
		return currentState;
	}

	public boolean isDone() {
		return currentState == FSM_ST_DONE;
	}

	public void doState() {
		try {
		 	switch (currentState) {
				case FSM_ST_NEW:
				case FSM_ST_RECEIVE_LINE:
					if (in.ready()) {

						input = in.readLine();
						if (input != null) {
							currentState = FSM_ST_PROCESS_LINE;
						}
					}
					break;

				case FSM_ST_PROCESS_LINE:
					out.println("Echo from server: " + input);
					System.out.println("Message from client: " + input);
					if (input.equalsIgnoreCase("exit")) {
						currentState = FSM_ST_END_CONNECTION;
					} else {
						currentState = FSM_ST_RECEIVE_LINE;
					}
					break;

				case FSM_ST_END_CONNECTION:
					out.close();
					in.close();
					socket.close();
					currentState = FSM_ST_DONE;
					break;

				case FSM_ST_DONE:
				default:
					break;
			}
		} catch (IOException ie) {
			System.err.println("IOException in doState().");
			currentState = FSM_ST_END_CONNECTION;
		}
	}

	public void kill() {
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			System.err.println("IOException in kill().");
		}
			
		currentState = FSM_ST_DONE;
	}
}