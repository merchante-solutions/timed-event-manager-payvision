/*************************************************************************

  FILE: $Archive: /Java/apps/Server/PortServer.java $

  Description:  
  
    PortServer
    
    This Service-based class works in conjunction with a supporting
    thread, a ServerPortListener.  This class and the listener class
    share access to an execution queue.  The listener object is resp-
    onsible for establishing connections and placing these connections
    on the queue.  This class monitors the queue and pops off any
    available connections in order to execute a doState() iteration
    (the connection objects themselves know how to handle the client
    requests, PortServer merely cycles through the connections and
    causes the connection handlers objects to do the actual work).

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/11/01 6:32p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.net.*;
import java.io.*;
import java.util.Properties;

public class PortServer extends Service {

	private Properties appProps = null;
	private Queue executeQueue = null;
	private ServerPortListener listener = null;

	public PortServer(Properties props) {
		super(Integer.parseInt(props.getProperty("command.port.number")));
		executeQueue = new Queue();
		appProps = props;
	}

	public boolean serviceStart() {
		if (listener == null)
		{
			listener = new ServerPortListener(
				Integer.parseInt(appProps.
					getProperty("server.port.number")),
				executeQueue);
			listener.start();
		}
		return true;
	}

	public void serviceRun() {
		Fsm connection = 
			(Fsm) executeQueue.pop(500);
		if (connection != null) {
			connection.doState();
			if (!connection.isDone()) {
				executeQueue.push(connection);
			}
		}
		yield();
	}

	public void serviceStop() {
		if (listener != null) {
			listener.stopRunning();
			yield();
			listener = null;
		}

		Fsm connection = (Fsm)executeQueue.pop();
		while (connection != null) {
			connection.kill();
			connection = (Fsm)executeQueue.pop();
		}
	}
}
