package com.mesapps.server;

import java.net.*;
import java.io.*;

public class CommandFsm extends Fsm {
	public final int FSM_ST_RECEIVE_COMMAND	  = 1;
	public final int FSM_ST_COMMAND_RECEIVED	= 2;
	public final int FSM_ST_RECEIVE_RESPONSE	= 3;
	public final int FSM_ST_SEND_RESPONSE	    = 4;
	public final int FSM_ST_END_CONNECTION	  = 5;

	private Socket          socket            = null;
	private PrintWriter     out               = null;
	private BufferedReader  in                = null;
	private String          command           = null;
	private String          commandResponse   = null;

	public CommandFsm(Socket connectSocket) {
		socket = connectSocket;
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
			currentState = FSM_ST_RECEIVE_COMMAND;
		} catch(IOException ie) {
			System.err.println(
				"Failed to open in/out stream on client socket.");
			currentState = FSM_ST_END_CONNECTION;
		}

		out.println("Command port connected.");
		System.out.println("Command connection established with client: " +
			socket.getPort());
	}
					
	public int getState() {
		return currentState;
	}

	public boolean isDone() {
		return currentState == FSM_ST_DONE;
	}

	public void doState() {
		try {
		 	switch (currentState) {
				case FSM_ST_NEW:
				case FSM_ST_RECEIVE_COMMAND:
					if (in.ready()) {

						command = in.readLine();
						if (command != null) {
							currentState = FSM_ST_COMMAND_RECEIVED;
						}
					}
					break;

				case FSM_ST_COMMAND_RECEIVED:
					// do nothing here...service has to detect this
					// state, process the command and then generate
					// a response; see getCommand() and sendResponse()
					break;

				case FSM_ST_SEND_RESPONSE:
					out.println(commandResponse);
					currentState = FSM_ST_END_CONNECTION;
					break;

				case FSM_ST_END_CONNECTION:
					out.close();
					in.close();
					socket.close();
					currentState = FSM_ST_DONE;
					break;

				case FSM_ST_DONE:
				default:
					break;
			}
		} catch (IOException ie) {
			System.err.println("IOException in doState().");
			currentState = FSM_ST_END_CONNECTION;
		}
	}

	public String getCommand() {
		if (currentState != FSM_ST_COMMAND_RECEIVED) {
			return null;
		}

		currentState = FSM_ST_RECEIVE_RESPONSE;
		return command;
	}

	public void sendResponse(String response) {
		if (currentState == FSM_ST_RECEIVE_RESPONSE) {
			commandResponse = response;
			currentState = FSM_ST_SEND_RESPONSE;
		}
	}

	public void kill() {
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			System.err.println("IOException in kill().");
		}
			
		currentState = FSM_ST_DONE;
	}
}
