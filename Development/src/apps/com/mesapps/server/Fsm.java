/*************************************************************************

  FILE: $Archive: /Java/apps/Server/Fsm.java $

  Description:
  
    Fsm
    
    Fsm is an interface that describes a finite-state machine object.
    Classes based on it implement getState() which allows callers to
    determine the current state of the object, isDone(), which returns
    true if the object has achieved the done state, and doState().  The
    doState() routine is responsible for performing all actions associated
    with a particular state.  The interface defines two universal states,
    FSM_ST_NEW which all Fsm's are placed in at creation by default, and
    FSM_ST_DONE which should be the final state of and Fsm when it's job
    has been completed.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/11/01 6:32p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

public abstract class Fsm {
	
	public static final int FSM_ST_NEW    = 0;
	public static final int FSM_ST_DONE   = -1;
  protected int           currentState  = FSM_ST_NEW;
	
	public abstract int getState();
	public abstract boolean isDone();
	public abstract void doState();
  public abstract void kill();
}
