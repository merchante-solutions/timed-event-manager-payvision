/*************************************************************************

  FILE: $Archive: /Java/apps/Server/Queue.java $

  Description:
  
    Queue
    
    Imposes queue-like behavior on a Vector object.  Allows objects to
    be pushed/popped in a FIFO fashion.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/18/01 4:34p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.server;

import java.util.Vector;

public class Queue {

	private Vector items;

	public boolean isEmpty() {

		return items.isEmpty();
	}

	public synchronized Object push(Object pushItem) {
		items.add(pushItem);
		notifyAll();
		return pushItem;
	}

	// the blocking pop version
	public synchronized Object pop(int timeout) {
		Object popItem = null;
		if (!items.isEmpty()) {
			popItem = items.firstElement();
			items.removeElementAt(0);
		}
		else {
			try {
				wait(timeout);
			} catch (Exception e) { }
		}

		return popItem;
	}

	// the non-blocking version
	public Object pop() {
		Object popItem = null;
		if (!items.isEmpty()) {
			popItem = items.firstElement();
			items.removeElementAt(0);
		}

		return popItem;
	}

	public synchronized Object peek() {
		Object peekItem = null;
		if (!items.isEmpty()) {
			peekItem = items.firstElement();
		}

		return peekItem;
	}

	public Queue(int initialSize) {
		items = new Vector(initialSize);
	}

	public Queue() {
		items = new Vector(10);
	}
}