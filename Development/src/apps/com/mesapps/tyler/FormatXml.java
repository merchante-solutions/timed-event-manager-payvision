package com.mesapps.tyler;

import java.io.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class FormatXml
{
  public static void main(String[] args)
  {
    try
    {
      // create a xml doc from the xml file
      FileInputStream fis = new FileInputStream(args[0]);
      SAXBuilder builder = new SAXBuilder();
      Document doc = builder.build(fis);
      fis.close();
    
      // create a formatter
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());

      // write the formatted xml to an output file
      FileOutputStream fos = new FileOutputStream("xml.txt");
      outputter.output(doc,fos);
      fos.flush();
      fos.close();
      
      System.out.println("\nXML.TXT created.");
    }
    catch (Exception e)
    {
      System.out.println("Exception thrown: " + e.toString());
    }
  }
}