package com.mesapps.tyler;

import java.util.*;

import com.payvision.gateway.ReportsApi.*;

import com.mes.api.TridentApiConstants;
import com.mes.payvision.*;
import com.mes.support.DateTimeFormatter;

public class PayvisionReportAPITest extends AppBase {

  public static void main(String[] args) {

    try
    {
      if(args.length == 0) {
        com.mes.database.SQLJConnectionBase.initStandalone("ERROR");
      } else {
        com.mes.database.SQLJConnectionBase.initStandalone(args[0]);
      }

      // set to a month ago
      Calendar cal = Calendar.getInstance();
      Date endDate = cal.getTime();
      cal.add(cal.MONTH,-1);
      Date startDate = cal.getTime();

      ReconciliationRequest request 
        = new ReconciliationRequest("tbaker",1277,startDate,endDate,true);

      System.out.println("sending request: " + request);
      request.sendRequest();
      System.out.println("request sent: " + request);

      ReconciliationResponse response = request.getReconciliationResponse();
      System.out.println("Response: " + response);

      if (response.getCode() == 0)
      {
        Reconciliation result = request.getReconciliation();
        System.out.println("Number of accounts retrieved: " 
          + result.getMerchantAccountsIncluded());
        System.out.println("Returned Start Date: " 
          + DateTimeFormatter.getFormattedDate(
            result.getStartDate().getTime(), 
            DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
        System.out.println("Returned End Date  : " 
          + DateTimeFormatter.getFormattedDate(
            result.getEndDate().getTime(),
            DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      
        ArrayOfReconciliationMerchantAccount merchAccts 
          = result.getMerchantAccounts();
        ReconciliationMerchantAccount[] merchAcctArray 
          = merchAccts.getReconciliationMerchantAccount();
      
        if(merchAcctArray == null) {
          System.out.println("Empty Array");
        } else {
          for (Iterator i = response.getSummaries().iterator(); i.hasNext();) {
            ReconciliationSummary summary = (ReconciliationSummary)i.next();
            System.out.println(""+summary);
            for (Iterator j = summary.getDetails().iterator(); j.hasNext();) {
              System.out.println(""+j.next());
            }
          }
        }
      }

/*
      TransactionsLocator sl = null;
      TransactionsSoapStub service = null;
       
      sl =  new TransactionsLocator();
      sl.setEndpointAddress("TransactionsSoap", 
        //"http://127.0.0.1:8080");
        "https://report.payvisionservices.com/ReportsApi/Transactions.asmx");
        //"https://testprocessor.payvisionservices.com/ReportsApi/Transactions.asmx");
      service = (TransactionsSoapStub)sl.getTransactionsSoap();
      
      // set to a month ago
      Calendar startDate = Calendar.getInstance();
      startDate.add(startDate.MONTH,-1);
      //startDate.set(2008,2,1,0,0,0);

      // set to now
      Calendar endDate = Calendar.getInstance();
      //endDate.set(endDate.get(Calendar.YEAR),endDate.get(Calendar.MONTH),
      //            endDate.get(Calendar.DAY_OF_MONTH),0,0,0);
      
      System.out.println("Retrieving Reports...");
      System.out.println("Start Date: " 
        + DateTimeFormatter.getFormattedDate(
          startDate.getTime(), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      System.out.println("End Date  : " 
        + DateTimeFormatter.getFormattedDate(
          endDate.getTime(), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      
      Reconciliation result = service.getReconciliation(
                                TridentApiConstants.PAYVISION_ECOMM_MEMBER_ID,
                                //1277,
                                TridentApiConstants.PAYVISION_ECOMM_MEMBER_GUID,
                                //"769E8A1E-6C46-4427-9CED-A13B3609DC93",
                                //TridentApiConstants.PAYVISION_ECOMM_MEMBER_ID,
                                1277,
                                startDate,endDate).getData();
                                //test account member id: 1277,
                                  
      System.out.println("Number of accounts retrieved: " 
        + result.getMerchantAccountsIncluded());
      System.out.println("Returned Start Date: " 
        + DateTimeFormatter.getFormattedDate(
          result.getStartDate().getTime(), 
          DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      System.out.println("Returned End Date  : " 
        + DateTimeFormatter.getFormattedDate(
          result.getEndDate().getTime(),
          DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      
      ArrayOfReconciliationMerchantAccount merchAccts 
        = result.getMerchantAccounts();
      ReconciliationMerchantAccount[] merchAcctArray 
        = merchAccts.getReconciliationMerchantAccount();
      
      if(merchAcctArray == null) {
        System.out.println("Empty Array");
      } else {
        for (int i = 0; i < merchAcctArray.length; ++i)
        {
          
          System.out.println("merch acct " + i + ": " + merchAcctArray[i]);
        }
      }
*/
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.error("Error: " + e);
    }
    
  }
}