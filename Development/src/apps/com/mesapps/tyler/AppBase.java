package com.mesapps.tyler;

import java.io.File;

import org.apache.log4j.*;

import com.mes.database.SQLJConnectionBase;

public class AppBase extends SQLJConnectionBase {

  static protected Logger log = Logger.getLogger(AppBase.class);

  static {
    File log4jConfig = new File("log4j.cfg");
    if (log4jConfig.exists()) {
      PropertyConfigurator.configure("log4j.cfg");
      log.info("Log4j configured successfully.");
    }
    else {
      System.out.println("Warning, log4j.cfg not found, log4j not configured...");
    }
  }

  public AppBase()
  {
  }

  public AppBase(String conStr)
  {
    super(conStr);
  }
}