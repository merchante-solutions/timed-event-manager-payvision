/*************************************************************************

  FILE: $Archive: /Java/apps/tyler/TestXml.java $

  Description:
  
  TestXml.java
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/03/03 1:00p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mesapps.tyler;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import java.io.FileInputStream;

public class TestXml
{
  private void run()
  {
    try
    {
      SAXBuilder builder = new SAXBuilder();
      Document doc = builder.build(new FileInputStream("c:\\resp.txt"));
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(doc,System.out);
    }
    catch (Exception e)
    {
      System.out.println("Exception: " + e);
    }
  }
  
  public static void main(String[] args)
  {
    TestXml tester = new TestXml();
    tester.run();
  }
}
