package com.mesapps.tyler;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.util.zip.*;

import com.enterprisedt.net.ftp.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Sync {
  static Logger log = Logger.getLogger(Sync.class);

  private Properties  properties;
  private String      pRemoteHost;
  private int         pRemotePort;
  private String      pRemoteUser;
  private String      pRemotePassword;
  private String      pRemoteSyncDir;
  private String      pLocalRootDir;
  private String      pLocalSyncDir;
  private long        lastSyncTs;
  private long        syncNum;
  private boolean     forceRedeploy;
  private boolean     isApiUpdate;
  private FTPClient   ftp;

  private void connect() throws Exception {
    if (ftp == null) {
      ftp = new FTPClient(pRemoteHost,pRemotePort);
      ftp.login(pRemoteUser,pRemotePassword);
    }
  }

  private void disconnect() throws Exception {
    if (ftp != null) {
      try {
        ftp.quit();
      }
      catch (Exception e) {}
      ftp = null;
    }
  }

  private void loadProperties() throws IOException {

    properties = new Properties();
    FileInputStream in = new FileInputStream("sync.properties");
    properties.load(in);
    in.close();

    pRemoteHost     = properties.getProperty("remote.host");
    pRemotePort     = Integer.parseInt(properties.getProperty("remote.port"));
    pRemoteUser     = properties.getProperty("remote.user");
    pRemotePassword = properties.getProperty("remote.password");
    pRemoteSyncDir  = properties.getProperty("remote.sync.dir");
    pLocalRootDir   = properties.getProperty("local.root.dir");
    pLocalSyncDir   = properties.getProperty("local.sync.dir");
  }

  private void loadLastSync() throws Exception {
    lastSyncTs = 0L;
    try {
      BufferedReader in = new BufferedReader(new FileReader("lastsync.txt"));
      String dateStr = in.readLine();
      SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
      Date lastSyncDate = df.parse(dateStr,new ParsePosition(0));
      lastSyncTs = lastSyncDate.getTime();
      String syncNumStr = in.readLine();
      if (syncNumStr != null)
        syncNum = Long.parseLong(syncNumStr) + 1;
      else
        syncNum = 1;
    }
    catch (FileNotFoundException e) {
      log.info("No prior sync time detected, all files will be synced.");
    }
  }

  private void storeLastSync() throws Exception {
    FileWriter out = new FileWriter("lastsync.txt");
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");
    out.write(df.format(new Date()));
    out.write("\n"+syncNum);
    out.flush();
    out.close();
  }

  public class ScanFilter implements FileFilter {

    Pattern pattern;

    public ScanFilter() {
      pattern = Pattern
        .compile("^((.)*\\.((java)|(class)|(jsp)|(xml)|(cfg)|(dtd)|(tld)|(bat)|(js)|(jpg)|(jpeg)|(gif)|(png)|(css)))$");
    }

    public boolean accept(File file) {
      if (file.isDirectory()) return false;
      if (file.lastModified() <= lastSyncTs) return false;
      boolean isMatch = pattern.matcher(""+file).find();
      return isMatch;
    }
  }

  private List scanFiles(File curDir, FileFilter filter) throws Exception {
    List allFiles = Arrays.asList(curDir.listFiles());
    List foundFiles = new ArrayList(Arrays.asList(curDir.listFiles(filter)));
    for (Iterator i = allFiles.iterator(); i.hasNext();) {
      File file = (File)i.next();
      if (file.isDirectory() && !file.isHidden()) {
        List subdirFiles = scanFiles(file,filter);
        foundFiles.addAll(subdirFiles);
      }
    }
    return foundFiles;
  }

  private List scanFiles() throws Exception {
    List foundFiles = scanFiles(new File(pLocalRootDir),new ScanFilter());
    for (Iterator i = foundFiles.iterator(); i.hasNext();)
    {
      File file = (File)i.next();
      if (!(""+file).endsWith("jsp") && !(""+file).endsWith("js")) {
        log.debug("forcing redeploy for file " + file);
        forceRedeploy = true;
        break;
      }
    }
    return foundFiles;
  }

  private String getRelativeFileStr(File rootDir, File file) {
    String fileStr = ""+file;
    String rootStr = ""+rootDir;
    if (fileStr.startsWith(rootStr)) {
      fileStr = fileStr.substring(rootStr.length() + 1);
    }
    return fileStr;
  }

  private File buildSyncFile(List newFiles) throws Exception {

    log.info("Building sync data file...");

    File syncFile = new File("syncdata.zip");
    FileOutputStream out = new FileOutputStream(syncFile);
    ZipOutputStream zip = new ZipOutputStream(out);
    File rootDir = new File(pLocalRootDir);
    zip.setMethod(zip.DEFLATED);

    int fileCnt = 0;
    long byteCnt = 0;
    for (Iterator i = newFiles.iterator(); i.hasNext(); ++fileCnt) {
      File file = (File)i.next();
      String relFileStr = getRelativeFileStr(rootDir,file);
      log.info("  + " + relFileStr);
      zip.putNextEntry(new ZipEntry(relFileStr));
      FileInputStream in = new FileInputStream(file);
      byte[] data = new byte[100000];
      int readBytes = in.read(data);
      while (readBytes > -1) {
        zip.write(data,0,readBytes);
        byteCnt += readBytes;
        readBytes = in.read(data);
      }
    }
    zip.close();

    log.info("Sync data file built: " + byteCnt + " bytes in " 
      + fileCnt + " files");

    return syncFile;
  }

  private void sendFile(File localFile, String remoteFileName,
    FTPTransferType transferType) throws Exception
  {
    if (localFile == null) return;
    log.info("Sending " + localFile.length() + " bytes in file " + localFile
      + " in " 
      + (transferType.equals(FTPTransferType.BINARY) ? "BINARY" : "ASCII")
      + " mode...");

    ftp.setType(transferType);
    ftp.chdir(pRemoteSyncDir);
    ftp.put(""+localFile,remoteFileName);
  }

  private void renameRemoteFile(String fromName, String toName) 
    throws Exception
  {
    ftp.rename(fromName,toName);
  }

  private void sendSyncFlagFile() throws Exception
  {
    File syncFlagFile = new File("sync.flg");
    FileWriter out = new FileWriter(syncFlagFile);
    String content = "syncreceived";
    out.write(content,0,content.length());
    out.close();
    sendFile(syncFlagFile,""+syncFlagFile,FTPTransferType.ASCII);
    syncFlagFile.delete();
  }
    
  private boolean sendSyncDataFile() throws Exception
  {
    List syncFiles = scanFiles();
    if (syncFiles.size() > 0) {
      File syncFile = buildSyncFile(syncFiles);
      sendFile(syncFile,"syncdata.zip.tmp",FTPTransferType.BINARY);
      renameRemoteFile("syncdata.zip.tmp","syncdata." + syncNum + ".zip");
      storeLastSync();
      return true;
    }
    else {
      log.info("No new files to sync.");
      return false;
    }
  }

  private void sendRedeployFlagFile() throws Exception
  {
    File flagFile = new File("redeploy.flg");
    FileWriter out = new FileWriter(flagFile);
    String content = "redeploy";
    out.write(content,0,content.length());
    out.close();
    sendFile(flagFile,""+flagFile,FTPTransferType.ASCII);
    flagFile.delete();
    log.info("Remote server redeployed.");
  }

  private void sendApiFlagFile() throws Exception
  {
    File flagFile = new File("api.flg");
    FileWriter out = new FileWriter(flagFile);
    String content = "api";
    out.write(content,0,content.length());
    out.close();
    sendFile(flagFile,""+flagFile,FTPTransferType.ASCII);
    flagFile.delete();
    log.info("API update signalled.");
  }

    
  private void run() throws Exception {
    try {
      log.info("Sync started.");
      loadProperties();
      loadLastSync();
      connect();
      boolean syncDataSent = sendSyncDataFile();
      if (isApiUpdate) {
        sendApiFlagFile();
      }
      if (forceRedeploy) {
        sendRedeployFlagFile();
      }
      if (syncDataSent) {
        sendSyncFlagFile();
      }
      log.info("Sync complete.");
    }
    catch (Exception e) {
      throw e;
    }
    finally {
      disconnect();
    }
  }

  public static void main(String[] args) {
    try {
      File log4jConfig = new File("log4j.cfg");
      if (log4jConfig.exists()) {
        PropertyConfigurator.configure("log4j.cfg");
      }
      else {
        System.out.println("Warning, log4j.cfg not found, log4j not configured...");
      }
      Sync s = new Sync();
      for (int i = 0; i <  args.length; ++i) {
        if (args[i].toLowerCase().equals("-r")) {
          s.forceRedeploy = true;
        }
        if (args[i].toLowerCase().equals("-a")) {
          s.isApiUpdate = true;
        }
      }
      s.run();
    }
    catch (Exception e) {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    System.exit(0);
  }
}