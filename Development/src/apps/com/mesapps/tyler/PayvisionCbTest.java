package com.mesapps.tyler;

import java.util.*;

import com.mes.payvision.*;

public class PayvisionCbTest extends AppBase {

  public static void main(String[] args) {

    try
    {
      if(args.length == 0) {
        com.mes.database.SQLJConnectionBase.initStandalone("ERROR");
      } else {
        com.mes.database.SQLJConnectionBase.initStandalone(args[0]);
      }

      // set to a month ago
      Calendar cal = Calendar.getInstance();
      Date endDate = cal.getTime();
      cal.add(cal.MONTH,-1);
      Date startDate = cal.getTime();

      // get transactions from test system
      ChargebackRequest request 
        = new ChargebackRequest("tbaker",1147,startDate,endDate,false);

      log.debug("Test Request: " + request);
      request.sendRequest();

      ChargebackResponse response = request.getChargebackResponse();
      log.debug("Test Response: " + response);

      for (Iterator i = response.getRecords().iterator(); i.hasNext();)
      {
        log.debug("Record: " + i.next());
      }

      PayvisionDb db = new PayvisionDb();
      db.saveChargebackRecords(response);
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.error("Error: " + e);
    }
    
  }
}