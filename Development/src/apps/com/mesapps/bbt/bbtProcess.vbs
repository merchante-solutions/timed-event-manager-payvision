'*********************************************************************
' Copyright 2002, Merchant e-Solutions, Inc. All rights reserved.
'
' The following are intended for use exclusively Merchant e-Solutions.
'
' Do not redistribute without written permission from Merchant e-Solutions.
'*********************************************************************


'Purpose
'-----------
'	- Get, process and upload BBT files onto the SNA server from BBT (Global and PaymentTech)

'Methodology
'-----------
'	- Download daily BBT files by calling VB Win script file: 'get/bbtGet.vbs'
'	- Process (Rename and move) and upload just downloaded bbt files 
'		onto SNA server by way of code in this script file
'	- Upload prepped bbt files by calling 'transfer/proc_file.bat' one by one (synchronously)
'		as renamed files may overlap!

'Assumptions/Assertions
'----------------------
'	- This script is intended to run ONCE per DAY
'	- If this script is not run in a given day, 
'		file processing for files on that given day will have to be 
'		done MANUALLY because the script identifies Payment Tech files on host w/ current date!
'	- Host machine should be Windows 2000
'	- This script file invokes bbtGet.vbs
 
'********************************************************************

option explicit

const cnstThisFilename="bbtProcess.vbs"
const cnstLogFilename="bbtProcess.log"
const cnstLogMethod="File"	'either "File" or "Win"

const cnstSrcFileDir="D:\DEV\BBT\get\bin\"
const cnstTgtFileDir="D:\DEV\BBT\transfer\vital\daily\"
const cnstPostProcessArchiveDir="D:\DEV\BBT\transfer\vital\daily\archive\"

const cnstDownloadBBTScriptFile = "D:\DEV\BBT\get\bbtGet.vbs"
const cnstTransferJavaBatFile = "D:\DEV\BBT\transfer\proc_file.bat"
const cnstTransferJavaBatDir = "D:\DEV\BBT\transfer\"

Log("********** START - "&Now()&" **********")
call Go()
Log("********** END   - "&Now()&" **********")

'debug
'WScript.Echo("DONE!")

sub Go()

'debug
'	on error resume next
'	Err.Clear
	
	dim WshShell,sProc,fso,dir,files,file,mesFilename,sFilename,iFCnt
	
	set WshShell = WScript.CreateObject("WScript.Shell")

'debug
'WScript.Echo("DEBUG - BB&T file download is DISABLED.")
	'download bbt files...
	sProc="cscript "&cnstDownloadBBTScriptFile
	Log("Downloading BB&T files (invoking '"+sProc+"')...")
	WshShell.Run sProc,8,TRUE	'wait for return! (require synchronous processing)
	if(Err<>0) then
		Log("Error ("&Err.Description&") attempting to download BB&T files.  Aborted.")
		exit sub
	end if
	Log("Downloading BB&T files complete.")

	set fso=CreateObject("Scripting.FileSystemObject")

	'process all files in source (download) dir...
	iFCnt=0
	Log("Process downloaded BB&T files...")
	set dir=fso.GetFolder(cnstSrcFileDir)
	set files=dir.Files
	for each file in files
		sFilename=""
		Log("Processing file: "&file.Name&"...")
		mesFilename=xfrFilename(file.Name)
		if(mesFilename="") then
			Log("Encountered BB&T file of unknown type: '"&file.Name&"'.  Skipped.")
		else
			iFCnt=iFCnt+1
			if(mesFilename<>file.Name) then
				sFilename=file.Name
				file.Name=mesFilename
				Log("BB&T file: "&sFilename&" renamed to: "&mesFilename&".")
			end if
			'move file
			fso.MoveFile (cnstSrcFileDir&file.Name), cnstTgtFileDir
			if(Err<>0) then
				Log("Error ("&Err.Description&") moving file ('"&sFilename&"')  File skipped.")
				Err.Clear
			else
'debug
'WScript.Echo("DEBUG - SNA uploading is DISABLED.")
				Log("Uploading file to SNA server: '"&mesFilename&"' (raw name: "&sFilename&")....")
				sProc = cnstTransferJavaBatFile&" "&GetFileTypeFromFileName(mesFilename)&" "&cnstTransferJavaBatDir
				Log("Invoking '"&sProc&"'...")
				WshShell.Run sProc,8,TRUE	'wait for return! (require synchronous processing)
				if(Err<>0) then
					Log("Error ("&Err.Description&") occurred attempting to process file: '"&mesFilename&"' ("&file.Name&").")
					Err.Clear
				end if
				
'NOTE: this not necessary: proc_file.bat takes care of moving the file
'				'processing done on file so move it to accommodate future file processing
'				'NOTE: file should be moved already via proc_file.bat
'				if(fso.FileExists(cnstTgtFileDir&mesFilename)) then
'					set file = fso.GetFile(cnstTgtFileDir&mesFilename)
'					if(Err=0) then
'						'rename file first
'						sFilename=Replace(FormatDateTime(Now(),2),"/","")&"_"&iFCnt&"_"&file.Name
'						file.Name=sFilename
'						Log("Archiving file '"&cnstTgtFileDir&mesFilename&"' to '"&cnstPostProcessArchiveDir&sFilename&"'...")
'						fso.MoveFile cnstTgtFileDir&sFilename, cnstPostProcessArchiveDir
'					end if
'				end if
				
			end if
		end if
	next

	Log("BB&T File processing complete.")

end sub

function GetFileTypeFromFileName(sFN)
	'returns string (file type)
	'	simply filename W/O extension
	dim i
	i=InStr(1,sFN,".")
	if(i>0) then
		GetFileTypeFromFileName=Left(sFN,i-1)
	else
		GetFileTypeFromFileName=sFN
	end if
end function

function xfrFilename(sFN)
	'returns string
	
	'Converts raw downloaded bbt filename to MES-ready filename
	'Returns 0-length string if filename format is unknown (inelidgible file)
	
	xfrFilename=""
	
	if(UCase(Left(sFN,3))="BBT") then
		'Payment Tech file
		xfrFilename="pti3867.dat"
	elseif(UCase(Left(sFN,3))="EDC") then
		'Global file (subject to filename transform)
		xfrFilename="gps3867.dat"
	elseif(UCase(Left(sFN,4))="MIRS") then
		'Global file (subject to filename transform)
		xfrFilename="c_gps3867.dat"
	elseif(UCase(Left(sFN,4))="AUTH") then
		'Global auth file (subject to filename transform)
		xfrFilename="c_auth3867.dat"
	elseif(UCase(Left(sFN,2))="L0") then
		'Global Common auth file (subject to filename transform)
		xfrFilename="l_auth3867.dat"
	elseif(UCase(Left(sFN,2))="A0") then
		'Global central auth file (subject to filename transform)
		xfrFilename="e_auth3867.dat"
	end if
	
end function

sub Log(byref logstr)
	'logs string to log file

	dim method
	
	method=cnstLogMethod
	
	if(len(method)=0) then
		method="Win"
	end if

	if(cnstLogMethod="File") then
		dim path,fn,fso,f

		path=WScript.ScriptFullName
		path=Left(path,(Len(path)-Len(cnstThisFilename)))
	
		fn=path&cnstLogFilename
		set fso=CreateObject("Scripting.FileSystemObject")
		set f=fso.OpenTextFile(fn,8,true,0)
		f.WriteLine logstr
		f.Close
	else
		'log to windows
		WScript.Echo(logstr)
	end if
end sub
