@echo off

rem		Calls FileTransfer %1 %2
rem		Where,
rem		  %1 - file "type" to transfer (filename w/o file extension)
rem		  %2 - dir containing desired working dir containing FileTransfer.jar/.class and related

rem		NOTE: shell's working directory MUST be directory that contains proc_file.bat

chdir %2
set command=c:\jdk1.2.2\bin\java.exe -Xmx64m -cp .;c:\jdk1.2.2\src.jar;.\activation.jar;.\mail.jar;.\transfer.jar;.\FileTransfer.jar FileTransfer %1

rem echo %command%
%command%
