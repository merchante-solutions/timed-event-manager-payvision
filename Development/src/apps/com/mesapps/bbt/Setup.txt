BB&T Deployment SETUP
---------------------

	-	Ensure Java JDK 1.2.2 is installed on deployment machine.
	-	Ensure 'ftp.jar' exists in {jdk v1.2.2}\jre\lib\ext directory.
	-	Ensure ProComPlus installed on deployment machine.
	-	Ensure deployment machine is running Windows 2000 operating system.
	-	Ensure deployment machine has modem installed correctly and is on.
	-	Ensure COM object of ProgID: "MES.autoDial" (InetConn.dll) is installed on deployment machine and is registered via regsrv32.
	-	Establish proper directory structure and files:
			-----------------------------
					- BBT
						* bbtProcess.vbs
						* bbtProcess.log
						* Setup.txt (this file)
						- get
							* bbtLog4j.cfg
							* bbtGet.java
							* GlobalFile.java
							* run.bat
							* GlobalFile.properties
							* bbtGet.log
							* bbtGet.vbs
							- bin
						- transfer
							* proc_file.bat
							* transfer.properties
							* <proc_file dependent files (not listed)>
							- config
							- log
							- vital
								- daily
			-----------------------------
	-	Set bbtProcess.vbs constants:
			E.g.:
			cnstSrcFileDir="D:\DEV\BBT\get\bin\"
			cnstTgtFileDir="D:\DEV\BBT\transfer\vital\daily\"
			cnstDownloadBBTScriptFile = "D:\DEV\BBT\get\bbtGet.vbs"
			cnstTransferJavaBatFile = "D:\DEV\BBT\transfer\proc_file.bat"
	-	Set bbtGet.vbs constants:
			E.g.:
			cnstPCPEXE = "D:\Program Files\Symantec\Procomm Plus\PROGRAMS\PW5.exe"
			cnstlog4JCfgFile="bbtLog4j.cfg"
			cnstGlobalJavaBatFile = "D:\DEV\BBT\get\run.bat"
			cnstPaymentTechPCPScrFile = "D:\Program Files\Symantec\Procomm Plus\Aspect\Paytech.wax"
	-	Set PROP_FILE_NAME constant in GlobalFile.java
			E.g.: 
			PROP_FILE_NAME      = "D:\\DEV\\BBT\\get\\GlobalFile.properties";
	-	Set Paytech.was constants then compile using Symantec's ProCommPlus script compiler (creates adjacent file: 'into Paytech.wax').
			E.g.:
			set dnldpath "D:\DEV\BBT\get\bin"
	-	Ensure Control Panel->Regional Settings->Short Date format of: MM/dd/yyyy.  Otherwise, Paytech.was script file will not run properly.
	-	Configure 'GlobalFile.properties' file.
	-	Configure 'transfer.properties' file.
	-	Ensure "PaymentTech" ProComm Plus data connection exists.
	-	Ensure "GlobalTech" Windows Dial-up connection exists.


Global Credentials
------------------------
Modem	- (800) 356-7626
User	- brb336
Pass	- 7d%%8x
ftp	- 206.227.211.218:1900	[only once connected via modem]

Paymentech Credentials
----------------------
(813) 354-4340
firstname EMERCH
lastname  EMERCH
pass      SOLUTION
