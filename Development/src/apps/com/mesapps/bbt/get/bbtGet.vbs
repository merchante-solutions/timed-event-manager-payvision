'*********************************************************************
' Copyright 2002, Merchant e-Solutions, Inc. All rights reserved.
'
' The following are intended for use exclusively Merchant e-Solutions.
'
' Do not redistribute without written permission from Merchant e-Solutions.
'*********************************************************************


'Purpose
'-------
'	Download BB&T files from Global and PaymentTech
'		(REF: "File Retreival BBT.doc")
'	This script serves as a shell to invoke separate programs 
'	 that do the actual downloading of files

'Assumptions/Assertions
'----------------------
'	This script file is invoked by bbtProcess.vbs

'********************************************************************

option explicit

const cnstThisFilename="bbtGet.vbs"
const cnstLogFilename="bbtGet.log"
const cnstLogMethod="File"	'either "File" or "Win"

const cnstbbtGetJavaFilename="bbtGet"

const cnstPCPEXE = "D:\Program Files\Symantec\Procomm Plus\PROGRAMS\PW5.exe"

const cnstlog4JCfgFile="bbtLog4j.cfg"
const cnstGlobalJavaBatFile = "D:\DEV\BBT\get\run.bat"
const cnstPaymentTechPCPScrFile = "D:\DEV\BBT\get\Paytech.wax"

Log("********** START - "&Now()&" **********")
call Go()
Log("********** END   - "&Now()&" **********")

'debug
'WScript.Echo("DONE!")

sub Go()

'	on error resume next
'	Err.Clear
	
	dim WshShell,oAD,connID,punk,sProc
	
	set WshShell = WScript.CreateObject("WScript.Shell")
	
	'get Global files... (wait for return)
	'first invoke modem-based connection to Global
	Log("Connecting to Global via modem...")
	set oAD=CreateObject("MES.autoDial")
	connID=oAD.Connect("GlobalTech")
	if(Err<>0) then
		Log("Error occurred attempting to connect to Global.")
		connID=0
	end if
	if(connID<>0) then
		Log("Connected to Global.")
		sProc=cnstGlobalJavaBatFile&" "&cnstbbtGetJavaFilename&" "&cnstlog4JCfgFile
		Log("about to invoke: '"&sProc&"'.")
		WshShell.Run sProc,8,TRUE
		if(Err<>0) then
			Log("Error running Global Java bat file.  Aborted.")
		end if
		oAD.Disconnect(connID)
		Log("Disconnected from Global.")
	end if
	set oAD=nothing
	
	'get PaymentTech files... (wait for return)
	Log("Connecting to PaymentTech...")
	punk=chr(34)&cnstPCPEXE&chr(34)&" "&chr(34)&cnstPaymentTechPCPScrFile&chr(34)
'debug
'WScript.Echo punk
'Log(punk)
	WshShell.Run punk,8,TRUE
	if(Err<>0) then
		Log("Error running Payment Tech Pro-Comm PLUS script file.  Aborted.")
	end if
	Log("Connection to PaymentTech ended.")
	
end sub

sub Log(byref logstr)
	'logs string to log file

	dim method
	
	method=cnstLogMethod
	
	if(len(method)=0) then
		method="Win"
	end if

	if(cnstLogMethod="File") then
		dim path,fn,fso,f

		path=WScript.ScriptFullName
		path=Left(path,(Len(path)-Len(cnstThisFilename)))
	
		fn=path&cnstLogFilename
		set fso=CreateObject("Scripting.FileSystemObject")
		set f=fso.OpenTextFile(fn,8,true,0)
		f.WriteLine logstr
		f.Close
	else
		'log to windows
		WScript.Echo(logstr)
	end if
end sub
