@echo off
rem		BUILD Batch file
rem			INPUT PARAMS:
rem			 1. {filename}|"all" (WITHOUT .java extension)

set javac=c:\jdk1.2.2\bin\javac
set jflags=-g
set srcPath=D:\DEV\BBT\get\
set classpath=-classpath .;%srcPath%;D:\DEV\jakarta-log4j-1.1.3\dist\lib\log4j.jar;D:\DEV\java_ftp\build\ftp.jar
set command=%javac% %jflags% %classpath% %srcPath%%1.java

if not %srcPath%%1 == "all"	del %srcPath%%1.class

echo %command%
%command%
