/*************************************************************************

  GlobalFile.java


  Description
  -----------
  Encapsulates logic for the transfer of Global files 
   from Global to MES' staging server

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/01/03 12:40p $
  Version            : $Revision: 4 $

  
  Change History
  --------------
     NONE

  
  Assumptions/Assertions
  ----------------------
    - ftpMoveFiles(): Assumes proper TCP/IP connection has already been established to Global
      for the ftp transfer of files from Global to MES

  

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

import java.util.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;

// com.enterprisedt.net.ftp classes
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.FTPConnectMode;

// log4j (logging utility)
import org.apache.log4j.Category;

public class GlobalFile
{
  // constants
  public static final String  PROP_FILE_NAME      = "D:\\DEV\\BBT\\get\\GlobalFile.properties";

  // data members
  static Category cat = Category.getInstance(GlobalFile.class.getName());
    // log4j
  private Properties props = null;
  
  
  /* 
  ** CONSTRUCTOR GlobalFile
  */
  public GlobalFile()
  {
  }

  
  /*
  ** METHOD transfer
  ** Main entry point for this class.
  ** FTP Transfers Global files from their dial-up ftp site to MES staging server
  */
  public void transfer()
  {
    try
    {
      // load the properties file
    cat.info("GlobalFile - Load properties...");
      if(null==(props = loadProperties(PROP_FILE_NAME)))
      throw new Exception("transfer() - loadProperties failed.  Aborted.");
// debug
//props.list(System.out);
      
    // transfer Global files from Global to MES
    cat.info("GlobalFile - getting files from Global via ftp...");
    ftpMoveFiles();
        
    }
    catch(Exception e)
    {
     cat.error("transfer() exception: "+e.toString());
    }
  }  // transfer()


  /*
  ** METHOD ftpMoveFiles
  **
  ** gets files from Global and puts them on MES server via ftp
  */
  private void ftpMoveFiles()
  {
   try
   {
    String    ftpHost = null;
    int       ftpPort = 0;
    String    ftpUser = null;
    String    ftpPassword = null;
    String    ftpRelTgtDir = null;
    String    localDir = props.getProperty("GlobalFile.deploymachine.tempdir");
    String   sFn = null;
    
    // download Global files via ftp
    ftpHost      = props.getProperty("GlobalFile.global.ftp.host");
    ftpPort      = Integer.parseInt(props.getProperty("GlobalFile.global.ftp.port"));
    ftpUser      = props.getProperty("GlobalFile.global.ftp.user");
    ftpPassword  = props.getProperty("GlobalFile.global.ftp.password");
    ftpRelTgtDir = props.getProperty("GlobalFile.global.ftp.reltargetdir");
    
    FTPClient ftp = new FTPClient(ftpHost, ftpPort);
    ftp.login(ftpUser, ftpPassword);
    ftp.setType(FTPTransferType.ASCII);
    ftp.setConnectMode(FTPConnectMode.ACTIVE);
      // CRITICAL - must issue this command else Socket error occurrs
    
    if(ftpRelTgtDir.length()>0) {
      cat.info("Changing directory on ftp host to '"+ftpRelTgtDir+"'...");
      ftp.chdir(ftpRelTgtDir);
    }
    String files[] =  ftp.dir("");
      // currently don't know of good filter to employ
    int iFileCount = 0;
    for(int i=0; i<files.length;i++) {
      if(IsGlobalFileByFilename(files[i])) {
        sFn = localDir+files[i];
        cat.info("Found Global file to download: '"+files[i]+"' to '"+sFn+"'.  Downloading...");
        ftp.get(sFn,files[i]);
        iFileCount++;
      }
    }
    ftp.quit();
    cat.info("Number of files downloaded: "+iFileCount);
    
  }
  catch(Exception e)
   {
     cat.error("ftpMoveFiles(): " + e.toString());
   }
  
  }  // ftpMoveFiles()

  
  /*
  ** METHOD IsGlobalFileByFilename
  **
  ** Determines if specified filename is a Global file
  */
  private boolean IsGlobalFileByFilename(String sFile)
  {
    boolean result = false;
    
    if( sFile.substring(0, 4).equals("AUTH")  ||  // central auth files
        sFile.substring(0, 2).equals("A0")    ||  // east auth files
        sFile.substring(0, 3).equals("EDC")   ||  // east capture files
        sFile.substring(0, 2).equals("L0")    ||  // common format auth files
        sFile.substring(0, 4).equals("MIRS"))     // central capture files
    {
      result = true;
    }
    
    return result;
  }
  
  
  /*
  ** METHOD loadProperties
  **
  ** Instantiates and loads a standard java.util.Properties object with
  ** all of the configuration options for the file transfer
  */
  private Properties loadProperties(String propFileName)
  {
    Properties result = null;

    try
    {
      // load the properties from a file
      if(fileExists(propFileName))
      {
        FileInputStream   in    = new FileInputStream(propFileName);
        result = new Properties();
        result.load(in);
        in.close();
      }
    }
    catch(Exception e)
    {
      cat.error("loadProperties(): " + e.toString());
    }

    return result;
  }  // loadProperties()

  
  /*
  ** METHOD fileExists
  **
  ** Checks to see if a file with the specified name exists on the file system.
  ** fileName should be a fully-qualified name (with path info)
  */
  private boolean fileExists(String fileName)
  {
    boolean result = false;
    try
    {
      result = new File(fileName).exists();
    }
    catch(Exception e)
    {
      cat.error("fileExists(" + fileName + "): " + e.toString());
    }

    return result;
  }  // fileExists()


}  // class GlobalFile
