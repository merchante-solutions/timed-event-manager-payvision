@echo off
rem		RUN *.java file
rem			INPUT PARAMS:
rem			 1. {filename}
rem			 2. {log4j log config filename (local name only)}

set java=c:\jdk1.2.2\bin\java
set jflags=-g
set srcPath=D:\DEV\BBT\get\
set classpath=-classpath .;%srcPath%;D:\DEV\jakarta-log4j-1.1.3\dist\lib\log4j.jar;D:\DEV\java_ftp\build\ftp.jar
set command=%java% %jflags% %classpath% %srcPath%%1.java
set logCfgFile=%srcPath%%2
set target=%1 %logCfgFile%
set command=%java% %classpath% %target% 

echo %command%
%command%
