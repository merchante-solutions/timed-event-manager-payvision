// package com.mes.bbtGet;

import java.lang.String;

// log4j
import org.apache.log4j.Category;
import org.apache.log4j.PropertyConfigurator;

public class bbtGet
{
	static Category cat = Category.getInstance(bbtGet.class.getName());
	
	public static void main(String[] args)
	{
		try {
			// config log4j w/ config file
			PropertyConfigurator.configure(args[0]);
		
			cat.info("bbtGet START.");
		
			// Global
			cat.info("get Global files...");
			GlobalFile gf = new GlobalFile();
			gf.transfer();
			
			cat.info("bbtGet END.");
		}
		catch(Exception e) {
			cat.error("Exception: "+e.getMessage()+"' occurred.");
		}
	}
	
}
