/*************************************************************************

  FILE: $Archive: /Java/apps/transfer/FileTransfer.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-07-08 16:48:18 -0700 (Tue, 08 Jul 2014) $
  Version            : $Revision: 22835 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mesapps.transfer;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.database.SQLJConnectionBase;
import com.mes.loader.app.manager.ApplicationManager;
import com.mes.loader.batch.exception.MesException;
import com.mes.net.MailMessage;
import com.mes.tools.DBTools;
import com.mes.tools.FileTransmissionLogger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FileTransfer {
    public FileTransfer.NDMFile ndm = null;

    public FileTransfer(String var1) {
        this.ndm = new FileTransfer.NDMFile(var1);
    }

    public FileTransfer(String var1, String var2) {
        this.ndm = new FileTransfer.NDMFile(var1, var2);
    }

    public void transfer() {
        this.ndm.transfer();
    }

    public static void main(String[] args) {
        FileTransfer var1 = null;

        try {
            if(args.length == 2) {
                var1 = new FileTransfer(args[0], args[1]);
                var1.transfer();
            } else if(args.length == 1) {
                var1 = new FileTransfer(args[0]);
                var1.transfer();
            } else {
                System.out.println("ERROR: No file specified");
            }
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.toString());
        } catch (Error er) {
            System.out.println("Error: " + er.toString());
        }

        Runtime.getRuntime().exit(0);
    }

    public class NDMFile extends SQLJConnectionBase {
        public static final String PROP_FILE_NAME = "transfer.properties";
        public static final int FTP_PROD_PORT = 21;
        public static final int FTP_ARC_PORT = 21;
        public static final int FTP_RETRIES = 3;
        public static final int STATE_MONTH = 0;
        public static final int STATE_DAY = 1;
        public static final int STATE_YEAR = 2;
        public static final int FILE_MIF = 0;
        public static final int FILE_DDF = 1;
        public static final int FILE_ACH = 2;
        public static final int FILE_VISA = 3;
        public static final int FILE_MC = 4;
        public static final int FILE_MON_MIF = 5;
        public static final int FILE_MON_EXT = 6;
        public static final int FILE_TC33 = 7;
        public static final int FILE_ACH_TOT = 8;
        public static final int FILE_ACH_FEE = 9;
        public static final int FILE_CHGBK = 10;
        public static final int FILE_WORKED = 11;
        public static final int FILE_STATEMENT = 12;
        public static final int FILE_IPS = 13;
        public static final int FILE_TC57 = 14;
        public static final int FILE_BET = 15;
        public static final int FILE_256 = 16;
        public static final int FILE_PNV = 17;
        public static final int FILE_256_SABRE = 18;
        public static final int FILE_DCC_REV = 19;
        public static final int FILE_DCC_REV_MONTHLY = 20;
        public static final int FILE_XML_STATEMENT = 21;
        public static final int FILE_ADF = 22;
        public static final int FILE_ACH_RETURN = 23;
        public static final int FILE_INTCHG_DETAIL = 24;
        public static final int FILE_ELAN_DETAIL = 25;
        public static final int FILE_VAU = 26;
        public static final int FILE_VAU_REPORT = 27;
        public static final int FILE_CDF = 28;
        public static final int FILE_GPS_DDF = 100;
        public static final int FILE_PTI_DDF = 101;
        public static final int FILE_GPS2_DDF = 102;
        public static final int FILE_GPS_E_AUTH = 103;
        public static final int FILE_GPS_C_AUTH = 104;
        public static final int FILE_GPS_L_AUTH = 105;
        public static final int FILE_CMS_CCP = 200;
        public static final int FILE_CMS_CIF = 201;
        public static final int FILE_ACH_REJECT = 300;
        public static final int FILE_GCF = 400;
        public static final int FILE_CERTEGY_MIF = 1000;
        public static final int FILE_CERTEGY_CB = 1001;
        public static final int FILE_CERTEGY_DDF = 1002;
        public static final int FILE_CERTEGY_STMT = 1003;
        public static final int FILE_CERTEGY_EXTRACT = 1004;
        public static final int FILE_CERTEGY_RETRIEVAL = 1005;
        public static final int FILE_CERTEGY_UNUSED = 1006;
        public static final int FILE_CERTEGY_CHG_TBL = 1007;
        public static final int FILE_CERTEGY_INDEX = 1008;
        public static final int FILE_CERTEGY_CNC_AUTH = 1009;
        public static final int FILE_BML_RECONCILIATION = 1010;
        public static final int FILE_COMDATA_MIF = 1100;
        public static final int FILE_COMDATA_DDF = 1101;
        public static final int FILE_COMDATA_CDF = 1102;
        public static final int FILE_CERTEGY_CHECK_MIF = 2000;
        public static final int FILE_CERTEGY_CHECK_ILF = 2001;
        public static final int FILE_CERTEGY_CHECK_BILL = 2002;
        public static final int FILE_BANKSERV_CONFIRM = 3000;
        public static final int FILE_BANKSERV_EXTRACT = 3001;
        public static final int FILE_BANKSERV_OUT = 3002;
        public static final int FILE_TRIDENT_AUTH = 4000;
        public static final int FILE_VS_DEBIT_RAW = 5000;
        public static final int FILE_VS_DEBIT_ILINK_RAW = 5001;
        public static final int FILE_VS_DEBIT_RPT = 5002;
        public static final int FILE_VS_DEBIT_ILINK_RPT = 5003;
        public static final int ZIP_BUFFER_LEN = 16384;
        public static final int AR_EFF_DATE_OFF = 20;
        public static final int AR_SETTL_DATE_OFF = 10;
        public static final int AR_RPT_DATE_OFF = 155;
        private String fileBase = "";
        private String bankNumber = "";
        private Properties props = null;
        private boolean fileNameAssigned = false;
        private String configPath = "";
        private String sourcePath = "";
        private String sourceFile = "";
        private String source = "";
        private String destination = "";
        private String archive = "";
        private int fileType = 0;
        private String extEmail = "";
        private boolean useCompression = true;
        private boolean sendFileEmail = true;
        private String delimiter = "";
        private int checkLength = -1;
        private boolean error = false;
        private String errorString = "";
        private String uniqueName = "";
        private String rawFileName = "";
        private String flagFileName = "";
        private String zipFileName = "";
        private String prodZipFileName = "";
        private long fileSize = 0L;
        private long loadFileId = 0L;
        private Vector Bins = new Vector();
        private String lastId = "";
        private int curRec = 1;
        private int lineCount = 0;
        private String processingDate = "";
        private String cbStoredVal = "";
        private boolean footerFound = false;
        private String d256Code = "";
        private HashMap rejectDuplicates = new HashMap();
        private String prodFtpHost;
        private String prodFtpUser;
        private String prodFtpPassword;
        private String archiveFtpHost;
        private String archiveFtpUser;
        private String archiveFtpPassword;

        private void loadRequiredProperties() {
            prodFtpHost = validate("transfer.ftp.production.host");
            prodFtpUser = validate("transfer.ftp.production.user");
            prodFtpPassword = validate("transfer.ftp.production.password");
            archiveFtpHost = validate("transfer.ftp.archive.host");
            archiveFtpUser = validate("transfer.ftp.archive.user");
            archiveFtpPassword = validate("transfer.ftp.archive.password");
        }

        private String validate(String propertyName) {
            String value = props.getProperty(propertyName);

            if(value == null || value.trim().length() == 0) {
                throw new RuntimeException("Required property not specified: " + propertyName);
            }

            return value;
        }

        private String hex(char chr) {
            String var2 = Integer.toString(chr, 16).toUpperCase();
            return "0000".substring(var2.length(), 4) + var2;
        }

        public void transfer() {
            try {
                this.props = this.loadProperties("transfer.properties");
                ndm.loadRequiredProperties();

                this.debugMessage("extracting file properties");
                this.extractFileProperties();
                this.debugMessage("checking for file existence");
                if(this.fileExists(this.source)) {
                    if(this.props.getProperty("transfer.database.enabled", "Y").equals("Y")) {
                        try {
                            this.debugMessage("setting connection string for single connections");
                        } catch (Exception e) {
                            this.logEntry("Exception creating database connection: " + e.toString());
                        }
                    }

                    if(!this.fileNameAssigned) {
                        this.debugMessage("generating unique name");
                        this.generateUniqueName();
                        this.debugMessage("copying RAW file to .raw");
                        File var1 = new File(this.source);
                        var1.renameTo(new File(this.sourcePath + this.rawFileName));
                    }

                    this.debugMessage("parsing records (" + this.sourcePath + this.rawFileName + ")");
                    if(!this.getExtendedProperty(this.props, "transfer.file.pristine.").equals("Y")) {
                        this.parseRecords(this.sourcePath + this.rawFileName);
                    }

                    if(this.props.getProperty("transfer.database.enabled", "Y").equals("Y")) {
                        try {
                            this.debugMessage("logging to file transmission log");
                            FileTransmissionLogger.logStage(this.uniqueName, 1, this.fileSize, Integer.parseInt(this.bankNumber));
                            this.debugMessage("done logging");
                        } catch (Exception e) {
                            this.logEntry("Exception logging transmission: " + e.toString());
                        } catch (Error er) {
                            this.logEntry("Error logging transmission: " + er.toString());
                        }
                    }

                    this.debugMessage("moving files");
                    this.moveFiles();
                    if(!this.error) {
                        this.sendEmail();
                        this.logEntry("FILE TRANSFER SUCCESS");
                    } else {
                        this.debugMessage("error condition");
                        this.logEntry("FILE TRANSFER ERROR");
                    }
                } else {
                    this.debugMessage("File " + this.sourceFile + " does not exist");
                    this.showError("File " + this.sourceFile + " does not exist");
                }

                this.debugMessage("done with transfer()");
            } catch (Exception ex) {
                this.showError("transfer(): " + ex.toString());
            } finally {
                new ApplicationManager().execute();
            }

        }

        public void moveFilesOnly(String fileName) {
            try {
                this.initialize(fileName.substring(0, fileName.length() - 11));
                this.props = this.loadProperties("transfer.properties");
                this.extractFileProperties();
                this.uniqueName = fileName + ".dat";
                this.flagFileName = fileName + ".flg";
                this.zipFileName = fileName + ".zip";
                this.prodZipFileName = fileName + ".dat.zip";
                this.rawFileName = fileName + ".raw";
                this.moveFiles();
                if(!this.error) {
                    this.sendEmail();
                }
            } catch (Exception ex) {
                System.out.println("moveFilesOnly(" + fileName + ", " + this.sourcePath + "): " + ex.toString());
            }

        }

        public void generateUniqueName() {
            int var1 = 0;
            String fileName = "";
            boolean var3 = false;
            Properties properties = null;
            if(!this.error) {
                try {
                    String propertiesFileName = this.configPath + this.fileBase + ".properties";
                    properties = this.loadProperties(propertiesFileName);
                    Calendar var6 = Calendar.getInstance();
                    SimpleDateFormat var7 = new SimpleDateFormat("MMddyy");
                    String var8 = var7.format(var6.getTime());
                    Calendar var9 = Calendar.getInstance();
                    int var10 = var9.get(5);
                    if(var10 < 15) {
                        var9.add(2, -1);
                    }

                    SimpleDateFormat sdf = new SimpleDateFormat("MMyyyy");
                    this.processingDate = sdf.format(var9.getTime());
                    if(properties != null) {
                        if(var8.equals(this.getExtendedProperty(properties, "transfer.file.lastdate."))) {
                            var1 = Integer.parseInt(this.getExtendedProperty(properties, "transfer.file.lastcount."));
                        }
                    } else {
                        properties = new Properties();
                    }

                    ++var1;
                    boolean flag = false;
                    int var15;
                    if(this.fileType == 4000) {
                        var15 = 100000;
                    } else {
                        var15 = 1000;
                    }

                    while(!var3 && var1 < var15) {
                        String var13 = "";
                        if(this.fileType == 4000) {
                            var13 = this.makeIntString(var1, 5);
                        } else {
                            var13 = this.makeIntString(var1, 3);
                        }

                        fileName = this.fileBase + "_" + var8 + "_" + var13;
                        if(this.fileExists(this.sourcePath + fileName + ".dat")) {
                            ++var1;
                        } else {
                            var3 = true;
                            this.uniqueName = fileName + ".dat";
                            this.flagFileName = fileName + ".flg";
                            this.zipFileName = fileName + ".zip";
                            this.prodZipFileName = fileName + ".dat.zip";
                            this.rawFileName = fileName + ".raw";
                        }
                    }

                    properties.setProperty("transfer.file.lastdate." + this.fileBase, var8);
                    properties.setProperty("transfer.file.lastcount." + this.fileBase, Integer.toString(var1));
                    FileOutputStream var16 = new FileOutputStream(propertiesFileName);
                    properties.store(var16, this.fileBase);
                    var16.close();
                    this.loadFileId = DBTools.loadFilenameToLoadFileId(this.uniqueName, this.Ctx);
                } catch (Exception var14) {
                    this.showError("generateUniqueName(): " + var14.toString());
                }
            }

        }

        private void parseRecords(String var1) {
            StringBuffer var2 = new StringBuffer("");
            BufferedReader var3 = null;
            FileOutputStream var4 = null;
            Object var5 = null;
            if(!this.error) {
                try {
                    String var6 = "";
                    StringBuffer var7 = new StringBuffer("");
                    this.fileSize = 0L;
                    var3 = new BufferedReader(new FileReader(var1));
                    if(this.useCompression) {
                        var4 = new FileOutputStream(this.sourcePath + this.prodZipFileName);
                        var5 = new ZipOutputStream(var4);
                        ((ZipOutputStream)var5).setMethod(8);
                        ZipEntry var8 = new ZipEntry(this.uniqueName);
                        ((ZipOutputStream)var5).putNextEntry(var8);
                    } else {
                        var5 = new FileOutputStream(this.sourcePath + this.uniqueName);
                    }

                    String var32 = "";
                    boolean var9 = false;
                    boolean var10 = false;
                    int var33;
                    int var34;
                    switch(this.fileType) {
                        case 4000:
                            var32 = "FH";
                            var33 = this.getRecordLength(var32);
                            var7.append(var32);

                            for(var34 = 0; var34 < var33 - var32.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec++));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                    }

                    for(var6 = var3.readLine(); var6 != null; var6 = var3.readLine()) {
                        if(this.checkLength > 0 && var6.length() != this.checkLength && var6.length() != 1) {
                            StringBuffer var11 = new StringBuffer("");
                            var11.append("*** RECORD LENGTH MISMATCH ***\n\n");
                            var11.append("The records in this file should all be of length ");
                            var11.append(this.checkLength);
                            var11.append(", however record #");
                            var11.append(this.curRec);
                            var11.append(" had a length of ");
                            var11.append(var6.length());
                            var11.append(".\n\n");
                            var11.append("This file was processed anyway, but this length mismatch may indicate a serious problem with the file.");
                            this.sendErrorEmail(var11.toString());
                        }

                        boolean var35 = false;
                        switch(this.fileType) {
                            case 1004:
                                if(var6.length() > 70 && var6.substring(66, 69).equals("1AA")) {
                                    var35 = true;
                                }
                        }

                        if(!var35) {
                            switch(this.fileType) {
                                case 21:
                                case 1004:
                                case 2000:
                                    var6 = var6.trim();
                            }

                            this.fileSize += (long)var6.length();
                            ++this.lineCount;
                            var6 = this.fixRecord(var6, (OutputStream)var5);
                            int var12;
                            if((this.fileType == 16 || this.fileType == 28) && var6.length() < 130 && this.lineCount % 2 > 0) {
                                var7.append(var6);

                                for(var12 = 0; var12 < this.getRecordLength(var6) - var6.length(); ++var12) {
                                    var7.append(' ');
                                }
                            } else if(var6.length() > 1 || this.fileType == 16 || this.fileType == 12 || this.fileType == 28 || this.fileType == 1003) {
                                var7.append(var6);
                                if(this.fileType != 21 && this.fileType != 5002 && this.fileType != 5003) {
                                    if(this.delimiter.equals("")) {
                                        var33 = this.getRecordLength(var6);

                                        for(var12 = 0; var12 < var33 - var6.length(); ++var12) {
                                            var7.append(' ');
                                        }
                                    }

                                    var7.append(this.delimiter);
                                    switch(this.fileType) {
                                        case 15:
                                            var7.append(this.processingDate);
                                            break;
                                        case 16:
                                        case 18:
                                        case 28:
                                            var7.append(this.d256Code);
                                            var7.append(this.delimiter);
                                            break;
                                        case 19:
                                        case 20:
                                        case 300:
                                        case 1000:
                                        case 1001:
                                        case 1002:
                                        case 1003:
                                        case 1008:
                                        case 1100:
                                        case 1101:
                                        case 2000:
                                            var7.append(this.bankNumber);
                                            var7.append(this.delimiter);
                                    }

                                    var7.append(this.uniqueName);
                                    var7.append(this.delimiter);
                                    if(this.delimiter.equals("")) {
                                        for(var12 = 0; var12 < 32 - this.uniqueName.length(); ++var12) {
                                            var7.append(' ');
                                        }
                                    }

                                    var7.append(Long.toString(this.loadFileId));
                                    var7.append(this.delimiter);
                                    if(this.delimiter.equals("")) {
                                        for(var12 = 0; var12 < 20 - Long.toString(this.loadFileId).length(); ++var12) {
                                            var7.append(' ');
                                        }
                                    }

                                    var7.append(Integer.toString(this.curRec));
                                }

                                ++this.curRec;
                                var7.append('\n');
                                ((OutputStream)var5).write(var7.toString().getBytes());
                                var7.setLength(0);
                            }
                        }
                    }

                    String var36 = "";
                    switch(this.fileType) {
                        case 3:
                        case 4:
                        case 10:
                        case 11:
                        case 22:
                        case 1001:
                        case 1005:
                        case 1101:
                        case 1102:
                        case 2001:
                        case 2002:
                            var36 = "FF";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);
                            var7.append(this.delimiter);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.delimiter);
                            switch(this.fileType) {
                                case 1101:
                                    var7.append(this.bankNumber);
                                    var7.append(this.delimiter);
                                default:
                                    var7.append(this.uniqueName);
                                    var34 = 0;
                            }

                            while(var34 < 32 - this.uniqueName.length()) {
                                var7.append(' ');
                                ++var34;
                            }

                            var7.append(this.delimiter);
                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 7:
                            var36 = "99";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 14:
                            var36 = "5799";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 102:
                            var36 = "T";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 103:
                        case 104:
                        case 1009:
                            var36 = "TT";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 300:
                            var36 = "T";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.bankNumber);
                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 1002:
                            var36 = "T";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.bankNumber);
                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 1004:
                            var2.append("MM541");

                            for(var34 = 0; var34 < 61; ++var34) {
                                var2.append(' ');
                            }

                            var2.append("FF");
                            var33 = this.getRecordLength(var2.toString());
                            var7.append(var2.toString());

                            for(var34 = 0; var34 < var33 - var2.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                            break;
                        case 4000:
                            var36 = "FF";
                            var33 = this.getRecordLength(var36);
                            var7.append(var36);

                            for(var34 = 0; var34 < var33 - var36.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(this.uniqueName);

                            for(var34 = 0; var34 < 32 - this.uniqueName.length(); ++var34) {
                                var7.append(' ');
                            }

                            var7.append(Integer.toString(this.curRec));
                            var7.append('\n');
                            ((OutputStream)var5).write(var7.toString().getBytes());
                            var7.setLength(0);
                    }

                    this.debugMessage("done parsing records");
                } catch (Exception var30) {
                    this.showError("parseRecords(" + var1 + "): " + var30.toString());
                } finally {
                    if(this.useCompression) {
                        try {
                            ((ZipOutputStream)var5).closeEntry();
                        } catch (Exception var29) {}
                    }

                    try {
                        ((OutputStream)var5).close();
                    } catch (Exception var28) {}

                    try {
                        var3.close();
                    } catch (Exception var27) {}

                }
            }

            this.debugMessage("Leaving parseRecords()");
        }

        public boolean isFileTypeCdf() {  
        	return (fileType == FILE_256 || fileType == FILE_256_SABRE || fileType == FILE_CDF);
        }

        private void moveFiles() {
            Object var1 = null;
            boolean var2 = false;
            FTPClient ftpClient = null;
            FileOutputStream fileOut = null;
            ZipOutputStream zipOut = null;
            ZipInputStream zipOut2 = null;
            FileInputStream fileOut2 = null;
            if(!this.error) {
                try {
                    this.debugMessage("  creating flag file");
                    FileOutputStream var10 = new FileOutputStream(this.sourcePath + this.flagFileName);
                    String var11 = this.sourcePath + this.uniqueName + "\n";
                    var10.write(var11.getBytes());
                    var10.close();
                    this.debugMessage("  transmitting files to loading server");
                    int count;

                    if(this.props.getProperty("transfer.ftp.production.enabled", "").equals("Y") && !this.getExtendedProperty(this.props, "transfer.file.production.").equals("N")) {
                        if(this.props.getProperty("transfer.ftp.split", "").equals("Y")) {
                            for(count = 0; count < 3; ++count) {
                                if(isFileTypeCdf()) {
                                    //don't move flag file for CDF
                                    break;
                                }
                                this.debugMessage("  connect to loading server for data file try #" + count + ":");

                                try {
                                    this.debugMessage("  sending data file");
                                    this.debugMessage("    creating FTPClient");
                                    ftpClient = new FTPClient(prodFtpHost, 21);
                                    this.debugMessage("    logging in");
                                    ftpClient.login(prodFtpUser, prodFtpPassword);
                                    ftpClient.setType(FTPTransferType.BINARY);
                                    this.debugMessage("    changing directory");
                                    ftpClient.chdir(this.destination);
                                    this.debugMessage("    putting prod file");
                                    if(this.getExtendedProperty(this.props, "transfer.file.pristine.").equals("Y")) {
                                        ftpClient.put(this.sourcePath + this.rawFileName, this.uniqueName);
                                    } else if(this.useCompression) {
                                        ftpClient.put(this.sourcePath + this.prodZipFileName, this.prodZipFileName);
                                    } else {
                                        ftpClient.put(this.sourcePath + this.uniqueName, this.uniqueName);
                                    }

                                    this.debugMessage("    closing ftp connection");
                                    ftpClient.quit();
                                    break;
                                } catch (Exception e) {
                                    if(count == 2) {
                                        this.debugMessage("    FTP RETRY COUNT EXCEEDED, THROWING EXCEPTION");
                                        this.logEntry("  FTP attempt #" + count + " failed - " + e.toString());
                                        throw e;
                                    }
                                } finally {
                                    if(ftpClient != null) {
                                        try {
                                            ftpClient.quit();
                                        } catch (Exception var101) {}
                                    }

                                }
                            }
                            //Transfer flag file
                            for(count = 0; count < 3; ++count) {
                                if(isFileTypeCdf()) {
                                    //don't move flag file for CDF
                                    break;
                                }
                                this.debugMessage("  connect to loading server for flag file try #" + count + ":");

                                try {
                                    this.debugMessage("  sending flag file");
                                    this.debugMessage("    creating FTPClient");
                                    ftpClient = new FTPClient(prodFtpHost, 21);
                                    this.debugMessage("    logging in");
                                    ftpClient.login(prodFtpUser, prodFtpPassword);
                                    ftpClient.setType(FTPTransferType.BINARY);
                                    this.debugMessage("    changing directory");
                                    ftpClient.chdir(this.destination);
                                    this.debugMessage("    putting flag file");
                                    ftpClient.put(this.sourcePath + this.flagFileName, this.flagFileName);
                                    this.debugMessage("    closing ftp connection");
                                    ftpClient.quit();
                                    break;
                                } catch (Exception ex) {
                                    if(count == 2) {
                                        this.debugMessage("    FTP RETRY COUNT EXCEEDED, THROWING EXCEPTION");
                                        this.logEntry("  FTP attempt #" + count + " failed - " + ex.toString());
                                        throw ex;
                                    }
                                } finally {
                                    if(ftpClient != null) {
                                        try {
                                            ftpClient.quit();
                                        } catch (Exception var100) {}
                                    }

                                }
                            }
                        } else {
                            for(count = 0; count < 3; ++count) {
                                if(isFileTypeCdf()) {
                                    //don't move flag file for CDF
                                    break;
                                }
                                this.debugMessage("  connect to loading server try #" + count + ":");

                                try {
                                    this.debugMessage("    creating FTPClient");
                                    ftpClient = new FTPClient(prodFtpHost, 21);
                                    this.debugMessage("    logging in");
                                    ftpClient.login(prodFtpUser, prodFtpPassword);
                                    ftpClient.setType(FTPTransferType.BINARY);
                                    this.debugMessage("    changing directory");
                                    ftpClient.chdir(this.destination);
                                    this.debugMessage("    putting prod file");
                                    if(this.getExtendedProperty(this.props, "transfer.file.pristine.").equals("Y")) {
                                        ftpClient.put(this.sourcePath + this.rawFileName, this.uniqueName);
                                    } else if(this.useCompression) {
                                        ftpClient.put(this.sourcePath + this.prodZipFileName, this.prodZipFileName);
                                    } else {
                                        ftpClient.put(this.sourcePath + this.uniqueName, this.uniqueName);
                                    }

                                    this.debugMessage("    putting flag file");
                                    ftpClient.put(this.sourcePath + this.flagFileName, this.flagFileName);
                                    this.debugMessage("    closing ftp connection");
                                    ftpClient.quit();
                                    break;
                                } catch (Exception var108) {
                                    if(count == 2) {
                                        this.debugMessage("    FTP RETRY COUNT EXCEEDED, THROWING EXCEPTION");
                                        this.logEntry("  FTP attempt #" + count + " failed - " + var108.toString());
                                        throw var108;
                                    }
                                } finally {
                                    if(ftpClient != null) {
                                        try {
                                            ftpClient.quit();
                                        } catch (Exception var99) {}
                                    }

                                }
                            }
                        }
                    }

                    this.debugMessage("  creating archive .ZIP file");
                    fileOut = new FileOutputStream(this.sourcePath + this.zipFileName);
                    zipOut = new ZipOutputStream(fileOut);
                    zipOut.setMethod(8);
                    this.debugMessage("  adding .raw file to archive zip (" + this.sourcePath + this.rawFileName + ")");
                    zipOut.putNextEntry(new ZipEntry(this.rawFileName));
                    int var112 = 0;
                    fileOut2 = new FileInputStream(this.sourcePath + this.rawFileName);
                    byte[] var111 = new byte[16384];

                    while(var112 != -1) {
                        var112 = fileOut2.read(var111, 0, var111.length);
                        if(var112 != -1) {
                            zipOut.write(var111, 0, var112);
                        }
                    }

                    zipOut.closeEntry();
                    fileOut2.close();
                    var1 = null;
                    if(this.useCompression & !this.getExtendedProperty(this.props, "transfer.file.pristine.").equals("Y")) {
                        this.debugMessage("  adding .dat file to archive zip (" + this.sourcePath + this.prodZipFileName + ")");
                        zipOut.putNextEntry(new ZipEntry(this.uniqueName));
                        var112 = 0;
                        if(this.useCompression) {
                            zipOut2 = new ZipInputStream(new FileInputStream(this.sourcePath + this.prodZipFileName));
                            zipOut2.getNextEntry();
                            var111 = new byte[16384];

                            while(var112 != -1) {
                                var112 = zipOut2.read(var111, 0, var111.length);
                                if(var112 != -1) {
                                    zipOut.write(var111, 0, var112);
                                }
                            }

                            zipOut2.close();
                        } else {
                            fileOut2 = new FileInputStream(this.sourcePath + this.uniqueName);
                            var111 = new byte[16384];

                            while(var112 != -1) {
                                var112 = fileOut2.read(var111, 0, var111.length);
                                if(var112 != -1) {
                                    zipOut.write(var111, 0, var112);
                                }
                            }

                            fileOut2.close();
                        }

                        zipOut.closeEntry();
                    }

                    zipOut.close();
                    this.debugMessage("  transmitting zip file to kirin");
                    if(this.props.getProperty("transfer.ftp.archive.enabled", "Y").equals("Y")) {
                        for(count = 0; count < 3; ++count) {
                            try {
                                ftpClient = new FTPClient(archiveFtpHost, 21);
                                ftpClient.login(archiveFtpUser, archiveFtpPassword);
                                ftpClient.setType(FTPTransferType.BINARY);
                                ftpClient.chdir(this.archive);
                                ftpClient.put(this.sourcePath + this.zipFileName, this.zipFileName);
                                ftpClient.put(this.sourcePath + this.flagFileName, this.flagFileName);
                                ftpClient.quit();
                                break;
                            } catch (Exception var102) {
                                if(count == 2) {
                                    throw var102;
                                }
                            } finally {
                                if(ftpClient != null) {
                                    try {
                                        ftpClient.quit();
                                    } catch (Exception var98) {}
                                }

                            }
                        }
                    }

                    this.debugMessage("  removing files");
                    if(this.props.getProperty("transfer.ftp.production.enabled", "Y").equals("Y") && !this.getExtendedProperty(this.props, "transfer.file.delete.").equals("N")) {
                        if(this.props.getProperty("transfer.file.secure", "N").equals("Y")) {
                            this.debugMessage("  SECURE REMOVAL");

                            String var13 = this.props.getProperty("transfer.file.secure.wipecommand", "/usr/local/bin/wipe");
                            var13 = var13 + " " + this.sourcePath;

                            for(int var15 = 0; var15 < 5; ++var15) {
                                String var16 = var13;
                                if(System.getProperties().getProperty("file.separator").equals("\\")) {
                                    var16 = var13.replace('/', '\\');
                                }

                                switch(var15) {
                                    case 0:
                                        var16 = var16 + this.flagFileName;
                                        break;
                                    case 1:
                                        var16 = var16 + this.uniqueName;
                                        break;
                                    case 2:
                                        var16 = var16 + this.rawFileName;
                                        break;
                                    case 3:
                                        var16 = var16 + this.zipFileName;
                                        break;
                                    case 4:
                                        var16 = var16 + this.prodZipFileName;
                                }
                                //If file type is CDF and file extension is .raw then skip deleting
                                if(!var16.endsWith("raw") || !isFileTypeCdf()) {
                                    deleteSecure(var16);
                                }
                            }
                        } else {
                            this.debugMessage("  NORMAL REMOVAL");
                            this.deleteFile(this.sourcePath + this.flagFileName);
                            this.deleteFile(this.sourcePath + this.uniqueName);
                            //if file type is CDF then skip the file deleting
                            if(!isFileTypeCdf()) {
                                this.deleteFile(this.sourcePath + this.rawFileName);
                            }
                            this.deleteFile(this.sourcePath + this.zipFileName);
                            this.deleteFile(this.sourcePath + this.prodZipFileName);
                        }
                    }
                } catch (Exception var110) {
                    this.showError("moveFiles(): " + var110.toString());
                }
            }

        }

        private void deleteSecure(String var16) throws InterruptedException, IOException {
            Runtime var14 = Runtime.getRuntime();
            Process var113 = null;

            this.debugMessage("*** SECURE DELETING: " + var16);
            var113 = var14.exec(var16);
            this.debugMessage("*** WAITING FOR SECURE DELETE TO FINISH");
            int var17 = var113.waitFor();
            this.debugMessage("***return code from secure delete = " + var17);
        }

        private String initEmail() {
            SimpleDateFormat var1 = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
            Calendar var2 = Calendar.getInstance();
            String var3 = "";

            try {
                var3 = "[" + var1.format(var2.getTime()) + "]";
            } catch (Exception var5) {
                this.logEntry("initEmail(): " + var5.toString());
            }

            return var3;
        }

        private void sendErrorEmail(String var1) {
            try {
                String var2 = this.initEmail();
                MailMessage var3 = new MailMessage();
                var3.setAddresses(110);
                var3.setSubject("File Transfer Error " + this.bankNumber + " (" + this.fileBase + ")");
                var3.setText(var2 + " - " + this.uniqueName + "\n\n" + var1);
                var3.send();
            } catch (Exception var4) {
                this.logEntry("sendErrorEmail(): " + var4.toString());
            }

        }

        private void sendEmail() {
            boolean var1 = !this.error;
            String var2 = "started";

            try {
                this.debugMessage("Sending email");
                if(this.props.getProperty("transfer.email.enabled", "Y").equals("Y") && this.sendFileEmail) {
                    String var3 = this.initEmail();
                    this.debugMessage("  email is enabled, setting up variables");
                    StringBuffer var4 = new StringBuffer("");
                    StringBuffer var5 = new StringBuffer("");
                    int var6 = var1?110:111;
                    MailMessage var7 = new MailMessage();
                    var7.setAddresses(var6);
                    StringTokenizer var8 = null;
                    if(var1) {
                        var8 = new StringTokenizer(this.extEmail, ";");

                        while(var8.hasMoreTokens()) {
                            String var9 = var8.nextToken();
                            (new StringBuffer()).append("  adding recipient: ").append(var9).toString();
                            this.debugMessage("    adding recipient: " + var9);
                            var7.addCC(var9);
                        }
                    }

                    var2 = "adding subject and message";
                    this.debugMessage("  adding subject and message");
                    var4.append(var3);
                    var4.append(" ");
                    if(var1) {
                        var5.append("File Transfer Success ");
                        var5.append(this.bankNumber);
                        var5.append(" (");
                        var5.append(this.fileBase);
                        var5.append(")");
                        var4.append("Successfully processed ");
                        var4.append(this.sourcePath);
                        var4.append(this.uniqueName);
                        var4.append(" (");
                        var4.append(this.curRec);
                        var4.append(" records)");
                    } else {
                        var5.append("File Transfer Error ");
                        var5.append(this.bankNumber);
                        var5.append(" (");
                        var5.append(this.fileBase);
                        var5.append(")");
                        var4.append("Failure processing ");
                        var4.append(this.source);
                        var4.append("\n");
                        var4.append(this.errorString);
                    }

                    var7.setSubject(var5.toString());
                    var7.setText(var4.toString());
                    var2 = "sending email";
                    this.debugMessage("  sending email");
                    var7.send();
                    this.debugMessage("Send Email complete");
                }
            } catch (Exception e) {
                this.error = true;
                this.logEntry("sendEmail(): " + e.toString());
            }

        }

        public void deleteFile(String filePath) {
            try {
                File var2 = new File(filePath);
                var2.delete();
            } catch (Exception var3) {
                this.showError("deleteFile(" + filePath + "): " + var3.toString());
            }

        }

        private void copyFile(String var1, String var2) {
            if(!this.error) {
                try {
                    BufferedReader var3 = new BufferedReader(new FileReader(var1));
                    BufferedWriter var4 = new BufferedWriter(new FileWriter(var2));
                    String var5 = "";

                    for(var5 = var3.readLine(); var5 != null; var5 = var3.readLine()) {
                        var4.write(var5, 0, var5.length());
                        var4.write(10);
                    }

                    var3.close();
                    var4.close();
                } catch (Exception var6) {
                    this.showError("copyFile(" + var1 + ", " + var2 + "): " + var6.toString());
                }
            }

        }

        private int getRecordLength(String var1) {
            short var2 = 0;

            try {
                switch(this.fileType) {
                    case 0:
                    case 5:
                        switch(var1.charAt(0)) {
                            case '1':
                                var2 = 32;
                                return var2;
                            case '5':
                                var2 = 2219;
                                return var2;
                            default:
                                return var2;
                        }
                    case 1:
                        var2 = 500;
                        break;
                    case 2:
                    case 9:
                    case 23:
                        var2 = 94;
                        break;
                    case 3:
                        var2 = 168;
                        break;
                    case 4:
                        var2 = 330;
                        break;
                    case 6:
                        var2 = 400;
                        break;
                    case 7:
                        var2 = 170;
                    case 8:
                    case 17:
                    default:
                        break;
                    case 10:
                    case 11:
                        var2 = 261;
                        break;
                    case 12:
                        var2 = 152;
                        break;
                    case 13:
                        var2 = 128;
                        break;
                    case 14:
                        var2 = 170;
                        break;
                    case 15:
                        var2 = 300;
                        break;
                    case 16:
                    case 18:
                    case 28:
                        var2 = 128;
                        break;
                    case 19:
                    case 20:
                        var2 = 10;
                        break;
                    case 22:
                        var2 = 900;
                        break;
                    case 24:
                        var2 = 318;
                        break;
                    case 25:
                        var2 = 300;
                        break;
                    case 100:
                        var2 = 80;
                        break;
                    case 101:
                        var2 = 250;
                        break;
                    case 102:
                        var2 = 140;
                        break;
                    case 103:
                        var2 = 253;
                        break;
                    case 104:
                        var2 = 151;
                        break;
                    case 105:
                        var2 = 201;
                        break;
                    case 200:
                        var2 = 88;
                        break;
                    case 201:
                        var2 = 72;
                        break;
                    case 300:
                        var2 = 243;
                        break;
                    case 400:
                        var2 = 421;
                        break;
                    case 1000:
                        var2 = 2000;
                        break;
                    case 1001:
                        var2 = 202;
                        break;
                    case 1002:
                        var2 = 297;
                        break;
                    case 1003:
                        var2 = 108;
                        break;
                    case 1004:
                        var2 = 569;
                        break;
                    case 1005:
                        var2 = 337;
                        break;
                    case 1006:
                        var2 = 10;
                        break;
                    case 1007:
                        var2 = 121;
                        break;
                    case 1008:
                        var2 = 945;
                        break;
                    case 1009:
                        var2 = 128;
                        break;
                    case 1010:
                        var2 = 80;
                        break;
                    case 1100:
                        var2 = 220;
                        break;
                    case 1101:
                        var2 = 384;
                        break;
                    case 1102:
                        var2 = 768;
                        break;
                    case 2000:
                        var2 = 231;
                        break;
                    case 2001:
                        var2 = 180;
                        break;
                    case 2002:
                        var2 = 2144;
                        break;
                    case 3000:
                    case 3001:
                    case 3002:
                        var2 = 1270;
                        break;
                    case 4000:
                        var2 = 135;
                        break;
                    case 5000:
                    case 5001:
                        var2 = 130;
                }
            } catch (Exception var4) {
                this.showError("getRecordLength(" + var1 + "): " + var4.toString());
            }

            return var2;
        }

        private int getPaddedDate(int var1, StringBuffer var2, char[] var3) {
            int var4 = 0;
            String var5 = "";
            String var6 = "";
            String var7 = "";
            boolean var8 = false;
            StringBuffer var9 = new StringBuffer("");
            int var10 = var1;

            try {
                while(var1 - var10 < 9) {
                    var9.append(var3[var1++]);
                }

                for(StringTokenizer var11 = new StringTokenizer(var9.toString(), "/"); var11.hasMoreTokens(); ++var4) {
                    switch(var4) {
                        case 0:
                            var5 = var11.nextToken().trim();
                            break;
                        case 1:
                            var6 = var11.nextToken().trim();
                            break;
                        case 2:
                            var7 = var11.nextToken().trim();
                    }
                }

                if(var5.length() == 1) {
                    var5 = "0" + var5;
                }

                if(var6.length() == 1) {
                    var6 = "0" + var6;
                }

                if(var7.length() < 4) {
                    Calendar var12 = Calendar.getInstance();
                    int var13 = var12.get(1);
                    int var14 = var12.get(2);
                    int var15 = Integer.parseInt(var5);
                    int var17;
                    if(var15 != 11 && var15 != 12 || var14 != 0 && var14 != 1) {
                        var17 = var13;
                    } else {
                        var17 = var13 - 1;
                    }

                    var7 = Integer.toString(var17);
                }

                var2.append(var5);
                var2.append("/");
                var2.append(var6);
                var2.append("/");
                var2.append(var7);
            } catch (Exception var16) {
                this.showError("getPaddedDate(): " + var16.toString());
            }

            return var9.length();
        }

        private String fixRecord(String var1, OutputStream var2) {
            StringBuffer var3 = new StringBuffer("");
            String var4 = "";
            String var5 = "";
            String var6 = "";
            boolean var8 = false;
            String var9 = "Start of method";
            String var10 = "";

            try {
                if(var1 != null && !var1.equals("")) {
                    char[] var11;
                    var9 = "creating buf array (fileType = " + this.fileType + ") ->" + var1 + "<-";
                    var11 = var1.toCharArray();
                    int var7;
                    label260:
                    switch(this.fileType) {
                        case 0:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                        case 8:
                        case 9:
                        case 12:
                        case 14:
                        case 17:
                        case 22:
                        case 100:
                        case 101:
                        case 102:
                        case 103:
                        case 104:
                        case 105:
                        case 200:
                        case 201:
                        case 1000:
                        case 1001:
                        case 1003:
                        case 1008:
                        case 1009:
                        case 2000:
                        case 3000:
                        case 3001:
                        case 3002:
                        case 4000:
                        default:
                            break;
                        case 1:
                            if(var1.length() <= 2440) {
                                break;
                            }

                            if(var11[17] == 68 && var11[18] == 84) {
                                if(var11[236] == 32) {
                                    for(var7 = 107; var7 < 113; ++var7) {
                                        var3.append(var11[var7]);
                                    }

                                    var4 = this.getCardType(var3.toString());
                                    var11[236] = var4.charAt(0);
                                    var11[237] = var4.charAt(1);
                                }

                                if(var11[197] == 32) {
                                    var11[197] = 78;
                                    var11[198] = 65;
                                }
                            }

                            if(this.bankNumber.equals("3860")) {
                                String var15 = var1.substring(17, 19);
                                if(!var15.equals("TH") && !var15.equals("TT") && var1.substring(20, 26).equals("000000")) {
                                    var11[20] = 52;
                                    var11[21] = 48;
                                    var11[22] = 56;
                                    var11[23] = 49;
                                    var11[24] = 49;
                                    var11[25] = 54;
                                }
                            }
                            break;
                        case 2:
                            if(this.bankNumber.equals("3860") && var1.length() > 47 && var11[0] == 54 && var1.substring(39, 45).equals("000000")) {
                                var11[39] = 52;
                                var11[40] = 48;
                                var11[41] = 56;
                                var11[42] = 49;
                                var11[43] = 49;
                                var11[44] = 54;
                            }
                            break;
                        case 6:
                            if(var1.length() > 62 && var11[0] == 80 && var11[1] == 76) {
                                if(var11[51] != 43) {
                                    var11[51] = 43;
                                }

                                if(var11[62] != 43) {
                                    var11[62] = 43;
                                }
                            }

                            if(var1.substring(0, 2).equals("FF")) {
                                if(this.footerFound) {
                                    var11[1] = 85;
                                } else {
                                    this.footerFound = true;
                                }
                            }
                            break;
                        case 10:
                        case 11:
                            if(var1.length() > 20 && this.bankNumber.equals("3867") && this.fileType == 10 && var1.substring(11, 16).equals("EXT2*") && this.cbStoredVal.charAt(4) == 50) {
                                var3.setLength(0);
                                var3.append(var1.substring(0, 16));
                                var3.append("0000");
                                var3.append(var1.substring(16));
                                var11 = var3.toString().toCharArray();
                            }

                            if(var1.length() > 20 && this.bankNumber.equals("3858") && var1.substring(11, 16).equals("EXT2*") && this.cbStoredVal.charAt(4) == 50) {
                                var3.setLength(0);
                                var3.append(var1.substring(0, 16));
                                var3.append("0000");
                                var3.append(var1.substring(16));
                                var11 = var3.toString().toCharArray();
                            }

                            if(var1.length() > 20 && this.bankNumber.equals("3858") && this.fileType == 11 && var1.substring(11, 16).equals("EXT2*") && this.cbStoredVal.charAt(4) == 51) {
                                var3.setLength(0);
                                var3.append(var1.substring(0, 16));
                                var3.append("0000");
                                var3.append(var1.substring(16));
                                var11 = var3.toString().toCharArray();
                            }

                            if(var1.length() > 11 && var1.substring(7, 11).equals("9012")) {
                                this.cbStoredVal = var1.substring(15, 20);
                            } else {
                                if(var11[7] == 57 || this.cbStoredVal == null || this.cbStoredVal.equals("")) {
                                    break;
                                }

                                var3.setLength(0);
                                var3.append(var11);
                                int var12 = var3.length();

                                for(var7 = 0; var7 < 256 - var12; ++var7) {
                                    var3.append(' ');
                                }

                                var3.append(this.cbStoredVal);
                                var11 = var3.toString().toCharArray();
                            }
                            break;
                        case 13:
                            var5 = var1.substring(0, 4);
                            if(var5.equals(this.lastId)) {
                                break;
                            }

                            var3.append("BEGIN ");
                            var3.append(var5);
                            var2.write(var3.toString().getBytes());

                            for(var7 = 0; var7 < 20; ++var7) {
                                var2.write(32);
                            }

                            var2.write(this.uniqueName.getBytes());
                            var2.write(10);
                            this.lastId = var5;
                            this.curRec = 1;
                            break;
                        case 16:
                        case 18:
                        case 28:
                            if(this.fileType != 18 && this.lineCount % 2 <= 0) {
                                break;
                            }

                            var10 = var1.substring(7, 11);
                            if(!var10.equals("9010") && !var10.equals("9011") && !var10.equals("9012") && !var10.equals("9013") && !var10.equals("9104") && !var10.equals("9107")) {
                                if(Character.isDigit(var11[11])) {
                                    this.d256Code = "DT ";
                                } else {
                                    this.d256Code = "EX ";
                                }
                                break;
                            }

                            this.d256Code = "   ";
                            break;
                        case 21:
                        case 1004:
                            var7 = 0;

                            while(true) {
                                if(var7 >= var11.length) {
                                    break label260;
                                }

                                switch(Character.getType(var11[var7])) {
                                    case 1:
                                    case 2:
                                    case 9:
                                    case 12:
                                    case 20:
                                    case 21:
                                    case 22:
                                    case 23:
                                    case 24:
                                    case 25:
                                    case 26:
                                    case 27:
                                        if(this.hex(var11[var7]).equals("017D") || this.hex(var11[var7]).equals("00DB")) {
                                            var11[var7] = 32;
                                        }
                                        break;
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 10:
                                    case 11:
                                    case 13:
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                    case 18:
                                    case 19:
                                    default:
                                        var11[var7] = 32;
                                }

                                ++var7;
                            }
                        case 24:
                            for(var7 = 0; var7 < var1.length(); ++var7) {
                                if(9 == var1.charAt(var7)) {
                                    var10 = var10 + " ";
                                } else {
                                    var10 = var10 + var1.charAt(var7);
                                }
                            }

                            var11 = var10.toCharArray();
                            break;
                        case 300:
                            if(var1.length() <= 1 || var11[1] == 69) {
                                break;
                            }

                            var7 = 0;
                            int var14 = var7 + 20;

                            for(var9 = "moving to first date"; var7 < var14; ++var7) {
                                var3.append(var11[var7]);
                            }

                            var9 = "extracting first date";
                            var7 += this.getPaddedDate(var7, var3, var11);
                            var14 = var7 + 10;

                            for(var9 = "moving to second date"; var7 < var14; ++var7) {
                                var3.append(var11[var7]);
                            }

                            var9 = "extracting second date";
                            var7 += this.getPaddedDate(var7, var3, var11);
                            var14 = var7 + 155;

                            for(var9 = "moving to third date"; var7 < var14; ++var7) {
                                var3.append(var11[var7]);
                            }

                            var9 = "extracting third date";
                            var7 += this.getPaddedDate(var7, var3, var11);

                            for(var9 = "adding rest of buffer to workbuf"; var7 < var1.length(); ++var7) {
                                var3.append(var11[var7]);
                            }

                            var10 = var3.toString().substring(4, 19) + var3.toString().substring(80, 95);
                            if(this.rejectDuplicates.containsKey(var10)) {
                                var3.setLength(0);
                            } else {
                                this.rejectDuplicates.put(var10, "PLACEHOLDER");
                            }

                            var9 = "creating return buffer";
                            var11 = var3.toString().toCharArray();
                            break;
                        case 1002:
                            if(var1.length() >= 120) {
                                var11[119] = 32;
                                var11[120] = 32;
                            }
                    }

                    var6 = new String(var11);
                } else {
                    var6 = "";
                }
            } catch (Exception var13) {
                this.showError("fixRecord(" + var9 + "): " + var13.toString());
            }

            return var6;
        }

        public void initialize(String fileName) {
            int var2 = -1;
            this.props = this.loadProperties("transfer.properties");
            if(this.props.getProperty("transfer.database.enabled", "Y").equals("Y")) {
                SQLJConnectionBase.initStandalone();
                var2 = DBTools.getFileBankNumber(fileName, this.Ctx);
            }

            this.fileBase = fileName;
            boolean var3 = false;
            if(var2 == -1) {
                this.bankNumber = fileName.substring(fileName.length() - 4);
            } else {
                this.bankNumber = Integer.toString(var2);
            }

            if(fileName.substring(0, 3).equals("ddf")) {
                this.Bins.add(new FileTransfer.NDMFile.CardBin(300000L, 305999L, "DC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(308800L, 309499L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(309600L, 310299L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(311200L, 312099L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(315800L, 315999L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(333700L, 334999L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(340000L, 349999L, "AM"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(352800L, 358999L, "JC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(360000L, 369999L, "DC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(370000L, 379999L, "AM"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(380000L, 389999L, "DC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(400000L, 499999L, "VS"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(500000L, 599999L, "MC"));
                this.Bins.add(new FileTransfer.NDMFile.CardBin(601100L, 601199L, "DS"));
            }

        }

        public NDMFile(String var2) {
            this.initialize(var2);
        }

        public NDMFile(String var2, String var3) {
            this.initialize(var3);
            this.uniqueName = var2 + ".dat";
            this.flagFileName = var2 + ".flg";
            this.zipFileName = var2 + ".zip";
            this.prodZipFileName = var2 + ".dat.zip";
            this.rawFileName = var2 + ".raw";
            this.fileNameAssigned = true;
        }

        public NDMFile() {
        }

        private Properties loadProperties(String var1) {
            Properties var2 = null;

            try {
                if(this.fileExists(var1)) {
                    FileInputStream var3 = new FileInputStream(var1);
                    var2 = new Properties();
                    var2.load(var3);
                    var3.close();
                }
            } catch (Exception var4) {
                this.showError("loadProperties(): " + var4.toString());
            }

            return var2;
        }

        private String getCardType(String var1) {
            String var2 = "";

            try {
                long var3 = Long.parseLong(var1);

                for(int var5 = 0; var5 < this.Bins.size(); ++var5) {
                    FileTransfer.NDMFile.CardBin var6 = (FileTransfer.NDMFile.CardBin)this.Bins.elementAt(var5);
                    if(var3 >= var6.startBin && var3 <= var6.endBin) {
                        var2 = var6.cardType;
                        break;
                    }
                }
            } catch (Exception var7) {
                this.showError("getCardType(" + var1 + "): " + var7.toString());
            }

            return var2;
        }

        private String getExtendedProperty(Properties prop, String propertyName) {
            String var3 = "";

            try {
                String var4 = propertyName + this.fileBase;
                var3 = prop.getProperty(var4, "");
            } catch (Exception var5) {
                this.showError("getExtendedProperty(" + propertyName + "): " + var5.toString());
            }

            return var3;
        }

        private void extractFileProperties() {
            try {
                this.configPath = this.props.getProperty("transfer.config.path", "");
                this.debugMessage("  configPath:  " + this.configPath);
                this.sourcePath = this.getExtendedProperty(this.props, "transfer.file.source.");
                this.debugMessage("  sourcePath:  " + this.sourcePath);
                this.sourceFile = this.getExtendedProperty(this.props, "transfer.file.name.");
                this.debugMessage("  sourceFile:  " + this.sourceFile);
                if(this.fileNameAssigned) {
                    this.source = this.sourcePath + this.rawFileName;
                } else {
                    this.source = this.sourcePath + this.sourceFile;
                }

                this.debugMessage("  source: " + this.source);
                this.debugMessage("  destination: " + this.getExtendedProperty(this.props, "transfer.file.destination."));
                this.destination = this.getExtendedProperty(this.props, "transfer.file.destination.");
                this.debugMessage("  archive:     " + this.getExtendedProperty(this.props, "transfer.file.archive."));
                this.archive = this.getExtendedProperty(this.props, "transfer.file.archive.");
                this.debugMessage("  fileType:    " + this.getExtendedProperty(this.props, "transfer.file.type."));
                this.fileType = Integer.parseInt(this.getExtendedProperty(this.props, "transfer.file.type."));
                this.debugMessage("  extEmail:    " + this.getExtendedProperty(this.props, "transfer.file.email."));
                this.extEmail = this.getExtendedProperty(this.props, "transfer.file.email.");
                this.debugMessage("  useCompression: " + this.getExtendedProperty(this.props, "transfer.file.compress."));
                this.useCompression = !this.getExtendedProperty(this.props, "transfer.file.compress.").equals("N");
                this.debugMessage("  sendFileEmail: " + this.getExtendedProperty(this.props, "transfer.file.email."));
                this.sendFileEmail = !this.getExtendedProperty(this.props, "transfer.file.email.").equals("N");
                this.debugMessage("  delimiter: " + this.getExtendedProperty(this.props, "transfer.file.delim."));
                this.delimiter = this.getExtendedProperty(this.props, "transfer.file.delim.");
                this.debugMessage("  checkLength: " + this.getExtendedProperty(this.props, "transfer.file.checklength."));

                try {
                    this.checkLength = Integer.parseInt(this.getExtendedProperty(this.props, "transfer.file.checklength."));
                } catch (Exception var2) {
                    this.checkLength = -1;
                }
            } catch (Exception var3) {
                this.showError("extractFileProperties(): " + var3.toString());
            }

        }

        private String makeIntString(int var1, int var2) {
            StringBuffer var3 = new StringBuffer("");

            try {
                String var4 = Integer.toString(var1);

                for(int var5 = 0; var5 < var2 - var4.length(); ++var5) {
                    var3.append("0");
                }

                var3.append(var4);
            } catch (Exception var6) {
                this.showError("makeIntString(" + var1 + ", " + var2 + "): " + var6.toString());
            }

            return var3.toString();
        }

        private boolean fileExists(String var1) {
            boolean var2 = false;

            try {
                var2 = (new File(var1)).exists();
            } catch (Exception var4) {
                this.showError("fileExists(" + var1 + "): " + var4.toString());
            }

            return var2;
        }

        private void logEntry(String var1) {
            StringBuffer var2 = new StringBuffer("");

            try {
                this.debugMessage("Logging: " + var1);
                var2.append(this.props.getProperty("transfer.config.logfile", ""));
                var2.append(this.fileBase);
                var2.append(".log");
                BufferedWriter var3 = new BufferedWriter(new FileWriter(var2.toString(), true));
                SimpleDateFormat var4 = new SimpleDateFormat("[MM/dd/yy HH:mm:ss] ");
                String var5 = var4.format(Calendar.getInstance().getTime());
                var3.write(var5);
                var3.write(this.sourcePath);
                var3.write(this.uniqueName);
                var3.write(32);
                var3.write(var1);
                var3.newLine();
                var3.close();
            } catch (Exception var6) {
                ;
            }

        }

        private void showError(String message) {
            this.error = true;
            this.errorString = message;
            this.logEntry(message);
            this.sendEmail();
        }

        private void debugMessage(String message) {
            if(this.props.getProperty("transfer.debug", "N").equals("Y")) {
                System.out.println("NDMFile: " + message);
            }

        }

        public class CardBin {
            public long startBin = 0L;
            public long endBin = 0L;
            public String cardType = "";

            public CardBin(long var2, long var4, String var6) {
                this.startBin = var2;
                this.endBin = var4;
                this.cardType = var6;
            }
        }
    }
}
