/*************************************************************************

  FILE: $Archive: /Java/apps/transfer/GPSTransfer.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-11 12:57:47 -0700 (Wed, 11 Jul 2007) $
  Version            : $Revision: 13871 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mesapps.transfer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;
import java.util.Properties;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class GPSTransfer
{
  // constants
  public static final String    FILE_MAP_FILENAME       = "gps_file_map.txt";
  public static final String    PROP_FILE_NAME          = "transfer.properties";
  public static final String    GPS_FILE_DIR            = "gpsmes/";
  
  public static final int       POSTFIX_LENGTH          = 15;
  
  // class variables
  private HashMap               fileMap       = new HashMap();
  private boolean               debug         = true;
  private String                newName       = "";
  
  public GPSTransfer()
  {
  }
  
  private void buildFileMap()
  {
    BufferedReader in;
    try
    {
      in = new BufferedReader(new FileReader(FILE_MAP_FILENAME));
      
      String line = "";
      
      line = in.readLine();
      
      while(line != null)
      {
        // tokenize the line and add to the HashMap
        StringTokenizer tokenizer = new StringTokenizer(line, ":");
        
        // trust that there are exactly two tokens in this line
        String fileKey    = tokenizer.nextToken();
        String fileValue  = tokenizer.nextToken();
        
        // add key and value to the HashMap
        fileMap.put(fileKey, fileValue);
        
        // get the next line
        line = in.readLine();
      }
      
      in.close();
    }
    catch(Exception e)
    {
      System.out.println("buildFileMap(): " + e.toString());
    }
  }

  public static void main(String args[])
  {
    try
    {
      GPSTransfer gps = new GPSTransfer();
    
      gps.process(args);
    }
    catch(Exception e)
    {
      System.out.println("main()" + e.toString());
    }
    Runtime.getRuntime().exit(0);
  }
  
  private void getFileType(String fname)
  {
    String        trueName            = "";
    StringBuffer  outFileName         = new StringBuffer("");
    
    try
    {
      // iterate through all file names in map to see if it matches
      Iterator it = fileMap.keySet().iterator();
      
      while(it.hasNext())
      {
        String mapName = (String)(it.next());
        
        if(fname.length() >= mapName.length())
        {
          debugMessage("checking against mapName: (" + fname.substring(0, mapName.length()) + "/" + mapName + ")");
          
          if(fname.substring(0, mapName.length()).equals(mapName))
          {
            debugMessage("found map name: " + mapName);
            trueName = fname.substring(0, mapName.length());
            break;
          }
        }
      }
      
      // get output name from hashmap
      debugMessage("true name is: " + trueName);
      
      outFileName.append((String)(fileMap.get(trueName)));
      
      newName = outFileName.toString();
      debugMessage("new file name is: " + newName);
    }
    catch(Exception e)
    {
      System.out.println("getFileType(" + fname + "): " + e.toString());
    }
  }
  
  private void logEntry(String msg)
  {
    try
    {
      BufferedWriter logFile = new BufferedWriter(new FileWriter("gpsrename.log", true));
      
      // get date and time
      DateFormat df = new SimpleDateFormat("[MM/dd/yy HH:mm:ss] ");
      String dateStamp = df.format(Calendar.getInstance().getTime());
      
      logFile.write(dateStamp);
      logFile.write(msg);
      logFile.newLine();
      logFile.close();
    }
    catch(Exception e)
    {
    }
  }
  
  private void renameFile(String oldName, String newName)
  {
    try
    {
      debugMessage("renaming " + oldName + " to " + newName);
      logEntry("renaming " + oldName + " to " + newName);
      new File(oldName).renameTo(new File(newName));
    }
    catch(Exception e)
    {
      System.out.println("renameFile(" + oldName + ", " + newName + "): " + e.toString());
    }
  }
  
  private void process(String args[])
  {
    try
    {
      if(args.length == 1)
      {
        // build map of filenames
        debugMessage("Building file map");
        buildFileMap();
        
        // get properties (primarily to see if we are in debug mode)
        debugMessage("Getting properties");
        FileInputStream in      = new FileInputStream(PROP_FILE_NAME);
        Properties      props   = new Properties();
        props.load(in);
        in.close();
        
        debugMessage("getting debug property");
        debug = props.getProperty("transfer.debug", "N").equals("Y");
        
        // first and only arg is the name of the file to process
        // determine file type from prefix
        debugMessage("getting file type");
        getFileType(args[0]);
        
        if(newName.equals(""))
        {
          throw new Exception("newName is null");
        }
        
        // rename old file to new dat file 
        debugMessage("renaming file");
        renameFile(GPS_FILE_DIR + args[0], GPS_FILE_DIR + newName + ".dat");
        
        // now process new file
        debugMessage("Calling FileTransfer on " + newName);
        FileTransfer ndm = new FileTransfer(newName);
        ndm.transfer();
      }
      else
      {
        System.out.println("ERROR: No file specified");
      }
      
      debugMessage("Process complete");
    }
    catch(Exception e)
    {
      System.out.println("process(): " + e.toString());
    }
  }
  
  private void debugMessage(String message)
  {
    if(debug)
    {
      System.out.println("GPSTransfer: " + message);
    }
  }
}
