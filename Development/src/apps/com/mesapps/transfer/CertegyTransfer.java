/*************************************************************************

  FILE: $Archive: /Java/apps/transfer/CertegyTransfer.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/30/04 3:44p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mesapps.transfer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.BufferedReader;

import java.util.Calendar;
import java.util.Date;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;
import java.util.Properties;
import java.util.HashMap;
import java.util.StringTokenizer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CertegyTransfer
{
  public static final int       ZIP_BUFFER_LEN          = 4096;
  
  public static final String    PREFIX_ACH              = "esachsq";
  public static final String    PREFIX_CHARGEBACK       = "escbisq";
  public static final String    PREFIX_DAILY_DETAIL     = "esitmsq";
  public static final String    PREFIX_MIF              = "esmstsq";
  public static final String    PREFIX_RETRIEVAL        = "esrtvsq";
  public static final String    PREFIX_TC33             = "estc33";
  public static final String    PREFIX_TC33_2           = "esmtc33";
  public static final String    PREFIX_GROUP_NAMES      = "esprmsq";
  public static final String    PREFIX_STATEMENT_1      = "stmtpr1r";
  public static final String    PREFIX_STATEMENT_2      = "stmtpr2r";
  public static final String    PREFIX_STATEMENT_3      = "stmtpr3r";
  public static final String    PREFIX_STATEMENT_4      = "stmtpr4r";
  public static final String    PREFIX_STATEMENT_5      = "stmtpr5r";
  public static final String    PREFIX_EXTRACT          = "meprofrr";
  public static final String    PREFIX_CHG_TBL          = "estbl01";
  public static final String    PREFIX_INDEX_FILE       = "esidxsq";
  
  public static final String    PREFIX_BHEADER          = "esbhdsq";
  public static final String    PREFIX_PAYTECH          = "esptech";
  public static final String    PREFIX_ADJUSTMENT       = "esadjsq";
  public static final String    PREFIX_NEW_CB           = "eschgsq";

  public static final int       CERTEGY_FT_UNKNOWN      = -1;  
  public static final int       CERTEGY_FT_ACH          = 1;
  public static final int       CERTEGY_FT_CHARGEBACK   = 2;
  public static final int       CERTEGY_FT_DAILY_DETAIL = 3;
  public static final int       CERTEGY_FT_MIF          = 4;
  public static final int       CERTEGY_FT_RETRIEVAL    = 5;
  public static final int       CERTEGY_FT_TC33         = 6;
  public static final int       CERTEGY_FT_GROUP_NAMES  = 7;
  public static final int       CERTEGY_FT_STATEMENT    = 8;
  public static final int       CERTEGY_FT_EXTRACT      = 9;
  public static final int       CERTEGY_FT_CHG_TBL      = 10;
  public static final int       CERTEGY_FT_INDEX        = 11;
  
  public static final int       CERTEGY_FT_BHEADER      = 100;
  public static final int       CERTEGY_FT_PAYTECH      = 101;
  public static final int       CERTEGY_FT_ADJUSTMENT   = 102;
  public static final int       CERTEGY_FT_NEW_CB       = 103;
  
  public static final String    FILE_MAP_FILENAME       = "file_map.txt";

  public static final String    ARCHIVE_PATH            = "arc/";  
  public static final String    OUTFILE_PATH            = "vital/daily/";
  
  public static final String    PROP_FILE_NAME          = "transfer.properties";
  
  private static String         fileName      = "";
  private static String         filePrefix    = "";
  private static int            fileType      = -1;
  private static String         outFileName   = "";
  
  private static boolean        debug         = true;
  
  private static HashMap        fileMap       = new HashMap();
  
  private static String         arcFileName   = "";
  
  private static void renameFile(String fname)
  {
    try
    {
      // get date/time string to rename this file
      Date          today       = Calendar.getInstance().getTime();
      StringBuffer  newFileName = new StringBuffer(ARCHIVE_PATH);
      String        dateString  = ((DateFormat)(new SimpleDateFormat("-MMddyy-hhmmss-SSS"))).format(today);
      
      newFileName.append(fname);
      newFileName.append(dateString);
      
      arcFileName = newFileName.toString();
      
      File  thisFile   = new File(fname);
      
      debugMessage("  renaming '" + fname + "' to '" + newFileName.toString() + "'");
      thisFile.renameTo(new File(newFileName.toString()));
      
      fileName = newFileName.toString();
    }
    catch(Exception e)
    {
      System.out.println("renameFile(" + fname + "): " + e.toString());
    }
  }
  
  private static void extractFile()
  {
    byte[]            zipData   = new byte[ZIP_BUFFER_LEN];
    int               bytesRead = 0;
    
    try
    {
      FileInputStream   zipIn   = new FileInputStream(fileName);
      ZipInputStream    zin     = new ZipInputStream(zipIn);
      
      FileOutputStream  outFile = new FileOutputStream(OUTFILE_PATH + outFileName);
      
      ZipEntry          zentry  = zin.getNextEntry();
      
      while(bytesRead != -1)
      {
        bytesRead = zin.read(zipData);
        
        if(bytesRead != -1)
        {
          // write to file
          outFile.write(zipData, 0, bytesRead);
        }
      }
      
      zin.close();
      outFile.close();
    }
    catch(Exception e)
    {
      System.out.println("extractFile(): " + e.toString());
    }    
  }
  
  private static void buildFileMap()
  {
    BufferedReader in;
    try
    {
      in = new BufferedReader(new FileReader(FILE_MAP_FILENAME));
      
      String line = "";
      
      line = in.readLine();
      
      while(line != null)
      {
        // tokenize the line and add to the HashMap
        StringTokenizer tokenizer = new StringTokenizer(line, ":");
        
        // trust that there are exactly two tokens in this line
        String fileKey    = tokenizer.nextToken();
        String fileValue  = tokenizer.nextToken();
        
        // add key and value to the HashMap
        fileMap.put(fileKey, fileValue);
        
        // get the next line
        line = in.readLine();
      }
      
      in.close();
    }
    catch(Exception e)
    {
      System.out.println("buildFileMap(): " + e.toString());
    }
  }

  public static void main(String args[])
  {
    try
    {
      if(args.length == 1)
      {
        // build map of filenames
        buildFileMap();
        
        // get properties (primarily to see if we are in debug mode)
        FileInputStream in      = new FileInputStream(PROP_FILE_NAME);
        Properties      props   = new Properties();
        props.load(in);
        in.close();
        
        debug = props.getProperty("transfer.debug", "N").equals("Y");
        
        // first and only arg is the name of the file to process
        // rename the file to something unique so we can accept new files
        debugMessage("\n\nRenaming file...");
        renameFile(args[0]);

        // get the prefix
        debugMessage("Getting prefix...");
        filePrefix = fileName.substring(ARCHIVE_PATH.length(), fileName.indexOf('.'));
        debugMessage("  prefix = " + filePrefix);

        // build the HashMap of filename mappings
        debugMessage("Establishing output file name");
        outFileName = (String)(fileMap.get(filePrefix));
        debugMessage("Output filename: " + outFileName);
        
        // only do the following if this is a valid (used) certegy file type
        if(outFileName != null)
        {
          // extract the file into the proper output directory
          debugMessage("Extracting file");
          extractFile();
          
          // now process the file normally using the NDMFile
          FileTransfer ndm   = new FileTransfer(outFileName.substring(0, outFileName.indexOf('.')));
          ndm.transfer();
          
          // if this was successful then remove the archived version to save disk space
          // temporarily turn this off for tc33 files
          if(!filePrefix.substring(0, 4).equals("tc33"))
          {
            debugMessage("Deleting temp archive file");
            File delFile = new File(arcFileName);
            delFile.delete();
          }
        }
        else
        {
          debugMessage("File is of unknown type.  It has been renamed but will not be processed");
        }
      }
      else
      {
        System.out.println("ERROR: No file specified");
      }
      
      debugMessage("Process complete");
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
  
  private static void debugMessage(String message)
  {
    if(debug)
    {
      System.out.println("CertegyTransfer: " + message);
    }
  }
}
