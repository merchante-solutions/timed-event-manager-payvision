/*************************************************************************

  FILE: $Archive: /Java/apps/transfer/CertegyTransfer.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/30/04 3:44p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mesapps.transfer;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Iterator;

public class RenameFile
{
  public static final String  FILE_MAP_FILENAME   = "file_map.txt";
  
  private static HashMap      fileMap             = new HashMap();
  
  public RenameFile()
  {
  }
  
  private void buildFileMap()
    throws Exception 
  {
    BufferedReader in;
    
    in = new BufferedReader(new FileReader(FILE_MAP_FILENAME));
    
    String line = in.readLine();
    
    while(line != null)
    {
      StringTokenizer tokenizer = new StringTokenizer(line, ":");
      
      String fileKey = tokenizer.nextToken();
      
      FileMapper mapper = new FileMapper(fileKey, tokenizer.nextToken());
      
      // add key and value to HashMap
      fileMap.put(fileKey, mapper);
      
      // get next line
      line = in.readLine();
    }
  }
  
  private void run(String fileName)
  {
    try
    {
      // build hashmap of filename mappings
      buildFileMap();
      
      File newFile = new File(fileName);
      
      Iterator fileKeys = fileMap.keySet().iterator();
      
      while(fileKeys.hasNext())
      {
        String fileKey = (String)(fileKeys.next());
        
        // get FileMapper object associated with this key
        if(fileMap.containsKey(fileKey))
        {
          FileMapper mapper = (FileMapper)(fileMap.get(fileKey));
          
          // determine if this is the right map
          if(newFile.getName().substring(0, mapper.fileKey.length()).equals(mapper.fileKey))
          {
            // rename file
            newFile.renameTo(new File(mapper.fileValue));
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::run(): " + e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    if(args.length == 1)
    {
      RenameFile renameFile = new RenameFile();
    
      renameFile.run(args[0]);
    }
    else
    {
      System.out.println("Wrong number of args to RenameFile (" + args.length + ")");
    }
  }
  
  public class FileMapper
  {
    public String fileKey;
    public String fileValue;
    
    public FileMapper(String _fileKey, String _fileValue)
    {
      System.out.println("Creating new FileMapper:");
      System.out.println("  key: " + _fileKey);
      System.out.println("  val: " + _fileValue);
      fileKey = _fileKey;
      fileValue = _fileValue;
    }
  }
}
