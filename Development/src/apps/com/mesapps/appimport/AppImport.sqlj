package com.mesapps.appimport;

import java.util.Vector;
import java.util.HashMap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StreamTokenizer;
import java.io.StringReader;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

import com.mes.app.AppSequence;
import com.mes.app.AppSeqNumGenerator;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.AutoApprove;

public class AppImport extends SQLJConnectionBase
{
  private String      fileName  = "";
  private Vector      apps      = new Vector();
  private int         appType;
  private AppSequence appSeq;
  private HashMap     BusinessType    = null;
  private HashMap     IndustryType    = null;
  private HashMap     CompanyType     = null;
  private HashMap     LocationType    = null;
  private HashMap     RefundType      = null;
  private HashMap     PricingType     = null;
  private HashMap     Equipment       = null;
  private HashMap     DiscRateType    = null;
  
  public static final int APP_FIELD_COUNT = 170;
  
  public static final String  TRAINING_MES    = "M";
  public static final String  TRAINING_REP    = "S";
  public static final String  TRAINING_NONE   = "N";
  
  private String      phoneTraining   = TRAINING_NONE;
  
  public AppImport(String _fileName, int _appType, String _phoneTraining)
  {
    fileName  = _fileName;
    appType   = _appType;
    phoneTraining = _phoneTraining;
  }
  
  private void loadHashMaps()
    throws Exception 
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      BusinessType = new HashMap();
      
      #sql [Ctx] it =
      {
        select  lower(bustype_desc) bustype_desc,
                bustype_code
        from    bustype
      };
      rs = it.getResultSet();
      while(rs.next())
      {
        BusinessType.put(rs.getString("bustype_desc"), new Integer(rs.getInt("bustype_code")));
      }
      rs.close();
      it.close();
      BusinessType.put("default", new Integer(1));
      BusinessType.put("sole prop", new Integer(1));
      BusinessType.put("sole proprietor", new Integer(1));
      BusinessType.put("corp", new Integer(2));
      BusinessType.put("corporation", new Integer(2));
      BusinessType.put("llc", new Integer(8));
      BusinessType.put("not for profit corp", new Integer(6));
      BusinessType.put("not for profit org", new Integer(6));
      
      IndustryType = new HashMap();
      #sql [Ctx] it =
      {
        select lower(industype_descr) industype_descr,
               industype_code
        from   industype
      };
      rs = it.getResultSet();
      while(rs.next())
      {
        IndustryType.put(rs.getString("industype_descr"), new Integer(rs.getInt("industype_code")));
      }
      rs.close();
      it.close();
      IndustryType.put("default", new Integer(1));
      
      CompanyType = new HashMap();
      CompanyType.put("default", new Integer(1));
      CompanyType.put("single", new Integer(1));
      CompanyType.put("single outlet", new Integer(1));
      CompanyType.put("chain", new Integer(2));
      
      LocationType = new HashMap();
      #sql [Ctx] it =
      {
        select  lower(loctype_desc) loctype_desc,
                loctype_code
        from    loctype
      };
      rs = it.getResultSet();
      while(rs.next())
      {
        LocationType.put(rs.getString("loctype_desc"), new Integer(rs.getInt("loctype_code")));
      }
      rs.close();
      it.close();
      LocationType.put("default", new Integer(1));
      LocationType.put("retail storefront", new Integer(1));
      LocationType.put("store front", new Integer(1));
      LocationType.put("storefront", new Integer(1));
      LocationType.put("internet storefront", new Integer(2));
      LocationType.put("business office", new Integer(3));
      LocationType.put("private residence", new Integer(4));
      LocationType.put("residence", new Integer(4));
      LocationType.put("other", new Integer(5));
      
      RefundType = new HashMap();
      #sql [Ctx] it =
      {
        select  lower(refundtype_desc)  refundtype_desc,
                refundtype_code
        from    refundtype
      };
      rs = it.getResultSet();
      while(rs.next())
      {
        RefundType.put(rs.getString("refundtype_desc"), new Integer(rs.getInt("refundtype_code")));
      }
      rs.close();
      it.close();
      RefundType.put("default", new Integer(2));
      
      PricingType = new HashMap();
      PricingType.put("default", new Integer(1));
      PricingType.put("retail", new Integer(1));
      PricingType.put("moto", new Integer(2));
      PricingType.put("hotel", new Integer(3));
      
      Equipment = new HashMap();
      Equipment.put("hypercom t7e", new EquipDetails("HPT7EIP", 2, 5));
      Equipment.put("hypercom t7p", new EquipDetails("HPT7PRIP", 2, 5));
      Equipment.put("hypercom t7p-t", new EquipDetails("HPT7PTIP", 2, 5));
      Equipment.put("lipman 2085", new EquipDetails("NRT2085", 10, 5));
      Equipment.put("verifone omni 3200", new EquipDetails("VFOMNI3200IP", 1, 5));
      Equipment.put("verifone omni 396", new EquipDetails("VFOMNI396", 1, 1));
      Equipment.put("verifone trans 380 x 2", new EquipDetails("VFTRANZ380X2", 1, 1));
      Equipment.put("verifone tranz 330", new EquipDetails("VFTRANZ330", 1, 1));
      Equipment.put("verifone tranz 380", new EquipDetails("VFTRANZ380", 1, 1));
      Equipment.put("verifone tranz 460", new EquipDetails("VFTRANZ460IP", 1, 5));
      Equipment.put("p250", new EquipDetails("VFP250", 1, 2));
      Equipment.put("nurit 3010 motient", new EquipDetails("NRT3010MOTIENT", 10, 5));
      Equipment.put("citizen", new EquipDetails("CIDP560", 8, 2));
      Equipment.put("omni 3200", new EquipDetails("VFOMNI3200IP", 1, 5));
      Equipment.put("tranz 330", new EquipDetails("VFTRANZ330", 1, 1));
      Equipment.put("p-250", new EquipDetails("VFP250", 1, 2));
      
      DiscRateType = new HashMap();
      DiscRateType.put("default", new Integer(2));
      DiscRateType.put("r", new Integer(2));
      DiscRateType.put("i", new Integer(3));
      DiscRateType.put("v", new Integer(4));
      DiscRateType.put("n/a", new Integer(2));
    }
    catch(Exception e)
    {
      System.out.println("loadHashMaps(): " + e.toString());
      throw e;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void createApp(AppImportApp app)
    throws Exception
  {
    try
    {
      // create appSequence
      if(appSeq == null)
      {
        appSeq = new AppSequence(Ctx);
        appSeq.setAppType(appType);
      }
      
      // create record in application table
      #sql [Ctx]
      {
        insert into application
        (
          app_seq_num,
          appsrctype_code,
          app_created_date,
          app_scrn_code,
          app_user_id,
          app_user_login,
          app_type,
          screen_sequence_id
        )
        values
        (
          :(app.appGen.getAppSeqNum()),
          :(appSeq.getAppSourceType()),
          sysdate,
          null,
          301413,
          'mesappgen',
          :(appSeq.getAppType()),
          :(appSeq.getSeqId())
        )
      };
      
      // create record in merchant table
      #sql [Ctx]
      {
        insert into merchant
        (
          app_seq_num,
          merc_cntrl_number,
          merch_bank_number
        )
        values
        (
          :(app.appGen.getAppSeqNum()),
          :(app.appGen.getControlNumber()),
          :(appSeq.getBankNum())
        )
      };
      
    }
    catch(Exception e)
    {
      System.out.println("createApp(): " + e.toString());
      throw e;
    }
  }
  
  private String clean(String raw)
    throws Exception
  {
    StringBuffer result = new StringBuffer();
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          result.append(raw.charAt(i));
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("clean(" + raw + "): " + e.toString());
      throw e;
    }
    return result.toString();
  }
  
  private int getMapType(HashMap map, String key)
    throws Exception
  {
    int result = 1;
    try
    {
      if(map.containsKey(key.toLowerCase()))
      {
        result = ((Integer)(map.get(key.toLowerCase()))).intValue();
      }
      else
      {
        result = ((Integer)(map.get("default"))).intValue();
      }
    }
    catch(Exception e)
    {
      System.out.println("getMapType(" + key + "): " + e.toString());
      throw e;
    }
    return result;
  }
  
  private String flagIfPresent(String field)
    throws Exception
  {
    String result = "Y";
    try
    {
      if(field.equals(""))
      {
        result = "N";
      }
    }
    catch(Exception e)
    {
      System.out.println("flagIfPresent("+field+"): " + e.toString());
      throw e;
    }
    return result;
  }
  
  private String yesNo(String test, String def)
    throws Exception
  {
    String result = def;
    try
    {
      if(test.toLowerCase().startsWith("y"))
      {
        result = "Y";
      }
      else if(test.toLowerCase().startsWith("n"))
      {
        result = "N";
      }
    }
    catch(Exception e)
    {
      System.out.println("yesNo(" + test + "): " + e.toString());
      throw e;
    }
    return result;
  }
  
  private void importMerchant(AppImportApp app)
    throws Exception
  {
    try
    {
      // account type
      String accType = "C";
      if(app.field(0).equals("New"))
      {
        accType = "N";
      }
      
      int locNum = (app.isBlank(29) ? 1 : Integer.parseInt(app.field(29)));
      
      #sql[Ctx]
      {
        update  merchant
        set     account_type = :accType,
                asso_number = :(app.field(1)),
                merch_business_name = substr(:(app.field(2)), 1, 25),
                merch_legal_name = :(app.field(3)),
                merch_federal_tax_id = :(clean(app.field(4))),
                merch_email_address = :(app.field(11)),
                merch_business_establ_month = :(app.field(12)),
                merch_business_establ_year = :(app.field(13)),
                bustype_code = :(getMapType(BusinessType,app.field(24))),
                industype_code = :(getMapType(IndustryType,app.field(25))),
                merch_busgoodserv_descr = :(app.field(26)),
                app_sic_code = :(app.field(27)),
                sic_code = :(app.field(27)),
                merch_mcc = :(app.field(27)),
                merch_application_type = :(getMapType(CompanyType,app.field(28))),
                merch_num_of_locations = :locNum,
                loctype_code = :(getMapType(LocationType,app.field(30))),
                merch_years_at_loc = :(app.field(31)),
                total_num_multi_merch = :(app.field(32)),
                num_multi_merch = :(app.field(33)),
                multi_merch_master_dba = :(app.field(34)),
                multi_merch_master_cntrl = :(app.field(35)),
                merch_prior_cc_accp_flag = :(yesNo(app.field(36),"N")),
                merch_prior_processor = :(app.field(37)),
                merch_prior_statements = :(yesNo(app.field(38),"N")),
                merch_cc_acct_term_flag = :(yesNo(app.field(39),"N")),
                merch_cc_term_name = :(app.field(40)),
                merch_term_reason = :(app.field(41)),
                merch_term_month = :(app.field(42)),
                merch_term_year = :(app.field(43)),
                merch_month_tot_proj_sales = :(app.field(77)),
                merch_month_visa_mc_sales = :(app.field(78)),
                merch_average_cc_tran = :(app.field(79)),
                merch_mail_phone_sales = :(app.field(80)),
                refundtype_code = :(getMapType(RefundType,app.field(81))),
                pricing_grid = :(getMapType(PricingType,app.field(87)))
        where   app_seq_num = :(app.appGen.getAppSeqNum())
      };
    }
    catch(Exception e)
    {
      System.out.println("importMerchant(): " + e.toString());
      throw e;
    }
  }
  
  private void insertAddress( long appSeqNum,
                              int addressType,
                              String  name,
                              String  address1,
                              String  address2,
                              String  city,
                              String  state,
                              String  zip,
                              String  phone,
                              String  fax)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into address
        (
          app_seq_num,
          addresstype_code,
          address_name,
          address_line1,
          address_line2,
          address_city,
          countrystate_code,
          address_zip,
          address_phone,
          address_fax
        )
        values
        (
          :appSeqNum,
          :addressType,
          substr(:name, 1, 32),
          substr(:address1, 1, 32),
          substr(:address2, 1, 32),
          substr(:city, 1, 25),
          substr(:state, 1, 2),
          :zip,
          :phone,
          :fax
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("insertAddress(" + addressType + "): " + e.toString());
      throw e;
    }
  }
  
  private void importAddress(AppImportApp app)
    throws Exception
  {
    try
    {
      // business address
      insertAddress(app.appGen.getAppSeqNum(),
                    1,
                    app.field(2),
                    app.field(6),
                    app.field(7),
                    app.field(8),
                    app.field(9),
                    app.field(10),
                    clean(app.field(5)),
                    clean(app.field(23)));
    
      // mailing address
      if(app.isBlank(18))
      {
        // use business address for mailing
        insertAddress(app.appGen.getAppSeqNum(),
                      2,
                      app.field(2),
                      app.field(6),
                      app.field(7),
                      app.field(8),
                      app.field(9),
                      app.field(10),
                      clean(app.field(5)),
                      clean(app.field(23)));
      }
      else
      {
        insertAddress(app.appGen.getAppSeqNum(),
                      2,
                      app.field(17),
                      app.field(18),
                      app.field(19),
                      app.field(20),
                      app.field(21),
                      app.field(22),
                      "",
                      "");
      }
                    
      // owner 1
      insertAddress(app.appGen.getAppSeqNum(),
                    4,
                    app.field(53) + " " + app.field(54),
                    app.field(57),
                    "",
                    app.field(58),
                    app.field(59),
                    app.field(60),
                    clean(app.field(64)),
                    "");
    
      // owner 2
      insertAddress(app.appGen.getAppSeqNum(),
                    5,
                    app.field(65) + " " + app.field(66),
                    app.field(69),
                    "",
                    app.field(70),
                    app.field(71),
                    app.field(72),
                    clean(app.field(76)),
                    "");
                    
      // bank address
      insertAddress(app.appGen.getAppSeqNum(),
                    9,
                    app.field(44),
                    app.field(49),
                    "",
                    app.field(50),
                    app.field(51),
                    app.field(52),
                    "",
                    "");
                    
      // shipping address
      if(app.field(161).toLowerCase().equals("m"))
      {
          // use mailing address
        insertAddress(app.appGen.getAppSeqNum(),
                      3,
                      app.field(17),
                      app.field(18),
                      app.field(19),
                      app.field(20),
                      app.field(21),
                      app.field(22),
                      "",
                      "");
      }
      else
      {
        // use business address
        insertAddress(app.appGen.getAppSeqNum(),
                      3,
                      app.field(2),
                      app.field(6),
                      app.field(7),
                      app.field(8),
                      app.field(9),
                      app.field(10),
                      clean(app.field(5)),
                      clean(app.field(23)));
      }
    }
    catch(Exception e)
    {
      System.out.println("importAddress(): " + e.toString());
      throw e;
    }
  }
  
  private void importContact(AppImportApp app)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into merchcontact
        (
          app_seq_num,
          merchcont_prim_first_name,
          merchcont_prim_last_name,
          merchcont_prim_phone
        )
        values
        (
          :(app.appGen.getAppSeqNum()),
          :(app.field(14)),
          :(app.field(15)),
          :(clean(app.field(16)))
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("importContact(): " + e.toString());
      throw e;
    }
  }
  
  private void importBank(AppImportApp app)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into merchbank
        (
          app_seq_num,
          merchbank_name,
          merchbank_num_years_open,
          merchbank_acct_num,
          merchbank_transit_route_num,
          merchbank_info_source,
          bankacc_type,
          merchbank_acct_srnum
        )
        values
        (
          :(app.appGen.getAppSeqNum()),
          :(app.field(44)),
          :(app.field(45)),
          :(clean(app.field(46))),
          :(clean(app.field(47))),
          substr(:(app.field(48)),1,30),
          2,
          1
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("importBank(): " + e.toString());
      throw e;
    }
  }
  
  private void insertBusOwner(long appSeqNum,
                              int busOwner,
                              String firstName,
                              String lastName,
                              String ssn,
                              String ownerPerc,
                              String periodMonth,
                              String periodYear,
                              String title)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into businessowner
        (
          app_seq_num,
          busowner_num,
          busowner_first_name,
          busowner_last_name,
          busowner_ssn,
          busowner_owner_perc,
          busowner_period_month,
          busowner_period_year,
          busowner_title
        )
        values
        (
          :appSeqNum,
          :busOwner,
          :firstName,
          :lastName,
          :(clean(ssn)),
          :ownerPerc,
          :periodMonth,
          :periodYear,
          :title
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("insertBusOwner(" + appSeqNum + ", " + busOwner + "): " + e.toString());
      throw e;
    }
  }
                              
  private void importBusinessOwner(AppImportApp app)
    throws Exception
  {
    try
    {
      // Primary Business Owner
      insertBusOwner(app.appGen.getAppSeqNum(),
                     1,
                     app.field(53),
                     app.field(54),
                     app.field(55),
                     app.field(56),
                     app.field(61),
                     app.field(62),
                     app.field(63));
    
      if(!app.isBlank(65))
      {
        // Secondary owner
        insertBusOwner(app.appGen.getAppSeqNum(),
                       2,
                       app.field(65),
                       app.field(66),
                       app.field(67),
                       app.field(68),
                       app.field(73),
                       app.field(74),
                       app.field(75));
      }
    }
    catch(Exception e)
    {
      System.out.println("importBusinessOwner(): " + e.toString());
      throw e;
    }
  }
  
  private void importEquipment(AppImportApp app)
    throws Exception
  {
    try
    {
      // imprinter plates
      if(!app.isBlank(82) && Integer.parseInt(app.field(82)) > 0)
      {
        #sql [Ctx]
        {
          insert into merchequipment
          (
            app_seq_num,
            equiptype_code,
            equiplendtype_code,
            equip_model,
            merchequip_equip_quantity
          )
          values
          (
            :(app.appGen.getAppSeqNum()),
            9,
            :(mesConstants.APP_EQUIP_PURCHASE),
            'IPPL',
            :(app.field(82))
          )
        };
      }
      
      // owned equipment
      if(!app.isBlank(118))
      {
        // retrieve equip details from hash map
        if(Equipment.containsKey(app.field(118).toLowerCase()))
        {
          EquipDetails ed = (EquipDetails)(Equipment.get(app.field(118).toLowerCase()));
          
          int numTerms = 1;
          
          if(!app.isBlank(122))
          {
            numTerms = Integer.parseInt(app.field(122));
          }
          
          #sql [Ctx]
          {
            insert into merchequipment
            (
              app_seq_num,
              equiptype_code,
              equiplendtype_code,
              merchequip_equip_quantity,
              prod_option_id,
              equip_model,
              merchequip_amount,
              quantity_deployed
            )
            values
            (
              :(app.appGen.getAppSeqNum()),
              :(ed.type),
              3,
              :numTerms,
              1,
              :(ed.model),
              0,
              0
            )
          };
        }
        else
        {
          throw new Exception("Unknown Equipment: " + app.field(118));
        }
      }
      
      // printer
      if(!app.isBlank(124))
      {
        if(Equipment.containsKey(app.field(124).toLowerCase()))
        {
          EquipDetails ed = (EquipDetails)(Equipment.get(app.field(124).toLowerCase()));
          
          #sql [Ctx]
          {
            insert into merchequipment
            (
              app_seq_num,
              equiptype_code,
              equiplendtype_code,
              merchequip_equip_quantity,
              prod_option_id,
              equip_model,
              merchequip_amount,
              quantity_deployed
            )
            values
            (
              :(app.appGen.getAppSeqNum()),
              :(ed.type),
              3,
              1,
              1,
              :(ed.model),
              0,
              0
            )
          };
        }
        else
        {
          throw new Exception("Unknown Printer: " + app.field(124));
        }
      }
      
      // pinpads and check readers maybe later
    }
    catch(Exception e)
    {
      System.out.println("importEquipment(): " + e.toString());
      throw e;
    }
  }
  
  private void insertPayOption(long appSeqNum,
                               int cardType,
                               int counter,
                               String merchNumber,
                               String splitDial,
                               String pip)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into merchpayoption
        (
          app_seq_num,
          cardtype_code,
          card_sr_number,
          merchpo_card_merch_number,
          merchpo_split_dial,
          merchpo_pip
        )
        values
        (
          :appSeqNum,
          :cardType,
          :counter,
          :(clean(merchNumber)),
          :splitDial,
          :pip
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("insertPayOption("+appSeqNum+", "+cardType+"): " + e.toString());
      throw e;
    }
  }
  
  private void insertTranCharge(long appSeqNum,
                                int cardType,
                                String monthlyMin,
                                String discrateType,
                                String discRate,
                                String passThru,
                                String perTran)
    throws Exception
  {
    try
    {
      //System.out.println("monthlyMin = " + monthlyMin);
      //System.out.println("discrateType = " + discrateType);
      //System.out.println("discRate = " + discRate);
      //System.out.println("passthru = " + passThru);
      //System.out.println("discratetype = " + getMapType(DiscRateType, discRate));
      if(cardType == mesConstants.APP_CT_VISA || cardType == mesConstants.APP_CT_MC ||
         cardType == mesConstants.APP_CT_VISA_CASH_ADVANCE || cardType == mesConstants.APP_CT_MC_CASH_ADVANCE)
      {
        #sql [Ctx]
        {
          insert into tranchrg
          (
            app_seq_num,
            cardtype_code,
            tranchrg_mmin_chrg,
            tranchrg_discrate_type,
            tranchrg_float_disc_flag,
            tranchrg_disc_rate,
            tranchrg_pass_thru
          )
          values
          (
            :appSeqNum,
            :cardType,
            :monthlyMin,
            :(getMapType(DiscRateType,discRate)),
            'N',
            :discRate,
            :passThru
          )
        };
      }
      else
      {
        #sql [Ctx]
        {
          insert into tranchrg
          (
            app_seq_num,
            cardtype_code,
            tranchrg_per_tran
          )
          values
          (
            :appSeqNum,
            :cardType,
            :perTran
          )
        };
      }
    }
    catch(Exception e)
    {
      System.out.println("insertTranCharge("+appSeqNum+", "+cardType+"): " + e.toString());
      throw e;
    }
  }
                                
  private void importCards(AppImportApp app)
    throws Exception
  {
    try
    {
      int ctCounter = 0;
      
      long appSeqNum = app.appGen.getAppSeqNum();
      
      // visa
      insertPayOption(appSeqNum, mesConstants.APP_CT_VISA, ++ctCounter, "0", "N", "N");
      insertTranCharge(appSeqNum, mesConstants.APP_CT_VISA, app.field(88), app.field(84), app.field(85), app.field(86), "");
      // mc
      insertPayOption(appSeqNum, mesConstants.APP_CT_MC, ++ctCounter, "0", "N", "N");
      insertTranCharge(appSeqNum, mesConstants.APP_CT_MC, app.field(88), app.field(84), app.field(85), app.field(86), "");
      
      // amex
      if(!app.isBlank(100) && !app.field(100).toLowerCase().equals("n"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_AMEX, ++ctCounter, app.field(102), yesNo(app.field(103),"N"), yesNo(app.field(104),"N"));
        insertTranCharge(appSeqNum, mesConstants.APP_CT_AMEX, "", "", "", "", app.field(101));
      }
      
      // discover
      if(!app.isBlank(105) && !app.field(105).toLowerCase().equals("n"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_DISCOVER, ++ctCounter, app.field(107), "N", "N");
        insertTranCharge(appSeqNum, mesConstants.APP_CT_DISCOVER, "", "", "", "", app.field(106));
      }
      
      // jcb
      if(!app.isBlank(97) && !app.field(97).toLowerCase().equals("n"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_JCB, ++ctCounter, app.field(99), "N", "N");
        insertTranCharge(appSeqNum, mesConstants.APP_CT_JCB, "", "", "", "", app.field(98));
      }
      
      // diners
      if(!app.isBlank(94) && !app.field(94).toLowerCase().equals("n"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_DINERS_CLUB, ++ctCounter, app.field(96), "N", "N");
        insertTranCharge(appSeqNum, mesConstants.APP_CT_DINERS_CLUB, "", "", "", "", app.field(95));
      }
      
      // debit
      if(!app.isBlank(89) && !app.field(89).toLowerCase().equals("n") && !app.field(89).toLowerCase().equals("0"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_DEBIT, ++ctCounter, "0", "N", "N");
        insertTranCharge(appSeqNum, mesConstants.APP_CT_DEBIT, "", "", "", "", app.field(90));
      }
      
      // ebt
      if(!app.isBlank(91) && !app.field(91).toLowerCase().equals("n"))
      {
        insertPayOption(appSeqNum, mesConstants.APP_CT_EBT, ++ctCounter, app.field(93), "N", "N");
        insertTranCharge(appSeqNum, mesConstants.APP_CT_EBT, "", "", "", "", app.field(92));
      }
      
      // dial pay
      if(!app.isBlank(157) && !app.field(157).toLowerCase().equals("n"))
      {
        insertTranCharge(appSeqNum, mesConstants.APP_CT_DIAL_PAY, "", "", "", "", app.field(158));
      }
    }
    catch(Exception e)
    {
      System.out.println("importCards(): " + e.toString());
      throw e;
    }
  }
  
  private void insertPOS(long appSeqNum, int posCode, String posParam)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into merch_pos
        (
          app_seq_num,
          pos_code,
          pos_param
        )
        values
        (
          :appSeqNum,
          :posCode,
          :posParam
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("insertPOS("+posCode+", "+posParam+"): " + e.toString());
      throw e;
    }
  }
  
  private void importPOS(AppImportApp app)
    throws Exception
  {
    try
    {
      long appSeqNum = app.appGen.getAppSeqNum();
      
      if(!app.isBlank(118))
      {
        // dial terminal
        insertPOS(appSeqNum, mesConstants.APP_MPOS_DIAL_TERMINAL, "");
      }
      else if(!app.isBlank(150))
      {
        // internet
        insertPOS(appSeqNum, mesConstants.APP_MPOS_OTHER, app.field(151));
      }
      else if(!app.isBlank(153))
      {
        // pos partner
        insertPOS(appSeqNum, mesConstants.APP_MPOS_POS_PARTNER, "");
      }
      else if(!app.isBlank(157))
      {
        // dial pay
        insertPOS(appSeqNum, mesConstants.APP_MPOS_DIAL_PAY, "");
      }
      else if(!app.isBlank(159))
      {
        // other
        insertPOS(appSeqNum, mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT, app.field(160));
      }
      
      // pos features
      #sql [Ctx]
      {
        insert into pos_features
        (
          app_seq_num,
          access_code_flag,
          access_code,
          auto_batch_close_flag,
          auto_batch_close_time,
          header_line4_flag,
          header_line4,
          header_line5_flag,
          header_line5,
          footer_flag,
          footer,
          reset_ref_flag,
          invoice_prompt_flag,
          fraud_control_flag,
          password_protect_flag,
          purchasing_card_flag,
          avs_flag,
          card_truncation_flag,
          customer_service_phone,
          terminal_reminder_time,
          terminal_reminder_flag,
          clerk_enabled_flag,
          cash_back_flag,
          wireless_lli_flag,
          wireless_lli_number,
          wireless_esn_flag,
          wireless_esn_number,
          cash_back_limit,
          debit_surcharge_flag,
          phone_training,
          debit_surcharge_amount
        )
        values
        (
          :appSeqNum,
          :(flagIfPresent(app.field(127))),
          :(app.field(127)),
          :(flagIfPresent(app.field(128))),
          :(app.field(128)),
          :(flagIfPresent(app.field(129))),
          :(app.field(129)),
          :(flagIfPresent(app.field(130))),
          :(app.field(130)),
          :(flagIfPresent(app.field(131))),
          :(app.field(131)),
          :(yesNo(app.field(132),"N")),
          :(yesNo(app.field(133),"N")),
          :(yesNo(app.field(134),"N")),
          :(yesNo(app.field(135),"N")),
          :(yesNo(app.field(136),"N")),
          :(yesNo(app.field(137),"N")),
          :(yesNo(app.field(138),"N")),
          :(app.field(139)),
          :(app.field(140)),
          :(yesNo(app.field(141),"N")),
          :(yesNo(app.field(142),"N")),
          :(yesNo(app.field(143),"N")),
          :(flagIfPresent(app.field(144))),
          :(app.field(144)),
          :(flagIfPresent(app.field(145))),
          :(app.field(145)),
          :(app.field(146)),
          :(yesNo(app.field(147),"N")),
          :phoneTraining,
          :(app.field(148))
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("importPOS(): " + e.toString());
      throw e;
    }
  }
  
  private void insertMiscCharge(long appSeqNum, int miscCode, String amount)
    throws Exception
  {
    try
    {
      #sql [Ctx]
      {
        insert into miscchrg
        (
          app_seq_num,
          misc_code,
          misc_chrg_amount
        )
        values
        (
          :appSeqNum,
          :miscCode,
          :amount
        )
      };
    }
    catch(Exception e)
    {
      System.out.println("insertMiscCharge("+appSeqNum+", "+miscCode+", "+amount+"): " + e.toString());
      throw e;
    }
  }
  
  private void importMiscCharge(AppImportApp app)
    throws Exception
  {
    try
    {
      long appSeqNum = app.appGen.getAppSeqNum();
      if(!app.isBlank(155))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_POS_MONTHLY_SERVICE_FEE, app.field(155));
      }
      if(!app.isBlank(156))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_TRAINING_FEE_PC, app.field(156));
      }
      if(!app.isBlank(162))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_CHARGEBACK, app.field(162));
      }
      if(!app.isBlank(163))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE, app.field(163));
      }
      if(!app.isBlank(164))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_HELP_DESK, app.field(164));
      }
      if(!app.isBlank(165))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT, app.field(165));
      }
      if(!app.isBlank(166))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_WEB_REPORTING, app.field(166));
      }
      if(!app.isBlank(167))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP, app.field(167));
      }
      if(!app.isBlank(168))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP, app.field(168));
      }
      if(!app.isBlank(169))
      {
        insertMiscCharge(appSeqNum, mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP, app.field(169));
      }
    }
    catch(Exception e)
    {
      System.out.println("importMiscCharge(): " + e.toString());
      throw e;
    }
  }
  
  private void cleanUpApp(AppImportApp app)
    throws Exception
  {
    try
    {
      commit();
      
      appSeq.setAppSeqNum(app.appGen.getAppSeqNum());
      
      // mark app as completed
      #sql [Ctx]
      {
        update  screen_progress
        set     screen_complete = 255
        where   screen_pk = :(app.appGen.getAppSeqNum()) and
                screen_sequence_id = 2
      };
      
      #sql [Ctx]
      {
        update  merchant
        set     merch_credit_status = 5
        where   app_seq_num = :(app.appGen.getAppSeqNum())
      };
      
      commit();
      
      setAutoCommit(true);
      
      // go nuts
      AutoApprove.autoApprove(app.appGen.getAppSeqNum(), Ctx);
    }
    catch(Exception e)
    {
      System.out.println("cleanUpApp(): " + e.toString());
      throw e;
    }
  }
  
  private void doImport(AppImportApp app)
    throws Exception 
  {
    try
    {
      connect();
      setAutoCommit(false);
      
      System.out.println("importing app: " + app.appGen.getAppSeqNum() + " (" + app.field(2) + ")");
      
      // load hash maps
      loadHashMaps();
      
      // create app
      createApp(app);
      
      // merchant
      importMerchant(app);
      
      // address
      importAddress(app);
      
      // contact
      importContact(app);
      
      // bank
      importBank(app);
      
      // business owners
      importBusinessOwner(app);
      
      // equipment
      importEquipment(app);
      
      // card types
      importCards(app);
      
      // pos stuff
      importPOS(app);
      
      // misc charges
      importMiscCharge(app);
      
      // clean up (mark app as completed, move through credit, etc)
      cleanUpApp(app);
      
      commit();
    }
    catch(Exception e)
    {
      System.out.println("import(): " + e.toString());
      rollback();
      throw e;
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void run()
  {
    BufferedReader    in;
    String            ln;
    try
    {
      in = new BufferedReader(new FileReader(fileName));
      
      ln = in.readLine();
      
      int count = 0;
      while(ln != null)
      {
        System.out.println("processing: " + ++count + " (" + ln.substring(0, 30) +")");
        apps.add(new AppImportApp(ln));
        ln = in.readLine();
      }
      
      // process apps
      for(int i=0; i<apps.size(); ++i)
      {
        AppImportApp app = (AppImportApp)(apps.elementAt(i));
        
        doImport(app);
      }
    }
    catch(Exception e)
    {
      System.out.println("run(): " + e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    if(args.length != 3)
    {
      System.out.println("USAGE: AppImport <filename> <appType> <training type ([M]es, [S]ales, [N]one)>");
    }
    else
    {
      SQLJConnectionBase.setConnectString(SQLJConnectionBase.PROD_DIRECT_CONNECTION_STRING);
      AppImport ai = new AppImport(args[0], Integer.parseInt(args[1]), args[2]);
      
      ai.run();
    }
  }
  
  // inner classes
  public class EquipDetails
  {
    public String   model;
    public int      mfr;
    public int      type;
    
    public EquipDetails(String _model, int _mfr, int _type)
    {
      model = _model;
      mfr = _mfr;
      type = _type;
    }
  }
  
  public class AppImportApp
  {
    public  String      line      = null;
    public  String[]    appFields = new String[APP_FIELD_COUNT];
    AppSeqNumGenerator  appGen;
    
    public AppImportApp(String _line)
      throws Exception
    {
      line = _line;
      
      StreamTokenizer tokens = new StreamTokenizer(new StringReader(line));
      
      tokens.quoteChar('\"');
      tokens.whitespaceChars(',', ',');
      tokens.parseNumbers();
      
      int tokenCount = 0;
      while(tokens.nextToken() != StreamTokenizer.TT_EOF)
      {
        //String next = tokens.sval;
        //System.out.println("token: " + next);
        appFields[tokenCount] = tokens.sval.replace('\"', ' ').trim();
        ++tokenCount;
      }
      
      if(tokenCount != APP_FIELD_COUNT)
      {
        throw new Exception("AppImportApp constructor: Wrong number of items in line: (" + tokenCount + "/" + APP_FIELD_COUNT +")");
      }
      
      // generate appSeqNum and control number
      appGen = new AppSeqNumGenerator(Ctx);
    }
    
    public boolean isBlank(int idx)
      throws Exception
    {
      return(appFields[idx].equals(""));
    }
    
    public String field(int idx)
    {
      return(appFields[idx]);
    }
  }
  
}