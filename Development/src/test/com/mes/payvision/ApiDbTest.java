package com.mes.payvision;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.OracleConnectionPool;
import com.mes.database.OracleConnectionPool.PoolConnection;
import com.mes.support.LoggingConfigurator;
import sqlj.runtime.ref.DefaultContext;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, OracleConnectionPool.class})
public class ApiDbTest {

	@Test
	public void testGetHostAddrAndGetEndpoint() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(OracleConnectionPool.class));
		PowerMockito.spy(OracleConnectionPool.class);

		OracleConnectionPool ocpMock = mock(OracleConnectionPool.class);
		PoolConnection pcMock = mock(PoolConnection.class);
		DefaultContext dcMock = mock(DefaultContext.class);
		Connection conMock = mock(Connection.class);
		PreparedStatement psMock = mock(PreparedStatement.class);

		when(OracleConnectionPool.getInstance(null)).thenReturn(ocpMock);
		when(ocpMock.getFreeConnection(anyObject(), anyBoolean())).thenReturn(pcMock);
		when(pcMock.getContext()).thenReturn(dcMock);
		when(pcMock.getConnection()).thenReturn(conMock);
		when(conMock.prepareStatement(anyString())).thenReturn(psMock);

		ApiDb apiDbSpy = spy(ApiDb.class);
		ChargebackResponse cbRespMock = mock(ChargebackResponse.class);
		
		apiDbSpy.saveChargebackRecords(cbRespMock);
		
		verify(cbRespMock, times(1)).getHostAddr();
		verify(cbRespMock, times(1)).getEndpoint();
	}
}
