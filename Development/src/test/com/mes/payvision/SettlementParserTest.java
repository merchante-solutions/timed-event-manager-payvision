package com.mes.payvision;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SettlementParserTest {

    private static final String PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL = "Pre- and post- upgrade values should be equal";

    @Test
    public void testGetUsdFeeTotal() {
        // Given
        final BigDecimal fakeDiscountFees = new BigDecimal(12.536446464);
        final BigDecimal fakeRate = new BigDecimal(2.446835922);

        // When

        // old way
        final BigDecimal oldWayDivideResult = fakeDiscountFees.divide(fakeRate, 2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWaySetScaleResult = oldWayDivideResult.setScale(2, BigDecimal.ROUND_HALF_UP);

        // new way
        final BigDecimal newWayDivideResult = fakeDiscountFees.divide(fakeRate, 2, RoundingMode.HALF_UP);
        final BigDecimal newWaySetScaleResult = newWayDivideResult.setScale(2, RoundingMode.HALF_UP);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayDivideResult, newWayDivideResult);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWaySetScaleResult, newWaySetScaleResult);
    }

    @Test
    public void testGetTranAnalysis() {
        // Given
        final BigDecimal fakeTranAmount = new BigDecimal(348.90214526757);
        final BigDecimal fakeTranRate = new BigDecimal(1.246856898);

        // When

        // old way
        final BigDecimal oldWayDivideResult = fakeTranAmount.divide(fakeTranRate, 2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWaySetScaleResult = oldWayDivideResult.setScale(2, BigDecimal.ROUND_HALF_UP);

        // new way
        final BigDecimal newWayDivideResult = fakeTranAmount.divide(fakeTranRate, 2, RoundingMode.HALF_UP);
        final BigDecimal newWaySetScaleResult = newWayDivideResult.setScale(2, RoundingMode.HALF_UP);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayDivideResult, newWayDivideResult);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWaySetScaleResult, newWaySetScaleResult);
    }

    @Test
    public void testDistributeFees() {
        // Given
        final BigDecimal fakeTranTotal = new BigDecimal(348.90214526757);
        final BigDecimal fakeUSDdFee = new BigDecimal(57.7797972111);
        final BigDecimal fakeTranRate = new BigDecimal(1.246856898);
        final BigDecimal fakeFeeTotal = new BigDecimal(31527.98537893);

        // When

        // old way
        final BigDecimal oldWayDivideResult = fakeTranTotal.divide(fakeTranRate, 2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWaySetScaleResult = oldWayDivideResult.setScale(2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWayUSDFeeDividedByTranTotal = fakeUSDdFee.divide(fakeTranTotal, 6, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWayUSDFeeDividedMultipliedAndScaled = oldWayUSDFeeDividedByTranTotal.multiply(fakeFeeTotal).setScale(2, BigDecimal.ROUND_HALF_UP);

        // new way
        final BigDecimal newWayDivideResult = fakeTranTotal.divide(fakeTranRate, 2, RoundingMode.HALF_UP);
        final BigDecimal newWaySetScaleResult = newWayDivideResult.setScale(2, RoundingMode.HALF_UP);
        final BigDecimal newWayUSDFeeDividedByTranTotal = fakeUSDdFee.divide(fakeTranTotal, 6, RoundingMode.HALF_UP);
        final BigDecimal newWayUSDFeeDividedMultipliedAndScaled = oldWayUSDFeeDividedByTranTotal.multiply(fakeFeeTotal).setScale(2, RoundingMode.HALF_UP);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayDivideResult, newWayDivideResult);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWaySetScaleResult, newWaySetScaleResult);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayUSDFeeDividedByTranTotal, newWayUSDFeeDividedByTranTotal);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayUSDFeeDividedMultipliedAndScaled, newWayUSDFeeDividedMultipliedAndScaled);
    }

    @Test
    public void testGenerateTransactionFees() {
        // Given
        final BigDecimal usdFeeTotal = new BigDecimal(123.45678901);
        final BigDecimal feeRate = new BigDecimal(0.0234567891);
        final BigDecimal refundTotal = new BigDecimal(23.67866);

        // When

        // old way
        final BigDecimal oldWayUSDFeeTotalDividedByFeeRate = usdFeeTotal.divide(feeRate, 6, BigDecimal.ROUND_HALF_UP);
        final BigDecimal oldWayRefundTotalMultipliedAndScaled = refundTotal.multiply(feeRate).setScale(2, BigDecimal.ROUND_HALF_UP);

        // new way
        final BigDecimal newWayUSDFeeTotalDividedByFeeRate = usdFeeTotal.divide(feeRate, 6, RoundingMode.HALF_UP);
        final BigDecimal newWayRefundTotalMultipliedAndScaled = refundTotal.multiply(feeRate).setScale(2, RoundingMode.HALF_UP);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayUSDFeeTotalDividedByFeeRate, newWayUSDFeeTotalDividedByFeeRate);
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayRefundTotalMultipliedAndScaled, newWayRefundTotalMultipliedAndScaled);
    }
}