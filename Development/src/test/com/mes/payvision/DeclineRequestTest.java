package com.mes.payvision;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.payvision.reports.v1_1.axis.declines.ArrayOfDecline;
import com.mes.payvision.reports.v1_1.axis.declines.CardType;
import com.mes.payvision.reports.v1_1.axis.declines.Decline;
import com.mes.payvision.reports.v1_1.axis.declines.DeclineResult;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesLocator;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesSoap_BindingStub;
import com.mes.payvision.reports.v1_1.axis.declines.TransactionType;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, DeclineRequest.class, DeclinesLocator.class, DeclinesSoap_BindingStub.class})
public class DeclineRequestTest {

	private DeclineRequest declineRequest;
	private Calendar mockCalender;
	private Date dateSpy;
	private DeclinesLocator declinesLocator;
	private DeclinesSoap_BindingStub clientMock;
	private DeclineResponse declineResponse;
	private DeclineResult declineResult;
	private DeclineRecord declineRecord;

	@Before
	public void setUp() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(DeclinesLocator.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(DeclinesSoap_BindingStub.class));

		declineRequest = spy(DeclineRequest.class);

		PowerMockito.mockStatic(Calendar.class);
		mockCalender = spy(Calendar.class);
		dateSpy = spy(Date.class);
		when(Calendar.getInstance()).thenReturn(mockCalender);

		declineRequest.setStartDate(dateSpy);
		declineRequest.setEndDate(dateSpy);

		declinesLocator = spy(DeclinesLocator.class);
		PowerMockito.whenNew(DeclinesLocator.class).withAnyArguments().thenReturn(declinesLocator);
		PowerMockito.whenNew(DeclinesLocator.class).withNoArguments().thenReturn(declinesLocator);

		declineResponse = spy(DeclineResponse.class);
		PowerMockito.whenNew(DeclineResponse.class).withAnyArguments().thenReturn(declineResponse);

		declineResult = spy(getDeclineResult());

		clientMock = spy(DeclinesSoap_BindingStub.class);
		when(declinesLocator.getDeclinesSoap()).thenReturn(clientMock);
		when(clientMock.getDeclinedTransactions(Mockito.anyLong(), Mockito.anyString(), Mockito.anyInt(), (Calendar) Mockito.any(), (Calendar) Mockito.any())).thenReturn(declineResult);

		declineRecord = spy(DeclineRecord.class);
		PowerMockito.whenNew(DeclineRecord.class).withNoArguments().thenReturn(declineRecord);

	}

	@Test
	public void testSendRequest_EndPointAddress() throws Exception {
		declineRequest.sendRequest();
		verify(declinesLocator, times(1)).setEndpointAddress("DeclinesSoap", null + MesDefaults.PAYVISION_DECLINES_PATH);
	}

	@Test
	public void testSendRequest_LongSetters() throws Exception {
		declineRequest.sendRequest();
		assertEquals(declineResponse.getRecords().size(), declineResult.getData().getDecline().length);
		ArgumentCaptor<Long> procTranId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> cardId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> merchAccountId = ArgumentCaptor.forClass(Long.class);
		verify(declineRecord).setProcTranId(procTranId.capture());
		verify(declineRecord).setCardId(cardId.capture());
		verify(declineRecord).setMerchAcctId(merchAccountId.capture());
		assertEquals("java.lang.Long", procTranId.getValue().getClass().getName());
		assertEquals("java.lang.Long", cardId.getValue().getClass().getName());
		assertEquals("java.lang.Long", merchAccountId.getValue().getClass().getName());
	}

	private DeclineResult getDeclineResult() {

		DeclineResult declineResult = new DeclineResult();
		declineResult.setCode(0);
		declineResult.setMessage("Successfully processed.");
		ArrayOfDecline arrayOfDecline = new ArrayOfDecline();
		arrayOfDecline.setDecline(getDeclineRecords());
		declineResult.setData(arrayOfDecline);
		return declineResult;
	}

	private Decline[] getDeclineRecords() {
		Decline[] declineRecords = new Decline[1];
		Decline declineRecord = new Decline();
		declineRecord.setBankCode("05");
		declineRecord.setBankMessage("Do not Honour");
		declineRecord.setCardGuid("5d26a166-705c-4a6b-956d-390a6411058d");
		declineRecord.setCardId(345896993123l);
		declineRecord.setCardTypeId(CardType.VisaDebit);
		declineRecord.setCountryId(840);
		declineRecord.setMemberId(5005123);
		declineRecord.setMerchantAccountId(38965123456789l);
		declineRecord.setMerchantAmount(new BigDecimal(100));
		declineRecord.setOriginalAmount(new BigDecimal(100));
		declineRecord.setOriginalCurrencyId(826);
		declineRecord.setProcessedTransactionGuid("77204e0a-fc83-416f-92ff-7f329c010ad1");
		declineRecord.setProcessedTransactionId(18366670871234l);
		declineRecord.setRequestDate(Calendar.getInstance());
		declineRecord.setRequestDate(Calendar.getInstance());
		declineRecord.setTrackingMemberCode("4d90600d91c74eb6bf0f24619e53d349");
		declineRecord.setTransactionType(TransactionType.Payment);
		declineRecords[0] = declineRecord;
		return declineRecords;
	}

}
