package com.mes.payvision;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({ "javax.management.*", "javax.crypto.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({ LoggingConfigurator.class, DbProperties.class, SQLJConnectionBase.class, ApiRequest.class })
public class ApiResponseTest {
	String toStringData = "ApiResponse [ code: 0, message: string data for testing, req id: 0, req date: null, req type: null, host pt: null, end pt: null, user: null, cr mem id: 0, cr mem guid: null, mem id: 0, start date: null, end date: null ]";
	String endpointData, hostAddrData, newObjectStringData = "string data for testing";

	int newObjectIntData = 0;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
	}

	@Test
	public void testHostAndEndpointGetters() throws Exception {
		ApiRequest apiReqMock = mock(ApiRequest.class);
		spy(new ApiResponse(apiReqMock, newObjectIntData, newObjectStringData));

		verify(apiReqMock, times(1)).getHostAddr();
		verify(apiReqMock, times(1)).getEndpoint();
	}

	@Test
	public void testHostAndEndpointSetters() throws Exception {
		ApiRequest apiReqMock = mock(ApiRequest.class);
		ApiResponse apiRespSpy = spy(new ApiResponse(apiReqMock, newObjectIntData, newObjectStringData));
		apiRespSpy.setEndpoint(endpointData);
		apiRespSpy.setHostAddr(hostAddrData);

		assertEquals(apiRespSpy.getEndpoint(), endpointData);
		assertEquals(apiRespSpy.getHostAddr(), hostAddrData);
	}

	@Test
	public void testToString() throws Exception {
		ApiRequest apiReqMock = mock(ApiRequest.class);
		ApiResponse apiRespSpy = spy(new ApiResponse(apiReqMock, newObjectIntData, newObjectStringData));
		apiRespSpy.setEndpoint(endpointData);
		apiRespSpy.setHostAddr(hostAddrData);

		assertEquals(apiRespSpy.toString(), toStringData);
	}
}
