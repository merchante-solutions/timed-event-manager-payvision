package com.mes.payvision;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import com.mes.config.DbProperties;
import com.mes.database.OracleConnectionPool;
import com.mes.database.OracleConnectionPool.PoolConnection;
import com.mes.support.LoggingConfigurator;
import sqlj.runtime.ref.DefaultContext;
import org.junit.Assert;
import org.junit.Test;
import java.math.BigDecimal;
import java.math.RoundingMode;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, OracleConnectionPool.class})
public class PayvisionDbTest {

	@Test
	public void test() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(OracleConnectionPool.class));
		PowerMockito.spy(OracleConnectionPool.class);
		
		OracleConnectionPool ocpMock = mock(OracleConnectionPool.class);
		PoolConnection pcMock = mock(PoolConnection.class);
		DefaultContext dcMock = mock(DefaultContext.class);
		Connection conMock = mock(Connection.class);
		PreparedStatement psMock = mock(PreparedStatement.class);
		ApiResponse apiResponseMock = mock(ApiResponse.class);
		
		when(OracleConnectionPool.getInstance(null)).thenReturn(ocpMock);
		when(ocpMock.getFreeConnection(anyObject(), anyBoolean())).thenReturn(pcMock);
		when(pcMock.getContext()).thenReturn(dcMock);
		when(pcMock.getConnection()).thenReturn(conMock);
		when(conMock.prepareStatement(anyString())).thenReturn(psMock);

		PayvisionDb pvDbSpy = spy(PayvisionDb.class);
		
		ChargebackResponse cbRespMock = mock(ChargebackResponse.class);
		pvDbSpy.saveChargebackRecords(cbRespMock);
		
		verify(cbRespMock, times(1)).getHostAddr();
		verify(cbRespMock, times(1)).getEndpoint();
		
		DeclineResponse declinesRespMock = mock(DeclineResponse.class);
		pvDbSpy.saveDeclineRecords(declinesRespMock);
		
		verify(declinesRespMock, times(1)).getHostAddr();
		verify(declinesRespMock, times(1)).getEndpoint();
		
		ReconciliationResponse reconcilRespMock = mock(ReconciliationResponse.class);
		pvDbSpy.saveReconciliationData(reconcilRespMock);
		
		verify(reconcilRespMock, times(1)).getHostAddr();
		verify(reconcilRespMock, times(1)).getEndpoint();
	}

    @Test
    public void testGetBaseConversionRateToUsd() {
        // Given
        final BigDecimal baseRate = new BigDecimal(0.792067);

        // When
        
        // old way
        final BigDecimal oldWayResult = new BigDecimal(1).divide(baseRate, baseRate.scale(), BigDecimal.ROUND_HALF_UP);
        // new way
        final BigDecimal newWayResult = new BigDecimal(1).divide(baseRate, baseRate.scale(), RoundingMode.HALF_UP);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }
}
