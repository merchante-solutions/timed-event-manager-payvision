package com.mes.payvision;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.sql.Date;
import java.util.ArrayList;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.payvision.ApiLoader.LastResponse;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, ApiLoader.class})
public class ApiLoaderTest {
	static PayvisionDb pvDbSpy = null;
	static DateRange drSpy = null;
	static ApiLoader apiLoaderSpy = null;
	static ArrayList<LastResponse> listMock = null;
	static String stringSpy = null;
	static Integer intSpy = null;
	static Date dateSpy = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
	}

	@Test
	public void testReconciliationRequestSetter() throws Exception {
		pvDbSpy = spy(new PayvisionDb());
		drSpy = spy(DateRange.class);
		listMock = getLastResponsesMock();
		dateSpy = spy(new Date(0));
		apiLoaderSpy = PowerMockito.spy(new ApiLoader());

		PowerMockito.whenNew(PayvisionDb.class).withNoArguments().thenReturn(pvDbSpy);
		PowerMockito.whenNew(ApiLoader.class).withNoArguments().thenReturn(apiLoaderSpy);
		PowerMockito.whenNew(DateRange.class).withAnyArguments().thenReturn(drSpy);
		PowerMockito.doReturn(listMock).when(apiLoaderSpy, "getLastResponses", anyString());

		ReconciliationRequest recReqSpy = spy(ReconciliationRequest.class);
		PowerMockito.whenNew(ReconciliationRequest.class).withAnyArguments().thenReturn(recReqSpy);
		doNothing().when(recReqSpy).sendRequest();

		ReconciliationResponse recRespSpy = spy(ReconciliationResponse.class);
		when(recReqSpy.getReconciliationResponse()).thenReturn(recRespSpy);
		doNothing().when(pvDbSpy).saveReconciliationData(recRespSpy);

		ApiLoader.loadTransactionDates("testing 123", -1, dateSpy, dateSpy);		
		ApiLoader.loadTransactionDates("testing 321", -1, drSpy);
		
		verify(recReqSpy, times(2)).setEndpoint(MesDefaults.PAYVISION_TRANSACTIONS_PATH);
	}

	@Test
	public void testChargebackRequestSetter() throws Exception {
		pvDbSpy = spy(new PayvisionDb());
		drSpy = spy(DateRange.class);
		listMock = getLastResponsesMock();
		dateSpy = spy(new Date(0));
		apiLoaderSpy = PowerMockito.spy(new ApiLoader());
		
		PowerMockito.whenNew(PayvisionDb.class).withNoArguments().thenReturn(pvDbSpy);
		PowerMockito.whenNew(ApiLoader.class).withNoArguments().thenReturn(apiLoaderSpy);
		PowerMockito.whenNew(DateRange.class).withAnyArguments().thenReturn(drSpy);
		PowerMockito.doReturn(listMock).when(apiLoaderSpy, "getLastResponses", anyString());

		ChargebackRequest chargeBackReqSpy = spy(ChargebackRequest.class);
		PowerMockito.whenNew(ChargebackRequest.class).withAnyArguments().thenReturn(chargeBackReqSpy);
		doNothing().when(chargeBackReqSpy).sendRequest();

		ChargebackResponse chargebackRespSpy = spy(ChargebackResponse.class);
		when(chargeBackReqSpy.getChargebackResponse()).thenReturn(chargebackRespSpy);
		doNothing().when(pvDbSpy).saveChargebackRecords(chargebackRespSpy);

		ApiLoader.loadChargebackDate("123 Testing", -1,  dateSpy);
		ApiLoader.loadChargebackDates("321 Testing", -1, dateSpy, dateSpy);
		ApiLoader.loadChargebackDates("Payvision Modifications",-1, drSpy);
		
		verify(chargeBackReqSpy, times(3)).setEndpoint(MesDefaults.PAYVISION_CHARGEBACKS_PATH);
	}

	@Test
	public void testDeclineRequestSetter() throws Exception {
		pvDbSpy = spy(new PayvisionDb());
		drSpy = spy(DateRange.class);
		listMock = getLastResponsesMock();
		dateSpy = spy(new Date(0));
		apiLoaderSpy = PowerMockito.spy(new ApiLoader());
		
		PowerMockito.whenNew(PayvisionDb.class).withNoArguments().thenReturn(pvDbSpy);
		PowerMockito.whenNew(ApiLoader.class).withNoArguments().thenReturn(apiLoaderSpy);
		PowerMockito.whenNew(DateRange.class).withAnyArguments().thenReturn(drSpy);
		PowerMockito.doReturn(listMock).when(apiLoaderSpy, "getLastResponses", anyString());

		DeclineRequest declinesReqSpy = spy(DeclineRequest.class);
		PowerMockito.whenNew(DeclineRequest.class).withAnyArguments().thenReturn(declinesReqSpy);
		doNothing().when(declinesReqSpy).sendRequest();

		DeclineResponse declinesRespSpy = spy(DeclineResponse.class);
		when(declinesReqSpy.getDeclineResponse()).thenReturn(declinesRespSpy);
		doNothing().when(pvDbSpy).saveDeclineRecords(declinesRespSpy);

		ApiLoader.loadDeclineDates("Lenox Square", -1, dateSpy, dateSpy);
		ApiLoader.loadDeclineDates("Phipps Plaza", -1, drSpy);
		
		verify(declinesReqSpy, times(2)).setEndpoint(MesDefaults.PAYVISION_DECLINES_PATH);
	}

	public static ArrayList<LastResponse> getLastResponsesMock() {
		ArrayList<LastResponse> listMock = new ArrayList<LastResponse>();
		LastResponse lrMock = mock(LastResponse.class);
		listMock.add(lrMock);
		return listMock;
	}
}
