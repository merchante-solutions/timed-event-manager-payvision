package com.mes.payvision;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.payvision.reports.v1_1.axis.transactions.ArrayOfReconciliationMerchantAccount;
import com.mes.payvision.reports.v1_1.axis.transactions.ArrayOfReconciliationTransaction;
import com.mes.payvision.reports.v1_1.axis.transactions.CardType;
import com.mes.payvision.reports.v1_1.axis.transactions.Reconciliation;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationMerchantAccount;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationResult;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationTotal;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationTransaction;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionType;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsLocator;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsSoap_BindingStub;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, ReconciliationRequest.class, TransactionsLocator.class, TransactionsSoap_BindingStub.class})
public class ReconciliationRequestTest {

	private ReconciliationRequest reconciliationRequest;
	private TransactionsLocator transactionsLocator;
	private TransactionsSoap_BindingStub clientMock;
	private ReconciliationResult reconciliationResult;
	private ReconciliationSummary summary;
	private TransactionDetail transactionDetail;
	private ReconciliationResponse reconciliationResponse;

	@Before
	public void setUp() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(TransactionsLocator.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(TransactionsSoap_BindingStub.class));

		PowerMockito.mockStatic(Calendar.class);
		Calendar calMoc = spy(Calendar.class);
		Date dateSpy = spy(Date.class);
		when(Calendar.getInstance()).thenReturn(calMoc);

		reconciliationRequest = spy(ReconciliationRequest.class);
		reconciliationRequest.setStartDate(dateSpy);
		reconciliationRequest.setEndDate(dateSpy);

		transactionsLocator = spy(TransactionsLocator.class);
		PowerMockito.whenNew(TransactionsLocator.class).withAnyArguments().thenReturn(transactionsLocator);
		PowerMockito.whenNew(TransactionsLocator.class).withNoArguments().thenReturn(transactionsLocator);

		clientMock = spy(TransactionsSoap_BindingStub.class);
		reconciliationResult = spy(getReconciliationResult());

		when(transactionsLocator.getTransactionsSoap()).thenReturn(clientMock);
		when(clientMock.getReconciliation(Mockito.anyLong(), Mockito.anyString(), Mockito.anyInt(), (Calendar) Mockito.any(), (Calendar) Mockito.any())).thenReturn(reconciliationResult);

		reconciliationResponse = spy(ReconciliationResponse.class);
		PowerMockito.whenNew(ReconciliationResponse.class).withAnyArguments().thenReturn(reconciliationResponse);

		summary = spy(ReconciliationSummary.class);
		PowerMockito.whenNew(ReconciliationSummary.class).withAnyArguments().thenReturn(summary);

		transactionDetail = spy(TransactionDetail.class);
		PowerMockito.whenNew(TransactionDetail.class).withAnyArguments().thenReturn(transactionDetail);

	}

	@Test
	public void testSendRequest_EndPointAddress() throws Exception {
		reconciliationRequest.sendRequest();
		verify(transactionsLocator, times(1)).setEndpointAddress("TransactionsSoap", null + MesDefaults.PAYVISION_TRANSACTIONS_PATH);
	}

	@Test
	public void testSendRequest_LongSetters() throws Exception {

		reconciliationRequest.sendRequest();
		assertEquals(reconciliationResponse.getSummaries().size(), reconciliationResult.getData().getMerchantAccounts().getReconciliationMerchantAccount().length);
		ArgumentCaptor<Long> merchantAccountId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> tranMerchantAccountId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> cardId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> transactionId = ArgumentCaptor.forClass(Long.class);
		verify(summary).setMerchAcctId(merchantAccountId.capture());
		verify(transactionDetail).setMerchAcctId(tranMerchantAccountId.capture());
		verify(transactionDetail).setCardId(cardId.capture());
		verify(transactionDetail).setMerchAcctId(merchantAccountId.capture());
		verify(transactionDetail).setTranId(transactionId.capture());
		assertEquals("java.lang.Long", tranMerchantAccountId.getValue().getClass().getName());
		assertEquals("java.lang.Long", merchantAccountId.getValue().getClass().getName());
		assertEquals("java.lang.Long", cardId.getValue().getClass().getName());
		assertEquals("java.lang.Long", transactionId.getValue().getClass().getName());
	}

	private ReconciliationResult getReconciliationResult() {
		ReconciliationResult reconciliationResult = new ReconciliationResult();
		reconciliationResult.setCode(0);
		reconciliationResult.setMessage("Successfully processed.");
		reconciliationResult.setData(getReconciliation());
		return reconciliationResult;
	}

	private Reconciliation getReconciliation() {
		Reconciliation reconciliation = new Reconciliation();
		reconciliation.setStartDate(Calendar.getInstance());
		reconciliation.setEndDate(Calendar.getInstance());
		reconciliation.setMerchantAccountsIncluded(1);
		reconciliation.setMerchantAccounts(getMerchantAccounts());
		return reconciliation;
	}

	private ArrayOfReconciliationMerchantAccount getMerchantAccounts() {
		ArrayOfReconciliationMerchantAccount arrayOfReconciliationMerchantAccount = new ArrayOfReconciliationMerchantAccount();
		ReconciliationMerchantAccount[] reconciliationMerchantAccounts = new ReconciliationMerchantAccount[1];
		ReconciliationMerchantAccount reconciliationMerchantAccount = new ReconciliationMerchantAccount();
		reconciliationMerchantAccount.setMerchantAccountId(1234567891015l);
		reconciliationMerchantAccount.setSettlementCurrency(978);
		reconciliationMerchantAccount.setAuthorizes(new ReconciliationTotal(1406, new BigDecimal(34495.14)));
		reconciliationMerchantAccount.setCaptures(new ReconciliationTotal(1396, new BigDecimal(34495.14)));
		reconciliationMerchantAccount.setVoids(new ReconciliationTotal(89, new BigDecimal(1673.90)));
		reconciliationMerchantAccount.setPayments(new ReconciliationTotal(0, new BigDecimal(0)));
		reconciliationMerchantAccount.setRefunds(new ReconciliationTotal(1, new BigDecimal(27.00)));
		reconciliationMerchantAccount.setCredits(new ReconciliationTotal(1, new BigDecimal(27.00)));
		reconciliationMerchantAccount.setCardFundTransfers(new ReconciliationTotal(0, new BigDecimal(0)));
		reconciliationMerchantAccount.setReferralApprovals(new ReconciliationTotal(0, new BigDecimal(0)));
		reconciliationMerchantAccount.setTransactions(getReconsiliationTransactions());
		reconciliationMerchantAccounts[0] = reconciliationMerchantAccount;
		arrayOfReconciliationMerchantAccount.setReconciliationMerchantAccount(reconciliationMerchantAccounts);
		return arrayOfReconciliationMerchantAccount;
	}

	private ArrayOfReconciliationTransaction getReconsiliationTransactions() {
		ArrayOfReconciliationTransaction arrayOfReconciliationTransaction = new ArrayOfReconciliationTransaction();
		ReconciliationTransaction[] reconciliationTransactions = new ReconciliationTransaction[1];
		ReconciliationTransaction reconciliationTransaction = new ReconciliationTransaction();
		reconciliationTransaction.setTransactionId(183672778112l);
		reconciliationTransaction.setTransactionGuid("af0e273e-1db7-4fb9-baa6-42ff56b24e97");
		reconciliationTransaction.setTrackingMemberCode("4107a344a0c64f65afe60af5bb8606e4");
		reconciliationTransaction.setOriginalAmount(new BigDecimal(24));
		reconciliationTransaction.setOriginalCurrencyId(826);
		reconciliationTransaction.setMerchantAmount(new BigDecimal(24));
		reconciliationTransaction.setCurrencyRate(new BigDecimal(1));
		reconciliationTransaction.setCardId(343986957122l);
		reconciliationTransaction.setCardGuid("825e5a77-025e-4181-b25b-23ad32f0fbae");
		reconciliationTransaction.setCardTypeId(CardType.Amex);
		reconciliationTransaction.setTransactionDate(Calendar.getInstance());
		reconciliationTransaction.setTransactionType(TransactionType.Authorize);
		reconciliationTransaction.setCountryId(276);
		reconciliationTransaction.setBatchDate("20190419");
		reconciliationTransactions[0] = reconciliationTransaction;
		arrayOfReconciliationTransaction.setReconciliationTransaction(reconciliationTransactions);
		return arrayOfReconciliationTransaction;
	}
}
