package com.mes.payvision;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ArrayOfCdcEntryItem;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ArrayOfChargeback;
import com.mes.payvision.reports.v1_1.axis.chargebacks.CdcEntryItem;
import com.mes.payvision.reports.v1_1.axis.chargebacks.Chargeback;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebackResult;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksLocator;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksSoap_BindingStub;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, ChargebackRequest.class, ChargebacksLocator.class, ChargebacksSoap_BindingStub.class})
public class ChargebackRequestTest {

	private ChargebackRequest chargebackRequest;
	private Calendar mockCalender;
	private Date dateSpy;
	private ChargebacksLocator chargebacksLocator;
	private ChargebackResult chargebackResult;
	private ChargebacksSoap_BindingStub clientMock;
	private ChargebackRecord chargebackRecord;
	private ChargebackResponse chargebackResponse;

	@Before
	public void setUp() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(ChargebacksLocator.class));
		PowerMockito.suppress(PowerMockito.methodsDeclaredIn(ChargebacksSoap_BindingStub.class));

		PowerMockito.mockStatic(Calendar.class);
		mockCalender = spy(Calendar.class);
		dateSpy = spy(Date.class);
		when(Calendar.getInstance()).thenReturn(mockCalender);

		chargebackRequest = spy(ChargebackRequest.class);
		chargebackRequest.setStartDate(dateSpy);
		chargebackRequest.setEndDate(dateSpy);
		chargebacksLocator = spy(ChargebacksLocator.class);
		PowerMockito.whenNew(ChargebacksLocator.class).withAnyArguments().thenReturn(chargebacksLocator);
		PowerMockito.whenNew(ChargebacksLocator.class).withNoArguments().thenReturn(chargebacksLocator);

		clientMock = spy(ChargebacksSoap_BindingStub.class);

		chargebackResult = spy(getChargeBackResult());
		when(chargebacksLocator.getChargebacksSoap()).thenReturn(clientMock);
		when(clientMock.getChargebacks(Mockito.anyLong(), Mockito.anyString(), Mockito.anyInt(), (Calendar) Mockito.any(), (Calendar) Mockito.any())).thenReturn(chargebackResult);
		chargebackRecord = spy(ChargebackRecord.class);
		PowerMockito.whenNew(ChargebackRecord.class).withNoArguments().thenReturn(chargebackRecord);
		chargebackResponse = spy(ChargebackResponse.class);
		PowerMockito.whenNew(ChargebackResponse.class).withAnyArguments().thenReturn(chargebackResponse);
	}

	@Test
	public void testSendRequest_EndPointAddress() throws Exception {
		chargebackRequest.sendRequest();
		verify(chargebacksLocator, times(1)).setEndpointAddress("ChargebacksSoap", null + MesDefaults.PAYVISION_CHARGEBACKS_PATH);
	}

	@Test
	public void testSendRequest_LongSetters() throws Exception {
		chargebackRequest.sendRequest();
		assertEquals(chargebackResponse.getRecords().size(), chargebackResult.getData().getChargeback().length);
		ArgumentCaptor<Long> chargeBackId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> cardId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> merchAccountId = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> transactionId = ArgumentCaptor.forClass(Long.class);
		verify(chargebackRecord).setCbId(chargeBackId.capture());
		verify(chargebackRecord).setCardId(cardId.capture());
		verify(chargebackRecord).setMerchAcctId(merchAccountId.capture());
		verify(chargebackRecord).setTranId(transactionId.capture());
		assertEquals("java.lang.Long", chargeBackId.getValue().getClass().getName());
		assertEquals("java.lang.Long", cardId.getValue().getClass().getName());
		assertEquals("java.lang.Long", merchAccountId.getValue().getClass().getName());
		assertEquals("java.lang.Long", transactionId.getValue().getClass().getName());
	}

	private ChargebackResult getChargeBackResult() {
		ChargebackResult chargebackResult = new ChargebackResult();
		chargebackResult.setCode(0);
		chargebackResult.setMessage("Successfully processed.");
		ArrayOfChargeback arrayOfChargeback = new ArrayOfChargeback();
		arrayOfChargeback.setChargeback(getChargeBackRecords());
		chargebackResult.setData(arrayOfChargeback);
		return chargebackResult;
	}

	private Chargeback[] getChargeBackRecords() {
		Chargeback[] chargebacks = new Chargeback[1];
		Chargeback chargeback = new Chargeback();
		chargeback.setChargebackId(1234567891012l);
		chargeback.setChargebackTypeId(2);
		chargeback.setCaseId("9101200232");
		chargeback.setAmount(new BigDecimal(100));
		chargeback.setCurrencyId(840);
		chargeback.setProcessedDate(Calendar.getInstance());
		chargeback.setReasonCode("4837");
		chargeback.setCardId(409273163409l);
		chargeback.setCardGuid("57f1501f-d0dc-4278-a422-e930f3d0b2b7");
		chargeback.setMerchantAccountId(38965389651l);
		chargeback.setOriginalTransactionDate(Calendar.getInstance());
		chargeback.setTransactionId(181759619311l);
		chargeback.setAdditionalInfo(getAdditionalInfo());
		chargebacks[0] = chargeback;
		return chargebacks;
	}

	private ArrayOfCdcEntryItem getAdditionalInfo() {
		ArrayOfCdcEntryItem arrayOfCdcEntryItem = new ArrayOfCdcEntryItem();
		CdcEntryItem[] cdcEntryItems = new CdcEntryItem[1];

		CdcEntryItem deadLineItem = new CdcEntryItem();
		deadLineItem.setKey("DeadLine");
		deadLineItem.setValue("29.05.2019");
		cdcEntryItems[0] = deadLineItem;

		arrayOfCdcEntryItem.setCdcEntryItem(cdcEntryItems);
		return arrayOfCdcEntryItem;
	}
}
