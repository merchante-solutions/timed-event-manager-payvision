package com.mes.tools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.database.MeSQueryHandler;
import com.mes.database.MesQueryHandlerList;
import com.mes.tools.DccRateUtil.CardBrandType;

public class DccRateUtilTest {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	@Test
	public void testGetMaxEffectiveDate_MasterCard() throws SQLException, ParseException {

		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults());

		Date maxEffDate = rateUtilMock.getMaxEffectiveDate(CardBrandType.MASTER_CARD);

		assertEquals(1, objArrayCaptor.getAllValues().size());
		List arguments = objArrayCaptor.getAllValues();

		assertEquals(sdf.parse("2020-02-02 00:00:00"), maxEffDate);
	}

	@Test(expected = SQLException.class)
	public void testGetMaxEffectiveDate_NotFound() throws SQLException, ParseException {

		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		List<Map<String, Object>> resultList = getEffectiveDateResults();
		resultList.clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("dcc.effective_date"), objArrayCaptor.capture()))
				.thenReturn(resultList);
		try {
			rateUtilMock.getMaxEffectiveDate(CardBrandType.MASTER_CARD);
		}
		catch (SQLException sqe) {
			assertEquals("Max Effective Date not found for MC", sqe.getMessage());
			throw sqe;
		}
	}

	@Test(expected = SQLException.class)
	public void testGetMaxEffectiveDate_NULL_Result() throws SQLException, ParseException {

		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		List<Map<String, Object>> resultList = getEffectiveDateResults();
		resultList.clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("dcc.effective_date"), objArrayCaptor.capture()))
				.thenReturn(null);
		try {
			rateUtilMock.getMaxEffectiveDate(CardBrandType.MASTER_CARD);
		}
		catch (SQLException sqe) {
			assertEquals("Max Effective Date not found for MC", sqe.getMessage());
			throw sqe;
		}
	}

	@Test
	public void testGetMaxEffectiveDate_Visa() throws SQLException, ParseException {

		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults());

		Date maxEffDate = rateUtilMock.getMaxEffectiveDate(CardBrandType.VISA);

		assertEquals(1, objArrayCaptor.getAllValues().size());

		assertEquals(sdf.parse("2020-02-02 00:00:00"), maxEffDate);
	}
	
	@Test
	public void testGetDccRate_MC_WithFXDate_Midday() throws Exception {
		
		Date mcFxRateDate = sdf.parse("2020-01-26 12:55:15");
		
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(10).when(rateUtilMock).getEffectiveDateCount();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();
		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(java.util.Date.class))).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_WithEffectiveDate_MultiMC());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", (Object[]) null);

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840", mcFxRateDate);
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009236789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009232222518d), BigDecimal.valueOf(rate.getSellRate()));
	}
	
	@Test
	public void testGetDccRate_MC_WithFXDate_Midnight() throws Exception {
		
		Date mcFxRateDate = sdf.parse("2020-01-26 00:00:00");
		
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(10).when(rateUtilMock).getEffectiveDateCount();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();
		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(java.util.Date.class))).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_WithEffectiveDate_MultiMC());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", (Object[]) null);

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840", mcFxRateDate);
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009236789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009232222518d), BigDecimal.valueOf(rate.getSellRate()));
	}
	
	@Test
	public void testGetDccRate_MC_WithFXDate_Midnite_PM() throws Exception {
		
		Date mcFxRateDate = sdf.parse("2020-01-26 23:59:59");
		
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(10).when(rateUtilMock).getEffectiveDateCount();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();
		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(java.util.Date.class))).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_WithEffectiveDate_MultiMC());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", (Object[]) null);

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840", mcFxRateDate);
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009236789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009232222518d), BigDecimal.valueOf(rate.getSellRate()));
	}
	
	@Test
	public void testLoadRates_WithDate_NonZeroEffCount() throws Exception {
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(10).when(rateUtilMock).getEffectiveDateCount();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_WithEffectiveDate_MultiMC());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", (Object[]) null);

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009136789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009132222518d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("VS", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.00919753d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.00913506d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("DS", "008", "840");
		assertNotNull(rate);
	}

	@Test
	public void testLoadRates_WithDate() throws Exception {
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(0).when(rateUtilMock).getEffectiveDateCount();

		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();

		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_WithEffectiveDate());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", (Object[]) null);

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009136789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009132222518d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("VS", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.00919753d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.00913506d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("DS", "008", "840");
		assertNotNull(rate);
	}

	@Test
	public void testLoadRates_NULL_Date() throws Exception {
		DccRateUtil rateUtilMock = Mockito.mock(DccRateUtil.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doCallRealMethod().when(rateUtilMock).loadRates(Mockito.any(java.util.Date.class));
		doReturn(0).when(rateUtilMock).getEffectiveDateCount();
		
		when(rateUtilMock.getMaxEffectiveDate(Mockito.any())).thenCallRealMethod();
		when(rateUtilMock.getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("visa_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-21 00:00:00"));

		when(localQHMock.executePreparedStatement(Mockito.eq("0com.mes.tools.DccRateUtil"), Mockito.contains("mc_settlement_dcc"), objArrayCaptor.capture()))
				.thenReturn(getEffectiveDateResults("2020-01-27 00:00:00"));

		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		when(localQHMock.executePreparedStatement(Mockito.eq("2com.mes.tools.DccRateUtil"), Mockito.contains("item_key"), objDatesCaptor.capture()))
				.thenReturn(getRateResults_NullEffectiveDate());

		Whitebox.invokeMethod(rateUtilMock, "loadRates", sdf.parse("2020-01-27 00:00:00"));

		assertNotNull(objDatesCaptor.getAllValues());
		assertEquals(5, objDatesCaptor.getAllValues().size());

		DccRateUtil.DccRate rate = rateUtilMock.getDccRate("MC", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009136789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009132222518d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("VS", "008", "840");
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.00909753d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.00903506d), BigDecimal.valueOf(rate.getSellRate()));

		rate = rateUtilMock.getDccRate("DS", "008", "840");
		assertNotNull(rate);
	}

	private List<Map<String, Object>> getEffectiveDateResults() throws ParseException {
		return getEffectiveDateResults("2020-02-02 00:00:00");
	}

	private List<Map<String, Object>> getEffectiveDateResults(String dateString) throws ParseException {
		List<Map<String, Object>> resultList = new ArrayList<>();
		Map<String, Object> row = new HashMap<String, Object>();
		row.put("effective_date", new java.sql.Date(sdf.parse(dateString).getTime()));
		resultList.add(row);
		return resultList;
	}

	private List<Map<String, Object>> getRateResults_NullEffectiveDate() throws ParseException {
		List<Map<String, Object>> rateResults = new ArrayList<>();
		rateResults.add(new HashMap<>());
		rateResults.get(0).put("item_key", "VS008840");
		rateResults.get(0).put("effective_date", sdf.parse("2020-01-21 00:00:00"));
		rateResults.get(0).put("base_currency_code", "008");
		rateResults.get(0).put("counter_currency_code", "840");
		rateResults.get(0).put("card_type", "VS");
		rateResults.get(0).put("buy_rate", BigDecimal.valueOf(0.00909753d));
		rateResults.get(0).put("sell_rate", BigDecimal.valueOf(0.00903506d));
		rateResults.add(new HashMap<>());
		rateResults.get(1).put("item_key", "DS008840");
		rateResults.get(1).put("effective_date", sdf.parse("2020-01-21 00:00:00"));
		rateResults.get(1).put("base_currency_code", "008");
		rateResults.get(1).put("counter_currency_code", "840");
		rateResults.get(1).put("card_type", "DS");
		rateResults.get(1).put("buy_rate", BigDecimal.valueOf(0.00909753d));
		rateResults.get(1).put("sell_rate", BigDecimal.valueOf(0.00903506d));
		rateResults.add(new HashMap<>());
		rateResults.get(2).put("item_key", "MC008840");
		rateResults.get(2).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(2).put("base_currency_code", "008");
		rateResults.get(2).put("counter_currency_code", "840");
		rateResults.get(2).put("card_type", "MC");
		rateResults.get(2).put("buy_rate", BigDecimal.valueOf(0.009136789813d));
		rateResults.get(2).put("sell_rate", BigDecimal.valueOf(0.009132222518d));

		return rateResults;
	}

	private List<Map<String, Object>> getRateResults_WithEffectiveDate() throws ParseException {
		List<Map<String, Object>> rateResults = new ArrayList<>();
		rateResults.add(new HashMap<>());
		rateResults.get(0).put("item_key", "VS008840");
		rateResults.get(0).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(0).put("base_currency_code", "008");
		rateResults.get(0).put("counter_currency_code", "840");
		rateResults.get(0).put("card_type", "VS");
		rateResults.get(0).put("buy_rate", BigDecimal.valueOf(0.00919753d));
		rateResults.get(0).put("sell_rate", BigDecimal.valueOf(0.00913506d));
		rateResults.add(new HashMap<>());
		rateResults.get(1).put("item_key", "DS008840");
		rateResults.get(1).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(1).put("base_currency_code", "008");
		rateResults.get(1).put("counter_currency_code", "840");
		rateResults.get(1).put("card_type", "DS");
		rateResults.get(1).put("buy_rate", BigDecimal.valueOf(0.00909753d));
		rateResults.get(1).put("sell_rate", BigDecimal.valueOf(0.00903506d));
		rateResults.add(new HashMap<>());
		rateResults.get(2).put("item_key", "MC008840");
		rateResults.get(2).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(2).put("base_currency_code", "008");
		rateResults.get(2).put("counter_currency_code", "840");
		rateResults.get(2).put("card_type", "MC");
		rateResults.get(2).put("buy_rate", BigDecimal.valueOf(0.009136789813d));
		rateResults.get(2).put("sell_rate", BigDecimal.valueOf(0.009132222518d));

		return rateResults;
	}
	
	private List<Map<String, Object>> getRateResults_WithEffectiveDate_MultiMC() throws ParseException {
		List<Map<String, Object>> rateResults = new ArrayList<>();
		rateResults.add(new HashMap<>());
		rateResults.get(0).put("item_key", "VS008840");
		rateResults.get(0).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(0).put("effective_start_date",sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(0).put("effective_end_date", sdf.parse("2020-01-27 23:59:59"));
		rateResults.get(0).put("base_currency_code", "008");
		rateResults.get(0).put("counter_currency_code", "840");
		rateResults.get(0).put("card_type", "VS");
		rateResults.get(0).put("buy_rate", BigDecimal.valueOf(0.00919753d));
		rateResults.get(0).put("sell_rate", BigDecimal.valueOf(0.00913506d));
		rateResults.add(new HashMap<>());
		rateResults.get(1).put("item_key", "DS008840");
		rateResults.get(1).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(1).put("effective_start_date",sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(1).put("effective_end_date", sdf.parse("2020-01-27 23:59:59"));
		rateResults.get(1).put("base_currency_code", "008");
		rateResults.get(1).put("counter_currency_code", "840");
		rateResults.get(1).put("card_type", "DS");
		rateResults.get(1).put("buy_rate", BigDecimal.valueOf(0.00909753d));
		rateResults.get(1).put("sell_rate", BigDecimal.valueOf(0.00903506d));
		rateResults.add(new HashMap<>());
		rateResults.get(2).put("item_key", "MC008840");
		rateResults.get(2).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(2).put("effective_start_date",sdf.parse("2020-01-25 00:00:00"));
		rateResults.get(2).put("effective_end_date", sdf.parse("2020-01-25 23:59:59"));
		rateResults.get(2).put("base_currency_code", "008");
		rateResults.get(2).put("counter_currency_code", "840");
		rateResults.get(2).put("card_type", "MC");
		rateResults.get(2).put("buy_rate", BigDecimal.valueOf(0.009136789813d));
		rateResults.get(2).put("sell_rate", BigDecimal.valueOf(0.009132222518d));
		rateResults.add(new HashMap<>());
		rateResults.get(3).put("item_key", "MC008840");
		rateResults.get(3).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(3).put("effective_start_date",sdf.parse("2020-01-26 00:00:00"));
		rateResults.get(3).put("effective_end_date", sdf.parse("2020-01-26 23:59:59"));
		rateResults.get(3).put("base_currency_code", "008");
		rateResults.get(3).put("counter_currency_code", "840");
		rateResults.get(3).put("card_type", "MC");
		rateResults.get(3).put("buy_rate", BigDecimal.valueOf(0.009236789813d));
		rateResults.get(3).put("sell_rate", BigDecimal.valueOf(0.009232222518d));
		rateResults.add(new HashMap<>());
		rateResults.get(4).put("item_key", "MC008840");
		rateResults.get(4).put("effective_date", sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(4).put("effective_start_date",sdf.parse("2020-01-27 00:00:00"));
		rateResults.get(4).put("effective_end_date", sdf.parse("2020-01-27 23:59:59"));
		rateResults.get(4).put("base_currency_code", "008");
		rateResults.get(4).put("counter_currency_code", "840");
		rateResults.get(4).put("card_type", "MC");
		rateResults.get(4).put("buy_rate", BigDecimal.valueOf(0.009136789813d));
		rateResults.get(4).put("sell_rate", BigDecimal.valueOf(0.009132222518d));

		return rateResults;
	}
}
