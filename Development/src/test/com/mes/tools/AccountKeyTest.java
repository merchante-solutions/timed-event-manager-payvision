package com.mes.tools;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

public class AccountKeyTest {

	@Test
	@SuppressWarnings({ "deprecation" })
	public void toQueryString() {
		String merchNum = "391478789";
		try {
			Assert.assertEquals(URLEncoder.encode(merchNum, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(merchNum));
		} catch (UnsupportedEncodingException e) {
			fail("UnsupportedEncodingException");
		}
	}

}
