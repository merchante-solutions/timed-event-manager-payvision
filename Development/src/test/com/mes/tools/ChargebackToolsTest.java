package com.mes.tools;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import java.sql.Date;
import java.sql.SQLException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;
import com.mes.constants.MesQueues;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ref.DefaultContext;

public class ChargebackToolsTest {

	@Before
	public void setup() {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}
	
	@Test
	public void testGetChargebackTableName_Amex() throws Exception {
		ChargebackTools cbTools = new ChargebackTools();
		String tablename = Whitebox.invokeMethod(cbTools,"getChargebackTableName","AM");
		assertEquals("network_chargeback_amex",tablename);
	}
	
	@Test
	public void testGetChargebackTableName_Discover()throws Exception{
		String tablename = Whitebox.invokeMethod(ChargebackTools.class,"getChargebackTableName","DS");
		assertEquals("network_chargeback_discover",tablename);
	}
	
	@Test
	public void testGetChargebackTableName_Mastercard()throws Exception{
		String tablename = Whitebox.invokeMethod(ChargebackTools.class,"getChargebackTableName","MC");
		assertEquals("network_chargeback_mc",tablename);
	}
	
	@Test
	public void testGetChargebackTableName_Visa()throws Exception{
		String tablename = Whitebox.invokeMethod(ChargebackTools.class,"getChargebackTableName","VS");
		assertEquals("network_chargeback_visa",tablename);
	}
	
	@Test(expected=SQLException.class)
	public void testExecuteUpdate_NullContext() throws Exception {
		DefaultContext defaultContext = null;
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = "String Param";
		
		try {
			Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		}catch(SQLException e) {
			assertEquals("found null connection context",e.getMessage());
			throw e;
		}
	}
	
	@Test(expected=SQLException.class)
	public void testExecuteUpdate_NullExecutionContext() throws Exception {
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = null;
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = "String Param";
		
		try {
			Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		}catch(SQLException e) {
			assertEquals("found null execution context",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testExecuteUpdate_String() throws Exception {
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = "String Param";
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,sqlKey,updateSql)).thenReturn(statement);
		
		doNothing().when(statement).setString(1, (String)params[0]);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testExecuteUpdate_Integer() throws Exception {
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = new Integer(100);
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,sqlKey,updateSql)).thenReturn(statement);
		
		doNothing().when(statement).setInt(1, (Integer)params[0]);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testExecuteUpdate_Double() throws Exception {
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = new Double(250.55);
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,sqlKey,updateSql)).thenReturn(statement);
		
		doNothing().when(statement).setDouble(1, (Double)params[0]);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testExecuteUpdate_Long() throws Exception {
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = new Long(500l);
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,sqlKey,updateSql)).thenReturn(statement);
		
		doNothing().when(statement).setLong(1, (Long)params[0]);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testExecuteUpdate_Date() throws Exception {
		
		String sqlKey = "sqlKey";
		String updateSql = "update sql";
		Object[] params = new Object[1];
		params[0] = new java.util.Date();
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,sqlKey,updateSql)).thenReturn(statement);
		
		doNothing().when(statement).setDate(1,new java.sql.Date(((java.util.Date)params[0]).getTime()));
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "executeUpdate", defaultContext,sqlKey,updateSql,params);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testCreateMerchantCredit() throws Exception {

		Long loadSec = 101l;
		
		Object[] params = new Object[1];
		params[0] = loadSec;
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(statement);
		
		doNothing().when(statement).setLong(1, loadSec);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Integer rowCount = Whitebox.invokeMethod(ChargebackTools.class, "createMerchantCredit", defaultContext,loadSec);
		assertEquals(new Integer(1),rowCount);
	}
	
	@Test
	public void testUpdateQueue()throws Exception{

		Long loadSec = 101l;
		Integer itemType = MesQueues.Q_ITEM_TYPE_CHARGEBACK;
		Integer queueType = MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL;
		
		Object[] params = new Object[3];
		params[0] = queueType;
		params[1] = loadSec;
		params[2] = itemType;
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(statement);
		
		doNothing().when(statement).setInt(1, queueType);
		doNothing().when(statement).setLong(2,loadSec);
		doNothing().when(statement).setInt(3,itemType);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Whitebox.invokeMethod(ChargebackTools.class, "updateQueue", defaultContext,queueType,itemType,loadSec);
	}
	
	@Test
	public void testUpdateChargebackForReversal()throws Exception{
		Long loadSec = 501l;
		java.sql.Date reversalDate = new java.sql.Date(System.currentTimeMillis());
		Double reversalAmount = 501.55;
		
		Object[] params = new Object[3];
		params[0] = reversalDate;
		params[1] = reversalAmount;
		params[2] = loadSec;
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(statement);
		
		doNothing().when(statement).setDate(1,new java.sql.Date(((java.util.Date)params[0]).getTime()));
		doNothing().when(statement).setDouble(2,reversalAmount);
		doNothing().when(statement).setLong(3,loadSec);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Whitebox.invokeMethod(ChargebackTools.class, "updateChargebackForReversal", defaultContext, loadSec, reversalDate, reversalAmount);
	}
	
	@Test
	public void testReverseCardbrandChargeback_MC() throws Exception {
		Long loadSec = 501l;
		java.sql.Date reversalDate = new java.sql.Date(System.currentTimeMillis());
		Double reversalAmount = 501.55;
		String ct = "MC";
		
		Object[] params = new Object[3];
		params[0] = reversalDate;
		params[1] = reversalAmount;
		params[2] = loadSec;
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(statement);
		
		doNothing().when(statement).setDate(1, reversalDate);
		doNothing().when(statement).setDouble(2,reversalAmount);
		doNothing().when(statement).setLong(3,loadSec);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		Whitebox.invokeMethod(ChargebackTools.class, "reverseCardbrandChargeback", defaultContext, ct, loadSec, reversalDate, reversalAmount);
	}
	
	@Test
	public void testReverseCardbrandChargeback_Amex() throws Exception {
		Long loadSec = 501l;
		java.sql.Date reversalDate = new java.sql.Date(System.currentTimeMillis());
		Double reversalAmount = 501.55;
		String ct = "AM";
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),
														   Mockito.contains("6com.mes.tools.ChargebackTools:"),
														   Mockito.contains("network_chargeback_amex"))).thenReturn(statement);
		
		doNothing().when(statement).setDate(1, reversalDate);
		doNothing().when(statement).setDouble(2,reversalAmount);
		doNothing().when(statement).setLong(3,loadSec);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(statement.getUpdateCount()).thenReturn(1);
		
		//opt blue update
		oracle.jdbc.OraclePreparedStatement optBStmt = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		when(oracleContext.prepareOracleBatchableStatement(defaultContext,"7com.mes.tools.ChargebackTools","update network_chargeback_amex_optb set reversal_date=nvl( :1, trunc(sysdate)), reversal_amount=:2 where load_sec=:3 ")).thenReturn(optBStmt);
		
		doNothing().when(optBStmt).setDate(1, reversalDate);
		doNothing().when(optBStmt).setDouble(2,reversalAmount);
		doNothing().when(optBStmt).setLong(3,loadSec);
		
		doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
		when(optBStmt.getUpdateCount()).thenReturn(1);
		
		Whitebox.invokeMethod(ChargebackTools.class, "reverseCardbrandChargeback", defaultContext, ct, loadSec, reversalDate, reversalAmount);
	}
	
	@Test
	public void testCreateChargebackActivity() throws Exception {
		Long loadSec = 1001l;
		String actionCode ="K";
		String userMsg = "Reversal - No Credit";
		
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ExecutionContext executionContext = Mockito.mock(sqlj.runtime.ExecutionContext.class);
		ExecutionContext.OracleContext oracleContext = Mockito.mock(sqlj.runtime.ExecutionContext.OracleContext.class);
		oracle.jdbc.OraclePreparedStatement statement = Mockito.mock(oracle.jdbc.OraclePreparedStatement.class);
		
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(),
														   Mockito.contains("com.mes.tools.ChargebackTools"),
														   Mockito.contains("insert into network_chargeback_activity"))).thenReturn(statement);
		
		doNothing().when(statement).setString(1, actionCode);
		doNothing().when(statement).setString(2, userMsg);
		doNothing().when(statement).setString(3, "system");
		doNothing().when(statement).setLong(4, loadSec);
		
		when(statement.getUpdateCount()).thenReturn(1);
		
		Whitebox.invokeMethod(ChargebackTools.class, "createChargebackActivity", defaultContext, loadSec, actionCode, userMsg);
	}
}
