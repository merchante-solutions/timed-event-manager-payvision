package com.mes.tools;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

public class EquipmentKeyTest {

	@Test
	@SuppressWarnings({ "deprecation" })
	public void toQueryString_TermAppProfileSummaryKey() {
		String someString = "E36B";
		try {
			Assert.assertEquals(URLEncoder.encode(someString, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(someString));
		} catch (UnsupportedEncodingException e) {
			fail("UnsupportedEncodingException");
		}
	}
	
	@Test
	@SuppressWarnings({ "deprecation" })
	public void toQueryString_EquipInvKey() {
		String someString = "E36B";
		try {
			Assert.assertEquals(URLEncoder.encode(someString, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(someString));
		} catch (UnsupportedEncodingException e) {
			fail("UnsupportedEncodingException");
		}
	}
	
	@Test
	@SuppressWarnings({ "deprecation" })
	public void toQueryString_MerchEquipKey() {
		String someString = "E36B";
		try {
			Assert.assertEquals(URLEncoder.encode(someString, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(someString));
		} catch (UnsupportedEncodingException e) {
			fail("UnsupportedEncodingException");
		}
	}

}
