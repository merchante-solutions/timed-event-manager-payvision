package com.mes.tools;

import org.junit.Assert;
import org.junit.Test;

public class FixHierarchyBeanTest {

	@Test
	@SuppressWarnings("deprecation")
	public void checkOrg() {
		int assoc = 1234;
		Assert.assertEquals(new Integer(assoc).toString(), String.valueOf(assoc));		
	}

	@Test
	@SuppressWarnings("deprecation")
	public void getOrgNum() {
		int assoc = 1234;
		Assert.assertEquals(new Integer(assoc).toString(), String.valueOf(assoc));		
	}

	@Test
	@SuppressWarnings("deprecation")
	public void addParentOrg() {
		int assoc = 1234;
		Assert.assertEquals(new Integer(assoc).toString(), String.valueOf(assoc));		
	}

}
