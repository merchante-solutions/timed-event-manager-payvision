package com.mes.tools;

import org.junit.Assert;
import org.junit.Test;

public class GroupFixerTest {

	@Test
	@SuppressWarnings({ "deprecation" })
	public void HierarchyConstructor() {
		long testLong = 12345L;
		Long autoBoxedLong = testLong;
		Assert.assertEquals(new Long(testLong), autoBoxedLong);
	}

	@Test
	@SuppressWarnings({ "deprecation" })
	public void addRow() {
		long testLong = 12345L;
		Long autoBoxedLong = testLong;
		Assert.assertEquals(new Long(testLong), autoBoxedLong);
	}

}
