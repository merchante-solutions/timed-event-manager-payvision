package com.mes.tools;

import org.apache.xalan.xsltc.runtime.Hashtable;
import org.junit.Assert;
import org.junit.Test;

public class THierarchyTest {

	@Test
	@SuppressWarnings({ "deprecation" })
	public void loadHierarchy() {
		long testLong = 12345L;
		Long autoBoxedLong = testLong;
		Assert.assertEquals(new Long(testLong), autoBoxedLong);
	}

	@Test
	@SuppressWarnings({ "deprecation" })
	public void getNodeOpen() {
		long testLong = 12345L;
		boolean someBoolean = true;
		Long autoBoxedLong = testLong;
		Assert.assertEquals(new Long(testLong), autoBoxedLong);
	}
	
	@Test
	@SuppressWarnings({ "deprecation" })
	public void toggleStatus() {
		long testLong = 12345L;
		boolean someBoolean = true;
		Boolean autoBoxedBoolean = !someBoolean;

		Long autoBoxedLong = testLong;
		Assert.assertEquals(new Long(testLong), autoBoxedLong);
		Assert.assertEquals(new Boolean(!someBoolean), autoBoxedBoolean);
	}

}
