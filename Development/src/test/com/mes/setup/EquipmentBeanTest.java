package com.mes.setup;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class EquipmentBeanTest {

	private final static Integer RETAIL = 1;

	private int imprinterPlates = 1;

	@Test
	public void testGetImprinterPlates() {
		if (this.imprinterPlates > 0) {
			String deprecatedWay = new Integer(this.imprinterPlates).toString();
			String nonDeprecatedWay = String.valueOf(this.imprinterPlates);
			assertEquals(deprecatedWay, nonDeprecatedWay);
		}
	}

	@Test
	public void testStaticFields() {

		Integer RETAILDep = new Integer(1);

		assertEquals(RETAIL, RETAILDep);
	}

}
