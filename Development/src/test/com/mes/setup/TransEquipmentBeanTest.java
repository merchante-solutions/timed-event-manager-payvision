package com.mes.setup;

import org.junit.Assert;
import org.junit.Test;

public class TransEquipmentBeanTest {

    @Test
    public void testGetImprinterPlates() {
        // Given
        int fakeImprinterPlates = 53657;
        int number = 2;

        // When

        // old way
        String expectedResult = new Integer(fakeImprinterPlates).toString();
        Integer oldWayFakeRestaurant = new Integer(number);

        // new way
        String actualResult = String.valueOf(fakeImprinterPlates);
        Integer newWayFakeRestaurant = number;

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", expectedResult, actualResult);
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayFakeRestaurant, newWayFakeRestaurant);
    }
}