package com.mes.setup;

import org.junit.Assert;
import org.junit.Test;

public class AppIdBeanTest {

    @Test
    public void testSetSeqNum() {
        // Given
        int fakeYear = 99865;

        // old way
        String oldWayYear = new Integer(fakeYear).toString();

        // new way
        String newWayYear = Integer.toString(fakeYear);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayYear, newWayYear);
    }
}