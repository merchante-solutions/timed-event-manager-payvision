package com.mes.setup;

import org.junit.Assert;
import org.junit.Test;

public class AppEditBeanTest {

    @Test
    public void testValidDate() {
        // Given
        final int nextDay = 30;

        // When

        // old way
        final String oldWayResult = new Integer(nextDay).toString();

        // new way
        final String newWayResult = Integer.toString(nextDay);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetSpecificDateNext() {
        // Given
        final int nextDay = 15;

        // When

        // old way
        final String oldWayResult = new Integer(nextDay).toString();

        // new way
        final String newWayResult = Integer.toString(nextDay);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetControlNumber() {
        // Given
        final long controlNumber = 53678885L;

        // When

        // old way
        final String oldWayResult = new Long(controlNumber).toString();

        // new way
        final String newWayResult = Long.toString(controlNumber);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetMerchantNumber() {
        // Given
        final long merchantNumber = 123095674L;

        // When

        // old way
        final String oldWayResult = new Long(merchantNumber).toString();

        // new way
        final String newWayResult = Long.toString(merchantNumber);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }
}