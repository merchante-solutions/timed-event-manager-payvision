package com.mes.setup;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PricingBeanTest {

	private double transDiscountRate10 = -1;

	@Test
	@SuppressWarnings({"deprecation"})
	public void testGetTax() {
		double subtotalPur = 0.0;
		double taxRate = 4000.99;
		double oldWay = new Double(subtotalPur * taxRate / 100.0).doubleValue();
		double newWay = Double.valueOf(subtotalPur * taxRate / 100.0);
		// assertEquals(double,double) is deprecated so need to pass delta precision
		assertEquals(oldWay, newWay, 0.00001);

	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testFormatCurrency() {
		String deprecatedWay = new Double(this.transDiscountRate10).toString();
		String nonDeprecatedWay = String.valueOf(transDiscountRate10);

		assertEquals("Both deprecatedWay and nonDeprecatedWay need to be same", deprecatedWay, nonDeprecatedWay);
	}

}
