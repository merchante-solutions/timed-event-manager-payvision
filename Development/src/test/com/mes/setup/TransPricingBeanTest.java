package com.mes.setup;

import org.junit.Assert;
import org.junit.Test;

public class TransPricingBeanTest {

    @Test
    public void testGetTax() {
        // Given
        final double fakeTaxRate = 456.67;
        final double fakeSubtotalPur = 3.567;

        // When

        // old way
        final double oldWayResult = new Double(fakeSubtotalPur * fakeTaxRate / 100.0).doubleValue();

        // new way
        final double newWayResult = fakeSubtotalPur * fakeTaxRate / 100.0;

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult, Double.MIN_VALUE);
    }

    @Test
    public void testGetTransDiscountRate9() {
        // Given
        final double transDiscountRate9 = 465375.645;

        // When

        // old way
        final String oldWayResult = new Double(transDiscountRate9).toString();

        // new way
        final String newWayResult = String.valueOf(transDiscountRate9);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransDiscountRate10() {
        // Given
        final double transDiscountRate10 = 9532.31140;

        // When

        // old way
        final String oldWayResult = new Double(transDiscountRate10).toString();

        // new way
        final String newWayResult = String.valueOf(transDiscountRate10);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransPerItem10() {
        // Given
        final double transPerItem10 = 1345.6789;

        // When

        // old way
        final String oldWayResult = new Double(transPerItem10).toString();

        // new way
        final String newWayResult = String.valueOf(transPerItem10);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransPerItem11() {
        // Given
        final double transPerItem11 = 97865.1234;

        // When

        // old way
        final String oldWayResult = new Double(transPerItem11).toString();

        // new way
        final String newWayResult = String.valueOf(transPerItem11);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransDiscountRate11() {
        // Given
        final double transDiscountRate11 = 98421.3756;

        // When

        // old way
        final String oldWayResult = new Double(transDiscountRate11).toString();

        // new way
        final String newWayResult = String.valueOf(transDiscountRate11);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransPerItem12() {
        // Given
        final double transPerItem12 = 98701.2345;

        // When

        // old way
        final String oldWayResult = new Double(transPerItem12).toString();

        // new way
        final String newWayResult = String.valueOf(transPerItem12);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTransDiscountRate12() {
        // Given
        final double transDiscountRate12 = 4231.7896;

        // When

        // old way
        final String oldWayResult = new Double(transDiscountRate12).toString();

        // new way
        final String newWayResult = String.valueOf(transDiscountRate12);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testGetTaxRate() {
        // Given
        final double taxRate = 24.68687;

        // When

        // old way
        final String oldWayResult = Double.toString(taxRate);

        // new way
        final String newWayResult = String.valueOf(taxRate);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
    }

    @Test
    public void testFormatCurrency() {
        // Given
        final int tempInt = 65;
        final String subString = "fake-sub-string";

        // When

        // old way
        final String oldWayResult = new Integer(tempInt).toString() + ".00";
        final String oldWayResult2 = subString + "." + new Integer(tempInt).toString();

        // new way
        final String newWayResult = tempInt + ".00";
        final String newWayResult2 = subString + "." + tempInt;

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult, newWayResult);
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayResult2, newWayResult2);
    }
}