package com.mes.setup;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MerchInfoBeanTest {
	private int imprinterPlates = 1;

	@Test
	@SuppressWarnings({"deprecation"})
	public void testGetImprinterPlates() {

		if (this.imprinterPlates > 0) {
			String deprecatedWay = new Integer(this.imprinterPlates).toString();
			String nonDeprecatedWay = String.valueOf(this.imprinterPlates);

			assertEquals(deprecatedWay, nonDeprecatedWay);
		}
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testIsValidMod10check() {

		StringBuffer sb = new StringBuffer();
		sb.append(6666);
		int multiplier = 10;
		int m = 2;
		int sumOldWay = (Integer.parseInt((new Character(sb.charAt(m))).toString()) * multiplier);
		int sumNewWay = (Integer.parseInt(Character.toString(sb.charAt(m))) * multiplier);

		assertEquals(sumOldWay, sumNewWay);
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testSetAmexRate() {
		String oldWay = "12322";

		assertEquals(new Double(oldWay.trim()).toString(), Double.valueOf(oldWay.trim()).toString());
	}

}