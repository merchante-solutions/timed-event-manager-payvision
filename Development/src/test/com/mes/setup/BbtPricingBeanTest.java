package com.mes.setup;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BbtPricingBeanTest {

	@Test
	@SuppressWarnings({"deprecation"})
	public void getTaxTest() {
		double subtotalPur = 0.0;
		double taxRate = 5000.88;
		double oldWay = new Double(subtotalPur * taxRate / 100.0).doubleValue();
		double newWay = Double.valueOf(subtotalPur * taxRate / 100.0);
		// here 0.00001 is delta precision loss
		assertEquals(oldWay, newWay, 0.00001);
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testDoubleToString() {
		double bbtDiscountRate13 = -1;
		String deprecatedWay = new Double(bbtDiscountRate13).toString();
		String nonDeprecatedWay = String.valueOf(bbtDiscountRate13);
		assertEquals(deprecatedWay, nonDeprecatedWay);
	}

}