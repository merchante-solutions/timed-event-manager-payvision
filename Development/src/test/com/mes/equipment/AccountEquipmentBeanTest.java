package com.mes.equipment;

import static org.junit.Assert.assertEquals;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.junit.Test;

public class AccountEquipmentBeanTest {

	@Test
	@SuppressWarnings({"deprecation"})
	public void testLoadString() throws URISyntaxException, UnsupportedEncodingException {
		String testUrl = "http://www.test.com?key1=value+1&key2=value%40%21%242&key3=value%253";
		URI uri = new URI(testUrl);
		assertEquals(URLEncoder.encode(uri.getRawQuery()), URLEncoder.encode(uri.getRawQuery(), StandardCharsets.UTF_8.toString()));
	}

}