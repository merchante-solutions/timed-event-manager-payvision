package com.mes.events;

import org.junit.Assert;
import org.junit.Test;

public class ServerThreadTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void performAction() {
        // Given
        long upTime = System.nanoTime();
        boolean isShutdown = upTime % 2 == 1;

        // When

        // old way
        final Long oldWayLongObject = new Long(upTime);
        final Boolean oldWayBooleanObject = new Boolean(isShutdown);

        // Then
        Assert.assertTrue(MESSAGE, oldWayLongObject.equals(upTime));
        Assert.assertTrue(MESSAGE, oldWayLongObject == upTime);

        Assert.assertTrue(MESSAGE, oldWayBooleanObject.equals(isShutdown));
        Assert.assertTrue(MESSAGE, oldWayBooleanObject == isShutdown);
    }

    @Test
    public void notifyInTheWeeds() {
        // Given
        int id = System.identityHashCode(this);
        long curTime = System.currentTimeMillis();

        // When

        // old way
        final Integer oldWayInteger = new Integer(id);
        final Long oldWayLongObject = new Long(curTime);

        // Then
        Assert.assertTrue(MESSAGE, oldWayInteger.equals(id));
        Assert.assertTrue(MESSAGE, oldWayInteger == id);

        Assert.assertTrue(MESSAGE, oldWayLongObject.equals(curTime));
        Assert.assertTrue(MESSAGE, oldWayLongObject == curTime);
    }
}
