package com.mes.events;

import org.junit.Assert;
import org.junit.Test;

public class ServerListenThreadTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void shutdown() {
        // Given
        long curTime = System.currentTimeMillis();

        // When

        // old way
        final Long oldWayLongObject = new Long(curTime);

        // Then
        Assert.assertTrue(MESSAGE, curTime == oldWayLongObject);
        Assert.assertTrue(MESSAGE, oldWayLongObject.equals(curTime));
    }
}
