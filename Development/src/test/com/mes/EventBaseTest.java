package com.mes;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import oracle.jdbc.OracleConnection;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.startup.EventBase;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DbProperties.class, SQLJConnectionBase.class, EventBase.class, MailMessage.class, RuntimeRefErrors.class})
public abstract class EventBaseTest {

    @Mock
    public ExecutionContext executionContext;
    @Mock
    public ResultSetMetaData resultSetMetaData;
    @Mock
    public OracleResultSet oracleResultSet;
    @Mock
    public ResultSetIterator resultSetIterator;
    @Mock
    public DefaultContext ctx;
    @Mock
    public ExecutionContext.OracleContext oracleContext;
    @Mock
    public OraclePreparedStatement oraclePreparedStatement;
    @Mock
    public OracleConnection connection;

    @BeforeClass
    public static void setUpBeforeClass() {
        System.setProperty("log4j.configuration", "Development/log4j.properties");
    }

    @Before
    public void setUp() throws Exception {
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
        PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));

        /* Mock static method calls */
        PowerMockito.mockStatic(MailMessage.class);
        PowerMockito.doNothing().when(MailMessage.class, "sendSystemErrorEmail", anyString(), anyString());
        PowerMockito.mockStatic(DbProperties.class);
        PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

        // Mock database classes
        executionContext = Mockito.mock(ExecutionContext.class);
        resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        oracleResultSet = Mockito.mock(OracleResultSet.class);
        resultSetIterator = Mockito.spy(ResultSetIterator.class);
        ctx = Mockito.mock(DefaultContext.class);
        oracleContext = Mockito.mock(ExecutionContext.OracleContext.class);
        oraclePreparedStatement = Mockito.mock(OraclePreparedStatement.class);
    }

    public void mockDatabaseCalls() throws SQLException {
        Mockito.when(ctx.getExecutionContext()).thenAnswer((Answer<ExecutionContext>) invocation -> executionContext);
        Mockito.when(executionContext.getOracleContext()).thenReturn(oracleContext);
        Mockito.when(oracleContext.prepareOracleStatement(any(DefaultContext.class), anyString(), anyString())).thenReturn(oraclePreparedStatement);
        Mockito.when(oracleContext.isNew()).thenReturn(false);
        Mockito.when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
        Mockito.when(oracleResultSet.getMetaData()).thenReturn(resultSetMetaData);
        Mockito.when(resultSetMetaData.getColumnCount()).thenReturn(1);
        Mockito.when(oracleResultSet.getString(1)).thenReturn("N").thenReturn("Y");
        Mockito.when(oracleResultSet.next()).thenReturn(false).thenReturn(true);
        Mockito.doNothing().when(oracleResultSet).close();
        Mockito.doNothing().when(ctx).close();
    }
}
