package com.mes.prospects;

import static org.junit.Assert.assertEquals;
import org.apache.batik.dom.util.HashTable;
import org.junit.Before;
import org.junit.Test;

public class PipelineReportTest {
	private HashTable deprecatedWay = new HashTable();
	private HashTable nonDeprecatedWay = new HashTable();
	private static long KEY = 43242434L;
	private static String VALUE = "testvalue";

	@Before
	@SuppressWarnings({"deprecation"})
	public void setUp() {
		deprecatedWay.put(new Long(KEY), VALUE);
		nonDeprecatedWay.put(KEY, VALUE);
	}

	@Test
	public void testReportVector() {
		assertEquals(deprecatedWay.get(VALUE), nonDeprecatedWay.get(VALUE));
	}

}