package com.mes.prospects;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ActivityReportTest {
	private static final long testValue = 323423423L;

	@Test
	@SuppressWarnings({"deprecation"})
	public void testDirectReportVector() {
		Long deprecated = new Long(testValue);
		Long nonDeprecated = testValue;
		assertEquals("Deprecated values should be equals with nonDeprecated", deprecated, nonDeprecated);
	}
}
