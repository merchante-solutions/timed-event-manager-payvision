package com.mes.tpg;

import org.junit.Assert;
import org.junit.Test;

public class TpgPreAuthDataBeanTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void createDashboardDataJsonObj() {
        // Given
        double primitiveDouble = 45531364.98755353;
        int primitiveInteger = 64786868;

        // When

        // old way
        final Double oldWayDouble = new Double(primitiveDouble);
        final Integer oldWayInteger = new Integer(primitiveInteger);

        // Then
        
        Assert.assertEquals(oldWayDouble, primitiveDouble, Double.MIN_VALUE);
        Assert.assertTrue(MESSAGE, oldWayInteger.equals(primitiveInteger));
        Assert.assertTrue(MESSAGE, oldWayInteger == primitiveInteger);
    }
}
