package com.mes.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.mes.support.StringUtilities;

public class StringUtilitiesTest {

	@Test
	public void testDataSwapWithStr1Null() {

		assertEquals("str 2", StringUtilities.dataSwap(null, "str 2"));
		assertEquals("---str 2---", StringUtilities.dataSwap(null, "---str 2---"));
		assertEquals(null, StringUtilities.dataSwap(null, "    "));
		assertEquals("    str 2", StringUtilities.dataSwap(null, "    str 2"));
		assertEquals(null, StringUtilities.dataSwap(null, ""));
		assertEquals(null, StringUtilities.dataSwap(null, null));

	}

	@Test
	public void testDataSwapWithStr1Blank() {

		assertEquals("str 2", StringUtilities.dataSwap("", "str 2"));
		assertEquals("---str 2---", StringUtilities.dataSwap("", "---str 2---"));
		assertEquals("", StringUtilities.dataSwap("", "    "));
		assertEquals("    str 2", StringUtilities.dataSwap("", "    str 2"));
		assertEquals("", StringUtilities.dataSwap("", ""));
		assertEquals("", StringUtilities.dataSwap("", null));

	}

	@Test
	public void testDataSwapWithStr1Space() {

		assertEquals("str 2", StringUtilities.dataSwap("   ", "str 2"));
		assertEquals("---str 2---", StringUtilities.dataSwap("   ", "---str 2---"));
		assertEquals(" ", StringUtilities.dataSwap(" ", "    "));
		assertEquals(" ", StringUtilities.dataSwap(" ", null));
		assertEquals(" str 1 ", StringUtilities.dataSwap(" str 1 ", "    str 2"));

	}

	@Test
	public void testDataSwapWithStr1Hypen() {

		assertEquals("str 2", StringUtilities.dataSwap("----", "str 2"));
		assertEquals("str 2", StringUtilities.dataSwap("  ----  ", "str 2"));
		assertEquals("Str ----  1", StringUtilities.dataSwap("Str ----  1", "str 2"));
		assertEquals("Str ----  1---", StringUtilities.dataSwap("Str ----  1---", "str 2"));
		assertEquals("---Str ----  1---", StringUtilities.dataSwap("---Str ----  1---", "str 2"));
		assertEquals("---Str 1", StringUtilities.dataSwap("---Str 1", "str 2"));
		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", "str 2"));

		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", null));
		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", "-----"));
		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", "     --    "));
		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", "        "));
		assertEquals("Str 1---", StringUtilities.dataSwap("Str 1---", "    str -- 2    "));

		assertEquals("----", StringUtilities.dataSwap("----", ""));
		assertEquals("----", StringUtilities.dataSwap("----", null));
		assertEquals("----", StringUtilities.dataSwap("----", " "));

	}

	@Test
	public void testDataSwapWithStr2Null() {

		assertEquals("str 1", StringUtilities.dataSwap("str 1", null));
		assertEquals("  str 1   ", StringUtilities.dataSwap("  str 1   ", null));
		assertEquals("-----", StringUtilities.dataSwap("-----", null));
		assertEquals("--str 1---", StringUtilities.dataSwap("--str 1---", null));
	}

	@Test
	public void testDataSwapWithStr2Blank() {

		assertEquals("str 1", StringUtilities.dataSwap("str 1", ""));
		assertEquals("  str 1   ", StringUtilities.dataSwap("  str 1   ", ""));
		assertEquals("-----", StringUtilities.dataSwap("-----", ""));
		assertEquals("--str 1---", StringUtilities.dataSwap("--str 1---", ""));
	}

	@Test
	public void testDataSwapWithStr2Space() {

		assertEquals("str 1", StringUtilities.dataSwap("str 1", "    "));
		assertEquals("  str 1   ", StringUtilities.dataSwap("  str 1   ", "    "));
		assertEquals("-----", StringUtilities.dataSwap("-----", "    "));
		assertEquals("--str 1---", StringUtilities.dataSwap("--str 1---", "    "));
	}

	@Test
	public void testDataSwapWithStr2DataSpace() {

		assertEquals("str 1", StringUtilities.dataSwap("str 1", "str 2"));
		assertEquals("str 1", StringUtilities.dataSwap("str 1", "  str 2  "));
		assertEquals("   str 1   ", StringUtilities.dataSwap("   str 1   ", "  str 2  "));
	}
}
