package com.mes.util;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mes.utils.MesStringUtil;

public class StringUtilsTest {

	private final String PASSWORD = "1287sadffsgsg3ryqw9yh233";
	private final String USERNAME = "mes_api_sandbox";
	private final String MASK = "******";
	private static final String API_PASSWORD_REGEX = "api_password=\\[*[a-zA-Z0-9]*\\]*[,}]";

	@Test
	public void sanitizeTest() throws JsonParseException, JsonMappingException, IOException {
		String formData = "{dba_zipcode=[368773749], api_username=[" + USERNAME + "], first_name=[THE], phone=[7065363027],api_password=[" + PASSWORD + "], api_password=" + PASSWORD + ", dba_city=[SMITHS STATION], last_name=[DUGOUT], dba_state=[AL], dba_address=[9571 LEE ROAD 246], products[]=[[{\"id\":null,\"quantity\":1},{\"id\":null,\"quantity\":1}]], dba_name=[THE DUGOUT], merchant_id=[10228095],api_password=" + PASSWORD + "}";
		String expectedResults = "{dba_zipcode=[368773749], api_username=[" + USERNAME + "], first_name=[THE], phone=[7065363027],api_password=" + MASK + ", api_password=" + MASK + ", dba_city=[SMITHS STATION], last_name=[DUGOUT], dba_state=[AL], dba_address=[9571 LEE ROAD 246], products[]=[[{\"id\":null,\"quantity\":1},{\"id\":null,\"quantity\":1}]], dba_name=[THE DUGOUT], merchant_id=[10228095],api_password=" + MASK + "}";
		String sanitizedData = MesStringUtil.sanitize(formData, API_PASSWORD_REGEX);
	
		assertTrue(sanitizedData.indexOf(USERNAME) > -1);
		assertEquals(sanitizedData.indexOf(PASSWORD), -1);
		assertEquals(sanitizedData.trim(), expectedResults.trim());

	}

	@Test
	public void sanitizeTestNull() throws JsonParseException, JsonMappingException, IOException {
		String sanitizedData = MesStringUtil.sanitize(null, API_PASSWORD_REGEX);
		assertNull(sanitizedData);

	}

	@Test
	public void sanitizeTestNoPassword() throws JsonParseException, JsonMappingException, IOException {
		String formData = "no password";
		String expectedResults = "no password";
		String sanitizedData = MesStringUtil.sanitize(formData, API_PASSWORD_REGEX);
		assertEquals(sanitizedData.trim(), expectedResults.trim());

	}

}
