package com.mes.app.verisign;

import static org.junit.Assert.assertEquals;
import java.util.Vector;
import org.junit.Before;
import org.junit.Test;

public class VaEndTest {
	private static int testValue = 33;
	@SuppressWarnings("rawtypes")
	private Vector deprecatedVector = new Vector();
	@SuppressWarnings("rawtypes")
	private Vector nonDeprecatedVector = new Vector();

	@Before
	@SuppressWarnings({"deprecation"})
	public void setUp() {
		deprecatedVector.add(new Integer(testValue));
		nonDeprecatedVector.add(testValue);
	}

	@Test
	public void testLoadCardsAccepted() {
		assertEquals(deprecatedVector.get(0), nonDeprecatedVector.get(0));
	}

}