package com.mes.app.maintenance;

import static org.junit.Assert.assertEquals;
import java.util.Hashtable;
import org.junit.Before;
import org.junit.Test;

public class HierarchyLoaderTest {
	private static long nodeId = 3423423423L;
	private static String keyValue = "testKeyValue";
	@SuppressWarnings("rawtypes")
	private Hashtable deprecatedHash = new Hashtable();
	@SuppressWarnings("rawtypes")
	private Hashtable nonDeprecatedHash = new Hashtable();

	@Before
	@SuppressWarnings({"deprecation"})
	public void setUp() {
		deprecatedHash.put(new Long(nodeId), keyValue);
		nonDeprecatedHash.put(nodeId, keyValue);
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testGetKey() {
		Long deprecatedKey = new Long(nodeId);
		Long nonDeprecatedKey = Long.valueOf(nodeId);

		assertEquals("DeprecatedKey value need to be same as nonDeprecatedKey value", deprecatedKey, nonDeprecatedKey);
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testLoadHierarchy() {
		assertEquals(deprecatedHash.get(deprecatedHash.keySet().toArray()[0]), nonDeprecatedHash.get(nonDeprecatedHash.keySet().toArray()[0]));
	}

}
