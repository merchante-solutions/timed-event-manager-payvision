package com.mes.acr;

import org.junit.Assert;
import org.junit.Test;

public class ACRDataBean_DDA_BaseTest {

	@Test
	public void isTrnMod10Compliant() {
		char testChar ='7';
		int oldSchool = Integer.parseInt((new Character(testChar).toString()));
		int newSchool = Integer.parseInt(String.valueOf(testChar));
		
		Assert.assertEquals(oldSchool, newSchool);
		
		String oldString = new Character(testChar).toString();
		String newString = testChar + "";

		Assert.assertEquals(oldString, newString);
	}

}
