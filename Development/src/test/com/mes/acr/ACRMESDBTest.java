package com.mes.acr;

import org.junit.Assert;
import org.junit.Test;

public class ACRMESDBTest {

	@Test
	public void getACRRelatedObject() {
		long longId = 123L;
		Assert.assertEquals(new Long(longId).intValue(), Long.valueOf(longId).intValue());
	}
}
