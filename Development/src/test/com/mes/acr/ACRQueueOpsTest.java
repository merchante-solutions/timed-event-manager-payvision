package com.mes.acr;

import org.junit.Assert;
import org.junit.Test;

import com.mes.constants.MesQueues;

public class ACRQueueOpsTest {

	@Test
	public void getBBTQueueOps() {
		Integer autoBoxedInt = MesQueues.Q_CHANGE_REQUEST_BBT_PENDING;
		Assert.assertEquals(new Integer(MesQueues.Q_CHANGE_REQUEST_BBT_PENDING), autoBoxedInt);
	}
	
	@Test
	public void getMESQueueOps() {
		Integer autoBoxedInt = MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP;
		Assert.assertEquals(new Integer(MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP), autoBoxedInt);
	}

}
