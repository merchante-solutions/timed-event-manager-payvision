package com.mes.acr;

import org.junit.Assert;
import org.junit.Test;

public class ACRDataBeanBaseTest {

	@Test
	public void addCommand() {
		int colNum = 5;
		Integer autoboxedInt = colNum;
		Assert.assertEquals(new Integer(colNum), autoboxedInt);
	}

}
