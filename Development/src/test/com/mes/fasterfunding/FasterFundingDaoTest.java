package com.mes.fasterfunding;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.mes.fasterfunding.FasterFundingDao;

@RunWith(MockitoJUnitRunner.class)
public class FasterFundingDaoTest {
    
    @Mock
    Connection con;
    
    @Mock
    PreparedStatement ps;
    
    @Mock
    ResultSet rs;
    
    @InjectMocks
    FasterFundingDao fasterFundingDao;
    
    @Before
    public void setup() throws SQLException {
        Mockito.when(con.prepareStatement(Mockito.anyString())).thenReturn(ps);
        Mockito.when(ps.executeQuery()).thenReturn(rs);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
    }
    
    @Test
    public void testloadFasterFundingConfig() throws SQLException {
        Mockito.when(rs.getLong("ELIGIBLE_BANK")).thenReturn(3943L).thenReturn(3003L);
        Mockito.when(rs.getString("ELIGIBLE_ENTRY_DESCRIPTION")).thenReturn("MERCH DEP,AMEX DEP").thenReturn("MERCH DEP|AMEX DEP");
        Map<Long,String> configDetails = fasterFundingDao.loadFasterFundingConfig();
        assertEquals(configDetails.get(3943L), "MERCH DEP,AMEX DEP");
        assertTrue(configDetails.keySet().contains(3943L));
        assertTrue(configDetails.keySet().contains(3003L));
        assertFalse(configDetails.keySet().contains(3941L));
    }

}
