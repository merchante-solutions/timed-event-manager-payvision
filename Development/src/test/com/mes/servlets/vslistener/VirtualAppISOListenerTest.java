package com.mes.servlets.vslistener;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class VirtualAppISOListenerTest {

    @Test
    @SuppressWarnings({"deprecation"})
    public void testGetRequestData() throws UnsupportedEncodingException {
        // Given
        final char[] inputBuffer = {'T', 'h', 'e', '+', 'q', 'u', 'i', 'c', 'k', '+', 'b', 'r', 'o', 'w', 'n', '+', 'f', 'o', 'x', '+', 'j', 'u', 'm', 'p', 'e',
                'd', '+', 'o', 'v', 'e', 'r', '+', 't', 'h', 'e', '+', 'l', 'a', 'z', 'y', '+', 'd', 'o', 'g', '%', '2', '1'};

        // When

        // old way
        final String oldWayDecode = URLDecoder.decode(String.valueOf(inputBuffer));
        // new way
        final String newWayDecode = URLDecoder.decode(String.valueOf(inputBuffer), StandardCharsets.UTF_8.name());

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayDecode, newWayDecode);
    }
}