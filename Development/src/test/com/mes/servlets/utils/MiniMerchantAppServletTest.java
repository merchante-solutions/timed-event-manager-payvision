package com.mes.servlets.utils;

import org.junit.Assert;
import org.junit.Test;

public class MiniMerchantAppServletTest {

    @Test
    public void testRoute() {
        // Given
        final int appType = 5678;
        final Integer oldWayInteger = new Integer(appType);

        // Then
        Assert.assertTrue("Pre- and post- upgrade values should be equal", oldWayInteger.equals(appType));
        Assert.assertTrue("Pre- and post- upgrade values should be equal", appType == oldWayInteger);
    }
}
