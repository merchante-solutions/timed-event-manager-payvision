package com.mes.servlets.utils;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class EquipProfileServletTest {

    @Test
    @SuppressWarnings({"deprecation"})
    public void testDoCommand() throws UnsupportedEncodingException {
        // Given
        final String destination = "/srv/eqprof?method=rlfp";

        // When
        final String oldWayEncode = URLEncoder.encode(destination);
        final String newWayEncode = URLEncoder.encode(destination, StandardCharsets.UTF_8.name());

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayEncode, newWayEncode);
    }
}
