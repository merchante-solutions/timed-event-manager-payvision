package com.mes.flatfile;

import static org.junit.Assert.assertEquals;
import java.util.Vector;
import org.junit.Test;

public class MASTapeTest {
	private static int testValue = 44;
	@SuppressWarnings("rawtypes")
	private Vector deprecatedVector = new Vector();
	@SuppressWarnings("rawtypes")
	private Vector nonDeprecatedVector = new Vector();

	@Test
	@SuppressWarnings({"deprecation"})
	public void testBuildMASTape() {
		deprecatedVector.add(new Integer(testValue));
		nonDeprecatedVector.add(testValue);

		assertEquals(deprecatedVector.get(0), nonDeprecatedVector.get(0));
	}

}