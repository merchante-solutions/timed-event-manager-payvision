package com.mes.flatfile;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class GCFOrderFileTest {
	private long primitiveLong = 2222L;
	@SuppressWarnings("rawtypes")
	private List deprecatedWay = new ArrayList();
	@SuppressWarnings("rawtypes")
	private List nonDeprecatedWay = new ArrayList();

	@Test
	@SuppressWarnings({"deprecation"})
	public void testBuild() {
		deprecatedWay.add(new Long(primitiveLong));
		nonDeprecatedWay.add(primitiveLong);
		
		assertEquals(deprecatedWay.get(0), nonDeprecatedWay.get(0));
	}

}
