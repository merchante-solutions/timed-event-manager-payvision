package com.mes.extraction.utils;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mes.extraction.utils.ExtractionConstants;;

@RunWith(MockitoJUnitRunner.class)
public class ExtractionConstantsTest {
	
	@Test
	public void testExtractionConstantsValues() {
		ExtractionConstants pt_discount_ic_calc = ExtractionConstants.PT_DISCOUNT_IC_CALC;
		assertEquals(pt_discount_ic_calc.getValue(), 1);
		assertEquals(ExtractionConstants.PT_DISCOUNT_IC_CALC.getValue(),1);
	}

}
