package com.mes.tmg;

import org.junit.Assert;
import org.junit.Test;

public class NoteTest {

	@Test
	@SuppressWarnings("deprecation")
	public void hashCodeMethod() {
		long noteId = 123L;
		Long autoBoxedLong = noteId;
		Assert.assertEquals(new Long(noteId).hashCode(), Long.valueOf(noteId).hashCode());
	}

}
