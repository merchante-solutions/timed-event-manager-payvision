package com.mes.tmg.inventory;

import org.junit.Assert;
import org.junit.Test;

public class AmpViewStateTest {

	@Test
	@SuppressWarnings("deprecation")
	public void partKey() {
		int partIdx = 123;
		Integer autoBoxedInt = partIdx;
		Assert.assertEquals(new Integer(partIdx), autoBoxedInt);
	}
}
