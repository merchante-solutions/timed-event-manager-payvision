package com.mes.tmg.inventory;

import org.junit.Assert;
import org.junit.Test;

public class IndexedPartTest {

	@Test
	@SuppressWarnings("deprecation")
	public void IndexedPart() {
		int partIdx = 123;
		Integer autoBoxedInt = partIdx;
		Assert.assertEquals(new Integer(partIdx), autoBoxedInt);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void makeKey() {
		int partIdx = 123;
		Integer autoBoxedInt = partIdx;
		Assert.assertEquals(new Integer(partIdx), autoBoxedInt);
	}

}
