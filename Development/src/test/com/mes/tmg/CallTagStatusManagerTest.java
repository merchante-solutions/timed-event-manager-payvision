package com.mes.tmg;

import org.junit.Assert;
import org.junit.Test;

public class CallTagStatusManagerTest {

	@Test
	@SuppressWarnings("deprecation")
	public void addStatus() {
		long partId = 123L;
		Long autoBoxedLong = partId;
		Assert.assertEquals(new Long(partId), autoBoxedLong);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void getStatus() {
		long partId = 123L;
		Long autoBoxedLong = partId;
		Assert.assertEquals(new Long(partId), autoBoxedLong);
	}

}
