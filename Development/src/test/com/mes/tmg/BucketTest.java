package com.mes.tmg;

import org.junit.Assert;
import org.junit.Test;

public class BucketTest {

	@Test
	@SuppressWarnings("deprecation")
	public void hashCodeMethod() {
		long buckId = 123L;
		Long autoBoxedLong = buckId;
		Assert.assertEquals(new Long(buckId).hashCode(), Long.valueOf(buckId).hashCode());
	}

}
