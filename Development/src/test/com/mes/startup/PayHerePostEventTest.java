package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

import com.google.code.tempusfugit.concurrency.annotations.Intermittent;

public class PayHerePostEventTest {

	@SuppressWarnings("deprecation")
	@Test(timeout = 5000)
	@Intermittent
	public void process() {
		Thread withoutStop = new OnePassThreadLoop();
		Thread withStop = new OnePassThreadLoop();

		withoutStop.start();
		withStop.start();
		Assert.assertTrue(Thread.activeCount() >= 2);
		withStop.stop();

		try {
			withoutStop.join();
		} catch (java.lang.InterruptedException ee) {
		}
		Assert.assertFalse("withoutStop Thread is dead", withoutStop.isAlive());

		try {
			withStop.join();
		} catch (java.lang.InterruptedException ee) {
		}
		Assert.assertFalse("withStop Thread is dead", withStop.isAlive());

	}

	class OnePassThreadLoop extends Thread {
		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(500l);
				} catch (InterruptedException e) {
					break;
				}
				try {
					Thread.sleep(500l);
				} catch (InterruptedException e) {
					break;
				}
				break;
			}
		}
	}

}
