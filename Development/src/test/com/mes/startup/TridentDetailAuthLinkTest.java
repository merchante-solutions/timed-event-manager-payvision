package com.mes.startup;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.startup.TridentDetailAuthLink.ApiAuthLookupEntry;
import com.mes.startup.TridentDetailAuthLink.AuthLookupEntry;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.internal.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.ResultSetIterImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TridentDetailAuthLink.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class, RuntimeRefErrors.class})
public class TridentDetailAuthLinkTest {

	private OracleResultSet oracleResultSet = null;
	private OracleResultSet oracleResultSet2 = null;
	private TridentDetailAuthLink tridentDetaiAuthLinkSpy = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Suppress method call and super class constructor objects */

		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));

		/* Mock static method calls */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		/* Mock required objects */
		OracleContext oracleContext = Mockito.mock(OracleContext.class);
		oracleResultSet = Mockito.mock(OracleResultSet.class);
		ResultSetIterator resultSetIterator = Mockito.spy(ResultSetIterator.class);
		ResultSetIterImpl resultSetIterImpl = Mockito.mock(ResultSetIterImpl.class);
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		ResultSetMetaData resultSetMetaData = Mockito.mock(ResultSetMetaData.class);

		OraclePreparedStatement oraclePreparedStatement = Mockito.mock(OraclePreparedStatement.class);
		tridentDetaiAuthLinkSpy = PowerMockito.spy(new TridentDetailAuthLink());
		ExecutionContext executionContext = Mockito.mock(ExecutionContext.class);

		Whitebox.setInternalState((SQLJConnectionBase) tridentDetaiAuthLinkSpy, "Ctx", defaultContext);

		PowerMockito.whenNew(ResultSetIterImpl.class).withAnyArguments().thenReturn(resultSetIterImpl);
		when(oracleContext.prepareOracleStatement(Mockito.any(DefaultContext.class), Mockito.any(String.class), Mockito.any(String.class))).thenReturn(oraclePreparedStatement);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(DefaultContext.class), Mockito.any(String.class), Mockito.any(String.class))).thenReturn(oraclePreparedStatement);
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
		when(resultSetIterator.getResultSet()).thenReturn(oracleResultSet);
		when(resultSetIterImpl.getResultSet()).thenReturn(oracleResultSet);
		when(oracleResultSet.getMetaData()).thenReturn(resultSetMetaData);
		when(resultSetMetaData.getColumnCount()).thenReturn(1);

	}
	
	@Test
	public void testLinkTridentAuthsForCR() throws Exception {

		when(tridentDetaiAuthLinkSpy.getEventArg(0)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(1)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(2)).thenReturn("true");
		when(tridentDetaiAuthLinkSpy.getEventArgCount()).thenReturn(3);
		oracleResultSet2 = Mockito.mock(OracleResultSet.class);

		when(oracleResultSet2.getString("auth_code")).thenReturn("TCV001");
		when(oracleResultSet2.getString("dci")).thenReturn("C");
		when(oracleResultSet2.getLong("batch_rec_id")).thenReturn(Long.valueOf(1343434));
		when(oracleResultSet2.getString("card_number")).thenReturn("4294221526813141");
		when(oracleResultSet2.getInt("detail_idx")).thenReturn(Integer.valueOf(333));
		when(oracleResultSet2.getLong("merchant_number")).thenReturn(Long.valueOf(941000922770l));
		when(oracleResultSet2.getDate("tran_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getString("tran_id")).thenReturn("a670f48921983c1d8cea2cf001c4e3ab");
		when(oracleResultSet2.getString("network_card_type")).thenReturn("VS");

		TridentDetailAuthLink.AuthLookupEntry authLookupEntry = new AuthLookupEntry(oracleResultSet2);

		@SuppressWarnings("unchecked")
		Vector<TridentDetailAuthLink.AuthLookupEntry> v = Mockito.spy(Vector.class);
		v.addElement(authLookupEntry);
		when(v.size()).thenReturn(1);
		when(v.elementAt(0)).thenReturn(authLookupEntry);
		PowerMockito.whenNew(Vector.class).withAnyArguments().thenReturn(v);

		tridentDetaiAuthLinkSpy.linkTridentAuths();
		PowerMockito.verifyPrivate(tridentDetaiAuthLinkSpy, times(1)).invoke("isStaticAuthCode", any(String.class));

	}

	@Test
	public void testLinkTridentAuthsForCV() throws Exception {

		when(tridentDetaiAuthLinkSpy.getEventArg(0)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(1)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(2)).thenReturn("true");
		when(tridentDetaiAuthLinkSpy.getEventArgCount()).thenReturn(3);
		oracleResultSet2 = Mockito.mock(OracleResultSet.class);

		when(oracleResultSet2.getString("auth_code")).thenReturn("TCR001");
		when(oracleResultSet2.getString("dci")).thenReturn("C");
		when(oracleResultSet2.getLong("batch_rec_id")).thenReturn(Long.valueOf(1343434));
		when(oracleResultSet2.getString("card_number")).thenReturn("4294221526813141");
		when(oracleResultSet2.getInt("detail_idx")).thenReturn(Integer.valueOf(333));
		when(oracleResultSet2.getLong("merchant_number")).thenReturn(Long.valueOf(941000922770l));
		when(oracleResultSet2.getDate("tran_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getString("tran_id")).thenReturn("a670f48921983c1d8cea2cf001c4e3ab");
		when(oracleResultSet2.getString("network_card_type")).thenReturn("VS");

		TridentDetailAuthLink.AuthLookupEntry authLookupEntry = new AuthLookupEntry(oracleResultSet2);

		@SuppressWarnings("unchecked")
		Vector<TridentDetailAuthLink.AuthLookupEntry> v = Mockito.spy(Vector.class);
		v.addElement(authLookupEntry);
		when(v.size()).thenReturn(1);
		when(v.elementAt(0)).thenReturn(authLookupEntry);
		PowerMockito.whenNew(Vector.class).withAnyArguments().thenReturn(v);

		tridentDetaiAuthLinkSpy.linkTridentAuths();
		PowerMockito.verifyPrivate(tridentDetaiAuthLinkSpy, times(1)).invoke("isStaticAuthCode", any(String.class));

	}
	
	@Test
	public void testLinkTridentApiAuthsForCR() throws Exception {

		when(tridentDetaiAuthLinkSpy.getEventArg(0)).thenReturn("2");
		when(tridentDetaiAuthLinkSpy.getEventArg(1)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(2)).thenReturn("true");
		when(tridentDetaiAuthLinkSpy.getEventArgCount()).thenReturn(3);

		oracleResultSet2 = Mockito.mock(OracleResultSet.class);
		when(oracleResultSet2.getString("auth_code")).thenReturn("TCR001");
		when(oracleResultSet2.getDate("begin_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getDate("end_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getInt("currency_code")).thenReturn(Integer.valueOf(1));
		when(oracleResultSet2.getString("dci")).thenReturn("C");
		when(oracleResultSet2.getLong("rec_id")).thenReturn(Long.valueOf(1343434));
		when(oracleResultSet2.getString("card_number")).thenReturn("4294221526813141");
		when(oracleResultSet2.getDouble("tran_amount")).thenReturn(Double.valueOf(333.11));
		when(oracleResultSet2.getLong("merchant_number")).thenReturn(Long.valueOf(941000922770l));
		when(oracleResultSet2.getString("trident_tran_id")).thenReturn("a670f48921983c1d8cea2cf001c4e3ab");
		when(oracleResultSet2.getString("network_card_type")).thenReturn("VS");

		TridentDetailAuthLink.ApiAuthLookupEntry authApiLookupEntry = new ApiAuthLookupEntry(oracleResultSet2);

		@SuppressWarnings("unchecked")
		Vector<TridentDetailAuthLink.ApiAuthLookupEntry> v = Mockito.spy(Vector.class);
		v.addElement(authApiLookupEntry);
		when(v.size()).thenReturn(1);
		when(v.elementAt(0)).thenReturn(authApiLookupEntry);
		PowerMockito.whenNew(Vector.class).withAnyArguments().thenReturn(v);

		tridentDetaiAuthLinkSpy.linkTridentApiAuths();
		PowerMockito.verifyPrivate(tridentDetaiAuthLinkSpy, times(1)).invoke("isStaticAuthCode", any(String.class));
	}

	@Test
	public void testLinkTridentApiAuthsForCV() throws Exception {

		when(tridentDetaiAuthLinkSpy.getEventArg(0)).thenReturn("2");
		when(tridentDetaiAuthLinkSpy.getEventArg(1)).thenReturn("1");
		when(tridentDetaiAuthLinkSpy.getEventArg(2)).thenReturn("true");
		when(tridentDetaiAuthLinkSpy.getEventArgCount()).thenReturn(3);

		oracleResultSet2 = Mockito.mock(OracleResultSet.class);
		when(oracleResultSet2.getString("auth_code")).thenReturn("TCV001");
		when(oracleResultSet2.getDate("begin_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getDate("end_date")).thenReturn(new java.sql.Date(System.currentTimeMillis()));
		when(oracleResultSet2.getInt("currency_code")).thenReturn(Integer.valueOf(1));
		when(oracleResultSet2.getString("dci")).thenReturn("C");
		when(oracleResultSet2.getLong("rec_id")).thenReturn(Long.valueOf(1343434));
		when(oracleResultSet2.getString("card_number")).thenReturn("4294221526813141");
		when(oracleResultSet2.getDouble("tran_amount")).thenReturn(Double.valueOf(333.11));
		when(oracleResultSet2.getLong("merchant_number")).thenReturn(Long.valueOf(941000922770l));
		when(oracleResultSet2.getString("trident_tran_id")).thenReturn("a670f48921983c1d8cea2cf001c4e3ab");
		when(oracleResultSet2.getString("network_card_type")).thenReturn("VS");

		TridentDetailAuthLink.ApiAuthLookupEntry authApiLookupEntry = new ApiAuthLookupEntry(oracleResultSet2);

		@SuppressWarnings("unchecked")
		Vector<TridentDetailAuthLink.ApiAuthLookupEntry> v = Mockito.spy(Vector.class);
		v.addElement(authApiLookupEntry);
		when(v.size()).thenReturn(1);
		when(v.elementAt(0)).thenReturn(authApiLookupEntry);
		PowerMockito.whenNew(Vector.class).withAnyArguments().thenReturn(v);

		tridentDetaiAuthLinkSpy.linkTridentApiAuths();
		PowerMockito.verifyPrivate(tridentDetaiAuthLinkSpy, times(1)).invoke("isStaticAuthCode", any(String.class));
	}

	@Test
	public void testIsStaticAuthCode() {
		Assert.assertTrue(tridentDetaiAuthLinkSpy.isStaticAuthCode("TCR001"));
		Assert.assertTrue(tridentDetaiAuthLinkSpy.isStaticAuthCode("TCV001"));
		Assert.assertFalse(tridentDetaiAuthLinkSpy.isStaticAuthCode("T50631"));
		Assert.assertFalse(tridentDetaiAuthLinkSpy.isStaticAuthCode(""));
		Assert.assertFalse(tridentDetaiAuthLinkSpy.isStaticAuthCode(null));
	}

}
