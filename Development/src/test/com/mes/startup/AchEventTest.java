package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Calendar;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;

import com.mes.EventBaseTest;
import com.mes.ach.AchEntryData;

@PrepareForTest({AchEvent.class})
public class AchEventTest extends EventBaseTest {
    private static final String fakeValue = "foo";
    
    @Mock
    public Connection con;
    @Rule
    private ExpectedException expectedException = ExpectedException.none();
    private AchEvent systemUnderTest;
    private Map<Long,String> ffConfig = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        systemUnderTest = new AchEvent();
        systemUnderTest.EventArgsVector = new Vector<String>();
        ffConfig.put(3943L, "MERCH DEP,AMEX DEP");
        ffConfig.put(3003L, "MERCH DEP|AMEX DEP");
        Whitebox.setInternalState(systemUnderTest, "Ctx", ctx);
        Whitebox.setInternalState(systemUnderTest, "fasterFundingConfig", ffConfig);
        mockDatabaseCalls();
    }
    
    @Test
    public void testParseLongArgForValidLong() throws Exception {
        // Given
        Long two = 2L;
        String twoString = two.toString();
        addSameValuedMultipleEventsArg(twoString, 3);

        // When
        Long result = Whitebox.invokeMethod(systemUnderTest, "parseLongArg", 2);

        // Then
        assertEquals("Parsed long argument", two, result);

    }

    @Test
    public void testParseLongArgForInValidLong() throws Exception {
        // Given
        addSameValuedMultipleEventsArg(fakeValue, 3);

        // When
        Long result = Whitebox.invokeMethod(systemUnderTest, "parseLongArg", 2);

        // Then
        assertNull("Invalid Long returns null", result);
    }

    @Test
    public void testParseStringArgForValidIndex() throws Exception {
        // Given
        addSameValuedMultipleEventsArg(fakeValue, 3);

        // When
        String result = Whitebox.invokeMethod(systemUnderTest, "parseStringArg", 2);

        // Then
        assertEquals("Fetching String argument passed for valid index", fakeValue, result);
    }

    @Test
    public void testParseStringArgForInValidIndex() throws Exception {
        // Given
        addSameValuedMultipleEventsArg(fakeValue, 3);

        // When
        String result = Whitebox.invokeMethod(systemUnderTest, "parseStringArg", -1);

        // Then
        assertNull("Invalid index returns null", result);
    }

    private void addSameValuedMultipleEventsArg(String value, int count) {
        for(int i = 0; i < count; i++) {
            systemUnderTest.EventArgsVector.add(value);
        }
    }

    @Test
    public void testGenerateTridentWhereClause_3943() throws Exception {
        boolean fasterFunding = true;
         Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);
        String actualResult1 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_MERCH_DEP, fasterFunding);
        String actualResult2 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_MERCH_DEP, !fasterFunding);
        String actualResult3 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_AMEX_DEP, fasterFunding);
        String actualResult4 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_AMEX_DEP, !fasterFunding);
        String actualResult5 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_CHARGEBACK, !fasterFunding);
        String actualResult6 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", AchEntryData.ED_MANUAL_ADJ, fasterFunding);

        // Then
        assertEquals("MERCH DEP SAME DAY", AchEvent.SAME_DAY_FUNDING_CLAUSE, actualResult1);
        assertEquals("MERCH DEP NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult2);
        assertEquals("AMEX DEP SAME DAY", AchEvent.SAME_DAY_FUNDING_CLAUSE, actualResult3);
        assertEquals("AMEX DEP NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult4);
        assertEquals("CHARGE BACK NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult5);
        assertEquals("MANUAL ADJ SAME DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult6);
    }

    @Test
    public void testGetMifWhereClause_3943() throws Exception {
        boolean fasterFunding = true;
        Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);
        String actualResult1 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_MERCH_DEP, fasterFunding);
        String actualResult2 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_MERCH_DEP, !fasterFunding);
        String actualResult3 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_AMEX_DEP, fasterFunding);
        String actualResult4 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_AMEX_DEP, !fasterFunding);
        String actualResult5 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_MANUAL_ADJ, fasterFunding);
        String actualResult6 = Whitebox.invokeMethod(systemUnderTest, "getMifWhereClause", AchEntryData.ED_CHARGEBACK, !fasterFunding);
        
        StringBuilder merchDepSameDayClause = new StringBuilder(Whitebox.getInternalState(systemUnderTest, "MERCH_DEP_SAME_DAY_MERCHANT_CLAUSE", AchEvent.class).toString());
        merchDepSameDayClause.append(")");
        StringBuilder merchDepNextDayClause = new StringBuilder(Whitebox.getInternalState(systemUnderTest, "MERCH_DEP_NEXT_DAY_MERCHANT_CLAUSE", AchEvent.class).toString());
        merchDepNextDayClause.append(")");
        
        // Then
        assertEquals("MERCH DEP SAME DAY", merchDepSameDayClause.toString(), actualResult1);
        assertEquals("MERCH DEP NEXT DAY", merchDepNextDayClause.toString(), actualResult2);
        assertEquals("AMEX DEP SAME DAY", Whitebox.getInternalState(systemUnderTest, "AMEX_DEP_SAME_DAY_MERCHANT_CLAUSE", AchEvent.class), actualResult3);
        assertEquals("AMEX DEP NEXT DAY", Whitebox.getInternalState(systemUnderTest, "AMEX_DEP_NEXT_DAY_MERCHANT_CLAUSE", AchEvent.class), actualResult4);
        assertEquals("AMEX DEP NEXT DAY", "", actualResult5);
        assertEquals("AMEX DEP NEXT DAY", "", actualResult6);
    }

    @Test
    public void testGenerateFundingClausesMERCHDEPSameDay_3943() throws Exception {
        // Given
        setUpEventsArg(AchEntryData.ED_MERCH_DEP, "Y");
         Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);
        
        // When
        Whitebox.invokeMethod(systemUnderTest, "generateFundingClauses");
        
        StringBuilder merchDepSameDayClause = new StringBuilder(Whitebox.getInternalState(systemUnderTest, "MERCH_DEP_SAME_DAY_MERCHANT_CLAUSE", AchEvent.class).toString());
        merchDepSameDayClause.append(")");

        // Then
        assertEquals("MERCH DEP SAME DAY", AchEvent.SAME_DAY_FUNDING_CLAUSE, Whitebox.getInternalState(systemUnderTest, "achTridentWhereClause", AchEvent.class));

        assertEquals("MERCH DEP SAME DAY", merchDepSameDayClause.toString(), Whitebox.getInternalState(systemUnderTest, "fundingWhereClause", AchEvent.class));
    }

    @Test
    public void testGenerateFundingClausesMERCHDEPNextDay_3943() throws Exception {
        // Given
        setUpEventsArg(AchEntryData.ED_MERCH_DEP, "N");
         Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);

        // When
        Whitebox.invokeMethod(systemUnderTest, "generateFundingClauses");
        
        StringBuilder merchDepNextDayClause = new StringBuilder(Whitebox.getInternalState(systemUnderTest, "MERCH_DEP_NEXT_DAY_MERCHANT_CLAUSE", AchEvent.class).toString());
        merchDepNextDayClause.append(")");

        // Then
        assertEquals("MERCH DEP NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, Whitebox.getInternalState(systemUnderTest, "achTridentWhereClause", AchEvent.class));
        
        assertEquals("MERCH DEP NEXT DAY", merchDepNextDayClause.toString(), Whitebox.getInternalState(systemUnderTest, "fundingWhereClause", AchEvent.class));

    }

    private void setUpEventsArg(String edMerchDep, String fasterFundingFlag) {
        String nextFundingFlag = "N";
        systemUnderTest.EventArgsVector.add(fakeValue);
        systemUnderTest.EventArgsVector.add("394300000");
        systemUnderTest.EventArgsVector.add(edMerchDep);
        addSameValuedMultipleEventsArg(fakeValue, 3);
        systemUnderTest.EventArgsVector.add(nextFundingFlag);
        systemUnderTest.EventArgsVector.add(fasterFundingFlag);
    }

    @Test
    public void testGenerateFundingClausesAMEXDEPSameDay_3943() throws Exception {
        // Given
        setUpEventsArg(AchEntryData.ED_AMEX_DEP, "Y");
         Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);

        // When
        Whitebox.invokeMethod(systemUnderTest, "generateFundingClauses");

        // Then
        assertEquals("AMEX DEP SAME DAY", AchEvent.SAME_DAY_FUNDING_CLAUSE, Whitebox.getInternalState(systemUnderTest, "achTridentWhereClause", AchEvent.class));

        final String actualResult = Whitebox.getInternalState(systemUnderTest, "fundingWhereClause");
        assertEquals("AMEX DEP SAME DAY", Whitebox.getInternalState(systemUnderTest, "AMEX_DEP_SAME_DAY_MERCHANT_CLAUSE", AchEvent.class), actualResult);
    }

    @Test
    public void testGenerateFundingClausesAMEXDEPNextDay_3943() throws Exception {
        // Given
        setUpEventsArg(AchEntryData.ED_AMEX_DEP, "N");
         Whitebox.setInternalState(systemUnderTest, "bankNumber", 3943);

        // When
        Whitebox.invokeMethod(systemUnderTest, "generateFundingClauses");

        // Then
        assertEquals("AMEX DEP NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, Whitebox.getInternalState(systemUnderTest, "achTridentWhereClause", AchEvent.class));

        final String actualResult = Whitebox.getInternalState(systemUnderTest, "fundingWhereClause");
        assertEquals("AMEX DEP NEXT DAY", Whitebox.getInternalState(systemUnderTest, "AMEX_DEP_NEXT_DAY_MERCHANT_CLAUSE", AchEvent.class), actualResult);
    }

    @Test
    public void testGenerateBankNumberForValidNumbers() throws Exception {
        // Given
        Integer fakeBankNumber = 3943;

        // When
        Integer actualResult1 = Whitebox.invokeMethod(systemUnderTest, "generateBankNumber", "39430000");
        Integer actualResult2 = Whitebox.invokeMethod(systemUnderTest, "generateBankNumber", "3943");

        // Then
        assertEquals("", fakeBankNumber, actualResult1);
        assertEquals("", fakeBankNumber, actualResult2);
    }

    @Test
    public void testGenerateBankNumberForInvalidNumbers() throws Exception {
        // Given
        expectedException.expect(Exception.class);

        // When
        Whitebox.invokeMethod(systemUnderTest, "generateBankNumber", "39");

        // Then
        expectedException.expectMessage("Can't determine bank number from: 39");
    }
    
    
    @Test
    @SuppressWarnings("deprecation")
    public void testExtractCieloStatementEntries() {
        // When
        
        // old way
        final int oldWayDay = Calendar.getInstance().getTime().getDay();
        
        // new way
        final int newWayDay = LocalDateTime.now().getDayOfWeek().getValue();
        
        // Then
        assertEquals("Pre- and Post- Java 8 upgrade values should be the same", oldWayDay, newWayDay);
    }

    @Test
    public void buildOffsetDetailRecord() {
        // Given
        long OffsetSponsoredAmount = System.currentTimeMillis();

        // When
        final double oldWay = (new Long(OffsetSponsoredAmount)).doubleValue() / 100;
        final double newWay = (double) OffsetSponsoredAmount / 100;

        // Then
        assertEquals(oldWay, newWay, Double.MIN_VALUE);
    }

    @Test
    public void extractTridentAchEntries() {
        // Given
        long recId = System.nanoTime();

        // When
        final Long oldWay = new Long(recId);

        // Then
        assertTrue(recId == oldWay);
        assertTrue(oldWay.equals(recId));
    }
    @Test
    public void testGenerateTridentWhereClause_3003() throws Exception {
        boolean fasterFunding = true;
        Whitebox.setInternalState(systemUnderTest, "bankNumber", 3003);
        String actualResult1 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", "MERCH DEP|AMEX DEP", fasterFunding);
        String actualResult2 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", "MERCH DEP|AMEX DEP", !fasterFunding);
        String actualResult3 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", "MERCH CHBK", !fasterFunding);
        String actualResult4 = Whitebox.invokeMethod(systemUnderTest, "generateTridentWhereClause", "MERCH ADJ", fasterFunding);

        // Then
        assertEquals("MERCH DEP SAME DAY", AchEvent.SAME_DAY_FUNDING_CLAUSE, actualResult1);
        assertEquals("MERCH DEP NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult2);
        assertEquals("CHARGE BACK NEXT DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult3);
        assertEquals("MANUAL ADJ SAME DAY", AchEvent.NEXT_DAY_FUNDING_CLAUSE, actualResult4);
    }
}
