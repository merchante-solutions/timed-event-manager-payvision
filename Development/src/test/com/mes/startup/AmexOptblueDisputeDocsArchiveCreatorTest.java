package com.mes.startup;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Calendar;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
@RunWith(PowerMockRunner.class)
@PrepareForTest({AmexOptblueDisputeDocsArchiveCreator.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class, MailMessage.class, RuntimeRefErrors.class, Connection.class})
public class AmexOptblueDisputeDocsArchiveCreatorTest {
    
    @Mock
    PreparedStatement pstmt;
    private AmexOptblueDisputeDocsArchiveCreator mockedAmOptblueDisputeDocsArchiveCreator = null;
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
      System.setProperty("log4j.configuration", "Development/log4j.properties");
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
      /* Suppress method call and super class constructor objects */
      PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
      PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
      PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
      PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
      PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
      PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
      PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
      PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));
      /* Mock static method calls */
      PowerMockito.mockStatic(MailMessage.class);
      PowerMockito.doNothing().when(MailMessage.class, "sendSystemErrorEmail", anyString(), anyString());
      PowerMockito.mockStatic(DbProperties.class);
      PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);
      /* Mock required objects */
      mockedAmOptblueDisputeDocsArchiveCreator = PowerMockito.spy(new AmexOptblueDisputeDocsArchiveCreator());
      Mockito.doReturn(true).when((SQLJConnectionBase) mockedAmOptblueDisputeDocsArchiveCreator).connect(true);
      DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
     
      Connection con = Mockito.mock(Connection.class);
      when(defaultContext.getConnection()).thenReturn(con);
      Whitebox.setInternalState((SQLJConnectionBase) mockedAmOptblueDisputeDocsArchiveCreator, "Ctx", defaultContext);
      Whitebox.setInternalState((SQLJConnectionBase) mockedAmOptblueDisputeDocsArchiveCreator, "con", con);
      when(con.prepareStatement(Mockito.anyString())).thenReturn(pstmt);
    }
    @Test
    public void testrecordTimeStampBegin() throws Exception {
        Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
        mockedAmOptblueDisputeDocsArchiveCreator.recordTimestampBegin(1L, beginTime);
        Mockito.verify(pstmt, Mockito.times(1)).executeUpdate();
    }
    @Test
    public void testrecordTimeStampEnd() throws Exception {
        Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
        mockedAmOptblueDisputeDocsArchiveCreator.recordTimestampEnd(1L, beginTime);
        Mockito.verify(pstmt, Mockito.times(1)).executeUpdate();
    }
}