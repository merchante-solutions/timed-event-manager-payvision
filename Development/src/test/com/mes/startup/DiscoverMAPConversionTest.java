package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class DiscoverMAPConversionTest {

    @Test
    public void convertBank() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";
        // Given
        final long merchantNumber = System.nanoTime();

        // When

        // old way
        final Long oldWayLongObject = new Long(merchantNumber);

        // Then
        Assert.assertTrue(message, merchantNumber == oldWayLongObject);
        Assert.assertTrue(message, oldWayLongObject.equals(merchantNumber));
    }
}
