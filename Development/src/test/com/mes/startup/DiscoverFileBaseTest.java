package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.InterchangeBase;
import com.mes.settlement.InterchangeDiscover;
import com.mes.settlement.InterchangeMasterCard;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordDiscover;
import com.mes.settlement.SettlementRecordMC;import com.mes.settlement.VisakBatchData;

import masthead.mastercard.clearingiso.IpmOfflineFileWriter;
import sqlj.runtime.ResultSetIterator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DiscoverFileBase.class, DbProperties.class, SettlementDb.class})
public class DiscoverFileBaseTest {

	DiscoverFileBase mockDiscoverFileBase;

	@Mock
	ResultSetIterator resultSetIterator;

	@Mock
	ResultSet resultSet;

	@Mock
	InterchangeDiscover interchangeDiscover;

	SettlementDb settlementDb;
	SettlementRecordDiscover settlementRecordDS = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		VisakBatchData        batchData           = new VisakBatchData();
		
		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		settlementDb = Mockito.mock(SettlementDb.class);
		PowerMockito.whenNew(SettlementDb.class).withAnyArguments().thenReturn(settlementDb);
		PowerMockito.whenNew(InterchangeDiscover.class).withAnyArguments().thenReturn(interchangeDiscover);
		Mockito.doReturn(true).when(settlementDb).connect(true);

		Mockito.doReturn(resultSet).when(resultSetIterator).getResultSet();

		settlementRecordDS = Mockito.spy(SettlementRecordDiscover.class);
		PowerMockito.whenNew(SettlementRecordDiscover.class).withNoArguments().thenReturn(settlementRecordDS);

		PowerMockito.mockStatic(SettlementDb.class);
		PowerMockito.when(SettlementDb.loadIrdListMC(settlementRecordDS)).thenReturn(new ArrayList());

		PowerMockito.doNothing().when(SettlementDb.class, "updateIcCatDowngrade", settlementRecordDS);

		mockDiscoverFileBase = PowerMockito.mock(DiscoverFileBase.class, Mockito.CALLS_REAL_METHODS);
		Whitebox.setInternalState(mockDiscoverFileBase, "SettlementRecMT", SettlementRecord.MT_FIRST_PRESENTMENT);
	    PowerMockito.doNothing().when(mockDiscoverFileBase, "buildFileHeader", Mockito.any());
		PowerMockito.doNothing().when(mockDiscoverFileBase, "buildDetailRecords", Mockito.any(IpmOfflineFileWriter.class), Mockito.any(SettlementRecordMC.class), Mockito.any(Boolean.class));
		Mockito.doNothing().when(mockDiscoverFileBase).buildFileTrailer(any());
		PowerMockito.doNothing().when(mockDiscoverFileBase, "closeFile", Mockito.any());
		Mockito.doNothing().when(mockDiscoverFileBase).sendFile(anyString());

		HashMap EmvProperties = new HashMap();
		EmvProperties.put("visa", "");
		EmvProperties.put("mastercard", "");
		EmvProperties.put("amex", "");
		EmvProperties.put("discover", "");

		Whitebox.setInternalState(mockDiscoverFileBase, "EmvProperties", EmvProperties);

		Mockito.doNothing().when(settlementRecordDS).setFields(resultSet, false, false);
		
		Mockito.doNothing().when(settlementRecordDS).setFieldsBase(batchData,resultSet,0);

		Mockito.doReturn("6011000990139424").when(settlementRecordDS).getCardNumberFull();
	}

	private void setupInternalRejects(String icCode) throws Exception {
		when(resultSetIterator.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(resultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(settlementRecordDS.getString("ic_cat")).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount == 0)
					return "000";
				return icCode;
			}
		});

	}

	//@Test
	public void testHandleSelectedRecords_internRejectsTrue() throws Exception {

		setupInternalRejects("641");
		Whitebox.setInternalState(mockDiscoverFileBase, "ActionCode", "X");
		Mockito.doReturn("N").when(settlementRecordDS).getData("external_reject");
		String workFileName = "ds_settle3941_091619_001.ipm";
		Long workFileid = 89639622l;
		mockDiscoverFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);

		assertEquals(workFileName, settlementRecordDS.getData("load_filename"));
		assertEquals(workFileid, Long.valueOf(settlementRecordDS.getData("load_file_id")));
		assertEquals("Y", settlementRecordDS.getData("ach_flag"));
		assertEquals("N", settlementRecordDS.getData("external_reject"));
		assertEquals("641", settlementRecordDS.getData("ic_cat_downgrade"));
		Mockito.verify(settlementDb, Mockito.times(1)).updateDTRecord(settlementRecordDS);
	}

	//@Test
	public void testHandleSelectedRecords_noReject() throws Exception {

		setupInternalRejects("");
		Mockito.doReturn(null).when(settlementRecordDS).getData("external_reject");
		Whitebox.setInternalState(mockDiscoverFileBase, "ActionCode", null);

		String workFileName = "ds_settle3941_091619_002.ipm";
		long workFileid = 89639623l;

		mockDiscoverFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);
		assertEquals("", settlementRecordDS.getData("load_filename"));
		assertEquals("", settlementRecordDS.getData("load_file_id"));
		assertEquals("", settlementRecordDS.getData("ach_flag"));
		assertEquals(null, settlementRecordDS.getData("external_reject"));
		assertEquals("", settlementRecordDS.getData("ic_cat_downgrade"));
		Mockito.verify(settlementDb, Mockito.times(0)).updateDTRecord(settlementRecordDS);
	}

}
