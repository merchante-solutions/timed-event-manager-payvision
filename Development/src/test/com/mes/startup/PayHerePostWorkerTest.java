package com.mes.startup;

import com.mes.EventBaseTest;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import sqlj.runtime.ref.DefaultContext;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PayHerePostWorkerTest extends EventBaseTest {

    private final long fakeRecId = 12345L;
    @Mock
    private PayHerePostEvent.PayHerePostWorker systemUnderTest;
    @Mock
    private sqlj.runtime.ref.ResultSetIterImpl resultSetIterImpl;
    @Mock
    private Appender appender;
    @Captor
    private ArgumentCaptor argumentCaptor;

    @BeforeClass
    public static void setUpBeforeClass() {
        System.setProperty("log4j.configuration", "Development/log4j.properties");
        System.setProperty("db.configuration", "Development/db.properties");
    }

    @SuppressWarnings({"unchecked"})
    private static void updateEnv(String name, String val) throws ReflectiveOperationException {
        Map<String, String> environment = System.getenv();
        Field field = environment.getClass().getDeclaredField("m");
        field.setAccessible(true);
        ((Map<String, String>) field.get(environment)).put(name, val);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(systemUnderTest, "RecId", fakeRecId);
        Whitebox.setInternalState(systemUnderTest, "Ctx", ctx);
        Logger.getRootLogger().addAppender(appender);
        mockDatabaseCalls();
    }

    @After
    public void tearDown() {
        Logger.getRootLogger().removeAppender(appender);
    }

    @Test
    public void run() throws Exception {
        // Given
        preRunSetup();
        List<String> keys = Arrays.asList("response: null", "retrieving post data", "posting data", "connecting to: http://127.0.0.1:7890");

        // When
        systemUnderTest.run();

        // Then
        verify(appender, atLeast(5)).doAppend((LoggingEvent) argumentCaptor.capture());
        List<LoggingEvent> loggingEvents = argumentCaptor.getAllValues();
        final Map<String, Level> map = loggingEvents.stream().collect(Collectors.toMap(LoggingEvent::getRenderedMessage, LoggingEvent::getLevel));
        final List<Level> levels = map.values().stream().distinct().collect(Collectors.toList());

        assertThat(map.keySet(), hasItems(keys.toArray(new String[0])));
        assertTrue("Should contain DEBUG level", levels.contains(Level.DEBUG));
    }

    private void preRunSetup() throws Exception {
        when(resultSetIterator.getResultSet()).thenReturn(oracleResultSet);
        when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
        when(oracleContext.prepareOracleBatchableStatement(any(), anyString(), anyString())).thenReturn(oraclePreparedStatement);
        when(oracleContext.prepareOracleStatement(any(DefaultContext.class), anyString(), anyString())).thenReturn(oraclePreparedStatement);
        when(oracleResultSet.next()).thenReturn(true).thenReturn(false);
        when(oracleResultSet.getString("post_data")).thenReturn("foo");
        when(oracleResultSet.getString("resp_uidpwd")).thenReturn("bar");
        when(oracleResultSet.getLong("ph_rec_id")).thenReturn(fakeRecId);
        when(oracleResultSet.getString("resp_url")).thenReturn("http://127.0.0.1:7890");
        when(oraclePreparedStatement.getConnection()).thenReturn(connection);
        when(resultSetIterImpl.getResultSet()).thenReturn(oracleResultSet);
        doNothing().when(oracleResultSet).close();
        doNothing().when(oracleContext).oracleCloseQuery();
        doNothing().when(oracleContext).oracleExecuteBatchableUpdate();
        doNothing().when(resultSetIterImpl).close();
        doNothing().when(systemUnderTest).cleanUp();
        doCallRealMethod().when(systemUnderTest).run();
        updateEnv("te.configuration", "Development/te.properties");
    }

}
