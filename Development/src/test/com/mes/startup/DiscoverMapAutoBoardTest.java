package com.mes.startup;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.when;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.error.RuntimeRefErrors;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DiscoverMapAutoBoard.class, DbProperties.class, TsysFileBase.class, MailMessage.class, SQLJConnectionBase.class})
public class DiscoverMapAutoBoardTest {

	private DiscoverMapAutoBoard autoBoardSpy = null;
	private ResultSet mockedResultSet = null;
	private BufferedReader readerMock = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		/* Suppress method call and super class constructor objects */

		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));

		/* Mock static method calls */
		PowerMockito.mockStatic(MailMessage.class);
		PowerMockito.doNothing().when(MailMessage.class, "sendSystemErrorEmail", anyString(), anyString());
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		/* Mock required objects */
		autoBoardSpy = PowerMockito.spy(new DiscoverMapAutoBoard());
		mockedResultSet = Mockito.mock(ResultSet.class);
		readerMock = PowerMockito.mock(BufferedReader.class);

		// when called new BufferedReader(Reader) return mock
		Constructor<BufferedReader> constructor = Whitebox.getConstructor(BufferedReader.class, Reader.class);
		PowerMockito.whenNew(constructor)//
				.withArguments(Mockito.any(Reader.class))//
				.thenReturn(readerMock);

		// when called new FileReader(String) return null
		Constructor<FileReader> constructorFileReader = Whitebox.getConstructor(FileReader.class, String.class);
		PowerMockito.whenNew(constructorFileReader)//
				.withArguments(Matchers.any(String.class))//
				.thenReturn(null);

		Mockito.doReturn(true).when((SQLJConnectionBase) autoBoardSpy).connect(true);
		Mockito.doNothing().when((EventBase) autoBoardSpy).logEntry(anyString(), anyString());
		Mockito.doNothing().when((SQLJConnectionBase) autoBoardSpy).cleanUp();

		Connection mockCon = Mockito.mock(Connection.class);
		PreparedStatement mockPs = Mockito.mock(PreparedStatement.class);
		Whitebox.setInternalState((SQLJConnectionBase) autoBoardSpy, "con", mockCon);
		when(mockCon.prepareStatement(anyString())).thenReturn(mockPs);
		when(mockPs.executeQuery()).thenReturn(mockedResultSet);
		when(mockPs.executeBatch()).thenReturn(new int[1]);

	}

	private void setupStaticMock() throws Exception {
		when(mockedResultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount == 0 || noCount < 10)
					return true;
				return false;
			}
		});

		when(mockedResultSet.getLong(anyString())).thenAnswer(new Answer<Object>() {
			private int count = 0;
			private int returnValue = 0;

			public Object answer(InvocationOnMock invocation) {
				returnValue++;
				int noCount = count++;
				if (noCount == 0 || noCount < 10)
					return returnValue;
				return 0;
			}
		});

		when(mockedResultSet.getString(anyString())).thenAnswer(new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) {
				return "testing";
			}
		});
	}

	@Test
	public void testGetReviewedDataValidRecordTypesWithError() throws Exception {
		setupStaticMock();
		PowerMockito.when(readerMock.readLine()).thenReturn("00~00000610069~03182~2019-01-18-01.00.05.616392~                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ")
												.thenReturn("11~00000650494~00000000000~601172000463678~XACTLY CORPORATION~001~2003~ ~                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ")
												.thenReturn("10~00000650495~00000000000~601172000788587~KEATING LANDSCAPE & TREE~001~2039~ ~                                                                                                                                                                                                                                                                                                                                                                                                                                                            ")
												.thenReturn(null);

		autoBoardSpy.getReviewedData("test.dat");
		PowerMockito.verifyPrivate(autoBoardSpy,times(2)).invoke("addToListMerchantsInError",any(String.class), any(StringBuilder.class), any(long.class), any(long.class), any(Map.class));
		PowerMockito.verifyPrivate(autoBoardSpy,times(1)).invoke("updateDABErrorStatus",any());
	}
	
	@Test
	public void testGetReviewedDataValidRecordTypesWithErrorWithCMNF() throws Exception {
		setupStaticMock();
		PowerMockito.when(readerMock.readLine()).thenReturn("00~00000610069~03182~2019-01-18-01.00.05.616392~                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ")
												.thenReturn("21~00000650495~00000000000~601172000766336~RETAIL-1402~00000000000~POPLAR SPRINGS INN~ ~9245 ROGUES RD~ ~CASANOVA~VA~20139~USA~7884600~7011~2014-02-28~2014-03-06~RICHARD~THOMPSON~XXXXX9459~7330 LOCUST RUN DR~ ~MARSHALL~VA~20115~USA~2009~                                                                                                                                                                                                                                                                                              ")
												.thenReturn(null);

		autoBoardSpy.getReviewedData("test.dat");
		PowerMockito.verifyPrivate(autoBoardSpy,times(0)).invoke("addToListMerchantsInError",any(String.class), any(StringBuilder.class), any(long.class), any(long.class), any(Map.class));
		PowerMockito.verifyPrivate(autoBoardSpy,times(0)).invoke("updateDABErrorStatus",any());
	}

	@Test
	public void testGetReviewedDataInValidRecordTypes() throws Exception {
		setupStaticMock();
		PowerMockito.when(readerMock.readLine()).thenReturn("00~00000610069~03182~2019-01-18-01.00.05.616392~                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ")
												.thenReturn("05~00000650436~00000000000~601172000869981~LAPEYRE STAIR, INC~ ~         ~                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ")
												.thenReturn("06~00000650436~00000000000~601172000642222~OB BILLFISH CLASSIC~ ~         ~                                                                                                                                                                                                                                                                                                                                                                                                                                                                ")
												.thenReturn(null);

		autoBoardSpy.getReviewedData("test.dat");
		PowerMockito.verifyPrivate(autoBoardSpy,times(0)).invoke("addToListMerchantsInError",any(String.class), any(StringBuilder.class), any(long.class), any(long.class), any(Map.class));
		PowerMockito.verifyPrivate(autoBoardSpy,times(0)).invoke("updateDABErrorStatus",any());
	}

}
