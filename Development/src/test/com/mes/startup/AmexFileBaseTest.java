package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.io.BufferedReader;
import java.sql.ResultSet;
import java.util.HashMap;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.InterchangeBase;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordAmex;
import masthead.mastercard.clearingiso.IpmOfflineFileWriter;
import sqlj.runtime.ResultSetIterator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AmexFileBase.class, DbProperties.class, SettlementDb.class})
public class AmexFileBaseTest {

	AmexFileBase mockAmexFileBase;

	@Mock
	ResultSetIterator resultSetIterator;

	@Mock
	ResultSet resultSet;

	SettlementDb settlementDb;
	SettlementRecordAmex settlementRecordAmex = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		settlementDb = Mockito.mock(SettlementDb.class);
		PowerMockito.whenNew(SettlementDb.class).withAnyArguments().thenReturn(settlementDb);
		Mockito.doReturn(true).when(settlementDb).connect(true);

		Mockito.doReturn(resultSet).when(resultSetIterator).getResultSet();

		settlementRecordAmex = Mockito.spy(SettlementRecordAmex.class);
		PowerMockito.whenNew(SettlementRecordAmex.class).withNoArguments().thenReturn(settlementRecordAmex);

		mockAmexFileBase = PowerMockito.mock(AmexFileBase.class, Mockito.CALLS_REAL_METHODS);
		Whitebox.setInternalState(mockAmexFileBase, "SettlementRecMT", SettlementRecord.MT_FIRST_PRESENTMENT);
		PowerMockito.doNothing().when(mockAmexFileBase, "buildFileHeader", Mockito.any(BufferedReader.class));
		PowerMockito.doNothing().when(mockAmexFileBase, "buildDetailRecords", Mockito.any(IpmOfflineFileWriter.class), Mockito.any(SettlementRecordAmex.class), Mockito.any(Boolean.class));
		PowerMockito.doNothing().when(mockAmexFileBase, "buildObFileHeader", Mockito.any(BufferedReader.class));

		Mockito.doNothing().when(mockAmexFileBase).buildFileTrailer(any());
		Mockito.doNothing().when(mockAmexFileBase).sendFile(anyString());

		HashMap EmvProperties = new HashMap();
		EmvProperties.put("visa", "");
		EmvProperties.put("mastercard", "");
		EmvProperties.put("amex", "");
		EmvProperties.put("discover", "");

		Whitebox.setInternalState(mockAmexFileBase, "EmvProperties", EmvProperties);

		Mockito.doNothing().when(settlementRecordAmex).setFields(resultSet, false, false);

		Mockito.doReturn("345607014110874").when(settlementRecordAmex).getCardNumberFull();

		when(resultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});
	}

	@Test
	public void testHandleSelectedRecords_internRejectsTrue() throws Exception {

		Whitebox.setInternalState(mockAmexFileBase, "ActionCode", "X");
		Mockito.doReturn("N").when(settlementRecordAmex).getData("external_reject");
		String workFileName = "am_settle3941_100719_001.dat";
		long workFileid = 89639622l;
		mockAmexFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);

		assertEquals(workFileName, settlementRecordAmex.getData("load_filename"));
		assertEquals(workFileid, Long.parseLong(settlementRecordAmex.getData("load_file_id")));
		assertEquals("Y", settlementRecordAmex.getData("ach_flag"));
		assertEquals("N", settlementRecordAmex.getData("external_reject"));
		Mockito.verify(settlementDb, Mockito.times(1)).updateDTRecord(settlementRecordAmex);
	}

	@Test
	public void testHandleSelectedRecords_internalRejectsFalse() throws Exception {

		Mockito.doReturn(null).when(settlementRecordAmex).getData("external_reject");
		Whitebox.setInternalState(mockAmexFileBase, "ActionCode", null);

		String workFileName = "am_settle3941_100719_002.dat";
		long workFileid = 89639623l;

		mockAmexFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);
		assertEquals("", settlementRecordAmex.getData("load_filename"));
		assertEquals("", settlementRecordAmex.getData("load_file_id"));
		assertEquals("", settlementRecordAmex.getData("ach_flag"));
		assertEquals(null, settlementRecordAmex.getData("external_reject"));
		Mockito.verify(settlementDb, Mockito.times(0)).updateDTRecord(settlementRecordAmex);
	}

	@Test
	public void testHandleSelectedOBRecords_internRejectsTrue() throws Exception {

		Whitebox.setInternalState(mockAmexFileBase, "ActionCode", "X");
		Mockito.doReturn("N").when(settlementRecordAmex).getData("external_reject");
		String workFileName = "am_ob_settle3943_100719_001.dat";
		long workFileid = 89639622l;
		mockAmexFileBase.handleSelectedOBRecords(resultSet, workFileName, workFileid);

		assertEquals(workFileName, settlementRecordAmex.getData("load_filename"));
		assertEquals(workFileid, Long.parseLong(settlementRecordAmex.getData("load_file_id")));
		assertEquals("Y", settlementRecordAmex.getData("ach_flag"));
		assertEquals("N", settlementRecordAmex.getData("external_reject"));
		Mockito.verify(settlementDb, Mockito.times(1)).updateDTRecord(settlementRecordAmex);
	}

	@Test
	public void testHandleSelectedOBRecords_internalRejectsFalse() throws Exception {

		Mockito.doReturn(null).when(settlementRecordAmex).getData("external_reject");
		Whitebox.setInternalState(mockAmexFileBase, "ActionCode", null);

		String workFileName = "am_ob_settle3943_100719_002.dat";
		long workFileid = 89639624l;

		mockAmexFileBase.handleSelectedOBRecords(resultSet, workFileName, workFileid);
		assertEquals("", settlementRecordAmex.getData("load_filename"));
		assertEquals("", settlementRecordAmex.getData("load_file_id"));
		assertEquals("", settlementRecordAmex.getData("ach_flag"));
		assertEquals(null, settlementRecordAmex.getData("external_reject"));
		Mockito.verify(settlementDb, Mockito.times(0)).updateDTRecord(settlementRecordAmex);
	}

}
