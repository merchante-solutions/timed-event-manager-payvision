package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class AppStatusSummaryTest {

    @Test
    public void execute() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";

        // Given
        long appSeqNum = System.nanoTime();

        // When

        // old way
        final Long oldWayLongObject = new Long(appSeqNum);

        // Then
        Assert.assertTrue(message, oldWayLongObject.equals(appSeqNum));
        Assert.assertTrue(message, appSeqNum == oldWayLongObject);
    }
}
