package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.ResultSetMetaData;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueTools;
import com.mes.startup.RejectQueueProcessor._Item;
import com.mes.user.UserBean;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.internal.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.ResultSetIterImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RejectQueueProcessor.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class, RuntimeRefErrors.class, QueueTools.class})
public class RejectQueueProcessorTest {

	private OracleResultSet oracleResultSet = null;
	RejectQueueProcessor rejectQueueProcessor = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {
		/* Suppress method call and super class constructor objects */

		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));

		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		PowerMockito.mockStatic(QueueTools.class);
		PowerMockito.doNothing().when(QueueTools.class, "moveQueueItem", anyLong(), anyInt(), anyInt(), any(UserBean.class), anyString());

		/* Mock required objects */

		rejectQueueProcessor = PowerMockito.spy(new RejectQueueProcessor());
		Mockito.doReturn(true).when((SQLJConnectionBase) rejectQueueProcessor).connect(true);
		ExecutionContext executionContext = Mockito.mock(ExecutionContext.class);

		ResultSetMetaData resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
		oracleResultSet = Mockito.mock(OracleResultSet.class);
		ResultSetIterator resultSetIterator = Mockito.spy(ResultSetIterator.class);
		ResultSetIterImpl resultSetIterImpl = Mockito.mock(ResultSetIterImpl.class);
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		OracleContext oracleContext = Mockito.mock(OracleContext.class);
		OraclePreparedStatement oraclePreparedStatement = Mockito.mock(OraclePreparedStatement.class);

		Whitebox.setInternalState((SQLJConnectionBase) rejectQueueProcessor, "Ctx", defaultContext);

		PowerMockito.whenNew(ResultSetIterImpl.class).withAnyArguments().thenReturn(resultSetIterImpl);

		when(oracleContext.prepareOracleStatement(Mockito.any(DefaultContext.class), Mockito.any(String.class), Mockito.any(String.class))).thenReturn(oraclePreparedStatement);
		when(oracleContext.prepareOracleBatchableStatement(Mockito.any(DefaultContext.class), Mockito.any(String.class), Mockito.any(String.class))).thenReturn(oraclePreparedStatement);

		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);

		when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
		when(resultSetIterator.getResultSet()).thenReturn(oracleResultSet);
		when(resultSetIterImpl.getResultSet()).thenReturn(oracleResultSet);

		when(oracleResultSet.getMetaData()).thenReturn(resultSetMetaData);

		when(oracleResultSet.getLong("id")).thenReturn(1l);
		when(oracleResultSet.getString("last_user")).thenReturn("system");
		when(oracleResultSet.getString("external_reject")).thenReturn("N");

		when(resultSetMetaData.getColumnCount()).thenReturn(1);
		when(oracleResultSet.wasNull()).thenReturn(false);
	}

	private void setupInternalReject() throws Exception {

		when(oracleResultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 2)
					return true;
				return false;
			}
		});

	}

	@Test
	public void testExecute_MCInternalRejectTrue() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("MC");
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertEquals("N", ((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_MCInternalRejectFalse() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("MC");
		when(oracleResultSet.getString("external_reject")).thenReturn(null);
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertNull(((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_VisaInternalRejectTrue() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("VS");
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertEquals("N", ((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_VisaInternalRejectFalse() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("VS");
		when(oracleResultSet.getString("external_reject")).thenReturn(null);
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertNull(((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_AmexInternalRejectTrue() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("AM");
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertEquals("N", ((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_AmexInternalRejectFalse() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("AM");
		when(oracleResultSet.getString("external_reject")).thenReturn(null);
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertNull(((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_DiscoverInternalRejectTrue() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("DS");
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertEquals("N", ((_Item) clearList.get(0)).externalReject);
	}

	@Test
	public void testExecute_DiscoverInternalRejectFalse() throws Exception {
		setupInternalReject();
		when(oracleResultSet.getString("card_type")).thenReturn("DS");
		when(oracleResultSet.getString("external_reject")).thenReturn(null);
		assertTrue(rejectQueueProcessor.execute());
		List clearList = (List) Whitebox.getInternalState(rejectQueueProcessor, "clearList");
		assertNull(((_Item) clearList.get(0)).externalReject);
	}

}
