package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.database.MesQueryHandlerList;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DccRateUtil;
import com.mes.tools.DccRateUtil.DccRate;
import static org.junit.Assert.assertFalse;

public class DailyDetailFileProcessEventTest {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = null;
	DccRateUtil rateUtilMock = null;
	MesQueryHandlerResultSet queryHandler = null;
	String preparedSql = null;
	@Captor
	ArgumentCaptor<Object[]> objCaptor;

	@Mock
	MesResultSet resultSetMock;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	public void setUpMockData() throws ParseException, SQLException {
		dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		objCaptor.getAllValues().clear();
		queryHandler = Mockito.spy(MesQueryHandlerResultSet.class);
		rateUtilMock = Mockito.mock(DccRateUtil.class);
		doReturn(queryHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();

	}
	
	

	@Test
	public void testGetTransactions() throws Exception {

		setUpMockData();
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		mockResultSet.add(new HashMap<String, Object>());
		mockResultSet.get(0).put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(0).put("batch_number", 61777);
		mockResultSet.get(0).put("fx_markup", BigDecimal.valueOf(1.5d));
		mockResultSet.get(0).put("ddf_dt_id", BigDecimal.valueOf(123));
		mockResultSet.get(0).put("base_currency_code", "036");
		mockResultSet.get(0).put("counter_currency_code", "840");
		mockResultSet.get(0).put("card_type", "MC");
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		doReturn(mockHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("13com.mes.startup.DailyDetailFileProcessEvent"), Mockito.any(), objCaptor.capture());

		Whitebox.invokeMethod(dailyDetailFileProcessEventMock, "getTransactions", aFileRecMock);

		assertNotNull(objCaptor.getAllValues());
		assertEquals(2, objCaptor.getAllValues().size());
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		if (mockResultSet.next()) {
			assertEquals(mockResultSet.getInt("batch_number"), 61777);
			assertEquals(BigDecimal.valueOf(mockResultSet.getDouble("fx_markup")), BigDecimal.valueOf(1.5d));
			assertEquals(mockResultSet.getString("counter_currency_code"), "840");
			assertEquals(mockResultSet.getString("base_currency_code"), "036");
			assertEquals(BigDecimal.valueOf(mockResultSet.getLong("ddf_dt_id")), BigDecimal.valueOf(123));
			assertEquals(mockResultSet.getString("card_type"), "MC");
			assertEquals(0, mockResultSet.getDate("batch_date").compareTo(new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime())));
			assertEquals(0, mockResultSet.getDate("fx_conversion_date").compareTo(new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime())));
		}

	}

	@Test
	public void testGetExchangeRate() throws Exception {
		setUpMockData();
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		MesQueryHandlerList localQHMock = Mockito.spy(MesQueryHandlerList.class);
		DccRateUtil utilMock = Mockito.mock(DccRateUtil.class);
		DccRate rateMock = Mockito.spy(DccRate.class);

		DccRate dccrateMock = new DccRate();
		dccrateMock.buyRate = 0.009136789813d;
		dccrateMock.sellRate = 0.009132222518d;

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		Map<String, Object> testMap = setUpExchangeRateDate();

		doReturn(localQHMock).when(rateUtilMock).getQueryHandler();
		doReturn(0).when(rateUtilMock).getEffectiveDateCount();
		doReturn(rateMock).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap);
		doReturn(dccrateMock).when(utilMock).getDccRate(Mockito.anyString(), Mockito.anyString(), Mockito.any());
		
		
		ArgumentCaptor<Object[]> objDatesCaptor = ArgumentCaptor.forClass(Object[].class);
		objDatesCaptor.getAllValues().clear();
		Whitebox.invokeMethod(dailyDetailFileProcessEventMock, "getExchangeRate", aFileRecMock, testMap);
		DccRateUtil.DccRate rate = utilMock.getDccRate("MC", "008", "840");
		
		
		assertNotNull(rate);
		assertEquals(BigDecimal.valueOf(0.009136789813d), BigDecimal.valueOf(rate.getBuyRate()));
		assertEquals(BigDecimal.valueOf(0.009132222518d), BigDecimal.valueOf(rate.getSellRate()));
		assertNotNull(rate);
	}

	
	
	@Test
	public void testUpdateMerchantClearing() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		DccRate fxRateMock = Mockito.spy(DccRate.class);
		MesQueryHandlerResultSet localQHMock = Mockito.mock(MesQueryHandlerResultSet.class);
		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		
		
		doReturn(0.009136789813d).when(fxRateMock).getBuyRate();
		doReturn(0.009132222518d).when(fxRateMock).getSellRate();
		doReturn(localQHMock).when(dailyDetailFileProcessEventMock).getQueryHandler();
		doReturn(1).when(localQHMock).executePreparedUpdate(Mockito.eq("14com.mes.startup.DailyDetailFileProcessEvent"), Mockito.any(), objArrayCaptor.capture());

		dailyDetailFileProcessEventMock.updateMerchantClearing(localQHMock, testMap, fxRateMock);
		
		assertEquals(16, objArrayCaptor.getAllValues().size());
		List argumentList = objArrayCaptor.getAllValues();
		assertEquals(Double.valueOf(0.009136789813), argumentList.get(0));
		assertEquals(Double.valueOf(0.009132222518), argumentList.get(1));
		assertEquals(BigDecimal.valueOf(1.5), argumentList.get(2));

	}
	
	

	@Test
	public void testUpdatePresentment() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		DccRate fxRateMock = Mockito.spy(DccRate.class);
		MesQueryHandlerResultSet localQHMock = Mockito.mock(MesQueryHandlerResultSet.class);
		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		
		
		doReturn(0.009136789813d).when(fxRateMock).getBuyRate();
		doReturn(0.009132222518d).when(fxRateMock).getSellRate();
		doReturn(localQHMock).when(dailyDetailFileProcessEventMock).getQueryHandler();
		doReturn(1).when(localQHMock).executePreparedUpdate(Mockito.eq("15com.mes.startup.DailyDetailFileProcessEvent"), Mockito.any(), objArrayCaptor.capture());

		dailyDetailFileProcessEventMock.updatePresentment(localQHMock, testMap, fxRateMock);
		
		assertEquals(6, objArrayCaptor.getAllValues().size());
		List argumentList = objArrayCaptor.getAllValues();
		assertEquals(argumentList.get(0), "036");
		assertEquals(Double.valueOf(0.009136789813), argumentList.get(1));
		assertEquals(Double.valueOf(0.009132222518), argumentList.get(2));
		assertEquals(61777, argumentList.get(3));
		assertEquals(BigDecimal.valueOf(123), argumentList.get(4));
	}
	
	

	
	
	

	@Test
	public void testCalcDccFundingAmounts() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		mockResultSet.add(new HashMap<String, Object>());
		mockResultSet.get(0).put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(0).put("batch_number", 61777);
		mockResultSet.get(0).put("fx_markup", BigDecimal.valueOf(1.5d));
		mockResultSet.get(0).put("ddf_dt_id", BigDecimal.valueOf(123));
		mockResultSet.get(0).put("base_currency_code", "036");
		mockResultSet.get(0).put("counter_currency_code", "840");
		mockResultSet.get(0).put("card_type", "MC");
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		
		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		
		DccRate dccrateMock = new DccRate();
		dccrateMock.buyRate = 0.009136789813d;
		dccrateMock.sellRate = 0.009132222518d;
		doReturn(mockHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();
		
		
		doReturn(mockResultSet).when(dailyDetailFileProcessEventMock).getTransactions(aFileRecMock);
		doReturn(dccrateMock).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap);
		doNothing().when(dailyDetailFileProcessEventMock).updateMerchantClearing(mockHandler, testMap, dccrateMock);
		doNothing().when(dailyDetailFileProcessEventMock).updatePresentment(mockHandler, testMap, dccrateMock);
		
		assertTrue(dailyDetailFileProcessEventMock.calcDccFundingAmounts(aFileRecMock));

	}

	protected MesQueryHandlerList getQueryHandler() {
		return new MesQueryHandlerList();
	}

	private Map<String, Object> setUpExchangeRateDate() {
		Map<String, Object> testDataMap = new HashMap<String, Object>();
		testDataMap.put("card_type", "MC");
		testDataMap.put("base_currency_code", "036");
		testDataMap.put("counter_currency_code", "840");
		testDataMap.put("fx_conv_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		return testDataMap;

	}
	
	@Test
	public void testCalcDccFundingAmounts_multiTrx_BothRatesFound() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		MesResultSet mockResultSet = getTrxResultSet();
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(1).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		Map<String, Object> testMap1 = new HashMap<String, Object>();
		testMap1.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap1.put("batch_number", 61778);
		testMap1.put("fx_markup", BigDecimal.valueOf(1.3d));
		testMap1.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap1.put("base_currency_code", "046");
		testMap1.put("counter_currency_code", "840");
		testMap1.put("card_type", "MC");
		testMap1.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		DccRate dccrateMock = new DccRate();
		dccrateMock.buyRate = 0.009136789813d;
		dccrateMock.sellRate = 0.009132222518d;
		doReturn(mockHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();

		DccRate dccrateMock1 = new DccRate();
		dccrateMock.buyRate = 0.009136789823d;
		dccrateMock.sellRate = 0.009132222118d;
		doReturn(mockHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();

		doReturn(mockResultSet).when(dailyDetailFileProcessEventMock).getTransactions(aFileRecMock);

		doReturn(dccrateMock).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap);
		doNothing().when(dailyDetailFileProcessEventMock).updateMerchantClearing(mockHandler, testMap, dccrateMock);
		doNothing().when(dailyDetailFileProcessEventMock).updatePresentment(mockHandler, testMap, dccrateMock);

		doReturn(dccrateMock1).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap1);
		doNothing().when(dailyDetailFileProcessEventMock).updateMerchantClearing(mockHandler, testMap1, dccrateMock1);
		doNothing().when(dailyDetailFileProcessEventMock).updatePresentment(mockHandler, testMap1, dccrateMock1);

		assertTrue(dailyDetailFileProcessEventMock.calcDccFundingAmounts(aFileRecMock));
		Mockito.verify(dailyDetailFileProcessEventMock, Mockito.times(1)).updateMerchantClearing(mockHandler, testMap, dccrateMock);
		Mockito.verify(dailyDetailFileProcessEventMock, Mockito.times(1)).updateMerchantClearing(mockHandler, testMap1, dccrateMock1);

	}

	@Test
	public void testCalcDccFundingAmounts_RateNotFound() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);

		mockResultSet.add(new HashMap<String, Object>());
		mockResultSet.get(0).put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(0).put("batch_number", 61777);
		mockResultSet.get(0).put("fx_markup", BigDecimal.valueOf(1.5d));
		mockResultSet.get(0).put("ddf_dt_id", BigDecimal.valueOf(123));
		mockResultSet.get(0).put("base_currency_code", "036");
		mockResultSet.get(0).put("counter_currency_code", "840");
		mockResultSet.get(0).put("card_type", "MC");
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		DccRate dccrateMock = new DccRate();

		doReturn(mockResultSet).when(dailyDetailFileProcessEventMock).getTransactions(aFileRecMock);

		doReturn(null).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap);

		assertFalse(dailyDetailFileProcessEventMock.calcDccFundingAmounts(aFileRecMock));
		Mockito.verify(dailyDetailFileProcessEventMock, Mockito.times(0)).updateMerchantClearing(mockHandler, testMap, dccrateMock);

	}

	@Test
	public void testCalcDccFundingAmounts_multiTrx_OneRateNotFound() throws Exception {
		setUpMockData();

		DailyDetailFileProcessEvent dailyDetailFileProcessEventMock = Mockito.spy(DailyDetailFileProcessEvent.class);
		FileRec aFileRecMock = new FileRec("testfile.dat", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		MesResultSet mockResultSet = getTrxResultSet();
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(1).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap.put("batch_number", 61777);
		testMap.put("fx_markup", BigDecimal.valueOf(1.5d));
		testMap.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap.put("base_currency_code", "036");
		testMap.put("counter_currency_code", "840");
		testMap.put("card_type", "MC");
		testMap.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		Map<String, Object> testMap1 = new HashMap<String, Object>();
		testMap1.put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		testMap1.put("batch_number", 61778);
		testMap1.put("fx_markup", BigDecimal.valueOf(1.3d));
		testMap1.put("ddf_dt_id", BigDecimal.valueOf(123));
		testMap1.put("base_currency_code", "046");
		testMap1.put("counter_currency_code", "840");
		testMap1.put("card_type", "MC");
		testMap1.put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		DccRate dccrateMock = new DccRate();
		dccrateMock.buyRate = 0.009136789813d;
		dccrateMock.sellRate = 0.009132222518d;
		doReturn(mockHandler).when(dailyDetailFileProcessEventMock).getQueryHandler();

		doReturn(mockResultSet).when(dailyDetailFileProcessEventMock).getTransactions(aFileRecMock);

		doReturn(dccrateMock).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap);
		doNothing().when(dailyDetailFileProcessEventMock).updateMerchantClearing(mockHandler, testMap, dccrateMock);
		doNothing().when(dailyDetailFileProcessEventMock).updatePresentment(mockHandler, testMap, dccrateMock);

		doReturn(null).when(dailyDetailFileProcessEventMock).getExchangeRate(aFileRecMock, testMap1);

		assertFalse(dailyDetailFileProcessEventMock.calcDccFundingAmounts(aFileRecMock));
		Mockito.verify(dailyDetailFileProcessEventMock, Mockito.times(1)).updateMerchantClearing(mockHandler, testMap, dccrateMock);
		Mockito.verify(dailyDetailFileProcessEventMock, Mockito.times(0)).updateMerchantClearing(mockHandler, testMap1, dccrateMock);

	}

	public MesResultSet getTrxResultSet() {
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);

		mockResultSet.add(new HashMap<String, Object>());
		mockResultSet.get(0).put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(0).put("batch_number", 61777);
		mockResultSet.get(0).put("fx_markup", BigDecimal.valueOf(1.5d));
		mockResultSet.get(0).put("ddf_dt_id", BigDecimal.valueOf(123));
		mockResultSet.get(0).put("base_currency_code", "036");
		mockResultSet.get(0).put("counter_currency_code", "840");
		mockResultSet.get(0).put("card_type", "MC");
		mockResultSet.get(0).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));

		mockResultSet.add(new HashMap<String, Object>());
		mockResultSet.get(1).put("batch_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		mockResultSet.get(1).put("batch_number", 61778);
		mockResultSet.get(1).put("fx_markup", BigDecimal.valueOf(1.3d));
		mockResultSet.get(1).put("ddf_dt_id", BigDecimal.valueOf(123));
		mockResultSet.get(1).put("base_currency_code", "046");
		mockResultSet.get(1).put("counter_currency_code", "840");
		mockResultSet.get(1).put("card_type", "MC");
		mockResultSet.get(1).put("fx_conversion_date", new java.sql.Date(DateTimeFormatter.parseDate("03/02/2020", "MM/dd/yyyy").getTime()));
		return mockResultSet;
	}
}
