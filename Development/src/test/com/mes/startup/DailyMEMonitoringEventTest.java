package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;

public class DailyMEMonitoringEventTest {

    @Test
    @SuppressWarnings({"deprecation"})
    public void getProcessTypeCountResult() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";
        
        // Given
        final Date sqlDate = Date.valueOf("2019-05-15");

        // When
        final int oldWayDayOfMonth = sqlDate.getDate();

        final int newWayDayOfMonth = sqlDate.toLocalDate().getDayOfMonth();

        // Then
        Assert.assertEquals(message, oldWayDayOfMonth, newWayDayOfMonth);
    }
}
