package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;
import java.util.Vector;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.mes.ach.AchDb;
import com.mes.ach.AchStatementRecord;
import com.mes.config.DbProperties;
import com.mes.data.MesDataSourceManager;
import com.mes.data.payouts.PayoutsFundingProcess;
import com.mes.data.payouts.PayoutsFundingProcessService;
import com.mes.database.SQLJConnectionBase;

import sqlj.runtime.error.RuntimeRefErrors;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AchDb.class,PayoutsFundingProcessService.class, MesDataSourceManager.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class,
     Connection.class})
public class AchDbTest {
    
    @Mock
    Vector stmts;
    
    @Mock
    DataSource dataSource;
    
    @Captor
    ArgumentCaptor<List<PayoutsFundingProcess>> captor;
    
    @Before
    public void setup() throws Exception {
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
        PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));
        /* Mock static method calls */
        PowerMockito.mockStatic(DbProperties.class);
        PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);
        
        PowerMockito.whenNew(MesDataSourceManager.class).withAnyArguments().thenReturn(null);
        Whitebox.setInternalState(MesDataSourceManager.class, "INSTANCE", PowerMockito.mock(MesDataSourceManager.class));
        PowerMockito.when(MesDataSourceManager.INSTANCE.getDataSource()).thenReturn(dataSource);
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
      System.setProperty("log4j.configuration", "Development/log4j.properties");
      System.setProperty("DbConfigurationFileName", "Development/mes_datasource.properties");
    }
    
    public static AchStatementRecord getAchRecord() {
        AchStatementRecord achStatementRecord = new AchStatementRecord();
        achStatementRecord.setAchAmount(20);
        achStatementRecord.setBankNumber(3941);
        achStatementRecord.setPostDate(new Date(0));
        achStatementRecord.setRecId(1234L);
        achStatementRecord.setMerchantId(941000300500L);
        return achStatementRecord;
    }
    
    @Test
    public void testCreatePayoutsFundingProcess_Prefunding() throws Exception {
        PayoutsFundingProcessService service = PowerMockito.mock(PayoutsFundingProcessService.class);
        PowerMockito.whenNew(PayoutsFundingProcessService.class).withNoArguments().thenReturn(service);
        PowerMockito.doNothing().when(service).insert(Mockito.anyListOf(PayoutsFundingProcess.class));
        Mockito.when(stmts.size()).thenReturn(1);
        AchStatementRecord achRecord = getAchRecord();
        achRecord.setPreFundInd("Y");
        achRecord.setSameDayInd("N");
        Mockito.when(stmts.elementAt(Mockito.anyInt())).thenReturn(achRecord);
        
        AchDb.createPayoutsFundingProcessEntries(stmts);
        
        Mockito.verify(service, Mockito.times(1)).insert(captor.capture());
        List<PayoutsFundingProcess> process = captor.getAllValues().get(0);
        
        assertEquals(Integer.valueOf(1), process.get(0).getProcessType());
        assertEquals("DDF", process.get(0).getUserCreated());
    }
    
    @Test
    public void testCreatePayoutsFundingProcess_SameDay() throws Exception {
        PayoutsFundingProcessService service = PowerMockito.mock(PayoutsFundingProcessService.class);
        PowerMockito.whenNew(PayoutsFundingProcessService.class).withNoArguments().thenReturn(service);
        PowerMockito.doNothing().when(service).insert(Mockito.anyListOf(PayoutsFundingProcess.class));
        Mockito.when(stmts.size()).thenReturn(1);
        AchStatementRecord achRecord = getAchRecord();
        achRecord.setPreFundInd("N");
        achRecord.setSameDayInd("Y");
        Mockito.when(stmts.elementAt(Mockito.anyInt())).thenReturn(achRecord);
        
        AchDb.createPayoutsFundingProcessEntries(stmts);
        
        Mockito.verify(service, Mockito.times(1)).insert(captor.capture());
        List<PayoutsFundingProcess> process = captor.getAllValues().get(0);
        
        assertEquals(Integer.valueOf(2), process.get(0).getProcessType());
        assertEquals("DDF", process.get(0).getUserCreated());
        
    }
    
    @Test
    public void testCreatePayoutsFundingProcess_Prefund_SameDay() throws Exception {
        PayoutsFundingProcessService service = PowerMockito.mock(PayoutsFundingProcessService.class);
        PowerMockito.whenNew(PayoutsFundingProcessService.class).withNoArguments().thenReturn(service);
        PowerMockito.doNothing().when(service).insert(Mockito.anyListOf(PayoutsFundingProcess.class));
        Mockito.when(stmts.size()).thenReturn(1);
        AchStatementRecord achRecord = getAchRecord();
        achRecord.setPreFundInd("Y");
        achRecord.setSameDayInd("Y");
        Mockito.when(stmts.elementAt(Mockito.anyInt())).thenReturn(achRecord);
        
        AchDb.createPayoutsFundingProcessEntries(stmts);
        
        Mockito.verify(service, Mockito.times(1)).insert(captor.capture());
        List<PayoutsFundingProcess> process = captor.getAllValues().get(0);
        
        assertEquals(Integer.valueOf(3), process.get(0).getProcessType());
        assertEquals("DDF", process.get(0).getUserCreated());
        
    }
    
    @Test
    public void testCreatePayoutsFundingProcess_NoProgram() throws Exception {
        PayoutsFundingProcessService service = PowerMockito.mock(PayoutsFundingProcessService.class);
        PowerMockito.whenNew(PayoutsFundingProcessService.class).withNoArguments().thenReturn(service);
        PowerMockito.doNothing().when(service).insert(Mockito.anyListOf(PayoutsFundingProcess.class));
        Mockito.when(stmts.size()).thenReturn(1);
        AchStatementRecord achRecord = getAchRecord();
        achRecord.setPreFundInd("N");
        achRecord.setSameDayInd("N");
        Mockito.when(stmts.elementAt(Mockito.anyInt())).thenReturn(achRecord);
        
        AchDb.createPayoutsFundingProcessEntries(stmts);
        
        Mockito.verify(service, Mockito.times(0)).insert(captor.capture());
    }
}
