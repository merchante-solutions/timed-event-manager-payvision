package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.ResultSetMetaData;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.data.merchant.ProcessForcedPostsService;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.settlement.SettlementDb;
import masthead.formats.visak.Batch;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.internal.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.ResultSetIterImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RiskBatchScanner.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class, MailMessage.class, RuntimeRefErrors.class})
public class RiskBatchScannerTest {

	private RiskBatchScanner mockedRiskBatchScanner = null;
	private RiskBatchScannerInfo mockedRiskBatchScannerInfo = null;
	private SettlementDb mockedSettlementDB = null;
	private OracleResultSet oracleResultSet = null;
	private ProcessForcedPostsService processForcedPostsService = null;

	private int[] PrioritiesByIndex;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		PrioritiesByIndex = new int[RiskBatchScanner.SCANNER_COUNT];
		for (int idxScanner = 0; idxScanner < RiskBatchScanner.SCANNER_COUNT; ++idxScanner) {
			PrioritiesByIndex[idxScanner] = RiskBatchScanner.SCANNER_UNKNOWN;
		}

		/* Suppress method call and super class constructor objects */

		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
		PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));

		/* Mock static method calls */
		PowerMockito.mockStatic(MailMessage.class);
		PowerMockito.doNothing().when(MailMessage.class, "sendSystemErrorEmail", anyString(), anyString());
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		/* Mock required objects */

		mockedSettlementDB = Mockito.mock(SettlementDb.class);
		mockedRiskBatchScannerInfo = getRiskBatchScannerInfo();
		mockedRiskBatchScanner = PowerMockito.spy(new RiskBatchScanner());
		Mockito.doReturn(true).when((SQLJConnectionBase) mockedRiskBatchScanner).connect(true);
		ExecutionContext executionContext = Mockito.mock(ExecutionContext.class);

		PowerMockito.when(mockedSettlementDB._loadRawBatchDataVisak(mockedRiskBatchScannerInfo.BatchId)).thenReturn(new Batch());

		processForcedPostsService = Mockito.mock(ProcessForcedPostsService.class);
		PowerMockito.whenNew(ProcessForcedPostsService.class).withAnyArguments().thenReturn(processForcedPostsService);

		ResultSetMetaData resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
		oracleResultSet = Mockito.mock(OracleResultSet.class);
		ResultSetIterator resultSetIterator = Mockito.spy(ResultSetIterator.class);
		ResultSetIterImpl resultSetIterImpl = Mockito.mock(ResultSetIterImpl.class);
		DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
		OracleContext oracleContext = Mockito.mock(OracleContext.class);
		OraclePreparedStatement oraclePreparedStatement = Mockito.mock(OraclePreparedStatement.class);

		Whitebox.setInternalState((SQLJConnectionBase) mockedRiskBatchScanner, "Ctx", defaultContext);

		PowerMockito.whenNew(ResultSetIterImpl.class).withAnyArguments().thenReturn(resultSetIterImpl);
		when(oracleContext.prepareOracleStatement(Mockito.any(DefaultContext.class), Mockito.any(String.class), Mockito.any(String.class))).thenReturn(oraclePreparedStatement);
		when(defaultContext.getExecutionContext()).thenReturn(executionContext);
		when(executionContext.getOracleContext()).thenReturn(oracleContext);
		when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
		when(resultSetIterator.getResultSet()).thenReturn(oracleResultSet);
		when(resultSetIterImpl.getResultSet()).thenReturn(oracleResultSet);
		when(oracleResultSet.getLong("batch_id")).thenReturn(mockedRiskBatchScannerInfo.BatchId);
		when(oracleResultSet.getMetaData()).thenReturn(resultSetMetaData);
		when(resultSetMetaData.getColumnCount()).thenReturn(1);
		when(oracleResultSet.wasNull()).thenReturn(false);
	}

	private void setupStaticCFPMock() throws Exception {
		when(oracleResultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount == 0 || noCount == 2)
					return true;
				return false;
			}
		});

		when(oracleResultSet.getInt(1)).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				if (count++ == 0)
					return 1;
				return 0;
			}
		});
	}

	/* CFP flag disabled for merchant */
	@Test
	public void testScanBatchMerchantCFPDisabled() throws Exception {
		setupStaticCFPMock();
		PrioritiesByIndex[RiskBatchScanner.SCANNER_FORCE_POST] = RiskBatchScanner.SCANNER_FORCE_POST;
		Whitebox.setInternalState(mockedRiskBatchScanner, "PrioritiesByIndex", PrioritiesByIndex);
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(mockedRiskBatchScannerInfo.MerchantNumberString))).thenReturn(false);
		mockedRiskBatchScanner.scanBatch(mockedRiskBatchScannerInfo, mockedSettlementDB, 1);
		String[] HoldsByIndex = (String[]) Whitebox.getInternalState(mockedRiskBatchScanner, "HoldsByIndex");
		assertEquals(HoldsByIndex[RiskBatchScanner.SCANNER_FORCE_POST], "hold-offline-not-permitted-1");

	}

	/* CFP flag enabled for merchant */
	@Test
	public void testScanBatchMerchantCFPEnabled() throws Exception {
		setupStaticCFPMock();
		PrioritiesByIndex[RiskBatchScanner.SCANNER_FORCE_POST] = RiskBatchScanner.SCANNER_FORCE_POST;
		Whitebox.setInternalState(mockedRiskBatchScanner, "PrioritiesByIndex", PrioritiesByIndex);
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(mockedRiskBatchScannerInfo.MerchantNumberString))).thenReturn(true);
		mockedRiskBatchScanner.scanBatch(mockedRiskBatchScannerInfo, mockedSettlementDB, 1);
		String[] HoldsByIndex = (String[]) Whitebox.getInternalState(mockedRiskBatchScanner, "HoldsByIndex");
		assertNull(HoldsByIndex[RiskBatchScanner.SCANNER_FORCE_POST]);
	}

	/* ForcedPost hold reason disabled */
	@Test
	public void testScanBatchCFPHoldReasonDisabled() throws Exception {
		setupStaticCFPMock();
		Whitebox.setInternalState(mockedRiskBatchScanner, "PrioritiesByIndex", PrioritiesByIndex);
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(mockedRiskBatchScannerInfo.MerchantNumberString))).thenReturn(true);
		mockedRiskBatchScanner.scanBatch(mockedRiskBatchScannerInfo, mockedSettlementDB, 1);
		String[] HoldsByIndex = (String[]) Whitebox.getInternalState(mockedRiskBatchScanner, "HoldsByIndex");
		assertNull(HoldsByIndex[RiskBatchScanner.SCANNER_FORCE_POST]);
	}

	@Test
	public void testScanBatchNotHavingCFPTXN() throws Exception {
		setupStaticCFPMock();
		when(oracleResultSet.getInt(1)).thenAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) {
				return 0;
			}
		});
		Whitebox.setInternalState(mockedRiskBatchScanner, "PrioritiesByIndex", PrioritiesByIndex);
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(mockedRiskBatchScannerInfo.MerchantNumberString))).thenReturn(true);
		mockedRiskBatchScanner.scanBatch(mockedRiskBatchScannerInfo, mockedSettlementDB, 1);
		String[] HoldsByIndex = (String[]) Whitebox.getInternalState(mockedRiskBatchScanner, "HoldsByIndex");
		assertNull(HoldsByIndex[RiskBatchScanner.SCANNER_FORCE_POST]);
	}

	private RiskBatchScannerInfo getRiskBatchScannerInfo() {
		RiskBatchScannerInfo batchScannerInfo = new RiskBatchScannerInfo();
		batchScannerInfo.AccountIsTest = "N";
		batchScannerInfo.BatchId = 1;
		batchScannerInfo.AccountStatus = "Z";
		batchScannerInfo.AmountCredits = 10.0;
		batchScannerInfo.AmountDebits = 20.0;
		batchScannerInfo.AmountNet = 10;
		batchScannerInfo.AmountSum = 30;
		batchScannerInfo.BatchNumber = 1;
		batchScannerInfo.BatchType = 1;
		batchScannerInfo.CurrencyCode = "840";
		batchScannerInfo.BatchDateMinusOpenedDate = 1;
		batchScannerInfo.DbaName = "cs test ProfitStars-MRI";
		batchScannerInfo.MerchantNumberString = "941000072761";
		batchScannerInfo.ProfileIdString = "94100007276100000013";
		batchScannerInfo.TranCount = 2;
		return batchScannerInfo;
	}

	@Test
	public void scanBatches() {
		// Given
		long batchId = System.currentTimeMillis();

		// When
		final Long oldWayLongObject = new Long(batchId);

		// Then
		assertTrue(batchId == oldWayLongObject);
		assertTrue(oldWayLongObject.equals(batchId));
	}
}
