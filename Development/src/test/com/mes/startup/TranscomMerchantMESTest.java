package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class TranscomMerchantMESTest {

    @Test
    public void markAppsToProcess() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";

        // Given
        long appSeqNum = System.nanoTime();

        // When
        final Long oldWayLongObject = new Long(appSeqNum);

        // Then
        Assert.assertTrue(message, appSeqNum == oldWayLongObject);
        Assert.assertTrue(message, oldWayLongObject.equals(appSeqNum));
    }
}
