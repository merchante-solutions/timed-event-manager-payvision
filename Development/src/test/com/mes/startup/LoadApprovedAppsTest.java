package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class LoadApprovedAppsTest {

    @Test
    public void loadApps() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";

        // Given
        int appSeqNum = System.identityHashCode(this);

        // When

        // old way
        final Integer oldWayInteger = new Integer(appSeqNum);

        // Then
        Assert.assertTrue(message, oldWayInteger.equals(appSeqNum));
        Assert.assertTrue(message, appSeqNum == oldWayInteger);
    }
}
