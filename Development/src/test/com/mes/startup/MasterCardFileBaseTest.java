package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.InterchangeBase;
import com.mes.settlement.InterchangeMasterCard;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordMC;
import masthead.mastercard.clearingiso.IpmOfflineFileWriter;
import sqlj.runtime.ResultSetIterator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MasterCardFileBase.class, DbProperties.class, SettlementDb.class})
public class MasterCardFileBaseTest {

	MasterCardFileBase mockMasterCardFileBase;

	@Mock
	ResultSetIterator resultSetIterator;

	@Mock
	ResultSet resultSet;

	@Mock
	InterchangeMasterCard interchangeMasterCard;

	SettlementDb settlementDb;
	SettlementRecordMC settlementRecordMC = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		settlementDb = Mockito.mock(SettlementDb.class);
		PowerMockito.whenNew(SettlementDb.class).withAnyArguments().thenReturn(settlementDb);
		PowerMockito.whenNew(InterchangeMasterCard.class).withAnyArguments().thenReturn(interchangeMasterCard);
		Mockito.doReturn(true).when(settlementDb).connect(true);

		Mockito.doReturn(resultSet).when(resultSetIterator).getResultSet();

		settlementRecordMC = Mockito.spy(SettlementRecordMC.class);
		PowerMockito.whenNew(SettlementRecordMC.class).withNoArguments().thenReturn(settlementRecordMC);

		PowerMockito.mockStatic(SettlementDb.class);
		PowerMockito.when(SettlementDb.loadIrdListMC(settlementRecordMC)).thenReturn(new ArrayList());

		PowerMockito.doNothing().when(SettlementDb.class, "updateIcCatDowngrade", settlementRecordMC);

		mockMasterCardFileBase = PowerMockito.mock(MasterCardFileBase.class, Mockito.CALLS_REAL_METHODS);
		Whitebox.setInternalState(mockMasterCardFileBase, "SettlementRecMT", SettlementRecord.MT_FIRST_PRESENTMENT);
		Mockito.doNothing().when(mockMasterCardFileBase).buildFileHeader(any());
		PowerMockito.doNothing().when(mockMasterCardFileBase, "buildDetailRecords", Mockito.any(IpmOfflineFileWriter.class), Mockito.any(SettlementRecordMC.class), Mockito.any(Boolean.class));
		Mockito.doNothing().when(mockMasterCardFileBase).buildFileTrailer(any());
		Mockito.doNothing().when(mockMasterCardFileBase).sendFile(anyString());

		HashMap EmvProperties = new HashMap();
		EmvProperties.put("visa", "");
		EmvProperties.put("mastercard", "");
		EmvProperties.put("amex", "");
		EmvProperties.put("discover", "");

		Whitebox.setInternalState(mockMasterCardFileBase, "EmvProperties", EmvProperties);

		Mockito.doNothing().when(settlementRecordMC).setFields(resultSet, false, false);

		Mockito.doReturn("5154402715726744").when(settlementRecordMC).getCardNumberFull();
	}

	private void setupInternalRejects(String icCode) throws Exception {
		when(resultSetIterator.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(resultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(settlementRecordMC.getString("ic_cat")).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount == 0)
					return "000";
				return icCode;
			}
		});

	}

	@Test
	public void testHandleSelectedRecords_internRejectsTrue() throws Exception {

		setupInternalRejects("641");
		Whitebox.setInternalState(mockMasterCardFileBase, "ActionCode", "X");
		Mockito.doReturn("N").when(settlementRecordMC).getData("external_reject");
		String workFileName = "mc_settle3941_091619_001.ipm";
		Long workFileid = 89639622l;
		mockMasterCardFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);

		assertEquals(workFileName, settlementRecordMC.getData("load_filename"));
		assertEquals(workFileid, Long.valueOf(settlementRecordMC.getData("load_file_id")));
		assertEquals("Y", settlementRecordMC.getData("ach_flag"));
		assertEquals("N", settlementRecordMC.getData("external_reject"));
		assertEquals("641", settlementRecordMC.getData("ic_cat_downgrade"));
		Mockito.verify(settlementDb, Mockito.times(1)).updateDTRecord(settlementRecordMC);
	}

	@Test
	public void testHandleSelectedRecords_noReject() throws Exception {

		setupInternalRejects("");
		Mockito.doReturn(null).when(settlementRecordMC).getData("external_reject");
		Whitebox.setInternalState(mockMasterCardFileBase, "ActionCode", null);

		String workFileName = "mc_settle3941_091619_002.ipm";
		long workFileid = 89639623l;

		mockMasterCardFileBase.handleSelectedRecords(resultSetIterator, workFileName, workFileid);
		assertEquals("", settlementRecordMC.getData("load_filename"));
		assertEquals("", settlementRecordMC.getData("load_file_id"));
		assertEquals("", settlementRecordMC.getData("ach_flag"));
		assertEquals(null, settlementRecordMC.getData("external_reject"));
		assertEquals("", settlementRecordMC.getData("ic_cat_downgrade"));
		Mockito.verify(settlementDb, Mockito.times(0)).updateDTRecord(settlementRecordMC);
	}

}
