package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class CieloActivityDetailEventTest {

    @Test
    public void loadNetRevenue() {
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";

        // Given
        double totalFees = Math.random();

        // When

        // old way
        final Double oldWayDoubleObject = new Double(totalFees);

        // Then
        Assert.assertTrue(message, totalFees == oldWayDoubleObject);
        Assert.assertTrue(message, oldWayDoubleObject.equals(totalFees));
        Assert.assertEquals(message, oldWayDoubleObject, totalFees, Double.MIN_VALUE);
    }
}
