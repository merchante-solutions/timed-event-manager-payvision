package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class TridentDebitEventTest {

    @Test
    public void loadChargebackSystem() {
        // Given
        long recId = System.nanoTime();

        // When
        final Long oldWayLongObject = new Long(recId);

        // Then
        Assert.assertTrue(oldWayLongObject.equals(recId));
        Assert.assertTrue(recId == oldWayLongObject);
    }
}
