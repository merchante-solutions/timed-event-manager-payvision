package com.mes.startup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.mes.config.DbProperties;
import com.mes.constants.FlatFileDefConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.persist.vo.BusOwnerData;

import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AmexOptBlueSPMFileBase.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class,
        MailMessage.class, RuntimeRefErrors.class, Connection.class })
public class AmexOptBlueSPMFileBaseTest {

    @Mock
    private ResultSet rs;

    @Mock
    public Connection con;

    @Mock
    private PreparedStatement stmt;

    private AmexOptBlueSPMFileBase systemUnderTest = null;

    private String sellerId = "941000123692";

    private Integer busOwnerNum = 6;

    private Integer addrTypeCode = 13;

    @Before
    public void setUp() throws Exception {
        /* Suppress method call and super class constructor objects */
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
        PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_NO_ROW_SELECT_INTO"));
        PowerMockito.suppress(PowerMockito.method(RuntimeRefErrors.class, "raise_MULTI_ROW_SELECT_INTO"));
        /* Mock static method calls */
        PowerMockito.mockStatic(DbProperties.class);
        PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);
        /* Mock required objects */
        systemUnderTest = PowerMockito.spy(new AmexOptBlueSPMFileBase());
        Mockito.doReturn(true).when((SQLJConnectionBase) systemUnderTest).connect(true);
        DefaultContext defaultContext = Mockito.mock(DefaultContext.class);

        Connection con = Mockito.mock(Connection.class);
        Mockito.when(defaultContext.getConnection()).thenReturn(con);
        Whitebox.setInternalState((SQLJConnectionBase) systemUnderTest, "Ctx", defaultContext);
        Whitebox.setInternalState((SQLJConnectionBase) systemUnderTest, "con", con);
        Mockito.when(con.prepareStatement(Mockito.anyString())).thenReturn(stmt);
        Mockito.when(stmt.executeQuery()).thenReturn(rs);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(false);
    }

    @Test
    public void getBusOwnerDataTest() throws Exception {
        mockbusownerdata();
        BusOwnerData bus = Whitebox.invokeMethod(systemUnderTest, "getBusOwnerData", sellerId, busOwnerNum,
                addrTypeCode);
        assertEquals("busfirstName", bus.getOwnerFirstName());
        assertEquals("busLastName", bus.getOwnerLastName());
        assertEquals("GA", bus.getOwnerState());
        assertEquals("300006000", bus.getOwnerZip());
        assertEquals("20-06-1992", bus.getOwnerDob());
        assertEquals(StringUtils.rightPad("84-456700", 30), bus.getOwnerSSN());
        assertEquals("peachtree", bus.getOwnerAddress());
    }

    public void mockbusownerdata() throws Exception {
        Mockito.when(rs.getString(FlatFileDefConstants.BUSOWNER_FIRST_NAME)).thenReturn("busfirstName");
        Mockito.when(rs.getString(FlatFileDefConstants.BUSOWNER_LAST_NAME)).thenReturn("busLastName");
        Mockito.when(rs.getString(FlatFileDefConstants.ADDRESS_LINE1)).thenReturn("peachtree");
        Mockito.when(rs.getString(FlatFileDefConstants.ADDRESS_CITY)).thenReturn("atlanta");
        Mockito.when(rs.getString(FlatFileDefConstants.COUNTRYSTATE_CODE)).thenReturn("GA");
        Mockito.when(rs.getString(FlatFileDefConstants.ADDRESS_ZIP)).thenReturn("300006");
        Mockito.when(rs.getString(FlatFileDefConstants.BUSOWNER_DL_DOB)).thenReturn("20-06-1992");
        Mockito.when(rs.getString(FlatFileDefConstants.BUSOWNER_SSN)).thenReturn("84-4567");
        Mockito.when(rs.getString(FlatFileDefConstants.OWNER_SSN)).thenReturn("84-4567");
    }
}
