package com.mes.startup;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.mbs.MonthEndValidation;
import com.mes.net.EmailQueueHelper;
import com.mes.startup.MonthlyExtractSummarizer.SummaryJob;
import com.mes.support.LoggingConfigurator;
import com.mes.support.SyncLog;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.sqlj.runtime.Oracle;
import sqlj.runtime.ConnectionContext;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.ConnectionContextImpl;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.ResultSetIterImpl;

import java.net.URLEncoder;
import java.nio.charset.Charset;

//@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
//@RunWith(PowerMockRunner.class)
//@PrepareForTest({SummaryJob.class, ResultSetMetaData.class, OracleResultSet.class, ResultSetIterImpl.class, ResultSet.class, PreparedStatement.class,
//		OraclePreparedStatement.class, OracleContext.class, EmailQueueHelper.class, Oracle.class, SyncLog.class, ConnectionContext.class,
//		ConnectionContextImpl.class, ExecutionContext.class, DefaultContext.class, LoggingConfigurator.class, DbProperties.class,
//		MonthlyExtractSummarizer.class, EventBase.class, SQLJConnectionBase.class, RuntimeRefErrors.class})
public class MonthlyExtractSummarizerTest {
	
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception {
//	
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(ExecutionContext.class));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(OraclePreparedStatement.class));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(PreparedStatement.class));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(ResultSet.class));
//		PowerMockito.suppress(PowerMockito.method(ResultSetIterImpl.class, "close"));
//		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(RuntimeRefErrors.class));
//		String tmpConnectionString = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(PORT=1521)(HOST=*******.*******.******.********.*****)))(CONNECT_DATA=(service_name=********.******.******.********.****))));oracle.jdbc.driver.OracleDriver;user=******;password=******";
//		DefaultContext defConMock = mock(DefaultContext.class);
//		OracleContext oracleConMock = mock(OracleContext.class);
//		OracleResultSet oracleResultMock = mock(OracleResultSet.class);
//		OraclePreparedStatement oraclePreparedMock = mock(OraclePreparedStatement.class);
//		ResultSetMetaData resultSetMetaDataMock = mock(ResultSetMetaData.class);
//		PowerMockito.spy(SQLJConnectionBase.class);
//		PowerMockito.spy(DefaultContext.class);
//		ExecutionContext exeConMock = spy(ExecutionContext.class);
//		PowerMockito.when(SQLJConnectionBase.getDirectConnectString()).thenReturn(tmpConnectionString);
//		PowerMockito.when(DefaultContext.getDefaultContext()).thenReturn(defConMock);
//		PowerMockito.when(defConMock.getExecutionContext()).thenReturn(exeConMock);
//		PowerMockito.when(exeConMock.getOracleContext()).thenReturn(oracleConMock);
//		PowerMockito.when(oracleConMock.oracleExecuteQuery()).thenReturn(oracleResultMock);
//		PowerMockito.when(oracleResultMock.getMetaData()).thenReturn(resultSetMetaDataMock);
//
//		/* EmailQueue methodName Query1 */
//		String secondParam = "0com.mes.net.EmailQueueHelper";
//		String thirdParam = "select    address_type-1 address_type,\n" + "                  address\n" + "        from      email_addresses\n" + "        where     email_id =  :1 \n" + "                  and enabled = 'Y'";
//		PowerMockito.when(oracleConMock.prepareOracleStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query1 */
//		secondParam = "63com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "BEGIN load_file_index_init(  :1   )\n      \n; END;";
//		PowerMockito.when(oracleConMock.prepareOracleStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query2 */
//		secondParam = "64com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "select  load_filename_to_load_file_id( :1  ),\n                get_file_bank_number( :2  )\n         \n        from    dual";
//		PowerMockito.when(oracleConMock.prepareOracleStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query3 */
//		secondParam = "65com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "delete\n          from    monthly_extract_summary sm\n          where   sm.load_file_id =  :1  \n                  and \n                  (\n                     :2   = 0 \n                    or sm.merchant_number in \n                        ( \n                          select  gm.merchant_number \n                          from    organization    o, \n                                  group_merchant  gm \n                          where   o.org_group =  :3   \n                                  and gm.org_num = o.org_num \n                        )\n                  )";
//		PowerMockito.when(oracleConMock.prepareOracleStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query4 */
//		secondParam = "66com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "select  gn.hh_load_sec                     as load_sec\n        from    monthly_extract_gn        gn, \n                mif                       mf\n        where   gn.load_file_id = load_filename_to_load_file_id( :1  )\n                and mf.merchant_number = gn.hh_merchant_number\n                and \n                (\n                   :2   = 0 or\n                  not exists\n                  (\n                    select  sm.merchant_number\n                    from    monthly_extract_summary   sm\n                    where   sm.merchant_number = gn.hh_merchant_number\n                            and sm.active_date = gn.hh_active_date\n                  )\n                )\n                and\n                (\n                   :3   = 0 \n                  or gn.hh_merchant_number in \n                      ( \n                        select  gm.merchant_number \n                        from    organization    o, \n                                group_merchant  gm \n                        where   o.org_group =  :4   \n                                and gm.org_num = o.org_num \n                      )\n                )                        \n                and \n                (\n                  ( \n                     :5   like 'mon_ext%'\n                    and ( nvl(mf.processor_id,0) != 1\n                          or mf.conversion_date > gn.hh_active_date )\n                  )\n                  or\n                  ( \n                     :6   like 'mbs_ext%'\n                    and nvl(mf.processor_id,0) = 1\n                    and mf.conversion_date <= gn.hh_active_date\n                  )\n                )\n        order by gn.hh_load_sec";
//		PowerMockito.when(oracleConMock.prepareOracleStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query5 */
//		secondParam = "67com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 9,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query6 */
//		secondParam = "68com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 2,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query7 */
//		secondParam = "69com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 5,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query8 */
//		secondParam = "70com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 12,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query9 */
//		secondParam = "71com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n              ( process_type, load_filename )\n            values\n              ( 7,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query10 */
//		secondParam = "72com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n                ( process_type, load_filename )\n              values\n                ( 10,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query11 */
//		secondParam = "73com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into commission_process\n              ( process_type, load_filename )\n            values\n              ( 1,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query12 */
//		secondParam = "74com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into merch_prof_process\n              ( process_type, load_filename )\n            values\n              ( 13,  :1   )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		String mockStringDate = "2019-01-01";
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
//		java.util.Date date = sdf.parse(mockStringDate);
//		java.sql.Date mockDate = new Date(date.getTime());
//		MonthEndValidation mevSpy = spy(new MonthEndValidation(0, mockDate));
//
//		/* MonthlyExtractSummary loadMonthlyExtractSummary Query13 */
//		secondParam = "75com.mes.startup.MonthlyExtractSummarizer";
//		thirdParam = "insert into statement_process ( process_type,process_sequence,load_filename )\n              values (0,0, :1  )";
//		PowerMockito.when(oracleConMock.prepareOracleBatchableStatement(defConMock, secondParam, thirdParam)).thenReturn(oraclePreparedMock);
//
//		PowerMockito.doNothing().when(oracleConMock).oracleClose();
//		PowerMockito.doNothing().when(oracleConMock).oracleCloseQuery();
//
//		// System.out.println("oracleConMock - " + oracleConMock.toString());
//		// System.out.println("oraclePreparedMock - " + oracleConMock.oracleExecuteQuery().toString());
//		// System.out.println("oracleResultMock - " + oracleResultMock.toString());
//	}


	@Test
	public void testLoadMonthlyExtractSummary() {
		final String message = "Pre- and post- Java 8 upgrade values should be equal.";

		// Given
		long loadSec = System.nanoTime();

		// When
		final Long oldWayLongObject = new Long(loadSec);

		// Then
		Assert.assertTrue(message, loadSec == oldWayLongObject);
		Assert.assertTrue(message, oldWayLongObject.equals(loadSec));
	}

	// TODO this method is too big to mock. Needs to be refactored
	@Test
	public void testRun() throws Exception {
		/*
		 * ExpenseRecord exprec21 = new ExpenseRecord();
		 * exprec21.setLoadSec(85834787);
		 * exprec21.setItemCat("EX");
		 * exprec21.setMerchantNumber(85834787);
		 * exprec21.setActiveDate( new java.sql.Date(new SimpleDateFormat("yyyy-mm-dd").parse("2019-02-01").getTime()));
		 * exprec21.setDescription("MasterCard SafetyNet and Fraud Dashboard");
		 * exprec21.setRateCalculation("mcAuthCount * 0.01");
		 * exprec21.setItemCount(7);
		 * exprec21.setItemAmount(0.0);
		 * exprec21.setTotFeeAmount(0.07);
		 */

	//	MonthlyExtractSummarizer mesSpy = spy(MonthlyExtractSummarizer.class);
	//	MonthlyExtractSummarizer.SummaryJob summaryJobSpy = spy(mesSpy.new SummaryJob(85834791));
	//	summaryJobSpy.run();

		// ExpenseRecord expenseRecordMock = mock(ExpenseRecord.class);
		// MonthlyExtractSummarizer monthExtractSummarizerSpy = new MonthlyExtractSummarizer();// spy(MonthlyExtractSummarizer.class);
		// ExpenseRecord expenseRecordMock = mock(ExpenseRecord.class);
		// MonthlyExtractSummarizer monthExtractSummarizerSpy = spy(MonthlyExtractSummarizer.class);
		// monthExtractSummarizerSpy.loadMonthlyExtractSummary("mbs_ext3941_021119_003.dat", false);
		// doCallRealMethod().when(monthExtractSummarizerMock).loadMonthlyExtractSummary("mbs_ext3941_021119_003.dat", false);
		// doCallRealMethod().when(expenseRecordMock).setLoadSec(85834787);
		// monthExtractSummarizerSpy.loadMonthlyExtractSummary("mbs_ext3941_021119_003.dat", false);
		// PowerMockito.whenNew(ExpenseRecord.class).withNoArguments().thenReturn(expenseRecordMock);
		// doNothing().when( expenseRecordMock ).setLoadSec(85834787);
		// monthExtractSummarizerMock.loadMonthlyExtractSummary("mbs_ext3941_021119_003.dat", false);
		// Mockito.verify(expenseRecordMock, Mockito.times(1)).setLoadSec(85834787);
		// Mockito.verify(expenseRecordMock, Mockito.times(1)).setLoadSec(85834787);

		// ExpenseRecord expenseRecordMock = mock(ExpenseRecord.class);
		// verify(expenseRecordMock, atLeastOnce()).setLoadSec(85834787);
		// PowerMockito.doNothing().when(expenseRecordMock, "setLoadSec", 10L);
		// PowerMockito.whenNew(ExpenseRecord.class).withNoArguments().thenReturn(expenseRecordMock);
		// verify(expenseRecordMock, atLeastOnce()).setLoadSec(85834787);
		// verify(expenseRecordMock, atLeastOnce()).setItemCat(anyString());
		// verify(expenseRecordMock, atLeastOnce()).setMerchantNumber(anyLong());
		// verify(expenseRecordMock, atLeastOnce()).setActiveDate(any(Date.class));
		// verify(expenseRecordMock, atLeastOnce()).setDescription(anyString());
		// verify(expenseRecordMock, atLeastOnce()).setRateCalculation(anyString());
		// verify(expenseRecordMock, atLeastOnce()).setItemCount(anyInt());
		// verify(expenseRecordMock, atLeastOnce()).setItemAmount(anyDouble());
		// verify(expenseRecordMock, atLeastOnce()).setTotFeeAmount(anyDouble());
		// verify(expenseRecordMock, atLeastOnce()).setItemAmount(anyDouble());
	}
}
