package com.mes.startup;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.InterchangeBase;
import com.mes.settlement.InterchangeVisa;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordVisa;
import sqlj.runtime.ResultSetIterator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({VisaFileBase.class, DbProperties.class, SettlementDb.class})
@SuppressStaticInitializationFor("com.mes.settlement.InterchangeVisa")
public class VisaFileBaseTest {

	VisaFileBase mockVisaFileBase;

	@Mock
	ResultSetIterator resultSetIterator;

	@Mock
	ResultSet resultSet;

	@Mock
	InterchangeVisa interchangeVisa;

	SettlementDb settlementDb;
	SettlementRecordVisa settlementRecordVisa = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		settlementDb = Mockito.mock(SettlementDb.class);
		PowerMockito.whenNew(SettlementDb.class).withAnyArguments().thenReturn(settlementDb);
		PowerMockito.whenNew(InterchangeVisa.class).withAnyArguments().thenReturn(interchangeVisa);
		Mockito.doReturn(true).when(settlementDb).connect(true);

		Mockito.doReturn(resultSet).when(resultSetIterator).getResultSet();

		settlementRecordVisa = Mockito.spy(SettlementRecordVisa.class);
		PowerMockito.whenNew(SettlementRecordVisa.class).withNoArguments().thenReturn(settlementRecordVisa);

		PowerMockito.mockStatic(SettlementDb.class);
		PowerMockito.when(SettlementDb.loadIrdListMC(settlementRecordVisa)).thenReturn(new ArrayList<Object>());

        PowerMockito.doNothing().when(SettlementDb.class, "updateIcCatDowngrade", settlementRecordVisa);

		mockVisaFileBase = PowerMockito.mock(VisaFileBase.class, Mockito.CALLS_REAL_METHODS);
		Whitebox.setInternalState(mockVisaFileBase, "SettlementRecMT", SettlementRecord.MT_FIRST_PRESENTMENT);
		PowerMockito.doNothing().when(mockVisaFileBase, "buildDetailRecords", Mockito.any(BufferedReader.class), Mockito.any(SettlementRecord.class), Mockito.any(Boolean.class));
		PowerMockito.doNothing().when(mockVisaFileBase, "closeFile", Mockito.any(BufferedWriter.class));
		Mockito.doNothing().when(mockVisaFileBase).sendFile(anyString());

		HashMap<String, String> EmvProperties = new HashMap<String, String>();
		EmvProperties.put("visa", "");
		EmvProperties.put("mastercard", "");
		EmvProperties.put("amex", "");
		EmvProperties.put("discover", "");

		Whitebox.setInternalState(mockVisaFileBase, "EmvProperties", EmvProperties);
		Whitebox.setInternalState(mockVisaFileBase, "ActionCode", "X");

		Mockito.doNothing().when(settlementRecordVisa).setFields(resultSet, false, false);
		Mockito.doReturn("4111111111111111").when(settlementRecordVisa).getCardNumberFull();
		Mockito.doReturn("N").when(settlementRecordVisa).getData("external_reject");
		Mockito.doReturn(123456l).when(mockVisaFileBase).loadFilenameToLoadFileId(anyString());
	}

	private void setupInternalRejects() throws Exception {
		when(resultSetIterator.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(resultSet.next()).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount < 1)
					return true;
				return false;
			}
		});

		when(settlementRecordVisa.getString("ic_cat")).thenAnswer(new Answer<Object>() {
			private int count = 0;

			public Object answer(InvocationOnMock invocation) {
				int noCount = count++;
				if (noCount == 0)
					return "000";
				return "641";
			}
		});

	}

	@Test
	public void testHandleSelectedRecords_internRejectsTrue() throws Exception {

		setupInternalRejects();
		String workFileName = "vs_settle9999_102819_004.ctf";
		mockVisaFileBase.handleSelectedRecords(resultSetIterator, workFileName);

		assertEquals(workFileName, settlementRecordVisa.getData("load_filename"));
        assertEquals("Y", settlementRecordVisa.getData("ach_flag"));
        assertEquals("N", settlementRecordVisa.getData("external_reject"));
        Mockito.verify(settlementDb, Mockito.times(1)).updateDTRecord(settlementRecordVisa);
	}

	@Test
	public void testHandleSelectedRecords_internRejectsFalse() throws Exception {

		setupInternalRejects();
		Mockito.doReturn(null).when(settlementRecordVisa).getData("external_reject");

		String workFileName = "vs_settle9999_102819_004.ctf";

		mockVisaFileBase.handleSelectedRecords(resultSetIterator, workFileName);
		assertEquals("", settlementRecordVisa.getData("ach_flag"));
	    assertEquals("", settlementRecordVisa.getData("load_filename"));
	    assertEquals(null, settlementRecordVisa.getData("external_reject"));
		Mockito.verify(settlementDb, Mockito.times(0)).updateDTRecord(settlementRecordVisa);
	}
}