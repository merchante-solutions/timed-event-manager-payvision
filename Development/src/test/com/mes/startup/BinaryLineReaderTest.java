package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class BinaryLineReaderTest {

    @Test
    public void readLine() {
        // Given
        byte[] bytes = {5};

        // When
        final Byte oldWayByteObject = new Byte(bytes[0]);

        // Then
        Assert.assertTrue(oldWayByteObject.equals(bytes[0]));
        Assert.assertTrue(bytes[0] == oldWayByteObject);
    }
}
