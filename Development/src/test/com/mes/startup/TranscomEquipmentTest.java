package com.mes.startup;

import org.junit.Assert;
import org.junit.Test;

public class TranscomEquipmentTest {

    @Test
    public void markAppsToProcess() {
        // Given
        final long appSeqNum = System.nanoTime();

        // When

        // old way
        final Long oldWayLongObject = new Long(appSeqNum);

        // Then
        final String message = "Pre- and post- Java 8 upgrade values should be equal.";
        Assert.assertTrue(message, appSeqNum == oldWayLongObject);
        Assert.assertTrue(message, oldWayLongObject.equals(appSeqNum));
    }
}
