package com.mes.startup;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.util.Vector;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.mes.EventBaseTest;
import com.mes.settlement.SettlementRecordExtractor;

@RunWith(PowerMockRunner.class)
@PrepareForTest({BatchExtractionEvent.class})
public class BatchExtractionEventTest extends EventBaseTest {
    
    @Mock
    public Connection con;
    private BatchExtractionEvent systemUnderTest;
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        PowerMockito.whenNew(SettlementRecordExtractor.class).withAnyArguments().thenReturn(null);
        systemUnderTest = new BatchExtractionEvent();
        systemUnderTest.EventArgsVector = new Vector<String>();
        Whitebox.setInternalState(systemUnderTest, "Ctx", ctx);
        mockDatabaseCalls();
    }
    
    @Test
    public void testextractMbsBatches_fasterFundingEnabled() throws Exception {
        Whitebox.setInternalState(systemUnderTest, "fasterFundingEnabled", true);
        Whitebox.invokeMethod(systemUnderTest, "extractMbsBatches");
        
        assertEquals("Faster Funding", Whitebox.getInternalState(systemUnderTest, "fasterFundingClause", BatchExtractionEvent.class), BatchExtractionEvent.FASTER_FUNDING_ENABLED);
    }
    
    @Test
    public void testextractMbsBatches_fasterFundingDisabled() throws Exception {
        Whitebox.setInternalState(systemUnderTest, "fasterFundingEnabled", false);
        Whitebox.invokeMethod(systemUnderTest, "extractMbsBatches");
        
        assertEquals("Faster Funding", Whitebox.getInternalState(systemUnderTest, "fasterFundingClause", BatchExtractionEvent.class), BatchExtractionEvent.FASTER_FUNDING_DISABLED);
    }
    
}
