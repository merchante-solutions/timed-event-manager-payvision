package com.mes.support;

import java.security.CodeSource;
import org.junit.Test;
import com.mes.commons.clients.encryption.domain.DecryptionRequest;
import com.mes.commons.clients.encryption.domain.DecryptionResponse;
import com.mes.commons.clients.encryption.service.EncryptionClientJerseyImpl;

//This is an integration test remove the @Ignore to manually run it
//@Ignore
public class MesEncryptionTest {

	@Test
	public void testEncryption()
	{
		
	}
	
	@Test 
	public void testDecryptString() throws Exception
	{
		EncryptionClientJerseyImpl client = new EncryptionClientJerseyImpl();
		DecryptionRequest decryptionRequest = new DecryptionRequest("http://encryptng01.dev.atx.merchante-solutions.com:8484", "000010000010000200000008", "958e4e87181500a9");
		
		DecryptionResponse response = client.decrypt(decryptionRequest);
		System.out.println(response.getResponseData());
	}
}
