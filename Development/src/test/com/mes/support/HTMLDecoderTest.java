package com.mes.support;

import org.junit.Assert;
import org.junit.Test;

public class HTMLDecoderTest {

	@Test
	public void staticBlock() {
		Character oldWay = new Character((char) 34);
		Character newWay = (char) 34;
		Assert.assertEquals(oldWay, newWay);
	}
}
