package com.mes.support;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Assert;
import org.junit.Test;

public class MesMathTest {

	@Test
	public void round() {
		double iLikePie = 3.14159;
		int decimalCount = 3;
		
		BigDecimal oldWayBigDecimal = new BigDecimal(Double.toString(iLikePie));
		oldWayBigDecimal = oldWayBigDecimal.setScale(decimalCount, BigDecimal.ROUND_HALF_UP);
		
		BigDecimal newWayBigDecimal = new BigDecimal(Double.toString(iLikePie));
		newWayBigDecimal = newWayBigDecimal.setScale(decimalCount, RoundingMode.HALF_UP);
		
		Assert.assertEquals(oldWayBigDecimal, newWayBigDecimal);
	}

}
