
package com.mes.support;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mes.tools.CurrencyCodeConverter;


@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({CurrencyCodeConverter.class, TridentTools.class})
public class TridentToolsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		PowerMockito.suppress(method(CurrencyCodeConverter.class, "load"));
	}
	
	@Test
	public void testDecodeVisakCardTypeDiscUnionPay() {  //Bin range: (62292600-62379699)
		assertEquals("DS",TridentTools.decodeVisakCardType("6229268659525708"));
		assertEquals("DS",TridentTools.decodeVisakCardType("6229264443480128"));
		assertEquals("DS",TridentTools.decodeVisakCardType("6229260593054333"));
		assertEquals("DS",TridentTools.decodeVisakCardType("6229266588222561"));
		assertEquals("DS",TridentTools.decodeVisakCardType("6229268874702777"));
	}
	
}