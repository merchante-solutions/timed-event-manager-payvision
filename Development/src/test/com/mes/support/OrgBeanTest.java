package com.mes.support;

import org.junit.Assert;
import org.junit.Test;

public class OrgBeanTest {

	@Test
	public void addMerchant() {
		long merchantNumber = 3858424299L;
		Long autoBoxedLong = merchantNumber;
		
		Assert.assertEquals(new Long(merchantNumber), autoBoxedLong);
	}
	
	@Test
	public void addChildMerchant() {
		long merchantNumber = 3858424299L;
		Long autoBoxedLong = merchantNumber;
		
		Assert.assertEquals(new Long(merchantNumber), autoBoxedLong);
	}

}
