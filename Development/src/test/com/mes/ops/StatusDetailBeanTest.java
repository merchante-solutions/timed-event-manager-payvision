package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class StatusDetailBeanTest {

    private static final String message = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void getData() {
        // Given
        int deptCode = 65366;

        // When

        // old way
        final Integer oldWayInteger = new Integer(deptCode);

        // Then
        Assert.assertTrue(message, oldWayInteger == deptCode);
        Assert.assertTrue(message, oldWayInteger.equals(deptCode));
    }

    @Test
    public void setMerchantNumber() {
        // Given
        long merchantNumber = System.nanoTime();

        // When

        // old way
        final String oldWayString = new Long(merchantNumber).toString();

        // new way
        final String newWayString = Long.toString(merchantNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void setControlNumber() {
        // Given
        long controlNumber = System.currentTimeMillis();

        // When

        // old way
        final String oldWayString = new Long(controlNumber).toString();

        // new way
        final String newWayString = Long.toString(controlNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }
}
