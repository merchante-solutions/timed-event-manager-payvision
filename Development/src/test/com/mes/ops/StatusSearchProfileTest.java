package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class StatusSearchProfileTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void getControlNumber() {
        // Given
        long controlNumber = System.currentTimeMillis();
        
        // When
        
        // old way
        final String oldWayString = new Long(controlNumber).toString();

        // new way
        final String newWayString = Long.toString(controlNumber);

        // Then
        Assert.assertEquals(MESSAGE, oldWayString, newWayString);
    }

    @Test
    public void getMerchantNumber() {
        // Given
        long merchantNumber = System.nanoTime();

        // When

        // old way
        final String oldWayString = new Long(merchantNumber).toString();

        // new way
        final String newWayString = Long.toString(merchantNumber);

        // Then
        Assert.assertEquals(MESSAGE, oldWayString, newWayString);
    }
}
