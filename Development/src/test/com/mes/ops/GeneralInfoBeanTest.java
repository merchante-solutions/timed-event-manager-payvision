package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class GeneralInfoBeanTest {

    private final String message = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void setIncStatus() {
        // Given
        int busTypeCode = 578901;

        // When

        // old way
        final String oldWayString = new Integer(busTypeCode).toString();

        // new way
        final String newWayString = Integer.toString(busTypeCode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getSicCode() {
        // Given
        int sicCode = 640629;

        // When

        // old way
        final String oldWayString = new Integer(sicCode).toString();

        // new way
        final String newWayString = Integer.toString(sicCode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getMccCode() {
        // Given
        int mccCode = 674522;

        // When

        // old way
        final String oldWayString = new Integer(mccCode).toString();

        // new way
        final String newWayString = Integer.toString(mccCode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getBankNumber() {
        // Given
        int bankNumber = 39413000;

        // When

        // old way
        final String oldWayString = new Integer(bankNumber).toString();

        // new way
        final String newWayString = Integer.toString(bankNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getMerchantNumber() {
        // Given
        long merchantNumber = System.currentTimeMillis();

        // When

        // old way
        final String oldWayString = new Long(merchantNumber).toString();

        // new way
        final String newWayString = Long.toString(merchantNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getAssociation() {
        // Given
        long association = System.nanoTime();

        // When

        // old way
        final String oldWayString = new Long(association).toString();

        // new way
        final String newWayString = Long.toString(association);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }
}
