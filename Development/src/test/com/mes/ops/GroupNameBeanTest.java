package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class GroupNameBeanTest {
    private static final String message = "Pre- and post- upgrade values should be equal.";

    @Test
    public void getBankNumber() {
        // Given
        final int bankNumber = 385830910;

        // When

        // old way
        final String oldWayString = new Integer(bankNumber).toString();

        // new way
        final String newWayString = Integer.toString(bankNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getGroupNumber() {
        // Given
        final int groupNumber = 5942684;

        // When

        // old way
        final String oldWayString = new Integer(groupNumber).toString();

        // new way
        final String newWayString = Integer.toString(groupNumber);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getHierarchyNode() {
        // Given
        final long hierarchyNode = System.nanoTime();

        // When

        // old way
        final String oldWayString = new Long(hierarchyNode).toString();

        // new way
        final String newWayString = Long.toString(hierarchyNode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }
}
