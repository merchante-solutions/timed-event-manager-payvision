package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class CreditDataBeanTest {

    private static final String message = "Pre- and post- Java 8 upgrade values are equal.";

    @Test
    public void setIncStatuses() {
        // Given
        int busTypeCode = 7890123;

        // When

        // old way
        final String oldWayString = new Integer(busTypeCode).toString();

        // new way
        final String newWayString = Integer.toString(busTypeCode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void getSicCode() {
        // Given
        int sicCode = 465789;

        // When

        // old way
        final String oldWayString = new Integer(sicCode).toString();

        // new way
        final String newWayString = Integer.toString(sicCode);

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
    }

    @Test
    public void formatCurrency() {
        // Given
        int tempInt = 56436;
        String subStr = "foo";

        // When

        // old way
        final String oldWayString = new Integer(tempInt).toString() + ".00";
        final String oldWayString2 = subStr + "." + new Integer(tempInt).toString();

        // new way
        final String newWayString = tempInt + ".00";
        final String newWayString2 = subStr + "." + tempInt;

        // Then
        Assert.assertEquals(message, oldWayString, newWayString);
        Assert.assertEquals(message, oldWayString2, newWayString2);
    }
}
