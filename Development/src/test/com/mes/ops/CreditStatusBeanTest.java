package com.mes.ops;

import org.junit.Assert;
import org.junit.Test;

public class CreditStatusBeanTest {

    @Test
    public void loadOptionData() {
        // Given
        long appDocCode = 745767768646L;

        // old way
        final Long oldWayLongObject = new Long(appDocCode);

        // new way
        Assert.assertTrue("Pre- and post- upgrade values should be equal.", oldWayLongObject == appDocCode);
        Assert.assertTrue("Pre- and post- upgrade values should be equal.", oldWayLongObject.equals(appDocCode));
    }
}
