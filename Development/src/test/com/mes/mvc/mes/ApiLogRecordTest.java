package com.mes.mvc.mes;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import org.junit.Test;

public class ApiLogRecordTest {
	private long requestDuration = 1223L;
	ApiLogRecord logRecord;

	@Test
	@SuppressWarnings({"deprecatopm"})
	public void testGetDurationForDisplay() {
		logRecord = new ApiLogRecord();
		logRecord.setRequestDuration(requestDuration);
		BigDecimal durationOldWay = new BigDecimal("" + requestDuration).setScale(3);
		durationOldWay = durationOldWay.divide(new BigDecimal("1000"), BigDecimal.ROUND_HALF_UP);

		assertEquals("" + durationOldWay + " secs", logRecord.getDurationForDisplay());
	}

}