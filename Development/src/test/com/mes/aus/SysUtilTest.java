package com.mes.aus;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class SysUtilTest {

	@Test
	@SuppressWarnings("deprecation")
	public void classFields() {
		int assoc = 1234;
		Integer autoBoxedInteger = assoc;
		Assert.assertEquals(new Integer(assoc), autoBoxedInteger);
	}
}
