package com.mes.aus;

import org.junit.Assert;
import org.junit.Test;

public class AusDbTest {

	@Test
	@SuppressWarnings("deprecation")
	public void getUnprocessedInboundFileIds() {
		long assoc = 1234;
		Long autoBoxedLong = assoc;
		Assert.assertEquals(new Long(assoc), autoBoxedLong);
	}
}
