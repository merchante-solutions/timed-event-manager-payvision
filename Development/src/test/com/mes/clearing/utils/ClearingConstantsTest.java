package com.mes.clearing.utils;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import com.mes.clearing.utils.ClearingConstants;


@RunWith(MockitoJUnitRunner.class)
public class ClearingConstantsTest {
	
	@Test
	public void testClearingConstantsValues() {
		ClearingConstants pt_cb_adj = ClearingConstants.PT_CB_ADJ;
		assertEquals(pt_cb_adj.getValue(), 2);
		assertEquals(ClearingConstants.PT_CB_ADJ.getValue(),2);
		
		ClearingConstants pt_mojave_summary = ClearingConstants.PT_MOJAVE_SUMMARY;
		assertEquals(pt_mojave_summary.getValue(), 14);
		assertEquals(ClearingConstants.PT_MOJAVE_SUMMARY.getValue(),14);
		
		ClearingConstants sum_type_ddf = ClearingConstants.SUM_TYPE_DDF;
		assertEquals(sum_type_ddf.getValue(), 1);
		assertEquals(ClearingConstants.SUM_TYPE_DDF.getValue(),1);
		
		ClearingConstants proc_type_respn = ClearingConstants.PROC_TYPE_RESPN;
		assertEquals(proc_type_respn.getValue(), 2);
		assertEquals(ClearingConstants.PROC_TYPE_RESPN.getValue(),2);	
		
		ClearingConstants proc_type_cbdis = ClearingConstants.PROC_TYPE_CBDIS;
		assertEquals(proc_type_cbdis.getValue(), 1);
		assertEquals(ClearingConstants.PROC_TYPE_CBDIS.getValue(),1);	
		
	}

}
