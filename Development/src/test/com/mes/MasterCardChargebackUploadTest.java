package com.mes;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import com.mes.config.ConfigurationManager;
import com.mes.config.MesDefaults;
import com.mes.startup.MasterCardChargebackUpload;

@PrepareForTest({MasterCardChargebackUpload.class, MesDefaults.class,ConfigurationManager.class})
// Note in order to test this test class please make the entries in te.properties file for DK_MC_CHBK_MNL_ICA for each ICA number test cases
public class MasterCardChargebackUploadTest extends EventBaseTest {



	private String ICA_NUMBER_6739="006739";
	private String ICA_NUMBER_013074="013074";
	private String ICA_NUMBER_005185="005185";
	private String ICA_NUMBER_013921="013921";
	private String ICA_NUM_DEFAULT   ="0";

	@Mock
	public System asystem;


	public String path="006739,005185,013074,013921";
	
	@Mock
	public MesDefaults aMesDefaults;
	
	@Mock
	public ConfigurationManager aConfigurationManager;
	
	


	@Before
	public void setUp() throws Exception {
		super.setUp();
		
		PowerMockito.mockStatic(MesDefaults.class);
		PowerMockito.when(MesDefaults.getString(anyString())).thenReturn(path);
	}

	/*This Method will test that the passed query should not fetch the records having the ICA Number as 006739 */

	@Test
	public void testICA6739Excluded() throws Exception {
		
		

		String exludedICA=MesDefaults.getString("DK_MC_CHBK_MNL_ICA")!=null 
				&& !MesDefaults.getString("DK_MC_CHBK_MNL_ICA").isEmpty() ? MesDefaults.getString("DK_MC_CHBK_MNL_ICA") : ICA_NUM_DEFAULT;
		assertTrue(getFinalqueryToTest(exludedICA).contains(ICA_NUMBER_6739));


	}

	@Test
	public void testICA013074Excluded() throws Exception {

		String exludedICA=MesDefaults.getString("DK_MC_CHBK_MNL_ICA")!=null && !MesDefaults.getString("DK_MC_CHBK_MNL_ICA").isEmpty() ? MesDefaults.getString("DK_MC_CHBK_MNL_ICA") : ICA_NUM_DEFAULT;
		assertTrue(getFinalqueryToTest(exludedICA).contains(ICA_NUMBER_013074));


	}

	@Test
	public void testICA005185Excluded() throws Exception {
		String exludedICA=MesDefaults.getString("DK_MC_CHBK_MNL_ICA")!=null && !MesDefaults.getString("DK_MC_CHBK_MNL_ICA").isEmpty() ? MesDefaults.getString("DK_MC_CHBK_MNL_ICA") : ICA_NUM_DEFAULT;
		assertTrue(getFinalqueryToTest(exludedICA).contains(ICA_NUMBER_005185));


	}

	@Test
	public void testICA013921Excluded() throws Exception {
		
		String exludedICA=MesDefaults.getString("DK_MC_CHBK_MNL_ICA")!=null && !MesDefaults.getString("DK_MC_CHBK_MNL_ICA").isEmpty() ? MesDefaults.getString("DK_MC_CHBK_MNL_ICA") : ICA_NUM_DEFAULT;
		assertTrue(getFinalqueryToTest(exludedICA).contains(ICA_NUMBER_013921));


	}


	public String getFinalqueryToTest(String exludedICA)
	{
		String Query= "select  0                                           as batch_id,          -- not used in chargeback tables\n                  cb.cb_load_sec                              as batch_record_id,   -- really \"cb_load_sec\"\n                  cb.card_number                              as card_number,\n                  dukpt_decrypt_wrapper(cb.card_number_enc)   as card_number_full,\n                  cb.card_number_enc                          as card_number_enc,\n                  cb.tran_date                                as transaction_date,\n                  cbm.transaction_date                        as transaction_time,\n                  cb.reference_number                         as reference_number,\n                  cb.bin_number                               as bin_number,\n                  abs( cb.tran_amount )                       as transaction_amount,\n                  cbm.original_amt                            as original_amount,\n                  cbm.original_currency_code                  as currency_code,\n                  cbm.credit_debit_ind                        as debit_credit_indicator,\n                  cbm.cardholder_act_term_ind                 as cat_indicator,\n                  cbm.authorization_number                    as auth_code,\n                  substr(cbm.business_activity_ind,1,3)       as card_program_id,\n                  substr(cbm.business_activity_ind,4,1)       as bsa_type,\n                  substr(cbm.business_activity_ind,5,6)       as bsa_id_code,\n                  substr(cbm.business_activity_ind,11,2)      as ird,\n                  substr(cbm.processing_code,1,2)             as processing_code,\n                  substr(cbm.processing_code,3,2)             as cardholder_from_type,\n                  substr(cbm.processing_code,5,2)             as cardholder_to_type,\n                  cbm.pos_data_code                           as pos_data_code,\n                  cbm.ecomm_security_level_ind                as ecomm_security_level_ind,\n                  cbm.banknet_ref_num                         as auth_banknet_ref_num,\n                  cbm.banknet_date                            as auth_banknet_date,\n                  cba.function_code                           as function_code,\n                  lpad(cba.repr_reason_code,4,'0')            as reason_code,\n                  cbm.class_code                              as sic_code,\n                  cb.merchant_number                          as merchant_number,\n                  nvl(cbm.merchant_name,mf.dba_name)          as dba_name,\n                  nvl(cbm.merchant_city,mf.dmcity)            as dba_city,\n                  nvl(cbm.merchant_state,mf.dmstate)          as dba_state,\n                  mf.dmzip                                    as dba_zip,\n                  cbm.merchant_country                        as country_code,\n                  cbm.mc_ica_num                              as ica_number,\n                  cbm.cb_ref_num                              as cb_ref_num,\n                  cba.document_indicator                      as documentation_ind,\n                  cba.user_message                            as mc_member_message_block,\n                  least((cbm.usage_code+1),3)                 as mc_usage_code,\n                  cba.action_code                             as action_code,\n                  get_cb_represent_date(cb.cb_load_sec)       as batch_date         -- really \"cpd\"\n          from    network_chargebacks           cb, \n                  network_chargeback_activity   cba,\n                  chargeback_action_codes       ac,\n                  network_chargeback_mc         cbm,\n                  mif                           mf\n          where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                  and cb.incoming_date between ( :1 -180) and  :2 \n                  and cb.merchant_number != 0\n                  and not cb.card_number_enc is null\n                  and cba.cb_load_sec = cb.cb_load_sec\n                  and cba.load_filename =  :3 \n                  and ac.short_action_code = cba.action_code\n                  and nvl(ac.represent,'N') = 'Y'   -- REPR, REMC, REVR only\n                  and cbm.load_sec = cb.cb_load_sec\n                  and mf.merchant_number(+) = cb.merchant_number and  cbm.mc_ica_num NOT IN ("+exludedICA+")\n          order by cb.cb_load_sec";
		return Query;
	}


}
