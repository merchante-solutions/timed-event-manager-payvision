package com.mes.chargeback.utils;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mes.chargeback.utils.ChargeBackConstants;

@RunWith(MockitoJUnitRunner.class)
public class ChargeBackConstantsTest {
	
	@Test
	public void testChargeBackContantValues() {
		ChargeBackConstants cb_file = ChargeBackConstants.PROC_TYPE_CB_FILE;
		assertEquals(cb_file.getIntValue(), 2);
		assertEquals(ChargeBackConstants.PROC_TYPE_CB_FILE.getIntValue(),2);
		
		ChargeBackConstants pt_cb_adj = ChargeBackConstants.PT_CB_ADJ;
		assertEquals(pt_cb_adj.getIntValue(), 2);
		assertEquals(ChargeBackConstants.PT_CB_ADJ.getIntValue(),2);
		
		ChargeBackConstants nxt_day_fund_clause = ChargeBackConstants.NEXT_DAY_FUNDING_CLAUSE;
		assertEquals(nxt_day_fund_clause.getStringValue(), "and atd.same_day_funding = 'N'");
		assertEquals(ChargeBackConstants.NEXT_DAY_FUNDING_CLAUSE.getStringValue(),"and atd.same_day_funding = 'N'");
	}

}
