package com.mes.reports;

import java.util.HashMap;
import org.junit.Assert;
import org.junit.Test;

public class MbsReconcileDataBeanTest {

	@Test
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void testValidateAtsToAtd() {
		double testValue = 78.997;
		String testString = "32423";
		HashMap deprecatedHashMap = new HashMap();
		HashMap nonDeprecatedHashMap = new HashMap();
		deprecatedHashMap.put(testString, new Double(testValue));
		nonDeprecatedHashMap.put(testString, testValue);

		Assert.assertEquals("deprecatedHashMap should be equal to nonDeprecatedHashMap", deprecatedHashMap.get(testString), nonDeprecatedHashMap.get(testString));
	}
}