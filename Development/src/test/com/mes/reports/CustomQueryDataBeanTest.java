package com.mes.reports;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CustomQueryDataBeanTest {

	@Test
	public void testInitQueryParams() {
		long testValue = 100;
		Object deprecatedWay = new Long(testValue);
		Object newWay = testValue;

		assertEquals(deprecatedWay, newWay);
	}

}