package com.mes.reports;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SalesActivityTest {

	@Test
	public void testGetManagerId() {
		long managerId = 4234234234L;
		Long oldWay = new Long(managerId);
		Long newWay = managerId;

		assertEquals("oldWay value should be equal to newWay value", oldWay, newWay);

	}

	@Test
	public void testGetReportData() {
		long testValue = -1L;
		Long deprecatedWay = new Long(testValue);
		Long nonDeprecatedWay = testValue;

		assertEquals(deprecatedWay, nonDeprecatedWay);
	}

}