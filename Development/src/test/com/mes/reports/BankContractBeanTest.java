package com.mes.reports;


import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.util.Vector;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.FieldBean;
import com.mes.reports.BankContractBean.ContractItem;
import com.mes.startup.EventBase;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.internal.OracleResultSet;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.OraRTResultSet;
import sqlj.runtime.ref.ResultSetIterImpl;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ReportSQLJBean.class, FieldBean.class, SQLJConnectionBase.class, DbProperties.class, RuntimeRefErrors.class, Connection.class, ResultSetIterImpl.class, OraRTResultSet.class, 
     BankContractBean.class })
public class BankContractBeanTest {
    
    private BankContractBean bankContractBean;
    
    @Mock
    ExecutionContext executionContext;
    
    @Mock
    OracleContext oracleContext;
    
    @Mock
    Connection con;
    
    @Mock
    OraclePreparedStatement ps;
    
    @Mock
    OracleResultSet oracleResultSet;
    
    @Mock
    private Vector reportRows;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
      System.setProperty("log4j.configuration", "Development/log4j.properties");
    }
    
    @Before
    public void setup() {
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(ReportSQLJBean.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(FieldBean.class));
        PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
        PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
        PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
        PowerMockito.mockStatic(DbProperties.class);
        PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);
        
        DefaultContext defaultContext = Mockito.mock(DefaultContext.class);
        
        bankContractBean = PowerMockito.spy(new BankContractBean());
        Mockito.doReturn(39410000L).when((ReportSQLJBean) bankContractBean).orgIdToHierarchyNode(Mockito.anyLong());
        
        Whitebox.setInternalState((SQLJConnectionBase) bankContractBean, "con", con);
        Whitebox.setInternalState((SQLJConnectionBase) bankContractBean, "Ctx", defaultContext);
        Whitebox.setInternalState((ReportSQLJBean) bankContractBean, "Ctx", defaultContext);
        Whitebox.setInternalState((ReportSQLJBean) bankContractBean, "con", con);
        Whitebox.setInternalState((ReportSQLJBean) bankContractBean, "ReportRows", reportRows);
        
        Mockito.doReturn(true).when((ReportSQLJBean) bankContractBean).connect(true);
        
        Connection con = Mockito.mock(Connection.class);
        when(defaultContext.getConnection()).thenReturn(con);
        when(defaultContext.getExecutionContext()).thenReturn(executionContext);
        when(executionContext.getOracleContext()).thenReturn(oracleContext);
    }
    
    @Test
    public void testLoadContract() throws Exception {
        when(oracleContext.prepareOracleStatement(Mockito.any(DefaultContext.class), Mockito.anyString(), Mockito.anyString())).thenReturn(ps);
        ResultSetIterImpl resultSetIterImpl = Mockito.mock(ResultSetIterImpl.class);
        PowerMockito.whenNew(ResultSetIterImpl.class).withAnyArguments().thenReturn(resultSetIterImpl);
        when(oracleResultSet.next()).thenReturn(true).thenReturn(false);
        when(oracleContext.oracleExecuteQuery()).thenReturn(oracleResultSet);
        when(resultSetIterImpl.getResultSet()).thenReturn(oracleResultSet);
        mockContractItem();
        
        ArgumentCaptor<ContractItem> contractItemCaptor = ArgumentCaptor.forClass(ContractItem.class);
        bankContractBean.loadContract(1L, 6, new Date(0), 941000109165L);
        Mockito.verify(reportRows).add(contractItemCaptor.capture());
        equals(contractItemCaptor.getValue().RecordId == 1L);
        equals(contractItemCaptor.getValue().ContractType == 4);
        equals("Liability".equals(contractItemCaptor.getValue().ContractDesc));
        equals(contractItemCaptor.getValue().NodeId == 3941300396L);
        equals("ACH Return/Re-presentment Fee".equals(contractItemCaptor.getValue().ItemDescription));
    }
    
    public void mockContractItem() throws Exception {
        when(oracleResultSet.getLong("rec_id")).thenReturn(1L);
        when(oracleResultSet.getInt("contract_type")).thenReturn(4);
        when(oracleResultSet.getString("contract_desc")).thenReturn("Liability");
        when(oracleResultSet.getLong("hierarchy_node")).thenReturn(3941300396L);
        when(oracleResultSet.getString("org_name")).thenReturn("Test Org");
        when(oracleResultSet.getInt("item_type")).thenReturn(2);
        when(oracleResultSet.getString("item_desc")).thenReturn("ACH Return/Re-presentment Fee");
        when(oracleResultSet.getString("product_code")).thenReturn(null);
        when(oracleResultSet.getDate("valid_date_begin")).thenReturn(new Date(0));
        when(oracleResultSet.getDate("valid_date_end")).thenReturn(new Date(0));
        when(oracleResultSet.getDouble("rate")).thenReturn(2.3D);
        when(oracleResultSet.getString("rate_type")).thenReturn("P");
        when(oracleResultSet.getInt("exempt_count")).thenReturn(0);
        when(oracleResultSet.getInt("exclude")).thenReturn(1);
        when(oracleResultSet.getString("billing_months")).thenReturn("5");
    }

}
