package com.mes.reports;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MesUserProfileTest {

	@Test
	public void testGetSalesRepSSNFormatted() {
		int SalesRepSSN = 22;
		String retValOldWay = new Integer(SalesRepSSN).toString();
		String retValNewWay = Integer.toString(SalesRepSSN);

		assertEquals(retValOldWay, retValNewWay);
	}

}