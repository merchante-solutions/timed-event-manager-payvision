package com.mes.reports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.reports.MbsReconcileDataBean.BinSummary;
import com.mes.reports.MbsReconcileDataBean.ClearingSummary;

public class MasterCardClearingSummaryTest {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	int bank_number = 3941;
	int cutOff = 0;
	Date beginDate = null;
	DailyClearingSummaryReport reportMock = null;
	Connection mockJdbcConnection = null;
	PreparedStatement mockStatement = null;
	ResultSetMetaData mockMetaData = null;
	String preparedSql = null;
	MesQueryHandlerResultSet queryHandler = null;

	@Captor
	ArgumentCaptor<Date> dateCaptor;

	@Captor
	ArgumentCaptor<Integer> bankCaptor;
	@Captor
	ArgumentCaptor<Integer> cutoffCaptor;
	@Captor
	ArgumentCaptor<Object[]> objCaptor;

	@Mock
	MesQueryHandlerResultSet localQHMock;

	@Mock
	MesResultSet resultSetMock;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	public void setUpMockData() throws ParseException, SQLException, ClearingSummaryException {
		beginDate = new SimpleDateFormat("MM/dd/yyyy").parse("02/02/2020");
		reportMock = Mockito.mock(DailyClearingSummaryReport.getInstance("MC").getClass());
		objCaptor.getAllValues().clear();
		queryHandler = Mockito.spy(MesQueryHandlerResultSet.class);
		mockJdbcConnection = Mockito.mock(Connection.class);
		mockStatement = Mockito.mock(PreparedStatement.class);
		mockMetaData = Mockito.mock(ResultSetMetaData.class);
		doReturn(queryHandler).when(reportMock).getQueryHandler();
		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

	}

	@Test
	public void testLoadFirstPresentment_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadFirstPresentment(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());

		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("sales_amount", BigDecimal.valueOf(757919.79d));
		mockResultSet.get(0).put("ca_amount", BigDecimal.valueOf(524.87d));
		mockResultSet.get(0).put("credits_amount", BigDecimal.valueOf(145.08d));
		mockResultSet.get(0).put("funding_amount", BigDecimal.valueOf(7579.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,sales_amount", "credits_amount", "ca_amount", "funding_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(4);
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("sales_amount");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("credits_amount");
		when(mockMetaData.getColumnName(Mockito.eq(3))).thenReturn("ca_amount");
		when(mockMetaData.getColumnName(Mockito.eq(4))).thenReturn("funding_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("0com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadFirstPresentment", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());
		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getSalesAmount()), BigDecimal.valueOf(757919.79d));
		assertEquals(BigDecimal.valueOf(bs.getCashAdvanceAmount()), BigDecimal.valueOf(524.87d));
		assertEquals(BigDecimal.valueOf(bs.getCreditsAmount()), BigDecimal.valueOf(145.08d));
		assertEquals(BigDecimal.valueOf(bs.getFundingAmount()), BigDecimal.valueOf(7579.79d));
		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(0));
		assertEquals(0, argsCapture.get(2));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadReprocessedRejects_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadReprocessedRejects(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("reject_amount", BigDecimal.valueOf(111.79d));
		mockResultSet.get(0).put("reversal_amount", BigDecimal.valueOf(111.87d));
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(1).put("bin_number", "524770");
		mockResultSet.get(1).put("reject_amount", BigDecimal.valueOf(222.79d));
		mockResultSet.get(1).put("reversal_amount", BigDecimal.valueOf(222.87d));
		List<String> columnList = Arrays.asList(new String[] {"bin_number,reject_amount", "reversal_amount"});
		Whitebox.setInternalState(mockResultSet, "columnList", columnList);
		when(mockMetaData.getColumnCount()).thenReturn(3);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("reject_amount");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("reversal_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("1com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadReprocessedRejects", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(2, mockResultSet.size());
		assertEquals(2, aClearingSummary.getBinData().entrySet().size());
		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.get(0).get("bin_number").toString());
		assertEquals(BigDecimal.valueOf(bs.getReprocessedRejectsAmount()), BigDecimal.valueOf(111.79d));
		assertEquals(BigDecimal.valueOf(bs.getReversalsAmount()), BigDecimal.valueOf(111.87d));

		BinSummary bs1 = aClearingSummary.getBinSummary(mockResultSet.get(1).get("bin_number").toString());
		assertEquals(BigDecimal.valueOf(bs1.getReprocessedRejectsAmount()), BigDecimal.valueOf(222.79d));
		assertEquals(BigDecimal.valueOf(bs1.getReversalsAmount()), BigDecimal.valueOf(222.87d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(3));
		assertEquals(0, argsCapture.get(1));
		assertEquals(2, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadloadRejects_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadRejects(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());

		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("reject_amount", BigDecimal.valueOf(123.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,reject_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("reject_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("2com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadRejects", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getRejectsAmount()), BigDecimal.valueOf(123.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(3));
		assertEquals(0, argsCapture.get(1));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadIncomingChargebacks_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadIncomingChargebacks(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("cb_amount", BigDecimal.valueOf(13.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,cb_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("cb_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("3com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadIncomingChargebacks", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getIncomingChargebacksAmount()), BigDecimal.valueOf(13.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(0));
		assertEquals(0, argsCapture.get(2));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadIncomingChargebackReversals_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadIncomingChargebackReversals(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("reversal_amount", BigDecimal.valueOf(33.79d));
		mockResultSet.get(0).put("fx_adj_amount", BigDecimal.valueOf(2.19d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,reversal_amount,fx_adj_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(3);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("reversal_amount");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("fx_adj_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("4com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadIncomingChargebackReversals", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getIncomingReversalsAmount()), BigDecimal.valueOf(33.79d));
		assertEquals(BigDecimal.valueOf(bs.getFxAdjustmentAmount()), BigDecimal.valueOf(2.19d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(0));
		assertEquals(0, argsCapture.get(2));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadSecondPresentments_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadSecondPresentments(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("sp_amount", BigDecimal.valueOf(43.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,sp_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("sp_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("5com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadSecondPresentments", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getSecondPresentmentsAmount()), BigDecimal.valueOf(43.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(3));
		assertEquals(0, argsCapture.get(1));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadSecondPresentmentRejects_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadSecondPresentmentRejects(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("reject_amount", BigDecimal.valueOf(32.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,reject_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("reject_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("6com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadSecondPresentmentRejects", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getSecondPresentmentRejectsAmount()), BigDecimal.valueOf(32.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(3));
		assertEquals(0, argsCapture.get(1));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadFeeCollection_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadFeeCollection(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("fees_amount", BigDecimal.valueOf(132.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,fees_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);

		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("fees_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("7com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadFeeCollection", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getFeeCollectionAmount()), BigDecimal.valueOf(132.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(0));
		assertEquals(0, argsCapture.get(2));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

	@Test
	public void testLoadAssocData_MasterCard() throws Exception {
		setUpMockData();
		ClearingSummary aClearingSummary = new ClearingSummary();

		doCallRealMethod().when(reportMock).loadAssocData(aClearingSummary, beginDate, cutOff, bank_number);

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("bin_number", "514061");
		mockResultSet.get(0).put("reimbursement_fees", BigDecimal.valueOf(332.79d));
		mockResultSet.get(0).put("assoc_fees", BigDecimal.valueOf(112.79d));
		mockResultSet.get(0).put("total_amount", BigDecimal.valueOf(432.79d));

		List<String> columnList = Arrays.asList(new String[] {"bin_number,reimbursement_fees,assoc_fees,total_amount"});

		Whitebox.setInternalState(mockResultSet, "columnList", columnList);
		when(mockMetaData.getColumnCount()).thenReturn(4);
		when(mockMetaData.getColumnName(Mockito.eq(0))).thenReturn("bin_number");
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("reimbursement_fees");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("assoc_fees");
		when(mockMetaData.getColumnName(Mockito.eq(3))).thenReturn("total_amount");

		doReturn(mockMetaData).when(mockResultSet).getMetaData();

		when(mockStatement.executeQuery()).thenReturn(mockResultSet);

		doReturn(mockResultSet).when(queryHandler).executePreparedStatement(Mockito.eq("8com.mes.reports.mc.MasterCardClearingSummary"), Mockito.any(), objCaptor.capture());
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);

		Whitebox.invokeMethod(reportMock, "loadAssocData", aClearingSummary, beginDate, cutOff, bank_number);
		assertNotNull(mockResultSet);
		assertTrue(CollectionUtils.isNotEmpty(mockResultSet));
		assertEquals(1, mockResultSet.size());

		BinSummary bs = aClearingSummary.getBinSummary(mockResultSet.getString("bin_number"));
		assertEquals(BigDecimal.valueOf(bs.getReimbursementFees()), BigDecimal.valueOf(332.79d));
		assertEquals(BigDecimal.valueOf(bs.getAssocFees()), BigDecimal.valueOf(112.79d));
		assertEquals(BigDecimal.valueOf(bs.getActualAmount()), BigDecimal.valueOf(432.79d));

		assertNotNull(objCaptor.getAllValues());
		assertEquals(4, objCaptor.getAllValues().size());
		List<Object[]> argsCapture = objCaptor.getAllValues();
		assertEquals(3941, argsCapture.get(0));
		assertEquals(0, argsCapture.get(2));
		assertEquals(1, aClearingSummary.getBinData().entrySet().size());

	}

}
