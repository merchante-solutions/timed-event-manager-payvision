package com.mes.reports;

import static org.junit.Assert.assertEquals;
import java.util.Hashtable;
import org.junit.Test;

public class DiscountRevenueTest {

	@Test
	public void testGetManagerUserId() {
		long x = 44;
		Long deprecatedWay = new Long(x);
		Long newWay = x;
		assertEquals(deprecatedWay, newWay);
	}

	@Test
	public void testLoadDirectChildren() {
		long xy = 99;
		Long oldWay = new Long(xy);
		Long newWay = xy;
		assertEquals(oldWay, newWay);
	}

	@Test
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void testGetReportData() {
		long testValue = 324234;
		String testString = "234234";
		Hashtable deprecated = new Hashtable();
		Hashtable nonDeprecated = new Hashtable();
		deprecated.put(testString, new Long(testValue));
		nonDeprecated.put(testString, testValue);

		assertEquals("deprecated value should be equal to nonDeprecated value", deprecated.get(testString), nonDeprecated.get(testString));
	}

}
