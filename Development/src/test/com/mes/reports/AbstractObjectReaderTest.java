package com.mes.reports;

import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

public class AbstractObjectReaderTest {

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testSetFeature() {
		boolean testBoolean = true;
		String testString = "342342";
		Map deprecatedMap = new java.util.HashMap();
		Map nonDeprecatedMap = new java.util.HashMap();
		deprecatedMap.put(testString, new Boolean(testBoolean));
		nonDeprecatedMap.put(testString, testBoolean);
		
		Assert.assertEquals("Value of deprecatedMap should be equal to value of nonDeprecatedMap", deprecatedMap.get(testString), nonDeprecatedMap.get(testString));
	}

}