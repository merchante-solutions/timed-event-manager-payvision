package com.mes.reports;

import static org.junit.Assert.assertEquals;
import java.util.Vector;
import org.junit.Test;

public class ReportSQLJBeanTest {

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testGetChildAssocIds() {
		Vector retVal = new Vector();
		Long element = 334L;
		retVal.add(0, new Long(element));
		Vector newWayRetVal = new Vector();
		newWayRetVal.add(0, element);

		assertEquals(retVal.get(0), newWayRetVal.get(0));
	}

}