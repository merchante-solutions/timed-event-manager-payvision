package com.mes.forms;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CurrencyFieldTest {

	@Test
	@SuppressWarnings({"deprecation"})
	public void testValidate() {
		String testCurrencyValue = "44.99";

		assertEquals(new Float(testCurrencyValue), Float.valueOf(testCurrencyValue));
	}

}