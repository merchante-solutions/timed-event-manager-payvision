package com.mes.forms;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class NumberFieldTest {
	private static String testStringFloat = "16.0f";
	private static String testFloatWithoutf = "128.3";
	private static String testLongString = "3432424223";

	@Test
	@SuppressWarnings("deprecation")
	public void testValidate() {
		assertEquals(new Float(testStringFloat), Float.valueOf(testStringFloat));
		assertEquals(new Float(testFloatWithoutf), Float.valueOf(testFloatWithoutf));
	}

	@Test
	@SuppressWarnings("deprecation")
	public void numberValidation_validate() {
		assertEquals(new Float(testFloatWithoutf), Float.valueOf(testFloatWithoutf));
		assertEquals(new Long(testLongString), Long.valueOf(testLongString));
	}
}