package com.mes.forms;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PhoneNumberValidationTest {

	@Test
	@SuppressWarnings({"deprecation"})
	public void testValidate() {
		String testString = "3344234567";
		assertEquals(new Long(testString), Long.valueOf(testString));
	}
}
