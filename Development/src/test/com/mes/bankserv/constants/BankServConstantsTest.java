package com.mes.bankserv.constants;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import com.mes.bankserv.constants.BankServConstants;


@RunWith(MockitoJUnitRunner.class)
public class BankServConstantsTest {
	
	@Test
	public void testBankServConstantsValues() {
		BankServConstants next_day_funding_clause = BankServConstants.NEXT_DAY_FUNDING_CLAUSE;
		assertEquals(next_day_funding_clause.getValue(), "and atd.same_day_funding = 'N'");
		assertEquals(BankServConstants.NEXT_DAY_FUNDING_CLAUSE.getValue(),"and atd.same_day_funding = 'N'");
		
	}

}
