package com.mes.utils;

import org.junit.Assert;
import org.junit.Test;

public class CBTPricingTest {

    @Test
    public void testGetProfitMargin() {
        // Given
        final double profitMargin = 12345678.91;

        // When

        // old way
        final String oldWayObject = new Double(profitMargin).toString();
        // new way
        final String newWayObject = String.valueOf(profitMargin);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayObject, newWayObject);
    }

    @Test
    public void testGetAssocNumberString() {
        // Given
        final long assocNumber = 3456789012L;

        // When

        // old way
        final String oldWayString = new Long(assocNumber).toString();

        // new way
        final String newWayString = String.valueOf(assocNumber);

        // Then
        Assert.assertEquals("Pre- and post- upgrade values should be equal", oldWayString, newWayString);
    }
}