package com.mes.utils;

import org.junit.Assert;
import org.junit.Test;

public class PricingGridBeanTest {

    public static final String PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL = "Pre- and post- upgrade values should be equal";

    @Test
    public void testGetAnnualSales() {
        // Given
        final long annualSales = 53476321L;

        // When

        // old way
        final String oldWayString = new Long(annualSales).toString();

        // new way
        final String newWayString = String.valueOf(annualSales);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayString, newWayString);
    }

    @Test
    public void testGetAvgTicket() {
        // Given
        double avgTicket = 35964.86867;

        // When

        // old way
        final String oldWayString = new Double(avgTicket).toString();

        // new way
        final String newWayString = String.valueOf(avgTicket);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayString, newWayString);
    }

    @Test
    public void testGetTranCount() {
        // Given
        final long tranCount = 570912386L;

        // When

        // old way
        final String oldWayString = new Long(tranCount).toString();

        // new way
        final String newWayString = String.valueOf(tranCount);

        // Then
        Assert.assertEquals(PRE_AND_POST_UPGRADE_VALUES_SHOULD_BE_EQUAL, oldWayString, newWayString);
    }
}
