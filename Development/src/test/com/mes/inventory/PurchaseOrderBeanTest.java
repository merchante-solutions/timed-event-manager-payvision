package com.mes.inventory;

import static org.junit.Assert.assertEquals;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.Test;

public class PurchaseOrderBeanTest {

	@Test
	public void testCreateReferenceNum() {
		Calendar cal = new GregorianCalendar();
		String YYYY = String.valueOf(cal.get(Calendar.YEAR));
		String YY = YYYY.substring(2);
		Calendar calOldWay = new GregorianCalendar();
		String YYYYOldWay = new Integer(calOldWay.get(Calendar.YEAR)).toString();
		String YYOldWay = YYYYOldWay.substring(2);

		assertEquals(YY, YYOldWay);
	}

}
