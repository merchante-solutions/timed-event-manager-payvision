package com.mes.inventory;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class MerchantSearchBeanTest {
	private static final long controlNumber = 12345678910L;
	private static final String controlNumberArg = "12345678910";
	private static final String merchantNumber = "445";
	MerchantSearchBean merchantSearchBean = null;

	@Before
	public void setUp() {
		merchantSearchBean = new MerchantSearchBean();
		merchantSearchBean.setControlNumber(controlNumberArg);
		merchantSearchBean.setMerchantNumber(merchantNumber);
	}

	@Test
	public void testGetControlNumber() {
		String resultOldWay = new Long(this.controlNumber).toString();

		assertEquals(resultOldWay, merchantSearchBean.getControlNumber());
	}

	@Test
	public void testGetMerchantNumber() {

		assertEquals(new Long(this.merchantNumber).toString(), merchantSearchBean.getMerchantNumber());
	}

}
