package com.mes.inventory;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class EquipSearchBeanTest {
	private static final String endDate = "12/10/11";

	@Test
	public void GetEndDateNextTest() {

		int index1 = endDate.indexOf('/');
		int index2 = endDate.lastIndexOf('/');
		String nonDeprecatedDay = endDate.substring(index1 + 1, index2);
		int nextDay = Integer.parseInt(nonDeprecatedDay);
		nextDay++;
		nonDeprecatedDay = String.valueOf(nextDay);
		String deprecatedDay = endDate.substring(index1 + 1, index2);
		deprecatedDay = new Integer(nextDay).toString();

		assertEquals("New day should be equals with Old day", nonDeprecatedDay, deprecatedDay);
	}

}