package com.mes.inventory;

import org.junit.Assert;
import org.junit.Test;

public class RepairSearchBeanTest {

	@Test
	public void getDateNext() {
		int nextDay = 2;
		String oldDay = new Integer(nextDay).toString();
		String newDay = String.valueOf(nextDay);
		
		Assert.assertEquals(oldDay, newDay);
	}

}
