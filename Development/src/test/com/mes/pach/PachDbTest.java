package com.mes.pach;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class PachDbTest {

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testGetUnassignedBatchFileTypes() {
		long testValue = 78L;
		List deprecatedList = new ArrayList();
		List nonDeprecatedList = new ArrayList();
		deprecatedList.add(new Long(testValue));
		nonDeprecatedList.add(testValue);
		
		assertEquals("deprecatedList should be equal to nonDeprecatedList", deprecatedList.get(0), nonDeprecatedList.get(0));
	}

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testGetAchpRiskProcessTypes() {
		int testValue = 23;
		ArrayList deprecatedList = new ArrayList();
		ArrayList nonDeprecatedList = new ArrayList();
		deprecatedList.add(new Integer(testValue));
		nonDeprecatedList.add(testValue);
		
		assertEquals("Value got from deprecatedList need to matched with nonDeprecatedList", deprecatedList.get(0), nonDeprecatedList.get(0));
	}

}