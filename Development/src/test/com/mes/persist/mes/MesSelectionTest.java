package com.mes.persist.mes;

import java.util.HashMap;
import org.junit.Assert;
import org.junit.Test;

public class MesSelectionTest {

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testAddSubSel() {

		HashMap deprecatedMap = new HashMap();
		HashMap nonDeprecatedMap = new HashMap();
		int x = 78;
		String aa = "333";
		deprecatedMap.put(aa, new Integer(x));
		nonDeprecatedMap.put(aa, x);

		Assert.assertEquals(deprecatedMap.get(aa), nonDeprecatedMap.get(aa));
	}

}
