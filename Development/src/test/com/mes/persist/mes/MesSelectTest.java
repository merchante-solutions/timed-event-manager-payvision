package com.mes.persist.mes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MesSelectTest {

	@Test
	public void testGetColumnValue() {
		int sampleInt = 123;
		long sampleLong = 0L;
		float sampleFloat = 12.2F;
		double sampleDouble = 45.45543;

		Object deprecatedInteger = new Integer(sampleInt);
		Object deprecatedLong = new Long(sampleLong);
		Object deprecatedFloat = new Float(sampleFloat);
		Object deprecatedDouble = new Double(sampleDouble);

		assertEquals(sampleInt, deprecatedInteger);
		assertEquals(sampleLong, deprecatedLong);
		assertEquals(sampleFloat, deprecatedFloat);
		assertEquals(sampleDouble, deprecatedDouble);
	}

}