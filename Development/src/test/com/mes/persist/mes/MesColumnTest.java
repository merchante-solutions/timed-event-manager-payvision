package com.mes.persist.mes;

import org.junit.Assert;
import org.junit.Test;

public class MesColumnTest {

	@Test
	public void valueAsNumber_Date() {
		final long expectedResult = System.currentTimeMillis();
		final Object deprecatedResult = new Long(expectedResult);
		Assert.assertEquals(expectedResult, deprecatedResult);
	}

	@Test
	public void testSetLong() {
		long value = 44L;
		Object oldWay = new Long(value);
		Assert.assertEquals(oldWay, Long.valueOf(value));
	}

	@Test
	public void testSetInt() {
		int testValue = 111;
		Object oldWay = new Integer(testValue);
		Assert.assertEquals(oldWay, Integer.valueOf(testValue));
	}

	@Test
	public void testSetDouble() {
		double premitive = 133.223;
		Object oldWay = new Double(premitive);
		Assert.assertEquals(oldWay, Double.valueOf(premitive));
	}

	@Test
	public void testSetFloat() {
		float testValue = 444.4566F;
		Object oldWay = new Float(testValue);
		Assert.assertEquals(oldWay, Float.valueOf(testValue));
	}

}
