package com.mes.persist;

import org.junit.Assert;
import org.junit.Test;

public class InFilterTest {

	@Test
	public void addValue_int() {
		int x = 78;
		Object oldWay = new Integer(x);
		Object newWay = Integer.valueOf(x);
		Assert.assertEquals(oldWay, newWay);
	}

	@Test
	public void addValue_long() {
		long x = 78L;
		Object oldWay = new Long(x);
		Object newWay = Long.valueOf(x);
		Assert.assertEquals(oldWay, newWay);
	}

	@Test
	public void addValue_float() {
		float x = 78F;
		Object oldWay = new Float(x);
		Object newWay = Float.valueOf(x);
		Assert.assertEquals(oldWay, newWay);
	}

	@Test
	public void addValue_double() {
		double x = 78.42;
		Object oldWay = new Double(x);
		Object newWay = Double.valueOf(x);
		Assert.assertEquals(oldWay, newWay);
	}

}