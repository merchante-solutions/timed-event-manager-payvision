package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.when;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.startup.EventBase;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SettlementRecordVisa.class, DbProperties.class, SQLJConnectionBase.class, FlatFileRecord.class})
public class SettlementRecordVisaTest {

	private SettlementRecordVisa mockedSettlementRecordVisa;
	private SettlementRecordVisa settlementRecordVisa;
	private FlatFileRecord flatFileRecordTCR0;
	private FlatFileRecord flatFileRecordTCR1;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(Mockito.anyString())).thenReturn(true);
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(FlatFileRecord.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));
		settlementRecordVisa = PowerMockito.spy(new SettlementRecordVisa());
		mockedSettlementRecordVisa = new SettlementRecordVisa();
	}

	@Test
	public void testTaxIndicator_TaxAmount_GT_0_PG() throws Exception {

		mockedSettlementRecordVisa.setData("tax_amount", "2.37");
		mockedSettlementRecordVisa.fixBadData();
		assertEquals("1", mockedSettlementRecordVisa.getData("tax_indicator"));

		mockedSettlementRecordVisa.setData("tax_amount", "0.00");
		mockedSettlementRecordVisa.fixBadData();
		assertEquals("2", mockedSettlementRecordVisa.getData("tax_indicator"));
	}

	@Test
	public void testTaxIndicator_TaxAmount_NotPresent_PG() throws Exception {
		mockedSettlementRecordVisa.fixBadData();
		assertEquals("", mockedSettlementRecordVisa.getData("tax_indicator"));
	}

	@Test
	public void testTaxIndicator_Trident() throws Exception {

		mockedSettlementRecordVisa.setData("tax_amount", "0.00");
		mockedSettlementRecordVisa.setData("tax_indicator", "2");

		mockedSettlementRecordVisa.fixBadData();
		assertEquals("2", mockedSettlementRecordVisa.getData("tax_indicator"));
	}
	
	@Test
	public void testBuildFirstPresentmentRecords_SpecialConditionIndBDSFalse() throws Exception {
		setUpSpecialConditionIndicator();
		when(settlementRecordVisa.getData("special_conditions_ind")).thenReturn("8");
		assertEquals(" 8", ((FlatFileRecord) settlementRecordVisa.buildFirstPresentmentRecords().get(1)).getFieldData("special_conditions_ind"));
	}

	@Test
	public void testBuildFirstPresentmentRecords_SpecialConditionIndNullTrue() throws Exception {
		setUpSpecialConditionIndicator();
		when(settlementRecordVisa.getData("special_conditions_ind")).thenReturn(null);
		assertTrue(StringUtils.isBlank(((FlatFileRecord) settlementRecordVisa.buildFirstPresentmentRecords().get(1)).getFieldData("special_conditions_ind")));
	}

	@Test
	public void testBuildSecondPresentmentRecords_SpecialConditionIndBDSFalse() throws Exception {
		setUpSpecialConditionIndicator();
		when(settlementRecordVisa.getData("special_conditions_ind")).thenReturn("8");
		assertEquals(" 8", ((FlatFileRecord) settlementRecordVisa.buildSecondPresentmentRecords().get(1)).getFieldData("special_conditions_ind"));
	}

	@Test
	public void testBuildSecondPresentmentRecords_SpecialConditionIndNullTrue() throws Exception {
		setUpSpecialConditionIndicator();
		when(settlementRecordVisa.getData("special_conditions_ind")).thenReturn(null);
		assertTrue(StringUtils.isBlank(((FlatFileRecord) settlementRecordVisa.buildSecondPresentmentRecords().get(1)).getFieldData("special_conditions_ind")));
	}

	private void setUpSpecialConditionIndicator() {
		initialiseTCR0FlatFileRecord();
		initialiseTCR1FlatFileRecord();
		when(settlementRecordVisa.getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0)).thenReturn(flatFileRecordTCR0);
		when(settlementRecordVisa.getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR1)).thenReturn(flatFileRecordTCR1);
		when(settlementRecordVisa.getData("card_number_full")).thenReturn("4189080409331656");
	}

	private void initialiseTCR0FlatFileRecord() {
		flatFileRecordTCR0 = PowerMockito.spy(new FlatFileRecord());
		Vector<FlatFileRecordField> fields = new Vector<FlatFileRecordField>();
		fields.add(new FlatFileRecordField(1, "tran_code", 1, 2, 0, null, null));
		fields.add(new FlatFileRecordField(2, "tran_code_qualifier", 1, 32, 0, null, null));
		fields.add(new FlatFileRecordField(3, "tran_component_seq_num", 1, 8, 0, null, null));
		fields.add(new FlatFileRecordField(4, "card_number", 1, 16, 0, null, null));
		fields.add(new FlatFileRecordField(5, "card_number_ext", 1, 3, 0, null, "000"));
		fields.add(new FlatFileRecordField(6, "floor_limit_ind", 0, 6, 0, null, null));
		fields.add(new FlatFileRecordField(7, "crb_exception_file_ind", 0, 32, 0, null, null));
		fields.add(new FlatFileRecordField(8, "pcas_ind", 0, 6, 0, null, null));
		fields.add(new FlatFileRecordField(9, "reference_number", 0, 32, 0, null, null));
		fields.add(new FlatFileRecordField(10, "usage_code", 0, 1, 0, null, "1"));
		Whitebox.setInternalState(flatFileRecordTCR0, "fields", fields);
	}

	private void initialiseTCR1FlatFileRecord() {
		flatFileRecordTCR1 = PowerMockito.spy(new FlatFileRecord());
		Vector<FlatFileRecordField> fields = new Vector<FlatFileRecordField>();
		fields.add(new FlatFileRecordField(1, "tran_code", 1, 2, 0, null, null));
		fields.add(new FlatFileRecordField(2, "tran_code_qualifier", 1, 1, 0, null, null));
		fields.add(new FlatFileRecordField(3, "tran_component_seq_num", 1, 1, 0, null, "1"));
		fields.add(new FlatFileRecordField(4, "bus_format_code", 0, 1, 0, null, null));
		fields.add(new FlatFileRecordField(5, "token_assurance_level", 1, 2, 0, null, null));
		fields.add(new FlatFileRecordField(6, "reserved", 0, 9, 0, null, null));
		fields.add(new FlatFileRecordField(7, "cb_ref_num", 1, 6, 0, null, "0"));
		fields.add(new FlatFileRecordField(8, "documentation_ind", 0, 1, 0, null, null));
		fields.add(new FlatFileRecordField(9, "message_text", 0, 50, 0, null, null));
		fields.add(new FlatFileRecordField(10, "special_conditions_ind", 0, 2, 0, null, null));
		Whitebox.setInternalState(flatFileRecordTCR1, "fields", fields);
	}

}