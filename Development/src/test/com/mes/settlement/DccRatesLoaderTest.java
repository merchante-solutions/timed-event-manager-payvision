package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.sql.SQLException;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.mes.config.MesDefaults;
import com.mes.database.MesQueryHandlerList;

public class DccRatesLoaderTest {

	@Mock
	MesDefaults mesDefaultsMock;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}
	
	@Test
	public void testTrimFilename_DOS_Windows() {
		
		String filename="ccr3941_072319_094912.dat";
		String filepath="D:\\test\\pathname\\"+filename;
		
		DccRatesLoader mockLoader = Mockito.spy(DccRatesLoader.class);
		String testFilename = mockLoader.trimFilename(filepath);
		
		assertEquals(filename,testFilename);
		
	}
	
	@Test
	public void testTrimFilename_UNIX() {
		
		String filename="ccr3941_072319_094912.dat";
		String filepath="/test/pathname/"+filename;
		
		DccRatesLoader mockLoader = Mockito.spy(DccRatesLoader.class);
		String testFilename = mockLoader.trimFilename(filepath);
		
		assertEquals(filename,testFilename);
		
	}
}
