package com.mes.settlement;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mes.config.DbProperties;
import com.mes.data.MesDataSourceManager;
import com.mes.database.SQLJConnectionBase;
import com.mes.startup.EventBase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


import javax.sql.DataSource;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MesDataSourceManager.class, DbProperties.class, Connection.class,SQLJConnectionBase.class, EventBase.class})
public class SettlementDbTest {

	private static final int DEF_TYPE_CDF_AUTO_RENT_REC = 44;
	private static final long TEST_VALUE = 31234234L;
	private static final String DEF_TYPE_CDF_AUTO_DEP_STRING = "DEF_TYPE_DEPRECATED";
	private static final String DEF_TYPE_CDF_AUTO_NON_DEP_STRING = "DEF_TYPE_NONDEPRECATED";
	@SuppressWarnings("rawtypes")
	private HashMap deprecatedWay = new HashMap();
	@SuppressWarnings("rawtypes")
	private HashMap nonDeprecatedWay = new HashMap();
	@SuppressWarnings("rawtypes")
	private List deprecatedList = new ArrayList();
	@SuppressWarnings("rawtypes")
	private List nonDeprecatedList = new ArrayList();

	@Mock
	DataSource dataSource;

	@Before
	@SuppressWarnings({"deprecation"})
	public void setUp() throws Exception {
		deprecatedWay.put(DEF_TYPE_CDF_AUTO_DEP_STRING, new Integer(DEF_TYPE_CDF_AUTO_RENT_REC));
		nonDeprecatedWay.put(DEF_TYPE_CDF_AUTO_NON_DEP_STRING, DEF_TYPE_CDF_AUTO_RENT_REC);

		deprecatedList.add(new Long(TEST_VALUE));
		nonDeprecatedList.add(TEST_VALUE);


		PowerMockito.whenNew(MesDataSourceManager.class).withAnyArguments().thenReturn(null);
		Whitebox.setInternalState(MesDataSourceManager.class, "INSTANCE", PowerMockito.mock(MesDataSourceManager.class));
		PowerMockito.when(MesDataSourceManager.INSTANCE.getDataSource()).thenReturn(dataSource);
	}

	@Test
	public void testHashMap() {
		assertEquals(deprecatedWay.get(DEF_TYPE_CDF_AUTO_DEP_STRING), nonDeprecatedWay.get(DEF_TYPE_CDF_AUTO_NON_DEP_STRING));
	}

	@Test
	public void testList() {
		assertEquals(deprecatedList.get(0), nonDeprecatedList.get(0));
	}
}
