package com.mes.settlement;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.database.MeSQueryHandler;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.startup.EventBase;
import com.mes.tools.CurrencyCodeConverter;
import masthead.mastercard.clearingiso.IpmOfflineMessage;

public class SettlementRecordMCTest2 {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSetForeignExchangeFields_MissingParameters() throws Exception {
		MesDefaults mesDefaultsMock = Mockito.spy(MesDefaults.class);
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		
		record.setData("settlement_date","06/10/2020 12:04:00");
		
		record.setData("auth_date", "06/09/2020 12:04:00");
		record.setData("auth_time", "06/09/2020 12:04:00");
		
		doReturn(null).when(record).getCutoverDate();
		doReturn(null).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/10/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_BeforeCutoverDate() throws Exception {
		MesDefaults mesDefaultsMock = Mockito.spy(MesDefaults.class);
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/10/2020 12:04:00");
		
		record.setData("auth_date", "06/08/2020 12:04:00");
		record.setData("auth_time", "06/08/2020 12:04:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/10/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_DayOfCutoverDate_Before() throws Exception {
		MesDefaults mesDefaultsMock = Mockito.spy(MesDefaults.class);
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/10/2020 12:04:00");
		
		record.setData("auth_date", "06/09/2020 12:04:00");
		record.setData("auth_time", "06/09/2020 12:04:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/10/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_DayOfCutoverDate_After() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/10/2020 12:04:00");
		record.setData("batch_date","06/10/2020 12:04:00");
		record.setData("auth_date", "06/09/2020 12:06:00");
		record.setData("auth_time", "06/09/2020 12:06:00");
		record.setData("funding_currency_code","840");
	    record.setData("currency_code","036");
		
	    doReturn(sdf.parse("2020-06-10 12:04:00")).when(record).getInitialBatchDate(Mockito.any());
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/09/2020 12:06:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_AfterCutoverDate() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/15/2020 12:04:00");
		record.setData("batch_date","06/15/2020 12:04:00");
		
		record.setData("auth_date", "06/10/2020 12:06:00");
		record.setData("auth_time", "06/10/2020 12:06:00");

		record.setData("funding_currency_code","840");
	    record.setData("currency_code","036");
		
	    doReturn(sdf.parse("2020-06-15 12:04:00")).when(record).getInitialBatchDate(Mockito.any());
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/10/2020 12:06:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_LateSettlement() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/20/2020 12:04:00");
		
		record.setData("auth_date", "06/10/2020 12:06:00");
		record.setData("auth_time", "06/10/2020 12:06:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/20/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_Friday_SettleSaturday() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/20/2020 12:04:00");
		record.setData("batch_date","06/20/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");

		record.setData("funding_currency_code","840");
	    record.setData("currency_code","036");
		
	    doReturn(sdf.parse("2020-06-20 12:04:00")).when(record).getInitialBatchDate(Mockito.any());
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/12/2020 12:06:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_Credit() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/20/2020 12:04:00");
		record.setData("debit_credit_indicator", "C");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/20/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_Friday_SettleSunday() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/21/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/21/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_NotAuthLinked_Zero() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("auth_rec_id", "0");
		record.setData("settlement_date","06/21/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/21/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testSetForeignExchangeFeilds_NotAuthLinked_Empty() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("auth_rec_id", "");
		record.setData("settlement_date","06/21/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");
		
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		record.setForeignExchangeFields();
		
		assertNotNull(record.getData("fx_conv_date"));
		assertEquals("06/21/2020 12:04:00",record.getData("fx_conv_date"));
	}
	
	@Test
	public void testGetInitialBatchDate_FoundRecord() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/20/2020 12:04:00");
		record.setData("batch_date","06/20/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");

		record.setData("funding_currency_code","840");
	    record.setData("currency_code","036");
	    
	    ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		
		MesQueryHandlerResultSet mockQueryHandler = Mockito.mock(MesQueryHandlerResultSet.class);
	    MesResultSet rs = Mockito.spy(MesResultSet.class);
	    rs.add(new HashMap<>());
	    rs.get(0).put("batch_date", new java.sql.Timestamp(sdf.parse("2020-06-13 00:00:00").getTime()));

	    doReturn(rs).when(mockQueryHandler).executePreparedStatement(Mockito.eq("com.mes.settlement.SettlementRecordMC"), 
	    															 Mockito.contains("daily_detail_file_dt"), 
	    															 objArrayCaptor.capture());
	    doReturn(mockQueryHandler).when(record).getQueryHandler();
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		java.util.Date batchDate = record.getInitialBatchDate(sdf.parse("2020-06-20 00:00:00"));
		
		assertEquals(sdf.parse("2020-06-13 00:00:00"),batchDate);
	}
	
	@Test
	public void testGetInitialBatchDate_NoRecordFound() throws Exception {
		SettlementRecordMC record = Mockito.spy(getSettlementRecordMC());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		record.setData("settlement_date","06/20/2020 12:04:00");
		record.setData("batch_date","06/20/2020 12:04:00");
		
		record.setData("auth_date", "06/12/2020 12:06:00");
		record.setData("auth_time", "06/12/2020 12:06:00");

		record.setData("funding_currency_code","840");
	    record.setData("currency_code","036");
	    
	    ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();
		
		MesQueryHandlerResultSet mockQueryHandler = Mockito.mock(MesQueryHandlerResultSet.class);
	    MesResultSet rs = Mockito.spy(MesResultSet.class);

	    doReturn(rs).when(mockQueryHandler).executePreparedStatement(Mockito.eq("com.mes.settlement.SettlementRecordMC"), 
	    															 Mockito.any(), 
	    															 objArrayCaptor.capture());
	    doReturn(mockQueryHandler).when(record).getQueryHandler();
		doReturn(sdf.parse("2020-06-09 12:05:00")).when(record).getCutoverDate();
		doReturn(9).when(record).getMesDefaultsSetting(Mockito.eq(MesDefaults.MC_FX_AUTH_NUMBER_DAYS));
		
		java.util.Date batchDate = record.getInitialBatchDate(sdf.parse("2020-06-20 00:00:00"));
		
		assertEquals(sdf.parse("2020-06-20 00:00:00"),batchDate);
	}

	/**
	 * This method returns the mocked SettlementRecordMC
	 * @return SettlementRecordMC
	 */
	private SettlementRecordMC getSettlementRecordMC() {
		SettlementRecordMC record = new SettlementRecordMC();
		record.setData("rec_id", "3173104961");
		record.setData("bin_number", "514061");
		record.setData("ica_number", "5185");
		record.setData("bank_number", "3941");
		record.setData("merchant_number", "941000101136");
		record.setData("dba_name", "THAI GARDEN");
		record.setData("legal_corp_name", "THAI GARDEN");
		record.setData("dba_address", "102 SE STARK");
		record.setData("dba_city", "PORTLAND");
		record.setData("dba_state", "OR");
		record.setData("dba_zip", "97220");
		record.setData("sic_code", "5812");
		record.setData("card_acceptor_type", "3000YNNN");
		record.setData("country_code", "USA");
		record.setData("phone_number", "5038875935");
		record.setData("card_number", "515440xxxxxx6744");
		record.setData("card_number_full", "5154402715726744");
		record.setData("card_type", "MB");
		record.setData("gcms_product_id", "MVE");
		record.setData("card_program_id", "MCC");
		record.setData("product_class", "MVE");
		record.setData("cash_disbursement", "N");
		record.setData("batch_number", "151");
		record.setData("batch_id", "6140643016");
		record.setData("batch_record_id", "2624267690");
		record.setData("batch_date","02/02/2020");
		record.setData("settlement_date","02/03/2020");
		record.setData("debit_credit_indicator", "D");
		record.setData("transaction_amount", "45");
		record.setData("currency_code", "840");
		record.setData("ic_cat", "8637");
		record.setData("ic_cat_billing", "8637");
		record.setData("auth_rec_id", "12345");
		record.setData("auth_amount", "45");
		record.setData("auth_amount_total", "45");
		record.setData("auth_currency_code", "840");
		record.setData("auth_code", "T41584");
		record.setData("auth_date", "02/01/2020 12:05:00");
		record.setData("auth_time", "02/01/2020 12:05:00");
		record.setData("auth_date_local","02/01/2020 14:05:00");
		record.setData("auth_time_local","02/01/2020 14:05:00");
	    record.setData("auth_time_zone","CST");
		record.setData("auth_source_code", "7");
		record.setData("auth_response_code", "00");
		record.setData("auth_retrieval_ref_num", "904418007275");
		record.setData("auth_banknet_ref_num", "MVE000001");
		record.setData("auth_banknet_date", "0213");
		record.setData("auth_avs_response", "0");
		record.setData("trident_tran_id", "c333307156043fdd9f08d4bc6f74d99b");
		record.setData("cat_indicator", "CT6");
		record.setData("alm_code", "Z");
		record.setData("bsa_type", "2");
		record.setData("bsa_id_code", "010001");
		record.setData("ird", "V5");
		record.setData("reference_number", "25140619044037435074831");
		record.setData("acq_reference_number", "3743507483");
		record.setData("ecomm_security_level_ind", "220");
		record.setData("pos_data_code", "099950S99140");
		record.setData("processing_code", "20");
		record.setData("merchant_tax_id", "999302829");
		record.setData("tax_amount", "0");
		record.setData("pos_entry_mode", "81");
		record.setData("card_present", "N");
		record.setData("moto_ecommerce_ind", "7");
		record.setData("multiple_clearing_seq_num", "0");
		record.setData("multiple_clearing_seq_count", "0");
		record.setData("customer_service_phone", "5038875935");
		record.setData("rate_daily", "0");
		record.setData("level_iii_data_present", "N");
		record.setData("card_number_enc", "0000100000100002000000104693FCDA9415726751C0F757B88B3256");
		record.setData("test_flag", "N");
		record.setData("last_modified_date", "2019-02-19 11:56:44.0");
		record.setData("load_file_id", "49193588");
		record.setData("load_filename", "mddf3941_021319_013.dat");
		record.setData("output_file_id", "49193591");
		record.setData("output_filename", "mc_settle9999_021319_002.ipm");
		record.setData("debit_type", "0");
		record.setData("funding_currency_code", "840");
		record.setData("funding_amount", "45");
		record.setData("pos_terminal_id", "90011562");

		return record;
	}
}