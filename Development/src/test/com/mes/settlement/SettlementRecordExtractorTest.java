package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.internal.verification.VerificationModeFactory;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.constants.mesConstants;
import com.mes.data.merchant.ProcessForcedPostsService;
import com.mes.database.SQLJConnectionBase;
import com.mes.startup.EventBase;
import com.mes.support.PropertiesFile;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SettlementRecordExtractor.class, DbProperties.class, SQLJConnectionBase.class, SettlementTools.class, InterchangeAmex.class,
		PropertiesFile.class, SettlementDb.class, AmexOptBlueBinRange.class,InterchangeVisa.class})
@SuppressStaticInitializationFor("com.mes.settlement.InterchangeVisa")
public class SettlementRecordExtractorTest {

	private SettlementRecordExtractor mockSettlementRecordExtractor = null;
	private SettlementDb mockedSettlementDB = null;
	private ProcessForcedPostsService processForcedPostsService = null;
	private SettlementRecord mockSettlemetRecord = null;
	private VisakBatchData batchData = null;
	private PropertiesFile mockProps = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(SettlementRecord.class, "fixBadData"));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SettlementDb.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		PowerMockito.mockStatic(InterchangeVisa.class);

		mockSettlementRecordExtractor = PowerMockito.spy(new SettlementRecordExtractor());
		mockedSettlementDB = Mockito.mock(SettlementDb.class);

		PowerMockito.whenNew(SettlementDb.class).withAnyArguments().thenReturn(mockedSettlementDB);
		Mockito.doReturn(true).when(mockedSettlementDB).connect(true);

		processForcedPostsService = Mockito.mock(ProcessForcedPostsService.class);
		PowerMockito.whenNew(ProcessForcedPostsService.class).withAnyArguments().thenReturn(processForcedPostsService);
		batchData = getBatchDetails();

		List<Long> detailList = new ArrayList<>();
		detailList.add(1l);

		SettlementRecord settlementRecord = new SettlementRecordMC();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		mockSettlemetRecord.RecSettleRecType = 2;

		PowerMockito.when(mockedSettlementDB._loadMerchantAndBatchData(anyInt(), anyInt())).thenReturn(batchData);
		PowerMockito.when(mockedSettlementDB._loadBatchRecIds(anyInt(), anyInt())).thenReturn(detailList);
		PowerMockito.when(mockedSettlementDB._extractTran(anyInt(), Mockito.any(VisakBatchData.class), anyLong())).thenReturn(mockSettlemetRecord);

		PowerMockito.mockStatic(SettlementTools.class);
		PowerMockito.when(SettlementTools.generateReferenceNumber(mockSettlemetRecord)).thenReturn("12345");
		PowerMockito.when(SettlementTools.getIrdListMC(mockSettlemetRecord)).thenReturn(null);
	}

	/* offline transaction and CFP flag disabled for merchant */
	@Test
	public void testExtractBatchDataCFPDisabled() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}

	/* offline transaction and CFP flag enabled for merchant */
	@Test
	public void testExtractBatchDataCFPEnabled() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(true);
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertNotEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}

	/* not an offline transaction */
	@Test
	public void testExtractBatchDataWithoutCPFTxn() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("MCC000111      ");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("C");
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertNotEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}

	/* offline credit transaction and CFP flag disabled for merchant */
	@Test
	public void testExtractBatchDataWithCreditTxn() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("C");
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertNotEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}

	private VisakBatchData getBatchDetails() {
		batchData = new VisakBatchData();
		batchData.MerchantId = 941000072761l;
		batchData.AccountStatus = "B";
		batchData.BankNumber = 3941;
		batchData.BatchId = 1;
		batchData.BatchNumber = 1;
		batchData.setTestAccount(false);
		return batchData;
	}

	private void mockCPFData() {
		Vector<String> batchList = new Vector<String>(2);
		batchList.addElement(String.valueOf(mesConstants.MBS_BT_VITAL) + "," + "1");
		Mockito.doReturn(batchList).when(mockedSettlementDB)._loadBatchesToExtract(anyInt(), anyString());

		PowerMockito.when(mockSettlemetRecord.getCardTypeEnhanced()).thenReturn("MC");
		PowerMockito.when(mockSettlemetRecord.isCardNumberValid()).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.isCurrencyCodeAcceptable(batchData)).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.isMerchantNumberValid()).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.getData("account_status")).thenReturn("B");
		PowerMockito.when(mockSettlemetRecord.getLong("rec_id")).thenReturn(12345l);
		Mockito.doNothing().when(mockSettlemetRecord).fixBadData();
	}

	@Test
	public void testExtractBatchDataForVpaymentProductCodeVP() throws Exception {
		mockvPaymentBatchData();
		PowerMockito.mockStatic(AmexOptBlueBinRange.class);
		when(AmexOptBlueBinRange.isPrePaidCard(anyString())).thenCallRealMethod();
		when(AmexOptBlueBinRange.isVpayment(anyString())).thenCallRealMethod();

		PowerMockito.when(mockedSettlementDB._getOptBlueProductCode(Mockito.any(String.class))).thenReturn("VP");

		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_PG, "mddf3941_022819_000.dat");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertEquals("9153", (String) settlementRecord.getValue().getData("ic_cat"));
		PowerMockito.verifyStatic(AmexOptBlueBinRange.class, VerificationModeFactory.times(1));
		AmexOptBlueBinRange.isVpayment(anyString());

	}

	@Test
	public void testExtractBatchDataForVpaymentProductCodeRP() throws Exception {
		mockvPaymentBatchData();
		PowerMockito.mockStatic(AmexOptBlueBinRange.class);
		when(AmexOptBlueBinRange.isPrePaidCard(anyString())).thenCallRealMethod();
		when(AmexOptBlueBinRange.isVpayment(anyString())).thenCallRealMethod();

		PowerMockito.when(mockedSettlementDB._getOptBlueProductCode(Mockito.any(String.class))).thenReturn("RP");

		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_PG, "mddf3941_022819_000.dat");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertEquals("", (String) settlementRecord.getValue().getData("ic_cat"));
		PowerMockito.verifyStatic(AmexOptBlueBinRange.class, VerificationModeFactory.times(0));
		AmexOptBlueBinRange.isVpayment(anyString());

	}

	private void mockvPaymentBatchData() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordAmex();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		mockSettlemetRecord.RecSettleRecType = 3;
		PowerMockito.when(mockedSettlementDB._extractTran(anyInt(), Mockito.any(VisakBatchData.class), anyLong())).thenReturn(mockSettlemetRecord);
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		PowerMockito.when(mockSettlemetRecord.getData("amex_se_number")).thenReturn("1101618122");
		PowerMockito.when(mockSettlemetRecord.getData("card_number_full")).thenReturn("555555");

		mockProps = PowerMockito.mock(PropertiesFile.class);
		PowerMockito.when(mockProps.getString("se_list_3943")).thenReturn("1101618866,1101618809,1101618874,1101618841,1101618825,1101618775,1101618791,1101618817,1101618833,1101618858,1104346457,1104346440,2100687664");
		PowerMockito.when(mockProps.getString("se_list_9999")).thenReturn("1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346465,1104346473,2100687672");

		PowerMockito.spy(PropertiesFile.class);
		PowerMockito.whenNew(PropertiesFile.class).withAnyArguments().thenReturn(mockProps);

		IcInfo mockIcInfo = PowerMockito.spy(new IcInfo());
		mockIcInfo.setIcCode("9153");

		Mockito.doReturn(mockIcInfo).when(mockedSettlementDB)._getBestIcInfo(Mockito.any(SettlementRecord.class), Mockito.any(String.class), Mockito.any(Integer.class), Mockito.any(Date.class), Mockito.eq("...OBV-1101618122"), Mockito.any(String.class));

		Vector<String> batchList = new Vector<String>(2);
		batchList.addElement(String.valueOf(mesConstants.MBS_BT_PG) + "," + "1");
		Mockito.doReturn(batchList).when(mockedSettlementDB)._loadBatchesToExtract(anyInt(), anyString());

		PowerMockito.when(mockSettlemetRecord.getCardTypeEnhanced()).thenReturn("AM");
		PowerMockito.when(mockSettlemetRecord.isCardNumberValid()).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.isCurrencyCodeAcceptable(batchData)).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.isMerchantNumberValid()).thenReturn(true);
		PowerMockito.when(mockSettlemetRecord.getData("account_status")).thenReturn("B");
		PowerMockito.when(mockSettlemetRecord.getLong("rec_id")).thenReturn(12345l);
		Mockito.doNothing().when(mockSettlemetRecord).fixBadData();
	}

	// card type PL
	@Test
	public void testExtractDetailRecords_PLCardTypeTrue() throws Exception {

		mockCPFData();
		PowerMockito.when(mockSettlemetRecord.getCardTypeEnhanced()).thenReturn("PL");
		PowerMockito.when(mockSettlemetRecord.getLong("rec_id")).thenReturn(0l);
		mockSettlemetRecord.RecSettleRecType = 0;

		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);

		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "256s3941_021319_001.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertNotEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}

	// card type non PL
	@Test
	public void testExtractDetailRecords_PLCardTypeFalse() throws Exception {

		mockCPFData();
		PowerMockito.when(mockSettlemetRecord.getCardTypeEnhanced()).thenReturn("MC");
		PowerMockito.when(mockSettlemetRecord.getLong("rec_id")).thenReturn(0l);
		mockSettlemetRecord.RecSettleRecType = 0;

		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);

		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "256s3941_021319_001.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertEquals("0199", (String) settlementRecord.getValue().getData("reject_reason"));
	}
	
	@Test
	@SuppressWarnings({"deprecation", "rawtypes"})
	public void testextractBatchData() {
		List deprecatedList = new ArrayList();
		List nonDeprecatedList = new ArrayList();
		long value = 87998L;
		
		deprecatedList.add(new Long(value));
		nonDeprecatedList.add(value);
		
		assertEquals(deprecatedList.get(0), nonDeprecatedList.get(0));
	}
	
	@Test
	public void testExtractBatchData_InternalRejectTrue() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(false);
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertRejectRecord(settlementRecord.capture());
		assertEquals("N", (String) settlementRecord.getValue().getData("external_reject"));
	}
	
	@Test
	public void testExtractBatchData_InternalRejectFalse() throws Exception {
		mockCPFData();
		when(processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(batchData.getMerchantId()))).thenReturn(true);
		mockSettlementRecordExtractor.extractDetailRecords(mesConstants.MBS_BT_VITAL, "cdf3941_1214_D1.dat");
		PowerMockito.when(mockSettlemetRecord.getData("debit_credit_indicator")).thenReturn("D");
		ArgumentCaptor<SettlementRecord> settlementRecord = ArgumentCaptor.forClass(SettlementRecord.class);
		verify(mockedSettlementDB)._insertSettlementRecord(settlementRecord.capture());
		assertEquals("", (String) settlementRecord.getValue().getData("external_reject"));
	}
}
