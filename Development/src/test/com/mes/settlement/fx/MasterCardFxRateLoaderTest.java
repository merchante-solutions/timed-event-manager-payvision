package com.mes.settlement.fx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.config.MesDefaults;
import com.mes.database.MesQueryHandlerList;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.settlement.DccRatesLoader;

public class MasterCardFxRateLoaderTest {

	@Mock
	MesDefaults mesDefaultsMock;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}
	
	@Test
	public void testCleanData_EffectiveDate() throws SQLException {
		String tableName = "mc_settlement_dcc";
		Calendar effDate = Calendar.getInstance();
		effDate.roll(Calendar.DATE, -1);
		
		MesQueryHandlerList mockQueryHandler = Mockito.mock(MesQueryHandlerList.class);
		when(mockQueryHandler.executePreparedUpdate(Mockito.contains(tableName), Mockito.contains(tableName), Mockito.any())).thenReturn(10);
		
		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(mockQueryHandler).when(mockLoader).getMeSQueryHandler();
		
		mockLoader.cleanData(new java.sql.Date(effDate.getTime().getTime()));
	}

	@Test
	public void testGetEffectiveDateMC_BeforeCutover() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");
		Calendar testCalendar = Calendar.getInstance();
		testCalendar.set(2019, Calendar.JULY, 24, 0, 0, 0);
		testCalendar.set(Calendar.MILLISECOND, 0);

		String rawData = "H201907231643021                                                                                                             ";
		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();

		List<Calendar> effDays = mockLoader.getEffectiveDates(rawData);

		assertNotNull(effDays);
		assertEquals(1, effDays.size());
		assertEquals(sdf.format(testCalendar.getTime()), sdf.format(effDays.get(0).getTime()));
	}

	@Test
	public void testGetEffectiveDateMC_BeforeCutover_Saturday() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");
		Calendar testCalendar = Calendar.getInstance();
		testCalendar.set(2020, Calendar.JUNE, 7, 0, 0, 0);
		testCalendar.set(Calendar.MILLISECOND, 0);

		Calendar testCalendar2 = Calendar.getInstance();
		testCalendar2.set(2020, Calendar.JUNE, 8, 0, 0, 0);
		testCalendar2.set(Calendar.MILLISECOND, 0);

		String rawData = "H202006061643021                                                                                                             ";
		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();

		List<Calendar> effDays = mockLoader.getEffectiveDates(rawData);

		assertNotNull(effDays);
		assertEquals(2, effDays.size());
		assertEquals(sdf.format(testCalendar.getTime()), sdf.format(effDays.get(0).getTime()));
		assertEquals(sdf.format(testCalendar2.getTime()), sdf.format(effDays.get(1).getTime()));
	}

	@Test
	public void testGetEffectiveDateMC_CutoverNextDay() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		Calendar testCalendar = Calendar.getInstance();
		testCalendar.set(2020, Calendar.JUNE, 9, 0, 0, 0);
		testCalendar.set(Calendar.MILLISECOND, 0);

		String rawData = "H202006081643021                                                                                                            ";

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		List<Calendar> effDays = mockLoader.getEffectiveDates(rawData);

		assertNotNull(effDays);
		assertEquals(1, effDays.size());
		assertEquals(sdf.format(testCalendar.getTime()), sdf.format(effDays.get(0).getTime()));
	}

	@Test
	public void testGetEffectiveDateMC_CutoverDay() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		Calendar testCalendar = Calendar.getInstance();
		testCalendar.set(2020, Calendar.JUNE, 9, 12, 5, 0);
		testCalendar.set(Calendar.MILLISECOND, 0);

		String rawData = "H202006091643021                                                                                                            ";

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		List<Calendar> effDays = mockLoader.getEffectiveDates(rawData);

		assertNotNull(effDays);
		assertEquals(1, effDays.size());
		assertEquals(sdf.format(testCalendar.getTime()), sdf.format(effDays.get(0).getTime()));
	}

	@Test
	public void testGetEffectiveDateMC_AfterCutover_Saturday() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		Calendar testCalendar = Calendar.getInstance();
		testCalendar.set(2020, Calendar.JUNE, 13, 12, 5, 0);
		testCalendar.set(Calendar.MILLISECOND, 0);

		Calendar testCalendar2 = Calendar.getInstance();
		testCalendar2.set(2020, Calendar.JUNE, 14, 12, 5, 0);
		testCalendar2.set(Calendar.MILLISECOND, 0);

		String rawData = "H202006131643021                                                                                                            ";

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		List<Calendar> effDays = mockLoader.getEffectiveDates(rawData);

		assertNotNull(effDays);
		assertEquals(2, effDays.size());
		assertEquals(sdf.format(testCalendar.getTime()), sdf.format(effDays.get(0).getTime()));
		assertEquals(sdf.format(testCalendar2.getTime()), sdf.format(effDays.get(1).getTime()));
	}

	@Test
	public void testLoadMasterCardRates_SaturdayFile_BeforeCutover() throws Exception {
		String filename = "Testing/ccr3941_072319_saturday.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"), Mockito.any()))
											   .thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"),
											   objArrayCaptor.capture())).thenReturn(1);

		mockLoader.loadFXRates(filename);

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(44, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();
		assertEquals("008", argumentList.get(1));
		assertEquals(sdf.parse("2020-01-26 00:00:00"), (Date) argumentList.get(4));
		assertEquals(Double.valueOf(109.447631), argumentList.get(5));
		assertEquals(Double.valueOf(109.502369), argumentList.get(6));
		assertEquals("ccr3941_072319_saturday.dat", argumentList.get(7));
		assertEquals("2020-01-26 00:00:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-01-26 23:59:59", sdf.format((Date) argumentList.get(10)));

		assertEquals("008", argumentList.get(12));
		assertEquals("2020-01-27 00:00:00", sdf.format((Date) argumentList.get(15)));
		assertEquals(Double.valueOf(109.447631), argumentList.get(16));
		assertEquals(Double.valueOf(109.502369), argumentList.get(17));
		assertEquals("ccr3941_072319_saturday.dat", argumentList.get(18));
		assertEquals("2020-01-27 00:00:00", sdf.format((Date) argumentList.get(20)));
		assertEquals("2020-01-27 23:59:59", sdf.format((Date) argumentList.get(21)));
	}

	@Test
	public void testLoadMasterCardRates_WeekdayFile_BeforeCutover() throws Exception {
		String filename = "Testing/ccr3941_072319_weekday.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), Mockito.contains("mc_settlement_dcc"), Mockito.any()))
				.thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), Mockito.contains("mc_settlement_dcc"),
				objArrayCaptor.capture())).thenReturn(1);

		mockLoader.loadFXRates(filename);

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(22, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();
		assertEquals("008", argumentList.get(1));
		assertEquals("2020-01-30 00:00:00", sdf.format((Date) argumentList.get(4)));
		assertEquals(Double.valueOf(109.447631), argumentList.get(5));
		assertEquals(Double.valueOf(109.502369), argumentList.get(6));
		assertEquals("ccr3941_072319_weekday.dat", argumentList.get(7));
		assertEquals("2020-01-30 00:00:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-01-30 23:59:59", sdf.format((Date) argumentList.get(10)));
	}

	@Test
	public void testLoadMasterCardRates_WeekdayFile_DayBeforeCutover() throws Exception {
		String filename = "Testing/ccr3941_weekday_after_fx_daybefore.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), Mockito.contains("mc_settlement_dcc"), Mockito.any()))
				.thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), Mockito.contains("mc_settlement_dcc"),
				objArrayCaptor.capture())).thenReturn(1);

		mockLoader.loadFXRates(filename);
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(22, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();

		assertEquals("008", argumentList.get(1));
		assertEquals("2020-06-09 00:00:00", sdf.format((Date) argumentList.get(4)));
		assertEquals(Double.valueOf(109.447631), argumentList.get(5));
		assertEquals(Double.valueOf(109.502369), argumentList.get(6));
		assertEquals("ccr3941_weekday_after_fx_daybefore.dat", argumentList.get(7));
		assertEquals("2020-06-09 00:00:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-06-09 12:04:59", sdf.format((Date) argumentList.get(10)));

		assertEquals("012", argumentList.get(12));
		assertEquals("2020-06-09 00:00:00", sdf.format((Date) argumentList.get(15)));
		assertEquals(Double.valueOf(119.561443), argumentList.get(16));
		assertEquals(Double.valueOf(119.5734), argumentList.get(17));
		assertEquals("ccr3941_weekday_after_fx_daybefore.dat", argumentList.get(18));
		assertEquals("2020-06-09 00:00:00", sdf.format((Date) argumentList.get(20)));
		assertEquals("2020-06-09 12:04:59", sdf.format((Date) argumentList.get(21)));
	}

	@Test
	public void testLoadMasterCardRates_Cutover() throws Exception {
		String filename = "Testing/ccr3941_fx_cutover_day.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"), Mockito.any()))
											   .thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"),
											   objArrayCaptor.capture())).thenReturn(1);

		mockLoader.loadFXRates(filename);
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(22, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();

		assertEquals("008", argumentList.get(1));
		assertEquals("2020-06-09 12:05:00", sdf.format((Date) argumentList.get(4)));
		assertEquals(Double.valueOf(109.447631), argumentList.get(5));
		assertEquals(Double.valueOf(109.502369), argumentList.get(6));
		assertEquals("ccr3941_fx_cutover_day.dat", argumentList.get(7));
		assertEquals("2020-06-09 12:05:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-06-10 12:04:59", sdf.format((Date) argumentList.get(10)));

		assertEquals("012", argumentList.get(12));
		assertEquals("2020-06-09 12:05:00", sdf.format((Date) argumentList.get(15)));
		assertEquals(Double.valueOf(119.561443), argumentList.get(16));
		assertEquals(Double.valueOf(119.5734), argumentList.get(17));
		assertEquals("ccr3941_fx_cutover_day.dat", argumentList.get(18));
		assertEquals("2020-06-09 12:05:00", sdf.format((Date) argumentList.get(20)));
		assertEquals("2020-06-10 12:04:59", sdf.format((Date) argumentList.get(21)));
	}

	@Test
	public void testLoadMasterCardRates_SaturdayFile_AfterCutover() throws Exception {
		String filename = "Testing/ccr3941_saturday_after_fx_cutover.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"), Mockito.any()))
											   .thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"),
											   objArrayCaptor.capture())).thenReturn(1);
		mockLoader.loadFXRates(filename);

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(44, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();
		assertEquals("008", argumentList.get(1));
		assertEquals("2020-06-13 12:05:00", sdf.format((Date) argumentList.get(4)));
		assertEquals("2020-06-13 12:05:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-06-14 12:04:59", sdf.format((Date) argumentList.get(10)));

		assertEquals("2020-06-14 12:05:00", sdf.format((Date) argumentList.get(15)));
		assertEquals("2020-06-14 12:05:00", sdf.format((Date) argumentList.get(20)));
		assertEquals("2020-06-15 12:04:59", sdf.format((Date) argumentList.get(21)));
	}

	@Test
	public void testLoadMasterCardRates_WeekdayFile_AfterCutover() throws Exception {
		String filename = "Testing/ccr3941_weekday_after_fx_cutover.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		MasterCardFxRateLoader mockLoader = Mockito.spy(MasterCardFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(cutoverDate).when(mockLoader).getCutoverDate();
		doReturn(getMCFlatFile()).when(mockLoader).getFlatFileRecord(MasterCardFxRateLoader.RecordType.MC_T057_DETAIL);

		doReturn(localQHMock).when(mockLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"), Mockito.any()))
											   .thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("3com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("mc_settlement_dcc"),
											   objArrayCaptor.capture())).thenReturn(1);
		mockLoader.loadFXRates(filename);

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(22, objArrayCaptor.getAllValues().size()); // two rows inserted from file

		List argumentList = objArrayCaptor.getAllValues();
		assertEquals("008", argumentList.get(1));
		assertEquals("2020-06-10 12:05:00", sdf.format((Date) argumentList.get(4)));
		assertEquals("2020-06-10 12:05:00", sdf.format((Date) argumentList.get(9)));
		assertEquals("2020-06-11 12:04:59", sdf.format((Date) argumentList.get(10)));

	}

	private FlatFileRecord getMCFlatFile() {
		FlatFileRecord ffr = new FlatFileRecord();
		Vector fieldVector = new Vector();
		fieldVector.add(new FlatFileRecordField(1, "rec_type", 0, 1, 0, null, null));
		fieldVector.add(new FlatFileRecordField(2, "source_currency_code", 1, 3, 0, null, null));
		fieldVector.add(new FlatFileRecordField(3, "base_currency_code", 1, 3, 0, null, null));
		fieldVector.add(new FlatFileRecordField(4, "source_currency_exponent", 1, 1, 0, null, null));
		fieldVector.add(new FlatFileRecordField(5, "rate_class", 0, 1, 0, null, null));
		fieldVector.add(new FlatFileRecordField(6, "rate_format_indicator", 0, 1, 0, null, null));
		fieldVector.add(new FlatFileRecordField(7, "buy_rate", 1, 15, 7, null, null));
		fieldVector.add(new FlatFileRecordField(8, "mid_rate", 1, 15, 7, null, null));
		fieldVector.add(new FlatFileRecordField(9, "sell_rate", 1, 15, 7, null, null));
		fieldVector.add(new FlatFileRecordField(10, "reserved", 1, 15, 0, null, null));
		fieldVector.add(new FlatFileRecordField(11, "filler", 0, 55, 0, null, null));
		Whitebox.setInternalState(ffr, "fields", fieldVector);
		return ffr;
	}
}
