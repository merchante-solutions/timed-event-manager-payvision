package com.mes.settlement.fx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.config.MesDefaults;
import com.mes.database.MesQueryHandlerList;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.settlement.DccRatesLoader;
import com.mes.settlement.DccRatesLoader.RecordType;

public class VisaFxRateLoaderTest {

	@Mock
	MesDefaults mesDefaultsMock;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	@Test
	public void testCleanData_fileName() throws SQLException {

		String tableName = "visa_settlement_dcc";
		String filename = "ccr3941_072319_094912-072319-094924.3127.dat";
		
		MesQueryHandlerList mockQueryHandler = Mockito.mock(MesQueryHandlerList.class);
		when(mockQueryHandler.executePreparedUpdate(Mockito.anyString(), Mockito.contains(tableName), Mockito.any())).thenReturn(10);
		
		VisaFxRateLoader mockLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(mockQueryHandler).when(mockLoader).getMeSQueryHandler();
		
		mockLoader.cleanData(filename);
	}
	
	@Test
	public void testGetEffectiveDates_Weekday() throws Exception {

		Calendar weekday = Calendar.getInstance();
		weekday.set(2020, Calendar.JANUARY, 30, 0, 0, 0);

		VisaCurrencyEntry entry = new VisaCurrencyEntry();
		entry.setEffectiveDate(new java.sql.Date(weekday.getTime().getTime()));

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		List<Calendar> effectiveDates = rateLoader.getEffectiveDates(entry);
		assertNotNull(effectiveDates);
		assertFalse(effectiveDates.isEmpty());
		assertEquals(1, effectiveDates.size());
		assertEquals(weekday, effectiveDates.get(0));
	}

	@Test
	public void testGetEffectiveDates_Weekend() throws Exception {
		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.FEBRUARY, 1, 0, 0, 0);

		VisaCurrencyEntry entry = new VisaCurrencyEntry();
		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		List<Calendar> effectiveDates = rateLoader.getEffectiveDates(entry);
		assertNotNull(effectiveDates);
		assertFalse(effectiveDates.isEmpty());
		assertEquals(3, effectiveDates.size());
		assertEquals(saturday, effectiveDates.get(0));

		saturday.add(Calendar.DAY_OF_MONTH, 1);
		assertEquals(saturday, effectiveDates.get(1));

		saturday.add(Calendar.DAY_OF_MONTH, 1);
		assertEquals(saturday, effectiveDates.get(2));
	}

	@Test
	public void testGetEffectiveDates_LongWeekend() throws Exception {
		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.FEBRUARY, 1, 0, 0, 0);

		VisaCurrencyEntry entry = new VisaCurrencyEntry();
		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(true)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		List<Calendar> effectiveDates = rateLoader.getEffectiveDates(entry);
		assertNotNull(effectiveDates);
		assertFalse(effectiveDates.isEmpty());
		assertEquals(4, effectiveDates.size());
		assertEquals(saturday, effectiveDates.get(0));

		saturday.add(Calendar.DAY_OF_MONTH, 1);
		assertEquals(saturday, effectiveDates.get(1));

		saturday.add(Calendar.DAY_OF_MONTH, 1);
		assertEquals(saturday, effectiveDates.get(2));

		saturday.add(Calendar.DAY_OF_MONTH, 1);
		assertEquals(saturday, effectiveDates.get(3));
	}

	@Test
	public void testGetLoopCount_Weekday() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.JANUARY, 30, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(1), count);
	}

	@Test
	public void testGetLoopCount_Weekday_Holiday() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.JANUARY, 30, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(true)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(2), count);
	}

	@Test
	public void testGetLoopCount_Friday() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.JANUARY, 31, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(1), count);
	}

	@Test
	public void testGetLoopCount_Friday_Longweekend() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.JANUARY, 31, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(true)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(4), count);
	}

	@Test
	public void testGetLoopCount_Saturday() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.FEBRUARY, 1, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));
		
		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(3), count);
	}

	@Test
	public void testGetLoopCount_Saturday_Longweekend() throws SQLException {
		VisaCurrencyEntry entry = new VisaCurrencyEntry();

		Calendar saturday = Calendar.getInstance();
		saturday.set(2020, Calendar.FEBRUARY, 1, 0, 0, 0);

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		doReturn(Boolean.valueOf(true)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));
		
		entry.setEffectiveDate(new java.sql.Date(saturday.getTime().getTime()));
		Integer count = rateLoader.getLoopCount(entry);

		assertEquals(Integer.valueOf(4), count);
	}

	@Test
	public void testLoadFXRates() throws Exception {
		String filename = "Testing/vs_dcc3941_success.dat";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		java.util.Date cutoverDate = sdf.parse("2020-06-09 00:00:00:000");

		ArgumentCaptor<Object[]> objArrayCaptor = ArgumentCaptor.forClass(Object[].class);
		objArrayCaptor.getAllValues().clear();

		VisaFxRateLoader rateLoader = Mockito.spy(VisaFxRateLoader.class);
		MesQueryHandlerList localQHMock = Mockito.mock(MesQueryHandlerList.class);

		doReturn(getFlatFile(RecordType.VISA_TC56_TCR0)).when(rateLoader).getFlatFileRecord(RecordType.VISA_TC56_TCR0);
		doReturn(getFlatFile(RecordType.VISA_TC56_TCR1)).when(rateLoader).getFlatFileRecord(RecordType.VISA_TC56_TCR1);
		doReturn(Boolean.valueOf(false)).when(rateLoader).isHoliday(Mockito.any(java.util.Date.class));

		doReturn(localQHMock).when(rateLoader).getMeSQueryHandler();

		when(localQHMock.executePreparedUpdate(Mockito.contains("1com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("visa_settlement_dcc"), Mockito.any()))
											  .thenReturn(Integer.valueOf(1));

		when(localQHMock.executePreparedUpdate(Mockito.eq("5com.mes.settlement.DccRatesLoader"), 
											   Mockito.contains("visa_settlement_dcc"),
											   objArrayCaptor.capture())).thenReturn(1);
		rateLoader.loadFXRates(filename);

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		assertEquals(110, objArrayCaptor.getAllValues().size());
		List arguments = objArrayCaptor.getAllValues();
		assertEquals("A", arguments.get(0));
		assertEquals("004", arguments.get(1));
		assertEquals("840", arguments.get(2));
		assertEquals("2020-01-21 00:00:00", sdf.format((Date) arguments.get(3)));
		assertEquals(Double.valueOf(0.0130429), arguments.get(4));
		assertEquals(Double.valueOf(0.0130429), arguments.get(5));
		assertEquals("vs_dcc3941_success.dat", arguments.get(6));
		assertEquals("2020-01-21 00:00:00", sdf.format((Date) arguments.get(8)));
		assertEquals("2020-01-21 23:59:59", sdf.format((Date) arguments.get(9)));

		assertEquals("A", arguments.get(20));
		assertEquals("008", arguments.get(21));
		assertEquals("840", arguments.get(22));
		assertEquals("2020-01-21 00:00:00", sdf.format((Date) arguments.get(23)));
		assertEquals(Double.valueOf(0.00909753), arguments.get(24));
		assertEquals(Double.valueOf(0.00903506), arguments.get(25));
		assertEquals("vs_dcc3941_success.dat", arguments.get(26));
		assertEquals("2020-01-21 00:00:00", sdf.format((Date) arguments.get(28)));
		assertEquals("2020-01-21 23:59:59", sdf.format((Date) arguments.get(29)));
	}

	private FlatFileRecord getFlatFile(RecordType recordType) {
		FlatFileRecord ffr = new FlatFileRecord();
		Vector fieldVector = new Vector();
		if (recordType == RecordType.VISA_TC56_TCR0) {
			fieldVector.add(new FlatFileRecordField(1, "tran_code", 0, 2, 0, null, null));
			fieldVector.add(new FlatFileRecordField(2, "tran_code_qualifier", 0, 1, 0, null, null));
			fieldVector.add(new FlatFileRecordField(3, "tran_component_seq_num", 0, 1, 0, null, null));
			fieldVector.add(new FlatFileRecordField(4, "dest_bin", 0, 6, 0, null, null));
			fieldVector.add(new FlatFileRecordField(5, "source_bin", 0, 6, 0, null, null));
			fieldVector.add(new FlatFileRecordField(6, "currency_entry_1", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(7, "currency_entry_2", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(8, "currency_entry_3", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(9, "currency_entry_4", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(10, "currency_entry_5", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(11, "reserved", 0, 17, 0, null, null));
		}
		else if (recordType == RecordType.VISA_TC56_TCR1) {
			fieldVector.add(new FlatFileRecordField(1, "tran_code", 0, 2, 0, null, null));
			fieldVector.add(new FlatFileRecordField(2, "tran_code_qualifier", 0, 1, 0, null, null));
			fieldVector.add(new FlatFileRecordField(3, "tran_component_seq_num", 0, 1, 0, null, null));
			fieldVector.add(new FlatFileRecordField(4, "currency_entry_6", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(5, "currency_entry_7", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(6, "currency_entry_8", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(7, "currency_entry_9", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(8, "currency_entry_10", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(9, "currency_entry_11", 0, 27, 0, null, null));
			fieldVector.add(new FlatFileRecordField(10, "reserved", 0, 2, 0, null, null));
		}
		Whitebox.setInternalState(ffr, "fields", fieldVector);
		return ffr;
	}
}
