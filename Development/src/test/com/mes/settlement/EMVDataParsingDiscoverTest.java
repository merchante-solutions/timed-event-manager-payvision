package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.when;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.startup.EventBase;
import com.mes.utils.TLVUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SettlementRecordDiscover.class, DbProperties.class, SQLJConnectionBase.class, EventBase.class, FlatFileRecord.class, TLVUtil.class})
public class EMVDataParsingDiscoverTest {

	private Map<Integer, Integer> tlvDataMap = null;
	private static Map<Integer, String> emvDataSample = null;

	private static final int tag_91 = 0x91;
	private static final int tag_84 = 0x84;
	private static final int tag_95 = 0x95;
	private static final int tag_9F33 = 0x9F33;
	private static final int tag_9F37 = 0x9F37;
	private static final int tag_9F34 = 0x9F34;
	private static final int tag_9F10 = 0x9F10;
	private static final int tag_9F5B = 0x9F5B;
	private static final int tag_9F09 = 0x9F09;
	private static final int tag_9F06 = 0x9F06;
	private static final int tag_9F1E = 0x9F1E;

	private SettlementRecordDiscover mockSettlementRecDiscover;
	private FlatFileRecord mockFlatFileRec;
 
	@BeforeClass
	public static void setupClass() {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setup() {
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(Mockito.anyString())).thenReturn(true);
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(FlatFileRecord.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.method(EventBase.class, "logError", String.class, String.class));

		tlvDataMap = new HashMap<>();

		mockSettlementRecDiscover = PowerMockito.spy(new SettlementRecordDiscover());
 
		doReturn("6011000000000004").when(mockSettlementRecDiscover).getCardNumberFull();
		doReturn(false).when(mockSettlementRecDiscover).isFieldBlank("icc_data");
		mockSettlementRecDiscover.isEmvEnabled = true;

		fillVector_Fields();
		emvSampleDataFill();

	}

	@Test
	public void testSettlementRecordDiscover_BuildFirstPresentmentRecords_AN_WithOutSpaces_Fields() throws Exception {

		doReturn(emvDataSample.get(11)).when(mockSettlementRecDiscover).getData("icc_data");
		doReturn(emvDataSample.get(9)).when(mockSettlementRecDiscover).getData("icc_data_2");

		List<?> transactionRecords = mockSettlementRecDiscover.buildFirstPresentmentRecords();

		assertEquals("00000000000097D5658072E091C30032", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("issuer_authentication_data"));
		assertEquals("00000000001234567890ABCDEFABCDEF", ((FlatFileRecord) transactionRecords.get(2)).getFieldData("issuer_application_data_2"));

	}

	@Test
	public void testSettlementRecordDiscover_BuildFirstPresentmentRecords_AN_Spaces_Fields() throws Exception {

		doReturn(emvDataSample.get(5)).when(mockSettlementRecDiscover).getData("icc_data");
		doReturn(emvDataSample.get(2)).when(mockSettlementRecDiscover).getData("icc_data_2");

		List<?> transactionRecords = mockSettlementRecDiscover.buildFirstPresentmentRecords();

		assertEquals("     000000000000105A08000000000", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("issuer_application_data_1"));
		assertEquals("    C8", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("terminal_capabilities"));
		assertEquals("     08000", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("terminal_verification_results"));
		assertEquals("            97D5658072E091C30032", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("issuer_authentication_data"));
		assertEquals("  0A66B3", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("unpredictable_number"));
		assertEquals("                  A0000001523010", ((FlatFileRecord) transactionRecords.get(2)).getFieldData("dedicated_file_name"));
		assertEquals("    00", ((FlatFileRecord) transactionRecords.get(1)).getFieldData("cardholder_verification_method_results"));

	}

	@Test(expected = IllegalArgumentException.class)
	public void testSettlementRecordDiscover_BuildFirstPresentmentRecords_NonHexData() throws Exception {
		doReturn(emvDataSample.get(10)).when(mockSettlementRecDiscover).getData("icc_data");
		mockSettlementRecDiscover.buildFirstPresentmentRecords();
	}

	@Test
	public void testTLVUtil_IsHexStringWithSpaceRegexpression() {

		assertEquals(true, emvDataSample.get(1).matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(true, emvDataSample.get(2).matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(true, "ABCDEF1234567890".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(true, "abcdef1234567890".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(true, " ABC DEF123 4567 890 ".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));

		assertEquals(false, "PABC DEF123 4567 890 ".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(false, "PA@BC DEF123 4567 890 ".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(false, "PA%BC!DEF123 4567 890 ".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
		assertEquals(false, "PA%BC!DE]123 4567 abc ".matches(TLVUtil.IS_HEX_STRING_WITH_SPACE));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTLVUtil_HexStringToByteArray_InvalidData() throws Exception {

		assertEquals("", TLVUtil.hexStringToByteArray(emvDataSample.get(7)));
	}

	@Test
	public void testTLVUtil_HexStringToByteArray_ValidData() {

		assertEquals(true, Arrays.equals(TLVUtil.hexStringToByteArray(emvDataSample.get(2)), new byte[] {-124, 16, -17, -17, -17, -17, -17, -17, -17, -17, -17,
				-96, 0, 0, 1, 82, 48, 16, -97, 30, 4, 0, 0, 0, 0}));
		assertEquals(true, Arrays.equals(TLVUtil.hexStringToByteArray("1234567890ABCDEF"), new byte[] {18, 52, 86, 120, -112, -85, -51, -17}));
	}

	@Test
	public void testTLVUtil_getEMVData() throws Exception {

		assertEquals(tag_84, TLVUtil.getEmvTag(emvDataSample.get(2), emvDataSample.get(2).indexOf(' '), 2));
		assertEquals(tag_91, TLVUtil.getEmvTag(emvDataSample.get(4), emvDataSample.get(4).indexOf(' '), 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTLVUtil_TlvSpaceCalculation_SpaceRightPadded() throws Exception {

		tlvDataMap = TLVUtil.tlvSpaceCalculation(emvDataSample.get(6));
		assertEquals(12, (int) tlvDataMap.get(tag_91));
		assertEquals(18, (int) tlvDataMap.get(tag_84));
		assertEquals(5, (int) tlvDataMap.get(tag_95));
	}

	@Test
	public void testTLVUtil_TlvSpaceCalculation_SpaceLeftPadded() throws Exception {
		tlvDataMap = TLVUtil.tlvSpaceCalculation(emvDataSample.get(5));

		assertEquals(12, (int) tlvDataMap.get(tag_91));
		assertEquals(18, (int) tlvDataMap.get(tag_84));
		assertEquals(5, (int) tlvDataMap.get(tag_95));
		assertEquals(4, (int) tlvDataMap.get(tag_9F33));
		assertEquals(2, (int) tlvDataMap.get(tag_9F37));
		assertEquals(4, (int) tlvDataMap.get(tag_9F34));
		assertEquals(5, (int) tlvDataMap.get(tag_9F10));
		assertEquals(4, (int) tlvDataMap.get(tag_9F1E));

		assertEquals(null, tlvDataMap.get(tag_9F5B));
		assertEquals(null, tlvDataMap.get(tag_9F09));
		assertEquals(null, tlvDataMap.get(tag_9F06));
	}

	@Test
	public void testTLVUtil_TlvSpaceCalculation() throws Exception {

		tlvDataMap = TLVUtil.tlvSpaceCalculation(emvDataSample.get(8));
		assertEquals(12, (int) tlvDataMap.get(tag_91));
		assertEquals(18, (int) tlvDataMap.get(tag_84));
		assertEquals(null, tlvDataMap.get(tag_95));
	}

	@Test
	public void testTLVUtil_TlvDataLeftPadding() throws Exception {

		tlvDataMap.put(tag_91, 12);
		tlvDataMap.put(tag_84, 18);

		String tagData_91 = "EFEFEFEFEFEFD3CA0967F68B4E5F0032";
		String tagData_84 = "EFEFEFEFEFEFEFEFEFA0000001523010";

		assertEquals(TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_91, tagData_91, ' ').length(), tagData_91.length());
		assertEquals(TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_91, tagData_91, '0').length(), tagData_91.length());
		assertEquals(TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_91, tagData_91, ' ').length(), tagData_91.length());
		assertEquals(TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_84, tagData_84, '@').length(), tagData_84.length());
		assertEquals(TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_9F09, tagData_84, '@').length(), tagData_84.length());

		assertEquals("            D3CA0967F68B4E5F0032", TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_91, tagData_91, ' '));
		assertEquals("000000000000000000A0000001523010", TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_84, tagData_84, '0'));
		assertEquals("@@@@@@@@@@@@@@@@@@A0000001523010", TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_84, tagData_84, '@'));
		assertEquals(tagData_84, TLVUtil.tlvDataLeftPadding(tlvDataMap, tag_9F09, tagData_84, '@'));

	}

	public void fillVector_Fields() {
		when(mockSettlementRecDiscover.getFlatFileRecord(Mockito.any(Integer.class))).thenAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) {
				mockFlatFileRec = PowerMockito.spy(new FlatFileRecord());

				Vector<FlatFileRecordField> fields = new Vector<FlatFileRecordField>();
				fields.add(new FlatFileRecordField(1, "issuer_authentication_data", 0, 32, 0, "", null));
				fields.add(new FlatFileRecordField(2, "dedicated_file_name", 0, 32, 0, "", null));
				fields.add(new FlatFileRecordField(3, "unpredictable_number", 0, 8, 0, "", null));
				fields.add(new FlatFileRecordField(4, "terminal_verification_results", 0, 10, 0, "", null));
				fields.add(new FlatFileRecordField(5, "terminal_capabilities", 0, 6, 0, "", null));
				fields.add(new FlatFileRecordField(6, "cardholder_verification_method_results", 0, 6, 0, "", null));
				fields.add(new FlatFileRecordField(7, "issuer_application_data_1", 0, 32, 0, "", null));
				fields.add(new FlatFileRecordField(8, "cardholder_verification_method_results", 0, 6, 0, "", null));
				fields.add(new FlatFileRecordField(9, "issuer_application_data_2", 0, 32, 0, "", null));
				Whitebox.setInternalState(mockFlatFileRec, "fields", fields);
				return mockFlatFileRec;
			}
		});

	}

	public static void emvSampleDataFill() {
		emvDataSample = new HashMap<>();

		// 91 Tag + spaces
		emvDataSample.put(1, "9F2608F8C5E9A1903D0BCB9F101000000000000000000105A080000000009F360200A6950500000080009A031904109C01009F02060000001030009F1A0208409F33036020C89F3704330A66B3820278009F03060000000000009110            97D5658072E091C300329F3501009F2701805F2A0208409F34031E0300");

		// 84 Tag + spaces
		emvDataSample.put(2, "8410                  A00000015230109F1E0400000000");

		emvDataSample.put(3, "911000000000000097D5658072E091C300329F0206  00001030009F1A0208409F33036020C89F3704330A66B3820278009F03060000000000009F3501009F2701805F2A0208409F34031E0300");

		emvDataSample.put(4, "9110            97D5658072E091C300329F02060000001030009F1A0208409F33036020C89F3704330A66B3820278009F03060000000000009F3501009F2701805F2A0208409F34031E0300");

		// Multiple Tag + Spaces
		emvDataSample.put(5, "9505     080009F1010     000000000000105A080000000008410                  A00000015230109F1E04    00009F02060000001030009F1A0208409F3303    C89F3704  0A66B3820278009F03060000000000009F3501009F2701805F2A0208409F3403    009110            97D5658072E091C30032");

		// Right Padded + Spaces
		emvDataSample.put(6, "950508000     9F1010     000000000000105A080000000008410                  A00000015230109F1E04    00009F0206  00001030009F1A0208409F3303    C89F3704  0A66B3820278009F03060000000000009F3501009F2701805F2A0208409F3403    009110            97D5658072E091C30032");

		// Contains Invalid Length(31)
		emvDataSample.put(7, "8410                 A00000015230109F1E0400000000");

		// Having ODD length
		emvDataSample.put(8, "950500000080009F1010     000000000000105A080000000008410                  A00000015230109F1E04    00009F02060000001030009F1A0208409F3303    C89F3704  0A66B3820278009F03060000000000009F3501009F2701805F2A0208409F3403    009110            97D5658072E091C30032");

		// Valid EMV Data + No Spaces
		emvDataSample.put(9, "8410000000000000000000A00000015230109F1E04000000009F101000000000001234567890ABCDEFabcdef");

		// Contains Non HexaDecimal Char 'X'
		emvDataSample.put(10, "84100000000X000000000A00000015230109F1E0400000000");

		// Valid EMV Data + No Spaces
		emvDataSample.put(11, "9F2608F8C5E9A1903D0BCB9F101000000000000000000105A080000000009F360200A6950500000080009A031904109C01009F02060000001030009F1A0208409F33036020C89F3704330A66B3820278009F0306000000000000911000000000000097D5658072E091C300329F3501009F2701805F2A0208409F34031E0300");

		// Contains Non HexaDecimal Char 'X'
		emvDataSample.put(12, "841000000000X000000000A00000015230109F1E0400000000");

		// Having AN Tag(91) + Spaces And Numeric Tag(9F02) + Spaces
		emvDataSample.put(13, "9F2608F8C5E9A1903D0BCB9F101000000000000000000105A080000000009F360200A6950500000080009A031904109C01009F0206    001030009F1A0208409F33036020C89F3704330A66B3820278009F03060000000000009110            97D5658072E091C300329F3501009F2701805F2A0208409F34031E0300");
	}

}
