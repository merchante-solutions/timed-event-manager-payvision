package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.InterchangeBase.IC_BaseClass;
import com.mes.settlement.InterchangeDiscover.IC_Discover;
import com.mes.startup.EventBase;

@RunWith(PowerMockRunner.class)
@PrepareForTest({InterchangeDiscover.class, InterchangeBase.class, SQLJConnectionBase.class, EventBase.class, IC_BaseClass.class, DbProperties.class})
public class InterchangeDiscoverTest {

	private SettlementRecord mockSettlemetRecord = null;
	private InterchangeDiscover mockInterchangeDiscover = null;
	private InterchangeBase mockInterchangeBase = null;
	private IC_BaseClass mockIcBaseClass = null;
	private List mockList = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {
		/* Supress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(EventBase.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.constructor(InterchangeBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(InterchangeDiscover.class));
		PowerMockito.suppress(PowerMockito.constructor(IC_BaseClass.class));
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(DbProperties.class));

		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		mockInterchangeDiscover = Mockito.spy(InterchangeDiscover.class);

		SettlementRecord settlementRecord = new SettlementRecordDiscover();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		mockSettlemetRecord.RecSettleRecType = 2;

		mockList = Mockito.mock(List.class);
		Mockito.doReturn(1).when(mockList).size();

		mockInterchangeBase = Mockito.mock(InterchangeBase.class);
		mockIcBaseClass = Mockito.mock(IC_BaseClass.class);

		PowerMockito.whenNew(IC_BaseClass.class).withAnyArguments().thenReturn(mockIcBaseClass);
		PowerMockito.whenNew(InterchangeBase.class).withAnyArguments().thenReturn(mockInterchangeBase);

	}

	@Test
	public void testGetBestClassSicCode4111() throws Exception {
		mockInterchangeDiscover.DisqualMap = new HashMap();

		IcProgramRulesDiscover rulesMock = Mockito.mock(IcProgramRulesDiscover.class);
		Mockito.doReturn(rulesMock).when(mockList).get(0);
		Mockito.doReturn("Y").when(rulesMock).getAmountToleranceFlags();

		IC_Discover mockIcClass = Mockito.mock(IC_Discover.class);
		Vector disqualList = new Vector<>();
		mockIcClass.DisqualList = disqualList;
		Mockito.doReturn(mockIcClass).when(mockInterchangeDiscover).getInterchangeClass("IC_Discover");
		Mockito.doCallRealMethod().when(mockIcClass).tranQualifies(mockSettlemetRecord, rulesMock);

		mockSettlemetRecord.setData("sic_code", "4111");
		PowerMockito.when(mockSettlemetRecord.getDouble("transaction_amount")).thenReturn(15.0);
		PowerMockito.when(mockSettlemetRecord.getDouble("auth_amount_total")).thenReturn(10.0);

		IcInfo mockInfo = Mockito.mock(IcInfo.class);
		doReturn("interchange_program").when(mockInfo).getIcString();
		doReturn(mockInfo).when(mockInterchangeDiscover).getBestIrfInfoFromInfoList(any(SettlementRecord.class), anyString());

		mockInterchangeDiscover.getBestClass(mockSettlemetRecord, mockList);

		assertEquals(0, mockInterchangeDiscover.DisqualMap.size());
	}

	@Test
	public void testGetBestClassSicCodeNot4111() throws Exception {
		mockInterchangeDiscover.DisqualMap = new HashMap();

		IcProgramRulesDiscover rulesMock = Mockito.mock(IcProgramRulesDiscover.class);
		Mockito.doReturn(rulesMock).when(mockList).get(0);
		Mockito.doReturn("Y").when(rulesMock).getAmountToleranceFlags();

		IC_Discover mockIcClass = Mockito.mock(IC_Discover.class);
		Vector disqualList = new Vector<>();
		mockIcClass.DisqualList = disqualList;
		Mockito.doReturn(mockIcClass).when(mockInterchangeDiscover).getInterchangeClass("IC_Discover");
		Mockito.doCallRealMethod().when(mockIcClass).tranQualifies(mockSettlemetRecord, rulesMock);

		mockSettlemetRecord.setData("sic_code", "8931");
		PowerMockito.when(mockSettlemetRecord.getDouble("transaction_amount")).thenReturn(15.0);
		PowerMockito.when(mockSettlemetRecord.getDouble("auth_amount_total")).thenReturn(10.0);

		IcInfo mockInfo = Mockito.mock(IcInfo.class);
		doReturn("interchange_program").when(mockInfo).getIcString();
		doReturn(mockInfo).when(mockInterchangeDiscover).getBestIrfInfoFromInfoList(any(SettlementRecord.class), anyString());

		mockInterchangeDiscover.getBestClass(mockSettlemetRecord, mockList);

		assertEquals(1, mockInterchangeDiscover.DisqualMap.size());
	}

}
