package com.mes.settlement.lineitem;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordAmex;
import com.mes.settlement.SettlementRecordMC;
import com.mes.settlement.SettlementRecordVisa;
import dto.LevelDto;
import dto.VisaLineItem;;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SettlementRecordMC.class, DbProperties.class, SQLJConnectionBase.class})
public class SetLineItemDataTest {

	private SettlementRecord mockedSettlementRecord;
	private SettlementDb mockSettlementDb;
	private LevelDto levelDataBO;

	private static final String sampleLevelDtoResponse = "{\r\n" + "    \"creationDate\": \"Wed Jun 24 02:18:38 UTC 2020\",\r\n" + "    \"active\": true,\r\n" + "    \"createdBy\": \"Alfred Kumar\",\r\n" + "    \"merchantNumber\": \"941000074788\",\r\n" + "    \"version\": 39,\r\n" + "    \"levelIII\": {\r\n" + "        \"merchantTaxId\": \"3\",\r\n" + "        \"discountAmount\": \"3.0\",\r\n" + "        \"cardholderRefNumber\": \"3\",\r\n" + "        \"freightOrShippingAmount\": \"3.0\",\r\n" + "        \"requesterName\": \"3\",\r\n" + "        \"shippingAmount\": \"3.0\",\r\n" + "        \"dutyAmount\": \"3.0\",\r\n" + "        \"vatAmount\": \"3.0\"\r\n" + "    },\r\n" + "    \"levelII\": {\r\n" + "        \"invoiceNumber\": \"3\",\r\n" + "        \"shipToZip\": \"31111\"\r\n" + "    },\r\n" + "    \"lineItem\": {\r\n" + "        \"visaLineItem\": {\r\n" + "            \"itemCommodityCode\": \"3\",\r\n" + "            \"itemDescriptor\": \"3\",\r\n" + "            \"productCode\": \"3\",\r\n" + "            \"quantity\": \"3\",\r\n" + "            \"unitOfMeasure\": \"3\",\r\n" + "            \"unitCost\": \"3.0\",\r\n" + "            \"vatTaxAmount\": \"3.0\",\r\n" + "            \"vatTaxRate\": \"3.0\",\r\n" + "            \"discountPerLine\": \"3.0\",\r\n" + "            \"lineItemTotal\": \"3.0\",\r\n" + "            \"debitOrCreditIndicator\": \"D\"\r\n" + "        },\r\n" + "        \"mcLineItem\": {\r\n" + "            \"itemDescription\": \"3\",\r\n" + "            \"productCode\": \"3\",\r\n" + "            \"itemQuantity\": \"3\",\r\n" + "            \"itemUnitMeasure\": \"3\",\r\n" + "            \"taxRateApplied\": \"3.0\",\r\n" + "            \"taxAmount\": \"3.0\",\r\n" + "            \"discountIndicator\": \"N\",\r\n" + "            \"netOrGrossIndicator\": \"G\",\r\n" + "            \"extendedItemAmount\": \"3.0\",\r\n" + "            \"debitCreditIndicator\": \"D\",\r\n" + "            \"itemDiscountAmount\": \"3.0\"\r\n" + "        },\r\n" + "        \"amexLineItem\": {\r\n" + "            \"extendedItemAmount\": \"3.0\",\r\n" + "            \"itemDescription\": \"3\",\r\n" + "            \"itemQuantity\": \"3\"\r\n" + "        }\r\n" + "    }\r\n" + "}";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.when(DbProperties.configure(Mockito.anyString())).thenReturn(true);
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		mockSettlementDb = PowerMockito.spy(new SettlementDb());

	}

	private void setupHasLevelDataFalse() {
		PowerMockito.when(mockedSettlementRecord.getData("batch_record_id")).thenReturn("123");
		PowerMockito.when(mockedSettlementRecord.getLong("batch_record_id")).thenReturn(123l);
		PowerMockito.when(mockedSettlementRecord.hasLevelIIIData()).thenReturn(false);
	}

	@Test
	public void testSetLineItemDataVisa() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordVisa());
		setupHasLevelDataFalse();

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);
	}

	@Test
	public void testSetLineItemDataVisaEmptyDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordVisa());
		setupHasLevelDataFalse();

		levelDataBO = new LevelDto();
		dto.LineItem lineitem = new dto.LineItem();
		lineitem.setVisaLineItem(new VisaLineItem());
		levelDataBO.setLineItem(lineitem);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);

	}

	@Test
	public void testSetLineItemDataVisaFullDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordVisa());
		setupHasLevelDataFalse();

		levelDataBO = mockLevelDto(sampleLevelDtoResponse);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(1, items.size());
	}

	@Test
	public void testSetLineItemDataMC() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordMC());
		setupHasLevelDataFalse();

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);
	}

	@Test
	public void testSetLineItemDataMCEmptyDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordMC());
		setupHasLevelDataFalse();

		levelDataBO = new LevelDto();
		dto.LineItem lineitem = new dto.LineItem();
		lineitem.setVisaLineItem(new VisaLineItem());
		levelDataBO.setLineItem(lineitem);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);

	}

	@Test
	public void testSetLineItemDataMCFullDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordMC());
		setupHasLevelDataFalse();

		levelDataBO = mockLevelDto(sampleLevelDtoResponse);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(1, items.size());
	}

	@Test
	public void testSetLineItemDataAmex() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordAmex());
		setupHasLevelDataFalse();

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);
	}

	@Test
	public void testSetLineItemDataAmexEmptyDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordAmex());
		setupHasLevelDataFalse();

		levelDataBO = new LevelDto();
		dto.LineItem lineitem = new dto.LineItem();
		lineitem.setVisaLineItem(new VisaLineItem());
		levelDataBO.setLineItem(lineitem);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(null, items);

	}

	@Test
	public void testSetLineItemDataAmexFullDto() throws Exception {
		mockedSettlementRecord = Mockito.spy(new SettlementRecordAmex());
		setupHasLevelDataFalse();

		levelDataBO = mockLevelDto(sampleLevelDtoResponse);

		List<com.mes.settlement.LineItem> items = mockSettlementDb.setLineItemData(mockedSettlementRecord, levelDataBO);
		assertEquals(1, items.size());
	}

	private LevelDto mockLevelDto(String serviceResponse) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(serviceResponse, LevelDto.class);
	}

}