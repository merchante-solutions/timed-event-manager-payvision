package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class AmexOptBlueBinRangeTest {

	@Test
	public void testIsVpaymentWithVP() {
		assertEquals(true, AmexOptBlueBinRange.isVpayment("VP"));
	}

	@Test
	public void testIsVpaymentWithNull() {
		assertEquals(false, AmexOptBlueBinRange.isVpayment(null));
	}
	
	@Test
	public void testIsVpaymentWithEmpty() {
		assertEquals(false, AmexOptBlueBinRange.isVpayment(""));
	}

	@Test
	public void testIsVpaymentOtherThanVP() {
		assertEquals(false, AmexOptBlueBinRange.isVpayment("RP"));
	}

}
