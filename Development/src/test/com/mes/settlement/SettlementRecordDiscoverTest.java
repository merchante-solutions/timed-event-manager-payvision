package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Test;
import org.mockito.Mockito;
import com.mes.constants.mesConstants;

public class SettlementRecordDiscoverTest {

	private static final int DEF_TYPE_CDF_PT1_REC = 703;
	private static final int DEF_TYPE_DISCOVER_SDR05_SEQ_1 = 9301;
	@SuppressWarnings("rawtypes")
	private static final HashMap deprecatedHashMap = new HashMap();
	@SuppressWarnings("rawtypes")
	private static final HashMap nonDeprecatedHashMap = new HashMap();
	@SuppressWarnings({"deprecation"})
	private static final ArrayList<Integer> deprecatedType = new ArrayList() {
		{
			add(new Integer(DEF_TYPE_DISCOVER_SDR05_SEQ_1));
		}
	};
	private static final ArrayList<Integer> nonDeprecatedType = new ArrayList() {
		{
			add(DEF_TYPE_DISCOVER_SDR05_SEQ_1);
		}
	};

	@Test
	@SuppressWarnings({"deprecation"})
	public void testHashMap() {
		deprecatedHashMap.put(new Integer(DEF_TYPE_CDF_PT1_REC), DEF_TYPE_CDF_PT1_REC);
		nonDeprecatedHashMap.put(DEF_TYPE_CDF_PT1_REC, DEF_TYPE_CDF_PT1_REC);

		assertEquals(deprecatedHashMap.get(deprecatedHashMap.keySet().toArray()[0]), nonDeprecatedHashMap.get(nonDeprecatedHashMap.keySet().toArray()[0]));
	}
	
	@Test
	public void testSetPosData_Enabled() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		discoverSettlement.setData("moto_ecommerce_ind", "8");
		discoverSettlement.setData("pos_condition_code","0");
		discoverSettlement.setData("disc_partial_auth_ind","2");
		discoverSettlement.setData("device_code", "G");
		
		discoverSettlement.setPosData();
		
		String posData = discoverSettlement.getData("pos_data");
		
		assertNotNull(posData);
		assertEquals('2', posData.charAt(1));
	}
	
	@Test
	public void testSetPosData_DisEnabled() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		discoverSettlement.setData("moto_ecommerce_ind", "8");
		discoverSettlement.setData("pos_condition_code","0");
		discoverSettlement.setData("disc_partial_auth_ind","0");
		discoverSettlement.setData("device_code", "G");
		
		discoverSettlement.setPosData();
		
		String posData = discoverSettlement.getData("pos_data");
		
		assertNotNull(posData);
		assertEquals('0', posData.charAt(1));
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_TSYS() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		
		TridentAuthData authData = new TridentAuthData();
		authData.setDiscPartialAuthInd("2");
		authData.setPartialAuthInd(null);
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_VITAL, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("2", partialAuthInd);
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_Trident_True() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		
		TridentAuthData authData = new TridentAuthData();
		authData.setDiscPartialAuthInd(null);
		authData.setPartialAuthInd("Y");
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_VISAK, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("2", partialAuthInd);
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_Trident_False() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		
		TridentAuthData authData = new TridentAuthData();
		authData.setDiscPartialAuthInd(null);
		authData.setPartialAuthInd("N");
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_VISAK, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("0", partialAuthInd);
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_PG_Y() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		discoverSettlement.setData("is_partial_auth_enabled", "Y");
		
		TridentAuthData authData = null;
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_PG, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("2", partialAuthInd);
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_PG_Null() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		
		TridentAuthData authData = null;
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_PG, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("0", partialAuthInd);
	}
	
	@Test
	public void testGetDiscoverPartialAuthInd_PG_N() {
		SettlementRecordDiscover discoverSettlement = Mockito.spy(SettlementRecordDiscover.class);
		discoverSettlement.setData("is_partial_auth_enabled", "N");
		
		TridentAuthData authData = null;
		
		String partialAuthInd = discoverSettlement.getDiscoverPartialAuthInd(mesConstants.MBS_BT_PG, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("0", partialAuthInd);
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testArrayList() {

		assertEquals(deprecatedType.get(0), nonDeprecatedType.get(0));
	}
}