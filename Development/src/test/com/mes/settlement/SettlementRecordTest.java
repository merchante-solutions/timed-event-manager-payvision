package com.mes.settlement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.mockito.Matchers;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyInt;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import org.apache.log4j.Logger;
import java.sql.Date;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DbProperties.class, SettlementDb.class, SQLJConnectionBase.class, TridentAuthData.class})
public class SettlementRecordTest {
	static Logger log = Logger.getLogger(SettlementRecordTest.class);

	private SettlementRecordAmex settlementRecordAmexSpy = null;
	private SettlementRecordMC settlementRecordMCSpy = null;
	private SettlementRecord mockSettlemetRecord = null;
	private TridentAuthData authData = null;
	private SettlementRecordVisa settlementRecordVisaSpy = null;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
	}

	@Before
	public void setUp() throws Exception {

		/* Suppress constructor and method calls */
		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		/* Mock static classes */
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.mockStatic(SettlementDb.class);
		PowerMockito.mockStatic(TridentAuthData.class);
		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);

		settlementRecordAmexSpy = PowerMockito.spy(new SettlementRecordAmex());
		settlementRecordMCSpy = PowerMockito.spy(new SettlementRecordMC());
		settlementRecordVisaSpy = PowerMockito.spy(new SettlementRecordVisa());
	}

	@Test
	public void testSetAuthDataForMCWithStaticAuthCode() {
		settlementRecordMCSpy.RecBatchType = 2;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("rec_auth_id", "0");
		settlementRecordMCSpy.setData("auth_avs_response", "00");
		settlementRecordMCSpy.setData("debit_credit_indicator", "C");
		settlementRecordMCSpy.setData("transaction_type", "C");
		settlementRecordMCSpy.setData("auth_avs_zip", "30005");
		settlementRecordMCSpy.setData("auth_code", "TCR001");
		settlementRecordMCSpy.setData("auth_currency_code", "840");
		settlementRecordMCSpy.setData("auth_cvv2_response", "123");
		settlementRecordMCSpy.setData("auth_fpi", "C");
		settlementRecordMCSpy.setData("auth_response_code", "00");
		settlementRecordMCSpy.setData("auth_retrieval_ref_num", "T11313113131");
		settlementRecordMCSpy.setData("auth_source_code", "1");
		settlementRecordMCSpy.setData("market_specific_data_ind", "A");
		settlementRecordMCSpy.setData("spend_qualified_ind", "A");
		settlementRecordMCSpy.setData("token_assurance_level", "X");
		settlementRecordMCSpy.setData("card_sequence_number", "1081093813913901");
		settlementRecordMCSpy.setData("iso_ecommerce_indicator", "1");
		settlementRecordMCSpy.setData("pos_condition_code", "10");
		settlementRecordMCSpy.setData("pos_environment", "1");
		settlementRecordMCSpy.setData("auth_amount", "100");
		settlementRecordMCSpy.setData("auth_amount_total", "100");
		settlementRecordMCSpy.setData("auth_tran_id", "T10810321");
		settlementRecordMCSpy.setData("auth_val_code", "3234");
		settlementRecordMCSpy.setAuthData();

		assertEquals("", settlementRecordMCSpy.getData("auth_avs_response"));
		assertEquals("", settlementRecordMCSpy.getData("auth_avs_zip"));
		assertEquals("", settlementRecordMCSpy.getData("auth_code"));
		assertEquals("", settlementRecordMCSpy.getData("auth_currency_code"));
		assertEquals("", settlementRecordMCSpy.getData("auth_cvv2_response"));
		assertEquals("", settlementRecordMCSpy.getData("auth_fpi"));
		assertEquals("", settlementRecordMCSpy.getData("auth_response_code"));
		assertEquals("", settlementRecordMCSpy.getData("auth_retrieval_ref_num"));
		assertEquals("", settlementRecordMCSpy.getData("auth_source_code"));
		assertEquals("", settlementRecordMCSpy.getData("market_specific_data_ind"));
		assertEquals("", settlementRecordMCSpy.getData("spend_qualified_ind"));
		assertEquals("", settlementRecordMCSpy.getData("token_assurance_level"));
		assertEquals("", settlementRecordMCSpy.getData("card_sequence_number"));
		assertEquals("", settlementRecordMCSpy.getData("iso_ecommerce_indicator"));
		assertEquals("", settlementRecordMCSpy.getData("pos_condition_code"));
		assertEquals("", settlementRecordMCSpy.getData("pos_environment"));
		assertEquals("", settlementRecordMCSpy.getData("auth_amount"));
		assertEquals("", settlementRecordMCSpy.getData("auth_amount_total"));
		assertEquals("", settlementRecordMCSpy.getData("auth_tran_id"));
		assertEquals("", settlementRecordMCSpy.getData("auth_val_code"));
	}
	
	@Test
	public void testSetAuthDataForMC() {
		settlementRecordMCSpy.RecBatchType = 2;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("rec_auth_id", "0");
		settlementRecordMCSpy.setData("auth_avs_response", "00");
		settlementRecordMCSpy.setData("debit_credit_indicator", "C");
		settlementRecordMCSpy.setData("transaction_type", "C");
		settlementRecordMCSpy.setData("auth_avs_zip", "30005");
		settlementRecordMCSpy.setData("auth_code", "T43001");
		settlementRecordMCSpy.setData("auth_currency_code", "840");
		settlementRecordMCSpy.setData("auth_cvv2_response", "123");
		settlementRecordMCSpy.setData("auth_fpi", "C");
		settlementRecordMCSpy.setData("auth_response_code", "00");
		settlementRecordMCSpy.setData("auth_retrieval_ref_num", "T11313113131");
		settlementRecordMCSpy.setData("auth_source_code", "1");
		settlementRecordMCSpy.setData("market_specific_data_ind", "A");
		settlementRecordMCSpy.setData("spend_qualified_ind", "A");
		settlementRecordMCSpy.setData("token_assurance_level", "X");
		settlementRecordMCSpy.setData("card_sequence_number", "1081093813913901");
		settlementRecordMCSpy.setData("iso_ecommerce_indicator", "1");
		settlementRecordMCSpy.setData("pos_condition_code", "10");
		settlementRecordMCSpy.setData("pos_environment", "1");
		settlementRecordMCSpy.setData("auth_amount", "100");
		settlementRecordMCSpy.setData("auth_amount_total", "100");
		settlementRecordMCSpy.setData("auth_tran_id", "T10810321");
		settlementRecordMCSpy.setData("auth_val_code", "3234");
		
		settlementRecordMCSpy.setAuthData();
		
		assertEquals("T43001", settlementRecordMCSpy.getData("auth_code"));
		assertEquals("T10810321", settlementRecordMCSpy.getData("auth_tran_id"));
		assertEquals("3234", settlementRecordMCSpy.getData("auth_val_code"));
		assertEquals("30005", settlementRecordMCSpy.getData("auth_avs_zip"));
		assertEquals("100.00", settlementRecordMCSpy.getData("auth_amount"));
	}

	@Test
	public void testSetAuthDataForAmexWithStaticAuthCode() {
		settlementRecordAmexSpy.RecBatchType = 2;
		settlementRecordAmexSpy.RecSettleRecType = 3;
		settlementRecordAmexSpy.setData("rec_auth_id", "0");
		settlementRecordAmexSpy.setData("auth_avs_response", "00");
		settlementRecordAmexSpy.setData("debit_credit_indicator", "C");
		settlementRecordAmexSpy.setData("transaction_type", "C");
		settlementRecordAmexSpy.setData("auth_avs_zip", "30005");
		settlementRecordAmexSpy.setData("auth_code", "TCR001");
		settlementRecordAmexSpy.setData("auth_currency_code", "840");
		settlementRecordAmexSpy.setData("auth_cvv2_response", "123");
		settlementRecordAmexSpy.setData("auth_fpi", "C");
		settlementRecordAmexSpy.setData("auth_response_code", "00");
		settlementRecordAmexSpy.setData("auth_retrieval_ref_num", "T11313113131");
		settlementRecordAmexSpy.setData("auth_source_code", "1");
		settlementRecordAmexSpy.setData("market_specific_data_ind", "A");
		settlementRecordAmexSpy.setData("spend_qualified_ind", "A");
		settlementRecordAmexSpy.setData("token_assurance_level", "X");
		settlementRecordAmexSpy.setData("card_sequence_number", "1081093813913901");
		settlementRecordAmexSpy.setData("iso_ecommerce_indicator", "1");
		settlementRecordAmexSpy.setData("pos_condition_code", "10");
		settlementRecordAmexSpy.setData("pos_environment", "1");
		settlementRecordAmexSpy.setData("auth_amount", "100");
		settlementRecordAmexSpy.setData("auth_amount_total", "100");
		settlementRecordAmexSpy.setData("auth_tran_id", "T10810321");
		settlementRecordAmexSpy.setData("auth_val_code", "3234");

		settlementRecordAmexSpy.setAuthData();

		assertEquals("", settlementRecordAmexSpy.getData("auth_avs_response"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_avs_zip"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_code"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_currency_code"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_cvv2_response"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_fpi"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_response_code"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_retrieval_ref_num"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_source_code"));
		assertEquals("", settlementRecordAmexSpy.getData("market_specific_data_ind"));
		assertEquals("", settlementRecordAmexSpy.getData("spend_qualified_ind"));
		assertEquals("", settlementRecordAmexSpy.getData("token_assurance_level"));
		assertEquals("", settlementRecordAmexSpy.getData("card_sequence_number"));
		assertEquals("", settlementRecordAmexSpy.getData("iso_ecommerce_indicator"));
		assertEquals("", settlementRecordAmexSpy.getData("pos_condition_code"));
		assertEquals("", settlementRecordAmexSpy.getData("pos_environment"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_amount"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_amount_total"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_tran_id"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_val_code"));
	}
	
	@Test
	public void testSetAuthDataForAmex() {
		settlementRecordAmexSpy.RecBatchType = 2;
		settlementRecordAmexSpy.RecSettleRecType = 3;
		settlementRecordAmexSpy.setData("rec_auth_id", "0");
		settlementRecordAmexSpy.setData("auth_avs_response", "00");
		settlementRecordAmexSpy.setData("debit_credit_indicator", "C");
		settlementRecordAmexSpy.setData("transaction_type", "C");
		settlementRecordAmexSpy.setData("auth_avs_zip", "30005");
		settlementRecordAmexSpy.setData("auth_code", "T43001");
		settlementRecordAmexSpy.setData("auth_currency_code", "840");
		settlementRecordAmexSpy.setData("auth_cvv2_response", "123");
		settlementRecordAmexSpy.setData("auth_fpi", "C");
		settlementRecordAmexSpy.setData("auth_response_code", "00");
		settlementRecordAmexSpy.setData("auth_retrieval_ref_num", "T11313113131");
		settlementRecordAmexSpy.setData("auth_source_code", "1");
		settlementRecordAmexSpy.setData("market_specific_data_ind", "A");
		settlementRecordAmexSpy.setData("spend_qualified_ind", "A");
		settlementRecordAmexSpy.setData("token_assurance_level", "X");
		settlementRecordAmexSpy.setData("card_sequence_number", "1081093813913901");
		settlementRecordAmexSpy.setData("iso_ecommerce_indicator", "1");
		settlementRecordAmexSpy.setData("pos_condition_code", "10");
		settlementRecordAmexSpy.setData("pos_environment", "1");
		settlementRecordAmexSpy.setData("auth_amount", "100");
		settlementRecordAmexSpy.setData("auth_amount_total", "100");
		settlementRecordAmexSpy.setData("auth_tran_id", "T10810321");
		settlementRecordAmexSpy.setData("auth_val_code", "3234");

		settlementRecordAmexSpy.setAuthData();

		assertEquals("", settlementRecordAmexSpy.getData("auth_code"));
		assertEquals("", settlementRecordAmexSpy.getData("auth_tran_id"));
		assertNotEquals("", settlementRecordAmexSpy.getData("auth_val_code"));
	}
	
	@Test
	public void testSetAuthDataForAmexForDTransactionType() {
		settlementRecordAmexSpy.RecBatchType = 2;
		settlementRecordAmexSpy.RecSettleRecType = 3;
		settlementRecordAmexSpy.setData("rec_auth_id", "0");
		settlementRecordAmexSpy.setData("auth_avs_response", "00");
		settlementRecordAmexSpy.setData("debit_credit_indicator", "D");
		settlementRecordAmexSpy.setData("transaction_type", "C");
		settlementRecordAmexSpy.setData("auth_avs_zip", "30005");
		settlementRecordAmexSpy.setData("auth_code", "843001");
		settlementRecordAmexSpy.setData("auth_currency_code", "840");
		settlementRecordAmexSpy.setData("auth_cvv2_response", "123");
		settlementRecordAmexSpy.setData("auth_fpi", "C");
		settlementRecordAmexSpy.setData("auth_response_code", "00");
		settlementRecordAmexSpy.setData("auth_retrieval_ref_num", "T11313113131");
		settlementRecordAmexSpy.setData("auth_source_code", "1");
		settlementRecordAmexSpy.setData("market_specific_data_ind", "A");
		settlementRecordAmexSpy.setData("spend_qualified_ind", "A");
		settlementRecordAmexSpy.setData("token_assurance_level", "X");
		settlementRecordAmexSpy.setData("card_sequence_number", "1081093813913901");
		settlementRecordAmexSpy.setData("iso_ecommerce_indicator", "1");
		settlementRecordAmexSpy.setData("pos_condition_code", "10");
		settlementRecordAmexSpy.setData("pos_environment", "1");
		settlementRecordAmexSpy.setData("auth_amount", "100");
		settlementRecordAmexSpy.setData("auth_tran_fee_amount", "3");
		settlementRecordAmexSpy.setData("auth_amount_total", "100");
		settlementRecordAmexSpy.setData("auth_tran_id", "T10810321");
		settlementRecordAmexSpy.setData("auth_val_code", "1234");

		settlementRecordAmexSpy.setAuthData();
		
		assertEquals("843001", settlementRecordAmexSpy.getData("auth_code"));
		assertEquals("T10810321", settlementRecordAmexSpy.getData("auth_tran_id"));
		assertEquals("1234", settlementRecordAmexSpy.getData("auth_val_code"));
		assertEquals("3.00", settlementRecordAmexSpy.getData("auth_tran_fee_amount"));
	}
	
	@Test
	public void testAuthTranFeeAmount() {
		double authTranFeeAmount = 3;
		
		TridentAuthData authData = PowerMockito.spy(new TridentAuthData());
		SettlementRecordAmex record = settlementRecordAmexSpy;

		PowerMockito.when(SettlementDb.loadAuthDataVital(anyLong(), Matchers.any(Date.class), anyInt(), anyString(), anyString(), anyString())).thenReturn(authData);

		authData.setAuthTranFeeAmount(authTranFeeAmount);

		record.RecBatchType = 3;
		record.RecSettleRecType = 3;
		record.setData("load_filename", "file1234.txt");
		record.setData("auth_code", "T43001");
		record.setData("transaction_date", "01/31/2019");

		try {
			record.setAuthData();
		} catch(Exception e) {
			log.error("Error.", e);
			throw e;
		}

		assertEquals("3.00", record.getData("auth_tran_fee_amount"));
	}
	
	@Test
	public void testIsForcePostTransaction_VSCardTypeTrue() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordVisa();
		mockSettlemetRecord = Mockito.spy(settlementRecord);

		/* tranId less than 15 characters */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("77070000104410");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 0 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("000000000000000");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 1 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("111111111111111");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* validation code is missing */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("770700001044100");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* blank tranId */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("               ");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

	}

	@Test
	public void testIsForcePostTransaction_VSCardTypeFalse() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordVisa();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("770700001044100");
		PowerMockito.when(mockSettlemetRecord.getData("auth_val_code")).thenReturn("1224");
		assertFalse(mockSettlemetRecord.isForcePostTransaction());
	}

	@Test
	public void testIsForcePostTransaction_MCCardTypeTrue() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordMC();
		mockSettlemetRecord = Mockito.spy(settlementRecord);

		/* tranId less than 9 characters */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("MCC00011");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 0 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("000000000");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 1 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("111111111");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* first 3 characters are numeric */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("12300011");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* blank tranId */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("               ");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

	}

	@Test
	public void testIsForcePostTransaction_MCCardTypeFalse() throws Exception {

		SettlementRecord settlementRecord = new SettlementRecordMC();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("MCC000111");
		assertFalse(mockSettlemetRecord.isForcePostTransaction());
	}

	@Test
	public void testIsForcePostTransaction_AmexCardTypeTrue() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordAmex();
		mockSettlemetRecord = Mockito.spy(settlementRecord);

		/* tranId less than 15 characters */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("01220000000010");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 0 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("000000000000000");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 1 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("111111111111111");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* blank tranId */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("               ");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());
	}

	@Test
	public void testIsForcePostTransaction_AmexCardTypeFalse() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordAmex();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("012200000000100");
		assertFalse(mockSettlemetRecord.isForcePostTransaction());
	}

	@Test
	public void testIsForcePostTransaction_DSCardTypeTrue() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordDiscover();
		mockSettlemetRecord = Mockito.spy(settlementRecord);

		/* tranId less than 15 characters */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("12345611100010");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 0 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("000000000000000");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* tranId contains all 1 */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("111111111111111");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());

		/* blank tranId */
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("               ");
		assertTrue(mockSettlemetRecord.isForcePostTransaction());
	}

	@Test
	public void testIsForcePostTransaction_DSCardTypeFalse() throws Exception {
		SettlementRecord settlementRecord = new SettlementRecordDiscover();
		mockSettlemetRecord = Mockito.spy(settlementRecord);
		PowerMockito.when(mockSettlemetRecord.getData("network_tran_id")).thenReturn("123456111000100");
		assertFalse(mockSettlemetRecord.isForcePostTransaction());
	}
	
	@Test
	public void testGetPartialAuthEnabled_NotDiscover() {
		settlementRecordMCSpy.RecBatchType = mesConstants.MBS_BT_VISAK;
		settlementRecordMCSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_MC;
		
		TridentAuthData authData = new TridentAuthData();
		
		String partialAuthInd = settlementRecordMCSpy.getPartialAuthEnabled(mesConstants.MBS_BT_VISAK, authData);
		
		assertNull(partialAuthInd);
	}
	
	@Test 
	public void testGetPartialAuthEnabled_VISAK_Enabled() {
		settlementRecordMCSpy.RecBatchType = mesConstants.MBS_BT_VISAK;
		settlementRecordMCSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_DISC;
		
		TridentAuthData authData = new TridentAuthData();
		authData.setPartialAuthInd("Y");
		authData.setDiscPartialAuthInd(null);
		
		String partialAuthInd = settlementRecordMCSpy.getPartialAuthEnabled(mesConstants.MBS_BT_VISAK, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("Y", partialAuthInd);
		
	}
	
	@Test 
	public void testGetPartialAuthEnabled_VISAK_DisEnabled() {
		settlementRecordMCSpy.RecBatchType = mesConstants.MBS_BT_VISAK;
		settlementRecordMCSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_DISC;
		
		TridentAuthData authData = new TridentAuthData();
		authData.setPartialAuthInd("N");
		authData.setDiscPartialAuthInd(null);
		
		String partialAuthInd = settlementRecordMCSpy.getPartialAuthEnabled(mesConstants.MBS_BT_VISAK, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("N", partialAuthInd);
	}
	
	@Test 
	public void testGetPartialAuthEnabled_TSYS_Enabled() {
		settlementRecordMCSpy.RecBatchType = mesConstants.MBS_BT_VITAL;
		settlementRecordMCSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_DISC;
		
		TridentAuthData authData = new TridentAuthData();
		authData.setPartialAuthInd(null);
		authData.setDiscPartialAuthInd("2");
		
		String partialAuthInd = settlementRecordMCSpy.getPartialAuthEnabled(mesConstants.MBS_BT_VITAL, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("Y", partialAuthInd);
	}
	
	@Test 
	public void testGetPartialAuthEnabled_TSYS_Disabled() {
		settlementRecordMCSpy.RecBatchType = mesConstants.MBS_BT_VITAL;
		settlementRecordMCSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_DISC;
		
		TridentAuthData authData = new TridentAuthData();
		authData.setPartialAuthInd(null);
		authData.setDiscPartialAuthInd("0");
		
		String partialAuthInd = settlementRecordMCSpy.getPartialAuthEnabled(mesConstants.MBS_BT_VITAL, authData);
		
		assertNotNull(partialAuthInd);
		assertEquals("N", partialAuthInd);
	}
	
	@Test
	public void testPosDataCodeCardPresentY() throws Exception {
		settlementRecordMCSpy.RecBatchType = 3;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		settlementRecordMCSpy.setData("card_present","Y");
		settlementRecordMCSpy.setPosData();
		assertEquals('1', settlementRecordMCSpy.getData("pos_data_code").charAt(5));
	}
	
	@Test
	public void testPosDataCodeCardPresentN() throws Exception {
		settlementRecordMCSpy.RecBatchType = 3;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		settlementRecordMCSpy.setData("card_present","N");
		settlementRecordMCSpy.setPosData();
		assertEquals('0', settlementRecordMCSpy.getData("pos_data_code").charAt(5));
	}
	
	@Test
	public void testPosData_PanManualEntry() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		settlementRecordMCSpy.setData("mc_pos_entry_mode", "010");
		settlementRecordMCSpy.setPosData();
		assertEquals('1', settlementRecordMCSpy.getData("pos_data_code").charAt(6));
	}
	
	@Test
	public void testPosData_PanAutoEntry() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		settlementRecordMCSpy.setData("mc_pos_entry_mode", "900");
		settlementRecordMCSpy.setPosData();
		assertEquals('B', settlementRecordMCSpy.getData("pos_data_code").charAt(6));
	}
	
	@Test
	public void testPosData_PanOrTokenlEntryViaEcomm() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		settlementRecordMCSpy.setData("mc_pos_entry_mode", "810");
		settlementRecordMCSpy.setPosData();
		assertEquals('S', settlementRecordMCSpy.getData("pos_data_code").charAt(6));
	}
	
	@Test
	public void testPosData_PosCardholderPresence() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0;
		settlementRecordMCSpy.RecSettleRecType = 2;
		settlementRecordMCSpy.setData("pos_data_code","099999099001");
		
		// case : 0 
		settlementRecordMCSpy.setData("mc_pos_data", "102010000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('0', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
		
		// case : 1 
		settlementRecordMCSpy.setData("mc_pos_data", "102110000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('1', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
		
		// case : 2 
		settlementRecordMCSpy.setData("mc_pos_data", "102210000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('2', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
		
		// case : 3 
		settlementRecordMCSpy.setData("mc_pos_data", "102310000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('3', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
		
		// case : 4
		settlementRecordMCSpy.setData("mc_pos_data", "102410000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('4', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
		
		// case : 5 
		settlementRecordMCSpy.setData("mc_pos_data", "102510000660084099216");
		settlementRecordMCSpy.setPosData();
		assertEquals('5', settlementRecordMCSpy.getData("pos_data_code").charAt(4));
	}
	
	@Test
	public void testPosData_PosCardPresent() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0; // '0' Applicable for all batches
		settlementRecordMCSpy.RecSettleRecType = 2; // Only for MC trxns
		settlementRecordMCSpy.setData("pos_data_code", "999999999999");
		settlementRecordMCSpy.setData("mc_pos_data", "102400000660084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 5 set to 0 (Card Present)
		settlementRecordMCSpy.setPosData();
		assertEquals('1', settlementRecordMCSpy.getData("pos_data_code").charAt(5));
	}

	@Test
	public void testPosData_PosCardNotPresent() throws Exception {
		settlementRecordMCSpy.RecBatchType = 0;									// '0' Applicable for all batches
		settlementRecordMCSpy.RecSettleRecType = 2;								// Only for MC trxns
		settlementRecordMCSpy.setData("pos_data_code","999999999999");
		settlementRecordMCSpy.setData("mc_pos_data", "102410000660084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 5 set to 1 (Card not Present)
		settlementRecordMCSpy.setPosData();
		assertEquals('0', settlementRecordMCSpy.getData("pos_data_code").charAt(5));
	}
	
	@Test
	public void testPosData_TerminalInputCapability() throws Exception {

		settlementRecordMCSpy.RecBatchType = 0; // '0' Applicable for all batches
		settlementRecordMCSpy.RecSettleRecType = 2; // Only for MC trxns
		settlementRecordMCSpy.setData("pos_data_code", "999999999999");
		
		// case : 0, Unknown or Unspecified
		settlementRecordMCSpy.setData("mc_pos_data", "102410000600084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 0 
		settlementRecordMCSpy.setPosData();
		assertEquals('0', settlementRecordMCSpy.getData("pos_data_code").charAt(0));
		

		// case : 1, No terminal used (voice/ARU authorization); server
		settlementRecordMCSpy.setData("mc_pos_data", "102410000610084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 1 
		settlementRecordMCSpy.setPosData();
		assertEquals('1', settlementRecordMCSpy.getData("pos_data_code").charAt(0));
		
		// case : 3, Contactless M/Chip (Proximity Chip)
		settlementRecordMCSpy.setData("mc_pos_data", "102410000630084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 3 
		settlementRecordMCSpy.setPosData();
		assertEquals('M', settlementRecordMCSpy.getData("pos_data_code").charAt(0));
		
		// case : 5, EMV specification (compatible chip reader) and magnetic stripe reader.
		settlementRecordMCSpy.setData("mc_pos_data", "102410000650084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 5 
		settlementRecordMCSpy.setPosData();
		assertEquals('D', settlementRecordMCSpy.getData("pos_data_code").charAt(0));
		
		// case : 6, Key entry only
		settlementRecordMCSpy.setData("mc_pos_data", "102410000660084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 6 
		settlementRecordMCSpy.setPosData();
		assertEquals('6', settlementRecordMCSpy.getData("pos_data_code").charAt(0));
		
		// case : 7, Magnetic stripe reader and key entry
		settlementRecordMCSpy.setData("mc_pos_data", "102410000670084099216"); // Authorzation MC_POS_DATA(DE61) - sub-field 11 set to 7 
		settlementRecordMCSpy.setPosData();
		assertEquals('B', settlementRecordMCSpy.getData("pos_data_code").charAt(0));

	}
	
	@Test 
	public void testAuthValCodeNull_Tsys()  throws Exception {

		TridentAuthData authData = new TridentAuthData();
		settlementRecordVisaSpy.RecBatchType = mesConstants.MBS_BT_VITAL;
		settlementRecordVisaSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_VISA;
		
		authData.setAuthValCode(null);
		settlementRecordVisaSpy.setData("auth_val_code", "D7MM");;
		settlementRecordVisaSpy.setAuthData(authData);
		assertEquals("D7MM", settlementRecordVisaSpy.getData("auth_val_code"));
		
		
		authData.setAuthValCode("");
		settlementRecordVisaSpy.setData("auth_val_code", "R3L6");;
		settlementRecordVisaSpy.setAuthData(authData);
		assertEquals("R3L6", settlementRecordVisaSpy.getData("auth_val_code"));
	}
	
	@Test 
	public void testAuthValCodeNotNull_Tsys()  throws Exception {

		TridentAuthData authData = new TridentAuthData();
		settlementRecordVisaSpy.RecBatchType = mesConstants.MBS_BT_VITAL;
		settlementRecordVisaSpy.RecSettleRecType = SettlementRecord.SETTLE_REC_VISA;

		authData.setAuthValCode("R3L6");
		settlementRecordVisaSpy.setData("auth_val_code", "4321");;
		settlementRecordVisaSpy.setAuthData(authData);

		assertEquals("R3L6", settlementRecordVisaSpy.getData("auth_val_code"));
		
	}
}
