package com.mes.settlement;

import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.mockito.Mock;
import org.mockito.Spy;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;
import com.mes.database.SQLJConnectionBase;
import com.mes.config.DbProperties;
import com.mes.tools.DccRateUtil;
import org.mockito.Matchers;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.when;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.RoundingMode;
import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SQLJConnectionBase.class, DbProperties.class, InterchangeBase.class, InterchangeMasterCard.class, DccRateUtil.class, SettlementDb.class})
public class InterchangeMasterCardTest {
	@Spy
	InterchangeMasterCard imc = new InterchangeMasterCard();

	@Mock
	DccRateUtil dccRateUtil;

	@Spy
	IcInfo icInfo = new IcInfo();

	@BeforeClass
	public static void beforeClass() throws Exception {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	@Before
	public void setup() throws Exception {
		// Prepare classes for mocking of static methods.
		PowerMockito.mockStatic(DbProperties.class);
		PowerMockito.mockStatic(DccRateUtil.class);
		PowerMockito.mockStatic(SettlementDb.class);

		PowerMockito.suppress(PowerMockito.defaultConstructorIn(SQLJConnectionBase.class));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "initialize"));
		PowerMockito.suppress(PowerMockito.method(SQLJConnectionBase.class, "logEntry", String.class, String.class));

		PowerMockito.when(DbProperties.configure(anyString())).thenReturn(true);
		PowerMockito.when(DccRateUtil.getInstance(Matchers.any(Date.class))).thenReturn(dccRateUtil);
		PowerMockito.when(SettlementDb.getBestIcInfo(Matchers.any(SettlementRecord.class), anyString(), anyInt(), Matchers.any(Date.class), anyString(), anyString())).thenReturn(icInfo);
	}

	@Test
	public void testFXRate() throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Date batchDate = df.parse("2019-02-01");
		String cardType = "MB";
		String fundingCurrencyCode = "840";
		String currencyCode = "643";
		double transactionAmount = 10.0;
		String debitIndicator = "D";

		double buyRate = .5;
		double sellRate = .5;

		icInfo.setIcString("---75---123456789abcdef");

		SettlementRecord tran = new SettlementRecordMC();
		tran.setData("batch_date", batchDate);
		tran.setData("card_type", cardType);
		tran.setData("funding_currency_code", fundingCurrencyCode);
		tran.setData("currency_code", currencyCode);
		tran.setData("transaction_amount", transactionAmount);
		tran.setData("debit_credit_indicator", debitIndicator);

		PowerMockito.when(dccRateUtil.getBuyRate(anyString(), anyString(), anyString())).thenReturn(buyRate);
		PowerMockito.when(dccRateUtil.getSellRate(anyString(), anyString(), anyString())).thenReturn(sellRate);

		imc.getBestClass(tran, new ArrayList<>());

		BigDecimal expected = BigDecimal.valueOf(sellRate).setScale(2, RoundingMode.HALF_EVEN);
		BigDecimal actual = BigDecimal.valueOf(tran.getDouble("fx_rate")).setScale(2, RoundingMode.HALF_EVEN);

		assertEquals(expected.toString(), actual.toString());
	}
}
