package com.mes.maintenance;

import org.junit.Assert;
import org.junit.Test;

public class TridentProfileTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void tridentProfile() {
        // Given
        final int primitiveTid = 1;

        // When

        // old way
        final Integer oldWayIntegerObject = new Integer(primitiveTid);

        // Then
        Assert.assertTrue(MESSAGE, oldWayIntegerObject.equals(primitiveTid));
        Assert.assertTrue(MESSAGE, oldWayIntegerObject == primitiveTid);
    }
}
