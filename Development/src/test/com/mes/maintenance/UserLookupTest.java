package com.mes.maintenance;

import org.junit.Assert;
import org.junit.Test;

public class UserLookupTest {

    private static final String MESSAGE = "Pre- and post- Java 8 upgrade values should be equal.";

    @Test
    public void getData() {
        // Given
        long currentTimeMillis = System.currentTimeMillis();

        // When

        // old way
        final Long oldWayLongObject = new Long(currentTimeMillis);

        // Then
        Assert.assertTrue(MESSAGE, oldWayLongObject.equals(currentTimeMillis));
        Assert.assertTrue(MESSAGE, oldWayLongObject == currentTimeMillis);
    }
}
