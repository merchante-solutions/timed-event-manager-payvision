package com.mes.b2f;

import org.junit.Assert;
import org.junit.Test;

public class B2FTest {

	@Test
	public void registerDevice() {
		long longValue = 12L;
		Assert.assertEquals((new Long(longValue)).intValue(), (int) longValue);
	}
}
