package com.mes.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MeSQueryHandlerTest {

	@Before
	public void setup() {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}

	@Test
	public void testExecutePreparedUpdate_Date() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		Date testDate = new Date();

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setDate(1, new java.sql.Date((testDate).getTime()));
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testDate);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_Calendar() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		Calendar testCalendar = Calendar.getInstance();

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setTimestamp(1, new Timestamp(testCalendar.getTimeInMillis()), testCalendar);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testCalendar);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_String() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		String testString = "Chariot";

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setString(1, testString);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testString);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_Integer() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		Integer testInteger = 12345;

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setInt(1, testInteger);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testInteger);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_Double() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		Double testDouble = Double.valueOf(1.123456789);

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setDouble(1, testDouble);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testDouble);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_BigDecimal() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		BigDecimal testDecimal = BigDecimal.valueOf(1234.12444);

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setBigDecimal(1, testDecimal);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testDecimal);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}

	@Test
	public void testExecutePreparedUpdate_Long() throws SQLException {
		String sqlName = "sql name";
		String updateSql = "update table set value=? where x=y";
		Long testLong = Long.valueOf(1000000l);

		MeSQueryHandler queryHandler = Mockito.spy(MeSQueryHandler.class);

		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);

		when(mockJdbcConnection.prepareStatement(Mockito.eq(updateSql))).thenReturn(mockStatement);
		doNothing().when(mockStatement).setLong(1, testLong);
		when(mockStatement.executeUpdate()).thenReturn(Integer.valueOf(1));

		doReturn(mockJdbcConnection).when(queryHandler).getConnection();

		Integer updateCount = queryHandler.executePreparedUpdate(sqlName, updateSql, testLong);

		assertNotNull(updateCount);
		assertEquals(updateCount, Integer.valueOf(1));

	}
}
