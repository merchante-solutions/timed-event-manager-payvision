package com.mes.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;

public class MesQueryHandlerResultSetTest {
	@Before
	public void setup() {
		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}
	
	@Test
	public void testExecutePreparedStatement_NoRows() throws SQLException {
		String sqlName = "sql name";
		String preparedSql = "SELECT SESSIONTIMEZONE, CURRENT_TIMESTAMP FROM DUAL;";
		Date testDate = new Date();
		
		MesQueryHandlerResultSet queryHandler = Mockito.spy(MesQueryHandlerResultSet.class);
		
		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);
		
		ResultSet mockResultSet = Mockito.mock(ResultSet.class);
		when(mockResultSet.next()).thenReturn(Boolean.FALSE);
		when(mockStatement.executeQuery()).thenReturn(mockResultSet);
		
		doReturn(mockJdbcConnection).when(queryHandler).getConnection();
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);
		
		doNothing().when(mockStatement).setDate(1, new java.sql.Date((testDate).getTime()));
		
		MesResultSet resultSet = queryHandler.executePreparedStatement(sqlName, preparedSql, testDate);
		
		assertNotNull(resultSet);
		assertTrue(CollectionUtils.isEmpty(resultSet));
		
	}
	
	@Test
	public void testExecutePreparedStatement_OneRow() throws SQLException {
		String sqlName = "sql name";
		String preparedSql = "SELECT SESSIONTIMEZONE, CURRENT_TIMESTAMP FROM DUAL;";
		Date testDate = new Date();
		
		MesQueryHandlerResultSet queryHandler = Mockito.spy(MesQueryHandlerResultSet.class);
		
		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);
		ResultSetMetaData mockMetaData = Mockito.mock(ResultSetMetaData.class);
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("sessiontimezone");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("CURRENT_TIMESTAMP");
		doReturn(mockMetaData).when(mockResultSet).getMetaData();
		
		doReturn("America/New_York").when(mockResultSet).getObject(Mockito.eq(1));
		doReturn(new java.sql.Timestamp(testDate.getTime())).when(mockResultSet).getObject(Mockito.eq(2));
		mockResultSet.add(new HashMap<>());
		
		when(mockStatement.executeQuery()).thenReturn(mockResultSet);
		
		doReturn(mockJdbcConnection).when(queryHandler).getConnection();
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);
		
		doNothing().when(mockStatement).setDate(1, new java.sql.Date((testDate).getTime()));
		
		MesResultSet resultSet = queryHandler.executePreparedStatement(sqlName, preparedSql, testDate);
		
		assertNotNull(resultSet);
		assertTrue(CollectionUtils.isNotEmpty(resultSet));
		assertEquals(1,resultSet.size());
		
		assertTrue(resultSet.next());
		assertEquals("America/New_York",resultSet.getString("sessiontimezone"));
		assertEquals(testDate,resultSet.getDate("current_timestamp"));
	}
	
	@Test
	public void testExecutePreparedStatement_TwoRows() throws SQLException {
		String sqlName = "sql name";
		String preparedSql = "SELECT SESSIONTIMEZONE, CURRENT_TIMESTAMP FROM DUAL;";
		Date testDate = new Date();
		
		MesQueryHandlerResultSet queryHandler = Mockito.spy(MesQueryHandlerResultSet.class);
		
		Connection mockJdbcConnection = Mockito.mock(Connection.class);
		PreparedStatement mockStatement = Mockito.mock(PreparedStatement.class);
		ResultSetMetaData mockMetaData = Mockito.mock(ResultSetMetaData.class);
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(0).put("sessiontimezone", "America/New_York");
		mockResultSet.get(0).put("CURRENT_TIMESTAMP", new java.sql.Timestamp(testDate.getTime()));
		mockResultSet.add(new HashMap<>());
		mockResultSet.get(1).put("sessiontimezone", "America/Los Angeles");
		mockResultSet.get(1).put("CURRENT_TIMESTAMP", new java.sql.Timestamp(testDate.getTime()));
		List<String> columnList = Arrays.asList(new String[]{"sessiontimezone","CURRENT_TIMESTAMP"});
		Whitebox.setInternalState(mockResultSet, "columnList", columnList);
		when(mockMetaData.getColumnCount()).thenReturn(2);
		when(mockMetaData.getColumnName(Mockito.eq(1))).thenReturn("sessiontimezone");
		when(mockMetaData.getColumnName(Mockito.eq(2))).thenReturn("CURRENT_TIMESTAMP");
		
		doReturn(mockMetaData).when(mockResultSet).getMetaData();
		
		when(mockStatement.executeQuery()).thenReturn(mockResultSet);
		
		doReturn(mockJdbcConnection).when(queryHandler).getConnection();
		when(mockJdbcConnection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)).thenReturn(mockStatement);
		
		doNothing().when(mockStatement).setDate(1, new java.sql.Date((testDate).getTime()));
		
		MesResultSet resultSet = queryHandler.executePreparedStatement(sqlName, preparedSql, testDate);
		
		assertNotNull(resultSet);
		assertTrue(CollectionUtils.isNotEmpty(resultSet));
		assertEquals(2,resultSet.size());
		
		assertTrue(resultSet.next());
		assertEquals("America/New_York",resultSet.getString("sessiontimezone"));
		assertEquals(testDate,resultSet.getDate("current_timestamp"));
		
		assertTrue(resultSet.next());
		assertEquals("America/Los Angeles",resultSet.getString("sessiontimezone"));
		assertEquals(new java.sql.Date(testDate.getTime()),resultSet.getDate("current_timestamp"));
	}
}
