package com.mes.database;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class OracleConnectionPoolTest {

	@Test
	public void classFields() {
		int testInteger = 1;
		Integer autoBoxedInteger = testInteger;
		Assert.assertEquals(new Integer(testInteger), autoBoxedInteger);
	}

}
