package com.mes.api;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TridentApiTransactionTest {
	@Test
	public void requestAuthorizationGts() {
		int someInt = 123;
		long someLong = 0L;
		boolean someBoolean = true;
		Integer autoBoxedInteger = someInt;
		Long autoBoxedLong = 0L;
		Boolean autoBoxedBoolean = someBoolean;

		assertEquals(new Integer(someInt), autoBoxedInteger);
		assertEquals(new Long(someLong), autoBoxedLong);
		assertEquals(new Boolean(someBoolean), autoBoxedBoolean);
	}
}