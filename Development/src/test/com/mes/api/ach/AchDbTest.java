package com.mes.api.ach;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AchDbTest {

	@Test
	public void lookupNewTransitNumRecords() {
		// Given
		int someIntValue = 3423;
		Integer oldWay = new Integer(someIntValue);
		Integer newWay = someIntValue;

		// Then
		assertEquals("pre- and post- upgrade values should be the same", oldWay, newWay);
	}
}