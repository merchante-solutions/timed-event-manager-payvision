package com.mes.api.bml;

import org.junit.Assert;
import org.junit.Test;

public class BmlIdCacheTest {

    @Test
    public void add() {
        // Given
        int id = 4356908;

        // When

        // old way
        final Integer oldWayIntegerObject = new Integer(id);

        // Then
        Assert.assertTrue("Pre- and post- Java 8 upgrade values should be equal.", oldWayIntegerObject.equals(id));
        Assert.assertTrue("Pre- and post- Java 8 upgrade values should be equal.", oldWayIntegerObject == id);
    }
}
