package com.mes.api;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RefundTransactionTaskTest {
	protected String ExternalTranId = "8489324892384";

	@Test
	public void testDoTask() {
		int sampleInt = 88;
		boolean sampleBoolean = true;
		Integer autoBoxedInteger = sampleInt;
		Boolean autoBoxedBoolean = sampleBoolean;

		assertEquals(new Integer(sampleInt), autoBoxedInteger);
		assertEquals(new Boolean(sampleBoolean), autoBoxedBoolean);
		assertEquals(new Long(ExternalTranId), Long.valueOf(ExternalTranId));
	}

}