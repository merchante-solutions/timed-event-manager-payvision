package com.mes.calltag;

import static org.junit.Assert.assertEquals;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.batik.dom.util.HashTable;
import org.junit.Test;

public class CallTagDetailQueueBeanTest {
	private String testUrl = "";
	private HashTable deprecatedWay = new HashTable();
	private HashTable nonDeprecatedWay = new HashTable();
	private static final String KEY = "key";
	private static final int VALUE = 22;

	@Test
	@SuppressWarnings({"deprecation"})
	public void testGenerateQueueCommands() throws UnsupportedEncodingException, URISyntaxException {
		testUrl = "http://www.test.com?key1=value+1&key2=value%40%21%242&key3=value%253";
		URI uri = new URI(testUrl);
		assertEquals(URLEncoder.encode(uri.getRawQuery()), URLEncoder.encode(uri.getRawQuery(), StandardCharsets.UTF_8.toString()));
	}

	@Test
	@SuppressWarnings({"deprecation"})
	public void testAddCommand() {
		deprecatedWay.put(KEY, new Integer(VALUE));
		nonDeprecatedWay.put(KEY, VALUE);
		assertEquals(deprecatedWay.get(KEY), nonDeprecatedWay.get(KEY));
	}

}