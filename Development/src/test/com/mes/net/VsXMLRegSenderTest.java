package com.mes.net;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

public class VsXMLRegSenderTest {

	@Test
	@SuppressWarnings("deprecation")
	public void getMessagePostData() {
		String msg = "Some kind of XML message";
		try {
			Assert.assertEquals(URLEncoder.encode(msg, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(msg));
		} catch (UnsupportedEncodingException e) {
			fail("UnsupportedEncodingException");
		}
	}

}
