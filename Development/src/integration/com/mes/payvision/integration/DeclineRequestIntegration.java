package com.mes.payvision.integration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.payvision.DeclineRequest;
import com.mes.payvision.DeclineResponse;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesLocator;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesSoap_BindingStub;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, DeclineRequest.class, DeclinesLocator.class, DeclinesSoap_BindingStub.class})
public class DeclineRequestIntegration {
	String expectedResponse = "DeclineRecord [ req id: 0, mem id: 132983, proc tran id:1836667085, proc tran guid:77204e0a-fc83-416f-92ff-7f329c010ad1, tran type:Payment, track mem code:4d90600d91c74eb6bf0f24619e53d349, orig amt:100, orig cur code:826, merch amt:100, merch cur rate:0.866551, req date:Thu Apr 18 09:17:43 EDT 2019, proc date:Thu Apr 18 09:17:43 EDT 2019, card id:345896993, card guid:5d26a166-705c-4a6b-956d-390a6411058d, card type:VisaDebit, merch acct id:38965, cntry code:840, bank code:05, bank msg:Do not Honour ]";

	@Test
	public void test() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));

		DeclinesLocator service = spy(new DeclinesLocator());
		PowerMockito.whenNew(DeclinesLocator.class).withNoArguments().thenReturn(service);
		service.setEndpointAddress("DeclinesSoap", "http://localhost:8082/ReportsApiV1.1/Declines.asmx");
		doNothing().when(service).setEndpointAddress(anyString(), anyString());

		DeclineRequest declineReq = new DeclineRequest();
		declineReq.setStartDate(new Date());
		declineReq.setEndDate(new Date());
		declineReq.setMemberId(132983);
		declineReq.setCrMemberId(4319);
		declineReq.setCrMemberGuid("4d315e6d-03e8-4845-bf6a-5ebd714696f1");
		declineReq.sendRequest();

		DeclineResponse resp = (DeclineResponse) declineReq.getResponse();

		assertEquals(expectedResponse, resp.getRecords().get(0).toString());
	}
}
