package com.mes.payvision.integration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.payvision.ReconciliationRequest;
import com.mes.payvision.ReconciliationResponse;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsLocator;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsSoap_BindingStub;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, ReconciliationRequest.class, TransactionsLocator.class, TransactionsSoap_BindingStub.class})
public class ReconciliationRequestIntegration {
	String expectedResponse = "ReconciliationSummary [ merch acct id: 40881, curr code: 978, auth cnt: 1406, auth tot: 34495.14, cap cnt: 1396, cap tot: 35033.98, void cnt: 89, void tot: 1673.90, pay cnt: 0, pay tot: 0, refund cnt: 1, refund tot: 27.00, refer cnt: 0, refer tot: 0, xfer cnt: 0, xfer tot: 0 ]";

	@Test
	public void test() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));

		TransactionsLocator service = spy(new TransactionsLocator());
		PowerMockito.whenNew(TransactionsLocator.class).withNoArguments().thenReturn(service);
		service.setEndpointAddress("TransactionsSoap", "http://localhost:8082/ReportsApiV1.1/Transactions.asmx");
		doNothing().when(service).setEndpointAddress(anyString(), anyString());

		ReconciliationRequest reconcilReq = new ReconciliationRequest();
		reconcilReq.setStartDate(new Date());
		reconcilReq.setEndDate(new Date());
		reconcilReq.setMemberId(132983);
		reconcilReq.setCrMemberId(4319);
		reconcilReq.setCrMemberGuid("4d315e6d-03e8-4845-bf6a-5ebd714696f1");
		reconcilReq.sendRequest();

		ReconciliationResponse resp = (ReconciliationResponse) reconcilReq.getResponse();

		assertEquals(expectedResponse, resp.getSummaries().get(0).toString());
	}
}
