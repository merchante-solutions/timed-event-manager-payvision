package com.mes.payvision.integration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.mes.config.DbProperties;
import com.mes.payvision.ChargebackRequest;
import com.mes.payvision.ChargebackResponse;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksLocator;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksSoap_BindingStub;
import com.mes.support.LoggingConfigurator;

@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggingConfigurator.class, DbProperties.class, ChargebackRequest.class, ChargebacksLocator.class, ChargebacksSoap_BindingStub.class})
public class ChargebackRequestIntegration {
	String expectedResponse = "ChargebackRecord [ req id: 0, mem id: 132983, cb id: 9356493, case id: 9101200232, type id: 2, amt: 100, cur code: 840, proc date: Wed May 22 09:17:43 EDT 2019, reas code: 4837, card id: 409273163, card guid: 57f1501f-d0dc-4278-a422-e930f3d0b2b7, tran date: Wed May 22 09:17:43 EDT 2019, tran id: 1817596193, merch acct id: 38965, ChargebackEntryItem [ req id: 0, mem id: 132983, cb id: 9356493, key: DeadLine, value: 29.05.2019 ], ChargebackEntryItem [ req id: 0, mem id: 132983, cb id: 9356493, key: Comments, value: Test comments ] ]";

	@Test
	public void test() throws Exception {
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(LoggingConfigurator.class));
		PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DbProperties.class));

		ChargebacksLocator service = spy(new ChargebacksLocator());
		PowerMockito.whenNew(ChargebacksLocator.class).withNoArguments().thenReturn(service);
		service.setEndpointAddress("ChargebacksSoap", "http://localhost:8082/ReportsApiV1.1/Chargebacks.asmx");
		doNothing().when(service).setEndpointAddress(anyString(), anyString());

		ChargebackRequest chrgBckReq = new ChargebackRequest();
		chrgBckReq.setStartDate(new Date());
		chrgBckReq.setEndDate(new Date());
		chrgBckReq.setMemberId(132983);
		chrgBckReq.setCrMemberId(4319);
		chrgBckReq.setCrMemberGuid("4d315e6d-03e8-4845-bf6a-5ebd714696f1");
		chrgBckReq.sendRequest();

		ChargebackResponse resp = (ChargebackResponse) chrgBckReq.getResponse();

		assertEquals(expectedResponse, resp.getRecords().get(0).toString());
	}
}
