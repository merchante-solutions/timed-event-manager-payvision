package com.mes.startup;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.TridentTools;
import com.mes.tools.FileUtils;

/**
 * Loads the incoming IMGER file into DB and notifies CB team 
 * Scans IMGRJ file and alerts engineering
 * 
 */
public class AmexOptblueIMGERLoader extends EventBase {

	private static final long serialVersionUID = 1L;	
	static Logger log = Logger.getLogger(AmexOptblueIMGERLoader.class);

	/**
	 * Point of entry
	 * @return
	 */
	public boolean execute() {
		try {
			connect( true );
			
			loadFiles( );
			
		} catch (Exception e) {
			logEvent( this.getClass().getName(), "execute()", e.toString() );
			logEntry( "execute()", e.toString() );
		}
		
		return true;
	}
	
	/**
	 * Download the files from Amex SFTP site, load them into MeS system
	 * and archives
	 * @param loadType
	 */
	protected void loadFiles() {
		
		Sftp sftp = null;
		String filePrefix = null;
		
		String remoteFilename = null;
		String loadFilename = null;
		
		boolean notify = false;
		
		try {
			
			for(Map.Entry<String, String> entry : DisputeFilePrefixes.entrySet() ) {
				
				String loadType = entry.getKey();
				filePrefix = DisputeFilePrefixes.get( loadType );
				log.debug( "Begining loadType " + loadType );
			
				// get the Sftp connection
				sftp = getAmexSFTPConnection();
				log.debug( "Connected to Amex SFTP site" );
				
				EnumerationIterator enumi = new EnumerationIterator();
				Iterator<String> it = enumi.iterator( sftp.getNameListing( getFileMask( loadType ) ) );
				log.debug(getFileMask( loadType ));
				
				while ( it.hasNext() ) {
					
					// Re-check connection as we loop through because Amex SFTP site has a tendency to time out / drop connection after a couple of 
					// file downloads (as observed in Amex ESA downloads)
					if (!sftp.isConnected() ) {
						
						sftp = getAmexSFTPConnection();
						log.debug("Reconnected to Amex SFTP site");
					}
	
					remoteFilename = (String) it.next();
					loadFilename = generateFilename(filePrefix);
					
					sftp.download(loadFilename, remoteFilename);
					log.info("Downloaded " + remoteFilename + " as " + loadFilename);
					
	                if ( IMGER.equals(loadType) ){
	                	loadIMGERAndNotify( loadFilename );
	                	
	                } else if( IMGRJ.equals(loadType) ){
	                	loadIMGRJAndNotify( loadFilename );
	                	
	                }	                
					
	                archiveDailyFile(loadFilename);
					log.debug(loadFilename + " archived");
				}
			}
		} catch (Exception e) {
			logEntry("loadFiles()", e.toString());

		}
	}
		
	private void loadIMGERAndNotify( String inputFilename ) {
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		PreparedStatement pStmt2 = null;
						
		int paramIndex = 1;
	    int colIndex = 1;
	    
	    BufferedReader in = null;	    	    	    
	    String line = null;	 
	    String loadFilename = null;
	    long loadFileId = 0L;
	    
	    // The record level errors are sent to Chargeback / Retrieval Team
	    // The batch level errors are sent to Enginners
	    // Hence the recordLevelErrorMailBody and 
	    StringBuffer recordLevelErrorMailBody = new StringBuffer("");
	    StringBuffer batchLevelErrorMailBody = new StringBuffer("");
	    boolean isRetrieval = true;
	    
		try {
	    	loadFilename = FileUtils.getNameFromFullPath( inputFilename ).toLowerCase();
	    	if(isTestMode)
	    		loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		
	    	loadFileId = loadFilenameToLoadFileId( loadFilename );
	    	
	    	// As we have no date in the incoming file we assume the load file date to be the incoming date 
	    	String filePrefix = DisputeFilePrefixes.get(IMGER);
			int prefixLength = filePrefix.length();
			
	    	Date originalFileLoadDate = 
					new Date(DateTimeFormatter.parseDate(loadFilename.substring((prefixLength+1),(prefixLength+1+6)),"MMddyy").getTime()) ;

	    	in = new BufferedReader( new FileReader(inputFilename) );

	    	pStmt2 = con.prepareStatement( INSERT_AMEX_OPTB_IMGER.toString() );
	    	
	    	while( ( line = in.readLine() ) != null ) {
	    			    		
	    		// If record is marked as 'ERROR' then process it
	    		if( line.length() > 13 && ERROR.equals( line.substring(8, 13) ) ) {
	    			
	    			ErrorInfo data = new ErrorInfo(line);
	    			
	    			// Look up the data based on whether it is a Inquiry or Chargeback
	    			String query = GET_RET_ERROR_DETAILS.toString();
	    			paramIndex = 1;
	    			colIndex = 1;
	    			
	    			pStmt = con.prepareStatement( GET_IF_BATCH_ID_IS_CB_OR_RET.toString() );
	    			pStmt.setLong( paramIndex++, data.getBatchId() );
	    			pStmt.setLong( paramIndex++, data.getBatchId() );
	    			
	    			rs = pStmt.executeQuery();
	    			if(rs.next()) {
	    				if( RETRIEVAL.equals(rs.getString( colIndex )) ) {
	    					query = GET_RET_ERROR_DETAILS.toString();
	    					isRetrieval = true;
	    				} else {
	    					query = GET_CB_ERROR_DETAILS.toString();
	    					isRetrieval = false;
	    				};
	    			}
	    			
	    			pStmt.close();
	    			
	    			pStmt = con.prepareStatement( query );	    					
					paramIndex = 1;
					colIndex = 1;
					
					pStmt.setString( paramIndex++, data.getSeAccountNo() );
					pStmt.setString( paramIndex++, data.getCaseNo() );
					pStmt.setString( paramIndex++, data.getCmAcctNo() );
					pStmt.setLong( paramIndex++, data.getBatchId() );
					pStmt.setString( paramIndex++, data.getFileName() );
					
					rs = pStmt.executeQuery();
					
					// Insert the data into IMGER table
					if( rs.next() ) {
						long loadSec = rs.getLong( colIndex++ );
						long docId  = rs.getLong( colIndex++ );
						String caseNo = rs.getString( colIndex++ );
						String cardNumber = rs.getString( colIndex++ );
						String cardNumberEnc = rs.getString( colIndex++ ); 
						String seNumb = rs.getString( colIndex++ );
						
						paramIndex = 1;						
						//rec_id,  cm_acct_no,  cm_acct_no_enc,  case_no,  se_acct_no,  batch_id,  file_list,  error_list,  load_sec
						pStmt2.setString(paramIndex++, TridentTools.encodeCardNumber(cardNumber));
						pStmt2.setString(paramIndex++, cardNumberEnc);
						pStmt2.setString(paramIndex++, caseNo);
						pStmt2.setString(paramIndex++, seNumb);
						pStmt2.setLong(paramIndex++, data.getBatchId());
						pStmt2.setString(paramIndex++, data.getFileName());
						pStmt2.setString(paramIndex++, data.getError().toString());
						pStmt2.setLong(paramIndex++, loadSec);
						pStmt2.setString(paramIndex++, loadFilename);
						pStmt2.setLong(paramIndex++, loadFileId);
						pStmt2.setDate(paramIndex++, originalFileLoadDate);
						pStmt2.setString(paramIndex++, isRetrieval ? "R" : "C");
												
						int count = pStmt2.executeUpdate();
						log.debug(count + " row inserted for " + data.toString());
						
						recordLevelErrorMailBody.append(data.toString() + NL);						
					}
	    		} else if(line.indexOf(BATCH_ERROR_STATUS) >= 0) {
	    			batchLevelErrorMailBody.append(line  + NL);
	    		}
	    	}
	    	
	    	if(!"".equals( recordLevelErrorMailBody.toString() ))
	    		notify( loadFilename, isRetrieval ? IMGER_FOR_RESPN : IMGER_FOR_CBDIS, recordLevelErrorMailBody);
	    	
	    	if(!"".equals(batchLevelErrorMailBody.toString()))
	    		notify( loadFilename, IMGER_RJ, batchLevelErrorMailBody);
	    	
	    } catch(Exception e) {
		  logEntry("loadIMGERAndNotify(" + loadFilename + ")", e.toString());
		
	    } finally {
	    	try {
	    		if(in != null) in.close();
	    	} catch ( Exception ignore ) {}
	    	
	    	try	{  
	    	  if (pStmt != null ) pStmt.close(); 
	    	} catch( Exception ignore ) {}
	      
	    	try { 
	    	  if (rs != null ) rs.close(); 
	    	} catch( Exception ignore ) {}
	    	
	    	try	{  
		    	  if (pStmt2 != null ) pStmt2.close(); 
		    	} catch( Exception ignore ) {}
	            
	  	}
		
	}
	
	private void loadIMGRJAndNotify( String inputFilename ) {
		
		BufferedReader in = null;	    	    	    
	    String line = null;	 
	    
	    String loadFilename = null;
	    long loadFileId = 0L;
	    
	    StringBuffer recordLevelErrorMailBody = new StringBuffer("");
	    boolean isRetrieval = true;
	    
		try {
	    	loadFilename = FileUtils.getNameFromFullPath( inputFilename ).toLowerCase();
	    	if(isTestMode)
	    		loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		
	    	loadFileId = loadFilenameToLoadFileId( loadFilename );
	    				
	    	in = new BufferedReader( new FileReader(inputFilename) );
	    		    	
	    	while( ( line = in.readLine() ) != null ) {
	    		recordLevelErrorMailBody.append(line + NL);	    		
	    	}
	    	
	    	if(!"".equals( recordLevelErrorMailBody.toString() ))
	    		notify( loadFilename, IMGER_RJ, recordLevelErrorMailBody);
	    	
	    } catch(Exception e) {
		  logEntry("loadIMGERAndNotify(" + loadFilename + ")", e.toString());
		
	    } finally {
	    	try {
	    		if(in != null) in.close();
	    	} catch ( Exception ignore ) {}
	    	    
	  	}
		
	}
	
	/*
	 * Helper functions
	 */
	
	/**
	 * Amex sends in files with specific prefixes. This method maps the load to the file prefix
	 * @param loadType
	 * @return
	 */
	protected String getFileMask(String loadType){    	
    	String fileMask = "";
    	String filePrefix = isTestMode ? TEST_PREFIX : PROD_PREFIX;
    			
		if( loadType.equals(IMGER) ){
			fileMask = filePrefix + "." + loadType + ".*";
			
		} else if(loadType.equals(IMGRJ)){
			fileMask = "([^ ]*).*RJ";
			
		}
		
    	return fileMask;
    }	
	
	
	protected Sftp  getAmexSFTPConnection() throws Exception {
		Sftp sftp =  getSftp(	
						MesDefaults.getString( MesDefaults.DK_AMEX_HOST ),
						MesDefaults.getString( MesDefaults.DK_AMEX_USER ),
						MesDefaults.getString( MesDefaults.DK_AMEX_PASSWORD ),
						MesDefaults.getString( MesDefaults.DK_AMEX_INCOMING_PATH ), 
						false // !binary
					);
				
		return sftp;
	}
	
	
	/**
	 * Notifications to Dispute department regarding the load
	 * @param loadFilename
	 */
	protected void notify( String loadFilename, int errorType, StringBuffer buffer ) {
		
	    int addrId = -1;
	    String subject = null;
	    
	    try {
	    	
	      if ( errorType == IMGER_FOR_CBDIS ) {
	    	  addrId    = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
	    	  subject   = "Amex OPTB Chargebacks IMGER";
	        
	      } else if ( errorType == IMGER_FOR_RESPN ) {
	    	  addrId = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;
	    	  subject = "Amex OPTB Retrievals IMGER";
	        
	      } else if ( errorType == IMGER_RJ ) {
	    	  addrId = MesEmails.MSG_ADDRS_AMEX_IMGRJ;
	    	  subject = "Amex OPTB IMGRJ or IMGER with Batch Error Status";
		  }
	      
	      MailMessage msg = new MailMessage();
	      msg.setAddresses(addrId);
	      msg.setSubject(subject + " - " + loadFilename);
	      msg.setText(buffer.toString());
	      msg.send();
	      
	      log.debug("Email Sent to address id = " + addrId + " with body as follows ");
	      log.debug(buffer.toString());
	      
	    } catch( Exception e ) {
	      logEntry("notify(" + loadFilename + ")",e.toString());
	      
	    }
	}

	
	public static void main(String args[]) {
		AmexOptblueIMGERLoader imageErrorRejectLoader = null;
		int idx = 0;
		
		try {
			if (args.length > 0 && args[idx].equals("printtestprops")) {
					EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
							MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_INCOMING_PATH });				
			} else if (args.length > 0 && args[idx].equals("test")) {
				isTestMode = true;
				idx = 1;
			}

			imageErrorRejectLoader = new AmexOptblueIMGERLoader();
			imageErrorRejectLoader.connect();
							
			if (IMGER.equals(args[idx])) {
				imageErrorRejectLoader.loadIMGERAndNotify(args[++idx]);
					
			} else if (IMGRJ.equals(args[idx])) {				
				imageErrorRejectLoader.loadIMGRJAndNotify(args[++idx]);
					
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				imageErrorRejectLoader.cleanUp();
			} catch (Exception e) { }
						
		}
		
		Runtime.getRuntime().exit(0);
	}
	
	
	// Load Types
	private final static String	IMGER = "IMGER";
	private final static String	IMGRJ = "IMGRJ";
	
	private final static String	CHARGEBACK = "CHARGEBACK";
	private final static String	RETRIEVAL = "RETRIEVAL";
			
	// Mes Defined Dispute File Prefixes
	private static String IMG_FILE_PREFIX = "amex_optb_img";
	private static String IMGRJ_FILE_PREFIX = "amex_optb_imgrj";
		
	private static final Map<String, String> DisputeFilePrefixes = new HashMap<String, String>() {
		{
			put(IMGER, IMG_FILE_PREFIX + "3941" ); 	// Amex Optblue Incoming IMGER File
			put(IMGRJ, IMGRJ_FILE_PREFIX ); 	// Amex Optblue Incoming IMGRJ File
		}
	};
	
	// Amex Defined File Prefixes
	private final static String PROD_PREFIX = "MERCHESOBPRD";
	private final static String TEST_PREFIX = "MERCHESOBTST";
	
	// Loading mode
	private static boolean isTestMode = false;
	
	// IMGER type
	private static final int IMGER_FOR_CBDIS = 1;
	private static final int IMGER_FOR_RESPN = 2;
	private static final int IMGER_RJ = 3;
	
	private static final String ERROR = "ERROR";
	private static final String BATCH_ERROR_STATUS = "Batch Error Status";
	
	// ************** SQL Queries	
	private static final StringBuffer GET_IF_BATCH_ID_IS_CB_OR_RET = new StringBuffer("");  
	
	private static final StringBuffer GET_CB_ERROR_DETAILS = new StringBuffer("");
	private static final StringBuffer GET_RET_ERROR_DETAILS = new StringBuffer("");
	
	private static final StringBuffer INSERT_AMEX_OPTB_IMGER = new StringBuffer("");
	
	private static final String NL = " \n";
		
	static {
		GET_IF_BATCH_ID_IS_CB_OR_RET.append(
			"select '" + RETRIEVAL + "' load_type" + NL +
			"from network_retrieval_amex_optb" + NL +
			"where batch_id = ?" + NL +
			"union" + NL +
			"select '" + CHARGEBACK + "' load_type" + NL +
			"from network_chargeback_amex_optb" + NL +
			"where batch_id = ?" + NL );
		
		GET_CB_ERROR_DETAILS.append(
				"select ncao.load_sec load_sec,cd.doc_id doc_id, ncao.case_number case_no, dukpt_decrypt_wrapper(ncao.card_number_enc) cm_acct_no, ncao.card_number_enc cm_acct_no_enc, ncao.se_numb se_acct_no,  d.doc_name doc_name " + NL + 
				"from network_chargeback_amex_optb ncao, network_chargebacks nc, chargeback_docs cd, document d"  + NL + 
				"where ncao.se_numb = nvl(?, ncao.se_numb)"  + NL + 
				"    and ncao.case_number = nvl(?, ncao.case_number)"  + NL + 
				"    and ncao.card_number_enc =    nvl(dukpt_encrypt_wrapper(?), ncao.card_number_enc)"  + NL + 
				"    and ncao.batch_id = ?"  + NL + 
				"    and ncao.load_sec = nc.cb_load_sec"  + NL + 
				"    and ncao.load_sec = cd.cb_load_sec"  + NL + 
				"    and d.doc_id (+) = cd.doc_id"  + NL + 
				"    and ? like ('%' || d.doc_name || '%') " + NL 
			);
		
		GET_RET_ERROR_DETAILS.append(
			"select nrao.load_sec load_sec, cd.doc_id doc_id, nrao.inquiry_case_number case_no, dukpt_decrypt_wrapper(nrao.card_number_enc) cm_acct_no, nrao.card_number_enc cm_acct_no_enc, nrao.se_number se_acct_no,  d.doc_name doc_name" + NL + 
			"from network_retrieval_amex_optb nrao , network_retrievals nr, chargeback_docs cd, document d" + NL + 
			"where nrao.se_number = nvl (? , nrao.se_number)" + NL +
			"    and nrao.inquiry_case_number = nvl (? , nrao.inquiry_case_number)" + NL + 
			"    and nrao.card_number_enc = nvl(dukpt_encrypt_wrapper(?), nrao.card_number_enc)" + NL + 
			"    and nrao.batch_id = ?" + NL + 
			"    and nrao.load_sec = nr.retr_load_sec" + NL + 
			"    and nrao.load_sec = cd.cb_load_sec" + NL + 
			"    and d.doc_id = cd.doc_id" + NL +
			" 	 and ? like ('%' || d.doc_name || '%') "
			);
		
		INSERT_AMEX_OPTB_IMGER.append(
			//"insert into amex_optb_imger (  rec_id,  cm_acct_no,  cm_acct_no_enc,  case_no,  se_acct_no,  batch_id,  file_list,  error_list,  load_sec) " + NL + 
			//"values (amex_optb_imger_rec_id_seq.nextval,  ? , ?, ?, ?, ? , ?, ?, ?)" );
			"insert into amex_optb_imger (  rec_id,  cm_acct_no,  cm_acct_no_enc,  case_no,  se_acct_no,  batch_id,  file_list,  error_list,  load_sec, load_filename, load_file_id, incoming_date, dispute_type) " + NL + 
			"values (amex_optb_imger_rec_id_seq.nextval,  ? , ?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?)" );
	}	
	
	private static final String BTCH_ID = "BTCH_ID=";
	private static final String CASE_NO = "CASE_NO=";
	private static final String CM_ACCT_NO = "CM_ACCT_NO=";
	private static final String SE_ACCT_NO = "SE_ACCT_NO=";
	private static final String FILENAME = "Filename=";
	private static final String ERROR_L = "ERROR=";
		
	private class ErrorInfo {
		
		private long batchId = 0l;
		private String caseNo = null;
		private String cmAcctNo = null;
		private String seAccountNo = null;
		private String fileName = null;
		private StringBuffer error = new StringBuffer("");
		
		ErrorInfo( String data ) {
			StringTokenizer st = new StringTokenizer(data, "|");
			
			while(st.hasMoreTokens()) {
				String dataFragment = st.nextToken();
				
				if ( dataFragment.startsWith( BTCH_ID ) ) {
					batchId = Long.parseLong( dataFragment.substring( BTCH_ID.length() ) );
					
				} else if ( dataFragment.startsWith( CASE_NO ) ) {
					caseNo = dataFragment.substring( CASE_NO.length() );
							
				} else if ( dataFragment.startsWith( CM_ACCT_NO ) ) {
					cmAcctNo = dataFragment.substring( CM_ACCT_NO.length() );
							
				} else if ( dataFragment.startsWith( SE_ACCT_NO ) ) {
					seAccountNo = dataFragment.substring( SE_ACCT_NO.length() );
							
				} else if ( dataFragment.startsWith( FILENAME ) ) {
					fileName = dataFragment.substring( FILENAME.length() );
							
				} else if ( dataFragment.startsWith( ERROR_L ) ) {
					if(error.length() > 0)
						error.append("; ");
					error.append(dataFragment.substring( ERROR_L.length() ));
							
				}
			}	    
		}

		public long getBatchId() {
			return batchId;
		}

		public String getCaseNo() {
			return caseNo;
		}

		public String getCmAcctNo() {
			return cmAcctNo;
		}

		public String getSeAccountNo() {
			return seAccountNo;
		}

		public String getFileName() {
			return fileName;
		}

		public StringBuffer getError() {
			return error;
		}

		public String toString() {			
			StringBuffer buf = new StringBuffer("");
			
			buf.append("[ batchId = " + batchId + ", ");
			buf.append("caseNo = " + caseNo + ", ");
			buf.append("cmAcctNo = " + TridentTools.encodeCardNumber( cmAcctNo ) + ", ");
			buf.append("seAccountNo = " + seAccountNo + ", ");
			buf.append("error = " + error.toString() + "] ");
			buf.append("fileName = " + fileName + "] ");
			
			return buf.toString();
		}
	}
}