/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/forms/HiddenDateField.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-05-27 11:12:17 -0700 (Fri, 27 May 2011) $
  Version            : $Revision: 18862 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;


public class HiddenDateField extends DateField
{
  public HiddenDateField(String fname)
  {
    super(fname,fname,10,10,true);
  }

  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"hidden\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("value=\"" + getRenderData() + "\" ");
    html.append(">");
    return html.toString();
  }
}
