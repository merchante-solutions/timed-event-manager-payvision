/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlImage.java $

  Description:

  HtmlImage
  
  Html element that renders an html image tag.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlImage extends HtmlTag
{
  // add image properties
  {
    addProperty("align");
    addProperty("alt");
    addProperty("title");
    addProperty("border","0");
    addProperty("height");
    addProperty("hspace");
    addProperty("id");
    addProperty("ismap");
    addProperty("longdesc");
    addProperty("lowsrc");
    addProperty("src");
    addProperty("usemap");
    addProperty("vspace");
    addProperty("width");
  }
  
  public void setImage(String imageUrl)
  {
    setProperty("src",imageUrl);
  }
  
  public void setWidth(int width)
  {
    try
    {
      setProperty("width",Integer.toString(width));
    }
    catch (Exception e)
    {
    }
  }
  
  public void setHeight(int height)
  {
    try
    {
      setProperty("height",Integer.toString(height));
    }
    catch (Exception e)
    {
    }
  }

  public String getName()
  {
    return "img";
  }
}
