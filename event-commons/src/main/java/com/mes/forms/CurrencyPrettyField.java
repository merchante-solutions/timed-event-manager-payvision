/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/CurrencyField.java $

  Description:
  
  CurrencyField

  Customizes a NumberField to be a currency field with two digits of 
  precision.  Data is zero padded.  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class CurrencyPrettyField extends CurrencyField
{
  public CurrencyPrettyField(String fname, 
                             String label, 
                             int length, 
                             int htmlSize, 
                             boolean nullAllowed)
  {
    super(fname, label, length, htmlSize, nullAllowed);
  }
  
  public CurrencyPrettyField(String fname,
                             int length,
                             int htmlSize,
                             boolean nullAllowed)
  {
    super(fname, length, htmlSize, nullAllowed);
  }
  
  public CurrencyPrettyField(String fname,
                             String label,
                             int length,
                             int htmlSize,
                             boolean nullAllowed,
                             float lbound,
                             float ubound)
  {
    super(fname, label, length, htmlSize, nullAllowed, lbound, ubound);
  }
  
  protected String processData(String rawData)
  {
    try
    {
      StringBuffer prettyData = new StringBuffer(super.processData(rawData));
      
      prettyData.insert(0, '$');
      
      return prettyData.toString();
    }
    catch(Exception e)
    {
      logEntry("processData("+ rawData + ")", e.toString());
    }
    
    // return raw data as is because there were problems processing it
    return rawData;
  }
}
