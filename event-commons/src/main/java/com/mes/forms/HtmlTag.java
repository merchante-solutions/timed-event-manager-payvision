/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlTag.java $

  Description:
  
  HtmlTag

  Basic html element.  Abstract: defines interface for rendering self as
  html and naming self.  Provides functionality for defining and managing
  tag properties.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-22 10:31:52 -0800 (Thu, 22 Feb 2007) $
  Version            : $Revision: 13480 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public abstract class HtmlTag implements HtmlRenderable
{
  private HtmlProperties props = new HtmlProperties();
  
  {
    addProperty("style");
    addProperty("class");
    addProperty("onAbort");
    addProperty("onBlur");
    addProperty("onChange");
    addProperty("onClick");
    addProperty("onError");
    addProperty("onFocus");
    addProperty("onLoad");
    addProperty("onMouseOut");
    addProperty("onMouseOver");
    addProperty("onSelect");
    addProperty("onSubmit");
    addProperty("onUnload");
  }

  public void setProperties(HtmlProperties newProps)
  {
    props.setAll(newProps);
  }
  
  public void setProperties(String tag, HtmlProperties newProps)
  {
    if (getName().equals(tag))
    {
      props.setAll(newProps);
    }
  }

  public void setProperty(String name, String value)
  {
    if (name != null)
    {
      props.set(name,(value != null ? value : ""));
    }
  }
  
  public void setProperty(String tag, String name, String value)
  {
    if (getName().equals(tag))
    {
      setProperty(name,value);
    }
  }
  
  protected void addProperty(String name)
  {
    props.add(name);
  }
  
  protected void addProperty(String name, String value)
  {
    addProperty(name);
    setProperty(name,value);
  }
  
  public void clearProperty(String name)
  {
    props.clear(name);
  }
  
  protected void copyProperties(HtmlTag element)
  {
    props.setAll(element.props);
  }
  
  protected String renderProperties()
  {
    return props.toString();
  }
  
  public HtmlTag getClone()
  {
    HtmlTag clone = null;
    
    try
    {
      clone = (HtmlTag)this.getClass().newInstance();
      clone.copyProperties(this);
    }
    catch (Exception e)
    {
    }
    return clone;
  }
  
  protected String renderTag()
  {
    StringBuffer html = new StringBuffer();
    return "<" + getName() + renderProperties() + ">";
  }
  
  public String renderHtml()
  {
    return renderTag();
  }
  
  public abstract String getName();
}