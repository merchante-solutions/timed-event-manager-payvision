/*@lineinfo:filename=StateDropDownTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/StateDropDownTable.sqlj $

  Description:
  
  StateDropDownTable
  
  Loads a list of state abbreviations from the countrystate db table
  into a drop down table.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class StateDropDownTable extends DropDownTable
{
  public StateDropDownTable()
  {
    getData();
  }

  protected boolean getData()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      addElement("","--");
      
      connect();
  
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    countrystate_code cc1,
//                    countrystate_code cc2
//          from      countrystate
//          order by  countrystate_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    countrystate_code cc1,\n                  countrystate_code cc2\n        from      countrystate\n        order by  countrystate_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.StateDropDownTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.StateDropDownTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
  
      rs = it.getResultSet();
  
      while (rs.next())
      {
        addElement(rs);
        getOk = true;
      }
  
      it.close();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() 
        + "::getData()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }

    return getOk;
  }
}/*@lineinfo:generated-code*/