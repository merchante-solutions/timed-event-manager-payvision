/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/CurrencyField.java $

  Description:
  
  CurrencyField

  Customizes a NumberField to be a currency field with two digits of 
  precision.  Data is zero padded.  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.text.NumberFormat;
import java.util.StringTokenizer;

public class CurrencyField extends Field
{
  /*
  ** public class CurrencyRangeValidation implements Validation
  **
  ** Validates a number field against a numeric range INCLUSIVE of both a lower bound and upper bound.
  ** Field data is cast to a float so that this validation may apply to both integers decimal numbers.
  */
  public class CurrencyRangeValidation implements Validation
  {
    float lbound;
    float ubound;
    
    private String errorText = "";
    
    public CurrencyRangeValidation(float lbound,float ubound)
    {
      this.lbound=lbound;
      this.ubound=ubound;
    }
    
    public boolean validate(String fieldData)
    {
      boolean isValid = true;

      try
      {
        // if data is null or empty then don't do validation
        // (evaluates as valid, let NotEmptyValidation handle it from here)
        if (fieldData != null && fieldData.length() > 0)
        {
          Float test;
          try
          {
            test = Float.valueOf(fieldData);
            
            if (test < lbound || test > ubound)
            {
              NumberFormat nf = NumberFormat.getCurrencyInstance(); 
              throw new Exception(
                "Currency value must be within the range: " 
                + nf.format(lbound) + " - " + nf.format(ubound));
            }
          }
          catch (NumberFormatException ne)
          {
            throw new Exception("Not a valid currency value.");
          }
        }
      
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() <= 0)
        {
          errorText = e.toString();
        }
        isValid = false;
      }
    
      return isValid;
      
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  
  }
  
  /*
  ** public class CurrencyValidation implements Validation
  **
  ** A validation object that validates that a string represents an amount
  ** of currency.
  */
  public class CurrencyValidation implements Validation
  {
    // represents the total number of digits allowed
    // (i.e. size = 5 allows 123.45)
    int size;
  
    public CurrencyValidation(int size)
    {
      this.size = size;
    }
  
    private String errorText = "";
  
    /*
    ** public boolean validate(String fieldData)
    **
    ** Determines if fieldData can convert to a numeric value representing
    ** a dollar amount.
    **
    ** RETURNS: true if fieldData contains a valid dollar amount, else false. 
    */
    public boolean validate(String fieldData)
    {
      boolean isValid = true;

      try
      {
        // if data is null or empty then don't do validation
        // (evaluates as valid, let NotEmptyValidation handle it from here)
        if (fieldData != null && fieldData.length() > 0)
        {
          // remove surrounding white space chars (if any)
          fieldData = fieldData.trim();
          
          // field should look like <dollar>.<cents>
          int dollarSize = 0;
          int centsSize = 0;
          boolean hitDot = false;
          for (int i = 0; i < fieldData.length(); ++i)
          {
            char ch = fieldData.charAt(i);
            
            if (!Character.isDigit(ch))
            {
              // account for negative numbers and Dollar sign (for pretty fields)
              if(i==0 && (ch=='-' || ch=='$'))
                continue;
            
              if (hitDot || ch !='.')
              {
                throw new Exception("Not a valid number");
              }
              hitDot = true;
            }
              
            if (!hitDot)
            {
              if (++dollarSize > size - 2)
              {
                throw new Exception("Dollar amount is too large");
              }
            }
            else if (ch != '.')
            {
              if (++centsSize > 2)
              {
                throw new Exception("Too many cents digits");
              }
            }
          }
        }
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() <= 0)
        {
          errorText = e.toString();
        }
        isValid = false;
      }
    
      return isValid;
    }
  
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public CurrencyField(String fname,
                       String label,
                       int length,
                       int htmlSize,
                       boolean nullAllowed)
  {
    super(fname,label,length + 1,htmlSize,nullAllowed);
    addValidation(new CurrencyValidation(length));
  }
  public CurrencyField(String fname,
                      int length,
                      int htmlSize,
                      boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }
  
  public CurrencyField(String fname,
                      String label,
                      int length,
                      int htmlSize,
                      boolean nullAllowed,
                      float lbound,
                      float ubound)
  {
    super(fname,label,length + 1,htmlSize,nullAllowed);
    addValidation(new CurrencyValidation(length));
    addValidation(new CurrencyRangeValidation(lbound,ubound));
  }
  public CurrencyField(String fname,
                      int length,
                      int htmlSize,
                      boolean nullAllowed,
                      float lbound,
                      float ubound)
  {
    this(fname,fname,length,htmlSize,nullAllowed,lbound,ubound);
  }

  /*
  ** protected String processData(String rawData)
  **
  ** Formats the raw data into dollars and cents.
  **
  ** RETURNS: formatted currency amount if able, else raw data as is.
  */
  protected String processData(String rawData)
  {
    try
    {
      // test to make sure the data is a parsable float
      Float.toString(Float.parseFloat(rawData));
      
      // make sure there is a decimal followed by two digits
      StringTokenizer st = new StringTokenizer(rawData,".");
      String dollar = "0";
      if (rawData.charAt(0) != '.')
      {
        dollar = st.nextToken();
      }
      StringBuffer cents = new StringBuffer("");
      if (st.hasMoreTokens())
      {
         cents.append(st.nextToken());
      }
      while (cents.length() < 2)
      {
        cents.append("0");
      }
      
      // return currency formatted as dollars and cents
      return (dollar + "." + cents.toString());
    }
    catch (Exception e) { }
    
    // must've been problem with rawData, return it as is
    return rawData;
  }
}
