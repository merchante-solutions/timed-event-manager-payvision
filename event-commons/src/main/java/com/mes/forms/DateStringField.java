/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DateStringField.java $

  Description:

  DateStringField

  A specialized FieldGroup that represents a date.  It may be configured to
  represent MM/YY or MM/DD/YY.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-21 16:31:22 -0700 (Mon, 21 Jul 2008) $
  Version            : $Revision: 15146 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Stack;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import com.mes.tools.DBGrunt;

public class DateStringField extends FieldGroup
{
  // log4j initialization
  static Logger log =
    Logger.getLogger(DateStringField.class);

  private boolean monthYear = false;

  private Field   monthField;
  private Field   dayField;
  private Field   yearField;

  public class DateValidation implements Validation
  {
    private String errorText;

    public boolean validate(String fdata)
    {
      if (fdata == null || fdata.length() == 0)
      {
        return true;
      }

      // make sure date is set in proper form
      Date date = getUtilDate();
      if (date == null)
      {
        errorText = "Date must be in the format "
          + (monthYear ? "MM/YYYY" : "MM/DD/YYYY");
        return false;
      }

      // don't allow dates before year 1800
      int year = yearField.asInteger();
      if (year < 1800)
      {
        errorText = "Invalid date";
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }

  public class CurrentDateValidation implements Validation
   {
    private String errorText;

    public boolean validate(String fdata)
    {
      if (fdata == null || fdata.length() == 0)
      {
        return true;
      }

      // make sure date is set in proper form
      Date date = getUtilDate();
      if (date == null)
      {
        errorText = "Date must be in the format "
          + (monthYear ? "MM/YYYY" : "MM/DD/YYYY");
        return false;
      }

      GregorianCalendar cal = new GregorianCalendar();

      cal.setTime(DBGrunt.getCurrentDate());

      cal.roll(Calendar.DAY_OF_YEAR, -1);

      Date yesterday = cal.getTime();

      //log.debug("date = "+date.getTime());
      //log.debug("yesterday = "+yesterday.getTime());

      if (date.before(yesterday))
      {
        errorText = "Date cannot be earlier than today.";
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }

  public DateStringField(String fname,
                   String label,
                   boolean nullAllowed,
                   boolean monthYear)
  {
    super(fname,label);

    this.length       = (monthYear ? 7 : 10);
    this.htmlSize     = (monthYear ? 6 : 9);
    this.nullAllowed  = nullAllowed;
    this.monthYear    = monthYear;

    if (!nullAllowed)
    {
      addValidation(new RequiredValidation(),"required");
    }

    addValidation(new DateValidation());

    monthField  = new Field(fname + "Month",2,2,true);
    dayField    = new Field(fname + "Day",2,2,true);
    yearField   = new Field(fname + "Year",4,4,true);

    add(monthField);
    add(dayField);
    add(yearField);
  }
  public DateStringField(String fname,
                   boolean nullAllowed,
                   boolean monthYear)
  {
    this(fname,fname,nullAllowed,monthYear);
  }
  public DateStringField(String fname,
                   boolean nullAllowed)
  {
    this(fname,nullAllowed,false);
  }

  private Date parseDateString(String dateString)
  {
    ParsePosition pos = new ParsePosition(0);
    SimpleDateFormat sdf
      = new SimpleDateFormat((monthYear ? "MM/yy" : "MM/dd/yy"));
    return sdf.parse(dateString,pos);
  }

  private String formatDateString(Date date)
  {
    if (date != null)
    {
      SimpleDateFormat sdf
        = new SimpleDateFormat((monthYear ? "MM/yyyy" : "MM/dd/yyyy"));
      return sdf.format(date);
    }
    return "";
  }

  public String getData()
  {
    // if util date is available, return a formatted string of it
    Date date = getUtilDate();
    if (date != null)
    {
      return formatDateString(date);
    }

    // return raw data
    return super.getData();
  }

  private String parseData(String rawData)
  {
    String parseData = null;

    // try to parse the raw data into a date
    Date date = parseDateString(rawData);

    // if date parsed, parse it back into the correct format
    if (date != null)
    {
      parseData = formatDateString(date);
    }

    // return the result of the parsing (null or formatted date string)
    return parseData;
  }

  protected String processData(String rawData)
  {
    // make sure data is not null
    if (rawData != null)
    {
      // see if the data can be parsed as a date
      String parseData = parseData(rawData);
      if (parseData != null)
      {
        // replace rawData with parsed date string
        rawData = parseData;

        // load the sub fields with date elements

        // load the elements into a stack
        StringTokenizer tok = new StringTokenizer(rawData,"/");
        Stack stack = new Stack();
        while (tok.hasMoreTokens())
        {
          stack.push(tok.nextToken());
        }

        // load the year field
        if (!stack.empty())
        {
          yearField.setData((String)stack.pop());
        }

        // load the day field
        if (!monthYear && !stack.empty())
        {
          dayField.setData((String)stack.pop());
        }

        // load the month field
        if (!stack.empty())
        {
          monthField.setData((String)stack.pop());
        }
      }
      else
      {
        // raw data wasn't parsable date data 
        // so clear out the internal sub fields
        yearField.setData("");
        monthField.setData("");
        dayField.setData("");
      }
    }

    return rawData;
  }

  public Date getUtilDate()
  {
    // construct string using sub fields
    String day    = dayField.getData();
    String month  = monthField.getData();
    String year   = yearField.getData();

    String dateString = month + "/" + (monthYear ? "" : day + "/") + year;

    //log.debug("getUtilDate() (mm/dd/yyyy) = "+ dateString);

    return parseDateString(dateString);
  }

  public void setUtilDate(Date date)
  {
    setData(formatDateString(date));
  }

  public String getSqlDateString()
  {
    if (!hasError && fdata != null && fdata.length() > 0)
    {
      SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
      StringTokenizer tok = new StringTokenizer(sdf.format(getUtilDate()),"-");
      return tok.nextToken() + "-" +
             tok.nextToken().substring(0,3) + "-" +
             tok.nextToken();
    }
    return super.getData();
  }

  public void resetDefault()
  {
    super.resetDefault();
    dayField.resetDefault();
    monthField.resetDefault();
    yearField.resetDefault();
  }

  public void setCurrentDateValidation()
  {
    addValidation(new CurrentDateValidation());
  }

}
