/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlTableRow.java $

  Description:
  
  HtmlTableRow

  Extends HtmlContainer to define an html table row element.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Iterator;

public class HtmlTableRow extends HtmlContainer
{
  {
    addProperty("align");
    addProperty("bgcolor");
    addProperty("valign");
    isTransparent = false;
  }
  
  public String getName()
  {
    return "tr";
  }
  
  public HtmlTableCell add(HtmlTableCell cell, int idx)
  {
    return (HtmlTableCell)addContent(cell,idx);
  }
  public HtmlTableCell add(HtmlTableCell cell)
  {
    return add(cell,contents.size());
  }
  
  public HtmlTableCell getCell(int idx)
  {
    return (HtmlTableCell)getContent(idx);
  }
  
  public HtmlTableRow cloneRow()
  {
    HtmlTableRow row = new HtmlTableRow();
    row.copyProperties(this);
    for (Iterator i = contents.iterator(); i.hasNext();)
    {
      HtmlTableCell cell = (HtmlTableCell)i.next();
      row.add(cell.cloneCell());
    }
    return row;
  }
}