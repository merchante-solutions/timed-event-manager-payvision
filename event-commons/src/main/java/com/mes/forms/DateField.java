/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DateField.java $

  Description:

  DateField

  Contains a date.  Valid input must be in the format of mm/dd/yy or
  mm/dd/yyyy.  Methods are available for exporting the field data as
  a java.util.Date object and as a text string formatted for SQL
  statements (i.e. '12-Dec-2001').

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import com.mes.support.DateTimeFormatter;

public class DateField extends Field
{
  public class DateValidation implements Validation
  {
    public boolean validate(String fdata)
    {
      boolean isValid = true;

      try
      {
        if (fdata != null && fdata.length() > 0)
        {
          // attempt to parse the date data
          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
          ParsePosition pos = new ParsePosition(0);
          Date parseDate = sdf.parse(fdata,pos);
          if (parseDate == null)
          {
            sdf = new SimpleDateFormat("MM/dd/yyyy");
            pos = new ParsePosition(0);
            parseDate = sdf.parse(fdata,pos);
            if (parseDate == null)
            {
              throw new Exception("Invalid date, must be in the format mm/dd/yyyy");
            }
          }
        }
      }
      catch (Exception e)
      {
        isValid = false;
      }

      return isValid;
    }

    public String getErrorText()
    {
      return "Date must be in the format 'mm/dd/yyyy'";
    }
  }

  private String        DateFormatString        = "yyyy-MM-dd hh:mm:ss.SSS a";
  private String        DateFormatInput         = "MM/dd/yyyy";

  public DateField(String fname,
                   String label,
                   boolean nullAllowed)
  {
    // allows date strings like 'mm/dd/yyyy'
    this(fname,label,10,10,nullAllowed);
  }

  public DateField(String fname,
                   String label,
                   int length,
                   int htmlSize,
                   boolean nullAllowed)
  {
    // allows date strings like 'mm/dd/yyyy'
    super(fname,label,length,htmlSize,nullAllowed);
    addValidation(new DateValidation());
  }



  public DateField(String fname,
                   boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }

  public DateField(String fname,
                   String label,
                   Date initDate,
                   boolean nullAllowed)
  {
    this(fname,label,nullAllowed);
    setUtilDate(initDate);
  }
  public DateField(String fname,
                   Date initDate,
                   boolean nullAllowed)
  {
    this(fname,fname,initDate,nullAllowed);
  }

  public String getRenderData()
  {
    Date        javaDate;

    if("MM/dd/yyyy".equals(DateFormatInput))
    {
      javaDate = getUtilDate();
    }
    else
    {
      javaDate = getUtilDate(DateFormatInput);
    }

    String      retVal      = null;

    if ( javaDate == null || javaDate.getTime()==0L || javaDate.getTime() == -57600000L)
    // NOTE: -57600000L represents '12/31/69 12:00 AM' which is the 0 value for db dates.
    {
      retVal = "";
    }
    else
    {
      retVal = DateTimeFormatter.getFormattedDate(javaDate,DateFormatString);
    }
    return( retVal );
  }

  public java.sql.Date getSqlDate()
  {
    java.sql.Date     sqlDate     = null;

    try
    {
      sqlDate = new java.sql.Date( getUtilDate().getTime() );
    }
    catch(Exception e)
    {
      // ignore, return null
    }

    return( sqlDate );
  }

  public java.sql.Timestamp getSqlTimestamp()
  {
    java.sql.Timestamp     sqlTimestamp     = null;

    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat(DateFormatInput);
      Date _date = sdf.parse(getData());
      sqlTimestamp = new java.sql.Timestamp(_date.getTime());
    }
    catch(Exception e)
    {
      // ignore, return null
    }

    return( sqlTimestamp );
  }

  public String getSqlDateString()
  {
    if (!hasError && fdata != null && fdata.length() > 0)
    {

      SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
      StringTokenizer tok = new StringTokenizer(sdf.format(getUtilDate()),"-");
      return tok.nextToken() + "-" +
             tok.nextToken().substring(0,3) + "-" +
             tok.nextToken();
    }
    return super.getData();
  }

  public Date getUtilDate()
  {
    return( getUtilDate("MM/dd/yy") );
  }

  public Date getUtilDate(String format)
  {
    Date utilDate = null;

    try
    {
      if (!hasError && fdata != null && fdata.length() > 0)
      {
        // attempt to format the field data into a date object
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        ParsePosition pos = new ParsePosition(0);
        Date parseDate = sdf.parse(fdata,pos);

        if (parseDate == null)
        {
          // try with 4 digit year if default parse fails
          if ( "MM/dd/yy".equals(format) )
          {
            sdf = new SimpleDateFormat("MM/dd/yyyy");
            pos = new ParsePosition(0);
            parseDate = sdf.parse(fdata,pos);
          }

          // if still null, this date is invalid
          // for the requested format
          if (parseDate == null)
          {
            throw new Exception("Invalid date");
          }
        }
        utilDate = parseDate;
      }
    }
    catch (Exception e) {}

    return utilDate;
  }

  public void setDateFormat( String newFormat )
  {
    DateFormatString = newFormat;
  }

  public void setDateFormatInput( String newFormat )
  {
    DateFormatInput = newFormat;
  }

  public void setUtilDate(Date newDate)
  {
    setUtilDate(newDate,DateFormatInput);
  }

  public void setUtilDate(Date newDate, String format)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      setData(sdf.format(newDate));
    }
    catch( NullPointerException npe )
    {
      setData("");    // null date, set field to empty string
    }
  }
}
