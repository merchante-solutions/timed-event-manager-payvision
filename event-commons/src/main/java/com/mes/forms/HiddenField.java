/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HiddenField.java $

  Description:
  
  HiddenField
  
  Extends field to define a hidden html form field.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HiddenField extends Field
{
  public HiddenField(String fname)
  {
    super(fname,0,0,true);
  }
  public HiddenField(String fname, String initValue)
  {
    super(fname,0,0,true);
    setDefaultValue(initValue);
    setData(initValue);
  }
  public HiddenField(Field field)
  {
    super(field.fname,0,0,true);
    setData(field.getData());
  }
  
  protected void validateData()
  {
    hasError = false;
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"hidden\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("value=\"" + getRenderData() + "\" ");
    html.append(">");
    return html.toString();
  }
}
