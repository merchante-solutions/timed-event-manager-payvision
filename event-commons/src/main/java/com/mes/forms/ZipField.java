/*@lineinfo:filename=ZipField*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ZipField.sqlj $

  Description:
  
  ZipField
  
  A field containing a zip code.  Validates zip code against 
  rapp_app_valid_zipcodes.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;
import com.mes.database.SQLJConnectionBase;

public class ZipField extends Field
{
  public class ZipValidation extends SQLJConnectionBase
    implements Validation
  {
    private Field stateField;
    
    public ZipValidation(Field stateField)
    {
      this.stateField = stateField;
    }
    
    public boolean validate(String zip)
    {
      boolean isValid = true;
      
      if (zip != null && zip.length() > 0)
      {
        if (zip.length() == 9)
        {
          zip = zip.substring(0,5);
        }
        int zipNum;
        try
        {
          zipNum = Integer.parseInt(zip);
        }
        catch (Exception e)
        {
          return false;
        }
        
        if (stateField != null && stateField.getData() != null 
            && stateField.getData().length() != 0)
        {
          try
          {
            connect();
            
            int zipCount = 0;
            
            /*@lineinfo:generated-code*//*@lineinfo:77^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(state_code)
//                
//                from    rapp_app_valid_zipcodes
//                where   lower(state_code) = lower(:stateField.getData()) and
//                        :zipNum between zip_low and zip_high
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_122 = stateField.getData();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(state_code)\n               \n              from    rapp_app_valid_zipcodes\n              where   lower(state_code) = lower( :1 ) and\n                       :2  between zip_low and zip_high";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.ZipField",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_122);
   __sJT_st.setInt(2,zipNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   zipCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^13*/
            
            isValid = (zipCount > 0);
          }
          catch (Exception e)
          {
            String func = this.getClass().getName() + "::validate(zip = " 
              + zip + ")";
            String desc = e.toString();
            System.out.println(func + ": " + desc);
            logEntry(func,desc);
          }
          finally
          {
            cleanUp();
          }
        }
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return "Invalid zip code";
    }
  }
    
  public ZipField(String fname,
                  String label,
                  boolean nullAllowed,
                  Field stateField)
  {
    // sizing allows 9 digit numbers with up to 1 formatting char
    // xxxxx-xxxx, xxxxx xxxx, xxxxx.xxxx
    super(fname,label,10,10,nullAllowed);
    addValidation(new ZipValidation(stateField));
  }
  public ZipField(String fname,
                  boolean nullAllowed,
                  Field stateField)
  {
    this(fname,fname,nullAllowed,stateField);
  }
  public ZipField(String fname,
                  String label,
                  boolean nullAllowed)
  {
    this(fname,label,nullAllowed,null);
  }
  public ZipField(String fname,
                  boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }
  
  public String getRenderData()
  {
    if (fdata.length() == 9 && !hasError)
    {
      return  fdata.substring(0,5) + "-" + fdata.substring(5,9);
    }
    return fdata;
  }
  
  /*
  ** protected String processData(String rawData)
  **
  ** Strips out valid formatting characters so that valid zip codes
  ** will be stored internally as just a string of numeric digits.
  **
  ** RETURNS: filtered data if no errors, else raw data.
  */
  protected String processData(String rawData)
  {
    try
    {
      // scan through string, strip valid formatting chars
      StringTokenizer tok = new StringTokenizer(rawData," -.");
      StringBuffer tokBuf = new StringBuffer();
      while (tok.hasMoreTokens())
      {
        tokBuf.append(tok.nextToken());
      }
    
      // parse the stripped string to make sure it has only numeric data
      Long.parseLong(tokBuf.toString());
    
      // don't filter data that's wrong length
      if (tokBuf.length() != 5 && tokBuf.length() != 9)
      {
        throw new Exception("bad length");
      }
    
      // return the filtered data
      return tokBuf.toString();
    }
    catch (Exception e) { }
    
    return rawData;
  }
}/*@lineinfo:generated-code*/