/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/FieldValidator.java $

  Description:

  FieldValidator

  Takes a field or field group and performs field validation.  When
  validation fails, FieldError objects are created for each invalid field
  and stored.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Iterator;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;

public class FieldValidator
{

  // create class log category
  static Logger log = Logger.getLogger(FieldValidator.class);

  private Vector fieldErrors = new Vector();

  public boolean validateFields(Field field)
  {
    boolean isAllValid = true;

    field.validateData();
    if (field.getHasError())
    {
      log.debug("field: " + field.getName() + " failed validation");
      isAllValid = false;
      fieldErrors.add(new FieldError(field));
    }

    if (field instanceof FieldGroup)
    {
      FieldGroup fg = (FieldGroup)field;
      for (Iterator i = fg.iterator(); i != null && i.hasNext(); )
      {
        Field f = (Field)i.next();
        if (!validateFields(f))
        {
          isAllValid = false;
        }
      }
    }

    return isAllValid;
  }

  public Vector getFieldErrors()
  {
    return fieldErrors;
  }
}

