/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlForm.java $

  Description:

  HtmlForm
  
  An HtmlRenderable class that defines a form framework.  The form defines
  a form header and a form body.  The form body contains HtmlFormSection 
  objects.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlForm implements HtmlRenderable
{
  private HtmlTable tblMain;
  private HtmlTable tblBody;
  
  public HtmlForm()
  {
    // main html table
    tblMain = new HtmlTable();
    tblMain.setProperty("class","formFrame");
    tblMain.setProperty("align","center");
    tblMain.setProperty("cellspacing","0");
    tblMain.setProperty("cellpadding","1");
    
    // form header
    tblMain.add(new HtmlTableRow()).add(new HtmlTableCell());
    HtmlTableCell tdHeader = tblMain.getRow(0).getCell(0);
    tdHeader.add("Form Header");
    tdHeader.setProperty("class","formHeader");
    
    // formBody
    tblMain.add(new HtmlTableRow()).add(new HtmlTableCell());
    HtmlTableCell tdBody = tblMain.getRow(1).getCell(0);
    tblBody = (HtmlTable)tdBody.add(new HtmlTable());
  }
  
  /*
  ** Accessors
  */
  public void setWidth(String width)
  {
    tblMain.setProperty("width",width);
  }
  
  public void setWidth(int width)
  {
    setWidth(Integer.toString(width));
  }
  
  public void setBordersVisible(boolean visible)
  {
    tblMain.setAllProperties("table","border",(visible ? "1" : "0"));
  }
  
  /*
  ** public String renderHtml()
  **
  ** Renders html with 0 indent level.
  **
  ** RETURNS: rendered html as a String.
  */
  public String renderHtml()
  {
    return renderHtml(0);
  }
  
  /*
  ** public String renderHtml(int indentLevel)
  **
  ** Renders html with specified indent level.
  **
  ** RETURNS: rendered html as a String.
  */
  public String renderHtml(int indentLevel)
  {
    return tblMain.renderHtml(indentLevel);
  }
  
  /*
  ** public HtmlFormSection add(HtmlFormSection sec)
  **
  ** Adds a form section to the form body in a new body row.
  **
  ** RETURNS: reference to the section added.
  */
  public HtmlFormSection add(HtmlFormSection sec)
  {
    HtmlTableCell td = tblBody.add(new HtmlTableRow()).add(new HtmlTableCell());
    td.setProperty("class","formBody");
    td.add(sec);
    return sec;
  }
}
