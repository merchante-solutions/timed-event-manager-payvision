/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/FieldGroup.java $

  Description:

  FieldGroup

  Contains a group of fields and provides methods for validating and
  accessing them.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-03-18 15:18:37 -0700 (Fri, 18 Mar 2011) $
  Version            : $Revision: 18582 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public class FieldGroup extends Field
{
  public class AndCondition implements Condition, Serializable
  {
    Vector conditions = new Vector();

    public void add(Condition condition)
    {
      conditions.add(condition);
    }

    public boolean isMet()
    {
      for (Iterator i = conditions.iterator(); i.hasNext(); )
      {
        Condition c = (Condition)i.next();
        if (!c.isMet())
        {
          return false;
        }
      }
      return true;
    }
  }

  /*
  ** public FieldGroup(String fname, String label)
  **
  ** Constructor.
  */
  public FieldGroup(String fname, String label)
  {
    super(fname,label,0,0,true);
  }

  public FieldGroup(String fname)
  {
    this(fname,fname);
  }

  /*
  ** public FieldGroup(String fname, Condition condition)
  **
  ** Constructor.
  */
  public FieldGroup(String fname, Condition condition)
  {
    super(fname,0,0,true);
    addCondition(condition);
    setData(fname);
  }

  private AndCondition conditions = new AndCondition();

  public void addCondition(Condition condition)
  {
    conditions.add(condition);
  }

  private Vector    fields  = new Vector();
  private Hashtable aliases = new Hashtable();

  /*
  ** private Field findField(String fname,boolean ignoreCase)
  **
  ** Looks for a field by name.  When sub groups are encountered, they
  ** are searched as well.  If no field is found the alias table is
  ** queried and if a match is found the field linked to is returned.
  **
  ** RETURNS: reference to the Field if found, else null.
  */
  private Field findField(String fname, boolean ignoreCase)
  {
    return findField(fname, ignoreCase, true);
  }
  private Field findField(String fname, boolean ignoreCase, boolean recurse)
  {
    // find the named field
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();

      // case sensitive match or case insensitive match upon request
      if ( (f.getName().equals(fname)) ||
           ((ignoreCase == true) &&
            (f.getName().toLowerCase().equals(fname.toLowerCase())) ) )
      {
        return f;
      }
      else if (recurse && f instanceof FieldGroup)
      {
        Field f2 = ((FieldGroup)f).findField(fname,ignoreCase);
        if (f2 != null)
        {
          return f2;
        }
      }
    }

    // no field found, return any alias matches (null if no match)
    return (Field)aliases.get(fname);
  }

  /*
  ** public Field getField(String fname)
  **
  ** RETURNS: Field reference to named field, else null if does not exist.
  */
  public Field getField(String fname)
  {
    // case sensitive by default
    return( findField(fname, false) );
  }

  public Field getField(String fname, boolean ignoreCase)
  {
    return( findField(fname, ignoreCase) );
  }

  public Field getField(String fname, boolean ignoreCase, boolean recurse)
  {
    return( findField(fname, ignoreCase, recurse) );
  }

  /*
  ** private void clearAliases(String fieldName)
  **
  ** Scans the alias table fields, all aliases that point to a field with
  ** the given field name are removed.
  */
  private void clearAliases(String fieldName)
  {
    for (Iterator i = aliases.values().iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f.getName().equals(fieldName))
      {
        i.remove();
      }
    }
  }

  public void clearLabel(boolean recurse)
  {
    this.sname = "";

    if(recurse) {
      for (Iterator i = fields.iterator(); i.hasNext();)
      {
        Field f = (Field)i.next();

        if (f instanceof FieldGroup)
        {
          ((FieldGroup)f).clearLabel(true);
        }
      }
    }
  }

  /*
  ** public void removeAllFields()
  **
  ** Removes all fields contained in this group.
  */
  public void removeAllFields()
  {
    // remove all fields from sub groups
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).removeAllFields();
      }

      // remove all aliases for this field
      clearAliases(f.getName());
    }

    // clear out the fields vector
    fields.removeAllElements();
  }

  /*
  ** public FieldGroup deleteField(String fname)
  **
  ** Looks for a field contained within this field group and if found deletes it.
  ** If a subgroup is encountered the delete operation will be passed onto it.
  ** Continues until the field is found and deleted, a subgroup containing the
  ** field reports successful deletion or no more fields are found.
  **
  ** RETURNS: field group that contained the deleted field if successful,
  **          null if not found.
  */
  public FieldGroup deleteField(String fname)
  {
    // if named field is this group's name then just return...
    // can't delete self, no point in looking below self
    if (getName().equals(fname))
    {
      return null;
    }

    // iterate through all fields contained within this group
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();

      // if found the field, remove it
      if (f.getName().equals(fname))
      {
        // have field groups remove all contained fields
        if (f instanceof FieldGroup)
        {
          ((FieldGroup)f).removeAllFields();
        }

        // remove all aliases for this field
        clearAliases(f.getName());

        i.remove();

        // field was found in this field group, so return self
        // as the parent of the deleted field
        return this;
      }
      // if field group found, see if it can delete the field
      else if (f instanceof FieldGroup)
      {
        // attempt to delete the field from the field group
        FieldGroup parent = ((FieldGroup)f).deleteField(fname);

        // if field was found, a parent will be returned
        if (parent != null)
        {
          // return the parent of the deleted field within
          return parent;
        }
      }
    }
    // field not found in self or any sub groups
    return null;
  }

  /*
  ** private void deleteContainedFields(FieldGroup group)
  **
  ** Iterates through group, deletes all fields contained within it and any
  ** sub group fields.
  */
  private void deleteContainedFields(FieldGroup group)
  {
    for (Iterator i = group.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        deleteContainedFields((FieldGroup)f);
      }
      deleteField(f.getName());
    }
  }

  /*
  ** public boolean add(Field newField)
  **
  ** Adds a field to this group.  Makes sure a field by the same name doesn't
  ** already exist.
  **
  ** RETURNS: true if add successful, else false (field name already in use).
  */
  public boolean add(Field newField)
  {
    // any existing field by same name will
    // be deleted, new field will replace it
    FieldGroup parent = deleteField(newField.getName());

    // if adding a field group need to make sure any
    // fields contained in the new field group are also
    // deleted from the existing pool of fields
    if (newField instanceof FieldGroup)
    {
      deleteContainedFields((FieldGroup)newField);
    }

    // add the new field to the parent's vector or this
    // field group's vector if no parent was found
    // making sure to set conditions in new field
    if (parent != null)
    {
      newField.setOptionalCondition(parent.conditions);
      parent.fields.add(newField);
    }
    else
    {
      newField.setOptionalCondition(conditions);
      fields.add(newField);
    }

    return true;
  }

  /*
  ** public Field addAlias(String fieldName, String aliasName)
  **
  ** Adds an alias name to the alias list.  Aliases allow fields to be referenced
  ** by more than one name.
  **
  ** RETURNS: the real field that the alias is pointed to, null if field does
  **          not exist or the alias name is an actual field name.
  */
  public Field addAlias(String fieldName, String aliasName)
  {
    // if field does not exist, fail
    Field field = findField(fieldName,false);
    if (field == null)
    {
      return null;
    }

    // if alias name is an actual field name, fail
    Field aliasLookup = findField(aliasName,false);
    if (aliasLookup != null && aliasLookup.getName().equals(aliasName))
    {
      return null;
    }

    // assign the alias to the field, this will overwrite
    // existing alias's previous assignment and add new aliases
    aliases.put(aliasName,field);
    return field;
  }

  /*
  ** public boolean setData(String fname, String fdata)
  **
  ** Sets the data given to the named field.
  **
  ** RETURNS: true if data was assigned, else false (i.e. field did not exist).
  */
  public boolean setData(String fname, String fdata)
  {
    boolean setOk = false;

    // locate the field
    Field f = findField(fname,false);
    if (f != null)
    {
      // set the data
      f.setData(fdata);
      setOk = true;
    }

    return setOk;
  }
  
  public boolean setData(String fname, int    fdata) { return(setData(fname,String.valueOf(fdata))); }
  public boolean setData(String fname, long   fdata) { return(setData(fname,String.valueOf(fdata))); }
  public boolean setData(String fname, double fdata) { return(setData(fname,String.valueOf(fdata))); }
  public boolean setData(String fname, float  fdata) { return(setData(fname,String.valueOf(fdata))); }

  /*
  ** public String getData(String fname)
  **
  ** Gets the data for the named field.
  **
  ** RETURNS: String field value, or null if not found.
  */
  public String getData(String fname)
  {
    Field f = findField(fname,false);
    if (f != null)
    {
      return f.getData();
    }
    return null;
  }

  /*
  ** public String getLabel(String fname)
  **
  ** Retrieves the label for the specified field
  **
  ** RETURNS: String field label, or "" if not found
  */
  public String getLabel(String fname)
  {
    String result = "";
    Field f= findField(fname,false);
    if(f != null)
    {
      result = f.getLabel();
    }
    return (result);
  }

  /*
  ** public String renderHtml(String fname)
  **
  ** RETURNS: rendered html for the named field, null if not found.
  */
  public String renderHtml(String fname)
  {
    Field f = findField(fname,false);
    if (f != null)
    {
      return f.renderHtml();
    }
    return null;
  }

  public String renderHtml(String fname, int radioIndex)
  {
    return( renderHtml(fname, radioIndex, false, true) );
  }
  
  public String renderHtml(String fname, int radioIndex, boolean showJavaScript, boolean showLabel)
  {
    String      retVal      = null;
    try
    {
      RadioButtonField  rbf = (RadioButtonField) findField(fname,false);
      retVal = rbf.renderHtmlField(radioIndex,showJavaScript,showLabel);
    }
    catch( ClassCastException e )
    {
      logEntry("renderHtml(" + fname + ")", e.toString() );
    }
    catch( NullPointerException e )
    {
      logEntry("renderHtml(" + fname + ")", e.toString() );
    }
    return(retVal);
  }

  /*
  ** private boolean conditionsMet()
  **
  ** RETURNS: true if all conditions are met, else false.
  */
  private boolean conditionsMet()
  {
    return conditions.isMet();
  }

  /*
  ** public boolean isValid()
  **
  ** Validates this group and all fields contained within it.  Validation
  ** is tried on ALL fields, although a single invalid field will make the
  ** result false.
  **
  ** RETURNS: true if all contained fields and this field are valid,
  **          else false.
  */
  public boolean isValid()
  {
    boolean contentsValid = true;

    validateData();

    if (hasError)
    {
      contentsValid = false;
    }

    // validate all fields contained in this group
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      // validate the actual field
      Field f = (Field)i.next();
      if (!f.isValid())
      {
        contentsValid = false;
      }
    }

    return contentsValid;
  }

  /*
  ** public Vector getErrors()
  **
  ** Collects all invalid fields' error text Strings into a Vector.
  **
  ** RETURNS: Vector containing all found error text messages.
  */
  public Vector getErrors()
  {
    Vector errors = new Vector();

    // check for error within this group
    if (hasError)
    {
      errors.add((sname.length()>0? (sname + ": " + errorText) : errorText));
    }

    // look for errors in all contained fields
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();

      // if has error then get error text within the field
      if (f.getHasError())
      {
        // call getErrors() on subgroups
        if (f instanceof FieldGroup)
        {
          errors.addAll(((FieldGroup)f).getErrors());
        }
        // otherwise just add the individual fields error text
        else
        {
          errors.add((f.sname.length()>0? (f.sname + ": " + f.getErrorText()) : f.getErrorText()));
        }
      }
    }

    return errors;
  }

  /*
  ** public void resetErrorState()
  **
  ** Resets errors state in all contained fields and self.
  */
  public void resetErrorState()
  {
    super.resetErrorState();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      ((Field)i.next()).resetErrorState();
    }
  }

  /*
  ** public void setHtmlExtra(String htmlExtra)
  **
  ** Sets the html extra data in self and all fields contained in self.
  */
  public void setHtmlExtra(String htmlExtra)
  {
    super.setHtmlExtra(htmlExtra);
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).setHtmlExtra(htmlExtra);
      }
      else
      {
        f.setHtmlExtra(htmlExtra);
      }
    }
  }
  
  /*
  ** public void setImmutable(boolean immutable)
  **
  ** Sets all contained fields to be immutable
  */
  public void setImmutable(boolean immutable)
  {
    super.setImmutable(immutable);
    
    for( Iterator i = fields.iterator(); i.hasNext(); )
    {
      Field f = (Field)i.next();
      
      if(f instanceof FieldGroup)
      {
        ((FieldGroup)f).setImmutable(immutable);
      }
      else
      {
        f.setImmutable(immutable);
      }
    }
  }
  
  /*
  ** public void setTrackChanges(boolean trackChanges)
  **
  ** Sets all contained fields trackChanges parameter to specified value
  */
  public void setTrackChanges(boolean trackChanges)
  {
    super.setTrackChanges(trackChanges);
    
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).setTrackChanges(trackChanges);
      }
      else
      {
        f.setTrackChanges(trackChanges);
      }
    }
  }
  
  /*
  ** public String getChangees()
  **
  ** Returns a description of all changes to the fields in this field group:
  **
  ** <fieldName> [<old val> | <new val>], ...
  **
  */
  public String listChanges()
  {
    StringBuffer changes = new StringBuffer("");
    
    for( Iterator i = fields.iterator(); i.hasNext(); )
    {
      // if field is set to track changes and indeed a change has occurred, add it to the list
      Field f = (Field)i.next();
      
      if( f instanceof FieldGroup )
      {
        changes.append(((FieldGroup)f).listChanges());
      }
      else if( f.trackChanges() && f.getHasChanged() )  // only add to list if field has changed
      {
        if(changes.length() > 0)
        {
          // add delimiter
          changes.append(",");
        }
      
        changes.append("(");
        changes.append(f.getName());
        changes.append(" [");
        changes.append(f.getOriginalData());
        changes.append("]=>[");
        changes.append(f.getData());
        changes.append("])");
      }
    }
    
    return( changes.toString() );
  }
  
  public boolean hasChanged()
  {
    return( ! ("").equals(listChanges()) );
  }
  public boolean hasChanged( String fieldName )
  {
    return( getField(fieldName).getHasChanged() );
  }
  
  public String toString()
  {
    StringBuffer result = new StringBuffer("");
    
    for( Iterator i = fields.iterator(); i.hasNext(); )
    {
      Field f = (Field)i.next();
      
      if( f instanceof FieldGroup )
      {
        result.append("\nGROUP " + f.getShowName() + " ("+f.getName()+") :\n");
        result.append( ((FieldGroup)f).toString() );
      }
      else
      {
        result.append(f.toString());
      }
    }
    
    return( result.toString() );
  }
  
  /*
  ** public void addHtmlExtra(String htmlExtra)
  **
  ** Adds the html extra data to self and all fields contained in self.
  */
  public void addHtmlExtra(String htmlExtra)
  {
    super.addHtmlExtra(htmlExtra);
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).addHtmlExtra(htmlExtra);
      }
      else
      {
        f.addHtmlExtra(htmlExtra);
      }
    }
  }

  /*
  ** public void setGroupHtmlExtra(String htmlExtra)
  **
  ** Legacy, calls setHtmlExtra().  Sets the html extra data in self and
  ** all fields contained in self.
  */
  public void setGroupHtmlExtra(String htmlExtra)
  {
    setHtmlExtra(htmlExtra);
  }

  /*
  ** public void addGroupValidation(Validation validation)
  **
  ** Adds the given Validation to all fields contained within this group.
  */
  public void addGroupValidation(Validation validation)
  {
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).addGroupValidation(validation);
      }
      else
      {
        f.addValidation(validation);
      }
    }
  }

  public void makeReadOnly()
  {
    super.makeReadOnly();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.makeReadOnly();
    }
  }

  public void unmakeReadOnly()
  {
    super.unmakeReadOnly();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.unmakeReadOnly();
    }
  }

  /*
  ** public void setShowErrorText(boolean showErrorText)
  **
  ** Overrides parent method to apply flag to all fields contained
  ** in this field group.
  */
  public void setShowErrorText(boolean showErrorText)
  {
    this.showErrorText = showErrorText;
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.setShowErrorText(showErrorText);
    }
  }

  /*
  ** public void setFixImage(String newPath, int newWidth, int newHeight)
  **
  ** Sets the fix image path and dimensions in self and all contained fields.
  */
  public void setFixImage(String newPath, int newWidth, int newHeight)
  {
    super.setFixImage(newPath,newWidth,newHeight);
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.setFixImage(newPath,newWidth,newHeight);
    }
  }

  /*
  ** public Iterator iterator()
  **
  ** RETURNS: an iterator to the fields vector, null if fields is null.
  */
  public Iterator iterator()
  {
    if (fields != null)
    {
      return fields.iterator();
    }
    return null;
  }

  /*
  ** public Vector getFieldsVector()
  **
  ** Generates a Vector containing all fields within this field group, and
  ** also expands all field groups within this field group into individual
  ** fields so that the Vector will contain all fields contained within
  ** this field group regardless of their depth.
  **
  ** RETURNS: Vector of all fields contained within this field group.
  */
  public Vector getFieldsVector()
  {
    Vector allFields = new Vector();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        allFields.addAll(((FieldGroup)f).getFieldsVector());
      }
      else
      {
        allFields.add(f);
      }
    }

    return allFields;
  }

  /*
  ** public int size()
  **
  ** RETURNS: number of fields contained in this group, 0 if fields is null or
  **          empty.
  */
  public int size()
  {
    return (fields == null ? 0 : fields.size());
  }

  /*
  ** public void reset()
  **
  ** Resets self and all contained fields.
  */
  public void reset()
  {
    super.reset();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.reset();
    }
  }

  /*
  ** public void resetDefaults
  **
  ** Sets fields back to default values
  */
  public void resetDefaults( )
  {
    resetDefaults(true);
  }
  
  public void resetDefaults( boolean recurse )
  {
    super.resetDefault();

    for(Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f= (Field)i.next();
      if ( recurse && f instanceof FieldGroup )
      {
        ((FieldGroup)f).resetDefaults();
      }
      f.resetDefault();
    }
  }

  private HtmlFormSection section;

  /*
  ** public HtmlFormSection setSection(HtmlFormSection section)
  **
  ** Allows a custom section to be set in the field group for non-standard
  ** layouts.
  **
  ** RETURNS: reference to section being set.
  */
  public HtmlFormSection setSection(HtmlFormSection section)
  {
    this.section = section;
    return section;
  }

  /*
  ** public HtmlFormSection getSection()
  **
  ** RETURNS: the html form section.
  */
  public HtmlFormSection getSection()
  {
    if (section == null)
    {
      section = new HtmlFormSection();
    }
    return section;
  }

  /*
  ** public boolean add(String label, Field newField, boolean showRequired)
  **
  ** Adds a field to this group and to the html form section.  The label
  ** is used by the html section.
  **
  ** RETURNS: true if add successful, else false.
  */
  public boolean add(String label, Field newField, boolean showRequired)
  {
    getSection().add(label,newField,showRequired);
    return add(newField);
  }
  public boolean add(String label, Field newField)
  {
    return add(label,newField,false);
  }

  /*
  ** public boolean add(int colIdx, String label, Field newField, boolean showRequired)
  **
  ** Adds a field to this group and to the html form section.  Allows
  ** a column in the form section to be specified.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean add(int colIdx, String label, Field newField, boolean showRequired)
  {
    getSection().add(colIdx,label,newField,showRequired);
    return add(newField);
  }
  public boolean add(int colIdx, String label, Field newField)
  {
    return add(colIdx,label,newField,false);
  }

  /*
  ** public String renderHtmlSection()
  **
  ** Renders html form section.  Any subgroups contained are also rendered.
  **
  ** RETURNS: rendred html.
  */
  public String renderHtmlSection()
  {
    StringBuffer html = new StringBuffer();
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        html.append(((FieldGroup)f).renderHtmlSection());
      }
    }
    if (section != null)
    {
      html.append(section.renderHtml());
    }
    return html.toString();
  }

  /*
  ** public boolean isSubmitted()
  **
  ** Returns TRUE if a ButtonField exsits in the field group that contains data
  ** (implying that a button was pressed on the form)
  */
  public boolean isSubmitted()
  {
    boolean result = false;

    try
    {
      Vector allFields = getFieldsVector();

      for(int i=0; i<allFields.size(); ++i)
      {
        Field f = (Field)(allFields.elementAt(i));

        if(f instanceof com.mes.forms.ButtonField && !f.getData().equals(""))
        {
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.forms.FieldGroup::isSubmitted():", e.toString());
    }

    return result;
  }
}
