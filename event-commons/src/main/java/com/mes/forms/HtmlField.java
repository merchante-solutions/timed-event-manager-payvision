/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlField.java $

  Description:
  
  HtmlField

  Extends the renderable interface to define an interface for field 
  html form field objects.  This interface is implemented by all Field
  based objects.  This interface describes objects that are labeled
  and have error indicator accessors.
  associated with them.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public interface HtmlField extends HtmlRenderable
{
  public String getLabel();
  public boolean isValid();
}
