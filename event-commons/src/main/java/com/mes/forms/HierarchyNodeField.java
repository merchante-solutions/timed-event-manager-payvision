/*@lineinfo:filename=HierarchyNodeField*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HierarchyNodeField.sqlj $

  Description:
  
  Extends number field to include validation that the number
  is both a valid hierarchy node and exists within the hierarchy
  that the specified user has access to.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

import sqlj.runtime.ref.DefaultContext;

public class HierarchyNodeField extends Field
{
  /*
  ** public class NumberRangeValidation implements Validation
  **
  ** Validates a number field against a numeric range INCLUSIVE of both a lower bound and upper bound.
  ** Field data is cast to a float so that this validation may apply to both integers decimal numbers.
  */
  public class HierarchyNodeValidation implements Validation
  {
    private DefaultContext  Ctx             = null;
    private String          ErrorMessage    = "";
    private long            UserNode        = 0L;
    
    public HierarchyNodeValidation( DefaultContext ctx, long userNode )
    {
      Ctx       = ctx;
      UserNode  = userNode;
    }
    
    public boolean validate(String fieldData)
    {
      ErrorMessage = null;
      
      try
      {
        // if data is null or empty then don't do validation
        // (evaluates as valid, let NotEmptyValidation handle it from here)
        if (fieldData != null && fieldData.length() > 0)
        {
          try
          {
            if ( isNodeParentOfNode( UserNode, Long.parseLong(fieldData) ) == false )
            {
              ErrorMessage = "Not a valid hierarchy node";
            }
          }
          catch (NumberFormatException ne)
          {
            throw new Exception("Not a valid number");
          }
        }
      }
      catch (Exception e)
      {
        ErrorMessage = e.getMessage();
        if (ErrorMessage == null || ErrorMessage.length() <= 0)
        {
          ErrorMessage = e.toString();
        }
      }
      return( ErrorMessage == null );
      
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    private long getParentNodeId( long nodeId )
    {
      long          parentNodeId = 0L;
    
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:97^9*/

//  ************************************************************
//  #sql [Ctx] { select org_num_to_hierarchy_node( po.parent_org_num )  
//            from   organization   o,
//                   parent_org     po
//            where  o.org_group = :nodeId and
//                   po.org_num = o.org_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select org_num_to_hierarchy_node( po.parent_org_num )   \n          from   organization   o,\n                 parent_org     po\n          where  o.org_group =  :1  and\n                 po.org_num = o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.HierarchyNodeField",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   parentNodeId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^9*/  
      }
      catch( java.sql.SQLException e )
      {
        // no parent, return 0L
      }
      return( parentNodeId );
    }
    
    private boolean isNodeParentOfNode( long parentNodeId, long childNodeId )
    {
      boolean       retVal          = false;
    
      if ( childNodeId == parentNodeId )
      {
        retVal = true;
      }
      else
      {
        do
        {
          // get the parent of the current node
          childNodeId = getParentNodeId( childNodeId );
        }
        while( childNodeId != 0L && childNodeId != parentNodeId );
      
        // found a the parent in the chain
        if ( childNodeId != 0L )
        {
          retVal = true;
        }
      }
      return( retVal );
    }
  }
  
  public HierarchyNodeField(  DefaultContext ctx,
                              long userNode,
                              String fname,
                              String label,
                              boolean nullAllowed )
  {
    super(fname,label,16,16,nullAllowed);
    addValidation(new HierarchyNodeValidation(ctx,userNode));
  }
  public HierarchyNodeField(  DefaultContext ctx,
                              long userNode,
                              String fname,
                              boolean nullAllowed )
  {
    this(ctx,userNode,fname,fname,nullAllowed);
  }
}/*@lineinfo:generated-code*/