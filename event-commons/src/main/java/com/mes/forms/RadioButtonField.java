/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/RadioButtonField.java $

  Description:

  RadioButtonField

  Extends field to define an html form radio button set.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-09-23 15:42:51 -0700 (Tue, 23 Sep 2008) $
  Version            : $Revision: 15383 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class RadioButtonField extends Field
{
  public static final int             RADIO_LABEL         = 0;
  public static final int             RADIO_VALUE         = 1;

  public static final int             LAYOUT_SPEC_VERTICAL      = 1;
  public static final int             LAYOUT_SPEC_HORIZONTAL    = 2;
  public static final int             LAYOUT_SPEC_TABLE         = 3;

  String[][]                  RadioButtons        = null;
  String[]                    OnClicks            = null;
  String                      RadioButtonError    = null;
  int                         SelectedButton      = -1;
  int                         layoutSpec          = LAYOUT_SPEC_VERTICAL;
  int                         numTblCols          = 0;
  Condition                   optionalCondition   = null;

  public RadioButtonField(String fname, String label, String[][] buttons, int sel,
    boolean nullAllowed, String errorMsg, int layoutSpec)
  {
    this(fname,label,buttons,sel,nullAllowed,errorMsg);

    this.layoutSpec = layoutSpec;
  }
  public RadioButtonField(String fname, String label, String[][] buttons, int sel,
    boolean nullAllowed, String errorMsg)
  {
    // do not pass the null allowed to the super
    // class because we want to handle the required
    // validation in this class while still doing other
    // validations in the base class.
    super(fname,label,0,0,true);

    this.nullAllowed  = nullAllowed;
    RadioButtonError  = errorMsg;
    RadioButtons      = buttons;
    SelectedButton    = sel;
    
    // set default value
    if(sel >=0 && sel <= buttons.length)
    {
      setDefaultValue(buttons[sel][RADIO_VALUE]);
    }
    
    // set up array of OnClick JavaScript functions
    OnClicks = new String[RadioButtons.length];
  }
  public RadioButtonField(String fname, String[][] buttons, int sel,
    boolean nullAllowed, String errorMsg)
  {
    this(fname,fname,buttons,sel,nullAllowed,errorMsg);
  }

  public RadioButtonField(String fname, String label, String[][] buttons,
    boolean nullAllowed, String errorMsg)
  {
    // see comment above with regard to nullAllowed flag
    super(fname,label,0,0,true);

    this.nullAllowed  = nullAllowed;
    RadioButtons      = buttons;
    RadioButtonError  = errorMsg;
  }
  public RadioButtonField(String fname, String[][] buttons, boolean nullAllowed,
    String errorMsg)
  {
    this(fname,fname,buttons,nullAllowed,errorMsg);
  }


  public void updateButtonInfo(int button, String label, String value)
  {
    if(RadioButtons != null){
      try
      {
        RadioButtons[button][RADIO_LABEL] = label;
        RadioButtons[button][RADIO_VALUE] = value;
      }
      catch(IndexOutOfBoundsException e)
      {
        //do nothing
      }
    }
  }

  public void   resetRadioButtons(String[][] buttons)
  {
    if(buttons == null || buttons.length < 1)
      return;

    this.RadioButtons = buttons;
    this.SelectedButton = -1;
  }

  public void resetRadioButtons()
  {
    this.SelectedButton = -1;
    this.fdata          = "";
  }

  public int    getLayoutSpec()         { return layoutSpec; }
  public int    getNumTableColumns()    { return numTblCols; }

  public int getNumOptions()
  {
    if (RadioButtons != null)
    {
      return RadioButtons.length;
    }
    return 0;
  }

  public String getLabel(int button)
  {
    try
    {
      return RadioButtons[button][RADIO_LABEL];
    }
    catch (Exception e) {}
    return "";
  }

  public String getValue(int button)
  {
    try
    {
      return RadioButtons[button][RADIO_VALUE];
    }
    catch (Exception e) {}
    return "";
  }

  public String getSelectedLabel()
  {
    return getLabel(SelectedButton);
  }

  private int errorTagIdx = -1;

  public void setErrorLabel(int errprTagIdx)
  {
    if (errorTagIdx >= 0 && errorTagIdx < RadioButtons.length)
    {
      this.errorTagIdx = errorTagIdx;
    }
  }

  public void setLayoutSpec(int v)
  {
    layoutSpec=v;
  }

  public void setNumTableColumns(int v)
  {
    if(v>0)
      numTblCols=v;
  }
  
  public void setSelectedButton(int v)
  {
    SelectedButton = v;
  }

  public int getButtonCount( )
  {
    int       retVal      = 0;

    if ( RadioButtons != null )
    {
      retVal = RadioButtons.length;
    }
    return( retVal );
  }

  public String getSelectedButtonValue()
  {
    String        retVal      = null;

    try
    {
      retVal = RadioButtons[SelectedButton][RADIO_VALUE];
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }

  public boolean isBlank( )
  {
    boolean       retVal = false;

    if ( SelectedButton < 0 ||
         SelectedButton >= getButtonCount() )
    {
      retVal = true;
    }
    return( retVal );
  }

  protected String processData( String data )
  {
    for( int i = 0; i < RadioButtons.length; ++i )
    {
      if ( RadioButtons[i][RADIO_VALUE].equals(data) )
      {
        SelectedButton = i;
        break;
      }
    }
    return( super.processData(data) );
  }

  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();

    String separator = "";

    switch(layoutSpec) {
      default:
      case LAYOUT_SPEC_VERTICAL:
        separator="</td></tr>\n<tr><td>";
        break;
      case LAYOUT_SPEC_HORIZONTAL:
        separator="</td>\n<td width=3>&nbsp;</td><td>";
        break;
    }

    int colNum = 0;

    for( int i = 0; i < RadioButtons.length; ++i )
    {
      colNum++;

      if ( i == 0 )
      {
        html.append("<table>\n<tr><td>");
      }
      else
      {
        if(layoutSpec==LAYOUT_SPEC_TABLE) {
          if(colNum==1) // first column
            separator="";
          else
            separator="</td>\n<td width=3>&nbsp;</td><td>";
        }
        html.append(separator);
      }

      html.append( renderHtmlField(i) );

      if(layoutSpec==LAYOUT_SPEC_TABLE && colNum==numTblCols)
        html.append("</td></tr>\n<tr><td>");

      if(colNum==numTblCols)
        colNum=0; // reset
    }
    if ( html.length() > 0 )
    {
      html.append("</td></tr>\n</table>");
    }
    return( html.toString() );
  }

  public String renderHtmlField(int button)
  {
    return( renderHtmlField(button, false, true) );
  }
  
  public String renderHtmlField(int button, boolean showJavaScript, boolean showLabel)
  {
    StringBuffer html = new StringBuffer();

    html.append("<input type=\"radio\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("value=\"" + RadioButtons[button][RADIO_VALUE] + "\" ");
    if ( SelectedButton == button )
    {
      html.append("checked ");
    }
    if( showJavaScript == true && OnClicks[button] != null && !OnClicks[button].equals(""))
    {
      html.append("onClick=\"");
      html.append(OnClicks[button]);
      html.append("\"");
    }
    
    html.append(">");
    
    if( showLabel )
    {
      html.append("<span " + getHtmlExtra() + ">");
      html.append(RadioButtons[button][RADIO_LABEL]);
      html.append("</span>");
    }

    boolean showErrorTag = false;
    if (errorTagIdx > -1 && errorTagIdx == button)
    {
      showErrorTag = true;
    }
    else if (errorTagIdx == -1 &&
             (SelectedButton == button || (SelectedButton < 0 && button == 0)))
    {
      showErrorTag = true;
    }

    if (showErrorTag)
    {
      html.append(renderErrorIndicator());
    }

    return( html.toString() );
  }
  
  /*
  ** public void setOnClick(int button, String OnClick)
  **
  ** Allows setting the OnClick JavaScript function for a particular button
  */
  public void setOnClick(int button, String onClick)
  {
    try
    {
      OnClicks[button] = onClick;
    }
    catch(Exception e)
    {
      log.error("setOnClick(" + button + ", " + onClick + ") " + e.toString());
    }
  }

  /*
  ** public String renderHtml()
  **
  ** Overrides base method to change how error indicator gets rendered.  Errors
  ** are now rendered within a specific radio button.
  **
  ** RETURN: rendered html representing the set of radio buttons.
  */
  public String renderHtml()
  {
    if(readOnly) {
      try {
        return RadioButtons[SelectedButton][RADIO_LABEL];
      }
      catch(Exception e) {
        return "";
      }
    }

    return renderHtmlField();
  }

  protected void validateData()
  {
    super.validateData();

    if ( hasError == false )
    {
      if ( (this.nullAllowed == false && (this.optionalCondition == null || this.optionalCondition.isMet())) && isBlank() )
      {
        hasError  = true;
        errorText = RadioButtonError;
      }
      else
      {
        hasError = false;
      }
    }
  }

  public void setOptionalCondition(Condition optionalCondition)
  {
    this.optionalCondition = optionalCondition;
  }

}
