/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlFormSection.java $

  Description:

  HtmlFormSection
  
  This is an HtmlTag based class that defines a section of a field
  form.  The section consists of a section header followed by a section
  body.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlFormSection implements HtmlRenderable
{
  // section html elements
  private HtmlTable       tblMain;
  private HtmlTable       tblHeader;
  private HtmlTable       tblSec;
  private HtmlTable       tblSubSec;
  
  private HtmlTableCell   tdTitle;
  private HtmlTableCell   tdHelp;
  
  private HtmlImage       imgMarginSpacer;
  
  public HtmlFormSection()
  {
    // main html table
    tblMain = new HtmlTable();
    tblMain.setProperty("class","formSectionFrame");
    tblMain.setProperty("align","center");
    tblMain.setProperty("cellspacing","0");
    tblMain.setProperty("cellpadding","1");
    tblMain.setProperty("width","100%");
    
    // section header
    tblMain.add(new HtmlTableRow()).add(new HtmlTableCell());
    HtmlTableCell tdMain 
              = tblMain.getRow(0).getCell(0);
    tdMain.setProperty("colspan","3");
    
    // section header table
    tblHeader = (HtmlTable)(tdMain.add(new HtmlTable()));
    tblHeader.setProperty("class","formSectionheader");
    tblHeader.setProperty("width","100%");
    tblHeader.setProperty("cellspacing","0");
    tblHeader.setProperty("cellpadding","4");
    
    HtmlTableRow trHeader 
              = tblHeader.add(new HtmlTableRow());

    // section header title cell
    tdTitle   = trHeader.add(new HtmlTableCell());
    tdTitle.setProperty("class","formSectionTitle");
    
    // section header help cell
    tdHelp    = trHeader.add(new HtmlTableCell());
    tdHelp.setProperty("class","formSectionHelp");
    tdHelp.setProperty("align","right");
    
    // section body frame cell
    HtmlTableRow trBody = tblMain.add(new HtmlTableRow());
    HtmlTableCell tdBody = trBody.add(new HtmlTableCell());
    tdBody.setProperty("width","100%");
    
    // section body margin table
    HtmlTable tblBody = (HtmlTable)tdBody.add(new HtmlTable());
    tblBody.setProperty("class","formSectionBody");
    tblBody.setProperty("cellspacing","0");
    tblBody.setProperty("cellpadding","0");
    
    trBody = tblBody.add(new HtmlTableRow());
    
    // left margin cell
    HtmlTableCell tdLeft = trBody.add(new HtmlTableCell());
    
    // margin spacer image
    imgMarginSpacer = (HtmlImage)tdLeft.add(new HtmlImage());
    imgMarginSpacer.setImage("/images/spcr_transp.gif");
    imgMarginSpacer.setWidth(10);
    imgMarginSpacer.setHeight(1);
    imgMarginSpacer.setProperty("border","0");
    tdLeft.setSuppressFormatting(true);
    
    // main body cell
    HtmlTableCell tdCenter = trBody.add(new HtmlTableCell());
    tdCenter.setProperty("width","100%");
    
    // main section table
    tblSec = (HtmlTable)tdCenter.add(new HtmlTable());
    tblSec.setProperty("width","100%");
    tblSec.setProperty("cellspacing","0");
    tblSec.setProperty("cellpadding","0");
    
    // right margin cell
    HtmlTableCell tdRight = trBody.add(new HtmlTableCell());
    tdRight.add(imgMarginSpacer);
    tdRight.setSuppressFormatting(true);
    
    //tblMain.setAllProperties("table","border","1");
  }
  
  /*
  ** Accessors
  */
  public void setSpacer(String spacerUrl)
  {
    imgMarginSpacer.setImage(spacerUrl);
  }
  
  public void setWidth(int width)
  {
    tblMain.setProperty("width",Integer.toString(width));
  }
  
  public void setMarginWidth(int width)
  {
    imgMarginSpacer.setWidth(width);
  }
  
  public void setTitle(String title)
  {
    tdTitle.add(new HtmlText(title));
  }
  
  public void setHelp(String help)
  {
    tdHelp.add(new HtmlText(help));
  }
  
  public void setHelp(HtmlRenderable hr)
  {
    tdHelp.add(hr);
  }
  
  public void setBordersVisible(boolean visible)
  {
    tblMain.setAllProperties("table","border",(visible ? "1" : "0"));
  }
  
  /*
  ** public String renderHtml()
  **
  ** Renders html with 0 indent level.
  **
  ** RETURNS: rendered html as a String.
  */
  public String renderHtml()
  {
    return renderHtml(0);
  }
  
  /*
  ** public String renderHtml(int indentLevel)
  **
  ** Renders html with specified indent level.
  **
  ** RETURNS: rendered html as a String.
  */
  public String renderHtml(int indentLevel)
  {
    return tblMain.renderHtml(indentLevel);
  }
  
  /*
  ** private HtmlTable getNewSubSecTable()
  **
  ** Creates a new subsection.
  **
  ** RETURNS: a reference to the table containing the new subsection.
  */
  private HtmlTable getNewSubSecTable()
  {
    HtmlTableRow tr = tblSec.add(new HtmlTableRow());
    HtmlTableCell td = tr.add(new HtmlTableCell());
    td.setProperty("width","100%");
    HtmlTable tbl = (HtmlTable)td.add(new HtmlTable());
    tbl.setProperty("width","100%");
    tbl.setProperty("cellspacing","0");
    tbl.setProperty("cellpadding","0");
    return tbl;
  }
  
  /*
  ** private HtmlTable getSubSecTable()
  **
  ** If no current subsection exists, a new subsection table is created.
  **
  ** RETURNS: the current subsection table.
  */
  private HtmlTable getSubSecTable()
  {
    if (tblSubSec == null)
    {
      tblSubSec = getNewSubSecTable();
    }
    return tblSubSec;
  }
  
  /*
  ** public HtmlTable addSubSecTable()
  **
  ** Causes a new subsection table to be created.
  **
  ** RETURNS: the new subsection table.
  */
  public HtmlTable addSubSecTable()
  {
    tblSubSec = getNewSubSecTable();
    return tblSubSec;
  }
  
  /*
  ** private boolean subSecIsEmpty()
  **
  ** Determines if any rows have been added to the current subsection.
  **
  ** RETURNS: true if current subsection is null or is empty.
  */
  private boolean subSecIsEmpty()
  {
    return (tblSubSec == null || tblSubSec.getContentSize() == 0);
  }

  /*
  ** public HtmlTableRow addRow(HtmlTableRow row)
  **
  ** Adds the table row to the form section.
  **
  ** RETURNS: reference to row added.
  */
  public HtmlTableRow addRow(HtmlTableRow row)
  {
    getSubSecTable().add(row);
    return row;
  }
  
  // template row used for default layout
  HtmlTableRow trTemplate;
  
  /*
  ** public HtmlTableRow setTemplateRow(HtmlTableRow trTemplate)
  **
  ** Allows a custom template row to be set.
  **
  ** RETURNS: the row being set.
  */
  public HtmlTableRow setTemplateRow(HtmlTableRow trTemplate)
  {
    this.trTemplate = trTemplate;
    return trTemplate;
  }
  
  /*
  ** public HtmlTableRow getTemplateRow()
  **
  ** Allows access to the template row.  If template row is null a default row
  ** is created.
  **
  ** RETURNS: the template row.
  */
  public HtmlTableRow getTemplateRow()
  {
    // if no template row exists create a 
    // default template row with one column
    if (trTemplate == null)
    {
      setColumns(1);
    }
    return trTemplate;
  }
  
  /*
  ** public HtmlTableRow setColumns(int colCount)
  **
  ** Updates the template row to contain the specified number of cells,
  ** evenly sized.  Starts a new sub section if current sub section is
  ** not empty.  Columns are created from the template column object.
  **
  ** RETURNS: refrenece to the new template row.
  */
  public HtmlTableRow setColumns(int colCount)
  {
    // create a new sub section if current is not empty
    if (!subSecIsEmpty())
    {
      getNewSubSecTable();
    }
    
    // create a new row
    HtmlTableRow tr = new HtmlTableRow();
    
    // add cells for each column
    int spacerCnt = 0;
    for (int i = 0; i < colCount; ++i)
    {
      // column spacer
      if (i > 0)
      {
        tr.add(new HtmlTableCell());
      }
      // label, spacer/required indicator, field
      tr.add(new HtmlTableCell()).setProperty("class","formText");
      tr.add(new HtmlTableCell()).setProperty("class","formText");
      tr.add(new HtmlTableCell()).setProperty("class","formText");
    }
    
    // set the new row as the template row
    setTemplateRow(tr);
    return tr;
  }
      
  /*
  ** public HtmlTableRow getNewRow()
  **
  ** Clones a new row from the template row.
  **
  ** RETURNS: a new empty row.
  */
  public HtmlTableRow getNewRow()
  {
    return getTemplateRow().cloneRow();
  }
  
  /*
  ** public HtmlTableRow getLastRow()
  **
  ** Finds the last row in the surrent subsection.
  **
  ** RETURNS: last row in subsection, null if subsection is empty.
  */
  public HtmlTableRow getLastRow()
  {
    HtmlTable tblSubSec = getSubSecTable();
    if (tblSubSec.getContentSize() == 0)
    {
      return getNewRow();
    }
    return tblSubSec.getRow(tblSubSec.getContentSize() - 1);
  }
  
  // used to flag a form as required
  protected HtmlSpan requiredIndicator = new HtmlSpan();
  {
    requiredIndicator.setProperty("class","formRequiredIndicator");
    requiredIndicator.add(new HtmlText("*"));
  }
  
  public HtmlSpan getRequiredIndicator()
  {
    return requiredIndicator;
  }
  
  /*
  ** public HtmlTableRow getNewSpanningRow()
  **
  ** Creates a row containing a single cell that spans all columns in table
  ** (based on template row).
  **
  ** RETURNS: the table row created.
  */
  public HtmlTableRow getNewSpanningRow()
  {
    // create row with single cell that spans template cells
    HtmlTableRow tr = new HtmlTableRow();
    HtmlTableCell td = tr.add(new HtmlTableCell());
    String colspanValue = Integer.toString(getTemplateRow().getContentSize());
    td.setProperty("colspan",colspanValue);
    td.setProperty("class","formText");
    return tr;
  }
  
  /*
  ** public void addSpacerRow(int height)
  **
  ** Adds a row with a spacer image of the specified height to create blank
  ** space on the form.
  */
  public void addSpacerRow(int height)
  {
    // clone the margin spacer image to get a spacer, size it, add it
    HtmlTableRow tr = getNewSpanningRow();
    HtmlTableCell td = tr.getCell(0);
    HtmlImage imgSpacer = (HtmlImage)imgMarginSpacer.getClone();
    imgSpacer.setProperty("height",Integer.toString(height));
    imgSpacer.setProperty("width","1");
    td.add(imgSpacer);
    td.setSuppressFormatting(true);
    addRow(tr);
  }
  
  /*
  ** public void add(int colIdx, String label, HtmlRenderable hr, boolean isRequired)
  **
  ** Adds a renderable object with a label to the last added row, if the
  ** column index indicates an empty pair of cells.  If the cells are
  ** not empty, a new row is created and added to the form.
  */
  public void add(int colIdx, String label, HtmlRenderable hr, boolean isRequired)
  {
    // get last row in sub section, 
    // or new one if sub section is empty
    HtmlTableRow tr = getLastRow();
    if (tr == null)
    {
      tr = getNewRow();
    }
    
    // determine if last row is too small or is not empty
    int labelIdx = colIdx * 4;
    int hrIdx = labelIdx + 2;
    if (tr.getContentSize() < hrIdx + 1
        || tr.getCell(labelIdx).getContentSize() != 0
        || tr.getCell(hrIdx).getContentSize() != 0)
    {
      // get a new row
      tr = addRow(getNewRow());
    }

    // check row size again to make sure it's large enough
    if (tr.getContentSize() > hrIdx)
    {
      // add the label and hr
      tr.getCell(labelIdx).add(new HtmlText(label));
      tr.getCell(hrIdx).add(hr);
      tr.getCell(labelIdx + 1)
        .add((isRequired ? (HtmlRenderable)requiredIndicator 
                         : (HtmlRenderable)new HtmlText("&nbsp;")));
        
      // add col spacer space if not first column
      if (labelIdx > 0)
      {
        tr.getCell(labelIdx - 1).add(new HtmlText("&nbsp;"));
      }
    }
  }
  public void add(int colIdx, String label, HtmlRenderable hr)
  {
    add(colIdx,label,hr,false);
  }
  public void add(String label, HtmlRenderable hr)
  {
    add(0,label,hr,false);
  }
  public void add(String label, HtmlRenderable hr, boolean isRequired)
  {
    add(0,label,hr,isRequired);
  }
  public void add(int colIdx, HtmlField hl, boolean isRequired)
  {
    add(colIdx,hl.getLabel(),hl,isRequired);
  }
  public void add(int colIdx, HtmlField hl)
  {
    add(colIdx,hl,false);
  }
  public void add(HtmlField hl, boolean isRequired)
  {
    add(0,hl,isRequired);
  }
  public void add(HtmlField hl)
  {
    add(0,hl,false);
  }
}
