/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/TaxIdField.java $

  Description:
  
  TaxIdField
  
  A field containing a 9-digit tax id (SSN or federal tax ID).  Validates
  the id and formats it when rendering it.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;

public class TaxIdField extends Field
{
  // validation scheme codes
  public static final int VAL_GENERIC     = 0;
  public static final int VAL_SSN         = 1;
  public static final int VAL_FED_TAX_ID  = 2;

  /*
  ** class TaxIdValidation
  **
  ** Validates a tax ID number.  May be configured to do validations
  ** specific to either SSN or Federal Tax ID rules.
  */
  public class TaxIdValidation implements Validation
  {
    private String errorText = "";
    
    private int valScheme;
    
    public TaxIdValidation(int valScheme)
    {
      this.valScheme = valScheme;
    }
    
    /*
    ** public boolean validate(String fieldData)
    **
    ** Always checks for length and invalid characters.  If valScheme is set
    ** to SSN then the id is validated against SSN rules, if scheme is set
    ** to Fed Tax ID then Fed Tax ID rules are used.
    **
    ** RETURNS: true if all validation passes, else false.
    */
    public boolean validate(String fieldData)
    {
      // don't try to validate missing or empty data
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }
      
      try
      {
        // see if invalid characters in the mix
        try
        {
          Long.parseLong(fieldData);
        }
        catch (NumberFormatException ne)
        {
          throw new Exception("Not a valid tax ID");
        }
      
        // check for invalid length
        if (fieldData.length() != 9)
        {
          throw new Exception("Tax ID must be 9 digits long");
        }
        
        // 
        // SSN Rules
        // 
        //  (Area number is leading three digits; group number is middle two 
        //   digits, serial number is last four digits.)
        //  
        //  1. VALID area number ranges: 001 - 665, 667 - 728, 764 - 772.
        //  2. VALID group number ranges: 01 - 99.
        //  3. VALID serial numbers ranges: 0001 - 9999.
        //
        if (valScheme == TaxIdField.VAL_SSN)
        {
          int area = Integer.parseInt(fieldData.substring(0,3));
          if (area == 0 || area > 772 || 
              (area > 728 && area < 764) || area == 666)
          {
            throw new Exception("Invalid SSN area");
          }
          
          int group = Integer.parseInt(fieldData.substring(3,5));
          if (group == 0)
          {
            throw new Exception("Invalid SSN group");
          }
          
          int serial = Integer.parseInt(fieldData.substring(5,9));
          if (serial == 0)
          {
            throw new Exception("Invalid SSN serial");
          }
        }
        //
        // Federal Tax ID Rules
        //
        //  (Prefix refers to first two digits of ID)
        //
        //  INVALID prefix values: 00, 07 - 09, 17 - 19, 28 - 29, 49, 78 - 79
        //
        else if (valScheme == TaxIdField.VAL_FED_TAX_ID)
        {
          int prefix = Integer.parseInt(fieldData.substring(0,2));
          switch (prefix)
          {
            case 0:
            case 7:
            case 8:
            case 9:
            case 17:
            case 18:
            case 19:
            case 28:
            case 29:
            case 49:
            case 78:
            case 79:
              throw new Exception("Invalid Fed Tax ID prefix");
          }
        }
        
        // no problems consider data fine
        return true;
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() == 0)
        {
          errorText = e.toString();
        }
      }
      
      return false;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  /*
  ** public TaxIdField(String fname,
  **                   String label,
  **                   int valScheme,
  **                   boolean nullAllowed)
  ** 
  ** Constructor.
  **
  ** Creates a field object sized to hold a tax id.  A validation scheme
  ** allows the object validation to be configured to do General validation
  ** and optionally do ssn or tax-id specific validation.
  */ 
  public TaxIdField(String fname,
                    String label,
                    int valScheme,
                    boolean nullAllowed)
  {
    // sizing allows 9 digits with up to 2 formatting chars
    // i.e. 999-99-9999 or 99-9999999
    super(fname,label,11,11,nullAllowed);
    addValidation(new TaxIdValidation(valScheme));
  }
  public TaxIdField(String fname,
                    String label,
                    boolean nullAllowed)
  {
    this(fname,label,VAL_GENERIC,nullAllowed);
  }
  public TaxIdField(String fname,
                    boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }
  
  public String getRenderData()
  {
    if (!hasError && fdata != null && fdata.length() == 9)
    {
      return  fdata.substring(0,3) + "-" + 
              fdata.substring(3,5) + "-" + 
              fdata.substring(5,9);
    }
    return fdata;
  }
  
  /*
  ** protected String processData(String rawData)
  **
  ** Strips out '-' and '.' characters for storage.  If other invalid
  ** characters are present the original string is returned.
  **
  ** RETURNS: filtered data if ok, else original raw data.
  */
  protected String processData(String rawData)
  {
    try
    {
      // scan through string, strip valid formatting chars
      StringTokenizer tok = new StringTokenizer(rawData," -.");
      StringBuffer tokBuf = new StringBuffer();
      while (tok.hasMoreTokens())
      {
        tokBuf.append(tok.nextToken());
      }
    
      // parse the stripped string to make sure it has only numeric data
      Long.parseLong(tokBuf.toString());
    
      // don't filter data that's wrong length
      if (tokBuf.length() > 9)
      {
        throw new Exception("bad length");
      }
    
      // store the filtered data
      return tokBuf.toString();
    }
    catch (Exception e) { }
    
    return rawData;
  }
}
