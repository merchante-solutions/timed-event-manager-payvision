/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/NumberField.java $

  Description:
  
  NumberField
  
  Extends field to define a numeric data field.  Allows specification of
  precision (fractional/float type numbers) and provides validation routine
  for the numeric data.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;
import com.mes.support.NumberFormatter;

public class NumberField extends Field
{
  /*
  ** public class NumberRangeValidation implements Validation
  **
  ** Validates a number field against a numeric range INCLUSIVE of both a lower bound and upper bound.
  ** Field data is cast to a float so that this validation may apply to both integers decimal numbers.
  */
  public class NumberRangeValidation implements Validation
  {
    float lbound;
    float ubound;
    int precision;
    
    private String errorText = "";
    
    public NumberRangeValidation(float lbound, float ubound, int precision)
    {
      this.lbound=lbound;
      this.ubound=ubound;
      this.precision=precision;
    }
    
    public boolean validate(String fieldData)
    {
      boolean isValid = true;

      try
      {
        // if data is null or empty then don't do validation
        // (evaluates as valid, let NotEmptyValidation handle it from here)
        if (fieldData != null && fieldData.length() > 0)
        {
          Float test;
          try
          {
            test = Float.valueOf(fieldData);
            if (test < lbound || test > ubound)
            {
              // some hackery to treat bounds as ints when precision is 0
              String lowerStr = null;
              String upperStr = null;
              if (precision == 0)
              {
                lowerStr = ""+(int)lbound;
                upperStr = ""+(int)ubound;
              }
              else
              {
                lowerStr = ""+lbound;
                upperStr = ""+ubound;
              }
              throw new Exception("Number must be within the range: "
                + lowerStr + " - " + upperStr);
            }
          }
          catch (NumberFormatException ne)
          {
            throw new Exception("Not a valid number");
          }
        }
      
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() <= 0)
        {
          errorText = e.toString();
        }
        isValid = false;
      }
    
      return isValid;
      
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  
  }
  
  /*
  ** public class NumericValidation implements Validation
  **
  ** A validation object that validates numeric data.
  */
  public class NumericValidation implements Validation
  {
    int precision;
    int size;
  
    public NumericValidation(int size, int precision)
    {
      this.size = size;
      this.precision = precision;
    }
  
    private String errorText = "";
  
    /*
    ** public boolean validate(String fieldData)
    **
    ** Determines if fieldData can convert to a numeric value that fits
    ** within the size and precision parameters.
    **
    ** RETURNS: true if fieldData contains a valid number, else false if 
    **          number is invalid or not within constraints.
    */
    public boolean validate(String fieldData)
    {
      boolean isValid = true;

      try
      {
        // if data is null or empty then don't do validation
        // (evaluates as valid, let NotEmptyValidation handle it from here)
        if (fieldData != null && fieldData.length() > 0)
        {
          // fractional numbers
          if (precision > 0)
          {
            Float test;
            try
            {
              test = Float.valueOf(fieldData);
            }
            catch (NumberFormatException ne)
            {
              throw new Exception("Not a valid number");
            }
        
            StringTokenizer st = new StringTokenizer(test.toString(),".");
            String front = st.nextToken();
            String back  = st.nextToken();
            if (back.length() > precision)
            {
              throw new Exception("Fractional portion may not exceed " + precision + " digits");
            }
            else if (front.length() > length - precision)
            {
              throw new Exception("Number is too large");
            }
          }
          // integers
          else
          {
            Long test;
            try
            {
              test = Long.valueOf(fieldData);
            }
            catch (NumberFormatException ne)
            {
              throw new Exception("Not a valid number");
            }
        
            if (test.toString().length() > length)
            {
              throw new Exception("Number is too large");
            }
          }
        }
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() <= 0)
        {
          errorText = e.toString();
        }
        isValid = false;
      }
    
      return isValid;
    }
  
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  // members
  protected   String            NumberFormat      = null;
  protected   int               Precision         = 0;
  
  public NumberField(String fname,
                     String label,
                     int length,
                     int htmlSize,
                     boolean nullAllowed,
                     int precision)
  {
    super(fname,label,length,htmlSize,nullAllowed);
    addValidation(new NumericValidation(length,precision));
    Precision = precision;
  }
  public NumberField(String fname,
                     int length,
                     int htmlSize,
                     boolean nullAllowed,
                     int precision)
  {
    this(fname,fname,length,htmlSize,nullAllowed,precision);
  }
  
  public NumberField(String fname,
                     String label,
                     int length,
                     int htmlSize,
                     boolean nullAllowed,
                     int precision,
                     float lbound,
                     float ubound)
  {
    this(fname,label,length,htmlSize,nullAllowed,precision);
    addValidation(new NumericValidation(length,precision));
    addValidation(new NumberRangeValidation(lbound,ubound,precision));
    Precision = precision;
  }
  public NumberField(String fname,
                     int length,
                     int htmlSize,
                     boolean nullAllowed,
                     int precision,
                     float lbound,
                     float ubound)
  {
    this(fname,fname,length,htmlSize,nullAllowed,precision,lbound,ubound);
  }
  
  public String getData( )
  {
    String      retVal      = super.getData();
    
    if ( NumberFormat != null )
    {
      try
      {
        retVal = NumberFormatter.getDoubleString( Double.parseDouble( retVal ), 
                                                  NumberFormat );
      }
      catch( Exception e )
      {
        // invalid format
      }  
    }
    return( retVal );
  }
  
  public int getPrecision()
  {
    return( Precision );
  }
  
  public void setNumberFormat( String newFormat )
  {
    NumberFormat = newFormat;
  }
}
