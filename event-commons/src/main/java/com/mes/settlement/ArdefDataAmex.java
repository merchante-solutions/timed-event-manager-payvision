package com.mes.settlement;

import java.sql.Date;

public class ArdefDataAmex extends ArdefDataBase 
  {
    protected Date    effectiveDate = null;
    protected String  productCode = null;
    protected String  amexCmBin = null;
    protected int     debitType = 0;
	
    /** To get the effectiveDate
     * @return effectiveDate
     */
    public Date getEffectiveDate() 
      {
        return this.effectiveDate;
      }

	/** To set the effectiveDate
	 * @param effectiveDate
	 */
	public void setEffectiveDate(Date effectiveDate) 
      {
	    this.effectiveDate = effectiveDate;
	  }
	
	/** To get the productCode
	 * @return productCode
	 */
	public String getProductCode() 
	  {
		return productCode;
	  }

	/** To set the productCode
	 * @param productCode
	 */
	public void setProductCode(String productCode) 
	  {
		this.productCode = productCode;
	  }

	/** To get the amexCmBin
	 * @return amexCmBin
	 */
	public String getAmexCmBin() 
	  {
	    return this.amexCmBin;
	  }

	/** To set the amexCmBin
	 * @param amexCmBin
	 */
	public void setAmexCmBin(String amexCmBin) 
	  {
		this.amexCmBin = amexCmBin;
	  } 

	/* (non-Javadoc)
	 * @see com.mes.settlement.ArdefDataBase#isInRange(java.lang.String)
	 */
	@Override
	public boolean isInRange(String cardNumber) 
	  {
		return false;
	  }

	/* (non-Javadoc)
	 * @see com.mes.settlement.ArdefDataBase#isForeignIssuer(int)
	 */
	@Override
	public boolean isForeignIssuer(int recBatchType) 
	  {
        boolean foreignIssuer = false;
        foreignIssuer = (getProductCode() != null && getProductCode().equalsIgnoreCase("IB")) ? true:false;
        return foreignIssuer;
	  }

	/* (non-Javadoc)
	 * @see com.mes.settlement.ArdefDataBase#getCardTypeEnhanced()
	 */
	@Override
	public String getCardTypeEnhanced() 
	  {
		return null;
      }

	/* (non-Javadoc)
	 * @see com.mes.settlement.ArdefDataBase#getCardTypeFull()
	 */
	@Override
	public String getCardTypeFull() 
	  {
		String cardTypeFull = "AM";
		return cardTypeFull;
	  }

	/* (non-Javadoc)
	 * @see com.mes.settlement.ArdefDataBase#getDebitType()
	 */
	@Override
	public int getDebitType() 
	  {
		return this.debitType;
	  }
	
	/** To set the debitType
	 * @param debitType
	 */
	public void setDebitType(int debitType) 
	  {
	    this.debitType = debitType;
	  }
  }
