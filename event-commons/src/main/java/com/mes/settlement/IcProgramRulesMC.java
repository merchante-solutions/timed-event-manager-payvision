/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/IcProgramRulesMC.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-04-16 10:23:37 -0700 (Sat, 16 Apr 2011) $
  Version            : $Revision: 18712 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import com.mes.support.StringUtilities;

public class IcProgramRulesMC
{
  protected   String        Ird                         = null;
  protected   String        CardType                    = null;
  protected   double        AmountMax                   = 0.0;
  protected   double        AmountMin                   = 0.0;
  protected   String        AmountMaxExemptSics         = null;
  protected   double        AmountTolerance             = 0.0;
  protected   String        AmountToleranceExemptions   = null;
  protected   String        AmountToleranceExemptionSics= null;
  protected   boolean       FleetAtFuel                 = false;
  protected   String        TimelinessExemptions        = null;
  protected   String        AddendumRequired            = null;
  protected   String        EligibilityFlag             = null;
  protected   String        PosEntryModeAllowed         = null;
  protected   String        ProgramRegIdMask            = null;
  protected   String        IcComplianceSwitch          = null;

  protected   String        AuthRequiredFlags           = null;
  protected   String        McAssignedIdFlags           = null;
  protected   String        TimelinessFlags             = null;

  public IcProgramRulesMC()
  {
  }

  public String getIrd()                              { return(Ird); }
  public void   setIrd(String value)                  { Ird = value; }

  public String getCardType()                         { return(CardType); }
  public void   setCardType(String value)             { CardType = value; }

  public double getAmountMax()                        { return(AmountMax); }
  public void   setAmountMax(double value)            { AmountMax = value; }
  
  public double getAmountMin()                        { return(AmountMin); }
  public void   setAmountMin(double value)            { AmountMin = value; }
  
  public String getAmountMaxExemptSics()              { return(AmountMaxExemptSics); }
  public void   setAmountMaxExemptSics(String value)  { AmountMaxExemptSics = value; }

  public double getAmountTolerance()                  { return(AmountTolerance); }
  public void   setAmountTolerance(double value)      { AmountTolerance = value; }

  public String getAmountToleranceExemptions()            { return(AmountToleranceExemptions); }
  public void   setAmountToleranceExemptions(String value){ AmountToleranceExemptions = value; }
  
  public String getAmountToleranceExemptSics()            { return(AmountToleranceExemptionSics); }
  public void   setAmountToleranceExemptSics(String value){ AmountToleranceExemptionSics = value; }
  
  public boolean  getFleetAtFuel()                    { return(FleetAtFuel); }
  public void     setFleetAtFuel(boolean value)       { FleetAtFuel = value; }

  public String getTimelinessExemptions()             { return(TimelinessExemptions); }
  public void   setTimelinessExemptions(String value) { TimelinessExemptions = value; }

  public String getAddendumRequired()                 { return(AddendumRequired); }
  public void   setAddendumRequired(String value)     { AddendumRequired = value; }

  public String getEligibilityFlag()                  { return(EligibilityFlag); }
  public void   setEligibilityFlag(String value)      { EligibilityFlag = value; }

  public String getPosEntryModeAllowed()              { return(PosEntryModeAllowed); }
  public void   setPosEntryModeAllowed(String value)  { PosEntryModeAllowed = value; }

  public String getProgramRegIdMask()                 { return(ProgramRegIdMask); }
  public void   setProgramRegIdMask(String value)     { ProgramRegIdMask = value; }
  
  public String getAuthRequiredFlags()                { return(AuthRequiredFlags); }
  public void   setAuthRequiredFlags(String value)    { AuthRequiredFlags = value; }
  
  public String getMcAssignedIdFlags()                { return(McAssignedIdFlags); }
  public void   setMcAssignedIdFlags(String value)    { McAssignedIdFlags = value; }
  
  public String getTimelinessFlags()                  { return(TimelinessFlags); }
  public void   setTimelinessFlags(String value)      { TimelinessFlags = value; }
  
  public String getIcComplianceSwitch()               { return(IcComplianceSwitch); }
  public void   setIcComplianceSwitch(String value)   { IcComplianceSwitch = value; }
  
  public void setRuleData(ResultSet resultSet)
    throws java.sql.SQLException
  {
    setIrd ( StringUtilities.leftJustify( resultSet.getString(  "ird"                             ), 2, ' ') );
    setFleetAtFuel          ( "Y".equals( resultSet.getString(  "fleet_at_fuel"                   ) ) );
    setAuthRequiredFlags                ( resultSet.getString(  "gcms_auth_reqd"                  ) +
                                          resultSet.getString(  "icc_auth_reqd"                   ) );
    setMcAssignedIdFlags                ( resultSet.getString(  "mc_assigned_id_reqd_validated"   ) );
    setTimelinessFlags                  ( resultSet.getString(  "gcms_timeliness"                 ) +
                                          resultSet.getString(  "icc_timeliness"                  ) );
    setIcComplianceSwitch               ( resultSet.getString(  "ic_compliance_switch"            ) );
    setCardType                         ( resultSet.getString(  "card_type"                       ) );
    setAmountMin                        ( resultSet.getDouble(  "amt_min"                         ) );
    setAmountMax                        ( resultSet.getDouble(  "amt_max"                         ) );
    setAmountMaxExemptSics              ( resultSet.getString(  "amt_max_exempt_sics"             ) );
    setAmountTolerance                  ( resultSet.getDouble(  "amt_tolerance"                   ) );
    setAmountToleranceExemptions        ( resultSet.getString(  "amt_tolerance_exemptions"        ) );
    setAmountToleranceExemptSics        ( resultSet.getString(  "amt_tolerance_exempt_sics"       ) );
    setTimelinessExemptions             ( resultSet.getString(  "timeliness_exemptions"           ) );
    setPosEntryModeAllowed              ( resultSet.getString(  "pos_entry_mode_allowed"          ) );
    setAddendumRequired                 ( resultSet.getString(  "addendum_required"               ) );
    setEligibilityFlag                  ( resultSet.getString(  "eligibility_flag"                ) );
    setProgramRegIdMask                 ( resultSet.getString(  "program_reg_id_mask"             ) );
  }
  
  public void showData()
  {
    System.out.println( "Ird                      : " + getIrd());
    System.out.println( "CardType                 : " + getCardType());
    System.out.println( "AmountMax                : " + getAmountMax());
    System.out.println( "AmountMin                : " + getAmountMin());
    System.out.println( "AmountMaxExemptSics      : " + getAmountMaxExemptSics());
    System.out.println( "AmountTolerance          : " + getAmountTolerance());
    System.out.println( "AmountToleranceExemptions: " + getAmountToleranceExemptions());
    System.out.println( "AmountToleranceExemptSics: " + getAmountToleranceExemptSics());
    System.out.println( "FleetAtFuel              : " + getFleetAtFuel());
    System.out.println( "TimelinessExemptions     : " + getTimelinessExemptions());
    System.out.println( "AddendumRequired         : " + getAddendumRequired());
    System.out.println( "EligibilityFlag          : " + getEligibilityFlag());
    System.out.println( "PosEntryModeAllowed      : " + getPosEntryModeAllowed());
    System.out.println( "ProgramRegIdMask         : " + getProgramRegIdMask());
    System.out.println( "AuthRequiredFlags        : " + getAuthRequiredFlags());
    System.out.println( "McAssignedIdFlags        : " + getMcAssignedIdFlags());
    System.out.println( "TimelinessFlags          : " + getTimelinessFlags());
    System.out.println( "IcComplianceSwitch       : " + getIcComplianceSwitch());
  }
}
