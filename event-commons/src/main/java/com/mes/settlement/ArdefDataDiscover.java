/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/ArdefDataDiscover.java $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import com.mes.constants.mesConstants;

public class ArdefDataDiscover
  extends ArdefDataBase
{
  // Discover ARDEF Card Types
  public static final String  CT_CONSUMER_CREDIT_REWARDS        = "001";  // Consumer Credit - Rewards
  public static final String  CT_COMMERCIAL_CREDIT              = "002";  // Commercial Credit
  public static final String  CT_CONSUMER_DEBIT                 = "003";  // Consumer Debit
  public static final String  CT_COMMERCIAL_DEBIT               = "004";  // Commercial Debit
  public static final String  CT_CONSUMER_PREPAID_GIFT          = "005";  // Consumer Prepaid - Gift
  public static final String  CT_CONSUMER_PREPAID               = "006";  // Consumer Prepaid - ID Known
  public static final String  CT_CONSUMER_CREDIT_PREMIUM        = "007";  // Consumer Credit - Premium
  public static final String  CT_CONSUMER_CREDIT_CORE           = "008";  // Consumer Credit – Core
  public static final String  CT_PRIVATE_LABEL                  = "009";  // Private Label Credit Card
  public static final String  CT_COMMERIAL_CREDIT_EXEC_BUS      = "010";  // Commercial Credit - Executive Business
  public static final String  CT_CONSUMER_CREDIT_PREMIUM_PLUS   = "011";  // Consumer Credit - Premium Plus
  public static final String  CT_COMMERCIAL_PREPAID             = "012";  // Commercial Prepaid - Non-Reloadable
  public static final String  CT_PAYPAL_PAYMENT_CARD            = "013";  // PayPal Payment Card
  public static final String  CT_COMMERCIAL_PREPAID_RELOADABLE  = "015";  // Commercial Prepaid - Reloadable

  protected static final String DiscoverConsumerCardTypes =
      ""  + CT_CONSUMER_CREDIT_REWARDS
    + "," + CT_CONSUMER_DEBIT
    + "," + CT_CONSUMER_PREPAID_GIFT
    + "," + CT_CONSUMER_PREPAID
    + "," + CT_CONSUMER_CREDIT_PREMIUM
    + "," + CT_CONSUMER_CREDIT_CORE
    + "," + CT_CONSUMER_CREDIT_PREMIUM_PLUS;
    
  protected static final String DiscoverCheckCardTypes =
          String.format("%s,%s,%s,%s,%s,%s", CT_CONSUMER_DEBIT, CT_COMMERCIAL_DEBIT,
                  CT_CONSUMER_PREPAID_GIFT, CT_CONSUMER_PREPAID, CT_COMMERCIAL_PREPAID, CT_COMMERCIAL_PREPAID_RELOADABLE);

  private static final String DiscoverCommercialCardTypes =
          String.format("%s,%s,%s,%s,%s", CT_COMMERCIAL_CREDIT, CT_COMMERCIAL_DEBIT, CT_COMMERIAL_CREDIT_EXEC_BUS,
          CT_COMMERCIAL_PREPAID, CT_COMMERCIAL_PREPAID_RELOADABLE);
    
  protected int     AccountLevelInd             = 0;
  protected int     AtmInd                      = 0;
  protected int     CardNumberLengthMin         = 0;
  protected int     CardNumberLengthMax         = 0;
  protected String  CardProductId               = null;
  protected int     DebitType                   = 0;
  protected Date    EffectiveDate               = null;
  protected int     ForeignCardInd              = 0;
  protected String  IssuerCountryCode           = null;
  protected int     IssuingNetwork              = 0;
  protected int     PrefixLength                = -1;
  protected String  Prefix                      = null;
  protected int     StipEligible                = 0;
  
  public boolean isInRange( String cardNumber )
  {
    boolean retVal = false;
    try
    {
      if ( Prefix.equals( cardNumber.substring(0,PrefixLength) ) )
      {
        retVal = true;
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public boolean isForeignIssuer( int recBatchType ) 
  { 
    boolean retVal = false;

    switch( recBatchType )
    {
      case mesConstants.MBS_BT_CIELO:     retVal = true;                              break;  // Brazil
      default:                            retVal = ( ForeignCardInd != 0 );           break;  // United States
    }
    return( retVal );
  }
  
  public String getCardTypeEnhanced()
  {
    String    cardTypeEnh = "DS";
    switch( IssuingNetwork )
    {
      case 0: cardTypeEnh = "DS"; break;
      case 1: cardTypeEnh = "DC"; break;
      case 2: cardTypeEnh = "JC"; break;
      case 3: cardTypeEnh = "CU"; break;
      case 4: cardTypeEnh = "PP"; break;
    }
    
    if ( CardProductId != null && DiscoverCommercialCardTypes.indexOf(CardProductId) >= 0 )
    {
      cardTypeEnh   += "B";   // commercial credit or debit
    }
    else if ( CardProductId != null && DiscoverCheckCardTypes.indexOf(CardProductId) >= 0 )
    {
      cardTypeEnh   += "D";   // consumer debit
    }
    else
    {
      cardTypeEnh   += "C";   // consumer credit
    }
    return( cardTypeEnh );
  }
  
  public String getCardTypeFull()
  {
    return( "DS" );
  }
  
  public int     getAccountLevelInd()     { return( AccountLevelInd );      }
  public int     getAtmInd()              { return( AtmInd );               }
  public int     getCardNumberLengthMin() { return( CardNumberLengthMin );  }
  public int     getCardNumberLengthMax() { return( CardNumberLengthMax );  }
  public String  getCardProductId()       { return( CardProductId );        }
  public int     getDebitType()           { return( DebitType );            }
  public Date    getEffectiveDate()       { return( EffectiveDate );        }
  public int     getForeignCardInd()      { return( ForeignCardInd );       }
  public String  getIssuerCountryCode()   { return( IssuerCountryCode );    }
  public int     getIssuingNetwork()      { return( IssuingNetwork );       }
  public int     getPrefixLength()        { return( PrefixLength );         }
  public String  getPrefix()              { return( Prefix );               }
  public int     getStipEligible()        { return( StipEligible );         }

  public void setAccountLevelInd      ( int value     ) { AccountLevelInd = value;      }
  public void setAtmInd               ( int value     ) { AtmInd = value;               }
  public void setCardNumberLengthMin  ( int value     ) { CardNumberLengthMin = value;  }
  public void setCardNumberLengthMax  ( int value     ) { CardNumberLengthMax = value;  }
  public void setCardProductId        ( String value  ) { CardProductId = value;        }
  public void setDebitType            ( int value     ) { DebitType = value;            }
  public void setEffectiveDate        ( Date value    ) { EffectiveDate = value;        }
  public void setForeignCardInd       ( int value     ) { ForeignCardInd = value;       }
  public void setIssuerCountryCode    ( String value  ) { IssuerCountryCode = value;    }
  public void setIssuingNetwork       ( int value     ) { IssuingNetwork = value;       }
  public void setPrefixLength         ( int value     ) { PrefixLength = value;         }
  public void setPrefix               ( String value  ) { Prefix = value;               }
  public void setStipEligible         ( int value     ) { StipEligible = value;         }
  
  public void showData()
  {
    System.out.println("AccountLevelInd     : " + AccountLevelInd       );
    System.out.println("AtmInd              : " + AtmInd                );
    System.out.println("CardNumberLengthMin : " + CardNumberLengthMin   );
    System.out.println("CardNumberLengthMax : " + CardNumberLengthMax   );
    System.out.println("EffectiveDate       : " + EffectiveDate         );
    System.out.println("ForeignCardInd      : " + ForeignCardInd        );
    System.out.println("IssuerCountryCode   : " + IssuerCountryCode     );
    System.out.println("IssuingNetwork      : " + IssuingNetwork        );
    System.out.println("PrefixLength        : " + PrefixLength          );
    System.out.println("Prefix              : " + Prefix                );
    System.out.println("CardProductId       : " + CardProductId         );
    System.out.println("DebitType           : " + DebitType             );
    System.out.println("StipEligible        : " + StipEligible          );
  }
}
