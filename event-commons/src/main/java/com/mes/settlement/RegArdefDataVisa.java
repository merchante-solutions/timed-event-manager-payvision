/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/RegArdefDataVisa.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-08-12 16:46:29 -0700 (Mon, 12 Aug 2013) $
  Version            : $Revision: 21393 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

public class RegArdefDataVisa
  extends ArdefDataBase
{
  protected long   RangeHigh                    = 0L;
  protected long   RangeLow                     = 0L;
  
  public long   getRangeHigh()              { return( RangeHigh );    }
  public long   getRangeLow()               { return( RangeLow );     }

  public void setRangeHigh( long value )    { RangeHigh = value;      }
  public void setRangeLow( long value )     { RangeLow = value;       }
  
  // unused methods from base class
  public boolean isInRange( String cardNumber ) { return( false );  }   
  public boolean isForeignIssuer( int recBT)    { return( false );  }
  public String getCardTypeEnhanced()           { return( null );   }
  public String getCardTypeFull()               { return( null );   }
  public int    getDebitType()                  { return( -1 );     }
  
  public void showData()
  {
    System.out.println("RangeHigh               : " + RangeHigh );
    System.out.println("RangeLow                : " + RangeLow );
  }
}
