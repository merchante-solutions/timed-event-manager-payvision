/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordMC.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-05-22 10:40:55 -0700 (Fri, 22 May 2015) $
  Version            : $Revision: 23635 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.CurrencyCodeConverter;
import com.mes.tools.SicCodes;
//TLV tools to extract necessary information from ICC data
import masthead.formats.iso.TlvFieldData;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.util.ByteUtil;
import masthead.util.Util13;

public class SettlementRecordMC extends SettlementRecord
{
  public SettlementRecordMC()
  {
    super(mesConstants.MBS_BT_VISAK, "MC", SettlementRecord.SETTLE_REC_MC);
    DbaZipMaxLength = 10;
  }

  // all MC_PROC_ID_s (forwarding institution id's) should be right-justified, zeroed-filled to length 6
  public static final String    MC_PROC_ID_CIELO    = "012088";
  public static final String    MC_PROC_ID_MES      = "011606";
  public static final String    MC_PROC_ID_DEFAULT  = MC_PROC_ID_MES;
  
  private static Boolean fxSettingsErrorLogged = false;
  
  private   String    ForwardingInstitutionId       = MC_PROC_ID_DEFAULT;
  private   int       CurrencyDecimalPlaces         = -1;
  
  private IpmOfflineMessage buildCommercialCardAddendum( )
    throws Exception
  {
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    msg.setMTI("1644");
    
    msg.setDataElement(24, "696");              // Function Code (696 = Financial Detail Addendum)
    msg.setDataElement(33, ForwardingInstitutionId);
    
    // private data subelements
    msg.setPrivateDataSubelement(501, encodeTransactionDescription(98,0,1) ); // Transaction Description, 1 = idx for main addendum record
    
    msg.setPrivateDataSubelement(595, getData("card_acceptor_type") );  // 595 - Card Acceptor Type (bunch of business characteristics

    msg.setPrivateDataSubelement(596, encodeTaxId() );                  // 596 - Card Acceptor Tax ID
    
    msg.setDataElement(94, getData("ica_number") );     // acquiring ICA value
    
    if( !getField("customer_code").isBlank() )
    {
      msg.setPrivateDataSubelement(508, getData("customer_code"));    // 508 - Customer Code
    }
    
    if( getData("addendum_required").indexOf("TT") >= 0 )
    {
      msg.setPrivateDataSubelement(597, encodeTaxAmount() );      // 597 - Tax Amount
    
      if ( getDouble("tax_amount") == 0.0 )
      {
        msg.setPrivateDataSubelement(686, "1" ); // 686 - Tax Exempt Indicator (1 = tax exempt)
      }
    }
    else if( getData("addendum_required").indexOf("T0") >= 0 )
    {
      // subfields 1 & 2 & 3 = 12 digit tax amount (all zeroes) & tax exponent (2) & tax sign (C or D, it can always be 'D')
      String exemptAmount = "000000000000"
                            + CurrencyDecimalPlaces
                            + "D";
      msg.setPrivateDataSubelement(597, exemptAmount );   // 597 - Tax Amount
      msg.setPrivateDataSubelement(686, "1" );            // 686 - Tax Exempt Indicator (1 = tax exempt)
    }
    
    return( msg );
  }
  
  private IpmOfflineMessage buildInstallmentAddendum( )
    throws Exception
  {
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    msg.setMTI("1644");
    
    msg.setDataElement(24, "696");  // Function Code: 696 = Financial Detail Addendum
    msg.setDataElement(32, StringUtilities.rightJustify(getData("ica_number"),11,'0'));
    msg.setDataElement(33, ForwardingInstitutionId);
    msg.setDataElement(94, StringUtilities.rightJustify(getData("ica_number"),11,'0')); // acquiring ICA value
    
    msg.setPrivateDataSubelement(501, encodeTransactionDescription(99,0,1) ); // Transaction Description, 1 = idx for main addendum record
    
    int     installmentCount  = getInt("multiple_clearing_seq_count");
    
    if( installmentCount > 0 )      // has installments
    {
      String  freeFormDesc      = null;
      String  installmentType   = null;
      
      if( installmentCount >= 50 )
      {
        installmentCount -= 50;     // remove visa's "+50 for parcelado emissor"
        installmentType = "403";    // emissor
        freeFormDesc    = "00000000000000";
      }
      else
      {
        installmentType = "407";    // loja
        freeFormDesc = StringUtilities.rightJustify(getData("multiple_clearing_seq_num"),2,'0')
                     + TridentTools.encodeAmount( getDouble("transaction_amount", 1.0), 12, CurrencyDecimalPlaces );
      }

//    double boardingFee  = getDouble("boarding_fee", 1.0);
//    double downPayment  = getDouble("down_payment", 1.0);
//    if( boardingFee > 0.00 || downPayment > 0.00 ) // possibly supposed to be all airline
//    {
//      installmentType     = "488";
//      freeFormDesc += TridentTools.encodeAmount( boardingFee, 12, CurrencyDecimalPlaces )
//                   +  TridentTools.encodeAmount( downPayment, 12, CurrencyDecimalPlaces );
//    }
//    else
      {
        freeFormDesc += "000000000000000000000000";
      }
      freeFormDesc = TridentTools.encodeAmount( getDouble("auth_amount_total", 1.0), 12, CurrencyDecimalPlaces )
                   + StringUtilities.rightJustify(String.valueOf(installmentCount),2,'0')
                   + freeFormDesc;
                   
      msg.setPrivateDataSubelement(663, installmentType + freeFormDesc);
    }
    return( msg );
  }
  
  private IpmOfflineMessage buildLodgingAddendum( )
    throws Exception
  {
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    msg.setMTI("1644");
    
    msg.setDataElement(24, "696");  // Function Code: 696 = Financial Detail Addendum
    msg.setDataElement(33, ForwardingInstitutionId);
    
    msg.setPrivateDataSubelement(501, encodeTransactionDescription(6,0,1) );
    
    // all hotel transactions get customer service phone# and property phone #
    msg.setPrivateDataSubelement(552, StringUtilities.leftJustify(getData("customer_service_phone"),17,' ')); // Customer Service Toll-free #
    msg.setPrivateDataSubelement(577, StringUtilities.leftJustify(getData("phone_number"          ),17,' ')); // Property phone #
    
    // PDS0574 - Check-In Date
    // PDS0575 - Check-Out Date
    // PDS0576 - Folio Number
    msg.setPrivateDataSubelement(574, DateTimeFormatter.getFormattedDate(getDate("statement_date_begin"), "yyMMdd") );
    msg.setPrivateDataSubelement(575, DateTimeFormatter.getFormattedDate(getDate("statement_date_end"), "yyMMdd") );
    msg.setPrivateDataSubelement(576, getData("purchase_id") );
    
    if( getData("addendum_required").indexOf("L2") >= 0 )
    {
      // some extra stuff for this Interchange level of hotel transaction
      msg.setPrivateDataSubelement(580, TridentTools.encodeAmount(getDouble("rate_daily"),12,CurrencyDecimalPlaces)+CurrencyDecimalPlaces);
      msg.setPrivateDataSubelement(581, TridentTools.encodeAmount(getDouble("total_room_tax"),12,CurrencyDecimalPlaces)+CurrencyDecimalPlaces+getData("debit_credit_indicator"));
      msg.setPrivateDataSubelement(703, TridentTools.encodeInteger(getInt("hotel_rental_days"),4) );
      msg.setPrivateDataSubelement(711, getData("hotel_fire_safety_act_ind") );
    }
    
    msg.setDataElement(94, getData("ica_number") );     // acquiring ICA value
    
    return( msg ); 
  }
  
  private IpmOfflineMessage buildLineItemDetail( LineItem item, int idx )
    throws Exception
  {
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    msg.setMTI("1644");
    
    msg.setDataElement(24, "696");  // Function Code: 696 = Financial Detail Addendum
    msg.setDataElement(33, ForwardingInstitutionId);
    
    msg.setPrivateDataSubelement(501, encodeTransactionDescription(98,950,idx ) );  // Transaction Description
    
    // Jack says we will always have all the line item fields so put them all in
    msg.setPrivateDataSubelement(641, item.getData("product_code") );      // Product Code
    msg.setPrivateDataSubelement(642, item.getData("item_description") );  // Item Description
    msg.setPrivateDataSubelement(643, TridentTools.encodeAmount(item.getDouble("item_quantity"),12)+"2"); // Item Quantity (plus exponent)
    msg.setPrivateDataSubelement(645, item.getData("item_unit_measure") );  // Item Unit of Measure
    msg.setPrivateDataSubelement(646, TridentTools.encodeAmount(item.getDouble("item_unit_price"),12,CurrencyDecimalPlaces)+CurrencyDecimalPlaces);  // Item Unit Cost (plus exponent)
    msg.setPrivateDataSubelement(647, TridentTools.encodeAmount(item.getDouble("extended_item_amount"),12,CurrencyDecimalPlaces)+CurrencyDecimalPlaces+item.getData("debit_credit_indicator")); // Extended Item Cost (total cost of line item) + exponent + debit/credit indicator
    msg.setPrivateDataSubelement(648, encodeItemDiscount(item) );   // Item Discount
    msg.setPrivateDataSubelement(654, item.getData("debit_credit_indicator") );
    msg.setPrivateDataSubelement(682, encodeItemTaxData(item) );
    
    msg.setDataElement(94, getData("ica_number") );     // acquiring ICA value
    
    return( msg );
  }
  
  private List buildFirstPresentmentRecords()
    throws Exception
  {
    List    recs    = new ArrayList(1);
    
    // create IPM message
    IpmOfflineMessage msg = new IpmOfflineMessage();
    //IRD V5 related constants
    final String IRDV5 = new String("V5");
    final String PROCESS_CODE_20 = new String("20");
    
    // add 1240 (First Presentment record)
    msg.setMTI("1240");
    msg.setDataElement(2, getCardNumberFull());           // Primary Account Number (PAN)
    msg.setDataElement(3, getData("processing_code")+     // Processing Code
                          "00" + "00" );                  // cardholder_from_type & cardholder_to_type
    msg.setDataElement(4,                                 // Amount, Transaction
      TridentTools.encodeAmount( getDouble("transaction_amount", 1.0), 12, CurrencyDecimalPlaces ) );
    msg.setDataElement(12,                                // Date and Time, Local Transaction
      DateTimeFormatter.getFormattedDate( getDate("transaction_time","MM/dd/yyyy HH:mm:ss"), "yyMMddHHmmss" ) );  
    msg.setDataElement(22, getData("pos_data_code"));     // Point of Service Data Code
    msg.setDataElement(24, "200");                        // Function Code (200 = First Presentment)
    msg.setDataElement(26, getData("sic_code"));          // Card Acceptor Business Code (MCC)
    msg.setDataElement(31, getData("reference_number"));  // Acquirer Reference Data
    msg.setDataElement(33, ForwardingInstitutionId);      // Forwarding Institution Id
    if ( !isFieldBlank("auth_code") )
    {
      msg.setDataElement(38, getData("auth_code"));         // Approval Code
    }      
    if ( getInt("service_code") != 0 )
    {
      msg.setDataElement(40, StringUtilities.rightJustify(getData("service_code"),3,'0') );
    }
    msg.setDataElement(42, getData("merchant_number"));   // Card Acceptor ID Code
    msg.setDataElement(43, encodeNameLocation());         // Card Acceptor Name/Location
    
    /**isEmvEnabled = true*/
    
    if( isEmvEnabled && !isFieldBlank("icc_data") )
    {
        // ICC / EMV data
        String emvData = getData("icc_data");
        int cardSequenceNumber = getInt("card_sequence_number");
        TlvFieldData input = new TlvFieldData();
        input.unpack(ByteUtil.parseHexString(emvData), 0, true);

        //Card Sequence Number is picked up from TAG-5F34 of EMV Data
        if(input.hasTag(mesConstants.EMV_TAG_APP_PAN_SEQ_NUM)){
            msg.setDataElement(23, StringUtilities.rightJustify(input.getValue(mesConstants.EMV_TAG_APP_PAN_SEQ_NUM),3,'0'));
            
            //Removing the EMV_TAG_APP_PAN_SEQ_NUM tag from EmvData which is not mandatory for Data Element 55
            input.removeTag(mesConstants.EMV_TAG_APP_PAN_SEQ_NUM);
            msg.setDataElement(55, ByteUtil.parseHexString(input.get()));
        } else {
            if(cardSequenceNumber != 0){
              msg.setDataElement(23, StringUtilities.rightJustify(String.valueOf(cardSequenceNumber),3,'0'));
            }
            msg.setDataElement(55, ByteUtil.parseHexString(input.get()));
        }

    }

    
    if( !isFieldBlank("auth_banknet_ref_num") )
    {
      msg.setDataElement(63, encodeTransactionLifeCycleId());
      
    }
    
    
    // private data subelements encoded in data element 48 
    msg.setPrivateDataSubelement(23, StringUtilities.leftJustify(getData("cat_indicator"),3,' ')); // Terminal Type
    
    // private data subelement 43 is used for special programs (supermarket, gaming, etc.
    if( ! isBlank(getData("program_registration_id")) )
    {
      msg.setPrivateDataSubelement(43, getData("program_registration_id"));
    }
    
    if( ! isBlank(getData("ecomm_security_level_ind")) )
    {
      msg.setPrivateDataSubelement(52, getData("ecomm_security_level_ind"));
    }
    
    msg.setPrivateDataSubelement(148, getData("currency_code")+CurrencyDecimalPlaces); // currency code + decimal places
    
	String tic = getData("tic");
	if (!isBlank(tic)) {
		msg.setPrivateDataSubelement(17, tic);
	}

    if ( !isCashDisbursement() )
    {
      msg.setPrivateDataSubelement(158, encodeMCBusinessActivity() );
    }      
    msg.setPrivateDataSubelement(165, "M");
    
    if ( ! "Y".equals(getData("card_present")) )
    {
      // encode CardAcceptorInquiryInfo
      String  phones  = ( StringUtilities.leftJustify(getData("customer_service_phone"),16,' ')) +
                        ( StringUtilities.leftJustify(getData("phone_number"          ),16,' '));
      msg.setPrivateDataSubelement(170, phones );
    }
    
    if( ! isBlank(getData("legal_corp_name")) )
    {
      msg.setPrivateDataSubelement(173, getData("legal_corp_name") );
    }
    
    // private data subelement 175 is used for url
    if( ! isBlank(getData("dm_contact_info")) )
    {
      msg.setPrivateDataSubelement(175, getData("dm_contact_info") );
    }

    // private data subelement 176 is used for special programs (supermarket, warehouse, etc)
    if( ! isBlank(getData("mc_assigned_id")) )
    {
      msg.setPrivateDataSubelement(176, getData("mc_assigned_id"));
    }

    // set Cielo specific fields    
    if( ProcessorType == PROC_TYPE_CIELO )
    {
      if ( !isFieldBlank("exp_date") )
      {
        msg.setDataElement(14,StringUtilities.rightJustify(getData("exp_date"),4,'0'));
      }
    
      if ( !isFieldBlank("auth_retrieval_ref_num") )
      {
        msg.setDataElement(37, getData("auth_retrieval_ref_num"));// Retrieval Reference Number
      }
      
      if ( !isFieldBlank("pos_terminal_id") )
      {
        msg.setDataElement(41, getData("pos_terminal_id"));
      }
    }

    msg.setDataElement(49, getData("currency_code")); // currency code, transaction
    
    msg.setDataElement(94, getData("ica_number") );     // acquiring ICA value
    
    //subfields for DE 54
    if(getDouble("tran_fee_amount") != 0 )
    {
    	msg.setDataElement(54, encodeAdditionalAmounts());
    }
    
    // add message to array
    recs.add(msg);
    
    // ADDENDUM MESSAGES
    if( ProcessorType == PROC_TYPE_CIELO )
    {
      if ( getInt("multiple_clearing_seq_count") > 0 )
      {
        recs.add( buildInstallmentAddendum() );
      }
    }
    else    // US Addendum records
    {
      if( ("MB").equals(getData("card_type")) &&
          ( "00".equals(getData("processing_code")) || "18".equals(getData("processing_code")) || 
        		  (PROCESS_CODE_20.equals(getData("processing_code")) && IRDV5.equals(getData("ird")))))
      {
        // corporate card 
        recs.add( buildCommercialCardAddendum() );
      }
    
      if( getData("addendum_required").indexOf("C3") >= 0 && LineItems != null )
      {
        // corporate card level III
        int idx = 1;
        for( Iterator i = LineItems.iterator(); i.hasNext(); )
        {
          LineItem item = (LineItem)i.next();
        
          // idx should start at 2 for line items
          recs.add( buildLineItemDetail(item, ++idx) );
        }
      }
      else if ( "E".equals(getData( "level_iii_data_present" )) )
      {
        LineItem item = generateLineItem();
        recs.add( buildLineItemDetail(item,2) );
      }
    
      if( getData("addendum_required").indexOf("L1") >= 0 ||
          getData("addendum_required").indexOf("L2") >= 0 )
      {
        // lodging
        recs.add( buildLodgingAddendum() );
      }
    }
    
    return( recs );
  }

  private List buildSecondPresentmentRecords()
    throws Exception
  {
    List        recs            = new ArrayList(1);
    double      originalAmount  = getDouble("original_amount");
    double      tranAmount      = getDouble("transaction_amount");
    
    // create IPM message
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    // add 1240 (Second Presentment record)
    msg.setMTI("1240");
    msg.setDataElement(2, getCardNumberFull());           // Primary Account Number (PAN)
    msg.setDataElement(3, getData("processing_code")+     // Processing Code
                          "00" + "00" );                  // cardholder_from_type & cardholder_to_type
    msg.setDataElement(4, TridentTools.encodeAmount(tranAmount,12,CurrencyDecimalPlaces) );   // Amount, Transaction
    msg.setDataElement(12,                                // Date and Time, Local Transaction
      DateTimeFormatter.getFormattedDate( getDate("transaction_time",DATE_TIME_FORMAT), "yyMMddHHmmss" ) );  
    msg.setDataElement(22, getData("pos_data_code"));     // Point of Service Data Code
    
    if ( !isFieldBlank("function_code") )   // value provided by the cb system
    {
      msg.setDataElement(24, getData("function_code"));
    }
    else    // no value provided, attempt to derive proper function code
    {
      if ( tranAmount < originalAmount )
      {
        msg.setDataElement(24, "282");     // partial second presentment
      }
      else
      {
        msg.setDataElement(24, "205");     // full second presentment
      }      
    }      
    
    msg.setDataElement(25, getData("reason_code") );   // reason code for representment - see page 7-63
    msg.setDataElement(26, getData("sic_code") );      // Card Acceptor Business Code (MCC)
    msg.setDataElement(30, TridentTools.encodeAmount(originalAmount,12,CurrencyDecimalPlaces) + "000000000000" );    // Amounts, Original
    msg.setDataElement(31, getData("reference_number") );  // Acquirer Reference Data
    msg.setDataElement(33, ForwardingInstitutionId);
    if ( !isFieldBlank("auth_code") )
    {
      msg.setDataElement(38, getData("auth_code") );   // Approval Code
    }      
    msg.setDataElement(43, encodeNameLocation() );     // Card Acceptor Name/Location
    
    if( !isFieldBlank("auth_banknet_ref_num") )
    {
      msg.setDataElement(63, encodeTransactionLifeCycleId());
    }
    
    // private data subelements
    msg.setPrivateDataSubelement(23, StringUtilities.leftJustify(getData("cat_indicator"),3,' ')); // Terminal Type
    
    if( ! ("").equals(getData("ecomm_security_level_ind")) )
    {
      msg.setPrivateDataSubelement(52, getData("ecomm_security_level_ind") ); // Electronic Commerce Security Level Indicator
    }
    
    if( ! ("").equals(getData("tax_amount")) )
    {
      msg.setPrivateDataSubelement( 80, encodeMCTax("tax_amount") );
    }
    
    msg.setPrivateDataSubelement(148, getData("currency_code")+CurrencyDecimalPlaces); // currency code + decimal places
    msg.setPrivateDataSubelement(149, getData("currency_code")+"000" ); // currency code, original transaction amount
    msg.setPrivateDataSubelement(158, encodeMCBusinessActivity() );
    msg.setPrivateDataSubelement(165, "M" );
    
    msg.setPrivateDataSubelement(262, getData("documentation_ind") ); // Documentation Indicator
    
    msg.setDataElement(49, getData("currency_code") ); // currency code, transaction
    
    msg.setDataElement(94, getData("ica_number") );     // acquiring ICA value
    
    msg.setDataElement(95, getData("cb_ref_num") );

    recs.add(msg);
    
    return( recs );
  }
  
  private List buildFeeCollectionRecords()
    throws Exception
  {
    List        recs            = new ArrayList(1);
    boolean     retrFeeBilling  = "7614".equals(getData("reason_code"));
    
    // create IPM message
    IpmOfflineMessage msg = new IpmOfflineMessage();
    
    // add 1740 (Fee Collection/Retrievel Fee Billing record)
    msg.setMTI("1740");                                                   // MTI
    msg.setDataElement(2, getData("card_number_full"));                   // Primary Account Number (PAN)
    msg.setDataElement(3, getData("processing_code")+                     // Processing Code
                          "00" + "00" );                                  // cardholder_from_type & cardholder_to_type
    msg.setDataElement(24, getData("function_code"));                     // Function Code (700 = Fee Collection - Member-generated)
    msg.setDataElement(25, getData("reason_code") );                      // Message Reason Code
    
    String encodedAmount = TridentTools.encodeAmount( getDouble("transaction_amount"), 12, CurrencyDecimalPlaces );
    if ( retrFeeBilling )
    {
      msg.setDataElement(30, encodedAmount + "000000000000");             // Amounts, Original
      msg.setPrivateDataSubelement(149, getData("currency_code") + "000");// Currency Codes, Original Tran Amt + Original Reconciliation Amt
    }
    else
    {
      msg.setDataElement( 4, encodedAmount);                              // Amount, Transaction
      msg.setDataElement(49, getData("currency_code"));                   // Currency Code, Transaction
    }

    if ( !isFieldBlank("reference_number") )
    {
      msg.setDataElement(31, getData("reference_number"));                // Acquirer Reference Data
    }      
    msg.setDataElement(33, ForwardingInstitutionId);                      // Forwarding Institution Id

    // private data subelements encoded in data element 48 
    msg.setPrivateDataSubelement(137, StringUtilities.rightJustify(getData("batch_record_id"),20,'0') );    // Fee Collection Control Number
    msg.setPrivateDataSubelement(148, getData("currency_code") + CurrencyDecimalPlaces);      // currency code + decimal places
    msg.setPrivateDataSubelement(165, "M");                               // Settlement Indicator ("M" = normal clearing & net settlement)
    if ( retrFeeBilling )                                                 // 7614 = settlement of retrieval request fulfillments not processed through MasterCom
    {
      msg.setPrivateDataSubelement(230, "1");                             // Fulfillment Document Code ("1" = Hard copy original document)
      msg.setPrivateDataSubelement(264, getData("original_reason_code")); // Original Retrievel Reason for Request
    }      
    //msg.setDataElement(71, "");                                         // Message Number, set it buildFileTrailer(..)
    msg.setDataElement(72, getData("message_text"));                      // Data Record
    msg.setDataElement(73, "000000");                                     // Date, Action
    msg.setDataElement(93, getData("dest_ica_number") );                  // destination member id
    msg.setDataElement(94, getData("ica_number") );                       // acquiring ICA value

    recs.add(msg);
    
    return( recs );
  }
  
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    List            recs      = null;
    
    // format fields correctly
    if( getInt("bank_number") == mesConstants.BANK_ID_CIELO )
    {
      ForwardingInstitutionId   = MC_PROC_ID_CIELO;
      ProcessorType             = PROC_TYPE_CIELO;
    }
    else
    {
      ForwardingInstitutionId   = MC_PROC_ID_MES;
      ProcessorType             = PROC_TYPE_MES;
    }      
    CurrencyDecimalPlaces     = CurrencyCodeConverter.getFractionDigits(getString("currency_code","840"));
    setData( "ica_number"             , StringUtilities.rightJustify(getData("ica_number"),6,'0') );

    switch(messageType)
    {
      case MT_FIRST_PRESENTMENT:
        recs = buildFirstPresentmentRecords();
        break;
        
      case MT_SECOND_PRESENTMENT:
        recs = buildSecondPresentmentRecords();
        break;
        
      case MT_FEE_COLLECTION:
        recs = buildFeeCollectionRecords();
        break;
    }
    
    return( recs );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("mcData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
    fgroup.add( new Field         ( "ica_number"                , "ICA"                     , 6   , 8 , false ) );
    fgroup.add( new Field         ( "mc_assigned_id"            , "MC Assigned Id"          , 6   , 8 , false ) );
    fgroup.add( new Field         ( "program_registration_id"   , "Program Reg Id"          , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "card_acceptor_type"        , "Card Acceptor Type"      , 8   ,10 , true  ) );
    fgroup.add( new Field         ( "product_class"             , "Product Class"           , 3   , 5 , false ) );
    fgroup.add( new Field         ( "card_program_id"           , "Card Program ID"         , 3   , 5 , false ) );
    fgroup.add( new Field         ( "gcms_product_id"           , "GCMS Product ID"         , 3   , 5 , false ) );
    fgroup.add( new Field         ( "customer_code"             , "Customer Code"           ,25   ,25 , true  ) );
   
    fgroup.add( new Field         ( "auth_banknet_ref_num"      , "BankNet Ref Num"         , 9   ,11 , true  ) );
    fgroup.add( new Field         ( "auth_banknet_date"         , "BankNet Date"            , 4   , 6 , true  ) );
    fgroup.add( new Field         ( "processing_code"           , "Processing Code"         , 2   , 4 , false ) );
    fgroup.add( new Field         ( "bsa_type"                  , "BSA Type"                , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "bsa_id_code"               , "BSA ID Code"             , 6   , 8 , true  ) );
    fgroup.add( new Field         ( "ird"                       , "IRD"                     , 4   , 6 , true  ) );
    fgroup.add( new Field         ( "addendum_required"         , "Addendum Indicator"      ,16   ,18 , true  ) );
    fgroup.add( new Field         ( "cat_indicator"             , "CAT Indicator"           , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "ecomm_security_level_ind"  , "eCommerce Security Level", 3   , 5 , true  ) );
    fgroup.add( new Field         ( "service_code"              , "Service Code"            , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "merchant_reference_number" , "Merchant Ref Num"        ,17   ,19 , true  ) );
    fgroup.add( new Field         ( "mapping_service_ind"       , "Mapping Service Ind"     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "exp_date"                  , "Card Exp Date"           , 4   , 6 , true  ) );
    fgroup.add( new DateField     ( "fx_conv_date"              , "FX Conversion Date"      ,19   ,21 , true  ) );
    
    // hotel fields
    fgroup.add( new Field         ( "customer_service_phone"    , "Customer Service Phone"  ,17   ,19 , true  ) );
    fgroup.add( new Field         ( "hotel_fire_safety_act_ind" , "Hotel Fire Safety Act Ind",1   , 3 , true  ) );
    
    // auto rental fields
    fgroup.add( new Field         ( "rental_return_city"        , "Rental Return City"      ,25   ,25 , true  ) );
    fgroup.add( new Field         ( "rental_return_state"       , "Rental Return State"     , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "rental_return_country"     , "Rental Return Country"   , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "rental_return_location_id" , "Rental Return Location Id",10  ,10 , true  ) );
    fgroup.add( new Field         ( "rate_daily_weekly_ind"     , "Rental Rate Indicator"   , 1   , 1 , true  ) );
    fgroup.add( new Field         ( "rental_location_city"      , "Rental Location City"    ,25   ,25 , true  ) );
    fgroup.add( new Field         ( "rental_location_state"     , "Rental Location State"   , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "rental_location_country"   , "Rental Location Country" , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "rental_location_id"        , "Rental Location Id"      ,10   ,10 , true  ) );
    fgroup.add( new Field         ( "rental_class_id"           , "Rental Class Id"         , 4   , 4 , true  ) );
    fgroup.add( new Field         ( "tic"                       , "tic"                     , 2   , 2 , true  ) );
    fgroup.add( new Field         ( "mc_pos_entry_mode"         , "mc_pos_entry_mode"       , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "mc_pos_data"               , "mc_pos_data"             ,26   ,26 , true  ) );
    
    // new fields to support MC Chargebacks
    fgroup.add( new HiddenField   ( "function_code"             ) );
    
    // fields to support MC Fee Collection
    fgroup.add( new HiddenField   ( "dest_ica_number"           ) );
    fgroup.add( new HiddenField   ( "original_reason_code"      ) );
    fgroup.add( new HiddenField   ( "message_text"              ) );

    // fields for ICC
    fgroup.add( new Field         ( "icc_data"         , "EMV data (icc)"       , 510   , 510 , true  ) );
    fgroup.add( new HiddenField   ( "card_sequence_number"      ) );
    
    // set field default values
    String cardAcceptorType = 
        "3" +   // s01 - Business Type - 3 (Individual/Sole Proprietorship)
        "0" +   // s02 - Business Owner Type - 0 (Unknown)
        "0" +   // s03 - Business Certification Type - 0 (Unknown)
        "0" +   // s04 - Business Racial/Ethnic Type - 0 (Unknown)
        "Y" +   // s05 - Business Type Provided Code - Y (Provided)
        "N" +   // s06 - Business Owner Type Provided Code - N (Not provided)
        "N" +   // s07 - Business Certification Type Provided Code - N (Not provided)
        "N" ;   // s08 - Business Racial/Ethnic Type Provided Code - N (Not provided)
    fields.getField("card_acceptor_type").setDefaultValue( cardAcceptorType );
    setData( "card_acceptor_type", cardAcceptorType );
    setData( "cash_disbursement","N" );
  }    
  
  public String getCardTypeEnhanced()
  {
    return( getString("product_class") );   // mc uses product class as cte
  }
  
  public String getPlanType()
  {
    return( isCashDisbursement() ? "M$" : "MC" );
  }
  @Override
  public void setFieldsBatchData(VisakBatchData batchData)
    throws java.sql.SQLException
  {
    if( batchData != null )
    {
      super.setFieldsBatchData(batchData);

      setData( "bin_number"               , batchData.getBinNumberMC());
      setData( "eligibility_flags"        , batchData.getEligibilityFlagsMC() );
      setData( "ica_number"               , batchData.getIcaNumber());
      setData( "mc_assigned_id"           , batchData.getMcAssignedId() );
      setData( "program_registration_id"  , batchData.getMcProgramRegId() );
      setData( "fx_conv_date"             , DateTimeFormatter.getFormattedDate(batchData.getBatchDate(),DATE_TIME_FORMAT));
    }
  }
  
  @Override
  protected void setAuthDataToEmpty() {
	  super.setAuthDataToEmpty();
	  setData("fx_conv_date", getData("batch_date"));
  }

	public void setFields_MC() throws java.sql.SQLException {
		String cardNumber = getData("card_number_full");
		// tem1-2303 : extra logging to identify bad card numbers
		if (null == cardNumber) {
			log.error("setFields_MC() receiving empty/null card number " + " for Batch id:" + getData("batch_id") + " for Batch record id:"
					+ getData("batch_record_id"));
		}
		else if (!TridentTools.mod10Check(getData("card_number_full"))) {
			log.error("setFields_MC() receiving invalid card number for " + (cardNumber.length() >= 6 ? cardNumber.substring(0, 6) : cardNumber) + ":"
					+ Util13.hexStringN(cardNumber.getBytes()) + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
		}

		ArdefDataMC ardef = SettlementDb.loadArdefMC(cardNumber);

		// set fields that depend on the ARDEF data
		setArdefData(ardef);

		// cannot call until after batch_id and batch_record_id are set
		setAuthData();

		// use the auth_tran_id to calculate the auth_banknet_ref_num and auth_banknet_date
		String tranId = TridentTools.decodeBankNetInfo(getData("auth_tran_id"));
		String bankNetRefNum = (tranId == null) ? "" : tranId.substring(4);
		String bankNetDate = (tranId == null) ? "" : tranId.substring(0, 4);

		String almCode = "Z"; // no alm
		String cardProgId = ardef.getCardProgramId();
		String gcmsProdId = ardef.getGCMSProductId();
		String prodClass = ardef.getProductClass();

		// determine the auth date using the MMDD from the transaction id and the current calendar year.
		Calendar cal = Calendar.getInstance();
		java.util.Date javaDate = DateTimeFormatter.parseDate((bankNetDate + cal.get(Calendar.YEAR)), "MMddyyyy");
		if (javaDate != null) {
			// if the date if more than 30 days in the future,
			// assume we crossed over the year end and decrement the year.
			cal.add(Calendar.DAY_OF_MONTH, 30);
			if (javaDate.after(cal.getTime())) {
				cal.setTime(javaDate);
				cal.add(Calendar.YEAR, -1);
				javaDate = cal.getTime();
			}

			// if the auth_date is blank, set it to the calculated auth date
			if (isFieldBlank("auth_date")) {
				setData("auth_date", DateTimeFormatter.getFormattedDate(javaDate, "MM/dd/yyyy 00:00:00"));
			}

			// if the account range was doing alm when the auth was received, almCode = 6th char of auth_code
			if ("Y".equals(ardef.getAccountLevelInd()) && !javaDate.before(ardef.getAccountLevelActivationDate())) {
				String authCode = getData("auth_code");

				if (authCode.length() == 6 && Character.isLetter(authCode.charAt(5))) {
					almCode = String.valueOf(authCode.charAt(5));

					if (!"Z".equals(almCode)) {
						// first three chars of the banknetRefNum are the licensed product id
						// (we know it is not blank because we wouldn't be here if the banknetDate were blank)
						AlmGraduationData almData = SettlementDb.loadAlmGraduationData(almCode, bankNetRefNum.substring(0, 3));
						if (almData != null) {
							gcmsProdId = almData.getGCMSProductId();
							prodClass = almData.getProductClass();
						}
					}
				}
			}
		}
		setData("alm_code", almCode);
		setData("auth_banknet_ref_num", bankNetRefNum);
		setData("auth_banknet_date", bankNetDate);
		setData("card_program_id", cardProgId);
		setData("gcms_product_id", gcmsProdId);
		setData("mapping_service_ind", ardef.getMappingServiceInd());
		setData("product_class", prodClass);

		// establish the business service arrangement
		String bsa = SettlementDb.getBusinessServiceArrangement(cardNumber, cardProgId, getData("bin_number"));
		if (bsa != null && bsa.length() == 7) {
			setData("bsa_type", bsa.substring(0, 1));
			setData("bsa_id_code", bsa.substring(1));
		}

		// establish the processing code
		String pc = "";
		String pc18SicList = "4829,6050,6051,6529,6530,6534,6538,7511,7995";
		String pc28SicList = "6532,6533,6536,6537";

		if ("C".equals(getData("debit_credit_indicator"))) {
			pc = "20";
		} // return
		else if (isCashDisbursement()) {
			pc = "12";
		} // cash disbursement
		else if (getDouble("cashback_amount") != 0.0) {
			pc = "09";
		} // purchase w/cash-back
		else if (pc18SicList.indexOf(getData("sic_code")) >= 0) {
			pc = "18";
		} // purchase w/sic code = "unique"
		else if (pc28SicList.indexOf(getData("sic_code")) >= 0) {
			pc = "28";
		} // purchase w/sic code = "payment transaction"
		else {
			pc = "00";
		} // purchase

		setData("processing_code", pc);

		// dm_contact_info is supposed to be a website; try to make sure it is
		String dmContact = getData("dm_contact_info").trim().toUpperCase();
		if (!isBlank(dmContact)) {
			if (dmContact.indexOf(".") < 0)
				setData("dm_contact_info", "");
			else if (dmContact.length() > 13)
				setData("dm_contact_info", dmContact.substring(0, 13));
			else
				setData("dm_contact_info", dmContact);
		}
	}

	@Override
	public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType) throws java.sql.SQLException {
		RecBatchType = batchType;
		super.setFieldsBase(batchData, resultSet, batchType);
		setFields_MC();
	}

	@Override
	public void setFieldsVisak(VisakBatchData batchData, int recIdx) throws java.sql.SQLException {
		super.setFieldsVisak(batchData, recIdx);
		setFields_MC();
		setData("dba_name", batchData.getCardAcceptorName()); // passthru value of cardAcceptorname from Auth, even if null.
	}
	
	@Override
	public void setForeignExchangeFields() throws Exception {

		if (!isForeignExchange()) {
			super.setForeignExchangeFields();
			return;
		}

		if (log.isDebugEnabled()) {
			log.debug("[setForeignExchangeFields()] - setting FX conv date for MasterCard");
		}

		java.util.Date cutoverDate = getCutoverDate();
		java.util.Date authDate = getDate("auth_date", DATE_TIME_FORMAT);
		java.util.Date settlementDate = getDate("settlement_date", DATE_TIME_FORMAT);
		if (settlementDate == null) {
			settlementDate = getDate("batch_date", DATE_TIME_FORMAT) == null ? getDate("batch_date", DATE_TIME_SHORT) : getDate("batch_date", DATE_TIME_FORMAT);
		}
		Integer numberOfDays = getMesDefaultsSetting(MesDefaults.MC_FX_AUTH_NUMBER_DAYS);

		if (cutoverDate == null || (cutoverDate != null && numberOfDays == 0)) {
			if (!fxSettingsErrorLogged) {
				fxSettingsErrorLogged = true;
				log.error("[setForeignExchangeFields()] - Configuration error," + MesDefaults.MC_ENHANCED_FX_ACTIVE_TIME + " not set or "
						+ MesDefaults.MC_FX_AUTH_NUMBER_DAYS + " not set");
			}
			setData("fx_conv_date", DateTimeFormatter.getFormattedDate(settlementDate, DATE_TIME_FORMAT)); // explicitly set default fx_conv_date
			return; // exit the method with no action, cutover date not set or auth date before cutover date
		}
		// authorization occures before cutover
		if (authDate.getTime() < cutoverDate.getTime()) {
			setData("fx_conv_date", DateTimeFormatter.getFormattedDate(settlementDate, DATE_TIME_FORMAT)); // explicitly set default fx_conv_date
			return;
		}

		if (isAuthPreferredForeignExchange(authDate, settlementDate, numberOfDays)) {
			setData("fx_conv_date", DateTimeFormatter.getFormattedDate(authDate, DATE_TIME_FORMAT));
		}
		else {
			setData("fx_conv_date", DateTimeFormatter.getFormattedDate(settlementDate, DATE_TIME_FORMAT));
		}
	}

	protected Boolean isAuthPreferredForeignExchange(java.util.Date authDate, java.util.Date batchDate, Integer numberOfDays) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("[isForeignExchangePreffered()] - authDate:" + authDate + ", batchDate=" + batchDate + ": numberOfDays:" + numberOfDays);
		}
		// if not authorized, not linked to auth record
		if (!isAuthLinked() || "C".equals(getData("debit_credit_indicator"))) {
			return Boolean.FALSE;
		}
		Calendar authCal = Calendar.getInstance();
		authCal.setTime(authDate);
		authCal.add(Calendar.DAY_OF_MONTH, numberOfDays); // increment by specified number of days

		Boolean preferred = Boolean.FALSE;
		batchDate = getInitialBatchDate(batchDate);

		// if auth + days < batch date always return false
		if (authCal.getTimeInMillis() < batchDate.getTime()) {
			preferred = Boolean.FALSE;
		}
		// if auth+days is sunday, substract 1
		if (authCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			authCal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// if auth date + days is more than batch date, return true
		if (authCal.getTimeInMillis() > batchDate.getTime()) {
			preferred = Boolean.TRUE;
		}
		return preferred;
	}

	protected java.util.Date getCutoverDate() {
		try {
			String cutoverDate = MesDefaults.getString(MesDefaults.MC_ENHANCED_FX_ACTIVE_TIME);
			if (cutoverDate != null) {
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return fmt.parse(cutoverDate);
			}
		}
		catch (Exception e) {
			log.error("[getCutoverDate()] - Exception loading / parseing cutover date",e);
		}
		return null;
	}
	protected MesQueryHandlerResultSet getQueryHandler() {
		return new MesQueryHandlerResultSet();
	}
	
	protected java.util.Date getInitialBatchDate(java.util.Date currentBatchDate) {
		if (log.isDebugEnabled()) {
			log.debug("[getInitialBatchDate()] - determine the batch date of the first settlement record");
		}
		Long merchantNumber = getLong("merchant_number");
		Integer bankNumber = getInt("bank_number");
		Long authRecId = getLong("auth_rec_id");
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentBatchDate);
		cal.add(Calendar.DAY_OF_MONTH, -45);
		try {
			MesQueryHandlerResultSet queryHandler = getQueryHandler();
			Object[] params = new Object[5];
			params[0] = cal;
			params[1] = merchantNumber;
			params[2] = bankNumber;
			params[3] = authRecId;
			params[4] = cal;

			//@formatter:off
			String sqlString = "select "
							 + "  reference_number, auth_rec_id, bank_number, transaction_date, batch_date, auth_date, "
							 + "  fx_conv_date, transaction_amount, auth_amt "
							 + "from daily_detail_file_dt d1 "
							 + "where "
							 + "  batch_date >= ? and merchant_account_number=?"
							 + "  and bank_number=? and auth_rec_id=?"
							 + "  and exists (select min(reference_number) as ref_nbr"
							 + "              from daily_detail_file_dt d2"
							 + "              where "
							 + "                 d2.batch_date>=? "
							 + "                 and d2.merchant_account_number=d1.merchant_account_number"
							 + "                 and d2.auth_rec_id=d1.auth_rec_id"
							 + "              having min(d2.reference_number) = d1.reference_number)";
			//@formatter:on
			ResultSet resultSet =
					queryHandler.executePreparedStatement("com.mes.settlement.SettlementRecordMC", sqlString, params);
			if (resultSet.next()) {
				return resultSet.getDate("batch_date");
			}
		}
		catch (SQLException e) {
			log.error("[getInitialBatchDate()] - unexepected error getting first settlement record", e);
		}
		return currentBatchDate;
	}
  
  private String encodeMCTax( String taxField )
  {
    StringBuffer  retVal = new StringBuffer("");
    
    retVal.append("010");  // VAT or sales tax code
    retVal.append(TridentTools.encodeAmount(getDouble(taxField), 12, CurrencyDecimalPlaces));
    retVal.append(getData("currency_code"));
    retVal.append(CurrencyDecimalPlaces); // currency code exponent (decimal places)
    retVal.append("D");  // assuming tax amount is always going to be a debit
    
    return( retVal.toString() );
  }
  
  private String encodeMCBusinessActivity( ) throws Exception
  {
    StringBuffer  retVal  = new StringBuffer("");
    
    // need to stop PRI/etc merchants with USA acq id from sending a bsa that won't match m/c's bsa
    if( "USA".equals(getData("country_code")) )
    {
      retVal.append( StringUtilities.leftJustify(getData("card_program_id"),3,' ') ); // s01
      retVal.append( StringUtilities.leftJustify(getData("bsa_type"),1,' ') );        // s02
      retVal.append( StringUtilities.leftJustify(getData("bsa_id_code"),6,' ') );     // s03
    }
    else
    {
      retVal.append( "          " );                                                  // s01, s02, 203 -- 10 spaces
    }
    retVal.append( StringUtilities.leftJustify(getData("ird"),2,' ') );               // s04
    
    return( retVal.toString() );
  }
  
  private String encodeNameLocation( )
    throws Exception
  {
    StringBuffer  retVal  = new StringBuffer("");
    
    retVal.append( StringUtilities.getStringMax(22,getData("dba_name")) );
    retVal.append( "\\" );
    retVal.append( getData("dba_address") );
    retVal.append( "\\" );
    retVal.append( StringUtilities.getStringMax(13,getData("dba_city")) );
    retVal.append( "\\" );
    retVal.append( StringUtilities.leftJustify(getData("dba_zip"),10,' ') );
    retVal.append( StringUtilities.leftJustify(getData("dba_state"),3,' ') );
    retVal.append( StringUtilities.leftJustify(getData("country_code"),3,' ') );
    
    return( retVal.toString() );
  }
  
  private String encodeAdditionalAmounts( )
  	throws Exception
  {
	  StringBuffer retVal = new StringBuffer("");
	  retVal.append("00");
	  //Surcharge Amount code 
	  retVal.append("42");
	  //Currency Code
	  retVal.append(getData("currency_code"));
	  retVal.append((getDouble("tran_fee_amount") > 0)?"D":"C");
	  retVal.append(TridentTools.encodeAmount(getDouble("tran_fee_amount"),12));
	  
	  return( retVal.toString() );
  }
  
  private String encodeTransactionDescription( int usageCode, int industryRecNum, int occurrence )
    throws Exception
  {
    StringBuffer retVal = new StringBuffer("");
    
    // subfield 3: Usage Code
    retVal.append( StringUtilities.rightJustify(String.valueOf(usageCode),2,'0') );
    
    // subfield 2: Industry Record Number
    retVal.append( StringUtilities.rightJustify(String.valueOf(industryRecNum),3,'0') );
    
    // subfield 3: Occurrence Indicator
    retVal.append(StringUtilities.rightJustify(Integer.toString(occurrence),3,'0') );
    
    // subfield 4: Associated First Presentment Number
    // must match DE 71 of associated First Presenment Message.  DE 71 is
    // set after all these messages are built -- will this just magically fill in?
    retVal.append("        ");
    
    return( retVal.toString() );
  }
  
  protected String encodeTransactionLifeCycleId( )
  {
    StringBuffer retVal = new StringBuffer("");
    
    retVal.append(" ");   // s01 - Life Cycle Support (always space)
    retVal.append( StringUtilities.leftJustify(getData("auth_banknet_ref_num"),9,' ') );
    retVal.append( StringUtilities.leftJustify(getData("auth_banknet_date"),4,' ') );
    retVal.append("  ");  // 2 spaces reserved
    
    return( retVal.toString() );
  }
  
  private String encodeTaxId( )
    throws Exception
  {
    String        merchantTaxId   = getData("merchant_tax_id");
    StringBuffer  retVal          = new StringBuffer("");
    
    // subfield 1 = 20 bytes; settlementDb has "decode(mf.federal_tax_id,0,null,lpad(mf.federal_tax_id,9,'0'))" to enforce USA rule
    //   for USA:     9 digit tax id with 11 trailing spaces
    //   for Brazil:  ? digit tax id with  ? trailing spaces
    retVal.append( StringUtilities.leftJustify(merchantTaxId,20,' ') );
    
    // subfield 2 = card acceptor tax id provided code
    retVal.append( isBlank(merchantTaxId) ? "R" : "Y" );
    
    return( retVal.toString() );
  }
  
  private String encodeTaxAmount( )
    throws Exception
  {
    StringBuffer  retVal    = new StringBuffer("");
    
    // subfield 1 = 12 digit tax amount
    retVal.append(TridentTools.encodeAmount(getDouble("tax_amount"), 12, CurrencyDecimalPlaces) );
    
    // subfield 2 = Tax exponent
    retVal.append(CurrencyDecimalPlaces);
    
    // subfield 3 = Tax sign (C or D)
    retVal.append(getData("debit_credit_indicator"));
    
    return( retVal.toString() );
  }
  
  private String encodeItemDiscount( LineItem item )
  {
    StringBuffer retVal = new StringBuffer("");
    
    if(item.getDouble("item_discount_amount") > 0.0)
    {
      retVal.append("Y"); // item is discounted
    }
    else
    {
      retVal.append("N"); // item is not discounted
    }
    
    // discount amount
    retVal.append(TridentTools.encodeAmount(item.getDouble("item_discount_amount"), 12, CurrencyDecimalPlaces));
    
    // calculate discount rate from discount amount and extended item amount
    double  discountRate = 0.0;
    if( item.getDouble("extended_item_amount") > 0.0 )
    {
      discountRate = item.getDouble("item_discount_amount") / item.getDouble("extended_item_amount");
      
      retVal.append(TridentTools.encodeAmount(discountRate,5));
    }
    else
    {
      // zero discount rate
      retVal.append("00000");
    }
    
    // discount rate exponent - 2 decimal places
    retVal.append("2");
    
    // discount debit/credit indicator - should be the same as the main amount of the line item i hope
    retVal.append(item.getData("debit_credit_indicator"));
    
    return( retVal.toString() );
  }
  
  // PDS 682: Detail Tax Amount 1
  private String encodeItemTaxData( LineItem item )
  {
    StringBuffer retVal = new StringBuffer("");
    
    // subfield 1: Tax Amount Indicator
    if( item.getDouble("tax_amount") > 0.0 )
    {
      retVal.append("Y");
    }
    else
    {
      retVal.append("N");
      
    }
    
    // subfield 2: Detail Tax Amount 1
    retVal.append( TridentTools.encodeAmount(item.getDouble("tax_amount"),12,CurrencyDecimalPlaces) );
    
    // subfield 3: Detal Tax Rate 1
    retVal.append( TridentTools.encodeAmount(item.getDouble("tax_rate"),5) );
    
    // subfield 4: Detail Tax Rate Exponent 1
    retVal.append("4");   // 4 = 4 decimals to make 2.35% = 00235 = .0235
    
    // subfield 5: Detail Tax Type Applied 1
    retVal.append( StringUtilities.leftJustify(item.getData("tax_type_applied"), 4, ' ') );
    
    // subfield 6: Detail Tax Type Identifier 1
    retVal.append( "00" );  // 02 =  state sales tax.  Not sure whether "00"(unknown) will pass
    
    // subfield 7: Card Acceptor Tax ID 1
    String taxId = StringUtilities.rightJustify(item.getData("card_acceptor_tax_id"), 9, '0');
    retVal.append( StringUtilities.leftJustify(taxId, 20, ' ') );
    
    // subfield 8: Detail Tax Amount Sign 1
    retVal.append( item.getData("debit_credit_indicator") );
    
    return( retVal.toString() );
  }
  
  
  public void fixBadData()
  {
    super.fixBadData();

    setData("dba_name"      , getData("dba_name"    ).replace('\\',' ') );
    setData("dba_address"   , getData("dba_address" ).replace('\\',' ') );
    setData("dba_city"      , getData("dba_city"    ).replace('\\',' ') );
  }
  
  public void fixBadDataAfterIcAssignment()
  {
    super.fixBadDataAfterIcAssignment();
  }
  
  public LineItem generateLineItem()
  {
    LineItem    li    = null;
    
    li = new LineItem( SettlementRecord.SETTLE_REC_MC );
    li.setData("item_description",SicCodes.getInstance().getMerchandiseDesc(getData("sic_code")));
    li.setData("product_code","MISC");
    li.setData("item_quantity","1");
    li.setData("item_unit_measure","EA");
    li.setData("card_acceptor_tax_id",getData("merchant_tax_id"));
    li.setData("tax_rate","");
    li.setData("tax_type_applied","stat");
    li.setData("tax_amount",getData("tax_amount"));
    li.setData("extended_item_amount",getData("transaction_amount"));
    li.setData("debit_credit_indicator","C");
    li.setData("item_discount_amount","");
    li.setData("item_unit_price",getData("transaction_amount"));
    
    return(li);
  }
  
}
