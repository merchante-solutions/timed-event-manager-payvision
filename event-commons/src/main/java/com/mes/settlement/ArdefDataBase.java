/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/ArdefDataBase.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-08-12 16:46:29 -0700 (Mon, 12 Aug 2013) $
  Version            : $Revision: 21393 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

public abstract class ArdefDataBase
{
  protected boolean   Enabled                     = false;
  protected long      LoadFileId                  = 0L;
  protected String    LoadFilename                = null;

  public abstract boolean isInRange( String cardNumber );
  public abstract boolean isForeignIssuer( int recBatchType );
  
  public abstract String getCardTypeEnhanced();
  public abstract String getCardTypeFull();
  public abstract int    getDebitType();
  
  public boolean  getEnabled()                  { return( Enabled );        }
  public long     getLoadFileId()               { return( LoadFileId );     }
  public String   getLoadFilename()             { return( LoadFilename );   }
  
  public void setEnabled     ( boolean value )  { Enabled = value;          }
  public void setLoadFileId  ( long value )     { LoadFileId   = value;     }
  public void setLoadFilename( String value )   { LoadFilename = value;     }
}
