/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordReport.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-12-12 15:59:43 -0800 (Thu, 12 Dec 2013) $
  Version            : $Revision: 21951 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;

public class SettlementRecordReport extends SettlementRecord
{
  public SettlementRecordReport()
  {
    super(mesConstants.MBS_BT_VISAK, "RP", SettlementRecord.SETTLE_REC_REPORT);
  }

  // report records do not output any network records
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    return( null );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("reportData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
    fgroup.add( new Field         ( "debit_network_id"          , "Debit Network Id"        , 2   , 4 , false ) );
    fgroup.add( new HiddenField   ( "funding_source"            ) );
  }    
  
  public String getCardTypeEnhanced()
  {
    return( "DB,EB".indexOf(getData("card_type")) >= 0 ? getString("debit_network_id") : super.getCardTypeEnhanced() );
  }
  
  public void setFields_Report()
    throws java.sql.SQLException
  {
    setData( "bin_number", StringUtilities.leftJustify(String.valueOf(getInt("bank_number")), 6, '0') );

    try
    {
      if( RecBatchType == mesConstants.MBS_BT_CIELO )
      {
        String cardType = getData("card_type");
        if( cardType != null && "DB".equals(cardType) )
        {
          // all trans are expected to be card_type = DB and debit_network_id = ME and (hopefully) in mc_ardef_table

          // set fields that depend on the ARDEF data
          String          fcn   = getData("card_number_full");
          ArdefDataMC     ardef = SettlementDb.loadArdefMC(fcn);
          if( ardef == null || !ardef.isInRange(fcn) )  setArdefData();
          else                                          setArdefData(ardef);

          setData( "card_type"          , "DB"                        );  // revert to previous card_type

          if( !"F".equals( getData("foreign_card_indicator")  ) ) setData( "region_issuer", "BR" );
          else if( "B".equals( ardef.getRegion()              ) ) setData( "region_issuer", "LC" );
          else                                                    setData( "region_issuer", "IR" );
        }
      }
      else
      {
        // cannot call until after batch_id and batch_record_id are set
        setAuthData();

        // cannot call until after auth_rec_id is set
        if( isFieldBlank("auth_rec_id") )
        {
          if( "DB,EB,PL".indexOf(getData("card_type")) >= 0 )
          {
            // the debit transactions often cannot auth link but is not merchant's fault;
            // set auth_rec_id = 0 (not null) so can distinguish from the signature "no auth link" trans
            setData( "auth_rec_id", 0 );
          }
        }

        String cardType = getData("card_type");
        if( "DB,EB".indexOf(cardType) >= 0 )
        {
          if( isFieldBlank("debit_network_id") )
          {
            if( RecBatchType == mesConstants.MBS_BT_PG )
              setData( "debit_network_id", "AC" );        // set PG trans to Acculynk generic
            else
              setData( "debit_network_id", cardType );    // default to generic DB or EB
          }
          else
          {
            switch( RecBatchType )
            {
              case mesConstants.MBS_BT_VISAK:
              case mesConstants.MBS_BT_PG:
                setData( "debit_network_id", SettlementDb.decodeDebitNetworkID(getData("debit_network_id")) );
                break;

//            case mesConstants.MBS_BT_VITAL:
//              break;
            }
          }

          // set fields that depend on the ARDEF data
          String  fcn = getData("card_number_full");
          ArdefDataBase                                           ardef = null;
          String  cType = TridentTools.decodeVisakCardType(fcn);

                if( mesConstants.VITAL_ME_AP_MC  .equals(cType) ) ardef = (ArdefDataBase)SettlementDb.loadArdefMC      (fcn);
          else  if( mesConstants.VITAL_ME_AP_DISC.equals(cType) ) ardef = (ArdefDataBase)SettlementDb.loadArdefDiscover(fcn);
          else  if( mesConstants.VITAL_ME_AP_JCB .equals(cType) ) ardef = (ArdefDataBase)SettlementDb.loadArdefDiscover(fcn);
//        else  if( mesConstants.VITAL_ME_AP_AMEX.equals(cType) )
//        else  if( mesConstants.VITAL_ME_AP_VISA.equals(cType) )

          if( ardef == null || !ardef.isInRange(fcn) )            ardef = (ArdefDataBase)SettlementDb.loadArdefVisa    (fcn);
          if( ardef == null || !ardef.isInRange(fcn) )            ardef = (ArdefDataBase)SettlementDb.loadArdefMC      (fcn);
          if( ardef == null || !ardef.isInRange(fcn) )            ardef = (ArdefDataBase)SettlementDb.loadArdefDiscover(fcn);
          if( ardef == null || !ardef.isInRange(fcn) )  setArdefData();
          else                                        { setArdefData(ardef);
                                                        if( ardef instanceof ArdefDataVisa )
                                                        {
                                                          setData( "product_id"     , ((ArdefDataVisa)ardef).getProductId()     );
                                                          setData( "funding_source" , ((ArdefDataVisa)ardef).getFundingSource() );
                                                        }
                                                      }

          setData( "card_type"          , cardType                    );  // revert to previous card_type

          if( !"F".equals( getData("foreign_card_indicator")  ) ) setData( "region_issuer", "US" );
          else                                                    setData( "region_issuer", "IR" );

          String    authFPI = getData("auth_fpi");
          if( authFPI.length() != 3 || "A0,A1,A2,AC,AF,AL,CU,MA,NE,NY,PU,ST,SH".indexOf(authFPI.substring(0,2)) < 0 ||
              ( "0".equals(getData("debit_type")) != ( "0".equals(authFPI.substring(1,2)) || "0".equals(authFPI.substring(2,3)) || "1".equals(authFPI.substring(1,2)) || "2".equals(authFPI.substring(1,2)))))
            authFPI = "---";            // default when we don't know if it is a special card
          setData( "eligibility_flags" , authFPI );
        }
      }
    }
    catch( Exception e )
    {
      logEntry("SettlementRecordReport(" + getData("batch_id") + "," + getData("batch_record_id") + ")", e.toString());
    }
  }
  
  public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType)
    throws java.sql.SQLException
  {
    RecBatchType = batchType;
    super.setFieldsBase(batchData, resultSet, batchType);
    setFields_Report();
  }
  
  public void setFieldsVisak(VisakBatchData batchData, int recIdx)
    throws java.sql.SQLException
  {
    super.setFieldsVisak(batchData, recIdx);

    // maintain consistency with previous field-setting by covering purchase_id with customer_code
    if( !isFieldBlank("customer_code") )  setData("purchase_id", getData("customer_code") );

    setFields_Report();
  }
}
