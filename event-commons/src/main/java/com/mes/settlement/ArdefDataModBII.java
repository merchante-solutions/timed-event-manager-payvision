/*************************************************************************

  FILE: $URL: http://10.1.61.151/svn/mesweb/branches/te/src/main/com/mes/settlement/ArdefDataModBII.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-01-23 15:10:34 -0800 (Thu, 23 Jan 2014) $
  Version            : $Revision: 22205 $

  Change History:
     See SVN database

  Copyright (C) 2000-2013,2014 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import com.mes.support.StringUtilities;

public class ArdefDataModBII
  extends ArdefDataBase
{
  protected String ArdefCountryCode             = null;
  protected int    CardAssociation              = 0;
  protected String CardType                     = null;
  protected String ClearingBankIdCredit         = null;
  protected String ClearingBankIdDebit          = null;
  protected int    DestIdCredit                 = 0;
  protected int    DestIdDebit                  = 0;
  protected String Endpoint                     = null;
  protected String EndpointEnabled              = null;
  protected String FundingSource                = null;
  protected String IssuerBankIdCredit           = null;
  protected String IssuerBankIdDebit            = null;
  protected String LastModifiedUser             = null;
  protected String MultiBankFlag                = null;
  protected long   RangeHigh                    = 0L;
  protected long   RangeLow                     = 0L;
  protected Date   ValidDateBegin               = null;
  protected Date   ValidDateEnd                 = null;
  
  public String getCardTypeEnhanced()
  {
    if( CardAssociation == 0 )
      return( null );

    return( StringUtilities.rightJustify(String.valueOf(CardAssociation),3,'0') );
  }
  
  public String getCardTypeFull()
  {
    return( "B2" );
  }
  
  public int getDebitType()
  {
    return(0);
  }
  
  public boolean isForeignIssuer( int recBatchType ) 
  { 
    return( !"076".equals(ArdefCountryCode) );    // always Brazil-centric answer
  }
  
  public boolean isInRange( String cardNumber )
  {
    boolean retVal = false;
    try
    {
      long cardBin = Long.parseLong( StringUtilities.leftJustify(cardNumber,19,'0') );
      if ( cardBin >= RangeLow && cardBin <= RangeHigh )
      {
        retVal = true;
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public String getArdefCountryCode()                     { return(ArdefCountryCode);     }
  public void   setArdefCountryCode( String value )       { ArdefCountryCode = value;     }
  
  public int    getCardAssociation()                      { return(CardAssociation);      }
  public void   setCardAssociation( int value )           { CardAssociation = value;      }
  
  public String getCardType()                             { return(CardType);             }
  public void   setCardType( String value )               { CardType = value;             }
  
  public String getClearingBankIdCredit()                 { return(ClearingBankIdCredit); }
  public void   setClearingBankIdCredit( String value )   { ClearingBankIdCredit = value; }
  
  public String getClearingBankIdDebit()                  { return(ClearingBankIdDebit);  }
  public void   setClearingBankIdDebit( String value )    { ClearingBankIdDebit = value;  }
  
  public int    getDestIdCredit()                         { return(DestIdCredit);         }
  public void   setDestIdCredit( int value )              { DestIdCredit = value;         }
  
  public int    getDestIdDebit()                          { return(DestIdDebit);          }
  public void   setDestIdDebit( int value )               { DestIdDebit = value;          }
  
  public String getEndpoint()                             { return(Endpoint);             }
  public void   setEndpoint( String value )               { Endpoint = value;             }
  
  public String getEndpointEnabled()                      { return(EndpointEnabled);      }
  public void   setEndpointEnabled( String value )        { EndpointEnabled = value;      }
  
  public String getFundingSource()                        { return(FundingSource);        }
  public void   setFundingSource( String value )          { FundingSource = value;        }
  
  public String getIssuerBankIdCredit()                   { return(IssuerBankIdCredit);   }
  public void   setIssuerBankIdCredit( String value )     { IssuerBankIdCredit = value;   }
  
  public String getIssuerBankIdDebit()                    { return(IssuerBankIdDebit);    }
  public void   setIssuerBankIdDebit( String value )      { IssuerBankIdDebit = value;    }
  
  public String getLastModifiedUser()                     { return(LastModifiedUser);     }
  public void   setLastModifiedUser( String value )       { LastModifiedUser = value;     }
  
  public String getMultiBankFlag()                        { return(MultiBankFlag);        }
  public void   setMultiBankFlag( String value )          { MultiBankFlag = value;        }
  
  public long   getRangeHigh()                            { return(RangeHigh);            }
  public void   setRangeHigh( long value )                { RangeHigh = value;            }
  
  public long   getRangeLow()                             { return(RangeLow);             }
  public void   setRangeLow( long value )                 { RangeLow = value;             }
  
  public Date   getValidDateBegin()                       { return(ValidDateBegin);       }
  public void   setValidDateBegin( Date value )           { ValidDateBegin = value;       }
  
  public Date   getValidDateEnd()                         { return(ValidDateEnd);         }
  public void   setValidDateEnd( Date value )             { ValidDateEnd = value;         }
  
  public void showData()
  {
    System.out.println("ArdefCountryCode      : " + ArdefCountryCode        );
    System.out.println("CardAssociation       : " + CardAssociation         );
    System.out.println("CardType              : " + CardType                );
    System.out.println("ClearingBankIdCredit  : " + ClearingBankIdCredit    );
    System.out.println("ClearingBankIdDebit   : " + ClearingBankIdDebit     );
    System.out.println("DestIdCredit          : " + DestIdCredit            );
    System.out.println("DestIdDebit           : " + DestIdDebit             );
    System.out.println("Endpoint              : " + Endpoint                );
    System.out.println("EndpointEnabled       : " + EndpointEnabled         );
    System.out.println("FundingSource         : " + FundingSource           );
    System.out.println("IssuerBankIdCredit    : " + IssuerBankIdCredit      );
    System.out.println("IssuerBankIdDebit     : " + IssuerBankIdDebit       );
    System.out.println("LastModifiedUser      : " + LastModifiedUser        );
    System.out.println("MultiBankFlag         : " + MultiBankFlag           );
    System.out.println("RangeHigh             : " + RangeHigh               );
    System.out.println("RangeLow              : " + RangeLow                );
    System.out.println("ValidDateBegin        : " + ValidDateBegin          );
    System.out.println("ValidDateEnd          : " + ValidDateEnd            );
  }                                                       
}
