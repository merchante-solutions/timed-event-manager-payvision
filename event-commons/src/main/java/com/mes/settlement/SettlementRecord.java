/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecord.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-10-28 17:49:48 -0700 (Wed, 28 Oct 2015) $
  Version            : $Revision: 23921 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenDateField;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.SicField;
import com.mes.forms.ZipField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesEncryption;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.CountryCodeConverter;
import dto.LevelDto;
import masthead.formats.visak.DetailRecord;
import masthead.formats.visak.MastercardLineItemDetailRecord;
import masthead.formats.visak.VisaLineItemDetailRecord;
import masthead.util.ByteUtil;
import masthead.util.Util13;

public abstract class SettlementRecord extends FieldBean
{
  protected   int           DbaZipMaxLength               = 5;
  protected   HashMap       FieldBeanToFlatFileNameMap    = null;
  protected   HashMap       FlatFileRecordMap             = null;
  protected   List          LineItems                     = null;
  protected   boolean       MerchantIsProcessedByMeS      = true;       // false for discover/amex non-aggregator
  protected   int           ProcessorType                 = PROC_TYPE_DEFAULT;
  
  protected   int           RecBatchType                  = mesConstants.MBS_BT_UNKNOWN;
  protected   String        RecCardType                   = null;       // two-char for extended class (eg VS, MC, RP)
  protected   int           RecSettleRecType              = -1;         // SettlementRecord.SETTLE_REC_???
  protected   String        ioVersion					  = null;
  
	enum DbBooleanType {
		TRUE("Y", Boolean.TRUE), FALSE("N", Boolean.FALSE);
		final String strValue;
		final Boolean value;

		private DbBooleanType(String strValue, Boolean value) {
			this.strValue = strValue;
			this.value = value;
		}
	}
  
  protected static final String DISC_PARTIAL_AUTH_DISABLED = "0";
  protected static final String DISC_PARTIAL_AUTH_ENABLED = "2";
  protected static final String DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
  protected static final String DATE_TIME_SHORT = "MM/dd/yyyy";
  
  // Processor Types
  public static final int       PROC_TYPE_MES       = 1;
  public static final int       PROC_TYPE_CIELO     = 2;
  public static final int       PROC_TYPE_DEFAULT   = PROC_TYPE_MES;

  // Settlement Record types
  public static final int   SETTLE_REC_REPORT     = 0;
  public static final int   SETTLE_REC_VISA       = 1;
  public static final int   SETTLE_REC_MC         = 2;
  public static final int   SETTLE_REC_AMEX       = 3;
  public static final int   SETTLE_REC_DISC       = 4;
  public static final int   SETTLE_REC_MODBII     = 5;

  // enhanced data
  public static final int     POS_ENHANCED_IND_PID  = 0;        // position of "." or "1"/"3"/"4"
  public static final int     POS_ENHANCED_IND_TAX  = 1;        // position of "." or "T"
  public static final int     POS_ENHANCED_IND_L3   = 2;        // position of "." or "L"
  public static final int     NUM_ENHANCED_IND      = 3;        // number of enhancements
  public static final String  DEF_ENHANCED_IND      = "...";    // NUM_ENHANCED_IND periods

  // message types
  public static final int   MT_FIRST_PRESENTMENT  = 1;
  public static final int   MT_SECOND_PRESENTMENT = 2;
  public static final int   MT_FEE_COLLECTION     = 3;
  public static final int   MT_VISA_MPS           = 4;

  // format types
  public static final int   FT_ID_METHOD        = 1;
  public static final int   FT_DATA_SOURCE      = 2;
  public static final int   FT_INPUT_CAP        = 3;
  public static final int   FT_OP_ENV           = 4;
  public static final int   FT_MOTO_IND         = 5;
  public static final int   FT_CARD_PRESENT     = 6;
  public static final int   FT_INP_MODE         = 7;
  public static final int   FT_AUTH_METHOD      = 8;
  public static final int   FT_AUTH_ENTITY      = 9;
  public static final int   FT_DEBIT_NET_IND    = 10;
  
  static Logger log = Logger.getLogger(SettlementRecord.class);

  public SettlementRecord(int batchType, String card, int settleRec)
  {
    RecBatchType                  = batchType;
    RecCardType                   = card;
    RecSettleRecType              = settleRec;
    
    createFields(null);
  }
  
  Integer getMesDefaultsSetting(String name) throws Exception {
  	return MesDefaults.getInt(name);
  }
  
  public void addFieldNameMapEntry( String fieldName, String flatFileName )
  {
    if ( FieldBeanToFlatFileNameMap == null )
    {
      FieldBeanToFlatFileNameMap = new HashMap();
    }
    FieldBeanToFlatFileNameMap.put(fieldName,flatFileName);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("settlementCommon");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
      // fields for xxx_settlement table data
    fgroup.add( new HiddenField   ( "rec_id"                    ) );
    fgroup.add( new HiddenField   ( "last_modified_date"        ) );
    fgroup.add( new HiddenField   ( "last_modified_user"        ) );
    fgroup.add( new HiddenField   ( "load_file_id"              ) );
    fgroup.add( new HiddenField   ( "load_filename"             ) );
    fgroup.add( new HiddenField   ( "output_file_id"            ) );
    fgroup.add( new HiddenField   ( "output_filename"           ) );
    fgroup.add( new HiddenField   ( "test_flag"                 ) );

      // fields for merchant data
    fgroup.add( new Field         ( "bank_number"               , "Bank Number"             , 4   , 6 , false ) );
    fgroup.add( new Field         ( "bin_number"                , "BIN"                     , 6   , 8 , false ) );
    fgroup.add( new NumberField   ( "merchant_number"           , "Merchant ID"             ,16   ,18 , false , 0 ) );
    fgroup.add( new HiddenField   ( "pos_terminal_id"           ) );
    fgroup.add( new NumberField   ( "terminal_number"           , "Terminal #"              , 4   , 6 , true  , 0 ) );
    fgroup.add( new Field         ( "merchant_tax_id"           , "Merchant Tax ID"         ,20   ,22 , true  ) );
    fgroup.add( new Field         ( "legal_corp_name"           , "Legal Corporate Name"    ,30   ,32 , true  ) );
    fgroup.add( new Field         ( "dba_name"                  , "DBA Name"                ,32   ,34 , false ) );
    fgroup.add( new Field         ( "dba_address"               , "Address"                 ,38   ,40 , false ) );
    fgroup.add( new Field         ( "dba_city"                  , "City"                    ,24   ,26 , false ) );
    fgroup.add( new Field         ( "dba_state"                 , "State"                   , 3   , 5 , false ) );
    fgroup.add( new ZipField      ( "dba_zip"                   , "Zip"                               , false, fgroup.getField("dba_state") ) );
    fgroup.add( new Field         ( "country_code"              , "Country Code"            , 3   , 5 , false ) );
    fgroup.add( new PhoneField    ( "phone_number"              , "Phone Number"                      , false ) );
    fgroup.add( new Field         ( "dm_contact_info"           , "DM Contact Info"         ,13   ,15 , true  ) );
    fgroup.add( new Field         ( "currency_code"             , "Currency Code"           , 3   , 5 , false ) );
    fgroup.add( new Field         ( "funding_currency_code"     , "Funding Currency Code"   , 3   , 5 , false ) );
    fgroup.add( new Field         ( "eligibility_flags"         , "Eligibility Flags"       ,64   ,24 , true  ) );
    fgroup.add( new SicField      ( "sic_code"                  , "SIC"                               , false ) );
    fgroup.add( new Field         ( "cash_disbursement"         , "Cash Disbursement"       , 1   , 3 , false ) );
    fgroup.add( new HiddenField   ( "account_status"            ) );
    fgroup.add( new HiddenField   ( "debit_flag"                ) );
    fgroup.add( new HiddenField   ( "ic_bet_number"             ) );
    fgroup.add( new HiddenField   ( "ic_enhancement"            ) );
    fgroup.add( new HiddenField   ( "ic_intervention"           ) );
    fgroup.add( new HiddenField   ( "sic_group"                 ) );

      // fields for batch data
    fgroup.add( new HiddenField   ( "batch_id"                  ) );
    fgroup.add( new HiddenField   ( "batch_record_id"           ) );
    fgroup.add( new HiddenField   ( "batch_number"              ) );
    fgroup.add( new HiddenDateField("batch_date"                ) );
    fgroup.add( new HiddenDateField("merchant_batch_date"       ) );
    fgroup.add( new HiddenDateField("settlement_date"           ) );

      // fields for transaction general data
    fgroup.add( new DateField     ( "transaction_date"          , "Tran Date"               ,           false ) );
    fgroup.add( new DateField     ( "transaction_time"          , "Tran Time"               ,19   ,21 , false ) );
    fgroup.add( new CurrencyField ( "tran_fee_amount"           , "Transaction Fee Amount"  ,12   ,14 , false ) );
    fgroup.add( new NumberField   ( "acq_reference_number"      , "Acq Reference Number"    ,11   ,13 , true  , 0 ) );
    fgroup.add( new Field         ( "reference_number"          , "Reference Number"        ,23   ,25 , true  ) );
    fgroup.add( new HiddenField   ( "account_data_source"       ) );
    fgroup.add( new HiddenField   ( "cardholder_id_code"        ) );
    fgroup.add( new Field         ( "card_present"              , "Card Present"            , 1   , 3 , true  ) );
    fgroup.add( new HiddenField   ( "device_code"               ) );
    fgroup.add( new Field         ( "pos_data_code"             , "POS Data Code"           ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "pos_entry_mode"            , "POS Entry Mode"          , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "cardholder_id_method"      , "Cardholder ID Method"    , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "moto_ecommerce_ind"        , "MOTO/eCommerce Ind"      , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "customer_code"             , "Customer Code"           ,25   ,27 , true  ) );
    fgroup.add( new Field         ( "purchase_id"               , "Purchase ID"             ,25   ,27 , true  ) );
    fgroup.add( new Field         ( "purchase_id_format"        , "Purchase ID Format"      , 1   , 3 , true  ) );
    fgroup.add( new HiddenField   ( "purchase_id_unenh"         ) );
    fgroup.add( new Field         ( "level_iii_data_present"    , "Level III Data"          , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "region_merchant"           , "Merchant Region"         , 3   , 5 , false ) );
    fgroup.add( new Field         ( "region_issuer"             , "Issuer Region"           , 3   , 5 , false ) );
    fgroup.add( new Field         ("multiple_clearing_seq_count", "Multiple Clearing Count" , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "multiple_clearing_seq_num" , "Multiple Clearing #"     , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "settlement_days"           , "Settlement Days"         , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "technology_type"           , "Technology Type"         , 1   , 3 , true  ) );
    fgroup.add( new HiddenField   ( "dbcr_installtype"          ) );
    fgroup.add( new HiddenField   ( "icc_data"                  ) );
    fgroup.add( new HiddenField   ( "chip_condition_code"       ) );
    fgroup.add( new HiddenField   ( "card_sequence_number"      ) );				//card sequence number for MC ICC
    fgroup.add( new HiddenField   ( "response_code_emv"         ) );				//response code after terminal for VISA EMV
    fgroup.add( new HiddenField   ( "business_format_code"      ) );
    fgroup.add( new Field         ( "usage_code"                , "Usage Code"              , 1   , 1 , true  ) );
    fgroup.add( new Field         ( "transaction_type"          , "Transaction Type"        , 1   , 1 , true  ) );
    
    // fields for card data
    fgroup.add( new HiddenField   ( "card_number"               ) );
    fgroup.add( new HiddenField   ( "card_number_enc"           ) );
    fgroup.add( new HiddenField   ( "card_number_full"          ) );    // used to allow card number to be extracted full from Oracle
    fgroup.add( new Field         ( "card_type"                 , "Card Type"               , 2   , 4 , false ) );
    fgroup.add( new Field         ( "card_type_enhanced"        , "Card Type Enhanced"      , 3   , 5 , false ) );
    fgroup.add( new Field         ( "debit_type"                , "Debit Type (Reg?Fraud?)" , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "foreign_card_indicator"    , "Foreign Card Indicator"  , 1   , 2 , false ) );
    fgroup.add( new Field         ( "cielo_card_association"    , "Card Association"        , 3   , 5 , false ) );
    fgroup.add( new Field         ( "cielo_product"             , "Product Type"            , 4   , 6 , false ) );
    fgroup.add( new Field         ( "cielo_tran_type"           , "Tran Type"               , 4   , 6 , false ) );
    fgroup.add( new HiddenField   ( "cielo_cnp_indicator"       ) );
    fgroup.add( new HiddenField   ( "cielo_signaure_file"       ) );
    fgroup.add( new Field         ( "alm_code"                  , "ALM Code"                , 1   , 3 , true  ) );

      // fields for amount data
    fgroup.add( new Field         ( "debit_credit_indicator"    , "Debit/Credit Ind"        , 1   , 3 , false ) );
    fgroup.add( new CurrencyField ( "transaction_amount"        , "Tran Amount"             ,12   ,14 , false ) );
    fgroup.add( new CurrencyField ( "fx_rate"                   , "Foreign Exchange Rate"   ,12   ,14 , false ) );
    fgroup.add( new CurrencyField ( "installment_amount"        , "Installment Amount"      ,12   ,14 , false ) );
    fgroup.add( new CurrencyField ( "funding_amount"            , "Funding Amount"          ,12   ,14 , false ) );
    fgroup.add( new CurrencyField ( "cashback_amount"           , "Cashback Amount"         ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "tip_amount"                , "Tip Amount"              ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "tax_amount"                , "Tax Amount"              ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "tax_indicator"             , "Tax Indicator"           , 1   , 3 , true  ) );

      // fields for interchange data
    fgroup.add( new HiddenField   ( "ach_flag"                  ) );
    fgroup.add( new HiddenField   ( "reject_reason"             ) );
    fgroup.add( new HiddenField   ( "external_reject"           ) );
    fgroup.add( new Field         ( "ic_cat"                    , "IC Cat Code"             , 5   , 7 , true  ) );
    fgroup.add( new Field         ( "ic_cat_billing"            , "IC Cat Billing"          , 5   , 7 , true  ) );
    fgroup.add( new Field         ( "ic_cat_downgrade"          , "IC Cat Downgrade"        , 5   , 7 , true  ) );
    fgroup.add( new Field         ( "reimbursement_attribute"   , "RA"                      , 1   , 3 , true  ) );

      // fields for authorization data
    fgroup.add( new NumberField   ( "auth_rec_id"               , "Auth Rec ID"             ,28   ,30 , true  , 0 ) );
    fgroup.add( new Field         ( "auth_retrieval_ref_num"    , "Retrieval Ref Num"       ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "trident_tran_id"           , "Trident Tran ID"         ,32   ,34 , true  ) );
    fgroup.add( new HiddenField   ( "client_reference_number"   ) );
    fgroup.add( new Field         ( "auth_currency_code"        , "Auth Currency Code"      , 3   , 5 , false ) );
    fgroup.add( new DateField     ( "auth_date"                 , "Auth Date"                         , true  ) );
    fgroup.add( new DateField     ( "auth_time"                 , "Auth Time"               ,19   ,21 , true  ) );
    fgroup.add( new DateField     ( "auth_date_local"           , "Auth Date Local"                   , true  ) );
    fgroup.add( new DateField     ( "auth_time_local"           , "Auth Time Local"         ,19   ,21 , true  ) );
    fgroup.add( new Field         ( "auth_time_zone"            , "Auth Time Zone"          ,3    ,3  , true  ) );
    fgroup.add( new CurrencyField ( "auth_amount"               , "Auth Amount"             ,12   ,14 , false ) );
    fgroup.add( new CurrencyField ( "auth_amount_total"         , "Auth Amount Total"       ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "auth_tran_fee_amount"      , "Auth Tran Fee Amount"    ,12   ,14 , false ) );
    fgroup.add( new Field         ( "auth_avs_zip"              , "Auth AVS Zip"            ,10   ,12 , true  ) );
    fgroup.add( new Field         ( "auth_code"                 , "Auth Code"               , 6   , 8 , true  ) );
    fgroup.add( new Field         ( "auth_avs_response"         , "AVS Response"            , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "auth_cvv2_response"        , "CVV2 Response"           , 1   , 3 , true  ) );
    fgroup.add( new HiddenField   ( "auth_fpi"                  ) );
    fgroup.add( new Field         ( "auth_response_code"        , "Auth Response Code"      , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "auth_returned_aci"         , "Returned ACI"            , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "auth_source_code"          , "Auth Source Code"        , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "auth_val_code"             , "Validation Code"         , 4   , 6 , true  ) );
    fgroup.add( new Field         ( "auth_tran_id"              , "Auth Tran ID"            ,15   ,17 , true  ) );
    fgroup.add( new Field         ( "network_tran_id"           , "Network Tran ID"         ,15   ,17 , true  ) );
    fgroup.add( new Field         ( "product_id"                , "Product ID"              , 2   , 4 , true  ) );
    fgroup.add( new HiddenField   ( "pos_condition_code"                  ) );
    fgroup.add( new Field         ( "tic"                       , "tic"                     , 2   , 2 , true  ) );
    fgroup.add( new Field         ( "mc_pos_entry_mode"         , "MC POS Entry Mode"       , 3   , 3 , true  ) );
    fgroup.add( new Field         ( "mc_pos_data"               , "MC POS Data"             ,26   ,26 , true  ) );
    fgroup.add( new Field   	  ( "is_partial_auth_enabled"   , "Partial Auth Enabled"              , true  ) );
    
    // fields for hotel & auto rental
    fgroup.add( new NumberField   ( "hotel_rental_days"         , "Number Days"             , 4   , 6 , true  , 0 ) );
    fgroup.add( new CurrencyField ( "rate_daily"                , "Rate Daily"              ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "renter_name"               , "Renter Name"             ,40   ,42 , true  ) );
    fgroup.add( new DateField     ( "statement_date_begin"      , "Statement Date Begin"              , true  ) );
    fgroup.add( new DateField     ( "statement_date_end"        , "Statement Date End"                , true  ) );
    fgroup.add( new CurrencyField ( "total_room_tax"            , "Total Room Tax"          ,12   ,14 , true  ) );

    // fields for Level 3
    fgroup.add( new Field         ( "dest_country_code"         , "Dest Country Code"       , 3   , 5 , true  ) );
    fgroup.add( new CurrencyField ( "discount_amount"           , "Discount Amount"         ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "duty_amount"               , "Duty Amount"             ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "order_date"                , "Order Date"              , 6   , 8 , true  ) );
    fgroup.add( new Field         ( "requester_name"            , "Requester Name"          ,38   ,40 , true  ) );
    fgroup.add( new Field         ( "ship_from_zip"             , "Ship From Zip"           ,10   ,12 , true  ) );
    fgroup.add( new Field         ( "ship_to_zip"               , "Ship to Zip"             ,10   ,12 , true  ) );
    fgroup.add( new CurrencyField ( "shipping_amount"           , "Shipping Amount"         ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "shipping_tax_amount"       , "Shipping Tax Amount"     ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "shipping_tax_rate"         , "Shipping Tax Rate"       , 9   ,11 , true  ) );
    fgroup.add( new Field         ( "vat_reference_number"      , "VAT Reference Number"    ,15   ,17 , true  ) );
    
    // fields to support chargebacks
    fgroup.add( new HiddenField   ( "cb_ref_num"                ) );
    fgroup.add( new HiddenField   ( "documentation_ind"         ) );
    fgroup.add( new HiddenField   ( "original_amount"           ) );
    fgroup.add( new HiddenField   ( "reason_code"               ) );
    fgroup.add( new HiddenField   ( "special_chargeback_ind"    ) );
    fgroup.add( new HiddenField   ( "dispute_condition"         ) );
    fgroup.add( new HiddenField   ( "vrol_case_number"          ) );
    fgroup.add( new HiddenField   ( "dispute_status"            ) );

    // fields to support MeS enhancements
    fgroup.add( new HiddenField   ( "enh_purchase_id"           ) );
    fgroup.add( new HiddenField   ( "enh_purchase_id_format"    ) );
    fgroup.add( new HiddenField   ( "enh_tax_amount"            ) );
    fgroup.add( new HiddenField   ( "enh_tax_indicator"         ) );
    fgroup.add( new HiddenField   ( "enh_level_iii_data_present") );
    
    fgroup.add( new DateField     ( "fx_conv_date"              , "FX Conversion Date"      ,19   ,21 , true  ) );
    ((DateField)getField("fx_conv_date"     )).setDateFormat(DATE_TIME_FORMAT);
    ((DateField)getField("fx_conv_date")).setDateFormatInput(DATE_TIME_FORMAT);
    
    // custom field changes
    ((DateField)getField("transaction_time")).setDateFormatInput(DATE_TIME_FORMAT);
    ((DateField)getField("auth_time")).setDateFormatInput(DATE_TIME_FORMAT);

    // for ALL DateField items: if don't ensure date format and date format input are the same, will lose dates if use WRS "Reject Admin"
    ((DateField)getField("transaction_date"     )).setDateFormat(DATE_TIME_SHORT);
    ((DateField)getField("transaction_time"     )).setDateFormat(DATE_TIME_FORMAT);
    ((DateField)getField("auth_date"            )).setDateFormat(DATE_TIME_FORMAT);
    ((DateField)getField("auth_date"            )).setDateFormatInput(DATE_TIME_FORMAT);
    ((DateField)getField("auth_date_local"      )).setDateFormat(DATE_TIME_FORMAT);
    ((DateField)getField("auth_date_local"      )).setDateFormatInput(DATE_TIME_FORMAT);
    ((DateField)getField("auth_time"            )).setDateFormat(DATE_TIME_FORMAT);
    ((DateField)getField("statement_date_begin" )).setDateFormat(DATE_TIME_SHORT);
    ((DateField)getField("statement_date_end"   )).setDateFormat(DATE_TIME_SHORT);
  }    
  
  public String translateFieldName( String fname )
  {
    String    retVal    = fname;
    
    if ( FieldBeanToFlatFileNameMap != null &&
         FieldBeanToFlatFileNameMap.containsKey(fname) )
    {
      retVal = (String)FieldBeanToFlatFileNameMap.get(fname);
    }
    return( retVal );
  }
  
  // convert the data in this record to a list of flat file records
  public abstract List buildNetworkRecords(int messageType) throws Exception;
  
  protected FlatFileRecord getFlatFileRecord( int recType )
  {
    FlatFileRecord      ffd     = null;
    
    if ( FlatFileRecordMap == null )
    {
      FlatFileRecordMap = new HashMap();
    }
    ffd = (FlatFileRecord)FlatFileRecordMap.get( String.valueOf(recType) );
    
    if ( ffd == null )
    {
      ffd = new FlatFileRecord( recType );
      FlatFileRecordMap.put( String.valueOf(recType), ffd );
    }
    ffd.resetAllFields();
    return( ffd );
  }
  
  public void clear()
  {
    fields.resetDefaults();
    LineItems = null;
  }
  
  public String getCardNumberFull()
  {
    String    retVal    = null;
    
    if ( !isFieldBlank("card_number_full") )
    {
      retVal = getData("card_number_full");
    }
    else
    {
       retVal = decryptEncCardNumber(false);
    }
    return( retVal );
  }
  
  public String getCardTypeEnhanced()
  {
    return( getString("card_type") );   // default to regular card type
  }
  
  public Boolean isForeignExchange() {
	  String fundingCurrencyCode = getData("funding_currency_code");
      String originalCurrencyCode = getData("currency_code");
      
	  return !originalCurrencyCode.equals(fundingCurrencyCode);
  }
  
  public Boolean isAuthLinked() {
	  return !(getLong("auth_rec_id") == 0);
  }
  
  public List getLineItems()
  {
    return( LineItems );
  }
  
  public String getPlanType()
  {
    return( getString("card_type") );   // default to regular card type
  }
  
  public boolean hasLevelIIIData()
  {
    return( "Y".equals( getData("level_iii_data_present") ) );
  }
  
  public boolean isCashDisbursement() 
  {
    return( "Y".equals( getData("cash_disbursement") ) );
  }
  
  public boolean isCardNumberValid()
  {
    return( getData("card_number_full").length() <= 19 );   // overload to check some cards
  }
  
  public boolean isCurrencyCodeAcceptable( VisakBatchData batchData )
  {
    String    tranCurrency = getData("currency_code");

    if( !tranCurrency.equals( getData("auth_currency_code")       ) ) return( false );  // tran & auth currency must match
    if( !tranCurrency.equals( batchData.getCurrencyCode()         ) ) return( false );  // tran & batch currency must match
    if( !tranCurrency.equals( batchData.getFundingCurrencyCode()  ) )                   // tran & funding currency must match...
    {
      if( "Y".equals( getData("cash_disbursement") )  ) return( false );                  // if cash disbursement transaction
      if( !batchData.getMultiCurrency()               ) return( false );                  // if not multi-currency merchant
      switch( RecSettleRecType )                                                          // if not multi-currency card type
      {
        case SETTLE_REC_VISA    : break;
        case SETTLE_REC_MC      : break;
        case SETTLE_REC_MODBII  : break;
        case SETTLE_REC_DISC    : break;
        case SETTLE_REC_AMEX    : 
          if ( isMerchantProcessedByMes() ) return( false );                              // if amex aggregator do not process multi-currency
          break; 
      //case SETTLE_REC_DISC    :
      //case SETTLE_REC_REPORT  :
        default                 : return( false );
      }
    }
    return( true );
  }
  
  public boolean isMerchantProcessedByMes()
  {
    return( MerchantIsProcessedByMeS ); // overload to check non-bank-cards
  }
  
  public boolean isMerchantNumberValid()
  {
    return( true );                     // overload to check non-bank-cards
  }
  
  
	protected String setDE22Subfield(StringBuffer de22, String value, int subfield) throws Exception {

		switch (subfield) {

		case 1:
			char mcAuthPosDataSF11 = value.charAt(10);

			if ('0' == mcAuthPosDataSF11)
				de22.setCharAt(0, '0');
			else if ('1' == mcAuthPosDataSF11)
				de22.setCharAt(0, '1');
			else if ('3' == mcAuthPosDataSF11)
				de22.setCharAt(0, 'M');
			else if ('5' == mcAuthPosDataSF11)
				de22.setCharAt(0, 'D');
			else if ('6' == mcAuthPosDataSF11)
				de22.setCharAt(0, '6');
			else if ('7' == mcAuthPosDataSF11)
				de22.setCharAt(0, 'B');
			
			break;

		case 5:
			char mcAuthPosDataSF4 = value.charAt(3);
			de22.setCharAt(4, mcAuthPosDataSF4);
			
			break;

		case 6:
			char mcAuthPosDataSF5 = value.charAt(4);

			if ('0' == mcAuthPosDataSF5)
				de22.setCharAt(5, '1');
			else if ('1' == mcAuthPosDataSF5)
				de22.setCharAt(5, '0');
			
			break;

		case 7:
			String mcAuthPosEntryModeSF1 = value.substring(0, 2);

			if (mcAuthPosEntryModeSF1.equalsIgnoreCase("01"))
				de22.setCharAt(6, '1');
			else if (mcAuthPosEntryModeSF1.equalsIgnoreCase("90"))
				de22.setCharAt(6, 'B');
			else if (mcAuthPosEntryModeSF1.equalsIgnoreCase("81"))
				de22.setCharAt(6, 'S');
			
			break;
		}

		return de22.toString();
	}
	
	protected boolean isMastercardEdit10Or16Rollback(String property) {

		try {
			return StringUtils.isNotEmpty(MesDefaults.getString(property)) && ("Y".equalsIgnoreCase(MesDefaults.getString(property)));
		}
		catch (Exception e) {
			log.error("Error in retrieving Rollback config flag for MC Edit - " + property + ". Defaulting it to no-rollback.  " + e.getMessage());
			return false;
		}
	}
	
  protected void setPosData()
  {
    String        motoEcommInd          = getPaddedString("moto_ecommerce_ind",1);
    StringBuffer  posDataCode           = null;

    
    switch( RecSettleRecType )
    {
      case SETTLE_REC_MC:
    	  
        boolean isUCAF_1or2 = false;
        String mcAuthPosData = getData("mc_pos_data");
        String mcAuthPosEntryMode = getData("mc_pos_entry_mode") ;
        
        if( "5678".indexOf(motoEcommInd) >= 0 )
        {
          // some cdf trans come as pos_entry_mode 90, card_present = N, moto_ecomm = 7;
          // if motoEcomm = secure ecomm, then make sure pos_entry mode is not 90/swiped
          if( "90".equals(getData("pos_entry_mode")) ) setData( "pos_entry_mode" , "01");

          // s01: SSL;  s02: cardholder auth method;  s03: ucafCollectionInd
          String  esli   = getPaddedString("ecomm_security_level_ind",3);
          if( "221,222".indexOf(esli) >= 0 )
          {
            isUCAF_1or2 = true;
          }
          else
          {
            setData( "ecomm_security_level_ind" , "220"       );
          }
          setData( "cat_indicator"            , "CT6"       );
        }
        else
        {
          setData( "ecomm_security_level_ind" , ""          );
          setData( "cat_indicator"            , "POI"       );
        }

        // POS Data Subfields....
        //  1: Terminal: Card Data Input Capability                    0 = unknown, 1=manual/no term, 2=mag strip, 4=OCR, 5=ICC, 6=keyed, A=contactless mag, B=mag & key, C=mag & ICC & key, D=mag & ICC, E=ICC & key, M=contactless M/Chip, V=other
        //  2: Terminal: Cardholder Authentication Capability          9 = unknown, 0=none, 1=PIN, 2=e-sig analysis, 5=broken, 6=other
        //  3: Terminal: Card Capture Capability                       9 = unknown, 0=none, 1=capable
        //  4: Terminal: Operating Environment                         9 = unknown, 0=no terminal, 1=on attended, 2=on unattended, 3=off attended, 4=off unattended, 5=on cardholder unattended, 6=off cardholder unattended
        //  5: Cardholder: Presense                                    0 = present, 1=NP/unspecified, 2=NP/mail, 3=NP/phone, 4=NP/standing/recurring, 5=NP/ecomm, 9=unknown
        //  6: Card: Presense                                          1 = present, 0=not present, 9=unknown
        //  7: Card: Input Mode                                           0=unknown, 1=manual/no term, 2=mag stripe, 6=keyed, A=contactless mag, B=mag stripe unaltered, C=online chip, F=offline chip, M=contactless M/Chip, N=contactless/PayPass, S=ecomm, T=via server
        //  8: Terminal: Cardholder Authentication Method                 0=none, 1=PIN, 2=esig analysis, 5=manual signature, 6=other manual eg DL, 9=unknown, S=other
        //  9: Terminal: Cardholder Authentication Entity                 0=not authented, 1=ICC/offline PIN, 2=CAD, 3=online PIN, 4=merchant/signature, 5=other, 9=unknown
        // 10: Terminal: Card Data Output Capability                   1 = none, 0=unknown, 2=mag stripe write, 3=ICC, S=other
        // 11: Terminal: Data Output Capability                        0 = unknown, 1=none, 2=printing, 3=display, 4=printing & display
        // 12: Terminal: PIN Capture Capability                        1 = unknown, 0=no PIN, 4-C=4-12 character PIN

        switch( RecBatchType )
        {
          case mesConstants.MBS_BT_VISAK:
            String        hdrDeviceCode         = getData("device_code");
            String        dtlAccountDataSource  = getData("account_data_source");
            String        dtlCardholderId       = getData("cardholder_id_code");

            posDataCode = new StringBuffer("099901---101");

            for( int idx = 0; idx <= 8; ++idx )
            {
              String    value = null;

              switch( idx )
              {
                case 0:   // Subfield 1: Terminal Data: Card Data Input Capability
                  value = SettlementDb.decodeVisakValue("D256",FT_INPUT_CAP,hdrDeviceCode);
                  break;

                case 3:   // Subfield 4: Terminal Operating Environment
                  value = SettlementDb.decodeVisakValue("D256",FT_OP_ENV,hdrDeviceCode);
                  break;

                case 4:   // Subfield 5: Cardholder Present Data
                  value = SettlementDb.decodeVisakValue("D256",FT_MOTO_IND,motoEcommInd.trim());
                  if( !value.equals("0") )
                  {
                    if( "Y".equals(getData("card_present") )                      ||
                        TridentTools.isVisakEntryModeSwiped(dtlAccountDataSource) )
                    {
                      // if the card present indicators or card data input mode and the moto/ecomm indicator are inconsistent,
                      // then force field 5 to match the card present indicators or card data input mode
                      value = "0";  // 0 = Cardholder present
                    }
                  }
                  break;

                case 5:   // Subfield 6: Card Present Data
                  value = SettlementDb.decodeVisakValue("D256",FT_CARD_PRESENT,dtlCardholderId);
                  if( value.equals("0") && TridentTools.isVisakEntryModeSwiped(dtlAccountDataSource) )
                  {
                    // if the card data input mode and cardholder identifcation mode are inconsistent,
                    // then force field 6 to match the card data input mode
                    value = "1";  // 1 = card present
                  }
                  break;

                case 6:   // Subfield 7: Card Data: Input Mode
                  value = SettlementDb.decodeVisakValue("D256",FT_INP_MODE,dtlAccountDataSource);
                  if( value.equals("6") && ("5678".indexOf(motoEcommInd) >= 0) )     // keyed && ecommerce
                  {
                    value = "S";    // change value to e-commerce, channel encrypted
                  }
                  break;

                case 7:   // Subfield 8: Cardholder Authentication Method
                  value = SettlementDb.decodeVisakValue("D256",FT_AUTH_METHOD,dtlCardholderId);
                  break;

                case 8:   // Subfield 9: Cardholder Authentication Entity
                  value = SettlementDb.decodeVisakValue("D256",FT_AUTH_ENTITY,dtlCardholderId);
                  break;
              }
              if( value != null ) posDataCode.setCharAt(idx, value.charAt(0));
            }

            if( hdrDeviceCode.equals("D") )   // if is Dial Terminal
            {
              posDataCode.setCharAt(2, '1');    // card capture capable
              posDataCode.setCharAt(10,'2');    // assume printing only

              if( "Y".equals(getData("debit_flag")) )
              {
                posDataCode.setCharAt(1, '1');    // pin entry capable
                posDataCode.setCharAt(11,'4');    // pin, 4-chars only
              }
            }

            if( isUCAF_1or2 )
            {
              posDataCode.setCharAt(1, '6');    // 6 = other                          (used SecureCode)
              posDataCode.setCharAt(7, 'S');    // S = other systematic verification  (used SecureCode)
              posDataCode.setCharAt(8, '5');    // 5 = other                          (used SecureCode)
            }
            setData( "pos_data_code"            , posDataCode.toString() );
            break;

          case mesConstants.MBS_BT_PG:
          case mesConstants.MBS_BT_VITAL:
            posDataCode   = new StringBuffer( getData("pos_data_code").trim() );
            if( posDataCode.length() != 12                                                                          ||
                ( ! " ".equals(motoEcommInd) && ( posDataCode.charAt(4) == '0' || posDataCode.charAt(5) == '1' ) )  ||
                (   " ".equals(motoEcommInd) && ( posDataCode.charAt(4) != '0' || posDataCode.charAt(5) != '1' ) )  )
            {
                    if(  "90".equals(getData("pos_entry_mode"))   ) setData( "pos_data_code" , "299101254140");
              else  if(   "Y".equals(getData("card_present"))     ) setData( "pos_data_code" , "099101699140");
              else  if(        isUCAF_1or2                        ) setData( "pos_data_code" , "069950SS5140");
              else  if("5678".indexOf(motoEcommInd) >= 0          ) setData( "pos_data_code" , "099950S99140");
              else  if(  "23".indexOf(motoEcommInd) >= 0          ) setData( "pos_data_code" , "099940699140");
              else  if(   "1".equals(motoEcommInd)                ) setData( "pos_data_code" , "099930699140");
              else                                                  setData( "pos_data_code" , "099910699140");
            }
        
            else if( RecBatchType == mesConstants.MBS_BT_VITAL )
            {
              if( "206".equals(posDataCode.substring(4,4+3))      ) setData( "cat_indicator"            , "POI" );
              if( posDataCode.charAt(6) == 'S' && !isUCAF_1or2    ) setData( "ecomm_security_level_ind" , "220" );
              
              /*
               * PROD-1916 
               * Overwriting posdatacode subfield-4 to 0 if subfield-1 is 1 to avoid MC data integrity error (b)
               */
              if(posDataCode.charAt(0) == '1') {
            	  posDataCode.setCharAt(3, '0');
            	  setData( "pos_data_code" , posDataCode.toString());
              }
              
              /*
               * TEM1-1649 
               * Overwriting pos_data_code subfield-5 when we have '9' to '0' or '1' if card_present is 'Y' or 'N' respectively to avoid MC data integrity error
               */
              if(posDataCode.charAt(4) == '9') {
            	  if("Y".equals(getData("card_present"))){
                	  posDataCode.setCharAt(4, '0');
                	  posDataCode.setCharAt(5, '1');
            	  } else {
                	  posDataCode.setCharAt(4, '1');
                	  posDataCode.setCharAt(5, '0');
            	  }
            	  setData( "pos_data_code" , posDataCode.toString());
              }

            }
				
            break;
        }
			posDataCode = new StringBuffer(getData("pos_data_code").trim());
			if ("10".equals(getData("pos_entry_mode"))) {
				posDataCode.setCharAt(6, '7');
				setData("pos_data_code", posDataCode.toString());
			}
			
			log.debug("Before applying MasterCard Edits 10 & 16, value of pos_data_code == " + posDataCode.toString());
			try {

				if (!isMastercardEdit10Or16Rollback(MesDefaults.MC_EDIT_10G_ROLLBACK_SWITCH)) {
					if (StringUtils.isNotEmpty(mcAuthPosEntryMode) && "01,90,81".indexOf(mcAuthPosEntryMode.substring(0, 2)) >= 0) {
						setData("pos_data_code", setDE22Subfield(posDataCode, mcAuthPosEntryMode, 7));
						log.debug("After applying MasterCard Edits 10(g), value of pos_data_code == " + posDataCode.toString());
					}
				}

				if (!isMastercardEdit10Or16Rollback(MesDefaults.MC_EDIT_10E_ROLLBACK_SWITCH)) {
					if (StringUtils.isNotEmpty(mcAuthPosData) && "012345".indexOf(mcAuthPosData.charAt(3)) >= 0) {
						setData("pos_data_code", setDE22Subfield(posDataCode, mcAuthPosData, 5));
						log.debug("After applying MasterCard Edits 10(e), value of pos_data_code == " + posDataCode.toString());
					}
				}

				if (!isMastercardEdit10Or16Rollback(MesDefaults.MC_EDIT_10F_ROLLBACK_SWITCH)) {
					if (StringUtils.isNotEmpty(mcAuthPosData) && "01".indexOf(mcAuthPosData.charAt(4)) >= 0) {
						setData("pos_data_code", setDE22Subfield(posDataCode, mcAuthPosData, 6));
						log.debug("After applying MasterCard Edits 10(f), value of pos_data_code == " + posDataCode.toString());
					}
				}

				if (!isMastercardEdit10Or16Rollback(MesDefaults.MC_EDIT_16_ROLLBACK_SWITCH)) {
					if (StringUtils.isNotEmpty(mcAuthPosData) && "013567".indexOf(mcAuthPosData.charAt(10)) >= 0) {
						setData("pos_data_code", setDE22Subfield(posDataCode, mcAuthPosData, 1));
						log.debug("After applying MasterCard Edits 16, value of pos_data_code == " + posDataCode.toString());
					}
				}
			}
			catch (Exception ex) {
				log.error("Exception occured while applying MC Edit 10(e),(f),(g); Edit 16");
			}
				
        break;

      case SETTLE_REC_AMEX:
        // POS Data Subfields....
        //  1: Terminal: Card Data Input Capability     vk=012      0 = unknown, 1=manual/no term, 2=mag stripe, 3=bar code, 5=icc, 6=keyed, X=mag stripe signature
        //  2: Terminal: Cardholder Authent Capability  vk=01       0 = unknown/not electronic, 1=pin, 6=other
        //  3: Terminal: Card Capture Capability        vk=01       0 = unknown/none, 1=capture
        //  4: Terminal: Operating Environment        !!vk=19       9 = delivery unknown/unspecified, 0=unknown/no term, 1=on attended, 2=on unattended, 3=off attended, 4=off unattended, 5=at cardholder unattended, S=electronic delivery, T=physical delivery
        //  5: Cardholder: Presence                   ??vk=013459   1 = NP/unknown, 0=present, 2=NP/Mail, 3=NP/phone, 4=NP/standing auth, 9=NP/recurrent, S=NP/electronic
        //  6: Card: Presence                         ??vk=019      0 = NP, 1=present, W=transponder, X=contactless
        //  7: Card: Data Input Mode                  ??vk=026ACMS  0 = unknown/unspecified/incomplete track, 1=manual/no term, 2=mag stripe, 3=bar code, 5=icc, 6=keyed, 9=tech fallback, S=keyed/secure, W=swiped/secure, X=mag stripe sig, Y=mag stripe sig/secure
        //  8: Cardholder: Authentication Method        vk=015      0 = unknown/not authented, 1=pin, 5=signature, 6=other manual, S=electronic environment
        //  9: Cardholder: Authentication Entity        vk=034      0 = unknown/not authented, 1=icc, 2=cad/card acceptor device, 4=merchant, 5=other
        // 10: Terminal: Card Data Output Capability    vk=1        1 = none, 0=unknown
        // 11: Terminal: Data Output Capability         vk=02       0 = unknown, 1=none, 2=printing, 3=display, 4=printing and display
        // 12: Terminal: PIN Capture Capability         vk=14       1 = unknown, 0=none, 4-C=4-12 character pin length

        switch( RecBatchType )
        {
          case mesConstants.MBS_BT_VISAK:
            posDataCode = new StringBuffer("000910000101");

            String        hdrDeviceCode         = getData("device_code");
            String        dtlAccountDataSource  = getData("account_data_source");
            String        dtlCardholderId       = getData("cardholder_id_code");

            if( hdrDeviceCode.equals("D") )   // if is Dial Terminal
            {
              posDataCode.setCharAt(2, '1');    // card capture capable
              posDataCode.setCharAt(10,'2');    // assume printing only

              if( "Y".equals(getData("debit_flag")) )
              {
                posDataCode.setCharAt(1, '1');    // pin entry capable
                posDataCode.setCharAt(11,'4');    // pin, 4-chars only
              }
            }

            for( int idx = 0; idx <= 11; ++idx )
            {
              String    value         = null;
              String    amexDefault   = null;
              String    amexSupported = null;

              switch( idx )
              {
                case 0:   // Subfield 1: Terminal Data: Card Data Input Capability
                  value = SettlementDb.decodeVisakValue("D256",FT_INPUT_CAP,hdrDeviceCode);
                  break;

                case 3:   // Subfield 4: Terminal Operating Environment
                  value = SettlementDb.decodeVisakValue("D256",FT_OP_ENV,hdrDeviceCode);
                  break;

                case 4:   // Subfield 5: Cardholder Present Data
                  value = SettlementDb.decodeVisakValue("D256",FT_MOTO_IND,motoEcommInd.trim());
                  if( !value.equals("0") )
                  {
                    if( "Y".equals(getData("card_present") )                      ||
                        TridentTools.isVisakEntryModeSwiped(dtlAccountDataSource) )
                    {
                      // if the card present indicators or card data input mode and the moto/ecomm indicator are inconsistent,
                      // then force field 5 to match the card present indicators or card data input mode
                      value = "0";  // 0 = Cardholder present
                    }
                  }
                  if( "5".equals(value) )value = "S";

                  amexSupported   = "01234S";
                  amexDefault     = "1";
                  break;

                case 5:   // Subfield 6: Card Present Data
                  value = SettlementDb.decodeVisakValue("D256",FT_CARD_PRESENT,dtlCardholderId);
                  if( value.equals("0") && TridentTools.isVisakEntryModeSwiped(dtlAccountDataSource) )
                  {
                    // if the card data input mode and cardholder identifcation mode are inconsistent,
                    // then force field 6 to match the card data input mode
                    value = "1";  // 1 = card present
                  }

                  amexSupported   = "01";
                  amexDefault     = "0";
                  break;

                case 6:   // Subfield 7: Card Data: Input Mode
                  value = SettlementDb.decodeVisakValue("AMEX",FT_INP_MODE,dtlAccountDataSource);
                  if( "6".equals(value) && ("5678".indexOf(motoEcommInd) >= 0) )     // keyed && ecommerce
                  {
                    value = "S";    // change value to e-commerce, channel encrypted
                  }

                  amexSupported   = "012569S";
                  amexDefault     = "0";
                  break;

                case 7:   // Subfield 8: Cardholder Authentication Method
                  value = SettlementDb.decodeVisakValue("D256",FT_AUTH_METHOD,dtlCardholderId);
                  break;

                case 8:   // Subfield 9: Cardholder Authentication Entity
                  value = SettlementDb.decodeVisakValue("D256",FT_AUTH_ENTITY,dtlCardholderId);

                  amexSupported   = "01245";
                  amexDefault     = "0";
                  break;
              }
              if( amexDefault != null && amexSupported.indexOf(value) < 0 )
              {
                value = amexDefault;
              }
              if( value != null ) posDataCode.setCharAt(idx, value.charAt(0));
            }
            setData( "pos_data_code"            , posDataCode.toString() );
            break;

          case mesConstants.MBS_BT_PG:
          case mesConstants.MBS_BT_VITAL:
            posDataCode = new StringBuffer( getData("pos_data_code").trim() );
            if( posDataCode.length() != 12 || posDataCode.charAt(1) == '9' || posDataCode.charAt(2) == '9' )
            {
                    if(  "90".equals(getData("pos_entry_mode"))   ) setData( "pos_data_code" , "200101200140" );
              else  if(   "Y".equals(getData("card_present"))     ) setData( "pos_data_code" , "000101000140" );
              else  if("5678".indexOf(motoEcommInd) >= 0          ) setData( "pos_data_code" , "0600S0000140" );
              else  if(  "23".indexOf(motoEcommInd) >= 0          ) setData( "pos_data_code" , "000090000140" );
              else  if(   "1".equals(motoEcommInd)                ) setData( "pos_data_code" , "000030000140" );
              else                                                  setData( "pos_data_code" , "000010000140" );
                    /* Added for Amex Tokenization*/
                   if ("20".equals(getData("iso_ecommerce_indicator")) && getData("pos_data_code") != null)
                   {
                	    StringBuffer  tempPos = new StringBuffer(getData("pos_data_code"));
                	    tempPos.setCharAt(5, 'Z');
                	    tempPos.setCharAt(6, '5');
                	    setData( "pos_data_code" , tempPos.toString() );
                   }
            }
            break;
        }
        break;
    }
  }

  protected void setArdefData()
  {
    setData( "card_type"                , RecCardType );
    setData( "card_type_enhanced"       , RecCardType );
    setData( "debit_type"               , 0           );
    
    // used for billing and reporting
    setData( "foreign_card_indicator"   , "" );   // for now, assume not-foreign (null) if no ardef
  }    
  
  protected void setArdefData( ArdefDataBase ardef )
  {
    setData( "card_type"                , ardef.getCardTypeFull() );
    setData( "card_type_enhanced"       , ardef.getCardTypeEnhanced() );
    setData( "debit_type"               , ardef.getDebitType() );
    
    // used for billing and reporting
    setData( "foreign_card_indicator"   , ardef.isForeignIssuer(RecBatchType) ? "F" : "" );
  }    
  
	protected void setAuthData() {
		{
			TridentAuthData authData = null;

			switch (RecBatchType) {
			case mesConstants.MBS_BT_VISAK:
				// set the data from the trident authorization if present
				if (getLong("auth_rec_id") > 0) {
					authData = SettlementDb.loadAuthDataTrident(getLong("auth_rec_id"));
				}
				break;

			// case mesConstants.MBS_BT_PG:
			// case mesConstants.MBS_BT_CIELO:
			// break;

			case mesConstants.MBS_BT_VITAL:
				// the Sabre transactions often cannot auth link but is not merchant's fault;
				// set auth_rec_id = 0 (not null) so can distinguish from the visaK/PG "no auth link" trans
				if ("256s".equals(getData("load_filename").substring(0, 4)))
					setData("auth_rec_id", 0);
				java.util.Date javaDate = getDate("transaction_date");
				if (!isFieldBlank("auth_code") && javaDate != null) {
					authData = SettlementDb.loadAuthDataVital(getLong("merchant_number"), new java.sql.Date(javaDate.getTime()), RecSettleRecType,
							getString("card_number"), getString("auth_code"), getString("auth_tran_id"));
				}
				break;
			}

			setAuthData(authData);

      // auth code: trim; convert to all upper case; delete bad characters if off-line
      // tran id:   trim; remove obviously bad values; verify correct length
      String        authCode  = getData("auth_code").trim().toUpperCase();
      String        tranID    = getData("auth_tran_id"  ).trim();
      String        valCode   = getData("auth_val_code" ).trim();
      switch( RecSettleRecType )
      {
        case SETTLE_REC_VISA:
          if( tranID.length() != 15   ||
             (valCode.length() != 4 && RecBatchType != mesConstants.MBS_BT_CIELO)   ||
              "000000000000000,111111111111111".indexOf(tranID) >= 0 )
          {
            // if the auth tran id or val code is invalid, clear them both
            valCode   = "";
            tranID    = "";
            break;
          }
          // if auth date is blank and tran id is present, set auth date to julian date (yddd in tran id)
          if( getDate("auth_date") == null )
          {
            setData( "auth_date", DateTimeFormatter.getFormattedDate(SettlementDb.decodeJulianDate(tranID.substring(2,6)),"MM/dd/yyyy 00:00:00") );
          }
          
          //TEM1-2290: converting space into hyphen for further processing for cdf/adf files 
          if( authData != null ){
        	  if(null != authData.getAuthSourceCode() && (" ".equals(authData.getAuthSourceCode() ))){
        		  setData( "auth_source_code", "-" );
        	  }
          }
          
          //TEM1-2291: this is to convert back auth_source_code to blank for 101 kind of transactions with approval code other than 00
          //           in the case of Sabre files
          if( "256s".equals( getData("load_filename").substring(0,4) ) ){
        	  if(null!=getData("auth_response_code")){
        		  if(!getData("auth_response_code").equals("00") &&
        				  null != getData("auth_source_code") && ("-".equals(getData("auth_source_code") ))){
        			  setData( "auth_source_code", "" );
        		  }
        	  }
          }else{
        	  if((null == authData) && ( "cdf".equals( getData("load_filename").substring(0,3) ) )){
        		  if( null != getData("auth_source_code") && ("-".equals(getData("auth_source_code") ))){
			          setData( "auth_source_code", "" );
		          }
        	  }
          }
          
          break;

			case SETTLE_REC_DISC:
				if (tranID.length() != 15 || "000000000000000,111111111111111".indexOf(tranID) >= 0) {
					tranID = "";
				}
				if (RecBatchType == mesConstants.MBS_BT_VISAK && authData != null)
					setData("track_condition_code", authData.getTrackCondCode());
				break;

			case SETTLE_REC_MODBII:
				if (tranID.replace('0', ' ').trim().length() == 0) {
					tranID = "";
				}
				break;
			}

			if (getLong("auth_rec_id") == 0) {
				boolean isStaticAuthCode = "C".equals(getData("debit_credit_indicator")) && ("TCR001".equals(authCode) || "TCV001".equals(authCode))
						&& !"O".equals(getData("transaction_type")) ? true : false;
				switch (RecSettleRecType) {
				case SETTLE_REC_AMEX:
					// if purchase, approval code must be either 2 or 6 digits; if return, must be all spaces
					if ("D".equals(getData("debit_credit_indicator"))) {
						for (int idx = 0; idx < authCode.length(); ++idx) {
							if (!Character.isDigit(authCode.charAt(idx))) {
								authCode = "";
							}
						}
						if (authCode.length() != 6 && authCode.length() != 2) {
							authCode = "000000";
							tranID = "";
						}
					}
					else if (isStaticAuthCode) {
						setAuthDataToEmpty();
						authCode = "";
						tranID = "";
						valCode = "";
					}
					else {
						authCode = "";
						tranID = "";
					}
					break;
				case SETTLE_REC_MC:
					if (isStaticAuthCode) {
						setAuthDataToEmpty();
						authCode = "";
						tranID = "";
						valCode = "";
					}
					else {
						for (int idx = 0; idx < authCode.length();) {
							if (Character.isLetterOrDigit(authCode.charAt(idx))) {
								++idx;
							}
							else {
								authCode = (new StringBuffer(authCode)).deleteCharAt(idx).toString();
							}
						}
					}
					break;
				default:
					if (isStaticAuthCode) {
						setAuthDataToEmpty();
						authCode = "";
						tranID = "";
						valCode = "";
					}
					else {
						for (int idx = 0; idx < authCode.length();) {
							if (Character.isLetterOrDigit(authCode.charAt(idx))) {
								++idx;
							}
							else {
								authCode = (new StringBuffer(authCode)).deleteCharAt(idx).toString();
							}
						}
					}
					break;
				}
			}

			setData("auth_code", authCode);
			setData("auth_tran_id", tranID);
			setData("auth_val_code", valCode);
		}
	}
  
	protected void setAuthData(TridentAuthData authData) {
		if (authData != null) {
			setData("auth_amount", authData.getAuthAmount());
			setData("auth_tran_fee_amount", authData.getAuthTranFeeAmount());
			setData("auth_avs_response", authData.getAvsResponse());
			setData("auth_avs_zip", authData.getAvsZip());
			setData("auth_code", authData.getAuthCode());
			setData("auth_currency_code", authData.getCurrencyCode());
			setData("auth_cvv2_response", authData.getCvv2Response());
			setData("auth_fpi", authData.getAuthFPI());
			setData("auth_rec_id", authData.getRecId());
			setData("auth_response_code", authData.getResponseCode());
			setData("auth_retrieval_ref_num", authData.getRetrievalRefNum());
			setData("auth_returned_aci", authData.getReturnedACI());
			setData("auth_source_code", authData.getAuthSourceCode());
			setData("auth_tran_id", authData.getAuthTranId());
			if(StringUtils.isNotBlank(authData.getAuthValCode()) && mesConstants.MBS_BT_VITAL == RecBatchType) {
				setData("auth_val_code", authData.getAuthValCode());
			}
			else if(RecBatchType == mesConstants.MBS_BT_VITAL) {
				//if authData.getAuthValCode() is blank don't set any value
			}
			else {
				setData("auth_val_code", authData.getAuthValCode());
			}
			setData("auth_date", DateTimeFormatter.getFormattedDate(authData.getAuthDate(), "MM/dd/yyyy HH:mm:ss"));
			setData("auth_time", DateTimeFormatter.getFormattedDate(authData.getAuthTime(), "MM/dd/yyyy HH:mm:ss"));
			setData("market_specific_data_ind", authData.getMarketSpecificDataInd());
			if (authData.getPosEntryMode() != null)
				setData("pos_entry_mode", authData.getPosEntryMode());
			setData("product_id", authData.getProductResultCode());
			setData("sic_code", authData.getSicCode());
			setData("spend_qualified_ind", authData.getSpendQualifiedInd());
			setData("token_assurance_level", authData.getTokenAssuranceLevel());
			setData("iso_ecommerce_indicator", authData.getIsoEcommerceIndicator());
			setData("pos_condition_code", authData.getPOSConditionCode());
			setData("trident_tran_id", authData.getTridentTranId());
			if (authData.getTic() != null)
				setData("tic", authData.getTic());
			if (authData.getPosEnvironment() != null)
				setData("pos_environment", authData.getPosEnvironment());
			if (authData.getCardSequenceNum() > 0)
				setData("card_sequence_number", authData.getCardSequenceNum());
			if (authData.getMcPosEntryMode() != null)
				setData("mc_pos_entry_mode", authData.getMcPosEntryMode());
			if (authData.getMcPosData() != null)
				setData("mc_pos_data", authData.getMcPosData());

			setData("is_partial_auth_enabled", getPartialAuthEnabled(RecBatchType, authData));

		}
	}

	protected String getPartialAuthEnabled(int recBatchType, TridentAuthData authData) {
		if (RecSettleRecType != SETTLE_REC_DISC) {
			return null;
		}
		if (recBatchType == mesConstants.MBS_BT_VISAK) {
			return authData.getPartialAuthInd();
		}
		if (recBatchType == mesConstants.MBS_BT_VITAL) {
			if (!DISC_PARTIAL_AUTH_DISABLED.equals(authData.getDiscPartialAuthInd())) {
				return DbBooleanType.TRUE.strValue;
			}
		}
		return DbBooleanType.FALSE.strValue;
	}

	protected void setAuthDataToEmpty() {
		setData("auth_avs_response", "");
		setData("auth_avs_zip", "");
		setData("auth_code", "");
		setData("auth_tran_id", "");
		setData("auth_val_code", "");
		setData("auth_currency_code", "");
		setData("auth_cvv2_response", "");
		setData("auth_fpi", "");
		setData("auth_response_code", "");
		setData("auth_retrieval_ref_num", "");
		setData("auth_source_code", "");
		setData("auth_tran_id", "");
		setData("market_specific_data_ind", "");
		setData("spend_qualified_ind", "");
		setData("token_assurance_level", "");
		setData("card_sequence_number", "");
		setData("iso_ecommerce_indicator", "");
		setData("pos_condition_code", "");
		setData("pos_environment", "");
		setData("auth_amount", "");
		setData("auth_amount_total", "");
	}
  
  protected void setCardNumber( String cardNumberFull )
  {
    String    cardNumber        = cardNumberFull;
    String    cardNumberEnc     = null;
    
    
    // attempt to encrypt the full card number
    try
    {
      cardNumberEnc   = ByteUtil.hexStringN(MesEncryption.getClient().encrypt(cardNumber.getBytes()));
      cardNumber      = TridentTools.encodeCardNumber(cardNumber);
    }
    catch( Exception eee )
    {
      // failed to encrypt the card number, still need to encode it
      cardNumberEnc = null;
      cardNumber      = TridentTools.encodeCardNumber(cardNumber);
    }
    
    setData( "card_number"      , cardNumber      );
    setData( "card_number_enc"  , cardNumberEnc   );
    setData( "card_number_full" , cardNumberFull  );
  }
  
  // set fields using the batchData
  public void setFieldsBatchData(VisakBatchData batchData)
    throws java.sql.SQLException
  {
    if( batchData != null )
    {
      setData( "account_status"           , batchData.getAccountStatus());
      setData( "bank_number"              , batchData.getBankNumber());
      setData( "batch_date"               , batchData.getBatchDate() );   // default is batch date, s/b overloaded by settlement extractor
      setData( "batch_id"                 , batchData.getBatchId() );
      setData( "batch_number"             , batchData.getBatchNumber() );
      setData( "currency_code"            , batchData.getCurrencyCode() );
      setData( "dba_address"              , batchData.getDbaAddress());
      setData( "debit_flag"               , batchData.getDebitFlag() );
      setData( "funding_currency_code"    , batchData.getFundingCurrencyCode() );
      setData( "ic_bet_number"            , batchData.getIcBetNumber());
      setData( "ic_enhancement"           , batchData.getIcEnhancement() );
      setData( "ic_intervention"          ,(batchData.getIcIntervention() ? "Y" : "N") );
      setData( "load_filename"            , batchData.getLoadFilename());
      setData( "load_file_id"             , batchData.getLoadFileId());
      setData( "merchant_batch_date"      , batchData.getBatchDate() );
      setData( "merchant_number"          , batchData.getMerchantId());
      setData( "merchant_tax_id"          , batchData.getFederalTaxId());
      setData( "pos_terminal_id"          , batchData.getPosTerminalId());

      String profileId = batchData.getProfileId();
      try
      {
        if( !isBlank(profileId) )
        {
          int termNum = Integer.parseInt( profileId.substring(profileId.length()-4) );
          setData( "terminal_number"          , termNum);
        }
      }
      catch( Exception e )
      {
      }
    }
  }
  
  public void setLevelDataFromIO(LevelDto levelDataBO) 
  {
	  try {
		  if (null != levelDataBO) {
			  displaySetLevelData();
			  // Level II
			  setData("ship_to_zip", StringUtilities.dataSwap(getData("ship_to_zip"), levelDataBO.getLevelII().getShipToZip()));
			  setData("purchase_id", StringUtilities.dataSwap(getData("purchase_id"), levelDataBO.getLevelII().getInvoiceNumber()));

			  if (RecSettleRecType == 1 || RecSettleRecType == 2) {
				  setData("customer_code", StringUtilities.dataSwap(getData("customer_code"), levelDataBO.getLevelII().getInvoiceNumber()));
			  }

			  // Level III
			  setData("merchant_tax_id", StringUtilities.dataSwap(getData("merchant_tax_id"), levelDataBO.getLevelIII().getMerchantTaxId()));
			  setData("discount_amount", StringUtilities.dataSwap(getData("discount_amount"), String.valueOf(levelDataBO.getLevelIII().getDiscountAmount())));
			  setData("shipping_amount", StringUtilities.dataSwap(getData("shipping_amount"), levelDataBO.getLevelIII().getShippingAmount()));
			  setData("duty_amount", StringUtilities.dataSwap(getData("duty_amount"), String.valueOf(levelDataBO.getLevelIII().getDutyAmount())));
			  setData("vat_amount", StringUtilities.dataSwap(getData("vat_amount"), levelDataBO.getLevelIII().getVatAmount()));
			  setData("requester_name", StringUtilities.dataSwap(getData("requester_name"), levelDataBO.getLevelIII().getRequesterName()));

			  displaySetLevelData();

			  if (RecBatchType == 1 || RecBatchType == 3) {
				  fillVisaKLineItemFromIO(levelDataBO);
			  }
		  }
	  }	
	  catch (Exception e) {
		  log.error("setIOLevelData :" + e.toString());
	  }
  }
	
  private void fillVisaKLineItemFromIO(LevelDto levelDataBO)
  {
	  List<LineItem> items = new ArrayList<>();

	  if( LineItems!=null && !LineItems.isEmpty() )
	  {
		  Iterator lineItemsIter = LineItems.iterator();
		  while (lineItemsIter.hasNext()) {
			  LineItem li = (LineItem) lineItemsIter.next();
			  
			  displayLineItem(li);

			  li = li.setFieldsFromIOLineItem(li, levelDataBO, RecSettleRecType);
			  displayLineItem(li);

			  items.add(li);
			  }
	  }
	  else
	  {
		  LineItem li = new LineItem(RecSettleRecType);
		  li = li.setFieldsFromIOLineItem(li, levelDataBO, RecSettleRecType);
		  items.add(li);
	  }
	  if (!items.isEmpty()) {
		  setData("level_iii_data_present", "Y");
		  setLineItems(items);
	  }
  }
  
  public void displaySetLevelData()
  {
	  log.debug("------------------LEVEL 2 & 3 Data");

	  log.debug("getData(\"ship_to_zip\") : " + getData("ship_to_zip"));
	  log.debug("getData(\"purchase_id\") : " + getData("purchase_id"));
	  log.debug("getData(\"tax_amount\") : " + getData("tax_amount"));
	  log.debug("getData(\"dest_country_code\") : " + getData("dest_country_code"));
	  log.debug("getData(\"customer_code\") : " + getData("customer_code"));
	  log.debug("getData(\"merchant_tax_id\") : " + getData("merchant_tax_id"));
	  log.debug("getData(\"customer_tax_id\") : " + getData("customer_tax_id"));
	  log.debug("getData(\"summary_commodity_code\") : " + getData("summary_commodity_code"));
	  log.debug("getData(\"discount_amount\") : " + getData("discount_amount"));
	  log.debug("getData(\"shipping_amount\") : " + getData("shipping_amount"));
	  log.debug("getData(\"duty_amount\") : " + getData("duty_amount"));
	  log.debug("getData(\"ship_from_zip\") : " + getData("ship_from_zip"));
	  log.debug("getData(\"vat_invoice_number\") : " + getData("vat_invoice_number"));
	  log.debug("getData(\"order_date\") : " + getData("order_date"));
	  log.debug("getData(\"vat_amount\") : " + getData("vat_amount"));

	  log.debug("getData(\"requester_name\") : " + getData("requester_name"));
	  log.debug("getData(\"vat_rate\") : " + getData("vat_rate"));

	  log.debug("getData(\"requester_name\") : " + getData("requester_name"));
	  log.debug("getData(\"cardholder_reference_number\") : " + getData("cardholder_reference_number"));
  }
  public void displayLineItem(LineItem li )
  {
	 log.debug("------------------LEVEL LINE ITEMS");
	  
	  if (RecSettleRecType == 1) {
		  log.debug("item_commodity_code : " + li.getData("item_commodity_code"));
		  log.debug("item_descriptor : " + li.getData("item_descriptor"));
		  log.debug("product_code : " + li.getData("product_code"));
		  log.debug("quantity : " + li.getData("quantity"));
		  log.debug("unit_of_measure : " + li.getData("unit_of_measure"));
		  log.debug("unit_cost : " + li.getData("unit_cost"));
		  log.debug("vat_tax_amount : " + li.getData("vat_tax_amount"));
		  log.debug("vat_tax_rate : " + li.getData("vat_tax_rate"));
		  log.debug("discount_per_line : " + li.getData("discount_per_line"));
		  log.debug("line_item_total : " + li.getData("line_item_total"));
		}
	  else if (RecSettleRecType == 2) {
		  log.debug("item_description : " + li.getData("item_description"));
		  log.debug("product_code : " + li.getData("product_code"));
		  log.debug("item_quantity : " + li.getData("item_quantity"));
		  log.debug("item_unit_measure : " + li.getData("item_unit_measure"));
		  log.debug("card_acceptor_tax_id : " + li.getData("card_acceptor_tax_id"));
		  log.debug("tax_rate : " + li.getData("tax_rate"));
		  log.debug("tax_amount : " + li.getData("tax_amount"));
		  log.debug("extended_item_amount : " + li.getData("extended_item_amount"));
		  log.debug("debit_credit_indicator : " + li.getData("debit_credit_indicator"));
		  log.debug("item_discount_amount : " + li.getData("item_discount_amount"));
		  log.debug("item_unit_price : " + li.getData("item_unit_price"));
		  log.debug("item_description : " + li.getData("item_discount_amount"));
		  }
	}
  
  // set fields using a result set
  public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType)
    throws java.sql.SQLException
  {
    setFieldsBatchData(batchData);

    // set the form fields with matching field names
    setFields(resultSet, false, false);
    
    String retVal = null;
    
    if ( !isBlank(getData("card_number_enc")) && isBlank(getData("card_number_full")) ) {
    	//Only encrypted card number available in the record
    	retVal = decryptEncCardNumber(true);
	    setData("card_number_full", retVal);
    	
    }
    if( isBlank(getData("phone_number")) )
      setData( "phone_number"             , batchData.getPhoneNumber() );

    if( RecBatchType == mesConstants.MBS_BT_CIELO )
    {
      setData( "dba_name"                 , batchData.getDbaName() );
      setData( "dba_city"                 , batchData.getDbaCity() );
//    setData( "dba_state"                , batchData.getDbaState() );
      setData( "dba_zip"                  , batchData.getDbaZip() );
      setData( "country_code"             , batchData.getCountryCode() );
      if( isBlank(getData("sic_code")) )
        setData( "sic_code"                 , batchData.getSicCode() );
    }
  }
  
	public void setForeignExchangeFields() throws Exception {
		java.util.Date settlementDate = getDate("settlement_date", DATE_TIME_FORMAT);
		if (settlementDate == null) {
			settlementDate = getDate("batch_date", DATE_TIME_SHORT);
		}
		if (log.isDebugEnabled()) {
			log.debug(String.format("[setForeignExchangeFields()] - set fx_conv_date=%1$tY-%1$tm-%1$td",settlementDate));
		}
		setData("fx_conv_date", DateTimeFormatter.getFormattedDate(settlementDate, DATE_TIME_FORMAT)); // explicitly set default fx_conv_date
	}
  
  private String decryptEncCardNumber(boolean setCardNumberFlag){
	  String retVal = null;
	  boolean retryFlag = false;
	  int retryCounter = 0;
	  try {
		  String cardNumberEnc = getData("card_number_enc");
		  retVal = MesEncryption.decryptString(cardNumberEnc, true, true);
		  if (retVal == null || retVal.length()==0) {
			  if(setCardNumberFlag){
				  log.error("setFieldsBase::decryptEncCardNumber(true) receiving empty/null card number " 
						  + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
			  } else {
				  log.error("getCardNumberFull::decryptEncCardNumber(false) returning empty card number for " + cardNumberEnc );
			  }
			  retryFlag = true;
		  } else if (!TridentTools.mod10Check(retVal)){
			  if(setCardNumberFlag){
				  log.error("setFieldsBase::decryptEncCardNumber(true) receiving invalid card number for " + cardNumberEnc + ":" + Util13.hexStringN(retVal.getBytes())
				  + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
			  } else {
				  log.error("getCardNumberFull::decryptEncCardNumber(false) returning invalid card number for " + cardNumberEnc + ":" + Util13.hexStringN(retVal.getBytes()));
			  }
			  retryFlag = true;
		  }
		  
		//Retry logic for 3 times if any error occurred while decrypt
		  while(retryCounter<3 && retryFlag){
			  retryFlag = false;
			  retryCounter++;
			  log.error("decryptEncCardNumber() retrying decrypt, retryCounter : " + retryCounter );
			  retVal = MesEncryption.decryptString(cardNumberEnc, true, true);
			  if (retVal == null || retVal.length()==0) {
				  log.error("MesEncryption.decryptString() returning empty card number for " + cardNumberEnc );
				  retryFlag = true;
			  } else if (!TridentTools.mod10Check(retVal)){
				  log.error("MesEncryption.decryptString() returning invalid card number for " + cardNumberEnc + ":" + Util13.hexStringN(retVal.getBytes()));
				  retryFlag = true;
			  }
		  }
	  } catch (Exception e) {
	      log.error("com.mes.settlement.SettlementRecord::decryptEncCardNumber() for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"), e);
		  SyncLog.LogEntry("com.mes.settlement.SettlementRecord::decryptEncCardNumber() for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"), e.toString());
	  }
	  return retVal;
  }
  
  // set fields using visa-k detail record
  public void setFieldsVisak(VisakBatchData batchData, int recIdx)
    throws java.sql.SQLException
  {
    setFieldsBatchData(batchData);

    // the CountryCodeConverter xxxToYYY methods take a country code in either format and returns the requested format
    CountryCodeConverter  ccc         = CountryCodeConverter.getInstance();
    DetailRecord          detailRec   = batchData.getDetailRecord(recIdx);

    setData( "batch_record_id"          , recIdx );
    setData( "country_code"             , ccc.numericCodeToAlphaCode( batchData.getCountryCode() ) );
    setData( "dba_name"                 , batchData.getDbaName() );
    setData( "dba_city"                 , batchData.getDbaCity() );
    setData( "dba_state"                , batchData.getDbaState() );
    setData( "dba_zip"                  , batchData.getDbaZip() );
    setData( "phone_number"             , batchData.getPhoneNumber() );
    setData( "sic_code"                 , batchData.getSicCode() );
    setData( "device_code"              , batchData.getHeader().getDeviceCode() );

    setData( "cash_disbursement"        , (TridentTools.isVisakCashAdvance(detailRec.getTransactionCode()) ? "Y" : "N") );
    setData( "debit_credit_indicator"   , (TridentTools.isVisakCredit(detailRec.getTransactionCode()) ? "C" : "D") );
    setData( "cardholder_id_code"       , detailRec.getCardholderIdentificationCode() );
    setData( "cardholder_id_method"     , SettlementDb.decodeVisakValue("D256",FT_ID_METHOD,detailRec.getCardholderIdentificationCode()) );
    setData( "card_present"             , (TridentTools.isVisakCardPresent(detailRec) ? "Y" : "N") );
    setData( "moto_ecommerce_ind"       , (TridentTools.isVisakCardPresent(detailRec) ? null : "1") );    // default CNP to one time MOTO; may set from group later
    setData( "account_data_source"      , detailRec.getAccountDataSource() );
    setData( "pos_entry_mode"           , SettlementDb.decodeVisakValue("D256",FT_DATA_SOURCE,detailRec.getAccountDataSource()) );
    setCardNumber(detailRec.getCardholderAccountNumber().trim());
    setData( "auth_returned_aci"        , detailRec.getReturnedACI() );
    setData( "auth_source_code"         , detailRec.getAuthorizationSourceCode() );
    setData( "auth_response_code"       , detailRec.getResponseCode() );
    setData( "auth_code"                , detailRec.getApprovalCode() );
    setData( "transaction_date"         , TridentTools.decodeVisakTranDateAsDate(detailRec.getLocalTransactionDate()) );
    DateField       df        = (DateField)getField("transaction_time");
    java.util.Date  javaDate  = TridentTools.decodeVisakTranDateTimeAsDate(detailRec.getLocalTransactionDate(),detailRec.getLocalTransactionTime());
    df.setUtilDate(javaDate,"MM/dd/yyyy HH:mm:ss");
    setData("auth_date"					, detailRec.getTransactionDateAndTime());
    setData("auth_time"					, detailRec.getTransactionDateAndTime());
    setData( "auth_avs_response"        , detailRec.getAVSResultCode().trim() );
    setData( "auth_tran_id"             , detailRec.getTransactionIdentifier() );
    setData( "auth_val_code"            , detailRec.getValidationCode() );
    setData( "reimbursement_attribute"  , detailRec.getReimbursementAttribute() );
    setData( "transaction_amount"       , TridentTools.decodeAmount(detailRec.getSettlementAmount()) );
    setData( "auth_amount"              , TridentTools.decodeAmount(detailRec.getAuthorizedAmount()) );
    setData( "auth_amount_total"        , TridentTools.decodeAmount(detailRec.getAuthorizedAmount()) );


    for( int groupId = 0; groupId < TridentTools.VISA_K_GROUP_COUNT; ++groupId )
    {
      if( detailRec.hasGroup(groupId) )
      {
        switch( groupId )
        {
          case /*   1 */ TridentTools.VISA_K_GROUP_CASHBACK:
            setData( "cashback_amount"          , TridentTools.decodeAmount(detailRec.getCashBackAmount()) );
            break;

          case /*   2 */ TridentTools.VISA_K_GROUP_REST:
            double  tipAmount   = TridentTools.decodeAmount(detailRec.getGratuityAmount());
            if( tipAmount != 0.0 )
            {
              setData( "tip_amount"               , tipAmount );
            }
            break;

          case /*   3 */ TridentTools.VISA_K_GROUP_DIRECT_MKT:
            setData( "auth_amount_total"        , TridentTools.decodeAmount(detailRec.getTotalAuthorizedAmount()) );
            setData( "purchase_id_format"       , detailRec.getMOTOPurchaseIdentifierFormatCode() );
            setData( "purchase_id"              , detailRec.getMOTOPurchaseIdentifier() );
            break;

          case /*   4 */ TridentTools.VISA_K_GROUP_AUTO_RENT:
            setData( "auth_amount_total"        , TridentTools.decodeAmount(detailRec.getAutoTotalAuthorizedAmount()) );
            setData( "purchase_id_format"       , detailRec.getAutoPurchaseIdentifierFormatCode() );
            setData( "purchase_id"              , detailRec.getAutoPurchaseIdentifier() );
            setData( "market_specific_data_ind" , detailRec.getAutoMarketSpecificDataIdentifier() );
            setData( "no_show_indicator"        , detailRec.getAutoNoShowIndicator() );
            setData( "extra_charges"            , detailRec.getAutoExtraCharges() );
            setData( "statement_date_begin"     , DateTimeFormatter.parseDate(detailRec.getAutoRentalDate(),"yyMMdd") );

            setData( "statement_date_end"       , TridentTools.decodeVisakTranDateAsDate(detailRec.getLocalTransactionDate()) );
            break;

          case /*   5 */ TridentTools.VISA_K_GROUP_HOTEL:
            setData( "auth_amount_total"        , TridentTools.decodeAmount(detailRec.getHotelTotalAuthorizedAmount()) );
            setData( "purchase_id_format"       , detailRec.getHotelPurchaseIdentifierFormatCode() );
            setData( "purchase_id"              , detailRec.getHotelPurchaseIdentifier() );
            setData( "market_specific_data_ind" , detailRec.getHotelMarketSpecificDataIdentifier() );
            setData( "no_show_indicator"        , detailRec.getHotelNoShowSpecialProgramIndicator() );
            setData( "extra_charges"            , detailRec.getHotelExtraCharges() );
            setData( "statement_date_begin"     , DateTimeFormatter.parseDate(detailRec.getHotelCheckinDate(),"yyMMdd") );

            setData( "statement_date_end"       , TridentTools.decodeVisakTranDateAsDate(detailRec.getLocalTransactionDate()) );
            break;

          case /*   6 */ TridentTools.VISA_K_GROUP_HOTEL_NON_LODGING:
            setData( "sic_code"                 , detailRec.getMerchantCategoryCodeOverride() );
            break;

          case /*   7 */ TridentTools.VISA_K_GROUP_HOTEL_AMEX:
//          setData( ""                         , detailRec.getChargeType() );
            setData( "statement_date_end"       , DateTimeFormatter.parseDate(detailRec.getCheckoutDate(),"yyMMdd") );
            setData( "hotel_rental_days"        , detailRec.getStayDuration() );
            setData( "rate_daily"               , TridentTools.decodeAmount(detailRec.getRoomRateAmount()) );
            break;

          case /*   8 */ TridentTools.VISA_K_GROUP_VMC_PURCH_CARD:
            setData( "tax_indicator"            , detailRec.getLevel2OptionalAmountIdentifier() );
            setData( "tax_amount"               , TridentTools.decodeAmount(detailRec.getLevel2OptionalAmount()) );
            setData( "customer_code"            , detailRec.getLevel2PurchaseOrderNumber() );
            break;

//        case /*   9 */ TridentTools.VISA_K_GROUP_PT:
//          break;

          case /*  10 */ TridentTools.VISA_K_GROUP_DIRECT_DEBIT:
            setData( "auth_retrieval_ref_num"   , detailRec.getRetrievalReferenceNumber() );
//          setData( ""                         , detailRec.getSystemTraceAuditNumber() );
            setData( "debit_network_id"         , detailRec.getNetworkIdentificationCode() );
//          setData( ""                         , detailRec.getSettlementDate() );
            break;

          case /*  11 */ TridentTools.VISA_K_GROUP_MULTI_TERM_STORES:
            setData( "terminal_number"          , detailRec.getTerminalNumberOverride() );
            break;

          case /*  12 */ TridentTools.VISA_K_GROUP_DM_RECURRING_PMTS:
            String clearingSequenceTemp = detailRec.getMOTOMultipleClearingSequenceNumber().trim();
            if ( !clearingSequenceTemp.equals("00") )
            {
              setData( "multiple_clearing_seq_num", clearingSequenceTemp );
            }

            clearingSequenceTemp = detailRec.getMOTOMultipleClearingSequenceCount().trim();
            if ( !clearingSequenceTemp.equals("00") )
            {
              setData( "multiple_clearing_seq_count", clearingSequenceTemp );
            }

            setData( "moto_ecommerce_ind"       , detailRec.getMOTOEcommerceIndicator() );
            break;

//        case /*  13 */ TridentTools.VISA_K_GROUP_AUTO_RENTAL:
//          setData( "renter_name"              , detailRec.getRenterName() );
//          setData( "rental_return_city"       , detailRec.getRentalReturnCity() );
//          setData( "rental_return_state cntry", detailRec.getRentalReturnStateCountry() );
//          setData( "rental_return_location_id", detailRec.getRentalReturnLocationID() );
//          break;

//        case /*  14 */ TridentTools.VISA_K_GROUP_SERVICE_DEV_IND:
//          setData( ""                         , detailRec.getServiceDevelopmentIndicator() );
//          break;

//        case /*  16 */ TridentTools.VISA_K_GROUP_EXISTING_DEBT_IND:
//          setData( ""                         , detailRec.getExistingDebtIndicator() );
//          break;

          case /*  17 */ TridentTools.VISA_K_GROUP_UNIVERSAL_CARD_AUTH:
            setData( "ecomm_security_level_ind" , "22" + detailRec.getUCAFCollectionIndicator() );
            break;

          case /*  18 */ TridentTools.VISA_K_GROUP_ECOMM_GOODS_IND:
            setData( "ecommerce_goods_ind"      , detailRec.getEcommerceGoodsIndicator() );
            break;

//        case /*  19 */ TridentTools.VISA_K_GROUP_CCS_PRIV_LABEL:
//          break;

//        case /*  20 */ TridentTools.VISA_K_GROUP_MERCH_VERIFY_VALUE:
//          break;

          case /*  21 */ TridentTools.VISA_K_GROUP_AMEX_PURCH_CARD:
            setData( "purchase_id"              , detailRec.getAmexSupplierReferenceNumber() );
            setData( "customer_code"            , detailRec.getAmexCardholderReferenceNumber() );
            setData( "ship_to_zip"              , detailRec.getAmexShippedToZIPCode() );
            setData( "tax_amount"               , TridentTools.decodeAmount(detailRec.getAmexSalesTax()) );
//          setData( ""                         , detailRec.getAmexChargeDescriptor1()) );
            break;

          case /*  22 */ TridentTools.VISA_K_GROUP_US_ONLY_NON_TE_COMM_CARD_LII:
            setData( "tax_indicator"            , detailRec.getNonUSLevel2OptionalAmountIdentifier() );
            setData( "tax_amount"               , TridentTools.decodeAmount(detailRec.getNonUSLevel2OptionalAmount()) );
            setData( "customer_code"            , detailRec.getNonUSLevel2PurchaseOrderNumber() );
            break;

          case /*  25 */ TridentTools.VISA_K_GROUP_NON_TE_COMM_CARD_LII:
            setData( "auth_amount_total"        , TridentTools.decodeAmount(detailRec.getLevel3TotalAuthorizedAmount()) );
            setData( "purchase_id_format"       , detailRec.getLevel3PurchaseIdentifierFormatCode() );
            setData( "purchase_id"              , detailRec.getLevel3PurchaseIdentifier() );
//          setData( ""                         , getLevel3LocalTaxIncludedFlag() );
//          setData( ""                         , TridentTools.decodeAmount(detailRec.getLevel3LocalTax()) );
//          setData( ""                         , getLevel3NationalTaxIncludedFlag );
//          setData( ""                         , TridentTools.decodeAmount(detailRec.getLevel3NationalTax()) );
            setData( "customer_code"            , detailRec.getLevel3PurchaseOrderNumber() );
            break;

          case /*  26 */ TridentTools.VISA_K_GROUP_VISA_ENHANCED_LIII:
//          setData( ""                         , detailRec.getVisaLevel3MerchantVATRegistrationNumber() );
//          setData( ""                         , detailRec.getVisaLevel3CustomerVATRegistrationNumber() );
//          setData( ""                         , detailRec.getVisaLevel3SummaryCommodityCode() );
            setData( "discount_amount"          , TridentTools.decodeAmount(detailRec.getVisaLevel3DiscountAmount()) );
            setData( "shipping_amount"          , TridentTools.decodeAmount(detailRec.getVisaLevel3FreightAmount()) );
            setData( "duty_amount"              , TridentTools.decodeAmount(detailRec.getVisaLevel3DutyAmount()) );
            setData( "ship_to_zip"              , detailRec.getVisaLevel3DestinationPostalCode() );
            setData( "ship_from_zip"            , detailRec.getVisaLevel3ShipFromPostalCode() );
            setData( "dest_country_code"        , detailRec.getVisaLevel3DestinationCountryCode() );
            setData( "vat_reference_number"     , detailRec.getVisaLevel3UniqueVATInvoiceReferenceNumber() );
            setData( "order_date"               , detailRec.getVisaLevel3OrderDate() );
            setData( "shipping_tax_amount"      , TridentTools.decodeAmount(detailRec.getVisaLevel3ShippingTaxAmount()) );
            setData( "shipping_tax_rate"        , TridentTools.decodeAmount(detailRec.getVisaLevel3ShippingTaxRate()) );

            setFieldsVisakLineItems(detailRec);
            break;

          case /*  27 */ TridentTools.VISA_K_GROUP_MC_ENHANCED_LIII:
            setData( "shipping_amount"          , detailRec.getMastercardLevel3FreightAmount() );
            setData( "duty_amount"              , detailRec.getMastercardLevel3DutyAmount() );
            setData( "ship_to_zip"              , detailRec.getMastercardLevel3DestinationPostalCode() );
            setData( "ship_from_zip"            , detailRec.getMastercardLevel3ShipFromPostalCode() );
            setData( "dest_country_code"        , detailRec.getMastercardLevel3DestinationCountryCode() );
//          setData( ""                         , detailRec.getMastercardLevel3AlternateTaxAmountIndicator() );
//          setData( ""                         , TridentTools.decodeAmount(detailRec.getMastercardLevel3AlternateTaxAmount()) );

            setFieldsVisakLineItems(detailRec);
            break;

//        case /*  28 */ TridentTools.VISA_K_GROUP_VISA_FLEET:
//          break;

//        case /*  29 */ TridentTools.VISA_K_GROUP_MC_FLEET:
//          break;

//        case /*  30 */ TridentTools.VISA_K_GROUP_AMEX_PC_CHARGE_DESC:
//          break;

          case /*  31 */ TridentTools.VISA_K_GROUP_MKT_SPEC_DATA_IND:
            setData( "market_specific_data_ind" , detailRec.getMarketSpecificDataIndicator() );
            break;

//        case /*  32 */ TridentTools.VISA_K_GROUP_POS_DATA_CODE:
//          setData( "pos_data_code"            , detailRec.getPOSDataCode() );
//          break;

//        case /*  33 */ TridentTools.VISA_K_GROUP_RETAIL_TAA:
//          setData( ""                         , detailRec.getRetailDepartmentName() );
//          break;

//        case /*  34 */ TridentTools.VISA_K_GROUP_LODGING_TAA:
//          setData( "renter_name"              , detailRec.getLodgingRenterName() );
//          break;

//        case /*  35 */ TridentTools.VISA_K_GROUP_TAx_LINE_ITEM_COUNTS:
//          break;

//        case /*  36 */ TridentTools.VISA_K_GROUP_LOCATION_DETAIL_TAA:
//          setData( ""                         , detailRec.getLocationDetailName() );
//          setData( ""                         , detailRec.getLocationDetailAddress() );
//          setData( "rental_location_city"     , detailRec.getLocationDetailCity() );
//          setData( ""                         , detailRec.getLocationDetailRegionCode() );
//          setData( "rental_location_country"  , detailRec.getLocationDetailCountryCode() );
//          setData( ""                         , detailRec.getLocationDetailPostalCode() );
//          break;

//        case /*  37 */ TridentTools.VISA_K_GROUP_MC_MISC_FIELDS:
//          setData( ""                         , detailRec.getMasterCardMiscVersionNumber() );
//          setData( "service_code"             , detailRec.getMasterCardMiscServiceCode() );
//          break;

//        case /*  38 */ TridentTools.VISA_K_GROUP_VISA_MISC_FIELDS:
//          setData( ""                         , detailRec.getVisaMiscVersionNumber() );
//          setData( ""                         , detailRec.getCardLevelResults() );
//          break;

//        case /*  39 */ TridentTools.VISA_K_GROUP_AMEX_CAPN_EXT:
//          setData( "requester_name"           , detailRec.getAmexCapnRequesterName() );
//          setData( "tax_amount"               , TridentTools.decodeAmount(detailRec.getAmexCapnTotalTaxAmount()) );
//          setData( "tax_type_code"            , detailRec.getAmexCapnTaxTypeCode() );
//          break;

//        case /*  40 */ TridentTools.VISA_K_GROUP_DISC_MISC_FIELDS:
//          setData( ""                         , detailRec.getDiscoverMiscVersionNumber() );
//          setData( "pos_data"                 , detailRec.getDiscoverMiscPosDataCode() );
//          setData( ""                         , detailRec.getDiscoverMiscPartialShipmentInd() );
//          break;

//        case /*  42 */ TridentTools.VISA_K_GROUP_AMEX_AUTO_RENTAL:
//          break;

//        case /*  43 */ TridentTools.VISA_K_GROUP_AMEX_INSURANCE:
//          break;

//        case /*  45 */ TridentTools.VISA_K_GROUP_TRANSACTION_SECURITY_INDICATOR:
//          break;

//        case /*  46 */ TridentTools.VISA_K_GROUP_MERCHANT_SOFT_DESCRIPTOR:
//          break;

          case /*  47 */ TridentTools.VISA_K_GROUP_DYNAMIC_DBA:
            if( detailRec.hasTransactionMerchantName()          ) { setData( "dba_name"                 , detailRec.getTransactionMerchantName() );         }
            if( detailRec.hasTransactionMerchantCityOrPhone()   ) { setData( "dba_city"                 , detailRec.getTransactionMerchantCityOrPhone() );
                                                                    setData( "dm_contact_info"          , detailRec.getTransactionMerchantCityOrPhone() );  }
            if( detailRec.hasTransactionMerchantState()         ) { setData( "dba_state"                , detailRec.getTransactionMerchantState() );
                                                                    // the queries in SettlementDB also convert the state code to a country code:  PR=PR, VI=VI, other=US
                                                                    String countryCode = detailRec.getTransactionMerchantState();
                                                                    if( "PR,VI".indexOf(countryCode) < 0 )countryCode = "US";
                                                                    setData( "country_code"             , ccc.numericCodeToAlphaCode( countryCode ) );      }
            if( detailRec.hasTransactionMerchantCityCode()      ) { setData( "dba_zip"                  , detailRec.getTransactionMerchantCityCode().substring(0,5) ); }
            if( detailRec.hasTransactionMerchantCategoryCode()  ) { setData( "sic_code"                 , detailRec.getTransactionMerchantCategoryCode() ); }
            break;
        }
      }
    }
  }

  // set fields using visa-k line item details
  public void setFieldsVisakLineItems(DetailRecord detailRec)
    throws java.sql.SQLException
  {
    if( detailRec.lineItems.size() > 0 )
    {
      List      items         = new ArrayList();
      Iterator  lineItemsIter = detailRec.lineItems.iterator();
      while( lineItemsIter.hasNext() )
      {
        LineItem li = new LineItem(RecSettleRecType);
        switch( RecSettleRecType )
        {
          case SETTLE_REC_VISA:
            li.setFieldsFromVisakLineItem( (VisaLineItemDetailRecord)lineItemsIter.next() );
            break;
          case SETTLE_REC_MC:
            li.setFieldsFromVisakLineItem( (MastercardLineItemDetailRecord)lineItemsIter.next() );
            break;
        }
        if( li != null && li.fixBadData() )
        {
          items.add(li);
        }
      }
      if( items.size() > 0 )
      {
        setData( "level_iii_data_present", "Y" );
        setLineItems(items);
      }
    }
  }

  public void setLineItems( List items )
  {
    LineItems = items;
  }
  
  public void showData()
  {
    try
    {
      Vector allFields = fields.getFieldsVector();

      for(int i=0; i<allFields.size(); ++i)
      {
        Field f = (Field)(allFields.elementAt(i));
        System.out.println(f.getName() + " : " + f.getData());
      }
    }
    catch(Exception e)
    {
      logEntry("showData()", e.toString());
    }
  }
  
  public String removeBadCharacters( String strToFix )
  {
    strToFix = strToFix.trim();
    if( "[invoice]".equals(strToFix) )
    {
      strToFix = "";
    }
    // remove unacceptable (to Visa anyway) leading characters: ` \ [ ^
    while( !isBlank(strToFix) && ("`\\[]^~".indexOf( strToFix.substring(0,1) ) >= 0 ) )
    {
      strToFix = strToFix.substring(1).trim();
    }
    return( strToFix );
  }

  public void fixBadData()
  {
    setData( "dba_name"             , getData("dba_name"    ).trim().toUpperCase() );
    setData( "dba_address"          , getData("dba_address" ).trim().toUpperCase() );
    setData( "dba_city"             , getData("dba_city"    ).trim().toUpperCase() );
    setData( "dba_state"            , getData("dba_state"   ).trim().toUpperCase() );

    setData( "enh_level_iii_data_present" , "N" );
    boolean   icEnhancement   = ("Y,V,M,B".indexOf(getData("ic_enhancement")) >= 0) && (getInt("debit_type") == 0);
    boolean   icIntervention  = "Y".equals(getData("ic_intervention"));
    if( icEnhancement || icIntervention )
    {
      String  cardType    = getPaddedString("card_type",2);
      if( "VB,MB".indexOf(cardType) >= 0 )    // if card that might benefit from tax info
      {
        double  taxAmount   = SettlementDb.calculateTax(  getDouble("transaction_amount") ,
                                                          getData("ship_to_zip")          ,
                                                          getData("auth_avs_zip")         ,
                                                          getData("dba_zip")              );
        if( taxAmount != 0.0 )
        {
          if( icEnhancement )
          {
            setData( "enh_tax_amount"       , taxAmount );
            setData( "enh_tax_indicator"    , "1"       );
          }

          if( icIntervention )
          {
            if( (getDouble("tax_amount")) == 0.0 )
            {
              setData( "tax_amount"       , taxAmount );
              setData( "tax_indicator"    , "1"       );
            }
          }
        }

        if( icEnhancement && !"Y".equals(getData("level_iii_data_present")) )
        {
          if( ( "VB".equals(cardType) && ("V,B".indexOf(getData("ic_enhancement"))) >= 0 ) ||
              ( "MB".equals(cardType) && ("M,B".indexOf(getData("ic_enhancement"))) >= 0 ) )
          {
            setData( "enh_level_iii_data_present" , "Y" );
          }
        }
      }
    }

    setData( "customer_code"      , removeBadCharacters(getData("customer_code")) );

    String purchaseId = removeBadCharacters(getData("purchase_id"));
    String pidFormat  = getData("purchase_id_format").trim();
    if( icIntervention && isBlank(purchaseId) )
    {
      purchaseId = getData("acq_reference_number");
    }
          if ( purchaseId.equals("") )   { pidFormat = "";     }
    else  if (  pidFormat.equals("") )   { pidFormat = "1";    }  // default to 1 / invoice#
    setData( "purchase_id_format" , pidFormat   );
    setData( "purchase_id"        , purchaseId  );
    setData( "purchase_id_unenh"  , purchaseId  );
    if( icEnhancement && isBlank(purchaseId) )
    {
      purchaseId = getData("acq_reference_number");
      if( !isBlank(purchaseId) )
      {
        setData( "enh_purchase_id_format" , "1"         );
        setData( "enh_purchase_id"        , purchaseId  );
      }
    }

    // not sure whether currency_code could possibly be blank here
    if( isFieldBlank("currency_code"      ) ) setData("currency_code"       , getData("funding_currency_code")  );
    if( isFieldBlank("auth_currency_code" ) ) setData("auth_currency_code"  , getData("currency_code")          );

    // if clearing & funding in same currency, set funding_amount now; if not, a timed event will calculate the funding_amount
    if( getData("currency_code").equals(getData("funding_currency_code")) )
    {
      setData("funding_amount", getData("transaction_amount"));
    }

    // if auth_amount_total was not explicity set, then use the auth_amount as the default
    if( getDouble("auth_amount_total") == 0.00 )
    {
      setData("auth_amount_total",getData("auth_amount"));
    }

    // discover doesn't use, m/c allows 4 digits, visa & amex allow 2 digits; we're capping at 99 days for all cards
    if( getInt("hotel_rental_days") > 99 )
    {
      setData("hotel_rental_days",99);
    }
    
    // truncate zip codes to fit the transaction's settlement table
    if ( getData("dba_zip").trim().length() > DbaZipMaxLength )
    {
      setData( "dba_zip", getData("dba_zip").trim().substring(0,DbaZipMaxLength) );
    }

    if( RecBatchType == mesConstants.MBS_BT_CIELO )
    {
      if( DbaZipMaxLength < 8 )
      {
        setData( "dba_zip", "" );     // make sure Visa sends five zeroes rather than the first five of Brazilian zip
      }

      setData( "debit_type", 0 );   // even if this is a regulated debit card, it is not regulated in Brazil

      // change the moto/ecpi if necessary
      String  posEntryMode  = getPaddedString("pos_entry_mode",2);
      if( "00".equals( posEntryMode ) )
      {
        if( "5960,5962,5964,5965,5966,5967,5968,5969".indexOf( getPaddedString("sic_code",4) ) >= 0 )
        {
          setData( "moto_ecommerce_ind", "1" );
        }
        else
        {
          setData( "moto_ecommerce_ind", "" );
        }
      }
      else if( "01".equals( posEntryMode ) )
      {
        String  cieloCnpInd = getPaddedString("cielo_cnp_indicator",1);
        if( "5960,5962,5964,5965,5966,5967,5968,5969".indexOf( getPaddedString("sic_code",4) ) >= 0 )
        {
          if( "A,B,C,D, ".indexOf( cieloCnpInd ) >= 0 )
          {
            setData( "moto_ecommerce_ind", "1" );
          }
        }
        else
        {
          if( "D".indexOf( cieloCnpInd ) >= 0 )
          {
            setData( "moto_ecommerce_ind", "1" );
          }
          else if( "B,C".indexOf( cieloCnpInd ) >= 0 )
          {
            if( "S,Y".indexOf( getPaddedString("cielo_signature_file",1) ) >= 0 )
            {
              setData( "moto_ecommerce_ind", "1" );
            }
          }
        }
      }
    }
    else
    {
      setPosData();
    }
  }
  
  public void fixBadDataAfterIcAssignment()
  {
  }
  
  public void saveEnhDataAfterIcAssignment( String enhRequired )
  {
    if( enhRequired           != null                 && 
        enhRequired.length()  >= NUM_ENHANCED_IND     &&
        !DEF_ENHANCED_IND.equals(enhRequired.substring(0,NUM_ENHANCED_IND)) )
    {
      for( int idx = 0; idx < NUM_ENHANCED_IND; ++idx )
      {
        char enhReq = enhRequired.charAt( idx );
        switch( idx )
        {
          case POS_ENHANCED_IND_PID:
            switch( enhReq )
            {
              case '1':
              case '3':
              case '4':
                setData( "purchase_id"        , getData("enh_purchase_id")  );
                setData( "purchase_id_format" , String.valueOf(enhReq)      );
                break;
            }
            break;

          case POS_ENHANCED_IND_TAX:
            if( enhReq == 'T' )
            {
              setData( "tax_amount"         , getData("enh_tax_amount")     );
              setData( "tax_indicator"      , getData("enh_tax_indicator")  );
            }
            break;

          case POS_ENHANCED_IND_L3:
            if( enhReq == 'L' )
            {
              setData( "level_iii_data_present" , "E"                       );
            }
            break;
        }
      }
    }
  }

  public String setEnhIndPID  ( String enhRequired )  { return( setEnhInd( enhRequired, POS_ENHANCED_IND_PID, '1' ) ); }
  public String setEnhIndTax  ( String enhRequired )  { return( setEnhInd( enhRequired, POS_ENHANCED_IND_TAX, 'T' ) ); }
  public String setEnhIndL3   ( String enhRequired )  { return( setEnhInd( enhRequired, POS_ENHANCED_IND_L3 , 'L' ) ); }
  public String setEnhInd     ( String enhRequired, int pos, char value )
  {
    StringBuffer  temp = new StringBuffer(enhRequired);
    temp.setCharAt(pos, value);
    return( temp.toString() );
  }
  
  protected boolean isEmvEnabled = false;
  
  public void setIsEmvEnabled (boolean isEmvEnabled) {
	  this.isEmvEnabled = isEmvEnabled;
  }
  
  public boolean isForcePostTransaction() 
  {
	  
	String tranId=this.getData("network_tran_id");
	  
	if(StringUtils.isBlank(tranId)) {
		return true;
	}
	
	if(this.RecSettleRecType == SettlementRecord.SETTLE_REC_VISA && StringUtils.isEmpty(this.getData("auth_val_code"))) {
		return true;
	}
	  
	if(this.RecSettleRecType == SettlementRecord.SETTLE_REC_VISA || this.RecSettleRecType == SettlementRecord.SETTLE_REC_AMEX || this.RecSettleRecType == SettlementRecord.SETTLE_REC_DISC) {
		if(tranId.length() < 15 || "000000000000000,111111111111111".indexOf(tranId) >= 0){
			return true;
		}
	}
	  
	if(this.RecSettleRecType == SettlementRecord.SETTLE_REC_MC) {
		if(tranId.length() < 9 || "000000000,111111111".indexOf(tranId) >= 0 || StringUtils.isNumeric(tranId.substring(0, 3))) {
			return true;
		}
	}
	  
	return false;
  }

}
