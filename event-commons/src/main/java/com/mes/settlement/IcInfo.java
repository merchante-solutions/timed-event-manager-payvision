/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/IcInfo.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import com.mes.support.MesMath;

public class IcInfo
{
  protected   String    CardType            = null;
  protected   String    IcCode              = "---";  // clearing system value
  protected   String    IcCodeBilling       = "---";  // billing system value
  protected   String    IcCodeRegulated     = "---";  // regulated debit lookup value
  protected   double    IcExpense           = 0.0;
  protected   String    IcLevel             = null;   // issuer interchange level
  protected   String    IcString            = null;   // encoded issuer values string
  protected   double    PerItem             = 0.0;
  protected   double    Rate                = 0.0;
  
  public String getCardType() { return( CardType ); }
  public void setCardType( String value ) { CardType = value; }

  public String getIcCode() { return( IcCode ); }
  public void setIcCode( String value ) { IcCode = value; }

  public String getIcCodeBilling() { return( IcCodeBilling ); }
  public void setIcCodeBilling( String value ) { IcCodeBilling = value; }

  public String getIcCodeRegulated() { return( IcCodeRegulated ); }
  public void setIcCodeRegulated( String value ) { IcCodeRegulated = value; }

  public double getIcExpense() { return( IcExpense ); }
  public void setIcExpense( double value ) { IcExpense = value; }
  
  public String getIcLevel() { return( IcLevel ); }
  public void setIcLevel( String value ) { IcLevel = value; }
  
  public String getIcString() { return( IcString ); }
  public void setIcString( String value ) { IcString = value; }
  
  public double calcFeeAmount( double tranAmount )
  {
    return( MesMath.round(((Rate * tranAmount * 0.01) + PerItem),2) );
  }
}