/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/LineItem.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import javax.servlet.http.HttpServletRequest;
import com.mes.api.ApiAMLineItemDetailRecord;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.support.StringUtilities;
import dto.LevelDto;
import masthead.formats.visak.MastercardLineItemDetailRecord;
import masthead.formats.visak.VisaLineItemDetailRecord;

public class LineItem extends FieldBean
{
  public LineItem( int recType )
  {
    createFields(null, recType);
  }
  
  protected void createFields(HttpServletRequest request, int recType)
  {
    super.createFields(request);
    
    switch( recType )
    {
      case SettlementRecord.SETTLE_REC_VISA:
        fields.add( new Field         ("item_commodity_code"        , "Item Commodity Code"       ,12 ,14 , true) );
        fields.add( new Field         ("item_descriptor"            , "Item Descriptor"           ,26 ,28 , true) );
        fields.add( new Field         ("product_code"               , "Product Code"              ,12 ,14 , true) );
        fields.add( new NumberField   ("quantity"                   , "Quantity"                  ,12 ,14 , true, 4) );
        fields.add( new Field         ("unit_of_measure"            , "Unit Of Measure"           ,12 ,14 , true) );
        fields.add( new NumberField   ("unit_cost"                  , "Unit Cost"                 ,12 ,14 , true, 4) );
        fields.add( new CurrencyField ("vat_tax_amount"             , "Tax Amount"                ,12 ,14 , true) );
        fields.add( new NumberField   ("vat_tax_rate"               , "Tax Rate"                  , 5 , 7 , true, 2) );
        fields.add( new CurrencyField ("discount_per_line"          , "Discount Per Line"         ,12 ,14 , true) );
        fields.add( new CurrencyField ("line_item_total"            , "Line Item Total"           ,12 ,14 , true) );
        break;

      case SettlementRecord.SETTLE_REC_MC:
        fields.add( new Field         ("product_code"               , "Product Code"              ,15 ,17 , true) );
      case SettlementRecord.SETTLE_REC_AMEX:
        fields.add( new Field         ("item_description"           , "Item Description"          ,35 ,37 , true) );
        fields.add( new NumberField   ("item_quantity"              , "Item Quantity"             ,13 ,15 , true, 0) );
        fields.add( new Field         ("item_unit_measure"          , "Item Unit Measure"         ,12 ,14 , true) );
        fields.add( new Field         ("card_acceptor_tax_id"       , "Card Acceptor Tax ID"      , 9 ,11 , true) );
        fields.add( new NumberField   ("tax_rate"                   , "Tax Rate"                  , 5 , 7 , true, 2) );
        fields.add( new Field         ("tax_type_applied"           , "Tax Applied"               , 4 , 6 , true) );
        fields.add( new CurrencyField ("tax_amount"                 , "Tax Amount"                ,12 ,12 , true) );
        fields.add( new CurrencyField ("extended_item_amount"       , "Extended Item Amount"      ,12 ,14 , true) );
        fields.add( new Field         ("debit_credit_indicator"     , "Debit Credit Ind"          , 1 , 3 , true) );
        fields.add( new CurrencyField ("item_discount_amount"       , "Item Discount Amount"      ,12 ,12 , true) );
        fields.add( new CurrencyField ("item_unit_price"            , "Item Unit Price"           ,12 ,14 , true) );
        break;
    }
  }
  
  public void setFieldsFromVisakLineItem( Object tli )
  {
    if ( tli instanceof VisaLineItemDetailRecord )
    {
      VisaLineItemDetailRecord vli        = (VisaLineItemDetailRecord)tli;
      
      setData("item_commodity_code"        , vli.getItemCommodityCode().trim() );
      setData("item_descriptor"            , vli.getItemDescriptor().trim() );
      setData("product_code"               , vli.getProductCode().trim() );
      setData("quantity"                   , Double.parseDouble(vli.getQuantity()) * 0.0001 );
      setData("unit_of_measure"            , vli.getUnitOfMeasure().trim() );
      setData("unit_cost"                  , Double.parseDouble(vli.getUnitCost()) * 0.0001 );
      setData("vat_tax_amount"             , Double.parseDouble(vli.getVatTaxAmount()) * 0.01 );
      setData("vat_tax_rate"               , Double.parseDouble(vli.getVatTaxRate()) * 0.01 );
      setData("discount_per_line"          , Double.parseDouble(vli.getDiscountPerLineItem()) * 0.01 );
      setData("line_item_total"            , Double.parseDouble(vli.getLineItemTotal()) * 0.01 );
    }
    else if ( tli instanceof MastercardLineItemDetailRecord )
    {
      MastercardLineItemDetailRecord mli  = (MastercardLineItemDetailRecord)tli;
      double quantity                     = Double.parseDouble(mli.getItemQuantity());
      double discountAmount               = Double.parseDouble(mli.getDiscountAmount())*0.01;
      double taxAmount                    = Double.parseDouble(mli.getTaxAmount())*0.01;
      double taxRate                      = Double.parseDouble(mli.getTaxRateApplied())*0.01;
      double totalAmount                  = Double.parseDouble(mli.getExtendedItemAmount())*0.01;
      double unitPrice;
      
      unitPrice = (quantity == 0.0 ? totalAmount : (totalAmount/quantity));
      
      setData("item_description"      , mli.getItemDescription().trim() );
      setData("product_code"          , mli.getProductCode().trim() );
      setData("item_quantity"         , String.valueOf(quantity) );
      setData("item_unit_measure"     , mli.getItemUnitOfMeasure().trim() );
      setData("card_acceptor_tax_id"  , mli.getAlternateTaxIdentifier() );
      setData("tax_rate"              , taxRate );
      setData("tax_type_applied"      , mli.getTaxTypeApplied().trim() );
      setData("tax_amount"            , String.valueOf(taxAmount) );
      setData("extended_item_amount"  , String.valueOf(totalAmount) );
      setData("debit_credit_indicator", mli.getDebitOrCreditIndicator() );
      setData("item_discount_amount"  , String.valueOf(discountAmount) );
      setData("item_unit_price"       , String.valueOf(unitPrice) );
      //@mli.getDiscountIndicator()
      //@mli.getNetOrGrossIndicator()
    }
    else if ( tli instanceof ApiAMLineItemDetailRecord )
    {
      ApiAMLineItemDetailRecord ali  = (ApiAMLineItemDetailRecord)tli;
      setData("item_description"      , ali.getChargeDescription().trim()  );
      setData("item_quantity"         , ali.getChargeItemQuantity() );
      if( getDouble("item_quantity") > 999 )
        setData("item_quantity"         , 999 );
      setData("extended_item_amount"  , ali.getChargeItemAmount()   );
    }
  }

  public LineItem setFieldsFromIOLineItem( LineItem li, LevelDto levelDataBO ,int cardType)
  {
    try 
    {
		if ( cardType == 1 )
		{
			li.setData("item_commodity_code", StringUtilities.dataSwap(li.getData("item_commodity_code"), levelDataBO.getLineItem().getVisaLineItem().getItemCommodityCode().trim()));
			li.setData("item_descriptor", StringUtilities.dataSwap(li.getData("item_descriptor"), levelDataBO.getLineItem().getVisaLineItem().getItemDescriptor().trim()));
			li.setData("product_code", StringUtilities.dataSwap(li.getData("product_code"), levelDataBO.getLineItem().getVisaLineItem().getProductCode().trim()));
			li.setData("quantity", StringUtilities.dataSwap(li.getData("quantity"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getQuantity()))));
			li.setData("unit_of_measure", StringUtilities.dataSwap(li.getData("unit_of_measure"), levelDataBO.getLineItem().getVisaLineItem().getUnitOfMeasure().trim()));
			
			li.setData("unit_cost", StringUtilities.dataSwap(li.getData("unit_cost"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getUnitCost()))));
			li.setData("vat_tax_amount", StringUtilities.dataSwap(li.getData("vat_tax_amount"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getVatTaxAmount()))));
			li.setData("vat_tax_rate", StringUtilities.dataSwap(li.getData("vat_tax_rate"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getVatTaxRate()))));
			li.setData("discount_per_line", StringUtilities.dataSwap(li.getData("discount_per_line"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getDiscountPerLine()))));
			li.setData("line_item_total", StringUtilities.dataSwap(li.getData("line_item_total"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getVisaLineItem().getLineItemTotal()))));
		}
		else if ( cardType == 2 )
		{
			double quantity = Double.parseDouble(""+levelDataBO.getLineItem().getMcLineItem().getItemQuantity());
			double discountAmount = Double.parseDouble(levelDataBO.getLineItem().getMcLineItem().getItemDiscountAmount());
			double taxAmount = Double.parseDouble(levelDataBO.getLineItem().getMcLineItem().getTaxAmount());
			double taxRate = Double.parseDouble(levelDataBO.getLineItem().getMcLineItem().getTaxRateApplied());
			double totalAmount = Double.parseDouble(levelDataBO.getLineItem().getMcLineItem().getExtendedItemAmount());
			double unitPrice = (quantity == 0.0 ? totalAmount : (totalAmount / quantity));

			li.setData("item_description", StringUtilities.dataSwap(li.getData("item_description"), levelDataBO.getLineItem().getMcLineItem().getItemDescription().trim()));
			li.setData("product_code", StringUtilities.dataSwap(li.getData("product_code"), levelDataBO.getLineItem().getMcLineItem().getProductCode().trim()));
			li.setData("item_quantity", StringUtilities.dataSwap(li.getData("item_quantity"), String.valueOf(quantity)));
			li.setData("item_unit_measure", StringUtilities.dataSwap(li.getData("item_unit_measure"), levelDataBO.getLineItem().getMcLineItem().getItemUnitMeasure().trim()));
			li.setData("tax_rate", StringUtilities.dataSwap(li.getData("tax_rate"), String.valueOf(taxRate)));
			li.setData("tax_amount", StringUtilities.dataSwap(li.getData("tax_amount"), String.valueOf(taxAmount)));
			li.setData("extended_item_amount", StringUtilities.dataSwap(li.getData("extended_item_amount"), String.valueOf(totalAmount)));
			li.setData("debit_credit_indicator", StringUtilities.dataSwap(li.getData("debit_credit_indicator"), levelDataBO.getLineItem().getMcLineItem().getDebitCreditIndicator()));
			li.setData("item_discount_amount", StringUtilities.dataSwap(li.getData("item_discount_amount"), String.valueOf(discountAmount)));
			li.setData("item_unit_price", StringUtilities.dataSwap(li.getData("item_unit_price"), String.valueOf(unitPrice)));
			
		}
		else if ( cardType == 3)
		{
			li.setData("item_description", StringUtilities.dataSwap(li.getData("item_description"), levelDataBO.getLineItem().getAmexLineItem().getItemDescription().trim()));
			li.setData("item_quantity", StringUtilities.dataSwap(li.getData("item_quantity"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getAmexLineItem().getItemQuantity()))));
			li.setData("extended_item_amount", StringUtilities.dataSwap(li.getData("extended_item_amount"), String.valueOf(Double.parseDouble(levelDataBO.getLineItem().getAmexLineItem().getExtendedItemAmount()))));
		}
		
	}
	catch (Exception e) 
    {
		log.error("failed to fill l2/l3 line item from IO service, transaction will be processed without L2/L3 data.. :", e);
	}
    
    return li;
  }
  public boolean fixBadData()
  {
    boolean retVal = true;

    if( fieldExists("item_descriptor") )        // if visa
    {
      if( isFieldBlank("item_descriptor")       ||
          getDouble("line_item_total") == 0.0   )
      {
        retVal = false;    // throw away this garbage
      }
      if( retVal == true )
      {
        if( getDouble("quantity") == 0.0 ) setData("quantity",1);    // have an amount for this item, assume quantity 1
      }
    }
    else                                        // if mc or amex
    {
      if( getDouble("extended_item_amount") == 0.0 )
      {
        retVal = false;    // throw away this garbage
      }
      else if( fieldExists("product_code") )      // if mc
      {
        if( "".equals(getData("item_description"    ).replace('0',' ').trim())  ||
            "".equals(getData("product_code"        ).replace('0',' ').trim())  ||
            "".equals(getData("item_unit_measure"   ).replace('0',' ').trim())  )
        {
          retVal = false;    // throw away this garbage
        }
      }
      else                                        // if amex
      {
        if( isFieldBlank("item_description") )
        {
          retVal = false;    // throw away this garbage
        }
      }
      if( retVal == true )
      {
        if( getDouble("item_quantity") == 0.0 ) setData("item_quantity",1); // have an amount for this item, assume quantity 1
        if( getDouble("tax_rate")     > 100.0 ) setData("tax_rate",(getDouble("tax_rate") * 0.01)); // eg got 999% instead of 9.99%
      }
    }

    return retVal;
  }
}