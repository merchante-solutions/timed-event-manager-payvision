/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/ArdefDataVisa.java $

  Description:

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-10-16 14:19:21 -0700 (Thu, 16 Oct 2014) $
  Version            : $Revision: 23192 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import com.mes.constants.mesConstants;

public class ArdefDataVisa
  extends ArdefDataBase
{
  protected int    AccountNumberLength          = 0;
  protected String ALPInd                       = null;
  protected String ArdefCountryCode             = null;
  protected int    ArdefRegion                  = 0;
  protected int    CheckDigitAlgorithm          = 0;
  protected String ComboCard                    = null;
  protected String CommCardLevelIISupport       = null;
  protected String CommCardLevelIIISupport      = null;
  protected String CommCardPosPromptInd         = null;
  protected int    DebitType                    = 0;
  protected String Domain                       = null;
  protected String FastFunds                    = null;
  protected String FundingSource                = null;
  protected String IssuerBin                    = null;
  protected String IssuerCountryCode            = null;
  protected int    IssuerVisaRegion             = 0;
  protected String LargeTicket                  = null;
  protected String NNSSIndicator                = null;
  protected String OriginalCredit               = null;
  protected String OriginalCreditMoneyTransfer  = null;
  protected String OriginalCreditGambling       = null;
  protected String ProcessorBin                 = null;
  protected String ProductId                    = null;
  protected String ProductSubtype               = null;
  protected long   RangeHigh                    = 0L;
  protected long   RangeLow                     = 0L;
  protected String RestrictedUse                = null;
  protected String SettlementMatch              = null;
  protected String TechnologyIndicator          = null;
  protected String TestIndicator                = null;
  protected String TravelAccount                = null;
  protected String TokenIndicator				= null;
  protected String VatEvidence                  = null;
  
  
  public boolean isInRange( String cardNumber )
  {
    boolean retVal = false;
    try
    {
      long cardBin = Long.parseLong( cardNumber.substring(0,9) );
      if ( cardBin >= RangeLow && cardBin <= RangeHigh )
      {
        retVal = true;
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public boolean isForeignIssuer( int recBatchType ) 
  { 
    boolean retVal = false;

    switch( recBatchType )
    {
      case mesConstants.MBS_BT_CIELO:     retVal = !"BR".equals(ArdefCountryCode);    break;  // Brazil
      default:                            retVal = !"US".equals(ArdefCountryCode);    break;  // United States
    }
    return( retVal );
  }
  
  // Returns true if argument is a token, false if not a token or if not a valid bin/token range
  public boolean isToken(String tokenNumber ) 
  { 
	  boolean retVal = false;
	  try
	  {
	     if (isInRange(tokenNumber) && TokenIndicator != null && TokenIndicator.equals("Y"))
	  	   retVal = true;	     
	  }
	  catch( Exception e )
	  {
	  }
	  return( retVal );
  }

  public String getArdefData()
  {
    // the "ardef_data" string is used in InterchangeVisa.java; it must match this position formatting
    // (and cannot change format without considering strings already saved in visa_settlement)
    char[]  ardefData = "                ".toCharArray();   // visa_settlement.ardef_data is varChar(16)

    if( ALPInd           != null &&          ALPInd.length() == 1 )  { ardefData[0] =           ALPInd.charAt(0);  }
    if( LargeTicket      != null &&     LargeTicket.length() == 1 )  { ardefData[1] =      LargeTicket.charAt(0);  }
    if( ProductSubtype   != null &&  ProductSubtype.length() == 2 )  { ardefData[2] =   ProductSubtype.charAt(0);
                                                                       ardefData[3] =   ProductSubtype.charAt(1);  }
    if( RestrictedUse    != null &&   RestrictedUse.length() == 1 )  { ardefData[4] =    RestrictedUse.charAt(0);  }
    if( SettlementMatch  != null && SettlementMatch.length() == 1 )  { ardefData[5] =  SettlementMatch.charAt(0);  }
    if( TravelAccount    != null &&   TravelAccount.length() == 1 )  { ardefData[6] =    TravelAccount.charAt(0);  }
    if( NNSSIndicator    != null &&   NNSSIndicator.length() == 1 )  { ardefData[7] =    NNSSIndicator.charAt(0);  }

    //     sixteen chars
    return( String.valueOf(ardefData) );
  }
  
  public String getCardTypeEnhanced()
  {
    if( ProductId == null || FundingSource == null )return( null );

    //      two-char Ardef Product Id         + one-char Ardef Account Funding Source
    return( (ProductId + "  ").substring(0,2) + (FundingSource + " ").substring(0,1) );
  }
  
  public String getCardTypeFull()
  {
    String    cardTypeFull;
    
    if ( RestrictedUse != null && (RestrictedUse + " ").charAt(0) == 'A' )
    {
      cardTypeFull  = "DB";   // ATM only
    }
    else if ( ProductId != null && InterchangeVisa.CardTypeVisa.isVisaCommercialProduct(ProductId) )
    {
      cardTypeFull  = "VB";   // commercial credit or debit
    }
    else if ( FundingSource != null && "DP".indexOf(FundingSource) >= 0 )
    {
      cardTypeFull  = "VD";   // consumer debit
    }
    else
    {
      cardTypeFull  = "VS";   // consumer credit
    }
    return( cardTypeFull );
  }
  public String getIssuerCountryCode()            { return( IssuerCountryCode );            }
  public long   getRangeHigh()                    { return( RangeHigh );                    }
  public long   getRangeLow()                     { return( RangeLow );                     }
  public String getIssuerBin()                    { return( IssuerBin );                    }
  public int    getCheckDigitAlgorithm()          { return( CheckDigitAlgorithm );          }
  public int    getAccountNumberLength()          { return( AccountNumberLength );          }
  public String getProcessorBin()                 { return( ProcessorBin );                 }
  public int    getDebitType()                    { return( DebitType );                    }
  public String getDomain()                       { return( Domain );                       }
  public int    getIssuerVisaRegion()             { return( IssuerVisaRegion );             }
  public String getLargeTicket()                  { return( LargeTicket );                  }
  public String getTechnologyIndicator()          { return( TechnologyIndicator );          }
  public int    getArdefRegion()                  { return( ArdefRegion );                  }
  public String getArdefCountryCode()             { return( ArdefCountryCode );             }
  public String getComboCard()                    { return( ComboCard );                    }
  public String getCommCardLevelIISupport()       { return( CommCardLevelIISupport );       }
  public String getCommCardLevelIIISupport()      { return( CommCardLevelIIISupport );      }
  public String getCommCardPosPromptInd()         { return( CommCardPosPromptInd );         }
  public String getFastFunds()                    { return( FastFunds );                    }
  public String getVatEvidence()                  { return( VatEvidence );                  }
  public String getNNSSIndicator()                { return( NNSSIndicator );                }
  public String getOriginalCredit()               { return( OriginalCredit );               }
  public String getALPInd()                       { return( ALPInd );                       }
  public String getOriginalCreditMoneyTransfer()  { return( OriginalCreditMoneyTransfer );  }
  public String getOriginalCreditGambling()       { return( OriginalCreditGambling );       }
  public String getProductId()                    { return( ProductId );                    }
  public String getFundingSource()                { return( FundingSource );                }
  public String getSettlementMatch()              { return( SettlementMatch );              }
  public String getTravelAccount()                { return( TravelAccount );                }
  public String getRestrictedUse()                { return( RestrictedUse );                }
  public String getProductSubtype()               { return( ProductSubtype );               }
  public String getTestIndicator()                { return( TestIndicator );                }
  public String getTokenIndicator()               { return( TokenIndicator );               }
  
  public void setIssuerCountryCode( String value )          { IssuerCountryCode = value;          }
  public void setRangeHigh( long value )                    { RangeHigh = value;                  }
  public void setRangeLow( long value )                     { RangeLow = value;                   }
  public void setIssuerBin( String value )                  { IssuerBin = value;                  }
  public void setCheckDigitAlgorithm( int value )           { CheckDigitAlgorithm = value;        }
  public void setAccountNumberLength( int value )           { AccountNumberLength = value;        }
  public void setProcessorBin( String value )               { ProcessorBin = value;               }
  public void setDebitType( int value )                     { DebitType = value;                  }
  public void setDomain( String value )                     { Domain = value;                     }
  public void setIssuerVisaRegion( int value )              { IssuerVisaRegion = value;           }
  public void setLargeTicket( String value )                { LargeTicket = value;                }
  public void setTechnologyIndicator( String value )        { TechnologyIndicator = value;        }
  public void setArdefRegion( int value )                   { ArdefRegion = value;                }
  public void setArdefCountryCode( String value )           { ArdefCountryCode = value;           }
  public void setComboCard( String value )                  { ComboCard = value;                  }
  public void setCommCardLevelIISupport( String value )     { CommCardLevelIISupport = value;     }
  public void setCommCardLevelIIISupport( String value )    { CommCardLevelIIISupport = value;    }
  public void setCommCardPosPromptInd( String value )       { CommCardPosPromptInd = value;       }
  public void setFastFunds( String value )                  { FastFunds = value;                  }
  public void setVatEvidence( String value )                { VatEvidence = value;                }
  public void setNNSSIndicator( String value )              { NNSSIndicator = value;              }
  public void setOriginalCredit( String value )             { OriginalCredit = value;             }
  public void setALPInd( String value )                     { ALPInd = value;                     }
  public void setOriginalCreditMoneyTransfer( String value ){ OriginalCreditMoneyTransfer = value;}
  public void setOriginalCreditGambling( String value )     { OriginalCreditGambling = value;     }
  public void setProductId( String value )                  { ProductId = value;                  }
  public void setFundingSource( String value )              { FundingSource = value;              }
  public void setSettlementMatch( String value )            { SettlementMatch = value;            }
  public void setTravelAccount( String value )              { TravelAccount = value;              }
  public void setRestrictedUse( String value )              { RestrictedUse = value;              }
  public void setProductSubtype( String value )             { ProductSubtype = value;             }
  public void setTestIndicator( String value )              { TestIndicator = value;              }
  public void setTokenIndicator( String value )             { TokenIndicator = value;             }
  
  public void showData()
  {
    System.out.println("CardTypeEnhanced        : " + getCardTypeEnhanced() );
    System.out.println("CardTypeFull            : " + getCardTypeFull() );
    System.out.println("IssuerCountryCode       : " + IssuerCountryCode );
    System.out.println("RangeHigh               : " + RangeHigh );
    System.out.println("RangeLow                : " + RangeLow );
    
    System.out.println("IssuerBin               : " + IssuerBin );
    System.out.println("CheckDigitAlgorithm     : " + CheckDigitAlgorithm );
    System.out.println("AccountNumberLength     : " + AccountNumberLength );
    System.out.println("ProcessorBin            : " + ProcessorBin );
    System.out.println("DebitType               : " + DebitType );
    System.out.println("Domain                  : " + Domain );
    System.out.println("IssuerVisaRegion        : " + IssuerVisaRegion );
    System.out.println("LargeTicket             : " + LargeTicket );
    System.out.println("TechnologyIndicator     : " + TechnologyIndicator );
    System.out.println("ArdefRegion             : " + ArdefRegion );
    System.out.println("ArdefCountryCode        : " + ArdefCountryCode );
    System.out.println("ComboCard               : " + ComboCard );
    System.out.println("CommCardLevelIISupport  : " + CommCardLevelIISupport );
    System.out.println("CommCardLevelIIISupport : " + CommCardLevelIIISupport );
    System.out.println("CommCardPosPromptInd    : " + CommCardPosPromptInd );
    System.out.println("FastFunds               : " + FastFunds );
    System.out.println("VatEvidence             : " + VatEvidence );
    System.out.println("NNSSIndicator           : " + NNSSIndicator );
    System.out.println("OriginalCredit          : " + OriginalCredit );
    System.out.println("ALPInd                  : " + ALPInd );
    System.out.println("OriginalCreditMoneyXfer : " + OriginalCreditMoneyTransfer );
    System.out.println("OriginalCreditGambling  : " + OriginalCreditGambling );
    System.out.println("ProductId               : " + ProductId );
    System.out.println("FundingSource           : " + FundingSource );
    System.out.println("SettlementMatch         : " + SettlementMatch );
    System.out.println("TravelAccount           : " + TravelAccount );
    System.out.println("RestrictedUse           : " + RestrictedUse );
    System.out.println("ProductSubtype          : " + ProductSubtype );
    System.out.println("TestIndicator           : " + TestIndicator );
    System.out.println("LoadFilename            : " + LoadFilename );
    System.out.println("LoadFileId              : " + LoadFileId );
    System.out.println("TokenIndicator          : " + TokenIndicator );
  }
}
