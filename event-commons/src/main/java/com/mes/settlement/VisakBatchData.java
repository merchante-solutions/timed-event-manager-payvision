/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/VisakBatchData.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;
import dto.LevelDto;
import masthead.formats.visak.Batch;
import masthead.formats.visak.DetailRecord;
import masthead.formats.visak.HeaderRecord;
import masthead.formats.visak.ParameterRecord;
import masthead.formats.visak.TrailerRecord;

public class VisakBatchData 
  implements Comparable
{
  private static final Logger log = Logger.getLogger(VisakBatchData.class);

  protected String            AccountStatus       = null;
  protected String            AcquirerBusinessId  = null;
  protected String            AmexSeNumber        = null;
  protected boolean           AmexAggregator      = false;
  protected int               BankNumber          = 0;
  protected Date              BatchDate           = null;
  protected String            BatchDateMMDD       = null;
  protected long              BatchId             = 0L;
  protected int               BatchNumber         = 0;
  protected String            BinNumber           = null;
  protected String            BinNumberMC         = null;
  protected String            CieloSignatureFile  = null;
  protected String            CountryCode         = null;
  protected String            CurrencyCode        = null;
  protected String            DbaAddress          = null;
  protected String            DbaCity             = null;
  protected String            DbaName             = null;
  protected String            DbaState            = null;
  protected String            DbaZip              = null;
  protected String            DebitFlag           = null;
  protected Date              DetailTSMin         = null;
  protected Date              DetailTSMax         = null;
  protected String            DiscoverAcquirerId  = null;
  protected String            DiscoverMID         = null;
  protected boolean           DiscoverAggregator  = false;
  protected String            EligibilityFlagsMC  = null;
  protected String            EligibilityFlagsVisa= null;
  protected String            FederalTaxId        = null;
  protected String            FundingCurrencyCode = null;
  protected String            IcaNumber           = null;
  protected String            IcBetNumber         = null;
  protected String            IcEnhancement       = "N";
  protected boolean           IcIntervention      = false;
  protected long              LoadFileId          = 0L;
  protected String            LoadFilename        = null;
  protected String            McAssignedId        = null;
  protected String            McProgramRegId      = null;
  protected long              MerchantId          = 0L;
  protected String            MotoEcommMerchant   = null;
  protected boolean           MultiCurrency       = false;
  protected String            MVV                 = null;
  protected String            PhoneNumber         = null;
  protected String            PosTermCap          = null;
  protected String            PosTermId           = null;
  protected String            ProfileId           = null;
  protected String            SicCode             = null;
  protected String            CardAcceptorName    = null;
  protected boolean           TestAccount         = true;
  protected Date 			  SettlementDate      = null;
  protected LevelDto 		  levelDto     		  = null;


  public LevelDto getLevelDto() {
	return levelDto;
}

public void setLevelDto(LevelDto levelDto) {
	this.levelDto = levelDto;
}
// visa-k batch data elements
  Batch                       VisakBatch          = null;
  
  public VisakBatchData( )
  {
  }
  
  public VisakBatchData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    AccountStatus           = resultSet.getString("account_status");
    AcquirerBusinessId      = resultSet.getString("acquirer_business_id");
    AmexSeNumber            = resultSet.getString("amex_se_number");
    AmexAggregator          = (resultSet.getString("am_aggregator_list")).indexOf(AmexSeNumber) >= 0;
    BankNumber              = resultSet.getInt("bank_number");
    BatchDate               = resultSet.getDate("batch_date");
    BatchDateMMDD           = resultSet.getString("batch_date_mmdd");
    BatchId                 = resultSet.getLong("batch_id");
    BatchNumber             = resultSet.getInt("batch_number");
    BinNumber               = resultSet.getString("bin_number");
    BinNumberMC             = resultSet.getString("bin_number_mc");
    CieloSignatureFile      = processString(resultSet.getString("cielo_signature_file"));
    CountryCode             = processString(resultSet.getString("country_code"));
    CurrencyCode            = processString(resultSet.getString("currency_code"));
    DbaAddress              = processString(resultSet.getString("addr"));
    DbaCity                 = processString(resultSet.getString("city"));
    DbaName                 = processString(resultSet.getString("dba_name"));
    DbaState                = processString(resultSet.getString("state"));
    DbaZip                  = processString(resultSet.getString("zip"));
    DebitFlag               = resultSet.getString("debit_flag");
    DetailTSMin             = resultSet.getDate("detail_ts_min");
    DetailTSMax             = resultSet.getDate("detail_ts_max");
    DiscoverAcquirerId      = resultSet.getString("ds_acquirer_id");
    DiscoverMID             = resultSet.getString("discover_merchant_number");
    DiscoverAggregator      = ( DiscoverMID.startsWith("601172") || DiscoverMID.equals("601101436679714") );
    EligibilityFlagsMC      = resultSet.getString("eflags_mc");
    EligibilityFlagsVisa    = resultSet.getString("eflags_visa");
    FederalTaxId            = processString(resultSet.getString("tax_id"));
    FundingCurrencyCode     = processString(resultSet.getString("funding_currency_code"));
    IcaNumber               = resultSet.getString("ica_number");
    IcBetNumber             = resultSet.getString("ic_bet_number");
    IcEnhancement           = (resultSet.getString("ic_enhancement") == null) ? "N" : resultSet.getString("ic_enhancement");
    IcIntervention          = processString(resultSet.getString("ic_intervention")).equals("Y");
    LoadFileId              = resultSet.getLong("load_file_id");
    LoadFilename            = processString(resultSet.getString("load_filename"));
    McAssignedId            = resultSet.getString("mc_assigned_id");
    McProgramRegId          = resultSet.getString("program_registration_id");
    MerchantId              = resultSet.getLong("merchant_number");
    MotoEcommMerchant       = resultSet.getString("moto_ecommerce_merchant");
    MultiCurrency           = processString(resultSet.getString("multi_currency")).equals("Y");
    MVV                     = resultSet.getString("mvv");
    PhoneNumber             = processString(resultSet.getString("phone_number"));
    PosTermCap              = processString(resultSet.getString("pos_term_cap"));
    PosTermId               = processString(resultSet.getString("pos_terminal_id"));
    ProfileId               = processString(resultSet.getString("profile_id"));
    SicCode                 = processString(resultSet.getString("mcc"));
    TestAccount             = (resultSet.getInt("test_account") != 0);

    // if received BatchDateMMDD, check if BatchDate needs to be changed to match merchant's date
    if ( BatchDateMMDD != null )
    {
      try
      {
        Calendar        calMeS       = Calendar.getInstance();

        if ( BatchDate != null )
        {
          // set calMes to date received at MeS
          calMeS.setTime(BatchDate);
        }

        // set javaDate to merchant's date and MeS's year
        java.util.Date  javaDate  = DateTimeFormatter.parseDate(BatchDateMMDD + calMeS.get(Calendar.YEAR),"MMddyyyy");

        // if javaDate is valid and not already equal to BatchDate
        if ( javaDate != null && !javaDate.equals(BatchDate) )
        {
          if ( BatchDate != null )
          {
            Calendar        calMerch     = Calendar.getInstance();

            // set calMerch to merchant's date and MeS's year
            calMerch.setTime( javaDate );

            // fix the year if necessary
            calMeS.add(Calendar.DAY_OF_MONTH,30);
            if ( calMerch.after(calMeS) )
            {
              calMerch.add(Calendar.YEAR,-1);
            }
            else
            {
              calMeS.add(Calendar.DAY_OF_MONTH,-(30+30));
              if ( calMerch.before(calMeS) )
              {
                calMerch.add(Calendar.YEAR,+1);
              }
            }

            // reset calMes to date received at MeS
            calMeS.setTime(BatchDate);

            // if merchant date is within 3 days of MeS date, then set BatchDate to merchant date
            calMeS.add(Calendar.DAY_OF_MONTH,4);
            if ( calMerch.before(calMeS) )
            {
              calMeS.add(Calendar.DAY_OF_MONTH,-(4+4));
              if ( calMerch.after(calMeS) )
              {
                BatchDate = new java.sql.Date( calMerch.getTime().getTime() );
              }
            }
          }
          else  // if BatchDate == null
          {
            BatchDate = new java.sql.Date( javaDate.getTime() );
          }
        }
      }
      catch( Exception e )
      {
	      log.error("Error.", e);
      }
    }          
  }
  
  protected String processString( String value ) { return( value == null ? "" : value ); }

  public int compareTo( Object obj )
  {
    return( 0 );
  }
  


  public String   getAccountStatus()                            { return( AccountStatus );                }
  public void     setAccountStatus( String value )              {         AccountStatus = value;          }
  
  public String   getAcquirerBusinessId()                       { return( AcquirerBusinessId );           }
  public void     setAcquirerBusinessId( String value )         {         AcquirerBusinessId = value;     }
  
  public String   getAmexSeNumber()                             { return( AmexSeNumber );                 }
  public void     setAmexSeNumber( String value )               {         AmexSeNumber = value;           }
  
  public boolean  getAmexSeIsAggregator()                       { return( AmexAggregator );               }
  public void     setAmexSeIsAggregator( boolean value )        {         AmexAggregator = value;         }
  
  public int      getBankNumber()                               { return( BankNumber );                   }
  public void     setBankNumber( int value )                    {         BankNumber = value;             }
  
  public Date     getBatchDate( )                               { return( BatchDate );                    }
  public String   getBatchDateFormatted(String fmt)             { return( DateTimeFormatter.getFormattedDate(BatchDate,fmt) ); }

  public long     getBatchId()                                  { return( BatchId );                      }
  public void     setBatchId( long value )                      {         BatchId = value;                }
  
  public int      getBatchNumber( )                             { return( BatchNumber );                  }

  public String   getBinNumber()                                { return( BinNumber );                    }
  public void     setBinNumber( String value )                  {         BinNumber = value;              }
  
  public String   getBinNumberMC()                              { return( BinNumberMC );                  }
  public void     setBinNumberMC( String value )                {         BinNumberMC = value;            }
  
  public String   getCieloSignatureFile()                       { return( CieloSignatureFile );           }
  public void     setCieloSignatureFile( String value )         {         CieloSignatureFile = value;     }
  
  public String   getCountryCode()                              { return( CountryCode );                  }
  public void     setCountryCode( String value )                {         CountryCode = value;            }
  
  public String   getCurrencyCode()                             { return( CurrencyCode );                 }
  public void     setCurrencyCode( String value )               {         CurrencyCode = value;           }
  
  public String   getDbaAddress()                               { return( DbaAddress );                   }
  public void     setDbaAddress( String value )                 {         DbaAddress = value;             }
  
  public String   getDbaCity()                                  { return( DbaCity );                      }
  public void     setDbaCity( String value )                    {         DbaCity = value;                }
  
  public String   getDbaName()                                  { return( DbaName );                      }
  public void     setDbaName( String value )                    {         DbaName = value;                }
  
  public String   getDbaState()                                 { return( DbaState );                     }
  public void     setDbaState( String value )                   {         DbaState = value;               }
  
  public String   getDbaZip()                                   { return( DbaZip );                       }
  public void     setDbaZip( String value )                     {         DbaZip = value;                 }
  
  public boolean  acceptsDebit()                                { return( "Y".equals(DebitFlag) );        }
  public String   getDebitFlag( )                               { return( DebitFlag );                    }
  public void     setDebitFlag( String value )                  {         DebitFlag = value;              }
  
  public Date     getDetailTSMin( )                             { return( DetailTSMin );                  }
  public Date     getDetailTSMax( )                             { return( DetailTSMax );                  }

  public String   getDiscoverAcquirerID()                       { return( DiscoverAcquirerId );           }
  public void     setDiscoverAcquirerID( String value )         {         DiscoverAcquirerId = value;     }
  
  public String   getDiscoverMID()                              { return( DiscoverMID );                  }
  public void     setDiscoverMID( String value )                {         DiscoverMID = value;            }
  
  public boolean  getDiscoverMIDIsAggregator()                  { return( DiscoverAggregator );           }
  public void     setDiscoverMIDIsAggregator( boolean value )   {         DiscoverAggregator = value;     }
  
  public String   getEligibilityFlagsMC()                       { return( EligibilityFlagsMC );           }
  public void     setEligibilityFlagsMC( String value )         {         EligibilityFlagsMC = value;     }
  
  public String   getEligibilityFlagsVisa()                     { return( EligibilityFlagsVisa );         }
  public void     setEligibilityFlagsVisa( String value )       {         EligibilityFlagsVisa = value;   }
  
  public String   getFederalTaxId()                             { return( FederalTaxId );                 }
  public void     setFederalTaxId( String value )               {         FederalTaxId = value;           }
  
  public String   getFundingCurrencyCode()                      { return( FundingCurrencyCode );          }
  public void     setFundingCurrencyCode( String value )        {         FundingCurrencyCode = value;    }
  
  public String   getIcaNumber()                                { return( IcaNumber );                    }
  public void     setIcaNumber( String value )                  {         IcaNumber = value;              }
  
  public String   getIcBetNumber()                              { return( IcBetNumber );                  }
  public void     setIcBetNumber( String value )                {         IcBetNumber = value;            }
  
  public String   getIcEnhancement()                            { return( IcEnhancement );                }
  public void     setIcEnhancement( String value )              {         IcEnhancement = value;          }
  
  public boolean  isIcInterventionEnabled()                     { return( IcIntervention );               }
  public boolean  getIcIntervention()                           { return( IcIntervention );               }
  public void     setIcIntervention( boolean value )            {         IcIntervention = value;         }
  
  public long     getLoadFileId()                               { return( LoadFileId );                   }
  public void     setLoadFileId( long value )                   {         LoadFileId = value;             }
  
  public String   getLoadFilename()                             { return( LoadFilename );                 }
  public void     setLoadFilename( String value )               {         LoadFilename = value;           }
  
  public String   getMcAssignedId()                             { return( McAssignedId );                 }
  public void     setMcAssignedId( String value )               {         McAssignedId = value;           }
  
  public String   getMcProgramRegId()                           { return( McProgramRegId );               }
  public void     setMcProgramRegId( String value )             {         McProgramRegId = value;         }
  
  public long     getMerchantId()                               { return( MerchantId );                   }
  public void     setMerchantId( long value )                   {         MerchantId = value;             }
  
  public String   getMotoEcommMerchant()                        { return( MotoEcommMerchant );            }
  public void     setMotoEcommMerchant( String value )          {         MotoEcommMerchant = value;      }
  
  public boolean  getMultiCurrency()                            { return( MultiCurrency );                }
  public void     setMultiCurrency( boolean value )             {         MultiCurrency = value;          }
  
  public String   getMVV()                                      { return( MVV );                          }
  public void     setMVV( String value )                        {         MVV = value;                    }
  
  public String   getPhoneNumber( )                             { return( PhoneNumber );                  }
  public void     setPhoneNumber( String value )                {         PhoneNumber = value;            }

  public String   getPosTermCap()                               { return( PosTermCap );                   }
  public void     setPosTermCap( String value )                 {         PosTermCap = value;             }
  
  public String   getPosTerminalId()                            { return( PosTermId );                    }
  public void     setPosTerminalId( String value )              {         PosTermId = value;              }
  
  public String   getProfileId()                                { return( ProfileId );                    }
  public void     setProfileId( String value )                  {         ProfileId = value;              }
  
  public String   getSicCode()                                  { return( SicCode );                      }
  public void     setSicCode( String value )                    {         SicCode = value;                }
  
  public boolean  getTestAccount()                              { return( TestAccount );                  }
  public void     setTestAccount( boolean value )               {         TestAccount = value;            }
  public Date     getSettlementDate()							{return SettlementDate;}
  public void     setSettlementDate(Date date)                  {       SettlementDate = date;            }
  public String   getCardAcceptorName()                         { return( CardAcceptorName );             }
  public void     setCardAcceptorName( String value )           {         CardAcceptorName = value;       }
  
  public Batch    getVisakBatch()                               { return( VisakBatch );                   }
  public void     setVisakBatch( Batch value ) 
  { 
    VisakBatch = value; 

    if ( VisakBatch != null )
    {
      HeaderRecord          hdr   = VisakBatch.getHeader();
      if ( hdr != null && hdr.hasGroup( HeaderRecord.GROUP_NUM_HOTEL_AUTO_RENTAL ) )
      {
        PhoneNumber = hdr.getMerchantLocalTelephoneNo();
      }

      ParameterRecord       tpr   = VisakBatch.getParameter();
      if ( tpr != null )
      {
        SicCode = tpr.getMerchantCategoryCode();
        CardAcceptorName=tpr.getMerchantName();
      }
    }
  }


  public HeaderRecord     getHeader         ()  { return( VisakBatch.getHeader()            );  }
  public ParameterRecord  getParameter      ()  { return( VisakBatch.getParameter()         );  }
  public List             getDetailRecords  ()  { return( VisakBatch.getDetailRecords()     );  }      
  public TrailerRecord    getTrailer        ()  { return( VisakBatch.getTrailer()           );  }
  public DetailRecord     getDetailRecord   ( int recIdx )
  {
    DetailRecord    retVal    = null;
    
    try
    {
      retVal = (DetailRecord)(VisakBatch.getDetailRecords()).get(recIdx);
    }
    catch( Exception e )
    {
//@      logEntry( "getDetailRecord(" + BatchId + "," + recIdx + ")", e.toString() );
    }
    return( retVal );
  }
}
