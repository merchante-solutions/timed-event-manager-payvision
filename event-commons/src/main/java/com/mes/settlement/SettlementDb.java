/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementDb.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-10-28 17:28:52 -0700 (Wed, 28 Oct 2015) $
  Version            : $Revision: 23917 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.mes.api.ApiAMLineItemDetailRecord;
import com.mes.api.ApiLineItemDetailRecord;
import com.mes.api.ApiMCLineItemDetailRecord;
import com.mes.api.ApiVisaLineItemDetailRecord;
import com.mes.constants.FormatTypes;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.data.MesDataSourceManager;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.DateField;
import com.mes.forms.FieldBean;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import dto.AmexLineItem;
import dto.LevelDto;
import dto.McLineItem;
import dto.VisaLineItem;
import masthead.formats.visak.Batch;
import masthead.formats.visak.BatchPackager;

public class SettlementDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(SettlementDb.class);
   
  // non-settlement days filter types
  private   static final int      NSF_NONE                    = 0;
  private   static final int      NSF_SUNDAYS_ONLY            = 1;
  private   static final int      NSF_SUNDAYS_AND_HOLIDAYS    = 2;
  private   static final int      NSF_HOLIDAYS_ONLY           = 3;
  
  protected static final String   crlf = "\r\n";
  
  // simple lookup queries for getFieldValueDesc(..)
  private   static final String   qFeeProgramIndicatorDesc = 
    " select  fpid.fee_program_desc_full  as program_desc " + crlf +
    " from    visa_fee_program_ind_desc   fpid            " + crlf +
    " where   fpid.fee_program_indicator = ?              ";
    
  private   static final String   qIcRateDesignatorDesc = 
    " select  irdd.ic_rate_des_desc_full  as program_desc " + crlf +
    " from    mc_ic_rate_designator_desc  irdd            " + crlf +
    " where   irdd.ic_rate_designator = ?                 ";

  private   static final String   qPosEntryModeDesc = 
    " select  emd.pos_entry_desc        " + crlf +
    " from    pos_entry_mode_desc   emd " + crlf +
    " where   emd.pos_entry_code = ?    ";
    

  // queries to get info for TridentAuthData
  private   static final String   qTridentAuthData_all =
    "         authorized_amount                     as auth_amount,             " +
    "         authorization_code                    as auth_code,               " +
    "         fee_program_indicator                 as auth_fpi,                " +
    "         stnd_in_processing_advice_code        as auth_source_code,        " +
    "         avs_result                            as avs_resp,                " +
    "         avs_zip                               as avs_zip,                 " +
    "         lpad(nvl(currency_code,'840'),3,'0')  as currency_code,           " +
    "         cvv2_result                           as cvv2_resp,               " +
    "         market_specific_data_indicator        as market_specific_data_ind," +
    "         product_level_result_code             as product_type_id,         " +
    "         response_code                         as response_code,           " +
    "         retrieval_refrence_number             as rrn,                     " +
    "         lpad(merchant_category_code,4,'0')    as sic_code,                " +
    "         spend_qualified_ind                   as spend_qualified_ind,     " +
    "         token_assurance_level                 as token_assurance_level,   " +
    "         iso_ecommerce_indicator               as iso_ecommerce_indicator, " +
    "         pos_condition_code                    as pos_condition_code,      " +
    "         validation_code                       as val_code                 ";
  
  private   static final String   qTridentExtvisak =
    "         tc33_ext.tic                      	as tic,                     " +
    "         tc33_ext.mc_pos_entry_mode            as mc_pos_entry_mode,       " + 
    "		  tc33_ext.is_partial_auth_enabled      as is_partial_auth_enabled, " +
    "         tc33_ext.mc_pos_data                  as mc_pos_data,             ";
   
  private   static final String   qTridentAuthData__visak =
    " select                                                                    " +
    "         to_char(transaction_time,'YYYY-MM-DD HH24:MI:SS') as auth_date,               " +
    "         transaction_time                      as auth_time,               " +
    "         to_char(transaction_time_local,'YYYY-MM-DD HH24:MI:SS') as auth_date_local, " +
    "         transaction_time_local                as auth_time_local,         " +
    "         time_zone                             as auth_time_zone,          " +
    "         decode( pos_entry_mode,      '10','01',                           " +
    "                                      '11','01',                           " +
    "                                      '12','01',                           " +
    "                                     '100','10',                           " +
    "                                     '901','90',                           " +
    "                                     '902','90',                           " +
    "                                         null) as pos_entry_mode,          " +
    "         returned_aci                          as returned_aci,            " +
    "         transaction_id_alpha                  as tran_id,                 " +
    "         tc33.trident_transaction_id           as trident_tran_id,         " +
    "         card_sequence_number                  as card_sequence_number,    " +
    "         pos_environment                       as pos_environment,         " +
    "         disc_tran_data_cond_code              as disc_tran_data_cond_code," +
    "         tc33.rec_id                           as rec_id,   	            " +
    qTridentExtvisak                                                  	          +
    qTridentAuthData_all                                                          +
    " from    tc33_trident     tc33, tc33_trident_ext	tc33_ext                 ";
    // add where clause when use

  private   static final String   qTridentExtVital =
    "         tc33_ext.mc_pos_entry_mode            as mc_pos_entry_mode,       " +
    "         tc33_ext.mc_pos_data                  as mc_pos_data,             " +
    "		  tc33_ext.disc_partial_auth_ind        as disc_partial_auth_ind,    ";

  private   static final String   qTridentAuthData__vital =
    " select  /*+ index (auth IDX_TC33_MERCH_DATE) */                           " +
    "         to_char(transaction_time,'YYYY-MM-DD HH24:MI:SS')  as auth_date,  " +
    "         transaction_time                      as auth_time,               " +
    "         to_char(transaction_time,'YYYY-MM-DD HH24:MI:SS')  as auth_date_local,         " +
    "         transaction_time		                as auth_time_local,         " +
    "         time_zone 							as auth_time_zone,			" +
    "         decode( pos_entry_mode_code, '10','01',                           " +
    "                                      '11','01',                           " +
    "                                      '12','01',                           " +
    "                                     '901','90',                           " +
    "                                     '902','90',                           " +
    "                                         null) as pos_entry_mode,          " +
    "         auth_characteristics_ind              as returned_aci,            " +
    "         card_sequence_number                  as card_sequence_number,    " +
    "         payment_service_transaction_id        as tran_id,                 " +
    "         null                                  as trident_tran_id,         " +
    "         null                                  as pos_environment,         " +
    "         tran_fee_amount                       as tran_fee_amount,         " +
    "         null                                  as tic,         			" +
    "         tc33.rec_id                           as rec_id,                  " +
    qTridentExtVital                                                              +
    qTridentAuthData_all                                                          +
    " from    tc33_authorization tc33, tc33_authorization_ext	tc33_ext                                                ";
    // add where clause when use

  // queries to get info for VisaKBatchData
  private   static final String   qVisakBatchData_all =
    "         mf.merchant_number                    as merchant_number,         " +
    "         mf.bank_number                        as bank_number,             " +
    "         mf.ic_bet_visa                        as ic_bet_number,           " +
    "         decode(mf.test_account,'N',0,1)       as test_account,            " +
    "         mb.vs_acquirer_id                     as acquirer_business_id,    " +
    "         mb.vs_bin_base_ii                     as bin_number,              " +
    "         mb.mc_bin_inet                        as bin_number_mc,           " +
    "         mb.mc_ica_number                      as ica_number,              " +
    "         nvl(mb.am_aggregators,'.')            as am_aggregator_list,      " +
    "         mb.ds_acquirer_id                     as ds_acquirer_id,          " +
    "         mf.merchant_verification_val          as mvv,                     " +
    "         mf.mc_program_reg_id                  as program_registration_id, " +
    "         mf.mc_assigned_id                     as mc_assigned_id,          " +
    "         mf.visa_eligibility_flags             as eflags_visa,             " +
    "         mf.mc_eligibility_flags               as eflags_mc,               " +
    "         decode(mf.debit_plan,null,'N','Y' )   as debit_flag,              " +
    "         nvl(mf.dmacctst,       '-')           as account_status,          " +
    "         nvl(mf.funding_currency_code,'840')   as funding_currency_code,   " +
    "         nvl(mf.ic_enhancement, 'N')           as ic_enhancement,          " +
    "         decode(mf.federal_tax_id,0,null,                                  " +
    "                lpad(mf.federal_tax_id,9,'0')) as tax_id,                  ";

  private   static final String   qVisakBatchData_batch_bt =
    "         bt.terminal_id                        as profile_id,              " +
    "         bt.currency_code                      as currency_code,           " +
    "         bt.load_filename                      as load_filename,           " +
    "         bt.load_file_id                       as load_file_id,            " +
    "         bt.merchant_batch_date                as batch_date,              " +
    "         get_trident_batch_id_field(bt.trident_batch_id,1)                 " +
    "                                               as batch_date_mmdd,         " +
    "         bt.detail_timestamp_min               as detail_ts_min,           " +
    "         bt.detail_timestamp_max               as detail_ts_max,           " +
    "         bt.batch_number                       as batch_number,            " +
    "         bt.batch_id                           as batch_id,                ";

  private   static final String   qVisakBatchData_batch_mh =
    "         decode( substr(mh.load_filename,1,3),                             " +
    "                 'cdf',substr(mh.reserved_3,42,20),                        " +
    "                 null )                        as profile_id,              " +
    "         nvl(mf.funding_currency_code,'840')   as currency_code,           " +
    "         mh.load_filename                      as load_filename,           " +
    "         load_filename_to_load_file_id(mh.load_filename) as load_file_id,  " +
    "         mh.batch_date                         as batch_date,              " +
    "         null                                  as batch_date_mmdd,         " +
    "         mh.batch_date                         as detail_ts_min,           " +
    "         mh.batch_date                         as detail_ts_max,           " +
    "         substr(lpad(nvl(mh.batch_num,1),3,'0'),-3)                        " +
    "                                               as batch_number,            " +
    "         mh.btc_seq_num                        as batch_id,                ";

  private   static final String   qVisakBatchData_isCielo_NO =
    "         null                                  as cielo_signature_file,    ";

  private   static final String   qVisakBatchData_isCielo_YES =
    "         nvl(mfc.signature_file_indicator,'N') as cielo_signature_file,    ";

  private   static final String   qVisakBatchData_motoecomm_NO =
    "         'N'                                   as moto_ecommerce_merchant, ";

  private   static final String   qVisakBatchData_motoecomm_sm =
    "         nvl(sm.moto_merchant,'N')             as moto_ecommerce_merchant, ";

  private   static final String   qVisakBatchData_term_mf =
    "         null                                  as pos_terminal_id,         " +
    "         nvl(mf.dmdsnum, '0')                  as discover_merchant_number," +
    "         nvl(mf.damexse, '0')                  as amex_se_number,          " +
    "         mf.sic_code                           as mcc,                     " +
    "         mf.dba_name                           as dba_name,                " +
    "         mf.phone_1                            as phone_number,            " +
    "         substr( nvl(mf.dmaddr,nvl(mf.addr1_line_1,mf.addr1_line_2)),1,25 )" +
    "                                               as addr,                    " +
    "         mf.dmcity                             as city,                    " +
    "         mf.dmstate                            as state,                   " +
    "         mf.dmzip                              as zip,                     " +
    "         mf.country_code_syf                   as country_code,            " +
    "         'N'                                   as multi_currency,          " +
    "         'N'                                   as ic_intervention,         ";

  private   static final String   qVisakBatchData_term_tp =
    "         tp.catid                              as pos_terminal_id,         " +
    "         nvl(tp.disc_caid, '0')                as discover_merchant_number," +
    "         nvl(tp.amex_caid, '0')                as amex_se_number,          " +
    "         lpad(nvl(tp.sic_code,mf.sic_code),4,'0') as mcc,                  " +
    "         nvl(tp.merchant_name,mf.dba_name)     as dba_name,                " +
    "         nvl(tp.phone_number,mf.phone_1)       as phone_number,            " +
    "         nvl(tp.addr_line_1,mf.addr1_line_1)   as addr,                    " +
    "         nvl(tp.addr_city,mf.dmcity)           as city,                    " +
    "         nvl(tp.addr_state,mf.dmstate)         as state,                   " +
    "         substr(nvl(tp.addr_zip,mf.dmzip),1,5) as zip,                     " +
    "         nvl(mf.country_code_syf,                                          " +
    "             decode(tp.addr_state,                                         " +
    "                    'PR','PR',                                             " +
    "                    'VI','VI',                                             " +
    "                    'US'))                     as country_code,            " +
    "         decode(tp.intl_processor,null,'N',                                " +
               mesConstants.INTL_PID_DCC+",'Y','N') as multi_currency,          " +
    "         nvl(tp.ic_intervention,'N')           as ic_intervention,         ";

  private   static final String   qVisakBatchData__mbs_batches =
    " select                                                                    " +
    qVisakBatchData_all                                                           +
    qVisakBatchData_batch_bt                                                      +
    qVisakBatchData_motoecomm_sm                                                  +
    qVisakBatchData_term_tp                                                       +
    qVisakBatchData_isCielo_NO                                                    +
    "         nvl(tpc.d256_term_cap,'9')            as pos_term_cap             " +
    " from    mbs_batches                   bt,                                 " +
    "         trident_profile               tp,                                 " +
    "         trident_product_codes         tpc,                                " +
    "         monthly_extract_summary       sm,                                 " +
    "         mbs_banks                     mb,                                 " +
    "         mif                           mf                                  " +
    " where   bt.batch_id               = ?                                     " +
    "         and tp.terminal_id        = bt.terminal_id                        " +
    "         and tpc.product_code(+)   = tp.product_code                       " +
    "         and sm.merchant_number(+) = bt.merchant_number                    " +
    "         and sm.active_date(+) =trunc(trunc((sysdate-5),'month')-1,'month')" +
    "         and mf.merchant_number    = bt.merchant_number                    " +
    "         and mb.bank_number        = bt.bank_number                        ";

  private   static final String   qVisakBatchData__vital =
    " select  /*+ ordered */                                                    " +
    qVisakBatchData_all                                                           +
    qVisakBatchData_batch_mh                                                      +
    qVisakBatchData_motoecomm_sm                                                  +
    qVisakBatchData_term_mf                                                       +
    qVisakBatchData_isCielo_NO                                                    +
    "         nvl(mh.pos_capabilities,'9')          as pos_term_cap             " +
    " from    daily_detail_file_d256_mh     mh,                                 " +
    "         monthly_extract_summary       sm,                                 " +
    "         mbs_banks                     mb,                                 " +
    "         mif                           mf                                  " +
    " where   mh.btc_seq_num            = ?                                     " +
    "         and sm.merchant_number(+) = mh.merchant_number                    " +
    "         and sm.active_date(+) =trunc(trunc((sysdate-5),'month')-1,'month')" +
    "         and mf.merchant_number    = mh.merchant_number                    " +
    "         and mb.bank_number        = mf.bank_number                        ";

  private   static final String   qVisakBatchData__cielo =
    " select                                                                    " +
    qVisakBatchData_all                                                           +
    qVisakBatchData_batch_bt                                                      +
    qVisakBatchData_motoecomm_NO                                                  +
    qVisakBatchData_term_mf                                                       +
    qVisakBatchData_isCielo_YES                                                   +
    "         null                                  as pos_term_cap             " +
    " from    mbs_batches                   bt,                                 " +
    "         mbs_banks                     mb,                                 " +
    "         mif_cielo                     mfc,                                " +
    "         mif                           mf                                  " +
    " where   bt.batch_id               = ?                                     " +
    "         and mfc.merchant_number   = bt.merchant_number                    " +
    "         and mf.merchant_number    = bt.merchant_number                    " +
    "         and mb.bank_number        = bt.bank_number                        ";


  // query to extract one tran for PG
  private   static final String   qExtractTranPG = 
    " select  /*+ ordered */                                                    " + crlf +
    "         tapi.merchant_number            as merchant_number,               " + crlf +
    "         tapi.dba_name                   as dba_name,                      " + crlf +
    "         tapi.dba_city                   as dba_city,                      " + crlf +
    "         tapi.dba_state                  as dba_state,                     " + crlf +
    "         substr(tapi.dba_zip,1,5)        as dba_zip,                       " + crlf +
    "         lpad(nvl(auth.merchant_category_code,                             " + crlf +
    "                  tapi.sic_code),4,'0')  as sic_code,                      " + crlf +
    "         nvl(tapi.country_code,                                            " + crlf +
    "             decode(tapi.dba_state,                                        " + crlf +
    "                    'PR','PR',                                             " + crlf +
    "                    'VI','VI',                                             " + crlf +
    "                    'US'))               as country_code,                  " + crlf +
    "         nvl(tapi.currency_code,'840')   as currency_code,                 " + crlf +
    "         tapi.phone_number               as phone_number,                  " + crlf +
    "         tapi.dm_contact_info            as dm_contact_info,               " + crlf +
    "   /**** transaction data ****/                                            " + crlf +
    "         tapi.card_number                as card_number,                   " + crlf +
    "         tapi.card_number_enc			  as card_number_enc,  				" + crlf +
    "         tapi.card_type                  as card_type,                     " + crlf +
    "         tapi.debit_credit_indicator     as debit_credit_indicator,        " + crlf +
    "         decode(tapi.transaction_type,'M','Y','N')                         " + crlf +
    "                                         as cash_disbursement,             " + crlf +
    "         case when tapi.transaction_date <= auth.transaction_time_local    " + crlf +
    "              then trunc(auth.transaction_time_local)                      " + crlf +
    "              else tapi.transaction_date end                               " + crlf +
    "                                         as transaction_date,              " + crlf +
    "         case when tapi.transaction_date <= auth.transaction_time_local    " + crlf +
    "              then auth.transaction_time_local                             " + crlf +
    "              else tapi.transaction_date end                               " + crlf +
    "                                         as transaction_time,              " + crlf +
    "         tapi.rec_id                     as batch_record_id,               " + crlf +
    "         tapi.transaction_amount         as transaction_amount,            " + crlf +
    "         tapi.reference_number           as acq_reference_number,          " + crlf +
    "         tapi.pos_entry_mode             as pos_entry_mode,                " + crlf +
    "         tapi.pos_data_code              as pos_data_code,                 " + crlf +
    "         tapi.reject_reason              as reject_reason,                 " + crlf +
    "   /**** auth data ****/                                                   " + crlf +
    "         tapi.auth_response_code         as auth_response_code,            " + crlf +
    "         tapi.auth_ref_num               as auth_retrieval_ref_num,        " + crlf +
    "         tapi.auth_tran_id               as auth_tran_id,                  " + crlf +
    "         tapi.auth_val_code              as auth_val_code,                 " + crlf +
    "         tapi.auth_code                  as auth_code,                     " + crlf +
    "         tapi.auth_source_code           as auth_source_code,              " + crlf +
    "         tapi.auth_avs_response          as auth_avs_response,             " + crlf +
    "         tapi.auth_amount                as auth_amount,                   " + crlf +
    "         tapi.auth_amount                as auth_amount_total,             " + crlf +
    "		  nvl(auth.transaction_time, tapi.transaction_date) as auth_date,  " + crlf +
    "		  nvl(auth.transaction_time, tapi.transaction_date)	as auth_time,   " + crlf +
    "         trunc(auth.transaction_time_local) as auth_date_local,            " + crlf +
    "         auth.transaction_time_local     as auth_time_local,               " + crlf +
    "         auth.time_zone                  as auth_time_zone,                " + crlf +
    "         tapi.auth_returned_aci          as auth_returned_aci,             " + crlf +
    "         tapi.avs_zip                    as auth_avs_zip,                  " + crlf +
    "         lpad(auth.currency_code,3,'0')  as auth_currency_code,            " + crlf +
    "         auth.cvv2_result                as auth_cvv2_response,            " + crlf +
    "         auth.fee_program_indicator      as auth_fpi,                      " + crlf +
    "         tapi.auth_rec_id                as auth_rec_id,                   " + crlf +
    "         tapi.trident_tran_id            as trident_tran_id,               " + crlf +
    "         tapi.client_reference_number    as client_reference_number,       " + crlf +
    "         tapi.debit_network_id           as debit_network_id,              " + crlf +
    "         auth.spend_qualified_ind        as spend_qualified_ind,           " + crlf +
    "         auth.token_assurance_level      as token_assurance_level,         " + crlf +
    "         auth.iso_ecommerce_indicator    as iso_ecommerce_indicator,       " + crlf +
    "         auth.pos_condition_code         as pos_condition_code,            " + crlf +
    "   /**** level II data ****/                                               " + crlf +
    "         tapi.tax_amount                 as tax_amount,                    " + crlf +
    "         tapi.purchase_id                as purchase_id,                   " + crlf +
    "         decode(tapi.card_type, 'MC', tapi.purchase_id,                    " + crlf +
    "         'VS',tapi.purchase_id, null)     as customer_code,                " + crlf + 
    "         decode(auth.market_specific_data_indicator,                       " + crlf +
    "                'A',3,  'H',4,  1)       as purchase_id_format,            " + crlf +
    "         tapi.requester_name             as requester_name,                " + crlf +
    "         tapi.name_of_place              as carrier_name,                  " + crlf +
    "         tapi.ship_to_zip                as ship_to_zip,                   " + crlf +
    "         tapi.shipping_amount            as shipping_amount,               " + crlf +
    "         decode( nvl(tapi.card_present,'N'),                               " + crlf +
    "                 'Y','1', /* signature */                                  " + crlf +
    "                 '4' ) /* moto */            as cardholder_id_method,      " + crlf +
    "         ('22' || tapi.ucaf_collection_ind)  as ecomm_security_level_ind,  " + crlf + 
    "         nvl(tapi.reimburse_attribute,'0')   as reimbursement_attribute,   " + crlf +
    "         tapi.cardholder_reference_number    as cardholder_reference_number," + crlf +
    "         nvl(tapi.card_present,'N')          as card_present,              " + crlf +
    "         null                                as special_conditions_ind,    " + crlf +
    "         tapi.statement_date_begin           as statement_date_begin,      " + crlf +
    "         tapi.statement_date_end             as statement_date_end,        " + crlf +
    "         case when (tapi.statement_date_end   is null or                   " + crlf +
    "                    tapi.statement_date_begin is null)                   then null " + crlf +
    "              when (tapi.statement_date_end = tapi.statement_date_begin) then 1    " + crlf +
    "              else (tapi.statement_date_end - tapi.statement_date_begin) end       " + crlf +
    "                                             as hotel_rental_days,         " + crlf +
    "         tapi.statement_city_begin           as statement_city_begin,      " + crlf +
    "         tapi.statement_city_end             as statement_city_end,        " + crlf +
    "         tapi.statement_region_begin         as statement_region_begin,    " + crlf +
    "         tapi.statement_region_end           as statement_region_end,      " + crlf +
    "         tapi.statement_country_begin        as statement_country_begin,   " + crlf +
    "         tapi.statement_country_end          as statement_country_end,     " + crlf +
    "         tapi.rate_daily                     as rate_daily,                " + crlf +
    "         nvl(tapi.moto_ecommerce_ind,'1')    as moto_ecommerce_ind,        " + crlf +
    "         auth.product_level_result_code      as product_id,                " + crlf +
    "         auth.market_specific_data_indicator as market_specific_data_ind,  " + crlf +
    "         nvl(tapi.level_iii_data_present,'N')as level_iii_data_present,    " + crlf +
    "   /**** recurring transaction data ****/                                  " + crlf +
    "         tapi.recurring_payment_ind          as recurring_payment_ind,     " + crlf +
    "         tapi.recurring_payment_number       as multiple_clearing_seq_num, " + crlf +
    "         tapi.recurring_payment_count        as multiple_clearing_seq_count," + crlf +
    "         null                                as information_indicator,     " + crlf +
    "         null                                as merchant_vol_indicator,    " + crlf +
    "         null                                as enhanced_data_1,           " + crlf +
    "         null                                as enhanced_data_2,           " + crlf +
    "         null                                as enhanced_data_3,           " + crlf +
    "         tapi.card_number_enc                as card_number_enc,           " + crlf +
    "         auth.device_code                	  as device_code,           	" + crlf +
    "         null                                as ecommerce_goods_indicator, " + crlf +
    "         auth.pos_environment                as pos_environment,           " + crlf +
    "         auth.disc_tran_data_cond_code       as track_condition_code,      " + crlf +
    "         tapi.transaction_type               as transaction_type,          " + crlf +
    "         authExt.tic                         as tic,                       " + crlf +
    "         authExt.mc_pos_entry_mode           as mc_pos_entry_mode,		    " + crlf +
    "         authExt.mc_pos_data                 as mc_pos_data,               " + crlf +
    "         authExt.is_partial_auth_enabled     as is_partial_auth_enabled    " + crlf +
    " from    trident_capture_api     tapi,                                     " + crlf +
    "         tc33_trident            auth,                                     " + crlf +
    "         tc33_trident_ext        authExt                                   " + crlf +
    " where   tapi.rec_id = ?                                                   " + crlf +
    "         and tapi.transaction_type in                                      " + crlf +
    "         (                                                                 " + crlf +
    "           select  tt.tran_type                                            " + crlf +
    "           from    trident_capture_api_tran_type tt                        " + crlf +
    "           where   nvl(tt.settle,'N') = 'Y'                                " + crlf +
    "         )                                                                 " + crlf +
    "         and tapi.test_flag = 'N'  /* only prod batches */                 " + crlf +
    "         and auth.trident_transaction_id(+) =tapi.trident_tran_id          " + crlf +
    "         and auth.trident_transaction_id = authExt.trident_transaction_id(+)";

  /**
   * RS Software:: Modified for EMV implementation,added icc_data,icc_data2,card_sequence_number
   */
  // query to extract one tran for Vital
  private   static final String   qExtractTranVital = 
    " select  /*+ index (dt PK_DDF_256_DT) */                                   " + crlf +
    "         dt.merchant_number                    as merchant_number,         " + crlf +
    "         dt.merch_dba_name                     as dba_name,                " + crlf +
    "         dt.merch_city                         as dba_city,                " + crlf +
    "         dt.merch_state                        as dba_state,               " + crlf +
    "         lpad(dt.merch_zip,5,'0')              as dba_zip,                 " + crlf +
    "         lpad(dt.mcc,4,'0')                    as sic_code,                " + crlf +
    "         dt.merch_country_code                 as country_code,            " + crlf +
    "   /**** transaction data ****/                                            " + crlf +
    "         dt.card_number                        as card_number,             " + crlf +
    "         dt.card_number_enc 					as card_number_enc,    		" + crlf +
    "         dt.card_type                          as card_type,               " + crlf +
    "         dt.debit_credit_indicator             as debit_credit_indicator,  " + crlf +
    "         decode(dt.transaction_code,102,'Y','N')                           " + crlf +
    "                                               as cash_disbursement,       " + crlf +
    "         dt.tran_date                          as transaction_date,        " + crlf +
    "         dt.btc_seq_num                        as batch_id,                " + crlf +
    "         dt.fin_seq_num                        as batch_record_id,         " + crlf +
    "         dt.tran_amount                        as transaction_amount,      " + crlf +
    "         nvl(dt.auth_currency_code,'840')      as currency_code,           " + crlf +
    "         dt.acquirer_ref_num                   as acq_reference_number,    " + crlf +
    "         dt.pos_entry_mode                     as pos_entry_mode,          " + crlf +
    "         null                                  as reject_reason,           " + crlf +
    "   /**** auth data ****/                                                   " + crlf +
    "         dt.auth_response_code                 as auth_response_code,      " + crlf +
    "         null                                  as auth_retrieval_ref_num,  " + crlf +
    "         (                                                                 " + crlf +
    "           decode(dt.card_type,'MC',substr(dt.auth_date_text,1,4),'') ||   " + crlf +
    "           dt.tran_id                                                      " + crlf +
    "         )                                     as auth_tran_id,            " + crlf +
    "         dt.tran_id                            as network_tran_id,          " + crlf +
    "         dt.val_code                           as auth_val_code,           " + crlf +
    "         dt.auth_code                          as auth_code,               " + crlf +
    "         (                                                                 " + crlf +
    "           decode(dt.card_type,'VS',                                       " + crlf +
    "             decode(dt.transaction_code,101,                               " + crlf +
    "               nvl(dt.auth_source_code,'-'),''),'') || dt.auth_source_code " + crlf +
    "         )                                     as auth_source_code,        " + crlf +
    "         dt.auth_amount                        as auth_amount,             " + crlf +
    "         dt.auth_date                          as auth_date,               " + crlf +
    "         dt.aci                                as auth_returned_aci,       " + crlf +
    "         dt.auth_currency_code                 as auth_currency_code,      " + crlf +
    "         dt.debit_net_id                       as debit_network_id,        " + crlf +
    "         dt.cardholder_id_method               as cardholder_id_method,    " + crlf +
    "         nvl(dt.reimbursement_attr_code,'0')   as reimbursement_attribute, " + crlf +
    "         decode( nvl(dt.moto_ec_indicator,' '),                            " + crlf +
    "                 ' ','Y',  -- blank                                        " + crlf +
    "                 'N')                          as card_present,            " + crlf +
    "         dt.merch_tran_indicator               as special_conditions_ind,  " + crlf +
    "         dt.moto_ec_indicator                  as moto_ecommerce_ind,      " + crlf +
    "         substr(dt.tran_date_expansion,1,1)    as market_specific_data_ind," + crlf +
    "         null                                  as level_iii_data_present,  " + crlf +
    "         dt.recur_pay_indicator                as recurring_payment_ind,   " + crlf +
    "         dt.card_number_enc                    as card_number_enc,         " + crlf +
    "         dt.passenger_name                     as passenger_name,          " + crlf +
    "   /**** system data ****/                                                 " + crlf +
    "         load_filename_to_load_file_id(dt.load_filename) as load_file_id,  " + crlf +
    "         dt.load_filename                      as load_filename,           " + crlf +
    "         dt.icc_data                           as icc_data,                " + crlf + //Added for EMV
    "         dt.icc_data2                          as icc_data_2,              " + crlf + //Added for EMV
    "         dt.card_sequence_number               as card_sequence_number,    " + crlf + //Added for EMV
    "         dt.auth_date_expansion                as tic		                " + crlf +
    " from    daily_detail_file_d256_dt     dt                                  " + crlf +
    " where   dt.batch_date           = ?                                       " + crlf +
    "         and dt.merchant_number  = ?                                       " + crlf +
    "         and dt.fin_seq_num      = ?                                       " + crlf +
    "         and dt.tran_amount     != 0                                       ";

  // query to extract one tran for Cielo
  private   static final String   qExtractTranCielo = 
    " select  /*+ index (dt IDX_DDF_CIELO_BID_BRID) */                            " + crlf + 
    "         dt.merchant_number                    as merchant_number,           " + crlf +
    "         dt.sic_code                           as sic_code,                  " + crlf +
    "         dt.catid                              as pos_terminal_id,           " + crlf +
    "         dt.pos_term_cap                       as pos_term_cap,              " + crlf +
    "         dt.bin_number                         as bin_number,                " + crlf +
    "   /**** transaction data ****/                                              " + crlf +
    "         dt.batch_id                           as batch_id,                  " + crlf +
    "         dt.batch_record_id                    as batch_record_id,           " + crlf +
    "         dt.card_number                        as card_number,               " + crlf +
    "         dt.card_number_enc                    as card_number_enc,           " + crlf +
   // "     dukpt_decrypt_wrapper(dt.card_number_enc) as card_number_full,          " + crlf +
    "         dt.card_exp_date                      as exp_date,                  " + crlf +
    "         dt.service_code                       as service_code,              " + crlf +
    "         dt.card_type                          as card_type,                 " + crlf +
    "         lpad(dt.card_association,3,'0')       as cielo_card_association,    " + crlf +
    "         dt.cielo_product                      as cielo_product,             " + crlf +
    "         dt.tran_code                          as cielo_tran_type,           " + crlf +
    "         dt.settlement_days                    as settlement_days,           " + crlf +
    "         dt.debit_credit_indicator             as debit_credit_indicator,    " + crlf +
    "         'N'                                   as cash_disbursement,         " + crlf +
    "         dt.transaction_date                   as transaction_date,          " + crlf +
    "         case when dt.transaction_date <> dt.auth_date                       " + crlf +
    "              then dt.transaction_date                                       " + crlf +
    "              else dt.auth_time          end   as transaction_time,          " + crlf +
    "         decode(dt.clearing_method,                                          " + crlf +
    "                  '2', dt.transaction_amount-                                " + crlf +
    "                         (trunc(dt.transaction_amount/                       " + crlf +
    "                                 dt.installment_count,2)*                    " + crlf +
    "                           (dt.installment_count-1)),                        " + crlf +
    "                  dt.transaction_amount)       as transaction_amount,        " + crlf +
    "         decode(dt.clearing_method,                                          " + crlf +
    "                  '2',    trunc(dt.transaction_amount/                       " + crlf +
    "                                 dt.installment_count,2),                    " + crlf +
    "                  null)                        as installment_amount,        " + crlf +
    "         decode(dt.clearing_method,                                          " + crlf +
    "                  '2',   case when dt.installment_count > 6                  " + crlf +
    "                              then 'CR2'                                     " + crlf +  // 7-12 parcelado loja (credito)
    "                              else 'CR1' end,                                " + crlf +  // 2- 6 parcelado loja (credito)
    "                         case when dt.load_filename like 'cddf__d%'          " + crlf +
    "                              then 'DB0'                                     " + crlf +  //  not parcelado loja (debito)
    "                              else 'CR0' end)  as dbcr_installtype,          " + crlf +  //  not parcelado loja (credito)
    "         dt.cashback_amount                    as cashback_amount,           " + crlf +
    "         decode( dt.card_association,                                        " + crlf +
    "                 9, ( substr(dt.catid,1,2) || substr(dt.catid,5) ||          " + crlf +  // diners = first 2 and last 4 of catid
    "                      lpad(mod(dt.tran_seq_num,1000000),6,'0') ),            " + crlf +  //          plus last 6 of tran seq num
    "                 null )                        as clearing_nsu,              " + crlf +
//  "         dt.departure_tax                      as tax_amount,                " + crlf +
    "         dt.currency_code                      as currency_code,             " + crlf +
    "         dt.acq_reference_number               as acq_reference_number,      " + crlf +
    "         dt.pos_entry_mode                     as pos_entry_mode,            " + crlf +
    "         dt.pos_data_code                      as pos_data_code,             " + crlf +
    "         dt.chip_condition_code                as chip_condition_code,       " + crlf +
    "         nvl(dt.vs_icc_data,dt.mc_icc_data)    as icc_data,                  " + crlf +  // if vs_icc is null,then take mc_icc (whether null or not)
    "   /**** auth data ****/                                                     " + crlf +
    "         0                                     as auth_rec_id,               " + crlf +  // because link impossible, set to 0 (not null)
    "         dt.auth_response_code                 as auth_response_code,        " + crlf +
    "         dt.auth_retrieval_ref_num             as auth_retrieval_ref_num,    " + crlf +
    "         dt.auth_tran_id                       as auth_tran_id,              " + crlf +
    "         dt.auth_val_code                      as auth_val_code,             " + crlf +
    "         dt.auth_code                          as auth_code,                 " + crlf +
    "         dt.auth_source_code                   as auth_source_code,          " + crlf +  // will change null to E or 9 later
    "         dt.auth_avs_response                  as auth_avs_response,         " + crlf +
    "         dt.auth_amount                        as auth_amount,               " + crlf +
    "         dt.auth_amount                        as auth_amount_total,         " + crlf +
    "         dt.auth_date                          as auth_date,                 " + crlf +
    "         dt.auth_time                          as auth_time,                 " + crlf +
    "         dt.auth_returned_aci                  as auth_returned_aci,         " + crlf +
    "         dt.currency_code                      as auth_currency_code,        " + crlf +
    "         dt.cvv2_response_code                 as auth_cvv2_response,        " + crlf +
    "         dt.market_specific_data_ind           as market_specific_data_ind,  " + crlf +
    "         '0'                                   as reimbursement_attribute,   " + crlf +
    "         'ME'                                  as debit_network_id,          " + crlf +  // either Maestro or not pin debit
    "   /**** level II data ****/                                                 " + crlf +
    "         dt.order_number                       as purchase_id,               " + crlf +
    "         dt.ecommerce_tid                      as client_reference_number,   " + crlf +
    "         dt.invoice_number                     as customer_code,             " + crlf +
    "         dt.cardholder_id_method               as cardholder_id_method,      " + crlf +
    "         decode(dt.ecommerce_indicator,                                      " + crlf +
    "                  null,'Y','N')                as card_present,              " + crlf +
    "         dt.cnp_indicator                      as cielo_cnp_indicator,       " + crlf +
    "         dt.cat_indicator                      as cat_indicator,             " + crlf +
    "         dt.technology_type                    as technology_type,           " + crlf +
    "         dt.ecomm_security_level_ind           as ecomm_security_level_ind,  " + crlf +
    "         dt.ecommerce_indicator                as moto_ecommerce_ind,        " + crlf +
    "         dt.recurring_indicator                as recurring_payment_ind,     " + crlf +
    "         decode(dt.clearing_method,                                          " + crlf +
    "                  '1',dt.installment_count,                                  " + crlf +
    "                  decode(dt.installment_count,                               " + crlf +
    "                           null,null,1))       as multiple_clearing_seq_num, " + crlf +
    "         decode(dt.clearing_method,                                          " + crlf +
    "                  '1',dt.installment_count+50,                               " + crlf +
    "                  dt.installment_count)        as multiple_clearing_seq_count" + crlf +
    " from    daily_detail_file_cielo_dt    dt                                    " + crlf +
    " where   dt.batch_id = ?                                                     " + crlf +
    "         and dt.batch_record_id = ?                                          ";

  private static final String EXTRACT_VISA_NON_CPS_PROGRAM_QUERY =
    " Select                                                                                               " + crlf +
    "    class_list                                     as    class_list,                                  " + crlf +
    "    pos_entry_mode                                 as    pos_entry_mode,                              " + crlf +
    "    card_holder_id_method                          as    card_holder_id_method,                       " + crlf +
    "    ecommerce_moto_indicator                       as    ecommerce_moto_indicator,                    " + crlf +
    "    auth_characteristics_ind                       as    auth_characteristics_ind,                    " + crlf +
    "    auth_characteristics_ind_db                    as    auth_characteristics_ind_db,                 " + crlf +
    "    reimbursement_attribute                        as    reimbursement_attribute,                     " + crlf +
    "    req_pymnt_service_non_auto                     as    req_pymnt_service_non_auto,                  " + crlf +
    "    req_pymnt_service_auto                         as    req_pymnt_service_auto,                      " + crlf +
    "    general_retail                                 as    general_retail,                              " + crlf +
    "    card_not_present_ecommerce                     as    card_not_present_ecommerce,                  " + crlf +
    "    large_ticket                                   as    large_ticket,                                " + crlf +
    "    large_ticket_gsa                               as    large_ticket_gsa,                            " + crlf +
    "    fleet                                          as    fleet,                                       " + crlf +
    "    level_x_data_ok                                as    level_x_data_ok,                             " + crlf +
    "    timeliness_auth                                as    timeliness_auth,                             " + crlf +
    "    timeliness_clear                               as    timeliness_clear                             ";


  private static final String EXTRACT_VISA_FPI =
    " Select                                                                             " + crlf +
    "     class_list                                  as    class_list,                  " + crlf +
    "     fpi_cnsmr_prepaid                           as    fpi_cnsmr_prepaid,           " + crlf +
    "     fpi_cnsmr_debit                             as    fpi_cnsmr_debit,             " + crlf +
    "     fpi_cnsmr_credit                            as    fpi_cnsmr_credit,            " + crlf +
    "     fpi_cnsmr_rewards                           as    fpi_cnsmr_rewards,           " + crlf +
    "     fpi_cnsmr_signature                         as    fpi_cnsmr_signature,         " + crlf +
    "     fpi_cnsmr_sign_pref                         as    fpi_cnsmr_sign_pref,         " + crlf +
    "     fpi_cnsmr_hi_net                            as    fpi_cnsmr_hi_net,            " + crlf +
    "     fpi_comm_bus_debit                          as    fpi_comm_bus_debit,          " + crlf +
    "     fpi_comm_bus_t1                             as    fpi_comm_bus_t1,             " + crlf +
    "     fpi_comm_bus_sig_t3                         as    fpi_comm_bus_sig_t3,         " + crlf +
    "     fpi_comm_bus_enh_t2                         as    fpi_comm_bus_enh_t2,         " + crlf +
    "     fpi_comm_corp                               as    fpi_comm_corp,               " + crlf +
    "     fpi_comm_purch                              as    fpi_comm_purch,              " + crlf +
    "     fpi_comm_prepaid                            as    fpi_comm_prepaid,            " + crlf +
    "     fpi_comm_bus_t4                             as    fpi_comm_bus_t4,             " + crlf +
    "     fpi_comm_bus_t5                             as    fpi_comm_bus_t5              " + crlf +
    " from               visa_fpi ";



  private static final String EXTRACT_VISA_CPS_PROGRAM_QUERY =
          String.format("%s, eligible_sic_codes as eligible_sic_codes, pidf_list as pidf_list, aci_list as aci_list  from  visa_ic_class_map "
                  , EXTRACT_VISA_NON_CPS_PROGRAM_QUERY);

  private static final String EXTRACT_VISA_SIC_CODE_GROUP =
    " Select                                                                               " + crlf +
    "    group_name                                     as    group_name,                  " + crlf +
    "    sic_codes                                      as    sic_codes                   " + crlf +
    "from     visa_sic_code_group        ";




  // map of Vital extension record format to the flat file record definition
  private static final HashMap  CdfExtRecToFfdType  =
    new HashMap()
    {
      {
        put("CARNT"     , MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC);
        put("OPTIN_A"   , MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC_OPT);
        put("DIRCT"     , MesFlatFiles.DEF_TYPE_CDF_DIRECT_MARKET_REC);
        put("FLEET"     , MesFlatFiles.DEF_TYPE_CDF_FLEET1_REC);
        put("FLEE2"     , MesFlatFiles.DEF_TYPE_CDF_FLEET2_REC);
        put("LODGE_VS"  , MesFlatFiles.DEF_TYPE_CDF_VISA_HOTEL_REC);
        put("LODGE_MC"  , MesFlatFiles.DEF_TYPE_CDF_MC_HOTEL_REC);
        put("LODGE_AM"  , MesFlatFiles.DEF_TYPE_CDF_AMEX_HOTEL_REC);
        put("OPTIN_H"   , MesFlatFiles.DEF_TYPE_CDF_HOTEL_REC_OPT);
        put("MERCH"     , MesFlatFiles.DEF_TYPE_CDF_MERCH_DESC_REC);
        put("AMEXR"     , MesFlatFiles.DEF_TYPE_CDF_REST_REC);
        put("RETAL"     , MesFlatFiles.DEF_TYPE_CDF_RETAIL_REC);
        put("TEMP1"     , MesFlatFiles.DEF_TYPE_CDF_TEMP_HELP1_REC);
        put("TEMP2"     , MesFlatFiles.DEF_TYPE_CDF_TEMP_HELP2_REC);
        put("EXT**"     , MesFlatFiles.DEF_TYPE_CDF_GEN_REC);
        put("EXTDD"     , MesFlatFiles.DEF_TYPE_CDF_DIRECT_DELIVERY_REC);
        put("PURC1_VS"  , MesFlatFiles.DEF_TYPE_CDF_VISA_PURC1_REC);
        put("PURC1_MC"  , MesFlatFiles.DEF_TYPE_CDF_MC_PURC1_REC);
        put("PURC1_AM"  , MesFlatFiles.DEF_TYPE_CDF_VISA_PURC1_REC);
        put("PURC2_VS"  , MesFlatFiles.DEF_TYPE_CDF_VISA_PURC2_REC);
        put("PURC2_MC"  , MesFlatFiles.DEF_TYPE_CDF_MC_PURC2_REC);
        put("PURC2_AM"  , MesFlatFiles.DEF_TYPE_CDF_MC_PURC2_REC);
        put("SHIP1_VS"  , MesFlatFiles.DEF_TYPE_CDF_VISA_SHIP1_REC);
        put("SHIP2_VS"  , MesFlatFiles.DEF_TYPE_CDF_VISA_SHIP2_REC);
        put("SHIP1_MC"  , MesFlatFiles.DEF_TYPE_CDF_MC_SHIP1_REC);
        put("SHIP2_MC"  , MesFlatFiles.DEF_TYPE_CDF_MC_SHIP2_REC);
      }
    };
  
  // map of SettlementRecord field names to the value description query
  private   HashMap   FieldNameToDescLookupSql    = null;
  
  public SettlementDb( )
  {
    // build the field decriptor lookup table
    FieldNameToDescLookupSql = new HashMap();
    //--------------------------------------------------------------------------------------
    //                                field name              description lookup SQL
    //--------------------------------------------------------------------------------------
    FieldNameToDescLookupSql.put( "fee_program_indicator",  qFeeProgramIndicatorDesc );
    FieldNameToDescLookupSql.put( "ic_rate_designator",     qIcRateDesignatorDesc );
    FieldNameToDescLookupSql.put( "pos_entry_mode",         qPosEntryModeDesc );
  }

  // returns the prefix each settlement record type uses with various settlement tables
  //    e.g.... xxx_settlement, xxx_settlement_line_items, settlement_excluded_dates.xxx, xxx_settlement_fee_collect
  private String getTablePrefix( int recType )
  {
    String tablePrefix = null;
    switch( recType )
    {
      case SettlementRecord.SETTLE_REC_VISA   : tablePrefix =     "visa"; break;
      case SettlementRecord.SETTLE_REC_MC     : tablePrefix =       "mc"; break;
      case SettlementRecord.SETTLE_REC_AMEX   : tablePrefix =     "amex"; break;
      case SettlementRecord.SETTLE_REC_DISC   : tablePrefix = "discover"; break;
      case SettlementRecord.SETTLE_REC_MODBII : tablePrefix =   "modbii"; break;
    }
    return( tablePrefix );
  }

  private long getSettlementRecId()
    throws Exception
  {
    PreparedStatement   ps          = null;
    long                recId       = 0L;
    
    try
    {
      connect();
      
      String sqlText = 
        " select  settlement_sequence.nextval   as rec_id " + crlf +
        " from    dual                                    ";
       
      ps = getPreparedStatement(sqlText);
      ResultSet rs = ps.executeQuery();
      rs.next();
      recId = rs.getLong("rec_id");
      rs.close();
    } 
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }     
    return( recId );
  }
  

  
  public static double calculateTax( double totalAmount, String zip1, String zip2, String zip3 )
  {
    return( new SettlementDb()._calculateTax(totalAmount,zip1,zip2,zip3) );
  }
  
  public double _calculateTax( double totalAmount, String zip1, String zip2, String zip3 )
  {
    PreparedStatement   ps          = null;
    double              taxAmount   = 0.0;

    try
    {
      connect();

      // calculate the tax using the online tax tables
      String sqlText = 
        " select  sales_tax_rate(substr(?,1,5)  ,'no-city'      ) as tax1, " + crlf +
        "         sales_tax_rate(substr(?,1,5)  ,'no-city'      ) as tax2, " + crlf +
        "         sales_tax_rate(substr(?,1,5)  ,'no-city'      ) as tax3  " + crlf +
        " from dual ";

      ps = getPreparedStatement(sqlText);
      ps.setString(1,zip1);
      ps.setString(2,zip2);
      ps.setString(3,zip3);
      ResultSet rs = ps.executeQuery();
      if( rs.next() )
      {
        double                taxRate   = rs.getDouble("tax1");
        if( taxRate == 0.00 ) taxRate   = rs.getDouble("tax2");
        if( taxRate == 0.00 ) taxRate   = rs.getDouble("tax3");

        taxAmount = MesMath.round(totalAmount - (totalAmount/(taxRate + 1)) , 2);
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("calculateTax(" + totalAmount + "," + zip1 + "," + zip2 + "," + zip3 + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( taxAmount );
  }
  
  public static String decodeDebitNetworkID( String posNetworkID )
  {
    return( new SettlementDb()._decodeDebitNetworkID(posNetworkID) );
  }
  
  public String _decodeDebitNetworkID( String posNetworkID )
  {
    PreparedStatement   ps          = null;
    ResultSet           resultSet   = null;
    String              retVal      = "DB";
    
    try
    {
      connect(true);
    
      ps = getPreparedStatement("select nvl(d256_value,'DB')as d256 from trident_debit_networks where pos_network_id = '" + posNetworkID + "'");
      resultSet = ps.executeQuery();
      if ( resultSet.next() ) 
      {
        retVal = resultSet.getString("d256");
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("_decodeDebitNetworkID(" + posNetworkID +")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch(Exception ee) {}
      cleanUp();
    }      
    return( retVal );
  }

  public static Date decodeJulianDate( String yddd )
  {
    return( new SettlementDb()._decodeJulianDate(yddd) );
  }
  
  public Date _decodeJulianDate( String yddd )
  {
    PreparedStatement   ps          = null;
    ResultSet           resultSet   = null;
    Date                retVal      = null;
    
    try
    {
      connect(true);
    
      ps = getPreparedStatement("select yddd_to_date(?) as julian_date from dual");
      ps.setString(1,yddd);
      resultSet = ps.executeQuery();
      if ( resultSet.next() ) 
      {
        retVal = resultSet.getDate("julian_date");
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("_decodeJulianDate(" + yddd +")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch(Exception ee) {}
      cleanUp();
    }      
    return( retVal );
  }

  public static String decodeVisakValue( String fmt, int fieldType, String visakValue )
  {
    return( decodeVisakValue(fmt,fieldType,visakValue,null) );
  }

  public static String decodeVisakValue( String fmt, int fieldType, String visakValue, String defaultValue )
  {
    return( new SettlementDb()._decodeVisakValue(fmt,fieldType,visakValue,defaultValue) );
  }

  public String _decodeVisakValue( String fmt, int fieldType, String visakValue, String defaultValue )
  {
    PreparedStatement   ps          = null;
    String              retVal      = defaultValue;
    String              sqlText     = null;
    
    try
    {
      connect(true);
      
      if ( fmt.equals("D256") || fmt.equals("VISA") )
      {
        sqlText = " select  fm.d256_value     as issuer_value ";
      }
      else if ( fmt.equals("MC") )
      {
//@        sqlText = " select  fm.mc_value       as issuer_value ";
        sqlText = " select  fm.d256_value     as issuer_value ";
      }
      else if ( fmt.equals("AMEX") )
      {
        sqlText = " select  fm.amex_value     as issuer_value ";
      }
      sqlText +=
        " from    visak_d256_field_map fm                           " +
        " where   fm.field_type = ? and                             " +
        "         nvl(fm.visak_value,'_space_') = nvl(?,'_space_')  ";
      
      ps = getPreparedStatement(sqlText);
      ps.setInt(1,fieldType);
      if ( visakValue == null || visakValue.equals("") )
      {
        ps.setNull(2,java.sql.Types.VARCHAR);
      }
      else
      {
        ps.setString(2,visakValue);
      }        
      ResultSet rs = ps.executeQuery();
      if(rs.next()) {
    	  retVal = rs.getString("issuer_value");
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("decodeVisakValue(" +fmt + "," + fieldType + ",'" + visakValue + "')", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static IcInfo getBestIcInfo( SettlementRecord tranInfo, String cardType, int icLvlLen, java.util.Date procDate, String fpi, String getBestIcInfoClause )
    throws Exception
  {
    return( new SettlementDb()._getBestIcInfo(tranInfo,cardType,icLvlLen,procDate,fpi,getBestIcInfoClause) );
  }
  
  public IcInfo _getBestIcInfo( SettlementRecord tranInfo, String cardType, int icLvlLen, java.util.Date procDate, String fpiList, String getBestIcInfoClause )
  {
    PreparedStatement ps          = null;
    IcInfo            retVal      = null;
    
    try
    {
      connect(true);

      if ( fpiList != null && fpiList.trim().length() >= (SettlementRecord.NUM_ENHANCED_IND+icLvlLen) )
      {
        String[]  fpis        = fpiList.split(",");
        String    strAmount   = tranInfo.getString("transaction_amount") + " * " + tranInfo.getString("fx_rate", "1.0");
        String    strCTE      = tranInfo.getCardTypeEnhanced();
        String    strCurr     = tranInfo.getString("currency_code");
        String    strEFlags   = tranInfo.getString("eligibility_flags");
        String    strSicCode  = tranInfo.getString("sic_code");
        String    strSicGroup = tranInfo.getString("sic_group");
        String    strProcessingCode = tranInfo.getString("processing_code");
        boolean   foundSub    = false;
        boolean   foundEnh    = false;
        
        StringBuffer sqlText = new StringBuffer();
        for( int loop = 0; loop < 2; ++ loop )    // first submitted, then enhanced
        {
          sqlText.append( "(                                                                    " + crlf );
          sqlText.append( " select  replace(icd.issuer_ic_level,'_','.')  as ic_level,          " + crlf );
          sqlText.append( "         icd.ic_code                           as ic_code,           " + crlf );
          sqlText.append( "         icd.reg_ic_code                       as reg_ic_code,       " + crlf );
          sqlText.append( "         '" + loop + "'                        as submitted_enhanced," + crlf );
          sqlText.append( "         (( " + strAmount + " * icd.ic_rate * 0.01) +                " + crlf );
          sqlText.append( "          icd.ic_per_item)                     as fee_amount,        " + crlf );
          sqlText.append( "         icd.eligible_sic_codes                as eligible_sic_codes " + crlf );
          sqlText.append( " from    daily_detail_file_ic_desc   icd                             " + crlf );
          sqlText.append( " where   card_type = '" + cardType + "'                              " + crlf );
          sqlText.append( "         and ? between icd.valid_date_begin and icd.valid_date_end   " + crlf );
          sqlText.append( "         and ( icd.eligible_card_types is null or                    " + crlf );
          sqlText.append( "               instr(icd.eligible_card_types,'" + strCTE + "') > 0)  " + crlf );
          sqlText.append( "         and ( icd.eligibility_flags is null or                      " + crlf );
          sqlText.append( "               instr(icd.eligibility_flags,'" + strEFlags + "') > 0) " + crlf );
          sqlText.append( "         and '" +strCurr+"' = nvl(icd.currency_code,'" +strCurr+ "') " + crlf );
          sqlText.append( "         and " + strAmount + " >= nvl(icd.amt_min," + strAmount + ") " + crlf );
          sqlText.append( "         and " + strAmount + " <= nvl(icd.amt_max," + strAmount + ") " + crlf );
          sqlText.append( "         and '" +strProcessingCode+"' = nvl(icd.processing_code,'" +strProcessingCode+ "') " + crlf );
          sqlText.append(           getBestIcInfoClause                                           + crlf );
          sqlText.append( "         and " + crlf );
          sqlText.append( "         (   " + crlf );

          for( int i = 0, j = 0; i < fpis.length; ++i )
          {
            if( SettlementRecord.DEF_ENHANCED_IND.equals(fpis[i].substring(0,SettlementRecord.NUM_ENHANCED_IND)) )
            {
              foundSub = true;
              if( loop != 0 )continue;
            }
            else
            {
              foundEnh = true;
              if( loop == 0 )continue;
            }
            sqlText.append("           ");
            if ( j > 0 ) { sqlText.append("or "); }
            ++j;
            sqlText.append( "'" + fpis[i].substring(SettlementRecord.NUM_ENHANCED_IND,SettlementRecord.NUM_ENHANCED_IND+icLvlLen) + "'" );
            sqlText.append( " like icd.issuer_ic_level" + crlf );
          }
          sqlText.append( "         ) " + crlf);
          sqlText.append( ") " + crlf );
          if( loop == 0 )
          {
            // if there aren't any "enhanced" potentials, then stop with this first half
            if( !foundEnh )break;

            sqlText.append( "union" + crlf );     // append "union"
            if( !foundSub )sqlText.setLength(0);  // if did not find any "submitted" potentials, then clear the first half
          }
        }
        sqlText.append( " order by fee_amount, submitted_enhanced " );
        
        // Hack put in as both PSL - Ecommerce and PSL - CNP had the same exact pricing structure leading to the CNP ICPs getting picked each time  
        if(cardType != null && "DS".equals(cardType)) {
          sqlText.append(", ic_level desc");
        }

	log.debug("Query to determine IRD:\n" + sqlText.toString());
        ps = getPreparedStatement(sqlText.toString());
                                    ps.setDate  (1,new java.sql.Date(procDate.getTime()));
        if( foundSub && foundEnh )  ps.setDate  (2,new java.sql.Date(procDate.getTime()));
        ResultSet rs = ps.executeQuery();
      
        foundEnh  = false;    // have we found an eligible item that is enhanced?
        retVal    = new IcInfo();
        while ( rs.next() )
        {
          if ( SettlementTools.isEligibleItem( true, strSicCode, strSicGroup, rs.getString("eligible_sic_codes") ) )
          {
            // is this particular item submitted?
            foundSub = "0".equals( rs.getString("submitted_enhanced") );
            
            // if have not already found & saved enhanced fpi data, save this fpi data (whether submitted or enhanced)
            if( !foundEnh )
            {
              String  bestFpi = rs.getString("ic_level");

              // save the entire fpi+ string for the first fpi that has a matching fpi & submitted/enhanced value
              for( int i = 0; i < fpis.length; ++i )
              {
                String  fpi = fpis[i].substring(SettlementRecord.NUM_ENHANCED_IND,SettlementRecord.NUM_ENHANCED_IND+icLvlLen);
                if( fpi != null && fpi.matches(bestFpi) )
                {
                  boolean isSubmitted = SettlementRecord.DEF_ENHANCED_IND.equals(fpis[i].substring(0,SettlementRecord.NUM_ENHANCED_IND));
                  if( isSubmitted == foundSub )
                  {
                    retVal.setIcString( fpis[i] );    // all ic info from string
                    break;
                  }
                }
              }
              retVal.setIcLevel( bestFpi );
              retVal.setIcCode( rs.getString("ic_code") );
              retVal.setIcExpense( rs.getDouble("fee_amount") );
            }

            // if this is submitted, save now and stop looking
            if( foundSub )
            {
              retVal.setIcCodeBilling( rs.getString("ic_code") );
              retVal.setIcCodeRegulated( rs.getString("reg_ic_code") );
              break;
            }

            // this is enhanced; keep looking, but don't save enhanced data again
            foundEnh        = true;
          }
        }
        rs.close();
      }
    }
    catch( Exception e )
    { 
      logEntry("_getBestIcInfo(" + cardType + "," + DateTimeFormatter.getFormattedDate(procDate,"MM/dd/yyyy") + "," + fpiList + "," + tranInfo.getString("transaction_amount") + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static long getExtractSequenceNextVal()
    throws Exception
  {
    return( new SettlementDb()._getExtractSequenceNextVal() );
  }
  
  public long _getExtractSequenceNextVal()
    throws Exception
  {
    PreparedStatement ps          = null;
    long              retVal      = 0L;
    
    try
    {
      ps = getPreparedStatement("select extract_sequence.nextval as seq_num from dual");
      ResultSet rs = ps.executeQuery();
      rs.next();
      retVal = rs.getLong("seq_num");
      rs.close();
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }          
  
  public static String getFieldValueDesc( String fname, String value )
  {
    return( new SettlementDb()._getFieldValueDesc(fname,value) );
  }
  
  public String _getFieldValueDesc( String fname, String value )
  {
    PreparedStatement   ps      = null;
    String              retVal  = null;
    
    try
    {
      connect(true);
      
      if ( FieldNameToDescLookupSql.containsKey(fname) )
      {
        ps = getPreparedStatement( (String)FieldNameToDescLookupSql.get(fname) );
        ps.setString(1,value);
        ResultSet rs = ps.executeQuery();
        if( rs.next() ) { retVal = rs.getString(1); }
        rs.close();
      }
    }
    catch( Exception e )
    {
      logEntry("_getFieldValueDesc(" + fname + "," + value + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }  

  public static int getFileBankNumber( String loadFilename )
  {
    return( new SettlementDb()._getFileBankNumber(loadFilename) );
  }
  
  public int _getFileBankNumber( String loadFilename )
  {
    PreparedStatement   ps                  = null;
    int                 retVal              = 0;

    try
    {
      connect(true);
      
      String sqlText = 
        " select  lft.file_prefix           as prefix,                  " +
        "         length(lft.file_prefix)   as prefix_length            " +
        " from    mes_load_file_types     lft                           " +
        " where   substr(?,1,length(lft.file_prefix)) = lft.file_prefix " +
        " order by length(lft.file_prefix) desc                         ";
     
      ps = getPreparedStatement(sqlText);
      ps.setString(1,loadFilename);
      ResultSet rs = ps.executeQuery();

      if ( rs.next() )
      {
        int prefixLength = rs.getInt("prefix_length");
        retVal = Integer.parseInt(loadFilename.substring(prefixLength,(prefixLength+4)));
      }
      rs.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("_getFileBankNumber(" + loadFilename + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ) {}
      cleanUp();
    }
    return( retVal );
  }  
  
  public static java.util.Date getFileDate( String loadFilename )
  {
    return( new SettlementDb()._getFileDate(loadFilename) );
  }
  
  public java.util.Date _getFileDate( String loadFilename )
  {
    int                 prefixLength        = 0;
    ResultSet           resultSet           = null;
    java.util.Date      retVal              = null;
    PreparedStatement   ps                  = null;

    try
    {
      String sqlText = 
        " select  lft.file_prefix           as prefix,        " + crlf +
        "         length(lft.file_prefix)   as prefix_length  " + crlf +
        " from    mes_load_file_types     lft                 " + crlf +
        " order by prefix_length desc                         ";
        
      ps = getPreparedStatement(sqlText);
      resultSet = ps.executeQuery();

      while( resultSet.next() )
      {
        prefixLength = resultSet.getInt("prefix_length");
        if ( loadFilename.substring(0,prefixLength).equals( resultSet.getString("prefix") ) )
        {
          retVal = DateTimeFormatter.parseDate(loadFilename.substring((prefixLength+5),(prefixLength+5+6)),"MMddyy");
          break;
        }
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }
  
  public static SettlementRecord getNewSettlementRecord( String cardType )
    throws Exception
  {
    return( new SettlementDb()._getNewSettlementRecord(cardType) );
  }
  
  public SettlementRecord _getNewSettlementRecord( String cardType )
    throws Exception
  {
    SettlementRecord    rec             = null;
    
    try
    {
      // create a new settlement record to hold the transaction data
            if ( "VS".equals(cardType) )    { rec = new SettlementRecordVisa();           }
      else  if ( "MC".equals(cardType) )    { rec = new SettlementRecordMC();             }
      else  if ( "AM".equals(cardType) )    { rec = new SettlementRecordAmex();           }
      else  if ( "DS".equals(cardType) )    { rec = new SettlementRecordDiscover();       }
      else  if ( "JC".equals(cardType) )    { rec = new SettlementRecordDiscover();       }
      else  if ( "B2".equals(cardType) )    { rec = new SettlementRecordModBII();         }
      else  if ( "VP".equals(cardType) || "MP".equals(cardType) )    { rec = null;         }
      else                                  { rec = new SettlementRecordReport();         }
    }
    finally
    {
    }
    return( rec );
  }
  
  
  public static int getNonSettlementDayCount( int recType, Calendar beginCal, Calendar endCal )
    throws Exception
  {
    return( new SettlementDb()._getNonSettlementDayCount(recType,beginCal,endCal,NSF_NONE) );
  }
  
  public static int getNonSettlementDayCount( int recType, Calendar beginCal, Calendar endCal, int nsFilterType )
    throws Exception
  {
    return( new SettlementDb()._getNonSettlementDayCount(recType,beginCal,endCal,nsFilterType) );
  }
  
  public int _getNonSettlementDayCount( int recType, Calendar beginCal, Calendar endCal, int nsFilterType )
    throws Exception
  {
    Date    beginDate = new java.sql.Date(beginCal.getTime().getTime());
    Date    endDate   = new java.sql.Date(endCal.getTime().getTime());
    
    return( _getNonSettlementDayCount(recType,beginDate,endDate,nsFilterType) );
  }
  
  public int _getNonSettlementDayCount( int recType, Date beginDate, Date endDate )
    throws Exception
  {
    return( _getNonSettlementDayCount(recType,beginDate,endDate,NSF_NONE) );
  }
  
  public int _getNonSettlementDayCount( int recType, Date beginDate, Date endDate, int nsFilterType )
    throws Exception
  {
    PreparedStatement   ps              = null;
    int                 retVal          = 0;
    
    try
    {
      connect(true);

      String  tablePrefix = getTablePrefix( recType );
      if( tablePrefix != null )
      {
        String sqlText = 
          " select  count(1)    as rec_count      " +
          " from    settlement_excluded_dates     " +
          " where   excluded_date between ? and ? " +
          "         and (                         " ;
        
        // Because (sunday = 'SUN' or null) and (tablePrefix = 'HOL' or null),
        // can just check for null or not null for the sunday/holiday filters.
        switch( nsFilterType )
        {
          case NSF_SUNDAYS_ONLY:          sqlText +=          "sunday is not null     "; break;
          case NSF_HOLIDAYS_ONLY:         sqlText +=  tablePrefix + " is not null     "; break;
//        case NSF_SUNDAYS_AND_HOLIDAYS:
//        case NSF_NONE:
          default:                        sqlText +=          "sunday is not null or  "
                                                  +   tablePrefix + " is not null     "; break;
        }
        sqlText +=  " )" ;

        ps = getPreparedStatement(sqlText);
        ps.setDate(1,beginDate);
        ps.setDate(2,endDate);
        ResultSet rs = ps.executeQuery();        
        rs.next();
        retVal = rs.getInt("rec_count");
        rs.close();
      }        
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static String getBusinessServiceArrangement( String cardNumber, String cpi, String binNumber )
  {
    return( new SettlementDb()._getBusinessServiceArrangement(cardNumber,cpi,binNumber) );
  }
  
  public String _getBusinessServiceArrangement( String cardNumber, String cpi, String binNumber )
  {
    PreparedStatement   ps          = null;
    String              retVal      = null;
  
    try
    {
      connect();
    
      String sqlText = 
        " select  (t90.bsa_type || t90.bsa_id_code)   as bsa                " + crlf +
        " from    mc_ep_ip0091t1    t91,                                    " + crlf +
        "         mc_ep_ip0090t1    t90                                     " + crlf +
        " where   t91.acquirer_bin = ?                                      " + crlf +
        "         and t91.card_program_id = ?                               " + crlf +
        "         and t90.card_program_id = t91.card_program_id             " + crlf +
        "         and t90.bsa_type = t91.bsa_type                           " + crlf +
        "         and t90.bsa_id_code = t91.bsa_id_code                     " + crlf +
        "         and t90.bsa_type_priority = t91.bsa_type_priority         " + crlf +
        "         and rpad(?,19,'0') between t90.issuer_account_range_low   " + crlf +
        "                                and t90.issuer_account_range_high  " + crlf +
        " order by t90.card_program_id_priority                             ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,binNumber);
      ps.setString(2,cpi);
      ps.setString(3,cardNumber);
      ResultSet rs = ps.executeQuery();        
     
      if ( rs.next() )
      {
        retVal = rs.getString("bsa");
      }      
      rs.close();
    }        
    catch( Exception e )
    {
      logEntry("_getBusinessServiceArrangement()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static boolean isBatchExtracted( long batchId )
  {
    return( new SettlementDb()._isBatchExtracted(batchId) );
  }
  
  public boolean _isBatchExtracted( long batchId )
  {
    PreparedStatement   ps          = null;
    int                 recCount    = 0;
    boolean             retVal      = false;
    ResultSet           rs          = null;
    String              sqlText     = null;
    
    try
    {
      connect(true);
     
      // for( 0...10 ) -- this range should include all SettlementRecord.SETTLE_REC_xxx's
      for( int recType = 0; recType < 10; ++recType )
      {
        String  tablePrefix = getTablePrefix( recType );
        if( tablePrefix != null )
        {
          sqlText = "select count(1) from " + tablePrefix + "_settlement  where batch_id = ?";
          ps = getPreparedStatement(sqlText);
        
          ps.setLong(1,batchId);
          rs = ps.executeQuery();
          rs.next();
          recCount = rs.getInt(1);
          rs.close();
          ps.close();
        
          if ( recCount != 0 )
          {
            retVal = true;
            break;
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("_isBatchExtracted(" + batchId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static String loadFileIdToLoadFilename( long loadFileId )
  {
    return( new SettlementDb()._loadFileIdToLoadFilename(loadFileId) );
  }
  
  public String _loadFileIdToLoadFilename( long loadFileId )
  {
    CallableStatement   cs              = null;
    String              loadFilename    = null;
    PreparedStatement   ps              = null;

    try
    {
      connect(true);
      
      String sqlText = 
        " select  load_file_id_to_load_filename(?)  as load_filename  " + 
        " from    dual                                                ";
        
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,loadFileId);
      ResultSet rs = ps.executeQuery();
      rs.next();
      loadFilename = rs.getString("load_filename");
      rs.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("_loadFileIdToLoadFilename( " + loadFileId + " )", e.toString() );
    }
    finally
    {
      try{ cs.close(); } catch( Exception ee ){}
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( loadFilename );
  }
  
  public static long loadFilenameToLoadFileId( String loadFilename )
  {
    return( new SettlementDb()._loadFilenameToLoadFileId(loadFilename) );
  }
  
  public long _loadFilenameToLoadFileId( String loadFilename )
  {
    CallableStatement   cs              = null;
    long                loadFileId      = 0L;
    PreparedStatement   ps              = null;

    try
    {
      connect(true);
      
      cs = getCallableStatement("{ call load_file_index_init(?) }");
      cs.setString( 1, loadFilename );
      cs.execute();
      
      String sqlText = 
        " select  load_filename_to_load_file_id(?)  as load_file_id   " + 
        " from    dual                                                ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,loadFilename);
      ResultSet rs = ps.executeQuery();
      rs.next();
      loadFileId = rs.getLong("load_file_id");
      rs.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("_loadFilenameToLoadFileId( " + loadFilename + " )", e.toString() );
    }
    finally
    {
      try{ cs.close(); } catch( Exception ee ){}
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( loadFileId );
  }
  
  public static void updateIcCatDowngrade( SettlementRecord rec )
  {
    new SettlementDb()._updateIcCatDowngrade(rec);
  }
  
  public void _updateIcCatDowngrade( SettlementRecord rec )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String  tablePrefix = getTablePrefix( rec.RecSettleRecType );
      if( tablePrefix != null )
      {
        String sqlText = 
          " update  " + tablePrefix + "_settlement  " + crlf + 
          " set     ic_cat_downgrade = ?            " + crlf +
          " where   rec_id = ?                      " ;

        ps = getPreparedStatement(sqlText);
        ps.setString( 1, rec.getString("ic_cat_downgrade") );
        ps.setLong  ( 2, rec.getLong("rec_id") );
        ps.executeUpdate();
      }        
    }
    catch( Exception e )
    {
      logEntry("_updateIcCatDowngrade()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static String loadAirlineCarrierCodes( String ticketingCode )
  {
    return( new SettlementDb()._loadAirlineCarrierCodes(ticketingCode) );
  }
  
  public String _loadAirlineCarrierCodes( String ticketingCode )
  {
    PreparedStatement     ps          = null;
    String                retVal      = ",SERVICE FEE";
    
    try
    {
      connect(true);
      
      String sqlText = 
        " select         nvl(acc.carrier_code2,'')                    as c_code," + crlf +
        "         substr(nvl(acc.carrier_name ,'SERVICE FEE'),1,25)   as c_name " + crlf +
        " from    airline_carrier_codes                                    acc  " + crlf +
        " where   acc.ticketing_code = " + ticketingCode                            ;
                        
      ps = getPreparedStatement(sqlText);
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = rs.getString("c_code") + "," + rs.getString("c_name");
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadAirlineCarrierCodes("+ticketingCode+")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static AlmGraduationData loadAlmGraduationData( String almCode, String licProdId )
  {
    return( new SettlementDb()._loadAlmGraduationData(almCode,licProdId) );
  }
  
  public AlmGraduationData _loadAlmGraduationData( String almCode, String licProdId )
  {
    PreparedStatement     ps          = null;
    AlmGraduationData     retVal      = null;
  
    try
    {
      connect(true);
    
      String sqlText = 
        " select  substr(epd.raw_data,29,3)   as product_class,   " + crlf +
        "         substr(epd.raw_data,26,3)   as gcms_product_id  " + crlf +
        " from    mc_ep_table_data  epd                           " + crlf +
        " where   epd.table_id = 'IP0018T1'                       " + crlf +
        "         and substr(epd.raw_data,20,1) = ?               " + crlf +
        "         and substr(epd.raw_data,21,3) = ?               ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,almCode);        
      ps.setString(2,licProdId);        
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = new AlmGraduationData(rs);
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadAlmGraduationData(" + almCode + "," + licProdId + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static HashMap loadCieloProducts( long merchantId )
  {
    return( new SettlementDb()._loadCieloProducts(merchantId) );
  }
  
  public HashMap _loadCieloProducts( long merchantId )
  {
    PreparedStatement     ps                = null;
    HashMap               retVal            = new HashMap();
    
    try
    {
      connect(true);
      
      String sqlText = 
          " select mcp.cielo_product                as cielo_product,   " + crlf + 
          "        nvl(mcp.installment_count_min,1) as installment_min, " + crlf + 
          "        nvl(mcp.installment_count_max,1) as installment_max  " + crlf + 
          " from   mif_cielo_products    mcp                            " + crlf +
          " where  mcp.merchant_number = ?                              ";
          
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,merchantId);
      ResultSet rs = ps.executeQuery();
      
      while ( rs.next() )
      {
        String            productId   = rs.getString("cielo_product");
        CieloProductData  productData = (CieloProductData)retVal.get(productId);
        if ( productData == null )
        {
          productData = new CieloProductData(rs);
        }
        else
        {
          productData.setInstallmentRange(rs);
        }
        retVal.put(productId,productData);
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadCieloProducts("+merchantId+")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }


  public static Map<String, VisaProgram> loadCpsVisaProgramMap( )
  {
  	return( new SettlementDb()._loadCpsVisaProgramMap() );
  }
  public static Map<String, VisaProgram> loadNonCpsVisaProgramMap( )
  {
    return( new SettlementDb()._loadNonCpsVisaProgramMap() );
  }

  public static Table<String, Fpi.CardProduct, String> loadFpiProductTable( )
  {
    return( new SettlementDb()._loadFpiProductTable() );
  }
  public static Map<Fpi.SicCodeGroup, String> loadSicCodeGroupMap() {
   return  new SettlementDb()._loadSicCodeGroupMap();
  }

  private Map<String, VisaProgram> _loadNonCpsVisaProgramMap() {
    Map<String, VisaProgram> retVal = new HashMap<String, VisaProgram>();
    String sqlText = String.format("%s from   visa_ic_non_cps_class_map  ", EXTRACT_VISA_NON_CPS_PROGRAM_QUERY);
    try (Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
            PreparedStatement stmt = con.prepareStatement(sqlText);
            ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        VisaProgram program = buildProgramFromResultSet(rs, false);
        retVal.put(program.getName(), program);
      }
    }
    catch (Exception e) {
      logEntry("_loadNonCpsVisaPrograms", e.toString());
    }
    return retVal;
  }

  private Map<String, VisaProgram> _loadCpsVisaProgramMap() {
    Map<String, VisaProgram> retVal = new HashMap<String, VisaProgram>();
    try (Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
            PreparedStatement stmt = con.prepareStatement(EXTRACT_VISA_CPS_PROGRAM_QUERY);
            ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        VisaProgram program = buildProgramFromResultSet(rs, true);
        retVal.put(program.getName(), program);
      }
    }
    catch (Exception e) {
      logEntry("_loadNonCpsVisaPrograms", e.toString());
    }
    return retVal;
  }

  private Table<String, Fpi.CardProduct, String> _loadFpiProductTable() {
    Table<String, Fpi.CardProduct, String> visaFpiTable = HashBasedTable.create();
    try (Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
            PreparedStatement stmt = con.prepareStatement(EXTRACT_VISA_FPI);
            ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        for (Fpi.CardProduct cardProduct : Fpi.CardProduct.values()) {
          if (StringUtils.isNotEmpty(rs.getString(cardProduct.getValue()))) {
            visaFpiTable.put(StringUtils.trim(rs.getString("class_list")), cardProduct, rs.getString(cardProduct.getValue()));
          }
        }
      }
    }
    catch (Exception e) {
      logEntry("_loadFpiProductList", e.toString());
    }
    return visaFpiTable;
  }

  private Map<Fpi.SicCodeGroup, String> _loadSicCodeGroupMap() {
    Map<Fpi.SicCodeGroup, String> retVal = new HashMap<Fpi.SicCodeGroup, String>();
    try (Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
            PreparedStatement stmt = con.prepareStatement(EXTRACT_VISA_SIC_CODE_GROUP);
            ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        retVal.put(Fpi.SicCodeGroup.valueOf(rs.getString("group_name")), rs.getString("sic_codes"));
      }
    }
    catch (Exception e) {
      logEntry("_loadSicCodeGroupMap", e.toString());
    }
    return retVal;
  }

  private VisaProgram buildProgramFromResultSet(ResultSet rs,boolean cpsFlag) throws SQLException {
    String eligibleSicCodes = cpsFlag ? rs.getString("eligible_sic_codes"):"" ;
    String aciList = cpsFlag ? rs.getString("aci_list"): "";
    String pidfList = cpsFlag ? rs.getString("pidf_list") : "";
    return VisaProgram.builder().withClassList((rs.getString("class_list")))
            .withPosEntryMode(rs.getString("pos_entry_mode"))
            .withCardHolderIdMethod(rs.getString("card_holder_id_method"))
            .withEcommerceMoto(rs.getString("ecommerce_moto_indicator"))
            .withAuthorizationCharIndicator(rs.getString("auth_characteristics_ind"))
            .withAuthorizationCharIndicatorDB(rs.getString("auth_characteristics_ind_db"))
            .withReimbursementAttribute((rs.getString("reimbursement_attribute")))
            .withRequestedPaymentServiceNonAuto((rs.getString("req_pymnt_service_non_auto")))
            .withRequestedPaymentServiceAuto((rs.getString("req_pymnt_service_auto")))
            .withGeneralRetail((rs.getString("general_retail")))
            .withCardNotPresentEcomm((rs.getString("card_not_present_ecommerce")))
            .withLargeTicket((rs.getString("large_ticket")))
            .withLargeTicketGsa((rs.getString("large_ticket_gsa")))
            .withFleet((rs.getString("fleet")))
            .withLevelXDataOk((rs.getString("level_x_data_ok")))
            .withTimelinessAuth((rs.getInt("timeliness_auth")))
            .withTimelinessClear((rs.getInt("timeliness_clear")))
            .withEligibleSicCodes(eligibleSicCodes)
            .withAciList(aciList)
            .withPurchaseIdFormatList(pidfList)
            .build();
  }







  
  public static List loadIrdListMC( SettlementRecord rec )
  {
    return( new SettlementDb()._loadIrdListMC(rec) );
  }
  
  public List _loadIrdListMC( SettlementRecord rec )
  {
    PreparedStatement ps          = null;
    List              retVal      = new ArrayList();
    
  
    try
    {
      connect(true);
      
      String sqlText = 
        " select  rul.* ,                                                           " + crlf +
        "         ird.*                                                             " + crlf +
        " from    mc_super_ird              ird,                                    " + crlf +
        "         mc_super_ird_sic_codes    irds,                                   " + crlf +
        "         mc_ic_program_rules       rul                                     " + crlf +
        " where   ird.card_program_id = ?                                           " + crlf +
        "         and ird.bsa_type = ?                                              " + crlf +
        "         and ird.bsa_id_code = ?                                           " + crlf +
        "         and ird.processing_code = ?                                       " + crlf +
        "         and ird.product_type_id in ( 3, ? )                               " + crlf +
        "         and                                                               " + crlf + 
        "         (                                                                 " + crlf + 
        "           ird.eligible_gcms_product_ids                                   " + crlf + 
        "               like '%' || nvl( ? ,'none') || '%'                          " + crlf + 
        "         )                                                                 " + crlf + 
        "         and                                                               " + crlf +
        "         (                                                                 " + crlf +
        "           ird.reqd_alm_code is null                                       " + crlf +
        "           or ird.reqd_alm_code like '%' || nvl( ? ,'Z') || '%'            " + crlf +
        "         )                                                                 " + crlf +
        "         and irds.rec_id = ird.rec_id                                      " + crlf +
        "         and irds.sic_code = ?                                             " + crlf +
        "         and rul.ird = ird.ird                                             " + crlf +
        "         and rul.duplicate_of_ird is null                                  " + crlf +
        "         and ( rul.card_type is null                                       " + crlf + 
        "               or rul.card_type = ? )                                      " + crlf +
        "         and ( rul.bsa_arrangement is null                                 " + crlf + 
        "               or ? like rul.bsa_arrangement )                             " + crlf +
        "         and ( rul.eligibility_flag is null                                " + crlf + 
        "               or instr(?, rul.eligibility_flag) > 0 )                     " + crlf +
        "         and ( rul.program_reg_id_mask is null                             " + crlf +
        "               or ? like rul.program_reg_id_mask )                         ";
      
      // adjust the ALM code if necessary  
      String almCode = rec.getData("alm_code");
      if ( "P".equals(almCode) )
      {
        almCode = "Z";
      }

      log.debug("Loading IRD list for MasterCard using:");
      log.debug("card_program_id: '" + rec.getData("card_program_id") + "'");
      log.debug("bsa_type: '" + rec.getData("bsa_type") + "'");
      log.debug("bsa_id_code: '" + rec.getData("bsa_id_code") + "'");
      log.debug("processing_code: '" + rec.getData("processing_code") + "'");
      log.debug("gcms_product_id: '" + rec.getData("gcms_product_id") + "'");
      log.debug("alm code: '" + almCode + "'");
      log.debug("sic_code: '" + rec.getData("sic_code") + "'");
      log.debug("eligibility_flags: '" + rec.getData("eligibility_flags") + "'");
      log.debug("program_registration_id: '" + rec.getData("program_registration_id") + "'");
        
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setString(++idx,rec.getData("card_program_id"));        
      ps.setString(++idx,rec.getData("bsa_type"));        
      ps.setString(++idx,rec.getData("bsa_id_code"));
      ps.setString(++idx,rec.getData("processing_code"));
      ps.setInt   (++idx,"MB".equals(rec.getData("card_type")) ? 2 : 1 );
      ps.setString(++idx,rec.getData("gcms_product_id"));
      ps.setString(++idx,almCode);
      ps.setString(++idx,rec.getData("sic_code"));
      ps.setString(++idx,rec.getData("card_type"));
      ps.setString(++idx,rec.getData("bsa_type") + rec.getData("bsa_id_code"));
      ps.setString(++idx,rec.getData("eligibility_flags"));
      ps.setString(++idx,rec.getData("program_registration_id"));
      ResultSet rs = ps.executeQuery();
      while ( rs.next() )
      {
        IcProgramRulesMC ruleSet = new IcProgramRulesMC();
        ruleSet.setRuleData(rs);
        retVal.add(ruleSet);
        
        // if the IRD value is 34 US Domestic, force the system to use this value only (bsa arrangement should not be null for ird 34)
        if ( "34".equals(ruleSet.getIrd()) && rs.getString("bsa_arrangement").equals("2010001") )
        {
          retVal.clear();
          retVal.add(ruleSet);
          break;
        }
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadIrdListMC(" + rec.getData("batch_id") + "," + rec.getData("batch_record_id") + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static List loadIcpListDiscover( SettlementRecord rec )
  {
    return( new SettlementDb()._loadIcpListDiscover(rec) );
  }
  
  public List _loadIcpListDiscover( SettlementRecord rec )
  {
    PreparedStatement ps          = null;
    List              retVal      = new ArrayList();
    
  
    try
    {
      connect(true);
      
      String icpColumn    = rec.getData("icp_column");
     // String icpColumnReg = ( rec.getInt("debit_type") == 0 ) ? icpColumn : (icpColumn + "_reg");
      String icpColumnReg ;
      switch ( rec.getInt("debit_type")){
	      case 0:   icpColumnReg = icpColumn ;						
	      			break;
	      case 1:  icpColumnReg = icpColumn + "_reg";				//Non Exempt
	      			break;
	      case 2:  icpColumnReg = icpColumn + "_reg_wo_fraud";		//Non Exempt without Fraud
					break;
		  default: icpColumnReg = icpColumn + "_reg";
      }
      if( icpColumn != null && icpColumn.length() > 0 )
      {
        String cpCNP    = (rec.getData("card_present"  ));
        String eMode    = (rec.getData("pos_entry_mode")+"00").substring(0,2);
        String pCode    = (rec.getData("process_code"  )+"00").substring(0,2);
        String condCode = (rec.getData("track_condition_code")+"00").substring(0,2);
        String tAmt     = (rec.getData("transaction_amount"));
        String motoEcommerceInd  = rec.getData("moto_ecommerce_ind");

        String sqlText  = 
          " select  rul.icp_" + icpColumn     + "   as icp_code                   , " + crlf +
          "         rul.icp_" + icpColumnReg  + "   as icp_code_reg               , " + crlf +
          "         nvl(rul.amt_tol       , 'N')    as amount_tolerance           , " + crlf +
          "         nvl(rul.auth          , 'N')    as auth_reqd                  , " + crlf +
          "         nvl(rul.timeliness    ,  0 )    as timeliness_days            , " + crlf +
          "         rul.eligible_sic_codes          as eligible_sic_codes         , " + crlf +
          "         rul.pos_entry                   as pos_entry                    " + crlf +
          " from    discover_ic_program_rules       rul                             " + crlf +
          " where   rul.icp_" + icpColumn + "       is not null                     " + crlf +
          // "     and nvl(rul.pos_entry   ,'" + eMode + "') like '%" + eMode + "%'    " + crlf +
          "     and nvl(rul.card_present,'" + cpCNP + "') like '%" + cpCNP + "%'    " + crlf +
          "     and nvl(rul.proc_code   ,'" + pCode + "') like '%" + pCode + "%'    " + crlf +
          "     and nvl(rul.tran_data_cond_code,'" + condCode + "') like '%" + condCode + "%'    " + crlf +
          "     and nvl(rul.amt_min     , " + tAmt  + " ) <= " + tAmt      + "      " + crlf +
          "     and nvl(rul.amt_max     , " + tAmt  + " ) >= " + tAmt      + "      " + crlf;
        
        if(motoEcommerceInd!=null && !"".equals(motoEcommerceInd))
          sqlText  += "     and nvl(rul.moto_ecomm_code     , '" + motoEcommerceInd  + "') like '%" + motoEcommerceInd + "%'    ";
      
        ps = getPreparedStatement(sqlText);
        ResultSet rs = ps.executeQuery();

        String sicCode  = rec.getData("sic_code");
        String dataCondCode = rec.getData("track_condition_code");
        while ( rs.next() )
        {
          // if the sic is either included or not excluded, then include the classname in the retVal
          if ( SettlementTools.isEligibleItem(  true, sicCode, null, rs.getString("eligible_sic_codes") ) && SettlementTools.isEligibleItem(true, eMode, null, rs.getString("pos_entry")))
          {
            IcProgramRulesDiscover ruleSet = new IcProgramRulesDiscover();
            ruleSet.setRuleData(rs);
            retVal.add(ruleSet);
          }
        }
        rs.close();
      }
    }
    catch( Exception e )
    {
      logEntry("_loadIcpListDiscover(" + rec.getData("batch_id") + "," + rec.getData("batch_record_id") + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static String loadSicCode( long merchantId )
  {
    return( new SettlementDb()._loadSicCode(merchantId) );
  }
  
  public String _loadSicCode( long merchantId )
  {
    PreparedStatement     ps          = null;
    String                retVal      = "";
    
    try
    {
      connect(true);
      
      String sqlText = 
         " select lpad(mf.sic_code,4,'0') as sic_code " +
         " from   mif   mf                            " +
         " where  mf.merchant_number = ?              ";
      
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,merchantId);
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = rs.getString("sic_code");
      }
      rs.close();
    }
    catch( Exception e )
    {
      // invalid SIC, just return ""
      logEntry("_loadSicCode("+merchantId+")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static HashMap loadMerchantAchFlags( long merchantId )
  {
    return( new SettlementDb()._loadMerchantAchFlags(merchantId) );
  }
  
  public HashMap _loadMerchantAchFlags( long merchantId )
  {
    PreparedStatement     ps          = null;
    HashMap               retVal      = new HashMap();
    
    try
    {
      connect(true);
      
      String[][] fieldMap = new String[][]
        {
          { "VB" , "vs_flag" },
          { "VD" , "vs_flag" },
          { "VS" , "vs_flag" },
          { "V$" , "v$_flag" },
          { "MB" , "mc_flag" },
          { "MD" , "mc_flag" },
          { "MC" , "mc_flag" },
          { "M$" , "m$_flag" },
          { "AM" , "am_flag" },
          { "DS" , "ds_flag" },
          { "PL" , "pl_flag" },
          { "DB" , "db_flag" },
          { "EB" , "db_flag" },
        };
      
      String sqlText = 
         " select substr(nvl(mf.visa_plan           ,'NN'),1,1) as vs_flag, " + crlf + 
         "        substr(nvl(mf.visa_cash_plan      ,'NN'),1,1) as v$_flag, " + crlf + 
         "        substr(nvl(mf.mastcd_plan         ,'NN'),1,1) as mc_flag, " + crlf + 
         "        substr(nvl(mf.mastcd_cash_plan    ,'NN'),1,1) as m$_flag, " + crlf + 
         "        substr(nvl(mf.amex_plan           ,'NN'),1,1) as am_flag, " + crlf + 
         "        substr(nvl(mf.discover_plan       ,'NN'),1,1) as ds_flag, " + crlf + 
         "        substr(nvl(mf.private_label_1_plan,'NN'),1,1) as pl_flag, " + crlf + 
         "        substr(nvl(mf.debit_plan          ,'NN'),1,1) as db_flag  " + crlf + 
         " from   mif   mf                                                  " +
         " where  mf.merchant_number = ?                                    ";
      
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,merchantId);
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        for( int i = 0; i < fieldMap.length; ++i )
        {
          retVal.put( fieldMap[i][0], rs.getString(fieldMap[i][1]) );
        }
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadMerchantAchFlags("+merchantId+")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static TridentAuthData loadAuthDataTrident(String tridentTranId)
  {
    return( new SettlementDb()._loadAuthDataTrident(tridentTranId) );
  }
  
  public TridentAuthData _loadAuthDataTrident(String tridentTranId)
  {
    TridentAuthData   authData    = null;
    PreparedStatement ps          = null;
    
    try
    {
      connect(true);
      String sqlText = 
          qTridentAuthData__visak                                     + crlf +
        "        ,trident_auth    tauth                             " + crlf +
        " where   tauth.trident_transaction_id = ?                  " + crlf +
        "         and auth.merchant_number = tauth.merchant_number  " + crlf +
        "         and auth.transaction_date between                 " + crlf +
        "               (tauth.request_timestamp-1) and             " + crlf +
        "               (tauth.request_timestamp+1)                 " + crlf +
        "         and auth.trident_transaction_id = tauth.trident_transaction_id ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,tridentTranId);
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        authData = new TridentAuthData( rs, mesConstants.MBS_BT_VISAK );
        if ( log.isDebugEnabled()) {
        	log.debug("[_loadAuthDataTrident()] - loaded authData from Trident - "+authData);
        }
      }
      rs.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ ps.close(); } catch(Exception ee) {}
      cleanUp();
    }
    return( authData );
  }
   
  public static TridentAuthData loadAuthDataTrident(long recId)
  {
    return( new SettlementDb()._loadAuthDataTrident(recId) );
  }
  
  public TridentAuthData _loadAuthDataTrident(long recId)
  {
    TridentAuthData   authData    = null;
    PreparedStatement ps          = null;
    
    try
    {
      connect(true);
      String sqlText  = qTridentAuthData__visak   +
              " where   tc33.rec_id = ?"
            + " and  tc33.trident_transaction_id = tc33_ext.trident_transaction_id(+) ";
      
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,recId);
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        authData = new TridentAuthData( rs, mesConstants.MBS_BT_VISAK );
        if ( log.isDebugEnabled()) {
      	  log.debug("[_loadAuthDataTrident()] - retrieved auth data for Trident, authData="+authData);
        }
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadAuthDataTrident(" + recId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch(Exception ee) {}
      cleanUp();
    }
    return( authData );
  }
  
  public static TridentAuthData loadAuthDataVital(long merchantId, Date tranDate, int recType, String cardNumber, String authCode, String tranId )
  {
    return( new SettlementDb()._loadAuthDataVital(merchantId,tranDate,recType,cardNumber,authCode,tranId) );
  }
  
	public TridentAuthData _loadAuthDataVital(long merchantId, Date tranDate, int recType, String cardNumber, String authCode, String tranId) {
		int attempt = 1; // default to "no date in tran id"
		TridentAuthData authData = null;
		Date authDateBegin = null;
		Date authDateEnd = null;
		Calendar cal = Calendar.getInstance();
		PreparedStatement ps = null;
		ResultSet rs = null;
		if (log.isDebugEnabled()) {
			log.debug("[_loadAuthDataVital()] - merchantId=" + merchantId + ", tranDate=" + tranDate + ", recType=" + recType + ", cardNumber=" + cardNumber
					+ ", authCode=" + authCode + ", tranId=" + tranId);
		}
		try {
			connect(true);

			if (tranId != null) {
				if (tranId.trim().length() < 12 || "000000000000000,111111111111111".indexOf(tranId) >= 0) {
					tranId = null;
				}
				else {
					switch (recType) {
					case SettlementRecord.SETTLE_REC_VISA:
						attempt = -1; // prepare to ignore bad tran ID
						if (tranId.trim().length() == 15) {
							java.sql.Date sqlDate = _decodeJulianDate(tranId.substring(2, 6));
							if (sqlDate != null) {
								cal.setTime(sqlDate);
								attempt = 0;
							}
						}
						break;

					case SettlementRecord.SETTLE_REC_MC:
						attempt = -1; // prepare to ignore bad tran ID
						String bankNetDate = TridentTools.decodeBankNetDate(tranId);
						if (bankNetDate != null) {
							// apparently starts with a good date (so use it first time through the for() loop),
							// but is not followed by three letter GCMS product id (so don't require a tranId match)
							if (!Character.isLetter(tranId.charAt(4)) || !Character.isLetter(tranId.charAt(5)) || !Character.isLetter(tranId.charAt(6))) {
								tranId = null;
							}

							cal.setTime(DateTimeFormatter.parseDate((bankNetDate + cal.get(Calendar.YEAR)), "MMddyyyy"));
							Calendar cal2 = Calendar.getInstance();
							cal2.add(Calendar.DAY_OF_MONTH, 30);
							if (cal.after(cal2)) {
								cal.add(Calendar.YEAR, -1);
							}
							attempt = 0;
						}
						break;
					}

					switch (attempt) {
					case -1:
						tranId = null;
						attempt = 1; // tran id is invalid
						break;

					case 0:
						cal.add(Calendar.DAY_OF_MONTH, -1);
						authDateBegin = new java.sql.Date(cal.getTime().getTime());
						cal.add(Calendar.DAY_OF_MONTH, 2);
						authDateEnd = new java.sql.Date(cal.getTime().getTime());

						if (authDateBegin == null)
							attempt = 1; // no valid date in tran id
						break;
					}
				}
			}

			String sqlText = qTridentAuthData__vital + " where   merchant_number = ?                               "
					+ "   and   tc33.transaction_date between ? and ?                  " + "   and   card_number = ?                                   "
					+ "   and   authorization_code = ?                            " + "   and   nvl(payment_service_transaction_id,'0') =         "
					+ "           nvl(?,nvl(payment_service_transaction_id,'0'))  "
					+ "   and   nvl(payment_service_transaction_id,'0') = tc33_ext.transaction_identifier(+) "
					+ "   and   tc33.transaction_date = tc33_ext.transaction_date(+) " + " order by transaction_time                                 ";
			if (log.isDebugEnabled()) {
				log.debug("[_loadAuthDataVital()] - SQL: " + sqlText);
			}
			ps = getPreparedStatement(sqlText);

			cal.setTime(tranDate);
			cal.add(Calendar.DAY_OF_MONTH, 2); // add 2 days in case pos device has wrong date.

			// make several attempts to locate the auth
			// 0. tranIdDate-1 => tranIdDate+1 (v/mc only)
			// 1. tranDate-5 => tranDate+2
			// 2. tranDate-15 => tranDate-5
			// 3. tranDate-25 => tranDate-15
			// 4. tranDate-35 => tranDate-25
			for (; attempt < 5 && authData == null; ++attempt) {
				if (attempt != 0) {
					// setup the date range
					authDateEnd = new java.sql.Date(cal.getTime().getTime());
					cal.add(Calendar.DAY_OF_MONTH, -((attempt == 1) ? 7 : 10));
					authDateBegin = new java.sql.Date(cal.getTime().getTime());
				}

				ps.setLong(1, merchantId);
				ps.setDate(2, authDateBegin);
				ps.setDate(3, authDateEnd);
				ps.setString(4, cardNumber);
				ps.setString(5, authCode);
				ps.setString(6, processString(tranId));
				rs = ps.executeQuery();

				if (rs.next()) {
					authData = new TridentAuthData(rs, mesConstants.MBS_BT_VITAL);
					if (log.isDebugEnabled()) {
						log.debug("[_loadAuthDataVital()] - retrieved auth data for TSYS, authData=" + authData);
					}
				}
				rs.close();

				// do not keep looking if have valid date in tran id/banknet info but did not find;
				// we don't always have the adf file before we extract the cdf, so the auth PROBABLY just isn't here.
				if (attempt == 0)
					break;
			}
		}
		catch (Exception e) {
			log.error("[_loadAuthDataVital()] - Exception retriving auth data ", e);
			logEntry("_loadAuthDataVital(" + merchantId + "," + tranDate + "," + tranId + ")", e.toString());
		}
		finally {
			try {
				ps.close();
			}
			catch (Exception ee) {
			}
			cleanUp();
		}
		return (authData);
	}
  
  public static String loadVisaRegionCode( String countryCode )
  {
    return( new SettlementDb()._loadVisaRegionCode(countryCode) );
  }
  
  public String _loadVisaRegionCode( String countryCode )
  {
    PreparedStatement     ps          = null;
    String                retVal      = "";
    
    try
    {
      connect(true);
      
      String sqlText = 
        " select  distinct                            " + crlf +
        "         ard.issuer_country_code,            " + crlf +
        "         ard.issuer_visa_region              " + crlf +
        " from    visa_ardef_table    ard             " + crlf +
        " where   ard.issuer_country_code = ?         " + crlf +
        "         and ard.enabled = 'Y'               " + crlf + 
        "         and nvl(test_indicator,' ') <> 'T'  ";
      
      ps = getPreparedStatement(sqlText);
      ps.setString(1,countryCode);
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = rs.getString("issuer_visa_region");
      }
      rs.close();
    }
    catch( Exception e )
    {
      // invalid SIC, just return ""
      logEntry("_loadVisaRegionCode("+countryCode+")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static Batch loadRawBatchDataVisak( long recId )
  {
    return( new SettlementDb()._loadRawBatchDataVisak(recId) );
  }
  
  public Batch _loadRawBatchDataVisak( long recId )
  {
    Batch                 batch       = null;
    Blob                  blobData    = null;
    byte[]                buffer      = null;
    int                   chunkSize   = 0;
    int                   dataLen     = 0;
    int                   dataRead    = 0;
    InputStream           is          = null;
    PreparedStatement     ps          = null;
    
    
    try
    {
      connect(true);
    
      String sqlText = 
        " select  raw_batch_data      " +
        " from    trident_capture tc  " +
        " where   tc.rec_id = ?       ";
    
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,recId);
      ResultSet rs = ps.executeQuery();
      rs.next();    
      blobData = (Blob)rs.getBlob("raw_batch_data");
      rs.close();
      ps.close();
    
      // use the blob ref to read the statement data into internal storage
      dataLen   = (int)blobData.length();
      chunkSize = 0;
      dataRead  = 0;
      is        = blobData.getBinaryStream();
      buffer    = new byte[dataLen];
    
      // extract the data from the blob object into 
      // a byte buffer in memory
      while (chunkSize != -1)
      {
        chunkSize = is.read(buffer,dataRead,(dataLen - dataRead));
        dataRead += chunkSize;
      }
      is.close();
    
      // now that we have the data in a byte array use
      // the trident library to extract the batch
      batch = BatchPackager.parseBatch(buffer);
    }
    catch( Exception e )
    {
      logEntry("_loadRawBatchDataVisak(" + recId + ")",e.toString());
    }
    finally
    {
      try{ is.close(); }catch( Exception ee ){}
      try{ ps.close(); }catch( Exception ee ){}
      cleanUp();
    }
    
    return( batch );
  }
  
  public static VisakBatchData loadMerchantAndBatchData( long batchId, int batchType )
  {
    return( new SettlementDb()._loadMerchantAndBatchData(batchId,batchType) );
  }
  
  public VisakBatchData _loadMerchantAndBatchData( long batchId, int batchType )
  {
    PreparedStatement   ps            = null;
    ResultSet           resultSet     = null;
    VisakBatchData      batchData     = null;
    
    try
    {
      connect(true);
      
      String sqlText;
      switch( batchType )
      {
        case mesConstants.MBS_BT_VISAK  :
        case mesConstants.MBS_BT_PG     :   sqlText = qVisakBatchData__mbs_batches;   break;
        case mesConstants.MBS_BT_VITAL  :   sqlText = qVisakBatchData__vital;         break;
        case mesConstants.MBS_BT_CIELO  :   sqlText = qVisakBatchData__cielo;         break;
        default                         :   sqlText = null;                           break;
      }
           
      if( sqlText != null )
      {
        // load the batch merchant data     
	    log.info("[_loadMerchantAndBatchData()] - Loading batch data, batchType:"+batchType+", batchId:"+batchId);
        ps = getPreparedStatement(sqlText);
        ps.setLong(1,batchId);
        resultSet = ps.executeQuery();
        resultSet.next();
        batchData = new VisakBatchData( resultSet );
        resultSet.close();
      }
    }
    catch( Exception e )
    {
	    log.error("[_loadMerchantAndBatchData()] - Error.", e);
	    logEntry("_loadMerchantAndBatchData(" + batchId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); }catch(Exception e){}
      cleanUp();
    }
    return( batchData );
  }
  
  public static SettlementRecord loadSettlementRecord( String cardType, long batchId, long batchRecordId )
  {
    return( new SettlementDb()._loadSettlementRecord( cardType, -1, batchId, batchRecordId, true ) );
  }
  
  public SettlementRecord _loadSettlementRecord( String cardType, long recId )
  {
    return( _loadSettlementRecord( cardType, recId, -1, -1, false ) );
  }
  
  public SettlementRecord _loadSettlementRecord( String cardType, long recId, long batchId, long batchRecordId, boolean loadLineItems )
  {
    PreparedStatement   ps          = null;
    SettlementRecord    rec         = null;
    
    try
    {
      connect(true);
      
      rec = _getNewSettlementRecord( cardType );
      
      String  whereClause = ( recId < 0 ) ? " settle.batch_id = ? and settle.batch_record_id = ? and settle.test_flag = 'N' "
                                          : " settle.rec_id = ? ";
      String  tablePrefix = getTablePrefix( rec.RecSettleRecType );
      if( tablePrefix != null )
      {
        String sqlText = 
          " select  settle.*                                " +
          " from    " + tablePrefix + "_settlement  settle  " + 
          " where   " + whereClause                           ;
        
        ps = getPreparedStatement(sqlText);
        if( recId < 0 )
        {
          ps.setLong(1,batchId);
          ps.setLong(2,batchRecordId);
        }
        else
        {
          ps.setLong(1,recId);
        }
        ResultSet rs = ps.executeQuery();
        if( rs.next() )
        {
          rec.setFields(rs, false, false);
          if( loadLineItems && rec.hasLevelIIIData() )
          {
            rec.setLineItems( _loadSettlementLineItemDetailRecords(rec.getLong("rec_id"),rec.RecSettleRecType) );
          }
        }
        else
        {
          rec = null;
        }
        rs.close();
      }        
      else
      {
        rec = null;
      }
    }
    catch( Exception e )
    {
      logEntry("_loadSettlementRecord(" + cardType + "," + recId + "," + batchId + "," + batchRecordId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( rec );
  }

  public List _loadVisaSettlementLineItemDetailRecords( long recId )
  {
    return( _loadSettlementLineItemDetailRecords( recId, SettlementRecord.SETTLE_REC_VISA ) );
  }
  
  public List _loadMasterCardSettlementLineItemDetailRecords( long recId )
  {
    return( _loadSettlementLineItemDetailRecords( recId, SettlementRecord.SETTLE_REC_MC ) );
  }
  
  public List _loadSettlementLineItemDetailRecords( long recId, int recType )
  {
    LineItem            item        = null;
    List                items       = null;
    PreparedStatement   ps          = null;
    
    try
    {
      connect(true);
      
      String  tablePrefix = getTablePrefix( recType );
      if( tablePrefix != null )
      {
        String sqlText = 
          " select  li.*                                          " +
          " from    " + tablePrefix + "_settlement_line_items  li " + 
          " where   li.rec_id = ?                                 " +
          " order by li.line_item_id                              ";
        
        ps = getPreparedStatement(sqlText);
        ps.setLong(1,recId);
        ResultSet rs = ps.executeQuery();
        
        while( rs.next() )
        {
          if ( items == null ) { items = new ArrayList(); }
          item = new LineItem(recType);
          item.setFields(rs, false, false);
          items.add(item);
        }
        rs.close();
      }        
    }
    catch( Exception e )
    {
      logEntry("_loadSettlementLineItemDetailRecord(" + recType + "," + recId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( items );
  }
  
  public static SettlementRecord extractTranVisak( VisakBatchData batchData, String cardType, int recIdx )
    throws Exception
  {
    return( new SettlementDb()._extractTranVisak( batchData,cardType,recIdx ) );
  }
  
  public SettlementRecord _extractTranVisak( VisakBatchData batchData, String cardType, int recIdx )
		    throws Exception
		  {
		    PreparedStatement           ps    = null;
		    SettlementRecord            rec   = null;
		    
		    PreparedStatement   tc33tridentAuth_ps    = null;
		    ResultSet tc33tridentAuth_rs =   null;
		    String lcrCardType = null;
		    boolean isMPVPCardType = false;
		    
		    try
		    {
		      connect(true);
		      
		      /*Code Change Start to avoid Duplicate Merchant funding */
		      String tc33CardTypeText =
		              " select  tc.card_type              " + crlf +
		              " from    tc33_trident tc     " + crlf +
		              " where   tc.rec_id  IN ( select  tdl.auth_rec_id    " + crlf +
		              " from    trident_detail_lookup tdl     " + crlf +
		              " where   tdl.batch_rec_id = ?          " + crlf +
		              " and tdl.detail_index = ? ) ";
		   
		      tc33tridentAuth_ps = getPreparedStatement(tc33CardTypeText);
		      tc33tridentAuth_ps.setLong(1,batchData.getBatchId());
		      tc33tridentAuth_ps.setInt(2,recIdx);
		      tc33tridentAuth_rs = tc33tridentAuth_ps.executeQuery();
	          if(tc33tridentAuth_rs.next()){
	        	  lcrCardType = tc33tridentAuth_rs.getString("card_type");
	            	if(lcrCardType != null && (lcrCardType.equalsIgnoreCase("VP") || lcrCardType.equalsIgnoreCase("MP"))){
	            		isMPVPCardType = true;
	            	}
		    }
	        tc33tridentAuth_rs.close();
		    if(isMPVPCardType){
		    	  rec = null; 
		      }else{
		    	  rec = getNewSettlementRecord(cardType);
		      }
		    /*Code Change End to avoid Duplicate Merchant funding */
		  
		      
		    if( rec != null )
		      {
		        String sqlText =
		          " select  tdl.auth_rec_id,              " + crlf +
		          "         tdl.acq_reference_number,     " + crlf +
		          "         tdl.reject_reason,            " + crlf +
		          "         tdl.icc_data,                 " + crlf +
		          "         tdl.card_sequence_number,     " + crlf +
		          "         tdl.pos_entry_mode,           " + crlf +
		          "         tdl.response_code_emv         " + crlf +
		          " from    trident_detail_lookup tdl     " + crlf +
		          " where   tdl.batch_rec_id = ?          " + crlf +
		          "     and tdl.detail_index = ?          ";

		        ps = getPreparedStatement(sqlText);
		        ps.setLong(1,batchData.getBatchId());
		        ps.setInt(2,recIdx);
		        ResultSet rs = ps.executeQuery();

		        if ( rs.next() )
		        {
		          if( rs.getLong("auth_rec_id") > 0 )   // >0 means it auth linked; if not linked, leave at null (NOT 0!)
		          {
		            rec.setData("auth_rec_id", rs.getLong("auth_rec_id"));
		          }

		          rec.setData("icc_data"            , rs.getString("icc_data"));
		          rec.setData("card_sequence_number", rs.getString("card_sequence_number"));
		          rec.setData("pos_entry_mode"      , rs.getString("pos_entry_mode"));
		          rec.setData("response_code_emv"   , rs.getString("response_code_emv"));
		          rec.setData("reject_reason"       , rs.getString("reject_reason"));
		          rec.setData("acq_reference_number", rs.getLong("acq_reference_number"));
		        }
		        else
		        {
		          rec.setData("acq_reference_number", 0L);
		        }
		       
		        rs.close();

		        rec.setData("card_type"           , cardType );   // some SRec's override later, some need this
		        
		        rec.setFieldsVisak(batchData,recIdx);
		        if( rec.getDouble("transaction_amount") == 0.0 )
		        {
		          rec = null;   // zero tran amt: would not have gotten here if the void indicator was set
		        }
		      }
		    }
		    finally
		    {
		      try{ tc33tridentAuth_ps.close(); } catch(Exception ee) { log.error("Exception While closing tc33tridentAuth_ps prepared statement");}
		      try{ ps.close(); } catch(Exception ee) { log.error("Exception While closing ps prepared statement");}
		      cleanUp();
		    }
		    return( rec );
		 }
		      
		      
		      
  
  public static List loadBatchRecIds( int batchType, long batchId )
  {
    return( new SettlementDb()._loadBatchRecIds( batchType, batchId ) );
  }
  
  public List _loadBatchRecIds( int batchType, long batchId )
  {
    PreparedStatement         ps        = null;
    List                      retVal    = new ArrayList();
    String                    sqlText   = null;
    
    try
    {
      connect(true);
      log.info("[_loadBatchRecIds()] - batchType:"+batchType+", batchId:"+batchId);

      switch( batchType )
      {
        case mesConstants.MBS_BT_PG:
          sqlText = 
            " select  tapi.rec_id      as batch_record_id         " + crlf +
            " from    trident_capture_api   tapi                  " + crlf +
            " where   tapi.batch_id = ?                           " + crlf +
            "         and tapi.transaction_type in                " + crlf +
            "         (                                           " + crlf +
            "           select  tt.tran_type                      " + crlf +
            "           from    trident_capture_api_tran_type tt  " + crlf +
            "           where   nvl(tt.settle,'N') = 'Y'          " + crlf +
            "         )                                           " + crlf +
            "         and tapi.test_flag = 'N'  /* only prod */   " + crlf +
            " order by tapi.rec_id                                ";
          break;

        case mesConstants.MBS_BT_VITAL:
          sqlText = 
            " select  /*+ index (dt IDX_DDF_D256_DT_BATCH_ID) */  " + crlf +
            "         dt.fin_seq_num   as batch_record_id         " + crlf +
            " from    daily_detail_file_d256_dt   dt              " + crlf +
            " where   dt.btc_seq_num        = ?                   " + crlf +
            "         and dt.tran_amount   != 0                   " + crlf +
            " order by dt.fin_seq_num                             ";
          break;

        case mesConstants.MBS_BT_CIELO:
          sqlText = 
            " select  /*+ index (dt IDX_DDF_CIELO_BID_BRID) */    " + crlf +
            "         dt.batch_record_id as batch_record_id       " + crlf +
            " from    daily_detail_file_cielo_dt   dt             " + crlf +
            " where   dt.batch_id           = ?                   " + crlf +
            " order by dt.batch_record_id                         ";
          break;
      }

      if( sqlText != null )
      {
        ps = getPreparedStatement(sqlText);
        ps.setLong(1,batchId);
        ResultSet rs = ps.executeQuery();
        while( rs.next() )
        {
          retVal.add(rs.getLong("batch_record_id"));
        }        
        rs.close();
      }
    }
    catch( Exception e )
    {
      logEntry("_loadBatchRecIds(" + batchId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
      if ( log.isDebugEnabled()) {
    	  log.debug("[_loadBatchRecIds()] - loaded "+retVal.size()+" records for batchType:"+batchType+", batchId:"+batchId);
      }
    }
    return( retVal );
  }
  
  public static SettlementRecord extractTran( int batchType, VisakBatchData batchData, long recId )
  {
    return( new SettlementDb()._extractTran( batchType, batchData, recId ) );
  }
  
  public SettlementRecord _extractTran( int batchType, VisakBatchData batchData, long recId )
  {
    PreparedStatement   ps          = null;
    SettlementRecord    rec         = null;
    String              sqlText     = null;
    log.info("[_extractTran()] - batchType:"+batchType+", recId="+recId);
    try
    {
      connect(true);
      
      switch( batchType )
      {
        case mesConstants.MBS_BT_PG:
          sqlText = qExtractTranPG;

          ps = getPreparedStatement(sqlText);
          ps.setLong(1,recId);
          break;

        case mesConstants.MBS_BT_VITAL:
          sqlText = qExtractTranVital;

          ps = getPreparedStatement(sqlText);
          ps.setDate(1,batchData.getBatchDate());
          ps.setLong(2,batchData.getMerchantId());
          ps.setLong(3,recId);
          break;

        case mesConstants.MBS_BT_CIELO:
          sqlText = qExtractTranCielo;

          ps = getPreparedStatement(sqlText);
          ps.setLong(1,batchData.getBatchId());
          ps.setLong(2,recId);
          break;
      }

      if( ps != null )
      {
	      log.info("Loading transaction data.");
	      if ( log.isDebugEnabled()) {
	    	  log.debug("[extractTran()] - batchType="+batchType+", batchDate="+batchData.getBatchDate()+", merchant="+batchData.getMerchantId()+", recId="+recId+" SQL for transaction: "+sqlText);
	      }
	      ResultSet rs = ps.executeQuery();
	      rs.next();
	      rec = _extractTranFromResultSet( batchType, batchData, rs );
	      rs.close();
	      ps.close();
      }
    }
    catch( Exception e )
    {
	    log.error("[_extractTran(Error.", e);
	    logEntry("_extractTran(" + recId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( rec );
  }
  
  public SettlementRecord _extractTranFromResultSet( int batchType, VisakBatchData batchData, ResultSet rsTran )
  {
    List                      items     = null;
    PreparedStatement         ps        = null;
    SettlementRecord          rec       = null;
    ResultSet                 rsExtra   = null;
    String                    sqlText   = null;

    try
    {
      String    cardType  = rsTran.getString("card_type");
      rec = _getNewSettlementRecord(cardType);
      if (log.isDebugEnabled()) {
    	  log.debug(String.format("[_extractTranFromResultSet()] - batchType:%s, cardType:%s, batchRecordId:%s", batchType, cardType,rsTran.getLong("batch_record_id")));
          
      }
      if( rec != null )
      {
        rec.setFieldsBase(batchData, rsTran, batchType);
        
        switch( batchType )
        {
          case mesConstants.MBS_BT_PG:
        	  
        	//set data condition code for discover
        	if("DS".equalsIgnoreCase(cardType)) {
        		rec.setData("track_condition_code", rsTran.getString("track_condition_code"));
        	}
            // if this record has level iii line item data, load it
        	
            if ( rec.hasLevelIIIData() || batchData.getLevelDto() != null)
            {
            	items = setLineItemData(rec, batchData.getLevelDto());
            }          
            break;

          case mesConstants.MBS_BT_VITAL:
            String[]  cdfFormatsByIndustry  = { "OPTIN" };
            String[]  cdfFormatsByCard      = { "LODGE","PURC1","PURC2","SHIP1","SHIP2" };

            String    dateString            = "";
            String    marketSpecDataInd     = rsTran.getString("market_specific_data_ind");
            
            if("DS".equalsIgnoreCase(cardType)) {
            	String tranDataCondCode = null;
            	if(rsTran.getString("auth_val_code") !=null && rsTran.getString("auth_val_code").length() >= 2) {
            		tranDataCondCode = rsTran.getString("auth_val_code").substring(0, 2);
            	}
            	else if (rec.getData( "auth_val_code") !=null && rec.getData( "auth_val_code").length() >= 2) {
            		tranDataCondCode = rec.getData("auth_val_code").substring(0, 2);
            	}
            	rec.setData("track_condition_code", tranDataCondCode);
            }

            // load extension record data
            sqlText = 
              " select  /*+ index (er IDX_DDF_D256_ER_SEQ_NUM_BDATE) */   " + crlf +
              "         er.*                                              " + crlf +
              " from    daily_detail_file_d256_er       er                " + crlf +
              " where   er.batch_date       = ?                           " + crlf +
              "         and er.fin_seq_num  = ?                           " + crlf +
              " order by er.sequence_number                               ";
        
            ps = getPreparedStatement(sqlText);
            ps.setDate(1,batchData.getBatchDate());
            ps.setLong(2,rec.getLong("batch_record_id"));
            rsExtra = ps.executeQuery();
      
            while( rsExtra.next() )
            {
              String    recordType  = rsExtra.getString("format_indicator");
              String    rawData     = StringUtilities.leftJustify("                " + rsExtra.getString("data"),256,' ');
        
              // handle special cases
              for( int scType = 0; scType < 2; ++scType )
              {
                String[] recordTypes = (scType == 0 ? cdfFormatsByIndustry : cdfFormatsByCard);
                for( int i = 0; i < recordTypes.length; ++i )
                {
                  if ( recordType.equals( recordTypes[i] ) )
                  {
                    String   extension   = "_" + (scType == 0 ? marketSpecDataInd : cardType);
                    recordType += extension;
                    break;
                  }
                }
              }
          
              // look for an matching record for this extension
              Integer recType = (Integer)CdfExtRecToFfdType.get(recordType);
              if ( recType != null )
              {
                LineItem li = null;
                FlatFileRecord ffd = new FlatFileRecord(Ctx,recType.intValue());
                //@System.out.println(recordType);//@
                //@System.out.println(rawData);//@
                ffd.suck(rawData);
            
                if ( !recordType.startsWith("PURC2") )   // line item detail
                {
                  ffd.setFieldBeanData(rec);  // use ffd data to set field bean data
                }

                switch( recType.intValue() )
                {
                  case MesFlatFiles.DEF_TYPE_CDF_GEN_REC:
                    if( rec.getLong("auth_rec_id") == 0 )
                      rec.setData("product_id",ffd.getFieldData("product_id_sometimes"));
                    if( rec.RecSettleRecType == SettlementRecord.SETTLE_REC_MC )
                    {
                      String posDataCode = rec.getData("pos_data_code").trim();
                      if( posDataCode.length() == 12 && !(posDataCode.charAt(5) == '1' && posDataCode.charAt(6) == 'S') )
                      {
                        switch( posDataCode.charAt(5) )
                        {
                          case '0': // card not present
                            if( "Y".equals(rec.getData("card_present")) )
                            {
                              rec.setData("moto_ecommerce_ind","1" );
                              rec.setData("card_present"      ,"N");
                            }
                            break;
                          case '1': // card present
                            if( "N".equals(rec.getData("card_present")) )
                            {
                              rec.setData("moto_ecommerce_ind","" );
                              rec.setData("card_present"      ,"Y");
                            }
                            break;
                        }
                      }
                    }
                    dateString = ffd.getFieldData("tran_time");
                    String tranInd = ffd.getFieldData("tran_fee_ind");
                    double tranFeeAmount = TridentTools.decodeAmount(ffd.getFieldData("tran_fee_amount"));                    
                    
                    if (tranFeeAmount != 0){
                    	tranFeeAmount = "C".equals(tranInd)?-tranFeeAmount:tranFeeAmount;
                    	rec.setData("tran_fee_amount", tranFeeAmount);
                    }
        
                    else 
                    	rec.setData("tran_fee_amount", "");
                                        
                    break;

                  case MesFlatFiles.DEF_TYPE_CDF_AMEX_HOTEL_REC:
                    li = new LineItem(rec.RecSettleRecType);
                    ffd.setFieldBeanData(li);   // set the line item data
                    li = setVitalLineItemsFromIO(li, rec, batchData.getLevelDto());
                    
                    if ( li.getDouble("extended_item_amount") == 0.0 && li.getDouble("item_quantity") == 0.0 )
                    {
                      li = null;    // throw away this garbage
                    }
                    else
                    {
                      li.setData("item_description","Room Rate & Nights");
                    }
                  // fall through  
                  case MesFlatFiles.DEF_TYPE_CDF_VISA_HOTEL_REC:
                  case MesFlatFiles.DEF_TYPE_CDF_MC_HOTEL_REC:
                    if ( !rec.isFieldBlank("purchase_id") )
                    {
                      rec.setData("purchase_id_format","4");
                    }
                    break;

                  case MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC:
                    if ( !rec.isFieldBlank("purchase_id") )
                    {
                      rec.setData("purchase_id_format","3");
                    }
                    break;

                  case MesFlatFiles.DEF_TYPE_CDF_VISA_PURC1_REC:
                    String orderDate = rec.getData("order_date");
                    if ( orderDate.length() == 6 )
                    {
                      rec.setData("order_date",(orderDate.substring(4) + orderDate.substring(0,4)));
                    }
                    break;

                  case MesFlatFiles.DEF_TYPE_CDF_VISA_PURC2_REC:
                    li = new LineItem(rec.RecSettleRecType);
                    ffd.setFieldBeanData(li);   // set the line item data
                  
                    li = setVitalLineItemsFromIO(li, rec, batchData.getLevelDto());

                    double  rate  = li.getDouble("vat_tax_rate");
                    while( rate > 22.00 ) rate = MesMath.round(rate*0.1,2);
                    li.setData("vat_tax_rate",rate);
                    break;

                  case MesFlatFiles.DEF_TYPE_CDF_MC_PURC2_REC:
                    li = new LineItem(rec.RecSettleRecType);
                    ffd.setFieldBeanData(li);   // set the line item data
                    
                    li = setVitalLineItemsFromIO(li, rec, batchData.getLevelDto());

                    switch( rec.RecSettleRecType )
                    {
                      case SettlementRecord.SETTLE_REC_MC:
                        int exp = 0;
                        if ( (exp = ffd.getFieldAsInt("item_quantity_exponent",0)) != 0 )
                        {
                          String  formattedData = ffd.getFieldData("item_quantity");
                          String  decimalData   = formattedData.substring(0,formattedData.length()-exp) + "." +
                                                  formattedData.substring(formattedData.length()-exp);
                          double  doubleData    = Double.parseDouble(decimalData);
                          if( doubleData < 0.01 )doubleData = 0.01;
                          li.setData("item_quantity",doubleData);
                        }

                        if ( li.isFieldBlank("item_unit_measure") )
                        {
                          li.setData("item_unit_measure","Each");
                        }
                        break;

                      case SettlementRecord.SETTLE_REC_AMEX:
                        String taxType = li.getData("tax_type_applied");
                        if ( taxType.length() == 4 )
                          rec.setData("tax_type_code",taxType.substring(1));

                        if ( li.getDouble("extended_item_amount") == 0.0 && li.getDouble("item_quantity") == 0.0 )
                        {
                          li.setData("extended_item_amount",rec.getDouble("transaction_amount"));
                          li.setData("item_quantity",1);
                        }
                        break;
                    }
                    break;
                }

                // if the line item is valid, add it to the list of items
                if ( li != null && li.fixBadData() )
                {
                  if ( items == null )
                  {
                    items = new ArrayList();
                  }
                  items.add(li);
                }
              }
              else
              {
                //@ should issue warning to system admin?
                System.out.println("Not handling " + recordType);//@
              }
            }
            rsExtra.close();
            ps.close();
        
            // add the transaction time (either from gen_rec or at midnight)
            if( dateString.trim().length() == 0 ) dateString = "000000";
            dateString  = rec.getData("transaction_date") + " " + dateString;
            java.util.Date  javaDate    = DateTimeFormatter.parseDate(dateString,"MM/dd/yyyy HHmmss");
            DateField df = (DateField)rec.getField("transaction_time");
            df.setUtilDate(javaDate,"MM/dd/yyyy HH:mm:ss");

            break;

          case mesConstants.MBS_BT_CIELO:
            switch( rec.RecSettleRecType )
            {
            //case SettlementRecord.SETTLE_REC_VISA   : // checks eflags in ic_desc; ICVisa changes eflags for Brazil NNSS
            //  break;

              case SettlementRecord.SETTLE_REC_MC     : // checks eflags in mc_ic_program_rules
              case SettlementRecord.SETTLE_REC_MODBII : // checks eflags in ic_desc
                // set eligibility_flags to "mif eFlags for this card type, installment group"
                String  eFlags    = rec.getData("eligibility_flags");
                if( eFlags.length() != 0 ) eFlags += ",";
                eFlags += rec.getData("dbcr_installtype");
                rec.setData("eligibility_flags" , eFlags);
                break;
            }
            break;
        }

        // add the line item detail
        if( items != null && items.size() > 0 )
        {
          rec.setLineItems(items);
          rec.setData("level_iii_data_present","Y");
        }
        else
        {
          rec.setData("level_iii_data_present","N");
        }
      }
    }
    catch( Exception e )
    {
      String params = "";
      log.error("[_extractTranFromResultSet()] - Exception extracting transaction", e);
      try{ params += rsTran.getLong("batch_id") + "," + rsTran.getLong("batch_record_id"); }
      catch( Exception ee ) {}
      logEntry("_extractTranFromResultSet(" + params + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
    }
    return( rec );
  }
  
  public LineItem setVitalLineItemsFromIO(LineItem li, SettlementRecord rec, LevelDto levelDto)
  {
	  if(levelDto == null)
	  {
		  return li;
	  }
	  
      log.debug(" ******************************** BEFORE -- TSYS LINE ITEM FILLED UP FROM IO ********************************");
	  rec.displayLineItem(li);
      li = li.setFieldsFromIOLineItem(li, levelDto, rec.RecSettleRecType);
      log.debug(" ******************************** AFTER -- TSYS LINE ITEM FILLED UP FROM IO ********************************");

      rec.displayLineItem(li);
      
      return li;
  }
  
  public List<LineItem> setLineItemData(SettlementRecord rec, LevelDto levelDataBO)
	{
		ArrayList<LineItem> items = null;
		ArrayList<String> rawData = new ArrayList<>();

		long recId = rec.getLong("batch_record_id");
		ApiLineItemDetailRecord pgli = null;

		String sqlText = " select  li.raw_line_item_data              " + crlf
						+ " from    trident_api_line_item_detail  li  " + crlf
						+ " where   li.rec_id = ?                     " + crlf
						+ " order by line_item_id                     ";
		
		if (rec.hasLevelIIIData()) {
			try (Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
					PreparedStatement stmt = con.prepareStatement(sqlText)) {
				stmt.setLong(1, recId);

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						rawData.add(rs.getString("raw_line_item_data"));
					}
				}
			}
			catch (Exception e) {
				logEntry("failed to fetch l2/l3 line item from DB, transaction will be processed without L2/L3 if IO set up is OFF: ", e.toString());
			}
		}

		if (rawData.isEmpty() && levelDataBO != null) {
			rawData.add(null);
		}

		for (String rawString : rawData) {

			try {
				switch (rec.RecSettleRecType) {
				case SettlementRecord.SETTLE_REC_VISA:
					pgli = new ApiVisaLineItemDetailRecord(rawString, levelDataBO != null ? levelDataBO.getLineItem().getVisaLineItem() : new VisaLineItem());
					break;
				case SettlementRecord.SETTLE_REC_MC:
					pgli = new ApiMCLineItemDetailRecord(rawString, levelDataBO != null ? levelDataBO.getLineItem().getMcLineItem() : new McLineItem());
					break;
				case SettlementRecord.SETTLE_REC_AMEX:
					pgli = new ApiAMLineItemDetailRecord(rawString,levelDataBO != null ? levelDataBO.getLineItem().getAmexLineItem() : new AmexLineItem());
					break;
				default:
					continue;
				}

				// convert the line item and add it to the settlement record data
				LineItem li = new LineItem(rec.RecSettleRecType);
				li.setFieldsFromVisakLineItem(pgli);

				// if the line item is valid, add it to the list of items
				if (li != null && li.fixBadData()) {
					if (items == null) {
						items = new ArrayList<LineItem>();
					}
					items.add(li);
				}
			}
			catch (Exception e) {
				log.error("failed to parse l2/l3 line item, transaction will be processed without L2/L3 data... :", e);
			}

		}
		return items;
	} 
  public static Vector loadBatchesToExtract( int batchType, String loadFilename )
  {
    return( new SettlementDb()._loadBatchesToExtract(batchType, loadFilename) );
  }
  
  public Vector _loadBatchesToExtract( int batchType, String loadFilename )
  {
    Vector              batchList                 = new Vector();
    int                 fileBankNumber            = 0;
    Date                fileDate                  = null;
    long                loadFileId                = 0L;
    PreparedStatement   ps                        = null;
    String              sqlText                   = null;

    try
    {
      connect(true);
      log.info(String.format("[_loadBatchesToExtract()] - batchType:%s, loadFilename:%s",batchType,loadFilename));
      // load filename is passed to re-extract a specific group of batches
      if ( loadFilename != null && batchType != mesConstants.MBS_BT_VITAL )
      {
        loadFileId      = _loadFilenameToLoadFileId(loadFilename);
        fileBankNumber  = _getFileBankNumber(loadFilename);
      
        // get the date of the file
        java.util.Date javaDate = _getFileDate(loadFilename);
        if ( javaDate == null )
        {
          javaDate = Calendar.getInstance().getTime();
        }
        fileDate = new java.sql.Date( javaDate.getTime() );
      }

      switch( batchType )
      {
        case mesConstants.MBS_BT_UNKNOWN:
        case mesConstants.MBS_BT_VISAK:
        case mesConstants.MBS_BT_PG:
        case mesConstants.MBS_BT_CIELO:
          sqlText = 
            " select  mb.mbs_batch_type               as batch_type,    " +
            "         mb.batch_id                     as batch_id       " +
            " from    mbs_batches   mb                                  " +
            " where   mb.merchant_batch_date between (?-31) and (?+1)   " +
            "         and mb.load_file_id = ?                           " +
            "         and ( mb.load_file_id != 0 or                     " +
            "               mb.load_filename is null )                  " +
            "         and ? in ( 9999 , mb.bank_number )                " +
            "         and ? in ( ? , mb.mbs_batch_type )                " +
            "         and mb.response_code = 0  /* only GB's         */ " +
            "         and mb.test_flag = 'N'    /* only prod batches */ " +
            " order by mb.total_count desc, mb.batch_id                 ";

          ps = getPreparedStatement(sqlText);
          ps.setDate(1,fileDate);
          ps.setDate(2,fileDate);
          ps.setLong(3,loadFileId);
          ps.setInt (4,fileBankNumber);
          ps.setInt (5,batchType);
          ps.setInt (6,mesConstants.MBS_BT_UNKNOWN);

          break;

        case mesConstants.MBS_BT_VITAL:
          sqlText = 
            " select  /*+ index (dt IDX_DDF_D256_DT_LOAD_FILENAME) */   " + crlf +
            "         ?                               as batch_type,    " + crlf +
            "         dt.btc_seq_num                  as batch_id,      " + crlf +
            "         count(1)                        as tranCount      " + crlf +
            " from    daily_detail_file_d256_dt   dt                    " + crlf +
            " where   dt.load_filename = ?                              " + crlf +
            " group by dt.btc_seq_num                                   " + crlf +
            " order by tranCount desc, dt.btc_seq_num                   ";

          ps = getPreparedStatement(sqlText);
          ps.setInt   (1,batchType);
          ps.setString(2,loadFilename);
          break;
      }

      if( ps != null )
      {
        ResultSet   resultSet = ps.executeQuery();

        while( resultSet.next() )
        {
          batchList.addElement( new String(resultSet.getString("batch_type") + "," + resultSet.getString("batch_id")) );
        }
        resultSet.close();
        ps.close();
      }
    }
    catch( Exception e )
    {
	    log.error("Error.", e);
	    logEntry("_loadBatchesToExtract(" + loadFilename + "," + fileBankNumber + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); }catch(Exception e){}
      cleanUp();
      log.info(String.format("[_loadBatchesToExtract()] loaded %s batches", batchList.size()));
    }
    return( batchList );
  }

  public static VisaCountryData loadVisaCountryData( String countryCode )
  {
    return( new SettlementDb()._loadVisaCountryData(countryCode) );
  }
  
  public VisaCountryData _loadVisaCountryData( String countryCode )
  {
    PreparedStatement ps      = null;
    VisaCountryData   retVal  = null;
    
    try
    {
      connect();
    
      ps = getPreparedStatement("select * from visa_country_code where country_code = ?");
      ps.setString(1, countryCode );
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = new VisaCountryData(rs);
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadVisaCountryData()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static void storeVisaCountryData( String rawData )
  {
    new SettlementDb()._storeVisaCountryData(rawData);
  }
  
  public void _storeVisaCountryData( String rawData )
  {
    FlatFileRecord    ffd   = null;
    PreparedStatement ps    = null;
    
    try
    {
      connect();
    
      ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_COUNTRY_CODE);
      ffd.suck(StringUtilities.leftJustify(rawData,88,' '));
      
      String sqlText = 
        " insert into visa_country_code     " + crlf + 
        " (                                 " + crlf + 
        "   country_code,                   " + crlf + 
        "   foreign_indicator,              " + crlf + 
        "   currency_code_default,          " + crlf + 
        "   cps_indicator,                  " + crlf + 
        "   epay_indicator,                 " + crlf + 
        "   area_net_indicator              " + crlf + 
        " )                                 " + crlf + 
        " values ( ?,?,?,?,?,? )            ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1, ffd.getFieldData("country_code") );
      ps.setString(2, ffd.getFieldData("foreign_indicator") );
      ps.setString(3, ffd.getFieldData("currency_code_default") );
      ps.setString(4, ffd.getFieldData("cps_indicator") );
      ps.setString(5, ffd.getFieldData("epay_indicator") );
      ps.setString(6, ffd.getFieldData("area_net_indicator") );
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_storeVisaCountryData()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      try{ ffd.cleanUp(); } catch( Exception ee ) {}
      cleanUp();
    }
  }
  
	public String[][] createUpdateFieldsDT(int recType) {
		String settlementFields[][] = {};
		switch (recType) {
		case SettlementRecord.SETTLE_REC_VISA:
			settlementFields = SettlementFieldsVisa;
			break;
		case SettlementRecord.SETTLE_REC_MC:
			settlementFields = SettlementFieldsMC;
			break;
		case SettlementRecord.SETTLE_REC_AMEX:
			settlementFields = SettlementFieldsAmex;
			break;
		case SettlementRecord.SETTLE_REC_DISC:
			settlementFields = SettlementFieldsDiscover;
			break;
		case SettlementRecord.SETTLE_REC_MODBII:
			settlementFields = SettlementFieldsModBII;
			break;
		}
		List<String[]> dailyDetailFieldsList = Arrays.asList(DailyDetailFileDtFields);
		List<String[]> settlementFieldsList = Arrays.asList(settlementFields);
		List<String[]> updateDailyDetailFieldsList = new ArrayList<>();
		String extraFields[][] = {{"load_file_id", "load_file_id", "L"}, {"load_filename", "load_filename", "S"}, {"external_reject", "reproc", "S"},
				{"reject_reason", "reject_reason", "S"}, {"ach_flag", "ach_flag", "S"}, {"purchase_id", "purchase_id", "S"}};

		dailyDetailFieldsList.forEach(dtfield -> settlementFieldsList.stream().filter(settlementfield -> dtfield[0].equals(settlementfield[0])).findFirst().ifPresent(settlementfield -> updateDailyDetailFieldsList.add(dtfield)));
		updateDailyDetailFieldsList.addAll(Arrays.asList(extraFields));
		return (updateDailyDetailFieldsList.toArray(new String[][] {}));
	}
  
  private static final String[][] DailyDetailFileDtFields = 
  {
    // field name                     column name              type
    { "merchant_number"             , "merchant_account_number"        , "L" },
    { "bank_number"                 , "bank_number"                    , "S" },
    { "batch_date"                  , "batch_date"                     , "D" },
    { "auth_code"                   , "authorization_num"              , "S" },
    { "auth_amount_total"           , "auth_amt"                       , "F" },
    { "auth_tran_fee_amount"        , "auth_tran_fee_amount"           , "F" },
    { "auth_currency_code"          , "auth_currency_code"             , "L" },
    { "auth_rec_id"                 , "auth_rec_id"                    , "L" },
    { "auth_date"					, "auth_date"					   , "T" },
    { "auth_response_code"          , "auth_respon_code"               , "S" },
    { "auth_source_code"            , "auth_source"                    , "S" },
    { "batch_id"                    , "batch_number"                   , "S" },
    { "card_number"                 , "cardholder_account_number"      , "S" },
    { "cardholder_id_method"        , "cardholder_i_d_method"          , "S" },
    { "card_number_enc"             , "card_number_enc"                , "S" },
    { "card_type"                   , "card_type"                      , "S" },
    { "card_type_enhanced"          , "card_type_enhanced"             , "S" },
    { "debit_type"                  , "debit_type"                     , "S" },
    { "funding_currency_code"       , "currency_code"                  , "S" },
    { "batch_record_id"             , "ddf_dt_id"                      , "L" },
    { "debit_credit_indicator"      , "debit_credit_indicator"         , "S" },
    { "foreign_card_indicator"      , "foreign_card_indicator"         , "S" },
    { "auth_returned_aci"           , "mci_auth_char_ind"              , "S" },
    { "merchant_batch_date"         , "merchant_batch_date"            , "D" },
    { "batch_number"                , "merchant_batch_number"          , "L" },
    { "dba_name"                    , "merchant_name"                  , "S" },
    { "moto_ecommerce_ind"          , "mo_to_indicator"                , "S" },
    { "debit_network_id"            , "netwk_identifier_debit"         , "S" },
    { "pos_entry_mode"              , "pos_entry_mode"                 , "S" },
    { "product_id"                  , "product_id"                     , "S" },
    { "purchase_id_unenh"           , "purchase_id"                    , "S" },
    { "reference_number"            , "reference_number"               , "S" },
    { "reimbursement_attribute"     , "reimbursement_attribute"        , "S" },
    { "ic_cat"                      , "enhanced_interchange"           , "S" },
    { "ic_cat_billing"              , "submitted_interchange2"         , "S" },
    { "ic_bet_number"               , "ic_bet_number"                  , "S" },
    { "reject_reason"               , "reject_reason"                  , "S" },
    { "ach_flag"                    , "ach_flag"                       , "S" },
    { "funding_amount"              , "transaction_amount"             , "F" },
    { "currency_code"               , "original_currency_code"         , "S" },
    { "transaction_amount"          , "original_transaction_amount"    , "F" },
    { "transaction_date"            , "transaction_date"               , "D" },
    { "auth_tran_id"                , "trans_id_visa"                  , "S" },
    { "trident_tran_id"             , "trident_tran_id"                , "S" },
    { "auth_val_code"               , "validat_code"                   , "S" },
    { "client_reference_number"     , "client_reference_number"        , "S" },
    { "multiple_clearing_seq_num"   , "installment_number"             , "I" },
    { "multiple_clearing_seq_count" , "installment_count"              , "I" },
    { "cielo_product"               , "cielo_product"                  , "I" },
    { "technology_type"             , "technology_type"                , "S" },
    { "tran_fee_amount"             , "tran_fee_amount"                , "F" },
    { "fx_conv_date"                , "fx_conv_date"                   , "T" },
//@    { "association_num"             , "association_num"                , "L" },
//@    { "auth_banknet_date"           , "banknet_auth_date_m_c"          , "D" },
//@    { "auth_banknet_ref_num"        , "banknet_ref_num_m_c"            , "S" },
//@    { "batch_julian_date"           , "batch_julian_date"              , "D" },
//@    { "best_interchange_eligible"   , "best_interchange_eligible"      , "S" },
//@    { "carryover_indicator"         , "carryover_indicator"            , "S" },
//@    { "cat_indicator"               , "cat_indicator"                  , "S" },
//@    { "data_source"                 , "data_source"                    , "L" },
//@    { "discount_amount"             , "discount_amount"                , "F" },
//@    { "discount_rate"               , "discount_rate"                  , "L" },
//@    { "downgrade_reason_1_din"      , "downgrade_reason_1_din"         , "S" },
//@    { "downgrade_reason_1_mc"       , "downgrade_reason_1_mc"          , "S" },
//@    { "downgrade_reason_1_visa"     , "downgrade_reason_1_visa"        , "S" },
//@    { "downgrade_rec_id"            , "downgrade_rec_id"               , "L" },
//@    { "extension_record_indicator"  , "extension_record_indicator"     , "S" },
//@    { "ic_billing_amount"           , "ic_billing_amount"              , "F" },
//@    { "ic_expense"                  , "ic_expense"                     , "L" },
    { "pos_terminal_id"             , "merchant_id"                    , "S" },
//@    { "net_deposit"                 , "net_deposit"                    , "L" },
//@    { "online_entry"                , "online_entry"                   , "S" },
//@    { "per_item"                    , "per_item"                       , "L" },
//@    { "record_identifier"           , "record_identifier"              , "S" },
//@    { "reversal_flag"               , "reversal_flag"                  , "S" },
//@    { "submitted_interchange"       , "submitted_interchange"          , "L" },
//@    { "switch_setted_indicator"     , "switch_setted_indicator"        , "S" },
//@    { "transacction_code"           , "transacction_code"              , "L" },
//@    { "transaction_type"            , "transaction_type"               , "S" },
  };
  
  private static final String[][] DailyDetailFileExtDtFields = 
  {
    // field name                     column name                   type
    { "merchant_number"             , "merchant_number"           , "L" },
    { "merchant_batch_date"         , "batch_date"                , "D" },
    { "auth_code"                   , "auth_code"                 , "S" },
    { "batch_id"                    , "batch_number"              , "L" },
    { "card_number"                 , "card_number"               , "S" },
    { "card_number_enc"             , "card_number_enc"           , "S" },
    { "card_type"                   , "card_type"                 , "S" },
    { "debit_credit_indicator"      , "debit_credit_indicator"    , "S" },
    { "batch_number"                , "merchant_batch_number"     , "I" },
    { "pos_entry_mode"              , "pos_entry_mode"            , "S" },
    { "purchase_id_unenh"           , "purchase_id"               , "S" },
    { "acq_reference_number"        , "reference_number"          , "S" },
    { "terminal_number"             , "terminal_number"           , "I" },
    { "transaction_amount"          , "transaction_amount"        , "F" },
    { "transaction_date"            , "transaction_date"          , "D" },
    { "trident_tran_id"             , "trident_tran_id"           , "S" },
    { "client_reference_number"     , "client_reference_number"   , "S" },
//@    { "tran_code"                   , "tran_code"             , "S" },
//@    { "acq_invoice_number"          , "acq_invoice_number"    , "S" },
//@    { "passenger_name"              , "passenger_name"        , "S" },
    { "pos_terminal_id"             , "pos_terminal_id"           , "S" },
  };
  
  private static final String[][] FeeCollectFieldsVisa = 
  {
    // field name                     column name             type
    { "merchant_number"         , "merchant_number"         , "L" },
    { "rec_id"                  , "settle_rec_id"           , "L" },
    { "transaction_date"        , "transaction_date"        , "D" },
    { "card_number"             , "card_number"             , "S" },
    { "card_number_enc"         , "card_number_enc"         , "S" },
    { "issuer_bin"              , "dest_bin"                , "I" },
    { "bin_number"              , "source_bin"              , "S" },
    { "reason_code"             , "reason_code"             , "S" },
    { "message_text"            , "message_text"            , "S" },
    { "debit_credit_indicator"  , "debit_credit_indicator"  , "S" },
    { "reimbursement_attribute" , "reimbursement_attribute" , "S" },
    { "currency_code"           , "currency_code"           , "S" },
    { "transaction_amount"      , "transaction_amount"      , "F" },
  };
  
  private static final String[][] FeeCollectFieldsMC  =
  {
    // field name                     column name             type
    { "merchant_number"         , "merchant_number"         , "L" },
    { "rec_id"                  , "settle_rec_id"           , "L" },
    { "transaction_date"        , "transaction_date"        , "D" },
    { "card_number"             , "card_number"             , "S" },
    { "card_number_enc"         , "card_number_enc"         , "S" },
    { "reason_code"             , "reason_code"             , "S" },
    { "message_text"            , "message_text"            , "S" },
    { "debit_credit_indicator"  , "debit_credit_indicator"  , "S" },
    { "currency_code"           , "currency_code"           , "S" },
    { "transaction_amount"      , "transaction_amount"      , "F" },
  };

  private static final String[][] SettlementFieldsVisa =
  {
    // field name                     type
    { "bin_number"                  , "S" },
    { "acquirer_business_id"        , "S" },
    { "bank_number"                 , "S" },
    { "merchant_number"             , "L" },
    { "dba_name"                    , "S" },
    { "dba_city"                    , "S" },
    { "dba_state"                   , "S" },
    { "dba_zip"                     , "S" },
    { "sic_code"                    , "S" },
    { "moto_ecommerce_merchant"     , "S" },
    { "mvv"                         , "S" },
    { "country_code"                , "S" },
    { "phone_number"                , "S" },
    { "card_number"                 , "S" },
    { "card_type"                   , "S" },
    { "card_type_enhanced"          , "S" },
    { "region_issuer"               , "S" },
    { "region_merchant"             , "S" },
    { "cash_disbursement"           , "S" },
    { "merchant_batch_date"         , "D" },
    { "batch_number"                , "S" },
    { "batch_id"                    , "L" },
    { "batch_record_id"             , "L" },
    { "transaction_date"            , "D" },
    { "debit_credit_indicator"      , "S" },
    { "transaction_amount"          , "F" },
    { "currency_code"               , "S" },
    { "funding_amount"              , "F" },
    { "funding_currency_code"       , "S" },
    { "ic_cat"                      , "S" },
    { "ic_cat_billing"              , "S" },
    { "ic_cat_downgrade"            , "S" },
    { "output_filename"             , "S" },
    { "auth_amount"                 , "F" },
    { "auth_amount_total"           , "F" },
    { "auth_currency_code"          , "S" },
    { "auth_code"                   , "S" },
    { "auth_source_code"            , "S" },
    { "auth_response_code"          , "S" },
    { "auth_date"                   , "D" },
    { "auth_returned_aci"           , "S" },
    { "auth_settled_aci"            , "S" },
    { "auth_retrieval_ref_num"      , "S" },
    { "auth_tran_id"                , "S" },
    { "auth_val_code"               , "S" },
    { "auth_avs_response"           , "S" },
    { "auth_cvv2_response"          , "S" },
    { "auth_rec_id"                 , "L" },
    { "trident_tran_id"             , "S" },
    { "client_reference_number"     , "S" },
    { "product_id"                  , "S" },
    { "cardholder_id_method"        , "S" },
    { "special_conditions_ind"      , "S" },
    { "fee_program_indicator"       , "S" },
    { "requested_payment_service"   , "S" },
    { "reimbursement_attribute"     , "S" },
    { "reference_number"            , "S" },
    { "acq_reference_number"        , "S" },
    { "purchase_id"                 , "S" },
    { "purchase_id_format"          , "S" },
    { "statement_date_begin"        , "D" },
    { "statement_date_end"          , "D" },
    { "tax_amount"                  , "F" },
    { "tax_indicator"               , "S" },
    { "pos_entry_mode"              , "S" },
    { "card_present"                , "S" },
    { "moto_ecommerce_ind"          , "S" },
    { "pos_term_cap"                , "S" },
    { "recurring_payment_ind"       , "S" },
    { "tech_ind"                    , "S" },
    { "multiple_clearing_seq_num"   , "I" },
    { "multiple_clearing_seq_count" , "I" },
    { "ship_to_zip"                 , "S" },
    { "shipping_amount"             , "F" },
    { "cashback_amount"             , "F" },
    { "tip_amount"                  , "F" },
    { "market_specific_data_ind"    , "S" },
    { "information_indicator"       , "S" },
    { "merchant_vol_indicator"      , "S" },
    { "ecommerce_goods_indicator"   , "S" },
    { "enhanced_data_1"             , "S" },
    { "enhanced_data_2"             , "S" },
    { "enhanced_data_3"             , "S" },
    { "level_iii_data_present"      , "S" },
    { "eligibility_flags"           , "S" },
    { "card_number_enc"             , "S" },
    { "last_modified_user"          , "S" },
    { "no_show_indicator"           , "S" },
    { "hotel_rental_days"           , "I" },
    { "extra_charges"               , "S" },
    { "rate_daily"                  , "F" },
    { "rate_weekly"                 , "F" },
    { "one_way_drop_off_charges"    , "F" },
    { "insurance_charges"           , "F" },
    { "fuel_charges"                , "F" },
    { "car_class_code"              , "S" },
    { "renter_name"                 , "S" },
    { "total_room_tax"              , "F" },
    { "folio_cash_advances"         , "F" },
    { "food_beverage_charges"       , "F" },
    { "prepaid_expenses"            , "F" },
    { "discount_amount"             , "F" },
    { "duty_amount"                 , "F" },
    { "ship_from_zip"               , "S" },
    { "dest_country_code"           , "S" },
    { "vat_reference_number"        , "S" },
    { "order_date"                  , "S" },
    { "shipping_tax_amount"         , "F" },
    { "shipping_tax_rate"           , "F" },
    { "issuer_bin"                  , "S" },
    { "customer_code"               , "S" },
    { "debit_type"                  , "S" },
    { "ardef_data"                  , "S" },
    { "chip_condition_code"         , "S" },
    { "icc_data"                    , "S" },
    { "card_sequence_number"        , "I" },
    { "pos_terminal_id"             , "S" },
    { "settlement_days"             , "I" },
    { "spend_qualified_ind"         , "S" },
    { "token_assurance_level"       , "S" },
    { "pos_environment"             , "S" },
    { "tran_fee_amount"             , "F" }, 
  };
  
  private static final String[][] SettlementFieldsVisaLineItem =
  {
    { "item_commodity_code"         , "S" },
    { "item_descriptor"             , "S" },
    { "product_code"                , "S" },
    { "quantity"                    , "F" },
    { "unit_of_measure"             , "S" },
    { "unit_cost"                   , "F" },
    { "vat_tax_amount"              , "F" },
    { "vat_tax_rate"                , "F" },
    { "discount_per_line"           , "F" },
    { "line_item_total"             , "F" },
  };
  
  private static final String[][] SettlementFieldsMC = 
  {
    // field name                     type
    { "bin_number"                  , "S" },
    { "ica_number"                  , "S" },
    { "bank_number"                 , "S" },
    { "merchant_number"             , "L" },
    { "legal_corp_name"             , "S" },
    { "dba_name"                    , "S" },
    { "dba_address"                 , "S" },
    { "dba_city"                    , "S" },
    { "dba_state"                   , "S" },
    { "dba_zip"                     , "S" },
    { "country_code"                , "S" },
    { "sic_code"                    , "S" },
    { "card_acceptor_type"          , "S" },
    { "mc_assigned_id"              , "S" },
    { "phone_number"                , "S" },
    { "dm_contact_info"             , "S" },
    { "card_number"                 , "S" },
    { "card_type"                   , "S" },
    { "exp_date"                    , "S" },
    { "gcms_product_id"             , "S" },
    { "card_program_id"             , "S" },
    { "product_class"               , "S" },
    { "cash_disbursement"           , "S" },
    { "merchant_batch_date"         , "D" },
    { "batch_number"                , "S" },
    { "batch_id"                    , "L" },
    { "batch_record_id"             , "L" },
    { "transaction_date"            , "D" },
    { "transaction_time"            , "T" },
    { "debit_credit_indicator"      , "S" },
    { "processing_code"             , "S" },
    { "transaction_amount"          , "F" },
    { "currency_code"               , "S" },
    { "funding_amount"              , "F" },
    { "funding_currency_code"       , "S" },
    { "ic_cat"                      , "S" },
    { "ic_cat_billing"              , "S" },
    { "ic_cat_downgrade"            , "S" },
    { "output_filename"             , "S" },
    { "auth_amount"                 , "F" },
    { "auth_amount_total"           , "F" },
    { "auth_currency_code"          , "S" },
    { "auth_code"                   , "S" },
    { "auth_source_code"            , "S" },
    { "auth_response_code"          , "S" },
    { "auth_date"                   , "T" },
    { "auth_retrieval_ref_num"      , "S" },
    { "auth_banknet_ref_num"        , "S" },
    { "auth_banknet_date"           , "S" },
    { "auth_avs_response"           , "S" },
    { "auth_cvv2_response"          , "S" },
    { "auth_rec_id"                 , "L" },
    { "trident_tran_id"             , "S" },
    { "client_reference_number"     , "S" },
    { "cat_indicator"               , "S" },
    { "alm_code"                    , "S" },
    { "bsa_type"                    , "S" },
    { "bsa_id_code"                 , "S" },
    { "ird"                         , "S" },
    { "addendum_required"           , "S" },
    { "reference_number"            , "S" },
    { "acq_reference_number"        , "S" },
    { "purchase_id"                 , "S" },
    { "purchase_id_format"          , "S" },
    { "ecomm_security_level_ind"    , "S" },
    { "pos_data_code"               , "S" },
    { "program_registration_id"     , "S" },
    { "service_code"                , "I" },
    { "customer_code"               , "S" },
    { "merchant_tax_id"             , "S" },
    { "merchant_reference_number"   , "S" },
    { "statement_date_begin"        , "D" },
    { "statement_date_end"          , "D" },
    { "tax_amount"                  , "F" },
    { "tax_indicator"               , "S" },
    { "pos_entry_mode"              , "S" },
    { "card_present"                , "S" },
    { "moto_ecommerce_ind"          , "S" },
    { "recurring_payment_ind"       , "S" },
    { "multiple_clearing_seq_num"   , "I" },
    { "multiple_clearing_seq_count" , "I" },
    { "ship_from_zip"               , "S" },
    { "ship_to_zip"                 , "S" },
    { "dest_country_code"           , "S" },
    { "shipping_amount"             , "F" },
    { "cashback_amount"             , "F" },
    { "tip_amount"                  , "F" },
    { "duty_amount"                 , "F" },
    { "level_iii_data_present"      , "S" },
    { "eligibility_flags"           , "S" },
    { "mapping_service_ind"         , "S" },
    { "customer_service_phone"      , "S" },    
    { "total_room_tax"              , "F" },    
    { "hotel_fire_safety_act_ind"   , "S" },    
    { "renter_name"                 , "S" },    
    { "rental_return_city"          , "S" },    
    { "rental_return_state"         , "S" },    
    { "rental_return_country"       , "S" },    
    { "rental_return_location_id"   , "S" },    
    { "rate_daily_weekly_ind"       , "S" },    
    { "rate_daily"                  , "F" },    
    { "rental_location_city"        , "S" },    
    { "rental_location_state"       , "S" },    
    { "rental_location_country"     , "S" },    
    { "rental_location_id"          , "S" },    
    { "rental_class_id"             , "S" },    
    { "hotel_rental_days"           , "S" },    
    { "card_number_enc"             , "S" },
    { "last_modified_user"          , "S" },
    { "debit_type"                  , "S" },
    { "icc_data"                    , "S" },
    { "pos_terminal_id"             , "S" },
    { "tran_fee_amount"             , "F" }, 
    { "tic"                         , "S" },
    { "fx_conv_date"                , "T" },
  };
  
  private static final String[][] SettlementFieldsMCLineItem =
  {
    { "item_description"            , "S" },
    { "product_code"                , "S" },
    { "item_quantity"               , "F" },
    { "item_unit_measure"           , "S" },
    { "card_acceptor_tax_id"        , "I" },
    { "tax_rate"                    , "F" },
    { "tax_type_applied"            , "S" },
    { "tax_amount"                  , "F" },
    { "extended_item_amount"        , "F" },
    { "debit_credit_indicator"      , "S" },
    { "item_discount_amount"        , "F" },
    { "item_unit_price"             , "F" },
  };

  private static final String[][] SettlementFieldsAmex = 
  {
    { "airline_process_id"          , "S" },
    { "amex_se_number"              , "L" },
    { "approval_code"               , "S" },
    { "auth_rec_id"                 , "L" },
    { "auth_retrieval_ref_num"      , "S" },
    { "bank_number"                 , "S" },
    { "batch_id"                    , "L" },
    { "batch_number"                , "L" },
    { "batch_record_id"             , "L" },
    { "cardholder_reference_number" , "S" },
    { "card_number"                 , "S" },
    { "card_number_enc"             , "S" },
    { "carrier_code"                , "S" },
    { "carrier_name"                , "S" },
    { "client_reference_number"     , "S" },
    { "currency_code"               , "S" },
    { "dba_address"                 , "S" },
    { "dba_city"                    , "S" },
    { "dba_name"                    , "S" },
    { "dba_state"                   , "S" },
    { "dba_zip"                     , "S" },
    { "debit_credit_indicator"      , "S" },
    { "departure_date"              , "D" },
    { "doc_type"                    , "I" },
    { "fare_basis"                  , "S" },
    { "format_code"                 , "S" },
    { "funding_amount"              , "F" },
    { "funding_currency_code"       , "S" },
    { "hotel_rental_days"           , "I" },
    { "ic_cat"                      , "S" },
    { "issue_city"                  , "S" },
    { "level_iii_data_present"      , "S" },
    { "media_code"                  , "S" },
    { "merchant_batch_date"         , "D" },
    { "merchant_number"             , "L" },
    { "moto_ecommerce_ind"          , "S" },
    { "output_filename"             , "S" },
    { "passenger_name"              , "S" },
    { "phone_number"                , "S" },
    { "pos_data_code"               , "S" },
    { "pos_entry_mode"              , "S" },
    { "rate_daily"                  , "F" },
    { "reference_number"            , "L" },
    { "renter_name"                 , "S" },
    { "requester_name"              , "S" },
    { "roc_number"                  , "S" },
    { "settlement_date"             , "D" },
    { "shipped_to_zip"              , "S" },
    { "sic_code"                    , "S" },
    { "special_program_code"        , "S" },
    { "statement_date_begin"        , "D" },
    { "statement_date_end"          , "D" },
    { "statement_city_begin"        , "S" },
    { "statement_city_end"          , "S" },
    { "statement_region_begin"      , "S" },
    { "statement_region_end"        , "S" },
    { "statement_country_begin"     , "S" },
    { "statement_country_end"       , "S" },
    { "submission_method"           , "S" },
    { "tax_amount"                  , "F" },
    { "tax_type_code"               , "S" },
    { "ticket_number"               , "L" },
    { "tip_amount"                  , "F" },
    { "transaction_amount"          , "F" },
    { "transaction_date"            , "D" },
    { "transaction_id"              , "L" },
    { "tran_type"                   , "S" },
    { "trident_tran_id"             , "S" },
    { "icc_data"                    , "S" },
    { "iso_ecommerce_indicator"     , "S" },
    { "tran_fee_amount"             , "F" }, 
  };
  
  private static final String[][] SettlementFieldsAmexLineItem =
  {
    { "extended_item_amount"        , "F" },
    { "item_description"            , "S" },
    { "item_quantity"               , "F" },
  };

  private static final String[][] MerchantLevelDataUsageFields =
  {
	{ "merchant_number"            	, "S" },
	{ "card_type"               	, "S" },
	{ "batch_date"               	, "D" },
	{ "transaction_date"            , "D" },
  };
  private static final String[][] SettlementFieldsDiscover = 
  {
    { "acquirer_id"                 , "S" },
    { "alm_code"                    , "S" },
    { "approval_code"               , "S" },
    { "auth_date"                   , "D" },
    { "auth_time"                   , "T" },
    { "auth_rec_id"                 , "L" },
    { "auth_retrieval_ref_num"      , "S" },
    { "auth_source_code"            , "S" },
    { "bank_number"                 , "S" },
    { "batch_id"                    , "L" },
    { "batch_number"                , "L" },
    { "batch_record_id"             , "L" },
    { "card_number"                 , "S" },
    { "card_number_enc"             , "S" },
    { "card_program_id"             , "S" },
    { "card_type"                   , "S" },
    { "card_type_enhanced"          , "S" },
    { "client_code"                 , "S" },
    { "client_data"                 , "S" },
    { "client_prefix"               , "S" },
    { "client_reference_number"     , "S" },
    { "currency_code"               , "S" },
    { "dba_city"                    , "S" },
    { "dba_name"                    , "S" },
    { "dba_state"                   , "S" },
    { "dba_zip"                     , "S" },
    { "debit_credit_indicator"      , "S" },
    { "discover_merchant_number"    , "L" },
    { "funding_amount"              , "F" },
    { "funding_currency_code"       , "S" },
    { "ic_cat"                      , "S" },
    { "ic_cat_billing"              , "S" },
    { "ic_cat_downgrade"            , "S" },
    { "interchange_program"         , "S" },
    { "merchant_batch_date"         , "D" },
    { "merchant_number"             , "L" },
    { "moto_ecommerce_ind"          , "S" },
    { "network_reference_id"        , "S" },
    { "output_filename"             , "S" },
    { "pos_data"                    , "S" },
    { "pos_entry_mode"              , "S" },
    { "process_code"                , "S" },
    { "purchase_id"                 , "S" },
    { "reference_number"            , "L" },
    { "region_issuer"               , "S" },
    { "region_merchant"             , "S" },
    { "response_code"               , "S" },
    { "settlement_date"             , "D" },
    { "sic_code"                    , "S" },
    { "track_condition_code"        , "S" },
    { "transaction_amount"          , "F" },
    { "transaction_date"            , "D" },
    { "transaction_time"            , "T" },
    { "tran_type"                   , "S" },
    { "trident_tran_id"             , "S" },
    { "debit_type"                  , "S" },
    { "icc_data"                    , "S" },
    { "icc_data_2"                  , "S" },
    { "tran_fee_amount"             , "F" }, 
  };

  private static final String[][] SettlementFieldsModBII =
  {
    // field name                     type
    { "bin_number"                  , "S" },
    { "acquirer_business_id"        , "S" },
    { "bank_number"                 , "S" },
    { "merchant_number"             , "L" },
    { "dba_name"                    , "S" },
    { "dba_address"                 , "S" },
    { "dba_city"                    , "S" },
    { "dba_state"                   , "S" },
    { "dba_zip"                     , "S" },
    { "sic_code"                    , "S" },
    { "moto_ecommerce_merchant"     , "S" },
    { "mvv"                         , "S" },
    { "country_code"                , "S" },
    { "phone_number"                , "S" },
    { "card_number"                 , "S" },
    { "card_type"                   , "S" },
    { "card_type_enhanced"          , "S" },
    { "region_issuer"               , "S" },
    { "region_merchant"             , "S" },
    { "cash_disbursement"           , "S" },
    { "merchant_batch_date"         , "D" },
    { "batch_number"                , "S" },
    { "batch_id"                    , "L" },
    { "batch_record_id"             , "L" },
    { "transaction_date"            , "D" },
    { "debit_credit_indicator"      , "S" },
    { "transaction_amount"          , "F" },
    { "currency_code"               , "S" },
    { "funding_amount"              , "F" },
    { "funding_currency_code"       , "S" },
    { "ic_cat"                      , "S" },
    { "ic_cat_billing"              , "S" },
    { "ic_cat_downgrade"            , "S" },
    { "output_filename"             , "S" },
    { "auth_amount"                 , "F" },
    { "auth_amount_total"           , "F" },
    { "auth_currency_code"          , "S" },
    { "auth_code"                   , "S" },
    { "auth_source_code"            , "S" },
    { "auth_response_code"          , "S" },
    { "auth_date"                   , "D" },
    { "auth_returned_aci"           , "S" },
    { "auth_settled_aci"            , "S" },
    { "auth_retrieval_ref_num"      , "S" },
    { "auth_tran_id"                , "S" },
    { "auth_val_code"               , "S" },
    { "auth_avs_response"           , "S" },
    { "auth_cvv2_response"          , "S" },
    { "auth_rec_id"                 , "L" },
    { "trident_tran_id"             , "S" },
    { "client_reference_number"     , "S" },
    { "product_id"                  , "S" },
    { "cardholder_id_method"        , "S" },
    { "special_conditions_ind"      , "S" },
    { "fee_program_indicator"       , "S" },
    { "requested_payment_service"   , "S" },
    { "reimbursement_attribute"     , "S" },
    { "reference_number"            , "S" },
    { "acq_reference_number"        , "S" },
    { "purchase_id"                 , "S" },
    { "purchase_id_format"          , "S" },
    { "statement_date_begin"        , "D" },
    { "statement_date_end"          , "D" },
    { "tax_amount"                  , "F" },
    { "tax_indicator"               , "S" },
    { "pos_entry_mode"              , "S" },
    { "card_present"                , "S" },
    { "moto_ecommerce_ind"          , "S" },
    { "pos_term_cap"                , "S" },
    { "recurring_payment_ind"       , "S" },
    { "tech_ind"                    , "S" },
    { "multiple_clearing_seq_num"   , "I" },
    { "multiple_clearing_seq_count" , "I" },
    { "ship_to_zip"                 , "S" },
    { "shipping_amount"             , "F" },
    { "cashback_amount"             , "F" },
    { "tip_amount"                  , "F" },
    { "market_specific_data_ind"    , "S" },
    { "information_indicator"       , "S" },
    { "merchant_vol_indicator"      , "S" },
    { "ecommerce_goods_indicator"   , "S" },
    { "enhanced_data_1"             , "S" },
    { "enhanced_data_2"             , "S" },
    { "enhanced_data_3"             , "S" },
    { "level_iii_data_present"      , "S" },
    { "eligibility_flags"           , "S" },
    { "card_number_enc"             , "S" },
    { "last_modified_user"          , "S" },
    { "no_show_indicator"           , "S" },
    { "hotel_rental_days"           , "I" },
    { "extra_charges"               , "S" },
    { "rate_daily"                  , "F" },
    { "rate_weekly"                 , "F" },
    { "one_way_drop_off_charges"    , "F" },
    { "insurance_charges"           , "F" },
    { "fuel_charges"                , "F" },
    { "car_class_code"              , "S" },
    { "renter_name"                 , "S" },
    { "total_room_tax"              , "F" },
    { "folio_cash_advances"         , "F" },
    { "food_beverage_charges"       , "F" },
    { "prepaid_expenses"            , "F" },
    { "discount_amount"             , "F" },
    { "duty_amount"                 , "F" },
    { "ship_from_zip"               , "S" },
    { "dest_country_code"           , "S" },
    { "vat_reference_number"        , "S" },
    { "order_date"                  , "S" },
    { "shipping_tax_amount"         , "F" },
    { "shipping_tax_rate"           , "F" },
    { "issuer_bin"                  , "S" },
    { "customer_code"               , "S" },
    { "debit_type"                  , "S" },
    { "chip_condition_code"         , "S" },
    { "icc_data"                    , "S" },
    { "dest_id"                     , "I" },
    { "issuer_bank_id"              , "I" },
    { "cielo_product"               , "I" },
    { "settlement_days"             , "I" },
    { "pos_terminal_id"             , "S" },
    { "clearing_nsu"                , "S" },
    { "ic_expense"                  , "F" },
  };

  private static final String[][] InstallmentFields =
  {
    // field name                     type
    { "batch_id"                    , "L" },
    { "batch_record_id"             , "L" },
    { "batch_date"                  , "D" },
    { "card_type"                   , "S" },
    { "cielo_product"               , "I" },
    { "multiple_clearing_seq_num"   , "I" },
    { "multiple_clearing_seq_count" , "I" },
    { "transaction_amount"          , "F" },
    { "reference_number"            , "S" },
  };

  private   static final int      INS_UPD_SETTLEMENT_RECORD       = 0;
  private   static final int      INS_UPD_LINE_ITEM_DETAIL        = 1;
  private   static final int      INS_UPD_FEE_COLLECT_RECORD      = 2;
  private   static final int      INS_UPD_DDF_DT                  = 3;
  private   static final int      INS_UPD_DDF_EXT_DT              = 4;
  private   static final int      INS_UPD_INSTALLMENT             = 5;
  private   static final int      INS_UPD_DDF_DT_PROCESS          = 6;
  private   static final int      INS_UPD_LEVEL_DATA_USAGE        = 7;

  public static void insertSettlementRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_SETTLEMENT_RECORD   , true  );  }
  public void _insertSettlementRecord( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_SETTLEMENT_RECORD   , true  );  }
  public static void updateSettlementRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_SETTLEMENT_RECORD   , false );  }

  public static void insertLineItemDetail( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_LINE_ITEM_DETAIL    , true  );  }
  public void _insertLineItemDetail( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_LINE_ITEM_DETAIL    , true  );  }
  public void _insertMerchantLevelDataUsage( SettlementRecord rec )
	throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_LEVEL_DATA_USAGE    , true  );  }
  public static void updateLineItemDetail( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_LINE_ITEM_DETAIL    , false );  }

  public static void insertFeeCollectRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_FEE_COLLECT_RECORD  , true  );  }
  public void _insertFeeCollectRecord( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_FEE_COLLECT_RECORD  , true  );  }
  public static void updateFeeCollectRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_FEE_COLLECT_RECORD  , false );  }

  public void _insertDTRecord( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_DDF_DT              , true  );  }
  public static void updateDTRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_DDF_DT              , false );  }
  public void _insertExtDTRecord( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_DDF_EXT_DT          , true  );  }
  public static void updateExtDTRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_DDF_EXT_DT          , false );  }

  public void _insertInstallmentRecord( SettlementRecord rec )
    throws Exception                {                     _insertOrUpdateTable(rec, INS_UPD_INSTALLMENT         , true  );  }
  public static void updateInstallmentRecord( SettlementRecord rec )
    throws Exception                { new  SettlementDb()._insertOrUpdateTable(rec, INS_UPD_INSTALLMENT         , false );  }
  public static void insertDTProcessRecord(int processType, String workFilename, String batchDate )
		    throws Exception                { new  SettlementDb()._insertProcessTable(INS_UPD_DDF_DT_PROCESS, processType, workFilename, batchDate);  }
		 

    public void _insertProcessTable(int insUpdType, int processType, String workFilename, String batchDate) throws Exception {
        PreparedStatement ps = null;
        String tableName = null;
        String sqlText = null;
        try {
            connect(true);
            switch (insUpdType) {
            case INS_UPD_DDF_DT_PROCESS: {
                // @formatter:off
				sqlText = "insert into daily_detail_file_dt_process\n" + "       (  " + "         process_type,  "
						+ "         process_sequence,  " + "         batch_date,  " + "         load_filename  "
						+ "       )  " + "         values  " + "       (?,  0,  ?, ? )";
				// @formatter:on
            }

            }
            ps = getPreparedStatement(sqlText);
            ps.setInt(1, processType);
            ps.setString(2, batchDate);
            ps.setString(3, workFilename);
            ps.executeUpdate();
        }
        finally {
            try {
                ps.close();
            }
            catch (Exception ee) {
                logEntry("_insertProcessTable", ee);
            }
            cleanUp();
        }

    }
  public void _insertOrUpdateTable( SettlementRecord rec, int insUpdType, boolean isInsert )
    throws Exception
  {
    PreparedStatement       ps      = null;

    try
    {
      connect(true);

      boolean         endWithRecId        = true;
      String          sqlText             = null;
      String          qMarks              = "";
      String          insertSpecialFields = "";
      String          updateWhereClause   = " where rec_id = ? ";
      String          tableName           = null;
      String [][]     fieldNames          = null;
      int             fieldColumn         = 0;
      int             numItems            = 1;
      List            items               = null;
      FieldBean       item                = (FieldBean)rec;
      String          updateFields[][] = null;


      switch( insUpdType )
      {
        case INS_UPD_SETTLEMENT_RECORD:
          switch( rec.RecSettleRecType )
          {
            case SettlementRecord.SETTLE_REC_VISA   : tableName =                 "visa_settlement";  fieldNames = SettlementFieldsVisa;              break;
            case SettlementRecord.SETTLE_REC_MC     : tableName =                   "mc_settlement";  fieldNames = SettlementFieldsMC;                break;
            case SettlementRecord.SETTLE_REC_AMEX   : tableName =                 "amex_settlement";  fieldNames = SettlementFieldsAmex;              break;
            case SettlementRecord.SETTLE_REC_DISC   : tableName =             "discover_settlement";  fieldNames = SettlementFieldsDiscover;          break;
            case SettlementRecord.SETTLE_REC_MODBII : tableName =               "modbii_settlement";  fieldNames = SettlementFieldsModBII;            break;
          }
          qMarks                  =       " ?,?,?,?,"                 ;
          insertSpecialFields     =       "   test_flag,            " +
                                          "   batch_date,           " +
                                          "   load_file_id,         " +
                                          "   load_filename,        " +
                                          "   rec_id                " ;
          break;

        case INS_UPD_LINE_ITEM_DETAIL:
          items     = rec.getLineItems();
          numItems  = items.size();

          switch( rec.RecSettleRecType )
          {
            case SettlementRecord.SETTLE_REC_VISA   : tableName =      "visa_settlement_line_items";  fieldNames = SettlementFieldsVisaLineItem;      break;
            case SettlementRecord.SETTLE_REC_MC     : tableName =        "mc_settlement_line_items";  fieldNames = SettlementFieldsMCLineItem;        break;
            case SettlementRecord.SETTLE_REC_AMEX   : tableName =      "amex_settlement_line_items";  fieldNames = SettlementFieldsAmexLineItem;      break;
//@         case SettlementRecord.SETTLE_REC_DISC   : tableName =  "discover_settlement_line_items";  fieldNames = SettlementFieldsDiscoverLineItem;  break;
//@         case SettlementRecord.SETTLE_REC_MODBII : tableName =    "modbii_settlement_line_items";  fieldNames = SettlementFieldsModBIILineItem;    break;
          }
          qMarks                  =       " ?,"                       ;
          insertSpecialFields     =       "   line_item_id,         " +
                                          "   rec_id                " ;
          break;

        case INS_UPD_FEE_COLLECT_RECORD:
          switch( rec.RecSettleRecType )
          {
            case SettlementRecord.SETTLE_REC_VISA   : tableName =     "visa_settlement_fee_collect";  fieldNames = FeeCollectFieldsVisa;              break;
            case SettlementRecord.SETTLE_REC_MC     : tableName =       "mc_settlement_fee_collect";  fieldNames = FeeCollectFieldsMC;                break;
//@         case SettlementRecord.SETTLE_REC_AMEX   : tableName =     "amex_settlement_fee_collect";  fieldNames = FeeCollectFieldsAmex;              break;
//@         case SettlementRecord.SETTLE_REC_DISC   : tableName = "discover_settlement_fee_collect";  fieldNames = FeeCollectFieldsDiscover;          break;
//@         case SettlementRecord.SETTLE_REC_MODBII : tableName =   "modbii_settlement_fee_collect";  fieldNames = FeeCollectFieldsModBII;            break;
          }
          qMarks                  =       " ?,"                       ;
          insertSpecialFields     =       "   load_file_id,         " +
                                          "   rec_id                " ;
          fieldColumn             = 1;
          break;

        case INS_UPD_DDF_DT:
          tableName               = "daily_detail_file_dt";
          fieldNames              = DailyDetailFileDtFields;
          updateWhereClause       =       " where batch_date    = '" + DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"),"dd-MMM-yyyy")  +"'"+
                                          "   and batch_number  = " + rec.getLong("batch_id")                 +
                                          "   and ddf_dt_id     = " + rec.getLong("batch_record_id")          ;
          qMarks                  =       " ?,?,"                       ;
          insertSpecialFields     =       "   card_present,         " +
                                          "   load_file_id,         " +
                                          "   load_filename         " ;
          if (!isInsert) {
          updateFields = createUpdateFieldsDT(rec.RecSettleRecType);
          }
          endWithRecId            = false;
          fieldColumn             = 1;
          break;

        case INS_UPD_DDF_EXT_DT:
          tableName               = "daily_detail_file_ext_dt";
          fieldNames              = DailyDetailFileExtDtFields;
          updateWhereClause       =       " where row_sequence = " + rec.getLong("batch_record_id")           +
                                          "   and batch_number = " + rec.getLong("batch_id")                  ;
          qMarks                  =       " ?,"                       ;
          insertSpecialFields     =       "   load_file_id,         " +
                                          "   load_filename         " ;
          endWithRecId            = false;
          fieldColumn             = 1;
          break;

        case INS_UPD_INSTALLMENT:
          tableName               = "settlement_installments";
          fieldNames              = InstallmentFields;
          updateWhereClause       =       " where original_rec_id = " + rec.getLong("rec_id")                 +
                                          "   and batch_date     = '" + DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"),"dd-MMM-yyyy") + "'";
          qMarks                  =       " ?,?,?,"                   ;
          insertSpecialFields     =       "   load_file_id,         " +
                                          "   load_filename,        " +
                                          "   rec_id,               " +
                                          "   original_rec_id       " ;
          break;
          
        case INS_UPD_LEVEL_DATA_USAGE:
         tableName               = "merchant_level_data_usage";
         fieldNames              = MerchantLevelDataUsageFields;
         qMarks                  =       " ?,"                       ;
         insertSpecialFields     =       "   version,	            " +
        		 						 "   rec_id         		";

          break;
      }
      
      if ( log.isDebugEnabled() ) {
    	  log.debug("[_insertOrUpdateTable()] - tableName="+tableName+", insUpdType="+insUpdType+", isInsert="+isInsert);
      }

      if( tableName != null )
      {
        int i;

        if( isInsert )
        {
          sqlText =       " insert into " + tableName               +
                          " (                                     " ;

          for( i = 0; i < fieldNames.length; ++i )
          {
            sqlText +=        fieldNames[i][fieldColumn]        + "," ;
            qMarks  +=      " ?,"                                     ;
          }

          sqlText +=        insertSpecialFields                     +
                          " )                                     " +
                          " values ( " + qMarks + " ? )           " ;
        }
        else
        {
		  if (updateFields != null) {
			  fieldNames = updateFields;
		  }
          sqlText =       " update " + tableName                    +
                          " set                                   " ;

          for( i = 0; i < fieldNames.length-1; ++i )
          {
            sqlText +=        fieldNames[i][fieldColumn]  + " = ?,  " ;
          }

          sqlText +=        fieldNames[i][fieldColumn]  + " = ?   " +
                            updateWhereClause                       ;
        }
        if ( log.isDebugEnabled()) {
        	log.debug("[_insertOrUpdateTable()] - sqlText=\r\n"+sqlText);
        }
        ps = getPreparedStatement(sqlText);

        ++fieldColumn;

        for( int itemIdx = 0; itemIdx < numItems; ++itemIdx )
        {
          if( insUpdType == INS_UPD_LINE_ITEM_DETAIL )
          {
            item = (FieldBean)(items.get(itemIdx));
          }

          int offset = 1;
          for( i = 0; i < fieldNames.length; ++i, ++offset )
          {
            if( item.isFieldBlank( fieldNames[i][0] ) )
            {
              switch( fieldNames[i][fieldColumn].charAt(0) )
              {
                case 'S': ps.setNull  ( offset, java.sql.Types.VARCHAR  );                                    break;    // String

                case 'D':                                                                                               // Date,"MM/dd/yyyy"
                case 'T': ps.setNull  ( offset, java.sql.Types.DATE     );     
                    log.debug("[_insertOrUpdateTable()] Timestamp field : "+fieldNames[i][0]+" is blank");
                	break;    // Date with Time ,"MM/dd/yyyy HH:mm:ss"

                case 'F':                                                                                               // Float/Double
                case 'L':                                                                                               // Long
                case 'I': ps.setNull  ( offset, java.sql.Types.NUMERIC  );                                    break;    // Int

                default:  logEntry("_insertOrUpdateTable: invalid field type ", fieldNames[i][fieldColumn]);  break;
              }
            }
            else
            {
            	if ( log.isDebugEnabled()) {
            		String val = item.getField(fieldNames[i][0]) != null ? item.getField(fieldNames[i][0]).toString(): "null";
            		log.debug("[_insertOrUpdateTable()] - fieldName="+fieldNames[i][0]+", value="+val);
            	}
              switch( fieldNames[i][fieldColumn].charAt(0) )
              {
                case 'S': ps.setString    ( offset, item.getString (fieldNames[i][0],"",true ));              break;    // String

                case 'D': ps.setDate      ( offset, item.getSqlDate(fieldNames[i][0]         ));              break;    // Date
                case 'T': ps.setTimestamp ( offset, item.getSqlTimestamp(fieldNames[i][0]    ));              
                log.debug("[_insertOrUpdateTable()] Timestamp field : "+fieldNames[i][0]+" is "+item.getSqlTimestamp(fieldNames[i][0]));
                	break;    // Date with Time

                case 'F': ps.setDouble    ( offset, item.getDouble (fieldNames[i][0]         ));              break;    // Float/Double
                case 'L': ps.setLong      ( offset, item.getLong   (fieldNames[i][0]         ));              break;    // Long
                case 'I': ps.setInt       ( offset, item.getInt    (fieldNames[i][0]         ));              break;    // Int

                default:  logEntry("_insertOrUpdateTable: invalid field type ", fieldNames[i][fieldColumn]);  break;
              }
            }
          }

          if( isInsert )
          {
            switch( insUpdType )
            {
              case INS_UPD_SETTLEMENT_RECORD:
                // generate a new record id
                rec.setData("rec_id", getSettlementRecId() );

                ps.setString( offset++, rec.getString ( "test_flag"      ));
                ps.setDate  ( offset++, rec.getSqlDate( "batch_date"     ));
                ps.setLong  ( offset++, rec.getLong   ( "load_file_id"   ));
                ps.setString( offset++, rec.getString ( "load_filename"  ));
                break;

              case INS_UPD_LINE_ITEM_DETAIL:
                ps.setLong  ( offset++, itemIdx                           );  // line_item_id
                break;

              case INS_UPD_FEE_COLLECT_RECORD:
                // generate a new record id
                rec.setData("rec_id", getSettlementRecId() );

                ps.setLong  ( offset++, 0                                 );  // load_file_id
                break;

              case INS_UPD_DDF_DT:
            	  //find the value for card_present
            	  String card_present = "Y";       
            	  //AMEX
            	  if (("AM").equals(rec.getString("card_type")) && rec.getString("pos_data_code") != null) {
            		  try {
            			  if (rec.getString("pos_data_code").substring(5, 6).equals("0")) {
                    		  card_present = "N";             				  
            			  }
            		  } catch (Exception e)  {
            		      logEntry("_insertOrUpdateTable - card_present - AMEX pos_data_code=" + rec.getString("pos_data_code"),e.toString());
            		  }
            	  //Discover
            	  } else if (("DS").equals(rec.getString("card_type")) && rec.getString("pos_data") != null) {
            		  try {
            			  if (rec.getString("pos_data").substring(4, 5).equals("1")) {
                    		  card_present = "N";             				  
            			  }
            		  } catch (Exception e)  {
            		      logEntry("_insertOrUpdateTable -card_present- Discover pos_data=" + rec.getString("pos_data"),e.toString());
            		  }
            	  //Visa and MasterCard
                  } else if (rec.getString("card_present") != null) {
                	  card_present = rec.getString("card_present");
                  }
                  ps.setString( offset++, card_present);
              case INS_UPD_DDF_EXT_DT:
                ps.setLong  ( offset++, rec.getLong   ( "load_file_id"   ));
                ps.setString( offset++, rec.getString ( "load_filename"  ));
                break;

              case INS_UPD_INSTALLMENT:
                ps.setLong  ( offset++, rec.getLong   ( "load_file_id"   ));
                ps.setString( offset++, rec.getString ( "load_filename"  ));
                ps.setLong  ( offset++, 0L                                );  // rec_id
                break;
                
              case INS_UPD_LEVEL_DATA_USAGE:
                  ps.setString( offset++, rec.ioVersion);

                break;
            }
          }
          if( endWithRecId )
          {
            ps.setLong  ( offset++, rec.getLong   ( "rec_id"         ));
          }
          ps.executeUpdate();
        }
      }
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
  }
  
  public static void insertRejectRecord( SettlementRecord rec )
  {
    new SettlementDb()._insertRejectRecord(rec);
  }
  
  public void _insertRejectRecord( SettlementRecord rec )
  {
    PreparedStatement       ps          = null;

    try
    {
      connect(true);

      String sqlText = 
        " insert into REJECT_RECORD   " +
        " (                           " +
        "   rec_id,                   " +
        "   reject_id,                " +
        "   reject_type,              " +
        "   status,                   " +
        "   round,                    " +
        "   reject_date,              " +
        "   merchant_number,          " +
        "   presentment,              " +
        "   external                  " +
        " )                           " +
        " values                      " +
        " ( ?, ?, ?, -1, 1, ?, ?, 0, ? ) ";
    
      ps = getPreparedStatement(sqlText);
      ps.setLong  ( 1 , rec.getLong("rec_id")                         );
      ps.setString( 2 , "mbs"+rec.getData("reject_reason")            );
      ps.setString( 3 , rec.RecCardType                               );
      ps.setDate  ( 4,  rec.getSqlDate("settlement_date")             );
      ps.setString( 5 , rec.getString("merchant_number")              );
      ps.setString( 6 , rec.getString("external_reject")              );
      ps.executeQuery();

      QueueTools.insertQueue(rec.getLong("rec_id"), MesQueues.Q_REJECTS_UNWORKED);
    }
    catch( Exception e )
    {
      logEntry("_insertRejectRecord(" + rec.getLong("rec_id") + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static long resolveMerchantId( int bankNumber, String cardNum, String refNum, String tranId, boolean partialCardOk )
  {
    return( new SettlementDb()._resolveMerchantId(bankNumber, cardNum, refNum, tranId, partialCardOk) );
  }
  
  public static long resolveMerchantId( int bankNumber, String cardNum, String refNum, String tranId )
  {
    return( new SettlementDb()._resolveMerchantId(bankNumber, cardNum, refNum, tranId, false) );
  }
  
  public long _resolveMerchantId( int bankNumber, String cardNum, String refNum, String tranId, boolean partialCardOk )
  {
    long                merchantId      = 0L;
    PreparedStatement   ps              = null;
    ResultSet           rs              = null;
    
    try
    {
      java.sql.Date[] batchDates  = new java.sql.Date[2];
      Calendar        cal         = Calendar.getInstance();
      String          cardNumber  = TridentTools.encodeCardNumber(cardNum);
      String          cardType    = TridentTools.decodeVisakCardType(cardNum);
      java.sql.Date   cpd         = null;
      int             high        = 0;
      int             low         = 0;
      int             matchCount  = 0;
      String          revTranId   = null;
      
      connect(true);
      
      if ( tranId != null ) 
      {
        if ( "MC".equals(cardType) )
        {
          if ( tranId.length() != 13 || tranId.startsWith("999999999") || tranId.startsWith("000000000") )
          {
            tranId    = null;
            revTranId = null;
          }           
          else
          {
            revTranId = (tranId.substring(9) + tranId.substring(0,9));
          }
        }
        else if ( "VS".equals(cardType) )
        {
          if ( tranId.length() != 15 || "000000000000000".equals(tranId) || "999999999999999".equals(tranId) )
          {
            tranId = null;
          }            
        }
      }
      
      // if we are accepting a partial card number, establish the lookup string
      if ( partialCardOk ) { cardNumber = cardNumber.substring(0,12) + "%"; }
      
      // decode the central processing date for the first presentment
      String sqlText =
        " select  yddd_to_date(substr(?,8,4))  as cpd " + crlf + 
        " from    dual                                ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,refNum);
      rs = ps.executeQuery();
      rs.next();
      cpd = rs.getDate("cpd");
      rs.close();
      ps.close();
      
      sqlText = 
        " select  distinct                                            " + crlf +
        "         dt.merchant_account_number      as merchant_number  " + crlf + 
        " from    daily_detail_file_dt  dt                            " + crlf + 
        " where   dt.merchant_account_number in                       " + crlf + 
        "         (                                                   " + crlf + 
        "           select  mf.merchant_number                        " + crlf + 
        "           from    mif     mf                                " + crlf + 
        "           where   mf.bank_number = ?                        " + crlf +
        "         )                                                   " + crlf + 
        "         and dt.batch_date between ? and ?                   " + crlf + 
        "         and dt.reference_number  = ?                        " + crlf +
        "         and dt.cardholder_account_number like ?             ";
        
      if ( tranId != null )
      {
        sqlText += " and (dt.trans_id_visa is null or dt.trans_id_visa = ? or dt.trans_id_visa = ?)";
      }
      ps = getPreparedStatement(sqlText);
      
      // attempt to locate the cleared transaction in the ddf table
      // it is necessary to use ddf because incoming chargebacks from
      // first presentment cleared by TSYS will not be in the mastercard
      // settlement table.
      for( int attempt = 0; attempt < 6; ++attempt )
      {
        switch( attempt )
        {
          case 0: low =   0;  high =   0;  break;   // same day as ref # cpd
          case 1: low =   1;  high =   3;  break;   // next day to 3 days in future
          case 2: low =  -3;  high =  -1;  break;   // 3 days before to 1 day before
          case 3: low =  -6;  high =  -4;  break;   // 6 days before to 4 days before
          case 4: low =  -9;  high =  -7;  break;   // 9 days before to 7 days before
          case 5: low = -12;  high = -10;  break;   // 12 days before to 10 days before
        }
        
        // build the date range values
        for( int dateIdx = 0; dateIdx < 2; ++dateIdx )
        {
          cal.setTime(cpd);
          cal.add(Calendar.DAY_OF_MONTH,((dateIdx == 0) ? low : high));
          batchDates[dateIdx] = new java.sql.Date(cal.getTime().getTime());
        }
        
        System.out.println("  " + batchDates[0] + " " + batchDates[1]);//@
        
        ps.setInt   (1,bankNumber);
        ps.setDate  (2,batchDates[0]);
        ps.setDate  (3,batchDates[1]);
        ps.setString(4,refNum);
        ps.setString(5,cardNumber);
        if ( tranId != null ) 
        { 
          ps.setString(6,tranId);
          ps.setString(7,revTranId); 
        }
        rs = ps.executeQuery();
        
        matchCount = 0;
        while ( rs.next() )
        {
          merchantId = rs.getLong("merchant_number");
          matchCount++;
        }
        rs.close();
        
        if ( matchCount != 1 )  { merchantId = 0L;  } // too many matches to be certain
        if ( merchantId != 0L ) { break;            } // have mid, exit loop
      }
    }
    catch(Exception e)
    {
      logEntry( "resolveMerchantId(" + refNum + ")",e.toString() );
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( merchantId );
  }
  
  private void setPsDate( PreparedStatement ps, int fieldNumber, java.util.Date dateValue )
    throws java.sql.SQLException
  {
    if ( dateValue == null )
    {
      ps.setNull(fieldNumber,java.sql.Types.DATE);
    }
    else
    {
      ps.setDate(fieldNumber,new java.sql.Date(dateValue.getTime()));
    }
  }
  
  private void setPsTimestamp( PreparedStatement ps, int fieldNumber, java.util.Date dateValue )
    throws java.sql.SQLException
  {
    if ( dateValue == null )
    {
      ps.setNull(fieldNumber,java.sql.Types.TIMESTAMP);
    }
    else
    {
      ps.setTimestamp(fieldNumber,new java.sql.Timestamp(dateValue.getTime()));
    }
  }


//////////////
//////////////    ARDEF table functions
//////////////

  public void _clearArdefData( String tableName, boolean enabled )
  {
    PreparedStatement     ps          = null;
  
    try
    {
      connect(true);
      ps = getPreparedStatement("delete from " + tableName + " where enabled = ?");
      ps.setString(1,(enabled ? "Y" : "N"));
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_clearArdefData(" + tableName + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void  clearArdefDataDiscover(boolean enabled)  { new SettlementDb()._clearArdefDataDiscover(enabled);                           }
  public        void _clearArdefDataDiscover(boolean enabled)  { new SettlementDb()._clearArdefData( "discover_ardef_table"          ,enabled); }
  public static void  clearArdefDataMC(boolean enabled)        { new SettlementDb()._clearArdefDataMC(enabled);                                 }
  public        void _clearArdefDataMC(boolean enabled)        { new SettlementDb()._clearArdefData(       "mc_ardef_table"          ,enabled); }
  public static void  clearArdefDataModBII(boolean enabled)    { new SettlementDb()._clearArdefDataModBII(enabled);                             }
  public        void _clearArdefDataModBII(boolean enabled)    { new SettlementDb()._clearArdefData(   "modbii_ardef_table"          ,enabled); }
  public static void  clearArdefDataVisa(boolean enabled)      { new SettlementDb()._clearArdefDataVisa(enabled);                               }
  public        void _clearArdefDataVisa(boolean enabled)      { new SettlementDb()._clearArdefData(     "visa_ardef_table"          ,enabled); }
  public static void  clearRegArdefDataVisa(boolean enabled)   { new SettlementDb()._clearRegArdefDataVisa(enabled);                            }
  public        void _clearRegArdefDataVisa(boolean enabled)   { new SettlementDb()._clearArdefData(     "visa_ardef_table_regulated",enabled); }
  public static void  clearArdefDataAmex(boolean enabled)      { new SettlementDb()._clearArdefDataAmex(enabled);                               }
  public        void _clearArdefDataAmex(boolean enabled)      { new SettlementDb()._clearArdefData(     "amex_ardef_optblue_table"          ,enabled); }
  

  public static ArdefDataDiscover loadArdefDiscover( String cardNumber )
  {
    return( new SettlementDb()._loadArdefDiscover(cardNumber) );
  }
  
  public ArdefDataDiscover _loadArdefDiscover( String cardNumber )
  {
    ArdefDataDiscover     data        = null;
    PreparedStatement     ps          = null;
    
    try
    {
      data = new ArdefDataDiscover();
      
      connect(true);
      
      String sqlText = 
         " select *                                   " + 
         " from   discover_ardef_table      ard       " +
         " where  substr(?,1,ard.iin_prefix_length) = " +
         "          ard.iin_prefix                    " +
         "        and ? between                       " +
         "              ard.account_number_length_min " +
         "          and ard.account_number_length_max " +
         "        and ard.enabled = 'Y'               " +
         " order by ard.iin_prefix_length desc        ";
         
      ps = getPreparedStatement(sqlText);
      ps.setString(1,cardNumber);
      ps.setInt(2,cardNumber.length());
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        data.setAccountLevelInd(rs.getInt("account_level_indicator"));
        data.setAtmInd(rs.getInt("atm_indicator"));
        data.setCardNumberLengthMin(rs.getInt("account_number_length_min"));
        data.setCardNumberLengthMax(rs.getInt("account_number_length_max"));
        data.setEffectiveDate(rs.getDate("effective_date"));
        data.setForeignCardInd(rs.getInt("foreign_card_indicator"));
        data.setIssuerCountryCode(rs.getString("issuer_country_code"));
        data.setIssuingNetwork(rs.getInt("issuing_network"));
        data.setPrefixLength(rs.getInt("iin_prefix_length"));
        data.setPrefix(rs.getString("iin_prefix"));
        data.setCardProductId(rs.getString("card_product_id"));
        data.setDebitType(rs.getInt("debit_type"));
        data.setStipEligible(rs.getInt("stip_eligible"));
        data.setLoadFilename(rs.getString("load_filename"));
        data.setLoadFileId(rs.getLong("load_file_id"));
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadArdefDiscover()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( data );
  }
  
  public static ArdefDataMC loadArdefMC( String cardNumber )
  {
    return( new SettlementDb()._loadArdefMC(cardNumber) );
  }
  
  public ArdefDataMC _loadArdefMC( String cardNumber )
  {
    ArdefDataMC           data        = null;
    PreparedStatement     ps          = null;
    
    try
    {
      data = new ArdefDataMC();
      
      connect(true);
      
      String sqlText = 
         " select ard.*                                       " + crlf + 
         " from   mc_ardef_table      ard                     " + crlf + 
         " where  to_number(rpad(?,19,'0'))                   " + crlf + 
         "          between ard.range_low and ard.range_high  " + crlf + 
         "        and ard.active_inactive_code = 'A'          " + crlf + 
         "        and ard.enabled = 'Y'                       " + crlf + 
         " order by priority_code                             ";
         
      ps = getPreparedStatement(sqlText);
      ps.setString(1,cardNumber);
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        data.setEffectiveTs( rs.getTimestamp("effective_timestamp") );
        data.setTableId( rs.getString("table_id") );
        data.setRangeLow( rs.getLong("range_low") );
        data.setRangeHigh( rs.getLong("range_high") );
        data.setActiveInactiveCode( rs.getString("active_inactive_code") );
        data.setGCMSProductId( rs.getString("gcms_product_id") );
        data.setCardProgramId( rs.getString("card_program_id") );
        data.setPriorityCode( rs.getInt ("priority_code") );
        data.setMemberId( rs.getLong("member_id") );
        data.setProductTypeId( rs.getString("product_type_id") );
        data.setDebitType( rs.getInt("debit_type") );
        data.setEndpoint( rs.getString("endpoint") );
        data.setCountryCodeAlpha( rs.getString("country_code_alpha") );
        data.setCountryCodeNumeric( rs.getString("country_code_numeric") );
        data.setRegion( rs.getString("region") );
        data.setProductClass( rs.getString("product_class") );
        data.setTransactionRoutingInd( rs.getString("transaction_routing_ind") );
        data.setFirstPresentmentReassignSwitch( rs.getString("first_presentment_reassign_sw") );
        data.setProductReassignSwitch( rs.getString("product_reassignment_switch") );
        data.setPWCBOptInSwitch( rs.getString("pwcb_opt_in_switch") );
        data.setLicensedProductId( rs.getString("licensed_product_id") );
        data.setMappingServiceInd( rs.getString("mapping_service_ind") );
        data.setAccountLevelInd( rs.getString("account_level_ind") );
        data.setAccountLevelActivationDate( rs.getDate("account_level_activation_date") );
        data.setBillingCurrencyDefault( rs.getString("ch_billing_currency_default") );
        data.setBillingCurrencyExpDefault( rs.getInt ("ch_billing_currency_exp_deflt") );
        data.setBillingPrimaryTranCurrency_1( rs.getString("ch_billing_prim_trn_currency_1") );
        data.setBillingPrimaryCurrency_1( rs.getString("ch_billing_prim_currency_1") );
        data.setBillingPrimaryCurrencyExp_1( rs.getInt ("ch_billing_prim_currency_exp_1") );
        data.setBillingPrimaryTranCurrency_2( rs.getString("ch_billing_prim_trn_currency_2") );
        data.setBillingPrimaryCurrency_2( rs.getString("ch_billing_prim_currency_2") );
        data.setBillingPrimaryCurrencyExp_2( rs.getInt ("ch_billing_prim_currency_exp_2") );
        data.setBillingPrimaryTranCurrency_3( rs.getString("ch_billing_prim_trn_currency_3") );
        data.setBillingPrimaryCurrency_3( rs.getString("ch_billing_prim_currency_3") );
        data.setBillingPrimaryCurrencyExp_3( rs.getInt ("ch_billing_prim_currency_exp_3") );
        data.setBillingPrimaryTranCurrency_4( rs.getString("ch_billing_prim_trn_currency_4") );
        data.setBillingPrimaryCurrency_4( rs.getString("ch_billing_prim_currency_4") );
        data.setBillingPrimaryCurrencyExp_4( rs.getInt ("ch_billing_prim_currency_exp_4") );
        data.setChipToMagConvServiceInd( rs.getString("chip_to_mag_conv_service_ind") );
        data.setFloorExpDate( rs.getDate("floor_expiration_date") );
        data.setCoBrandParticipationSwitch( rs.getString("co_brand_participation_switch") );
        data.setSpendControlSwitch( rs.getString("spend_control_switch") );
        data.setLoadFilename( rs.getString("load_filename") );
        data.setLoadFileId( rs.getLong("load_file_id") );
        data.setMoneySendIndicator( rs.getString("money_send_ind") );
        data.setDurbinRegulatedRateInd( rs.getString("durbin_reg_rate_ind") );
      }
      rs.close();
    }
    catch( java.sql.SQLException se ){
    	if(cardNumber == null){
    		log.error("_loadArdefMC() received empty/null card number" );
    	}else{
    		log.error("_loadArdefMC() received invalid card number: " + (cardNumber.length()>=6?cardNumber.substring(0,6):cardNumber));
    	}
    	logEntry("_loadArdefMC()",se.toString());
    }
    catch( Exception e )
    {
      logEntry("_loadArdefMC()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( data );
  }
  
  public static ArdefDataModBII loadArdefModBII( String cardNumber )
  {
    return( new SettlementDb()._loadArdefModBII(cardNumber) );
  }
  
  public ArdefDataModBII _loadArdefModBII( String cardNumber )
  {
    ArdefDataModBII       data        = null;
    PreparedStatement     ps          = null;
    
    try
    {
      data = new ArdefDataModBII();
      
      connect(true);
      
      String sqlText = 
         " select ard.*                                       " + crlf +
         " from   modbii_ardef_table  ard                     " + crlf +
         " where  to_number(rpad(?,19,'0'))                   " + crlf +
         "          between ard.range_low and ard.range_high  " + crlf +
         "        and sysdate between ard.valid_date_begin    " + crlf +
         "                        and ard.valid_date_end      " + crlf +
         "        and ard.enabled = 'Y'                       ";
         
      ps = getPreparedStatement(sqlText);
      ps.setString(1,cardNumber);
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        data.setArdefCountryCode        ( rs.getString  ("country_code")            );
        data.setCardAssociation         ( rs.getInt     ("card_association")        );
        data.setCardType                ( rs.getString  ("card_type")               );
        data.setClearingBankIdCredit    ( rs.getString  ("clearing_bank_id_cr")     );
        data.setClearingBankIdDebit     ( rs.getString  ("clearing_bank_id_db")     );
        data.setDestIdCredit            ( rs.getInt     ("dest_id_cr")              );
        data.setDestIdDebit             ( rs.getInt     ("dest_id_db")              );
        data.setEndpoint                ( rs.getString  ("endpoint")                );
        data.setEndpointEnabled         ( rs.getString  ("endpoint_enabled")        );
        data.setFundingSource           ( rs.getString  ("funding_source")          );
        data.setIssuerBankIdCredit      ( rs.getString  ("issuer_bank_id_cr")       );
        data.setIssuerBankIdDebit       ( rs.getString  ("issuer_bank_id_db")       );
        data.setLastModifiedUser        ( rs.getString  ("last_modified_user")      );
        data.setMultiBankFlag           ( rs.getString  ("multi_bank_flag")         );
        data.setRangeHigh               ( rs.getLong    ("range_high")              );
        data.setRangeLow                ( rs.getLong    ("range_low")               );
        data.setValidDateBegin          ( rs.getDate    ("valid_date_begin")        );
        data.setValidDateEnd            ( rs.getDate    ("valid_date_end")          );
      }
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("_loadArdefModBII()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( data );
  }
  
  public static ArdefDataVisa loadArdefVisa( String cardNumber )
  {
    return( new SettlementDb()._loadArdefVisa(cardNumber) );
  }
  
  public ArdefDataVisa _loadArdefVisa( String cardNumber )
  {
    ArdefDataVisa         data        = null;
    PreparedStatement     ps          = null;
    
    try
    {
      data = new ArdefDataVisa();
      
      connect(true);
      
      String sqlText = 
         " select *                                           " + 
         " from   visa_ardef_table      ard                   " +
         " where  ? between ard.range_low and ard.range_high  " +
         "        and ard.account_number_length in (0,?)      " +
         "        and ard.enabled = 'Y'                       " + 
         "        and rownum = 1                              ";
         
      ps = getPreparedStatement(sqlText);
      ps.setString(1,cardNumber.substring(0,9));
      ps.setInt(2,cardNumber.length());
      ResultSet rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        data.setRangeHigh                   ( rs.getLong  ("range_high")                  );
        data.setRangeLow                    ( rs.getLong  ("range_low")                   );
        data.setIssuerCountryCode           ( rs.getString("issuer_country_code")         );
        data.setIssuerBin                   ( rs.getString("issuer_bin")                  );
        data.setCheckDigitAlgorithm         ( rs.getInt   ("check_digit_algorithm")       );
        data.setAccountNumberLength         ( rs.getInt   ("account_number_length")       );
        data.setProcessorBin                ( rs.getString("processor_bin")               );
        data.setDebitType                   ( rs.getInt   ("debit_type")                  );
        data.setDomain                      ( rs.getString("domain")                      );
        data.setIssuerVisaRegion            ( rs.getInt   ("issuer_visa_region")          );
        data.setLargeTicket                 ( rs.getString("large_ticket")                );
        data.setTechnologyIndicator         ( rs.getString("technology_indicator")        );
        data.setArdefRegion                 ( rs.getInt   ("ardef_region")                );
        data.setArdefCountryCode            ( rs.getString("ardef_country_code")          );
        data.setCommCardLevelIISupport      ( rs.getString("comm_card_level_ii_support")  );
        data.setCommCardLevelIIISupport     ( rs.getString("comm_card_level_iii_support") );
        data.setCommCardPosPromptInd        ( rs.getString("comm_card_pos_prompt_ind")    );
        data.setVatEvidence                 ( rs.getString("vat_evidence")                );
        data.setOriginalCredit              ( rs.getString("original_credit")             );
        data.setALPInd                      ( rs.getString("alp_ind")                     );
        data.setOriginalCreditMoneyTransfer ( rs.getString("original_credit_money_xfer")  );
        data.setOriginalCreditGambling      ( rs.getString("original_credit_gambling")    );
        data.setProductId                   ( rs.getString("product_id")                  );
        data.setComboCard                   ( rs.getString("combo_card")                  );
        data.setFastFunds                   ( rs.getString("fast_funds")                  );
        data.setFundingSource               ( rs.getString("funding_source")              );
        data.setSettlementMatch             ( rs.getString("settlement_match")            );
        data.setTravelAccount               ( rs.getString("travel_account")              );
        data.setRestrictedUse               ( rs.getString("restricted_use")              );
        data.setTestIndicator               ( rs.getString("test_indicator")              );
        data.setNNSSIndicator               ( rs.getString("nnss_indicator")              );
        data.setProductSubtype              ( rs.getString("product_subtype")             );
      }
      rs.close();
      ps.close();
      
      sqlText = 
         " select *                                           " + 
         " from   visa_ardef_table_regulated  ard             " +
         " where  rpad(substr(?,1,18),18,'0')                 " + 
         "          between ard.range_low and ard.range_high  " +
         "        and ard.enabled = 'Y'                       ";
         
      ps = getPreparedStatement(sqlText);
      ps.setString(1,cardNumber);
      rs = ps.executeQuery();
      data.setDebitType( rs.next() ? 1 : 0 );
      rs.close();
      ps.close();
    }
    catch( java.sql.SQLException se ){
    	if(cardNumber == null){
    		log.error("_loadArdefVisa() received empty/null card number ");
    	}else{
    		log.error("_loadArdefVisa() received invalid card number: " + (cardNumber.length()>=6?cardNumber.substring(0,6):cardNumber));
    	}
    	logEntry("_loadArdefVisa()",se.toString());
    }
    catch( Exception e )
    {
      logEntry("_loadArdefVisa()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( data );
  }


  public static ArdefDataDiscover parseArdefDataDiscover( String rawData )
  {
    ArdefDataDiscover   data = null;
    FlatFileRecord      ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_ARDEF);
    
    data = new SettlementDb()._parseArdefDataDiscover(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public ArdefDataDiscover _parseArdefDataDiscover( String rawData, FlatFileRecord ffd )
  {
    ArdefDataDiscover       data    = null;
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new ArdefDataDiscover();
      data.setAccountLevelInd(ffd.getFieldAsInt("account_level_indicator"));
      data.setAtmInd(ffd.getFieldAsInt("atm_indicator"));
      data.setCardNumberLengthMin(ffd.getFieldAsInt("account_number_length_min"));
      data.setCardNumberLengthMax(ffd.getFieldAsInt("account_number_length_max"));
      data.setForeignCardInd(ffd.getFieldAsInt("foreign_card_indicator"));
      data.setIssuerCountryCode(ffd.getFieldData("issuer_country_code"));
      data.setIssuingNetwork(ffd.getFieldAsInt("issuing_network"));
      data.setPrefixLength(ffd.getFieldAsInt("iin_prefix_length"));
      data.setPrefix(ffd.getFieldData("iin_prefix"));
      data.setCardProductId(ffd.getFieldData("card_product_id"));
      data.setStipEligible(ffd.getFieldAsInt("stip_eligible"));
      data.setDebitType(ffd.getFieldAsInt("regulation_indicator"));
    }
    catch( Exception e )
    {
      logEntry("_parseArdefDataDiscover()",e.toString());
    }
    finally
    {
    }
    return(data);
  }
  
  public static AmexOptBlueBinRange parseArdefDataAmexOptBlue( String rawData )
  {
	 AmexOptBlueBinRange   data = null;
    FlatFileRecord      ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_ARDEF);
    
    data = new SettlementDb()._parseArdefDataAmexOptBlue(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public AmexOptBlueBinRange _parseArdefDataAmexOptBlue( String rawData, FlatFileRecord ffd )
  {
	  AmexOptBlueBinRange       data    = null;
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new AmexOptBlueBinRange();
      data.setAmexCmBin(ffd.getFieldData("amex_cb_bin").trim());
      data.setAmexProductCode(ffd.getFieldData("product_code"));
      data.setEnabled("N");
    }
    catch( Exception e )
    {
      logEntry("_parseArdefDataAmexOptBlue()",e.toString());
    }
    finally
    {
    }
    return(data);
  }
  
  
  public static ArdefDataMC parseArdefDataMC( String rawData )
  {
    ArdefDataMC    data = null;
    FlatFileRecord ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MC_ARDEF);
    
    data = new SettlementDb()._parseArdefDataMC(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public ArdefDataMC _parseArdefDataMC( String rawData, FlatFileRecord ffd )
  {
    ArdefDataMC         data        = null;
    
    HashMap debitTypeMap = 
      new HashMap()
      {
        {
          put("B","2");
          put("1","1");
          put("N","0");
        }
      };
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new ArdefDataMC();
      data.setEffectiveTs(ffd.getFieldAsDate("effective_timestamp"));
      data.setActiveInactiveCode(ffd.getFieldData("active_inactive_code"));
      data.setTableId(ffd.getFieldData("table_id"));
      data.setRangeLow(ffd.getFieldAsLong("range_low"));
      data.setGCMSProductId(ffd.getFieldData("gcms_product_id"));
      data.setRangeHigh(ffd.getFieldAsLong("range_high"));
      data.setCardProgramId(ffd.getFieldData("card_program_id"));
      data.setPriorityCode(ffd.getFieldAsInt("priority_code"));
      data.setMemberId(ffd.getFieldAsLong("member_id"));
      data.setProductTypeId(ffd.getFieldData("product_type_id"));
      data.setEndpoint(ffd.getFieldData("endpoint"));
      data.setCountryCodeAlpha(ffd.getFieldData("country_code_alpha"));
      data.setCountryCodeNumeric(ffd.getFieldData("country_code_numeric"));
      data.setRegion(ffd.getFieldData("region"));
      data.setProductClass(ffd.getFieldData("product_class"));
      data.setTransactionRoutingInd(ffd.getFieldData("transaction_routing_ind"));
      data.setFirstPresentmentReassignSwitch(ffd.getFieldData("first_presentment_reassignment_switch"));
      data.setProductReassignSwitch(ffd.getFieldData("product_reassignment_switch"));
      data.setPWCBOptInSwitch(ffd.getFieldData("pwcb_opt_in_switch"));
      data.setLicensedProductId(ffd.getFieldData("licensed_product_id"));
      data.setMappingServiceInd(ffd.getFieldData("mapping_service_ind"));
      data.setAccountLevelInd(ffd.getFieldData("account_level_ind"));
      data.setAccountLevelActivationDate(ffd.getFieldAsDate("account_level_activation_date"));
      data.setBillingCurrencyDefault(ffd.getFieldData("ch_billing_currency_default"));
      data.setBillingCurrencyExpDefault(ffd.getFieldAsInt("ch_billing_currency_exp_default"));
      data.setBillingPrimaryTranCurrency_1(ffd.getFieldData("ch_billing_primary_tran_currency_1"));
      data.setBillingPrimaryCurrency_1(ffd.getFieldData("ch_billing_primary_currency_1"));
      data.setBillingPrimaryCurrencyExp_1(ffd.getFieldAsInt("ch_billing_primary_currency_exp_1"));
      data.setBillingPrimaryTranCurrency_2(ffd.getFieldData("ch_billing_primary_tran_currency_2"));
      data.setBillingPrimaryCurrency_2(ffd.getFieldData("ch_billing_primary_currency_2"));
      data.setBillingPrimaryCurrencyExp_2(ffd.getFieldAsInt("ch_billing_primary_currency_exp_2"));
      data.setBillingPrimaryTranCurrency_3(ffd.getFieldData("ch_billing_primary_tran_currency_3"));
      data.setBillingPrimaryCurrency_3(ffd.getFieldData("ch_billing_primary_currency_3"));
      data.setBillingPrimaryCurrencyExp_3(ffd.getFieldAsInt("ch_billing_primary_currency_exp_3"));
      data.setBillingPrimaryTranCurrency_4(ffd.getFieldData("ch_billing_primary_trans_currency_4"));
      data.setBillingPrimaryCurrency_4(ffd.getFieldData("ch_billing_primary_currency_4"));
      data.setBillingPrimaryCurrencyExp_4(ffd.getFieldAsInt("ch_billing_primary_currency_exp_4"));
      data.setChipToMagConvServiceInd(ffd.getFieldData("chip_to_mag_conversion_service_ind"));
      data.setFloorExpDate(ffd.getFieldAsDate("floor_expiration_date"));
      data.setCoBrandParticipationSwitch(ffd.getFieldData("co_brand_participation_switch"));
      data.setSpendControlSwitch(ffd.getFieldData("spend_control_switch"));
      data.setDebitType((String)debitTypeMap.get(ffd.getFieldData("regulated_rate_type_indicator")));
      data.setMoneySendIndicator(ffd.getFieldData("money_send_indicator")); 
      data.setDurbinRegulatedRateInd(ffd.getFieldData("durbin_reg_rate_indicator"));
    }
    catch( Exception e )
    {
      logEntry("_parseArdefDataMC()",e.toString());
    }
    finally
    {
    }
    return(data);
  }
  
  public static ArdefDataModBII parseArdefDataModBII( String rawData )
  {
    ArdefDataModBII     data  = null;
    FlatFileRecord      ffd   = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_ARDEF);
    
    data = new SettlementDb()._parseArdefDataModBII(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public ArdefDataModBII _parseArdefDataModBII( String rawData, FlatFileRecord ffd )
  {
    ArdefDataModBII     data  = null;
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new ArdefDataModBII();
      data.setArdefCountryCode        ( ffd.getFieldData  ("country_code").trim()         );
      data.setCardAssociation         ( ffd.getFieldAsInt ("card_association")            );
      data.setCardType                ( ffd.getFieldData  ("card_type").trim()            );
      data.setClearingBankIdCredit    ( ffd.getFieldData  ("clearing_bank_id_cr").trim()  );
      data.setClearingBankIdDebit     ( ffd.getFieldData  ("clearing_bank_id_db").trim()  );
      data.setEndpoint                ( ffd.getFieldData  ("endpoint").trim()             );
      data.setEndpointEnabled         ( ffd.getFieldData  ("endpoint_enabled").trim()     );
      data.setFundingSource           ( ffd.getFieldData  ("funding_source").trim()       );
      data.setIssuerBankIdCredit      ( ffd.getFieldData  ("issuer_bank_id_cr").trim()    );
      data.setIssuerBankIdDebit       ( ffd.getFieldData  ("issuer_bank_id_db").trim()    );
      data.setLastModifiedUser        ( ffd.getFieldData  ("last_modified_user").trim()   );
      data.setMultiBankFlag           ( ffd.getFieldData  ("multi_bank_flag").trim()      );
      data.setRangeHigh               ( ffd.getFieldAsLong("range_high")                  );
      data.setRangeLow                ( ffd.getFieldAsLong("range_low")                   );
      
      for( int i = 0; i < 2; ++i )
      {
        String fname    = (i == 0 ? "valid_date_begin" : "valid_date_end");
        String fvalue   = ffd.getFieldData(fname).trim();
        Date   sqlDate  = new java.sql.Date(DateTimeFormatter.parseDate(fvalue,"yyyy-MM-dd").getTime());
        
        if ( i == 0 )
        {
          data.setValidDateBegin(sqlDate);
        }
        else
        {
          data.setValidDateEnd(sqlDate);
        }
      }        
    }
    catch( Exception e )
    {
      logEntry("_parseArdefDataModBII()",e.toString());
    }
    finally
    {
    }
    return(data);
  }
  
  public static ArdefDataVisa parseArdefDataVisa( String rawData )
  {
    ArdefDataVisa  data = null;
    FlatFileRecord ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_ARDEF);
    
    data = new SettlementDb()._parseArdefDataVisa(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public ArdefDataVisa _parseArdefDataVisa( String rawData, FlatFileRecord ffd )
  {
    ArdefDataVisa       data        = null;
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new ArdefDataVisa();
      data.setRangeHigh(ffd.getFieldAsLong("range_high"));
      data.setRangeLow(ffd.getFieldAsLong("range_low"));
      data.setIssuerCountryCode(ffd.getFieldData("issuer_country_code"));
      data.setIssuerBin(ffd.getFieldData("issuer_bin"));
      data.setCheckDigitAlgorithm(ffd.getFieldAsInt("check_digit_algorithm"));
      data.setAccountNumberLength(ffd.getFieldAsInt("account_number_length"));
      data.setProcessorBin(ffd.getFieldData("processor_bin"));
      data.setDomain(ffd.getFieldData("domain"));
      data.setIssuerVisaRegion(ffd.getFieldAsInt("issuer_visa_region"));
      data.setLargeTicket(ffd.getFieldData("large_ticket"));
      data.setTechnologyIndicator(ffd.getFieldData("technology_indicator"));
      data.setArdefRegion(ffd.getFieldAsInt("ardef_region"));
      data.setArdefCountryCode(ffd.getFieldData("ardef_country_code"));
      data.setCommCardLevelIISupport(ffd.getFieldData("comm_card_level_ii_support"));
      data.setCommCardLevelIIISupport(ffd.getFieldData("comm_card_level_iii_support"));
      data.setCommCardPosPromptInd(ffd.getFieldData("comm_card_pos_prompt_ind"));
      data.setVatEvidence(ffd.getFieldData("vat_evidence"));
      data.setOriginalCredit(ffd.getFieldData("original_credit"));
      data.setALPInd(ffd.getFieldData("alp_ind"));
      data.setOriginalCreditMoneyTransfer(ffd.getFieldData("original_credit_money_xfer"));
      data.setOriginalCreditGambling(ffd.getFieldData("original_credit_gambling"));
      data.setProductId(ffd.getFieldData("product_id"));
      data.setComboCard(ffd.getFieldData("combo_card"));
      data.setFastFunds(ffd.getFieldData("fast_funds"));
      data.setFundingSource(ffd.getFieldData("funding_source"));
      data.setSettlementMatch(ffd.getFieldData("settlement_match"));
      data.setTravelAccount(ffd.getFieldData("travel_account"));
      data.setRestrictedUse(ffd.getFieldData("restricted_use"));
      data.setNNSSIndicator(ffd.getFieldData("nnss_indicator"));
      data.setProductSubtype(ffd.getFieldData("product_subtype"));
      data.setTestIndicator(ffd.getFieldData("test_indicator"));
      data.setTokenIndicator(ffd.getFieldData("token_indicator"));
    }
    catch( Exception e )
    {
      logEntry("_parseArdefDataVisa()",e.toString());
    }
    finally
    {
    }
    return(data);
  }
  
  public static RegArdefDataVisa parseRegArdefDataVisa( String rawData )
  {
    RegArdefDataVisa  data = null;
    FlatFileRecord    ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_REGARDEF);
    
    data = new SettlementDb()._parseRegArdefDataVisa(rawData,ffd);
    ffd.cleanUp();
    
    return(data);
  }
  
  public RegArdefDataVisa _parseRegArdefDataVisa( String rawData, FlatFileRecord ffd )
  {
    RegArdefDataVisa      data        = null;
    
    try
    {
      ffd.resetAllFields();
      ffd.suck(rawData);
      
      data = new RegArdefDataVisa();
      data.setRangeHigh(ffd.getFieldAsLong("range_high"));
      data.setRangeLow(ffd.getFieldAsLong("range_low"));
    }
    catch( Exception e )
    {
      logEntry("_parseRegArdefDataVisa()",e.toString());
    }
    finally
    {
    }
    return(data);
  }

  public static void storeArdefDiscover( ArdefDataDiscover data )
  {
    new SettlementDb()._storeArdefDiscover(data);
  }
  
  public void _storeArdefDiscover( ArdefDataDiscover data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into discover_ardef_table    " +
        " (                                   " +
        "   iin_prefix_length,                " +
        "   iin_prefix,                       " +
        "   account_number_length_min,        " +
        "   account_number_length_max,        " +
        "   card_product_id,                  " +
        "   debit_type,                       " +
        "   atm_indicator,                    " +
        "   stip_eligible,                    " +
        "   issuing_network,                  " +
        "   foreign_card_indicator,           " +
        "   issuer_country_code,              " +
        "   account_level_indicator,          " +
        "   effective_date,                   " +
        "   load_filename,                    " +
        "   load_file_id                      " +
        " )                                   " +
        " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setInt   (++idx,data.getPrefixLength());
      ps.setString(++idx,data.getPrefix().trim());
      ps.setInt   (++idx,data.getCardNumberLengthMin());
      ps.setInt   (++idx,data.getCardNumberLengthMax());
      ps.setString(++idx,data.getCardProductId().trim());
      ps.setInt   (++idx,data.getDebitType());
      ps.setInt   (++idx,data.getAtmInd());
      ps.setInt   (++idx,data.getStipEligible());
      ps.setInt   (++idx,data.getIssuingNetwork());
      ps.setInt   (++idx,data.getForeignCardInd());
      ps.setString(++idx,data.getIssuerCountryCode().trim());
      ps.setInt   (++idx,data.getAccountLevelInd());
      ps.setDate  (++idx,data.getEffectiveDate());
      ps.setString(++idx,data.getLoadFilename());
      ps.setLong  (++idx,data.getLoadFileId());
      ps.executeUpdate();
    }
    catch( Exception e )
    {
       logEntry("_storeArdefDiscover()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void storeArdefAmexOptBlue( AmexOptBlueBinRange data )
  {
    new SettlementDb()._storeArdefAmexOptBlue(data);
  }
  
  public void _storeArdefAmexOptBlue( AmexOptBlueBinRange data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into amex_ardef_optblue_table    " +
        " (                                   " +
        "   amex_cm_bin,                " +
        "   product_code,          " +      
        "   effective_date,             " +
        "   load_file_name,                    " +
        "   load_file_id,                " +
        "   enabled )                " +
        " values(?,?,?,?,?,?) ";
        
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setString(++idx,data.getAmexCmBin());
      ps.setString(++idx,data.getAmexProductCode());
      ps.setDate  (++idx,data.getEffectiveDate());    
      ps.setString(++idx,data.getLoadFileName());
      ps.setLong  (++idx,data.getLoadFileId());
      ps.setString  (++idx,data.getEnabled());
      ps.executeUpdate();
    }
    catch( Exception e )
    {
       logEntry("_storeArdefAmex()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void storeArdefMC( ArdefDataMC data )
  {
    new SettlementDb()._storeArdefMC(data);
  }
  
  public void _storeArdefMC( ArdefDataMC data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into mc_ardef_table        " +
        " (                                 " +
        "   effective_timestamp,            " +
        "   table_id,                       " +
        "   range_low,                      " +
        "   range_high,                     " +
        "   active_inactive_code,           " +
        "   gcms_product_id,                " +
        "   card_program_id,                " +
        "   priority_code,                  " +
        "   member_id,                      " +
        "   product_type_id,                " +
        "   debit_type,                     " +
        "   endpoint,                       " +
        "   country_code_alpha,             " +
        "   country_code_numeric,           " +
        "   region,                         " +
        "   product_class,                  " +
        "   transaction_routing_ind,        " +
        "   first_presentment_reassign_sw,  " +
        "   product_reassignment_switch,    " +
        "   pwcb_opt_in_switch,             " +
        "   licensed_product_id,            " +
        "   mapping_service_ind,            " +
        "   account_level_ind,              " +
        "   account_level_activation_date,  " +
        "   ch_billing_currency_default,    " +
        "   ch_billing_currency_exp_deflt,  " +
        "   ch_billing_prim_trn_currency_1, " +
        "   ch_billing_prim_currency_1,     " +
        "   ch_billing_prim_currency_exp_1, " +
        "   ch_billing_prim_trn_currency_2, " +
        "   ch_billing_prim_currency_2,     " +
        "   ch_billing_prim_currency_exp_2, " +
        "   ch_billing_prim_trn_currency_3, " +
        "   ch_billing_prim_currency_3,     " +
        "   ch_billing_prim_currency_exp_3, " +
        "   ch_billing_prim_trn_currency_4, " +
        "   ch_billing_prim_currency_4,     " +
        "   ch_billing_prim_currency_exp_4, " +
        "   chip_to_mag_conv_service_ind,   " +
        "   floor_expiration_date,          " +
        "   co_brand_participation_switch,  " +
        "   spend_control_switch,           " +
        "   load_filename,                  " +
        "   load_file_id,                    " +
        "   durbin_reg_rate_ind,             " +
        "   money_send_ind                   " +
        " )                                 " +
        " values( ?,?,?,?,?,?,?,?,?,?,      " +     // 1-10
        "         ?,?,?,?,?,?,?,?,?,?,      " +     // 11-20
        "         ?,?,?,?,?,?,?,?,?,?,      " +     // 21-30
        "         ?,?,?,?,?,?,?,?,?,?,      " +     // 31-40
        "         ?,?,?,?,?,? )                 ";      // 41-46


      int idx = 0;
      ps = getPreparedStatement(sqlText);
      
      setPsTimestamp( ps,++idx,data.getEffectiveTs() );
      ps.setString( ++idx, data.getTableId().trim() );
      ps.setLong  ( ++idx, data.getRangeLow() );
      ps.setLong  ( ++idx, data.getRangeHigh() );
      ps.setString( ++idx, data.getActiveInactiveCode().trim() );
      ps.setString( ++idx, data.getGCMSProductId().trim() );
      ps.setString( ++idx, data.getCardProgramId().trim() );
      ps.setInt   ( ++idx, data.getPriorityCode() );
      ps.setLong  ( ++idx, data.getMemberId() );
      ps.setString( ++idx, data.getProductTypeId().trim() );
      ps.setInt   ( ++idx, data.getDebitType() );
      ps.setString( ++idx, data.getEndpoint().trim() );
      ps.setString( ++idx, data.getCountryCodeAlpha().trim() );
      ps.setString( ++idx, data.getCountryCodeNumeric().trim() );
      ps.setString( ++idx, data.getRegion().trim() );
      ps.setString( ++idx, data.getProductClass().trim() );
      ps.setString( ++idx, data.getTransactionRoutingInd().trim() );
      ps.setString( ++idx, data.getFirstPresentmentReassignSwitch().trim() );
      ps.setString( ++idx, data.getProductReassignSwitch().trim() );
      ps.setString( ++idx, data.getPWCBOptInSwitch().trim() );
      ps.setString( ++idx, data.getLicensedProductId().trim() );
      ps.setString( ++idx, data.getMappingServiceInd().trim() );
      ps.setString( ++idx, data.getAccountLevelInd().trim() );
      setPsDate( ps,++idx,data.getAccountLevelActivationDate() );
      ps.setString( ++idx, data.getBillingCurrencyDefault().trim() );
      ps.setInt   ( ++idx, data.getBillingCurrencyExpDefault() );
      ps.setString( ++idx, data.getBillingPrimaryTranCurrency_1().trim() );
      ps.setString( ++idx, data.getBillingPrimaryCurrency_1().trim() );
      ps.setInt   ( ++idx, data.getBillingPrimaryCurrencyExp_1() );
      ps.setString( ++idx, data.getBillingPrimaryTranCurrency_2().trim() );
      ps.setString( ++idx, data.getBillingPrimaryCurrency_2().trim() );
      ps.setInt   ( ++idx, data.getBillingPrimaryCurrencyExp_2() );
      ps.setString( ++idx, data.getBillingPrimaryTranCurrency_3().trim() );
      ps.setString( ++idx, data.getBillingPrimaryCurrency_3().trim() );
      ps.setInt   ( ++idx, data.getBillingPrimaryCurrencyExp_3() );
      ps.setString( ++idx, data.getBillingPrimaryTranCurrency_4().trim() );
      ps.setString( ++idx, data.getBillingPrimaryCurrency_4().trim() );
      ps.setInt   ( ++idx, data.getBillingPrimaryCurrencyExp_4() );
      ps.setString( ++idx, data.getChipToMagConvServiceInd().trim() );
      setPsDate( ps,++idx, data.getFloorExpDate() );
      ps.setString( ++idx, data.getCoBrandParticipationSwitch().trim() );
      ps.setString( ++idx, data.getSpendControlSwitch().trim() );
      ps.setString( ++idx, data.getLoadFilename().trim() );
      ps.setLong  ( ++idx, data.getLoadFileId() );
      ps.setString( ++idx, data.getDurbinRegulatedRateInd().trim() );
      ps.setString( ++idx, data.getMoneySendIndicator().trim() );
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_storeArdefMC()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void storeArdefModBII( ArdefDataModBII data )
  {
    new SettlementDb()._storeArdefModBII(data);
  }
  
  public void _storeArdefModBII( ArdefDataModBII data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into modbii_ardef_table      " +
        " (                                   " +
        "   card_association,                 " +
        "   range_low,                        " +
        "   range_high,                       " +
        "   funding_source,                   " +
        "   country_code,                     " +
        "   card_type,                        " +
        "   issuer_bank_id_cr,                " +
        "   clearing_bank_id_cr,              " +
        "   issuer_bank_id_db,                " +
        "   clearing_bank_id_db,              " +
        "   multi_bank_flag,                  " +
        "   valid_date_begin,                 " +
        "   valid_date_end,                   " +
        "   last_modified_user,               " +
        "   endpoint_enabled,                 " +
        "   endpoint,                         " +
        "   load_filename,                    " +
        "   load_file_id                      " +
        " )                                   " +
        " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setInt   (++idx,data.getCardAssociation()        );
      ps.setLong  (++idx,data.getRangeLow()               );
      ps.setLong  (++idx,data.getRangeHigh()              );
      ps.setString(++idx,data.getFundingSource()          );
      ps.setString(++idx,data.getArdefCountryCode()       );
      ps.setString(++idx,data.getCardType()               );
      ps.setString(++idx,data.getIssuerBankIdCredit()     );
      ps.setString(++idx,data.getClearingBankIdCredit()   );
      ps.setString(++idx,data.getIssuerBankIdDebit()      );
      ps.setString(++idx,data.getClearingBankIdDebit()    );
      ps.setString(++idx,data.getMultiBankFlag()          );
      ps.setDate  (++idx,data.getValidDateBegin()         );
      ps.setDate  (++idx,data.getValidDateEnd()           );
      ps.setString(++idx,data.getLastModifiedUser()       );
      ps.setString(++idx,data.getEndpointEnabled()        );
      ps.setString(++idx,data.getEndpoint()               );
      ps.setString(++idx,data.getLoadFilename()           );
      ps.setLong  (++idx,data.getLoadFileId()             );
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_storeArdefModBII()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void storeArdefVisa( ArdefDataVisa data )
  {
    new SettlementDb()._storeArdefVisa(data);
  }
  
  public void _storeArdefVisa( ArdefDataVisa data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into visa_ardef_table      " +
        " (                                 " +
        "   range_high,                     " +
        "   range_low,                      " +
        "   issuer_bin,                     " +
        "   check_digit_algorithm,          " +
        "   account_number_length,          " +
        "   processor_bin,                  " +
        "   debit_type,                     " +
        "   domain,                         " +
        "   issuer_visa_region,             " +
        "   issuer_country_code,            " +
        "   large_ticket,                   " +
        "   technology_indicator,           " +
        "   ardef_region,                   " +
        "   ardef_country_code,             " +
        "   comm_card_level_ii_support,     " +
        "   comm_card_level_iii_support,    " +
        "   comm_card_pos_prompt_ind,       " +
        "   vat_evidence,                   " +
        "   original_credit,                " +
        "   alp_ind,                        " +
        "   original_credit_money_xfer,     " +
        "   original_credit_gambling,       " +
        "   product_id,                     " +
        "   combo_card,                     " +
        "   fast_funds,                     " +
        "   funding_source,                 " +
        "   settlement_match,               " +
        "   travel_account,                 " +
        "   restricted_use,                 " +
        "   nnss_indicator,                 " +
        "   product_subtype,                " +
        "   test_indicator,                 " +
        "   load_filename,                  " +
        "   load_file_id,                   " +
        "   token_indicator                 " +
        " )                                 " +
        " values(?,?,?,?,?,?,?,?,?,?,?,?,?, " +
        "        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setLong  (++idx,data.getRangeHigh());
      ps.setLong  (++idx,data.getRangeLow());
      ps.setString(++idx,data.getIssuerBin().trim());
      ps.setInt   (++idx,data.getCheckDigitAlgorithm());
      ps.setInt   (++idx,data.getAccountNumberLength());
      ps.setString(++idx,data.getProcessorBin().trim());
      ps.setInt   (++idx,data.getDebitType());
      ps.setString(++idx,data.getDomain().trim());
      ps.setInt   (++idx,data.getIssuerVisaRegion());
      ps.setString(++idx,data.getIssuerCountryCode().trim());
      ps.setString(++idx,data.getLargeTicket().trim());
      ps.setString(++idx,data.getTechnologyIndicator().trim());
      ps.setInt   (++idx,data.getArdefRegion());
      ps.setString(++idx,data.getArdefCountryCode().trim());
      ps.setString(++idx,data.getCommCardLevelIISupport().trim());
      ps.setString(++idx,data.getCommCardLevelIIISupport().trim());
      ps.setString(++idx,data.getCommCardPosPromptInd().trim());
      ps.setString(++idx,data.getVatEvidence().trim());
      ps.setString(++idx,data.getOriginalCredit().trim());
      ps.setString(++idx,data.getALPInd().trim());
      ps.setString(++idx,data.getOriginalCreditMoneyTransfer().trim());
      ps.setString(++idx,data.getOriginalCreditGambling().trim());
      ps.setString(++idx,data.getProductId().trim());
      ps.setString(++idx,data.getComboCard().trim());
      ps.setString(++idx,data.getFastFunds().trim());
      ps.setString(++idx,data.getFundingSource().trim());
      ps.setString(++idx,data.getSettlementMatch().trim());
      ps.setString(++idx,data.getTravelAccount().trim());
      ps.setString(++idx,data.getRestrictedUse().trim());
      ps.setString(++idx,data.getNNSSIndicator().trim());
      ps.setString(++idx,data.getProductSubtype().trim());
      ps.setString(++idx,data.getTestIndicator().trim());
      ps.setString(++idx,data.getLoadFilename());
      ps.setLong  (++idx,data.getLoadFileId());
      ps.setString(++idx,data.getTokenIndicator().trim());
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_storeArdefVisa()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static void storeRegArdefVisa( RegArdefDataVisa data )
  {
    new SettlementDb()._storeRegArdefVisa(data);
  }
  
  public void _storeRegArdefVisa( RegArdefDataVisa data )
  {
    PreparedStatement     ps          = null;
    
    try
    {
      connect(true);
      
      String sqlText = 
        " insert into visa_ardef_table_regulated  " +
        " (                 " +
        "   range_high,     " +
        "   range_low,      " +
        "   load_filename,  " +
        "   load_file_id    " +
        " )                 " +
        " values(?,?,?,?)   ";
      
      int idx = 0;
      ps = getPreparedStatement(sqlText);
      ps.setLong  (++idx,data.getRangeHigh());
      ps.setLong  (++idx,data.getRangeLow());
      ps.setString(++idx,data.getLoadFilename());
      ps.setLong  (++idx,data.getLoadFileId());
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_storeRegArdefVisa()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public void _toggleArdefData( String tableName )
  {
    PreparedStatement     ps          = null;
  
    try
    {
      connect(true);
      ps = getPreparedStatement("update " + tableName + " set enabled = decode(enabled,'Y','N','N','Y')");
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_toggleArdefData(" + tableName + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
  }
  
  public static String getOptBlueProductCode( String cardNumber )
  {
    return( new SettlementDb()._getOptBlueProductCode(cardNumber) );
  }
  
  public String _getOptBlueProductCode( String binNumber )
  {
    PreparedStatement     ps          = null;
    String                retVal      = "";
    
    try
    {
      connect(true);
      
      String sqlText = 
        " select  ard.product_code            " + crlf +   
        " from    amex_ardef_optblue_table    ard     " + crlf +
        " where   ard.enabled = 'Y'               " + crlf +
        "         and amex_cm_bin =?";
      
      ps = getPreparedStatement(sqlText);
      ps.setString(1,binNumber);
      ResultSet rs = ps.executeQuery();
      if ( rs.next() )
      {
        retVal = rs.getString("product_code");
      }
      rs.close();
    }
    catch( Exception e )
    {
      // invalid SIC, just return ""
      logEntry("_getOptBlueProductCode("+ binNumber +")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception e ){}
      cleanUp();
    }
    return( retVal );
  }
  
  public static long getOptblueSettleSeq(){
	  return ( new SettlementDb()._getOptblueSettleSeq());
  }
  
  public long _getOptblueSettleSeq(){
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  long seq = 0l;
	  try{
		  connect(true);
		  ps = getPreparedStatement("select AMEX_OPTBLUE_SETTLE_FILE_SEQ.NEXTVAL as seqN from dual");
		  rs = ps.executeQuery();
		  if(rs.next())
			  seq = rs.getLong("seqN");
		  
	  }catch(Exception e){
		  logEntry("_getOptblueSettleSeq()",e.toString());
	  }finally {
		  try{
			  if(ps != null) ps.close();
			  if(rs != null) rs.close();
		  }catch(Exception ignore){}
	  }
	  
	  return seq;
  }
  
  public static void  toggleArdefDataDiscover()  { new SettlementDb()._toggleArdefDataDiscover();                           }
  public        void _toggleArdefDataDiscover()  { new SettlementDb()._toggleArdefData( "discover_ardef_table"          );  }
  public static void  toggleArdefDataMC()        { new SettlementDb()._toggleArdefDataMC();                                 }
  public        void _toggleArdefDataMC()        { new SettlementDb()._toggleArdefData(       "mc_ardef_table"          );  }
  public static void  toggleArdefDataModBII()    { new SettlementDb()._toggleArdefDataModBII();                             }
  public        void _toggleArdefDataModBII()    { new SettlementDb()._toggleArdefData(   "modbii_ardef_table"          );  }
  public static void  toggleArdefDataVisa()      { new SettlementDb()._toggleArdefDataVisa();                               }
  public        void _toggleArdefDataVisa()      { new SettlementDb()._toggleArdefData(     "visa_ardef_table"          );  }
  public static void  toggleRegArdefDataVisa()   { new SettlementDb()._toggleRegArdefDataVisa();                            }
  public        void _toggleRegArdefDataVisa()   { new SettlementDb()._toggleArdefData(     "visa_ardef_table_regulated");  }
  
/**
 * This method is used to load ARDEF Data
 * @param binNumber
 * @return ArdefDataAmex
 */
 public static ArdefDataAmex loadArdefAmex( String binNumber )
   {
    return( new SettlementDb()._loadArdefAmex(binNumber) );
   }
  
 /** This method is used to load ARDEF Data
 * @param binNumber
 * @return ArdefDataAmex
 */
public ArdefDataAmex _loadArdefAmex( String binNumber )
    {
      ArdefDataAmex     data        = null;
      PreparedStatement     ps          = null;
      try
        {
    	  
          data = new ArdefDataAmex();
          connect(true);
          String sqlText = " select *  " + crlf + " from    amex_ardef_optblue_table    ard     " + crlf +
            " where   ard.enabled = 'Y'               " + crlf + "         and amex_cm_bin =?"; 
          ps = getPreparedStatement(sqlText);
          ps.setString(1,binNumber);
          ResultSet rs = ps.executeQuery();
          if ( rs.next() )
            {
              String enabled = null;
              enabled = rs.getString("enabled");
              if (enabled != null)
                {
                  data.setEnabled((enabled.equalsIgnoreCase("Y")) ? true:false);
                }
              data.setAmexCmBin(rs.getString("amex_cm_bin"));
              data.setProductCode(rs.getString("product_code"));
              data.setEffectiveDate(rs.getDate("effective_date"));
              data.setLoadFilename(rs.getString("load_file_name"));
              data.setLoadFileId(rs.getLong("load_file_id"));
            }
          rs.close();
        }
      catch( Exception e )
        {
          logEntry("_loadArdefAmex()",e.toString());
        }
      finally
        {
          try{ ps.close(); } catch( Exception e ){}
          cleanUp();
        }
      return( data );
    }

    /**
     * This method is used to retrieve the information from the Daily detail
     * file D256 Er
     * 
     * @param loadFileName
     * @param batchRecId
     * @param batchDate
     * @return List <DailyDetailFileD256ER>
     */
    public static List<DailyDetailFileD256ER> getD256ErData(String loadFileName, long batchRecId, Date batchDate,
            FormatTypes sicGroup) {
        return (new SettlementDb()._getD256ErData(loadFileName, batchRecId, batchDate, sicGroup));
    }

    /**
     * This method is used to retrieve the information from the Daily detail
     * file D256 Er
     * 
     * @param loadFileName
     * @param batchRecId
     * @param batchDate
     * @return List <DailyDetailFileD256ER>
     */
    public List<DailyDetailFileD256ER> _getD256ErData(String loadFileName, long batchRecId, Date batchDate,
            FormatTypes sicGroup) {
        PreparedStatement ps = null;
        ResultSet rsExtra = null;
        List<DailyDetailFileD256ER> results = null;
        String crlf = "\r\n";
        String orderby = " order by er.sequence_number                               ";
        String formatIndicatorIn = "";
        String sqlText = " select  /*+ index (er IDX_DDF_D256_ER_SEQ_NUM_BDATE) */   " + crlf
                + "         er.*                                              " + crlf
                + " from    daily_detail_file_d256_er       er                " + crlf
                + " where   er.load_filename       = ?                        " + crlf
                + "         and er.fin_seq_num  = ?                           " + crlf
                + "         and er.batch_date       = ?                       " + crlf;
        switch (sicGroup) {
        case AIRLINES:
            formatIndicatorIn = "and format_indicator in ('AIRL1','AIRL2') ";
            break;
        case CARRENTALS:
            formatIndicatorIn = "and format_indicator in ('CARNT','OPTIN') ";
            break;
        case HOTELS:
            formatIndicatorIn = "and format_indicator in ('LODGE','OPTIN') ";
            break;
        default:
            formatIndicatorIn = "";
            break;
        }

        StringBuffer sqlComplete = new StringBuffer(sqlText);
        sqlComplete.append(formatIndicatorIn).append(orderby);

        try {
            connect(true);
            ps = getPreparedStatement(sqlComplete.toString());
            ps.setString(1, loadFileName);
            ps.setLong(2, batchRecId);
            ps.setDate(3, batchDate);
            rsExtra = ps.executeQuery();
            results = new ArrayList<DailyDetailFileD256ER>();
            while (rsExtra.next()) {
                DailyDetailFileD256ER d256ER = new DailyDetailFileD256ER();
                d256ER.setTrnSeqNum(rsExtra.getLong("TRN_SEQ_NUM"));
                d256ER.setBtcSeqNum(rsExtra.getLong("BTC_SEQ_NUM"));
                d256ER.setBatchNum(rsExtra.getLong("BATCH_NUM"));
                d256ER.setBatchDate(rsExtra.getDate("BATCH_DATE"));
                d256ER.setMerchantNumber(rsExtra.getLong("MERCHANT_NUMBER"));
                d256ER.setFinSeqNum(rsExtra.getLong("FIN_SEQ_NUM"));
                d256ER.setSequenceNumber(rsExtra.getLong("SEQUENCE_NUMBER"));
                d256ER.setTransactionCode(rsExtra.getInt("TRANSACTION_CODE"));
                d256ER.setFormatIndicator(rsExtra.getString("FORMAT_INDICATOR"));
                d256ER.setData(rsExtra.getString("DATA"));
                d256ER.setLoadFileName(rsExtra.getString("LOAD_FILENAME"));
                results.add(d256ER);
            }
            rsExtra.close();
        } catch (Exception e) {
            logEntry("_getD256ErData", e.toString());
        } finally {
            try {
                ps.close();
            } catch (Exception e) {
            }
            cleanUp();
        }
        return results;
    }
    
}
