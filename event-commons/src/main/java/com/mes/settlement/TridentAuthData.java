/*************************************************************************
 * 
 * FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/TridentAuthData.java $
 * 
 * Description:
 * 
 * Last Modified By : $Author: ttran $
 * Last Modified Date : $Date: 2015-10-28 17:22:39 -0700 (Wed, 28 Oct 2015) $
 * Version : $Revision: 23916 $
 * 
 * Change History:
 * See SVN database
 * 
 * Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * 
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc. This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 * 
 **************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import com.mes.constants.mesConstants;

public class TridentAuthData {
	private double AuthAmount = 0.0;
	private double authTranFeeAmount = 0.0;
	private String AuthCode = null;
	private java.util.Date AuthDate = null;
	private java.util.Date AuthDateLocal = null;
	private String AuthFPI = null;
	private String AuthSourceCode = null;
	private Time AuthTime = null;
	private Time AuthTimeLocal = null;
	private String timeZone = null;
	private String AuthTranId = null;
	private String AuthValCode = null;
	private String AvsResponse = null;
	private String AvsZip = null;
	private String CurrencyCode = null;
	private String Cvv2Response = null;
	private String MarketSpecificDataInd = null;
	private String PosEntryMode = null;
	private String ProductResultCode = null;
	private long RecId = 0L;
	private String ResponseCode = null;
	private String RetrievalRefNum = null;
	private String ReturnedACI = null;
	private String SicCode = null;
	private String SpendQualifiedInd = null;
	private String tokenAssuranceLevel = null;
	private String isoEcommerceIndicator = null;
	private int POSConditionCode = 0;
	private String TridentTranId = null;
	private String IccData = null;
	private int CardSequenceNum = 0;
	private String PosEnvironment = null;
	private String trackCondCode = null;
	private String tic = null;
	protected String mcPosEntryMode = null;
	protected String mcPosData = null;
	private String partialAuthInd = null;
	private String discPartialAuthInd = null;

	public TridentAuthData() {
	}

	public TridentAuthData(ResultSet resultSet, int batchType) throws java.sql.SQLException {
		SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		AuthAmount = resultSet.getDouble("auth_amount");
		AuthCode = resultSet.getString("auth_code");
		try {
			AuthDate = dateFmt.parse(resultSet.getString("auth_date"));
		}
		catch (Exception e) {
			AuthDate = resultSet.getDate("auth_time");
		}
		try {
			AuthDateLocal = dateFmt.parse(resultSet.getString("auth_date_local"));
		}
		catch (Exception e) {
			AuthDateLocal = resultSet.getDate("auth_time_local");
		}
		AuthFPI = resultSet.getString("auth_fpi");
		AuthSourceCode = resultSet.getString("auth_source_code");
		AuthTime = resultSet.getTime("auth_time");
		AuthTimeLocal = resultSet.getTime("auth_time_local");
		timeZone = resultSet.getString("auth_time_zone");
		AuthTranId = resultSet.getString("tran_id");
		AuthValCode = resultSet.getString("val_code");
		AvsResponse = resultSet.getString("avs_resp");
		AvsZip = resultSet.getString("avs_zip");
		CurrencyCode = resultSet.getString("currency_code");
		Cvv2Response = resultSet.getString("cvv2_resp");
		MarketSpecificDataInd = resultSet.getString("market_specific_data_ind");
		PosEntryMode = resultSet.getString("pos_entry_mode");
		ProductResultCode = resultSet.getString("product_type_id");
		RecId = resultSet.getLong("rec_id");
		ResponseCode = resultSet.getString("response_code");
		RetrievalRefNum = resultSet.getString("rrn");
		ReturnedACI = resultSet.getString("returned_aci");
		SicCode = resultSet.getString("sic_code");
		SpendQualifiedInd = resultSet.getString("spend_qualified_ind");
		tokenAssuranceLevel = resultSet.getString("token_assurance_level");
		isoEcommerceIndicator = resultSet.getString("iso_ecommerce_indicator");
		POSConditionCode = resultSet.getInt("pos_condition_code");
		CardSequenceNum = resultSet.getInt("card_sequence_number");
		TridentTranId = resultSet.getString("trident_tran_id");
		PosEnvironment = resultSet.getString("pos_environment");
		tic = resultSet.getString("tic");
		mcPosEntryMode = resultSet.getString("mc_pos_entry_mode");
		mcPosData = resultSet.getString("mc_pos_data");
		if (batchType == mesConstants.MBS_BT_VISAK) {
			partialAuthInd = resultSet.getString("is_partial_auth_enabled");
		}
		else if (batchType == mesConstants.MBS_BT_VITAL) {
			discPartialAuthInd = resultSet.getString("disc_partial_auth_ind");
		}
		try {
			trackCondCode = resultSet.getString("disc_tran_data_cond_code");
		}
		catch (SQLException sqlex) {
			trackCondCode = null;
		}

		try {
			authTranFeeAmount = resultSet.getDouble("tran_fee_amount");
		}
		catch (SQLException e) {
			authTranFeeAmount = 0.0;
		}
	}

	public double getAuthAmount() {
		return AuthAmount;
	}

	public void setAuthAmount(double authAmount) {
		AuthAmount = authAmount;
	}

	public double getAuthTranFeeAmount() {
		return authTranFeeAmount;
	}

	public void setAuthTranFeeAmount(double authTranFeeAmount) {
		this.authTranFeeAmount = authTranFeeAmount;
	}

	public String getAuthCode() {
		return AuthCode;
	}

	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}

	public java.util.Date getAuthDate() {
		return AuthDate;
	}

	public void setAuthDate(java.util.Date authDate) {
		AuthDate = authDate;
	}

	public java.util.Date getAuthDateLocal() {
		return AuthDateLocal;
	}

	public void setAuthDateLocal(java.util.Date authDateLocal) {
		AuthDateLocal = authDateLocal;
	}

	public String getAuthFPI() {
		return AuthFPI;
	}

	public void setAuthFPI(String authFPI) {
		AuthFPI = authFPI;
	}

	public String getAuthSourceCode() {
		return AuthSourceCode;
	}

	public void setAuthSourceCode(String authSourceCode) {
		AuthSourceCode = authSourceCode;
	}

	public Time getAuthTime() {
		return AuthTime;
	}

	public void setAuthTime(Time authTime) {
		AuthTime = authTime;
	}

	public Time getAuthTimeLocal() {
		return AuthTimeLocal;
	}

	public void setAuthTimeLocal(Time authTimeLocal) {
		AuthTimeLocal = authTimeLocal;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getAuthTranId() {
		return AuthTranId;
	}

	public void setAuthTranId(String authTranId) {
		AuthTranId = authTranId;
	}

	public String getAuthValCode() {
		return AuthValCode;
	}

	public void setAuthValCode(String authValCode) {
		AuthValCode = authValCode;
	}

	public String getAvsResponse() {
		return AvsResponse;
	}

	public void setAvsResponse(String avsResponse) {
		AvsResponse = avsResponse;
	}

	public String getAvsZip() {
		return AvsZip;
	}

	public void setAvsZip(String avsZip) {
		AvsZip = avsZip;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getCvv2Response() {
		return Cvv2Response;
	}

	public void setCvv2Response(String cvv2Response) {
		Cvv2Response = cvv2Response;
	}

	public String getMarketSpecificDataInd() {
		return MarketSpecificDataInd;
	}

	public void setMarketSpecificDataInd(String marketSpecificDataInd) {
		MarketSpecificDataInd = marketSpecificDataInd;
	}

	public String getPosEntryMode() {
		return PosEntryMode;
	}

	public void setPosEntryMode(String posEntryMode) {
		PosEntryMode = posEntryMode;
	}

	public String getProductResultCode() {
		return ProductResultCode;
	}

	public void setProductResultCode(String productResultCode) {
		ProductResultCode = productResultCode;
	}

	public long getRecId() {
		return RecId;
	}

	public void setRecId(long recId) {
		RecId = recId;
	}

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getRetrievalRefNum() {
		return RetrievalRefNum;
	}

	public void setRetrievalRefNum(String retrievalRefNum) {
		RetrievalRefNum = retrievalRefNum;
	}

	public String getReturnedACI() {
		return ReturnedACI;
	}

	public void setReturnedACI(String returnedACI) {
		ReturnedACI = returnedACI;
	}

	public String getSicCode() {
		return SicCode;
	}

	public void setSicCode(String sicCode) {
		SicCode = sicCode;
	}

	public String getSpendQualifiedInd() {
		return SpendQualifiedInd;
	}

	public void setSpendQualifiedInd(String spendQualifiedInd) {
		SpendQualifiedInd = spendQualifiedInd;
	}

	public String getTokenAssuranceLevel() {
		return tokenAssuranceLevel;
	}

	public void setTokenAssuranceLevel(String tokenAssuranceLevel) {
		this.tokenAssuranceLevel = tokenAssuranceLevel;
	}

	public String getIsoEcommerceIndicator() {
		return isoEcommerceIndicator;
	}

	public void setIsoEcommerceIndicator(String isoEcommerceIndicator) {
		this.isoEcommerceIndicator = isoEcommerceIndicator;
	}

	public int getPOSConditionCode() {
		return POSConditionCode;
	}

	public void setPOSConditionCode(int pOSConditionCode) {
		POSConditionCode = pOSConditionCode;
	}

	public String getTridentTranId() {
		return TridentTranId;
	}

	public void setTridentTranId(String tridentTranId) {
		TridentTranId = tridentTranId;
	}

	public String getIccData() {
		return IccData;
	}

	public void setIccData(String iccData) {
		IccData = iccData;
	}

	public int getCardSequenceNum() {
		return CardSequenceNum;
	}

	public void setCardSequenceNum(int cardSequenceNum) {
		CardSequenceNum = cardSequenceNum;
	}

	public String getPosEnvironment() {
		return PosEnvironment;
	}

	public void setPosEnvironment(String posEnvironment) {
		PosEnvironment = posEnvironment;
	}

	public String getTrackCondCode() {
		return trackCondCode;
	}

	public void setTrackCondCode(String trackCondCode) {
		this.trackCondCode = trackCondCode;
	}

	public String getTic() {
		return tic;
	}

	public void setTic(String tic) {
		this.tic = tic;
	}

	public String getMcPosEntryMode() {
		return mcPosEntryMode;
	}

	public void setMcPosEntryMode(String mcPosEntryMode) {
		this.mcPosEntryMode = mcPosEntryMode;
	}

	public String getMcPosData() {
		return mcPosData;
	}

	public void setMcPosData(String mcPosData) {
		this.mcPosData = mcPosData;
	}

	public String getPartialAuthInd() {
		return partialAuthInd;
	}

	public void setPartialAuthInd(String partialAuthInd) {
		this.partialAuthInd = partialAuthInd;
	}

	public String getDiscPartialAuthInd() {
		return discPartialAuthInd;
	}

	public void setDiscPartialAuthInd(String discPartialAuthInd) {
		this.discPartialAuthInd = discPartialAuthInd;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TridentAuthData [AuthAmount=").append(AuthAmount).append(", \nauthTranFeeAmount=").append(authTranFeeAmount).append(", \nAuthCode=")
				.append(AuthCode).append(", \nAuthDate=").append(AuthDate).append(", \nAuthFPI=").append(AuthFPI).append(", \nAuthSourceCode=")
				.append(AuthSourceCode).append(", \nAuthTime=").append(AuthTime).append(", \nAuthTranId=").append(AuthTranId).append(", \nAuthValCode=")
				.append(AuthValCode).append(", \nAvsResponse=").append(AvsResponse).append(", \nAvsZip=").append(AvsZip).append(", \nCurrencyCode=")
				.append(CurrencyCode).append(", \nCvv2Response=").append(Cvv2Response).append(", \nMarketSpecificDataInd=").append(MarketSpecificDataInd)
				.append(", \nPosEntryMode=").append(PosEntryMode).append(", \nProductResultCode=").append(ProductResultCode).append(", \nRecId=").append(RecId)
				.append(", \nResponseCode=").append(ResponseCode).append(", \nRetrievalRefNum=").append(RetrievalRefNum).append(", \nReturnedACI=")
				.append(ReturnedACI).append(", \nSicCode=").append(SicCode).append(", \nSpendQualifiedInd=").append(SpendQualifiedInd)
				.append(", \ntokenAssuranceLevel=").append(tokenAssuranceLevel).append(", \nisoEcommerceIndicator=").append(isoEcommerceIndicator)
				.append(", \nPOSConditionCode=").append(POSConditionCode).append(", \nTridentTranId=").append(TridentTranId).append(", \nIccData=")
				.append(IccData).append(", \nCardSequenceNum=").append(CardSequenceNum).append(", \nPosEnvironment=").append(PosEnvironment)
				.append(", \ntrackCondCode=").append(trackCondCode).append(", \ntic=").append(tic).append(", \nmcPosEntryMode=").append(mcPosEntryMode)
				.append(", \nmcPosData=").append(mcPosData).append(", \npartialAuthInd=").append(partialAuthInd).append(", \ndiscPartialAuthInd=")
				.append(discPartialAuthInd).append("]");
		return builder.toString();
	}
}
