/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementTools.java $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.SyncLog;
import com.mes.support.ThreadPool;
import com.mes.support.TridentTools;
import com.mes.tools.AsciiToEbcdicConverter;
import masthead.util.ByteUtil;

public class SettlementTools
{
    private static final Logger log = Logger.getLogger(SettlementTools.class);
  public static class ReportLoadWorker
    implements Runnable
  {
    protected   long      BatchId         = -1L;
    protected   long      BatchRecordId   = -1L;
    protected   String    CardType        = null;
    protected   String    ProcByMeS       = null;
    
    public ReportLoadWorker( String cardType, long batchId, long batchRecordId, String procByMeS )
    {
      CardType      = cardType;
      BatchId       = batchId;
      BatchRecordId = batchRecordId;
      ProcByMeS     = procByMeS;
    }
    
    public void run()
    {
      try
      {
        addReportRecords( CardType, BatchId, BatchRecordId, ProcByMeS );
      }
      catch( Exception e )
      {
        SyncLog.LogEntry("run(" + CardType + "," + BatchId + "," + BatchRecordId + ")",e.toString());
        System.out.println("run(" + CardType + "," + BatchId + "," + BatchRecordId + ") - " + e.toString());
      }
      finally
      {
      }
    }
  }
  
  public static void addReject( String cardType, long recId, java.sql.Date rejectDate, String[] reasonCodes )
  {
    SettlementDb        db          = new SettlementDb();
    PreparedStatement   ps          = null;
    ResultSet           rs          = null;
  
    try
    {
      db.connect(true);
    
      ps = db.getPreparedStatement("select (count(1) + 1) as round from reject_record where rec_id = ?");
      ps.setLong(1,recId);
      rs = ps.executeQuery();
      rs.next();
      int round = rs.getInt("round");
      rs.close();
      ps.close();
      
      String sqlText = 
        " insert into reject_record   " +
        " (                           " +
        "   rec_id,                   " +
        "   reject_id,                " +
        "   reject_type,              " +
        "   status,                   " +
        "   round,                    " +
        "   reject_date,              " +
        "   presentment,              " +
        "   load_filename,            " +
        "   load_file_id              " +
        " )                           " +
        " values                      " +
        " (                           " +
        "   :recId,                   " +
        "   :reasonCode,              " +
        "   :cardType,                " +
        "   -1,                       " +
        "   :round,                   " +
        "   :rejectDate,              " +
        "   0,                        " +
        "   null,                     " +
        "   null                      " +
        " )                           ";
        
      ps = db.getPreparedStatement(sqlText);          
      
      for( int i = 0; i < reasonCodes.length; ++i )
      {
        String reasonCode = reasonCodes[i];
        System.out.println("inserting " + reasonCode + " for recId " + recId + " round " + round);
        
        ps.setLong  (1,recId);
        ps.setString(2,reasonCode);
        ps.setString(3,cardType);
        ps.setInt   (4,round);
        ps.setDate  (5,rejectDate);
        ps.executeUpdate();
      }
      ps.close();
      
      if ( QueueTools.hasItemEnteredQueue(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
      {
        QueueTools.moveQueueItem(recId,
                                 QueueTools.getCurrentQueueType(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                 MesQueues.Q_REJECTS_UNWORKED,
                                 null,
                                 null);
      }
      else  //then need to add item to q_data
      {
        QueueTools.insertQueue(recId, MesQueues.Q_REJECTS_UNWORKED);
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("SettlementTools.addReject(" + cardType + "," + recId + ")",e.toString());
    }
    finally
    {
      try{ rs.close(); } catch( Exception ee ) {}
      try{ ps.close(); } catch( Exception ee ) {}
    }
  }

  public static void addReportRecordsFile( String loadFilename )
  {
    String              cardType    = null;
    SettlementDb        db          = new SettlementDb();
    String              extraSelect = null;
    String              extraTable  = null;
    String              extraWhere  = null;
    ThreadPool          pool        = null;
    PreparedStatement   ps          = null;
    SettlementRecord    rec         = null;
    ResultSet           rs          = null;
    String              tableName   = null;
    
    try
    {
      db.connect(true);
      
      pool = new ThreadPool(20);
      
      for( int i = 0; i < 4; ++i )
      {
        int recCount = 0;
        
        extraSelect = " , 'Y' as proc_by_mes ";
        extraTable  = "";
        extraWhere  = "";

        switch(i)
        {
          case 0: tableName =     "visa_settlement";  cardType = "VS"; break;
          case 1: tableName =       "mc_settlement";  cardType = "MC"; break;
          case 2: tableName =     "amex_settlement";  cardType = "AM"; extraSelect = " , case when instr(nvl(mb.am_aggregators,'.'),amex_se_number) > 0 then 'Y' else 'N' end as proc_by_mes";
                                                                       extraTable  = " , mbs_banks mb ";
                                                                       extraWhere  = " and mb.bank_number = settle.bank_number"; break;
          case 3: tableName = "discover_settlement";  cardType = "DS"; extraSelect = " , decode( discover_merchant_number, '601101436679714', 'Y', decode(substr(discover_merchant_number,0,6),'601172','Y','N')) as proc_by_mes"; break;
        }
        
        ps = db.getPreparedStatement(" select settle.batch_id, settle.batch_record_id " + extraSelect +
                                     " from " + tableName + " settle " + extraTable +
                                     " where settle.load_file_id = load_filename_to_load_file_id(?) and settle.test_flag = 'N' " + extraWhere +
                                     " order by settle.batch_id, settle.batch_record_id");
        ps.setString(1,loadFilename);
        rs = ps.executeQuery();
        
        while( rs.next() )
        {
          Thread thread = pool.getThread( new ReportLoadWorker(cardType,rs.getLong("batch_id"),rs.getLong("batch_record_id"),rs.getString("proc_by_mes")) );
          thread.start();
          System.out.print("Loading " + cardType + ": " + ++recCount + "              \r");
        }
        pool.waitForAllThreads();
        
        rs.close();
        ps.close();
        System.out.println();
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementTools(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ ps.close();   } catch( Exception ee ) {}
      try{ db.cleanUp(); } catch( Exception ee ) {}
    }
  }

  public static void addReportRecords( String cardType, long batchId, long batchRecId, String procByMeS )
  {
    SettlementDb        db          = new SettlementDb();
    SettlementRecord    rec         = null;
    
    try
    {
      db.connect();
      db.setAutoCommit(false);
    
      rec = db.loadSettlementRecord(cardType,batchId,batchRecId);
      if( rec.getData("card_type") == "" )
        rec.setData("card_type", cardType);
    
      if( "Y".equals(procByMeS) )
      {
        db._insertDTRecord(rec);
      }
    
      if ( rec.getData("load_filename").startsWith("mddf") )
      {
        db._insertExtDTRecord(rec);
      }
      db.commit();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementTools(" + cardType + "," + batchId + "," + batchRecId + ")",e.toString());
    }
    finally
    {
      try{ db.cleanUp(); } catch( Exception ee ) {}
    }
  }
  
  public static String generateReferenceNumber( String cardType, String bin, int usage, boolean cashAdvance, String debitCreditInd, Date settlementDate, long acqRefNum )
  {
    String                retVal      = "";
    
    try
    {
      // credits, re-presentments and cash advances all use fmt code '7'
      if ( "C".equals(debitCreditInd) || (usage > 1) || cashAdvance)
      {
        retVal += "7";
      }
      else    // default to fmt code '2'
      {
        retVal += "2";
      }
      retVal += bin;
      retVal += DateTimeFormatter.getFormattedDate(settlementDate,"yyDDD").substring(1);
      retVal += NumberFormatter.getPaddedLong(acqRefNum,11);
      retVal += String.valueOf(TridentTools.calcMod10CheckDigit(retVal));
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("generateReferenceNumber()",e.toString());
    }
    return( retVal );
  }
  
  public static String generateReferenceNumber( SettlementRecord rec )
  {
    long        acqRefNum         = rec.getLong("acq_reference_number");
    String      bin               = rec.getData("bin_number");
    boolean     cashAdvance       = rec.isCashDisbursement();
    String      ct                = "";
    String      debitCreditInd    = rec.getData("debit_credit_indicator");
    Date        settlementDate    = rec.getDate("batch_date","MM/dd/yyyy");
    int         usage             = 1;    // default is first presentment
  
          if ( rec instanceof SettlementRecordVisa )  { ct = "VS";                      }
    else  if ( rec instanceof SettlementRecordMC )    { ct = "MC";  usage = 0;          }
    else  /* default */                               { ct = rec.getData("card_type");  }
    
    return( generateReferenceNumber(ct,bin,usage,cashAdvance,debitCreditInd,settlementDate,acqRefNum) );
  }


  static public List getClassListVisa(SettlementRecord rec) {
    List<VisaProgram> classList = new ArrayList();
    try {
      if (rec.isCashDisbursement()) {
        classList.add(VisaProgram.builder().withClassList("IC_Visa_CashDisbursement").build());
      }
      else if (!rec.getData("region_merchant").startsWith("US") || !rec.getData("region_issuer").startsWith("US")) {
        classList.add(VisaProgram.builder().withClassList("IC_Visa_NotUSDomestic").build());
      }
      else if ("C".equals(rec.getData("debit_credit_indicator"))) {
        classList.add(VisaProgram.builder().withClassList("IC_Visa_CreditVoucher").build());
      }
      else {
        String aci = rec.getData("auth_returned_aci");
        String pidFormat = rec.isFieldBlank("purchase_id_format") ? rec.getData("enh_purchase_id_format") : rec.getData("purchase_id_format");
        String sicCode = rec.getData("sic_code");

        Predicate<VisaProgram> matchedAci = p -> StringUtils.isEmpty(p.getAciList()) || testDataIsInList(1, aci, p.getAciList());
        Predicate<VisaProgram> matchPidFormat = p -> StringUtils.isEmpty(p.getPurchaseIdFormatList()) || testDataIsInList(1, pidFormat, p.getPurchaseIdFormatList());
        Predicate<VisaProgram> matchSicCode = p -> {
          try {
            return isEligibleItem(true, sicCode, null, p.getEligibleSicCodes());
          }
          catch (Exception e) {
            SyncLog.LogEntry("getClassListVisa()", e.toString());
          }
          return false;
        };
        classList = InterchangeVisa.cpsProgramMap.values().stream().filter(matchedAci.and(matchPidFormat.and(matchSicCode))).collect(Collectors.toList());
      }
    }
    catch (Exception e) {
      SyncLog.LogEntry("getClassListVisa()", e.toString());
    }
    finally {
    }

    log.debug(classList.size() + " Visa CPS Program(s) for transaction id / tsys batch_id batch_record_id " +
            (StringUtils.isNotEmpty(rec.getData("trident_tran_id")) ? rec.getData("trident_tran_id"):
                    rec.getData("batch_id") + rec.getData("batch_record_id")) +
            " Eligible Visa Cps program  names " + Arrays.toString(classList.toArray()));
    return (classList);
  }


  
  public static List getIrdListMC( SettlementRecord rec )
  {
    List                        irdList     = null;
    IcProgramRulesMC            ruleSet     = null;
    
    try
    {
      // cash advance transactions have 
      // only one option for IRD so setup a hard-coded value
      if ( rec.isCashDisbursement() )
      {
        irdList = new ArrayList(1);
        ruleSet = new IcProgramRulesMC();
        ruleSet.setAuthRequiredFlags("NN");
        ruleSet.setMcAssignedIdFlags("NN");
        ruleSet.setTimelinessFlags("0000NNNN0000NNNN");
        ruleSet.setIcComplianceSwitch("N");
        ruleSet.setAddendumRequired(null);
        ruleSet.setIrd("--");
        irdList.add(ruleSet);
      }
      else
      {
        irdList = SettlementDb.loadIrdListMC(rec);
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("getIrdListMC()", e.toString());
    }
    finally
    {
    }
    return( irdList );
  }
  
  public static java.sql.Date getSettlementBatchDate()
  {
    return( getSettlementDate(MesDefaults.DK_ACH_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDateAmex()
  {
    return( getSettlementDate(MesDefaults.DK_AMEX_CLEARING_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDateDiscover()
  {
    return( getSettlementDate(MesDefaults.DK_DISCOVER_CLEARING_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDateMC()
  {
    return( getSettlementDate(MesDefaults.DK_MC_GCMS_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDateModBII()
  {
    return( getSettlementDate(MesDefaults.DK_MODBII_CLEARING_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDateVisa()
  {
    return( getSettlementDate(MesDefaults.DK_VISA_BASE_II_CUTOFF) );
  }
  
  public static java.sql.Date getSettlementDate(String dkCutoffTime)
  {
    java.sql.Date     cpd     = null;
    
    try
    {
      // setup the settlement date for this outgoing file
      // when the current time is past the association 
      // cutoff time then set the settlement date to the
      // following day.
      int cutoffTime = 0;
      
      if ( dkCutoffTime != null )
      {
        try{ cutoffTime = MesDefaults.getInt(dkCutoffTime); } catch( Exception ee ) {}
      }        
      
      Calendar cal = Calendar.getInstance();
      int now = (cal.get(Calendar.HOUR_OF_DAY) * 100) + cal.get(Calendar.MINUTE);
      if ( now >= cutoffTime )
      {
        cal.add(Calendar.DAY_OF_MONTH,1);
      }
      
      // clear all the time components before creating SQL date
      // this allows value to be used to set a cal with a midnight time
      cal.set(Calendar.HOUR_OF_DAY,0);
      cal.set(Calendar.MINUTE,0);
      cal.set(Calendar.SECOND,0);
      cal.set(Calendar.MILLISECOND,0);
      
      cpd = new java.sql.Date(cal.getTime().getTime());
    }
    catch( Exception e )
    {
      if ( cpd == null ) 
      {
        try   { cpd = new java.sql.Date( Calendar.getInstance().getTime().getTime() ); }
        catch ( Exception ee ) {} // do nothing, return null date
      }
      SyncLog.LogEntry("getSettlementDate(" + dkCutoffTime + ")",e.toString());
    }
    finally
    {
    }
    return( cpd );
  }
  
  public static List parseMcIccTags( String iccDataRaw )
  {
    return( parseMcIccTags(iccDataRaw,true) );
  }
  
  public static List parseMcIccTags( String iccDataRaw, boolean convertToEbcdic )
  {
    int       i               = 0;
    List      retVal          = new ArrayList();
    String    singleByteTags  = "82,84,91,95,9A,9C";
    String    textByte        = null;
    
    try
    {
      String    iccData         = null;
      String    tag             = null;
      String    len             = null;
      String    val             = "";
      
      iccData = (convertToEbcdic ? AsciiToEbcdicConverter.asciiToEbcdic(iccDataRaw) : iccDataRaw);
    
      // remove the trailing nulls by parsing the 
      // tag/length/value data from the raw icc data
      for( i = 0; i < iccData.length(); i+=2 )
      {
        textByte = iccData.substring(i,i+2);
        if ( "00".equals(textByte) )
        {
          break;
        }
      
        // build the tag
        tag = textByte + (singleByteTags.indexOf(textByte) >= 0 ? "" : iccData.substring(i+2,i+4));
        i += ((singleByteTags.indexOf(textByte) >= 0) ? 2 : 4);
      
        // parse the length
        len = iccData.substring(i,i+2);
      
        // build the value
        val = "";
        for( int j = 0; j < (int)(ByteUtil.parseHexString(len)[0]); ++j )
        {
          i   += 2;
          val += iccData.substring(i,i+2);
        }
        retVal.add( new McIccTLV(tag,len,val) );
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("parseMcIccTags()",e.toString());
    }
    return( retVal );
  }

  public static boolean isEligibleItem(boolean isListOfEligiblesNotIneligibles, String strItem, String strGroup, String itemList) throws Exception {
    boolean foundItem = isListOfEligiblesNotIneligibles;

    if (itemList != null) {
      // if list starts with "!:" then it is an "opposite" list
      if ("!:".equals(itemList.substring(0, 2))) {
        itemList = itemList.substring(2);
        isListOfEligiblesNotIneligibles = !isListOfEligiblesNotIneligibles;
      }

      // if this item is in the list, then it is found
      foundItem = (itemList.indexOf(strItem) >= 0);

      // if list starts with "~" then it is a "group" list
      if (!foundItem && "~".equals(itemList.substring(0, 1)) && strGroup != null) {
        // if this item is in the group list, then it is found
        foundItem = (itemList.indexOf(strGroup) >= 0);
      }

      // if this item is not in the list but the list has at least one range,
      //   then see if this item is within a range in the list
      if (!foundItem && (itemList.indexOf("-") >= 0)) {
        int intItem = Integer.parseInt(strItem);

        // iterate through the items in the list, searching in all the ranges
        String[] items = itemList.split(",");
        for (int i = 0; i < items.length; ++i) {
          String item = items[i];

          if (item.indexOf("-") >= 0)    // this is a range
          {
            String[] range = item.split("-");
            if (intItem >= Integer.parseInt(range[0].trim()) && intItem <= Integer.parseInt(range[1].trim())) {
              foundItem = true;
              break;
            }
          }
        }
      }
    }
    return (foundItem == isListOfEligiblesNotIneligibles);
  }

  public static final boolean testDataIsInList(int dataLength, String testData, String tokenList) {
    if ((tokenList == null) || (tokenList.length() < dataLength)) {
      SyncLog.LogEntry("SettlementTools.testDataIsInList():  bad token list..." + tokenList);

      return (false);
    }
    if ((testData == null) || (testData.length() > dataLength)) {
      SyncLog.LogEntry("SettlementTools.testDataIsInList():  bad test data..." + testData);
      return (false);
    }

    while (testData.length() < dataLength) {
      testData += " ";
    } // pad string

    String[] tokens = tokenList.split(",");

    for (int i = 0; i < tokens.length; ++i) {
      String thisToken = tokens[i];
      int thisLength = thisToken.length();

      if (thisLength == dataLength) {
        // token contains one value; test data matches or not
        if (testData.equals(thisToken)) {
          return (true);
        }
      }
      else if ((thisLength == (dataLength * 2) + 1) && (thisToken.charAt(dataLength) == '-')) {
        // token contains two values separated by a '-'; test data is in set (inclusive) or not
        String lo = thisToken.substring(0, dataLength);
        String hi = thisToken.substring(dataLength + 1, thisLength);
        if ((testData.compareTo(lo) >= 0) && (testData.compareTo(hi) <= 0)) {
          return (true);
        }
      }
      else {

        SyncLog.LogEntry("SettlementTools.testDataIsInList():   bad token..." + thisToken);
      }
    }

    return (false);
  }
 
  public static void main( String[] args )
  {
    try
    {
      String  cmd         = args[0];
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      if ( "addReportRecords".equals(cmd) )
      {
        SettlementTools.addReportRecords(args[1],Long.parseLong(args[2]),Long.parseLong(args[3]),args[4]);
      }
      else if ( "addReportRecordsFile".equals(cmd) )
      {
        SettlementTools.addReportRecordsFile(args[1]);
      }
      else if ( "addReject".equals(cmd) )
      {
        Date javaDate = null;
        if ( args.length > 4 )
        {
          javaDate = DateTimeFormatter.parseDate(args[4],"MM/dd/yyyy");
        }
        if ( javaDate == null ) { javaDate = Calendar.getInstance().getTime(); }
        
        String[] reasonCodes = args[3].indexOf("/") < 0 ? new String[] { args[3] } : args[3].split("/");
        SettlementTools.addReject(args[1],                    // cardType
                                  Long.parseLong(args[2]),    // recId
                                  new java.sql.Date(javaDate.getTime()),  // reject date
                                  reasonCodes);               // reason codes
      }
      else if ( "generateReferenceNumber".equals(cmd) )
      {
        System.out.println( SettlementTools.generateReferenceNumber(args[1],
                                                                    args[2],
                                                                    Integer.parseInt(args[3]),
                                                                    false,
                                                                    "D",
                                                                    DateTimeFormatter.parseDate(args[4],"MM/dd/yyyy"),
                                                                    Long.parseLong(args[5]) ) );
      }
      else
      {
        System.out.println("Invalid command: " + cmd);
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ com.mes.database.OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}
