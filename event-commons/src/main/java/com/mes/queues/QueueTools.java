/*@lineinfo:filename=QueueTools*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueTools.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-10-25 13:17:18 -0700 (Thu, 25 Oct 2012) $
  Version            : $Revision: 20650 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.io.File;
import java.sql.ResultSet;
import java.sql.Timestamp;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserAppType;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class QueueTools extends SQLJConnectionBase
{
  public QueueTools()
  {
  }

  public QueueTools(String connectString)
  {
    super(connectString);
  }

  private String getAppTypeDesc(int appType)
  {
    String  appTypeDesc   = "UNKN";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:52^7*/

//  ************************************************************
//  #sql [Ctx] { select  appsrctype_code
//          
//          from    org_app
//          where   app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  appsrctype_code\n         \n        from    org_app\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appTypeDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:58^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppTypeDesc(" + appType + ")", e.toString());
    }

    return appTypeDesc;
  }

  private String getAcctChangeAffiliate(long id, int type, UserBean user)
  {
    String result = "UNKN";

    try
    {
      // get the closest org_app parent
      int merchantOrg = 0;

      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_num 
//          from    organization o,
//                  account_change_request acr
//          where   acr.change_sequence_id = :id and
//                  acr.merchant_number = o.org_group
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_num  \n        from    organization o,\n                account_change_request acr\n        where   acr.change_sequence_id =  :1  and\n                acr.merchant_number = o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantOrg = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/

      UserAppType uat = new UserAppType();

      if(uat.getAppType(merchantOrg, 0))
      {
        result = getAppTypeDesc(uat.getAppType());
      }
    }
    catch(Exception e)
    {
      logEntry("getAcctChangeAffiliate()", e.toString());
    }

    return result;
  }

  private String getAppType(long appSeqNum)
  {
    String    result      = "UNKN";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:107^7*/

//  ************************************************************
//  #sql [Ctx] { select  appsrctype_code
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  appsrctype_code\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + appSeqNum + ")", e.toString());
    }

    return result;
  }

  private String getAppTypeMms(long requestId)
  {
    String    result      = "UNKN";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:129^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.appsrctype_code
//          
//          from    application app,
//                  mms_stage_info msi
//          where   msi.request_id = :requestId and
//                  msi.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.appsrctype_code\n         \n        from    application app,\n                mms_stage_info msi\n        where   msi.request_id =  :1  and\n                msi.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:137^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppTypeMms(" + requestId + ")", e.toString());
    }

    return result;
  }

  private String getAppTypeCalltag(long calltagId)
  {
    String              result  = "UNKN";
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      // use org_app, t_hierarchy, and mif to determine app type of merchant
      // specified in the acr table for this acr id
      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  oa.appsrctype_code    app_type
//          from    t_hierarchy   th,
//                  org_app       oa,
//                  equip_calltag ct,
//                  mif           m
//          where   ct.ct_seq_num = :calltagId and
//                  ct.merch_number = m.merchant_number and
//                  m.association_node = th.descendent and
//                  th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.ancestor = oa.hierarchy_node
//          order by th.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  oa.appsrctype_code    app_type\n        from    t_hierarchy   th,\n                org_app       oa,\n                equip_calltag ct,\n                mif           m\n        where   ct.ct_seq_num =  :1  and\n                ct.merch_number = m.merchant_number and\n                m.association_node = th.descendent and\n                th.hier_type =  :2  and\n                th.ancestor = oa.hierarchy_node\n        order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,calltagId);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.QueueTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^7*/

      rs = it.getResultSet();

      // first item in result set is the app type
      if(rs.next())
      {
        result = rs.getString("app_type");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAppTypeCalltag(" + calltagId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  private String getAppTypeAcr(long acrId)
  {
    String              result  = "UNKN";

    try
    {
      // see if app exists already
      int   appCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app.app_seq_num)
//          
//          from    application app,
//                  acr         acr
//          where   acr.acr_seq_num = :acrId and
//                  acr.app_seq_num = app.app_seq_num and
//                  acr.app_seq_num != -1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app.app_seq_num)\n         \n        from    application app,\n                acr         acr\n        where   acr.acr_seq_num =  :1  and\n                acr.app_seq_num = app.app_seq_num and\n                acr.app_seq_num != -1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,acrId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/

      if(appCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:218^9*/

//  ************************************************************
//  #sql [Ctx] { select  app.appsrctype_code
//            
//            from    application app,
//                    acr         a
//            where   a.acr_seq_num = :acrId and
//                    a.app_seq_num = app.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.appsrctype_code\n           \n          from    application app,\n                  acr         a\n          where   a.acr_seq_num =  :1  and\n                  a.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,acrId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^9*/
      }
      else
      {
        // use org_app, t_hierarchy, and mif to determine app type of merchant
        // specified in the acr table for this acr id
        /*@lineinfo:generated-code*//*@lineinfo:232^9*/

//  ************************************************************
//  #sql [Ctx] { select  oa.appsrctype_code    app_type
//            
//            from    t_hierarchy   th,
//                    org_app       oa,
//                    acr           acr,
//                    mif           m
//            where   acr.acr_seq_num = :acrId and
//                    acr.merch_number = m.merchant_number and
//                    m.association_node = th.descendent and
//                    th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                    th.ancestor = oa.hierarchy_node
//            order by th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  oa.appsrctype_code    app_type\n           \n          from    t_hierarchy   th,\n                  org_app       oa,\n                  acr           acr,\n                  mif           m\n          where   acr.acr_seq_num =  :1  and\n                  acr.merch_number = m.merchant_number and\n                  m.association_node = th.descendent and\n                  th.hier_type =  :2  and\n                  th.ancestor = oa.hierarchy_node\n          order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,acrId);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getAppTypeAcr(" + acrId + ")", e.toString());
    }

    return result;
  }


  private String getAffiliate(long id, int type, int itemType, UserBean user)
  {
    String result = "";

    try
    {
      switch(itemType)
      {
        case MesQueues.Q_ITEM_TYPE_ACCT_CHANGE:
          result = getAcctChangeAffiliate(id, type, user);
          break;

        case MesQueues.Q_ITEM_TYPE_PROGRAMMING:
        case MesQueues.Q_ITEM_TYPE_MMS_ERROR:
          result = getAppTypeMms(id);
          break;

        case MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST:
          result = getAppTypeAcr(id);
          break;

        case MesQueues.Q_ITEM_TYPE_CALLTAG:
          result = getAppTypeCalltag(id);
          break;

        case MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS:
          result = "";
          break;

        case MesQueues.Q_ITEM_TYPE_ACTIVATE:
        case MesQueues.Q_ITEM_TYPE_MMS:
        case MesQueues.Q_ITEM_TYPE_ACTIVATION:
        case MesQueues.Q_ITEM_TYPE_DEPLOYMENT:
        case MesQueues.Q_ITEM_TYPE_CBT_CREDIT:
        case MesQueues.Q_ITEM_TYPE_CBT_DOCUMENTS:
        case MesQueues.Q_ITEM_TYPE_CBT_REVIEW:
        case MesQueues.Q_ITEM_TYPE_BBT_QA:
        case MesQueues.Q_ITEM_TYPE_REVIEW_QUEUE:
        default:
          result = getAppType(id);
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("getAffiliate()", e.toString());
    }

    return result;
  }

  private int getItemTypeFromType(int type)
  {
    int itemType = 0;

    try
    {

      /*@lineinfo:generated-code*//*@lineinfo:316^7*/

//  ************************************************************
//  #sql [Ctx] { select  item_type 
//          from    q_types
//          where   type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  item_type  \n        from    q_types\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^7*/

    }
    catch(Exception e)
    {
      logEntry("getItemTypeFromType(" + type + ")", e.toString());
    }

    return itemType;
  }

  private void setRushField(long id, int type)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:338^7*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//          set     is_rush = 1
//          where   id      = :id and
//                  type    = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data\n        set     is_rush = 1\n        where   id      =  :1  and\n                type    =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^7*/
    }
    catch(Exception e)
    {
      logEntry("setRushField(" + id + ":" + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void addQueue(long id, int type, UserBean user)
  {
    addQueue(id,type,user,false);
  }
  public void addQueue(long id, int type, UserBean user,boolean is_rush)
  {
    try
    {
      connect();

      int     itemType  = 0;
      String  loginName = "SYSTEM";

      if(user == null)
      {
        loginName = "SYSTEM";
      }
      else
      {
        loginName = user.getLoginName();
      }

      itemType = getItemTypeFromType(type);

      // only insert if there isn't already an item there
      int queueCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:383^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(id)
//          
//          from    q_data
//          where   id = :id and
//                  type  = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(id)\n         \n        from    q_data\n        where   id =  :1  and\n                type  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^7*/

      if(queueCount == 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:394^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//            (
//              id,
//              type,
//              item_type,
//              owner,
//              date_created,
//              source,
//              affiliate,
//              is_rush,
//              last_changed,
//              last_user
//            )
//            values
//            (
//              :id,
//              :type,
//              :itemType,
//              :MesQueues.Q_OWNER_MES,
//              sysdate,
//              :loginName,
//              :getAffiliate(id, type, itemType, user),
//              :is_rush,
//              sysdate,
//              :loginName
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_520 = getAffiliate(id, type, itemType, user);
   String theSqlTS = "insert into q_data\n          (\n            id,\n            type,\n            item_type,\n            owner,\n            date_created,\n            source,\n            affiliate,\n            is_rush,\n            last_changed,\n            last_user\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            sysdate,\n             :5 ,\n             :6 ,\n             :7 ,\n            sysdate,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setInt(4,MesQueues.Q_OWNER_MES);
   __sJT_st.setString(5,loginName);
   __sJT_st.setString(6,__sJT_520);
   __sJT_st.setBoolean(7,is_rush);
   __sJT_st.setString(8,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^9*/

        commit();
      }
      else
      {
        logEntry("addQueue(" + id + ", " + type + ")", "can't insert item into queue twice");
      }

      // log the action
      QueueLogger.log(id,QueueLogger.QLA_ADD,MesQueues.Q_NONE,type,user);
    }
    catch(Exception e)
    {
      logEntry("addQueue(" + id + ", " + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void deleteQueue(long id, int type, UserBean user)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      // log the action
      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  id
//          from    q_data
//          where   id = :id
//                  and type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  id\n        from    q_data\n        where   id =  :1 \n                and type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.queues.QueueTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:460^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        QueueLogger.log(id,QueueLogger.QLA_DELETE,type,MesQueues.Q_NONE,user);
      }

      /*@lineinfo:generated-code*//*@lineinfo:467^7*/

//  ************************************************************
//  #sql [Ctx] { delete  q_data
//          where   id    = :id and
//                  type  = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  q_data\n        where   id    =  :1  and\n                type  =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:472^7*/
    }
    catch(Exception e)
    {
      logEntry("deleteQueue(" + id + ", " + type + ", " + user.getLoginName()
        + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public void deleteQueueByItemType(long id, int itemType, UserBean user)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      // log the action
      /*@lineinfo:generated-code*//*@lineinfo:497^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  id,type
//          from    q_data
//          where   id            = :id
//                  and item_type = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  id,type\n        from    q_data\n        where   id            =  :1 \n                and item_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.queues.QueueTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        int type = rs.getInt("type");
        QueueLogger.log(id,QueueLogger.QLA_DELETE,type,MesQueues.Q_NONE,user);
      }

      /*@lineinfo:generated-code*//*@lineinfo:511^7*/

//  ************************************************************
//  #sql [Ctx] { delete  q_data
//          where   id         = :id and
//                  item_type  = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  q_data\n        where   id         =  :1  and\n                item_type  =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^7*/
    }
    catch(Exception e)
    {
      logEntry("deleteQueueByItemType(" + id + ", " + itemType + ", " + user.getLoginName()
        + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public boolean isItemInQueue(long id, int type)
  {
    try
    {
      connect();

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:539^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          from    q_data
//          where   id = :id and type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        from    q_data\n        where   id =  :1  and type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:544^7*/

      return (count>0);
    }
    catch(Exception e)
    {
      logEntry("isItemInQueue(" + id + ", " + type + ")", e.toString());
      return false;
    }
    finally
    {
      cleanUp();
    }
  }


  public boolean isItemInItemType(long id, int itemType)
  {
    try
    {
      connect();

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:568^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          from    q_data
//          where   id = :id and item_type = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        from    q_data\n        where   id =  :1  and item_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:573^7*/

      return (count>0);
    }
    catch(Exception e)
    {
      logEntry("isItemInItemType(" + id + ", " + itemType + ")", e.toString());
      return false;
    }
    finally
    {
      cleanUp();
    }
  }


  public boolean isUnlocked(long id, int type, UserBean user)
  {
    boolean   isUnlocked = false;

    try
    {
      String thisUser = user.getLoginName();
      int    recCnt   = 0;

      /*@lineinfo:generated-code*//*@lineinfo:598^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    q_data
//          where   id   = :id and
//                  type = :type
//              and nvl(locked_by,:thisUser) != :thisUser
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    q_data\n        where   id   =  :1  and\n                type =  :2 \n            and nvl(locked_by, :3 ) !=  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   __sJT_st.setString(3,thisUser);
   __sJT_st.setString(4,thisUser);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^7*/

      if( recCnt == 0 )
      {
        isUnlocked = true;
      }
    }
    catch(Exception e)
    {
      logEntry("isUnlocked(" + id + ", " + type + ")", e.toString());
    }

    return isUnlocked;
  }

  public boolean lockItem(long id, int type, UserBean user)
  {
    boolean       lockSuccess     = false;
    File          lockFile        = null;
    StringBuffer  lockFilename    = new StringBuffer("");
    Timestamp     lockDate        = null;

    try
    {
      connect();

      lockFilename.append(Long.toString(id));
      lockFilename.append("_");
      lockFilename.append(Integer.toString(type));
      lockFilename.append(".lck");

      lockFile = new File(lockFilename.toString());
      lockFile.deleteOnExit();

      if(isUnlocked(id, type, user) && lockFile.createNewFile())
      {
        // set the lock on the item
        /*@lineinfo:generated-code*//*@lineinfo:642^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//            set     locked_by = :user.getLoginName()
//            where   id    = :id   and
//                    type  = :type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_521 = user.getLoginName();
   String theSqlTS = "update  q_data\n          set     locked_by =  :1 \n          where   id    =  :2    and\n                  type  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_521);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:648^9*/

        lockSuccess = true;

        // mark date/time that this user locked this item for SLA stuff
        /*@lineinfo:generated-code*//*@lineinfo:653^9*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lockDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:658^9*/

        user.setQueueLockData(id, type, lockDate);
      }
    }
    catch(Exception e)
    {
      logEntry("lockItem(" + id + ", " + type + ", " + user.getLoginName() + ")", e.toString());
    }
    finally
    {
      cleanUp();

      if(lockFile != null)
      {
        lockFile.delete();
      }
    }

    return lockSuccess;
  }

  public void releaseUserLock(UserBean user)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:686^7*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//          set     locked_by = null
//          where   locked_by = :user.getLoginName()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_522 = user.getLoginName();
   String theSqlTS = "update  q_data\n        set     locked_by = null\n        where   locked_by =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_522);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:691^7*/
    }
    catch(Exception e)
    {
      logEntry("releaseUserLock(" + user.getLoginName() + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void releaseItemLock(long id, int type)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:709^7*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//          set     locked_by = null
//          where   id = :id and
//                  type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data\n        set     locked_by = null\n        where   id =  :1  and\n                type =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:715^7*/
    }
    catch(Exception e)
    {
      logEntry("releaseUserLock(" + id + ", " + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void copyQueue(long id, int type, int dest, UserBean user, boolean is_rush)
  {
    int     itemType  = 0;
    int     owner     = 0;
    String  source    = "";
    String  affiliate = "";
    int     rush      = (is_rush? 1:0);

    try
    {
      connect();

      String userLoginName = "SYSTEM";
      if(user != null)
      {
        userLoginName = user.getLoginName();
      }


      // can't copy from one queue into the same queue
      if(type != dest)
      {
        /*@lineinfo:generated-code*//*@lineinfo:749^9*/

//  ************************************************************
//  #sql [Ctx] { select  item_type,
//                    owner,
//                    source,
//                    affiliate,
//                    is_rush
//            
//            from    q_data
//            where   id = :id and
//                    type = :type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  item_type,\n                  owner,\n                  source,\n                  affiliate,\n                  is_rush\n           \n          from    q_data\n          where   id =  :1  and\n                  type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   owner = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   source = (String)__sJT_rs.getString(3);
   affiliate = (String)__sJT_rs.getString(4);
   is_rush = __sJT_rs.getBoolean(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:764^9*/

        /*@lineinfo:generated-code*//*@lineinfo:766^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//            (
//              id,
//              type,
//              item_type,
//              owner,
//              date_created,
//              source,
//              affiliate,
//              is_rush,
//              last_changed,
//              last_user
//            )
//            values
//            (
//              :id,
//              :dest,
//              :itemType,
//              sysdate,
//              :source,
//              :affiliate,
//              :is_rush,
//              sysdate,
//              :userLoginName
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n          (\n            id,\n            type,\n            item_type,\n            owner,\n            date_created,\n            source,\n            affiliate,\n            is_rush,\n            last_changed,\n            last_user\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n             :4 ,\n             :5 ,\n             :6 ,\n            sysdate,\n             :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,dest);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setString(4,source);
   __sJT_st.setString(5,affiliate);
   __sJT_st.setBoolean(6,is_rush);
   __sJT_st.setString(7,userLoginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:793^9*/
      }

      // log the action
      QueueLogger.log(id,QueueLogger.QLA_COPY,type,dest,user);
    }
    catch(Exception e)
    {
      logEntry("copyQueue(" + id + ", " + type + ", " + dest + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void moveQueue(long id, int type, int dest, UserBean user, String note)
  {
    // resolve is rush
    boolean is_rush = false;
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:816^7*/

//  ************************************************************
//  #sql [Ctx] { select is_rush
//          
//          from   q_data
//          where  id = :id and type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select is_rush\n         \n        from   q_data\n        where  id =  :1  and type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   is_rush = __sJT_rs.getBoolean(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:822^7*/

    }
    catch(java.sql.SQLException se)
    {
    }
    catch(Exception e)
    {
      logEntry("moveQueue[RUSH](" + id + ", " + type + ", " + dest + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    moveQueue(id,type,dest,user,note,is_rush);
  }

  public void moveQueue(long id, int type, int dest, UserBean user, String note, boolean is_rush)
  {

    try
    {
      connect();

      String userLoginName = "SYSTEM";
      if(user != null)
      {
        userLoginName = user.getLoginName();
      }

      //first get item type
      int itemType = getItemTypeFromType(dest);


      // move the item from it's current queue to the requested queue
      // resolve item type from dest incase moving from one queue item type to another
      /*@lineinfo:generated-code*//*@lineinfo:859^7*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//          set     type          = :dest,
//                  last_changed  = sysdate,
//                  last_user     = :userLoginName,
//                  item_type     = :itemType,
//                  is_rush       = :is_rush
//          where   id            = :id and
//                  type          = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data\n        set     type          =  :1 ,\n                last_changed  = sysdate,\n                last_user     =  :2 ,\n                item_type     =  :3 ,\n                is_rush       =  :4 \n        where   id            =  :5  and\n                type          =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,dest);
   __sJT_st.setString(2,userLoginName);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setBoolean(4,is_rush);
   __sJT_st.setLong(5,id);
   __sJT_st.setInt(6,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:869^7*/

      // now add a note showing the change of status
      //  (binding it to the destination queue NOT the source queue)
      if (note != null)
      {
        QueueNotes.addNote((int)id, dest, userLoginName, note);
      }

      // log the action
      QueueLogger.log(id,QueueLogger.QLA_MOVE,type,dest,user);
    }
    catch(Exception e)
    {
      logEntry("moveQueue(" + id + ", " + type + ", " + dest + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  //assumes that only one queue type is being used for a particular item type
  //returns the queue type being used for that item type.  NOTE:  If more than
  //one queue type is being used for the itemType in question, an exception will occur
  public int getCurrentType(long id, int itemType)
  {
    int type = -1;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:903^7*/

//  ************************************************************
//  #sql [Ctx] { select  type 
//          from    q_data
//          where   id        = :id and
//                  item_type = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  type  \n        from    q_data\n        where   id        =  :1  and\n                item_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   type = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:909^7*/
    }
    catch(java.sql.SQLException se)
    {
      // stop logging all the "no rows found" exceptions.  they clog java_log
    }
    catch(Exception e)
    {
      logEntry("getCurrentType(" + id + ", " + itemType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return type;
  }


  public boolean isItemLocked(long id, int type, String user)
  {
    boolean result = false;

    try
    {
      connect();

      String lockedBy = "";

      /*@lineinfo:generated-code*//*@lineinfo:938^7*/

//  ************************************************************
//  #sql [Ctx] { select  qd.locked_by
//          
//          from    q_data    qd
//          where   qd.type = :type and
//                  qd.id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  qd.locked_by\n         \n        from    q_data    qd\n        where   qd.type =  :1  and\n                qd.id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   __sJT_st.setLong(2,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lockedBy = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:945^7*/

      if(lockedBy != null && ! lockedBy.equals(user))
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("isItemLocked(" + id + ", " + type + ", " + user + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public int getItemType(int type)
  {
    int     result      = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:972^7*/

//  ************************************************************
//  #sql [Ctx] { select  max( decode(type, :type, item_type, 0) )
//          
//          from    q_types
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max( decode(type,  :1 , item_type, 0) )\n         \n        from    q_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:977^7*/
    }
    catch(Exception e)
    {
      logEntry("getItemType(" + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public String getQueueTypeDescription(int type)
  {
    String result = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:999^7*/

//  ************************************************************
//  #sql [Ctx] { select  description
//          
//          from    q_types
//          where   type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description\n         \n        from    q_types\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1005^7*/
    }
    catch(Exception e)
    {
      logEntry("getQueueTypeDescription(" + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public String getQueueTypeFunctionURL(int type)
  {
    String result = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1027^7*/

//  ************************************************************
//  #sql [Ctx] { select  function_url
//          
//          from    q_types
//          where   type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  function_url\n         \n        from    q_types\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1033^7*/
    }
    catch(Exception e)
    {
      logEntry("getQueueTypeFunctionURL(" + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public static boolean lockQueueItem(long id, int type, UserBean user)
  {
    boolean result = false;

    try
    {
      QueueTools    qt = new QueueTools();

      result  = qt.lockItem(id, type, user);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::lockQueueItem()", e.toString());
    }

    return result;
  }

  public static void insertQueue(long id, int type, UserBean user, boolean is_rush)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.addQueue(id, type, user, is_rush);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::insertQueue()", e.toString());
    }
  }

  public static void insertQueue(long id, int type, UserBean user)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.addQueue(id, type, user, false);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::insertQueue()", e.toString());
    }
  }

  public static void insertQueue(long id, int type)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.addQueue(id, type, null);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::insertQueue(id,type)", e.toString());
    }
  }

  public static void removeQueue(long id, int type)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.deleteQueue(id, type, null);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::removeQueue()", e.toString());
    }
  }


  public static void removeQueue(long id, int type, UserBean user)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.deleteQueue(id, type, user);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::removeQueue()", e.toString());
    }
  }

  public static void removeQueueByItemType(long id, int itemType)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.deleteQueueByItemType(id, itemType, null);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::removeQueueByItemType()", e.toString());
    }
  }


  public static void removeQueueByItemType(long id, int itemType, UserBean user)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.deleteQueueByItemType(id, itemType, user);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::removeQueueByItemType()", e.toString());
    }
  }

  public static void releaseLock(UserBean user)
  {
    try
    {
      QueueTools qt = new QueueTools();

      qt.releaseUserLock(user);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::releaseLock()", e.toString());
    }
  }

  public static void releaseLock(long id, int type)
  {
    try
    {
      QueueTools qt = new QueueTools();

      qt.releaseItemLock(id, type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::releaseLock(" + id + ", " + type + ")", e.toString());
    }
  }

  public static void moveQueueItem(long id, int type, int dest, UserBean user, String note)
  {
    try
    {
      QueueTools qt = new QueueTools();

      qt.moveQueue(id, type, dest, user, note);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::moveQueueItem(" + id + ", " + type + ", " + dest + ")", e.toString());
    }
  }

  public static void moveQueueItem(long id, int type, int dest, UserBean user, String note, boolean is_rush)
  {
    try
    {
      QueueTools qt = new QueueTools();

      qt.moveQueue(id, type, dest, user, note, is_rush);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::moveQueueItem[RUSH](" + id + ", " + type + ", " + dest + ")", e.toString());
    }
  }

  public static void copyQueueItem(long id, int type, int dest, UserBean user)
  {
    copyQueueItem(id, type, dest, user, false);
  }
  public static void copyQueueItem(long id, int type, int dest, UserBean user, boolean is_rush)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.copyQueue(id, type, dest, user, is_rush);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::copyQueueItem(" + id + ", " + type + ", " + dest + ")", e.toString());
    }
  }

  public static int getCurrentQueueType(long id, int itemType)
  {
    int type = -1;

    try
    {
      QueueTools qt = new QueueTools();
      type = qt.getCurrentType(id, itemType);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getCurrentQueueType(" + id + ", " + itemType + ")", e.toString());
    }

    return type;
  }

  public static boolean isLocked(long id, int type, String user)
  {
    boolean result = false;

    try
    {
      QueueTools qt = new QueueTools();

      result = qt.isItemLocked(id, type, user);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::isLocked(" + id + ", " + type + ", " + user + ")", e.toString());
    }

    return result;
  }

  public static int getQueueItemType(int type)
  {
    int result = 0;

    try
    {
      QueueTools qt = new QueueTools();

      result = qt.getItemType(type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getQueueItemType(" + type + ")", e.toString());
    }

    return result;
  }

  public static String getQueueDescription(int type)
  {
    String result = "";

    try
    {
      QueueTools qt = new QueueTools();

      result = qt.getQueueTypeDescription(type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getQueueDescription(" + type + ")", e.toString());
    }

    return result;
  }


  public static void setIsRush(long id, int type)
  {
    try
    {
      QueueTools qt = new QueueTools();
      qt.setRushField(id,type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::setIsRush(" + id + ":" + type + ")", e.toString());
    }
  }


  public static String getQueueFunctionURL(int type)
  {
    String result = "";

    try
    {
      QueueTools qt = new QueueTools();

      result = qt.getQueueTypeFunctionURL(type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getQueueFunctionURL(" + type + ")", e.toString());
    }

    return result;
  }

  public static boolean hasItemEnteredQueue(long id, int itemType)
  {
    boolean result = false;

    try
    {
      QueueTools qt = new QueueTools();

      result = qt.isItemInItemType(id,itemType);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::hasItemEnteredQueue(" + id + ", " + itemType + ")", e.toString());
    }

    return result;
  }

  /**
   * _getResidentQueues()
   *
   * Returns array of queue types indicating the queues a particular queue item
   * resides in given its item type and id.
   */
  public int[] _getResidentQueues(long id, int itemType)
  {
    int[] rq = null;

    try
    {
      connect();

      int cnt=0;

      /*@lineinfo:generated-code*//*@lineinfo:1369^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//          
//          from    q_data
//          where   id=:id and item_type = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n         \n        from    q_data\n        where   id= :1  and item_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1375^7*/

      if(cnt>0) {
        rq = new int[cnt];

        ResultSetIterator it;

        /*@lineinfo:generated-code*//*@lineinfo:1382^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  type
//            from    q_data
//            where   id=:id and item_type = :itemType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type\n          from    q_data\n          where   id= :1  and item_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.queues.QueueTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,itemType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.queues.QueueTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1387^9*/
        ResultSet rs = it.getResultSet();
        int i=0;
        while(rs.next())
          rq[i++]=rs.getInt(1);
        rs.close();
        it.close();
      } else
        rq=new int[0];
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::_getResidentQueues(" + id + ", " + itemType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return rq;
  }

  public static int[] getResidentQueues(long id, int itemType)
  {
    int[] result = null;

    try
    {
      result = (new QueueTools())._getResidentQueues(id,itemType);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getResidentQueues(" + id + ", " + itemType + ")", e.toString());
    }

    return result;
  }

  public int _getQueueCount(int type)
  {
    int cnt = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1433^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//          
//          from    q_data
//          where   type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n         \n        from    q_data\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1439^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getQueueCount(" + type + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return cnt;
  }

  public static int getQueueCount(int type)
  {
    int cnt = 0;

    try
    {
      cnt = (new QueueTools())._getQueueCount(type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::getQueueCount(" + type + ")", e.toString());
    }

    return cnt;
  }

  public static boolean isInQueue(long id, int type)
  {
    boolean inQueue = false;

    try
    {
      inQueue = (new QueueTools()).isItemInQueue(id,type);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::isInQueue(id="
        + id +", type=" + type + ")", e.toString());
    }

    return inQueue;
  }

  public boolean validRebuttalMove(long controlNumber, int currentQueue)
  {
    //easy ones first
    if( currentQueue == MesQueues.Q_CHARGEBACKS_MCHB)
    {
      return true;
    }

    if( currentQueue == MesQueues.Q_CHARGEBACKS_MERCH_REB ||
        currentQueue == MesQueues.Q_CHARGEBACKS_MERCH_REB_HIGH ||
        currentQueue == MesQueues.Q_CHARGEBACKS_REPR ||
        currentQueue == MesQueues.Q_CHARGEBACKS_REMC ||
        currentQueue == MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL)
    {
      return false;
    }

    if( currentQueue == MesQueues.Q_CHARGEBACKS_COMPLETED)
    {
      //need to figure out where this completed from... if REPR or REMC
      //we aren't going to allow the item to move to rebuttals
      int lastType = 0;
      try
      {

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1514^9*/

//  ************************************************************
//  #sql [Ctx] { select
//              decode(cac.action_code, 'REMC',1,'REPR',1,0)
//            
//            from
//              network_chargeback_activity nca,
//              chargeback_action_codes cac
//            where
//              nca.cb_load_sec = :controlNumber
//            and cac.short_action_code = nca.action_code
//            and nca.action_date =
//            ( select max(action_date) from
//              network_chargeback_activity
//              where cb_load_sec = :controlNumber)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n            decode(cac.action_code, 'REMC',1,'REPR',1,0)\n           \n          from\n            network_chargeback_activity nca,\n            chargeback_action_codes cac\n          where\n            nca.cb_load_sec =  :1 \n          and cac.short_action_code = nca.action_code\n          and nca.action_date =\n          ( select max(action_date) from\n            network_chargeback_activity\n            where cb_load_sec =  :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.queues.QueueTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,controlNumber);
   __sJT_st.setLong(2,controlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1530^9*/

      }
      catch(Exception e)
      {
        //leave at 0
      }
      finally
      {
        cleanUp();
      }

      if (lastType > 0)
      {
        return false;
      }

    }

    return true;
  }

  public static boolean validRebuttalQMove(long controlNumber, int currentQueue)
  {
    boolean result = false;

    try
    {
      QueueTools    qt = new QueueTools();

      result  = qt.validRebuttalMove(controlNumber, currentQueue);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("QueueTools::validRebuttalMove()", e.toString());
    }

    return result;
  }


}/*@lineinfo:generated-code*/