package com.mes.payvision;

import java.sql.Timestamp;
import java.util.Date;

public class ApiResponse
{
  private int code;
  private String message;
  private long requestId;
  private Date requestDate;
  private String requestType;
  private String hostAddr;
  private String endpoint;
  private String userName;
  private int crMemberId;
  private String crMemberGuid;
  private int memberId;
  private Date startDate;
  private Date endDate;

  public ApiResponse() {	  
  }
 
  public ApiResponse(ApiRequest request, int code, String message)
  {
    this.code = code;
    this.message = message;
    requestId = request.getRequestId();
    requestDate = request.getRequestDate();
    requestType = request.getRequestType();
    hostAddr = request.getHostAddr();
    endpoint = request.getEndpoint();
    userName = request.getUserName();
    crMemberId = request.getCrMemberId();
    crMemberGuid = request.getCrMemberGuid();
    memberId = request.getMemberId();
    startDate = request.getStartDate();
    endDate = request.getEndDate();
  }

  public void setCode(int code)
  {
    this.code = code;
  }
  public int getCode()
  {
    return code;
  }

  public void setMessage(String message)
  {
    this.message = message;
  }
  public String getMessage()
  {
    return message;
  }

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    
    return requestId;
  }

  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  public Date getRequestDate()
  {
    return requestDate;
  }
  public void setRequestTs(Timestamp requestTs)
  {
    requestDate = ApiRequest.tsToDate(requestTs);
  }
  public Timestamp getRequestTs()
  {
    return ApiRequest.dateToTs(requestDate);
  }

  public void setRequestType(String requestType)
  {
    this.requestType = requestType;
  }
  public String getRequestType()
  {
    return requestType;
  }

  public void setHostAddr(String hostAddr)
  {
    this.hostAddr = hostAddr;
  }
  
  public String getHostAddr()
  {
    return hostAddr;
  }
  
  public void setEndpoint(String endpoint)
  {
    this.endpoint = endpoint;
  }
  
  public String getEndpoint()
  {
    return endpoint;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCrMemberId(int crMemberId)
  {
    this.crMemberId = crMemberId;
  }
  public int getCrMemberId()
  {
    return crMemberId;
  }

  public void setCrMemberGuid(String crMemberGuid)
  {
    this.crMemberGuid = crMemberGuid;
  }
  public String getCrMemberGuid()
  {
    return crMemberGuid;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = ApiRequest.tsToDate(startTs);
  }
  public Timestamp getStartTs()
  {
    return ApiRequest.dateToTs(startDate);
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = ApiRequest.tsToDate(endTs);
  }
  public Timestamp getEndTs()
  {
    return ApiRequest.dateToTs(endDate);
  }

  public String toString()
  {
    return "ApiResponse [ "
      + "code: " + code
      + ", message: " + message
      + ", req id: " + requestId
      + ", req date: " + requestDate
      + ", req type: " + requestType
      + ", host pt: " + hostAddr
      + ", end pt: " + endpoint
      + ", user: " + userName
      + ", cr mem id: " + crMemberId
      + ", cr mem guid: " + crMemberGuid
      + ", mem id: " + memberId
      + ", start date: " + startDate
      + ", end date: " + endDate
      + " ]";
  }
}
