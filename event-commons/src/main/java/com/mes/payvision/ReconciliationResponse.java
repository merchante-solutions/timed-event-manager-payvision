package com.mes.payvision;

import java.util.ArrayList;
import java.util.List;

public class ReconciliationResponse extends ApiResponse
{
  private List summaries = new ArrayList();

  public ReconciliationResponse(){	  
  }

  public ReconciliationResponse(ReconciliationRequest request, int code, 
    String message)
  {
    super(request,code,message);
  }

  public List getSummaries()
  {
    return summaries;
  }
  public void addSummary(ReconciliationSummary summary)
  {
    summaries.add(summary);
  }
}