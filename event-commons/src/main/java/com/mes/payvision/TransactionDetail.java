package com.mes.payvision;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class TransactionDetail
{
  private long requestId;
  private int memberId;
  private long merchAcctId;
  private long tranId;
  private String tranGuid;
  private String trackMemCode;
  private BigDecimal origAmount;
  private int origCurrencyCode;
  private BigDecimal merchAmount;
  private BigDecimal currencyRate;
  private long cardId;
  private String cardGuid;
  private String cardTypeId;
  private Date tranDate;
  private int countryCode;
  private String batchDate;
  private BigDecimal fxAmountBase;

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public void setTranGuid(String tranGuid)
  {
    this.tranGuid = tranGuid;
  }
  public String getTranGuid()
  {
    return tranGuid;
  }

  public void setTrackMemCode(String trackMemCode)
  {
    this.trackMemCode = trackMemCode;
  }
  public String getTrackMemCode()
  {
    return trackMemCode;
  }

  public void setOrigAmount(BigDecimal origAmount)
  {
    this.origAmount = origAmount;
  }
  public BigDecimal getOrigAmount()
  {
    return origAmount;
  }

  public void setOrigCurrencyCode(int origCurrencyCode)
  {
    this.origCurrencyCode = origCurrencyCode;
  }
  public int getOrigCurrencyCode()
  {
    return origCurrencyCode;
  }

  public void setMerchAmount(BigDecimal merchAmount)
  {
    this.merchAmount = merchAmount;
  }
  public BigDecimal getMerchAmount()
  {
    return merchAmount;
  }

  public void setCurrencyRate(BigDecimal currencyRate)
  {
    this.currencyRate = currencyRate;
  }
  public BigDecimal getCurrencyRate()
  {
    return currencyRate;
  }

  public void setCardId(long cardId)
  {
    this.cardId = cardId;
  }
  public long getCardId()
  {
    return cardId;
  }

  public void setCardGuid(String cardGuid)
  {
    this.cardGuid = cardGuid;
  }
  public String getCardGuid()
  {
    return cardGuid;
  }

  public void setCardTypeId(String cardTypeId)
  {
    this.cardTypeId = cardTypeId;
  }
  public String getCardTypeId()
  {
    return cardTypeId;
  }

  public void setTranDate(Date tranDate)
  {
    this.tranDate = tranDate;
  }
  public Date getTranDate()
  {
    return tranDate;
  }
  public void setTranTs(Timestamp tranTs)
  {
    tranDate = ApiRequest.tsToDate(tranTs);
  }
  public Timestamp getTranTs()
  {
    return ApiRequest.dateToTs(tranDate);
  }

  public void setCountryCode(int countryCode)
  {
    this.countryCode = countryCode;
  }
  public int getCountryCode()
  {
    return countryCode;
  }

  public void setBatchDate(String batchDate)
  {
    this.batchDate = batchDate;
  }
  public String getBatchDate()
  {
    return batchDate;
  }

  public void setFxAmountBase(BigDecimal fxAmountBase)
  {
    this.fxAmountBase = fxAmountBase;
  }
  public BigDecimal getFxAmountBase()
  {
    return fxAmountBase;
  }

  public String toString()
  {
    return "TransactionDetail [ "
      + "req id: " + requestId
      + ", mem id: " + memberId
      + ", merch acct id: " + merchAcctId
      + ", tran id: " + tranId
      + ", tran guid: " + tranGuid
      + ", track mem code: " + trackMemCode
      + ", org amt: " + origAmount
      + ", org cur code: " + origCurrencyCode
      + ", merch amt: " + merchAmount
      + ", cur rate: " + currencyRate
      + ", card id: " + cardId
      + ", card guid: " + cardGuid
      + ", card type: " + cardTypeId
      + ", tran date: " + tranDate
      + ", country code: " + countryCode
      + ", batch date: " + batchDate
      + ", fx amt base: " + (fxAmountBase != null ? ""+fxAmountBase : "--")
      + " ]";
  }
}