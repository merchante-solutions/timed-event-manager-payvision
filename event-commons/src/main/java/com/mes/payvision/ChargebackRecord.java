package com.mes.payvision;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ChargebackRecord
{
  private long requestId;
  private int memberId;
  private long cbId;
  private String caseId;
  private int cbTypeId;
  private BigDecimal amount;
  private int currencyCode;
  private Date procDate;
  private String reasonCode;
  private long cardId;
  private String cardGuid;
  private Date tranDate;
  private long tranId;
  private long merchAcctId;

  private List entryItems = new ArrayList();

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setCbId(long cbId)
  {
    this.cbId = cbId;
  }
  public long getCbId()
  {
    return cbId;
  }

  public void setCaseId(String caseId)
  {
    this.caseId = caseId;
  }
  public String getCaseId()
  {
    return caseId;
  }

  public void setCbTypeId(int cbTypeId)
  {
    this.cbTypeId = cbTypeId;
  }
  public int getCbTypeId()
  {
    return cbTypeId;
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setCurrencyCode(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }
  public int getCurrencyCode()
  {
    return currencyCode;
  }

  public void setProcDate(Date procDate)
  {
    this.procDate = procDate;
  }
  public Date getProcDate()
  {
    return procDate;
  }
  public void setProcTs(Timestamp procTs)
  {
    procDate = ApiRequest.tsToDate(procTs);
  }
  public Timestamp getProcTs()
  {
    return ApiRequest.dateToTs(procDate);
  }

  public void setReasonCode(String reasonCode)
  {
    this.reasonCode = reasonCode;
  }
  public String getReasonCode()
  {
    return reasonCode;
  }

  public void setCardId(long cardId)
  {
    this.cardId = cardId;
  }
  public long getCardId()
  {
    return cardId;
  }

  public void setCardGuid(String cardGuid)
  {
    this.cardGuid = cardGuid;
  }
  public String getCardGuid()
  {
    return cardGuid;
  }

  public void setTranDate(Date tranDate)
  {
    this.tranDate = tranDate;
  }
  public Date getTranDate()
  {
    return tranDate;
  }
  public void setTranTs(Timestamp tranTs)
  {
    tranDate = ApiRequest.tsToDate(tranTs);
  }
  public Timestamp getTranTs()
  {
    return ApiRequest.dateToTs(tranDate);
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public List getEntryItems()
  {
    return entryItems;
  }
  public void addEntryItem(ChargebackEntryItem entryItem)
  {
    entryItems.add(entryItem);
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("ChargebackRecord [ ");
    buf.append("req id: " + requestId);
    buf.append(", mem id: " + memberId);
    buf.append(", cb id: " + cbId);
    buf.append(", case id: " + caseId);
    buf.append(", type id: " + cbTypeId);
    buf.append(", amt: " + amount);
    buf.append(", cur code: " + currencyCode);
    buf.append(", proc date: " + procDate);
    buf.append(", reas code: " + reasonCode);
    buf.append(", card id: " + cardId);
    buf.append(", card guid: " + cardGuid);
    buf.append(", tran date: " + tranDate);
    buf.append(", tran id: " + tranId);
    buf.append(", merch acct id: " + merchAcctId);
    for (Iterator i = entryItems.iterator(); i.hasNext();)
    {
      buf.append(", " + i.next());
    }
    if (entryItems.isEmpty()) buf.append(" (no entry items)");
    buf.append(" ]");
    return ""+buf;
  }
}