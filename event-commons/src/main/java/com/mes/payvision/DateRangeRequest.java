package com.mes.payvision;

import java.util.Date;

public abstract class DateRangeRequest extends ApiRequest
{
	public DateRangeRequest() {
	}
	
  public DateRangeRequest(String userName, int memberId, Date startDate,
    Date endDate) throws Exception
  {
    super(userName,memberId);
    this.startDate = startDate;
    this.endDate = endDate;
  }
}