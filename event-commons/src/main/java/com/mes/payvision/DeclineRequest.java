package com.mes.payvision;

import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.payvision.reports.v1_1.axis.declines.ArrayOfDecline;
import com.mes.payvision.reports.v1_1.axis.declines.Decline;
import com.mes.payvision.reports.v1_1.axis.declines.DeclineResult;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesLocator;
import com.mes.payvision.reports.v1_1.axis.declines.DeclinesSoap_BindingStub;

public class DeclineRequest extends DateRangeRequest
{
  static Logger log = Logger.getLogger(DeclineRequest.class);

  private Decline[] declines;

  public DeclineRequest() {	  
  }

  public DeclineRequest(String userName, int memberId, Date startDate, 
    Date endDate) throws Exception
  {
    super(userName,memberId,startDate,endDate);
  }

  public void sendRequest()
  {
    try
    {
      Calendar startCal = Calendar.getInstance();
      startCal.setTime(startDate);

      Calendar endCal = Calendar.getInstance();
      endCal.setTime(endDate);

      DeclinesLocator service = new DeclinesLocator();
      service.setEndpointAddress(getPortName(), hostAddr + MesDefaults.PAYVISION_DECLINES_PATH);
      DeclinesSoap_BindingStub client = (DeclinesSoap_BindingStub)service.getDeclinesSoap();

      DeclineResult result = client.getDeclinedTransactions(
        crMemberId,crMemberGuid,memberId,startCal,endCal);

      DeclineResponse response = 
        new DeclineResponse(this,result.getCode(),result.getMessage());
      
      ArrayOfDecline data = result.getData();
      declines = data.getDecline();

      if (result.getCode() == 0)
      {
        log.debug("Received " + declines.length + " decline records.");
        for (int i = 0; i < declines.length; ++i)
        {
          Decline decl = declines[i];
          DeclineRecord declRec = new DeclineRecord();
          declRec.setRequestId(requestId);
          declRec.setMemberId(memberId);
          declRec.setProcTranId(decl.getProcessedTransactionId());
          declRec.setProcTranGuid(decl.getProcessedTransactionGuid());
          declRec.setTrackMemCode(decl.getTrackingMemberCode());
          declRec.setOrigAmount(decl.getOriginalAmount());
          declRec.setOrigCurrencyCode(decl.getOriginalCurrencyId());
          declRec.setMerchAmount(decl.getMerchantAmount());
          declRec.setMerchCurrencyRate(decl.getMerchantCurrencyRate());
          declRec.setRequestDate(decl.getRequestDate().getTime());
          declRec.setProcessDate(decl.getDate().getTime());
          declRec.setTranType(decl.getTransactionType().getValue());
          declRec.setCardId(decl.getCardId());
          declRec.setCardGuid(decl.getCardGuid());
          declRec.setCardType(decl.getCardTypeId().getValue());
          declRec.setMerchAcctId(decl.getMerchantAccountId());
          declRec.setCountryCode(decl.getCountryId());
          declRec.setBankCode(decl.getBankCode());
          declRec.setBankMsg(decl.getBankMessage());
          response.addRecord(declRec);
        }
      }
      apiResponse = response;
    }
    catch (Exception e)
    {
      apiResponse = new DeclineResponse(this,-1,""+e);
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public String getRequestType()
  {
    return "getDeclines";
  }

  public String getPortName()
  {
    return "DeclinesSoap";
  }

  public Decline[] getDeclines()
  {
    return declines;
  }

  public DeclineResponse getDeclineResponse()
  {
    return (DeclineResponse)getResponse();
  }
}