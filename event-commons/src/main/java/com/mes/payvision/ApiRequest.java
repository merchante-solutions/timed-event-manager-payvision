package com.mes.payvision;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;


public abstract class ApiRequest extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ApiRequest.class);

  protected long requestId;
  protected Date requestDate;
  protected String userName;
  protected String hostAddr;
  protected int crMemberId;
  protected String crMemberGuid;
  protected int memberId;
  protected Date startDate;
  protected Date endDate;
  protected String endpoint;
  
  protected ApiResponse apiResponse;

  public ApiRequest() {  
  }
  
  public ApiRequest(String userName, int memberId) throws Exception
  {
    this.userName = userName;
    this.memberId = memberId;
    setHostAddr(MesDefaults.getString(MesDefaults.PAYVISION_HOST));
    crMemberId = Integer.valueOf(MesDefaults.getString(MesDefaults.PAYVISION_ECOMM_MEMBER_ID));
    crMemberGuid = MesDefaults.getString(MesDefaults.PAYVISION_ECOMM_MEMBER_GUID);
    requestDate = Calendar.getInstance().getTime();
    requestId = getNewRequestId();
  }

  private long getNewRequestId()
  {
    ResultSet rs = null;
    PreparedStatement ps = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " select payvision_api_sequence.nextval from dual ");
      rs = ps.executeQuery();
      rs.next();
      return rs.getLong(1);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Error fetching request id: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public static Date tsToDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }

  public static Timestamp dateToTs(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    
    return requestId;
  }

  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  public Date getRequestDate()
  {
    return requestDate;
  }
  public void setRequestTs(Timestamp requestTs)
  {
    requestDate = tsToDate(requestTs);
  }
  public Timestamp getRequestTs()
  {
    return dateToTs(requestDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCrMemberId(int crMemberId)
  {
    this.crMemberId = crMemberId;
  }
  public int getCrMemberId()
  {
    return crMemberId;
  }

  public void setCrMemberGuid(String crMemberGuid)
  {
    this.crMemberGuid = crMemberGuid;
  }
  public String getCrMemberGuid()
  {
    return crMemberGuid;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = tsToDate(startTs);
  }
  public Timestamp getStartTs()
  {
    return dateToTs(startDate);
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = tsToDate(endTs);
  }
  public Timestamp getEndTs()
  {
    return dateToTs(endDate);
  }

  public ApiResponse getResponse()
  {
    return apiResponse;
  }

  public abstract String getRequestType();

  public abstract String getPortName();
  
  public String getHostAddr() 
  {
	return hostAddr;
  }

  public void setHostAddr(String hostAddr) 
  {
	this.hostAddr = hostAddr;
  }

  public String getEndpoint() 
  {
	return endpoint;
  }

  public void setEndpoint(String endpoint) 
  {
	this.endpoint = endpoint;
  }

  public abstract void sendRequest();

  public String toString()
  {
    return this.getClass().getName() + " [ "
      + "req id: " + requestId
      + ", req date: " + requestDate
      + ", req type: " + getRequestType()
      + ", user: " + userName
      + ", host: " + getHostAddr() 
      + ", endpoint: " + getEndpoint() 
      + ", cr mem id: " + crMemberId
      + ", cr mem guid: " + crMemberGuid
      + ", mem id: " + memberId
      + ", start: " + startDate
      + ", end: " + endDate
      + " ]";
  }
}