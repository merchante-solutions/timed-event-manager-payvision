package com.mes.payvision;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class SettlementDetail
{
  public static final int TT_UNKNOWN    = 0;
  public static final int TT_PAYMENT    = 1;
  public static final int TT_REFUND     = 2;
  public static final int TT_AUTH       = 3;
  public static final int TT_CAPTURE    = 4;
  public static final int TT_VOID       = 5;
  public static final int TT_CHARGEBACK = 6;
  public static final int TT_REF_APPR   = 7;

  public static final String[] tranTypes =
  {
    "Unknown",
    "Payment",
    "Refund",
    "Authorize",
    "Capture",
    "Void",
    "Chargeback",
    "Referral Approval",
  };

  public static final String[] cardTypes =
  {
    "Unknown",
    "Mastercard",
    "Visa",
    "American Express",
    "Diners",
    "JCB",
    "Carte Bleue",
    "Galeria",
    "Delta",
    "Laser",
    "Solo",
    "Switch",
    "Enroute",
  };

  private long loadId;
  private int memberId;
  private long merchAcctId;
  private Date settleDate;
  private String trackMemCode;
  private Date tranDate;
  private BigDecimal tranAmount;
  private int currencyCode;
  private int cardType;
  private int tranType;
  private BigDecimal usdDiscountFee;

  public void setLoadId(long loadId)
  {
    this.loadId = loadId;
  }
  public long getLoadId()
  {
    return loadId;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setSettleDate(Date settleDate)
  {
    this.settleDate = settleDate;
  }
  public Date getSettleDate()
  {
    return settleDate;
  }
  public void setSettleTs(Timestamp settleTs)
  {
    settleDate = ApiRequest.tsToDate(settleTs);
  }
  public Timestamp getSettleTs()
  {
    return ApiRequest.dateToTs(settleDate);
  }

  public void setTrackMemCode(String trackMemCode)
  {
    this.trackMemCode = trackMemCode;
  }
  public String getTrackMemCode()
  {
    return trackMemCode;
  }

  public void setTranDate(Date tranDate)
  {
    this.tranDate = tranDate;
  }
  public Date getTranDate()
  {
    return tranDate;
  }
  public void setTranTs(Timestamp tranTs)
  {
    tranDate = ApiRequest.tsToDate(tranTs);
  }
  public Timestamp getTranTs()
  {
    return ApiRequest.dateToTs(tranDate);
  }

  public void setTranAmount(BigDecimal tranAmount)
  {
    this.tranAmount = tranAmount;
  }
  public BigDecimal getTranAmount()
  {
    return tranAmount;
  }

  public void setCurrencyCode(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }
  public int getCurrencyCode()
  {
    return currencyCode;
  }

  public void setCardType(int cardType)
  {
    this.cardType = cardType;
  }
  public int getCardType()
  {
    return cardType;
  }
  public String getCardTypeStr()
  {
    return getCardTypeStr(cardType);
  }
  public static String getCardTypeStr(int ctIdx)
  {
    return cardTypes[ctIdx > cardTypes.length ? 0 : ctIdx];
  }

  public void setTranType(int tranType)
  {
    this.tranType = tranType;
  }
  public int getTranType()
  {
    return tranType;
  }
  public String getTranTypeStr()
  {
    return getTranTypeStr(tranType);
  }
  public static String getTranTypeStr(int ttIdx)
  {
    return tranTypes[ttIdx >= tranTypes.length ? 0 : ttIdx];
  }

  public void setUsdDiscountFee(BigDecimal usdDiscountFee)
  {
    this.usdDiscountFee = usdDiscountFee;
  }
  public BigDecimal getUsdDiscountFee()
  {
    return usdDiscountFee;
  }

  public String toString()
  {
    return "SettlementDetail [ "
      + "load id: " + loadId
      + ", mem id: " + memberId
      + ", merch acct id: " + merchAcctId
      + ", settle date: " + settleDate
      + ", trk mem code: " + trackMemCode
      + ", tran date: " + tranDate
      + ", tran amt: " + tranAmount
      + ", cur code: " + currencyCode
      + ", card type: " + getCardTypeStr() + " (" + cardType + ")"
      + ", tran type: " + getTranTypeStr() + " (" + tranType + ")"
      + ", usd disc fee: " + usdDiscountFee
      + " ]";
  }
}