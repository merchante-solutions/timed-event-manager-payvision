package com.mes.payvision;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SettlementLoad
{
  private long loadId;
  private String loadFile;
  private Date loadDate;
  private String result;
  private String details;

  private List summaries = new ArrayList();

  public void setLoadId(long loadId)
  {
    this.loadId = loadId;
  }
  public long getLoadId()
  {
    return loadId;
  }

  public void setLoadFile(String loadFile)
  {
    this.loadFile = loadFile;
  }
  public String getLoadFile()
  {
    return loadFile;
  }

  public void setLoadDate(Date loadDate)
  {
    this.loadDate = loadDate;
  }
  public Date getLoadDate()
  {
    return loadDate;
  }
  public void setLoadTs(Timestamp loadTs)
  {
    loadDate = ApiRequest.tsToDate(loadTs);
  }
  public Timestamp getLoadTs()
  {
    return ApiRequest.dateToTs(loadDate);
  }

  public void setResult(String result)
  {
    this.result = result;
  }
  public String getResult()
  {
    return result;
  }

  public void setDetails(String details)
  {
    this.details = details;
  }
  public String getDetails()
  {
    return details;
  }

  public void addSummary(SettlementSummary summary)
  {
    summaries.add(summary);
  }
  public List getSummaries()
  {
    return summaries;
  }

  public String toString()
  {
    return "SettlementLoad [ "
      + "load id: " + loadId
      + ", load file: " + loadFile
      + ", load date: " + loadDate
      + ", result: " + result
      + ", details: " + (details != null ? details : "--")
      + " ]";
  }
}