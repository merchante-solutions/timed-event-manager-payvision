package com.mes.payvision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ReconciliationSummary
{
  private long         merchAcctId;
  private int         currencyCode;
  private int         authCount;
  private BigDecimal  authTotal;
  private int         captureCount;
  private BigDecimal  captureTotal;
  private int         voidCount;
  private BigDecimal  voidTotal;
  private int         paymentCount;
  private BigDecimal  paymentTotal;
  private int         refundCount;
  private BigDecimal  refundTotal;
  private int         referralCount;
  private BigDecimal  referralTotal;
  private int         transferCount;
  private BigDecimal  transferTotal;

  private List details = new ArrayList();

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setCurrencyCode(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }
  public int getCurrencyCode()
  {
    return currencyCode;
  }

  public void setAuthCount(int authCount)
  {
    this.authCount = authCount;
  }
  public int getAuthCount()
  {
    return authCount;
  }

  public void setAuthTotal(BigDecimal authTotal)
  {
    this.authTotal = authTotal;
  }
  public BigDecimal getAuthTotal()
  {
    return authTotal;
  }

  public void setCaptureCount(int captureCount)
  {
    this.captureCount = captureCount;
  }
  public int getCaptureCount()
  {
    return captureCount;
  }

  public void setCaptureTotal(BigDecimal captureTotal)
  {
    this.captureTotal = captureTotal;
  }
  public BigDecimal getCaptureTotal()
  {
    return captureTotal;
  }

  public void setVoidCount(int voidCount)
  {
    this.voidCount = voidCount;
  }
  public int getVoidCount()
  {
    return voidCount;
  }

  public void setVoidTotal(BigDecimal voidTotal)
  {
    this.voidTotal = voidTotal;
  }
  public BigDecimal getVoidTotal()
  {
    return voidTotal;
  }

  public void setPaymentCount(int paymentCount)
  {
    this.paymentCount = paymentCount;
  }
  public int getPaymentCount()
  {
    return paymentCount;
  }

  public void setPaymentTotal(BigDecimal paymentTotal)
  {
    this.paymentTotal = paymentTotal;
  }
  public BigDecimal getPaymentTotal()
  {
    return paymentTotal;
  }

  public void setRefundCount(int refundCount)
  {
    this.refundCount = refundCount;
  }
  public int getRefundCount()
  {
    return refundCount;
  }

  public void setRefundTotal(BigDecimal refundTotal)
  {
    this.refundTotal = refundTotal;
  }
  public BigDecimal getRefundTotal()
  {
    return refundTotal;
  }

  public void setReferralCount(int referralCount)
  {
    this.referralCount = referralCount;
  }
  public int getReferralCount()
  {
    return referralCount;
  }

  public void setReferralTotal(BigDecimal referralTotal)
  {
    this.referralTotal = referralTotal;
  }
  public BigDecimal getReferralTotal()
  {
    return referralTotal;
  }

  public void setTransferCount(int transferCount)
  {
    this.transferCount = transferCount;
  }
  public int getTransferCount()
  {
    return transferCount;
  }

  public void setTransferTotal(BigDecimal transferTotal)
  {
    this.transferTotal = transferTotal;
  }
  public BigDecimal getTransferTotal()
  {
    return transferTotal;
  }

  public List getDetails()
  {
    return details;
  }
  public void addDetail(TransactionDetail detail)
  {
    details.add(detail);
  }

  public String toString()
  {
    return "ReconciliationSummary [ "
      + "merch acct id: " + merchAcctId
      + ", curr code: " + currencyCode
      + ", auth cnt: " + authCount
      + ", auth tot: " + authTotal
      + ", cap cnt: " + captureCount
      + ", cap tot: " + captureTotal
      + ", void cnt: " + voidCount
      + ", void tot: " + voidTotal
      + ", pay cnt: " + paymentCount
      + ", pay tot: " + paymentTotal
      + ", refund cnt: " + refundCount
      + ", refund tot: " + refundTotal
      + ", refer cnt: " + referralCount
      + ", refer tot: " + referralTotal
      + ", xfer cnt: " + transferCount
      + ", xfer tot: " + transferTotal
      + " ]";
  }
}