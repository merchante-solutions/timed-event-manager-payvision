/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesChargebacks.java $

  Description:
    Utility class for getting a database connection from an established
    connection pool.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-08-02 17:09:01 -0700 (Tue, 02 Aug 2011) $
  Version            : $Revision: 19090 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesChargebacks
{
  // Action Sources
  public static final int         AS_NONE                   = 0;  //
  public static final int         AS_TSYS                   = 1;  //
  public static final int         AS_AUTO_LIST              = 2;  //
  public static final int         AS_AUTO_REASON            = 3;  //
  public static final int         AS_WEB                    = 4;  //
  public static final int         AS_AUTO_REASON_BY_NODE    = 5;  //
  public static final int         AS_AUTO_REASON_BY_RULE    = 6;  //
  public static final int         AS_AUTO_PROCESS           = 7;
  public static final int         AS_AUTO_AMEX              = 8;
  public static final int         AS_AUTO_BML               = 9;
  public static final int         AS_AUTO_DEBIT             = 10;
  public static final int         AS_AUTO_DISCOVER          = 11;

  public static class CBAutoAction
  {
    public String     ActionCode    = null;
    public int        ActionSource  = 0;

    public CBAutoAction(int at, String ac)
    {
      ActionCode    = ac;
      ActionSource  = at;
    }
  };

  //based on CHARGEBACK_REASON_DESC
  public static final String      CT_MC                   = "MC";  //
  public static final String      CT_VS                   = "VS";  //
  public static final String      CT_AM                   = "AM";  //
  public static final String      CT_BL                   = "BL";  //
  public static final String      CT_DB                   = "DB";  //
  public static final String      CT_DS                   = "DS";  //
};
