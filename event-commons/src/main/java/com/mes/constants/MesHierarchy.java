 /*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesHierarchy.java $

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/23/03 2:36p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesHierarchy
{
  /*
  ** Hierarchy Types
  */
  public static final int   HT_BANK_PORTFOLIOS      = 1;
  public static final int   HT_SALES_HIERARCHIES    = 2;
  public static final int   HT_MES_SALES            = 3;
  public static final int   HT_DISCOVER_SALES       = 4;
  public static final int   HT_VERISIGN_SALES       = 5;
  public static final int   HT_NSI_SALES            = 6;
  public static final int   HT_BBT_SALES            = 7;
  public static final int   HT_PROSPECT_DB          = 8;
  public static final int   HT_ASSOC_MAPPINGS       = 9;
  public static final int   HT_CERTEGY_MERCHANT     = 10;
  
  /*
  ** Hierarchy Groups
  */
  public static final int   HG_SALES                = 1;
  public static final int   HG_MES                  = 2;
  public static final int   HG_DISCOVER             = 3;
  public static final int   HG_BANKS                = 4;
  public static final int   HG_VERISIGN             = 5;
  public static final int   HG_NSI                  = 6;
  public static final int   HG_BBT                  = 7;
  public static final int   HG_PROSPECT             = 8;
  
  /*
  ** Entity Types
  */
  public static final int   ET_HIERARCHY_ROOT       = 0;
  public static final int   ET_HIERARCHY_NODE       = 1;
  public static final int   ET_SALES_REP            = 2;
  public static final int   ET_MERCHANT             = 3;
  public static final int   ET_ASSOCIATION          = 4;
  public static final int   ET_GROUP                = 5;
  public static final int   ET_AM_GROUP             = 6;
  public static final int   ET_AM_CATEGORY          = 7;
  public static final int   ET_AM_ASSOC             = 8;
  
  /*
  ** Entity Types for bank portfolios
  */
  public static final int   ET_BANK_NODE            = 1;
  public static final int   ET_SUPER_NODE           = 2;
  public static final int   ET_ASSN_NODE            = 4;
  public static final int   ET_GROUP_NODE           = 5;
  
  /*
  ** Root Nodes
  */ 
  public static final long  RN_TSYS                 = 9999999999L;
  public static final long  RN_CERTEGY_CHECK_MERCH  = 1001999999L;
};
  
  
