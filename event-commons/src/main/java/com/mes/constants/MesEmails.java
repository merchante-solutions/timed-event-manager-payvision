/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesEmails.java $

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:49:20 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23998 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesEmails
{
  /*
  ** Email Template Identifiers
  */

  public static final int     ET_TRIDENT_VAR_FORM                   = 1;
  public static final int     ET_TRIDENT_VT_LIMITED                 = 2;
  public static final int     ET_TRIDENT_VAR_FORM_CBT               = 3;
  public static final int     ET_TRIDENT_VAR_FORM_GOOGLE            = 4;

  /*
  ** Email Message Address Sets
  */

  public static final int     MSG_ADDRS_NSI_BATCH_FILE              = 1;
  public static final int     MSG_ADDRS_NSI_BATCH_ERROR             = 2;
  public static final int     MSG_ADDRS_NSI_APP_SUBMITTED           = 3;
  public static final int     MSG_ADDRS_NSI_CREDIT_APPROVE          = 4;
  public static final int     MSG_ADDRS_VS_APP_SUBMITTED            = 5;
  public static final int     MSG_ADDRS_CHANGE_REQUEST              = 6;
  public static final int     MSG_ADDRS_SALES_CONTACT               = 7;
  public static final int     MSG_ADDRS_XML_ERROR                   = 8;
  public static final int     MSG_ADDRS_DEMO_PASSWORD               = 9;
  public static final int     MSG_ADDRS_VS_AUTO_VNUM                = 10;
  public static final int     MSG_ADDRS_VS_VIRTUAL_APP_COMPLETE     = 11;
  public static final int     MSG_ADDRS_TIMED_EVENT_ERROR           = 12;
  public static final int     MSG_ADDRS_TRANSCOM_TRANSMIT_SUCCESS   = 13;
  public static final int     MSG_ADDRS_MERCHANT_NUMBER_POOL        = 14;
  public static final int     MSG_ADDRS_UBOC_CONTACT                = 15;
  public static final int     MSG_ADDRS_UBOC_CONTACT_DEMO           = 16;
  public static final int     MSG_ADDRS_RAP_DOWNLOAD                = 17;
  public static final int     MSG_ADDRS_RAP_UPLOAD                  = 18;
  public static final int     MSG_ADDRS_BBT_ENROLLMENT              = 19;
  public static final int     MSG_ADDRS_CLIENT_USER_EMAIL           = 20;
  public static final int     MSG_ADDRS_BBT_PRE_ENROLLMENT          = 21;
  public static final int     MSG_ADDRS_HIERARCHY_UPDATE            = 22;
  public static final int     MSG_ADDRS_TRANSCOM_TRANSMIT_FAILURE   = 23;
  public static final int     MSG_ADDRS_MAS_TAPE_UPLOAD             = 24;
  public static final int     MSG_ADDRS_BATCH_MAINT                 = 25;
  public static final int     MSG_ADDRS_TIMED_EVENT_MGR             = 26;
  public static final int     MSG_ADDRS_MONTH_END_PROCESS           = 27;
  public static final int     MSG_ADDRS_RETR_REQ_AUTO_NOTIFY        = 28;
  public static final int     MSG_ADDRS_CB_AUTO_NOTIFY              = 29;
  public static final int     MSG_ADDRS_BANKSERV_TRANSMIT           = 30;
  public static final int     MSG_ADDRS_SABRE_NOTIFY                = 31;
  public static final int     MSG_ADDRS_SABRE_FAILURE               = 32;
  public static final int     MSG_ADDRS_OUTGOING_CB_NOTIFY          = 33;
  public static final int     MSG_ADDRS_OUTGOING_CB_FAILURE         = 34;
  public static final int     MSG_ADDRS_SYSTEM_ERROR                = 35;
  public static final int     MSG_ADDRS_CBT_QUARTERLY_DATA          = 36;
  public static final int     MSG_ADDRS_OUTGOING_BM_NOTIFY          = 37;
  public static final int     MSG_ADDRS_OUTGOING_BM_FAILURE         = 38;
  public static final int     MSG_ADDRS_SUPPLY_ORDER_NOTIFY         = 39;
  public static final int     MSG_ADDRS_CBT_SALES_TAX_REPORT        = 40;
  public static final int     MSG_ADDRS_FILETRANSMISSION_MONITOR    = 41;
  public static final int     MSG_ADDRS_ACR_CBT_MGMT                = 42;
  public static final int     MSG_ADDRS_OUTGOING_UATP_NOTIFY        = 43;
  public static final int     MSG_ADDRS_OUTGOING_UATP_FAILURE       = 44;
  public static final int     MSG_ADDRS_MERCH_PROF_FAILURE          = 45;
  public static final int     MSG_ADDRS_RAP_RESPONSE_ADMIN          = 46;
  public static final int     MSG_ADDRS_RAP_RESPONSE_OPERATIONS     = 47;
  public static final int     MSG_ADDRS_AMEX_ESA_SETUP_ERROR        = 48;
  public static final int     MSG_ADDRS_AMEX_ESA_SETUP_STATUS       = 49;
  public static final int     MSG_ADDRS_AUTO_IMAGE_ATTACH           = 50;
  public static final int     MSG_ADDRS_VS_ISO_APP_NOTIFY           = 51;
  public static final int     MSG_ADDRS_DISCOVER_RAP_ERRORS         = 52;
  public static final int     MSG_ADDRS_DISCOVER_RAP_NOTIFY         = 53;
  public static final int     MSG_ADDRS_OUTGOING_AMEX_NOTIFY        = 54;
  public static final int     MSG_ADDRS_OUTGOING_AMEX_FAILURE       = 55;
  public static final int     MSG_ADDRS_VPS_ISO_CREDIT              = 56;
  public static final int     MSG_ADDRS_VPS_ISO_SERVICE_CALLS       = 57;
  public static final int     MSG_ADDRS_B3_TRAILER_NOTIFY           = 58;
  public static final int     MSG_ADDRS_OUTGOING_TRIDENT_NOTIFY     = 59;
  public static final int     MSG_ADDRS_OUTGOING_TRIDENT_FAILURE    = 60;
  public static final int     MSG_ADDRS_OUTGOING_DISCOVER_NOTIFY    = 61;
  public static final int     MSG_ADDRS_OUTGOING_DISCOVER_FAILURE   = 62;
  
  public static final int     MSG_ADDRS_AUTH_DAILY_BILLING_NOTIFY   = 76;
  public static final int     MSG_ADDRS_AUTH_DAILY_BILLING_FAILURE  = 77;
  public static final int     MSG_ADDRS_MONTHLY_EXTRACT_LOAD_NOTIFY = 78;
  public static final int     MSG_ADDRS_MIF_RECONCILE               = 79;
  public static final int     MSG_ADDRS_EXT_D256_DISC_AMEX_NOTIFY   = 80;
  
  public static final int     MSG_ADDRS_RISK_BATCH_HOLD_NOTIFY      = 85;

  public static final int     MSG_ADDRS_TPG_COMMENT                 = 91;
  public static final int     MSG_ADDRS_ARDEF_LOADER                = 92;

  public static final int     MSG_ADDRS_DEBIT_ADJ_NOTIFY            = 94;
  public static final int     MSG_ADDRS_DEBIT_ADJ_FAILURE           = 95;
  public static final int     MSG_ADDRS_PCI_QUIZ_NOTIFY             = 96;

  public static final int     MSG_ADDRS_ACH_EVENT_FAILURE           = 97;

  public static final int     MSG_ADDRS_CARDINAL_NOTIFY             = 100;
  public static final int     MSG_ADDRS_STERLING_ACH_NOTIFY         = 101;

  public static final int     MSG_ADDRS_MATCH_INCOMING              = 102;

  public static final int     MSG_ADDRS_VISA_DOWNGRADE_TOTALS       = 103;

  public static final int     MSG_ADDRS_WPP_CONTACT                 = 104;
  public static final int     MSG_ADDRS_WPP_COMPLETE                = 105;

  public static final int     MSG_ADDRS_PAYVISION_SETTLEMENT        = 106;

  public static final int     MSG_ADDRS_STERLING_QUARTERLY_DATA     = 107;

  public static final int     MSG_ADDRS_BANKSERV_JPMC               = 108;

  public static final int     MSG_ADDRS_TOMDAILEY_NOTIFY            = 109;

  public static final int     MSG_ADDRS_FILEPROC_SUCCESS            = 110;
  public static final int     MSG_ADDRS_FILEPROC_ERROR              = 111;

  public static final int     MSG_ADDRS_TSYS_ACH_SUCCESS            = 112;

  public static final int     MSG_ADDRS_OUTGOING_VISA_NOTIFY        = 113;
  public static final int     MSG_ADDRS_OUTGOING_VISA_FAILURE       = 114;
  public static final int     MSG_ADDRS_ALPINE_ACH                  = 115;
  public static final int     MSG_ADDRS_ACR_MGMT_Q_GREEN            = 116;

  public static final int     MSG_ADDRS_OUTGOING_MC_NOTIFY          = 117;
  public static final int     MSG_ADDRS_OUTGOING_MC_FAILURE         = 118;

  public static final int     MSG_ADDRS_ALPINE_PROF                 = 119;

  public static final int     MSG_ADDRS_MC_IPM_FILE_REJECT          = 120;

  public static final int     MSG_ADDRS_DEBIT_ADJ_ERROR             = 121;

  public static final int     MSG_ADDRS_ACCULYNK_NOTIFY             = 122;
  
  public static final int     MSG_ADDRS_OUTGOING_GRS_NOTIFY         = 125;
  public static final int     MSG_ADDRS_OUTGOING_GRS_FAILURE        = 126;
  
  public static final int     MSG_ADDRS_AMEX_CB_NOTIFY              = 128;
  public static final int     MSG_ADDRS_AMEX_RETR_NOTIFY            = 129;
                              
  public static final int     MSG_ADDRS_RESEARCH_BATCH_HOLD_NOTIFY  = 130;
  public static final int     MSG_ADDRS_SETTLEMENT_NOTIFY           = 131;

  // FIS file notifications (3942)
  public static final int     MSG_ADDRS_OUTGOING_FIS_NOTIFY         = 132;
  public static final int     MSG_ADDRS_OUTGOING_FIS_FAILURE        = 133;
  
  // Wells Fargo auto boarding file notifications
  public static final int     MSG_ADDRS_OUTGOING_WFAB_NOTIFY        = 134;
  public static final int     MSG_ADDRS_OUTGOING_WFAB_FAILURE       = 135;
  
  public static final int     MSG_ADDRS_AMEX_INVALID_PAYMENT        = 136;
  
  public static final int     MSG_ADDRS_DISCOVER_CB_NOTIFY          = 137;
  public static final int     MSG_ADDRS_DISCOVER_RETR_NOTIFY        = 138;
  
  public static final int     MSG_ADDRS_RED_UPLOAD                  = 139;

  public static final int     MSG_ADDRS_FX_IC_NOTICE                = 140;

  public static final int     MSG_ADDRS_CERTIFICATION_NOTIFY        = 141;

  public static final int     MSG_ADDRS_MBS_ERRORS                  = 150;
  
  public static final int     MSG_ADDRS_NEW_ACCOUNTS                = 160;
  
  public static final int     MSG_ADDRS_AMEX_CONVERSIONS            = 162;
  
  public static final int     MSG_ADDRS_NEW_ACCOUNT_LOAD_FAIL       = 163;

  public static final int     MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_NOTIFY  = 164;
  public static final int     MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_FAILURE = 165;
  
  public static final int     MSG_ADDRS_AMEX_FILE_VAL_REPORT_NOTIFY = 166;
  
  public static final int     MSG_ADDRS_NEW_ACCOUNT_NOTIFY          = 167;
  public static final int     MSG_ADDRS_WF_THIN_FILE                = 168;
  
  public static final int     MSG_ADDRS_SVB_ACH_NOTIFY              = 169;
  
  public static final int     MSG_ADDRS_CIELO_BATCH_LOAD_FAILURE    = 170;
  
  public static final int     MSG_ADDRS_CIELO_FUNDING_FILE_NOTIFY   = 171;
  
  // Cielo Electronic Statements
  public static final int     MSG_ADDRS_CIELO_SALES_WITH_FUTURE_INSTALLEMNTS    = 172;
  public static final int     MSG_ADDRS_CIELO_PAYMENTS    = 173;
  
  public static final int     MSG_ADDRS_NS_DAILY_UPLOAD_NOTIFY      = 174;
  public static final int     MSG_ADDRS_NS_MONTHLY_UPLOAD_NOTIFY    = 175;
  
  public static final int     MSG_ADDRS_MONTH_END_VALIDATION_NOTIFY    = 176;
  public static final int     MSG_ADDRS_DAILY_MONTH_END_VALIDATION_NOTIFY    = 177;
  
  //Amex Optblue Response Notifications
  public static final int     MSG_ADDRS_AMEX_SPM_RESP       = 180;
  public static final int     MSG_ADDRS_AMEX_SDP_RISK       = 184;
  public static final int     MSG_ADDRS_PRODUCT_GROUP       = 186;
  public static final int     MSG_ADDRS_AMEX_IMGRJ       	= 190;
  
  //Controlscan Email notification
  public static final int     MSG_ADDRS_CONTROLSCAN_NOTIFY   = 183;
  
  // Visa AMMF Outgoing Notification
  public static final int     MSG_ADDRS_OUTGOING_VS_AMMF    = 182;
  
  public static final int     MSG_ADDRS_INCORRECT_WF_MERCHANTS_TO_MES = 185;
  
  public static final int	  MSG_ADDRS_CONTROLSCAN_ERROR_NOTIFY = 187;
  
  public static final int	  MSG_ADDRS_AUS_RESPONSE_UPDATE = 188;
  
  // Visa Chargeback Notification
  public static final int     MSG_ADDRS_VS_CHGBK_ERROR       = 147;

};
