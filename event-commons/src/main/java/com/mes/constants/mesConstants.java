/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/mesConstants.java $

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-09-14 09:55:55 -0700 (Mon, 14 Sep 2015) $
  Version            : $Revision: 23827 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

import java.util.Locale;

/*
** CLASS mesConstants
*/
public class mesConstants
{
  public static final Locale  Locale_BR                 = new Locale("pt","BR");

  // supported export file formats
  public static final int     FF_CSV                    = 0;
  public static final int     FF_PDF                    = 1;
  public static final int     FF_FIXED                  = 2;
  public static final int     FF_CSV_STATIC             = 3;
  public static final int     FF_TAB                    = 4;
  public static final int     FF_TAB_NOQUOTE            = 5;
  public static final int     FF_JSON                   = 6;

  // supported Bankserv ACH sources
  public static final int     BANKSERV_SOURCE_MANUAL    = 1;
  public static final int     BANKSERV_SOURCE_ACR       = 2;
  public static final int     BANKSERV_SOURCE_SUPPLY    = 3;
  public static final int     BANKSERV_SOURCE_ALLEGIANT = 4;
  public static final int     BANKSERV_SOURCE_UPLOAD    = 5;
  public static final int     BANKSERV_SOURCE_FUNDING   = 6;
  public static final int     BANKSERV_SOURCE_BML       = 7;

  //BLOB extension formats currently supported (as reflected in table DOCUMENT_TYPE)
  //unknown = default = 0
  public static final int     FILETYPE_PDF              = 1;
  public static final int     FILETYPE_DOC              = 2;
  public static final int     FILETYPE_XLS              = 3;
  public static final int     FILETYPE_TXT              = 4;
  public static final int     FILETYPE_CSV              = 5;
  public static final int     FILETYPE_XML              = 6;

  //encryptions
  public static final int     FILETYPE_PGP              = 7;//PGP
  public static final int     FILETYPE_ENC              = 8;//JAVA

  //third-party or specialized files
  public static final int     FILETYPE_ACCULYNK         = 30;
  public static final int     FILETYPE_FISERV           = 31;
  public static final int     FILETYPE_ELAN             = 32;

  //images
  public static final int     FILETYPE_TIF              = 50;
  public static final int     FILETYPE_GIF              = 51;
  public static final int     FILETYPE_JPG              = 52;
  public static final int     FILETYPE_BMP              = 53;
  public static final int     FILETYPE_PNG              = 54;

  //Cascading Style Sheets
  public static final int     FILETYPE_CSS              = 60;

  // processor types (MIF.PROCESSOR_ID)
  public static final int     PROCESSOR_TSYS            = 0;
  public static final int     PROCESSOR_MES             = 1;

  // MBS Batch Types (used by auth link, risk scan, extraction)
  public static final int     MBS_BT_UNKNOWN            =    0;
  public static final int     MBS_BT_VISAK              =    1;
  public static final int     MBS_BT_PG                 =    2;
  public static final int     MBS_BT_VITAL              =    3;
  public static final int     MBS_BT_CIELO              =  101;

  // constants for custom_query_param_types
  public static final int     PT_INVALID                = -1;
  public static final int     PT_HIDDEN                 = 0;
  public static final int     PT_STRING                 = 1;
  public static final int     PT_NUMBER                 = 2;
  public static final int     PT_DATE                   = 3;
  public static final int     PT_CURRENCY               = 4;
  public static final int     PT_HIERARCHY_NODE         = 5;
  public static final int     PT_DROP_DOWN_TABLE        = 6;
  public static final int     PT_SQL_LIST               = 7;
  public static final int     PT_OPTIONAL_STRING        = 8;
  public static final int     PT_READ_ONLY_STRING       = 9;

  // function values
  public static final long    FUNC_NONE                 = 0x0000000000000000L;
  public static final long    FUNC_ALL                  = 0x0FFFFFFFFFFFFFFFL;
  public static final long    FUNC_REPORT_MERCH         = 0x0000000000000001L;
  public static final long    FUNC_REPORT_ADMIN         = 0x0000000000000002L;
  public static final long    FUNC_PROFILE_CHANGE       = 0x0000000000000004L;
  public static final long    FUNC_APPLICATION_PROXY    = 0x0000000000000008L;
  public static final long    FUNC_ACCOUNT_SERVICING    = 0x0000000000000010L;
  public static final long    FUNC_REPORT_BANK          = 0x0000000000000020L;
  public static final long    FUNC_UNUSED10             = 0x0000000000000040L;
  public static final long    FUNC_SALES_ADMINISTRATION = 0x0000000000000080L;
  public static final long    FUNC_REPORT_SALES         = 0x0000000000000100L;
  public static final long    FUNC_REPORT_HR            = 0x0000000000000200L;
  public static final long    FUNC_REPORT_MANAGEMENT    = 0x0000000000000400L;
  public static final long    FUNC_REPORT_MIS           = 0x0000000000000800L;
  public static final long    FUNC_CUSTOMER_SERVICE     = 0x0000000000001000L;
  public static final long    FUNC_SYSTEM_ADMIN         = 0x0000000000002000L;
  public static final long    FUNC_APPLICATION_VIEW     = 0x0000000000004000L;
  public static final long    FUNC_REPORT_ACCOUNTING    = 0x0000000000008000L;
  public static final long    FUNC_APPLICATION_DEMO     = 0x0000000000010000L;

  // report menu types
  public static final int     RMENU_ID_MERCHANT         = 1;
  public static final int     RMENU_ID_SUMMARY          = 2;
  public static final int     RMENU_ID_BANK             = 3;
  public static final int     RMENU_ID_RISK             = 4;
  public static final int     RMENU_ID_CHARGEBACK       = 5;

  // online application types
  public static final int     APP_TYPE_ALL_EQUIP              = -1;   // used to select equipment from equipment_application table
  public static final int     APP_TYPE_UNKNOWN                = -1;
  public static final int     APP_TYPE_MES                    = 0;
  public static final int     APP_TYPE_CBT                    = 1;
  public static final int     APP_TYPE_VERISIGN               = 2;
  public static final int     APP_TYPE_DEMO                   = 3;
  public static final int     APP_TYPE_CEDAR                  = 4;
  public static final int     APP_TYPE_ORANGE                 = 5;
  public static final int     APP_TYPE_SVB                    = 6;
  public static final int     APP_TYPE_NSI                    = 7;
  public static final int     APP_TYPE_TRANSCOM               = 8;
  public static final int     APP_TYPE_DISCOVER               = 9;
  public static final int     APP_TYPE_NBSC                   = 10;
  public static final int     APP_TYPE_BBT                    = 11;
  public static final int     APP_TYPE_DISCOVER_IMS           = 12;
  public static final int     APP_TYPE_VISTA                  = 13;
  public static final int     APP_TYPE_GOLD                   = 14;
  public static final int     APP_TYPE_SUMMIT                 = 15;
  public static final int     APP_TYPE_DISCOVER_BANK_REFERRAL = 16;
  public static final int     APP_TYPE_WEST_AMERICA           = 17;
  public static final int     APP_TYPE_VISA                   = 18;
  public static final int     APP_TYPE_MESA                   = 19;
  public static final int     APP_TYPE_AGENT                  = 20;
  public static final int     APP_TYPE_INTAGIO                = 21;
  public static final int     APP_TYPE_MESB                   = 22;
  public static final int     APP_TYPE_RIVER_CITY             = 23;
  public static final int     APP_TYPE_FOOTHILL               = 24;
  public static final int     APP_TYPE_VERISIGN_V2            = 25;
  public static final int     APP_TYPE_ELM_NON_DEPLOY         = 27;
  public static final int     APP_TYPE_MES_NEW                = 28;
  public static final int     APP_TYPE_MOUNTAIN_WEST          = 29;
  public static final int     APP_TYPE_BANNER                 = 30;
  public static final int     APP_TYPE_CBT_TEST               = 31;
  public static final int     APP_TYPE_CBT_NEW                = 31;
  public static final int     APP_TYPE_MESC                   = 32;
  public static final int     APP_TYPE_NET_SUITE              = 33;
  public static final int     APP_TYPE_SABRE                  = 34;
  public static final int     APP_TYPE_MERCHCOM               = 35; // merchant.com became
  public static final int     APP_TYPE_AUTHNET                = 35; // authorize.net
  public static final int     APP_TYPE_GBB                    = 36;
  public static final int     APP_TYPE_FMST                   = 37;
  public static final int     APP_TYPE_BCB                    = 38;
  public static final int     APP_TYPE_CSB                    = 39;
  public static final int     APP_TYPE_FMBANK                 = 40;
  public static final int     APP_TYPE_FFB                    = 41;
  public static final int     APP_TYPE_STERLING               = 42;
  public static final int     APP_TYPE_NBSC2                  = 43;
  public static final int     APP_TYPE_VPS_ISO                = 44;
  public static final int     APP_TYPE_EXCHANGE               = 45;
  public static final int     APP_TYPE_STOREFRONT             = 46;
  public static final int     APP_TYPE_COUNTRY_CLUB           = 47;
  public static final int     APP_TYPE_FNMS                   = 48;
  public static final int     APP_TYPE_MESR                   = 49;
  public static final int     APP_TYPE_NETSUITE_2             = 50;
  public static final int     APP_TYPE_COMMERCIAL             = 52;
  public static final int     APP_TYPE_SOUTH_VALLEY           = 54;
  public static final int     APP_TYPE_INET                   = 65;
  public static final int     APP_TYPE_STERLING_3942          = 66;
  public static final int     APP_TYPE_CARDINAL               = 68;
  public static final int     APP_TYPE_MESP                   = 73;
  public static final int     APP_TYPE_WEBEX                  = 74;
  public static final int     APP_TYPE_CENTURY                = 75;
  public static final int     APP_TYPE_PAYPAL_REFERRAL        = 76;
  public static final int     APP_TYPE_RIVERVIEW              = 80;
  public static final int     APP_TYPE_TOMD                   = 81;
  public static final int     APP_TYPE_ALPINE                 = 84;
  public static final int     APP_TYPE_NETSUITE_MES           = 85;
  public static final int     APP_TYPE_FIRST_COMMUNITY        = 86;
  public static final int     APP_TYPE_PACWEST                = 87;
  public static final int     APP_TYPE_SIGN_SITE              = 91;
  public static final int     APP_TYPE_CBT_3941               = 92;
  public static final int     APP_TYPE_SKAGIT                 = 93;
  public static final int     APP_TYPE_WELLS_FARGO            = 94;
  public static final int     APP_TYPE_BILLING_TREE           = 95;
  public static final int     APP_TYPE_SVB_WELLS_FARGO        = 96;
  public static final int     APP_TYPE_GOOGLE                 = 97;
  public static final int     APP_TYPE_NORTHWEST_GEORGIA      = 98;
  public static final int     APP_TYPE_GOOGLE_MANUAL          = 99;
  public static final int     APP_TYPE_HERITAGE_BANK          = 100;
  public static final int     APP_TYPE_MERCANTILE_BANK        = 101;
  public static final int     APP_TYPE_LEGENDS_BANK           = 102;
  public static final int     APP_TYPE_CLAYTON_BANK           = 103;
  public static final int     APP_TYPE_TRANSCOM_WF            = 104;
  public static final int     APP_TYPE_SVB_3003               = 105;
  public static final int     APP_TYPE_CLACKAMAS_COUNTY       = 106;
  public static final int     APP_TYPE_CENTRAL_VALLEY_BANK    = 107;
  public static final int     APP_TYPE_ONLINE_ORDERING        = 108;
  public static final int     APP_TYPE_CMDI                   = 109;
  public static final int     APP_TYPE_ADVANCED_PAYMENT_SYSTEM = 110;
  public static final int     APP_TYPE_DEMOSPHERE             = 112;
  public static final int     APP_TYPE_BANK_OF_THE_PRAIRIE    = 113;

  // online application statuses
  public static final int     APP_STATUS_INCOMPLETE       = 1;    // application started but not finished
  public static final int     APP_STATUS_COMPLETE         = 2;    // application has been completed
  public static final int     APP_STATUS_APPROVED         = 3;    // application has been approved
  public static final int     APP_STATUS_DECLINED         = 4;    // application has been declined
  public static final int     APP_STATUS_PENDED           = 5;    // application has been pended
  public static final int     APP_STATUS_CANCELLED        = 6;    // application has been cancelled
  public static final int     APP_STATUS_SETUP_COMPLETE   = 7;    // application has been set up
  public static final int     APP_STATUS_ADDED_CARD       = 8;    // verisign app has had card added
  public static final int     APP_STATUS_REMOVED_CARD     = 9;    // verisign app has had card removed

  public static final int     APP_STATUS_CLIENT_REVIEW    = 100;
  public static final int     APP_STATUS_CLIENT_PEND      = 101;
  public static final int     APP_STATUS_CLIENT_DECLINE   = 102;
  public static final int     APP_STATUS_CLIENT_CANCELED  = 103;

  // bank ids
  public static final int     BANK_ID_CERTEGY           = 1000;
  public static final int     BANK_ID_MES               = 3941;
  public static final int     BANK_ID_CBT               = 3858;
  public static final int     BANK_ID_ONE_HEALTH        = 4655;
  public static final int     BANK_ID_BBT               = 3867;
  public static final int     BANK_ID_UBOC              = 3870;
  public static final int     BANK_ID_NBSC              = 3860;
  public static final int     BANK_ID_STERLING          = 3942;
  public static final int     BANK_ID_MES_WF            = 3943;
  public static final int     BANK_ID_SVB               = 3003;
  public static final int     BANK_ID_CIELO             = 8008;

  // bank bins
  public static final String  BANK_BIN_MES              = "433239";
  public static final String  BANK_BIN_CBT              = "413747";
  public static final String  BANK_BIN_NBSC             = "448203";
  public static final String  BANK_BIN_BBT              = "407314";
  public static final String  BANK_BIN_STERLING         = "442328";
  public static final String  BANK_BIN_MES_WF           = "443654";
  public static final String  BANK_BIN_SVB              = "438762";

  // user types
  public static final int     USER_INVALID              = -1;
  public static final int     USER_NONE                 = 0;
  public static final int     USER_MERCHANT             = 1;
  public static final int     USER_MES                  = 2;
  public static final int     USER_BANK                 = 3;
  public static final int     USER_SALES                = 4;
  public static final int     USER_MERCHANT_ADMIN       = 5;    // corp office (org reports, no bank reports)
  public static final int     USER_VERISIGN_REP         = 6;
  public static final int     USER_VERISIGN_MANAGER     = 7;    //CAN ACCESS ALL VERISIGN APPLICATIONS
  public static final int     USER_DEMO                 = 8;
  public static final int     USER_DEFAULT              = 10;
  public static final int     USER_CBT                  = 11;
  public static final int     USER_CEDAR                = 12;

    // sales rep type ids
  public static final int     SRT_INVALID               = -1;
  public static final int     SRT_AGENT_BANK            = 1;
  public static final int     SRT_DIRECT                = 2;
  public static final int     SRT_AGENT_BANK_OR_DIRECT  = 3;
  public static final int     SRT_ADMINISTRATOR              = 4;
  public static final int     SRT_CLIENT_SALES_REP      = 5;

    // sales region ids
  public static final int     SRGN_INVALID              = -1;
  public static final int     SRGN_WEST                 = 1;
  public static final int     SRGN_EAST                 = 2;
  public static final int     SRGN_NATIONAL             = 3;

    // sales plan ids
  public static final int     SPID_INVALID                        =  -1;
  public static final int     SPID_CUSTOM                         =   0;
  public static final int     SPID_SALARIED_MGR                   =   1;
  public static final int     SPID_DIRECT_SALES_MGR               =   2;
  public static final int     SPID_NOT_USED                       =   3;
  public static final int     SPID_DIRECT_SALES_REP               =   4;
  public static final int     SPID_MARATHON_REFERRAL              =   5;
  public static final int     SPID_MARATHON_REFERRAL_OFF_GRID     =   6;
  public static final int     SPID_DISCOVER_SALES_REP             =   7;

  // application card type values (reference into mes.merchpayoption)
  public static final int     APP_CT_NONE               = 0;
  public static final int     APP_CT_VISA               = 1;
  public static final int     APP_CT_VISA_CASH_ADVANCE  = 2;
  public static final int     APP_CT_VISA_LARGE_TICKET  = 3;
  public static final int     APP_CT_MC                 = 4;
  public static final int     APP_CT_MC_CASH_ADVANCE    = 5;
  public static final int     APP_CT_MC_LARGE_TICKET    = 6;
  public static final int     APP_CT_PRIVATE_LABEL_1    = 7;
  public static final int     APP_CT_PRIVATE_LABEL_2    = 8;
  public static final int     APP_CT_PRIVATE_LABEL_3    = 9;
  public static final int     APP_CT_DINERS_CLUB        = 10;
  public static final int     APP_CT_VISA_BUSINESS_CARD = 11;
  public static final int     APP_CT_VISA_CHECK_CARD    = 12;
  public static final int     APP_CT_MC_BUSINESS_CARD   = 13;
  public static final int     APP_CT_DISCOVER           = 14;
  public static final int     APP_CT_JCB                = 15;
  public static final int     APP_CT_AMEX               = 16;
  public static final int     APP_CT_DEBIT              = 17;
  public static final int     APP_CT_CHECK_AUTH         = 18;
  public static final int     APP_CT_OTHERS             = 19;
  public static final int     APP_CT_INTERNET           = 20;
  public static final int     APP_CT_DIAL_PAY           = 21;
  public static final int     APP_CT_EBT                = 22;
  public static final int     APP_CT_TYME_NETWORK       = 23;
  public static final int     APP_CT_VALUTEC_GIFT_CARD  = 24;
  public static final int     APP_CT_MC_CHECK_CARD      = 25;
  public static final int     APP_CT_SPS                = 26;
  public static final int     APP_CT_PAYPAL             = 27;
  public static final int     APP_CT_BILLMELATER        = 28;
  public static final int     APP_CT_DEBIT_PIN_PASS_THRU     = 29;

  public static final int     APP_CT_COUNT              = 30;

  // bb&t specific check provider values
  public static final String  APP_BBT_CHECK_PROVIDER_GLOBAL = "GLOBAL";
  public static final String  APP_BBT_CHECK_PROVIDER_TTECH  = "TTECH";
  public static final String  APP_BBT_CHECK_PROVIDER_CPS    = "CPS";

  // application equipment sections (see equipment_application table)
  public static final int     APP_ES_NEEDED             = 1;
  public static final int     APP_ES_OWNED              = 2;

  // application pos types (maps to pos_type in pos_category)
  public static final int     POS_DIAL_TERMINAL         = 1;
  public static final int     POS_INTERNET              = 2;
  public static final int     POS_PC                    = 3;
  public static final int     POS_DIAL_AUTH             = 4;
  public static final int     POS_OTHER                 = 5;
  public static final int     POS_STAGE_ONLY            = 6;
  public static final int     POS_GLOBAL_PC             = 7;
  public static final int     POS_GPS                   = 8;
  public static final int     POS_WIRELESS_TERMINAL     = 9;
  public static final int     POS_THIRD_PARTY_SOFTWARE  = 10;
  public static final int     POS_CHARGE_EXPRESS        = 11;
  public static final int     POS_CHARGE_PRO            = 12;
  public static final int     POS_TOUCHTONECAPTURE      = 13;
  public static final int     POS_VIRTUAL_TERMINAL      = 14;
  public static final int     POS_PAYMENT_GATEWAY       = 15;
  //for PCI and POS - differentiation between this and POS_DIAL_TERMINAL above
  public static final int     POS_NETWORK_DIAL_TERMINAL = 16;
  public static final int     POS_VT_LIMITED            = 17;
  public static final int     POS_UNKNOWN               = 99;


  // payment soltions types (see mes.paySolType)
  //legacy stuff... add new pos to list above this..
  public static final int     APP_PAYSOL_DIAL_TERMINAL        = 1;
  public static final int     APP_PAYSOL_INTERNET             = 2;
  public static final int     APP_PAYSOL_PC                   = 3;
  public static final int     APP_PAYSOL_DIAL_PAY             = 4;
  public static final int     APP_PAYSOL_OTHER_PRODUCT        = 5;
  public static final int     APP_PAYSOL_STAGE_ONLY           = 6;
  public static final int     APP_PAYSOL_GLOBAL_PC_WINDOWS    = 7;
  public static final int     APP_PAYSOL_GPS                  = 8;
  public static final int     APP_PAYSOL_WIRELESS_TERMINALS   = 9;
  public static final int     APP_PAYSOL_THIRD_PARTY_SOFTWARE = 10;
  public static final int     APP_PAYSOL_PC_CHARGE_EXPRESS    = 11;
  public static final int     APP_PAYSOL_PC_CHARGE_PRO        = 12;
  public static final int     APP_PAYSOL_TOUCHTONECAPTURE     = 13;
  public static final int     APP_PAYSOL_VIRTUAL_TERMINAL     = 14;

  // application pricing scenarios
  public static final int     APP_PS_INVALID              = -1;
  public static final int     APP_PS_VARIABLE_RATE        = 1;
  public static final int     APP_PS_FIXED_RATE           = 2;
  public static final int     APP_PS_INTERCHANGE          = 3;
  public static final int     APP_PS_FIXED_PLUS_PER_ITEM  = 4;
  public static final int     APP_PS_MTWEST_4090_5090     = 66;
  public static final int     APP_PS_MTWEST_4178_5178     = 67;
  public static final int     APP_PS_MTWEST_4179_5179     = 68;
  public static final int     APP_PS_MTWEST_4180_5180     = 69;

  //application pricing scenarios for cbt
  public static final int     CBT_APP_BUNDLED_RATE_PLAN           = 5;
  public static final int     CBT_APP_UNBUNDLED_RATE_PLAN         = 6;
  public static final int     CBT_APP_INTERCHANGE_PLUS_PLAN       = 7;
  public static final int     CBT_APP_PERITEM_DISCRATE_PLAN       = 8;

  //application pricing scenarios for transcom
  public static final int     TRANS_APP_BUCKET_ONLY_PLAN          = 9;
  public static final int     TRANS_APP_BUCKET_WITH_PERITEM_PLAN  = 10;
  public static final int     TRANS_APP_DETAILED_STATEMENT_PLAN   = 11;
  public static final int     TRANS_APP_INTER_PASSTHRU_PLAN       = 12;

  //application pricing scenarios for bbt
  public static final int     BBT_GENERIC_RATE_PLAN               = 13;

  public static final int     IMS_RETAIL_RESTAURANT_PLAN          = 14;
  public static final int     IMS_MOTO_INTERNET_DIALPAY_PLAN      = 15;

  // application pricing scenarios for elmhurst
  public static final int     ELM_APP_PS_RATE_PER_ITEM            = 16;
  public static final int     ELM_APP_PS_RATE_ONLY                = 17;
  public static final int     ELM_APP_PS_FLAT_RATE                = 18;
  public static final int     ELM_APP_PS_BUY_RATE_PLUS            = 19;
  public static final int     ELM_APP_PS_TIERED                   = 20;

  // application pricing scenarios for BB&T
  public static final int     BBT_STANDARD_PRICING_PLAN           = 21;
  public static final int     BBT_INTERCHANGE_PLUS_PLAN           = 22;
  public static final int     BBT_TIERED_PRICING_PLAN             = 23;

  //cbt interchange fee types
  public static final int     CBT_APP_INTERCHANGE_TYPE_INVALID      = -1;
  public static final int     CBT_APP_INTERCHANGE_TYPE_PASSTHRU     = 1;
  public static final int     CBT_APP_INTERCHANGE_TYPE_PUNITIVE50   = 2;
  public static final int     CBT_APP_INTERCHANGE_TYPE_PUNITIVE75   = 3;
  public static final int     CBT_APP_INTERCHANGE_TYPE_PUNITIVE100  = 4;
  public static final int     CBT_APP_INTERCHANGE_TYPE_RETAIL       = 5;
  public static final int     CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING = 6;

  public static final int     CBT_APP_IC_TYPE_PT_RETAIL     = 1;
  public static final int     CBT_APP_IC_TYPE_PT_MOTO       = 2;
  public static final int     CBT_APP_IC_TYPE_PT_HOTEL      = 3;
  public static final int     CBT_APP_IC_TYPE_RP_55_135     = 4;
  public static final int     CBT_APP_IC_TYPE_RP_80_135     = 5;
  public static final int     CBT_APP_IC_TYPE_RP_100_135    = 6;
  public static final int     CBT_APP_IC_TYPE_RP_125_135    = 7;
  public static final int     CBT_APP_IC_TYPE_RP_125_160    = 8;
  public static final int     CBT_APP_IC_TYPE_MLP_100       = 9;
  public static final int     CBT_APP_IC_TYPE_MLP_125       = 10;
  public static final int     CBT_APP_IC_TYPE_MLP_150       = 11;

  //bet type codes
  public static final int     APP_BET_TYPE_RETAIL               = 1;
  public static final int     APP_BET_TYPE_MOTO                 = 2;


  // application pricing grids
  public static final int     APP_PG_INVALID              = -1;
  public static final int     APP_PG_RETAIL               = 1;
  public static final int     APP_PG_MOTO                 = 2;
  public static final int     APP_PG_HOTEL                = 3;
  public static final int     APP_PG_SUPERMARKET          = 4;
  public static final int     APP_PG_INTERNET             = 5;
  public static final int     APP_PG_EMERGING_MARKET      = 6;
  public static final int     APP_PG_LODGING              = 7;
  public static final int     APP_PG_RETAIL_RESTAURANT    = 8;
  public static final int     APP_PG_MOTO_INTERNET        = 9;
  public static final int     APP_PG_RESTAURANT           = 10;

  // industry types
  public static final int     APP_INDUSTYPE_RETAIL              = 1;
  public static final int     APP_INDUSTYPE_RESTAURANT          = 2;
  public static final int     APP_INDUSTYPE_HOTEL               = 3;
  public static final int     APP_INDUSTYPE_MOTEL               = 4;
  public static final int     APP_INDUSTYPE_SERVICES            = 5;
  public static final int     APP_INDUSTYPE_INTERNET            = 6;
  public static final int     APP_INDUSTYPE_DIRECTMARKET        = 7;
  public static final int     APP_INDUSTYPE_OTHER2              = 8;
  public static final int     APP_INDUSTYPE_CASH_ADVANCE        = 9;
  public static final int     APP_INDUSTYPE_LODGING             = 10;
  public static final int     APP_INDUSTYPE_GOV_PUR_CARD        = 11;
  public static final int     APP_INDUSTYPE_RETAIL_GOV_PUR_CARD = 12;
  public static final int     APP_INDUSTYPE_MOTO_GOV_PUR_CARD   = 13;
  public static final int     APP_INDUSTYPE_MOTO                = 14;
  public static final int     APP_INDUSTYPE_MANUFACTORING       = 15;
  public static final int     APP_INDUSTYPE_WHOLESALE           = 16;
  public static final int     APP_INDUSTYPE_SUPERMARKET         = 17;
  public static final int     APP_INDUSTYPE_OTHER               = 99;

  // equipment information (reference into mes.equipLendType)
  public static final int     APP_EQUIP_IN_STOCK          = 0;
  public static final int     APP_EQUIP_PURCHASE          = 1;
  public static final int     APP_EQUIP_RENT              = 2;
  public static final int     APP_EQUIP_OWNED             = 3;
  public static final int     APP_EQUIP_BUY_REFURBISHED   = 4;
  public static final int     APP_EQUIP_LEASE             = 5;
  public static final int     APP_EQUIP_UNKNOWN           = 6;
  public static final int     APP_EQUIP_LOAN              = 7;
  public static final int     APP_EQUIP_SWAP              = 8;
  public static final int     APP_EQUIP_COUNT             = 9;

  // equipment type codes ( see equiptype )
  public static final int     APP_EQUIP_TYPE_PERIPHERAL         = 0;
  public static final int     APP_EQUIP_TYPE_TERMINAL           = 1;
  public static final int     APP_EQUIP_TYPE_PRINTER            = 2;
  public static final int     APP_EQUIP_TYPE_PINPAD             = 3;
  public static final int     APP_EQUIP_TYPE_IMPRINTER          = 4;
  public static final int     APP_EQUIP_TYPE_TERM_PRINTER       = 5;
  public static final int     APP_EQUIP_TYPE_TERM_PRINT_PINPAD  = 6;
  public static final int     APP_EQUIP_TYPE_OTHERS             = 7;
  public static final int     APP_EQUIP_TYPE_PC_SOFTWARE        = 8;
  public static final int     APP_EQUIP_TYPE_IMPRINTER_PLATES   = 9;
  public static final int     APP_EQUIP_TYPE_ACCESSORIES        = 10;
  public static final int     APP_EQUIP_TYPE_COUNT              = 11;   // must be last

  // equipment status (inventory)
  public static final int     EQUIP_STATUS_RECEIVED_IN_STOCK          = 0;
  public static final int     EQUIP_STATUS_DEPLOYED_PURCHASE          = 1;
  public static final int     EQUIP_STATUS_DEPLOYED_RENT              = 2;
  public static final int     EQUIP_STATUS_ENCRYPTED_IN_STOCK         = 3;
  public static final int     EQUIP_STATUS_OBSOLETE                   = 4;
  public static final int     EQUIP_STATUS_DEPLOYED_LEASE             = 5;
  public static final int     EQUIP_STATUS_DEPLOYED_LOAN              = 7;
  public static final int     EQUIP_STATUS_REPAIR_OTHER               = 8;
  public static final int     EQUIP_STATUS_HELP_DESK                  = 9;
  public static final int     EQUIP_STATUS_TRAINING                   = 10;
  public static final int     EQUIP_STATUS_REPAIR_POS_PORTAL          = 11;
  public static final int     EQUIP_STATUS_DEFECTIVE                  = 12;
  public static final int     EQUIP_STATUS_MISSING                    = 13;
  public static final int     EQUIP_STATUS_LOST                       = 14;
  public static final int     EQUIP_STATUS_NOT_YET_RECOVERED          = 15;
  public static final int     EQUIP_STATUS_REPAIR_VMS                 = 16;
  public static final int     EQUIP_STATUS_DEAD                       = 17;
  public static final int     EQUIP_STATUS_TRANSFERRED                = 18;
  public static final int     EQUIP_STATUS_CHARGED                    = 19;
  public static final int     EQUIP_STATUS_DEPLOYED_PP_EXCHANGE       = 20;
  public static final int     EQUIP_STATUS_REPAIR_HYPERCOM            = 21;
  public static final int     EQUIP_STATUS_REPAIR_GCF                 = 22;
  public static final int     EQUIP_STATUS_REPAIR_ALLIANCE            = 23;
  public static final int     EQUIP_STATUS_ENCRYPTION_VMS             = 24;
  public static final int     EQUIP_STATUS_REPAIR_DISCOVER_ALLIANCE   = 25;
  public static final int     EQUIP_STATUS_DEPLOYED_MERCHANT_OWNED    = 26;
  public static final int     EQUIP_STATUS_DEPLOYED_SOLD_AS_IS        = 27;
  public static final int     EQUIP_STATUS_DEPLOYED_MES_INTERNAL_LOAN = 28;

  //setup kit types (cbt)
  public static final int     CBT_SETUP_KIT_TYPE_EDC            = 1;
  public static final int     CBT_SETUP_KIT_TYPE_DIALPAY        = 2;
  public static final int     CBT_SETUP_KIT_TYPE_ECR            = 3;

  //setup kit sizes (cbt)
  public static final int     CBT_SETUP_KIT_SIZE_FULL_KIT       = 1;
  public static final int     CBT_SETUP_KIT_SIZE_MINI_KIT       = 2;
  public static final int     CBT_SETUP_KIT_SIZE_NO_KIT         = 3;


  // application merchant payment solutions ( see mes.merch_pos )
  public static final int     APP_MPOS_NONE                     = 0;

  public static final int     APP_MPOS_DIAL_TERMINAL            = 101;
  public static final int     APP_MPOS_VERISIGN_PAYFLOW_LINK    = 201;
  public static final int     APP_MPOS_VERISIGN_PAYFLOW_PRO     = 202;

  public static final int     APP_MPOS_ACTIVE_C_STOREFRONT        = 203;
  public static final int     APP_MPOS_ACTIVE_C_ENTERPRISE        = 204;

  public static final int     APP_MPOS_ACTIVE_C_STOREFRONT_BASIC  = 203;
  public static final int     APP_MPOS_ACTIVE_C_STOREFRONT_PLUS   = 204;

  public static final int     APP_MPOS_ACTIVE_C_SOFTWARE        = 205;

  public static final int     APP_MPOS_VIRTUAL_NET              = 206;
  public static final int     APP_MPOS_CYBERSOURCE              = 207;
  public static final int     APP_MPOS_CYBERCASH                = 208;

  public static final int     APP_MPOS_CYBERCASH_IC_VERIFY      = 208;
  public static final int     APP_MPOS_CYBERCASH_CASH_REGISTER  = 209;
  public static final int     APP_MPOS_CYBERCASH_WEB_AUTHORIZE  = 210;


  public static final int     APP_MPOS_ESTOREFRONT_SHOPPING_CART_LINK   = 211;
  public static final int     APP_MPOS_ESTOREFRONT_BUILDER_LINK         = 212;
  public static final int     APP_MPOS_ESTOREFRONT_BUILDER_PRO          = 213;
  public static final int     APP_MPOS_AUTHORIZE_NET                    = 214;

  // USA ePay currently used only by CB&T
  public static final int     APP_MPOS_USA_EPAY                 = 215;


  public static final int     APP_MPOS_OTHER                    = 299;

  public static final int     APP_MPOS_POS_PARTNER              = 301;
  public static final int     APP_MPOS_POS_PARTNER_2000         = 302;
  public static final int     APP_MPOS_DIAL_PAY                 = 401;
  public static final int     APP_MPOS_OTHER_VITAL_PRODUCT      = 501;

  public static final int     APP_MPOS_STAGE_ONLY               = 601;
  public static final int     APP_MPOS_GLOBAL_PC_WINDOWS        = 701;
  public static final int     APP_MPOS_GPS                      = 801;
  public static final int     APP_MPOS_WIRELESS_TERMINAL        = 901;
  public static final int     APP_MPOS_THIRD_PARTY_SOFTWARE     = 1001;
  public static final int     APP_MPOS_PC_CHARGE_EXPRESS        = 1101;
  public static final int     APP_MPOS_PC_CHARGE_PRO            = 1201;
  public static final int     APP_MPOS_TOUCH_TONE_CAPTURE       = 1301;
  public static final int     APP_MPOS_VIRTUAL_TERMINAL         = 1401;
  //corresponds to both pos_category and trident_product_codes
  public static final int     APP_MPOS_PAYMENT_GATEWAY_CNP      = 1501; //PG
  public static final int     APP_MPOS_PAYMENT_GATEWAY_CP       = 1601; //GS
  public static final int     APP_MPOS_VT_LIMITED               = 1701; //PG

  public static final int     APP_MPOS_UNKNOWN                  = 9999;

  // new application one time fee
  public static final int     APP_MISC_CHARGE_NONE                          = 0;
  public static final int     APP_MISC_CHARGE_NEW_ACCOUNT_APP               = 1;
  public static final int     APP_MISC_CHARGE_NEW_ACCOUNT_SETUP             = 2;
  public static final int     APP_MISC_CHARGE_INTERNET_SERVICE_SETUP        = 3;
  public static final int     APP_MISC_CHARGE_INTERNET_SERVICE              = 4;
  public static final int     APP_MISC_CHARGE_CHARGEBACK                    = 5;
  public static final int     APP_MISC_CHARGE_HELP_DESK                     = 6;
  public static final int     APP_MISC_CHARGE_MONTHLY_STATEMENT             = 7;
  public static final int     APP_MISC_CHARGE_MONTHLY_SERVICE_FEE           = 7;
  public static final int     APP_MISC_CHARGE_WEB_REPORTING                 = 8;
  public static final int     APP_MISC_CHARGE_IMPRINTER                     = 9;
  public static final int     APP_MISC_CHARGE_POS                           = 10;
  public static final int     APP_MISC_CHARGE_MEMBERSHIP_FEES               = 11;
  public static final int     APP_MISC_CHARGE_MESSAGE_CHARGES               = 12;
  public static final int     APP_MISC_CHARGE_MONTHLY_GATEWAY               = 13;
  public static final int     APP_MISC_CHARGE_GATEWAY_SETUP                 = 14;
  public static final int     APP_MISC_CHARGE_RUSH_FEE                      = 15;
  public static final int     APP_MISC_CHARGE_UNIQUE_FEE                    = 16;
  public static final int     APP_MISC_CHARGE_RETRIEVAL_FEE                 = 17;
  public static final int     APP_MISC_CHARGE_TERM_REPROG_FEE               = 18;
  public static final int     APP_MISC_CHARGE_PINPAD_SWAP_FEE               = 19;
  public static final int     APP_MISC_CHARGE_ACH_DEPOSIT_FEE               = 20;
  public static final int     APP_MISC_CHARGE_DEBIT_ACCESS_FEE              = 21;
  public static final int     APP_MISC_CHARGE_ANNUAL_FEE                    = 22;
  public static final int     APP_MISC_CHARGE_TRAINING_FEE                  = 23;
  public static final int     APP_MISC_CHARGE_MISC1_FEE                     = 24;
  public static final int     APP_MISC_CHARGE_MISC2_FEE                     = 25;
  public static final int     APP_MISC_CHARGE_VOICE_AUTH_FEE                = 26;
  public static final int     APP_MISC_CHARGE_POS_MONTHLY_SERVICE_FEE       = 27;
  public static final int     APP_MISC_CHARGE_SETUP_KIT_FEE                 = 28;
  public static final int     APP_MISC_CHARGE_PINPAD_ENCRYPTION_FEE         = 29;
  public static final int     APP_MISC_CHARGE_EMS_SERVICE_FEE               = 30;
  public static final int     APP_MISC_CHARGE_PRINTER_REPROG_FEE            = 31;
  public static final int     APP_MISC_CHARGE_WIRELESS_SETUP_FEE            = 32;
  public static final int     APP_MISC_CHARGE_WIRELESS_MONTHLY_FEE          = 33;
  public static final int     APP_MISC_CHARGE_WIRELESS_TRANSACTION_FEE      = 34;
  public static final int     APP_MISC_CHARGE_MONTHLY_MEMBERSHIP_FEE        = 35;
  public static final int     APP_MISC_CHARGE_THIRD_PARTY_SOFTWARE_FEE      = 36;
  public static final int     APP_MISC_CHARGE_REFERRAL_AUTH_FEE             = 37;
  public static final int     APP_MISC_CHARGE_ACCOUNT_SERVICING             = 38;
  public static final int     APP_MISC_CHARGE_ACH_REJECT_FEE                = 39;
  public static final int     APP_MISC_CHARGE_DIALPAY_MONTLY_FEE            = 40;
  public static final int     APP_MISC_CHARGE_WEEKLY_CONFIRMATION           = 41;
  public static final int     APP_MISC_CHARGE_REPROGRAMMING_FEE             = 42;
  public static final int     APP_MISC_CHARGE_MONTHLY_MAINTENANCE_FEE       = 43;
  public static final int     APP_MISC_CHARGE_DOWNLOAD_FEE                  = 44;
  public static final int     APP_MISC_CHARGE_EMERGENCY_REPLACEMENT         = 45;
  public static final int     APP_MISC_CHARGE_SUPPLIES                      = 46;
  public static final int     APP_MISC_CHARGE_DEBIT_SETUP_FEE               = 47;
  public static final int     APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE         = 48;
  public static final int     APP_MISC_CHARGE_TRAINING_FEE_PC               = 49;
  public static final int     APP_MISC_CHARGE_MONTHLY_SERVICE_FEE_PC        = 50;
  public static final int     APP_MISC_CHARGE_MONTHLY_INTERNET_REPORT_FEE   = 51;
  public static final int     APP_MISC_CHARGE_PCCHARGE_MONTHLY_SERVICE_FEE  = 52;
  public static final int     APP_MISC_CHARGE_DIALPAY_SETUP_FEE             = 53;

  public static final int     APP_MISC_CHARGE_MULTI_MERCHANT_FEE            = 65;

  public static final int     APP_MISC_CHARGE_PC_SOFTWARE_FEE               = 66;
  public static final int     APP_MISC_CHARGE_NETWORK_ACCESS_FEE            = 67;
  public static final int     APP_MISC_CHARGE_WIRELESS_AUTH_FEE             = 68;
  public static final int     APP_MISC_CHARGE_WIRELESS_ACTIVATION_FEE       = 69;
  public static final int     APP_MISC_CHARGE_BBT_EMS_FEE                   = 70;
  public static final int     APP_MISC_CHARGE_SLIM_CD_SETUP_FEE             = 71;
  public static final int     APP_MISC_CHARGE_WIRELESS_ADDRESS_MONTHLY_FEE  = 72;
  public static final int     APP_MISC_CHARGE_VT_SETUP_FEE                  = 73;
  public static final int     APP_MISC_CHARGE_VT_MONTHLY_FEE                = 74;
  public static final int     APP_MISC_CHARGE_USAEPAY_SETUP_FEE             = 75;
  public static final int     APP_MISC_CHARGE_USAEPAY_MONTHLY_FEE           = 76;
  public static final int     APP_MISC_CHARGE_MONTHLY_SUPPLIES              = 77;
  public static final int     APP_MISC_CHARGE_PCI_COMPLIANCE                = 78;
  public static final int     APP_MISC_CHARGE_PCI_MONTHLY                   = 79;
  public static final int     APP_MISC_CHARGE_VTL_SETUP                     = 80;
  public static final int     APP_MISC_CHARGE_VTL_MONTHLY                   = 81;

  public static final int     APP_MISC_CHARGE_VISA_APF                      = 82;
  public static final int     APP_MISC_CHARGE_VISA_AUTH_MISUSE              = 83;
  public static final int     APP_MISC_CHARGE_VISA_ZERO_FLOOR_LIMIT         = 84;

  public static final int     APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING   = 86;
  public static final int     APP_MISC_CHARGE_QUARTERLY_PCI_PENALTY_FEE     = 88;
  public static final int     APP_MISC_CHARGE_ANNUAL_COMPLIANCE_DEC         = 89;
  public static final int     APP_MISC_CHARGE_ANNUAL_FEE_JAN                = 90;

  public static final int     APP_MISC_CHARGE_TERMINAL_RENTAl               = 91;
  public static final int     APP_MISC_CHARGE_ANNUAL_ADMINISTRATIVE_FEE     = 92;
  
  //SVB wanted their 'Compliance Fee Reporting' to show up as 'Compliance Fee' instead of 'Annual Compliance Fee'
  // like it does for all other BINS. Hence the need for a separate charge item   
  public static final int     APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING_SVB   = 95;

  public static final int     APP_MISC_CHARGE_COUNT                         = 82;

  // commission type values (see mes.commission_type)
  public static final int     COMM_TYPE_CYBERCASH_COST                      = -13;
  public static final int     COMM_TYPE_CYBERCASH_COST_SETUP                = -12;
  public static final int     COMM_TYPE_VERISIGN_PRO_COST                   = -11;
  public static final int     COMM_TYPE_VERISIGN_PRO_COST_SETUP             = -10;
  public static final int     COMM_TYPE_VERISIGN_LINK_COST                  = -9;
  public static final int     COMM_TYPE_VERISIGN_LINK_COST_SETUP            = -8;
  public static final int     COMM_TYPE_DEBIT_COST                          = -7;
  public static final int     COMM_TYPE_NON_BANK_AUTH_COST                  = -6;
  public static final int     COMM_TYPE_NONE                                = 0;
  public static final int     COMM_TYPE_VMC_DISC_ADVANCED                   = 1;
  public static final int     COMM_TYPE_VMC_DISC_MONTHLY                    = 2;
  public static final int     COMM_TYPE_VMC_DISC_TRUE_UP                    = 3;
  public static final int     COMM_TYPE_EQUIP_RENTAL_ADVANCE                = 4;
  public static final int     COMM_TYPE_STATEMENT_FEES_MONTHLY              = 5;
  public static final int     COMM_TYPE_APP_FEES                            = 6;
  public static final int     COMM_TYPE_SW_FEES                             = 7;
  public static final int     COMM_TYPE_ECOMMERCE_SETUP_FEES                = 8;
  public static final int     COMM_TYPE_DEBIT_FEES                          = 9;
  public static final int     COMM_TYPE_AMEX_ESA_BONUS                      = 10;
  public static final int     COMM_TYPE_DISCOVER_RAP_FEES                   = 11;
  public static final int     COMM_TYPE_NON_BANK_FEES                       = 12;
  public static final int     COMM_TYPE_RETENTION                           = 13;
  public static final int     COMM_TYPE_EQUIP_RENTAL_MONTHLY                = 14;
  public static final int     COMM_TYPE_EQUIP_SALES                         = 15;
  public static final int     COMM_TYPE_ECOMMERCE_MONTHLY_FEES              = 16;
  public static final int     COMM_TYPE_STATEMENT_FEES_ADVANCE              = 17;
  public static final int     COMM_TYPE_STATEMENT_FEES_TRUE_UP              = 18;
  public static final int     COMM_TYPE_EQUIP_SERVICE_FEE_ADVANCE           = 19;
  public static final int     COMM_TYPE_EQUIP_SERVICE_FEE_TRUE_UP           = 20;
  public static final int     COMM_TYPE_VMC_DISC_ADV_MONTHLY                = 21;
  public static final int     COMM_TYPE_EQUIP_RENTAL_TRUE_UP                = 22;
  public static final int     COMM_TYPE_EQUIP_SERVICE_FEE_MONTHLY           = 23;
  public static final int     COMM_TYPE_PERFORMANCE_BONUS                   = 24;
  public static final int     COMM_TYPE_MISC_ADJUSTMENT                     = 25;
  public static final int     COMM_TYPE_THIRD_PARTY_OTHER                   = 26;
  public static final int     COMM_TYPE_COUNT                               = 27;   // must be last

  // commission related constants
  public static final int     COMM_EQUIP_DEPRECIATION_YEARS             = 3;
  public static final int     COMM_EQUIP_RENTAL_PAYABLE_MONTHS          = 12;
  public static final int     COMM_MONTHLY_STATEMENT_PAYABLE_MONTHS     = 12;
  public static final int     COMM_EQUIP_SERVICE_FEE_PAYABLE_MONTHS     = 12;

  public static final int     COMM_MONTH_TO_MONTH_PAYMENT_COUNT         = 13;
  public static final double  COMM_DEBIT_PER_ITEM_BASE_COST             = 0.23;
  public static final double  COMM_DEBIT_PER_ITEM_MIN_FOR_COMP          = 0.24;
  public static final double  COMM_NON_BANK_PER_ITEM_BASE_COST          = 0.065;
  public static final double  COMM_NON_BANK_PER_ITEM_MIN_FOR_COMP       = 0.10;

  // e-commerce base costs
  public static final double  COMM_CYBERCASH_PER_ITEM_BASE_COST                 =  0.10;
  public static final double  COMM_CYBERCASH_SETUP_BASE_COST                    = 85.00;
  public static final double  COMM_VERISIGN_PAYFLOW_LINK_MONTHLY_BASE_COST      = 10.00;
  public static final double  COMM_VERISIGN_PAYFLOW_LINK_SETUP_BASE_COST        = 39.00;
  public static final double  COMM_VERISIGN_PAYFLOW_PRO_MONTHLY_BASE_COST       = 25.00;
  public static final double  COMM_VERISIGN_PAYFLOW_PRO_SETUP_BASE_COST         = 69.00;

  // Visa, MasterCard, Debit interchange categories (see mes.ic_category_desc)
  public static final int     IC_DB_CAT_ALL_OTHER             =  1 ;    //  All-Other
  public static final int     IC_DB_CAT_HONOR                 =  2 ;    //  Honor
  public static final int     IC_DB_CAT_INTERLINK             =  3 ;    //  Interlink
  public static final int     IC_DB_CAT_04                    =  4 ;    //  Category 4
  public static final int     IC_DB_CAT_05                    =  5 ;    //  Category 5
  public static final int     IC_DB_CAT_06                    =  6 ;    //  Category 6
  public static final int     IC_DB_CAT_07                    =  7 ;    //  Category 7
  public static final int     IC_DB_CAT_08                    =  8 ;    //  Category 8
  public static final int     IC_DB_CAT_09                    =  9 ;    //  Category 9
  public static final int     IC_DB_CAT_10                    =  10;    //  Category 10
  public static final int     IC_MC_CAT_DOMESTIC_SALES        =  1 ;    //  Domestic Sales
  public static final int     IC_MC_CAT_DOMESTIC_CASH         =  2 ;    //  Domestic Cash
  public static final int     IC_MC_CAT_KEY_ENTERED           =  3 ;    //  Key-Entered
  public static final int     IC_MC_CAT_FOREIGN_SALES         =  4 ;    //  Foreign Sales
  public static final int     IC_MC_CAT_FOREIGN_CASH          =  5 ;    //  Foreign Cash
  public static final int     IC_MC_CAT_CORPORATE_PROD        =  6 ;    //  Corporate Prod
  public static final int     IC_MC_CAT_FOREIGN_STD           =  7 ;    //  Foreign Std
  public static final int     IC_MC_CAT_SMK2_WAREHOUSE        =  8 ;    //  SMK2/Warehouse
  public static final int     IC_MC_CAT_MERIT_1_PT            =  9 ;    //  Merit 1/PT
  public static final int     IC_MC_CAT_CONV_PREM             =  10;    //  Conv/Prem
  public static final int     IC_MC_CAT_MERIT_3               =  11;    //  Merit 3
  public static final int     IC_MC_CAT_CORPORATE_TE          =  12;    //  Corporate T&E
  public static final int     IC_VS_CAT_DOMESTIC_SALES        =  1 ;    //  Domestic Sales
  public static final int     IC_VS_CAT_DOMESTIC_CASH         =  2 ;    //  Domestic Cash
  public static final int     IC_VS_CAT_DOMESTIC_ATM          =  3 ;    //  Domestic ATM
  public static final int     IC_VS_CAT_FOREIGN_PRE_PS2000    =  4 ;    //  For/Pre-PS2000
  public static final int     IC_VS_CAT_FOREIGN_CASH          =  5 ;    //  Foreign Cash
  public static final int     IC_VS_CAT_SIP_CHECK             =  6 ;    //  SIP Check
  public static final int     IC_VS_CAT_EPS                   =  7 ;    //  EPS
  public static final int     IC_VS_CAT_SIP_CREDIT            =  8 ;    //  SIP Credit
  public static final int     IC_VS_CAT_PSIRF                 =  9 ;    //  PSIRF
  public static final int     IC_VS_CAT_CPS                   =  10;    //  CPS
  public static final int     IC_VS_CAT_CPS_PASS_TP1          =  11;    //  CPS Pass TP1
  public static final int     IC_VS_CAT_CPS_RETAIL_2          =  12;    //  CPS Retail 2
  public static final int     IC_VS_CAT_CPS_NP_COM_KEY        =  13;    //  CPS NP/COM Key
  public static final int     IC_VS_CAT_EIRF                  =  14;    //  EIRF
  public static final int     IC_VS_CAT_CPS_CK_OTHER          =  15;    //  CPS CK-Other
  public static final int     IC_VS_CAT_CPS_CK_PS_TP          =  16;    //  CPS CK-PS TP
  public static final int     IC_VS_CAT_CPS_CK_TRAVEL         =  17;    //  CPS CK-Travel
  public static final int     IC_VS_CAT_BUS_STD_OTHER         =  18;    //  Bus Std-Other
  public static final int     IC_VS_CAT_BUS_STD_TRAVEL        =  19;    //  Bus Std-Travel
  public static final int     IC_VS_CAT_BUS_ELEC_OTHER        =  20;    //  Bus Elec-Other
  public static final int     IC_VS_CAT_BUS_ELEC_TRAVEL       =  21;    //  Bus Elec-Travel
  public static final int     IC_VS_CAT_INTL_AIR_LOCAL        =  22;    //  Intl Air Local
  public static final int     IC_VS_CAT_INTL_RED_LOCAL        =  23;    //  Intl Red Local
  public static final int     IC_VS_CAT_INTL_INTR_REG         =  24;    //  Intl Intr Reg
  public static final int     IC_VS_CAT_INTL_S_AIR_FEE        =  25;    //  Intl S Air Fee

  // Vital Monthly Extract (ME) Plan Card Types - monthly_extract_pl records
  public static final String  VITAL_ME_PL_VISA                = "VISA";
  public static final String  VITAL_ME_PL_VSBS                = "VSBS";
  public static final String  VITAL_ME_PL_VSDB                = "VSDB";
  public static final String  VITAL_ME_PL_VS$                 = "VS$" ;
  public static final String  VITAL_ME_PL_VLGT                = "VLGT";
  public static final String  VITAL_ME_PL_MC                  = "MC"  ;
  public static final String  VITAL_ME_PL_MCBS                = "MCBS";
  public static final String  VITAL_ME_PL_MCDB                = "MCDB";
  public static final String  VITAL_ME_PL_MC$                 = "MC$" ;
  public static final String  VITAL_ME_PL_MLGT                = "MLGT";
  public static final String  VITAL_ME_PL_DINERS              = "DC"  ;
  public static final String  VITAL_ME_PL_DISC                = "DS"  ;
  public static final String  VITAL_ME_PL_AMEX                = "AMEX";
  public static final String  VITAL_ME_PL_JCB                 = "JCB" ;
  public static final String  VITAL_ME_PL_DEBIT               = "DEBC";
  public static final String  VITAL_ME_PL_EBT                 = "EBT" ;
  public static final String  VITAL_ME_PL_PL                  = "PL"  ;
  public static final String  VITAL_ME_PL_PL1                 = "PL1" ;
  public static final String  VITAL_ME_PL_PL2                 = "PL2" ;
  public static final String  VITAL_ME_PL_PL3                 = "PL3" ;
  public static final String  VITAL_ME_PL_BML                 = "BML" ;
  public static final String  VITAL_ME_ACH                    = "AC"  ;


  // MES added strings, used by report/BankContractBean.class
  public static final String  VITAL_ME_PL_VMC                 = "VMC" ;

  // Vital Monthly Extract (ME) Auth Card Types - monthly_extract_ap records
  public static final String  VITAL_ME_AP_OVERALL             = "OV";
  public static final String  VITAL_ME_AP_VISA                = "VS";
  public static final String  VITAL_ME_AP_VB                  = "VB";
  public static final String  VITAL_ME_AP_VD                  = "VD";
  public static final String  VITAL_ME_AP_MC                  = "MC";
  public static final String  VITAL_ME_AP_MB                  = "MB";
  public static final String  VITAL_ME_AP_MD                  = "MD";
  public static final String  VITAL_ME_AP_PL                  = "PL";
  public static final String  VITAL_ME_AP_PLAN_1              = "P1";
  public static final String  VITAL_ME_AP_PLAN_2              = "P2";
  public static final String  VITAL_ME_AP_PLAN_3              = "P3";
  public static final String  VITAL_ME_AP_DINERS              = "DC";
  public static final String  VITAL_ME_AP_DISC                = "DS";
  public static final String  VITAL_ME_AP_AMEX                = "AM";
  public static final String  VITAL_ME_AP_CARTE_BLANCHE       = "CB";
  public static final String  VITAL_ME_AP_TELECREDIT          = "TC";
  public static final String  VITAL_ME_AP_TELECHECK           = "TK";
  public static final String  VITAL_ME_AP_WELCOME_CHECK       = "WE";
  public static final String  VITAL_ME_AP_SCAN                = "SC";
  public static final String  VITAL_ME_AP_JCB                 = "JC";
  public static final String  VITAL_ME_AP_DEBIT               = "DB";
  public static final String  VITAL_ME_AP_EBT                 = "EB";
  public static final String  VITAL_ME_AP_ERROR               = "ER";
  public static final String  VITAL_ME_AP_PRIVATE_LABEL       = "PL";
  public static final String  VITAL_ME_AP_BML                 = "BL";

  // user data 3 in the mes.monthly_extract_gn table is encoded
  // with the following strings in position 3-4
  public static final String  VITAL_ME_ACTIVE_C_MANAGED       = "AM";
  public static final String  VITAL_ME_ACTIVE_C_V28_SOFTWARE  = "A2";
  public static final String  VITAL_ME_CYBERCASH              = "CY";
  public static final String  VITAL_ME_CYBERSOURCE            = "CS";
  public static final String  VITAL_ME_DIAL_PAY_AUTH_CAPTURE  = "DP";
  public static final String  VITAL_ME_DIAL_TERMINAL          = "DT";
  public static final String  VITAL_ME_POS_PARTNER            = "PO";
  public static final String  VITAL_ME_POS_PARTNER_2000       = "P2";
  public static final String  VITAL_ME_VERISIGN_PAYFLOW_LINK  = "VL";
  public static final String  VITAL_ME_VERISIGN_PAYFLOW_PRO   = "VF";
  public static final String  VITAL_ME_VIRTUAL_NET            = "VN";
  public static final String  VITAL_ME_VIRTUAL_TERMINAL       = "VT";
  // new product codes for payflow pro and link as of 11/01/2009 (PRF-1500)
  public static final String  VITAL_ME_PAYPAL_PAYFLOW_LINK    = "PL";
  public static final String  VITAL_ME_PAYPAL_PAYFLOW_PRO     = "PF";

  /*
  ** Address types (address.addresstype_code)
  */
  public static final int     ADDR_TYPE_NONE                  = 0;
  public static final int     ADDR_TYPE_BUSINESS              = 1;
  public static final int     ADDR_TYPE_MAILING               = 2;
  public static final int     ADDR_TYPE_SHIPPING              = 3;
  public static final int     ADDR_TYPE_OWNER1                = 4;
  public static final int     ADDR_TYPE_OWNER2                = 5;
  public static final int     ADDR_TYPE_OWNER3                = 6;
  public static final int     ADDR_TYPE_OWNER4                = 7;
  public static final int     ADDR_TYPE_CHK_ACCT_BANK         = 9;
  public static final int     ADDR_TYPE_MSR                   = 10;  // Merchant Sales Rep

  /*
  ** Business types (businessowner.busowner_num)
  */
  public static final int     BUS_OWNER_PRIMARY               = 1;
  public static final int     BUS_OWNER_SECONDARY             = 2;
  public static final int     BUS_OWNER_TERTIARY              = 3;
  public static final int     BUS_OWNER_QUATERNARY            = 4;

  /*
  ** Online App constants
  */
  public static final int     PRICING_GRID_INVALID          = 0;
  public static final int     PRICING_GRID_RETAIL           = 1;
  public static final int     PRICING_GRID_MOTO             = 2;
//  public static final int     PRICING_GRID_HOTEL            = 3;

  public static final int     NUM_PRICING_GRIDS             = 3;

  /*
  ** Transcom ebt options
  */
  public static final int     EBT_OPTION_INVALID            = 0;
  public static final int     EBT_OPTION_CASH_BENEFITS      = 1;
  public static final int     EBT_OPTION_FOOD_STAMPS        = 2;
  public static final int     EBT_OPTION_BOTH               = 3;


  /*
  ** Screen Sequence constants
  */
  public static final long    SEQUENCE_DATA_EXPAND          = 1;
  public static final long    SEQUENCE_APPLICATION          = 2;   //4 page app
  public static final long    SEQUENCE_CREDIT_EVAL          = 3;
  public static final long    SEQUENCE_ID_APPLICATION       = 4;  //2 page app
  public static final long    SEQUENCE_VERISIGN_APPLICATION = 5;
  public static final long    SEQUENCE_EQUIPMENT_DEPLOYMENT = 7;
  public static final long    SEQUENCE_APPLICATION_TRANSCOM = 8;   //transcom app
  public static final long    SEQUENCE_APPLICATION_BBT      = 9;   //bbt app
  public static final long    SEQUENCE_APPLICATION_VISA     = 10;   //visa app

  /*
  ** Screen IDs
  */
  // online app 4 page
  public final static int     SCREEN_ID_MERCH_INFO          = 1;
  public final static int     SCREEN_ID_EQUIPMENT           = 2;
  public final static int     SCREEN_ID_PRICING             = 4;
  public final static int     SCREEN_ID_THANK_YOU           = 8;

  // online app 2 page
  public final static int     SCREEN_MERCH_INFO             = 1;
  public final static int     SCREEN_AGREEMENT              = 2;

  // VERISIGN INTEGRATED ONLINE APP
  public final static int     SCREEN_ID_INSTRUCTIONS        = 1;
  public final static int     SCREEN_ID_BUSINESS            = 2;
  public final static int     SCREEN_ID_OWNERSHIP           = 4;
  public final static int     SCREEN_ID_TRANSACTIONS        = 8;
  public final static int     SCREEN_ID_AGREEMENT           = 16;
  public final static int     SCREEN_ID_THANKYOU            = 32;


 //******************************************************
  // to be removed..left for old stuff
  /*
  ** Screen Sequence constants
  */
  public static final long    SEQUENCE_VERISIGN_APP         = 4;
  public static final long    SEQUENCE_DEMO_APP             = 5;
  public static final long    SEQUENCE_CBT_APP              = 6;
  public static final long    SEQUENCE_CEDAR_APP            = 7;


  // verisign online app
  public final static int     SCREEN_VS_MERCH_INFO          = 1;
  public final static int     SCREEN_VS_AGREEMENT           = 2;

  // demo online app
  public final static int     SCREEN_DEMO_MERCH_INFO        = 1;
  public final static int     SCREEN_DEMO_AGREEMENT         = 2;
//********************************************************

  // data expansion
  public final static long    SCREEN_GENERAL_INFO           = 0x0001L;
  public final static long    SCREEN_INDIVIDUAL_MERCHANT    = 0x0002L;
  public final static long    SCREEN_MERCHANT_DISCOUNT      = 0x0004L;
  public final static long    SCREEN_BET_TABLE              = 0x0008L;
  public final static long    SCREEN_DDA_CHARGE             = 0x0010L;
  public final static long    SCREEN_DESTINATION_FLAGS      = 0x0020L;
  public final static long    SCREEN_ADDRESS_USAGE          = 0x0040L;
  public final static long    SCREEN_OTHER_CARD             = 0x0080L;

  // credit evaluation
  public final static long    SCREEN_CREDIT_REVIEW          = 0x0001L;
  public final static long    SCREEN_CREDIT_ASSIGN          = 0x0002L;
  public final static long    SCREEN_CREDIT_STATUS          = 0x0004L;
  public final static long    SCREEN_CREDIT_LETTER          = 0x0008L;

  // EQUIPMENT DEPLOYMENT
  public final static long    SCREEN_EQUIPMENT_DEPLOYED     = 0x0001L;

  // Rush types
  public final static long    RUSH_ACCOUNT_CHANGE           = 1;
  public final static long    RUSH_ACCOUNT_SETUP            = 2;

  // account lookup action types
  public final static int     LU_ACTION_INVALID             = -1;
  public final static int     LU_ACTION_CALL_TRACKING       = 1;
  public final static int     LU_ACTION_CHANGE_REQUEST      = 2;
  public final static int     LU_ACTION_ACCOUNT_LOOKUP      = 3;
  public final static int     LU_ACTION_CALLTAG             = 4;

  // service call types
  public final static int     CALL_TYPE_INVALID             = 0;
  public final static int     CALL_TYPE_TROUBLESHOOTING     = 1;
  public final static int     CALL_TYPE_TRAINING            = 2;
  public final static int     CALL_TYPE_STATEMENT           = 3;
  public final static int     CALL_TYPE_RECONCILE           = 4;
  public final static int     CALL_TYPE_MAINTENANCE         = 5;
  public final static int     CALL_TYPE_TRANSFERRED         = 6;
  public final static int     CALL_TYPE_SALES               = 7;
  public final static int     CALL_TYPE_SUPPLIES            = 8;
  public final static int     CALL_TYPE_APP_STATUS          = 9;
  public final static int     CALL_TYPE_CHARGEBACK          = 10;
  public final static int     CALL_TYPE_RETRIEVAL           = 11;
  public final static int     CALL_TYPE_PRICING             = 12;
  public final static int     CALL_TYPE_RISK_CREDIT         = 13;
  public final static int     CALL_TYPE_INTERNAL            = 14;
  public final static int     CALL_TYPE_ACTIVATION          = 15;
  public final static int     CALL_TYPE_CRMS                = 16;
  public final static int     CALL_TYPE_SALES_REP_CALL      = 17;
  public final static int     CALL_TYPE_DOWNLINE_LOAD       = 18;
  public final static int     CALL_TYPE_VITAL_PROBLEM       = 19;
  public final static int     CALL_TYPE_REPRICING_LETTER    = 20;
  public final static int     CALL_TYPE_DISCOVER_MIN_DISC   = 21;
  public final static int     CALL_TYPE_HELP_EMAIL          = 22;
  public final static int     CALL_TYPE_VMC_SETTLEMENT      = 23;
  public final static int     CALL_TYPE_QD                  = 24;
  public final static int     CALL_TYPE_SHOPPING_CART       = 25;
  public final static int     CALL_TYPE_DEPLOYMENT          = 26;
  public final static int     CALL_TYPE_WELCOME_CALL        = 27;
  public final static int     CALL_TYPE_COMPLAINT_FEEDBACK  = 28;
  public final static int     CALL_TYPE_CONVERSION          = 83;
  public final static int     CALL_TYPE_OTHER               = 99;

  // service call statuses
  public final static int     CALL_STATUS_OPEN              = 1;
  public final static int     CALL_STATUS_CLOSED            = 2;

  //transcom shipping methods
  public final static int     SHIPPING_METHOD_2DAY          = 1;
  public final static int     SHIPPING_METHOD_OVERNIGHT_AM  = 2;
  public final static int     SHIPPING_METHOD_OVERNIGHT_SAT = 3;
  public final static int     SHIPPING_METHOD_STANDARD      = 4;
  public final static int     SHIPPING_METHOD_1TO3DAY       = 5;
  public final static int     SHIPPING_METHOD_NEXTDAYAM     = 6;
  public final static int     SHIPPING_METHOD_NEXTDAYPM     = 7;
  public final static int     SHIPPING_METHOD_SATURDAY      = 8;
  public final static int     SHIPPING_METHOD_GROUND        = 9;
  public final static int     SHIPPING_METHOD_COD           = 10;
  public final static int     SHIPPING_METHOD_USPS          = 11;

  public final static int     SHIPPING_METHOD_FEDEX_2DAY      = 20;
  public final static int     SHIPPING_METHOD_FEDEX_OVERNIGHT = 21;
  public final static int     SHIPPING_METHOD_FEDEX_SATURDAY  = 22;
  public final static int     SHIPPING_METHOD_CLIENT_PICKUP   = 23;


  //bank account types
  public final static int      BANK_ACCT_TYPE_SAVING_ACCOUNT      = 1;
  public final static int      BANK_ACCT_TYPE_CHECKING_ACCOUNT    = 2;
  public final static int      BANK_ACCT_TYPE_LINE_OF_CREDIT      = 3;
  public final static int      BANK_ACCT_TYPE_COMMON_ACCOUNT      = 4;
  public final static int      BANK_ACCT_TYPE_COMMERCIAL_LOAN     = 5;
  public final static int      BANK_ACCT_TYPE_INVESTMENT_ACCOUNT  = 6;
  public final static int      BANK_ACCT_TYPE_CREDIT_UNION        = 7;
  public final static int      BANK_ACCT_TYPE_MORTGAGE            = 8;
  public final static int      BANK_ACCT_TYPE_OTHER               = 9;

  //bbt business natures
  public static final int BUSINESS_NATURE_UNDEFINED       = 0;
  public static final int BUSINESS_NATURE_RETAIL          = 1;
  public static final int BUSINESS_NATURE_RESTAURANT      = 2;
  public static final int BUSINESS_NATURE_LODGING         = 3;
  public static final int BUSINESS_NATURE_GOV_PURCHCARD   = 4;
  public static final int BUSINESS_NATURE_KEYTAIL         = 5;
  public static final int BUSINESS_NATURE_MOTO            = 6;
  public static final int BUSINESS_NATURE_INTERNET        = 7;


  //bb&t processor options
  public final static int     APP_PROCESSOR_TYPE_VITAL                = 1;
  public final static int     APP_PROCESSOR_TYPE_GLOBAL               = 2;
  public final static int     APP_PROCESSOR_TYPE_GLOBAL_CENTRAL       = APP_PROCESSOR_TYPE_GLOBAL;
  public final static int     APP_PROCESSOR_TYPE_PAYMENTECH           = 3;
  public final static int     APP_PROCESSOR_TYPE_GLOBAL_EAST          = 4;


  //bbt equipment payment methods
  public final static String     PAY_METH_PURCHASE                    = "0";
  public final static String     PAY_METH_INVOICE                     = "1";
  public final static String     PAY_METH_ACH                         = "2";
  public final static String     PAY_METH_STATEMENT                   = "C";
  public final static String     PAY_METH_RENTAL                      = "I";
  public final static String     PAY_METH_LEASE_48MONTHS              = "J";
  public final static String     PAY_METH_LEASE_24MONTHS              = "R";
  public final static String     PAY_METH_LEASE_36MONTHS              = "T";


  //action codes for equip_merchant_tracking (mesdb.EQUIP_ACTION_TYPE)
  public final static int     ACTION_CODE_UNDEFINED                   = 0;
  public final static int     ACTION_CODE_DEPLOYMENT_NEW_SETUP        = 1;
  public final static int     ACTION_CODE_SWAP                        = 2;
  public final static int     ACTION_CODE_REPAIR                      = 3;
  public final static int     ACTION_CODE_DEPLOYMENT_ADDITIONAL       = 4;
  public final static int     ACTION_CODE_DEPLOYMENT_SWAP             = 5;
  public final static int     ACTION_CODE_PINPAD_EXCHANGE             = 6;
  public final static int     ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE  = 7;
  public final static int     ACTION_CODE_CALLTAG                     = 8;
  public final static int     ACTION_CODE_ACR                         = 9;

  public static final int     CALLTAG_INCOMPLETE                = 0;
  public static final int     CALLTAG_COMPLETE                  = 1;
  public static final int     CALLTAG_CANCEL                    = 10;

  public static final int     RAP_UPLOAD_TYPE_ERROR             = 1;
  public static final int     RAP_UPLOAD_TYPE_RESPONSE          = 2;

  //activation service call statuses
  public static final String TRAINING_COMPLETE            = "Training Complete";
  public static final String REFER_TO_SALES_REP           = "Refer To Sales Rep";
  public static final String CUSTOMER_CANCELLED_ACCOUNT   = "Customer Cancelled Account";
  public static final String FOLLOW_UP_REQUIRED           = "Follow Up Required";
  public static final String RESCHEDULING_REQUESTED       = "Rescheduling Requested";
  public static final String NO_CONTACT_AT_SCHEDULED_TIME = "No Contact At Scheduled Time";
  public static final String INITIAL_SCHEDULING           = "Initial Scheduling";

  //new activation service call statuses
  //Scheduling Call
  public static final String SCHEDULING_CALL_SCHEDULED              = "Scheduled";
  public static final String SCHEDULING_CALL_LEFT_MESSAGE           = "Left Message";
  public static final String SCHEDULING_CALL_TOLD_TO_CALL_BACK      = "Told to Call Back";
  public static final String SCHEDULING_CALL_ALREADY_DEPOSITING     = "Already Depositing";
  public static final String SCHEDULING_CALL_REFUSED_ACCOUNT        = "Refused Account";
  public static final String SCHEDULING_CALL_REFER_TO_REP           = "Refer to Rep";

  //Training Call
  public static final String TRAINING_CALL_COMPLETED                = "Completed";
  public static final String TRAINING_CALL_ALREADY_DEPOSITING       = "Already Depositing";
  public static final String TRAINING_CALL_RESCHEDULE               = "Reschedule";
  public static final String TRAINING_CALL_NOT_AVAILABLE_RESCHEDULE = "Not Available/Reschedule";
  public static final String TRAINING_CALL_MAINTENANCE_REQUIRED     = "Maintenance Required";

  // header name containing client IP address in HttpServletRequest object
  public static final String  REQUEST_HEADER_CLIENT_IP              = "x-forwarded-for";
  //ACH Fee Description
  public static final String ACH_TRANSACTION_FEE          = "ACH Transaction Fee";
  public static final String ACH_RETURN_REP_FEE           = "ACH Return/Re-presentment Fee";
  public static final String ACH_UNAUTH_RETURN_REP_FEE    = "ACH Unauth Rtn/Re-presentment State Fee";
  //ACH Charge Description
  public static final String ACH_MONTHLY_SERVICE_FEE      = "ACH Monthly Service Fee";
  public static final String ACH_SMARTPAY_EXP_MONTHLY_FEE = "ACH SmartPay Express Monthly Fee";
  public static final String ACH_HOST_PAY_MONTHLY_FEE     = "ACH Hosted Pay Monthly Fee";
  public static final String ACH_SMARTPAY_EXP_SETUP_FEE   = "ACH SmartPay Express Setup Fee";

  // TSYS Cobol encoding of signed numbers
  public final static char[][] CobolPosValueEncoding =
  {
    { '0','{' },
    { '1','A' },
    { '2','B' },
    { '3','C' },
    { '4','D' },
    { '5','E' },
    { '6','F' },
    { '7','G' },
    { '8','H' },
    { '9','I' },
  };

  public final static char[][] CobolNegValueEncoding =
  {
    { '0','}' },
    { '1','J' },
    { '2','K' },
    { '3','L' },
    { '4','M' },
    { '5','N' },
    { '6','O' },
    { '7','P' },
    { '8','Q' },
    { '9','R' },
  };

  public static String getVitalExtractCardTypeDesc( String vitalExtractCardType )
  {
    String            retVal = "Unknown";
    try
    {
      if ( vitalExtractCardType.equals( VITAL_ME_PL_VISA ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_VISA ) )
      {
        retVal = "Visa";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_VSBS   ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_VB     ) )

      {
        retVal = "Visa Business";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_VSDB   ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_VD     ) )
      {
        retVal = "Visa Debit";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_VS$    ) )
      {
        retVal = "Visa Cash Advance";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_VLGT   ) )
      {
        retVal = "Visa Large Ticket";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_MC     ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_MC ) )
      {
        retVal = "MasterCard";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_MCBS   ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_MB     ) )
      {
        retVal = "MasterCard Business";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_MCDB   ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_MD     ) )
      {
        retVal = "MasterCard Debit";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_MC$    ) )
      {
        retVal = "MasterCard Cash Advance";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_MLGT   ) )
      {
        retVal = "MasterCard Large Ticket";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_DINERS ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_DINERS ) )
      {
        retVal = "Diners";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_AP_CARTE_BLANCHE ) )
      {
        retVal = "Carte Blanche";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_DISC   ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_DISC ) )
      {
        retVal = "Discover";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_AMEX ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_AMEX ) )
      {
        retVal = "American Express";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_JCB ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_JCB ) )
      {
        retVal = "JCB";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_DEBIT ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_DEBIT ) )
      {
        retVal = "Debit";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_EBT ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_EBT ) )
      {
        retVal = "EBT";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_BML ) ||
                vitalExtractCardType.equals( VITAL_ME_AP_BML ) )
      {
        retVal = "Bill Me Later";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_PL ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_PL ) )
      {
        retVal = "Private Label";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_PL1 ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_PLAN_1 ) )
      {
        retVal = "Custom Plan 1";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_PL2 ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_PLAN_2 ) )
      {
        retVal = "Custom Plan 2";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_PL_PL3 ) ||
           vitalExtractCardType.equals( VITAL_ME_AP_PLAN_3 ) )
      {
        retVal = "Custom Plan 3";
      }
      else if ( vitalExtractCardType.equals( VITAL_ME_ACH) )
      {
        retVal = "ACH";
      }
    }
    catch( NullPointerException e )
    {
    }
    return( retVal );
  }

  public static String getVitalExtractProductDesc( String vitalExtractProductCode )
  {
    String            retVal = null;
    try
    {
      if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_ACTIVE_C_MANAGED ) )
      {
        retVal = "Active C Managed Solution";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_ACTIVE_C_V28_SOFTWARE ) )
      {
        retVal = "Active C v2.8 Software";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_CYBERCASH ) )
      {
        retVal = "Cybercash";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_CYBERSOURCE ) )
      {
        retVal = "Cybersource";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_DIAL_PAY_AUTH_CAPTURE ) )
      {
        retVal = "Dial Pay Auth & Capture";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_DIAL_TERMINAL ) )
      {
        retVal = "Dial Terminal";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_POS_PARTNER ) )
      {
        retVal = "POS Partner";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_POS_PARTNER_2000 ) )
      {
        retVal = "POS Partner 2000";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_LINK ) )
      {
        retVal = "Verisign Payflow Link";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_PRO ) )
      {
        retVal = "Verisign Payflow Pro";
      }
      else if ( vitalExtractProductCode.equals( mesConstants.VITAL_ME_VIRTUAL_NET ) )
      {
        retVal = "Virtual Net";
      }
    }
    catch( NullPointerException e )
    {
    }
    return( retVal );
  }

  //codes for possible errors on app..
  public static final String    ERROR_ALL                   = "allerrors";
  public static final String    ERROR_ABA_NOT_FOUND         = "abanotfound";

  // equipment profile generator codes (corres. to mesdb.EQUIP_PROFILE_GENERATORS table)
  public static final int       PROFGEN_UNDEFINED                   = 0;
  public static final int       PROFGEN_MMS                         = 1;
  public static final int       PROFGEN_VERICENTRE                  = 2;
  public static final int       PROFGEN_TERMMASTER                  = 3;
  public static final int       PROFGEN_VCTM                        = 4;  // pseudo (vericentre/termmaster)
  public static final int       PROFGEN_TRIDENT                     = 5;  // pseudo (PC-only setups that don't require a download)
  public static final int       PROFGEN_NURIT                       = 6;
  public static final int       DEFAULT_PROFGEN                     = PROFGEN_MMS;

  // equipment terminal applications
  public static final String    MMS_STAGE_BUILD_TERM_APP            = "STAGE";
  public static final String    DEFAULT_TERMAPP_MMS                 = MMS_STAGE_BUILD_TERM_APP;
  public static final String    DEFAULT_TERMAPP_VERICENTRE          = "VC";
  public static final String    DEFAULT_TERMAPP_TERMMASTER          = "TM";
  public static final String    TRIDENT_STAGE_BUILD_TERM_APP        = "TRI-STAGE";
  public static final String    TRIDENT_PAYANYWHERE_TERM_APP        = "PAYANY";
  public static final String    TRIDENT_VIRTUAL_TERMINAL_TERM_APP   = "VIR-TERM";
  public static final String    TRIDENT_API_TERM_APP                = "TRI-GATEWAY";
  public static final String    TRIDENT_VIRTUAL_TERMINAL_LIMITED    = "VT-LIMITED";
  public static final String    TRIDENT_PAYHERE_TERM_APP            = "PAY-HERE";

  // equipment terminal profile processing methods
  public static final String    ETPPM_UNDEFINED                     = null;
  public static final String    ETPPM_FTP                           = "FTP";
  public static final String    ETPPM_MANUAL                        = "MANUAL";
  public static final String    ETPPM_AUTO_TERMMASTER               = "TM";
  public static final String    ETPPM_AUTO_VERICENTRE               = "VC";

  // equipment terminal profile processing status
  public  static final String   ETPPS_UNDEFINED                     = null;
  public  static final String   ETPPS_NEW                           = "new";
  public  static final String   ETPPS_INPROCESS                     = "inproc";
  public  static final String   ETPPS_RECEIVED                      = "received";
  public  static final String   ETPPS_RESUBMITTED                   = "resubmitted";
  public  static final String   ETPPS_NO_RESPONSE                   = "no response";

  // equipment terminal profile processing responses
  public  static final String   ETPPR_UNDEFINED                     = null;
  public  static final String   ETPPR_PENDING                       = "pending";
  public  static final String   ETPPR_SUCCESS                       = "success";
  public  static final String   ETPPR_FAIL                          = "fail";

  // [Account] equipment type
  public static final int       EQUIPTYPE_UNDEFINED                 = 0;
  public static final int       EQUIPTYPE__START                    = 1;
  public static final int       EQUIPTYPE_MERCHEQUIP                = EQUIPTYPE__START;
  public static final int       EQUIPTYPE_EQUIPINV                  = 2;
  public static final int       EQUIPTYPE_TERMAPP                   = 3;
  public static final int       EQUIPTYPE_TIDREQUEST                = 4;
  public static final int       EQUIPTYPE__END                      = EQUIPTYPE_TIDREQUEST;

  // Account key
  public static final int       ACCOUNTKEY_UNDEFINED                = 0;
  public static final int       ACCOUNTKEY_APPSEQNUM                = 1;
  public static final int       ACCOUNTKEY_MERCHNUM                 = 2;
  public static final int       ACCOUNTKEY_MERCCNTRLNUMBER          = 3;

  // verisign xml message formats
  public static final int       VS_XML_FMT_ORIGINAL                 = 0;
  public static final int       VS_XML_FMT_XML_REG                  = 1;
  public static final int       VS_XML_FMT_XML_REG_V2               = 2;

  // front end hosts
  public static final int       HOST_INVALID                        = -1;
  public static final int       HOST_VITAL                          = 0;
  public static final int       HOST_TRIDENT                        = 1;

  // trident catid types
  public static final int       TRIDENT_CATID_TERM_MASTER           = 1;
  public static final int       TRIDENT_CATID_VERICENTRE            = 2;
  public static final int       TRIDENT_CATID_NURIT                 = 3;
  public static final int       TRIDENT_CATID_VIRTUAL_TERMINAL      = 8;
  public static final int       TRIDENT_CATID_VAR                   = 9;

  // terminal communication types
  public static final int       TERM_COMM_TYPE_DIAL                 = 1;
  public static final int       TERM_COMM_TYPE_IP                   = 2;
  public static final int       TERM_COMM_TYPE_WIRELESS             = 3;

  // Trident Product Codes
  public static final String    TRIDENT_PC_PC                       = "PC";
  public static final String    TRIDENT_PC_DIAL_TERMINAL            = "DT";
  public static final String    TRIDENT_PC_DIRECT_CONNECT           = "DC";
  public static final String    TRIDENT_PC_VIRTUAL_TERMINAL         = "VT";
  public static final String    TRIDENT_PC_TPG_SWIPE                = "GS";
  public static final String    TRIDENT_PC_TPG_ECOMMERCE            = "PG";
  public static final String    TRIDENT_PC_CONVERSION               = "CD";
  public static final String    TRIDENT_PC_PAYFLOW_LINK             = "VL";
  public static final String    TRIDENT_PC_PAYFLOW_PRO              = "VF";
  public static final String    TRIDENT_PC_VIRTUAL_NET_SSL          = "VN";
  public static final String    TRIDENT_PC_VT_LIMITED               = "VM";
  public static final String    TRIDENT_PC_VOICE_AUTH               = "VA";
  public static final String    TRIDENT_PC_TELE_PAY                 = "TP";
  public static final String    TRIDENT_PC_PAY_HERE                 = "PH";

  //Letter generation for Call Tags
  public static String[][] CT_LETTERS =
  {
    {"1","General"},
    {"2","MOE Loan Swap"},
    {"3","MOE Loan Swap Back"},
    {"4","PinPad Encrypt"},
    {"5","PinPad Reject"},
    {"6","PinPad Swap"},
    {"7","Restricted Swap"},
    {"8","Swap"}
  };

  //generic tags for servlet processing
  public static final String MERCH_NUM_TAG                           = "merchant";

  //quizzes, questionnaires, exams, tests
  public static final int QUIZ_PCI_QUESTIONNAIRE                     = 1;
  public static final int QUIZ_PCI_QUESTIONNAIRE_V2                  = 2;
  public static final int QUIZ_PCI_QUESTIONNAIRE_V3                  = 3;

  public static final String QUIZ_HISTORY_TAG                        = "hisId";
  public static final String QUIZ_TYPE_TAG                           = "qtype";
  public static final String QUIZ_ACTION_TAG                         = "action";
  public static final String QUIZ_ID_TAG                             = "quid";
  public static final String QUIZ_PAGE_TAG                           = "pgNum";

  public static final int FOREIGN_ID_TYPE_FISERV                      = 1;
  public static final int FOREIGN_ID_TYPE_ELAN                        = 2;

  public static final int APP_DOC_COMP_CR_WORKSHEET                   = 1;

  // International Processor Ids
  public static final int     INTL_PID_NONE                           = 0;
  public static final int     INTL_PID_DCC                            = 1;
  public static final int     INTL_PID_MES                            = 2;
  public static final int     INTL_PID_PAYVISION                      = 3;
  public static final int     INTL_PID_ADYEN                          = 4;
  public static final int     INTL_PID_BRASPAG                        = 5;
  public static final int     INTL_PID_GTS                            = 6;
  public static final int     INTL_PID_ITPS                           = 7;
  
  //Dropdown code types
  public static final int MERCHANT_INFO_ACCT_MANAGED_BY_CODE_TYPE = 1;
  public static final int MERCHANT_INFO_TIER_X_CODE_TYPE          = 2;

  // EMV Tag Definitions
  public static final int EMV_TAG_TRAN_TYPE_REC = 0x9C;					//Transaction Type
  public static final int EMV_TAG_TERMINAL_TRAN_DATE=0x9A;				//Terminal Transaction Date
  public static final int EMV_TAG_TVR=0x95;								//Terminal Verification Results
  public static final int EMV_TAG_CURRENCY_CODE=0x5F2A;					//Terminal Currency Code
  public static final int EMV_TAG_COUNTRY_CODE=0x9F1A;					//Terminal Country Code
  public static final int EMV_TAG_APP_TRAN_CNTR=0x9F36;					//Application Transaction Counter
  public static final int EMV_TAG_APP_INTERCHG_PROFILE=0x82;			//Application Interchange Profile
  public static final int EMV_TAG_AMT_OTHER=0x9F03;						//Amount Other
  public static final int EMV_TAG_APP_CRYPTOGRAM=0x9F26;				//Application Cryptogram
  public static final int EMV_TAG_UNPREDICTABLE_NUM=0x9F37;				//Unpredictable Number
  public static final int EMV_TAG_ISSUER_APP_DATA=0x9F10;				//Issuer Application Data
  public static final int EMV_TAG_CRYPTO_INFO_DATA=0x9F27;				//Cryptogram Information Data
  public static final int EMV_TAG_CRYPTO_AMOUNT=0x9F02;					//Amount authorized										
  public static final int EMV_TAG_APP_PAN_SEQ_NUM=0x5F34;				//Card Sequence Number
  public static final int EMV_TAG_TERMINAL_CAPABILITIES=0x9F33;		    //Terminal Capabilities
  public static final int EMV_TAG_TERMINAL_TYPE=0x9F35;		    		//Terminal Type
  public static final int EMV_TAG_ISSUER_AUTH_DATA=0x91;		    	//Issuer Authentication Data
  public static final int EMV_TAG_APP_USAGE=0x9F07;						//Application Usage Control
  public static final int EMV_TAG_CARDHOLDER_VERIF=0x9F34;				//Card holder Verification Method Results
  public static final int EMV_TAG_FORM_FACTOR=0x9F6E;  				    //Form factor indicator
  public static final int EMV_TAG_ISSUER_SCRIPT=0x9F5B;	 			    //Issuer script results
  public static final int EMV_TAG_AUTH_RESPONSE=0x8A;	 			    //Authorization response code
  public static final int EMV_TAG_POS_ENTRY_MODE=0x9F39;	 			//POS entry mode
  public static final int EMV_TAG_DEDICATED_FILE_NAME=0x84;       //Dedicated File Name
  

}
