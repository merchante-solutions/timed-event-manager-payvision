/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesQueues.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-08-15 13:49:16 -0700 (Mon, 15 Aug 2011) $
  Version            : $Revision: 19137 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public final class MesQueues
{
  // queue item types
  public static final int     Q_ITEM_TYPE_ACCT_CHANGE         = 1;
  public static final int     Q_ITEM_TYPE_ACTIVATE            = 2;
  public static final int     Q_ITEM_TYPE_PROGRAMMING         = 3;
  public static final int     Q_ITEM_TYPE_MMS                 = 4;
  public static final int     Q_ITEM_TYPE_MMS_ERROR           = 5;
  public static final int     Q_ITEM_TYPE_CBT_CREDIT          = 7;
  public static final int     Q_ITEM_TYPE_MES_CREDIT          = 8;
  public static final int     Q_ITEM_TYPE_MES_MATCHCREDIT     = 9;
  public static final int     Q_ITEM_TYPE_CHANGE_REQUEST      = 10;
  public static final int     Q_ITEM_TYPE_ACH_REJECTS         = 11;
  public static final int     Q_ITEM_TYPE_ACTIVATION          = 12;
  public static final int     Q_ITEM_TYPE_DEPLOYMENT          = 13;
  public static final int     Q_ITEM_TYPE_RISK                = 14;
  public static final int     Q_ITEM_TYPE_COLLECTIONS         = 15;
  public static final int     Q_ITEM_TYPE_LEGAL               = 16;
  public static final int     Q_ITEM_TYPE_CBT_DOCUMENTS       = 17;
  public static final int     Q_ITEM_TYPE_CHARGEBACK          = 18;
  public static final int     Q_ITEM_TYPE_RETRIEVAL           = 19;
  public static final int     Q_ITEM_TYPE_CALLTAG             = 20;
  public static final int     Q_ITEM_TYPE_NON_RETURNABLE      = 21;
  public static final int     Q_ITEM_TYPE_CBT_MMS             = 22;
  public static final int     Q_ITEM_TYPE_CBT_REVIEW          = 23;
  public static final int     Q_ITEM_TYPE_BBT_QA              = 24;
  public static final int     Q_ITEM_TYPE_REVIEW_QUEUE        = 25;

  // cb&t review queue types, versions 1, 2
  public static final int     Q_ITEM_TYPE_CBT_DATA_ASSIGN     = 26;
  public static final int     Q_ITEM_TYPE_CBT_RISKY_SIC       = 27;
  public static final int     Q_ITEM_TYPE_CBT_PULL_CREDIT     = 28;
  public static final int     Q_ITEM_TYPE_CBT_CREDIT_REVIEW   = 29;
  public static final int     Q_ITEM_TYPE_CBT_DOC             = 30;

  // cb&t review queue types, versions 3 ("tiering")
  public static final int     Q_ITEM_TYPE_CBT_SCORING         = 31;
  public static final int     Q_ITEM_TYPE_CBT_MCC             = 32;
  public static final int     Q_ITEM_TYPE_CBT_TIER            = 33;
  public static final int     Q_ITEM_TYPE_CBT_34_REVIEW       = 34;
  public static final int     Q_ITEM_TYPE_CBT_HIGH_VOLUME     = 35;
  public static final int     Q_ITEM_TYPE_CBT_LOW_SCORE       = 36;
  public static final int     Q_ITEM_TYPE_CBT_LARGE_TKT       = 37;
  public static final int     Q_ITEM_TYPE_CBT_TIER_DOC        = 38;
  public static final int     Q_ITEM_TYPE_CBT_FINANCIAL       = 40;
  public static final int     Q_ITEM_TYPE_CBT_HIGH_RISK       = 41;

  //PCI group
  public static final int     Q_ITEM_TYPE_PCI_EXCEPTIONS      = 43;

  //backend package reject queue types
  public static final int     Q_ITEM_TYPE_PACKAGE_REJECTS     = 44;

  // mbs remittance queue type
  public static final int     Q_ITEM_TYPE_MBS_REMITTANCE      = 45;

  // trident debit queue types
  public static final int     Q_ITEM_TYPE_DEBIT_EXCEPTIONS    = 39;


  /**************************************************
  ** QUEUE TYPES are grouped by the hundreds digit
  ***************************************************/

  public static final int     Q_NONE                          = -1;

  public static final int     Q_ACCT_CHANGE_NEW               = 100;
  public static final int     Q_ACCT_CHANGE_PEND              = 101;
  public static final int     Q_ACCT_CHANGE_CANCEL            = 102;
  public static final int     Q_ACCT_CHANGE_COMPLETE          = 103;
  public static final int     Q_ACCT_CHANGE_CREDIT            = 104;
  public static final int     Q_ACCT_CHANGE_SETUP             = 105;
  public static final int     Q_ACCT_CHANGE_DEPLOYMENT        = 106;
  public static final int     Q_ACCT_CHANGE_ACTIVATION        = 107;
  public static final int     Q_ACCT_CHANGE_ADJUSTMENTS       = 108;
  public static final int     Q_ACCT_CHANGE_ACCT_ENTRY        = 109;
  public static final int     Q_ACCT_CHANGE_MMS               = 110;

  public static final int     Q_ACTIVATE_NEW                  = 200;
  public static final int     Q_ACTIVATE_ASSIGNED             = 201;

  public static final int     Q_PROGRAMMING_REGULAR           = 300;
  public static final int     Q_PROGRAMMING_SPECIAL           = 301;
  public static final int     Q_PROGRAMMING_COMPLETE          = 302;
  public static final int     Q_PROGRAMMING_TM_NEW            = 303;
  public static final int     Q_PROGRAMMING_TM_PENDING        = 304;
  public static final int     Q_PROGRAMMING_TM_COMPLETED      = 305;
  public static final int     Q_PROGRAMMING_VC_NEW            = 306;
  public static final int     Q_PROGRAMMING_VC_PENDING        = 307;
  public static final int     Q_PROGRAMMING_VC_COMPLETED      = 308;


  public static final int     Q_PCI_EXCEPTION_NEW             = 350;
  public static final int     Q_PCI_EXCEPTION_COMPLIANT       = 351;
  public static final int     Q_PCI_EXCEPTION_NONCOMPLIANT    = 352;
  public static final int     Q_PCI_EXCEPTION_CALL            = 353;

  public static final int     Q_REJECTS_RAW                   = 370;  //???
  public static final int     Q_REJECTS_UNWORKED              = 371;
  public static final int     Q_REJECTS_UNWORKED_2ND          = 376;
  public static final int     Q_REJECTS_PENDING_CLEAR         = 372;
  public static final int     Q_REJECTS_PENDING_KILL          = 373;
  public static final int     Q_REJECTS_KILL                  = 374;
  public static final int     Q_REJECTS_COMPLETED             = 375;

  public static final int     Q_MMS_NEW                       = 400;
  public static final int     Q_MMS_PEND                      = 401;
  public static final int     Q_MMS_COMPLETE                  = 402;
  public static final int     Q_MMS_VCTM_NEW                  = 403;
  public static final int     Q_MMS_EXCEPTION                 = 404;
  public static final int     Q_MMS_CERTIFICATION             = 405;

  public static final int     Q_MMS_ERROR                     = 500;



  // cb&t app review queues, version 1
  public static final int     Q_CBT_NEW                       = 600;
  public static final int     Q_CBT_PENDING                   = 601;
  public static final int     Q_CBT_DECLINED                  = 602;
  public static final int     Q_CBT_CANCELLED                 = 603;
  public static final int     Q_CBT_APPROVED                  = 604;

  public static final int     Q_CBT_NEW_HIGH_VOLUME           = 610;
  public static final int     Q_CBT_PENDING_HIGH_VOLUME       = 611;
  public static final int     Q_CBT_DECLINED_HIGH_VOLUME      = 612;
  public static final int     Q_CBT_CANCELLED_HIGH_VOLUME     = 613;
  public static final int     Q_CBT_APPROVED_HIGH_VOLUME      = 614;

  public static final int     Q_CBT_INCOMPLETE                = 620;
  public static final int     Q_CBT_COMPLETE                  = 621;
  public static final int     Q_CBT_REVIEW_CANCELLED          = 622;

  public static final int     Q_CBT_DOC_WORKING               = 630;
  public static final int     Q_CBT_DOC_COMPLETE              = 631;


  // cb&t app review queues, version 2
  public static final int     Q_CBT_DATA_ASSIGN_NEW           = 700;
  public static final int     Q_CBT_DATA_ASSIGN_COMPLETED     = 701;
  public static final int     Q_CBT_DATA_ASSIGN_CANCELED      = 702;

  public static final int     Q_CBT_RISKY_SIC_NEW             = 710;
  public static final int     Q_CBT_RISKY_SIC_APPROVED        = 711;
  public static final int     Q_CBT_RISKY_SIC_DECLINED        = 712;
  public static final int     Q_CBT_RISKY_SIC_CANCELED        = 713;

  public static final int     Q_CBT_PULL_CREDIT_NEW           = 720;
  public static final int     Q_CBT_PULL_CREDIT_COMPLETED     = 721;
  public static final int     Q_CBT_PULL_CREDIT_CANCELED      = 722;

  public static final int     Q_CBT_LOW_SCORE_NEW             = 730;
  public static final int     Q_CBT_LOW_SCORE_PENDED          = 731;
  public static final int     Q_CBT_LOW_SCORE_APPROVED        = 732;
  public static final int     Q_CBT_LOW_SCORE_DECLINED        = 733;
  public static final int     Q_CBT_LOW_SCORE_CANCELED        = 734;

  public static final int     Q_CBT_HIGH_VOLUME_NEW           = 740;
  public static final int     Q_CBT_HIGH_VOLUME_PENDED        = 741;
  public static final int     Q_CBT_HIGH_VOLUME_APPROVED      = 742;
  public static final int     Q_CBT_HIGH_VOLUME_DECLINED      = 743;
  public static final int     Q_CBT_HIGH_VOLUME_CANCELED      = 744;

  public static final int     Q_CBT_LARGE_TICKET_NEW          = 750;
  public static final int     Q_CBT_LARGE_TICKET_PENDED       = 751;
  public static final int     Q_CBT_LARGE_TICKET_APPROVED     = 752;
  public static final int     Q_CBT_LARGE_TICKET_DECLINED     = 753;
  public static final int     Q_CBT_LARGE_TICKET_CANCELED     = 754;

  public static final int     Q_CBT_DOC_NEW                   = 760;
  public static final int     Q_CBT_DOC_COMPLETED             = 761;
  public static final int     Q_CBT_DOC_CANCELED              = 762;
  public static final int     Q_CBT_DOC_PENDED                = 763;



  public static final int     Q_MES_CREDIT_NEW                = 801;
  public static final int     Q_MES_CREDIT_PENDING            = 802;
  public static final int     Q_MES_CREDIT_DECLINE            = 803;

  public static final int     Q_MATCHCREDIT                   = 901;

  /* New Account Change Request (ACR) queues */
  public static final int     Q_CHANGE_REQUEST_START          = 1001;
  public static final int     Q_CHANGE_REQUEST_RESEARCH       = Q_CHANGE_REQUEST_START;
  public static final int     Q_CHANGE_REQUEST_RISK           = 1002;
  public static final int     Q_CHANGE_REQUEST_ACCOUNT_SETUP  = 1003;   // deprecated (replaced by tsys/mms account setup queues)
  public static final int     Q_CHANGE_REQUEST_DEPLOYMENT     = 1004;
  public static final int     Q_CHANGE_REQUEST_RESEARCH_QA    = 1005;   // aka manager review queue
  public static final int     Q_CHANGE_REQUEST_ACTIVATIONS    = 1006;
  public static final int     Q_CHANGE_REQUEST_PRICING        = 1007;
  public static final int     Q_CHANGE_REQUEST_COMPLETED      = 1008;
  public static final int     Q_CHANGE_REQUEST_CANCELLED      = 1009;
  public static final int     Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP = 1010;
  public static final int     Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP  = 1011;
  public static final int     Q_CHANGE_REQUEST_SUPPLIES       = 1012;

  public static final int     Q_CHANGE_REQUEST_CBT_NEW        = 1013;
  public static final int     Q_CHANGE_REQUEST_CBT_MMS        = 1014;
  public static final int     Q_CHANGE_REQUEST_CBT_TSYS       = 1015;
  public static final int     Q_CHANGE_REQUEST_CBT_MGMT       = 1016;
  public static final int     Q_CHANGE_REQUEST_ASG_MGMT       = 1017;


  //should have left room here for extra CBT queues
  //see below (1040)

  /*BBT separate (but mirror) queues

  ACR Pending Docs              = RISK
  ACR Data Entry                = DEPLOYMENT
  ACR QA                        = RESEARCH
  ACR Outbound Call Needed      = ACTIVATIONS
  ACR Data Entry Exception      = PRICING
  ACR Client Service Exception  = TSYS_ACCOUNT_SETUP
  ACR Accounting                = MMS_ACCOUNT_SETUP

  */
  public static final int     Q_CHANGE_REQUEST_BBT_PENDING              = 1018;
  public static final int     Q_CHANGE_REQUEST_BBT_DATA_ENTRY           = 1019;
  public static final int     Q_CHANGE_REQUEST_BBT_QA                   = 1020;
  public static final int     Q_CHANGE_REQUEST_BBT_OUTBOUND_CALL        = 1021;
  public static final int     Q_CHANGE_REQUEST_BBT_DATA_ENTRY_EXCP      = 1022;
  public static final int     Q_CHANGE_REQUEST_BBT_CLIENT_SERVICE_EXCP  = 1023;
  public static final int     Q_CHANGE_REQUEST_BBT_ACCOUNTING           = 1024;
  public static final int     Q_CHANGE_REQUEST_BBT_MGMT                 = 1025;
  public static final int     Q_CHANGE_REQUEST_BBT_COMPLETED            = 1026;
  public static final int     Q_CHANGE_REQUEST_BBT_CANCELLED            = 1027;
  public static final int     Q_CHANGE_REQUEST_BBT_ACCT_MGR             = 1028;

/*
MAY 09 - below

new MES queues for ACRs
TSYS AUDIT
POS AUDIT

new CB&T queues for ACRs
CB&T TSYS AUDIT,
CB&T POS AUDIT
*/

  public static final int     Q_CHANGE_REQUEST_DEPLOY_REVIEW  = 1029;
  public static final int     Q_CHANGE_REQUEST_MES_CONVERSION = 1030;
  public static final int     Q_CHANGE_REQUEST_NON_BANKCARD   = 1031;
  public static final int     Q_CHANGE_REQUEST_TSYS_AUDIT     = 1032;
  public static final int     Q_CHANGE_REQUEST_POS_AUDIT      = 1033;

  public static final int     Q_CHANGE_REQUEST_CBT_RISKMGMT   = 1040;
  public static final int     Q_CHANGE_REQUEST_CBT_TSYS_AUDIT = 1041;
  public static final int     Q_CHANGE_REQUEST_CBT_POS_AUDIT  = 1042;

/*
APR 10 - below

new MES CB&T queues for ACRs
Help desk CB&T MGMT,
ASG CB&T MGMT
*/
  public static final int     Q_CHANGE_REQUEST_HELP_CBT_MGMT  = 1034;
  public static final int     Q_CHANGE_REQUEST_ASG_CBT_MGMT   = 1035;
  public static final int     Q_CHANGE_REQUEST_3858_POS_SETUP = 1036;

  public static final int     Q_CHANGE_REQUEST_BANNER_MGMT    = 1051;
  public static final int     Q_CHANGE_REQUEST_BUTTE_MGMT     = 1052;
  public static final int     Q_CHANGE_REQUEST_STERLING_MGMT  = 1053;
  public static final int     Q_CHANGE_REQUEST_FM_MGMT        = 1054;
  public static final int     Q_CHANGE_REQUEST_TRANSCOM_MGMT  = 1055;
  public static final int     Q_CHANGE_REQUEST_CEDARS_MGMT    = 1056;
  public static final int     Q_CHANGE_REQUEST_CDMS_MGMT      = 1057;
  public static final int     Q_CHANGE_REQUEST_ELMHURST_MGMT  = 1058;
  public static final int     Q_CHANGE_REQUEST_FF_MGMT        = 1059;
  public static final int     Q_CHANGE_REQUEST_FOOTHILL_MGMT  = 1060;
  public static final int     Q_CHANGE_REQUEST_MWB_MGMT       = 1061;
  public static final int     Q_CHANGE_REQUEST_RIVER_MGMT     = 1062;
  public static final int     Q_CHANGE_REQUEST_SILICON_MGMT   = 1063;
  public static final int     Q_CHANGE_REQUEST_GOLD_MGMT      = 1064;
  public static final int     Q_CHANGE_REQUEST_ORANGE_MGMT    = 1065;
  public static final int     Q_CHANGE_REQUEST_EXCHANGE_MGMT  = 1066;
  public static final int     Q_CHANGE_REQUEST_COUNTRY_MGMT   = 1067;
  public static final int     Q_CHANGE_REQUEST_BELL_MGMT      = 1068;
  public static final int     Q_CHANGE_REQUEST_COMM_MGMT      = 1069;
  public static final int     Q_CHANGE_REQUEST_AMERICA_MGMT   = 1070;
  public static final int     Q_CHANGE_REQUEST_FIRSTAM_MGMT   = 1071;
  public static final int     Q_CHANGE_REQUEST_SVALLEY_MGMT   = 1072;
  public static final int     Q_CHANGE_REQUEST_FDAKOTA_MGMT   = 1073;
  public static final int     Q_CHANGE_REQUEST_PIONEER_MGMT   = 1075;
  public static final int     Q_CHANGE_REQUEST_HAYPBW_MGMT    = 1076;
  public static final int     Q_CHANGE_REQUEST_HAYSUMMIT_MGMT = 1077;
  public static final int     Q_CHANGE_REQUEST_FSECURITY_MGMT = 1078;
  public static final int     Q_CHANGE_REQUEST_STERLING2_MGMT = 1079;
  public static final int     Q_CHANGE_REQUEST_SATLANTIC_MGMT = 1080;
  public static final int     Q_CHANGE_REQUEST_STEARNS_MGMT   = 1081;
  public static final int     Q_CHANGE_REQUEST_21CENTURY_MGMT = 1082;
  public static final int     Q_CHANGE_REQUEST_GRANT_MGMT     = 1083;
  public static final int     Q_CHANGE_REQUEST_FMLB_MGMT      = 1084;
  public static final int     Q_CHANGE_REQUEST_CENTURY_MGMT   = 1085;
  public static final int     Q_CHANGE_REQUEST_COMMKS_MGMT    = 1086;
  public static final int     Q_CHANGE_REQUEST_GREEN_MGMT     = 1087;
  public static final int     Q_CHANGE_REQUEST_BEACH_MGMT     = 1088;
  public static final int     Q_CHANGE_REQUEST_RIVERVIEW_MGMT = 1089;
  public static final int     Q_CHANGE_REQUEST_AMWEST_MGMT    = 1090;
  public static final int     Q_CHANGE_REQUEST_ARMSTRONG_MGMT = 1091;
  public static final int     Q_CHANGE_REQUEST_REPUBLIC_MGMT  = 1092;
  public static final int     Q_CHANGE_REQUEST_ALPINE_MGMT    = 1093;
  public static final int     Q_CHANGE_REQUEST_FIRST_COMM_MGMT= 1094;
  public static final int     Q_CHANGE_REQUEST_PCWB_MGMT      = 1095;
  public static final int     Q_CHANGE_REQUEST_BNC_MGMT       = 1096;
  public static final int     Q_CHANGE_REQUEST_SKAGIT_MGMT    = 1097;
  public static final int     Q_CHANGE_REQUEST_BILLING_TREE_MGMT    = 1098;
  public static final int     Q_CHANGE_REQUEST_SVB_MGMT    = 1099;


  public static final int     Q_CHANGE_REQUEST_END            = 1099;

  public static final int     Q_PRODUCT_DETERMINE_NEW         = 2800;
  public static final int     Q_PRODUCT_DETERMINE_CANCELED    = 2801;

  public static String getACRQueueDescriptor(int q)
  {
    switch(q) {
      case MesQueues.Q_CHANGE_REQUEST_RESEARCH:
        return "Research";
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        return "Risk";
      case MesQueues.Q_CHANGE_REQUEST_ACCOUNT_SETUP:
        return "Account Setup";
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        return "TSYS Account Setup";
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
        return "MMS Account Setup";
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
        return "Deployment";
      case MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW:
        return "Deployment Working";
      case MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA:
        return "Mgmt Review";
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        return "Activiations";
      case MesQueues.Q_CHANGE_REQUEST_PRICING:
        return "Pricing";
      case MesQueues.Q_CHANGE_REQUEST_COMPLETED:
      case MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED:
        return "Completed";
      case MesQueues.Q_CHANGE_REQUEST_CANCELLED:
      case MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED:
        return "Cancelled";
      case MesQueues.Q_CHANGE_REQUEST_BBT_MGMT:
        return "BBT Mgmt";
      case MesQueues.Q_CHANGE_REQUEST_SUPPLIES:
        return "Supplies";
      case MesQueues.Q_CHANGE_REQUEST_CBT_NEW:
        return "CB&T New";
      case MesQueues.Q_CHANGE_REQUEST_CBT_TSYS:
        return "CB&T TSYS";
      case MesQueues.Q_CHANGE_REQUEST_CBT_MMS:
        return "CB&T MMS";
      case MesQueues.Q_CHANGE_REQUEST_CBT_MGMT:
        return "CB&T MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ASG_MGMT:
        return "ASG MGMT";
      case MesQueues.Q_CHANGE_REQUEST_CBT_RISKMGMT:
        return "CB&T Risk MGMT";
      case MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT:
        return "Help Desk CB&T MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT:
        return "ASG CB&T MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BANNER_MGMT:
        return "Banner MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BUTTE_MGMT:
        return "Butte MGMT";
      case MesQueues.Q_CHANGE_REQUEST_STERLING_MGMT:
        return "Sterling MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FM_MGMT:
        return "Farmers & Merchants MGMT";
      case MesQueues.Q_CHANGE_REQUEST_TRANSCOM_MGMT:
        return "Transcom MGMT";
      case MesQueues.Q_CHANGE_REQUEST_CEDARS_MGMT:
        return "Cedars MGMT";
      case MesQueues.Q_CHANGE_REQUEST_CDMS_MGMT:
        return "CDMS MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ELMHURST_MGMT:
        return "Elmhurst MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FF_MGMT:
        return "First Financial MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FOOTHILL_MGMT:
        return "Foothill MGMT";
      case MesQueues.Q_CHANGE_REQUEST_MWB_MGMT:
        return "MWB MGMT";
      case MesQueues.Q_CHANGE_REQUEST_RIVER_MGMT:
        return "River City MGMT";
      case MesQueues.Q_CHANGE_REQUEST_SILICON_MGMT:
        return "Silicon MGMT";
      case MesQueues.Q_CHANGE_REQUEST_GOLD_MGMT:
        return "Gold MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ORANGE_MGMT:
        return "Orange MGMT";
      case MesQueues.Q_CHANGE_REQUEST_EXCHANGE_MGMT:
        return "Exchange MGMT";
      case MesQueues.Q_CHANGE_REQUEST_COUNTRY_MGMT:
        return "CCBHC MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BELL_MGMT:
        return "BCMS MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY:
        return "Data Entry";
      case MesQueues.Q_CHANGE_REQUEST_BBT_PENDING:
        return "Pending Docs";
      case MesQueues.Q_CHANGE_REQUEST_BBT_QA:
        return "QA";
      case MesQueues.Q_CHANGE_REQUEST_BBT_OUTBOUND_CALL:
        return "Outbound Call";
      case MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY_EXCP:
        return "Data Entry Excp";
      case MesQueues.Q_CHANGE_REQUEST_BBT_CLIENT_SERVICE_EXCP:
        return "Client Service Excp";
      case MesQueues.Q_CHANGE_REQUEST_BBT_ACCOUNTING:
        return "Accounting";
      case MesQueues.Q_CHANGE_REQUEST_BBT_ACCT_MGR:
        return "Account Mgr";
      case MesQueues.Q_CHANGE_REQUEST_TSYS_AUDIT:
        return "TSYS Audit";
      case MesQueues.Q_CHANGE_REQUEST_POS_AUDIT:
        return "POS Audit";
      case MesQueues.Q_CHANGE_REQUEST_MES_CONVERSION:
        return "Conversion";
      case MesQueues.Q_CHANGE_REQUEST_NON_BANKCARD:
        return "Non-Bankcard";
      case MesQueues.Q_CHANGE_REQUEST_FIRSTAM_MGMT:
        return "First Am MGMT";
      case MesQueues.Q_CHANGE_REQUEST_SVALLEY_MGMT:
        return "S Valley MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FDAKOTA_MGMT:
        return "F Dakota MGMT";
      case MesQueues.Q_CHANGE_REQUEST_PIONEER_MGMT:
        return "Pioneer MGMT";
      case MesQueues.Q_CHANGE_REQUEST_HAYPBW_MGMT:
        return "Hayward PBW MGMT";
      case MesQueues.Q_CHANGE_REQUEST_HAYSUMMIT_MGMT:
        return "Hayward Summit MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FSECURITY_MGMT:
        return "F Security MGMT";
      case MesQueues.Q_CHANGE_REQUEST_STERLING2_MGMT:
        return "Sterling MGMT";
      case MesQueues.Q_CHANGE_REQUEST_SATLANTIC_MGMT:
        return "S Atlantic MGMT";
      case MesQueues.Q_CHANGE_REQUEST_STEARNS_MGMT:
        return "Stearns MGMT";
      case MesQueues.Q_CHANGE_REQUEST_21CENTURY_MGMT:
        return "21 Century MGMT";
      case MesQueues.Q_CHANGE_REQUEST_GRANT_MGMT:
        return "Grant MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FMLB_MGMT:
        return "F&M Long Beach MGMT";
      case MesQueues.Q_CHANGE_REQUEST_CENTURY_MGMT:
        return "Century MGMT";
      case MesQueues.Q_CHANGE_REQUEST_COMMKS_MGMT:
        return "Comm Bakers KS MGMT";
      case MesQueues.Q_CHANGE_REQUEST_GREEN_MGMT:
        return "Green MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BEACH_MGMT:
        return "Beach First MGMT";
      case MesQueues.Q_CHANGE_REQUEST_RIVERVIEW_MGMT:
        return "Riverview MGMT";
      case MesQueues.Q_CHANGE_REQUEST_AMWEST_MGMT:
        return "Am West MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ARMSTRONG_MGMT:
        return "Armstrong MGMT";
      case MesQueues.Q_CHANGE_REQUEST_REPUBLIC_MGMT:
        return "Republic MGMT";
      case MesQueues.Q_CHANGE_REQUEST_ALPINE_MGMT:
        return "Alpine MGMT";
      case MesQueues.Q_CHANGE_REQUEST_FIRST_COMM_MGMT:
        return "First Comm MGMT";
      case MesQueues.Q_CHANGE_REQUEST_PCWB_MGMT:
        return "PCWB MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BNC_MGMT:
        return "BNC MGMT";
      case MesQueues.Q_CHANGE_REQUEST_SKAGIT_MGMT:
        return "Skagit MGMT";
      case MesQueues.Q_CHANGE_REQUEST_BILLING_TREE_MGMT:
        return "Billing Tree MGMT";
      case MesQueues.Q_CHANGE_REQUEST_SVB_MGMT:
        return "SVB MGMT";
      case MesQueues.Q_CHANGE_REQUEST_3858_POS_SETUP:
        return "CB&T POS Acct Setup";
      default:
        return "MGMT";
    }
  }

  public static final int     Q_ACH_REJECT_CATEGORY_1         = 1101;
  public static final int     Q_ACH_REJECT_CATEGORY_2         = 1102;
  public static final int     Q_ACH_REJECT_CATEGORY_3         = 1103;
  public static final int     Q_ACH_REJECT_CATEGORY_MISC      = 1104;
  public static final int     Q_ACH_REJECT_CATEGORY_URO       = 1105;
  public static final int     Q_ACH_REJECT_COMPLETED          = 1106;
  public static final int     Q_ACH_REJECT_MANUAL_CHARGE_REC  = 1107;

  public static final int     Q_ACTIVATION_NEW                = 1201;
  public static final int     Q_ACTIVATION_PENDING            = 1202;
  public static final int     Q_ACTIVATION_SCHEDULED          = 1203;
  public static final int     Q_ACTIVATION_COMPLETED          = 1204;
  public static final int     Q_ACTIVATION_REFERRED           = 1205;

  public static final int     Q_DEPLOYMENT_NEW                = 1301;
  public static final int     Q_DEPLOYMENT_COMPLETED          = 1302;

  public static final int     Q_RISK_NEW                      = 1401;
  public static final int     Q_RISK_COMPLETED                = 1402;

  public static final int     Q_COLLECTIONS_NEW               = 1501;
  public static final int     Q_COLLECTIONS_COMPLETED         = 1502;

  public static final int     Q_LEGAL_NEW                     = 1601;
  public static final int     Q_LEGAL_COMPLETED               = 1602;

  // chargeback/retrieval all queues
  public static final int     Q_RETRIEVALS_ALL                = 1700;
  public static final int     Q_CHARGEBACKS_ALL               = 1701;

  // specific retrieval queues
  public static final int     Q_RETRIEVALS_VISA_CANCELLED     = 1708;
  public static final int     Q_RETRIEVALS_MC_CANCELLED       = 1709;
  public static final int     Q_RETRIEVALS_VISA_INCOMING      = 1710;
  public static final int     Q_RETRIEVALS_VISA_PENDING       = 1711;
  public static final int     Q_RETRIEVALS_VISA_COMPLETED     = 1712;
  public static final int     Q_RETRIEVALS_MC_INCOMING        = 1713;
  public static final int     Q_RETRIEVALS_MC_PENDING         = 1714;
  public static final int     Q_RETRIEVALS_MC_COMPLETED       = 1715;
  public static final int     Q_RETRIEVALS_VISA_NON_FULFILLED = 1716;
  public static final int     Q_RETRIEVALS_MC_NON_FULFILLED   = 1717;
  public static final int     Q_RETRIEVALS_VISA_SECOND_LETTER = 1718;
  public static final int     Q_RETRIEVALS_MC_SECOND_LETTER   = 1719;
  public static final int     Q_RETRIEVALS_AMEX_INCOMING      = 1720;
  public static final int     Q_RETRIEVALS_AMEX_PENDING       = 1721;
  public static final int     Q_RETRIEVALS_AMEX_COMPLETED     = 1722;
  public static final int     Q_RETRIEVALS_AMEX_NON_FULFILLED = 1723;
  public static final int     Q_RETRIEVALS_DS_INCOMING        = 1724;
  public static final int     Q_RETRIEVALS_DS_PENDING         = 1725;
  public static final int     Q_RETRIEVALS_DS_COMPLETED       = 1726;
  public static final int     Q_RETRIEVALS_DS_NON_FULFILLED   = 1727;

  // Visa incoming queues
  public static final int     Q_CHARGEBACKS_VISA_WITH_DOC     = 1750;
  public static final int     Q_CHARGEBACKS_VISA_NO_DOC       = 1751;
  public static final int     Q_CHARGEBACKS_VISA_RESEARCH     = 1752;

  // completed
  public static final int     Q_CHARGEBACKS_COMPLETED_REVERSAL= 1753;

  // second time and worked queues
  public static final int     Q_CHARGEBACKS_SECOND_TIME       = 1760;
  public static final int     Q_CHARGEBACKS_FULL_PMT          = 1761;
  public static final int     Q_CHARGEBACKS_PARTIAL_PMT       = 1762;
  public static final int     Q_CHARGEBACKS_MCHB              = 1763;
  public static final int     Q_CHARGEBACKS_REPR              = 1764;
  public static final int     Q_CHARGEBACKS_REMC              = 1765;
  public static final int     Q_CHARGEBACKS_MCLS              = 1766;
  public static final int     Q_CHARGEBACKS_LOSS              = 1767;
  public static final int     Q_CHARGEBACKS_COLL              = 1768;

  // MC incoming queues
  public static final int     Q_CHARGEBACKS_MC_WITH_DOC       = 1770;
  public static final int     Q_CHARGEBACKS_MC_NO_DOC         = 1771;
  public static final int     Q_CHARGEBACKS_MC_RESEARCH       = 1772;

  //New queue definitions
  public static final int     Q_CHARGEBACKS_MERCH_REB_HIGH    = 1779; //added to improve rebuttal handling
  public static final int     Q_CHARGEBACKS_COMPLETED         = 1780; //MCHB, REPR, REMC
  public static final int     Q_CHARGEBACKS_MERCH_REB         = 1781; //holding queue - still MCHB
  public static final int     Q_CHARGEBACKS_NEW               = 1782; //No auto-sorting
  public static final int     Q_CHARGEBACKS_OVER_125          = 1783;
  public static final int     Q_CHARGEBACKS_SECOND_TIME_MC    = 1784;
  public static final int     Q_CHARGEBACKS_PRE_ARB           = 1785; //visa 2nd time - manually entered
  public static final int     Q_CHARGEBACKS_PRE_COMPLIANCE    = 1786; //holding queue - manually entered
  public static final int     Q_CHARGEBACKS_AMEX              = 1787; // - manually entered
  public static final int     Q_CHARGEBACKS_MGMT              = 1788; //locks CB for management processing
  public static final int     Q_CHARGEBACKS_EXCEPTION         = 1789; //exception queue for merchants otherwise auto processed
  public static final int     Q_CHARGEBACKS_DISCOVER          = 1790; // discover incoming

  // worked chargebacks.  because these are very large
  // queues the software does not load items by default
  public static final int[] Q_WORKED_CHARGEBACKS =
  {
    Q_CHARGEBACKS_FULL_PMT,
    Q_CHARGEBACKS_PARTIAL_PMT,
    Q_CHARGEBACKS_MCHB,
    Q_CHARGEBACKS_REPR,
    Q_CHARGEBACKS_REMC,
    Q_CHARGEBACKS_MCLS,
    Q_CHARGEBACKS_LOSS,
    Q_CHARGEBACKS_COMPLETED,
    Q_CHARGEBACKS_COMPLETED_REVERSAL
  };
  public static final boolean isWorkedChargebackQueue(int qtype)
  {
    boolean     retVal    = false;

    for ( int i = 0; i < Q_WORKED_CHARGEBACKS.length; ++i )
    {
      if ( qtype == Q_WORKED_CHARGEBACKS[i] )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }

  // debit exceptions
  public static final int     Q_DEBIT_EXCEPTION_ALL               = 1800;
  public static final int     Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED  = 1801;
  public static final int     Q_DEBIT_EXCEPTION_MES_NOT_FUNDED    = 1802;
  public static final int     Q_DEBIT_EXCEPTION_MERCHANT_DEBIT    = 1811;
  public static final int     Q_DEBIT_EXCEPTION_MERCHANT_CREDIT   = 1812;
  public static final int     Q_DEBIT_EXCEPTION_CH_DEBIT          = 1813;
  public static final int     Q_DEBIT_EXCEPTION_CH_CREDIT         = 1814;
  public static final int     Q_DEBIT_EXCEPTION_HELD              = 1815;
  public static final int     Q_DEBIT_EXCEPTION_LOSS              = 1816;
  public static final int     Q_DEBIT_EXCEPTION_NO_FINANCIAL_ADJ  = 1818;

  // Calltag queues
  public static final int     Q_CALLTAG__START                = 2001;
  public static final int     Q_CALLTAG_INCOMPLETE            = Q_CALLTAG__START;
  public static final int     Q_CALLTAG_COMPLETE              = 2002;
  public static final int     Q_CALLTAG_CANCELLED             = 2003;
  public static final int     Q_CALLTAG_TERMINATED            = 2004; // i.e. Equipment not received
  public static final int     Q_CALLTAG__END                  = 2099;

  // NON-RETURNABLE QUEUES
  public static final int     Q_NON_RETURNABLE_NEW            = 1901;

  //cbt mms queues
  public static final int     Q_CBT_MMS_NEW                   = 2200;
  public static final int     Q_CBT_MMS_PEND                  = 2201;
  public static final int     Q_CBT_MMS_COMPLETE              = 2202;
  public static final int     Q_CBT_MMS_EXCEPTION             = 2203;
  public static final int     Q_CBT_MMS_EXCEPTION_COMPLETE    = 2204;

  // mbs remittance queues
  public static final int     Q_MBS_REMITTANCE_PENDING        = 2300;
  public static final int     Q_MBS_REMITTANCE_PAID           = 2301;
  public static final int     Q_MBS_REMITTANCE_COLLECTION     = 2302;

  //bbt qa queues
  public static final int     Q_BBT_QA_SETUP                  = 2401; // i.e. qa
  public static final int     Q_BBT_QA_PENDING                = 2402;
  public static final int     Q_BBT_QA_CANCELLED              = 2403;
  public static final int     Q_BBT_QA_COMPLETED              = 2404; // i.e. approved
  public static final int     Q_BBT_QA_DECLINED               = 2405;
  public static final int     Q_BBT_QA_PREP                   = 2406;
  public static final int     Q_BBT_QA_DATAENTRY              = 2407; // i.e. pre-qa

  // cbt app review queues, version 3 ("tiering")
  public static final int     Q_CBT_TIER_SCORING_AUTO         = 2600;
  public static final int     Q_CBT_TIER_SCORING_COMPLETED    = 2601;
  public static final int     Q_CBT_TIER_SCORING_CANCELED     = 2602;

  public static final int     Q_CBT_TIER_MCC_ASSIGN_NEW       = 2620;
  public static final int     Q_CBT_TIER_MCC_ASSIGN_COMPLETED = 2621;
  public static final int     Q_CBT_TIER_MCC_ASSIGN_CANCELED  = 2622;

  public static final int     Q_CBT_TIER_NUM_ASSIGN_NEW       = 2640;
  public static final int     Q_CBT_TIER_NUM_ASSIGN_COMPLETED = 2641;
  public static final int     Q_CBT_TIER_NUM_ASSIGN_CANCELED  = 2642;

  public static final int     Q_CBT_TIER_34_REVIEW_NEW        = 2660;
  public static final int     Q_CBT_TIER_34_REVIEW_APPROVED   = 2661;
  public static final int     Q_CBT_TIER_34_REVIEW_DECLINED   = 2662;
  public static final int     Q_CBT_TIER_34_REVIEW_CANCELED   = 2663;
  public static final int     Q_CBT_TIER_34_REVIEW_PENDED     = 2664;

  // (high risk)
  public static final int     Q_CBT_TIER_34_HIGH_VOLUME_NEW      = 2690;
  public static final int     Q_CBT_TIER_34_HIGH_VOLUME_APPROVED = 2691;
  public static final int     Q_CBT_TIER_34_HIGH_VOLUME_DECLINED = 2692;
  public static final int     Q_CBT_TIER_34_HIGH_VOLUME_CANCELED = 2693;
  public static final int     Q_CBT_TIER_34_HIGH_VOLUME_PENDED   = 2694;

  public static final int     Q_CBT_TIER_HIGH_VOLUME_NEW      = 2680;
  public static final int     Q_CBT_TIER_HIGH_VOLUME_APPROVED = 2681;
  public static final int     Q_CBT_TIER_HIGH_VOLUME_DECLINED = 2682;
  public static final int     Q_CBT_TIER_HIGH_VOLUME_CANCELED = 2683;
  public static final int     Q_CBT_TIER_HIGH_VOLUME_PENDED   = 2684;

  public static final int     Q_CBT_TIER_LOW_SCORE_NEW        = 2700;
  public static final int     Q_CBT_TIER_LOW_SCORE_APPROVED   = 2701;
  public static final int     Q_CBT_TIER_LOW_SCORE_DECLINED   = 2702;
  public static final int     Q_CBT_TIER_LOW_SCORE_CANCELED   = 2703;
  public static final int     Q_CBT_TIER_LOW_SCORE_PENDED     = 2704;

  public static final int     Q_CBT_TIER_LARGE_TKT_NEW        = 2720;
  public static final int     Q_CBT_TIER_LARGE_TKT_APPROVED   = 2721;
  public static final int     Q_CBT_TIER_LARGE_TKT_DECLINED   = 2722;
  public static final int     Q_CBT_TIER_LARGE_TKT_CANCELED   = 2723;
  public static final int     Q_CBT_TIER_LARGE_TKT_PENDED     = 2724;

  public static final int     Q_CBT_TIER_DOC_NEW              = 2740;
  public static final int     Q_CBT_TIER_DOC_COMPLETED        = 2741;
  public static final int     Q_CBT_TIER_DOC_CANCELED         = 2742;
  public static final int     Q_CBT_TIER_DOC_PENDED           = 2743;

  public static final int     Q_CBT_TIER_FINANCIAL_NEW        = 2760;
  public static final int     Q_CBT_TIER_FINANCIAL_APPROVED   = 2761;
  public static final int     Q_CBT_TIER_FINANCIAL_DECLINED   = 2762;
  public static final int     Q_CBT_TIER_FINANCIAL_CANCELED   = 2763;
  public static final int     Q_CBT_TIER_FINANCIAL_PENDED     = 2764;


  // Client Review Queue
  public static final int     Q_CLIENT_REVIEW_NEW             = 3001;
  public static final int     Q_CLIENT_REVIEW_PEND            = 3002;
  public static final int     Q_CLIENT_REVIEW_DECLINE         = 3003;
  public static final int     Q_CLIENT_REVIEW_APPROVED        = 3004;
  public static final int     Q_CLIENT_REVIEW_CANCELED        = 3005;


  // owners
  public static final int     Q_OWNER_MES                     = 1;

  // actions
  public static final int     Q_ACTION_UNLOCK                 = 1;
  public static final int     Q_ACTION_MOVE                   = 2;

  // queue groups
  public static final int     QG_CBT_CREDIT_QUEUES            = 1;
  public static final int     QG_CBT_MILLION_DOLLAR_QUEUES    = 2;
  public static final int     QG_CBT_DOC_QUEUES               = 3;
  public static final int     QG_ACH_REJECT_QUEUES            = 4;
  public static final int     QG_OLD_ACR_QUEUES               = 5;
  public static final int     QG_NEW_ACR_QUEUES               = 6;
  public static final int     QG_CHARGEBACK_QUEUES            = 7;
  public static final int     QG_RETRIEVAL_QUEUES             = 8;
  public static final int     QG_CALLTAG_QUEUES               = 9;
  public static final int     QG_BBT_QA_QUEUES                = 10;
  public static final int     QG_BBT_PRE_QA_QUEUES            = 11;
  public static final int     QG_MMS_QUEUES                   = 12;
  public static final int     QG_DEBIT_EXCEPTION_QUEUES       = 13;
  public static final int     QG_PCI_QUEUES                   = 14;
  public static final int     QG_MBS_REMITTANCE_QUEUES        = 15;
  public static final int     QG_ALL_APP_QUEUES               = 100;

  public static String getItemTypeString(int ItemType)
  {
    String ItemTypeString = "Other";

    switch(ItemType)
    {
      case Q_ITEM_TYPE_BBT_QA:
        ItemTypeString = "BB&T QA";
        break;

      case Q_ITEM_TYPE_ACCT_CHANGE:
        ItemTypeString = "Account Change";
        break;

      case Q_ITEM_TYPE_ACTIVATE:
        ItemTypeString = "Activation";
        break;

      case Q_ITEM_TYPE_PROGRAMMING:
        ItemTypeString = "Programming";
        break;

      case Q_ITEM_TYPE_MMS:
        ItemTypeString = "MMS";
        break;

      case Q_ITEM_TYPE_CBT_MMS:
        ItemTypeString = "CB&T MMS";
        break;

      case Q_ITEM_TYPE_MMS_ERROR:
        ItemTypeString = "MMS Error";
        break;

      case Q_ITEM_TYPE_CBT_CREDIT:
        ItemTypeString = "CBT Credit";
        break;

      case Q_ITEM_TYPE_MES_CREDIT:
        ItemTypeString = "MES Credit";
        break;

      case Q_ITEM_TYPE_MES_MATCHCREDIT:
        ItemTypeString = "Match";
        break;

      case Q_ITEM_TYPE_CHANGE_REQUEST:
        ItemTypeString = "ACR";
        break;

      case Q_ITEM_TYPE_ACH_REJECTS:
        ItemTypeString = "ACH Rejects";
        break;

      case Q_ITEM_TYPE_ACTIVATION:
        ItemTypeString = "Activation";
        break;

      case Q_ITEM_TYPE_DEPLOYMENT:
        ItemTypeString = "Deployment";
        break;

      case Q_ITEM_TYPE_RISK:
        ItemTypeString = "Risk";
        break;

      case Q_ITEM_TYPE_COLLECTIONS:
        ItemTypeString = "Collections";
        break;

      case Q_ITEM_TYPE_LEGAL:
        ItemTypeString = "Legal";
        break;

      case Q_ITEM_TYPE_RETRIEVAL:
        ItemTypeString = "Retrievals";
        break;

      case Q_ITEM_TYPE_CALLTAG:
        ItemTypeString = "Call Tag";
        break;

      case Q_ITEM_TYPE_NON_RETURNABLE:
        ItemTypeString = "Non-Returnable";
        break;

      case Q_ITEM_TYPE_PCI_EXCEPTIONS:
        ItemTypeString = "PCI Exception";
        break;

      case Q_ITEM_TYPE_PACKAGE_REJECTS:
        ItemTypeString = "Rejects";
        break;
    }

    return ItemTypeString;
  }

  // queue document item types

  // cbt doc items
  public static final int QDI_MERCHANT_AGREEMENT          = 100;
  public static final int QDI_VMC_VOLUME_TICKET           = 101;
  public static final int QDI_ONSITE_INSPECTION           = 102;
  public static final int QDI_PRIVACY_POLICY              = 103;
  public static final int QDI_PRIOR_ACTIVITY              = 104;
  public static final int QDI_WEBSITE_COPY                = 105;
  public static final int QDI_CREDIT_REPORT               = 106;
  public static final int QDI_PERSONAL_GUARANTY           = 107;
  public static final int QDI_LLC_PARTNERSHIP_RESOLUTION  = 108;
  public static final int QDI_3_YEARS_FINANCIALS          = 109;
  public static final int QDI_CORPORATE_RESOLUTION        = 110;
  public static final int QDI_2_YEARS_FINANCIALS          = 111;
  public static final int Q_CHARGEBACKS_DISCOVER_PRE_ARB  = 1773;
}
