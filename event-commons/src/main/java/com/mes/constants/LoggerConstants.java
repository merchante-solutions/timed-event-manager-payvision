package com.mes.constants;

/**
 * 
 * @author ssubbiah
 * @desc	This class defines the constants used for logging Messages
 *
 */
public class LoggerConstants {
	public static  final String FIELD_SEPARATOR=" ";
	public static  final String FIELD_ASSIGNER="=";
	
	//Base logging Fields
	public static final String LOGGER_PID					= "evtPID";  	// Unique Id for each event run
				
	//TE Base Logging Fields
	public static final String LOGGER_TEM_ID 				= "evtId"; 		// TimeEventManager ID in table	
	public static final String LOGGER_TEM_EVENTNAME			= "evtName";	// Name of timed event, refers to DevName in TimedEvents table
	public static final String LOGGER_TEM_FREQUENCY			= "evtFrq";		// Frequency of event {1-<daily, 2-daily/weekly, 3-Monthly/Qtr}
	public static final String LOGGER_TEM_MTYPE				= "mType";		
	public static final String LOGGER_TEM_PARAMS			= "param";		// Event arguments	
	
	// TE activity
	public static final String LOGGER_TEM_ACT				= "act";		// Activity	
	public static final String LOGGER_TEM_BEGIN				= "beg";		// Event Begin 
	public static final String LOGGER_TEM_END				= "end";		// Event End 
	public static final String LOGGER_TEM_ERROR				= "error";		// Event error
	public static final String LOGGER_TEM_MSG				= "msg";		// Event error
	
	public static final String LOGGER_TIME_ELAPSED 			= "eTime";		// Elapsed Time
	
	// TE Log Level
	public static final String LOGGER_TEM_lOGLEVEL_INFO		= "INFO";		// Log Level Info
	public static final String LOGGER_TEM_lOGLEVEL_ERROR	= "ERROR";		// Log Level Error

}
