/*@lineinfo:filename=EquipProfileGeneratorDictator*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment.profile;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;

/**
 * EquipProfileGeneratorDictator
 *
 * Determines the equipment "profile generator" based on certain criteria in two major ways:
 *  1. On the equipment level based on the innate equipment attributes.
 *  2. On the application level based on making assumptions about multiple equipment 
 *      items that may themselves have different profile generators associated with them.
 */
public final class EquipProfileGeneratorDictator extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(EquipProfileGeneratorDictator.class.getName());

  // constants
  // (NONE)
  
  // class methods
  
  public static String getEquipmentProfileGeneratorName(int code)
  {
    return (new EquipProfileGeneratorDictator())._getEquipmentProfileGeneratorName(code);
  }
  

  // object methods
  
  // construction
  public EquipProfileGeneratorDictator()
  {
  }

  public String _getEquipmentProfileGeneratorName(int code)
  {
    String name = "";

    try {

      connect();
            
      /*@lineinfo:generated-code*//*@lineinfo:51^7*/

//  ************************************************************
//  #sql [Ctx] { select 	  name
//          
//          from      EQUIP_PROFILE_GENERATORS
//          where     code = :code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t  name\n         \n        from      EQUIP_PROFILE_GENERATORS\n        where     code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.profile.EquipProfileGeneratorDictator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,code);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   name = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:57^7*/

    }
    catch(Exception e) {
      log.error("_getEquipmentProfileGeneratorName() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      cleanUp();
    }

    return name;
  }

  /**
   * getAppLevelProfileGeneratorCode()
   * 
   * Returns the application level profile generator.
   * NOTE: This method is kludgey since the app is not directly associated with an equipment profile generator.
   */
  public int getAppLevelProfileGeneratorCode(long appSeqNum)
  {
    int appLevelProfGen = mesConstants.DEFAULT_PROFGEN;

    // VC/TM specific criteria
    if((appHasEquipOfProfileGenerator(appSeqNum,mesConstants.PROFGEN_VERICENTRE) || appHasEquipOfProfileGenerator(appSeqNum,mesConstants.PROFGEN_TERMMASTER))
        && !appHasEquipOfProfileGenerator(appSeqNum,mesConstants.PROFGEN_MMS)) {
      
      try {

        connect();

        int count = 0;

        //log.debug("app found to have exclusive vc/tm equipment!  Testing vc/tm specific criteria...");
      
        /*@lineinfo:generated-code*//*@lineinfo:92^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//            
//            from	  merchant      m
//                    ,application  a
//                    ,app_type	    at
//                    ,users        u
//                    ,t_hierarchy  th
//            where	  m.app_seq_num = a.app_seq_num
//                    and a.app_type = at.app_type_code
//                    and a.app_user_login = u.login_name
//                    and u.hierarchy_node = th.ancestor
//                    and th.descendent = 394100000         
//                    and at.app_type_code <> 8
//                    and (m.MULTI_MERCH_MASTER_CNTRL is null)
//                    and m.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n           \n          from\t  merchant      m\n                  ,application  a\n                  ,app_type\t    at\n                  ,users        u\n                  ,t_hierarchy  th\n          where\t  m.app_seq_num = a.app_seq_num\n                  and a.app_type = at.app_type_code\n                  and a.app_user_login = u.login_name\n                  and u.hierarchy_node = th.ancestor\n                  and th.descendent = 394100000         \n                  and at.app_type_code <> 8\n                  and (m.MULTI_MERCH_MASTER_CNTRL is null)\n                  and m.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.profile.EquipProfileGeneratorDictator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^9*/

        if(count > 0)
          appLevelProfGen = mesConstants.PROFGEN_VCTM;
    
      }
      catch(Exception e) {
        log.error("getAppLevelProfileGeneratorCode() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        cleanUp();
      }

    }

    log.debug("getAppLevelProfileGeneratorCode() appLevelProfGen="+appLevelProfGen);
    return appLevelProfGen;
  }

  /**
   * appHasEquipOfProfileGenerator()
   * 
   * Test to see if a given app has one or more pieces of equipment associated with
   * the specified profile generator [code].
   * 
   * NOTE: if "indeterminate", the query comes back with the default profile generator as the comparator.
   */
  public boolean appHasEquipOfProfileGenerator(long appSeqNum, int profGenCode)
  {
    boolean rval = false;

    try {

      connect();
      
      int count = 0;
      int dpg = mesConstants.DEFAULT_PROFGEN;
      
      /*@lineinfo:generated-code*//*@lineinfo:147^7*/

//  ************************************************************
//  #sql [Ctx] { select	count(*)
//          
//          from	  merchequipment me
//  		            ,equip_prof_gen_bindings eb
//          where	  me.app_seq_num = :appSeqNum
//                  and me.EQUIP_MODEL=eb.EQUIP_MODEL(+) and me.PROD_OPTION_ID=eb.PROD_OPTION_ID(+)
//  		            and nvl(eb.profile_generator_code,:dpg) = :profGenCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\tcount(*)\n         \n        from\t  merchequipment me\n\t\t            ,equip_prof_gen_bindings eb\n        where\t  me.app_seq_num =  :1 \n                and me.EQUIP_MODEL=eb.EQUIP_MODEL(+) and me.PROD_OPTION_ID=eb.PROD_OPTION_ID(+)\n\t\t            and nvl(eb.profile_generator_code, :2 ) =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.equipment.profile.EquipProfileGeneratorDictator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,dpg);
   __sJT_st.setInt(3,profGenCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:156^7*/

      rval = (count > 0);
      log.debug("appHasEquipOfProfileGenerator(appSeqNum: "+appSeqNum+", profGenCode: "+profGenCode+")="+rval);
    }
    catch(Exception e) {
      log.error("appHasEquipOfProfileGenerator() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      cleanUp();
    }

    return rval;
  }

  /**
   * getEquipmentProfileGeneratorCode()
   * 
   * Returns the profile generator for a specified equipment item
   * based soley on the equipment attributes and the product option id. (mesdb.PRODOPTION)
   */
  public int getEquipmentProfileGeneratorCode(String equipModel, int prodOptionId)
  {
    int pgc = mesConstants.PROFGEN_UNDEFINED;

    try {

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:185^7*/

//  ************************************************************
//  #sql [Ctx] { select 	  profile_generator_code
//          
//          from      equip_prof_gen_bindings
//          where     equip_model = :equipModel
//                    and prod_option_id = :prodOptionId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t  profile_generator_code\n         \n        from      equip_prof_gen_bindings\n        where     equip_model =  :1 \n                  and prod_option_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.equipment.profile.EquipProfileGeneratorDictator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   __sJT_st.setInt(2,prodOptionId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pgc = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^7*/

    }
    catch(Exception e) {
      pgc = mesConstants.DEFAULT_PROFGEN;
    }
    finally {
      cleanUp();
    }

    return pgc;
  }

}/*@lineinfo:generated-code*/