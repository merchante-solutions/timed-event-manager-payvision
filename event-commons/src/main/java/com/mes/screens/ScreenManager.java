/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/screens/ScreenManager.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/05/01 4:57p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.screens;

import java.io.Serializable;
import java.util.Vector;

public class ScreenManager
  implements Serializable
{
  private Vector    screens     = new Vector();
  
  /*
  ** METHOD clearScreens
  */
  public void clear()
  {
    screens   = new Vector();
  }
  
  /*
  ** METHOD addScreen
  */
  public void addScreen(String className,
                        long id,
                        String jsp,
                        boolean onlyOnce,
                        String desc)
  {
    screens.add(new ScreenInfo(className, id, jsp, onlyOnce, desc));
  }
  
  /*
  ** METHOD screenAt
  */
  public ScreenInfo screenAt(int idx)
  {
    return (ScreenInfo)screens.elementAt(idx);
  }
  
  /*
  ** METHOD size
  */
  public int size()
  {
    return screens.size();
  }
  
  /*
  ** METHOD getIndex
  */
  public int getIndex(long findId)
  {
    for (int i=0; i < screens.size(); ++i)
    {
      if (screenAt(i).getScreenId() == findId)
      {
        return i;
      }
    }
    
    return 0;
  }
  
  /*
  ** METHOD hasPreviousPage
  */
  public boolean hasPreviousPage(long fromScreenId)
  {
    boolean result = false;
    
    try
    {
      int fromIdx   = getIndex(fromScreenId);
      
      if(fromIdx > 0 && fromIdx < size())
      {
        ScreenInfo prevScreen;
        int prevIdx = fromIdx;
        
        while(prevIdx > 0)
        {
          --prevIdx;
          prevScreen = screenAt(prevIdx);
          if(prevScreen.isVisitable())
          { 
            result = true;
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "hasPreviousPage: " + e.toString());
    }
    
    return result;
  }
  
  /*
  ** METHOD getPreviousPage
  */
  public String getPreviousPage(long fromScreenId)
  {
    String result = "";
    
    try
    {
      int fromIdx = getIndex(fromScreenId);
      
      if(fromIdx > 0 && fromIdx < size())
      {
        ScreenInfo prevScreen;
        int prevIdx = fromIdx;
        while(prevIdx > 0)
        {
          --prevIdx;
          prevScreen = screenAt(prevIdx);
          if(prevScreen.isVisitable())
          {
            result = prevScreen.getJspName();
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPreviousPage: " + e.toString());
    }
    
    return result;
  }
  
  /*
  ** METHOD hasNextPage
  */
  public boolean hasNextPage(long fromScreenId, boolean ignoreComplete)
  {
    boolean result = false;
    
    try
    {
      int fromIdx = getIndex(fromScreenId);
      
      if(fromIdx >= 0 && fromIdx < size() - 1)
      {
        ScreenInfo nextScreen = screenAt(fromIdx);
        if(ignoreComplete || nextScreen.getCompleted())
        {
          int nextIdx = fromIdx;
          while(nextIdx < size() -1)
          {
            ++nextIdx;
            nextScreen = screenAt(nextIdx);
            if(nextScreen.isVisitable())
            {
              result = true;
              break;
            }
          }
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "hasNextPage: " + e.toString());
    }
    
    return result;
  }
  public boolean hasNextPage(long fromScreenId)
  {
    return hasNextPage(fromScreenId, false);
  }
  
  /*
  ** METHOD getNextPage
  */
  public String getNextPage(long fromScreenId)
  {
    String result = "";
    
    int fromIdx = getIndex(fromScreenId);
    if(fromIdx >= 0 && fromIdx < size() - 1)
    {
      ScreenInfo nextScreen;
      int nextIdx = fromIdx;
      while(nextIdx < size())
      {
        ++nextIdx;
        nextScreen = screenAt(nextIdx);
        if(nextScreen.isVisitable())
        {
          result = nextScreen.getJspName();
          break;
        }
      }
    }
    
    return result;
  }
  
  /*
  ** METHOD getPageDescription
  */
  public String getPageDescription(long screenId)
  {
    String result = "";
    
    try
    {
      int screenIdx = getIndex(screenId);
      if(screenIdx >= 0 && screenIdx < size())
      {
        result = screenAt(screenIdx).getScreenDescription();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPageDescription: " + e.toString());
    }
    
    return result;
  }

  /*
  ** METHOD getBeanClass
  */
  public String getBeanClassName(long screenId)
  {
    String result = "";
    
    try
    {
      int screenIdx = getIndex(screenId);
      if(screenIdx >= 0 && screenIdx < size())
      {
        result = screenAt(screenIdx).getBeanClassName();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBeanClass: " + e.toString());
    }
    
    return result;
  }
  
  /*
  ** METHOD getFirstPage
  */
  public String getFirstPage()
  {
    ScreenInfo      screen      = screenAt(getIndex(0));
    
    return(screen.getJspName());
  }
 
  /*
  ** METHOD getPage
  */
  public String getPage(long screenId)
  {
    ScreenInfo      screen      = screenAt(getIndex(screenId));
    
    return(screen.getJspName());
  }
  
  /*
  ** METHOD getScreens
  */
  public Vector getScreens()
  {
    return this.screens;
  }
}
