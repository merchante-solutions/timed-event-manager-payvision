/*@lineinfo:filename=SequenceDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/screens/SequenceDataBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/21/03 4:08p $
  Version            : $Revision: 31 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.screens;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.database.SQLJConnectionBase;

public class SequenceDataBean extends SQLJConnectionBase
{
  // constants
  private static final int      APP_NOT_FINISHED  = 0;
  
  // common data items
  private boolean               submit            = false;
  
  // utility variables
  private Vector                errors            = new Vector();
  
  protected final void init()
  {
  }
  
  /*
  ** CONSTRUCTOR
  */  
  public SequenceDataBean()
  {
    init();
  }
  public SequenceDataBean(String connectString)
  {
    super(connectString);

    init();
  }
  
  /*
  ** FUNCTION getPreparedStatement
  */
  public PreparedStatement getPreparedStatement(String query)
  {
    PreparedStatement   ps = null;
    try
    {
      if(con == null || isConnectionStale())
      {
        connect();
      }
      
      if(con != null)
      {
        ps = con.prepareStatement(query);
      }
      else
      {
        logEntry("getPreparedStatement() 1:", "connection is null");
      }
    }
    catch(Exception e)
    {
      logEntry("getPreparedStatement() 2: ", e.toString());
    }
    
    return ps;
  }
  
  /*
  ** FUNCTION canShowSubmit
  **
  ** returns true if the user should see a "submit" button on the bottom of
  ** the application form.  This function should return true if at least one
  ** of the following are true:
  **
  ** 1. the application is not complete (merchant.merch_credit_status)
  ** 2. the user has "EDIT_APPLICATION" rights and the account has not activated
  */
  public boolean canShowSubmit(long primaryKey, boolean canEditApp)
  {
    boolean           result        = false;
    int               creditStatus  = 0;
    boolean           wasStale      = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status 
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.screens.SequenceDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      if(creditStatus == APP_NOT_FINISHED || canEditApp)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      result = true;
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return result;
  }
  
  /*
  ** FUNCTION getErrors
  */
  public Vector getErrors()
  {
    return this.errors;
  }
  
  /*
  ** FUNCTION addError
  */
  protected void addError(String error)
  {
    try
    {
      errors.add(error);
    }
    catch(Exception e)
    {
      logEntry("addError()", e.toString());
    }
  }
  
  /*
  ** FUNCTION hasErrors
  */
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  
  /*
  ** FUNCTION display
  */
  public void display(HttpServletResponse response)
  {
    try
    {
      PrintWriter out = response.getWriter();
      out.print(displayData());
    }
    catch (Exception e)
    {
      logEntry("display()", e.toString());
    }
  }
  
  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION getData
  */
  public void getData(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION submitData
  */
  public void submitData(HttpServletRequest req, long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate(HttpServletRequest req)
  {
    return(true);
  }
  
  /*
  ** FUNCTION updateData
  */
  public void updateData(HttpServletRequest req)
  {
  }

  /*
  ** FUNCTION updateData
  */
  public void updateData(long appSeqNum)
  {
  }

  
  /*
  ** FUNCTION isRecalculated
  */
  public boolean isRecalculated()
  {
    return false;
  }

  /*
  ** FUNCTION allowSubmit
  */
  public boolean allowSubmit()
  {
    return false;
  }


  /*
  ** FUNCTION isCancelled
  */
  public boolean isCancelled()
  {
    return false;
  }
  
  /*
  ** FUNCTION setDefaults
  */
  public void setDefaults(String appSeqNum)
  {
    try
    {
      setDefaults(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("setDefaults(" + appSeqNum + ")", e.toString());
    }
  }
  public void setDefaults(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate()
  {
    return(true);
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate(long primaryKey)
  {
    return(true);
  }
  
  /*
  ** FUNCTION displayData
  */
  public String displayData()
  {
    return "displayData() undefined";
  }
  
  /*
  ** FUNCTION canContinue
  */
  public boolean canContinue()
  {
    return true;
  }
  
  /*
  ** FUNCTION loadTrackingData
  ** 
  ** used to put data in the sequenceIdBean so it will be tracked
  */
  public void loadTrackingData(long primaryKey, SequenceIdBean sib)
  {
  }
  
  /***************************************************************************
  * Helper functions
  ***************************************************************************/
  public static boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }
  
  public boolean isRealNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      double i = Double.parseDouble(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }


  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 && 
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }
  
  public String getDigitsStr(String raw)
  {
    String        result      = "";
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }


  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    
    return result;
  }
  
  /*
  ** METHOD isValidPhone
  */
  public boolean isValidPhone(String phone)
  {
    // check for valid number of digits
    if (countDigits(phone) != 10)
    {
      return false;
    }
    
    // scan for invalid chars
    for (int i = 0; i < phone.length(); ++i)
    {
      if (!Character.isDigit(phone.charAt(i)) &&
          phone.charAt(i) != '(' &&
          phone.charAt(i) != ')' &&
          phone.charAt(i) != ' ' &&
          phone.charAt(i) != '-')
      {
        return false;
      }
    }
      
    // make sure translated value is valid
    if (getDigits(phone) < 1000000000L)
    {
      return false;
    }
    
    return true;
  }
  
  /*
  ** METHOD is isValidTaxId
  */
  public boolean isValidTaxId(String taxId)
  {
    // check for valid number of digits
    if (countDigits(taxId) != 9)
    {
      return false;
    }
    
    // scan for invalid chars
    for (int i = 0; i < taxId.length(); ++i)
    {
      if (!Character.isDigit(taxId.charAt(i)) &&
          taxId.charAt(i) != ' ' &&
          taxId.charAt(i) != '-')
      {
        return false;
      }
    }
      
    return true;
  }
  
  /*
  ** METHOD parseLong
  */
  public long parseLong(String val)
  {
    return getDigits(val);
  }

  /*
  ** METHOD parseDouble
  */
  public double parseDouble(String raw)
  {
    double        result      = 0.0;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Double.parseDouble(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && ! raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "parseDouble('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }
  
  /*
  ** METHOD parseInt
  */
  public int parseInt(String raw)
  {
    int           result      = 0;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Integer.parseInt(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && ! raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "parseInt('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }
  
  public boolean isSubmitted()
  {
    return this.submit;
  }
  
  /***************************************************************************
  * Getters and Setters
  ***************************************************************************/
  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean getSubmit()
  {
    return this.submit;
  }
}/*@lineinfo:generated-code*/