/*@lineinfo:filename=SequenceIdBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/screens/SequenceIdBean.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-10-08 10:25:00 -0700 (Wed, 08 Oct 2008) $
  Version            : $Revision: 15432 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.screens;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;

public class SequenceIdBean extends com.mes.database.SQLJConnectionBase
  implements Serializable
{
  public static final String  NULL_PAGE           = "/jsp/screens/nullscreen.jsp";

  // data members
  private long                sequenceId          = 0L;
  private long                primaryKey          = -1L;
  private long                completedScreens    = 0L;

  private String              validateUser        = "";
  private String              enforceOrder        = "";
  private String              sequenceDescription = "";
  private String              sequenceHeader      = "";
  private String              sequenceNavigate    = "";
  private String              sequenceAfterLoad   = "";
  private String              sequenceAfterSubmit = "";
  private String              sequenceErrors      = "";
  private String              sequenceFooter      = "";
  public  ScreenManager       screens             = new ScreenManager();

  private String              byPassPage          = "";

  /*
  ** METHOD init
  */
  public synchronized void init()
  {
    // reset values
    sequenceId        = 0L;
    primaryKey        = -1L;
    completedScreens  = 0L;

    validateUser        = "";
    enforceOrder        = "";
    sequenceDescription = "";
    sequenceHeader      = "";
    sequenceNavigate    = "";
    sequenceAfterLoad   = "";
    sequenceAfterSubmit = "";
    sequenceErrors      = "";
    sequenceFooter      = "";
    byPassPage          = "";
    // reset screens
    screens.clear();
  }

  /*
  ** CONSTRUCTOR SequenceIdBean
  */
  public SequenceIdBean()
  {
    init();
  }

  public static String extendedIdBeanName(long sequenceId)
  {
    String result = "";
    if(sequenceId == com.mes.constants.mesConstants.SEQUENCE_APPLICATION)
    {
      result = "com.mes.screens.AppIdBean";
    }
 
    return result;
  }
  
  /*
  ** METHOD getDataBeans
  */
  private Vector getDataBeans()
  {
    ScreenInfo    screen;
    Vector        beans   = new Vector();

    try
    {
      for(int i=0; i < screens.size(); ++i)
      {
        screen = screens.screenAt(i);
        if(screen.getBeanClass() != null)
        {
          beans.add(screen.newBeanInstance());
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDataBeans: " + e.toString());
    }

    return beans;
  }
  
  /*
  ** METHOD loadSequenceInfo
  */
  public synchronized void loadSequenceInfo(long sequenceId)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:154^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sequence_desc,
//                  sequence_header,
//                  sequence_navigate,
//                  sequence_after_load,
//                  sequence_after_submit,
//                  sequence_errors,
//                  sequence_footer,
//                  sequence_enforce_order,
//                  sequence_validate_user
//          from    screen_sequence_info
//          where   screen_sequence_id = :sequenceId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sequence_desc,\n                sequence_header,\n                sequence_navigate,\n                sequence_after_load,\n                sequence_after_submit,\n                sequence_errors,\n                sequence_footer,\n                sequence_enforce_order,\n                sequence_validate_user\n        from    screen_sequence_info\n        where   screen_sequence_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.screens.SequenceIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.screens.SequenceIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        sequenceDescription = rs.getString("sequence_desc");
        sequenceHeader      = rs.getString("sequence_header");
        sequenceNavigate    = rs.getString("sequence_navigate");
        sequenceAfterLoad   = rs.getString("sequence_after_load");
        sequenceAfterSubmit = rs.getString("sequence_after_submit");
        sequenceErrors      = rs.getString("sequence_errors");
        sequenceFooter      = rs.getString("sequence_footer");
        enforceOrder        = rs.getString("sequence_enforce_order");
        validateUser        = rs.getString("sequence_validate_user");
      }
      
      rs.close();
      it.close();
      
      // check for invalid entries
      sequenceHeader      = isBlank(sequenceHeader)       ? NULL_PAGE : sequenceHeader;
      sequenceNavigate    = isBlank(sequenceNavigate)     ? NULL_PAGE : sequenceNavigate;
      sequenceAfterLoad   = isBlank(sequenceAfterLoad)    ? NULL_PAGE : sequenceAfterLoad;
      sequenceAfterSubmit = isBlank(sequenceAfterSubmit)  ? NULL_PAGE : sequenceAfterSubmit;
      sequenceErrors      = isBlank(sequenceErrors)       ? NULL_PAGE : sequenceErrors;
      sequenceFooter      = isBlank(sequenceFooter)       ? NULL_PAGE : sequenceFooter;
      enforceOrder        = isBlank(enforceOrder)         ? "N" : enforceOrder;
      validateUser        = isBlank(validateUser)         ? "N" : validateUser;
    }
    catch(Exception e)
    {
      logEntry("loadSequenceInfo(" + sequenceId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD loadScreens
  */
  public synchronized void loadScreens()
  {
    ResultSet         rs          = null;
    ResultSetIterator it          = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:221^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_classname,
//                  screen_id,
//                  screen_jsp,
//                  screen_onetime,
//                  screen_description
//          from    screen_sequence
//          where   screen_sequence_id = :sequenceId
//          order by  screen_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_classname,\n                screen_id,\n                screen_jsp,\n                screen_onetime,\n                screen_description\n        from    screen_sequence\n        where   screen_sequence_id =  :1 \n        order by  screen_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.screens.SequenceIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.screens.SequenceIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:231^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        String  screenClass       = rs.getString("screen_classname");
        long    screenId          = rs.getLong  ("screen_id");
        String  screenJsp         = rs.getString("screen_jsp");
        boolean screenOneTime     = (rs.getString("screen_onetime").equals("Y")) ? true : false;
        String  screenDescription = rs.getString("screen_description");
        
        screens.addScreen(screenClass, screenId, screenJsp, screenOneTime, screenDescription);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadScreens(" + sequenceId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD loadScreenProgress
  */
  public synchronized void loadScreenProgress()
  {
    try
    {
      connect();
      
      this.completedScreens  = 0L; //reset completed screens before getting new one
      
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(screen_complete) 
//          from    screen_progress
//          where   screen_sequence_id = :sequenceId and
//                  screen_pk = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(screen_complete)  \n        from    screen_progress\n        where   screen_sequence_id =  :1  and\n                screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.screens.SequenceIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^7*/
      
      if(count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:284^9*/

//  ************************************************************
//  #sql [Ctx] { select  screen_complete 
//            from    screen_progress
//            where   screen_sequence_id = :sequenceId and
//                    screen_pk = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  screen_complete  \n          from    screen_progress\n          where   screen_sequence_id =  :1  and\n                  screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.screens.SequenceIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   completedScreens = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^9*/
        
        setCompletedScreens();
      }
    }
    catch(Exception e)
    {
      logEntry("loadScreenProgress()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public synchronized void setCompletedScreens()
  {
    // walk through each screen page and set complete if necessary
    Vector s = screens.getScreens();
    for(int i=0; i < s.size(); ++i)
    {
      ScreenInfo si = (ScreenInfo)s.elementAt(i);
      si.setCompleted((completedScreens & si.getScreenId()) == si.getScreenId());
    }
  }
  
  /*
  ** METHOD getData
  */
  public synchronized void getData(long sequenceId, long primaryKey)
  {
    try
    {
      // determine what needs to be loaded
      if(this.sequenceId != sequenceId || this.primaryKey != primaryKey)
      {
        init();
        setSequenceId(sequenceId);
        loadSequenceInfo(sequenceId);
        loadScreens();

        setPrimaryKey(primaryKey);
        loadScreenProgress();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
    }
  }

  /*
  ** METHOD submitData
  */
  public synchronized void submitData(long screenId)
  {
    try
    {
      connect();
      
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:352^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(screen_pk) 
//          from    screen_progress
//          where   screen_sequence_id  = :sequenceId and
//                  screen_pk           = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(screen_pk)  \n        from    screen_progress\n        where   screen_sequence_id  =  :1  and\n                screen_pk           =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.screens.SequenceIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^7*/

      this.completedScreens = (screenId | this.completedScreens);
      if(count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:363^9*/

//  ************************************************************
//  #sql [Ctx] { update  screen_progress
//            set     screen_complete = :completedScreens
//            where   screen_sequence_id = :sequenceId and
//                    screen_pk = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  screen_progress\n          set     screen_complete =  :1 \n          where   screen_sequence_id =  :2  and\n                  screen_pk =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.screens.SequenceIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,completedScreens);
   __sJT_st.setLong(2,sequenceId);
   __sJT_st.setLong(3,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:373^9*/

//  ************************************************************
//  #sql [Ctx] { insert into screen_progress
//            (
//              screen_complete,
//              screen_sequence_id,
//              screen_pk
//            )
//            values
//            (
//              :completedScreens,
//              :sequenceId,
//              :primaryKey
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into screen_progress\n          (\n            screen_complete,\n            screen_sequence_id,\n            screen_pk\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.screens.SequenceIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,completedScreens);
   __sJT_st.setLong(2,sequenceId);
   __sJT_st.setLong(3,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:387^9*/
      }
      
      setCompletedScreens();      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD hasPreviousPage
  */
  public synchronized boolean hasPreviousPage(long fromScreenId)
  {
    return screens.hasPreviousPage(fromScreenId);
  }

  /*
  ** METHOD getPreviousPage
  */
  public synchronized String getPreviousPage(long fromScreenId)
  {
    return screens.getPreviousPage(fromScreenId);
  }

  /*
  ** METHOD hasNextPage
  */
  public synchronized boolean hasNextPage(long fromScreenId, boolean ignoreComplete)
  {
    return screens.hasNextPage(fromScreenId, ignoreComplete);
  }

  /*
  ** METHOD getNextPage
  */
  public synchronized String getNextPage(long fromScreenId)
  {
    return screens.getNextPage(fromScreenId);
  }

  public synchronized String getNextPage(long fromScreenId, boolean cancelled)
  {
    return getNextPage(fromScreenId);
  }
  
  /*
  ** METHOD getFirstPage
  */
  public synchronized String getFirstPage()
  {
    return screens.getFirstPage();
  }

  /*
  ** METHOD getPage
  */
  public synchronized String getPage(long screenId)
  {
    return screens.getPage(screenId);
  }

  /*
  ** METHOD getPageDescription
  */
  public synchronized String getPageDescription(long screenId)
  {
    return screens.getPageDescription(screenId);
  }
  
  /*
  ** METHOD getBeanClass
  */
  public synchronized String getBeanClassName(long screenId)
  {
    return screens.getBeanClassName(screenId);
  }


  /*
  ** METHOD getScreens
  */
  public synchronized Vector getScreens()
  {
    return screens.getScreens();
  }

  
  public String getCurPage(long screenId)
  {
    return "";
  }
  
  
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public void setSequenceId(String sequenceId)
  {
    try
    {
      setSequenceId(Long.parseLong(sequenceId));
    }
    catch(Exception e)
    {
      logEntry("setSequenceId(" + sequenceId + ")", e.toString());
    }
  }
  public synchronized void setSequenceId(long sequenceId)
  {
    this.sequenceId = sequenceId;
  }
  public synchronized long getSequenceId()
  {
    return this.sequenceId;
  }
  
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      setPrimaryKey(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setPrimaryKey(" + primaryKey + ")", e.toString());
    }
  }
  public synchronized void setPrimaryKey(long primaryKey)
  {
    this.primaryKey = primaryKey;
  }
  public synchronized long getPrimaryKey()
  {
    return this.primaryKey;
  }

  public synchronized long getCompletedScreens()
  {
    return this.completedScreens;
  }
  public void setCompletedScreens(String completedScreens)
  {
    try
    {
      setCompletedScreens(Long.parseLong(completedScreens));
    }
    catch(Exception e)
    {
      logEntry("setCompletedScreens(" + completedScreens + ")", e.toString());
    }
  }
  public synchronized void setCompletedScreens(long completedScreens)
  {
    this.completedScreens = completedScreens;
    setCompletedScreens();
  }
  public synchronized void setSequenceDescription(String sequenceDescription)
  {
    this.sequenceDescription = sequenceDescription;
  }
  public synchronized String getSequenceDescription()
  {
    return this.sequenceDescription;
  }
  public synchronized void setSequenceHeader(String sequenceHeader)
  {
    this.sequenceHeader = sequenceHeader;
  }
  public synchronized String getSequenceHeader()
  {
    return this.sequenceHeader;
  }
  public synchronized void setSequenceNavigate(String sequenceNavigate)
  {
    this.sequenceNavigate = sequenceNavigate;
  }
  public synchronized String getSequenceNavigate()
  {
    return this.sequenceNavigate;
  }
  public synchronized void setSequenceAfterLoad(String sequenceAfterLoad)
  {
    this.sequenceAfterLoad = sequenceAfterLoad;
  }
  public synchronized String getSequenceAfterLoad()
  {
    return this.sequenceAfterLoad;
  }
  public synchronized void setSequenceAfterSubmit(String sequenceAfterSubmit)
  {
    this.sequenceAfterSubmit = sequenceAfterSubmit;
  }
  public synchronized String getSequenceAfterSubmit()
  {
    return this.sequenceAfterSubmit;
  }
  public synchronized void setSequenceErrors(String sequenceErrors)
  {
    this.sequenceErrors = sequenceErrors;
  }
  public synchronized String getSequenceErrors()
  {
    return this.sequenceErrors;
  }
  public synchronized void setSequenceFooter(String sequenceFooter)
  {
    this.sequenceFooter = sequenceFooter;
  }
  public synchronized String getSequenceFooter()
  {
    return this.sequenceFooter;
  }
  
  public synchronized boolean isEnforceOrder()
  {
    boolean result = false;
    if(this.enforceOrder.equals("Y"))
      result = true;
    return result;
  }
  public boolean isValidateOn(long sequenceId)
  {
    boolean result = true;
    return result;
  }
  public boolean isLoggedIn()
  {
    return false;
  }
  public boolean isSuperUser()
  {
    return false;
  }

  public synchronized boolean useByPassPage()
  {
    if(this.byPassPage != null && !this.byPassPage.equals("") && this.byPassPage.length() > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  public synchronized String getByPassPage()
  {
    return this.byPassPage;
  }

  public synchronized void setByPassPage(String byPassPage)
  {
    this.byPassPage = byPassPage;
  }

  public synchronized void resetByPassPage()
  {
    this.byPassPage = "";
  }


}/*@lineinfo:generated-code*/