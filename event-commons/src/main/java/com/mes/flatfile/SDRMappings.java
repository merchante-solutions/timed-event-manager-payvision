package com.mes.flatfile;

import java.util.HashMap;
import java.util.Map;

public class SDRMappings {
	
    public enum SDR09HotelMap {
		  
      SDR_09_FOLIO_NUMBER("purchase_id", "folio_number"),
      SDR_09_NOSHOW_INDICATOR("no_show_indicator", "no_show_indicator"),
      SDR_09_EXTRA_CHARGES("extra_charges", "extra_charges"),
      SDR_09_ARRIVAL_DATE("statement_date_begin", "arrival_date"),
      SDR_09_DEPATURE_DATE("statement_date_end", "departure_date"),
      SDR_09_PROGRAM_CODE("program_code", "program_code"),
      SDR_09_ROOM_RATE("rate_daily", "room_rate"),
      SDR_09_ROOM_TAX("total_tax", "room_tax"),
      SDR_09_PHONE_CALL_CHARGES("total_call_charges", "phone_charges"),
      SDR_09_RESTAURANT_ROOMSVC_CHARGES("food_beverage_charges", "restaurant_room_service_charges"),
      SDR_09_BAR_CHARGES("bar_charges", "mini_bar_charges"),
      SDR_09_GIFT_SHOP_CHARGES("gift_shop_charges", "gift_shop_charges"),
      SDR_09_LAUNDRY_CLEANING_AMOUNT("laundry_cleaning_amount", "laundry_charges"),
      SDR_09_OTHER_SVC_CHARGES("other_services_charges", "other_charges"),
      SDR_09_NUM_ROOM_NIGHTS("hotel_rental_days", "no_of_room_nights"),
      SDR_09_TOTAL_ROOM_TAX("total_room_tax", "room_tax"),
      SDR_09_PARKING_CHARGES("parking_charges", "valet_parking_charges"),
      SDR_09_MOVIE_CHARGES("movie_charges", "movie_charges"),
      SDR_09_BUSINESS_CTR_CHARGES("bus_ctr_charges", "businees_center_charges"),
      SDR_09_HEALTH_CLUB_CHARGES("health_club_charges", "health_club_charges"),
      SDR_09_MINI_BAR_CHARGES("mini_bar_charges", "mini_bar_charges"),
      SDR_09_PHONE_CHARGES("telephone_charges", "phone_charges"),
      SDR_09_OTHER_CHARGES("other_charges", "other_charges"),
      SDR_09_LAUNDRY_CHARGES("laundry_charges", "laundry_charges");

        private final String code;
        private final String description;

        private static final Map<String, String> MAP = new HashMap<String, String>();
        static {
            for (SDR09HotelMap s : SDR09HotelMap.values()) {
                MAP.put(s.code, s.description);
            }
        }

        private static final Map<String, String> reverseMAP = new HashMap<String, String>();
        static {
            for (SDR09HotelMap s : SDR09HotelMap.values()) {
                reverseMAP.put(s.description, s.code);
            }
        }

        private SDR09HotelMap(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public static String getSdrMappingForCdfField(String code) {
            return MAP.get(code);
        }

        public static String getCdfMappingForSdrField(String code) {
            return reverseMAP.get(code);
        }
    }

    public enum SDR06CarRentalMap {
    
      SDR_06_RENTAL_REFERENCE("rental_agreement_data","agreement_reference"),
      SDR_06_NO_SHOW("no_show_ind","no_show_indicator"),
      SDR_06_RENTAL_DATE("checkout_date","rental_date"),
      SDR_06_RENTER_NAME("renter_name_data","name"),
      SDR_06_RETURN_LOCATION_ID("return_loc_id","rental_location_id"),
      SDR_06_PROGRAM_CODE("program_code","vehicle_program_code"),
      SDR_06_RETURN_DATE("return_date","rental_return_date"),
      SDR_06_RENTAL_RATE_DAILY("daily_rental_rate","rental_rate"),
      SDR_06_ADJ_AMOUNT_IND("adj_amount_ind","adjustment_amount_indicator_code"),
      SDR_06_FUEL_CHARGES("fuel_charges","fuel_charge"),
      SDR_06_INSURANCE_IND("insurance_ind","insurance_indicator"),
      SDR_06_INSURANCE_CHARGES("insurance_charges","insurance_amount"),
      SDR_06_RENTAL_CLASS("rental_class","rental_class_id"),
      SDR_06_ONE_WAY_DROP_OFF_CHARGES("one_way_drop_charges","one_way_drop_off_charges"),
      SDR_06_TOTAL_MILES("total_miles","rental_distance"),
      SDR_06_MAX_FREE_MILES("max_free_miles","maximum_free_miles"),
      SDR_06_DAYS_RENTED("days_rented","days"),
      SDR_06_RETURN_LOC_CITY("retun_loc_city","return_city"),
      SDR_06_REG_MILEAGE_CHARGES("reg_mileage_charges","regular_mile_charge"),
      SDR_06_EXTRA_MILEAGE_CHARGES("extra_mileage_charges","extra_mile_charge"),
      SDR_06_TELEPHONE_CHARGES("telephone_charges","phome_charge"),
      SDR_06_OTHER_CHARGES("other_charges","other_charges"),
      SDR_06_CROP_ID("corp_id","corparate_client_code");

        private final String code;
        private final String description;

        private static final Map<String, String> MAP = new HashMap<String, String>();
        static {
            for (SDR06CarRentalMap s : SDR06CarRentalMap.values()) {
                MAP.put(s.code, s.description);
            }
        }

        private static final Map<String, String> reverseMAP = new HashMap<String, String>();
        static {
            for (SDR06CarRentalMap s : SDR06CarRentalMap.values()) {
                reverseMAP.put(s.description, s.code);
            }
        }

        private SDR06CarRentalMap(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public static String getSdrMappingForCdfField(String code) {
            return MAP.get(code);
        }

        public static String getCdfMappingForSdrField(String code) {
            return reverseMAP.get(code);
        }

    }

    public enum SDR05AirlineMap {
    
      SDR_05_PASSENGER_NAME("passenger_name", "passenger_name"),
      SDR_05_CUSTOMER_CODE("customer_code", "customer_code"),
      SDR_05_TOTAL_FARE_AMOUNT("total_fare_amount", "total_fare"),
      SDR_05_TOTAL_TAX_AMOUNT("total_tax_amount", "total_taxes"),
      SDR_05_EXCHANGE_TICKET_NUMBER("exchange_ticket_number", "exchange_ticket"),
      SDR_05_EXCHANGE_TICKET_FEE("exchange_ticket_amount", "exchange_fee"),
      SDR_05_TRAVEL_AGENCY_CODE("travel_agency_code", "travel_agency_code"),
      SDR_05_TRAVEL_AGENCY_NAME("travel_agency_name", "travel_agency_name"),
      SDR_05_TICKET_ISSUE_DATE("issue_date", "ticket_issue_date"),
      SDR_05_ISSUING_CARRIER("issuing_carrier", "carrier_code"),
      SDR_05_RESTRICTED_TICKET_IND("restricted_ticket_ind", "restriction_indicator"),
      SDR_05_TICKET_NUMBER("ticket_number", "ticket_number");

        private final String code;
        private final String description;

        private static final Map<String, String> MAP = new HashMap<String, String>();
        static {
            for (SDR05AirlineMap s : SDR05AirlineMap.values()) {
                MAP.put(s.code, s.description);
            }
        }

        private static final Map<String, String> reverseMAP = new HashMap<String, String>();
        static {
            for (SDR05AirlineMap s : SDR05AirlineMap.values()) {
                reverseMAP.put(s.description, s.code);
            }
        }

        private SDR05AirlineMap(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public static String getSdrMappingForCdfField(String code) {
            return MAP.get(code);
        }

        public static String getCdfMappingForSdrField(String code) {
            return reverseMAP.get(code);
        }
    }

    public enum SDR05AirlineLegMap {
    
      SDR_05_LEG_CONJ_TICKET_NUM("conj_ticket_num", "leg~_conjunction_ticket_number"),
      SDR_05_LEG_COUPON_NUMBER("coupon_number", "leg~_coupon"),
      SDR_05_CARRIER_CODE("carrier_code", "carrier_code"),
      SDR_05_FLIGHT_NUMBER("flight_number", "leg~_travel_number"),
      SDR_05_LEG_DEPARTURE_CODE("origin_airport_code", "leg~_departure_code"),
      SDR_05_LEG_STOP_OVER_CODE("stopover_code", "leg~_stop_over_code"),
      SDR_05_LEG_DESTINATION_CODE("dest_airport_code", "leg~_destination_code"),
      SDR_05_LEG_FARE_BASIS_CODE("fare_basis_code", "leg~_fare_basis_code"),
      SDR_05_DEPARTURE_DATE("departure_date", "arrival_date"),
      SDR_05_LEG_DEPARTURE_TIME("departure_time", "leg~_departure_time"),
      SDR_05_LEG_ARRIVAL_TIME("arrival_time", "leg~_arrival_time"),
      SDR_05_LEG_EXCHANGE_TICKET("exchange_ticket_number", "leg~_exchange_ticket"),
      SDR_05_LEG_AMOUNT("fare_amount", "leg~_amount"),
      SDR_05_LEG_FEES("fee_amount", "leg~_fees"),
      SDR_05_LEG_TAX("tax_amount", "leg~_tax"),
      SDR_05_LEG_RESTRICTIONS("endorsements_restrictions", "leg~_restrictions");

        private final String code;
        private final String description;

        private static final Map<String, String> MAP = new HashMap<String, String>();
        static {
            for (SDR05AirlineLegMap s : SDR05AirlineLegMap.values()) {
                MAP.put(s.code, s.description);
            }
        }

        private static final Map<String, String> reverseMAP = new HashMap<String, String>();
        static {
            for (SDR05AirlineLegMap s : SDR05AirlineLegMap.values()) {
                reverseMAP.put(s.description, s.code);
            }
        }

        private SDR05AirlineLegMap(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public static String getSdrMappingForCdfField(String code) {
            return MAP.get(code);
        }

        public static String getCdfMappingForSdrField(String code) {
            return reverseMAP.get(code);
        }
    }

}
