/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/FlatFileRecordField.java $

  Description:

    FlatFileRecordField

    Implementation of a single field in a FlatFileRecord.  Contains
    information necessary to spit out the field into a flat text file.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;

public class FlatFileRecordField
{
  protected   int       fieldNumber;
  protected   String    name;
  protected   int       type;
  protected   int       length;
  protected   int       decimalPlaces;
  protected   String    defaultValue;
  protected   String    data;
  protected   String    mask;

  public final static int   FIELD_TYPE_ALPHA                    = 0;
  public final static int   FIELD_TYPE_NUMERIC                  = 1;
  public final static int   FIELD_TYPE_DATE                     = 2;
  public final static int   FIELD_TYPE_ALPHA_NUMERIC            = 3;
  public final static int   FIELD_TYPE_ALPHA_ONLY               = 4;
  public final static int   FIELD_TYPE_ALPHA_NUMERIC_NO_PAD     = 5;
  public final static int   FIELD_TYPE_ALPHA_NUMERIC_LPAD       = 6;
  public final static int   FIELD_TYPE_ALPHA_NUMERIC_MIN_ONE    = 7;

  private void initialize(int fieldNumber, String name, int type, int length, int decimalPlaces, String mask, String defaultValue)
  {
    this.fieldNumber    = fieldNumber;
    this.name           = (name == null) ? "" : name;
    this.type           = type;
    this.length         = length;
    this.decimalPlaces  = decimalPlaces;
    this.mask           = (mask == null) ? "" : mask;
    this.defaultValue   = defaultValue;

    reset();    // set back to default value
  }

  public FlatFileRecordField(int fieldNumber, String name, int type, int length, int decimalPlaces, String mask, String defaultValue)
  {
    initialize(fieldNumber, name, type, length, decimalPlaces, mask, defaultValue);
  }

  public FlatFileRecordField(int fieldNumber, String name, int type, int length, int decimalPlaces, String mask)
  {
    initialize(fieldNumber, name, type, length, decimalPlaces, mask, "");
  }

  public void reset( )
  {
    if( this.defaultValue != null && !this.defaultValue.equals(""))
    {
      switch(type)
      {
        case FIELD_TYPE_ALPHA:
        case FIELD_TYPE_DATE:
        case FIELD_TYPE_ALPHA_NUMERIC:
        case FIELD_TYPE_ALPHA_ONLY:
        case FIELD_TYPE_ALPHA_NUMERIC_NO_PAD:
        case FIELD_TYPE_ALPHA_NUMERIC_LPAD:
        case FIELD_TYPE_ALPHA_NUMERIC_MIN_ONE:
          setData(this.defaultValue);
          break;

        case FIELD_TYPE_NUMERIC:
          setData(Double.parseDouble(this.defaultValue));
          break;
      }
    }
    else
    {
      this.data = "";
    }
  }
  
  public int getDecimalPlaces()
  {
    return decimalPlaces;
  }

  public int getNumber()
  {
    return fieldNumber;
  }
  public String getName()
  {
    return name;
  }
  public int getType()
  {
    return type;
  }
  public int getLength()
  {
    return length;
  }
  public String getMask()
  {
    return mask;
  }
  public String getRawData()
  {
    return data;
  }

  public void setData(String data)
  {
    if(data == null)
    {
      this.data = "";
    }
    else
    {
      this.data = data;
    }
  }

  public void setData(int data)
  {
    if(mask != null && !mask.equals(""))
    {
      // apply mask
      this.data = NumberFormatter.getLongString(Long.parseLong(Integer.toString(data)), mask);
    }
    else
    {
      this.data = Integer.toString(data);
    }
  }

  public void setData(long data)
  {
    if(mask != null && !mask.equals(""))
    {
      // apply mask
      this.data = NumberFormatter.getLongString(data, mask);
    }
    else
    {
      this.data = Long.toString(data);
    }
  }

  public void setData(double data)
  {
    if(mask != null && !mask.equals(""))
    {
      // apply mask
      this.data = NumberFormatter.getDoubleString(data, mask);
    }
    else
    {
      this.data = Long.toString(MesMath.doubleToLong(data, decimalPlaces));
    }
  }

  public void setData(Date data)
  {
    try
    {
      if(data == null)
      {
        this.data = "";
      }
      else
      {
        this.data = (new SimpleDateFormat(mask)).format(data);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.flatfile.FlatFileRecordField::setData(date)", e.toString());
    }
  }

  private String preFormat()
  {
    StringBuffer newData = new StringBuffer("");

    try
    {
      switch(type)
      {
        case FIELD_TYPE_ALPHA:
          // remove non-printable characters (nulls)
          for(int i=0; i < data.length(); ++i)
          {
            if(data.charAt(i) != 0x0000)
            {
              newData.append(data.charAt(i));
            }
          }
          break;
        case FIELD_TYPE_ALPHA_NUMERIC:
          // only allow numbers and letters
          for(int i=0; i < data.length(); ++i)
          {
            if(Character.isLetterOrDigit(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              newData.append(data.charAt(i));
            }
          }
          break;

        case FIELD_TYPE_ALPHA_ONLY:
          // only allow letters
          for(int i=0; i < data.length(); ++i)
          {
            if(Character.isLetter(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              newData.append(data.charAt(i));
            }
          }
          break;

        default:
          newData.append(data);
          break;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::preFormat()", e.toString());
    }

    return newData.toString();
  }

  public String getFormattedData()
  {
    StringBuffer  result = new StringBuffer("");
    int           i;
    String        dataStr  = null;

    try
    {
      // do any preformatting that the data type requires
      dataStr = preFormat();

      switch(type)
      {
        case FIELD_TYPE_ALPHA:
        case FIELD_TYPE_DATE:
        case FIELD_TYPE_ALPHA_NUMERIC:
        case FIELD_TYPE_ALPHA_ONLY:
          // pad right with spaces
          if(dataStr.length() > length)
          {
            result.append(dataStr.substring(0, length));
          }
          else
          {
            result.append(dataStr);
            for(i = 0; i < (length - dataStr.length()); ++i)
            {
              result.append(" ");
            }
          }
          break;
          
        case FIELD_TYPE_ALPHA_NUMERIC_LPAD:
          // pad left with spaces
          if(dataStr.length() > length)
          {
            result.append(dataStr.substring(0, length));
          }
          else
          {
            result.append(dataStr);
            for(i = 0; i < (length - dataStr.length()); ++i)
            {
              result.insert(0," ");
            }
          }
          break;

        case FIELD_TYPE_NUMERIC:
          // pad left with zeroes
          if(dataStr.length() > length)
          {
            // error condition
            throw new Exception("numeric data too long for field");
          }
          else
          {
            // add zeroes
            for(i = 0; i < (length - dataStr.length()); ++i)
            {
              result.append("0");
            }

            // add data
            result.append(dataStr);
          }
          break;

        case FIELD_TYPE_ALPHA_NUMERIC_NO_PAD:
          // no padding
          if(dataStr.length() > length)
          {
            result.append(dataStr.substring(0, length));
          }
          else
          {
            result.append(dataStr);
          }
          break;
          
        case FIELD_TYPE_ALPHA_NUMERIC_MIN_ONE:    
          // no padding, insure that a blank value is a single space
          if(dataStr.length() > length)
          {
            result.append(dataStr.substring(0, length));
          }
          else
          {
            result.append( ((dataStr.length() == 0) ? " " : dataStr) );
          }
          break;
      }
    }
    catch(Exception e)
    {
      result.append(name);
      result.append(" error: ");
      result.append(e.toString());
    }

    return result.toString();
  }
  
  // return the data formatted for display 
  public String getDisplayData()
  {
    StringBuffer  result = new StringBuffer("");
    int           i;
    String        dataStr  = null;

    try
    {
      // do any preformatting that the data type requires
      dataStr = preFormat();

      switch(type)
      {
        case FIELD_TYPE_NUMERIC:
          // pad left with zeroes
          result.append(dataStr);
          if ( decimalPlaces > 0 ) 
          {
            result.insert((result.length()-decimalPlaces),".");
          }
          break;
          
        //case FIELD_TYPE_ALPHA:
        //case FIELD_TYPE_DATE:
        //case FIELD_TYPE_ALPHA_NUMERIC:
        //case FIELD_TYPE_ALPHA_ONLY:
        //case FIELD_TYPE_ALPHA_NUMERIC_NO_PAD:
        //case FIELD_TYPE_ALPHA_NUMERIC_LPAD:
        //case FIELD_TYPE_ALPHA_NUMERIC_MIN_ONE:
        default:
          result.append(dataStr);
          break;
      }
    }
    catch(Exception e)
    {
      result.append(name);
      result.append(" error: ");
      result.append(e.toString());
    }
    return( result.toString() );
  }
  
  public void setFormattedData( String data )
    throws Exception
  {
    StringBuffer  decodedData   = new StringBuffer();

    switch(type)
    {
      case FIELD_TYPE_ALPHA:
        // remove non-printable characters (nulls)
        for(int i = 0; i < data.length(); ++i)
        {
          if( data.charAt(i) != 0x0000 )
          {
            decodedData.append(data.charAt(i));
          }
        }
        setData( decodedData.toString() );
        break;
        
      case FIELD_TYPE_ALPHA_NUMERIC:
      case FIELD_TYPE_ALPHA_NUMERIC_NO_PAD:
      case FIELD_TYPE_ALPHA_NUMERIC_LPAD:
        // only allow numbers and letters
        for(int i = 0; i < data.length(); ++i)
        {
          if( Character.isLetterOrDigit(data.charAt(i)) || 
              Character.isWhitespace(data.charAt(i)) )
          {
            decodedData.append(data.charAt(i));
          }
        }
        setData( decodedData.toString() );
        break;
        
      case FIELD_TYPE_ALPHA_ONLY:
        // only allow letters
        for(int i = 0; i < data.length(); ++i)
        {
          if( Character.isLetter(data.charAt(i)) || 
              Character.isWhitespace(data.charAt(i)) )
          {
            decodedData.append(data.charAt(i));
          }
        }
        setData( decodedData.toString() );
        break;
        
      case FIELD_TYPE_NUMERIC:
        if( this.mask != null && !this.mask.equals("") )
        {
          DecimalFormat    df = new DecimalFormat(this.mask);
          
          setData( df.parse(data).doubleValue() );
        }
        else
        {
          setData( Long.parseLong(data)  );
        }
        break;
        
      case FIELD_TYPE_DATE:
        setData( DateTimeFormatter.parseDate(data,this.mask) );
        break;

      default:
        setData(data);
        break;
    }
  }
}
