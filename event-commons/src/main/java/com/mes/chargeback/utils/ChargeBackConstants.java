package com.mes.chargeback.utils;

public enum ChargeBackConstants {
	PROC_TYPE_CB_FILE(2,""),PT_CB_ADJ(2,""),NEXT_DAY_FUNDING_CLAUSE(0,"and atd.same_day_funding = 'N'");
	
	private int intValue;
	private String stringValue;
	
	public int getIntValue() {
		return intValue;
	}
	
	public String getStringValue() {
		return stringValue;
	}
	
	private ChargeBackConstants(int intValue, String stringValue){
		this.intValue = intValue;
		this.stringValue = stringValue;
	}
}
