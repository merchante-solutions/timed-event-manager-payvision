/*@lineinfo:filename=AchOriginList*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ach/AchOriginList.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-06-01 09:21:14 -0700 (Wed, 01 Jun 2011) $
  Version            : $Revision: 18870 $

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AchOriginList extends SQLJConnectionBase
{
  public static class AchOrigin
  {
    protected   int       EmailAddressId        = 0;
    protected   String    EnabledFlag           = "";
    protected   long      OriginNode            = 0L;
    protected   HashMap   EntryDescriptions     = new HashMap();
    protected   int       SettlementDateOffset  = 0;
  
    public AchOrigin( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      EmailAddressId  = resultSet.getInt("email_address_id");
      OriginNode      = resultSet.getLong("origin_node");
      EnabledFlag     = resultSet.getString("enabled_flag");
      SettlementDateOffset = resultSet.getInt("settlement_date_offset");
    }
    
    public void addEntryDescription( String entryDesc, String enabledFlag )
    {
      EntryDescriptions.put(entryDesc,enabledFlag);
    }
    
    public int getEmailAddressId( )
    {
      return( EmailAddressId );
    }
    
    public long getOriginNode( )
    {
      return( OriginNode );
    }
    
    public int getSettlementDateOffset( )
    {
      return( SettlementDateOffset );
    }
    
    public void setEntryDescriptions( HashMap entries )
    {
      EntryDescriptions = entries;
    }
  }
  
  protected   Map     AchOrigins          = new HashMap();
  
  public AchOriginList()
  {
  }
  
  public long getMerchantOriginNode( long merchantId, String entryDesc )
  {
    return( getMerchantOriginNode(merchantId,entryDesc,false) );
  }
  
  public long getMerchantOriginNode( long merchantId, String entryDesc, boolean testMode )
  {
    String              acceptedFlags   = (testMode ? "'T','X'" : "'Y','N'");
    String              enabledFlag     = (testMode ? "T" : "Y");
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;
    long                retVal          = 0L;
    
    long                defaultNode     = 0L;
    
    try
    {
      connect();
      
      // establish default node
      /*@lineinfo:generated-code*//*@lineinfo:108^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_number(to_char(mf.bank_number)||'00000')
//          
//          from    mif mf
//          where   mf.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_number(to_char(mf.bank_number)||'00000')\n         \n        from    mif mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ach.AchOriginList",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   defaultNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^7*/
      
      // initialize result to be default node
      retVal = defaultNode;
      
      // find closest node. 3858 has backwards hierarchy.
      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ato.origin_node                             as origin_node,
//                  ato.origin_descriptor                       as origin_descriptor,
//                  ato.ach_enabled                             as enabled,
//                  (case 
//                    when ato.origin_node = hier.level_1 then 1
//                    when ato.origin_node = hier.level_2 then 2
//                    when ato.origin_node = hier.level_3 then 3
//                    when ato.origin_node = hier.level_4 then 4
//                    when ato.origin_node = hier.level_5 then 5
//                    when ato.origin_node = hier.level_6 then 6
//                    when ato.origin_node = hier.level_7 then 7
//                    when ato.origin_node = hier.level_8 then 8
//                    when ato.origin_node = hier.level_9 then 9
//                    when ato.origin_node = hier.level_10 then 10
//                  else -1
//                  end)                                        as hier_level
//          from    ach_trident_origin ato,
//                  (
//                    select  mf.merchant_number                                    level_1,
//                            mf.association_node                                   level_2,
//                            decode(mf.bank_number,
//                              3858, to_number(mf.bank_number||mf.group_1_association),
//                              to_number(mf.bank_number||mf.group_6_association))  level_3,
//                            decode(mf.bank_number,
//                              3858, to_number(mf.bank_number||mf.group_2_association), 
//                              to_number(mf.bank_number||mf.group_5_association))  level_4,
//                            decode(mf.bank_number, 
//                              3858, to_number(mf.bank_number||mf.group_3_association),
//                              to_number(mf.bank_number||mf.group_4_association))  level_5,
//                            decode(mf.bank_number,
//                              3858, to_number(mf.bank_number||mf.group_4_association),
//                              to_number(mf.bank_number||mf.group_3_association))  level_6,
//                            decode(mf.bank_number,
//                              3858, to_number(mf.bank_number||mf.group_5_association),
//                              to_number(mf.bank_number||mf.group_2_association))  level_7,
//                            decode(mf.bank_number,
//                              3858, to_number(mf.bank_number||mf.group_6_association),
//                              to_number(mf.bank_number||mf.group_1_association))  level_8,
//                            to_number(mf.bank_number||'00000')                    level_9,
//                            9999999999                                            level_10
//                    from    mif mf
//                    where   mf.merchant_number = :merchantId                  
//                  ) hier
//          where   (
//                    ato.origin_node = hier.level_1 or
//                    ato.origin_node = hier.level_2 or
//                    ato.origin_node = hier.level_3 or
//                    ato.origin_node = hier.level_4 or
//                    ato.origin_node = hier.level_5 or
//                    ato.origin_node = hier.level_6 or
//                    ato.origin_node = hier.level_7 or
//                    ato.origin_node = hier.level_8 or
//                    ato.origin_node = hier.level_9 or
//                    ato.origin_node = hier.level_10
//                  )
//                  and ato.origin_descriptor = :entryDesc
//                  and ato.ach_enabled in ( :acceptedFlags )
//          order by hier_level      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  ato.origin_node                             as origin_node,\n                ato.origin_descriptor                       as origin_descriptor,\n                ato.ach_enabled                             as enabled,\n                (case \n                  when ato.origin_node = hier.level_1 then 1\n                  when ato.origin_node = hier.level_2 then 2\n                  when ato.origin_node = hier.level_3 then 3\n                  when ato.origin_node = hier.level_4 then 4\n                  when ato.origin_node = hier.level_5 then 5\n                  when ato.origin_node = hier.level_6 then 6\n                  when ato.origin_node = hier.level_7 then 7\n                  when ato.origin_node = hier.level_8 then 8\n                  when ato.origin_node = hier.level_9 then 9\n                  when ato.origin_node = hier.level_10 then 10\n                else -1\n                end)                                        as hier_level\n        from    ach_trident_origin ato,\n                (\n                  select  mf.merchant_number                                    level_1,\n                          mf.association_node                                   level_2,\n                          decode(mf.bank_number,\n                            3858, to_number(mf.bank_number||mf.group_1_association),\n                            to_number(mf.bank_number||mf.group_6_association))  level_3,\n                          decode(mf.bank_number,\n                            3858, to_number(mf.bank_number||mf.group_2_association), \n                            to_number(mf.bank_number||mf.group_5_association))  level_4,\n                          decode(mf.bank_number, \n                            3858, to_number(mf.bank_number||mf.group_3_association),\n                            to_number(mf.bank_number||mf.group_4_association))  level_5,\n                          decode(mf.bank_number,\n                            3858, to_number(mf.bank_number||mf.group_4_association),\n                            to_number(mf.bank_number||mf.group_3_association))  level_6,\n                          decode(mf.bank_number,\n                            3858, to_number(mf.bank_number||mf.group_5_association),\n                            to_number(mf.bank_number||mf.group_2_association))  level_7,\n                          decode(mf.bank_number,\n                            3858, to_number(mf.bank_number||mf.group_6_association),\n                            to_number(mf.bank_number||mf.group_1_association))  level_8,\n                          to_number(mf.bank_number||'00000')                    level_9,\n                          9999999999                                            level_10\n                  from    mif mf\n                  where   mf.merchant_number =  ?                   \n                ) hier\n        where   (\n                  ato.origin_node = hier.level_1 or\n                  ato.origin_node = hier.level_2 or\n                  ato.origin_node = hier.level_3 or\n                  ato.origin_node = hier.level_4 or\n                  ato.origin_node = hier.level_5 or\n                  ato.origin_node = hier.level_6 or\n                  ato.origin_node = hier.level_7 or\n                  ato.origin_node = hier.level_8 or\n                  ato.origin_node = hier.level_9 or\n                  ato.origin_node = hier.level_10\n                )\n                and ato.origin_descriptor =  ? \n                and ato.ach_enabled in (  ");
   __sjT_sb.append(acceptedFlags);
   __sjT_sb.append("  )\n        order by hier_level");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.ach.AchOriginList:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,entryDesc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
      
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if( enabledFlag.equals(resultSet.getString("enabled")) )
        {
          retVal = resultSet.getLong("origin_node");
          
          break;  // exit from while loop because we found our item
        }
      }
      
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("getMerchantOriginNode(" + merchantId + ")",e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch( Exception ee ) {}
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public AchOrigin loadNode( long nodeId, boolean testMode, String  entryDescList, String fasterFundingFlag )
  {
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    AchOrigin           origin = null;
    fasterFundingFlag = fasterFundingFlag == null ? "N" : fasterFundingFlag;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ato.origin_node             as origin_node,
//                  atd.email_address_id        as email_address_id,
//                  ato.ach_enabled             as enabled_flag,
//                  atd.settlement_date_offset  as settlement_date_offset,
//                  ato.origin_descriptor       as entry_desc
//          from    ach_trident_origin ato,
//                  ach_trident_destinations atd
//          where   ato.origin_node = :nodeId
//                  and instr(:entryDescList,ato.origin_descriptor) > 0
//                  and ato.dest_id = atd.dest_id
//                  and atd.same_day_funding = :fasterFundingFlag
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ato.origin_node             as origin_node,\n                atd.email_address_id        as email_address_id,\n                ato.ach_enabled             as enabled_flag,\n                atd.settlement_date_offset  as settlement_date_offset,\n                ato.origin_descriptor       as entry_desc\n        from    ach_trident_origin ato,\n                ach_trident_destinations atd\n        where   ato.origin_node =  :1 \n                and instr( :2 ,ato.origin_descriptor) > 0\n                and ato.dest_id = atd.dest_id\n and atd.same_day_funding = :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ach.AchOriginList",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,entryDescList);
   __sJT_st.setString(3,fasterFundingFlag);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ach.AchOriginList",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^7*/
      rs = it.getResultSet();
      
      while( rs.next() )
      {
        if( origin == null )
        {
          origin = new AchOrigin(rs);
        }
        
        if( origin != null ) // shouldn't be null as we just created it three lines ago
        {
          origin.addEntryDescription(rs.getString("entry_desc"), rs.getString("enabled_flag"));
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadNode(" + nodeId + ", " + entryDescList + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( origin );
  }
}/*@lineinfo:generated-code*/
