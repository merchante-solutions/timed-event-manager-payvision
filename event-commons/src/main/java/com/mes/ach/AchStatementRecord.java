/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ach/AchStatementRecord.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2012-11-28 11:41:59 -0800 (Wed, 28 Nov 2012) $
  Version            : $Revision: 20730 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.Date;
import java.sql.ResultSet;

public class AchStatementRecord
{
  long    recId               = 0L;
  long    merchantNumber      = 0L;
  int     bankNumber          = 0;
  Date    postDate            = null;
  int     salesCount          = 0;
  double  salesAmount         = 0.0;
  int     creditsCount        = 0;
  double  creditsAmount       = 0.0;
  double  sponsoredAmount     = 0.0;
  double  nonSponsoredAmount  = 0.0;
  double  netAmount           = 0.0;
  double  achAmount           = 0.0;
  double  dailyDiscountAmount = 0.0;
  double  dailyICAmount       = 0.0;
  String  loadFilename        = "";
  int     transactionCode     = 0;
  String  entryDesc           = AchEntryData.ED_MERCH_DEP;
  Date    batchDate           = null;
  long    batchNumber         = 0L;
  String  sameDayInd          = "N";
  String  preFundInd          = "N";
  double  vsAmount			  = 0.0;
  double  mcAmount			  = 0.0;
  double  amAmount			  = 0.0;
  double  dsAmount			  = 0.0;
  double  plAmount			  = 0.0;
  double  dbAmount			  = 0.0;
  long    loadFileId          = 0L;
  
  public AchStatementRecord()
  {
  }
  
  public AchStatementRecord(ResultSet rs)
  {
    try
    {
      setRecId           ( rs.getLong("rec_id") );
      setMerchantId      ( rs.getLong("merchant_number") );
      setPostDate        ( rs.getDate("post_date") );
      setSalesCount      ( rs.getInt("sales_count") );
      setSalesAmount     ( rs.getDouble("sales_amount") );
      setCreditsCount    ( rs.getInt("credits_count") );
      setCreditsAmount   ( rs.getDouble("credits_amount") );
      setNetAmount       ( rs.getDouble("net_amount") );
      setAchAmount       ( (netAmount >=0.0 ? netAmount : (netAmount * -1)) );
      setTransactionCode ( (netAmount >=0.0 ? 22 : 27) );
      try {
    	  setBatchNumber ( rs.getLong("batch_number") );
      }catch(java.sql.SQLException sqle){
    	  //ignore, because in case of manual merchant chargeback no batch number exists
      }
      
      
      try
      {
        // allow the entry descriptor to be overloaded
        setEntryDesc( rs.getString("entry_desc") );
      }
      catch( java.sql.SQLException sqle )
      {
        // ignore, just use default of AchEntryData.ED_MERCH_DEP
      }
      
      try
      {
        setBatchDate( rs.getDate("batch_date") );
      }
      catch( java.sql.SQLException sqle )
      {
        // ignore, batch date only exists for MES back end 
      }
      
      try
      {
        setBankNumber( rs.getInt("bank_number") );
      }
      catch( java.sql.SQLException sqle )
      {
        // ignore, bank number only exists for MES back end 
      }
      
      try
      {
        setDailyDiscountAmount( rs.getDouble("daily_discount_amount") );
        setDailyICAmount      ( rs.getDouble("daily_ic_amount") );
        setLoadFilename       ( rs.getString("load_filename") );
      }
      catch( java.sql.SQLException sqle )
      {
        // ignore, these columns don't exist in the result set so just use default
      }
      
      try
      {
        setSponsoredAmount    ( rs.getDouble("sponsored_amount") );
        setNonSponsoredAmount ( rs.getDouble("non_sponsored_amount") );
      }
      catch( java.sql.SQLException sqle )
      {
        // columns don't exist in result set - make sure sponsored amount is set
        setSponsoredAmount     ( rs.getDouble("net_amount") );
      }
      
      try
      {
        setSameDayInd( rs.getString("same_day_ind") );
        setPreFundInd(rs.getString("pre_fund_ind"));
      } 
      catch( java.sql.SQLException sqle )
      {
       // ignore, these columns don't exist in the result set so just use default
      }
        // columns don't exist in result set
      try {
    	  setLoadFileId ( rs.getLong("load_file_id") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }

      try {
    	  setVsAmount ( rs.getDouble("vs_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }

      try {
    	  setMcAmount ( rs.getDouble("mc_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }

      try {
    	  setAmAmount ( rs.getDouble("am_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }

      try {
    	  setDsAmount ( rs.getDouble("ds_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }

      try {
    	  setPlAmount ( rs.getDouble("pl_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }
      
      try {
    	  setDbAmount ( rs.getDouble("db_amount") );
      }catch(java.sql.SQLException sqle){
    	// ignore, these columns don't exist in the result set so just use default
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry((this.getClass().getName() + ".StatementRecord()"), e.toString());
    }
  }
  
  public long   getMerchantId()             { return(merchantNumber); }
  public void   setMerchantId(long val)     { merchantNumber = val; }
  
  public Date   getBatchDate()               { return(batchDate); }
  public void   setBatchDate(Date val)       { batchDate = val; }
  
  public int    getBankNumber()              { return(bankNumber); }
  public void   setBankNumber(int val)       { bankNumber = val; }
  
  public Date   getPostDate()               { return(postDate); }
  public void   setPostDate(Date val)       { postDate = val; }
  
  public String getEntryDesc()              { return(entryDesc); }
  public void   setEntryDesc(String val)    { entryDesc = val; }
  
  public double getAchAmount()              { return(achAmount); }
  public void   setAchAmount(double val)    { achAmount = val; }
  
  public int    getTransactionCode()        { return(transactionCode); }
  public void   setTransactionCode(int val) { transactionCode = val; }
  
  public int    getSalesCount()             { return(salesCount); }
  public void   setSalesCount(int val)      { salesCount = val; }
  
  public double getSalesAmount()            { return(salesAmount); }
  public void   setSalesAmount(double val)  { salesAmount = val; }
  
  public int    getCreditsCount()           { return(creditsCount); }
  public void   setCreditsCount(int val)    { creditsCount = val; }
  
  public double getCreditsAmount()          { return(creditsAmount); }
  public void   setCreditsAmount(double val){ creditsAmount = val; }
  
  public double getNetAmount()              { return(netAmount); }
  public void   setNetAmount(double val)    { netAmount = val; }
  
  public void   setSponsoredAmount(double val)    { sponsoredAmount = val; }
  public double getSponsoredAmount()              { return( sponsoredAmount ); }
  public void   setNonSponsoredAmount(double val) { nonSponsoredAmount = val; }
  public double getNonSponsoredAmount()           { return( nonSponsoredAmount ); }
  
  public double getDailyDiscountAmount()    { return(dailyDiscountAmount); }
  public void   setDailyDiscountAmount(double val)    { dailyDiscountAmount = val; }
  
  public double getDailyICAmount()          { return(dailyICAmount); }
  public void   setDailyICAmount(double val){ dailyICAmount = val; }
  
  public String getLoadFilename()           { return( loadFilename ); }
  public void   setLoadFilename(String val) { loadFilename = val; }
  
  public long   getRecId()                  { return(recId); }
  public void   setRecId(long val)          { recId = val; }
  
  public long   getBatchNumber()            { return(batchNumber);}
  public void   setBatchNumber(long val)    { batchNumber = val; }
  
  public String getSameDayInd()              { return(sameDayInd); }
  public void   setSameDayInd(String val)    { sameDayInd = val; }
  
  public String getPreFundInd()              { return(preFundInd); }
  public void   setPreFundInd(String val)    { preFundInd = val; }
  public double   getVsAmount()            { return(vsAmount);}
  public void   setVsAmount(double val)    { vsAmount = val; }
  
  public double   getMcAmount()            { return( mcAmount);}
  public void   setMcAmount(double val)    { mcAmount = val; }
  
  public double   getAmAmount()            { return(amAmount);}
  public void   setAmAmount(double val)    { amAmount = val; }
  
  public double   getDsAmount()            { return(dsAmount);}
  public void   setDsAmount(double val)    { dsAmount = val; }
  
  public double   getPlAmount()            { return(plAmount);}
  public void   setPlAmount(double val)    { plAmount = val; }
  
  public double   getDbAmount()            { return(dbAmount);}
  public void   setDbAmount(double val)    { dbAmount = val; }
  
  public long   getLoadFileId()            { return(loadFileId);}
  public void   setLoadFileId(long val)    { loadFileId = val; }
}
