/*@lineinfo:filename=AchEntryData*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ach/AchEntryData.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-09-03 13:29:03 -0700 (Tue, 03 Sep 2013) $
  Version            : $Revision: 21460 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class AchEntryData extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(AchEntryData.class);

  public    static final String       ED_MERCH_DEP          = "MERCH DEP";
  public    static final String       ED_AMEX_DEP           = "AMEX DEP";
  public    static final String       ED_CHARGEBACK         = "MERCH CHBK";
  public    static final String       ED_DAILY_DISCOUNT     = "DAILY DISC";
  public    static final String       ED_DAILY_IC           = "DAILY IC";
  public    static final String       ED_ADJUSTMENTS        = "MERCH ADJ";
  public    static final String       ED_BILLING            = "BILLING";
  public    static final String       ED_MERCH_BILL         = "MERCH BILL";
  public    static final String       ED_MANUAL_ADJ         = "MANUAL ADJ";
  public    static final String       ED_DAILY_INT          = "DLY INT";
  public    static final String       ED_DAILY_INT_S        = "DLY INT S";
  public    static final String       ED_MERCHANT_FEES      = "MERCH FEES";
  public    static final String       ED_MERCH_FEE          = "MERCH FEE";
  public    static final String       ED_MERCH_RESERVE      = "MERCH RESV";
  
  private static final String SQL_INSERT_ACH_DETAIL = ""
          + "INSERT "
          + "INTO ach_trident_detail "
          + "  ( "
          + "     transaction_code, "
          + "     dfi_tr_number, "
          + "     dfi_account_number, "
          + "     post_date, "
          + "     ach_amount, "
          + "     sponsored_amount, "
          + "     non_sponsored_amount, "
          + "     merchant_number, "
          + "     dba_name, "
          + "     discretionary_data, "
          + "     trace_number, "
          + "     entry_class_code, "
          + "     entry_description, "
          + "     manual_description_id, "
          + "     ach_sequence, "
          + "     origin_node, "
          + "     origin_id, "
          + "     origin_name, "
          + "     origin_tr_number, "
          + "     destination_id, "
          + "     destination_name, "
          + "     company_id, "
          + "     company_name, "
          + "     file_id_modifier, "
          + "     source_filename, "
          + "     load_filename, "
          + "     load_file_id, "
          + "     assoc_level, "
          + "     ach_run_date, "
          + "     batch_id, "
          + "     batch_number, "
          + "     record_type, "
          + "     process_sequence "
          + "  ) "
          + "   "
          + "SELECT ? , "
          + "   ? , "
          + "   ? , "
          + "   ? , "
          + "   ABS( ? ), "
          + "   ? , "
          + "   ? , "
          + "   ? , "
          + "   SUBSTR( ? ,1,22), "
          + "   NULL,                         "
          + "   ? , "
          + "   nvl( ? ,'CCD'),              "
          + "   nvl( ? , ? ),                       "
          + "   ? , "
          + "   ? , "
          + "   ato.origin_node, "
          + "   atd.origin_id, "
          + "   atd.origin_name, "
          + "   atd.origin_tr_number, "
          + "   atd.destination_id, "
          + "   atd.destination_name, "
          + "   decode(adtc.debit_credit_indicator, 'C', atd.company_id_credits, atd.company_id_debits), "
          + "   atd.company_name, "
          + "   null,                        "
          + "   ? ,                          "
          + "   case   when  ?  = 0   then 'NOT SENT - ZERO NET'    else  ?   end,                      "
          + "  ? ,   "
          + "  ? ,   "
          + "  ? ,   "
          + "  ? ,   "
          + "  ? ,   "
          + "  ? ,   "
          + "  case when  ?  = 0  then -1 else 0 end "
          + "  from ach_trident_origin   ato, "
          + "       ach_trident_destinations atd, "
          + "       ach_detail_tran_code adtc "
          + "  where   ato.origin_node =  ? "
          + "       and ato.origin_descriptor = nvl( ? , ? ) "
          + "       and ato.dest_id = atd.dest_id "
          + "       and  ?  = adtc.ach_detail_trans_code ";

  private class StatementData
  {
    public    double        CreditsAmount       = 0.0;
    public    int           CreditsCount        = 0;
    public    double        NetAmount           = 0.0;
    public    double        SalesAmount         = 0.0;
    public    double        SponsoredAmount     = 0.0;
    public    double        NonSponsoredAmount  = 0.0;
    public    int           SalesCount          = 0;
    public    String        TranCode            = null;
    public    Date          BatchDate           = null;
    public    long          BatchNumber         = 0L;
    
    public StatementData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CreditsCount    = resultSet.getInt("credits_count");
      CreditsAmount   = resultSet.getDouble("credits_amount");
      NetAmount       = resultSet.getDouble("net_amount");
      SalesCount      = resultSet.getInt("sales_count");
      SalesAmount     = resultSet.getDouble("sales_amount");
      TranCode        = ((NetAmount < 0.0) ? "27" : "22");
      
      // ignore exceptions - if query didn't include these columns then no big deal
      try { BatchDate   = resultSet.getDate("batch_date");  } catch( Exception e ) {}
      try { BatchNumber = resultSet.getLong("rec_id");      } catch( Exception e ) {}
      
      try { SponsoredAmount = resultSet.getDouble("sponsored_amount"); } catch(Exception e) { SponsoredAmount = NetAmount; }
      try { NonSponsoredAmount = resultSet.getDouble("non_sponsored_amount"); } catch(Exception e) {}
    }
  }

  public    long          AchSequence         = 0L;
  public    String        ClassCode           = null;
  public    String        DbaName             = null;
  public    String        DdaNum              = null;
  public    String        EntryDesc           = null;
  public    long          MerchantId          = 0L;
  public    long          OriginNode          = 0L;
  public    Date          PostDate            = null;
  public    Date          BatchDate           = null;
  private   Vector        StatementRows       = new Vector();
  public    long          TraceNumber         = 0L;
  public    int           ManualDescriptionId = 0;
  public    String        TransitRouting      = null;
  public    String        LoadFilename        = null;
  public    long          LoadFileId          = 0L;
  public    int           AssocLevel          = 0;
  public    String        SourceFilename      = null;
  public    boolean       RemitFees           = false;
  public    long          RecId               = 0L;
  public    long          BatchId             = 0L;
  public    long          BatchNumber         = 0L;
  private   String        payoutRecordType    = null;

  public String getPayoutRecordType() {
     return payoutRecordType;
  }

  public void setPayoutRecordType(String payoutRecordType) {
      this.payoutRecordType = payoutRecordType;
  }

public AchEntryData()
  {
  }
  
  public void addStatementData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    StatementRows.add( new StatementData(resultSet) );
  }
  
  public void clear()
  {
    AchSequence         = 0L;
    ClassCode           = null;
    DbaName             = null;
    DdaNum              = null;
    EntryDesc           = null;
    MerchantId          = 0L;
    OriginNode          = 0L;
    PostDate            = null;
    TraceNumber         = 0L;
    TransitRouting      = null;
    RemitFees        = false;
    StatementRows.removeAllElements();
  }
  
  public double getNetAmount()
  {
    double      netAmount   = 0.0;
    
    for( int i = 0; i < StatementRows.size(); ++i )
    {
      netAmount += ((StatementData)StatementRows.elementAt(i)).NetAmount;
    }
    return( netAmount );
  }
  
  public String getTranCode()
  {
    return((getNetAmount() < 0.0) ? "27" : "22");
  }
  
  public void setAchSequence( long achSequence )
  {
    AchSequence = achSequence;
  }
  
  public void setClassCode( String classCode )
  {
    ClassCode = classCode;
  }
  
//@  public void setData( BatchData batchData )
//@  {
//@    DbaName         = batchData.DbaName;
//@    MerchantId      = batchData.MerchantId;
//@    NetAmount       = batchData.getTsysNetAmount();
//@    SalesCount      = batchData.getTsysSalesCount();
//@    SalesAmount     = batchData.getTsysSalesAmount();
//@    CreditsCount    = batchData.getTsysCreditsCount();
//@    CreditsAmount   = batchData.getTsysCreditsAmount();
//@    TranCode        = ((NetAmount < 0.0) ? "27" : "22");
//@  }
    
  public void setData( ResultSet resultSet )
    throws java.sql.SQLException 
  {
    AchSequence     = resultSet.getLong("ach_sequence");
    ClassCode       = resultSet.getString("class_code");
    DbaName         = resultSet.getString("dba_name");
    DdaNum          = resultSet.getString("dda_num");
    EntryDesc       = resultSet.getString("entry_desc");
    MerchantId      = resultSet.getLong("merchant_number");
    PostDate        = resultSet.getDate("post_date");
    TransitRouting  = resultSet.getString("transit_routing");
    
    try
    {
      LoadFilename  = resultSet.getString("load_filename");
      LoadFileId    = resultSet.getLong("load_file_id");
      
      try
      {
        // use deposit merchant number if present
        MerchantId    = resultSet.getLong("deposit_merchant_number");
        AssocLevel    = resultSet.getInt("assoc_level");
      }
      catch(Exception se)
      {
      }
      
      try
      {
        // remit payment flag
        RemitFees = ("Y").equals( resultSet.getString("remit_fees") );
      }
      catch(Exception re)
      {
      }
      
      // used to insert into remittance table, will match the hh_load_sec of the monthly_extract data
      RecId     = resultSet.getLong("rec_id");
      
       // Batch details
      BatchId         = resultSet.getLong("rec_id");
      BatchNumber     = resultSet.getLong("batch_number"); 
      
      setPayoutRecordType(resultSet.getString("record_type"));
    }
    catch(Exception e)
    {
      // ignore exception - if query didn't include these columns then no big deal
    }
  }
  
  public void setDdaNum( String ddaNum )
  {
    DdaNum = ddaNum;
  }
  
  public void setEntryDesc( String entryDesc )
  {
    EntryDesc = entryDesc;
  }
  
  public void setLoadFileId( long loadFileId )
  {
    LoadFileId = loadFileId;
  }
  
  public void setLoadFilename( String loadFilename )
  {
    LoadFilename = loadFilename;
  }
  
  public void setOriginNode( long nodeId )
  {
    OriginNode = nodeId;
  }
  
  public void setSourceFilename( String filename )
  {
    SourceFilename = filename;
  }
  
  public void setTraceNumber( long traceNumber )
  {
    TraceNumber = traceNumber;
  }
  
  public void setManualDescriptionId( int manualDescriptionId )
  {
    ManualDescriptionId = manualDescriptionId;
  }
  
  public boolean store( )
  {
    return( store(true, false) );
  }
  
  public boolean store( boolean addStatementRecords, boolean testMode )
  {
    boolean         autoCommit          = true;
    double          netAmount           = 0.0;
    double          sponsoredAmount     = 0.0;
    double          nonSponsoredAmount  = 0.0;
    boolean         retVal              = false;
    StatementData   row                 = null;
    String          tranCode            = getTranCode();
    
    try
    {
      connect(true);
      
      autoCommit = getAutoCommit();   // save current setting
      setAutoCommit(false);           // disable auto commit
      
      // calculate total amount and insert the statement ACH entries
      for( int i = 0; i < StatementRows.size(); ++i )
      {
        row = (StatementData)StatementRows.elementAt(i);
        netAmount += row.NetAmount;
        sponsoredAmount += row.SponsoredAmount;
        nonSponsoredAmount += row.NonSponsoredAmount;
      
        // do not create an entry in the deposit section 
        // of the statment for the billing ACH entry type
        if (  !ED_BILLING.equals(EntryDesc) &&  
              !ED_MERCH_BILL.equals(EntryDesc) && 
              addStatementRecords == true )
        {
          /*@lineinfo:generated-code*//*@lineinfo:294^11*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_statement
//              (
//                merchant_number,
//                post_date,
//                post_date_actual,
//                ach_amount,
//                transaction_code,
//                entry_description,
//                sales_count,
//                sales_amount,
//                credits_count,
//                credits_amount,
//                daily_discount_amount,
//                trace_number,
//                ach_sequence,
//                rec_id,
//                sponsored_amount,
//                non_sponsored_amount,
//                load_filename
//              )
//              values
//              (
//                :MerchantId,
//                :PostDate,
//                :PostDate,
//                abs( :row.NetAmount ),
//                :row.TranCode,
//                nvl(:EntryDesc,:ED_MERCH_DEP),  -- entry desc
//                :row.SalesCount,
//                :row.SalesAmount,
//                :row.CreditsCount,
//                :row.CreditsAmount,
//                0,                            -- daily discount amount
//                :TraceNumber,
//                -1,                           -- ach_sequence to -1 to prevent double processing
//                decode(:row.BatchNumber,0,null,:row.BatchNumber),
//                :row.SponsoredAmount,
//                :row.NonSponsoredAmount,
//                :LoadFilename
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_statement\n            (\n              merchant_number,\n              post_date,\n              post_date_actual,\n              ach_amount,\n              transaction_code,\n              entry_description,\n              sales_count,\n              sales_amount,\n              credits_count,\n              credits_amount,\n              daily_discount_amount,\n              trace_number,\n              ach_sequence,\n              rec_id,\n              sponsored_amount,\n              non_sponsored_amount,\n              load_filename\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n              abs(  :4   ),\n               :5  ,\n              nvl( :6  , :7  ),  -- entry desc\n               :8  ,\n               :9  ,\n               :10  ,\n               :11  ,\n              0,                            -- daily discount amount\n               :12  ,\n              -1,                           -- ach_sequence to -1 to prevent double processing\n              decode( :13  ,0,null, :14  ),\n               :15  ,\n               :16  ,\n               :17  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,PostDate);
   __sJT_st.setDate(3,PostDate);
   __sJT_st.setDouble(4,row.NetAmount);
   __sJT_st.setString(5,row.TranCode);
   __sJT_st.setString(6,EntryDesc);
   __sJT_st.setString(7,ED_MERCH_DEP);
   __sJT_st.setInt(8,row.SalesCount);
   __sJT_st.setDouble(9,row.SalesAmount);
   __sJT_st.setInt(10,row.CreditsCount);
   __sJT_st.setDouble(11,row.CreditsAmount);
   __sJT_st.setLong(12,TraceNumber);
   __sJT_st.setLong(13,row.BatchNumber);
   __sJT_st.setLong(14,row.BatchNumber);
   __sJT_st.setDouble(15,row.SponsoredAmount);
   __sJT_st.setDouble(16,row.NonSponsoredAmount);
   __sJT_st.setString(17,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^11*/
        }
        
        // update local BatchDate so that it can be stored in detail table
        BatchDate = row.BatchDate;
      }
      
      if ( netAmount != 0.0 )
      {
        if( testMode == false )
        {
          // insert remit payments into separate table
          if( RemitFees == true )
          {
            /*@lineinfo:generated-code*//*@lineinfo:350^13*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_remit
//                (
//                  hh_load_sec,
//                  merchant_number,
//                  invoice_amount
//                )
//                values
//                (
//                  :RecId,
//                  :MerchantId,
//                  :netAmount
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_remit\n              (\n                hh_load_sec,\n                merchant_number,\n                invoice_amount\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,RecId);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setDouble(3,netAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^13*/
          }
          else
          {
            // insert the payment ACH entry
            /*@lineinfo:generated-code*//*@lineinfo:369^13*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_detail
//                (
//                  transaction_code,
//                  dfi_tr_number,
//                  dfi_account_number,
//                  post_date,
//                  ach_amount,
//                  sponsored_amount,
//                  non_sponsored_amount,
//                  merchant_number,
//                  dba_name,
//                  discretionary_data,
//                  trace_number,
//                  entry_class_code,
//                  entry_description,
//                  manual_description_id,
//                  ach_sequence,
//                  origin_node,
//                  origin_id,
//                  origin_name,
//                  origin_tr_number,
//                  destination_id,
//                  destination_name,
//                  company_id,
//                  company_name,
//                  file_id_modifier,
//                  source_filename,
//                  load_filename,
//                  load_file_id,
//                  assoc_level,
//                  ach_run_date
//                )
//                select  :tranCode,
//                        :TransitRouting,
//                        :DdaNum,
//                        :PostDate,
//                        abs( :netAmount ),
//                        :sponsoredAmount,
//                        :nonSponsoredAmount,
//                        :MerchantId,
//                        substr(:DbaName,1,22),
//                        null,               -- discretionary data
//                        :TraceNumber,
//                        nvl(:ClassCode,'CCD'),        -- entry class code
//                        nvl(:EntryDesc,:ED_MERCH_DEP),  -- entry desc
//                        :ManualDescriptionId,
//                        :AchSequence,
//                        ato.origin_node,
//                        atd.origin_id,
//                        atd.origin_name,
//                        atd.origin_tr_number,
//                        atd.destination_id,
//                        atd.destination_name,
//                        decode(adtc.debit_credit_indicator,
//                          'C', atd.company_id_credits,
//                          atd.company_id_debits),
//                        atd.company_name,
//                        null,                 -- file id modifier
//                        :SourceFilename,      -- source of entry data
//                        :LoadFilename,        -- load filename
//                        :LoadFileId,          -- load file id,
//                        :AssocLevel,
//                        :BatchDate
//                from    ach_trident_origin   ato,
//                        ach_trident_destinations atd,
//                        ach_detail_tran_code adtc
//                where   ato.origin_node = :OriginNode
//                        and ato.origin_descriptor = nvl(:EntryDesc,:ED_MERCH_DEP)
//                        and ato.dest_id = atd.dest_id
//                        and :tranCode = adtc.ach_detail_trans_code
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_detail\n              (\n                transaction_code,\n                dfi_tr_number,\n                dfi_account_number,\n                post_date,\n                ach_amount,\n                sponsored_amount,\n                non_sponsored_amount,\n                merchant_number,\n                dba_name,\n                discretionary_data,\n                trace_number,\n                entry_class_code,\n                entry_description,\n                manual_description_id,\n                ach_sequence,\n                origin_node,\n                origin_id,\n                origin_name,\n                origin_tr_number,\n                destination_id,\n                destination_name,\n                company_id,\n                company_name,\n                file_id_modifier,\n                source_filename,\n                load_filename,\n                load_file_id,\n                assoc_level,\n                ach_run_date\n              )\n              select   :1  ,\n                       :2  ,\n                       :3  ,\n                       :4  ,\n                      abs(  :5   ),\n                       :6  ,\n                       :7  ,\n                       :8  ,\n                      substr( :9  ,1,22),\n                      null,               -- discretionary data\n                       :10  ,\n                      nvl( :11  ,'CCD'),        -- entry class code\n                      nvl( :12  , :13  ),  -- entry desc\n                       :14  ,\n                       :15  ,\n                      ato.origin_node,\n                      atd.origin_id,\n                      atd.origin_name,\n                      atd.origin_tr_number,\n                      atd.destination_id,\n                      atd.destination_name,\n                      decode(adtc.debit_credit_indicator,\n                        'C', atd.company_id_credits,\n                        atd.company_id_debits),\n                      atd.company_name,\n                      null,                 -- file id modifier\n                       :16  ,      -- source of entry data\n                       :17  ,        -- load filename\n                       :18  ,          -- load file id,\n                       :19  ,\n                       :20  \n              from    ach_trident_origin   ato,\n                      ach_trident_destinations atd,\n                      ach_detail_tran_code adtc\n              where   ato.origin_node =  :21  \n                      and ato.origin_descriptor = nvl( :22  , :23  )\n                      and ato.dest_id = atd.dest_id\n                      and  :24   = adtc.ach_detail_trans_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranCode);
   __sJT_st.setString(2,TransitRouting);
   __sJT_st.setString(3,DdaNum);
   __sJT_st.setDate(4,PostDate);
   __sJT_st.setDouble(5,netAmount);
   __sJT_st.setDouble(6,sponsoredAmount);
   __sJT_st.setDouble(7,nonSponsoredAmount);
   __sJT_st.setLong(8,MerchantId);
   __sJT_st.setString(9,DbaName);
   __sJT_st.setLong(10,TraceNumber);
   __sJT_st.setString(11,ClassCode);
   __sJT_st.setString(12,EntryDesc);
   __sJT_st.setString(13,ED_MERCH_DEP);
   __sJT_st.setInt(14,ManualDescriptionId);
   __sJT_st.setLong(15,AchSequence);
   __sJT_st.setString(16,SourceFilename);
   __sJT_st.setString(17,LoadFilename);
   __sJT_st.setLong(18,LoadFileId);
   __sJT_st.setInt(19,AssocLevel);
   __sJT_st.setDate(20,BatchDate);
   __sJT_st.setLong(21,OriginNode);
   __sJT_st.setString(22,EntryDesc);
   __sJT_st.setString(23,ED_MERCH_DEP);
   __sJT_st.setString(24,tranCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:441^13*/
          }
        }
        else
        {
          // insert the payment ACH entry
          /*@lineinfo:generated-code*//*@lineinfo:447^11*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_detail_test
//              (
//                transaction_code,
//                dfi_tr_number,
//                dfi_account_number,
//                post_date,
//                ach_amount,
//                merchant_number,
//                dba_name,
//                discretionary_data,
//                trace_number,
//                entry_class_code,
//                entry_description,
//                manual_description_id,
//                ach_sequence,
//                origin_node,
//                origin_id,
//                origin_name,
//                origin_tr_number,
//                destination_id,
//                destination_name,
//                company_id,
//                company_name,
//                file_id_modifier,
//                source_filename,
//                load_filename,
//                load_file_id                 
//              )
//              select  :tranCode,
//                      :TransitRouting,
//                      :DdaNum,
//                      :PostDate,
//                      abs( :netAmount ),
//                      :MerchantId,
//                      substr(:DbaName,1,22),
//                      null,               -- discretionary data
//                      :TraceNumber,
//                      nvl(:ClassCode,'CCD'),        -- entry class code
//                      nvl(:EntryDesc,:ED_MERCH_DEP),  -- entry desc
//                      :ManualDescriptionId,
//                      :AchSequence,
//                      ato.origin_node,
//                      atd.origin_id,
//                      atd.origin_name,
//                      atd.origin_tr_number,
//                      atd.destination_id,
//                      atd.destination_name,
//                      decode(adtc.debit_credit_indicator,
//                        'C', atd.company_id_credits,
//                        atd.company_id_debits),
//                      atd.company_name,
//                      null,               -- file id modifier
//                      :SourceFilename,      -- source of entry data
//                      :LoadFilename,        -- load filename
//                      :LoadFileId           -- load file id
//              from    ach_trident_origin  ato,
//                      ach_trident_destinations atd,
//                      ach_detail_tran_code adtc
//              where   ato.origin_node = :OriginNode
//                      and ato.origin_descriptor = nvl(:EntryDesc,:ED_MERCH_DEP)
//                      and ato.dest_id = atd.dest_id
//                      and :tranCode = adtc.ach_detail_trans_code
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_detail_test\n            (\n              transaction_code,\n              dfi_tr_number,\n              dfi_account_number,\n              post_date,\n              ach_amount,\n              merchant_number,\n              dba_name,\n              discretionary_data,\n              trace_number,\n              entry_class_code,\n              entry_description,\n              manual_description_id,\n              ach_sequence,\n              origin_node,\n              origin_id,\n              origin_name,\n              origin_tr_number,\n              destination_id,\n              destination_name,\n              company_id,\n              company_name,\n              file_id_modifier,\n              source_filename,\n              load_filename,\n              load_file_id                 \n            )\n            select   :1  ,\n                     :2  ,\n                     :3  ,\n                     :4  ,\n                    abs(  :5   ),\n                     :6  ,\n                    substr( :7  ,1,22),\n                    null,               -- discretionary data\n                     :8  ,\n                    nvl( :9  ,'CCD'),        -- entry class code\n                    nvl( :10  , :11  ),  -- entry desc\n                     :12  ,\n                     :13  ,\n                    ato.origin_node,\n                    atd.origin_id,\n                    atd.origin_name,\n                    atd.origin_tr_number,\n                    atd.destination_id,\n                    atd.destination_name,\n                    decode(adtc.debit_credit_indicator,\n                      'C', atd.company_id_credits,\n                      atd.company_id_debits),\n                    atd.company_name,\n                    null,               -- file id modifier\n                     :14  ,      -- source of entry data\n                     :15  ,        -- load filename\n                     :16             -- load file id\n            from    ach_trident_origin  ato,\n                    ach_trident_destinations atd,\n                    ach_detail_tran_code adtc\n            where   ato.origin_node =  :17  \n                    and ato.origin_descriptor = nvl( :18  , :19  )\n                    and ato.dest_id = atd.dest_id\n                    and  :20   = adtc.ach_detail_trans_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranCode);
   __sJT_st.setString(2,TransitRouting);
   __sJT_st.setString(3,DdaNum);
   __sJT_st.setDate(4,PostDate);
   __sJT_st.setDouble(5,netAmount);
   __sJT_st.setLong(6,MerchantId);
   __sJT_st.setString(7,DbaName);
   __sJT_st.setLong(8,TraceNumber);
   __sJT_st.setString(9,ClassCode);
   __sJT_st.setString(10,EntryDesc);
   __sJT_st.setString(11,ED_MERCH_DEP);
   __sJT_st.setInt(12,ManualDescriptionId);
   __sJT_st.setLong(13,AchSequence);
   __sJT_st.setString(14,SourceFilename);
   __sJT_st.setString(15,LoadFilename);
   __sJT_st.setLong(16,LoadFileId);
   __sJT_st.setLong(17,OriginNode);
   __sJT_st.setString(18,EntryDesc);
   __sJT_st.setString(19,ED_MERCH_DEP);
   __sJT_st.setString(20,tranCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^11*/
        }
        retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
      
        /*@lineinfo:generated-code*//*@lineinfo:515^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:518^9*/
      }          
    }
    catch( Exception e )
    {
      retVal = false;
      try{ /*@lineinfo:generated-code*//*@lineinfo:524^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:524^34*/ } catch( Exception ee ){}
      logEntry("store()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:526^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:526^32*/ } catch( Exception ee ){}
    }
    finally
    {
      setAutoCommit(autoCommit);  // return auto commit to original state
      cleanUp();
    }
    return( retVal );
  }
  
  private void _storeVector(Vector achEntries, boolean addStatementRecords, boolean testMode, String achTridentWhereClause)
  {
    StatementData   row                 = null;
    double          netAmount           = 0.0;
    double          sponsoredAmount     = 0.0;
    double          nonSponsoredAmount  = 0.0;
    String          tranCode            = "";
    
    try
    {
      connect(true);
      setAutoCommit(false);
      
      // store each entry
      for( int idx=0; idx<achEntries.size(); ++idx )
      {
        AchEntryData ed = (AchEntryData)achEntries.elementAt(idx);
        
        // reset amount variables
        netAmount           = 0.0;
        sponsoredAmount     = 0.0;
        nonSponsoredAmount  = 0.0;
        
        // calculate total amount and insert statement ACH entries
        for( int i=0; i < ed.StatementRows.size(); ++i )
        {
          row = (StatementData)ed.StatementRows.elementAt(i);
          netAmount += row.NetAmount;
          sponsoredAmount += row.SponsoredAmount;
          nonSponsoredAmount += row.NonSponsoredAmount;
          
          // do not create an entry in the deposit section 
          // of the statment for the billing ACH entry type
          if( !ED_BILLING.equals(ed.EntryDesc) && 
              !ED_MERCH_BILL.equals(ed.EntryDesc) &&
              addStatementRecords == true )
          {
            /*@lineinfo:generated-code*//*@lineinfo:573^13*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_statement
//                (
//                  merchant_number,
//                  post_date,
//                  post_date_actual,
//                  ach_amount,
//                  sponsored_amount,
//                  non_sponsored_amount,
//                  transaction_code,
//                  entry_description,
//                  sales_count,
//                  sales_amount,
//                  credits_count,
//                  credits_amount,
//                  daily_discount_amount,
//                  trace_number,
//                  ach_sequence,
//                  rec_id,
//                  load_filename
//                )
//                values
//                (
//                  :ed.MerchantId,
//                  :ed.PostDate,
//                  :ed.PostDate,
//                  abs( :row.NetAmount ),
//                  :sponsoredAmount,
//                  :nonSponsoredAmount,
//                  :row.TranCode,
//                  nvl(:ed.EntryDesc,:ED_MERCH_DEP),  -- entry desc
//                  :row.SalesCount,
//                  :row.SalesAmount,
//                  :row.CreditsCount,
//                  :row.CreditsAmount,
//                  0,                            -- daily discount amount
//                  :ed.TraceNumber,
//                  -1,                           -- ach_sequence to -1 to prevent double processing
//                  decode(:row.BatchNumber,0,null,:row.BatchNumber),
//                  :ed.LoadFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_statement\n              (\n                merchant_number,\n                post_date,\n                post_date_actual,\n                ach_amount,\n                sponsored_amount,\n                non_sponsored_amount,\n                transaction_code,\n                entry_description,\n                sales_count,\n                sales_amount,\n                credits_count,\n                credits_amount,\n                daily_discount_amount,\n                trace_number,\n                ach_sequence,\n                rec_id,\n                load_filename\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                abs(  :4   ),\n                 :5  ,\n                 :6  ,\n                 :7  ,\n                nvl( :8  , :9  ),  -- entry desc\n                 :10  ,\n                 :11  ,\n                 :12  ,\n                 :13  ,\n                0,                            -- daily discount amount\n                 :14  ,\n                -1,                           -- ach_sequence to -1 to prevent double processing\n                decode( :15  ,0,null, :16  ),\n                 :17  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ed.MerchantId);
   __sJT_st.setDate(2,ed.PostDate);
   __sJT_st.setDate(3,ed.PostDate);
   __sJT_st.setDouble(4,row.NetAmount);
   __sJT_st.setDouble(5,sponsoredAmount);
   __sJT_st.setDouble(6,nonSponsoredAmount);
   __sJT_st.setString(7,row.TranCode);
   __sJT_st.setString(8,ed.EntryDesc);
   __sJT_st.setString(9,ED_MERCH_DEP);
   __sJT_st.setInt(10,row.SalesCount);
   __sJT_st.setDouble(11,row.SalesAmount);
   __sJT_st.setInt(12,row.CreditsCount);
   __sJT_st.setDouble(13,row.CreditsAmount);
   __sJT_st.setLong(14,ed.TraceNumber);
   __sJT_st.setLong(15,row.BatchNumber);
   __sJT_st.setLong(16,row.BatchNumber);
   __sJT_st.setString(17,ed.LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^13*/
          }
          
          // update local BatchDate so that it can be stored in detail table
          BatchDate = row.BatchDate;
        }
        
        tranCode = (netAmount < 0.0) ? "27" : "22";
        
        if( testMode == false )
        {
          if( ed.RemitFees == true )
          {
            // add to special remittance table
            /*@lineinfo:generated-code*//*@lineinfo:629^13*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_remit
//                (
//                  hh_load_sec,
//                  merchant_number,
//                  invoice_amount
//                )
//                values
//                (
//                  :ed.RecId,
//                  :ed.MerchantId,
//                  abs(:netAmount)
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into monthly_extract_remit\n              (\n                hh_load_sec,\n                merchant_number,\n                invoice_amount\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                abs( :3  )\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ed.RecId);
   __sJT_st.setLong(2,ed.MerchantId);
   __sJT_st.setDouble(3,netAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^13*/
          }
          else
          {
            // insert the payment ACH entry
{
  // declare temps
  int paramIndex = 0;  
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append(SQL_INSERT_ACH_DETAIL);
   __sjT_sb.append(achTridentWhereClause);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.ach.AchEntryData:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(++paramIndex,tranCode);
   __sJT_st.setString(++paramIndex,ed.TransitRouting);
   __sJT_st.setString(++paramIndex,ed.DdaNum);
   __sJT_st.setDate(++paramIndex,ed.PostDate);
   __sJT_st.setDouble(++paramIndex,netAmount);
   __sJT_st.setDouble(++paramIndex,sponsoredAmount);
   __sJT_st.setDouble(++paramIndex,nonSponsoredAmount);
   __sJT_st.setLong(++paramIndex,ed.MerchantId);
   __sJT_st.setString(++paramIndex,ed.DbaName);
   __sJT_st.setLong(++paramIndex,ed.TraceNumber);
   __sJT_st.setString(++paramIndex,ed.ClassCode);
   __sJT_st.setString(++paramIndex,ed.EntryDesc);
   __sJT_st.setString(++paramIndex,ED_MERCH_DEP);
   __sJT_st.setInt(++paramIndex,ed.ManualDescriptionId);
   __sJT_st.setLong(++paramIndex,ed.AchSequence);
   __sJT_st.setString(++paramIndex,ed.SourceFilename);
   __sJT_st.setDouble(++paramIndex,netAmount);
   __sJT_st.setString(++paramIndex,ed.LoadFilename);
   __sJT_st.setLong(++paramIndex,ed.LoadFileId);
   __sJT_st.setInt(++paramIndex,ed.AssocLevel);
   __sJT_st.setDate(++paramIndex,ed.BatchDate);
   __sJT_st.setLong(++paramIndex,ed.BatchId);
   __sJT_st.setLong(++paramIndex,ed.BatchNumber);
   __sJT_st.setString(++paramIndex,ed.getPayoutRecordType());
   __sJT_st.setDouble(++paramIndex,netAmount);
   __sJT_st.setLong(++paramIndex,ed.OriginNode);
   __sJT_st.setString(++paramIndex,ed.EntryDesc);
   __sJT_st.setString(++paramIndex,ED_MERCH_DEP);
   __sJT_st.setString(++paramIndex,tranCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^13*/
          }
        }
        else
        {
          // insert into test table
          /*@lineinfo:generated-code*//*@lineinfo:741^11*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_detail_test
//              (
//                transaction_code,
//                dfi_tr_number,
//                dfi_account_number,
//                post_date,
//                ach_amount,
//                merchant_number,
//                dba_name,
//                discretionary_data,
//                trace_number,
//                entry_class_code,
//                entry_description,
//                manual_description_id,
//                ach_sequence,
//                origin_node,
//                origin_id,
//                origin_name,
//                origin_tr_number,
//                destination_id,
//                destination_name,
//                company_id,
//                company_name,
//                file_id_modifier,
//                source_filename,
//                load_filename,
//                load_file_id,
//                ach_run_date,
//                batch_id,
//                batch_number
//              )
//              select  :tranCode,
//                      :ed.TransitRouting,
//                      :ed.DdaNum,
//                      :ed.PostDate,
//                      abs( :netAmount ),
//                      :ed.MerchantId,
//                      substr(:ed.DbaName,1,22),
//                      null,               -- discretionary data
//                      :ed.TraceNumber,
//                      nvl(:ed.ClassCode,'CCD'),        -- entry class code
//                      nvl(:ed.EntryDesc,:ED_MERCH_DEP),  -- entry desc
//                      :ed.ManualDescriptionId,
//                      :ed.AchSequence,
//                      ato.origin_node,
//                      atd.origin_id,
//                      atd.origin_name,
//                      atd.origin_tr_number,
//                      atd.destination_id,
//                      atd.destination_name,
//                      decode(adtc.debit_credit_indicator,
//                        'C', atd.company_id_credits,
//                        atd.company_id_debits),
//                      atd.company_name,
//                      null,                 -- file id modifier
//                      :ed.SourceFilename,      -- source of entry data
//                      :ed.LoadFilename,        -- load filename
//                      :ed.LoadFileId,          -- load file id,
//                      :ed.BatchDate,
//                      :ed.BatchId,
//                      :ed.BatchNumber
//              from    ach_trident_origin   ato,
//                      ach_trident_destinations atd,
//                      ach_detail_tran_code adtc
//              where   ato.origin_node = :ed.OriginNode
//                      and ato.origin_descriptor = nvl(:ed.EntryDesc,:ED_MERCH_DEP)
//                      and ato.dest_id = atd.dest_id
//                      and :tranCode = adtc.ach_detail_trans_code
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_detail_test\n            (\n              transaction_code,\n              dfi_tr_number,\n              dfi_account_number,\n              post_date,\n              ach_amount,\n              merchant_number,\n              dba_name,\n              discretionary_data,\n              trace_number,\n              entry_class_code,\n              entry_description,\n              manual_description_id,\n              ach_sequence,\n              origin_node,\n              origin_id,\n              origin_name,\n              origin_tr_number,\n              destination_id,\n              destination_name,\n              company_id,\n              company_name,\n              file_id_modifier,\n              source_filename,\n              load_filename,\n              load_file_id,\n              ach_run_date,\n              batch_id,\n              batch_number\n            )\n            select   :1  ,\n                     :2  ,\n                     :3  ,\n                     :4  ,\n                    abs(  :5   ),\n                     :6  ,\n                    substr( :7  ,1,22),\n                    null,               -- discretionary data\n                     :8  ,\n                    nvl( :9  ,'CCD'),        -- entry class code\n                    nvl( :10  , :11  ),  -- entry desc\n                     :12  ,\n                     :13  ,\n                    ato.origin_node,\n                    atd.origin_id,\n                    atd.origin_name,\n                    atd.origin_tr_number,\n                    atd.destination_id,\n                    atd.destination_name,\n                    decode(adtc.debit_credit_indicator,\n                      'C', atd.company_id_credits,\n                      atd.company_id_debits),\n                    atd.company_name,\n                    null,                 -- file id modifier\n                     :14  ,      -- source of entry data\n                     :15  ,        -- load filename\n                     :16  ,          -- load file id,\n                     :17  ,\n                     :18  ,\n                     :19  \n            from    ach_trident_origin   ato,\n                    ach_trident_destinations atd,\n                    ach_detail_tran_code adtc\n            where   ato.origin_node =  :20  \n                    and ato.origin_descriptor = nvl( :21  , :22  )\n                    and ato.dest_id = atd.dest_id\n                    and  :23   = adtc.ach_detail_trans_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ach.AchEntryData",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranCode);
   __sJT_st.setString(2,ed.TransitRouting);
   __sJT_st.setString(3,ed.DdaNum);
   __sJT_st.setDate(4,ed.PostDate);
   __sJT_st.setDouble(5,netAmount);
   __sJT_st.setLong(6,ed.MerchantId);
   __sJT_st.setString(7,ed.DbaName);
   __sJT_st.setLong(8,ed.TraceNumber);
   __sJT_st.setString(9,ed.ClassCode);
   __sJT_st.setString(10,ed.EntryDesc);
   __sJT_st.setString(11,ED_MERCH_DEP);
   __sJT_st.setInt(12,ed.ManualDescriptionId);
   __sJT_st.setLong(13,ed.AchSequence);
   __sJT_st.setString(14,ed.SourceFilename);
   __sJT_st.setString(15,ed.LoadFilename);
   __sJT_st.setLong(16,ed.LoadFileId);
   __sJT_st.setDate(17,ed.BatchDate);
   __sJT_st.setLong(18,ed.BatchId);
   __sJT_st.setLong(19,ed.BatchNumber);
   __sJT_st.setLong(20,ed.OriginNode);
   __sJT_st.setString(21,ed.EntryDesc);
   __sJT_st.setString(22,ED_MERCH_DEP);
   __sJT_st.setString(23,tranCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:811^11*/
        }
        
        // commit after every 1000 rows
        if( idx%1000 == 0 )
        {
          log.debug("committing");
          commit();
        }
      }
      log.debug("final commit");
      commit();
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:826^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:826^33*/ } catch(Exception ee) {}
      logEntry("_storeVector()", e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:828^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:828^31*/ } catch(Exception ee) {}
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static void storeVector(Vector achEntries, boolean addStatementRecords, boolean testMode, String achTridentWhereClause)
  {
    // create object to manage database connection and storage
    AchEntryData ed = new AchEntryData();
    
    ed._storeVector(achEntries, addStatementRecords, testMode, achTridentWhereClause);
  }
}/*@lineinfo:generated-code*/