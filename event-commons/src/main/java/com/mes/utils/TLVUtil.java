package com.mes.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class TLVUtil {

	public static final String IS_HEX_STRING_WITH_SPACE = "^[A-Fa-f0-9 ]+$";
	public Map<Integer, Integer> tagSpaceMap = new HashMap<>();
	protected static List<Integer> tagTypeAN = Arrays.asList(0x84, 0x82, 0x91, 0x95, 0x9F33, 0x9F10, 0x9F37, 0x9F07, 0x9F34, 0x9F10, 0x9F5B, 0x9F09, 0x9F06, 0x9F1E, 0x9F26, 0x9F36);

	public static byte[] hexStringToByteArray(String emvData) {

		emvData = emvData.replaceAll("0x", "");
		emvData = emvData.trim();

		if (emvData.length() % 2 != 0) {
			throw new IllegalArgumentException("must contain an even number of bytes: '" + emvData + "'");
		}

		int len = emvData.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(emvData.charAt(i), 16) << 4) + Character.digit(emvData.charAt(i + 1), 16));
		}
		return data;
	}

	private Map<Integer, Integer> tlvSpaceCount(String emvData) {

		try {

			int spaceIndex = emvData.indexOf(' ');
			int tagLength = Integer.parseInt((emvData.substring(spaceIndex - 2, spaceIndex)), 16) * 2;
			int tagDataLengthIndex = tagLength + spaceIndex;
			String tempTagInfo = emvData.substring(spaceIndex, tagDataLengthIndex);
			int spaceCount = tempTagInfo.length() - tempTagInfo.replaceAll(" ", "").length();

			tagSpaceMap.put(getEmvTag(emvData, spaceIndex, 2), spaceCount);

			if (emvData.substring(tagDataLengthIndex).contains(" ")) {
				tlvSpaceCount(emvData.substring(tagDataLengthIndex));
			}
		}
		catch (Exception e) {
			throw new IllegalArgumentException("space in invalid Postion: '" + emvData + "'");

		}
		return tagSpaceMap;
	}

	public static Map<Integer, Integer> tlvSpaceCalculation(String emvData) {
		TLVUtil obj = new TLVUtil();
		return obj.tlvSpaceCount(emvData);
	}

	public static String tlvDataLeftPadding(Map<Integer, Integer> tlvDataMap, int tag, String orgData, char paddingChar) {

		if (tlvDataMap.containsKey(tag)) {
			orgData = StringUtils.leftPad(orgData.substring(tlvDataMap.get(tag)), orgData.length(), paddingChar);
		}

		return orgData;

	}

	public static int getEmvTag(String emvData, int spaceIndex, int tagSize) {

		int tag = Integer.parseInt(emvData.substring(spaceIndex - (tagSize + 2), spaceIndex - 2), 16);

		if (!tagTypeAN.contains(tag) && tagSize == 2)
			tag = getEmvTag(emvData, spaceIndex, 4);

		if (tagSize == 4 && !tagTypeAN.contains(tag)) {
			return 0;
		}

		return tag;
	}

}
