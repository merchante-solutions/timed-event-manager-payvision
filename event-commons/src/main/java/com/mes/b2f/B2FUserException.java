package com.mes.b2f;

public class B2FUserException extends Exception
{
  public B2FUserException(String message)
  {
    super( message );
  }
}
