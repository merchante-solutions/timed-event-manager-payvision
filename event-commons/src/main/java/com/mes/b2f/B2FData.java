/*************************************************************************

  FILE: $URL: $

  Description:  
  
  TCUserManager
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.b2f;

import java.sql.ResultSet;
import java.util.Hashtable;

public class B2FData
{
  private Hashtable challengeResponse = null;
  private String    welcomeText       = "";
  private boolean   userIdActivated   = false;
  
  public B2FData()
  {
  }
  
  public B2FData(ResultSet rs)
  {
    try
    {
      welcomeText = rs.getString("welcome_msg");
      
      challengeResponse = new Hashtable(3);
      
      challengeResponse.put( rs.getString("question_1"), rs.getString("answer_1") );
      challengeResponse.put( rs.getString("question_2"), rs.getString("answer_2") );
      challengeResponse.put( rs.getString("question_3"), rs.getString("answer_3") );
      
      userIdActivated = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), e.toString());
    }
  }
  
  public Hashtable getChallengeResponse()
  {
    return( challengeResponse );
  }
  
  public String getWelcomeText()
  {
    return( welcomeText );
  }
  
  public byte[] getWelcomeBlob()
  {                
    return( null );
  }
  
  public String getWelcomeBlobContentType()
  {
    return( "" );
  }
  
  public boolean isUserIdActivated()
  {
    return( userIdActivated );
  }
}