/*************************************************************************

  FILE: $URL: $

  Description:

  B2FSingleton

  Singleton that provides easy access to the shared B2F instance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-02 16:35:14 -0800 (Fri, 02 Mar 2007) $
  Version            : $Revision: 13506 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.b2f;

import org.apache.log4j.Logger;

public class B2FSingleton
{
  private static B2FSingleton   _instance     = null;
  
  protected boolean             initialized   = false;
  
  static Logger log = Logger.getLogger(B2FSingleton.class);
  
  public B2FSingleton()
  {
    initialized = true;
  }
  
  public static String getRealm()
  {
    return( new String("") );
  }
  
  public static B2F getB2fInstance()
    throws B2FInitializationException
  {
    B2F result = null;
    try
    {
      result = new B2F();
    }
    catch(Exception e)
    {
      throw new B2FInitializationException("Unable to get B2FInstance: " + e.toString());
    }
    
    return( result );
  }
}