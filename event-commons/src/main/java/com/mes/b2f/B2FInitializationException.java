package com.mes.b2f;

public class B2FInitializationException extends Exception
{
  public B2FInitializationException(String message)
  {
    super(message);
  }
}
