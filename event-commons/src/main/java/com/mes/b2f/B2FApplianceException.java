package com.mes.b2f;

public class B2FApplianceException extends Exception
{
  public B2FApplianceException(String message)
  {
    super(message);
  }
}
