/*@lineinfo:filename=ReportSQLJBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ReportSQLJBean.sqlj $

  Description:  
    Base class for report data beans
    
    This class should maintain any data that is common to all of the 
    possible report data beans.  The inheritors of this class should 
    maintain any page-specific report data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.HierarchyNodeField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesSystem;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ReportSQLJBean extends FieldBean
{
  //
  // OBSOLETE
  //
  // supported export file formats
  // *
  // Moved these constants to mesConstants.  These are left for legacy
  // support.    
  //
  //  DO NOT ADD NEW ITEMS HERE!  Add new constants to mesConstants.java
  //
  public static final int     FF_CSV                    = mesConstants.FF_CSV;
  public static final int     FF_PDF                    = mesConstants.FF_PDF;
  public static final int     FF_FIXED                  = mesConstants.FF_FIXED;
  public static final int     FF_TAB                    = mesConstants.FF_TAB;
  public static final int     FF_TAB_NOQUOTE            = mesConstants.FF_TAB_NOQUOTE;
  public static final int     FF_CSV_STATIC             = mesConstants.FF_CSV_STATIC;
  
  // report types
  public static final int     RT_INVALID                = -1;
  public static final int     RT_SUMMARY                = 0;
  public static final int     RT_DETAILS                = 1;
  public static final int     RT_USER                   = 2;
  
  // summary types
  public static final int     ST_PARENT                 = 0;
  public static final int     ST_CHILD                  = 1;

  public static final int     DISTRICT_UNASSIGNED       = -1;
  public static final int     DISTRICT_NONE             = 0;
  
  // hierarchy group indexes
  public static final int     H_GROUP_1                 = 0;
  public static final int     H_GROUP_2                 = 1;
  public static final int     H_GROUP_3                 = 2;
  public static final int     H_GROUP_4                 = 3;
  public static final int     H_GROUP_5                 = 4;
  public static final int     H_GROUP_6                 = 5;
  public static final int     H_GROUP_7                 = 6;
  public static final int     H_GROUP_8                 = 7;
  public static final int     H_GROUP_9                 = 8;
  public static final int     H_GROUP_10                = 9;
  public static final int     H_GROUP_COUNT             = 10;   // last
  
  // have a constant to use for empty text fields
  public static final String  EMPTY_TEXT_FIELD          = "";
  
  // common data items
  private boolean               Submit                  = false;
  
  // utility variables
  protected boolean             AjaxRequest             = false;
  protected boolean             ConvertCsvToXml         = false;
  protected boolean             DefaultReportDates      = false;
  protected int                 District                = 0;
  private Vector                ErrorList               = new Vector();
  protected StringBuffer        ErrorMessage            = new StringBuffer();
  protected boolean             FieldBeanSupportEnabled = false;
  protected boolean             FxDataIncluded          = false;
  private   ArrayList           HiddenSearchFields      = new ArrayList();
  protected int                 HierType                = MesHierarchy.HT_BANK_PORTFOLIOS;
  protected boolean             IgnoreDistricts         = false;
  protected Date                ReportDateBegin         = null;
  protected Date                ReportDateEnd           = null;
  protected HttpServletRequest  ServletRequest          = null;
  protected File                ReportFile              = null;
  protected long                ReportNodeId            = 0L;
  protected long                ReportNodeIdDefault     = 0L;
  protected Vector              ReportRows              = new Vector();
  protected int                 ReportType              = RT_INVALID;
  protected UserBean            ReportUserBean          = null;
  protected int                 SummaryType             = ST_PARENT;
  protected int                 XmlEncodingType         = -1;
  protected Vector              XmlFieldNames           = null;
  protected boolean             AppFilter               = false;
  protected long                AppFilterUserId         = -1L;
  
  // sub-classes
  public class ContactRecord
  {
    public String         Address1        = "";
    public String         Address2        = "";
    public String         BusinessPhone   = "";
    public String         City            = "";
    public String         ContactName     = "";
    public String         ContactPhone    = "";
    public String         State           = "";
    public String         Zip             = "";

    ContactRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      Address1      = resultSet.getString("addr1");
      Address2      = resultSet.getString("addr2");
      BusinessPhone = resultSet.getString("business_phone");
      City          = resultSet.getString("city");
      ContactName   = resultSet.getString("contact_name");
      ContactPhone  = resultSet.getString("contact_phone");
      State         = resultSet.getString("state");
      Zip           = resultSet.getString("zip");
    }                        
  }
  
  public class PortfolioEntry
  {
    public long           HierarchyNode     = -1L;
    public long           OrgId             = -1L;
    public String         OrgName           = "";
    
    PortfolioEntry( long orgId, String orgName, long node )
    {
      HierarchyNode   = node;
      OrgId           = orgId;
      OrgName         = orgName;
    }
  }
  
  public static class PortfolioTable extends DropDownTable
  {
    public PortfolioTable( long defaultNode )
    {
      ResultSetIterator   it          = null;
      ResultSet           rs          = null;
      
      try
      {
        this.connect();
        
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:198^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(th IDX_ANCESTOR) */
//                    o.org_group                            as hierarchy_node,
//                    ( o.org_group || ' - ' || o.org_name ) as org_name
//            from    t_hierarchy         th,
//                    portfolio_nodes     pn,
//                    organization        o
//            where   th.hier_type = 1 and
//                    th.ancestor = :defaultNode and
//                    pn.hierarchy_node = th.descendent and
//                    o.org_group = pn.hierarchy_node
//            order by org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(th IDX_ANCESTOR) */\n                  o.org_group                            as hierarchy_node,\n                  ( o.org_group || ' - ' || o.org_name ) as org_name\n          from    t_hierarchy         th,\n                  portfolio_nodes     pn,\n                  organization        o\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  pn.hierarchy_node = th.descendent and\n                  o.org_group = pn.hierarchy_node\n          order by org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,defaultNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^9*/
        rs = it.getResultSet();
        
        while (rs.next())
        {
          addElement(rs);
        }
      }
      catch (Exception e)
      {
        logEntry("PortfolioTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        this.cleanUp();
      }
    }
  }
  
  /*
  ** CONSTRUCTOR
  */  
  public ReportSQLJBean(boolean supportFieldBean)
  {
    setFieldBeanEnable(supportFieldBean);
    
    // make sure XSS Filtering is on
    setUseXSSFilter(true);
  }
  
  public ReportSQLJBean()
  {
    this(false);
  }
  
  public void addError(String error)
  {
    try
    {
      ErrorList.add(error);
    }
    catch(Exception e)
    {
      logEntry( "addError()", e.toString());
    }
  }
  
  public boolean allowParentLink()
  {
    boolean       retVal      = true;
    
    // if at default bank node and login node is the top, the do not
    // allow the parent link to be displayed.
    if ( ( getReportHierarchyNodeDefault() == HierarchyTree.DEFAULT_HIERARCHY_NODE ) &&
         ( getReportHierarchyNode() == 394100000 ) )
    {
      retVal = false;
    }         
    // if the user had reached their top node then stop
    else if ( getReportOrgId() == getReportOrgIdDefault() )
    {
      retVal = false;
    }
    return( retVal );
  }
  
  public String buildMethodName( String methodName, long orgId )
  {
    return( buildMethodName( methodName,orgId,null,null ) );
  }
  
  public String buildMethodName( String methodName, long orgId, Date beginDate )
  {
    return( buildMethodName( methodName,orgId,beginDate,null ) );
  }
  
  public String buildMethodName( String methodName, long orgId, Date beginDate, Date endDate )
  {
    StringBuffer      retVal = new StringBuffer( methodName );
    
    retVal.append("(");
    retVal.append(orgId);
    retVal.append(",");
    retVal.append(beginDate);
    retVal.append(",");
    retVal.append(endDate);
    retVal.append(")");
    
    return( retVal.toString() );
  }
  
  public void cleanUp()
  {
    try
    {
      ReportRows.removeAllElements();
      ErrorList.removeAllElements();
    }
    catch(Exception e)
    {
    }
    
    try
    {
      if ( ReportFile != null )
      {
        ReportFile.delete();
        ReportFile = null;
      }
    }
    catch(Exception e)
    {
    }      
    
    super.cleanUp();
  }
  
  protected void convertCsvToXml( StringBuffer line )
  {
    Vector          data              = new Vector();
    int             fieldIdx          = -1;
    String          fieldName         = null;
    String          fieldValue        = null;
    int             maxNameLen        = 0;
    
    splitCsvData(line,data);

    // determine the longest field name length    
    for ( int i = 0; i < XmlFieldNames.size(); ++i )
    {
      maxNameLen = Math.max(maxNameLen,((String)XmlFieldNames.elementAt(i)).length());
    }
    
    line.setLength(0);
    line.append("  <ReportRow>\n");
    for( int i = 0; i < data.size(); ++i )
    {
      fieldIdx    = (i % XmlFieldNames.size());
      fieldName   = encodeXmlValue((String)XmlFieldNames.elementAt(fieldIdx));
      fieldValue  = encodeXmlValue((String)data.elementAt(i));
      
      // if this line had multiple CSV rows in it
      // need to end the record and start a new one
      if ( i > 0 && fieldIdx == 0 ) 
      {
        line.append("  </ReportRow>\n");
        line.append("  <ReportRow>\n");
      }
      
      if ( XmlEncodingType == 0 )   // name/value
      {
        line.append("    <ReportColumn name=");
        line.append(StringUtilities.leftJustify("\"" + fieldName + "\"",maxNameLen+4,' '));
        line.append("value=\"");
        line.append(fieldValue);
        line.append("\"/>\n");
      }
      else
      {
        fieldName = fieldName.replaceAll(" ","");
        line.append("    <" + fieldName + ">");
        line.append(fieldValue);
        line.append("</" + fieldName + ">\n");
      }        
    }
    line.append("  </ReportRow>");
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup    = null;
    Field             field     = null;
    
    super.createFields(request);
    
    // set the class ajax request value
    AjaxRequest = HttpHelper.getBoolean(request,"ajax",false);
    
    XmlEncodingType = HttpHelper.getInt(request,"xmlEncoding",-1);
    ConvertCsvToXml = (XmlEncodingType == 0 || XmlEncodingType == 1);
    
    // set internal ServletRequest
    setServletRequest(request);
    
    if ( isFieldBeanEnabled() )
    { 
      fgroup = new FieldGroup("searchFields");
      fields.add(fgroup);
      
      field = new ComboDateField("beginDate", "Report Begin Date", true, false, 2000, false,true);
      ((ComboDateField)field).setExpansionFlag(false);
      fgroup.add(field);
    
      field = new ComboDateField("endDate", "Report End Date", true, false, 2000, false,true);
      ((ComboDateField)field).setExpansionFlag(false);
      fgroup.add(field);
    
      field = new HiddenField("reportType");
      field.setData( String.valueOf(RT_SUMMARY) );
      fgroup.add(field);
    
      long userNodeId = getUser().getHierarchyNode();
      if ( userNodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE && !isAjaxRequest() )
      {
        field = new DropDownField("portfolioId","Portfolio",new PortfolioTable(userNodeId),true);
        field.setData( String.valueOf(userNodeId) );
        fgroup.add(field);
      }
      
      field = new HiddenField("nodeId");
      field.setData( String.valueOf(userNodeId) );
      fgroup.add(field);
      
      if ( !isNodeMerchant(userNodeId) )
      {
        field = new HierarchyNodeField( Ctx, userNodeId, "zip2Node","Node ID", true );
        fgroup.add(field);
      }        
    
      field = new HiddenField("nodeIdDefault");
      field.setData( String.valueOf(userNodeId) );
      fgroup.add(field);
    
      field = new HiddenField("district");
      field.setData( String.valueOf(DISTRICT_NONE) );
      fgroup.add(field);
      
      // make sure these display nicely
      fgroup.setHtmlExtra("class=\"formFields\"");
    }      
  }
  
  public String decryptCardNumberFull( String cardNumberEnc, String cardNumber )
  {
    String    cardNumberFull  = null;
    
    try
    {
      if ( cardNumberEnc != null )
      {
//@        cardNumberFull = MesEncryption.decryptString(cardNumberEnc);
        /*@lineinfo:generated-code*//*@lineinfo:454^9*/

//  ************************************************************
//  #sql [Ctx] { select  dukpt_decrypt_wrapper(:cardNumberEnc) 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dukpt_decrypt_wrapper( :1 )  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardNumberEnc);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardNumberFull = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:458^9*/
      }          
    }
    catch( Exception e )
    {
      logEntry( "decryptCardNumberFull()", e.toString() );
    }
    return( ((cardNumberFull == null) ? cardNumber : cardNumberFull) );
  }
  
  public boolean encodeData( int fileFormat, String path, String filename )
  {
    JSONArray                     jsonArray = null;
    StringBuffer                  line      = new StringBuffer();
    BufferedWriter                out       = null;
    boolean                       retVal    = true;
    
    try
    {
      out = new BufferedWriter( new FileWriter( ( path + File.separator + filename + getDownloadFilenameExtension(fileFormat) ), false ) );
      
      // output the header
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          encodeHeaderCSV( line );
          break;
          
        case mesConstants.FF_FIXED:
          encodeHeaderFixed( line );        
          break;
          
        case mesConstants.FF_TAB:
          encodeHeaderTab( line );
          break;
          
        case mesConstants.FF_TAB_NOQUOTE:
          encodeHeaderTabNoQuote( line );
          break;
      }
      
      if ( line.length() > 0 )
      {
        out.write( line.toString() );
        out.newLine();
      }                
      
      // encode the file body
      for ( int i = 0; i < ReportRows.size(); ++i )
      {
        line.setLength(0);
        
        switch( fileFormat )
        {
          case mesConstants.FF_CSV:
            encodeLineCSV( ReportRows.elementAt(i), line );
            break;
            
          case mesConstants.FF_FIXED:
            encodeLineFixed( ReportRows.elementAt(i), line );
            break;
            
          case mesConstants.FF_TAB:
            encodeLineTab( ReportRows.elementAt(i), line );
            break;
            
          case mesConstants.FF_TAB_NOQUOTE:
            encodeLineTabNoQuote( ReportRows.elementAt(i), line);
            break;
            
          case mesConstants.FF_JSON:
            JSONObject jsonObj = encodeJSON( ReportRows.elementAt(i) );
            if ( jsonArray == null )
            { 
              jsonArray = new JSONArray(); 
            }
            if ( jsonObj != null )
            {
              jsonArray.add( jsonObj );
            }
            break;
            
          default:      // not supported
            retVal = false;
            break;
        }
        
        if ( line.length() > 0 )
        {
          out.write( line.toString() );
          out.newLine();
        }              
      }
      
      // encode the file footer
      line.setLength(0);
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          encodeTrailerCSV( line );
          break;
      
        case mesConstants.FF_FIXED:
          encodeTrailerFixed( line );
          break;
          
        case mesConstants.FF_TAB:
          encodeTrailerTab( line );
          break;          
          
        case mesConstants.FF_TAB_NOQUOTE:
          encodeTrailerTabNoQuote( line );
          break;          
          
        case mesConstants.FF_JSON:
          if ( jsonArray != null ) 
          { 
            line.append(jsonArray.toString());
          }
          break;
      }
      
      if ( line.length() > 0 )
      {
        out.write( line.toString() );
        out.newLine();
      }
      
    }
    catch( Exception e )
    {
      logEntry( "encodeData()",e.toString() );
      addError( "encodeData(): " + e.toString() );
      retVal = false;
    }      
    finally
    {
      try{ out.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }
  
  public boolean EncodeComplete = false;
  
  public void setEncodeComplete(boolean val)
  {
    EncodeComplete = val;
  }
  
  public ServletOutputStream encodeDataPrep( int fileFormat, HttpServletResponse response )
    throws java.io.IOException
  {
    StringBuffer            line      = null;
    ServletOutputStream     out       = response.getOutputStream();
    
    try
    {
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          response.setContentType("text/" + (ConvertCsvToXml ? "xml" : "csv"));
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
        
          // initialize the line buffer
          line = new StringBuffer("");
        
          // output header
          encodeHeaderCSV( line );
          if ( ConvertCsvToXml )
          {
            storeXmlFieldNames( line );
            out.print("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
            out.print( "<Report>\n");
          }
          else  // FF_CSV
          {
            out.print( line.toString() );
            out.print("\n" );
          }
          // flush buffer to inform browser that download has started
          out.flush();
          break;
          
        case mesConstants.FF_PDF:
          response.setContentType( "application/pdf" );
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
          break;
    
        case mesConstants.FF_FIXED:
          response.setContentType("text/plain");
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
          break;
          
        case mesConstants.FF_TAB:
          response.setContentType("text/tsv");
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
        
          // initialize the line buffer
          line = new StringBuffer("");
        
          // output header
          encodeHeaderTab( line );        
          out.print( line.toString() );
          out.print("\n" );
          
          // flush buffer to inform browser that download has started
          out.flush();
          break;
          
        case mesConstants.FF_TAB_NOQUOTE:
          response.setContentType("text/tsv");
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
        
          // initialize the line buffer
          line = new StringBuffer("");
        
          // output header
          encodeHeaderTabNoQuote( line );        
          out.print( line.toString() );
          out.print("\n" );
          
          // flush buffer to inform browser that download has started
          out.flush();
          break;
          
        case mesConstants.FF_CSV_STATIC:
          response.setContentType("text/csv");
          response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
          break;
      }
      
      setEncodeComplete(true);
    }
    catch(Exception e)
    {
      logEntry("encodeDataPrep()", e.toString());
    }
    
    return( out );
  }
  
  public boolean encodeData( int fileFormat, HttpServletResponse response )
    throws java.io.IOException
  {
    return( encodeData( fileFormat, response, null ) );
  }
  
  public boolean encodeData( int fileFormat, HttpServletResponse response, ServletOutputStream ros )
    throws java.io.IOException
  {
    StringBuffer            line      = null;
    ServletOutputStream     out       = null;
    BufferedReader          reader    = null;
    boolean                 retVal    = true;
    
    try
    {
      if( EncodeComplete )
      {
        // point to existing output stream
        out = ros;
      }
      else
      {
        out = encodeDataPrep( fileFormat, response );
        /*
        out       = response.getOutputStream();
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat) ) );
      
        // initialize the line buffer
        line = new StringBuffer("");
      
        // output header
        encodeHeaderCSV( line );        
        out.print( line.toString() );
        out.print("\n" );
        */
      }
      
      line = new StringBuffer("");
      int i = 0;
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          // initialize the line buffer
          for ( i = 0; i < ReportRows.size(); ++i )
          {
            encodeLineCSV( ReportRows.elementAt(i), line );
            if ( line.length() > 0 )
            {
              if ( ConvertCsvToXml )
              {
                convertCsvToXml(line);
              }
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          
          encodeTrailerCSV( line );
          if ( line.length() > 0 )
          {
            if ( ConvertCsvToXml )
            {
              convertCsvToXml(line);
            }
            out.print( line.toString() );
            out.print("\n" );
          }
          if ( ConvertCsvToXml )
          {
            out.print( "</Report>\n");
          }
          out.flush();
          break;
          
        case mesConstants.FF_TAB:
          // initialize the line buffer
          for ( i = 0; i < ReportRows.size(); ++i )
          {
            encodeLineTab( ReportRows.elementAt(i), line );
            if ( line.length() > 0 )
            {
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          
          encodeTrailerTab( line );
          if ( line.length() > 0 )
          {
            out.print( line.toString() );
            out.print("\n" );
          }              
          out.flush();
          break;
          
        case mesConstants.FF_TAB_NOQUOTE:
          // initialize the line buffer
          for ( i = 0; i < ReportRows.size(); ++i )
          {
            encodeLineTabNoQuote( ReportRows.elementAt(i), line );
            if ( line.length() > 0 )
            {
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          
          encodeTrailerTabNoQuote( line );
          if ( line.length() > 0 )
          {
            out.print( line.toString() );
            out.print("\n" );
          }              
          out.flush();
          break;
          
        case mesConstants.FF_PDF:
        {
          byte[] pdfData = encodePDF();
          out.write(pdfData,0,pdfData.length);
          break;
        }
        
        case mesConstants.FF_FIXED:
        {
          for ( i = 0; i < ReportRows.size(); ++i )
          {
            if ( line == null )
            {
              // initialize the line buffer
              line = new StringBuffer("");
              
              // first time, output header
              encodeHeaderFixed( line );        
              
              if(line != null && line.length() > 0)
              {
                out.print( line.toString() );
                out.print("\n" );
              }
            } 
            encodeLineFixed( ReportRows.elementAt(i), line );
            if ( line.length() > 0 )
            {
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          if ( line != null )
          {
            encodeTrailerFixed( line );
            if ( line.length() > 0 )
            {
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          break;
        }
        
        case mesConstants.FF_CSV_STATIC:   // pre-loaded download temp file
        {
          // open the report file
          reader = new BufferedReader( new FileReader( ReportFile ) );
        
          while( reader.ready() )
          {
            out.print( reader.readLine() );
            out.print("\n" );
          }
          reader.close();
          break;
        }
        
        case mesConstants.FF_JSON:
          JSONArray   jsonArray   = new JSONArray();
          for ( i = 0; i < ReportRows.size(); ++i )
          {
            JSONObject  jsonObj = encodeJSON( ReportRows.elementAt(i) );
            if ( jsonObj != null )
            {
              jsonArray.add( jsonObj );
            }
          }
          out.print( jsonArray.toString() );
          out.flush();
          break;
            
        default:      // not supported
          retVal = false;
          break;
      }
    }
    catch( Exception e )
    {
      logEntry( "encodeData()",e.toString() );
      addError( "encodeData(): " + e.toString() );
      retVal = false;
    }      
    finally
    {
      try{ reader.close(); } catch(Exception e) {}
    }
    return( retVal );
  }
  
  public String encodeFixedField( int length, int value )
  {
    int                 offset      = ((value < 0) ? 1 : 0);
    StringBuffer        retVal      = new StringBuffer("");
    
    retVal.append( value );
    while( retVal.length() < length )
    {
      // add leading zeros
      retVal.insert(offset,'0');
    }
    if ( retVal.length() > length )
    {
      retVal.setLength(length);
    }
    return( retVal.toString() );
  }
  
  public String encodeFixedField( int length, long longValue )
  {
    int                 offset      = ((longValue < 0L) ? 1 : 0);
    StringBuffer        retVal      = new StringBuffer("");
    
    retVal.append( longValue );
    while( retVal.length() < length )
    {
      // add leading zeros
      retVal.insert(offset,'0');
    }
    if ( retVal.length() > length )
    {
      retVal.setLength(length);
    }
    return( retVal.toString() );
  }
  
  public String encodeFixedField( int length, String value )
  {
    StringBuffer      retVal = new StringBuffer("");
    
    if ( value != null )
    {
      retVal.append(value);
    }
    while( retVal.length() < length )
    {
      // add leading zeros
      retVal.append(" ");
    }
    if ( retVal.length() > length )
    {
      retVal.setLength(length);
    }
    return( retVal.toString() );
  }
  
  public String encodeFixedField( int length, double value )
  {
    return( encodeFixedField( length, value, 0 ) );
  }
  
  public String encodeFixedField( int length, double value, int decimals )
  {
    int             offset    = ((value < 0.0) ? 1 : 0);
    StringBuffer    retVal    = new StringBuffer("");
    double          shift     = Math.pow(10, decimals);
    
    if ( shift != 0 )
    {
      value *= shift;
    }
    retVal.append( (int)value );
    while( retVal.length() < length )
    {
      // add leading zeros
      retVal.insert(offset,'0');
    }
    if ( retVal.length() > length )
    {
      retVal.setLength(length);
    }
    return( retVal.toString() );
  }
  
  public String encodeFixedField( int length, char fillChar )
  {
    StringBuffer        retVal    = new StringBuffer("");
    
    while( retVal.length() < length )
    {
      retVal.append(fillChar);
    }
    return( retVal.toString() );
  }
  
  public String encodeFixedField( Date value, String mask )
  {
    StringBuffer        retVal = new StringBuffer("");
    
    retVal.append( DateTimeFormatter.getFormattedDate(value,mask) );
    while( retVal.length() < mask.length() )
    {
      retVal.append(' ');
    }
    return( retVal.toString() );
  }
  
  // These should be overloaded by any class that supports
  // download via CSV.
  protected void encodeHeaderCSV( StringBuffer line ){ line.setLength(0); }
  protected void encodeLineCSV( Object record, StringBuffer line ){ line.setLength(0); }
  protected void encodeTrailerCSV( StringBuffer line ){ line.setLength(0); }
  
  // These should be overloaded by any class that supports
  // download via TAB.
  protected void encodeHeaderTab( StringBuffer line ){ line.setLength(0); }
  protected void encodeHeaderTabNoQuote( StringBuffer line ){ line.setLength(0); }
  protected void encodeLineTab( Object record, StringBuffer line ){ line.setLength(0); }
  protected void encodeLineTabNoQuote( Object record, StringBuffer line ){ line.setLength(0); }
  protected void encodeTrailerTab( StringBuffer line ){ line.setLength(0); }
  protected void encodeTrailerTabNoQuote( StringBuffer line ){ line.setLength(0); }
  
  // these should be overloaded by any class that supports
  // encoding its data in a fixed format.
  protected void encodeHeaderFixed( StringBuffer line ){ line.setLength(0); }
  protected void encodeLineFixed( Object record, StringBuffer line ){ line.setLength(0); }
  protected void encodeTrailerFixed( StringBuffer line ){ line.setLength(0); }
  
  // these should be overloaded by any class that supports
  // encoding its data as a JSON object
  protected JSONObject encodeJSON( Object obj ) {
    String messageType = HttpHelper.getString(request,"messageType","");
    JSONObject response = null;
    if("profileIds".equals(messageType) || "generalSettings".equals(messageType)) {
      response = new JSONObject();
      response.put("label", obj);
      response.put("value", obj);
    }
    return response;
  }
  
  public String encodeDataAsJSON()
  {
    JSONArray   jsonArray   = new JSONArray();
    Vector      rows        = getReportRows();
    
    for ( int i = 0; i < rows.size(); ++i )
    {
      JSONObject  jsonObj = encodeJSON( ReportRows.elementAt(i) );
      if ( jsonObj != null )
      {
        jsonArray.add( jsonObj );
      }
    }
    return( jsonArray.toString() );
  }

  public String encodeHierarchyNode( long nodeId )
  {
    StringBuffer          retVal      = new StringBuffer("");
    
    // add the node to the string value
    retVal.append(nodeId);
    
    // if the number of digits is more than 15, then
    // it needs to have a tick in front so that MS Excel
    // will treat it as a string instead of a number.
    // Excel cannot import numbers larger than 15 digits
    if ( retVal.length() > 15 )
    {
      retVal.insert(0,"\"'");
      retVal.append("\"");
    }
    return( retVal.toString() );
  }
  
  public void encodeMerchantUrl( StringBuffer buffer, long merchantId )
  {
    encodeMerchantUrl( buffer, merchantId, getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeMerchantUrl( StringBuffer buffer, long merchantId, Date reportDate )
  {
    encodeMerchantUrl( buffer, merchantId, reportDate, reportDate );
  }
  
  public void encodeMerchantUrl( StringBuffer buffer, long merchantId, Date beginDate, Date endDate )
  {
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("com.mes.MerchantId=");
    buffer.append(merchantId);
    encodeReportDate(buffer,beginDate,endDate);
  }
  
  public void encodeNodeUrl( StringBuffer buffer )
  {
    encodeNodeUrl( buffer, getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId )
  {
    encodeNodeUrl( buffer, nodeId, getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date reportDate )
  {
    encodeNodeUrl( buffer, nodeId, reportDate, reportDate );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    int hierType = getHierType();
  
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("nodeId=");
    buffer.append(nodeId);
    buffer.append("&com.mes.HierarchyNode=");
    buffer.append(nodeId);
    
    if ( hierType != MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      buffer.append("&hierType=");
      buffer.append(hierType);
    }
    encodeReportDate(buffer,beginDate,endDate);
  }
  
  public void encodeOrgUrl( StringBuffer buffer )
  {
    encodeOrgUrl( buffer, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeOrgUrl( StringBuffer buffer, long childOrgId )
  {
    encodeOrgUrl( buffer, childOrgId, getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeOrgUrl( StringBuffer buffer, long childOrgId, Date reportDate )
  {
    encodeOrgUrl( buffer, childOrgId, reportDate, reportDate );
  }
  
  public void encodeOrgUrl( StringBuffer buffer, long childOrgId, Date beginDate, Date endDate )
  {
    // encodeNodeUrl(...) is the single method that needs to be changed or 
    // overloaded to update the function of this method.
    encodeNodeUrl(buffer,orgIdToHierarchyNode(childOrgId),beginDate,endDate);
  }
  
  // These should be overloaded by any class that supports download as PDF.
  protected byte[] encodePDF( ){ return(null); }
  
  // takes data stored in the data bean and encodes it into the specified
  // format (if supported) in the path filename specified.
  public void encodeReportDate( StringBuffer buffer )
  {
    encodeReportDate( buffer, getReportDateBegin(), getReportDateEnd() );
  }
  
  public void encodeReportDate( StringBuffer buffer, Date reportDate )
  {
    encodeReportDate( buffer, reportDate, reportDate );
  }
  
  public void encodeReportDate( StringBuffer buffer, Date beginDate, Date endDate )
  {
    Calendar      cal   = Calendar.getInstance();
    
    if ( buffer != null )
    {
      if ( !buffer.toString().endsWith("&") && !buffer.toString().endsWith("?") )
      {
        buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
      }
      
      cal.setTime( beginDate );
      buffer.append( "beginDate.month=" );
      buffer.append( cal.get( Calendar.MONTH ) );
      buffer.append( "&beginDate.day=" );
      buffer.append( cal.get( Calendar.DAY_OF_MONTH ) );
      buffer.append( "&beginDate.year=" );
      buffer.append( cal.get( Calendar.YEAR ) );
      
      cal.setTime( endDate );
      buffer.append( "&endDate.month=" );
      buffer.append( cal.get( Calendar.MONTH ) );
      buffer.append( "&endDate.day=" );
      buffer.append( cal.get( Calendar.DAY_OF_MONTH ) );
      buffer.append( "&endDate.year=" );
      buffer.append( cal.get( Calendar.YEAR ) );
    }
  }
  
  protected String encodeXmlValue( String rawValue )
  {
    String      retVal      = rawValue;

    if ( retVal != null && retVal.trim().length() > 0 )
    {
      // replace the 5 pre-defined XML entity references
      retVal = retVal.replaceAll("<"   ,"&lt;"   );
      retVal = retVal.replaceAll(">"   ,"&gt;"   );
      retVal = retVal.replaceAll("&"   ,"&amp;"  );
      retVal = retVal.replaceAll("'"   ,"&apos;" );
      retVal = retVal.replaceAll("\""  ,"&quot;" );
    }
    return( retVal );
  }
  
  public boolean getAppFilter()
  {
    return( AppFilter );
  }
  
  public long getAppMerchantId( long appSeqNum )
  {
    long        merchantId  = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1227^7*/

//  ************************************************************
//  #sql [Ctx] { select  mr.merch_number 
//          from    merchant mr
//          where   mr.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.merch_number  \n        from    merchant mr\n        where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1232^7*/
    }
    catch( java.sql.SQLException e )
    {
      // merchant does not have an online
      // application, return 0L for the 
      // merchant number
    }
    return( merchantId );
  }
  
  public long getAppSeqNum( long merchantId )
  {
    long        appSeqNum = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1249^7*/

//  ************************************************************
//  #sql [Ctx] { select  mr.app_seq_num 
//          from    merchant mr
//          where   mr.merch_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.app_seq_num  \n        from    merchant mr\n        where   mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1254^7*/
    }
    catch( java.sql.SQLException e )
    {
      // merchant does not have an online
      // application, return 0L for the 
      // app sequence number.
    }
    return( appSeqNum );
  }
  
  public int getBankNumberFromNodeId( long nodeId )
  {
    int       bankNumber    = 0;
    String    nodeIdStr     = String.valueOf(nodeId);
    
    if ( nodeIdStr.length() >= 4 )
    {
      bankNumber = Integer.parseInt(nodeIdStr.substring(0,4));
    }
    return( bankNumber );
  }

  protected Vector getChildAssocIds( long orgId )
  {
    ResultSetIterator         it                = null;
    ResultSet                 resultSet         = null;
    Vector                    retVal            = new Vector();
    
    try
    {
      if ( getOrgMerchantId( orgId ) != 0L )
      {
        // since this is a merchant the association
        // is the parent of this orgId.
        long        assocId = 0L;
        /*@lineinfo:generated-code*//*@lineinfo:1290^9*/

//  ************************************************************
//  #sql [Ctx] { select org_num_to_hierarchy_node( parent_org_num ) 
//            from   parent_org
//            where  org_num = :orgId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select org_num_to_hierarchy_node( parent_org_num )  \n          from   parent_org\n          where  org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   assocId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1295^9*/
        retVal.add(assocId);
      }
      else    // this is a group or association in the hierarchy
      {
        /*@lineinfo:generated-code*//*@lineinfo:1300^9*/

//  ************************************************************
//  #sql [Ctx] it = { select assoc_number
//            from   group_assoc
//            where  group_number = org_num_to_hierarchy_node( :orgId )
//            order by assoc_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select assoc_number\n          from   group_assoc\n          where  group_number = org_num_to_hierarchy_node(  :1  )\n          order by assoc_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1306^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          retVal.add(resultSet.getLong("ASSOC_NUMBER"));
        }
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
    return( retVal );
  }
  
  // This method returns a vector of the org ids belonging
  // to the merchants under the specified node.
  protected Vector getChildMerchantOrgIds( long orgId )
  {
    Vector              children    = new Vector();
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1335^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    om.org_num    as org_num
//          from      group_merchant      gm,
//                    group_rep_merchant  grm,
//                    orgmerchant         om,
//                    organization        org
//          where     gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    om.org_merchant_num(+) = gm.merchant_number and
//                    org.org_num(+) = om.org_num
//          order by  org.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    om.org_num    as org_num\n        from      group_merchant      gm,\n                  group_rep_merchant  grm,\n                  orgmerchant         om,\n                  organization        org\n        where     gm.org_num =  :1  and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  om.org_merchant_num(+) = gm.merchant_number and\n                  org.org_num(+) = om.org_num\n        order by  org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1349^7*/
      resultSet = it.getResultSet();
    
      while(resultSet.next())
      {
        children.add(resultSet.getLong("org_num"));
      }
    }
    catch( Exception e )
    {
      logEntry( "getChildMerchantOrgIdsa(" + orgId + "): ", e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    return( children );
  }
  
  // This method will return a vector of Longs containing
  // the org ids of the immediate children of specified orgId
  protected Vector getChildOrgIds( long orgId )
  {
    Vector              children    = new Vector();
    ResultSetIterator   it          = null;
    long                merchantId  = 0L;
    ResultSet           resultSet   = null;
    
    try
    {
      // if this is a merchant, then it has
      // no children so make it a the only 
      // item in the children list.  This
      // allows the rest of the software to work 
      // correctly on leaf nodes of the hierarchy.
      if ( ( merchantId = getOrgMerchantId( orgId ) ) != 0L )
      {
        children.add(orgId);
      }
      else      // this is a non-leaf node, get its children
      {
        /*@lineinfo:generated-code*//*@lineinfo:1390^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    po.org_num
//            from      parent_org po,
//                      organization org
//            where     po.parent_org_num = :orgId and
//                      org.org_num       = po.org_num
//            order by  org.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    po.org_num\n          from      parent_org po,\n                    organization org\n          where     po.parent_org_num =  :1  and\n                    org.org_num       = po.org_num\n          order by  org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1398^9*/
        resultSet = it.getResultSet();
      
        while(resultSet.next())
        {
          children.add(resultSet.getLong("ORG_NUM"));
        }
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    return( children );
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        digits.append(raw.charAt(i));
      }
    }
    
    try
    {
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      result = 0L;
    }
    
    return result;
  }
  
  public String getDisplayName( )
  {
    return( getDisplayName( getReportHierarchyNode() ) );
  }
  
  public String getDisplayName( long node )
  {
    int               district        = 0;
    String            districtName    = null;
    StringBuffer      name            = new StringBuffer("");
    
    name.append( node );
    name.append( " - " );
    name.append( getNodeName(node) );
    
    if ( District != DISTRICT_NONE )
    {
      if ( District == DISTRICT_UNASSIGNED )
      {
        districtName = "Unassigned";
      }
      else
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1467^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(ad.district_desc,
//                          'District ' || to_char(:District, '0009'))   
//              from    assoc_districts   ad
//              where   ad.assoc_number(+) = :node and
//                      ad.district = :District
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(ad.district_desc,\n                        'District ' || to_char( :1 , '0009'))    \n            from    assoc_districts   ad\n            where   ad.assoc_number(+) =  :2  and\n                    ad.district =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,node);
   __sJT_st.setInt(3,District);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   districtName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1474^11*/
        }
        catch( java.sql.SQLException e )
        {
          districtName = "District " + District;
        }
      }
      name.append( " (" );
      name.append( districtName );
      name.append( ")" );
    }
    return( name.toString() );
  }
  
  public int getDistrict( )
  {
    int       retVal      = 0;
    
    retVal = getInt("district");
    if (retVal == 0)
    {
      retVal = District;
    }
    return( retVal );
  }
  
  /*
  ** FUNCTION getDownloadFilenameBase()
  *
  *   This method should be overloaded by report beans that support
  *   exporting report data via the download servlet.  It allows a
  *   bean to specify a download filename.
  */
  public String getDownloadFilenameBase()
  {
    return null;
  }
  
  /*
  ** FUNCTION getDownloadFilenameExtension()
  *
  *   This method should be overloaded by report beans that need to provide
  *   a different file extension than the default for the file format
  *
  */
  public String getDownloadFilenameExtension(int fileFormat)
  {
    String result = "";
    try
    {
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          result = (ConvertCsvToXml ? ".xml" : ".csv");
          break;
          
        case mesConstants.FF_TAB:
        case mesConstants.FF_TAB_NOQUOTE:
          result = ".tsv";
          break;
          
        case mesConstants.FF_PDF:
          result = ".pdf";
          break;
          
        case mesConstants.FF_FIXED:
          result = ".txt";
          break;
          
        case mesConstants.FF_CSV_STATIC:
          result = ".csv";
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("getDownloadFilenameExtension(" + fileFormat + ")", e.toString());
    }
    
    return result;
  }
  
  /*
  ** FUNCTION getErrors
  */
  public Vector getErrors()
  {
    return( ErrorList );
  }
  
  public int getHierType()
  {
    return( getInt("hierType",HierType) );
  }
  
  public boolean getIgnoreDistricts()
  {
    return( IgnoreDistricts );
  }
  
  public ContactRecord getMerchantContactData( long merchantId )
  {
    ContactRecord                   contactData = null;
    ResultSetIterator               it          = null;
    ResultSet                       resultSet   = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1581^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ( mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || mc.MERCHCONT_PRIM_LAST_NAME ) as contact_name,
//                   decode( mc.merchcont_prim_phone, null, null,  
//                           ( substr(mc.MERCHCONT_PRIM_PHONE,1,3) || '-' ||
//                             substr(mc.merchcont_prim_phone,4,3) || '-' ||
//                             substr(mc.merchcont_prim_phone,7,4) ) )        as contact_phone,
//                             substr(mf.phone_1,1,3) || '-' ||
//                             substr(mf.phone_1,4,3) || '-' ||
//                   substr(mf.phone_1,7,4)           as business_phone,
//                   mf.ADDR1_LINE_1                  as addr1,
//                   mf.ADDR1_LINE_2                  as addr2,                 
//                   mf.DMCITY                        as city,
//                   mf.DMSTATE                       as state,
//                   substr(mf.dmzip,1,5) || '-' ||
//                   substr(mf.dmzip,6,4)             as zip
//            from   merchant                       m,
//                   merchcontact                   mc,
//                   mif                            mf
//            where  mf.merchant_number = :merchantId and
//                   m.merch_number(+) = mf.merchant_number and
//                   mc.app_seq_num(+) = m.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ( mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || mc.MERCHCONT_PRIM_LAST_NAME ) as contact_name,\n                 decode( mc.merchcont_prim_phone, null, null,  \n                         ( substr(mc.MERCHCONT_PRIM_PHONE,1,3) || '-' ||\n                           substr(mc.merchcont_prim_phone,4,3) || '-' ||\n                           substr(mc.merchcont_prim_phone,7,4) ) )        as contact_phone,\n                           substr(mf.phone_1,1,3) || '-' ||\n                           substr(mf.phone_1,4,3) || '-' ||\n                 substr(mf.phone_1,7,4)           as business_phone,\n                 mf.ADDR1_LINE_1                  as addr1,\n                 mf.ADDR1_LINE_2                  as addr2,                 \n                 mf.DMCITY                        as city,\n                 mf.DMSTATE                       as state,\n                 substr(mf.dmzip,1,5) || '-' ||\n                 substr(mf.dmzip,6,4)             as zip\n          from   merchant                       m,\n                 merchcontact                   mc,\n                 mif                            mf\n          where  mf.merchant_number =  :1  and\n                 m.merch_number(+) = mf.merchant_number and\n                 mc.app_seq_num(+) = m.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1603^7*/
      resultSet     = it.getResultSet();
      
      if( resultSet.next() )
      {
        contactData = new ContactRecord( resultSet );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getMerchantContactData(" + merchantId + "): ", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( contactData );
  }
  
  // inline methods
  public long getMerchantGroup1( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_1) ); }
  public long getMerchantGroup2( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_2) ); }
  public long getMerchantGroup3( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_3) ); }
  public long getMerchantGroup4( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_4) ); }
  public long getMerchantGroup5( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_5) ); }
  public long getMerchantGroup6( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_6) ); }
  public long getMerchantGroup7( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_7) ); }
  public long getMerchantGroup8( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_8) ); }
  public long getMerchantGroup9( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_9) ); }
  public long getMerchantGroup10( long merchantId ) { return( getMerchantGroupId(merchantId,H_GROUP_10) ); }
  
  public long getMerchantGroupId( long merchantId, int groupNumber )
  {
    long[]          groupIds  = new long[H_GROUP_COUNT];
    long            retVal    = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1641^7*/

//  ************************************************************
//  #sql [Ctx] { select  g.group_1, g.group_2, g.group_3, g.group_4, g.group_5,
//                  g.group_6, g.group_7, g.group_8, g.group_9, g.group_10 
//          from    mif           mf,
//                  groups        g
//          where   mf.merchant_number = :merchantId and
//                  g.assoc_number = mf.association_node                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  g.group_1, g.group_2, g.group_3, g.group_4, g.group_5,\n                g.group_6, g.group_7, g.group_8, g.group_9, g.group_10  \n        from    mif           mf,\n                groups        g\n        where   mf.merchant_number =  :1  and\n                g.assoc_number = mf.association_node";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 10) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(10,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   groupIds[H_GROUP_1] = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_2] = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_3] = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_4] = __sJT_rs.getLong(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_5] = __sJT_rs.getLong(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_6] = __sJT_rs.getLong(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_7] = __sJT_rs.getLong(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_8] = __sJT_rs.getLong(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_9] = __sJT_rs.getLong(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   groupIds[H_GROUP_10] = __sJT_rs.getLong(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1659^7*/
      
      retVal = groupIds[groupNumber];
    }
    catch( Exception e )
    {
      // ignore
    }
    return( retVal );
  }
  
  public long getMerchantId( String profileId )
  {
    long  retVal      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1676^7*/

//  ************************************************************
//  #sql [Ctx] { select  tp.merchant_number 
//          from    trident_profile  tp
//          where   tp.terminal_id = :profileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tp.merchant_number  \n        from    trident_profile  tp\n        where   tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,profileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1681^7*/
      
    }
    catch( java.sql.SQLException e )
    {
      //ignore
    }
    
    return( retVal );
  }
  
  public int getNodeBankId( long nodeId )
  {
    int         retVal        = 0;
    int         rowCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1699^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( mf.bank_number ) 
//          from    mif   mf
//          where   mf.merchant_number = :nodeId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( mf.bank_number )  \n        from    mif   mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1704^7*/
      
      if ( rowCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1708^9*/

//  ************************************************************
//  #sql [Ctx] { select  mf.bank_number 
//            from    mif   mf
//            where   mf.merchant_number = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.bank_number  \n          from    mif   mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1713^9*/
      }
      else
      {
        retVal = Integer.parseInt( Long.toString( nodeId ).substring(0,4) );
         
        if ( retVal == 9999 )
        {
          retVal = mesConstants.BANK_ID_MES;
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getNodeBankId( " + nodeId + " )", e.toString() );
    }
    return( retVal );
  }
  
  public ContactRecord getNodeContactData( long nodeId )
  {
    ResultSetIterator it          = null;  
    ContactRecord     retVal      = null;
    ResultSet         resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1740^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ''                            as contact_name,
//                    o.org_address1                as addr1, 
//                    o.org_address2                as addr2, 
//                    o.org_city                    as city,
//                    o.org_state_area              as state,
//                    o.org_postal_code             as zip,
//                    o.org_general_phone_num       as business_phone,                 
//                    ''                            as contact_phone
//          from      organization        o
//          where     o.org_group = :nodeId          
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ''                            as contact_name,\n                  o.org_address1                as addr1, \n                  o.org_address2                as addr2, \n                  o.org_city                    as city,\n                  o.org_state_area              as state,\n                  o.org_postal_code             as zip,\n                  o.org_general_phone_num       as business_phone,                 \n                  ''                            as contact_phone\n        from      organization        o\n        where     o.org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1752^7*/
      resultSet = it.getResultSet();
      
      if( resultSet.next() )
      {
        retVal = new ContactRecord(resultSet);
      }
      resultSet.close();    
      it.close();
    }
    catch( Exception e )
    {
      logEntry("getNodeAddress("+nodeId+")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return( retVal );
  }
  
  public String getNodeName( long nodeId )
  {
    int             hierType    = getHierType();
    String          orgName     = "Not found";
    
    try
    {
      if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1782^9*/

//  ************************************************************
//  #sql [Ctx] { select org_name 
//            from   organization 
//            where  org_group = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select org_name  \n          from   organization \n          where  org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1787^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1791^9*/

//  ************************************************************
//  #sql [Ctx] { select  thn.name 
//            from    t_hierarchy_names   thn
//            where   thn.hier_type = :hierType and
//                    thn.hier_id = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  thn.name  \n          from    t_hierarchy_names   thn\n          where   thn.hier_type =  :1  and\n                  thn.hier_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierType);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1797^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( ( "getNodeName(" + nodeId + ")" ), e.toString() );
    }
    return( orgName );
  }
  
  public String getNodeUrl( String pageName )
  {
    return( getNodeUrl( pageName, getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() ) );
  }
  
  public String getNodeUrl( String pageName, long nodeId )
  {
    return( getNodeUrl( pageName, nodeId, getReportDateBegin(), getReportDateEnd() ) );
  }
  
  public String getNodeUrl( String pageName, long nodeId, Date reportDate )
  {
    return( getNodeUrl( pageName, nodeId, reportDate, reportDate ) );
  }
  
  public String getNodeUrl( String pageName, Date reportDate )
  {
    return( getNodeUrl( pageName, getReportHierarchyNode(), reportDate, reportDate ) );
  }
  
  public String getNodeUrl( String pageName, Date beginDate, Date endDate )
  {
    return( getNodeUrl( pageName, getReportHierarchyNode(), beginDate, endDate ) );
  }
  
  public String getNodeUrl( String pageName, long nodeId, Date beginDate, Date endDate )
  {
    StringBuffer        buffer = new StringBuffer( pageName );

    // encode the buffer with all the params for
    // the current node id.
    encodeNodeUrl( buffer, nodeId, beginDate, endDate );
    
    return( buffer.toString() );
  }
  
  public int getOrgBankId( long orgId )
  {
    long        merchantId      = 0L;
    int         retVal          = 0;
    
    try
    {
      if ( ( merchantId = getOrgMerchantId( orgId ) ) != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1852^9*/

//  ************************************************************
//  #sql [Ctx] { select bank_number 
//            from   mif
//            where  merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select bank_number  \n          from   mif\n          where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1857^9*/
      }
      else    // not a merchant, the data should be encoded in the hierarchy node
      {
        String  hierarchyNodeString = Long.toString( orgIdToHierarchyNode( orgId ) );
    
        retVal = Integer.parseInt( hierarchyNodeString.substring(0,4) );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( ( "getOrgBankId(" + orgId + ")" ), e.toString() );
    }
    return( retVal );
  }
  
  public long getOrgMerchantId( long orgId )
  {
    long        retVal = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1879^7*/

//  ************************************************************
//  #sql [Ctx] retVal = { values( org_merchant_id_query( :orgId ) )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OracleCallableStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN :1 := org_merchant_id_query(  :2  )  \n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleCall(__sJT_cc,"18com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
      __sJT_st.registerOutParameter(1,oracle.jdbc.OracleTypes.BIGINT);
   }
   // set IN parameters
   __sJT_st.setLong(2,orgId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
   // retrieve OUT parameters
   retVal = __sJT_st.getLong(1); if (__sJT_st.wasNull()) throw new sqlj.runtime.SQLNullException();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1882^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( ( "getOrgMerchantNumber(" + orgId + ")" ), e.toString() );
    }
    return( retVal );
  }
  
  public long getOrgId( )
  {
    return( getReportOrgId() );
  }
  
  public String getOrgName( long orgId )
  {
    long      nodeId    = getLong("nodeId",orgIdToHierarchyNode(orgId));
    
    return( getNodeName( nodeId ) );
  }

  public String getOrgUrl( String pageName )
  {
    return( getOrgUrl( pageName, getReportOrgId(), getReportDateBegin(), getReportDateEnd() ) );
  }
  
  public String getOrgUrl( String pageName, long orgId )
  {
    return( getOrgUrl( pageName, orgId, getReportDateBegin(), getReportDateEnd() ) );
  }
  
  public String getOrgUrl( String pageName, long orgId, Date reportDate )
  {
    return( getOrgUrl( pageName, orgId, reportDate, reportDate ) );
  }
  
  public String getOrgUrl( String pageName, Date reportDate )
  {
    return( getOrgUrl( pageName, getReportOrgId(), reportDate, reportDate ) );
  }
  
  public String getOrgUrl( String pageName, Date beginDate, Date endDate )
  {
    return( getOrgUrl( pageName, getReportOrgId(), beginDate, endDate ) );
  }
  
  public String getOrgUrl( String pageName, long orgId, Date beginDate, Date endDate )
  {
    StringBuffer        buffer = new StringBuffer( pageName );

    // encode the buffer with all the params for
    // the current org id.
    encodeOrgUrl( buffer, orgId, beginDate, endDate );
    
    return( buffer.toString() );
  }
  
  public long getParentOrgId( )
  {
    return getParentOrgId( getReportOrgId() );
  }
  
  public long getParentOrgId( long orgId )
  {
    int       hierType  = getHierType();
    long      retVal    = -1L;
    
    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      // bank portfolios hier type require conversion from org num 
      // to hierarchy node and back.
      retVal = hierarchyNodeToOrgId( getParentNode(orgIdToHierarchyNode(orgId)) );
    }
    else
    {
      // other hierarchy types do not use 
      // the organization table and therefore
      // orgId and nodeId are the same value
      retVal = getParentNode(orgId);
    }
    // only allow one place to do the actual hierarchy lookup
    return( retVal );
  }
  
  public long getParentNode( )
  {
    // only allow one place to do the actual hierarchy lookup
    return( getParentNode( getReportHierarchyNode() ) );
  }
  
  public long getParentNode( long nodeId )
  {
    int         hierType    = getHierType();
    long        retVal      = 0L;
    
    try
    {
      if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS && isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1981^9*/

//  ************************************************************
//  #sql [Ctx] { select  mf.association_node 
//            from    mif   mf
//            where   mf.merchant_number = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.association_node  \n          from    mif   mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1986^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1990^9*/

//  ************************************************************
//  #sql [Ctx] { select  th.ancestor 
//            from    t_hierarchy     th
//            where   th.hier_type = :hierType and
//                    th.descendent = :nodeId and
//                    th.relation = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  th.ancestor  \n          from    t_hierarchy     th\n          where   th.hier_type =  :1  and\n                  th.descendent =  :2  and\n                  th.relation = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierType);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1997^9*/
      }        
    }
    catch( java.sql.SQLException e )
    { 
      // no rows found, return 0L
    }
    catch( Exception e )
    {
      logEntry( e.toString(), "getParentNode(" + nodeId + ")" );
    }
    return( retVal );
  }
  
  public String getPcc( long merchantId )
  {
    String    retVal    = "";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2016^7*/

//  ************************************************************
//  #sql [Ctx] { select  mrs.pcc   
//          from    merchant        mr,
//                  merchant_sabre  mrs
//          where   mr.merch_number = :merchantId and
//                  mrs.app_seq_num = mr.app_seq_num                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mrs.pcc    \n        from    merchant        mr,\n                merchant_sabre  mrs\n        where   mr.merch_number =  :1  and\n                mrs.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2023^7*/
    }
    catch( java.sql.SQLException sqe )
    {
      // ignore, just return ""
    }
    catch( Exception e )
    {
      logEntry("getPcc(" + merchantId + ")",e.toString());
    }
    return( retVal );
  }
  
  public int getReportBankId( )
  {
    return( getNodeBankId( getReportHierarchyNode() ) );
  }
  
  public int getReportBankIdDefault( )
  {
    return( getNodeBankId( getReportHierarchyNodeDefault() ) );
  }
  
  public Date getReportDateBegin( )
  {
    Field         field     = getField("beginDate");
    Date          retVal    = null;
    
    if ( field != null )
    {
      retVal = ((ComboDateField)field).getSqlDate();
    }
    else
    {
      retVal = ReportDateBegin;
    }
    
    // never return null
    if ( retVal == null )
    {
      Calendar    cal = Calendar.getInstance();
      
      // if this request is coming from a report (ReportUserBean != null) and
      // the report user does not want the date to be defaulted to the current 
      // date then set default date to one day back
      if( ReportUserBean != null && !ReportUserBean.hasRight(MesUsers.RIGHT_REPORT_DEFAULT_CURR_DATE) )
      {
        cal.add(Calendar.DAY_OF_MONTH,-1);
      }
      retVal = new java.sql.Date( cal.getTime().getTime() );  
    }
    
    return( retVal );
  }
  
  public Date getReportDateEnd( )
  {
    Field         field     = getField("endDate");
    Date          retVal    = null;
    
    if ( field == null && isFieldBeanEnabled() )
    {
      // check for single date report
      field = getField("beginDate");   
    }
    
    if ( field != null )
    {
      retVal = ((ComboDateField)field).getSqlDate();
    }
    else
    {
      retVal = ReportDateEnd;
    }
    
    // if the value is still null, then
    // use the begin date as a default date
    if ( retVal == null )
    {
      retVal = getReportDateBegin();
    }
    
    return( retVal );
  }
  
  public int getReportDayCount()
  {
    int         retVal = 1;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2114^7*/

//  ************************************************************
//  #sql [Ctx] { select ((:getReportDateEnd() - :getReportDateBegin())+1)  
//          from   dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1254 = getReportDateEnd();
 java.sql.Date __sJT_1255 = getReportDateBegin();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select (( :1  -  :2 )+1)   \n        from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1254);
   __sJT_st.setDate(2,__sJT_1255);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2118^7*/
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public long getReportHierarchyNode( )
  {
    long        retVal      = 0L;
    
    retVal = getLong("nodeId");
    if ( retVal == 0L )
    {
      retVal = ReportNodeId;
    }
    return( retVal );
  }
  
  public long getReportHierarchyNodeDefault( )
  {
    long        retVal      = 0L;
    
    retVal = getLong("nodeIdDefault");
    if ( retVal == 0L )
    {
      retVal = ReportNodeIdDefault;
    }
    return( retVal );
  }
  
  public long getReportMerchantId(  )
  {
    int         recCount    = 0;
    long        merchantId  = 0L;
    
    try
    {
      merchantId = getReportHierarchyNode();
      
      /*@lineinfo:generated-code*//*@lineinfo:2159^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( merchant_number )  
//          from    mif     mf
//          where   mf.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( merchant_number )   \n        from    mif     mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2164^7*/
    
      if ( recCount == 0 )
      {
        // return the reports merchant id (returns 0L if ReportOrgId != Merchant)
        merchantId = 0L;
      }
    }
    catch( Exception e )
    {
      logEntry("getReportMerchantId()", e.toString());
    }      
    return( merchantId );
  }
  
  public long getReportOrgId( )
  {
    int       hierType  = getHierType();
    long      nodeId    = getReportHierarchyNode();
    long      retVal    = 0L;
    
    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      retVal = hierarchyNodeToOrgId(nodeId);
    }
    else
    {
      retVal = nodeId;
    }
    return( retVal );
  }
  
  public long getReportOrgIdDefault( )
  {
    int       hierType  = getHierType();
    long      nodeId    = getReportHierarchyNodeDefault();
    long      retVal    = 0L;
    
    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      retVal = hierarchyNodeToOrgId(nodeId);
    }
    else
    {
      retVal = nodeId;
    }
    
    return( retVal );
  }
  
  public String getReportOrgName( )
  {
    return( getNodeName( getReportHierarchyNode() ) );
  }
  
  public Vector getReportRows()
  {
    return( ReportRows );
  }
  
  public int getReportType( )
  {
    int       retVal    = RT_INVALID;
    
    if ( getField("reportType") != null )
    {
      retVal = getInt("reportType",RT_SUMMARY);
    }
    else
    {
      retVal = ReportType;
    }
    return( retVal );
  }
  
  public String getReportTitle()
  {
    return( "No Title Available" );
  }
  
  public Vector getSearchFieldsVector()
  {
    return( ((FieldGroup)getField("searchFields")).getFieldsVector() );
  }
  
  public boolean getSubmit()
  {
    return( Submit );
  }
  
  public int getSummaryType()
  {
    int       retVal    = getInt("summaryType");
    
    if ( retVal == 0 )
    {
      retVal = SummaryType;
    }
    return( retVal );
  }  
  
  
  public String getUserLoginId()
  {
    String     retVal    = "";
    
    if( ReportUserBean != null )
    {
      retVal = ReportUserBean.getUserLoginId();
    }
    return( retVal );
  }
  
  public boolean hasAssocDistricts( )
  {
    int         itemCount     = 0;
    
    try
    {
      if ( isOrgAssociation() )
      {
        long        assocId       = getReportHierarchyNode();
      
        /*@lineinfo:generated-code*//*@lineinfo:2287^9*/

//  ************************************************************
//  #sql [Ctx] { select nvl(count(ad.district),0)   
//            from    assoc_districts   ad
//            where   ad.assoc_number = :assocId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(count(ad.district),0)    \n          from    assoc_districts   ad\n          where   ad.assoc_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,assocId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2292^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("hasAssocDistricts()", e.toString());
    }
          
    return( itemCount > 0 );
  }
  
  public boolean hasErrors()
  {
    return(ErrorList.size() > 0);
  }
  
  public boolean hasFxData()
  {
    return( FxDataIncluded );
  }
  
  public void hideSearchField( String fname )
  {
    HiddenSearchFields.add(fname);
  }
  
  public long hierarchyNodeToOrgId( long hierarchyNode )
  {
    long      retVal = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2324^7*/

//  ************************************************************
//  #sql [Ctx] retVal = { values( hierarchy_node_to_org_num( :hierarchyNode ) )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OracleCallableStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN :1 := hierarchy_node_to_org_num(  :2  )  \n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleCall(__sJT_cc,"25com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
      __sJT_st.registerOutParameter(1,oracle.jdbc.OracleTypes.BIGINT);
   }
   // set IN parameters
   __sJT_st.setLong(2,hierarchyNode);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
   // retrieve OUT parameters
   retVal = __sJT_st.getLong(1); if (__sJT_st.wasNull()) throw new sqlj.runtime.SQLNullException();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2327^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("hierarchyNodeToOrgId()", e.toString());
    }
    return( retVal );
  }
  
  // can be overloaded to do data bean specific initialization
  // this method will be called by the JSP for all ReportSQLJBeans.
  public void initialize( )
  {
  }

  public void initReportFile( )
  {
    try
    {
      if ( ReportFile != null )
      {
        ReportFile.delete();
        ReportFile = null;
      }
      // create a temporary file to hold the report data
      // set the file to delete on the exit of the JVM.
      ReportFile = File.createTempFile("mesRpt",null); 
      ReportFile.deleteOnExit();
    }
    catch( java.io.IOException e )
    {
      logEntry("initReportFile()",e.toString());
    }
  }
    
  /***************************************************************************
  * Helper functions
  ***************************************************************************/
  public boolean isAjaxRequest( )
  {
    return( AjaxRequest );
  }
  
  public boolean isAppSubmittedByUser(long merchantNumber)
  {
    boolean result = false;
    
    try
    {
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:2378^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    group_rep_merchant
//          where   user_id = :AppFilterUserId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    group_rep_merchant\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppFilterUserId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2384^7*/
      
      result = (appCount > 0);
    }
    catch(Exception e)
    {
      logEntry("isAppSubmittedByUser(" + merchantNumber + ", " + ReportUserBean.getUserId() + ")", e.toString());
    }
    
    return result;
  }
  
  public boolean isEmail(String test)
  {
    boolean retVal = false;
    
    if(!isBlank(test) && test.indexOf('@') > 0)
    {
      retVal = true;
    }
    
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    return( false );    // download not supported by default
  }
  
  public boolean isFieldBeanEnabled( )
  {
    return( FieldBeanSupportEnabled );
  }
  
  public boolean isMerchantClosed( long merchantId )
  {
    int         count = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2423^7*/

//  ************************************************************
//  #sql [Ctx] { select count( mf.merchant_number ) 
//          from   mif mf
//          where  mf.merchant_number = :merchantId and
//                 nvl(mf.dmacctst,'A') in ( 'D','C','B' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( mf.merchant_number )  \n        from   mif mf\n        where  mf.merchant_number =  :1  and\n               nvl(mf.dmacctst,'A') in ( 'D','C','B' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2429^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( count != 0 );
  }
  
  public boolean isMerchantOnMesBackend( long merchantId )
  {
    int         count = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2442^7*/

//  ************************************************************
//  #sql [Ctx] { select count( mf.merchant_number ) 
//          from   mif mf
//          where  mf.merchant_number = :merchantId 
//                 and mf.processor_id = 1  -- mes back end
//                 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( mf.merchant_number )  \n        from   mif mf\n        where  mf.merchant_number =  :1  \n               and mf.processor_id = 1  -- mes back end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2449^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( count != 0 );
  }
  
  public boolean isNodeAssociation( )
  {
    return( isNodeAssociation( getReportHierarchyNode() ) );
  }
  
  public boolean isNodeAssociation( long nodeId )
  {
    int         count = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2467^7*/

//  ************************************************************
//  #sql [Ctx] { select count( g.assoc_number ) 
//          from   groups    g
//          where  g.assoc_number = :nodeId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( g.assoc_number )  \n        from   groups    g\n        where  g.assoc_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2472^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( count != 0 );
  }
  
  public boolean isNodeMerchant( )
  {
    return( isNodeMerchant( getReportHierarchyNode() ) );
  }
  
  public boolean isNodeMerchant( long nodeId )
  {
    int         count     = 0;
    int         hierType  = getHierType();
    
    try
    {
      if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2494^9*/

//  ************************************************************
//  #sql [Ctx] { select count( mf.merchant_number ) 
//            from   mif      mf
//            where  mf.merchant_number = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( mf.merchant_number )  \n          from   mif      mf\n          where  mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2499^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2503^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( th.descendent ) 
//            from    t_hierarchy    th
//            where   th.hier_type = :hierType and
//                    th.descendent = :nodeId and
//                    th.entity_type = 3 -- merchant  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( th.descendent )  \n          from    t_hierarchy    th\n          where   th.hier_type =  :1  and\n                  th.descendent =  :2  and\n                  th.entity_type = 3 -- merchant";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierType);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2510^9*/
      }        
    }
    catch( java.sql.SQLException e )
    {
    }
    return( count > 0 );
  }
  
  public boolean isNodeParentOfNode( long parentNode, long childNode )
  {
    int           recCount        = 0;
    
    try
    {
      if ( childNode == parentNode )
      {
        recCount = 1;
      }
      else
      {
        int hierType = getHierType();
      
        if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS && isNodeMerchant(childNode) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2535^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.ancestor) 
//              from    t_hierarchy     th,
//                      mif             mf
//              where   th.hier_type  = :hierType and
//                      th.ancestor   = :parentNode and
//                      mf.association_node = th.descendent and
//                      mf.merchant_number = :childNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.ancestor)  \n            from    t_hierarchy     th,\n                    mif             mf\n            where   th.hier_type  =  :1  and\n                    th.ancestor   =  :2  and\n                    mf.association_node = th.descendent and\n                    mf.merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierType);
   __sJT_st.setLong(2,parentNode);
   __sJT_st.setLong(3,childNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2544^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2548^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.ancestor) 
//              from    t_hierarchy     th
//              where   th.hier_type = :hierType and
//                      th.ancestor = :parentNode and
//                      th.descendent = :childNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.ancestor)  \n            from    t_hierarchy     th\n            where   th.hier_type =  :1  and\n                    th.ancestor =  :2  and\n                    th.descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierType);
   __sJT_st.setLong(2,parentNode);
   __sJT_st.setLong(3,childNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2555^11*/
        }        
      }
    }
    catch( Exception e )
    {
      logEntry("isNodeParentOfNode(" + parentNode + "," + childNode + ")", e.toString());
    }      
    
    return( recCount > 0 );
  }
  
  public boolean isNodeParentOfOrg( long parentNode, long childOrgId )
  {
    long      childNode   = childOrgId;
    
    if ( getHierType() == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      childNode = orgIdToHierarchyNode(childOrgId);
    }
    return( isNodeParentOfNode( parentNode, childNode ) );
  }
  
  public boolean isNumber(String test)
  {
    boolean retVal = false;
    
    try
    {
      long i = Long.parseLong(test);
      retVal = true;
    }
    catch(Exception e)
    {
    }
    
    return( retVal );
  }
  
  public boolean isOrgAssociation( )
  {
    return( isNodeAssociation( getReportHierarchyNode() ) );
  }
  
  public boolean isOrgAssociation( long orgId )
  {
    return( isNodeAssociation( orgIdToHierarchyNode(orgId) ) );
  }
  
  public boolean isOrgBank( long orgId )
  {
    int         retVal      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2610^7*/

//  ************************************************************
//  #sql [Ctx] { select decode( mod(o.org_group,100000),0,1,0 ) 
//          from   organization     o
//          where  o.org_num = :orgId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select decode( mod(o.org_group,100000),0,1,0 )  \n        from   organization     o\n        where  o.org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2615^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "isOrgBank()", e.toString() );
    }
    return( (retVal == 1) );
  }
  
  public boolean isOrgMerchant( )
  {
    return( isNodeMerchant( getReportHierarchyNode() ) );
  }
  
  public boolean isOrgMerchant( long orgId )
  {
    return( isNodeMerchant( orgIdToHierarchyNode(orgId) ) );
  }
  
  public boolean isOrgParentOfNode( long parentOrgId, long childNode )
  {
    long    parentNode = parentOrgId;
    
    if ( getHierType() == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      parentNode = orgIdToHierarchyNode( parentOrgId );
    }
    return( isNodeParentOfNode( parentNode, childNode ) );
  }
  
  public boolean isOrgParentOfOrg( long parentOrgId, long childOrgId )
  {
    long    childNode     = childOrgId;
    long    parentNode    = parentOrgId;
    
    if ( getHierType() == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      childNode     = orgIdToHierarchyNode( childOrgId );
      parentNode    = orgIdToHierarchyNode( parentOrgId );
    }
    return( isNodeParentOfNode(parentNode,childNode) );
  }
  
  public boolean isSameMonthYear( Date beginDate, Date endDate )
  {
    // compare the beginning date month and year with
    // the end date month and year values.  If they match,
    // then the report is for only one month so skip the
    // end date range.
    Calendar        cal     = Calendar.getInstance();
    boolean         retVal  = false;
    int             month   = -1;
    int             year    = -1;
    
    // extract the month and year from the begin date
    cal.setTime( beginDate );
    month = cal.get( Calendar.MONTH );
    year  = cal.get( Calendar.YEAR  );
    
    // setup for the comparison
    cal.setTime( endDate );
    
    // compare the month and year. 
    if ( ( month == cal.get( Calendar.MONTH ) ) &&
         ( year  == cal.get( Calendar.YEAR  ) ) )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public boolean isSearchFieldHidden( String fname )
  {
    return( HiddenSearchFields.contains(fname) );
  }
  
  public boolean isSubmitted()
  {
    return( Submit );
  }
  
  public boolean isTridentProfileId( String profileId )
  {
    int         recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2702^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(tp.terminal_id) 
//          from    trident_profile   tp
//          where   tp.terminal_id = :profileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tp.terminal_id)  \n        from    trident_profile   tp\n        where   tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,profileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2707^7*/
    }
    catch( Exception e )
    {
      logEntry("isTridentProfileId(" + profileId + ")", e.toString());
    }
    return( recCount > 0 );
  }
  
  public boolean isUserMerchant()
  {
    return( ReportUserBean.getMerchId() > 0 );
  }
  
  /*
  ** FUNCTION loadData
  *
  *   This method is overloaded by individual reports beans (if necessary)
  *   to read the data from the database into the bean.
  */
  public void loadData( )
  {
	  loadData( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
		//
		// String messageType = HttpHelper.getString(request,"messageType","");
		// if("profileIds".equals(messageType)) {
		// getProfileIds( HttpHelper.getLong(request,"nodeId",0L) );
		// //getProfileIds( getReportHierarchyNode()) ;
		// } else {
		// loadData( getReportOrgId(), getReportDateBegin(), getReportDateEnd()
		// );
		// }
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
  }
  
  public String loadDataAsJSON()
  {
    return("");
  }
  
  public Vector loadPortfolioList( )
  {
    long                  defaultNode = ReportUserBean.getHierarchyNode();
    ResultSetIterator     it          = null;
    Vector                portfolios  = new Vector();
    ResultSet             resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2759^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(th IDX_ANCESTOR) */
//                  distinct o.org_num                     as org_num, 
//                  ( o.org_group || ' - ' || o.org_name ) as org_name,
//                  o.org_group                            as hierarchy_node
//          from    t_hierarchy         th,
//                  portfolio_nodes     pn,
//                  organization        o
//          where   th.hier_type = 1 and
//                  th.ancestor = :defaultNode and
//                  pn.hierarchy_node = th.descendent and
//                  o.org_group = pn.hierarchy_node
//          order by org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(th IDX_ANCESTOR) */\n                distinct o.org_num                     as org_num, \n                ( o.org_group || ' - ' || o.org_name ) as org_name,\n                o.org_group                            as hierarchy_node\n        from    t_hierarchy         th,\n                portfolio_nodes     pn,\n                organization        o\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                pn.hierarchy_node = th.descendent and\n                o.org_group = pn.hierarchy_node\n        order by org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,defaultNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2773^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        portfolios.addElement( new PortfolioEntry( resultSet.getLong("org_num"),
                                                   resultSet.getString("org_name"),
                                                   resultSet.getLong("hierarchy_node") ) );
      }
      
      // if the user is assigned to the top of the hierarchy, then add
      // a node for the top (9999999999)
      if ( defaultNode == HierarchyTree.DEFAULT_HIERARCHY_NODE )
      {
        portfolios.addElement( new PortfolioEntry( hierarchyNodeToOrgId( HierarchyTree.DEFAULT_HIERARCHY_NODE ), 
                                                   Long.toString(HierarchyTree.DEFAULT_HIERARCHY_NODE) + " - Bank Portfolios",
                                                   HierarchyTree.DEFAULT_HIERARCHY_NODE ) );
      }
    }
    catch( java.sql.SQLException e )
    {
     logEntry( "loadPortfolioList( ): ", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }      
    return( portfolios );
  }
  
  public long orgIdToHierarchyNode( long orgId )
  {
    long        retVal = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2809^7*/

//  ************************************************************
//  #sql [Ctx] retVal = { values( org_num_to_hierarchy_node( :orgId ) )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OracleCallableStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN :1 := org_num_to_hierarchy_node(  :2  )  \n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleCall(__sJT_cc,"37com.mes.reports.ReportSQLJBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
      __sJT_st.registerOutParameter(1,oracle.jdbc.OracleTypes.BIGINT);
   }
   // set IN parameters
   __sJT_st.setLong(2,orgId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
   // retrieve OUT parameters
   retVal = __sJT_st.getLong(1); if (__sJT_st.wasNull()) throw new sqlj.runtime.SQLNullException();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2812^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "orgIdToHierarchyNode()", e.toString());
    }
    return( retVal );
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    Field           field           = null;
    java.util.Date  javaDate        = null;
    long            nodeId          = 0L;
    long            portfolioId     = 0L;
    
    super.postHandleRequest( request );
    
    //
    // Date extraction override:
    //
    //    if reportDateXXX params exist in the request override the 
    //    individual params (separate param for month day and year)
    //    extracted during the autoSetFields method.
    //
    if ( (javaDate = HttpHelper.getDate(request,"reportDateBegin","MM/dd/yyyy",null)) != null )
    {
      setReportDateBegin( new java.sql.Date( javaDate.getTime() ) );
    }
    if ( (javaDate = HttpHelper.getDate(request,"reportDateEnd","MM/dd/yyyy",null)) != null )
    {
      setReportDateEnd( new java.sql.Date( javaDate.getTime() ) );
    }
    
    // if a begin date was not provided by the user, then use default date
    if ( HttpHelper.getString(request,"beginDate.month",null) == null &&
         HttpHelper.getDate(request,"reportDateBegin","MM/dd/yyyy",null) == null )
    {
      DefaultReportDates = true;
    }
    
    // check for old style params coming from one of the CIF related screens
    if ( (nodeId = HttpHelper.getLong(request,"com.mes.HierarchyNode",0L)) != 0L &&
         (HttpHelper.getString(request,"nodeId",null) == null) )
    {
      setData("nodeId",String.valueOf(nodeId));
    }
    
    Submit = HttpHelper.getBoolean(request,"submitted",false);
    
    field = getField("zip2Node");
    
    if ( field != null && !field.isBlank() )
    {
      if ( field.isValid() )
      {
        // zip2node overrides node id value
        setData("nodeId",field.getData());
        
        // slave portfolio id to zip2node field
        setData("portfolioId",field.getData());
      }        
    }
    else if ( (portfolioId = HttpHelper.getLong(request,"portfolioId",0L)) != 0L )
    {
      if ( isNodeParentOfNode( getReportHierarchyNodeDefault(), portfolioId ) )
      {
        setData("nodeId",String.valueOf(portfolioId));
      }        
    }
    else 
    {
      // default the porfolio drop down to the current
      // report node.
      setData("portfolioId",getData("nodeId"));
    }
  }
  
  public void setFieldBeanEnable( boolean enable )
  {
    FieldBeanSupportEnabled = enable;
  }
  
  protected void setFxDataIncluded( boolean fxDataIncluded )
  {
    FxDataIncluded = fxDataIncluded;
  }
  
  /*
  ** FUNCTION setServletRequest
  *
  *   This method sets the internal ServletRequest variable.  This
  *   can be used to view request parameters later.
  */
  public void setServletRequest(HttpServletRequest request)
  {
    ServletRequest = request;
  }
  
  /*
  ** FUNCTION setProperties
  *
  *   This method should be overloaded by report beans that support
  *   exporting report data via the download servlet.  It allows a
  *   bean to set internal members from the attributes in a 
  *   HttpServletRequest.
  */
  public void setProperties(HttpServletRequest request)
  {
    if ( isFieldBeanEnabled() )
    {
      autoSetFields(request);   // stubbed so download servlet works
    }
    else    // use the older class variable method
    {
      Calendar        cal                       = Calendar.getInstance();
      long            hierarchyNode             = 0L;
      java.util.Date  javaDate                  = null;
      long            merchantId                = 0L;
      boolean         usingDefaultHierarchyNode = false;
    
      // determine if this is an ajax request
      AjaxRequest = HttpHelper.getBoolean(request,"ajax",false);
      
      XmlEncodingType = HttpHelper.getInt(request,"xmlEncoding",-1);
      ConvertCsvToXml = (XmlEncodingType == 0 || XmlEncodingType == 1);
      
      // set internal ServletRequest
      setServletRequest(request);
    
      // register the HttpRequest with big brother
      MesSystem.registerRequest(this,ReportUserBean.getUserLoginId(),request);
    
      // get the report type, default to summary level reporting
      HierType        = HttpHelper.getInt(request,"hierType",MesHierarchy.HT_BANK_PORTFOLIOS);
      ReportType      = HttpHelper.getInt(request,"reportType",RT_SUMMARY );
      District        = HttpHelper.getInt(request,"district", DISTRICT_NONE );
      IgnoreDistricts = HttpHelper.getBoolean(request,"ignoreDistricts",false);
    
      // setup the new default hierarchy if necessary
      if ( HierType != MesHierarchy.HT_BANK_PORTFOLIOS )
      {
        ReportNodeIdDefault  = ReportUserBean.getHierarchyNode(HierType);
      }
    
      // try to get the hierarchy node first    
      if ( ( hierarchyNode = HttpHelper.getLong(request,"com.mes.HierarchyNode",0L) ) != 0L )
      {
        setReportHierarchyNode(hierarchyNode);
      }
      // next try to get a merchant number
      else if ( ( merchantId = HttpHelper.getLong(request,"com.mes.MerchantId",0L) ) != 0L )
      {
        setReportHierarchyNode(merchantId);
      }
      // next try to the the nodeId param, introduce with FieldBean support
      else if ( ( hierarchyNode = HttpHelper.getLong(request,"nodeId",0L) ) != 0L )
      {
        setReportHierarchyNode(hierarchyNode);
      }
      // default is to accept an Org Id
      else 
      {
        // extract the report org id (this method called by the
        // download servlet).
        long orgId = HttpHelper.getLong(request,"com.mes.OrgId", -1L );
      
        if ( orgId == -1L )
        {
          // no org data available in the request, 
          // so set the current node to the login node.
          setReportHierarchyNode(ReportNodeIdDefault);
        
          // set the flag - required because of the 
          // possibility that ReportOrgId will not
          // be equal to the default now (see next line)
          usingDefaultHierarchyNode = true;
        
          // if the user logged in at the top of the hierarchy tree,
          // then force them down to the default bank level.
          if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
          {
            setReportHierarchyNode( 394100000 );
          }
        }
        else
        {
          setReportHierarchyNode( orgIdToHierarchyNode(orgId) );
        }
      }
    
      if ( ! isNodeParentOfNode( ReportNodeIdDefault, ReportNodeId ) ||
            (
              AppFilter && isNodeMerchant() && ! isAppSubmittedByUser(getReportHierarchyNode())
            ) 
         )
      {
        StringBuffer      msg = new StringBuffer();
      
        msg.append( "Requested node, " );
        msg.append( getReportHierarchyNode() );
        msg.append( ", is not a child of " );
        msg.append( getReportHierarchyNodeDefault() );
      
        addError( msg.toString() );
        setReportHierarchyNode(ReportNodeIdDefault);
      }
    
      // switch to parent summary mode whenever
      // the user is at their default hierarchy node
      if ( ( ReportNodeId == ReportNodeIdDefault ) ||
           ( usingDefaultHierarchyNode == true ) )
      {
        // support for both parent and child view when 
        // at the top level (login level).
        SummaryType = HttpHelper.getInt(request,"summaryType",ST_PARENT);
      }
      else  // default is child mode below the login level
      {
        SummaryType = HttpHelper.getInt(request,"summaryType",ST_CHILD);
      }
    
      //
      // Date extraction technique:
      //
      //    1) Attempt to get the reportDateXXX param from the request.  This
      //       must be in the MM/dd/yyyy format.
      //    2) If the reportDateXXX is not available, try loading the form
      //       parameters (separate param for month day and year)
      //    3) Finally, default to present date and set the using defaults flag.
      //
      javaDate = HttpHelper.getDate(request,"reportDateBegin","MM/dd/yyyy",null);
      if ( javaDate != null )
      {
        setReportDateBegin( new java.sql.Date( javaDate.getTime() ) );
      }
      else    // (javaDate == null)
      {
        try
        {
          // set from date
          cal.set( Calendar.MONTH,        Integer.parseInt(request.getParameter("beginDate.month")));
          cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt(request.getParameter("beginDate.day")) );
          cal.set( Calendar.YEAR,         Integer.parseInt(request.getParameter("beginDate.year")));
          setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        }
        catch (Exception e)
        {
          ReportDateBegin     = new java.sql.Date( Calendar.getInstance().getTime().getTime() );  // today
          DefaultReportDates  = true;
        }     
      }
       
      javaDate = HttpHelper.getDate(request,"reportDateEnd","MM/dd/yyyy",null);
      if ( javaDate != null )
      {
        setReportDateEnd( new java.sql.Date( javaDate.getTime() ) );
      }
      else    // (javaDate == null)
      {
        try
        {
          // set to date
          cal.set( Calendar.MONTH,        Integer.parseInt(request.getParameter("endDate.month")));
          cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt(request.getParameter("endDate.day")));
          cal.set( Calendar.YEAR,         Integer.parseInt(request.getParameter("endDate.year")));
          setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
        }
        catch (Exception e)
        {
          ReportDateEnd = new java.sql.Date( Calendar.getInstance().getTime().getTime() );  // today
          DefaultReportDates  = true;
        }
      }
    }      
  }
  
  public void setReportDateBegin( Date newDate )
  {
    ReportDateBegin = newDate;  // legacy
    
    try
    {
      ((ComboDateField)getField("beginDate")).setUtilDate( newDate );
    }
    catch( Exception e )
    {
      // ignore
    }
  }
  
  public void setReportDateEnd( Date newDate )
  {
    ReportDateEnd = newDate;  // legacy
    
    try
    {
      ((ComboDateField)getField("endDate")).setUtilDate( newDate );
    }
    catch( Exception e )
    {
      // ignore
    }
  }
  
  public void setReportHierarchyNode( long hierarchyNode )
  {
    ReportNodeId = hierarchyNode;
    setData( "nodeId", String.valueOf(hierarchyNode) );
  }
  
  public void setReportHierarchyNodeDefault( long hierarchyNode )
  {
    if( ReportUserBean == null ||
        isNodeParentOfNode( ReportUserBean.getHierarchyNode(), hierarchyNode ) )
    {
      ReportNodeIdDefault = hierarchyNode;
      setData( "nodeIdDefault", String.valueOf(hierarchyNode) );
    }
  }
  
  public void setReportOrgId( long orgId )
  {
    int     hierType  = getHierType();
    long    nodeId    = orgId;
    
    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      nodeId = orgIdToHierarchyNode(orgId);
    }
    setReportHierarchyNode(nodeId);
  }
  
  public void setReportOrgIdDefault( long orgId )
  {
    int     hierType  = getHierType();
    long    nodeId    = orgId;
    
    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      nodeId = orgIdToHierarchyNode(orgId);
    }
    setReportHierarchyNodeDefault(nodeId);
  }
  
  public void setReportType( int reportType )
  {
    ReportType = reportType;
    setData("reportType",String.valueOf(reportType));
  }
  
  public void setReportUserBean( UserBean rub )
  {
    ReportUserBean      = rub;
    
    try
    {
      // set the user bean in the base class
      super.setUser(rub);
    
      if(rub.getReportHierarchyNode() > 0)
      {
        ReportNodeIdDefault = rub.getReportHierarchyNode();
        AppFilter = true;
        AppFilterUserId = rub.getUserId();
      }
      else
      {
        ReportNodeIdDefault  = ReportUserBean.getHierarchyNode();
        AppFilter = false;
        AppFilterUserId = -1L;
      }
    }
    catch(Exception e)
    {
      logEntry("setReportUserBean(" + rub.getLoginName() + ")", e.toString());
    }
  }
  
  public void setSubmit(boolean newValue)
  {
    Submit = newValue;
  }
  
  protected void splitCsvData( StringBuffer csvData, Vector data )
  {
    char            ch                = '\0';
    boolean         stringStarted     = false;
    StringBuffer    token             = new StringBuffer();
    
    data.removeAllElements();
    
    // parse the CSV data and put each data 
    // element into a vector of data.
    for( int i = 0; i < csvData.length(); ++i )
    {
      ch = csvData.charAt(i);
      
      switch(ch)
      {
        case ' ':       // skip leading spaces
        case '\'':      // skip leading ' marks
          if ( token.length() == 0 )    
          {
            continue;
          }
          else
          {
            token.append(ch);
          }
          break;
          
        case '"':
          stringStarted = !stringStarted;
          break;
          
        case ',':
          if ( !stringStarted )
          {
            data.addElement(token.toString());
            token.setLength(0);
          }
          else  // comma inside a string value
          {
            token.append(ch);
          }
          break;
          
        case '\r':
          continue;
          
        case '\n':
          data.addElement(token.toString());
          token.setLength(0);
          break;
          
        default:
          token.append(ch);
          break;
      }
    }
    // boundary condition, add last token
    data.addElement(token.toString());
  }
  
  /*
  ** FUNCTION storeData
  *
  *   This method is overloaded by individual report beans (if necessary)
  *   to write teh data from the bean to the database.
  *
  */
  public String storeData( long orgId )
  {
    return "";
  }
  
  protected void storeXmlFieldNames( StringBuffer line )
  {
    if ( XmlFieldNames == null )
    {
      XmlFieldNames = new Vector();
    }
    splitCsvData(line,XmlFieldNames);
  }
  
  public boolean usingDefaultReportDates( )
  {
    return( DefaultReportDates );
  }
  
  public boolean validate()
  {
    return(true);
  }

  public void getProfileIds(long nodeId) {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:3295^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                              as profile_id,
//            tp.terminal_id || ' - ' || tp.merchant_name as profile_desc
//          from    organization          o,
//            group_merchant        gm,
//            trident_profile       tp,
//            trident_product_codes tpc
//          where   o.org_group = :nodeId
//            and gm.org_num = o.org_num
//            and tp.merchant_number = gm.merchant_number
//            and nvl(tp.api_enabled,'N') = 'Y'
//            and tp.profile_status != 'C'
//            and tpc.product_code = tp.product_code
//            and nvl(tpc.trident_payment_gateway,'N') = 'Y'
//          order by tp.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                              as profile_id,\n          tp.terminal_id || ' - ' || tp.merchant_name as profile_desc\n        from    organization          o,\n          group_merchant        gm,\n          trident_profile       tp,\n          trident_product_codes tpc\n        where   o.org_group =  :1 \n          and gm.org_num = o.org_num\n          and tp.merchant_number = gm.merchant_number\n          and nvl(tp.api_enabled,'N') = 'Y'\n          and tp.profile_status != 'C'\n          and tpc.product_code = tp.product_code\n          and nvl(tpc.trident_payment_gateway,'N') = 'Y'\n        order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.reports.ReportSQLJBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.reports.ReportSQLJBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3311^7*/
      rs = it.getResultSet();
      while( rs.next() )
      {
        ReportRows.addElement( rs.getString("profile_id") );
      }
      rs.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("getProfileIds()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/