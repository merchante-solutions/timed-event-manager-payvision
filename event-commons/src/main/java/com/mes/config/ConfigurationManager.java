package com.mes.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import masthead.encrypt.AesEncryption;

/**
 * Created by vbannikov on 2/24/2015.
 */
public class ConfigurationManager {
	
    public static final String TE_CLI_PROPERTY_NAME       = "te.configuration";
    public static final String TEM_PROPERTY_FILE_DEFAULT = "te.properties";

    private static ConfigurationManager instance;
    private Map<String, String> temConfiguration;
    static Logger logger = Logger.getLogger(ConfigurationManager.class);

    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
            instance.setTemConfiguration(new HashMap<String, String>());
        }

        return instance;
    }

    public static void main(String[] args) {
        ConfigurationManager configurationManager = ConfigurationManager.getInstance();
        try {
            configurationManager.loadTEMConfiguration(TEM_PROPERTY_FILE_DEFAULT);
            checkKey(configurationManager, "MERCH_PSP_FROM_EMAIL_ADDRESS");
            checkKey(configurationManager, "TEST_KEY");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void checkKey(ConfigurationManager configurationManager, String key) {
        String result;
        System.out.print("Checking \"" + key + "\" ... ");
        result = configurationManager.checkKeyListStatus(new String[]{key});
        if (result.isEmpty()) {
            System.out.print("OK");
        } else {
            System.out.print(result);
        }
        System.out.println("");
    }

    public boolean isLoaded() {
        return temConfiguration.size() > 0;
    }

    public Map<String, String> getTemConfiguration() {
        return temConfiguration;
    }

    public void setTemConfiguration(Map<String, String> temConfiguration) {
        this.temConfiguration = temConfiguration;
    }

    public Map<String, String> loadConfiguration(String configFilePath) throws Exception {
        Map<String, String> result = new HashMap<>();
        Properties properties = new Properties();
        File propFile;

        InputStream input;
        
        String teConfig = System.getenv(TE_CLI_PROPERTY_NAME);
        logger.info("System.getenv(\"te.configuration\") ####### te.properties file path :" + teConfig);
        if(teConfig == null){
            propFile = new File(configFilePath);
            logger.info("TeProperties using default property filename: " + propFile);
            logger.info("If this job is ran on Windows machine please set 'te.configuration=<te.properties file path>' key value pair in system environmental variables and restart the job.");
        }else{
            propFile = new File(teConfig);
            logger.info("TeProperties using java system parameter property filename: " + propFile);
        }

        logger.info("Configuration dir... " + propFile.getAbsolutePath());
        
        if(propFile.exists()&&!propFile.isDirectory()){
        	input = new FileInputStream(propFile);
		// load properties file
        	properties.load(input);
        }else{
        	System.err.println(
                   "ERROR - Unable to load te.Properties property file: " + configFilePath + "\n\n" +
                           "TE configuration failed.\n" +
                            "create te.properties in startup directory.\n");
        	throw new FileNotFoundException(configFilePath);
        }

        Enumeration<?> namesList = properties.propertyNames();
        while (namesList.hasMoreElements()) {
            String propertyKey = (String) namesList.nextElement();
            String propertyValue = properties.getProperty(propertyKey);

            result.put(propertyKey, propertyValue);
        }
        
        loadCriticalSystemProperties(result);
 
        return result;
    }
    
    private void loadCriticalSystemProperties(Map<String, String> result)
    {
    		setCriticalSystemProperty(result, AesEncryption.STATIC_ENC_AES_KEY, "COM_MES_ENCRYPTION_CLIENT_STATIC_KEY");
    }
    
	private void setCriticalSystemProperty(Map<String, String> result, String systemKey, String localKey) {
		try {
			System.setProperty(systemKey, result.get(localKey));
		}
		catch (Exception ex) {
			logger.error(String.format("Error Setting System property: %s", localKey));
			System.exit(1);
		}
	}

    public String getString(String key) {
        Map<String, String> temConfiguration = getInstance().temConfiguration;
        return temConfiguration.get(key);
    }

    public void loadTEMConfiguration(String configFilePath) throws Exception {
        temConfiguration = loadConfiguration(configFilePath);
    }

    public void printKeyListStatus(String[] keyList) {
        System.out.println("Checking property key list\n");
        System.out.println(checkKeyListStatus(keyList));
        System.out.println("Property key list checking complete!");
    }

    public String checkKeyListStatus(String[] keyList) {
        StringBuilder checkResult = new StringBuilder(keyList.length);
        for (String arg : keyList) {
            if (getString(arg) == null)
                checkResult.append("Checking key ").append(arg).append("\t.......MISSING in property file\n");
            else if (getString(arg).isEmpty())
                checkResult.append("Checking key ").append(arg).append("\t.......EMPTY value in property file\n");
            else
                checkResult.append("Checking key ").append(arg).append("\t.......OK\n");
        }

        return checkResult.toString();
    }
}
