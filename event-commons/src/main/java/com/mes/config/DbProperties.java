package com.mes.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;
import com.mes.support.LoggingConfigurator;

/**
 * Allows loading of db connection configuration from a property file.
 */

public class DbProperties {

    static { LoggingConfigurator.configure(); } // configure log4j if needed
    static Logger log = Logger.getLogger(DbProperties.class);
  
  /*
   * Examples of useage in classes.  The following show how to place a 
   * static block at the top of a class to make sure db config info
   * is loaded.
   *
   * To enforce db property loading use following in class with dependency

  static { DbProperties.requireConfiguration(); } // initialize database properties
  
   * To try to configure but not require
   
  static { DbProperties.configure(); } // initialize database properties
   
   */

    public static final String DB_CLI_PROPERTY_NAME       = "db.configuration";
    public static final String DB_CLI_OVERRIDE_PREFIX     = "db.override.";
    public static final String DB_DEFAULT_PROPERTY_FILE   = "db.properties";

    private static final String PN_DB_NAME                = "db.name";
    private static final String PN_FTPDB_NAME             = "ftpdb.name";
    private static final String PN_DB_DIRECT              = "db.direct";
    private static final String PN_DB_POOL                = "db.pool";

    private static final String PN_TYPE                   = "type";
    private static final String PN_URL                    = "url";
    private static final String PN_DRIVER                 = "driver";
    private static final String PN_USER                   = "user";
    private static final String PN_PASSWORD               = "password";

    private static final Set requiredProps;
    private static final Set requiredConProps;

    static {

        requiredProps = new HashSet();
        requiredProps.addAll(Arrays.asList(new String[] {
                PN_DB_NAME, PN_FTPDB_NAME, PN_DB_DIRECT, PN_DB_POOL}));
        requiredConProps = new HashSet();
        requiredConProps.addAll(Arrays.asList(new String[] {
                PN_TYPE, PN_URL, PN_DRIVER, PN_USER, PN_PASSWORD }));
    }

    private static File         propFile;
    private static Properties   props;
    private static boolean      configFlag  = false;
    private static Map          dbMap       = new HashMap();
    private static Map          cacheMap    = new HashMap();

    public static String dbName() {
        return props.getProperty(PN_DB_NAME);
    }

    public static String ftpdbName() {
        return props.getProperty(PN_FTPDB_NAME);
    }

    public static String dbDirect() {
        return props.getProperty(PN_DB_DIRECT);
    }

    public static String dbPool() {
        return props.getProperty(PN_DB_POOL);
    }

    /**
     * db property files must contain:
     *
     *   db.name
     *   ftpdb.name
     *   db.direct
     *   db.pool
     *   epicor.connection.string
     *   ms.sql.driver
     *
     * the name properties reference db connection sets defined as follows:
     *
     *   db.<name>.type
     *   db.<name>.url
     *   db.<name>.driver
     *   db.<name>.user
     *   db.<name>.password
     *
     * system properties (-Ddb.override.db.name=<name>) can by used to override
     * the required properties
     */
    private static void loadConfigData() throws Exception {

        InputStream in = null;

        try {

            // fetch property file
            in = new FileInputStream(propFile);
            props = new Properties();
            props.load(in);

            // process properties, store by db name in map
            List pnList = Collections.list(props.propertyNames());
            for (Iterator i = pnList.iterator(); i.hasNext();) {

                String pn = ""+i.next();

                //log.debug("Checking property name: " + pn);

                if (requiredProps.contains(pn)) continue;

                String[] parts = pn.split("\\.");

                if (!"db".equals(parts[0])) throw new Exception("Invalid property: " + pn);
                String dbNamePart = parts[1];
                String keyPart = parts[2];

                Map map = (Map)dbMap.get(dbNamePart);
                if (map == null) {

                    map = new HashMap();
                    dbMap.put(dbNamePart,map);
                }

                String value = props.getProperty(pn);
                map.put(keyPart,value);
            }

            // check for missing required properties and apply overrides
            for (Iterator i = requiredProps.iterator(); i.hasNext(); ) {

                // is the required property in the file?
                String propName = ""+i.next();
                if (props.getProperty(propName) == null) {
                    throw new Exception("Missing property " + propName);
                }

                // is there a command line override value to apply?
                String overrideName = DB_CLI_OVERRIDE_PREFIX + propName;
                String overrideValue = System.getProperty(overrideName);
                if (overrideValue != null) {
                    log.info("Overriding property " + propName + " with value " + overrideValue);
                    props.setProperty(propName,overrideValue);
                }
            }

            // validate data
            if (dbMap.get(dbName()) == null)
                throw new Exception("Invalid property " + PN_DB_NAME + ": " + dbName());

            if (dbMap.get(ftpdbName()) == null)
                throw new Exception("Invalid property " + PN_FTPDB_NAME + ": " + ftpdbName());

            if (dbMap.get(dbDirect()) == null)
                throw new Exception("Invalid property " + PN_DB_DIRECT + ": " + dbDirect());

            if (dbMap.get(dbPool()) == null)
                throw new Exception("Invalid property " + PN_DB_POOL + ": " + dbPool());

            for (Iterator i = dbMap.entrySet().iterator(); i.hasNext();) {
                Map.Entry e = (Map.Entry)i.next();
                Map db = (Map)e.getValue();
                for (Iterator j = requiredConProps.iterator(); j.hasNext();) {
                    String dbKey  = ""+e.getKey();
                    String key    = ""+j.next();
                    String pName  = "db." + dbKey + "." + key;
                    if (db.get(key) == null)
                        throw new Exception("Missing property " + pName);
                    if (key.equals(PN_TYPE)) {
                        String value  = ""+db.get(key);
                        if (!value.equals("direct") && !value.equals("pool"))
                            throw new Exception("Invalid property " + pName + " '"
                                    + value + "', must be direct or pool");
                    }
                }
            }
        }
        finally {
            try { in.close(); } catch (Exception e) { }
        }
    }

    /**
     * propFilename is optional.
     */
    public synchronized static boolean configure(String propFilename) {

        // only initialize once
        if (!configFlag) {

            try {

                // determine property filename to use
                if (propFilename == null) {
                    propFilename = System.getProperty(DB_CLI_PROPERTY_NAME);
                    if (propFilename == null) {
                        propFilename = DB_DEFAULT_PROPERTY_FILE;
                        log.info("DbProperties using default property filename: " + propFilename);
                    }
                    else {
                        log.info("DbProperties using java system parameter property filename: " + propFilename);
                    }
                }
                else {
                    log.info("DbProperties using provided property filename: " + propFilename);
                }

                // load properties
                propFile = new File(propFilename);
                if (propFile.exists() && !propFile.isDirectory()) {
                    loadConfigData();
                    configFlag = true;
                    log.info("DbProperties successfully configured:\n" + getLoggableConnectionString());
                }
                else {
                    System.err.println(
                            "ERROR - Unable to load DbProperties property file: " + propFilename + "\n\n" +
                                    "DB configuration failed.\n" +
                                    "Use java parameter -D" +  DB_CLI_PROPERTY_NAME + "=<filename> <app class>\n" +
                                    "or provide filename in DbProperties.configure() call\n" +
                                    "or create db.properties in startup directory.\n");
                }
            }
            catch (Exception e) {
                log.error("DbProperties error",e);
            }
        }

        return configFlag;
    }

    public static boolean configure() {
        return configure(null);
    }

    public static void requireConfiguration(String propFilename) {
        if (!configure(propFilename)) {
            log.error("Halting execution due to DbProperties error");
            System.exit(1);
        }
    }

    public static void requireConfiguration() {
        requireConfiguration(null);
    }

    public static boolean isConfigured() {
        return configFlag;
    }

    public static String dump() {

        StringBuffer buf = new StringBuffer("DbProperties [ ");
        buf.append("\n  configFlag: " + configFlag);
        buf.append(",\n  dbName: " + dbName());
        buf.append(",\n  ftpdbName: " + ftpdbName());
        buf.append(",\n  dbDirect: " + dbDirect());

        buf.append(",\n  dbMap: { ");
        for (Iterator k = dbMap.keySet().iterator(); k.hasNext();) {

            String dbKey = ""+k.next();
            buf.append("\n    " + dbKey + ": { ");
            Map dbKeyMap = (Map)dbMap.get(dbKey);
            for (Iterator k2 = dbKeyMap.keySet().iterator(); k2.hasNext();) {

                String dbKey2 = ""+k2.next();
                buf.append("\n      " + dbKey2 + ": " + dbKeyMap.get(dbKey2));
                buf.append((k2.hasNext() ? ", " : "\n    }"));
            }

            buf.append((k.hasNext() ? ", " : "\n  }"));
        }
        buf.append("\n]");
        return ""+buf;
    }
  

  /*
   * Get connection components by name
   */

    private static String getConnectionProperty(String name, String propName) {

        if (!dbMap.containsKey(name))
            throw new NullPointerException("No connection data for '" + name + "'");
        Map dbKeyMap = (Map)dbMap.get(name);
        return (String)dbKeyMap.get(propName);
    }

    public static String getConnectionType(String name) {
        return getConnectionProperty(name,PN_TYPE);
    }

    public static String getConnectionUrl(String name) {
        return getConnectionProperty(name,PN_URL);
    }

    public static String getConnectionDriver(String name) {
        return getConnectionProperty(name,PN_DRIVER);
    }

    public static String getConnectionUser(String name) {
        return getConnectionProperty(name,PN_USER);
    }

    public static String getConnectionPassword(String name) {
        return getConnectionProperty(name,PN_PASSWORD);
    }
  
  /*
   * Get primary connection components
   */

    public static String getConnectionType() {
        return getConnectionType(dbName());
    }

    public static String getConnectionUrl() {
        return getConnectionUrl(dbName());
    }

    public static String getConnectionDriver() {
        return getConnectionDriver(dbName());
    }

    public static String getConnectionUser() {
        return getConnectionUser(dbName());
    }

    public static String getConnectionPassword() {
        return getConnectionPassword(dbName());
    }
  
  /*
   * Get direct connection components
   */

    public static String getDirectConnectionType() {
        return getConnectionType(dbDirect());
    }

    public static String getDirectConnectionUrl() {
        return getConnectionUrl(dbDirect());
    }

    public static String getDirectConnectionDriver() {
        return getConnectionDriver(dbDirect());
    }

    public static String getDirectConnectionUser() {
        return getConnectionUser(dbDirect());
    }

    public static String getDirectConnectionPassword() {
        return getConnectionPassword(dbDirect());
    }

  /*
   * Get pool connection components
   */

    public static String getPoolConnectionType() {
        return getConnectionType(dbPool());
    }

    public static String getPoolConnectionUrl() {
        return getConnectionUrl(dbPool());
    }

    public static String getPoolConnectionDriver() {
        return getConnectionDriver(dbPool());
    }

    public static String getPoolConnectionUser() {
        return getConnectionUser(dbPool());
    }

    public static String getPoolConnectionPassword() {
        return getConnectionPassword(dbPool());
    }
    
  /*
   * Connection string building
   */

    public static String getConnectionString(String name) {

        if (!configFlag)
            throw new RuntimeException("DbProperties not configured");

        // retrieve/build connection string
        String cs = (String)cacheMap.get(name);
        if (cs == null) {
            String type     = getConnectionType(name);
            String url      = getConnectionUrl(name);
            String driver   = getConnectionDriver(name);
            String user     = getConnectionUser(name);
            String password = getConnectionPassword(name);
            cs = buildConnectString(type,url,driver,user,password);
            cacheMap.put(name,cs);
        }
        return cs;
    }

    /**
     * [direct;]url;driver;user=<username>;password=<password>
     *
     * Direct connections have a leading direct; prefix.
     */
    public static String buildConnectString(String type, String url,
                                            String driver, String user, String password)
    {
        StringBuffer buf = new StringBuffer();

        try  {

            if(type !=  null && "direct".equals(type)) {
                buf.append(type);
                buf.append(";");
            }

            buf.append(url);
            buf.append(";");
            buf.append(driver);
            buf.append(";user=");
            buf.append(user);
            buf.append(";password=");
            buf.append(password);
        }
        catch(Exception e) {
        }

        return buf.toString();
    }

    public static String getConnectionString() {
        return getConnectionString(dbName());
    }

    public static String getFtpdbConnectionString() {
        return getConnectionString(ftpdbName());
    }

    public static String getDirectConnectionString() {
        return getConnectionString(dbDirect());
    }

    public static String getPoolConnectionString() {
        return getConnectionString(dbPool());
    }

    public static boolean isPool(String dbName) {
        if (configFlag) {
            if (!dbMap.containsKey(dbName))
                throw new NullPointerException("Undefined db name key: " + dbName);
            Map dbKeyMap = (Map)dbMap.get(dbName);
            String type = ""+dbKeyMap.get("type");
            return "pool".equals(type);
        }
        return false;
    }

    public static boolean isPool() {
        return isPool(dbName());
    }

    public static boolean isValidDbName(String dbName) {
        return dbMap.containsKey(dbName);
    }

	public static String getLoggableConnection(String connectionString) {

		if (connectionString == null)
			return "";
		
		String[] parts = connectionString.split(";");
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < parts.length; ++i) {
			if (!parts[i].startsWith("password")) {
				buf.append((buf.length() > 0 ? ";" : "") + parts[i]);
			}
			else {
				buf.append((buf.length() > 0 ? ";" : "") + "password=******");
			}
		}
		return "" + buf.toString();
	}
    public static String getLoggableConnectionString(String dbName) {

        String csRaw = getConnectionString(dbName);
        return getLoggableConnection(csRaw);
    }

    public static String getLoggableConnectionString() {

        return getLoggableConnectionString(dbName());
    }

}
