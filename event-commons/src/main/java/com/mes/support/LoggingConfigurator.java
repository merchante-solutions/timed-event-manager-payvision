package com.mes.support;

import java.io.File;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * This class will serve as a single point to manage configuration of 
 * logging, mostly for standalone applications. Currently usinglog4j 1.x.
 *
 * The class whose static void main() serves as the entry point should
 * make a call to one of the configure methods below to setup logging.
 * This can also be in a static code block at the top of the class.
 *
 * e.g. static { LoggingConfigurator.configure(); }
 *
 * For convenience requireConfiguration() methods will cause the jvm
 * to exit if configuration is not successful.
 *
 * The log4j property filename can be specified at the command line
 * using -Dlog4j.configuration=<filename> or by passing in a filename
 * to the configuration method.  If neither is provided an attempt 
 * is made to load from the default property filename, 'log4j.properties'.
 */
 
public class LoggingConfigurator {

  static Logger log = Logger.getLogger(LoggingConfigurator.class);

  public static final String LOG4J_CLI_PROPERTY_NAME      = "log4j.configuration";
  public static final String LOG4J_DEFAULT_PROPERTY_FILE  = "log4j.properties";
  
  private static boolean configFlag = false;
  
  /**
   * propFn is optional.
   */
  public synchronized static boolean configure(String propFn) {  
  
    // only initialize once
    if (!configFlag) {
    
      try {
    
        // determine property filename to use
        if (propFn == null) {
          propFn = System.getProperty(LOG4J_CLI_PROPERTY_NAME);
          if (propFn == null) {
            propFn = LOG4J_DEFAULT_PROPERTY_FILE;
            System.out.println("Using default property filename: " + propFn);
          }
          else {
            System.out.println("Using java system parameter property filename: " + propFn);
          }
        }
        else {
          System.out.println("Using provided property filename: " + propFn);
        }
      
        // load properties
        File propF = new File(propFn);
        if (propF.exists() && !propF.isDirectory()) {
          PropertyConfigurator.configure(propFn);
          log.info("Log4j successfully configured.");
          configFlag = true;
        }
        else {
          System.err.println(
            "ERROR - Unable to load log4j property file: " + propFn + "\n\n" +
            "Log4j configuration failed.\n" +
            "Use java parameter -D" +  LOG4J_CLI_PROPERTY_NAME + "=<filename> <app class>\n" +
            "or provide filename in LoggingConfigurator.configure() call\n" +
            "or create log4j.properties in startup directory.\n");
        }
      }
      catch (Exception e) {
        System.err.println("Error: " + e);
        e.printStackTrace();
      }
    }
    
    return configFlag;
  }
  
  public static boolean configure() {
    return configure(null);
  }
  
  public static void requireConfiguration(String propFn) {
    if (!configure(propFn)) {
      System.err.println(
        "Halting execution due to failure of required logging configuration.");
      System.exit(1);
    }
  }
  
  public static void requireConfiguration() {
    requireConfiguration(null);
  }
  
  public boolean isConfigured() {
    return configFlag;
  }
}
  
  
  