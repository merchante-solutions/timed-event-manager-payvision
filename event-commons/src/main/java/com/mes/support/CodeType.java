package com.mes.support;

import java.util.HashMap;

public class CodeType
{
  protected static HashMap codeHash = new HashMap();

  protected String code;
  protected String description;

  protected CodeType(String code, String description)
  {
    this.code = code;
    this.description = description;
  }
  protected CodeType(String code)
  {
    this(code,null);
  }

  protected static Object forCode(String forStr, Object dflt)
  {
    Object o = codeHash.get(forStr);
    return o != null ? o : dflt;
  }
  protected static Object forCode(String forStr)
  {
    Object o = forCode(forStr,null);
    if (o == null)
    {
      throw new NullPointerException("Invalid indicator string: '"
        + forStr + "'");
    }
    return o;
  }

  public boolean equals(Object o)
  {
    return this.getClass().isInstance(o) 
      && ((CodeType)o).code.equals(code);
  }

  public String toString()
  {
    return code;
  }
  
  public String getDescription()
  {
    return description != null ? description : code;
  }
}