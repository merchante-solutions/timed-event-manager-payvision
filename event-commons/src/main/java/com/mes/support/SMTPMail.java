/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/SMTPMail.java $

  Description:  
  
    MailMessage

    Uses the Java Mail API to create an email message and send it.
    Supports multi-part messages (allows file attachments) and allows
    multiple recipients (of TO, CC and BCC types) to be specified.
    A set of sender/recipient addresses may be defined in the db table
    EMAIL_ADDRESSES.  These sets may be referenced with the setAddresses()
    call.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 6/12/02 1:32p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

public class SMTPMail
{
  static Logger log = Logger.getLogger(SMTPMail.class);
  
  // TODO: create MesProperties manager class  
  public static final String MESP_SMTP_HOST     = "mail.smtp.host";
  public static final String MESP_SMTP_PORT     = "mail.smtp.port";
  public static final String MESP_MAIL_TAG      = "mail.subject.tag";
  public static final String MESP_MAIL_PREFIX   = "mail.body.prefix";
  public static final String MESP_MAIL_POSTFIX  = "mail.body.postfix";
  
  private Session           session;
  private MimeMessage       message;
  private Multipart         body;
  private MimeBodyPart      textPart;
  private Properties        mesProps;
  
  public SMTPMail()
    throws javax.mail.MessagingException
  {
    initialize();
  }
  public SMTPMail(String smtpHost)
    throws javax.mail.MessagingException
  {
    initialize(smtpHost);
  }
  public SMTPMail(String smtpHost, int smtpPort)
    throws javax.mail.MessagingException
  {
    initialize(smtpHost,smtpPort);
  }
  
  private boolean loadMesProps() {
  
    FileInputStream in = null;
    
    try {
    
      Properties props = new Properties();
      File mpFile = new File("mes.properties");
      if (mpFile.exists()) {
        in = new FileInputStream(mpFile);
        props.load(in);
        mesProps = props;
        return true;
      }
    }
    catch (Exception e) {
      log.error(e);
      e.printStackTrace();
    }
    finally {
      try { in.close(); } catch (Exception e)  { }
    }
    
    return false;
  }
  
  private String getMesProperty(String key, String defawlt) {
    if (mesProps != null || loadMesProps()) {
      return mesProps.getProperty(key);
    }
    return defawlt;
  }
  
  private String getMesProperty(String key) {
    return getMesProperty(key, null);
  }

  private String getSubjectTag() {
    return getMesProperty(MESP_MAIL_TAG);
  }
  
  private String getBodyPrefix() {
    return getMesProperty(MESP_MAIL_PREFIX);
  }
  
  private String getBodyPostfix() {
    return getMesProperty(MESP_MAIL_POSTFIX);
  }
  
  private String getSmtpHost() {
    return getMesProperty(MESP_SMTP_HOST);
  }
  
  private int getSmtpPort() {
  
    int port = 25;
    String portStr = getMesProperty(MESP_SMTP_PORT,""+port);
    try {
      port = Integer.parseInt(portStr);
    } 
    catch (Exception e) { }
    return port;
  }
  
  private void initialize(String smtpHost, int smtpPort)
    throws javax.mail.MessagingException
  {
    log.debug("SMTPEmail initializing with SMTP host: " + smtpHost + ":" + smtpPort);
    
    // configure smtp host and port
    Properties props = System.getProperties();
    props.put("mail.smtp.host",smtpHost);
    props.put("mail.smtp.port",""+smtpPort);
    session   = Session.getInstance(props, null); 
    //session.setDebug(true);
    
    // create message, multipart body of message 
    // and the text part of the message
    message   = new MimeMessage(session);
    body      = new MimeMultipart();
    textPart  = new MimeBodyPart();
    body.addBodyPart(textPart);
    message.setContent(body);
  }
  private void initialize(String smtpHost)
    throws javax.mail.MessagingException
  {
    initialize(smtpHost,getSmtpPort());
  }
  private void initialize()
    throws javax.mail.MessagingException
  {
    initialize(getSmtpHost(),getSmtpPort());
  }
  
  public void setFrom(String address)
    throws javax.mail.MessagingException
  {
    if(address != null && !address.equals("")) {
      message.setFrom(new InternetAddress(address));
    }
  }
  
  public void addRecipient(RecipientType type, String address)
    throws javax.mail.MessagingException
  {
    if(address != null && ! address.equals("")) {
      message.addRecipient(type, new InternetAddress(address));
    }
  }
  
  public void addRecipient(String address)
    throws javax.mail.MessagingException
  {
    addRecipient(RecipientType.TO, address);
  }
  
  public void addTORecipient(String address)
    throws javax.mail.MessagingException
  {
    addRecipient(RecipientType.TO, address);
  }
  
  public void addCCRecipient(String address)
    throws javax.mail.MessagingException
  {
    addRecipient(RecipientType.CC, address);
  }
  
  public void addBCCRecipient(String address)
    throws javax.mail.MessagingException
  {
    addRecipient(RecipientType.BCC, address);
  }
  
  /**
   * Support for delimited recipient strings.
   */

  public void addRecipients(RecipientType type, String addresses)
    throws javax.mail.MessagingException
  {
    message.addRecipients(type,addresses);
  }
  public void addRecipients(String addresses)
    throws javax.mail.MessagingException
  {
    addRecipients(RecipientType.TO,addresses);
  }
  public void addTORecipients(String addresses)
    throws javax.mail.MessagingException
  {
    addRecipients(RecipientType.TO, addresses);
  }
  public void addCCRecipients(String addresses)
    throws javax.mail.MessagingException
  {
    addRecipients(RecipientType.CC, addresses);
  }
  public void addBCCRecipients(String addresses)
    throws javax.mail.MessagingException
  {
    addRecipients(RecipientType.BCC, addresses);
  }
  public void setSubject(String subject)
    throws javax.mail.MessagingException
  {
    if(subject != null) {
    
      // add subject tag if provided in mes.properties
      String subTag = getSubjectTag();
      message.setSubject(subject + (subTag != null ? " (" + subTag + ")" : ""));
    }
  }
  
  public void setText(String text)
    throws javax.mail.MessagingException
  {
    if(text != null) {
    
      // add prefix or suffix text to body if provided in mes.properties
      String textPre  = getBodyPrefix();
      String textPost = getBodyPostfix();
      String bodyText = 
        (textPre != null ? textPre + "\n" : "") +
        text +
        (textPost != null ? "\n" + textPost : "");
      textPart.setText(bodyText);
    }
  }
  
  public void addStringAsTextFile(String filename, String fileData)
    throws javax.mail.MessagingException
  {
    // create a new body part for the file attachment
    MimeBodyPart filePart = new MimeBodyPart();
    
    // add the file data string to the body part as a text file
    filePart.setText(fileData);
    
    // set file name
    filePart.setFileName(filename);
                                                                      
    // add the new body part to the main body
    body.addBodyPart(filePart);
  }
  
  public void addFile(String filename)
    throws javax.mail.MessagingException
  {
    // create a new body part for the file attachment
    MimeBodyPart filePart = new MimeBodyPart();
    
    // set the file as the contents of the file body part
    DataSource source = new FileDataSource(filename);
    filePart.setDataHandler(new DataHandler(source));
    
    // set file name less any path information
    filePart.setFileName(source.getName());
    
    // add the new body part to the main body
    body.addBodyPart(filePart);
  }
  
  /*
  ** public class ByteArrayDataSource implements DataSource
  **
  ** A DataSource implementation for byte arrays, allows byte arrays
  ** to be added as file attachments to the email.
  */
  public class ByteArrayDataSource implements DataSource
  {
    private byte[] data;
    private String mimeType = "application/octet-stream";
    
    public ByteArrayDataSource(byte[] data)
    {
      this.data = data;
    }
    public ByteArrayDataSource(byte[] data, String mimeType)
    {
      this.data     = data;
      this.mimeType = mimeType;
    }
    
    public String getContentType()
    {
      return mimeType;
    }
   
    public String getName()
    {
      return "Anonymous ByteArrayDataSource";
    }

    public InputStream getInputStream()
    {
      return new ByteArrayInputStream(data);
    }

    public OutputStream getOutputStream() throws IOException
    {
      throw new IOException("Output not supported");
    }
  }
  
  public void addByteArrayAsFile(String filename, byte[] data)
    throws Exception
  {
    // create a new body part for the file attachment
    MimeBodyPart filePart = new MimeBodyPart();
    
    // set the file as the contents of the file body part
    DataSource source = new ByteArrayDataSource(data);
    filePart.setDataHandler(new DataHandler(source));
    
    // set file name less any path information
    filePart.setFileName((new File(filename)).getName());
    
    // add the new body part to the main body
    body.addBodyPart(filePart);
  }

  public void send()
    throws javax.mail.MessagingException
  {
    Transport.send(message);
  }
}
