/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/ServerTools.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.InputStream;

public class ServerTools
{
  public static boolean isDevelopmentServer()
  {
    WebLogicPropertiesFile propFile = null;
    
    try
    {
      propFile = new WebLogicPropertiesFile("mes.properties");
    }
    catch( Exception e )
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new WebLogicPropertiesFile( inputStream );

      try { inputStream.close(); } catch(Exception re) {}
    }
    return(propFile.getBoolean("com.mes.development", false));
  }
}
  
