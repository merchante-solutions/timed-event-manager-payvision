/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/SyncLog.java $

  Description:  
    Utility class for writing log entries to a log file


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import com.mes.config.DbProperties;

public class SyncLog
{
  static { DbProperties.requireConfiguration(); } // verify db configuration
  
  // constants for error logging
  private final static int        SZ_SOURCE                   = 80;
  private final static int        SZ_MESSAGE                  = 1024;
  private final static int        SZ_SERVER_NAME              = 30;
  
  // constants for connection logging
  private final static int        SZ_CALLER                   = 80;
  private final static int        SZ_CON_MESSAGE              = 40;
  
  // constants for access logging
  private final static int        SZ_ID                       = 30;
  private final static int        SZ_LOCATION                 = 30;
  private final static int        SZ_RESOURCE                 = 1024;
  private final static int        SZ_RESULT                   = 30;
  
  private static String           jdbcString                  = "";
  private static String           jdbcConnectionString        = "";
  private static String           jdbcDriverClassName         = "";
  private static Properties       DbProps                     = null;
  
  private static HashMap errorTimestamps; // TODO: For java7: HashMap<String, ThrottleDetail>
  private static final int LOG_THROTTLE = 1000 * 60 * 5; // 5 minutes
  private static final int LOG_MAX_SIZE = 500;
  
  /*
  ** METHOD LogEntry
  **
  ** Writes an entry to the log file
  */
  public static void LogEntry(int Error, String Source, String Message)
  {
    Connection          con               = null;
    CallableStatement   st                = null;
    
    try
    {
      if(jdbcString == null || jdbcString.equals(""))
      {
        jdbcConnectionString = DbProperties.getDirectConnectionUrl();
        jdbcDriverClassName = DbProperties.getDirectConnectionDriver();
        DbProps = new Properties();
        DbProps.setProperty("user",DbProperties.getDirectConnectionUser());
        DbProps.setProperty("password",DbProperties.getDirectConnectionPassword());
        Class.forName(jdbcDriverClassName);
      }
      
      con = DriverManager.getConnection(jdbcConnectionString, DbProps);
      con.setAutoCommit(true);
      
      if(con != null)
      {
        String  serverName     = HttpHelper.getServerName(null);
        st = con.prepareCall("{ call log_entry_new(?, ?, ?, ?) }");
        st.setInt(1, Error);
        st.setString(2, (Source.length() > SZ_SOURCE ? Source.substring(0, SZ_SOURCE) : Source));
        st.setString(3, (Message.length() > SZ_MESSAGE ? Message.substring(0, SZ_MESSAGE) : Message));
        st.setString(4, (serverName.length() > SZ_SERVER_NAME ? serverName.substring(0, SZ_SERVER_NAME) : serverName));
      
        st.execute();
      }
    }
    catch(Exception e)
    {
      // what to do here?  exception logging an exception...
      System.out.println("LogEntry: " + e.toString());
    }
    finally
    {
      if(st != null)
      {
        try
        {
          st.close();
        }
        catch(Exception e)
        {
        }
      }
      
      if(con != null)
      {
        try
        {
          con.close();
        }
        catch(Exception e)
        {
        }
      }
    }
  }
  
  /**
   * Logs an error to the database with a default 30s throttle based on the source parameter.<br />
   * Unique source strings will be treated separately for throttling purposes.
   * @param source The source class and method.
   * @param e The exception being thrown.
   */
  public static void logThrottled(String source, Exception e) {
    logThrottled(source, e, LOG_THROTTLE);
  }
  
  /**
   * Logs an error to the database throttling by throttle ms based on the source parameter.<br />
   * Unique source strings will be treated separately for throttling purposes.
   * @param source The source class and method.
   * @param e The exception being thrown.
   * @param throttle Duration of time to throttle requests from this source by.
   */
  public static void logThrottled(String source, Exception e, int throttle) {
    if(errorTimestamps == null)
      errorTimestamps = new HashMap();
    // Keep the map from growing too large in memory. When it clears, any persisting errors will get re-logged.
    if(errorTimestamps.size() > LOG_MAX_SIZE)
      errorTimestamps.clear();
    
    ThrottleDetail log = null;
    
    long now = System.currentTimeMillis();
    if(errorTimestamps.containsKey(source))
      log = ((ThrottleDetail) errorTimestamps.get(source));
    else
      log = new ThrottleDetail();
    // Log it when outside the throttle window
    if(log.lastError < now-throttle) {
      log.lastError = now;
      log.logCt++;
      LogEntry(0, source+"("+log.logCt+")", stackToString(e));
      errorTimestamps.put(source, log);
      System.out.println("["+getTimestamp()+"][Logged: "+log.logCt+"] " + source + " - " + e.toString());
    }
  }
  
  private static class ThrottleDetail {
    public long lastError;
    public int logCt;
  }
  
  /**
   * Gets the current timestamp as a String in the format yyyy-mm-dd hh:mm:ss.fffffffff.
   * @return
   */
  public static String getTimestamp() {
    Date date = new Date();
    return new Timestamp(date.getTime()).toString();
  }
  
  public static void LogEntry(String Source, String Message)
  {
    LogEntry(0, Source, Message);
  }
  
  public static void LogEntry(String Message)
  {
    LogEntry(0, "No Source", Message);
  }
  
  public static void LogEntry(int Error, String Message)
  {
    LogEntry(Error, "No Source", Message);
  }
  
  public static void LogEntry(String source, Exception e) {
    LogEntry(0, source, stackToString(e));
  }
  
  private static String stackToString(Exception e) {
    if(e == null)
      return "null exception";
    StringBuffer buff = new StringBuffer();
    StackTraceElement[] stack = e.getStackTrace();
    buff.append(e.toString()).append("\r\n");
    if(stack != null)
      for(int i=0; i<stack.length; i++)
        buff.append(stack[i].toString()).append("\r\n");
    return buff.toString();
  }
}
