package com.mes.support.config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * A configuration data container.  Loads and maintains configuration data
 * in a java properties object.  If the source data is updated the property
 * data will reload.  Other than that this behaves like a wrapper around a
 * Properties collection and provides property accessors.  Currently these
 * properties are read only.
 *
 * Supports tokenized property strings with getTokenProperty().  Takes
 * a token to replacement string Map to support dynamic property values.
 */
public class Config {

  static Logger log = Logger.getLogger(Config.class);
  
  private Properties properties = null;
  private ConfigSource configSrc = null;
  
  public Config(Config that) {
  
    this.configSrc = that.configSrc;
    loadProperties(configSrc);
  }
  
  public Config(ConfigSource configSrc) {
    
    this.configSrc = configSrc;
    loadProperties(configSrc);
  }
  
  private synchronized void loadProperties(ConfigSource configSrc) {
  
    InputStream is = null;
  
    try {
    
      is = configSrc.getInputStream();
      Properties properties = new Properties();
      properties.load(is);
      this.properties = properties;
    }
    catch (Exception e) {
      
      throw new RuntimeException(e);
    }
    finally {
    
      try { is.close(); } catch (Exception e) { }
    }
  }
  
  private void updateProperties() {
  
    if (configSrc != null && (properties == null || configSrc.isUpdated())) {
    
      log.debug("Updating property data from config source " + configSrc);
      loadProperties(configSrc);
    }
  }
     
  public Properties getProperties() {
  
    updateProperties();
    return properties;
  }

  public String getProperty(String name, String defaultValue) {
  
    return getProperties().getProperty(name,defaultValue);
  }
  
  public String getProperty(String name) {
  
    return getProperty(name,null);
  }
  
  /**
   * Fetches property and if tokenMap provided replaces tokens in
   * property string with mapped replacements.
   */
  public String getTokenProperty(String name, Map tokenMap) {
  
    String propStr = getProperty(name);
    
    if (tokenMap != null) {
    
      for (Iterator i = tokenMap.keySet().iterator(); i.hasNext();) {
    
        String token = ""+i.next();
        String replaceStr = (String)tokenMap.get(token);
        if (replaceStr != null) {
          propStr = propStr.replaceAll(token,replaceStr);
        }
      }
    }
    return propStr;
  }
  
  /** 
   * Token map helper, converts an array of strings into a Map:
   *
   *  even -> odd
   *
   */
  public Map getTokenMap(String[] mapData) {
  
    Map tokenMap = new HashMap();
    for (int i = 0; i < mapData.length; i += 2) {
    
      if (i + 1 < mapData.length) {
      
        tokenMap.put(mapData[i],mapData[i + 1]);
      }
      else {
      
        log.warn("Incomplete mapData array");
      }
    }
    return tokenMap;
  }
}