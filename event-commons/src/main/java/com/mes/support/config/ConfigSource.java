package com.mes.support.config;

import java.io.InputStream;

public interface ConfigSource {

  public InputStream getInputStream();
  public boolean isUpdated();
}