package com.mes.support;

public class XSSFilter
{
  private static String[] filterChars = {"<", ">", "<", ">", "&#", "\"", "\\", "0x"};
  private static String[] replacementChars = {" ", " ", " ", " ", "#", "'", "/", "0 x"};

  public static String encodeXSS(String param) 
  {
    String value = param;

    if( param!=null) 
    {
      for(int i = 0; i < filterChars.length; i++) 
      {
        value = filterCharacters(filterChars[i], replacementChars[i], value);
      }
    }

    return value;
  }

  private static String filterCharacters(String originalChar, String newChar, String param)
  {
    StringBuffer sb = new StringBuffer(param);

    for(int position = param.toLowerCase().indexOf(originalChar); position >= 0; ) 
    {
      sb.replace(position, position + originalChar.length(), newChar);
      param = sb.toString();
      position = param.toLowerCase().indexOf(originalChar);
    }
    
    return sb.toString();
  }
}