/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/OrgBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/11/04 3:38p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Set;
import java.util.TreeSet;

public class OrgBean implements Comparable
{
  public int          orgNum;
  public String       orgName;
  public long         orgMerchant;
  public long         hierarchyNode;
  public String       repId;
  
  private TreeSet       children = null;
  private TreeSet       merchants = null;
  private TreeSet       childMerchants = null;
  
  public int compareTo(Object object)
  {
    OrgBean     joe       = (OrgBean) object;
    int         retVal;
    
    if ( ( retVal = orgName.compareTo( ((OrgBean)object).getOrgName() ) ) == 0 )
    {
      retVal = ( getOrgNum() - ((OrgBean)object).getOrgNum() );
    }
    return( retVal );
  }
  
  public void Initialize(int orgNum, String orgName, long orgMerchant, long hierarchyNode, String repId)
  {
    children = new TreeSet();
    merchants = new TreeSet();
    childMerchants = new TreeSet();
    this.orgNum = orgNum;
    this.orgName = orgName;
    this.orgMerchant = orgMerchant;
    this.hierarchyNode = hierarchyNode;
    this.repId = repId;
  }
  
  public OrgBean(int orgNum, String orgName)
  {
    Initialize(orgNum, orgName, 0L, 0L, "");
  }
  
  public OrgBean(int orgNum, String orgName, String repId)
  {
    Initialize(orgNum, orgName, 0L, 0L, repId);
  }
  
  public OrgBean(int orgNum, String orgName, long orgMerchant, long hierarchyNode)
  {
    Initialize(orgNum, orgName, orgMerchant, hierarchyNode, "");
  }
  
  public void addChild(int orgNum, String orgName, long orgMerchant, long hierarchyNode)
  {
    OrgBean child = new OrgBean(orgNum, orgName, orgMerchant, hierarchyNode);
    
    try
    {
      children.add(child);
    }
    catch(Exception e)
    {
    }
  }
  
  public void addChild(int orgNum, String orgName, String repId)
  {
    OrgBean child = new OrgBean(orgNum, orgName, repId);
    
    try
    {
      children.add(child);
    }
    catch(Exception e) 
    {
    }
  }
  
  public void addMerchant(long merchantNumber)
  {
    merchants.add(merchantNumber);
  }
  
  public void addChildMerchant(long merchantNumber)
  {
    childMerchants.add(merchantNumber);
  }
  
  public void addChild(int orgNum, String orgName)
  {
    OrgBean child = new OrgBean(orgNum, orgName);
    
    try
    {
      children.add(child);
    }
    catch(Exception e)
    {
    }
  }
  
  public int getOrgNum()
  {
    return this.orgNum;
  }
  
  public String getOrgName()
  {
    return this.orgName;
  }
  
  public long getOrgMerchant()
  {
    return this.orgMerchant;
  }
  
  public long getHierarchyNode()
  {
    return this.hierarchyNode;
  }
  
  public Set getChildren()
  {
    return this.children;
  }
  public Set getMerchants()
  {
    return this.merchants;
  }
  public Set getChildMerchants()
  {
    return this.childMerchants;
  }
  
}

