/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/HttpHelper.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-09-27 14:58:16 -0700 (Fri, 27 Sep 2013) $
  Version            : $Revision: 21542 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.StringTokenizer;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class HttpHelper
{
  // singleton for server-specific info
  private static HttpHelper singleton     = null;
  public static final String     SESSION_COOKIE_NAME     = "JSESSIONID";
  
  // server-specific URL for prepending to calls to response.sendRedirect()
  protected String          serverURL     = "";
  private String            serverName    = "";
  private boolean           devServer     = false;
  private boolean           prodServer    = false;
  private boolean           bobServer     = false;
  private boolean           useSSL        = false;
  private boolean           debug         = false;
  private boolean           secureServer  = false;
  private boolean           testServer    = false;
  private boolean           testUseProdDB = false;
  private boolean           isCluster     = false;
  private HashSet           alternateURLs = new HashSet();
  private String            dotNetURL     = "";
  private boolean           useB2F        = true;
  private String            b2fRealm      = "MesWeb_Prod";
  private boolean           useRAC        = true;
  private String            racService    = "messrv";
  
  public HttpHelper(HttpServletRequest request)
  {
    InputStream is = this.getClass().getClassLoader().getResourceAsStream("resources/mes.properties");
    
    PropertiesFile propFile = null;
    
    if( is != null )
    {
      propFile = new PropertiesFile(is);
    }
    else
    {
      // old way
      propFile = new WebLogicPropertiesFile("mes.properties");
    }
    
    // default devServer to true in case it isn't in the properties file
    devServer     = propFile.getBoolean("com.mes.development", true);
    prodServer    = ! devServer;
    
    secureServer  = propFile.getBoolean("com.mes.secure", false);
    
    testServer    = propFile.getBoolean("com.mes.test", false);
    
    bobServer     = propFile.getBoolean("com.mes.bob", false);
    
    testUseProdDB = propFile.getBoolean("com.mes.testUseProdDB", false);
    
    useSSL        = propFile.getBoolean("com.mes.useSSL", false);
    
    debug         = propFile.getBoolean("com.mes.debug", false);
    
    isCluster     = propFile.getBoolean("com.mes.cluster", false);
    
    useB2F        = propFile.getBoolean("com.mes.b2f", false);
    
    useRAC        = propFile.getBoolean("com.mes.RAC", true);
    racService    = propFile.getString("com.mes.racservice", "messrv");
    
    b2fRealm      = propFile.getString("com.mes.b2frealm", "MesWeb_Prod");
    
    // get alternate URLs
    String altURLs = propFile.getString("com.mes.alternateURLs", "");
    
    if(!altURLs.equals(""))
    {
      StringTokenizer st = new StringTokenizer(altURLs, ";");
      while(st.hasMoreTokens())
      {
        String newUrl = st.nextToken();
        alternateURLs.add(newUrl);
      }
    }
    
    serverURL = propFile.getString("com.mes.serverURL", "");
    serverName = propFile.getString("com.mes.hostname", "Unknown");
    
    // get dotNetURL
    dotNetURL = propFile.getString("com.mes.dotNetURL", "www.merchante-solutions.net");
    
    System.out.println("***** HttpHelper Profile *****");
    System.out.print("serverName: ");
    System.out.println(serverName);
    System.out.print("serverURL:  ");
    System.out.println(serverURL);
    System.out.print("BoBServer:  ");
    System.out.println(bobServer ? "true" : "false");
    System.out.print("devServer:  ");
    System.out.println(devServer ? "true" : "false");
    System.out.print("prodServer: ");
    System.out.println(prodServer ? "true" : "false");
    System.out.print("testServer: " );
    System.out.println(testServer ? "true" : "false");
    System.out.print("useSSL:     ");
    System.out.println(useSSL ? "true" : "false");
    System.out.print("Debug:      ");
    System.out.println(debug ? "true" : "false");
    System.out.print("Cluster:    ");
    System.out.println(isCluster ? "true" : "false");
    System.out.print("DotNetURL:  ");
    System.out.println(dotNetURL);
    System.out.print("Uses B2F:   ");
    System.out.println(useB2F ? "true" : "false");
    System.out.print("Uses RAC:   ");
    System.out.println(useRAC ? "true" : "false");
    System.out.println("RAC Service: " + racService);
    System.out.println("******************************\n");
  }
  
  public String getInstanceURL(HttpServletRequest request)
  {
    String        result      = "";
    String        url         = serverURL;
    String        protocol    = "";
    StringBuffer  temp        = new StringBuffer("");
    
    try
    {
      // see if requested url is accepted
      if(alternateURLs != null && alternateURLs.contains(request.getServerName()))
      {
        url = request.getServerName();
      }
      
      if (prodServer || useSSL)
      {
        //System.out.println(System.currentTimeMillis() - now + " - Redirecting to https");
        protocol = "https://";
      }
      else
      {
        protocol = "http://";
      }
    
      temp.append(protocol);
      temp.append(url);
      
      result = temp.toString();
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getInstanceURL(): " + e.toString());
    }
    return result;
  }
  
  public String getServerName()
  {
    return serverName;
  }
  
  public String getDotNetURL()
  {
    return dotNetURL;
  }
  
  public static synchronized void createSingleton(HttpServletRequest request)
  {
    if(singleton == null)
    {
      System.out.println("Creating HttpHelper singleton");
      singleton = new HttpHelper(request);
    }
  }
  
  public static String getServerURL(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.getInstanceURL(request);
  }
  
  public static String getServerURLNaked(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.serverURL;
  }
  
  public static String getServerName(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.getServerName();
  }
  
  public static void setServerName(String value)
  {
    if(singleton == null)
    {
      createSingleton(null);
    }
    singleton.serverName = value;
  }
  
  public static String getDotNetURL(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.getDotNetURL();
  }
  
  public static boolean isDevServer(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.devServer;
  }
  
  public static boolean isTestServer(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.testServer;
  }
  
  public static boolean isBoBServer(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.bobServer;
  }
  
  public static boolean doesUseProdDB(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.testUseProdDB;
  }
  
  public static boolean isProdServer(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.prodServer;
  }
  
  public static boolean isSecureServer(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.secureServer;
  }
  
  public static boolean doesUseSSL(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.useSSL;
  }
  
  public static boolean doesUseB2F(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.useB2F;
  }
  
  public static boolean doesUseRAC(HttpServletRequest request)
  {
    if( singleton == null )
    {
      createSingleton(request);
    }
    
    return( singleton.useRAC && (! singleton.testServer || singleton.devServer ));
  }
  
  public static String getRACService(HttpServletRequest request)
  {
    if( singleton == null )
    {
      createSingleton(request);
    }
    
    return( singleton.racService );
  }
  
  public static String getB2fRealm(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.b2fRealm;
  }
  
  public static boolean isDebug(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.debug;
  }
  
  public static boolean isCluster(HttpServletRequest request)
  {
    if(singleton == null)
    {
      createSingleton(request);
    }
    
    return singleton.isCluster;
  }
  
  public static Date getDate( HttpServletRequest request, String name, String format )
  {
    return( getDate( request, name, format, null ) );
  }
  
  public static String getRequestAttribute(HttpServletRequest request, String name)
  {
    String result = null;
    
    try
    {
      String paramName = "";
      
      // returns the first request parameter that matches the name specified (case ignored)
      Enumeration myEnum = request.getAttributeNames();
      while(myEnum.hasMoreElements())
      {
        paramName = (String)myEnum.nextElement();
        
        if(paramName.compareToIgnoreCase(name) == 0)
        {
          result = ((String) request.getAttribute(paramName)).trim();
          break;
        }
      }
      
      if( result == null )
      {
        // try to retrieve this data as a parameter from the request.
        result = request.getParameter(name);
      }
    }
    catch(Exception e)
    {
    }
    
    return result;
  }
  
  public static String getRequestParameter(HttpServletRequest request, String name)
  {
    String result = null;
    
    try
    {
      String paramName = "";
      
      // returns the first request parameter that matches the name specified (case ignored)
      Enumeration myEnum = request.getParameterNames();
      while(myEnum.hasMoreElements())
      {
        paramName = (String)myEnum.nextElement();
        
        if(paramName.compareToIgnoreCase(name) == 0)
        {
          result = request.getParameter(paramName).trim();
          break;
        }
      }
    }
    catch(Exception e)
    {
    }
    
    return result;
  }
  
  public static Date getDate( HttpServletRequest request, String name, String format, Date defaultDate )
  {
    SimpleDateFormat      dateFormat  = new SimpleDateFormat( format );
    Date                  value       = defaultDate;
    
    try
    {
      value = dateFormat.parse( getRequestParameter(request, name) );
    }
    catch( Exception e )
    {
    }
    return( value );
  }

  public static double getDouble( HttpServletRequest request, String name )
  {
    return( getDouble( request, name, 0.0 ) );
  }

  public static double getDouble( HttpServletRequest request, String name, double defaultValue )
  {
    double      value = defaultValue;
    
    try
    {
      value = Double.parseDouble( getRequestParameter(request, name) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    return( value );
  }
  
  public static float getFloat( HttpServletRequest request, String name )
  {
    return( getFloat( request, name, (float)0.0 ) );
  }

  public static float getFloat( HttpServletRequest request, String name, float defaultValue )
  {
    float       value = defaultValue;
    
    try
    {
      value = Float.parseFloat( getRequestParameter(request, name) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    return( value );
  }
  
  public static int getInt( HttpServletRequest request, String name )
  {
    return( getInt( request, name, 0  ) );
  }

  public static int getInt( HttpServletRequest request, String name, int defaultValue )
  {
    int value = defaultValue;
    
    try
    {
      value = Integer.parseInt( getRequestParameter(request, name) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  public static long getLong( HttpServletRequest request, String name )
  {
    return( getLong( request, name, 0L ) );
  }
  
  public static long getLong( HttpServletRequest request, String name, long defaultValue )
  {
    long value = defaultValue;
    
    try
    {
      value = Long.parseLong( getRequestParameter(request, name) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  public static boolean getBoolean( HttpServletRequest request, String name )
  {
    return( getBoolean( request, name, false ) );
  }
  
  public static boolean getBoolean( HttpServletRequest request, String name, boolean defaultValue )
  {
    String    boolString  = getString(request, name, null);
    boolean   result      = defaultValue;
    
    if( boolString != null )
    {
      if( boolString.compareToIgnoreCase("Y")==0 || boolString.compareToIgnoreCase("true")==0 || boolString.equals("1"))
      {
        result = true;
      }
      else if ( boolString.compareToIgnoreCase("N")==0 || boolString.compareToIgnoreCase("false")==0 || boolString.equals("0"))
      {
        result = false;
      }
      // else undefined, return default
    }      
    
    return( result );
  }
  
  public static String getString( HttpServletRequest request, String name )
  {
    return( getString( request, name, "" ) );
  }
  
  /**
   * Gets a <b>parameter</b> from the Servlet request, returning defaultValue in the event it is not found, not a String or null.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static String getString( HttpServletRequest request, String name, String defaultValue )
  {
    String value = defaultValue;
    
    try
    {
      value = getRequestParameter(request, name);
      
      if(value == null)
      {
        value = defaultValue;
      }
    }
    catch(Exception e)
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  /**
   * Gets an <b>attribute</b> from the Servlet request as a bool, returning defaultValue in the event it is not found, or not a boolean.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static boolean getBooleanAttribute(HttpServletRequest request, String name, boolean defaultValue) {
    String    boolString  = getStringAttribute(request, name, null);
    boolean   result      = defaultValue;

    if( boolString != null ) {
      if( boolString.compareToIgnoreCase("Y")==0 || boolString.compareToIgnoreCase("true")==0 || boolString.equals("1"))
        result = true;
      else if ( boolString.compareToIgnoreCase("N")==0 || boolString.compareToIgnoreCase("false")==0 || boolString.equals("0"))
        result = false;
    }      

    return( result );
  }
  
  /**
   * Gets an <b>attribute</b> from the Servlet request as a Date object, returning defaultValue in the event it is not found, or a valid date.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static Date getDateAttribute( HttpServletRequest request, String name, String format, Date defaultDate ) {
    SimpleDateFormat      dateFormat  = new SimpleDateFormat( format );
    Date                  value       = defaultDate;

    try {
      value = dateFormat.parse( getRequestAttribute(request, name) );
    }
    catch( Exception e ) {
    }
    
    return( value );
  }
  
  /**
   * Gets an <b>attribute</b> from the Servlet request as a double, returning defaultValue in the event it is not found, or not a double.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static double getDoubleAttribute( HttpServletRequest request, String name, double defaultValue ) {
    double      value = defaultValue;

    try {
      value = Double.parseDouble( getRequestAttribute(request, name) );
    }
    catch( Exception e ) {
      value = defaultValue;
    }
    
    return( value );
  }

  /**
   * Gets an <b>attribute</b> from the Servlet request as a integer, returning defaultValue in the event it is not found, or not an integer.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static int getIntAttribute( HttpServletRequest request, String name, int defaultValue ) {
    int value = defaultValue;

    try {
      value = Integer.parseInt( getRequestAttribute(request, name) );
    }
    catch( Exception e ) {
      value = defaultValue;
    }

    return( value );
  }
  
  /**
   * Gets an <b>attribute</b> from the Servlet request as a long, returning defaultValue in the event it is not found, or not a long.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static long getLongAttribute( HttpServletRequest request, String name, long defaultValue ) {
    long value = defaultValue;
    
    try {
      value = Long.parseLong( getRequestAttribute(request, name) );
    }
    catch( Exception e ) {
      value = defaultValue;
    }
    
    return( value );
  }
  
  /**
   * Gets an <b>attribute</b> from the Servlet request as a String, returning defaultValue in the event it is not found, not a String or null.
   * @param request
   * @param name
   * @param defaultValue
   * @return
   */
  public static String getStringAttribute(HttpServletRequest request, String name, String defaultValue) {
    String value = defaultValue;

    try {
      value = getRequestAttribute(request, name);

      if(value == null)
        value = defaultValue;
    }
    catch(Exception e) {
      value = defaultValue;
    }

    return(value);
  }
  
  public static String getSessionId(HttpServletRequest request)
  {
    String sessionId = "UNKNOWN";
    
    try
    {
      if( request != null )
      {
        Cookie[] cookies = request.getCookies();
      
        for(int i=0; i<cookies.length; ++i)
        {
          Cookie c = cookies[i];
          
          if( c.getName().equals( SESSION_COOKIE_NAME ) )
          {
            sessionId = c.getValue();
          }
        }
      }
    }
    catch(Exception e) 
    {
    }
    
    return( sessionId );
  }
  
  public static String getParamString(HttpServletRequest request, int maxLength)
  {
    StringBuffer  paramStr = new StringBuffer("");
    Enumeration   myEnum = request.getParameterNames();
    String        name = "";
    
    while( myEnum.hasMoreElements() )
    {
      name = (String)myEnum.nextElement();
      paramStr.append( name );
      paramStr.append("=");
      for( int i = 0; i < request.getParameterValues(name).length; ++i )
      {
        if( i > 0 )
        {
          paramStr.append(",");
        }
        
        paramStr.append( urlDecode(CardTruncator.truncate(request.getParameterValues(name)[i])) );
      }
      paramStr.append(";");
    }
    
    return( paramStr.substring(0, paramStr.length() > maxLength ? maxLength : paramStr.length()) );
  }
  
  public static String getOriginHostName( HttpServletRequest request )
  {
    String originFull = request.getHeader("origin");
    return( originFull.substring(originFull.indexOf("//")+2) );
  }
  
  public static String urlEncode( String data )
  {
    String    retVal    = "";
    
    try
    {
      retVal = URLEncoder.encode(data,"UTF-8");  
    }
    catch( Exception e )
    {
      // should never happen
    }
    return( retVal );
  }
  
  public static String urlDecode( String data )
  {
    String    retVal    = "";
    
    try
    {
      retVal = URLDecoder.decode(data,"UTF-8");  
    }
    catch( Exception e )
    {
      // should never happen
    }
    return( retVal );
  }
  
  public static void destroy()
  {
    System.out.println("Destroying HttpHelper");
    singleton = null;
  }

  public static String displayHtml(String raw)
  {
    StringBuffer retVal = new StringBuffer("");
    
    if(raw != null)
    {
      for(int i=0; i < raw.length(); ++i)
      {
        if(raw.charAt(i) == '<')
        {
          retVal.append("&lt;");
        }
        else if(raw.charAt(i) == '>')
        {
          retVal.append("&gt;");
        }
        else if(raw.charAt(i) == '&')
        {
          retVal.append("&amp;");
        }
        else
        {
          retVal.append(raw.charAt(i));
        }
      }
    }
    
    return retVal.toString();
  }

  public static java.sql.Date getSqlDate(String d, String format) {
    SimpleDateFormat df  = new SimpleDateFormat( format );
    java.sql.Date date = null;
    try {
      date = new java.sql.Date( df.parse(d).getTime() );
    } catch (Exception e) {
      System.out.println("Error parsing date: " + d);
    }
    return date;
  }

  public static String getQueryParam(HttpServletRequest request, String name) {
    String query = null;

    try {
      query = URLDecoder.decode(request.getQueryString(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      query = request.getQueryString();
      query = query.replaceAll("%20", " ");
    }
    String value = "";

    if(query!=null) {
      String[] params = query.split("&");
      for(int i=0; i<params.length; i++) {
        if(params[i].startsWith(name + "=")) {
          value = params[i].replaceFirst(name + "=", "");
        }
      }
    }

    return value;
  }

  public static String getParamByHttpMethod(HttpServletRequest request, String name) {
    String httpMethod = request.getMethod();
    String value = "";
    if("GET".equals(httpMethod)) {
      value = getQueryParam(request, name);
    } else {
      value = getString(request,name,"");
    }

    return value;
  }

}

