/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesCalendar.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-11-08 19:00:21 -0800 (Tue, 08 Nov 2011) $
  Version            : $Revision: 19519 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;

public class MesCalendar
{
  static Logger log = Logger.getLogger(MesCalendar.class);
  
  public static final int      ONE_DAY_MS      = 86400000;

  // cannot instantiate this object, just a collection of static methods
	private MesCalendar( )    
  {
  }
  
  public static Date addDays( Date inDate, int days )
  {
    Calendar      cal     = Calendar.getInstance();
    Date          retVal  = null;
    
    try
    {
      cal.setTime(inDate);
      cal.add(Calendar.DAY_OF_MONTH,days);
      retVal = cal.getTime();
    }
    catch( Exception e )
    {
      // ignore
    }
    return( retVal );
  }
  
  public static java.sql.Date addDaysSql( Date inDate, int days )
  {
    java.sql.Date     retVal      = null;
    Date              temp        = null;
    
    temp = addDays(inDate,days);
    if (temp != null)
    {
      retVal = new java.sql.Date(temp.getTime());
    }
    return( retVal );
  }
  
  public static double daysBetween( Date beginDate, Date endDate )
  {
    return( ((double)(endDate.getTime() - beginDate.getTime())/(double)ONE_DAY_MS) );
  }

  public static long millisecsBetween(Date beginDate, Date endDate)
  {
    return (endDate.getTime() - beginDate.getTime());
  }
  
  /**
   * Added 6/28/2011, tbaker
   */
   
  /**
   * For a date, returns a date object set to midnight on the same day.
   * This would be the earliest possible time in that day, not the latest.
   * Useful for date range filtering, etc.
   */
  public static Date midnightOf(Date date)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(cal.HOUR_OF_DAY,0);
    cal.set(cal.MINUTE,0);
    cal.set(cal.SECOND,0);
    cal.set(cal.MILLISECOND,0);
    return cal.getTime();
  }

  /**
   * Conversion between java.util.Date and java.sql.Timestamp
   */

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }

  // date/time formatting strings
  public static final String FMT_HMMSS_A        = "h:mm:ss a";
  public static final String FMT_HMMSSA         = "h:mm:ssa";
  public static final String FMT_HMM_A          = "h:mm a";
  public static final String FMT_HMMA           = "h:mma";
  public static final String FMT_H_A            = "h a";
  public static final String FMT_HA             = "ha";

  public static final String FMT_DATE1          = "MM/dd/yyyy";
  public static final String FMT_DATE2          = "M/d/yyyy";

  public static final String FMT_DT1            = "MM/dd/yyyy h:mm:ss a";
  public static final String FMT_DT2            = "M/d/yyyy h:mm:ss a";
  public static final String FMT_DT3            = "M/d/yyyy h:mm a";

  public static final String FMT_HTML           = "'<nobr>'EEE M/d/yy'</nobr> "
                                                + "<nobr>'h:mma'</nobr>'";
  public static final String FMT_HTML2          = "'<nobr>'EEE M/d/yy'</nobr> "
                                                + "<nobr>'h:mm:ss a'</nobr>'";

  // default time formats
  public static final String[] DEFAULT_TIME_PATTERNS  =
    { FMT_HMMSS_A, FMT_HMMSSA, FMT_HMM_A, FMT_HMMA, FMT_H_A, FMT_HA };

  // default date formats 
  public static final String[] DEFAULT_DATE_PATTERNS  = 
    { FMT_DATE1, FMT_DATE2 };

  // default time stamp formats (date + time components)
  public static final String[] DEFAULT_DT_PATTERNS    = 
    { FMT_DT1, FMT_DT2, FMT_DT3 };
  
  /**
   * Core java.util.Date parse methods.
   *
   * Parses date string with the given format object.  Return null
   * in the case of a parse exception, else java.util.Date object.
   */
  public static Date parseDate(String dateStr, SimpleDateFormat sdf)
  {
    try
    {
      return sdf.parse(dateStr);
    }
    catch (ParseException pe) { }
    return null;
  }

  /**
   * Attempts to parse a formatted date string using an array of date 
   * format patterns.  Returns java.util.Date object if successful, 
   * else null.
   */
  public static Date parseDate(String dateStr, String[] patterns)
  {
    if (dateStr != null && dateStr.length() > 0)
    {
      SimpleDateFormat sdf = new SimpleDateFormat();
      for (int i = 0; i < patterns.length; ++i)
      {
        sdf.applyPattern(patterns[i]);
        Date date = parseDate(dateStr,sdf);
        if (date != null)
        {
          return date;
        }
      }
      log.warn("Unable to parse date string '" + dateStr + "'");
    }
    return null;
  }
  public static Date parseDate(String dateStr, String pattern)
  {
    return parseDate(dateStr,new String[] { pattern });
  }
  
  /**
   * String to date conversion methods.  Supports time, date and datetime
   * (date + time) conversions.  Times use current date for the date portion.
   * Date use midnight for the time.
   */

  /**
   * Times: sets date to current date, parses time fields.
   */
  public static Date stringToTime(String timeStr, String[] patterns)
  {
    Date time = parseDate(timeStr,patterns);

    // set to current date 
    if (time != null)
    {
      Calendar curCal = Calendar.getInstance();
      Calendar timeCal = Calendar.getInstance();
      timeCal.setTime(time);
      timeCal.set(Calendar.YEAR,curCal.get(Calendar.YEAR));
      timeCal.set(Calendar.MONTH,curCal.get(Calendar.MONTH));
      timeCal.set(Calendar.DATE,curCal.get(Calendar.DATE));
      time = timeCal.getTime();
    }
    return time;
  }
  public static Date stringToTime(String timeStr)
  {
    return stringToTime(timeStr,DEFAULT_TIME_PATTERNS);
  }
  public static Date stringToTime(String timeStr, String pattern)
  {
    return stringToTime(timeStr,new String[] { pattern });
  }
  
  /**
   * Dates: sets time to midnight, parses date fields.
   */
  public static Date stringToDate(String dateStr, String[] patterns)
  {
    Date date = parseDate(dateStr,patterns);
    
    // set to midnight
    if (date != null)
    {
      Calendar dateCal = Calendar.getInstance();
      dateCal.setTime(date);
      dateCal.set(Calendar.HOUR,0);
      dateCal.set(Calendar.MINUTE,0);
      dateCal.set(Calendar.SECOND,0);
      dateCal.set(Calendar.MILLISECOND,0);
      date = dateCal.getTime();
    }
    return date;
  }
  public static Date stringToDate(String dateStr)
  {
    return stringToDate(dateStr,DEFAULT_DATE_PATTERNS);
  }
  public static Date stringToDate(String dateStr, String pattern)
  {
    return stringToDate(dateStr,new String[] { pattern });
  }

  /**
   * DateTimes: parses date and time fields. (Equivalent to parse* methods)
   */  
  public static Date stringToDateTime(String dtStr, String[] patterns)
  {
    return parseDate(dtStr,patterns);
  }
  public static Date stringToDateTime(String dtStr)
  {
    return stringToDateTime(dtStr,DEFAULT_DT_PATTERNS);
  }
  public static Date stringToDateTime(String dtStr, String pattern)
  {
    return stringToDateTime(dtStr,new String[] { pattern });
  }
  
  /**
   * String formatting methods.  Returns null if date is null.
   */
  
  public static String formatDate(Date date, SimpleDateFormat sdf)
  {
    return date == null ? null : sdf.format(date);
  }
  public static String formatDate(Date date, String pattern)
  {
    return formatDate(date,new SimpleDateFormat(pattern));
  }
}

