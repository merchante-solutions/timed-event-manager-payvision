/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/PropertiesFile.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-06-01 12:23:38 -0700 (Fri, 01 Jun 2012) $
  Version            : $Revision: 20223 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

public class PropertiesFile extends Properties
{
  public PropertiesFile( ) 
  {
  }
  
  public PropertiesFile( String filename )
  {
    load( filename );
  }
  
  public PropertiesFile( InputStream inputStream )
  {
    try
    {
      load( inputStream );
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + ": " + e.toString());
    }
  }
  
  public Enumeration getPropertyNames()
  {
    return propertyNames();
  }
  
  public boolean getBoolean( String name )
  {
    return( getBoolean( name, false ) );
  }
  
  public boolean getBoolean( String name, boolean defaultValue )
  {
    boolean       value = defaultValue;
    
    try
    {
      // make sure that string has something in it.
      if ( ( name != null ) && ! name.equals("") )
      {
        String boolString = getProperty(name);
        
        if( boolString != null )
        {
          if( boolString.compareToIgnoreCase("Y") == 0 || 
              boolString.compareToIgnoreCase("yes") == 0 || 
              boolString.compareToIgnoreCase("true") == 0 || 
              boolString.equals("1") )
          {
            value = true;
          }
          else if ( boolString.compareToIgnoreCase("N") == 0 || 
                    boolString.compareToIgnoreCase("no") == 0 || 
                    boolString.compareToIgnoreCase("false") == 0 || 
                    boolString.equals("0") )
          {
            value = false;
          }
          // else undefined, return default
        }      
      }
    }
    catch( Exception e )
    {
    }
    return( value );
  }
  
  public Date getDate( String name )
  {
    return( getDate( name, "MMddyyyy", null ) );
  }
  
  public Date getDate( String name, String format )
  {
    return( getDate( name, format, null ) );
  }

  public Date getDate( String name, String format, Date defaultValue )
  {
    Date        value = defaultValue;
    
    try
    {
      value = DateTimeFormatter.parseDate( getProperty( name ), format );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    return( value );
  }
  
  public double getDouble( String name )
  {
    return( getDouble( name, 0.0 ) );
  }

  public double getDouble( String name, double defaultValue )
  {
    double      value = defaultValue;
    
    try
    {
      value = Double.parseDouble( getProperty( name ) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    return( value );
  }
  
  public float getFloat( String name )
  {
    return( getFloat( name, (float)0.0 ) );
  }

  public float getFloat( String name, float defaultValue )
  {
    float       value = defaultValue;
    
    try
    {
      value = Float.parseFloat( getProperty( name ) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    return( value );
  }
  
  public int getInt( String name )
  {
    return( getInt( name, 0  ) );
  }

  public int getInt( String name, int defaultValue )
  {
    int value = defaultValue;
    
    try
    {
      value = Integer.parseInt( getProperty( name ) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  public long getLong( String name )
  {
    return( getLong( name, 0L ) );
  }
  
  public long getLong( String name, long defaultValue )
  {
    long value = defaultValue;
    
    try
    {
      value = Long.parseLong( getProperty( name ) );
    }
    catch( Exception e )
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  public String getString( String name )
  {
    return( getString( name, "" ) );
  }
  
  public String getString( String name, String defaultValue )
  {
    String value = defaultValue;
    
    try
    {
      if( getProperty( name ) != null )
      {
        value = getProperty( name );
      }
    }
    catch(Exception e)
    {
      value = defaultValue;
    }
    
    return( value );
  }
  
  public void load( String filename )
  {
    FileInputStream     fileStream          = null;
    try
    {
      fileStream = new FileInputStream( filename );
      load( new DataInputStream( fileStream ) );
    }
    catch( java.io.FileNotFoundException e )
    {
    }      
    catch( java.io.IOException e )
    {
    }
    finally
    {
      try
      {
        fileStream.close();   // release the file handle
      }
      catch( Exception e )
      {
      }
    }
  }
  
  public void store( String filename )
  {
    store( filename, null );
  }
  
  public void store( String filename, String header )
  {
    FileOutputStream        fileOutput = null;
    
    try
    {
      fileOutput = new FileOutputStream( filename );
      store( fileOutput, header );
    }
    catch( java.io.IOException e )
    {
    }
    finally
    {
      try
      {
        fileOutput.close();
      }
      catch( Exception e )
      {
      }
    }
  }
}