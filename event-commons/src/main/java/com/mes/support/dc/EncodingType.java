package com.mes.support.dc;

import java.io.InputStream;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.support.CodeType;

public class EncodingType extends CodeType
{
  static Logger log = Logger.getLogger(EncodingType.class);
  
  public static EncodingType UNKNOWN 
    = new EncodingType("UNKNOWN",           "Unknown");
  public static EncodingType MES_BLK_ENCRYPT_1
    = new EncodingType("MES.BLK.ENCRYPT.1", "MES Encrypted Block Encoding 1");
  public static EncodingType MES_BLK_COMPRESS_1
    = new EncodingType("MES.BLK.COMPRESS.1","MES Compressed Block Encoding 1");
  
  private static int MAX_LENGTH = 0;

  static
  {
    codeHash.put(""+UNKNOWN,UNKNOWN);
    codeHash.put(""+MES_BLK_ENCRYPT_1,MES_BLK_ENCRYPT_1);
    codeHash.put(""+MES_BLK_COMPRESS_1,MES_BLK_COMPRESS_1);

    // determine the longest code    
    for (Iterator i = codeHash.keySet().iterator(); i.hasNext();)
    {
      String code = ""+i.next();
      if (code.length() > MAX_LENGTH)
      {
        MAX_LENGTH = code.length();
      }
      MAX_LENGTH += 5;
    }
  };

  private EncodingType(String code, String description)
  {
    super(code,description);
  }

  public static EncodingType forType(String forStr, EncodingType dflt)
  {
    return (EncodingType)forCode(forStr,dflt);
  }

  public static EncodingType forType(String forStr)
  {
    return (EncodingType)forCode(forStr);
  }
  
  /**
   * Examine the leading bytes from an input string, match to an encoding 
   * type if possible.  The stream is closed upon return.  Returns
   * UNKNOWN if no match.
   */
  public static EncodingType forType(InputStream in)
    throws Exception
  {
    try
    {
      byte[] buf = new byte[MAX_LENGTH];
      int readCount = in.read(buf);
      if (readCount > 0)
      {
        String inStr = new String(buf,0,readCount);
        for (Iterator i = codeHash.keySet().iterator(); i.hasNext();)
        {
          String codeStr = ""+i.next();
          if (inStr.startsWith(codeStr))
          {
            return forType(codeStr);
          }
        }
      }
      return UNKNOWN;
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
  
  public static int maxLength()
  {
    return MAX_LENGTH;
  }
}
   
