package com.mes.support.dc;

public class MesSecureFileContainer extends EncodedFileContainerImpl
{
  public MesSecureFileContainer()
  {
    super(new MesSecureStreamEncoder());
  }
}