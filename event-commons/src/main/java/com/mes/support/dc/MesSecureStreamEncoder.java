package com.mes.support.dc;

import java.io.InputStream;
import org.apache.log4j.Logger;

/**
 * Provides a mechanism for compressing and encrypting streams of data.  Used
 * by MesSecureFileContainer to encode and decode large data files.
 */
public class MesSecureStreamEncoder implements StreamEncoder
{
  static Logger log = Logger.getLogger(MesSecureStreamEncoder.class);
  
  private static final String SEPARATOR = "@";
    
  /**
   * Encoding = compression + encryption.
   *
   * Nested input streams, inner stream is the deflater, outer the encrypter.
   * This should allow maximum compression of data before encryption.
   */
  public InputStream getEncodingInputStream(InputStream in)
  {
    return new MesEncryptingInputStream(new MesDeflatingInputStream(in));
  }
  
  /**
   * Decoding = decryption + expansion
   *
   * Nested input streams, inner stream is the decrypter, outer the inflater.
   */
  public InputStream getDecodingInputStream(InputStream in)
  {
    return new MesInflatingInputStream(new MesDecryptingInputStream(in));
  }
}