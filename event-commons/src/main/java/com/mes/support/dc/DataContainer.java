package com.mes.support.dc;

import java.io.InputStream;

public interface DataContainer
{
  public void setData(InputStream in) throws Exception;
  public void setData(byte[] data);
  public void setData(String dataStr);
  public byte[] getData();
  public boolean empty();
  public int size();
}