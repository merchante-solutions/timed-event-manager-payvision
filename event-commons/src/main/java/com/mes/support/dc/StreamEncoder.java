package com.mes.support.dc;

import java.io.InputStream;

public interface StreamEncoder
{
  public InputStream getEncodingInputStream(InputStream in);
  public InputStream getDecodingInputStream(InputStream in);
}