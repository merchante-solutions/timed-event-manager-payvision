package com.mes.support.dc;

public interface CompressionHandler
{
  public byte[] compress(byte[] data, int off, int len) throws Exception;
  public byte[] compress(byte[] data) throws Exception;
  public byte[] decompress(byte[] data) throws Exception;
}