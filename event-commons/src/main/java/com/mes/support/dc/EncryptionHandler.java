package com.mes.support.dc;

public interface EncryptionHandler
{
  public byte[] encrypt(byte[] data) throws Exception;
  public byte[] decrypt(byte[] data) throws Exception;
}