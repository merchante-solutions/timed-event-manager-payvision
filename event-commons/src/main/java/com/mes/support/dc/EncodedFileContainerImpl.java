package com.mes.support.dc;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class EncodedFileContainerImpl extends FileContainerImpl 
  implements EncodedFileContainer
{
  protected StreamEncoder encoder;
  protected boolean encodedFlag;
  protected long encodedSize;
  protected long size;
  
  public EncodedFileContainerImpl(StreamEncoder encoder)
  {
    this.encoder = encoder;
  }
  
  /**
   * Override data accessors.  These will wrap streams
   * with encoder/decoder input stream wrappers as
   * necessary.
   */
  
  public void setData(InputStream in)
  {
    encodedFlag = false;
    super.setData(in);
  }
  
  public InputStream getInputStream() 
  {
    if (item != null)
    {
      if (encodedFlag)
      {
        // need to decode, wrap in decoder filter stream
        return encoder.getDecodingInputStream(item.getInputStream());
      }
      // return direct access input stream, already decoded
      return item.getInputStream();
    }
    return null;
  }
  
  /**
   * Encoded data accessors.  These will wrap streams
   * with encoder/decoder input stream wrappers as
   * necessary.
   */

  public void setEncodedData(InputStream in)
  {
    encodedFlag = true;
    super.setData(in);
  }
  
  public void setEncodedData(byte[] data)
  {
    setEncodedData(new ByteArrayInputStream(data));
  }
  
  public InputStream getEncodedInputStream() 
  {
    if (item != null)
    {
      if (!encodedFlag)
      {
        // need to encode, wrap in encoder filter stream
        return encoder.getEncodingInputStream(item.getInputStream());
      }
      // already encoded, return direct access input stream
      return item.getInputStream();
    }
    return null;
  }
  
  public OutputStream getOutputStream()
  {
    encodedFlag = false;
    return super.getOutputStream();
  }
  
  public OutputStream getEncodedOutputStream()
  {
    encodedFlag = true;
    return super.getOutputStream();
  }
  
  public int writeEncodedData(OutputStream out)
  {
    InputStream in = null;
    
    try
    {
      in = getEncodedInputStream();
      return sendStream(in,out);
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
  
  public int encodeStream(InputStream in, OutputStream out)
  {
    return sendStream(encoder.getEncodingInputStream(in),out);
  }
                                    
  public int decodeStream(InputStream in, OutputStream out)
  {
    return sendStream(encoder.getDecodingInputStream(in),out);
  }
}