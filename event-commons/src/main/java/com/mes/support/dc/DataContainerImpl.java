package com.mes.support.dc;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.apache.log4j.Logger;

public class DataContainerImpl implements DataContainer
{
  static Logger log = Logger.getLogger(DataContainerImpl.class);
  
  protected byte[] data;

  public void setData(byte[] data)
  {
    this.data = data;
  }

  protected byte[] bytesFromInputStream(InputStream in) throws Exception
  {
    try
    {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] inBuf = new byte[1024];
      int readCount = 0;
      int readTotal = 0;
      while ((readCount = in.read(inBuf)) != -1)
      {
        baos.write(inBuf,0,readCount);
        readTotal += readCount;
      }
      return baos.toByteArray();
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }

  public void setData(InputStream in) throws Exception
  {
    setData(bytesFromInputStream(in));
  }

  public void setData(String dataStr)
  {
    setData(dataStr.getBytes());
  }

  public byte[] getData()
  {
    return data;
  }
    
  public boolean empty()
  {
    return data == null;
  }

  public int size()
  {
    return (empty() ? 0 : getData().length);
  }

  public String toString()
  {
    return new String(getData());
  }
}