package com.mes.support.dc;

import java.io.InputStream;
import org.apache.log4j.Logger;

public class MesDeflatingInputStream extends MesFilterInputStream
{
  static Logger log = Logger.getLogger(MesDeflatingInputStream.class);
  
  public static final int     DFLT_DEF_READ_SIZE  = 500000;
  public static final int     MAX_PREFIX_LEN      = 8;
  
  private CompressionHandler compressor;
  
  public MesDeflatingInputStream(InputStream in, int readSize)
  {
    super(in,readSize);
    this.compressor = new MesCompressionHandler();
  }
  public MesDeflatingInputStream(InputStream in)
  {
    this(in,DFLT_DEF_READ_SIZE);
  }
  
  protected EncodingType getEncodingType()
  {
    return EncodingType.MES_BLK_COMPRESS_1;
  }
  
  /**
   * Takes byte buffer with data to be deflated and a length.  Deflates
   * data and returns byte array containing a deflated data block length
   * prefix followed by deflated data: <length><SEPARATOR><deflated data>
   */
  protected byte[] filterData(byte[] data, int dataCount) throws Exception
  {
    // return block encoded compressed data
    return encodeBlock(compressor.compress(data,0,dataCount));
  }
} 

