package com.mes.support.dc;

import java.io.InputStream;

public interface EncodedDataContainer extends DataContainer
{
  public void setEncodedData(byte[] encData);
  public void setEncodedData(InputStream in) throws Exception;
  public byte[] getEncodedData();
}