package com.mes.support.dc;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;

public abstract class MesFilterInputStream extends FilterInputStream
{
  static Logger log = Logger.getLogger(MesFilterInputStream.class);
  
  public static final int     DFLT_READ_SIZE        = 50000;
  public static final String  DFLT_SEPARATOR        = "@";
  public static final int     DFLT_BLOCK_PREFIX_LEN = 10;
  
  private int     readSize;
  private byte[]  filteredData;
  private int     filteredPos;
  private boolean firstBlock = true;
  
  public MesFilterInputStream(InputStream in, int readSize)
  {
    super(in);
    this.readSize = readSize;
  }
  public MesFilterInputStream(InputStream in)
  {
    this(in,DFLT_READ_SIZE);
  }
  
  protected boolean isBlockEncoded()
  {
    return false;
  }
  
  protected abstract EncodingType getEncodingType();
  
  /**
   * Reads default block encoded prefix from data stream.  Determines size
   * of data block to follow. If end of data is encountered -1 is returned.
   */
  protected int getBlockSize() throws Exception
  {
    // discard encoding type prefix
    if (firstBlock)
    {
      // discard encoding type code + separator
      int discard = (""+getEncodingType()).length() + DFLT_SEPARATOR.length();
      for (int i = 0; i < discard; ++i)
      {
        if (in.read() == -1)
        {
          throw new IOException("Unexpected end of data (encoding type)");
        }
      }
      firstBlock = false;
    }
    
    byte[] prefix = new byte[MesEncryptingInputStream.DFLT_BLOCK_PREFIX_LEN];
    int readCount = 0;
    int readCh = 0;
    while ((readCh = in.read()) != -1 && 
            readCh != MesEncryptingInputStream.SEPARATOR.charAt(0))
    {
      // sanity check, data blocks should not be larger than
      // can be specified in the max prefix length number of bytes
      if (readCount >= MesEncryptingInputStream.DFLT_BLOCK_PREFIX_LEN)
      {
        throw new IOException("Invalid encrypted block prefix");
      }

      prefix[readCount++] = (byte)readCh;
    }
    
    // end of data encountered unexpectedly?
    if (readCh == -1 && readCount > 0)
    {
      throw new IOException("Unexpected end of data (prefix)");
    }
    
    // all data has been read from the data stream
    if (readCh == -1)
    {
      return -1;
    }
    
    // translate the prefix into an int block size
    String pStr = new String(prefix,0,readCount);
    return Integer.parseInt(pStr);
  }
  
  /**
   * Default block encoding: <block length><separator><data>
   * The first block encoded also has the encoding type in the prefix.
   */
  protected byte[] encodeBlock(byte[] data)
  {
    byte[] prefix = null;
    if (firstBlock)
    {
      prefix = (""+getEncodingType() + DFLT_SEPARATOR 
        + data.length + DFLT_SEPARATOR).getBytes();
      firstBlock = false;
    }
    else
    {
      prefix = (""+data.length + DFLT_SEPARATOR).getBytes();
    }
    byte[] block = new byte[prefix.length + data.length];
    System.arraycopy(prefix,0,block,0,prefix.length);
    System.arraycopy(data,0,block,prefix.length,data.length);
    return block;
  }
    
  protected abstract byte[] filterData(byte[] data, int dataCount) 
    throws Exception;
    
  private int readFilteredData() throws Exception
  {
    // don't fetch new data unless all of filtered data has been consumed
    if (filteredData != null && filteredPos < filteredData.length)
    {
      // data still available, return length of consumable data
      return filteredData.length - filteredPos;
    }
    
    // is the unfiltered data block encoded (need to do block decoding)
    byte[] data = null;
    int dataCount = 0;
    if (isBlockEncoded())
    {
      int blockSize = getBlockSize();
      if (blockSize == -1)
      {
        return -1;
      }
      data = new byte[blockSize];
      if ((dataCount = in.read(data)) < blockSize)
      {
        throw new IOException("Unexpected end of encoded data block");
      }
    }
    // just read as much non-block data as possible
    else
    {
      data = new byte[readSize];
      if ((dataCount = in.read(data)) == -1)
      {
        return -1;
      }
    }
    
    // get the filtered data, assign it as the filtered data buffer
    filteredData = filterData(data,dataCount);
    
    // reset filtered data offset
    filteredPos = 0;

    // return new size of filtered data buffer
    return filteredData.length;
  }

  public int read(byte[] buf, int off, int len) throws IOException
  {
    try
    {
      // load provided buf until full or end of data reached
      int readCount = 0;
      int readWanted = len - off;
      int fCount = 0;
      int copyLen = 0;
      while ((fCount = readFilteredData()) != -1 && readCount < readWanted)
      {
        // determine smaller of dest buffer and source buffer
        if (fCount > readWanted - readCount)
        {
          copyLen = readWanted - readCount;
        }
        else
        {
          copyLen = fCount;
        }
      
        System.arraycopy(filteredData,filteredPos,buf,off + readCount,copyLen);
        filteredPos += copyLen;
        readCount += copyLen;
      }
    
      // return end of data signal if no data read before end of file
      if (readCount == 0 && fCount == -1)
      {
        return -1;
      }
      
      // return data read count
      return readCount;
    }
    catch (Exception e)
    {
      log.error("Error: ", e);
      e.printStackTrace();
      throw new IOException(""+e);
    }
  }
  
  public int read(byte[] buf) throws IOException
  {
    return read(buf,0,buf.length);
  }
  
  public int read() throws IOException
  {
    byte[] buf = new byte[1];
    int readCount = read(buf);
    if (readCount == -1)
    {
      return -1;
    }
    return buf[0];
  }
}