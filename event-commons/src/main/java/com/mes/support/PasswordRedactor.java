package com.mes.support;

import java.util.StringTokenizer;
import java.util.Vector;

public class PasswordRedactor
{
  public static String redactPassword( String data, Vector passwordFieldNames )
  {
    StringBuffer value = new StringBuffer("");
    
    if( data != null && passwordFieldNames != null )
    {
      String token = ";";
      
      if( data.indexOf('&') > -1 && data.indexOf(';') == -1 )
      {
        // set token to be ampersand
        token = "&";
      }
      
      StringTokenizer st = new StringTokenizer(data, token, true);
      
      String nextStr = "";
      while( st.hasMoreTokens() )
      {
        nextStr = st.nextToken();
        
        boolean isPass = false;
        //System.out.println("* Testing: " + nextStr);
        for( int i = 0; i < passwordFieldNames.size(); ++i )
        {
          if( nextStr.indexOf( (String)passwordFieldNames.elementAt(i) ) >= 0 )
          {
            isPass = true;
            //System.out.println("  * Password detected");
            value.append(passwordFieldNames.elementAt(i));
            value.append("xxxxxx");
            break;
          }
        }
        
        if( ! isPass )
        {
          //System.out.println("  * not password value, so appending: " + nextStr);
          value.append(nextStr);
        }
      }
    }
    
    return( value.toString() );
  }
  
  public static void main(String[] args)
    throws Exception
  {
    //System.out.println( args[0] );
    
    Vector passwordFieldNames = new Vector();
    
    passwordFieldNames.add("password=");
    passwordFieldNames.add("passwordConfirm=");
    passwordFieldNames.add("userPassword=");
    passwordFieldNames.add("userPass=");
    passwordFieldNames.add("adyenPassword=");
    passwordFieldNames.add("newPassword=");
    
    System.out.println("redacting passwords");
    System.out.println( redactPassword(args[0], passwordFieldNames) );
    System.out.println("done...");
  }
}
