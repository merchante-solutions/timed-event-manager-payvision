/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/NumberFormatter.java $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter
{
  public static final String    CURRENCY_FORMAT     = "$###,###,##0.00;($###,###,##0.00)";
  public static final String    WEB_CURRENCY_FORMAT = "$###,###,##0.00;<font color=\"red\">($###,###,##0.00)</font>";
  
  public static String getDoubleString(double value, String format)
  {
    String result = "";
    
    try
    {
      DecimalFormat df = new DecimalFormat(format);
      result = df.format(value);
    }
    catch(Exception e)
    {
    }
    
    return( result );
  }
  
  public static String getLongString(long value, String format)
  {
    String result = "";
    
    try
    {
      DecimalFormat df = new DecimalFormat(format);
      result = df.format(value);
    }
    catch(Exception e)
    {
    }
    
    return( result );
  }
  
  public static String getPercentString( double value )
  {
    return( getPercentString(value,2) );
  }
    
  public static String getPercentString( double value, int decimalCount )
  {
    NumberFormat        format      = NumberFormat.getPercentInstance();
    String              retVal      = "";
    
    try
    {
      format.setMaximumFractionDigits(decimalCount);
      
      // assumes that 1% will be passed as 
      // 0.01 in the value param.
      retVal = format.format(value);
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public static String getPaddedInt(int value, int size)
  {
    return( getPaddedLong((long)value,size) );
  }
  
  public static String getPaddedLong(long value, int size)
  {
    String retVal = "ERROR";
    StringBuffer digitFormat    = new StringBuffer("");
    
    try
    {
      for(int i=0; i < size; ++i)
      {
        digitFormat.append("0");
      }
      
      DecimalFormat df = new DecimalFormat(digitFormat.toString());
      
      retVal = df.format(value);
    }
    catch(Exception e)
    {
    }
    
    return( retVal );
  }
  
  public static double parseDoubleCielo(String value)
  {
    return( parseDouble(value,new Locale("pt","BR")) );
  }
  
  public static double parseDouble(String value)
  {
    return( parseDouble(value,Locale.US) );
  }
  
  public static double parseDouble(String value, Locale theLocal)
  {
    double    retVal      = 0.0;
    
    try
    {
      NumberFormat nf = java.text.NumberFormat.getInstance(theLocal);
      retVal = nf.parse(value).doubleValue();
    }
    catch( Exception e )
    {
    }
    return( retVal );  
  }
}
