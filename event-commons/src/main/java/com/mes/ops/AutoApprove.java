/*@lineinfo:filename=AutoApprove*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoApprove.sqlj $

  Description:

    AutoApprove

    Tools for auto approving (and auto uploading if necessary) an
    online application

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2008-11-12 09:52:51 -0800 (Wed, 12 Nov 2008) $
  Version            : $Revision: 15520 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.AppNotifyBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class AutoApprove extends SQLJConnectionBase
{
  private long      appSeqNum           = 0L;
  private int       appType             = -1;
  
  private int       sicCode             = 0;
  private String    invCode             = "";
  private String    metCode             = "";
  private int       creditDelay         = 0;
  private int       debitDelay          = 0;
  private int       daysSuspend         = 0;
  private String    association         = "";
  private int       bankNumber          = 0;
  private String    statusIndicator     = "";
  private String    appSource           = "";
  
  private String    incStatus           = "";
  private String    userData1           = "";
  private String    userData2           = "";
  private String    userData3           = "";
  private String    userData4           = "";
  private String    userData5           = "";
  private String    userAccount1        = "";
  private String    userAccount2        = "";
  
  private boolean   assocAssigned       = false;
  private boolean   manualConversion    = false;
  
  /*
  ** AutoApprove()
  */ 
  public AutoApprove()
  {
    super();
  }
  
  /*
  ** AutoApprove(DefaultContext Ctx) constructor
  **
  ** Latches on to passed default context for database interaction
  */
  public AutoApprove(DefaultContext defCtx)
  {
    super();
    
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  public void setManualConversion(boolean manual)
  {
    manualConversion = manual;
  }
  
  /*
  ** moveToAccountSetup()
  **
  ** Moves account to Account Setup queues for normal processing
  */
  private void moveToAccountSetup()
  {
    try
    {
      java.sql.Timestamp  startDate;
      int                 appUser;
      String              appSource;
      
      // get app created date, user, and source
      /*@lineinfo:generated-code*//*@lineinfo:113^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.app_created_date,
//                  app.app_user_id,
//                  app.app_user_login
//          
//          from    application app
//          where   app.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.app_created_date,\n                app.app_user_id,\n                app.app_user_login\n         \n        from    application app\n        where   app.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   startDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   appUser = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appSource = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:125^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue
//          (
//            app_seq_num,
//            app_queue_type,
//            app_queue_stage,
//            app_start_date,
//            app_status,
//            app_user,
//            app_source,
//            app_last_date,
//            app_last_user,
//            is_rush
//          )
//          values
//          (
//            :appSeqNum,
//            :QueueConstants.QUEUE_SETUP,
//            :QueueConstants.Q_SETUP_NEW,
//            :startDate,
//            :QueueConstants.Q_STATUS_APPROVED,
//            :appUser,
//            :appSource,
//            sysdate,
//            'SYSTEM',
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue\n        (\n          app_seq_num,\n          app_queue_type,\n          app_queue_stage,\n          app_start_date,\n          app_status,\n          app_user,\n          app_source,\n          app_last_date,\n          app_last_user,\n          is_rush\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n          sysdate,\n          'SYSTEM',\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(3,QueueConstants.Q_SETUP_NEW);
   __sJT_st.setTimestamp(4,startDate);
   __sJT_st.setInt(5,QueueConstants.Q_STATUS_APPROVED);
   __sJT_st.setInt(6,appUser);
   __sJT_st.setString(7,appSource);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^7*/
      
      // notify
      AppNotifyBean anb = new AppNotifyBean();
      
      anb.notifyStatus(appSeqNum, mesConstants.APP_STATUS_APPROVED, -1);
    }
    catch(Exception e)
    {
      logEntry("moveToAccountSetup()", e.toString());
    }
  }
  
  /*
  ** assignAssociation()
  **
  ** Assigns association number as necessary.  Note that certain situations
  ** require that the account be put on hold until an association is built at
  ** TSYS.
  */
  private void assignAssociation()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              pcc         = null;
    String              primaryPcc  = null;
    String              mifAssoc    = "";
    String              merchAssoc  = "";
    
    try
    {
      switch(appType)
      {
        case mesConstants.APP_TYPE_SABRE:
          // establish type of sabre app
          /*@lineinfo:generated-code*//*@lineinfo:188^11*/

//  ************************************************************
//  #sql [Ctx] { select  upper(pcc),
//                      upper(primary_pcc)
//              
//              from    merchant_sabre
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(pcc),\n                    upper(primary_pcc)\n             \n            from    merchant_sabre\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pcc = (String)__sJT_rs.getString(1);
   primaryPcc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^11*/
          
          if(primaryPcc == null)
          {
            // generic, single-PCC merchant.  default is appropriate
            assocAssigned = true;
          }
          else if(primaryPcc.equals(pcc))
          {
            // primary location for a multi-pcc situation.  This account
            // must be moved into Account Setup for normal processing.
            // once the association is built and assigned
            moveToAccountSetup();
          }
          else
          {
            // this is an additional location for a multi-psuedo merchant.
            // check to see if association is assigned.  if so then go for it
            // otherwise we're in a holding pattern
            
            // get appSeqNUm and merchant number for primary account
            /*@lineinfo:generated-code*//*@lineinfo:217^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.dmagent,
//                        m.asso_number
//                from    mif       mf,
//                        merchant  m,
//                        merchant_sabre ms1,
//                        merchant_sabre ms2
//                where   ms1.app_seq_num = :appSeqNum and
//                        upper(ms2.pcc) = upper(ms1.primary_pcc) and
//                        ms2.app_seq_num = m.app_seq_num and
//                        m.merch_number = mf.merchant_number(+)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.dmagent,\n                      m.asso_number\n              from    mif       mf,\n                      merchant  m,\n                      merchant_sabre ms1,\n                      merchant_sabre ms2\n              where   ms1.app_seq_num =  :1  and\n                      upper(ms2.pcc) = upper(ms1.primary_pcc) and\n                      ms2.app_seq_num = m.app_seq_num and\n                      m.merch_number = mf.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AutoApprove",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^13*/
            
            rs = it.getResultSet();
            
            if(rs.next())
            {
              mifAssoc = rs.getString("dmagent");
              merchAssoc = rs.getString("asso_number");
            }
            
            rs.close();
            it.close();
            
            if(mifAssoc != null && !mifAssoc.equals(""))
            {
              // use association number from mif
              association = mifAssoc;
              assocAssigned = true;
            }
            else if(merchAssoc != null && !merchAssoc.equals(""))
            {
              // use association number from merchant
              association = merchAssoc;
              assocAssigned = true;
            }
            else
            {
              // no association assigned yet so this app is in limbo
            }
          }
          
          if(assocAssigned)
          {
            /*@lineinfo:generated-code*//*@lineinfo:262^13*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_sabre
//                set     association = :association
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_sabre\n              set     association =  :1 \n              where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,association);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^13*/
          }
          break;
          
        default:
          // just assign association as it was loaded from app_auto_approve_defaults
          assocAssigned = true;
          break;
      }
      
      if(assocAssigned)
      {
        // update association in merchant table
        /*@lineinfo:generated-code*//*@lineinfo:280^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     asso_number = :association
//            where   app_seq_num = :appSeqNum
//                    and asso_number is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     asso_number =  :1 \n          where   app_seq_num =  :2 \n                  and asso_number is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,association);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("assignAssociation()", e.toString());
    }
  }
  
  protected boolean isTridentApp(long primaryKey)
  {
    boolean           result        = false;

    try
    {
      connect();
      
      int recCount = 0;
      
      // currently only sure indicator of a trident app is one that uses TVT
      /*@lineinfo:generated-code*//*@lineinfo:306^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mp.app_seq_num)
//          
//          from    merch_pos mp,
//                  pos_category pc
//          where   mp.app_seq_num = :primaryKey and
//                  mp.pos_code = pc.pos_code and
//                  upper(nvl(pc.trident_only,'N')) = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mp.app_seq_num)\n         \n        from    merch_pos mp,\n                pos_category pc\n        where   mp.app_seq_num =  :1  and\n                mp.pos_code = pc.pos_code and\n                upper(nvl(pc.trident_only,'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:315^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isTridentApp("+primaryKey+"): " + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }
  
  /*
  ** getUserData()
  **
  ** Generates user data fields and stores them.
  **
  ** NOTE:  This method must generate the value for incStatus which will 
  **        actually be stored back in expandData()
  */
  private void getUserData()
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    
    try
    {
      switch(bankNumber)
      {
        case mesConstants.BANK_ID_MES:
        case mesConstants.BANK_ID_STERLING:
          /*@lineinfo:generated-code*//*@lineinfo:350^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.bustype_code          bustype_code,
//                      m.merch_referring_bank  user_data_2,
//                      decode(m.svb_cif_num,
//                        null, 
//                        lpad(nvl(m.merch_month_visa_mc_sales,0) * 12, 8, '0') ||
//                          lpad(nvl(m.merch_average_cc_tran,0), 4, '0') ||
//                          lpad(nvl(m.merch_mail_phone_sales,0), 3, '0') ||
//                          decode(m.account_type,
//                            'C', 'C',
//                            null, 'N',
//                            'N'),
//                        m.svb_cif_num)        user_data_4,
//                      decode(nvl(m.existing_verisign,'N'), 'Y', 'OV', pc.pos_abbrev)  pos_abbrev
//              from    merchant      m,
//                      merch_pos     mp,
//                      pos_category  pc
//              where   m.app_seq_num = :appSeqNum and
//                      m.app_seq_num = mp.app_Seq_num and
//                      mp.pos_code = pc.pos_code      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.bustype_code          bustype_code,\n                    m.merch_referring_bank  user_data_2,\n                    decode(m.svb_cif_num,\n                      null, \n                      lpad(nvl(m.merch_month_visa_mc_sales,0) * 12, 8, '0') ||\n                        lpad(nvl(m.merch_average_cc_tran,0), 4, '0') ||\n                        lpad(nvl(m.merch_mail_phone_sales,0), 3, '0') ||\n                        decode(m.account_type,\n                          'C', 'C',\n                          null, 'N',\n                          'N'),\n                      m.svb_cif_num)        user_data_4,\n                    decode(nvl(m.existing_verisign,'N'), 'Y', 'OV', pc.pos_abbrev)  pos_abbrev\n            from    merchant      m,\n                    merch_pos     mp,\n                    pos_category  pc\n            where   m.app_seq_num =  :1  and\n                    m.app_seq_num = mp.app_Seq_num and\n                    mp.pos_code = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.AutoApprove",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:371^11*/
      
          rs = it.getResultSet();
          
          if(rs.next())
          {
            incStatus       = rs.getString("bustype_code");
            userData2       = rs.getString("user_data_2");
            userData3       = appSource + (isTridentApp(appSeqNum)?"T":"V") + rs.getString("pos_abbrev");
            userData4       = rs.getString("user_data_4");
          }
          
          rs.close();
          it.close();
          break;
          
        // add other bank numbers as necessary
        default:
          break;
      }
      
      // update user data in merchant_data table
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:395^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_data
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^7*/
      
      if(recCount == 0)
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:406^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_data
//            (
//              app_seq_num,
//              user_data_1,
//              user_data_2,
//              user_data_3,
//              user_data_4,
//              user_data_5,
//              user_account_1,
//              user_account_2
//            )
//            values
//            (
//              :appSeqNum,
//              :userData1,
//              :userData2,
//              :userData3,
//              :userData4,
//              :userData5,
//              :userAccount1,
//              :userAccount2
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_data\n          (\n            app_seq_num,\n            user_data_1,\n            user_data_2,\n            user_data_3,\n            user_data_4,\n            user_data_5,\n            user_account_1,\n            user_account_2\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,userData1);
   __sJT_st.setString(3,userData2);
   __sJT_st.setString(4,userData3);
   __sJT_st.setString(5,userData4);
   __sJT_st.setString(6,userData5);
   __sJT_st.setString(7,userAccount1);
   __sJT_st.setString(8,userAccount2);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:430^9*/
      }
      else
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:435^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_data
//            set     user_data_1 = :userData1,
//                    user_data_2 = :userData2,
//                    user_data_3 = :userData3,
//                    user_data_4 = :userData4,
//                    user_data_5 = :userData5,
//                    user_account_1 = :userAccount1,
//                    user_account_2 = :userAccount2
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_data\n          set     user_data_1 =  :1 ,\n                  user_data_2 =  :2 ,\n                  user_data_3 =  :3 ,\n                  user_data_4 =  :4 ,\n                  user_data_5 =  :5 ,\n                  user_account_1 =  :6 ,\n                  user_account_2 =  :7 \n          where   app_seq_num =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userData1);
   __sJT_st.setString(2,userData2);
   __sJT_st.setString(3,userData3);
   __sJT_st.setString(4,userData4);
   __sJT_st.setString(5,userData5);
   __sJT_st.setString(6,userAccount1);
   __sJT_st.setString(7,userAccount2);
   __sJT_st.setLong(8,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^9*/
      }
      
    }
    catch(Exception e)
    {
      logEntry("getUserData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** expandData()
  **
  ** Generates account-specific data based on a combination of app-type defaults
  ** and app-specific data
  */
  private void expandData()
    throws Exception
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    
    try
    {
      // retrieve data from app_auto_approve_defaults
      /*@lineinfo:generated-code*//*@lineinfo:476^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(aaad.sic_code, -1)  sic_code,
//                  aaad.investigator_code  investigator_code,
//                  aaad.met_table          met_table,
//                  aaad.delay_credit_days  delay_credit_days,
//                  aaad.delay_debit_days   delay_debit_days,
//                  aaad.suspend_days       suspend_days,
//                  aaad.merchant_status    merchant_status,
//                  aaad.association        association,
//                  aaad.user_data_1        user_data_1,
//                  aaad.user_data_2        user_data_2,
//                  aaad.user_data_5        user_data_5,
//                  aaad.user_account_1     user_account_1,
//                  aaad.user_account_2     user_account_2,
//                  at.app_bank_number      bank_number,
//                  oa.app_source           app_source
//          from    app_auto_approve_defaults aaad,
//                  app_type                  at,
//                  org_app                   oa
//          where   aaad.app_type = :appType and
//                  aaad.app_type = at.app_type_code and
//                  aaad.app_type = oa.app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(aaad.sic_code, -1)  sic_code,\n                aaad.investigator_code  investigator_code,\n                aaad.met_table          met_table,\n                aaad.delay_credit_days  delay_credit_days,\n                aaad.delay_debit_days   delay_debit_days,\n                aaad.suspend_days       suspend_days,\n                aaad.merchant_status    merchant_status,\n                aaad.association        association,\n                aaad.user_data_1        user_data_1,\n                aaad.user_data_2        user_data_2,\n                aaad.user_data_5        user_data_5,\n                aaad.user_account_1     user_account_1,\n                aaad.user_account_2     user_account_2,\n                at.app_bank_number      bank_number,\n                oa.app_source           app_source\n        from    app_auto_approve_defaults aaad,\n                app_type                  at,\n                org_app                   oa\n        where   aaad.app_type =  :1  and\n                aaad.app_type = at.app_type_code and\n                aaad.app_type = oa.app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.AutoApprove",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:499^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        sicCode         = rs.getInt("sic_code");
        invCode         = rs.getString("investigator_code");
        metCode         = rs.getString("met_table");
        creditDelay     = rs.getInt("delay_credit_days");
        debitDelay      = rs.getInt("delay_debit_days");
        daysSuspend     = rs.getInt("suspend_days");
        association     = rs.getString("association");
        bankNumber      = rs.getInt("bank_number");
        statusIndicator = rs.getString("merchant_status");
        appSource       = rs.getString("app_source");
      }

      rs.close();
      it.close();

      // generate user data fields
      getUserData();

      // update merchant table to show values
      /*@lineinfo:generated-code*//*@lineinfo:524^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     allow_auto_upload         = 'Y',
//                  app_sic_code              = decode(:sicCode, -1, sic_code, :sicCode),
//                  sic_code                  = decode(:sicCode, -1, sic_code, :sicCode),
//                  merch_mcc                 = decode(:sicCode, -1, sic_code, :sicCode),
//                  merch_met_table_number    = :metCode,
//                  merch_bank_number         = :bankNumber,
//                  status_ind                = :statusIndicator,
//                  merch_visainc_status_ind  = :incStatus,
//                  merch_invg_code           = :invCode
//          where   app_seq_num               = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     allow_auto_upload         = 'Y',\n                app_sic_code              = decode( :1 , -1, sic_code,  :2 ),\n                sic_code                  = decode( :3 , -1, sic_code,  :4 ),\n                merch_mcc                 = decode( :5 , -1, sic_code,  :6 ),\n                merch_met_table_number    =  :7 ,\n                merch_bank_number         =  :8 ,\n                status_ind                =  :9 ,\n                merch_visainc_status_ind  =  :10 ,\n                merch_invg_code           =  :11 \n        where   app_seq_num               =  :12";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sicCode);
   __sJT_st.setInt(2,sicCode);
   __sJT_st.setInt(3,sicCode);
   __sJT_st.setInt(4,sicCode);
   __sJT_st.setInt(5,sicCode);
   __sJT_st.setInt(6,sicCode);
   __sJT_st.setString(7,metCode);
   __sJT_st.setInt(8,bankNumber);
   __sJT_st.setString(9,statusIndicator);
   __sJT_st.setString(10,incStatus);
   __sJT_st.setString(11,invCode);
   __sJT_st.setLong(12,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:537^7*/

      int recCount = 0;
      // update credit data in merchcredit table
      /*@lineinfo:generated-code*//*@lineinfo:541^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchcredit
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchcredit\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:547^7*/

      if(recCount ==  0)
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:552^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcredit
//            (
//              app_seq_num,
//              merch_credit_achpost_days,
//              merch_debit_achpost_days,
//              merch_days_suspend
//            )
//            values
//            (
//              :appSeqNum,
//              :creditDelay,
//              :debitDelay,
//              :daysSuspend
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchcredit\n          (\n            app_seq_num,\n            merch_credit_achpost_days,\n            merch_debit_achpost_days,\n            merch_days_suspend\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,creditDelay);
   __sJT_st.setInt(3,debitDelay);
   __sJT_st.setInt(4,daysSuspend);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:568^9*/
      }
      else
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:573^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchcredit
//            set     merch_credit_achpost_days = :creditDelay,
//                    merch_debit_achpost_days  = :debitDelay,
//                    merch_days_suspend        = :daysSuspend
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchcredit\n          set     merch_credit_achpost_days =  :1 ,\n                  merch_debit_achpost_days  =  :2 ,\n                  merch_days_suspend        =  :3 \n          where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,creditDelay);
   __sJT_st.setInt(2,debitDelay);
   __sJT_st.setInt(3,daysSuspend);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:580^9*/
      }

    }
    catch(Exception e)
    {
      throw new Exception("expandData() : " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private boolean doAutoApprove(long appSeqNum)
  {
    boolean result = false;

    try
    {
      connect();

      this.appSeqNum = appSeqNum;

      // get app type
      /*@lineinfo:generated-code*//*@lineinfo:606^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:612^7*/

      int deptCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:616^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_tracking
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_tracking\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.AutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   deptCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:622^7*/

      if(deptCount == 0)  // only do these steps if they haven't been done before
      {
        // assign merchant number
        (new AssignMidBean(Ctx)).assignMid(appSeqNum);

        // data expansion
        expandData();

        assignAssociation();

        // add to app tracking
        (new AppTrackingBean(Ctx)).addAllDepartmentTracking(appSeqNum);

        // mark credit as done in app tracking
        /*@lineinfo:generated-code*//*@lineinfo:638^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     date_completed = sysdate
//            where   app_seq_num = :appSeqNum and
//                    dept_code = :QueueConstants.DEPARTMENT_CREDIT
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     date_completed = sysdate\n          where   app_seq_num =  :1  and\n                  dept_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:644^9*/

        // mark as credit approved
        /*@lineinfo:generated-code*//*@lineinfo:647^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_credit_status = :QueueConstants.CREDIT_APPROVE
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_credit_status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.ops.AutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.CREDIT_APPROVE);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^9*/

        if(assocAssigned)
        {
          // auto-upload
          AutoUpload.doAutoUpload(appSeqNum);

          if( manualConversion == true )
          {
            // POS will be worked manually by conversion people
            AutoUpload.submitToTape(appSeqNum);
            AutoUpload.moveToSetupComplete(appSeqNum, null);
          }
          else if(!AutoUpload.checkManualSetupRequirement(appSeqNum))
          {
            AutoUpload.submitToTape(appSeqNum);
            AutoUpload.moveToMMS(appSeqNum, null);
            AutoUpload.moveToSetupComplete(appSeqNum, null);
          }
          else
          {
            AutoUpload.addToSetupQueue(appSeqNum);
          }
        }
      }

      result = true;
    }
    catch(Exception e)
    {
      logEntry("doAutoApprove(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public static boolean autoApprove(long appSeqNum)
  {
    AutoApprove aa = new AutoApprove();
    return( aa.doAutoApprove(appSeqNum) );
  }

  public static boolean autoApprove(long appSeqNum, DefaultContext Ctx)
  {
    AutoApprove aa = new AutoApprove(Ctx);
    return( aa.doAutoApprove(appSeqNum) );
  }
  
  public static boolean autoApprove(long appSeqNum, DefaultContext Ctx, boolean manualConversion)
  {
    AutoApprove aa = new AutoApprove(Ctx);
    aa.setManualConversion(manualConversion);
    
    return( aa.doAutoApprove(appSeqNum) );
  }
}/*@lineinfo:generated-code*/