/*@lineinfo:filename=AutoUploadTransactionDestination*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadTransactionDestination.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/08/04 4:36p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadTransactionDestination extends SQLJConnectionBase
{

  private int    appType              = -1;

  private String global               = "";
  private String deposits             = "";
  private String adjustments          = "";
  private String chargebacks          = "";
  private String reversals            = "";
  private String chargebackReversals  = "";
  private String ddaAdjustments       = "";
  private String batchrc              = "";
  private String otherTranType1       = "";
  private String otherTranType2       = "";
  private String otherTranType3       = "";

  public AutoUploadTransactionDestination(long primaryKey)
  {
    setPageDefaults(primaryKey);
    setDestinations();
    submitData(primaryKey);
  }
  
  private void setPageDefaults(long primaryKey)  
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:75^7*/

//  ************************************************************
//  #sql [Ctx] it = { select app_type 
//          from   application 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select app_type \n        from   application \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadTransactionDestination",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadTransactionDestination",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
      }
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setDestinations()
  {
    switch(this.appType)
    {
      case mesConstants.APP_TYPE_BBT: //use global 'M'
        global               = "M";
        deposits             = "";
        adjustments          = "";
        chargebacks          = "";
        reversals            = "";
        chargebackReversals  = "";
        ddaAdjustments       = "";
        batchrc              = "";
        otherTranType1       = "";
        otherTranType2       = "";
        otherTranType3       = "";
      break; 
      
      default:
        global               = "";
        deposits             = "M";
        adjustments          = "M";
        chargebacks          = "M";
        reversals            = "M";
        chargebackReversals  = "M";
        ddaAdjustments       = "M";
        batchrc              = "M";
        otherTranType1       = "R";
        otherTranType2       = "X";
        otherTranType3       = "X";
      break;
    }
  }

  private void submitData(long primaryKey)
  {

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { update merchant_data 
//          set    dest_deposits              = :deposits,
//                 dest_adjustments           = :adjustments,
//                 dest_chargebacks           = :chargebacks,
//                 dest_reversals             = :reversals,
//                 dest_chargeback_reversals  = :chargebackReversals,
//                 dest_dda_adjustments       = :ddaAdjustments,
//                 dest_batch_rc              = :batchrc,
//                 dest_other_tran_type1      = :otherTranType1,
//                 dest_other_tran_type2      = :otherTranType2,
//                 dest_other_tran_type3      = :otherTranType3,
//                 dest_global                = :global
//          where  app_seq_num                = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merchant_data \n        set    dest_deposits              =  :1 ,\n               dest_adjustments           =  :2 ,\n               dest_chargebacks           =  :3 ,\n               dest_reversals             =  :4 ,\n               dest_chargeback_reversals  =  :5 ,\n               dest_dda_adjustments       =  :6 ,\n               dest_batch_rc              =  :7 ,\n               dest_other_tran_type1      =  :8 ,\n               dest_other_tran_type2      =  :9 ,\n               dest_other_tran_type3      =  :10 ,\n               dest_global                =  :11 \n        where  app_seq_num                =  :12";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AutoUploadTransactionDestination",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,deposits);
   __sJT_st.setString(2,adjustments);
   __sJT_st.setString(3,chargebacks);
   __sJT_st.setString(4,reversals);
   __sJT_st.setString(5,chargebackReversals);
   __sJT_st.setString(6,ddaAdjustments);
   __sJT_st.setString(7,batchrc);
   __sJT_st.setString(8,otherTranType1);
   __sJT_st.setString(9,otherTranType2);
   __sJT_st.setString(10,otherTranType3);
   __sJT_st.setString(11,global);
   __sJT_st.setLong(12,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:156^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
}/*@lineinfo:generated-code*/