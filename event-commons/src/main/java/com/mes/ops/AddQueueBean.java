/*@lineinfo:filename=AddQueueBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AddQueueBean.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/17/03 10:23a $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import sqlj.runtime.ref.DefaultContext;

public class AddQueueBean extends com.mes.database.SQLJConnectionBase
{
  public AddQueueBean(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  public AddQueueBean()
  {
  }
  
  public AddQueueBean(String connectString)
  {
    super(connectString);
  }
  
  public boolean entryExists(long appSeqNum, int queueType)
  {
    boolean rc      = false;
    long    count   = 0L;
    
    try
    {
      connect();
      
      if(queueType == QueueConstants.QUEUE_CREDIT)
      {
        // if we are trying to insert into the credit queue, make sure that
        // there are NO OTHER entries in the app_queue table at all
        /*@lineinfo:generated-code*//*@lineinfo:68^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    app_queue
//            where   app_seq_num   = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    app_queue\n          where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AddQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:77^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    app_queue
//            where   app_seq_num     = :appSeqNum and
//                    app_queue_type  = :queueType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    app_queue\n          where   app_seq_num     =  :1  and\n                  app_queue_type  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AddQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,queueType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^9*/
      }
      
      if(count > 0)
      {
        rc = true;
      }
    }
    catch(Exception e)
    {
      logEntry("entryExists(" + appSeqNum + ", " + queueType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return rc;
  }
  
  public boolean entryExists(long appSeqNum, String tableName)
  {
    long                  count   = 0L;
    PreparedStatement     ps      = null;
    ResultSet             rs      = null;
    StringBuffer          qs      = new StringBuffer("");
    
    try
    {
      connect();
      
      qs.append("select count(app_seq_num) as count ");
      qs.append("from ");
      qs.append(tableName);
      qs.append(" where app_seq_num = ?");
      
      ps = Ctx.getConnection().prepareStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        count = rs.getInt("count");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("entryExists(" + appSeqNum + ", " + tableName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return (count > 0);
  }
  
  public boolean insertQueue(long    appSeqNum,
                             int     queueType,
                             int     queueStage,
                             int     queueStatus,
                             String  appSource,
                             long    appUserId,
                             String  appUserName)
  {
    boolean rc          = false;
    
    try
    {
      connect();
      
      int   appType   = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:160^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AddQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:166^7*/
      
      // determine if this queue entry already exists
      if(entryExists(appSeqNum, queueType))
      {
        // do nothing, we shouldn't be inserting in this case
      
        /*
        // update
        #sql [Ctx]
        {
          update  app_queue
          set     app_queue_stage   = :queueStage,
                  app_status        = :queueStatus,
                  app_last_date     = sysdate,
                  app_last_user     = :appUserName
        };
        */
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:187^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue 
//            (
//              app_seq_num,
//              app_queue_type,
//              app_queue_stage,
//              app_status,
//              app_source,
//              app_user,
//              app_last_user,
//              app_start_date,
//              app_last_date
//            )
//            values
//            (
//              :appSeqNum,
//              :queueType,
//              :queueStage,
//              :queueStatus,
//              :appSource,
//              :appUserId,
//              :appUserName,
//              sysdate,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue \n          (\n            app_seq_num,\n            app_queue_type,\n            app_queue_stage,\n            app_status,\n            app_source,\n            app_user,\n            app_last_user,\n            app_start_date,\n            app_last_date\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n            sysdate,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.AddQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,queueType);
   __sJT_st.setInt(3,queueStage);
   __sJT_st.setInt(4,queueStatus);
   __sJT_st.setString(5,appSource);
   __sJT_st.setLong(6,appUserId);
   __sJT_st.setString(7,appUserName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^9*/
      
        rc = true;
      }
    }
    catch(Exception e)
    {
      logEntry("insertQueue(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return rc;
  }
  
  public boolean hasPreviousProcessorStatements(long appSeqNum)
  {
    boolean result      = false;
    
    try
    {
      int count;
      
      /*@lineinfo:generated-code*//*@lineinfo:238^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_prior_statements) 
//          from    merchant
//          where   app_seq_num = :appSeqNum and
//                  merch_prior_statements = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_prior_statements)  \n        from    merchant\n        where   app_seq_num =  :1  and\n                merch_prior_statements = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AddQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
      
      result = (count > 0);
    }
    catch(Exception e)
    {
      logEntry("hasPreviousProcessorStatements(" + appSeqNum + ")", e.toString());
    }
    
    return result;
  }
  
  public boolean insertDocQueue(long    appSeqNum,
                                long    documents,
                                String  miscDoc)
  {
    boolean rc            = false;
    int     miscReceived  = 0;
    
    try
    {
      // if application has previous processor marked as "yes", automatically
      // set statements as required
      
      if(hasPreviousProcessorStatements(appSeqNum))
      {
        documents |= com.mes.ops.QueueConstants.DOC_3MONTHS_STATEMENTS;
      }
      
      if(miscDoc.equals(""))
      {
        miscReceived = 1;
      }
      
      if(entryExists(appSeqNum, "app_queue_document"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:280^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue_document
//            set     app_doc_need        = :documents,
//                    app_doc_misc_need   = :miscDoc,
//                    app_doc_misc_recvd  = :miscReceived
//            where   app_seq_num         = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue_document\n          set     app_doc_need        =  :1 ,\n                  app_doc_misc_need   =  :2 ,\n                  app_doc_misc_recvd  =  :3 \n          where   app_seq_num         =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.AddQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,documents);
   __sJT_st.setString(2,miscDoc);
   __sJT_st.setInt(3,miscReceived);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:291^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue_document
//            (
//              app_seq_num,
//              app_doc_need,
//              app_doc_received,
//              app_doc_misc_need,
//              app_doc_misc_recvd
//            )
//            values
//            (
//              :appSeqNum,
//              :documents,
//              0,
//              :miscDoc,
//              :miscReceived
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue_document\n          (\n            app_seq_num,\n            app_doc_need,\n            app_doc_received,\n            app_doc_misc_need,\n            app_doc_misc_recvd\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            0,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.AddQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,documents);
   __sJT_st.setString(3,miscDoc);
   __sJT_st.setInt(4,miscReceived);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:309^9*/
      }
      
      rc = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::insertDocQueue()", appSeqNum + ": " + e.toString());
    }
    
    return rc;
  }
  
  public boolean insertCreditQueue(long appSeqNum)
  {
    boolean rc        = false;
    
    try
    {
      connect();
      
      if(! entryExists(appSeqNum, "app_queue_credit"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:332^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue_credit
//            (
//              app_seq_num,
//              app_source_type
//            )
//            values
//            (
//              :appSeqNum,
//              'USER'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue_credit\n          (\n            app_seq_num,\n            app_source_type\n          )\n          values\n          (\n             :1 ,\n            'USER'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AddQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::insertCreditQueue()", appSeqNum + ": " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return rc;
  }
}/*@lineinfo:generated-code*/