package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.InsertCreditQueue;
import com.mes.ops.QueueConstants;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;

public class BaseQueueOps extends SQLJConnectionBase
{
  // these should map to valid credit statuses 
  // in QueueConstants/merch_credit_statuses table
  public static final int CS_NO_CHANGE        = -1;
  public static final int CS_CLIENT_REVIEW    = QueueConstants.CBT_REVIEW_INCOMPLETE;
  public static final int CS_CLIENT_CANCELED  = QueueConstants.CBT_CREDIT_CANCEL;
  public static final int CS_CLIENT_DECLINED  = QueueConstants.CBT_CREDIT_DECLINE;
  public static final int CS_CLIENT_PENDED    = QueueConstants.CBT_CREDIT_PEND;

  /**
   * Used to ensure a queue item is not in the given queue type for a particular
   * operation.  If queue item is in the forbidden queue an exception is thrown.
   */
  protected void validateNotInQueue(long appSeqNum, int queueType, String opName)
    throws Exception
  {
    if (QueueTools.isInQueue(appSeqNum,queueType))
    {
      throw new Exception("Application " + appSeqNum + " cannot be in queue " 
        + queueType + " for op " + opName);
    }
  }

  /**
   * Throws exception if the given app is not in the indicated queue.
   */
  protected void validateInQueue(long appSeqNum, int queueType, String opName)
    throws Exception
  {
    if (!QueueTools.isInQueue(appSeqNum,queueType))
    {
      throw new Exception("Application " + appSeqNum + " must be in queue " 
        + queueType + " for op " + opName);
    }
  }

  /**
   * Update merch credit status value, merchant.merch_credit_status
   */
  protected void setCreditStatus(long appSeqNum, int status) throws Exception
  {
    if (status == CS_NO_CHANGE) return;

    PreparedStatement ps = null;

    try
    {
      connect();

      // get current status from merchant table
      String qs = " update merchant             " +
                  " set merch_credit_status = ? " +
                  " where app_seq_num = ?       ";
      ps = con.prepareStatement(qs);
      ps.setInt(1,status);
      ps.setLong(2,appSeqNum);
      ps.execute();
    }
    catch (Exception e)
    {
      String msg = "appSeqNum = " + appSeqNum + ", status = " + status + ": " + e;
      logEntry("setCreditStatus()",msg);
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * Uses QueueTools to move the queue item to a new queue.  A queue item 
   * with item type corresponding to the queueType must exist.
   *
   * Returns true if successful.
   */
  protected void moveQueueItem(long appSeqNum, int newQueue, int newStatus, 
    UserBean user, String opName) throws Exception
  {
    int oldQueue = -1;
    try
    {
      // make sure new queue is valid destination for app
      oldQueue = QueueTools.getCurrentQueueType(appSeqNum,QueueTools
        .getQueueItemType(newQueue));
      if (oldQueue == -1)
      {
        throw new Exception("No item for application " + appSeqNum + " exists for "
          + " op " + opName + " to move to queue " + newQueue);
      }

      // move the queue item if not already in destination queue
      if (oldQueue != newQueue)
      {
        QueueTools.moveQueueItem(appSeqNum,oldQueue,newQueue,user,null);
      }

      // update credit status
      setCreditStatus(appSeqNum,newStatus);
    }
    catch (Exception e)
    {
      String msg = "old = " + oldQueue + ", new = " + newQueue + ": " + e;
      logEntry("moveQueueItem()",msg);
      throw e;
    }
  }

  /**
   * Inserts a new queue item into the indicated queue.
   *
   * Returns true if the item was successfully inserted into the queue.
   */
  protected void insertQueueItem(long appSeqNum, int queueType, int creditStatus,
    UserBean user, String opName) throws Exception
  {
    try
    {
      QueueTools.insertQueue(appSeqNum,queueType,user);
      setCreditStatus(appSeqNum,creditStatus);
    }
    catch (Exception e)
    {
      String msg = "appSeqNum = " + appSeqNum + ", queueType = " + queueType
        + ", user = " + user.getLoginName() + ": " + e;
      logEntry("insertQueueItem()",msg);
      throw e;
    }
  }

  protected void addMes(long appSeqNum, UserBean user) throws Exception
  {
    // for now use standard InsertCreditQueue...but this could be bad
    // under some old-style build systems that don't deal with circular
    // dependencies...
    (new InsertCreditQueue()).addToMesCreditQueue(appSeqNum,user);
  }


}