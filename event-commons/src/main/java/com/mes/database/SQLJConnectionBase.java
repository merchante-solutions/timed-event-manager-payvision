/*@lineinfo:filename=SQLJConnectionBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/SQLJConnectionBase.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.constants.mesConstants;
import com.mes.support.HttpHelper;
import com.mes.support.LoggingConfigurator;
import com.mes.support.MesSystem;
import com.mes.support.PropertiesFile;
import oracle.sqlj.runtime.Oracle;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class SQLJConnectionBase
  implements Serializable
{
  static final long serialVersionUID   = 1173220856959937319L;
  
  public static final Logger log = Logger.getLogger(SQLJConnectionBase.class);
  
  static { DbProperties.configure(); } // try to configure with db properties
  
  private OracleConnectionPool.PoolConnection   PoolCon = null;//@

  // This is the context that the embedded sql should use.
  private       boolean             AutoCommit                  = true;
  protected     Connection          con                         = null;
  private       long                ConnectionTS                = 0L;
  private       long                ConnectionTimeout           = (60000L * 2L);
  private       int                 ConnectionType              = -1;
  protected     DefaultContext      Ctx                         = null;
  private       Properties          DbProps                     = null;
  private       boolean             Initialized                 = false;
  private       String              JdbcConnectionString        = null;
  private       int                 connectCount                = 0;

  
  public static final     int     DIRECT_CONNECTION               = 0;
  public static final     int     POOL_CONNECTION                 = 1;
  
  protected static final  String  DEFAULT_CONNECTION_STRING       = "thisisnotusedanymore";

  private static final    String  NT_HOST_NAME_VAR                = "COMPUTERNAME";
  private static final    String  UNIX_HOST_NAME_VAR              = "HOSTNAME";

  public static final     long    CONNECTION_TIMEOUT_NONE         = -1L;

  // the db conn string - Referenced when instantiating this class
  protected static        String  connectString                   = DEFAULT_CONNECTION_STRING;
  
  protected               int     downloadRowCount                = 0;
  
  public static String getConnectString()
  {
    return connectString;
  }

  public static void setConnectString(String theConnectString)
  {
    if(theConnectString!=null && theConnectString.length()>0)
      SQLJConnectionBase.connectString=theConnectString;
  }
  
  public SQLJConnectionBase( )
  {
    initialize(connectString);
  }
  
  public SQLJConnectionBase( boolean autoCommit)
  {
    this.AutoCommit = autoCommit;
    
    initialize(connectString);
  }
  
  public SQLJConnectionBase( String connectionString ) 
  {
    initialize( connectionString );
  }
  
  public SQLJConnectionBase( String connectionString, boolean autoCommit)
  {
    this.AutoCommit = autoCommit;
    
    initialize( connectionString );
  }
  
  public String blankIfNull(String data)
  {
    String result = "";
    
    if(data != null)
    {
      result = data;
    }
    
    return result;
  }
  
  public void register(String loginName, HttpServletRequest request)
  {
    // register the user with this object
    try
    {
      MesSystem.registerRequest(this, loginName, request);
    }
    catch (Exception e)
    {
    }
  }

  public void cleanUp( )
  {
    cleanUp(false);
  }
  
	private void cleanUp(boolean forceClose) {
		if (log.isTraceEnabled()) {
			log.trace("[cleanUp()] - forceClose=" + forceClose);
		}
		try {
			// release the connection back to the connection pool
			try {
				// forceClose is true when connect is calling cleanup
				// because the current connection is stale. do not
				// want the call to cleanup to adjust the connect count
				connectCount = (connectCount - (forceClose ? 0 : 1));

				if (connectCount <= 0 || forceClose) {
					if (log.isTraceEnabled()) {
						log.trace("[cleanUp()] - attempt to close / release connection");
					}
					if (ConnectionType == DIRECT_CONNECTION) {
						try {
							Ctx.close();
							if (log.isTraceEnabled()) {
								log.trace("[cleanUp()] - closed / released direct connection");
							}
						}
						catch (Exception e) {
						}
					}
					else if (PoolCon != null) // pool connection
					{
						try {
							PoolCon.release(this);
							PoolCon = null;
							if (log.isTraceEnabled()) {
								log.trace("[cleanUp()] - pooled connection released,forceClose=" + forceClose);
							}
						}
						catch (Exception e) {
							log.error("[cleanUp()] - Unexpected exception releasing connection, "+e.toString(),e);
						}
					}

					con = null; // release this reference to the connection
					Ctx = null; // release this reference to the context

					// if this is not a force close, be sure the
					// connection count returns to 0
					connectCount = (forceClose ? connectCount : 0);
				}
				if (log.isTraceEnabled()) {
					log.trace("[cleanUp()] - connection count after cleanup, connectCount=" + connectCount);
				}
			}
			catch (Exception closeException) {
			}
		}
		catch (NullPointerException e) {
			System.out.println("SQLJConnectionBase::cleanUp - " + e.toString());// @
		}
		catch (Exception e) {
			System.out.println("SQLJConnectionBase::cleanup - " + e.toString());
		}
	}
  public void cleanUp(PreparedStatement ps)
  {
    try { ps.close(); } catch (Exception e) { }
    cleanUp();
  }
  public void cleanUp(ResultSetIterator it, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    try { it.close(); } catch (Exception e) { }
    cleanUp();
  }
  public void cleanUp(PreparedStatement ps, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    try { ps.close(); } catch (Exception e) { }
    cleanUp();
  }
  
  protected int decodeConnectionType( String typeString )
  {
    int             retVal        = POOL_CONNECTION;
    
    try
    {
      if ( typeString.equals("direct") )
      {
        retVal = DIRECT_CONNECTION;
      }
    }
    catch( Exception e )
    {
      // ignore, return default
    }
    
    return( retVal );
  }
  
  public void disconnect()
  {
    cleanUp();
  }

  public void commit()
  {
    try
    {
      if(con != null)
      {
        con.commit();
      }
    }
    catch(Exception e)
    {
      // System.out.println(this.getClass().getName() + "::commit() - " + e.toString());
      log.error(this.getClass().getName() + "::commit() - " + e);
    }
  }
  
  public void rollback()
  {
    try
    {
      if(con != null)
      {
        con.rollback();
      }
    }
    catch(Exception e)
    {
      // System.out.println(this.getClass().getName() + "::rollback() - " + e.toString());
	log.error(this.getClass().getName() + "::rollback() - " + e);
    }
  }
  
  /*
  ** METHOD connect
  **
  ** Establishes the connection assuming initialize() has been called
  */
  public boolean connect()
  {
    return( connect( false ) );
  }
  
	public boolean connect(boolean timeoutExempt) {
		if (log.isDebugEnabled()) {
			log.debug("[connect()] timeoutExempt=" + timeoutExempt);
		}
		boolean result = false;

		if (Initialized) {
			if (ConnectionType == DIRECT_CONNECTION) {
				if (isConnectionStale()) {
					cleanUp(true); // release any existing connections

					try {
						// get a direct SQLJ connection context and the underlying connection
						Ctx = Oracle.getConnection(JdbcConnectionString, DbProps, AutoCommit);
						con = Ctx.getConnection();
						result = true;
					}
					catch (java.sql.SQLException e) {
						log.error("Error connecting directly to db", e);
					}
				}
				else {
					result = true;
				}
			}
			else // pool connection
			{
				if (log.isTraceEnabled()) {
					log.trace("[connect()] - attempt to obtain pooled cnnection");
				}
				if (DefaultContext.getDefaultContext() != null) {
					// if we have a default connection context
					// then use it. This will be true when
					// the SQLJConnection is being used inside
					// of a Java Stored Procedure.
					Ctx = DefaultContext.getDefaultContext();
				}
				else if (!isConnectionStale()) // we have a good connection
				{
					result = true;
				}
				else // either a null or a closed connection present, reset Ctx
				{
					cleanUp(true); // release any existing connections
					if (log.isTraceEnabled()) {
						log.trace("[connect()] called cleanUp(true) to release previous connection");
					}
					try {
						if (log.isTraceEnabled()) {
							log.trace("[connect()] - attempt to obtain pooled connection");
						}
						PoolCon = OracleConnectionPool.getInstance().getFreeConnection(this, timeoutExempt);
						if (log.isTraceEnabled()) {
							log.trace("[connect()] - pooled connection obtained - " + (PoolCon != null));
						}
						Ctx = PoolCon.getContext();
						con = PoolCon.getConnection();

						// TAB - set auto commit in the connection
						con.setAutoCommit(AutoCommit);

						result = true;
					}
					catch (Exception e) {
						log.error("[connect()] - Error getting pool connection", e);
					}
				}
			}
			// increment connectCount to prepare for multiple cleanUp() calls later
			++connectCount;
			if (log.isTraceEnabled()) {
				log.trace("[connect()] - Current connection count - " + connectCount);
			}
		}
		return result;
	}

  /*
  ** METHOD initialize
  **
  ** PARAMS:
  **
  **    connectString : Formatted connection string.  
  **
  **    direct;<jdbc url>;<driver class name>;..[name=value] 
  **              OR
  **    pool;<pool name>
  **              OR
  **    default
  **
  ** See com.mes.defaults.DbProperties for connection string details.
  */
  protected void initialize(String connectString)
  {
    // may need to override if external db config properties loaded
    if (DbProperties.isConfigured()) {
    
      if (DbProperties.isValidDbName(connectString)) {
        String dbName = connectString;
        connectString = DbProperties.getConnectionString(dbName);
        log.debug("Set connectString to dbName " + dbName 
          + " in initialize()");
      }
      else {
      
        connectString = DbProperties.getConnectionString();
        log.debug(
          "Overriding connectString with DbProperties default in"
            + " initialize()");
      }
    }
    log.trace("[initialize()] - Initializing with connect string: " + DbProperties.getLoggableConnection(connectString));
    
    String            driverClassName         = null;
    MessageFormat     messageParser           = new MessageFormat("{0}={1}");
    Object[]          nameValuePair           = null;
    int               tokenCount              = 0;
    StringTokenizer   tokenizer               = new StringTokenizer( connectString, ";" );

    tokenCount = tokenizer.countTokens();

    try
    {
      if ( tokenCount > 0 )
      {
        ConnectionType = decodeConnectionType( tokenizer.nextToken() );
        switch( ConnectionType )
        {
          case DIRECT_CONNECTION:
            JdbcConnectionString = tokenizer.nextToken();
            driverClassName = tokenizer.nextToken();
            for( int i = 3; i < tokenCount; ++i )
            {
              if ( DbProps == null )
              {
                DbProps = new Properties();
              }            
              try
              {
                nameValuePair = messageParser.parse( tokenizer.nextToken() );
                DbProps.put( nameValuePair[0].toString(), nameValuePair[1].toString() );
              }
              catch( java.text.ParseException e )
              {
                // improperly formatted, ignore
              }
            }
            
            if ( driverClassName != null )
            {
              //
              // Dynmically load the driver class
              //
              // The forName call will dynamically load the named class
              // into the JVM.  This call is safe even if the class has 
              // already been loaded.
              //
              Class.forName( driverClassName );
            }
            break;
            
          default:      // do nothing, using the connection pool
            break;
        }
      }
      Initialized = true;
    }
    catch( java.lang.ClassNotFoundException e )
    {
    }
  } 
  
  public boolean isConnectionStale( )
  {
    boolean       retVal = true;
    
    try
    {
      // DefaultContext.isClosed() will ultimately call
      // java.sql.Connection.isClosed() which by the JDBC
      // specification should only return true if the method
      // java.sql.Connection.close() has been called.
      if ( !Ctx.isClosed() )
      {
        // simple test is required to test if the connection
        // is actually still alive and working correctly
        int isAlive = 0;
        /*@lineinfo:generated-code*//*@lineinfo:480^9*/

//  ************************************************************
//  #sql [Ctx] { select  1 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  1  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.database.SQLJConnectionBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   isAlive = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:484^9*/
      
        // connection is stale if 
        // isAlive does not equal 1
        retVal = (isAlive != 1);
      }        
    }
    catch(Exception e)
    {
    	; // Eat the exception here, if we've got it!
    }
    return( retVal );
  }
  
  public CallableStatement getCallableStatement(String queryString)
  {
    CallableStatement cs = null;
    
    try
    {
      cs = con.prepareCall(queryString);
    }
    catch(Exception e)
    {
      logEntry("getCallableStatement()", e.toString());
    }
    return(cs);
  }

  public Connection getConnection()
  {
    return( con );
  }

  public long getConnectTime( )
  {
    return( System.currentTimeMillis() - ConnectionTS );
  }

  public long getConnectTimeout( )
  {
    return( ConnectionTimeout );
  }
  
  public DefaultContext getContext()
  {
    return( Ctx );
  }

  public static String getHostName()
  {
    PropertiesFile          propFile  = null;
    String                  retVal    = "Unknown";

    try
    {
      // do this as a dynamic load class so that this base
      // class can be easily used outside of the WebLogic environment.
      propFile = (PropertiesFile) Class.forName("com.mes.support.WebLogicPropertiesFile").newInstance();
      propFile.load( "mes.properties" );
      retVal = propFile.getString("com.mes.hostname","Missing");
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public PreparedStatement getPreparedStatement(String queryString)
  {
    PreparedStatement ps = null;
    
    try
    {
      ps = con.prepareStatement(queryString);
    }
    catch(Exception e)
    {
//@      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getPreparedStatement()", e.toString());
	log.error(this.getClass().getName() + "::getPreparedStatement()", e);

    }
    
    return ps;
  }

    /**
     * logs information message
     * @param message
     */
    public void logInfo(String message)
    {
        log.info(message);
    }

    /**
     * logs warning message
     * @param message
     */
    public void logWarning(String message)
    {
        log.warn(message);
    }

    /**
     * logs error message
     * @param message
     */
    public void logError(String message)
    {
        log.error(message);
    }

    public void logEntry(String method, Exception e)
    {
        try
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            StringBuffer  errMsg      = new StringBuffer("");
            errMsg.append(this.getClass().getName());
            errMsg.append("::");
            errMsg.append(method);
            log.error(errMsg + ": " + sw.toString());

            log.debug(stackStr());
            
        }
        catch(Exception e1)
        {
            log.error(method);
            log.error(this.getClass().getName() + "::logEntry() - " + e1);
        }
        finally
        {
        }
    }
    
    private String stackStr() {
   
    // get stack trace elements
    StackTraceElement[] stes = Thread.currentThread().getStackTrace();
   
    // log all but first two elements
    // (skipping this method and Thread.getStackTrace())
    StringBuffer buf = new StringBuffer("Stack Trace:\n");
    for (int i = 0; i < stes.length; ++i) {
 
      if (i >= 2) {
     
        buf.append("  " + stes[i] + (i < stes.length - 1 ? "\n" : ""));
      }
    }
   
    return ""+buf;
   }

    public void logEntry(String method, String message)
  {
    logEntry(0, method, message);
  }
  
  public void logEntry(int error, String method, String message)
  {
    StringBuffer  errMsg      = new StringBuffer("");
    
    boolean       isStale     = isConnectionStale();
    
    try
    {
      if(isStale)
      {
        connect();
      }
      
      errMsg.append(this.getClass().getName());
      errMsg.append("::");
      errMsg.append(method);
      
      String  serverName = HttpHelper.getServerName(null);
      
      String truncErrMsg = (errMsg.length() > 80) ? errMsg.substring(0, 80) : errMsg.toString();
      String truncServerName = (serverName.length() > 30) ? serverName.substring(0, 30) : serverName;

      log.error(errMsg + ": " + message);
      
      log.debug(stackStr());
      
      /*@lineinfo:generated-code*//*@lineinfo:673^7*/

//  ************************************************************
//  #sql [Ctx] { insert into java_log
//          (
//            log_time,
//            log_error,
//            log_source,
//            log_message,
//            server_name
//          )
//          values
//          (
//            sysdate,
//            :error,
//            :truncErrMsg,
//            :message,
//            :truncServerName
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into java_log\n        (\n          log_time,\n          log_error,\n          log_source,\n          log_message,\n          server_name\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.database.SQLJConnectionBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,error);
   __sJT_st.setString(2,truncErrMsg);
   __sJT_st.setString(3,message);
   __sJT_st.setString(4,truncServerName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:691^7*/
      
    }
    catch(Exception e)
    {
	//      System.out.println(method + " - " + message);
	log.error(method + " - " + message);
      // can't really do anything about this
	//      System.out.println(this.getClass().getName() + "::logEntry() - " + e.toString());
      log.error(this.getClass().getName() + "::logEntry() - " + e);
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }

  public void logEntryConnection( String hostName, String className)
  {
    boolean       isStale     = isConnectionStale();
    long          seq         = 0L;
    
    try
    {
      if(isStale)
      {
        connect();
      }

      /*@lineinfo:generated-code*//*@lineinfo:723^7*/

//  ************************************************************
//  #sql [Ctx] { select connection_sequence.nextval  from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select connection_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.database.SQLJConnectionBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seq = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:726^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:728^7*/

//  ************************************************************
//  #sql [Ctx] { insert into connection_log
//            ( log_date, 
//              log_source, 
//              log_sequence, 
//              log_message
//            )
//          values 
//            ( sysdate, 
//              :className,
//              :seq,
//              :hostName
//            )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into connection_log\n          ( log_date, \n            log_source, \n            log_sequence, \n            log_message\n          )\n        values \n          ( sysdate, \n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.database.SQLJConnectionBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,className);
   __sJT_st.setLong(2,seq);
   __sJT_st.setString(3,hostName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:742^7*/
      /*@lineinfo:generated-code*//*@lineinfo:743^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:743^27*/
    }
    catch(Exception e)
    {
      // System.out.println("logEntryConnection( " + hostName + "," + className + " ) failed.  " + e.toString() );
	log.error("logEntryConnection( " + hostName + "," + className + " ) failed.  " + e );
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }

  public void resetConnection( )
  {
    resetConnection( DEFAULT_CONNECTION_STRING );
  }
  
  public void resetConnection( String connectionString )
  {
    this.cleanUp();                     // close any existing connection
    this.initialize(connectionString);  // reinitialize using the connection string
    this.connect();                     // reconnect using new connection data
  }
  
  public void setAutoCommit(boolean autoCommit)
  {
    this.AutoCommit = autoCommit;

    try
    {
      if( con != null )
      {
        con.setAutoCommit(autoCommit);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setAutoCommit()", e.toString());
      log.error(this.getClass().getName() + "::setAutoCommit()" + e);
    }
  }

  public boolean getAutoCommit()
  {
    return AutoCommit;
  }

  public void setConnectionTimeout( long timeout )
  {
    ConnectionTimeout = timeout;
  }

	@Override
	protected void finalize() throws Throwable {
		if (PoolCon != null ) {
			PoolCon.release(this);
		}
		PoolCon = null;
		con = null; 
		Ctx = null;
	}
  
  /*
  ** METHODS TO SUPPORT GENERIC DOWNLOADING OF DATA
  */
  public void setProperties(HttpServletRequest request, Object userBean)
  {
  }
  
  public void loadData(Object userBean)
  {
    // NOTE - this method must set the variable downloadRowCount to use 
    // the non-extended version of encodeData()
    
    // also this method will have to cast userBean as a UserBean if it wants
    // to use it
  }
  
  public boolean isEncodingSupported(int encoding)
  {
    // set up to allow CSV by default.  Others will require overloading this method
    boolean retVal = false;
    
    switch(encoding)
    {
      case mesConstants.FF_CSV:
        retVal = true;
        break;
      
      default:
        break;
    }
    return retVal;
  }
  
  protected String getDownloadFilenameBase()
  {
    return "generic_data";
  }
  
  protected void setDownloadHeader(int encoding, StringBuffer line)
  {
  }
  
  protected void setDownloadRow(int encoding, int element, StringBuffer line)
  {
  }
  
  public static String processString(String raw)
  {
    String result = "";
    
    if(raw != null)
    {
      result = raw;
    }
    
    return( result );
  }
  
  public boolean encodeData(int encoding, HttpServletResponse response)
    throws java.io.IOException
  {
    StringBuffer          line          = new StringBuffer("");
    boolean               retVal        = true;
    ServletOutputStream   out           = response.getOutputStream();
    try
    {
      // set up download filename base
      line.append(getDownloadFilenameBase());
      
      switch(encoding)
      {
        case mesConstants.FF_CSV:
          // setup download filename
          line.append(".csv");
          break;
          
        case mesConstants.FF_FIXED:
          line.append(".txt");
          break;
          
        default:
          retVal = false;
          break;
      }
      
      if(retVal) // i.e. encoding supported
      {    
        // initialize response
        response.setContentType("application/csv");
        response.setHeader("Content-Disposition", ("attachment; filename=" + line.toString()));
      
        // output header
        line.setLength(0);
        setDownloadHeader(encoding, line);
        
        if(line.length() > 0)
        {
          out.print(line.toString());
          out.print("\n");
        }
      
        for(int i = 0; i < downloadRowCount; ++i)
        {
          line.setLength(0);
          setDownloadRow(i, encoding, line);
        
          if(line.length() > 0)
          {
            out.print(line.toString());
            out.print("\n");
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("encodeData()", e.toString());
      retVal = false;
    }
    
    return retVal;
  }
  
  public static String getPoolConnectionString()
  {
    if (!DbProperties.isConfigured()) {
      log.warn("DbProperties not configured, cannot determine connection string");
      return null;
    }
    return DbProperties.getPoolConnectionString();
  }
  
  public static String getDirectConnectString()
  {
    if (!DbProperties.isConfigured()) {
      log.warn("DbProperties not configured, cannot determine connection string");
      return null;
    }
    return DbProperties.getDirectConnectionString();
  }
  
  public static void initStandalone()
  {
    initStandalone(true);
  }
  
  public static void initStandalonePool()
  {
    initStandalone(false);
  }
  
  public static void initStandalone(String errorLevel)
  {
    initStandalone(errorLevel, true);
  }
  
  public static void initStandalonePool(String errorLevel)
  {
    initStandalone(errorLevel, false);
  }
  
  public static void initStandalone(String errorLevel, boolean useDirect)
  {
    // use the logging configurator to configure log4j if needed
    LoggingConfigurator.configure();
    initStandalone(!useDirect);
    log.warn("Logging error level must be configured via external log4j properties file.");
  }
  
  public static void initStandalone(boolean poolFlag) {
  
    // db properties may not be configured, so try legacy configuration
    if (!DbProperties.isConfigured()) {
    
      // configure in legacy way since no DbProperties property file exists
      if (!poolFlag) {
        setConnectString(getDirectConnectString());
        log.info("DB connections will use direct connection (no connection pool)");
      }
      else {
        log.info("DB connections will use connection pool");
      }
    }
  }
}/*@lineinfo:generated-code*/
