package com.mes.database;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import org.apache.log4j.Logger;
import com.mes.data.MesDataSourceManager;

/**
 * Helper class encapsulating logic for execution of SQL using <code>java.sql.*</code>. </br>
 * For most use cases, create an instance of either <code>MesQueryHandlerList</code> or <code>MesQueryHandlerResultSet</code>,
 * and call the appropriate method.
 * </br>
 * <em>Note:</em> When processing groups of inserts, updates, or deletes inside of a transaction
 * encapsulate the inserts, updates, or deletes inside of a try-catch-finally block, where the
 * finally block calls <code>closeConnection</code>.
 * 
 * @author lmaloney
 *
 * @param <T>
 */
public abstract class MeSQueryHandler<T extends Collection<?>> {

	private Logger log = Logger.getLogger(MeSQueryHandler.class);
	private Boolean autoCommit = true;
	private Connection connection = null;

	public MeSQueryHandler() {
	}

	public Boolean isAutoCommit() {
		return autoCommit;
	}

	/**
	 * @param isAutoCommit
	 * @throws SQLException
	 */
	public void setAutoCommit(Boolean isAutoCommit) throws SQLException {
		if (connection != null) {
			connection.commit();
			connection.setAutoCommit(isAutoCommit);
		}
		autoCommit = isAutoCommit;
		if (log.isTraceEnabled()) {
			log.trace("[setAutoCommit()] - autoCommit set to " + autoCommit);
		}
	}

	/**
	 * If the connection exists, commits any open transactions and closes the connection.
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if (connection != null) {
			connection.commit();
			connection.setAutoCommit(Boolean.TRUE);
			connection.close();
			connection = null;
			if (log.isDebugEnabled()) {
				log.debug("[closeConnection()] - closed connection");
			}
		}
	}

	public void commit() throws SQLException {
		if (connection != null && !connection.getAutoCommit()) {
			connection.commit();
		}
	}

	public Connection getConnection() throws SQLException {
		Connection conn = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
		conn.setAutoCommit(autoCommit);
		if (log.isTraceEnabled()) {
			log.trace("[getConnection()] - obtained connection from pool");
		}
		return conn;
	}

	/**
	 * If the connection exists, roll back any open transactions.
	 * @param closeConnection - true to close / release the db connection
	 * @throws SQLException
	 */
	public void rollback(Boolean closeConnection) throws SQLException {
		if (connection != null && !connection.getAutoCommit()) {
			connection.rollback();
			if (log.isDebugEnabled()) {
				log.debug("[rollback()] - Successfully rolledback transaction");
			}
			if (closeConnection) {
				closeConnection();
			}
		}
	}

	/**
	 * Method to extract the results of the query into a object
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	protected abstract T copyQueryResults(ResultSet resultSet) throws SQLException;

	/**
	 * Executes a prepared statement and returns the results
	 * @param sqlName
	 * @param preparedSql - the SQL statement to execute
	 * @param params - the parameters for the SQL
	 * @return A <code>List</code> of HashMap<String,Object> representing each row
	 * @throws SQLException
	 */
	public T executePreparedStatement(String sqlName, String preparedSql, Object... params) throws SQLException {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[executePreparedStatement()] - executing prepared statement, sqlName=%s, sql=%s", sqlName, preparedSql));
		}
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			if (connection == null) {
				connection = getConnection();
			}
			preparedStatement = connection.prepareStatement(preparedSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			setParameterValues(preparedStatement, params);
			resultSet = preparedStatement.executeQuery();
			T resultCollection = copyQueryResults(resultSet);
			if (log.isDebugEnabled()) {
				log.debug(String.format("[executePreparedStatement()] - statement %s returned %s rows", sqlName, resultCollection.size()));
			}
			return resultCollection;
		}
		catch (SQLException sqe) {
			log.error("[executePreparedStatement()] SQL Exception sqlName=" + sqlName, sqe);
			rollback(Boolean.TRUE);
			throw sqe;
		}
		finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null && connection.getAutoCommit()) {
				closeConnection();
			}
			if (log.isTraceEnabled()) {
				log.trace("[executePreparedStatement()] - released/closed connection");
			}
		}
	}

	/**
	 * Execute a prepared update using
	 * @param sqlName
	 * @param updateSql - The SQL to use for the insert/update/delete
	 * @param params - the list of params
	 * @return number of rows effected
	 * @throws SQLException
	 */
	public Integer executePreparedUpdate(String sqlName, String updateSql, Object... params) throws SQLException {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[executePreparedUpdate()] - executing prepared statement, sqlName=%s, sql=%s", sqlName, updateSql));
		}
		PreparedStatement preparedStatement = null;
		try {
			if (connection == null) {
				connection = getConnection();
			}
			preparedStatement = connection.prepareStatement(updateSql);
			setParameterValues(preparedStatement, params);
			Integer rowCount = preparedStatement.executeUpdate();
			if (log.isDebugEnabled()) {
				log.debug(String.format("[executePreparedUpdate()] - %s rows effected by SQL %s", rowCount, sqlName));
			}
			return rowCount;
		}
		catch (SQLException e) {
			log.error("[executePreparedUpdate()] SQL Exception sqlName=" + sqlName, e);
			rollback(Boolean.TRUE);
			throw e;
		}
		finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null && connection.getAutoCommit()) {
				closeConnection();
			}
			if (log.isTraceEnabled()) {
				log.trace("[executePreparedUpdate()] - released/closed connection");
			}
		}
	}

	@Override
	protected void finalize() {
		try {
			Boolean connectionOpen = (connection != null);
			closeConnection();
			if (log.isTraceEnabled()) {
				log.trace("[finalize()] - " + (connectionOpen ? "closed open connection" : "connection already closed"));
			}
		}
		catch (Exception e) {
			log.error("[finalize()] - Unexpected exception when releasing resources", e);
		}
	}

	private void setParameterValues(PreparedStatement preparedStatement, Object... params) throws SQLException {
		int paramIndex = 0;
		for (Object param : params) {
			paramIndex++;

			if (param instanceof java.sql.Date) {
				java.sql.Date date = (java.sql.Date) param;
				preparedStatement.setTimestamp(paramIndex, new Timestamp(date.getTime()));
			}
			else if (param instanceof java.util.Date) {
				java.util.Date date = (java.util.Date) param;
				preparedStatement.setTimestamp(paramIndex, new Timestamp(date.getTime()));
			}
			else if (param instanceof Calendar) {
				Calendar cal = (Calendar) param;
				preparedStatement.setTimestamp(paramIndex, new Timestamp(cal.getTimeInMillis()), cal);
			}
			else if (param instanceof Boolean) {
				preparedStatement.setBoolean(paramIndex, (Boolean) param);
			}
			else if (param instanceof Integer) {
				preparedStatement.setInt(paramIndex, (Integer) param);
			}
			else if (param instanceof Float) {
				preparedStatement.setFloat(paramIndex, (Float) param);
			}
			else if (param instanceof Long) {
				preparedStatement.setLong(paramIndex, (Long) param);
			}
			else if (param instanceof Double) {
				preparedStatement.setDouble(paramIndex, (Double) param);
			}
			else if (param instanceof BigDecimal) {
				preparedStatement.setBigDecimal(paramIndex, (BigDecimal) param);
			}
			else if (param instanceof String) {
				preparedStatement.setString(paramIndex, (String) param);
			}
		}
	}
}
