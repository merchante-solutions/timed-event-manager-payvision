package com.mes.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MesQueryHandlerResultSet extends MeSQueryHandler<MesResultSet>{

	@Override
	protected MesResultSet copyQueryResults(ResultSet resultSet) throws SQLException {
		return new MesResultSet(resultSet);
	}
}
