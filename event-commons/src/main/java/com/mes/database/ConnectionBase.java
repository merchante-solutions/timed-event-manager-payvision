package com.mes.database;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
//import java.util.regex.*;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.support.MesCalendar;
import sqlj.runtime.ResultSetIterator;

public class ConnectionBase extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ConnectionBase.class);

  public static final String FMT_ORA_DATE = "''d-MMM-yyyy''";
  public static final String FMT_ORA_DT   = "''d-MMM-yyyy h:mm:ss a''";
  
  // used to indicate when in a transaction
  private boolean inTranFlag;

  // used to store the autocommit status of a connection
  // to be restored after a transaction is done
  private boolean storedAcFlag;

  /**
   * CONSTRUCTORS
   */

  public ConnectionBase() {
  }
  
  public ConnectionBase(String dbName) {
    super(dbName);
  }

  /**
   * Truncate a string.  Converts null to empty string.  Returns
   * string that is no longer than the maximum length specified.
   */
  public static String trunc(String src, int maxLen)
  {
    if (src == null)
    {
      return "";
    }
    if (src.length() > maxLen)
    {
      return src.substring(0,maxLen);
    }
    return src;
  }

  /**
   * java.util.Date <-> java.sql.Timestamp conversion
   */

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }
  
  /**
   * Date/time formatted string generation for use in non-prepared
   * statements.
   */
   
  public static String oraDateExpression(Date date)
  {
    return "to_date(" + MesCalendar.formatDate(date,FMT_ORA_DATE) 
      + ",'DD-MON-RR')";
  }
  
  public static String oraDateTimeExpression(Date date)
  {
    return "to_date(" + MesCalendar.formatDate(date,FMT_ORA_DT)
      + ",'DD-MON-RR HH:MI:SS AM')";
  }

  /**
   * Convert string to BigDecimal amount if possible.
   */
  protected BigDecimal stringToBigDecimal(String str)
  {
    BigDecimal bigDec = null;

    try
    {
      bigDec = new BigDecimal(str).setScale(2);
    }
    catch (Exception e)
    {
      log.warn("Invalid amount string: '" + str + "'");
    }

    return bigDec;
  }

  /**
   * Error logging
   */

  public void logEntry(Object src, String method, Exception e)
  {
    logEntry(src.getClass().getName() + "." + method,""+e);
  }
  public void logEntry(String method, Exception e)
  {
    logEntry(this,method,e);
  }
  /**
   * Logs error via log4j, logs java_log entry, throws runtime exception.
   */
  public void handleException(Object src, String method, Exception e)
  {
    log.error("Exception in " + method);
    log.error(""+e);
    e.printStackTrace();
    logEntry(src,method,e);
    throw new RuntimeException(method,e);
  }
  public void handleException(String method, Exception e)
  {
    handleException(this,method,e);
  }


  /**
   * TRANSACTION SUPPORT
   */

  /**
   * Throw exception if the indicated transaction status is not the
   * current status.
   */
  private void checkTranStatus(boolean tranStatus) throws Exception
  {
    if (inTranFlag != tranStatus)
    {
      String errMsg = inTranFlag ? 
        "Invalid operation, cannot perform while in transaction" :
        "Invalid operation, cannot perform outside of transaction";
      throw new Exception(errMsg);
    }
  }

  /**
   * Connection convenience methods, transaction safe.
   */
  public boolean connect()
  {
    if (!inTranFlag || con == null)
    {
      return super.connect();
    }
    return true;
  }
  public Statement connectStatement() throws SQLException
  {
    return connect() ? con.createStatement() : null;
  }
  public PreparedStatement connectPrepared(String stmt) throws SQLException
  {
    return connect() ? con.prepareStatement(stmt) : null;
  }
  public CallableStatement connectCallable(String stmt) throws SQLException
  {
    return connect() ? con.prepareCall(stmt) : null;
  }
    
  /**
   * CleanUp convenience methods, transaction safe.
   */
  public void cleanup()
  {
    if (!inTranFlag)
    {
      super.cleanUp();
    }
  }
  public void cleanup(PreparedStatement ps)
  {
    try { ps.close(); } catch (Exception e) { }
    cleanup();
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanup(PreparedStatement ps, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    cleanup(ps);
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanup(ResultSetIterator it, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    try { it.close(); } catch (Exception e) { }
    cleanup();
  }
  public void cleanup(Statement s)
  {
    try { s.close(); } catch (Exception e) { }
    cleanup();
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanup(Statement s, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    cleanup(s);
  }
  public void cleanup(CallableStatement cs)
  {
    try { cs.close(); } catch (Exception e) { }
    cleanup();
  }

  /**
   * Store/retrieve auto commit state for connection.
   */
  private void storeAcState()
  {
    storedAcFlag = getAutoCommit();
  }
  private void restoreAcState()
  {
    setAutoCommit(storedAcFlag);
  }

  /**
   * Begin transaction, transaction remains open until commit
   * or rollback action is taken (see below)
   */
  public void startTransaction()
  {
    try
    {
      checkTranStatus(false);
      storeAcState();
      setAutoCommit(false);
      connect();
      inTranFlag = true;
    }
    catch (Exception e)
    {
      handleException("startTransaction",e);
    }
  }

  public void commitTransaction()
  {
    try
    {
      checkTranStatus(true);
      commit();
      cleanup();
      restoreAcState();
      inTranFlag = false;
    }
    catch (Exception e)
    {
      handleException("commitTransaction",e);
    }
  }

  public void rollbackTransaction()
  {
    try
    {
      checkTranStatus(true);
      rollback();
      cleanup();
      restoreAcState();
      inTranFlag = false;
    }
    catch (Exception e)
    {
      handleException("rollbackTransaction",e);
    }
  }

  public void endTransaction(boolean commitFlag) {
    if (commitFlag)
      commitTransaction();
    else
      rollbackTransaction();
  }
}
