/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/mbs/MerchantBillingElement.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2011-04-05 17:22:51 -0700 (Tue, 05 Apr 2011) $
  Version            : $Revision: 18675 $

  Change History:
     See SVN database

  Copyright (C) 2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;

public class MerchantBillingElement
{
  public static final int       VT_SALES_ONLY           = 1;
  public static final int       VT_CREDITS_ONLY         = 2;
  public static final int       VT_SALES_PLUS_CREDITS   = 3;
  public static final int       VT_SALES_MINUS_CREDITS  = 4;

  protected String   BillingMonths        = null;
  protected int      ExemptItemCount      = 0;
  protected int      ItemType             = 0;
  protected String   ItemDescription      = null;
  protected double   PerItem              = 0.0;
  protected double   Rate                 = 0.0;
  protected String   ItemSubclass         = null;
  protected long     RecId                = -1L;
  protected Date     ValidDateBegin       = null;
  protected Date     ValidDateEnd         = null;
  protected int      VolumeType           = -1;
  
  public MerchantBillingElement( )
  {
  }
  
  public MerchantBillingElement( ResultSet resultSet )
    throws java.sql.SQLException 
  {
    RecId             = resultSet.getLong("rec_id");
    ItemType          = resultSet.getInt("item_type");
    ItemDescription   = resultSet.getString("item_desc");
    ItemSubclass      = resultSet.getString("item_subclass");
    ValidDateBegin    = resultSet.getDate("valid_date_begin");
    ValidDateEnd      = resultSet.getDate("valid_date_end");
    Rate              = resultSet.getDouble("rate");
    PerItem           = resultSet.getDouble("per_item");
    ExemptItemCount   = resultSet.getInt("exempt_count");
    BillingMonths     = resultSet.getString("billing_months");
    VolumeType        = resultSet.getInt("vol_type");
  }
  
  public String   getBillingMonths()  { return( BillingMonths   ); }
  public int      getExemptItemCount(){ return( ExemptItemCount ); }
  public int      getItemType()       { return( ItemType        ); }
  public String   getItemDescription(){ return( ItemDescription ); }
  public double   getPerItem()        { return( PerItem         ); }
  public double   getRate()           { return( Rate            ); }
  public String   getItemSubclass()   { return( ItemSubclass    ); }
  public long     getRecId()          { return( RecId           ); }
  public Date     getValidDateBegin() { return( ValidDateBegin  ); }
  public Date     getValidDateEnd()   { return( ValidDateEnd    ); }
  public int      getVolumeType()     { return( VolumeType      ); }
  
  public void setBillingMonths(String value)    { BillingMonths   = value; }
  public void setExemptItemCount(int value)     { ExemptItemCount = value; }
  public void setItemType(int value)            { ItemType        = value; }
  public void setItemDescription(String value)  { ItemDescription = value; }
  public void setItemSubclass(String value)     { ItemSubclass    = value; }
  public void setPerItem(double value)          { PerItem         = value; }
  public void setRate(double value)             { Rate            = value; }
  public void setRecId(long value)              { RecId           = value; }
  public void setValidDateBegin(Date value)     { ValidDateBegin  = value; }
  public void setValidDateEnd(Date value)       { ValidDateEnd    = value; }
  public void setVolumeType(int value)          { VolumeType      = value; }
  
  public boolean isActive( )
  {
    Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    return( isActive(sqlDate) );
  }
  
  public boolean isActive( Date activityDate )
  {
    boolean     retVal      = false;
    
    if ( ( ValidDateEnd != null ) &&
         !( ValidDateEnd.before(activityDate) ) )
    {
      retVal = true;
    }           
    return( retVal );
  }
}
