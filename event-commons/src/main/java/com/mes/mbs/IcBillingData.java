/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/mbs/IcBillingData.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2012-12-03 10:19:00 -0800 (Mon, 03 Dec 2012) $
  Version            : $Revision: 20750 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.ResultSet;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;

public class IcBillingData
{
  public String     IcLabel           = null;
  public String     GroupLabel        = null;
  public String     StatementMsg      = null;
  public String     IcCode            = null;
  public String     IcDesc            = null;
  public String     IcDescStmt        = null;
  public boolean    IsDebitCard       = false;
  public boolean    IsPureDifferential= false;
  public boolean    ApplyToReturns    = false;
  public boolean    ShowRates         = false;
  public boolean    UseAltRates       = false;
  public boolean[]  HasAbsolute       = { false, false };
  public double[]   Absolute          = {  0.00,  0.00 };
  public double[]   Additional        = {  0.00,  0.00 };
  public double[]   IcMinusRel        = {  0.00,  0.00 };
  public double[]   Markup            = {  0.00,  0.00 };
  public double[]   MarkupDebit       = {  0.00,  0.00 };
  
  public IcBillingData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    IcLabel           = resultSet.getString( "ic_label" );
    IcCode            = resultSet.getString( "ic_code" );
    IcDesc            = resultSet.getString( "ic_desc" );
    IcDescStmt        = resultSet.getString( "ic_desc_stmt" );
    GroupLabel        = resultSet.getString( "group_label" );
    ApplyToReturns    = "Y".equals( resultSet.getString( "apply_to_returns" ) );
    ShowRates         = "Y".equals( resultSet.getString( "show_rate_on_statement" ) );
    UseAltRates       = "Y".equals( resultSet.getString( "use_alt_rates" ) );
    IsDebitCard       = "Y".equals( resultSet.getString( "use_db_rates" ) );
    IsPureDifferential= (GroupLabel != null && GroupLabel.startsWith("- PassDiff"));

    HasAbsolute[0]    = resultSet.getString( "abs_rate"     ) != null;
    HasAbsolute[1]    = resultSet.getString( "abs_per_item" ) != null;

    Absolute[0]       = resultSet.getDouble( "abs_rate"     );
    Absolute[1]       = resultSet.getDouble( "abs_per_item" );

    Additional[0]     = resultSet.getDouble( "additional_rate" );
    Additional[1]     = resultSet.getDouble( "additional_per_item" );

    IcMinusRel[0]     = resultSet.getDouble( "ic_rate"      ) - resultSet.getDouble( "rel_rate"      );
    IcMinusRel[1]     = resultSet.getDouble( "ic_per_item"  ) - resultSet.getDouble( "rel_per_item"  );
    
    //@ is it correct to always have zero markup when both interchange rate & per-item <= 0.00 ?
    if ( resultSet.getDouble( "ic_rate"      ) > 0.00 ||
         resultSet.getDouble( "ic_per_item"  ) > 0.00 )
    {
      double negativeDebitMarkupMultiplier  = resultSet.getDouble( "neg_markup_to_return" ) / 100.0;

      for( int cnt = 0; cnt <= 1; ++cnt )
      {
        boolean calcDebit = false;

              if( HasAbsolute[cnt] )        { Markup[cnt] = Absolute[cnt];    }
        else  if( IsPureDifferential )      { Markup[cnt] = IcMinusRel[cnt];  }
        else  if( IcMinusRel[cnt] >= 0.00 ) { Markup[cnt] = IcMinusRel[cnt];  }
        else                                { calcDebit   = true;             }

        if( calcDebit )   MarkupDebit[cnt] = IcMinusRel[cnt] * negativeDebitMarkupMultiplier;
        else              MarkupDebit[cnt] = Markup[cnt];

        if ( Markup[cnt]      != 0.0 )      { Markup[cnt]       += Additional[cnt]; }
        if ( MarkupDebit[cnt] != 0.0 )      { MarkupDebit[cnt]  += Additional[cnt]; }
      }

      // if this BET does not apply per item when the rate is zero
      if( ! "Y".equals( resultSet.getString( "per_item_when_rate_zero" ) ) )
      {
        //@ are we also supposed to check if BET has non-zero ic rate?
        //@ items with zero ic rate and non-zero per item are mostly "cap" or "utility"
        //@ if ( resultSet.getDouble( "ic_rate" ) != 0.00 )
        {
          if( Markup[0]       == 0.00 ) { Markup[1]       = 0.00; }
          if( MarkupDebit[0]  == 0.00 ) { MarkupDebit[1]  = 0.00; }
        }
      }
    }
    
    // if this is a returns only category and this bet does
    // not apply to the returns then force the all values to 0
    if (  "Y".equals( resultSet.getString("returns_only") ) &&
         !"Y".equals( resultSet.getString("apply_to_returns") ) )
    {
      for ( int i = 0; i < Markup.length; ++i )
      {
        Markup[i]       = 0.00;
        MarkupDebit[i]  = 0.00;
        Additional[i]   = 0.00;
      }
    }         

    if( GroupLabel.charAt(0) == '-' ) StatementMsg  = IcLabel.substring(0,2) + " - " + IcDescStmt;
    else                              StatementMsg  = decodeStatementMsg( GroupLabel );

    if( ShowRates ) //@&& resultSet.getString( "rel_rate" ) != null )
    {
      double perItem = getPerItem();
      StatementMsg += "  (" + NumberFormatter.getPercentString(getRate()/100.0,3) + 
                      ((Math.abs(perItem) >= 0.0001) ? (" + " + MesMath.toCurrency(perItem)) : "") + 
                      ")";
    }
  }
  
  protected String decodeStatementMsg( String groupLabel )
  {
    String    smsg    = "";
    
    if ( groupLabel != null && groupLabel.indexOf("|") >= 0 )
    {
      smsg = groupLabel.substring(0,groupLabel.indexOf("|"));
    }
    else
    { 
      smsg = groupLabel;
    }
    return( smsg.trim() );
  }
  
  public double getMarkup( int cnt )
  {
    double  retVal  = (IsDebitCard ? MarkupDebit[cnt] : Markup[cnt]);
    return( retVal );
  }
  
  public double getRate()
  {
    return( getMarkup(0) );
  }
  
  public double getPerItem()
  {
    return( getMarkup(1) );
  }
}
  
