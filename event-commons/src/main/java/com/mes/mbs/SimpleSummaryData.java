/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/mbs/SimpleSummaryData.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2011-04-05 17:22:51 -0700 (Tue, 05 Apr 2011) $
  Version            : $Revision: 18675 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;

public class SimpleSummaryData
{
  protected   Date    ActivityDate          = null;
  protected   double  CreditsAmount         = 0.0;
  protected   int     CreditsCount          = 0;
  protected   String  DataSource            = null;
  protected   long    DataSourceId          = 0L;
  protected   double  PerItem               = 0.0;
  protected   double  Rate                  = 0.0;
  protected   double  FeesDue               = 0.0;
  protected   double  FeesPaid              = 0.0;
  protected   double  Expense               = 0.0;
  protected   double  ExpenseActual         = 0.0;
  protected   String  IcCat                 = null;
  protected   double  ItemAmount            = 0.0;
  protected   int     ItemCount             = 0;
  protected   String  ItemSubclass          = null;
  protected   int     ItemType              = -1;
  protected   String  MeLoadFilename        = null;
  protected   long    MeLoadFileId          = 0L;
  protected   long    MerchantId            = 0L;
  protected   long    RecId                 = 0L;
  protected   double  SalesAmount           = 0.0;
  protected   int     SalesCount            = 0;
  protected   int     VolumeType            = -1;
  
  public SimpleSummaryData( )
  {
  }
  
  public void clear()
  {
    ActivityDate          = null;
    CreditsAmount         = 0.0;
    CreditsCount          = 0;
    DataSource            = null;
    DataSourceId          = 0L;
    PerItem               = 0.0;
    Rate                  = 0.0;
    FeesDue               = 0.0;
    FeesPaid              = 0.0;
    Expense               = 0.0;
    ExpenseActual         = 0.0;
    IcCat                 = null;
    ItemAmount            = 0.0;
    ItemCount             = 0;
    ItemSubclass          = null;
    ItemType              = -1;
    MeLoadFilename        = null;
    MeLoadFileId          = 0L;
    MerchantId            = 0L;
    RecId                 = 0L;
    SalesAmount           = 0.0;
    SalesCount            = 0;
    VolumeType            = -1;
  }

  public  Date    getActivityDate()         { return( ActivityDate            ); }
  public  double  getCreditsAmount()        { return( CreditsAmount           ); }
  public  int     getCreditsCount()         { return( CreditsCount            ); }
  public  String  getDataSource()           { return( DataSource              ); }
  public  long    getDataSourceId()         { return( DataSourceId            ); }
  public  double  getPerItem()              { return( PerItem                 ); }
  public  double  getRate()                 { return( Rate                    ); }
  public  double  getFeesDue()              { return( FeesDue                 ); }
  public  double  getFeesPaid()             { return( FeesPaid                ); }
  public  double  getExpense()              { return( Expense                 ); }
  public  double  getExpenseActual()        { return( ExpenseActual           ); }
  public  String  getIcCat()                { return( IcCat                   ); }
  public  double  getItemAmount()           { return( ItemAmount              ); }
  public  int     getItemCount()            { return( ItemCount               ); }
  public  String  getItemSubclass()         { return( ItemSubclass            ); }
  public  int     getItemType()             { return( ItemType                ); }
  public  String  getMeLoadFilename()       { return( MeLoadFilename          ); }
  public  long    getMeLoadFileId()         { return( MeLoadFileId            ); }
  public  long    getMerchantId()           { return( MerchantId              ); }
  public  long    getRecId()                { return( RecId                   ); }
  public  double  getSalesAmount()          { return( SalesAmount             ); }
  public  int     getSalesCount()           { return( SalesCount              ); }
  public  int     getVolumeType()           { return( VolumeType              ); }
  
  public  void    setActivityDate       (    Date value ) { ActivityDate        = value; } 
  public  void    setCreditsAmount      (  double value ) { CreditsAmount       = value; }   
  public  void    setCreditsCount       (     int value ) { CreditsCount        = value; }    
  public  void    setDataSource         (  String value ) { DataSource          = value; }   
  public  void    setDataSourceId       (    long value ) { DataSourceId        = value; } 
  public  void    setPerItem            (  double value ) { PerItem             = value; }      
  public  void    setRate               (  double value ) { Rate                = value; }     
  public  void    setFeesDue            (  double value ) { FeesDue             = value; }      
  public  void    setFeesPaid           (  double value ) { FeesPaid            = value; }     
  public  void    setExpense            (  double value ) { Expense             = value; }     
  public  void    setExpenseActual      (  double value ) { ExpenseActual       = value; }     
  public  void    setIcCat              (  String value ) { IcCat               = value; } 
  public  void    setItemAmount         (  double value ) { ItemAmount          = value; }   
  public  void    setItemCount          (     int value ) { ItemCount           = value; }    
  public  void    setItemSubclass       (  String value ) { ItemSubclass        = value; } 
  public  void    setItemType           (     int value ) { ItemType            = value; }     
  public  void    setMeLoadFilename     (  String value ) { MeLoadFilename      = value; }   
  public  void    setMeLoadFileId       (    long value ) { MeLoadFileId        = value; } 
  public  void    setMerchantId         (    long value ) { MerchantId          = value; }   
  public  void    setRecId              (    long value ) { RecId               = value; }   
  public  void    setSalesAmount        (  double value ) { SalesAmount         = value; }   
  public  void    setSalesCount         (     int value ) { SalesCount          = value; }    
  public  void    setVolumeType         (     int value ) { VolumeType          = value; } 
}
