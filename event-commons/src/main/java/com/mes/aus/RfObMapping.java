package com.mes.aus;

public class RfObMapping
{
  private long reqfId;
  private long obId;
  private String sysCode;

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }

  public void setObId(long obId)
  {
    this.obId = obId;
  }
  public long getObId()
  {
    return obId;
  }

  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  public String getSysCode()
  {
    return sysCode;
  }

  public String toString()
  {
    return "ReqfObMapping ["
      + " reqfId: " + reqfId
      + ", obId: " + obId
      + ", sysCode: " + sysCode
      + " ]";
  }
}