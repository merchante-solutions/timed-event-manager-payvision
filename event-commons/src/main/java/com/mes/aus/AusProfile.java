package com.mes.aus;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AusProfile extends AusBase implements Serializable
{
  static Logger log = Logger.getLogger(AusProfile.class);

  private String  profileId;
  private String  merchNum;
  private Date    createDate;
  private String  userName;
  private String  merchName;
  private String  vauFileType;
  private String  abuFileType;
  private String  dauFileType;
  private String  autoFlag;
  private String  autoFrequency;
  private int     autoDay;
  private long    autoAusrId;
  private String  autoAusrName;
  private Date    autoLastDate;
  private String  autoTestFlag;
                               
  public String  getProfileId()
  {
    return profileId;
  }
  public void setProfileId(String  profileId)
  {
    this.profileId = profileId;
  }
  
  public String getMerchNum()
  {
    return merchNum;
  }
  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public String getMerchName()
  {
    return merchName;
  }
  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  
  public String getVauFileType()
  {
    return vauFileType;
  }
  public void setVauFileType(String vauFileType)
  {
    this.vauFileType = vauFileType;
  }
  
  public String getAbuFileType()
  {
    return abuFileType;
  }
  public void setAbuFileType(String abuFileType)
  {
    this.abuFileType = abuFileType;
  }
  
  public String getDauFileType()
  {
    return dauFileType;
  }
  public void setDauFileType(String dauFileType)
  {
    this.dauFileType = dauFileType;
  }

  public void setAutoFlag(String autoFlag)
  {
    validateFlag(autoFlag,"auto");
    this.autoFlag = autoFlag;
  }
  public String getAutoFlag()
  {
    return flagValue(autoFlag);
  }
  public boolean isAuto()
  {
    return flagBoolean(autoFlag);
  }
  
  public String getAutoFrequency()
  {
    return autoFrequency;
  }
  public void setAutoFrequency(String autoFrequency)
  {
    this.autoFrequency = autoFrequency;
  }
  
  public int getAutoDay()
  {
    return autoDay;
  }
  public void setAutoDay(int autoDay)
  {
    this.autoDay = autoDay;
  }
  
  public long getAutoAusrId()
  {
    return autoAusrId;
  }
  public void setAutoAusrId(long autoAusrId)
  {
    this.autoAusrId = autoAusrId;
  }
  
  public String getAutoAusrName()
  {
    return autoAusrName;
  }
  public void setAutoAusrName(String autoAusrName)
  {
    this.autoAusrName = autoAusrName;
  }
  
  public void setAutoLastDate(Date autoLastDate)
  {
    this.autoLastDate = autoLastDate;
  }
  public Date getAutoLastDate()
  {
    return autoLastDate;
  }
  public void setAutoLastTs(Timestamp autoLastTs)
  {
    autoLastDate = toDate(autoLastTs);
  }
  public Timestamp getAutoLastTs()
  {
    return toTimestamp(autoLastDate);
  }

  public void setAutoTestFlag(String autoTestFlag)
  {
    validateFlag(autoTestFlag,"autoTest");
    this.autoTestFlag = autoTestFlag;
  }
  public String getAutoTestFlag()
  {
    return flagValue(autoTestFlag);
  }
  public boolean isAutoTest()
  {
    return flagBoolean(autoTestFlag);
  }
  
  public String toString()
  {
    return " AusProfile [ "
      + "profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", createDate: " + createDate
      + ", userName: " + userName
      + ", merchName: " + merchName
      + ", vauFileType: " + vauFileType
      + ", abuFileType: " + abuFileType
      + ", dauFileType: " + dauFileType
      + ", autoFlag: " + autoFlag
      + ", autoFrequency: " + autoFrequency
      + ", autoDay: " + autoDay
      + ", autoAusrId: " + autoAusrId
      + ", autoAusrName: " + autoAusrName
      + ", autoLastDate: " + autoLastDate
      + ", autoTestFlag: " + isAutoTest()
      + " ]";
  }
}
