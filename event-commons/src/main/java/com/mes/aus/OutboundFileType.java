package com.mes.aus;

import org.apache.log4j.Logger;

public class OutboundFileType extends AusBase
{
  static Logger log = Logger.getLogger(OutboundFileType.class);
  
  private String fileType;
  private String sysCode;
  private String fileId;
  private String enableFlag;
  private String description;
  
  public void setFileType(String fileType)
  {
    this.fileType = fileType;
  }
  public String getFileType()
  {
    return fileType;
  }
  
  public String getSysCode()
  {
    return sysCode;
  }
  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  
  public String getFileId()
  {
    return fileId;
  }
  public void setFileId(String fileId)
  {
    this.fileId = fileId;
  }
  
  public String getEnableFlag()
  {
    return flagValue(enableFlag);
  }
  public void setEnableFlag(String enableFlag)
  {
    validateFlag(enableFlag,"enable");
    this.enableFlag = enableFlag;
  }
  public boolean isEnabled()
  {
    return flagBoolean(enableFlag);
  }
  
  public String getDescription()
  {
    return description;
  }
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public String toString() {
    return " OutboundFileType [ "
      + "fileType: " + fileType
      + ", sysCode: " + sysCode
      + ", fileId: " + fileId
      + ", enableFlag: " + enableFlag
      + ", description: " + description
      + " ]";
  }  
}