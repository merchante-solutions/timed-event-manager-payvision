package com.mes.aus;

import org.apache.log4j.Logger;
import com.mes.support.TridentTools;
import com.mes.support.dc.MesEncryptedDataContainer;

public class AusAccount extends MesEncryptedDataContainer
{
  static Logger log = Logger.getLogger(AusAccount.class);

  public static final String  AT_VISA     = "VISA";
  public static final String  AT_MC       = "MC";
  public static final String  AT_DISC     = "DISC";

  public static final String  AT_UNKNOWN  = "UNKN";
  public static final String  AT_INVALID  = "INVD";

  private String _truncated;
  private String accountType = AT_UNKNOWN;
  private String expDate;

  public AusAccount()
  {
  }
  public AusAccount(String accountNum, String expDate, String accountType)
  {
    setData(accountNum);
    setExpDate(expDate);
    setAccountType(accountType);
  }

  public String clearText()
  {
    return empty() ? null : new String(getData());
  }

  public String truncated()
  {
    if (!empty() && _truncated == null)
    {
      try
      {
        _truncated = 
          TridentTools.encodeCardNumber(clearText());
      }
      catch (Exception e)
      {
        log.error("Truncation error: " + e);
        e.printStackTrace();
      }
    }
    return _truncated;
  }

  public void setEncodedData(byte[] data)
  {
    super.setEncodedData(data);
    _truncated = null;
  }

  public static String determineAccountType(String accountNum)
  {
    if (accountNum != null && accountNum.length() > 1)
    {
      if (accountNum.startsWith("5"))
      {
        return AT_MC;
      }
      else if (accountNum.startsWith("4"))
      {
        return AT_VISA;
      }
      else if (accountNum.startsWith("6011"))
      {
        return AT_DISC;
      }
    }
    return AT_INVALID;
  }

  public static boolean isValidAccountType(String accountType)
  {
    return AT_VISA.equals(accountType) || 
           AT_MC.equals(accountType) || 
           AT_DISC.equals(accountType) ||
           AT_UNKNOWN.equals(accountType) ||
           AT_INVALID.equals(accountType);
  }

  public void setAccountType(String accountType)
  {
    if (!isValidAccountType(accountType))
    {
      throw new RuntimeException("Invalid account type: '" + accountType + "'");
    }
    this.accountType = accountType;
  }
  public String getAccountType()
  {
    return accountType;
  }

  public void setExpDate(String expDate)
  {
    this.expDate = expDate;
  }
  public String getExpDate()
  {
    return expDate;
  }

  public String toString()
  {
    return "AusAccount [ "
      + "type: " + accountType
      + ", acct num: " + truncated()
      + ", exp date: " + expDate
      + " ]";
  }
}

