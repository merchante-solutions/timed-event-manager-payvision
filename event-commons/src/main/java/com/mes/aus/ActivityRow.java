package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class ActivityRow extends AusBase
{
  static Logger log = Logger.getLogger(ActivityRow.class);

  private long reqfId;
  private String ausrName;
  private Date reqDate;
  private String merchNum;
  private String reqStatus;
  private String reqFileName;
  private String testFlag;
  private long rspfId;
  private Date rspDate;
  private String rspFileName;
  private String status;
  private int dlCount;

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }

  public void setAusrName(String ausrName)
  {
    this.ausrName = ausrName;
  }
  public String getAusrName()
  {
    return ausrName;
  }

  public void setReqDate(Date reqDate)
  {
    this.reqDate = reqDate;
  }
  public Date getReqDate()
  {
    return reqDate;
  }
  public void setReqTs(Timestamp submitTs)
  {
    reqDate = toDate(submitTs);
  }
  public Timestamp getReqTs()
  {
    return toTimestamp(reqDate);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setReqStatus(String reqStatus)
  {
    this.reqStatus = reqStatus;
  }
  public String getReqStatus()
  {
    return reqStatus;
  }

  public void setReqFileName(String reqFileName)
  {
    this.reqFileName = reqFileName;
  }
  public String getReqFileName()
  {
    return reqFileName;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setRspfId(long rspfId)
  {
    this.rspfId = rspfId;
  }
  public long getRspfId()
  {
    return rspfId;
  }

  public void setRspDate(Date rspDate)
  {
    this.rspDate = rspDate;
  }
  public Date getRspDate()
  {
    return rspDate;
  }
  public void setRspTs(Timestamp receiveTs)
  {
    rspDate = toDate(receiveTs);
  }
  public Timestamp getRspTs()
  {
    return toTimestamp(rspDate);
  }

  public void setRspFileName(String rspFileName)
  {
    this.rspFileName = rspFileName;
  }
  public String getRspFileName()
  {
    return rspFileName;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }
  public String getStatus()
  {
    return status;
  }

  public void setDlCount(int dlCount)
  {
    this.dlCount = dlCount;
  }
  public int getDlCount()
  {
    return dlCount;
  }

  public String toString()
  {
    return "ActivityRow [ "
      + "reqfId: " + reqfId
      + ", ausrName: " + ausrName
      + ", reqDate: " + reqDate
      + ", merchNum: " + merchNum
      + ", reqStatus: " + reqStatus
      + ", reqFileName: " + reqFileName
      + ", testFlag: " + testFlag
      + ", rspfId: " + rspfId
      + ", rspDate: " + rspDate
      + ", rspFileName: " + rspFileName
      + ", status: " + status
      + ", dlCount: " + dlCount
      + " ]";
  }
}