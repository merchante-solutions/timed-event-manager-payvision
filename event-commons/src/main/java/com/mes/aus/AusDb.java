package com.mes.aus;

import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.dc.EncodedFileContainer;
import com.mes.support.dc.EncodingType;
import com.mes.support.dc.MesSecureDataContainer;
import com.mes.tools.DropDownItem;
import oracle.sql.BLOB;


public class AusDb extends SQLJConnectionBase {
	static Logger log = Logger.getLogger(AusDb.class);

	public static final String CFG_ABU_FILE_SEQ_NUM = "abu_file_seq_num";
	public static final String CFG_ABU_FILE_SEQ_NUM_TEST = "abu_file_seq_num_test";
	public static final String CFG_ABU_DUMMY_DETAIL_FLAG = "abu_dummy_detail_flag";
	public static final String CFG_ABU_DUMMY_MERCH_NUM = "abu_dummy_merch_num";
	public static final String CFG_ABU_OB_XFER_LOC = "abu_ob_xfer_loc";
	public static final String CFG_ABU_IB_XFER_LOC = "abu_ib_xfer_loc";
	public static final String CFG_ABU_OB_XFER_ERR_CNT = "abu_ob_xfer_err_cnt";
	public static final String CFG_ABU_IB_XFER_ERR_CNT = "abu_ib_xfer_err_cnt";

	public static final String CFG_VAU_FILE_SEQ_NUM = "vau_file_seq_num";
	public static final String CFG_VAU_FILE_SEQ_NUM_TEST = "vau_file_seq_num_test";
	public static final String CFG_VAU_DUMMY_DETAIL_FLAG = "vau_dummy_detail_flag";
	public static final String CFG_VAU_DUMMY_MERCH_NUM = "vau_dummy_merch_num";
	public static final String CFG_VAU_OB_XFER_LOC = "vau_ob_xfer_loc";
	public static final String CFG_VAU_OB_ARC_LOC = "vau_ob_arc_loc";
	public static final String CFG_VAU_OB_XFER_ERR_CNT = "vau_ob_xfer_err_cnt";
	public static final String CFG_VAU_IB_XFER_LOC = "vau_ib_xfer_loc";
	public static final String CFG_VAU_IB_ARC_LOC = "vau_ib_arc_loc";
	public static final String CFG_VAU_ENDPOINT_HOST = "vau_endpoint_host";
	public static final String CFG_VAU_ENDPOINT_USER = "vau_endpoint_user";
	public static final String CFG_VAU_ENDPOINT_PASSWORD = "vau_endpoint_password";

	public static final String CFG_DAU_FILE_SEQ_NUM = "dau_file_seq_num";
	public static final String CFG_DAU_FILE_SEQ_NUM_TEST = "dau_file_seq_num_test";
	public static final String CFG_DAU_ENDPOINT_HOST_PROD = "dau_endpoint_host_prod";
	public static final String CFG_DAU_ENDPOINT_HOST_TEST = "dau_endpoint_host_test";
	public static final String CFG_DAU_OB_ENDPOINT_USER = "dau_ob_endpoint_user";
	public static final String CFG_DAU_OB_ENDPOINT_PASSWORD = "dau_ob_endpoint_password";
	public static final String CFG_DAU_OB_XFER_LOC = "dau_ob_xfer_loc";
	public static final String CFG_DAU_IB_ENDPOINT_USER = "dau_ib_endpoint_user";
	public static final String CFG_DAU_IB_ENDPOINT_PASSWORD = "dau_ib_endpoint_password";
	public static final String CFG_DAU_IB_XFER_FILE_MASK = "dau_ib_xfer_file_mask";
	public static final String CFG_DAU_IB_XFER_LOC = "dau_ib_xfer_loc";

	public static final String CFG_ERROR_EMAILS = "error_emails";
	public static final String CFG_REJECT_EMAILS = "reject_emails";

	public static final String CFG_AUTO_CARD_MAX = "auto_card_max";

	public static final String CFG_PURGE_DAYS_BACK = "purge_days_back";
	public static final String CFG_ACTIVITY_DAYS_BACK = "activity_days_back";

	private boolean directFlag;
	private String defaultAusEmails = "";

	public AusDb(boolean directFlag) {
		//super(directFlag ? DbProperties.dbDirect() : DbProperties.dbPool());
		this.directFlag = directFlag;
		try {
			defaultAusEmails = MesDefaults.getString(MesDefaults.DEFAULT_AUS_EMAILS);
		}
		catch (Exception e) {
			logEntry("AusDb() constructor - set defaultAusEmails property", e.toString());
		}
	}
	public AusDb() {
		// default direct flag off
		this(false);
	}
	public boolean isDirect() {
		return directFlag;
	}
	/**
	 * Error logging
	 */
	public void logEntry(Object src, String method, Exception e) {
		logEntry(src.getClass().getName() + "." + method, "" + e);
	}
	/**
	 * Parameter handling
	 */
	private String getConfigValue(String name) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);	//avoid connection kill
			s = con.createStatement();
			rs = s.executeQuery(" select value from aus_config where name = '" + name + "'");
			if (rs.next()) {
				return rs.getString("value");
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
			logEntry(this, "getConfigValue(name='" + name + "')", e);
			throw new RuntimeException(e);
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	private int getConfigValueInt(String name) {
		String value = getConfigValue(name);
		try {
			return Integer.parseInt(value);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
			logEntry(this, "getConfigValueInt(name='" + name + "')", e);
			throw new RuntimeException(e);
		}
	}
	private void setConfigValue(String name, String value) {
		Statement s = null;
		try {
			connect(true);	//avoid connection kill
			s = con.createStatement();
			int rowCount = s.executeUpdate(" update aus_config set value = '" + value + "' where name = '" + name + "'");
			if (rowCount < 1) {
				s.executeUpdate(" insert into aus_config ( name, value ) values ( '" + name + "', '" + value + "' ) ");
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	private void setConfigValue(String name, int value) {
		setConfigValue(name, String.valueOf(value));
	}
	/**
	 * VAU seqence now maintained in outbound file type record.
	 */
	public long getVauFileSeqNum(OutboundFileType obft) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);	//avoid connection kill
			s = con.createStatement();
			rs = s.executeQuery(" select seq_num from aus_file_types where file_type = '" + obft.getFileType() + "'");
			if (rs.next()) {
				return rs.getLong(1);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return 0;
	}
	public void incrementVauFileSeqNum(OutboundFileType obft) {
		Statement s = null;
		try {
			connect(true);	// -avoid connection kill
			s = con.createStatement();
			String sqlQuery = "update aus_file_types set seq_num = case when seq_num + 1 > seq_num_max then 1 else seq_num + 1 end where file_type = '" + obft.getFileType() + "'";
			s.executeQuery(sqlQuery);
			/*
			s.executeUpdate(" update aus_file_types "
					+ " set seq_num =         " + "  case                 "
					+ "   when seq_num + 1 >  " + "    seq_num_max then 1 "
					+ "   else seq_num + 1    " + "  end                  "
					+ " where file_type =    '" + obft.getFileType() + "' ");
			*/
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	public String getDauFileSeqNum(boolean isTest) {
		String snName = isTest ? CFG_DAU_FILE_SEQ_NUM_TEST : CFG_DAU_FILE_SEQ_NUM;
		String seqNum = getConfigValue(snName);
		if (seqNum == null) {
			seqNum = "1";
			setConfigValue(snName, seqNum);
		}
		return seqNum;
	}
	public void incrementDauFileSeqNum(boolean isTest) {
		String snName = isTest ? CFG_DAU_FILE_SEQ_NUM_TEST : CFG_DAU_FILE_SEQ_NUM;
		String curVal = getDauFileSeqNum(isTest);
		try {
			int newVal = Integer.parseInt(curVal);
			setConfigValue(snName, "" + (newVal + 1));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
	}
	public String getAbuFileSeqNum(boolean isTest) {
		String snName = isTest ? CFG_ABU_FILE_SEQ_NUM_TEST : CFG_ABU_FILE_SEQ_NUM;
		String seqNum = getConfigValue(snName);
		if (seqNum == null) {
			seqNum = "1";
			setConfigValue(snName, seqNum);
		}
		return seqNum;
	}
	public void incrementAbuFileSeqNum(boolean isTest) {
		String snName = isTest ? CFG_ABU_FILE_SEQ_NUM_TEST : CFG_ABU_FILE_SEQ_NUM;
		String curVal = getAbuFileSeqNum(isTest);
		try {
			int newVal = Integer.parseInt(curVal);
			setConfigValue(snName, "" + (newVal + 1));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
	}
	public String getAbuDummyDetailFlag() {
		return getConfigValue(CFG_ABU_DUMMY_DETAIL_FLAG);
	}
	public String getAbuDummyMerchNum() {
		return getConfigValue(CFG_ABU_DUMMY_MERCH_NUM);
	}
	public String getVauDummyDetailFlag() {
		return getConfigValue(CFG_VAU_DUMMY_DETAIL_FLAG);
	}
	public String getVauDummyMerchNum() {
		return getConfigValue(CFG_VAU_DUMMY_MERCH_NUM);
	}
	/**
	 * ABU file xfer config
	 */
	public String getAbuObXferLoc() {
		return getConfigValue(CFG_ABU_OB_XFER_LOC);
	}
	public String getAbuIbXferLoc() {
		return getConfigValue(CFG_ABU_IB_XFER_LOC);
	}
	public int getAbuObXferErrCnt() {
		return getConfigValueInt(CFG_ABU_OB_XFER_ERR_CNT);
	}
	public void incrementAbuObXferErrCnt() {
		setConfigValue(CFG_ABU_OB_XFER_ERR_CNT, getAbuObXferErrCnt() + 1);
	}
	public int getAbuIbXferErrCnt() {
		return getConfigValueInt(CFG_ABU_IB_XFER_ERR_CNT);
	}
	public void incrementAbuIbXferErrCnt() {
		setConfigValue(CFG_ABU_IB_XFER_ERR_CNT, getAbuIbXferErrCnt() + 1);
	}
	/**
	 * VAU file xfer configuration
	 */
	public int getVauObXferErrCnt() {
		return getConfigValueInt(CFG_VAU_OB_XFER_ERR_CNT);
	}
	public void incrementVauObXferErrCnt() {
		setConfigValue(CFG_VAU_OB_XFER_ERR_CNT, getVauObXferErrCnt() + 1);
	}
	public String getVauEndpointHost() {
		return getConfigValue(CFG_VAU_ENDPOINT_HOST);
	}
	public String getVauEndpointUser() {
		return getConfigValue(CFG_VAU_ENDPOINT_USER);
	}
	public String getVauEndpointPassword() {
		return getConfigValue(CFG_VAU_ENDPOINT_PASSWORD);
	}
	public String getVauObXferLoc() {
		return getConfigValue(CFG_VAU_OB_XFER_LOC);
	}
	public String getVauObArcLoc() {
		return getConfigValue(CFG_VAU_OB_ARC_LOC);
	}
	public String getVauIbXferLoc() {
		return getConfigValue(CFG_VAU_IB_XFER_LOC);
	}
	public String getVauIbArcLoc() {
		return getConfigValue(CFG_VAU_IB_ARC_LOC);
	}
	/**
	 * DAU file xfer configuration
	 */
	public String getDauEndpointHostProd() {
		return getConfigValue(CFG_DAU_ENDPOINT_HOST_PROD);
	}
	public String getDauEndpointHostTest() {
		return getConfigValue(CFG_DAU_ENDPOINT_HOST_TEST);
	}
	public String getDauEndpointHost(boolean isTest) {
		return isTest ? getDauEndpointHostTest() : getDauEndpointHostProd();
	}
	public String getDauObEndpointUser() {
		return getConfigValue(CFG_DAU_OB_ENDPOINT_USER);
	}
	public String getDauObEndpointPassword() {
		return getConfigValue(CFG_DAU_OB_ENDPOINT_PASSWORD);
	}
	public String getDauObXferLoc() {
		return getConfigValue(CFG_DAU_OB_XFER_LOC);
	}
	public String getDauIbXferLoc() {
		return getConfigValue(CFG_DAU_IB_XFER_LOC);
	}
	public String getDauIbEndpointUser() {
		return getConfigValue(CFG_DAU_IB_ENDPOINT_USER);
	}
	public String getDauIbEndpointPassword() {
		return getConfigValue(CFG_DAU_IB_ENDPOINT_PASSWORD);
	}
	public String getDauIbXferFileMask() {
		return getConfigValue(CFG_DAU_IB_XFER_FILE_MASK);
	}
	public String getErrorNotificationEmails() {
		String emails = getConfigValue(CFG_ERROR_EMAILS);
		return (emails != null ? emails : defaultAusEmails);
	}
	public String getRejectNotificationEmails() {
		String emails = getConfigValue(CFG_REJECT_EMAILS);
		return (emails != null ? emails : defaultAusEmails);
	}
	public int getAutoCardMax() {
		return getConfigValueInt(CFG_AUTO_CARD_MAX);
	}
	public int getPurgeDaysBack() {
		return getConfigValueInt(CFG_PURGE_DAYS_BACK);
	}
	public int getActivityDaysBack() {
		return getConfigValueInt(CFG_ACTIVITY_DAYS_BACK);
	}
	/**
	 * Fetches next value from aus id sequence.
	 */
	public long getNewId() throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connect(true);	//avoid connection kill
			ps = con.prepareStatement(" select aus_id_sequence.nextval from dual ");
			rs = ps.executeQuery();
			rs.next();
			long newId = rs.getLong(1);
			return newId;
		}
		finally {
			cleanUp(ps, rs);
		}
	}
	public class AusFileRowId {
		public String tableName;
		public String idColName;
		public long rowId;
		public AusFileContainer afc;

		public AusFileRowId(String tableName, String idColName, long rowId, AusFileContainer afc) {
			this.tableName = tableName;
			this.idColName = idColName;
			this.rowId = rowId;
			this.afc = afc;
		}
	}
	/**
	 * Loads file container with blob data. If data is not file container
	 * encoded it will be decoded via the old data container and then stored in
	 * the file container as unencoded data, otherwise it can be set directly in
	 * the file container in encoded form.
	 */
	private void setFileContainerData(EncodedFileContainer efc, ResultSet rs, String colName) throws Exception {
		BLOB b = (BLOB) rs.getBlob(colName);
		if (b != null) {
			EncodingType et = EncodingType.forType(b.getBinaryStream());
			if (et.equals(EncodingType.UNKNOWN)) {
				// legacy encoded, need to decode using data container
				MesSecureDataContainer dc = new MesSecureDataContainer();
				dc.setEncodedData(b.getBinaryStream());
				efc.setData(dc.getData());
			}
			else {
				// not legacy, set data directly in file container
				efc.setEncodedData(b.getBinaryStream());
			}
			log.debug("File container loaded with " + efc.size() + " bytes.");
		}
		else {
			log.error("Result set returned null blob when attempting to" + " load file container: " + efc);
		}
	}
	/**
	 * Uploads data from data container to specified table. Requires a
	 * AusFileRowId object to identify the table name, id column and id value,
	 * and to provide reference to an AusFile object.
	 *
	 * Assumes db connection already established.
	 *
	 * Table must contain columns named file_data (of type blob) and file_size.
	 */
	private boolean uploadAusFileData(AusFileRowId afrId) {
		Statement s = null;
		ResultSet rs = null;
		try {
			// set empty blob in upload record
			s = con.createStatement();
			int rowCount = s.executeUpdate(" update " + afrId.tableName + " set file_data = empty_blob() where " + afrId.idColName + " = " + afrId.rowId);
			if (rowCount != 1) {
				throw new RuntimeException("Upload id " + afrId.rowId + " in table " + afrId.tableName + " row count should be 1, is " + rowCount);
			}
			s.close();

			// fetch output stream to write to file data blob,
			// have the upload write the file data to it
			s = con.createStatement();
			rs = s.executeQuery(" select file_data from " + afrId.tableName + " where " + afrId.idColName + " = " + afrId.rowId + " for update ");
			rs.next();
			BLOB b = (BLOB) rs.getBlob(1);
			OutputStream out = b.setBinaryStream(1L);
			long fileSize = 0;
			try {
				fileSize = afrId.afc.writeEncodedData(out);
			}
			finally {
				try {
					out.flush();
					out.close();
				}
				catch (Exception e) {}
			}
			rs.close();
			s.close();

			// update the file size
			s = con.createStatement();
			s.executeUpdate(" update " + afrId.tableName + " set file_size = " + fileSize + " where " + afrId.idColName + " = " + afrId.rowId);
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
		}
		return false;
	}
	/**
	 * Insert a new upload record into aus_request_files with null file_data. To
	 * add the file data use setUploadFileData().
	 */
	public boolean insertRequestFile(RequestFile reqf, boolean dataFlag) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "insert into aus_request_files (reqf_id,create_ts,user_name,ausr_name,merch_num,file_name,file_size,status,test_flag,load_num) values (?,?,?,?,?,?,?,?,?,0)";
			ps = con.prepareStatement(sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_request_files "
					+ " ( reqf_id,                    "
					+ "   create_ts,                  "
					+ "   user_name,                  "
					+ "   ausr_name,                  "
					+ "   merch_num,                  "
					+ "   file_name,                  "
					+ "   file_size,                  "
					+ "   status,                     "
					+ "   test_flag,                  "
					+ "   load_num )                  "
					+ " values                        "
					+ " ( ?, ?, ?, ?, ?,              "
					+ "   ?, ?, ?, ?, 0 )             ");
			*/
			int colNum = 1;
			ps.setLong(colNum++, reqf.getReqfId());
			ps.setTimestamp(colNum++, reqf.getCreateTs());
			ps.setString(colNum++, reqf.getUserName());
			ps.setString(colNum++, reqf.getAusrName());
			ps.setString(colNum++, reqf.getMerchNum());
			ps.setString(colNum++, reqf.getFileName());
			ps.setLong(colNum++, 0L);
			ps.setString(colNum++, reqf.getStatus());
			ps.setString(colNum++, reqf.getTestFlag());
			ps.executeUpdate();

			// return true if dataFlag not true or
			// if no file data available to upload
			// or the result of uploading the file data
			if (!dataFlag || reqf.empty()) {
				return true;
			}
			return uploadAusFileData(new AusFileRowId("aus_request_files", "reqf_id", reqf.getReqfId(), reqf));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	/**
	 * Update upload record. Ignores file size, this is automatically set by
	 * uploadFileData().
	 */
	public boolean updateRequestFile(RequestFile reqf, boolean dataFlag) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "update aus_request_files set create_ts=?, user_name=?, ausr_name=?, merch_num=?, file_name=?, status=?, test_flag=?, load_num=? where reqf_id=? ";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" update aus_request_files set  "
					+ "  create_ts = ?,               "
					+ "  user_name = ?,               "
					+ "  ausr_name = ?,               "
					+ "  merch_num = ?,               "
					+ "  file_name = ?,               "
					+ "  status    = ?,               "
					+ "  test_flag = ?,               "
					+ "  load_num = ?                 "
					+ " where                         "
					+ "  reqf_id = ?                  ");
			*/
			int colNum = 1;
			ps.setTimestamp(colNum++, reqf.getCreateTs());
			ps.setString(colNum++, reqf.getUserName());
			ps.setString(colNum++, reqf.getAusrName());
			ps.setString(colNum++, reqf.getMerchNum());
			ps.setString(colNum++, reqf.getFileName());
			ps.setString(colNum++, reqf.getStatus());
			ps.setString(colNum++, reqf.getTestFlag());
			ps.setInt(colNum++, reqf.getParseNum());
			ps.setLong(colNum++, reqf.getReqfId());
			ps.executeUpdate();

			// return true if dataFlag not true or
			// if no file data available to upload
			// or the result of uploading the file data
			return (!dataFlag || reqf.empty() || uploadAusFileData(new AusFileRowId(
					"aus_request_files", "reqf_id", reqf.getReqfId(), reqf)));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	private RequestFile createRequestFile(ResultSet rs, boolean dataFlag) throws Exception {
		RequestFile reqf = new RequestFile();
		try{
			reqf.setReqfId(rs.getLong("reqf_id"));
			reqf.setCreateTs(rs.getTimestamp("create_ts"));
			reqf.setUserName(rs.getString("user_name"));
			reqf.setAusrName(rs.getString("ausr_name"));
			reqf.setMerchNum(rs.getString("merch_num"));
			reqf.setFileName(rs.getString("file_name"));
			reqf.setFileSize(rs.getInt("file_size"));
			reqf.setStatus(rs.getString("status"));
			reqf.setTestFlag(rs.getString("test_flag"));
			reqf.setParseNum(rs.getInt("load_num"));
			reqf.setDiscMerchNum(rs.getString("disc_merch_num"));
			if (dataFlag) {
				setFileContainerData(reqf, rs, "file_data");
			}
		}
		catch(Exception e){
			log.error("Error while processing reqf_id: " + (rs.getLong("reqf_id")>0L?String.valueOf(rs.getLong("reqf_id")):"") + "\n" + e);
			reqf.setStatus(reqf.ST_ERROR);
		}
		return reqf;
	}
	/**
	 * Fetch upload record with given reqfId.
	 */
	public RequestFile getRequestFile(long reqfId, boolean dataFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select r.*, mif.dmdsnum disc_merch_num from aus_request_files r, mif where r.merch_num = mif.merchant_number (+) and r.reqf_id = " + reqfId;
			rs = s.executeQuery(sqlQuery);			
			/*
			rs = s.executeQuery(" select                "
					+ "  r.*,                 "
					+ "  mif.dmdsnum disc_merch_num "
					+ " from                  " + "  aus_request_files r, "
					+ "  mif                  " + " where                 "
					+ "  r.reqf_id = " + reqfId
					+ "  and r.merch_num = mif.merchant_number (+)  ");
			*/
			if (rs.next()) {
				return createRequestFile(rs, dataFlag);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getRequestFilesWithStatus(String status, boolean dataFlag, boolean testFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select r.*, mif.dmdsnum disc_merch_num from aus_request_files r, mif where r.status = '" + status + "' and r.merch_num = mif.merchant_number (+) and r.test_flag = '" + (testFlag ? "y" : "n") + "'";
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select                "
					+ "  r.*,                 "
					+ "  mif.dmdsnum disc_merch_num "
					+ " from                  " + "  aus_request_files r, "
					+ "  mif                  " + " where                 "
					+ "  r.status = '" + status + "'                "
					+ "  and r.merch_num = mif.merchant_number (+)  "
					+ "  and r.test_flag = '" + (testFlag ? "y" : "n") + "'");
			*/
			List rfList = new ArrayList();
			while (rs.next()) {
				rfList.add(createRequestFile(rs, dataFlag));
			}
			return rfList;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	private OutboundFileType createOutboundFileType(ResultSet rs) throws Exception {
		OutboundFileType obft = new OutboundFileType();
		obft.setFileType(rs.getString("file_type"));
		obft.setSysCode(rs.getString("sys_code"));
		obft.setFileId(rs.getString("file_id"));
		obft.setEnableFlag(rs.getString("enable_flag"));
		obft.setDescription(rs.getString("description"));
		return obft;
	}
	/**
	 * Generates list of outbound file types for given system code. Null sysCode
	 * will generate ordered list of all file types.
	 */
	public List getOutboundFileTypes(String sysCode) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_file_types " + (sysCode != null ? " where sys_code = '" + sysCode + "' " : "") + " order by sys_code, file_type ");
			List ofts = new ArrayList();
			while (rs.next()) {
				ofts.add(createOutboundFileType(rs));
			}
			return ofts;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getObfReqfBatchForReqf(long reqfId, String sysCode, boolean testFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select                                              "
					+ "  r.*,                                               "
					+ "  mif.dmdsnum disc_merch_num,                        "
					+ "  t.*                                                "
					+ " from                                                "
					+ "  aus_request_files  r,                              "
					+ "  ( select                                           "
					+ "     merch_num,                                      "
					+ "     decode('"
					+ sysCode
					+ "',                       "
					+ "       'VAU',vau_file_type,                          "
					+ "       'ABU',abu_file_type,                          "
					+ "       dau_file_type) file_type                      "
					+ "     from                                            "
					+ "      aus_profiles ) p,                              "
					+ "  aus_file_types t,                                  "
					+ "  ( select reqf_id from aus_out_rf                   "
					+ "    where sys_code = '"
					+ sysCode
					+ "' ) orm,        "
					+ "  mif                                                "
					+ " where                                               "
					+ "  r.merch_num = p.merch_num                          "
					+ "  and r.reqf_id = "
					+ reqfId
					+ "                     "
					+ "  and p.file_type = t.file_type                      "
					+ "  and t.sys_code = '"
					+ sysCode
					+ "'                 "
					+ "  and t.enable_flag = 'y'                            "
					+ "  and r.reqf_id = orm.reqf_id(+)                     "
					+ "  and orm.reqf_id is null                            "
					+ "  and r.status = 'PROCESSED'                         "
					+ "  and r.merch_num = mif.merchant_number (+)          "
					+ "  and r.test_flag = '"
					+ (testFlag ? "y" : "n")
					+ "' "
					+ " order by t.file_type                                ");

			List batches = new ArrayList();
			RequestFileBatch batch = null;
			while (rs.next()) {
				if (batch == null || !rs.getString("file_type").equals(batch.getObfType().getFileType())) {
					batch = new RequestFileBatch(createOutboundFileType(rs));
					batches.add(batch);
				}

				// generate request files without file data
				batch.addRequestFile(createRequestFile(rs, false));
			}
			return batches;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Returns list of request file batches that have not been included in an
	 * outbound file to a specified updater service.
	 */
	public List getOutboundRequestFileBatches(String sysCode, boolean testFlag) {
		Statement s = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		try {
			log.debug("getOutboundRequestFileBatches - startTime: " + startTime);
			connect(true);
			s = con.createStatement();
			String testFlagStr = testFlag ? "y" : "n";

			// updated request supporting discover to filter out empty batches
			String queryStmt = 
							" select                                                       "
							+ "  r.*,                                                        "
							+ "  visa_cnt,                                                   "
							+ "  mc_cnt,                                                     "
							+ "  disc_cnt                                                    "
							+ "  disc_merch_num,                                             "
							+ "  t.*                                                         "
							+ " from                                                         "
							+ "   aus_request_files r,                                       "
							+ "   aus_file_types t,                                          "
							+ "   ( select                                                   "
							+ "      r.reqf_id,                                              "
							+ "      mif.dmdsnum disc_merch_num,                             "
							+ "      t.file_id,                                              "
							+ "      sum(decode(req.account_type,'VISA',1,0)) visa_cnt,      "
							+ "      sum(decode(req.account_type,'MC',1,0)) mc_cnt,          "
							+ "      sum(decode(req.account_type,'DISC',1,0)) disc_cnt       "
							+ "     from                                                     "
							+ "      aus_requests req,                                       "
							+ "      aus_request_files  r,                                   "
							+ "      ( select                                                "
							+ "        merch_num,                                            "
							+ "        decode('"
							+ sysCode
							+ "',                                      "
							+ "           'VAU',vau_file_type,                               "
							+ "           'ABU',abu_file_type,                               "
							+ "            dau_file_type) file_type                          "
							+ "         from                                                 "
							+ "          aus_profiles ) p,                                   "
							+ "      aus_file_types t,                                       "
							+ "      ( select reqf_id from aus_out_rf                        "
							+ "        where sys_code = '"
							+ sysCode
							+ "' ) orm,                      "
							+ "      mif                                                     "
							+ "     where                                                    "
							+ "      r.merch_num = p.merch_num                               "
							+ "      and p.file_type = t.file_type                           "
							+ "      and t.sys_code = '"
							+ sysCode
							+ "'                               "
							+ "      and t.enable_flag = 'y'                                 "
							+ "      and r.reqf_id = orm.reqf_id(+)                          "
							+ "      and orm.reqf_id is null                                 "
							+ "      and r.status = 'PROCESSED'                              "
							+ "      and r.merch_num = mif.merchant_number (+)               "
							+ "      and r.test_flag = '"
							+ testFlagStr
							+ "'                             "
							+ "      and r.reqf_id = req.reqf_id                             "
							+ "     group by r.reqf_id, mif.dmdsnum, t.file_id ) c           "
							+ " where                                                        "
							+ "  decode('"
							+ sysCode
							+ "','DAU',c.disc_cnt,c.visa_cnt + c.mc_cnt) > 0 "
							+ "  and ('"
							+ sysCode
							+ "' <> 'DAU' or c.disc_merch_num <> 0 )           "
							+ "  and r.reqf_id = c.reqf_id                                   "
							+ "  and t.file_id = c.file_id                                   "
							+ " order by t.file_id                                           ";
			
			log.debug("Query Stmt: " + queryStmt);
			rs = s.executeQuery(queryStmt);
			/*
			rs = s.executeQuery(" select                                                       "
					+ "  r.*,                                                        "
					+ "  visa_cnt,                                                   "
					+ "  mc_cnt,                                                     "
					+ "  disc_cnt                                                    "
					+ "  disc_merch_num,                                             "
					+ "  t.*                                                         "
					+ " from                                                         "
					+ "   aus_request_files r,                                       "
					+ "   aus_file_types t,                                          "
					+ "   ( select                                                   "
					+ "      r.reqf_id,                                              "
					+ "      mif.dmdsnum disc_merch_num,                             "
					+ "      t.file_id,                                              "
					+ "      sum(decode(req.account_type,'VISA',1,0)) visa_cnt,      "
					+ "      sum(decode(req.account_type,'MC',1,0)) mc_cnt,          "
					+ "      sum(decode(req.account_type,'DISC',1,0)) disc_cnt       "
					+ "     from                                                     "
					+ "      aus_requests req,                                       "
					+ "      aus_request_files  r,                                   "
					+ "      ( select                                                "
					+ "        merch_num,                                            "
					+ "        decode('"
					+ sysCode
					+ "',                                      "
					+ "           'VAU',vau_file_type,                               "
					+ "           'ABU',abu_file_type,                               "
					+ "            dau_file_type) file_type                          "
					+ "         from                                                 "
					+ "          aus_profiles ) p,                                   "
					+ "      aus_file_types t,                                       "
					+ "      ( select reqf_id from aus_out_rf                        "
					+ "        where sys_code = '"
					+ sysCode
					+ "' ) orm,                      "
					+ "      mif                                                     "
					+ "     where                                                    "
					+ "      r.merch_num = p.merch_num                               "
					+ "      and p.file_type = t.file_type                           "
					+ "      and t.sys_code = '"
					+ sysCode
					+ "'                               "
					+ "      and t.enable_flag = 'y'                                 "
					+ "      and r.reqf_id = orm.reqf_id(+)                          "
					+ "      and orm.reqf_id is null                                 "
					+ "      and r.status = 'PROCESSED'                              "
					+ "      and r.merch_num = mif.merchant_number (+)               "
					+ "      and r.test_flag = '"
					+ testFlagStr
					+ "'                             "
					+ "      and r.reqf_id = req.reqf_id                             "
					+ "     group by r.reqf_id, mif.dmdsnum, t.file_id ) c           "
					+ " where                                                        "
					+ "  decode('"
					+ sysCode
					+ "','DAU',c.disc_cnt,c.visa_cnt + c.mc_cnt) > 0 "
					+ "  and ('"
					+ sysCode
					+ "' <> 'DAU' or c.disc_merch_num <> 0 )           "
					+ "  and r.reqf_id = c.reqf_id                                   "
					+ "  and t.file_id = c.file_id                                   "
					+ " order by t.file_id                                           ");
			*/
			

			// original mc/visa only request
			/*
			 * rs = s.executeQuery(
			 * " select                                              " +
			 * "  r.*,                                               " +
			 * "  mif.dmdsnum disc_merch_num,                        " +
			 * "  t.*                                                " +
			 * " from                                                " +
			 * "  aus_request_files  r,                              " +
			 * "  ( select                                           " +
			 * "     merch_num,                                      " +
			 * "     decode('" + sysCode + "',                       " +
			 * "       'VAU',vau_file_type,                          " +
			 * "       'ABU',abu_file_type,                          " +
			 * "       dau_file_type) file_type                      " +
			 * "     from                                            " +
			 * "      aus_profiles ) p,                              " +
			 * "  aus_file_types t,                                  " +
			 * "  ( select reqf_id from aus_out_rf                   " +
			 * "    where sys_code = '" + sysCode + "' ) orm,        " +
			 * "  mif                                                " +
			 * " where                                               " +
			 * "  r.merch_num = p.merch_num                          " +
			 * "  and p.file_type = t.file_type                      " +
			 * "  and t.sys_code = '" + sysCode + "'                 " +
			 * "  and t.enable_flag = 'y'                            " +
			 * "  and r.reqf_id = orm.reqf_id(+)                     " +
			 * "  and orm.reqf_id is null                            " +
			 * "  and r.status = 'PROCESSED'                         " +
			 * "  and r.merch_num = mif.merchant_number (+)          " +
			 * "  and r.test_flag = '" + (testFlag ? "y" : "n") + "' " +
			 * " order by t.file_type                                ");
			 */
			List batches = new ArrayList();
			RequestFileBatch batch = null;
			while (rs.next()) {
				if (batch == null || !rs.getString("file_type").equals(batch.getObfType().getFileType())) {
					log.debug("batch is null.  Create OutboundFileType");
					batch = new RequestFileBatch(createOutboundFileType(rs));
					batches.add(batch);
				}
				// generate request files without file data
				batch.addRequestFile(createRequestFile(rs, false));
				log.debug("Generate request without file data - end");
			}
			log.debug("getOutboundRequestFileBatches - duration: " + (System.currentTimeMillis() - startTime));
			return batches;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List lookupRequestFiles(String ausrName, String merchNum) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			boolean nameFlag = (ausrName != null && !ausrName.equals(""));
			boolean mnFlag = (merchNum != null && !merchNum.equals(""));
			s = con.createStatement();
			String sqlQuery = "select r.*, mif.dmdsnum disc_merch_num from aus_request_files r, mif where r.reqf_id > 0 and r.merch_num = mif.merchant_number (+)  "
					+ (nameFlag ? " and r.ausr_name = '" + ausrName + "' " : "") + (mnFlag ? " and r.merch_num = '" + merchNum + "' " : "")
					+ " order by r.reqf_id desc ";
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select                "
					+ "  r.*,                 "
					+ "  mif.dmdsnum disc_merch_num "
					+ " from                  "
					+ "  aus_request_files r, "
					+ "  mif                  "
					+ " where                 "
					+ "  r.reqf_id > 0        "
					+ "  and r.merch_num = mif.merchant_number (+)  "
					+ (nameFlag ? " and r.ausr_name = '" + ausrName + "' " : "")
					+ (mnFlag ? " and r.merch_num = '" + merchNum + "' " : "")
					+ " order by r.reqf_id desc ");
			*/
			List rfList = new ArrayList();
			while (rs.next()) {
				rfList.add(createRequestFile(rs, false));
			}
			return rfList;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Create a new MES user ref and load it with current row of a result set.
	 */
	private UserRef createMesUserRef(ResultSet rs) throws Exception {
		UserRef ref = new UserRef();
		ref.setUserId(rs.getLong("user_id"));
		ref.setFullName(rs.getString("full_name"));
		ref.setLoginName(rs.getString("login_name"));
		ref.setTypeName(rs.getString("type_name"));
		ref.setAusrName(rs.getString("ausr_name"));
		ref.setAusEnabledFlag(rs.getString("aus_enabled_flag"));
		ref.setMerchNum(rs.getString("merch_num"));
		return ref;
	}
	/**
	 * Search for MES users matching a search term, generate user refs for found
	 * users.
	 */
	public List searchForUserRefs(String searchTerm) {
		Statement s = null;
		ResultSet rs = null;
		try {
			// fetch input stream to upload file data
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select                                          "
					+ "  u.user_id            user_id,                  "
					+ "  u.name               full_name,                "
					+ "  u.login_name         login_name,               "
					+ "  ut.name              type_name,                "
					+ "  au.ausr_name         ausr_name,                "
					+ "  nvl(au.enabled_flag,'n') aus_enabled_flag,     "
					+ "  mif.merchant_number  merch_num                 "
					+ " from                                            "
					+ "  users      u,                                  "
					+ "  user_types ut,                                 "
					+ "  aus_users  au,                                 "
					+ "  mif                                            "
					+ " where                                           "
					+ "  ( ( lower(u.name) like                         "
					+ "        lower('%"
					+ searchTerm
					+ "%') )          "
					+ "    or ( lower(u.login_name) like                "
					+ "        lower('%"
					+ searchTerm
					+ "%') ) )        "
					+ "  and u.login_name = au.ausr_name(+)             "
					+ "  and u.hierarchy_node = mif.merchant_number(+)  "
					+ "  and u.type_id = ut.type_id(+)                  ");
			List refs = new ArrayList();
			while (rs.next()) {
				refs.add(createMesUserRef(rs));
			}
			return refs;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public UserRef getMesUserRef(String loginName) {
		Statement s = null;
		ResultSet rs = null;
		try {
			// fetch input stream to upload file data
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select u.user_id user_id, u.name full_name, u.login_name login_name, ut.name type_name, au.ausr_name ausr_name, "
					+ "nvl(au.enabled_flag,'n') aus_enabled_flag, mif.merchant_number merch_num from users u, user_types ut, aus_users  au, mif "
					+ "where u.login_name = '" + loginName + "' and u.login_name = au.ausr_name(+) and u.hierarchy_node = mif.merchant_number(+) and u.type_id = ut.type_id(+)";
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select                                          "
					+ "  u.user_id            user_id,                  "
					+ "  u.name               full_name,                "
					+ "  u.login_name         login_name,               "
					+ "  ut.name              type_name,                "
					+ "  au.ausr_name         ausr_name,                "
					+ "  nvl(au.enabled_flag,'n') aus_enabled_flag,     "
					+ "  mif.merchant_number  merch_num                 "
					+ " from                                            "
					+ "  users      u,                                  "
					+ "  user_types ut,                                 "
					+ "  aus_users  au,                                 "
					+ "  mif                                            "
					+ " where                                           "
					+ "  u.login_name = '"
					+ loginName
					+ "'             "
					+ "  and u.login_name = au.ausr_name(+)             "
					+ "  and u.hierarchy_node = mif.merchant_number(+)  "
					+ "  and u.type_id = ut.type_id(+)                  ");
			*/	
			if (rs.next()) {
				return createMesUserRef(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Insert new aus user record.
	 */
	public boolean insertAusUser(AusUser ausUser) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "insert into aus_users (ausr_id,create_ts,user_name,ausr_name,ausr_desc,test_flag,notify_flag,token_flag,email,enabled_flag) values(?,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement(sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_users "
					+ " ( ausr_id,            " + "   create_ts,          "
					+ "   user_name,          " + "   ausr_name,          "
					+ "   ausr_desc,          " + "   test_flag,          "
					+ "   notify_flag,        " + "   token_flag,         "
					+ "   email,              " + "   enabled_flag )      "
					+ " values                "
					+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
			*/
			int colNum = 1;
			ps.setLong(colNum++, ausUser.getAusrId());
			ps.setTimestamp(colNum++, ausUser.getCreateTs());
			ps.setString(colNum++, ausUser.getUserName());
			ps.setString(colNum++, ausUser.getAusrName());
			ps.setString(colNum++, ausUser.getAusrDesc());
			ps.setString(colNum++, ausUser.getTestFlag());
			ps.setString(colNum++, ausUser.getNotifyFlag());
			ps.setString(colNum++, ausUser.getTokenFlag());
			ps.setString(colNum++, ausUser.getEmail());
			ps.setString(colNum++, ausUser.getEnabledFlag());
			ps.executeUpdate();
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	/**
	 * Create new AusUser populated with data in given result set
	 */
	private AusUser createAusUser(ResultSet rs) throws Exception {
		AusUser user = new AusUser();
		user.setAusrId(rs.getLong("ausr_id"));
		user.setCreateTs(rs.getTimestamp("create_ts"));
		user.setUserName(rs.getString("user_name"));
		user.setAusrName(rs.getString("ausr_name"));
		user.setAusrDesc(rs.getString("ausr_desc"));
		user.setTestFlag(rs.getString("test_flag"));
		user.setEmail(rs.getString("email"));
		user.setEnabledFlag(rs.getString("enabled_flag"));
		user.setTokenFlag(rs.getString("token_flag"));
		user.setNotifyFlag(rs.getString("notify_flag"));
		return user;
	}
	/**
	 * Load a list with AusUsers matching a search term..
	 */
	public List lookupAusUsers(String searchTerm) {
		Statement s = null;
		ResultSet rs = null;

		try {
			connect(true);
			s = con.createStatement();
			String lowerSt = searchTerm != null ? searchTerm.toLowerCase() : null;
			rs = s.executeQuery(" select * from aus_users "
					+ (lowerSt != null
							? " where                                 "
									+ "  ( ( lower(ausr_name) like         "
									+ "        lower('%" + lowerSt + "%') )   "
									+ "    or ( lower(ausr_desc) like      "
									+ "        lower('%" + lowerSt + "%') ) ) "
							: ""));

			List users = new ArrayList();
			while (rs.next()) {
				users.add(createAusUser(rs));
			}
			return users;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Load an AusUser matching ausrId.
	 */
	public AusUser getAusUser(long ausrId) {
		Statement s = null;
		ResultSet rs = null;

		try {
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_users where ausr_id = " + ausrId);
			if (rs.next()) {
				return createAusUser(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public AusUser getAusUser(String ausrName) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_users where ausr_name = '" + ausrName + "'");
			if (rs.next()) {
				return createAusUser(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Update existing aus user record.
	 */
	public boolean updateAusUser(AusUser ausUser) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "update aus_users set ausr_desc=?, test_flag=?, email=?, notify_flag=?, token_flag=?, enabled_flag=? where ausr_id=? ";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" update aus_users set  "
					+ "  ausr_desc    = ?,    " + "  test_flag    = ?,    "
					+ "  email        = ?,    " + "  notify_flag  = ?,    "
					+ "  token_flag   = ?,    " + "  enabled_flag = ?     "
					+ " where                 " + "  ausr_id = ?          ");
			*/
			int colNum = 1;
			ps.setString(colNum++, ausUser.getAusrDesc());
			ps.setString(colNum++, ausUser.getTestFlag());
			ps.setString(colNum++, ausUser.getEmail());
			ps.setString(colNum++, ausUser.getNotifyFlag());
			ps.setString(colNum++, ausUser.getTokenFlag());
			ps.setString(colNum++, ausUser.getEnabledFlag());
			ps.setLong(colNum++, ausUser.getAusrId());
			ps.executeUpdate();
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	/**
	 * Delete aus user record.
	 */
	public boolean deleteAusUser(long ausrId) {
		Statement s = null;
		try {
			connect(true);
			s = con.createStatement();
			int rowCount = s.executeUpdate("delete from aus_users where ausr_id = " + ausrId);
			return rowCount > 0;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	private ProfileRef createProfileRef(ResultSet rs) throws Exception {
		ProfileRef ref = new ProfileRef();
		ref.setTerminalId(rs.getString("terminal_id"));
		ref.setMerchNum(rs.getString("merch_num"));
		ref.setMerchName(rs.getString("merch_name"));
		ref.setProfileId(rs.getString("profile_id"));
		ref.setAusFlag(rs.getString("aus_flag"));
		return ref;
	}
	/**
	 * Fetches the profile reference matching the given profile ID.
	 */
	public ProfileRef getProfileRef(String profileId) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select tp.terminal_id terminal_id, tp.merchant_number merch_num, tp.merchant_name merch_name, p.profile_id profile_id, nvl2(p.profile_id,'y','n') aus_flag from trident_profile tp, aus_profiles p where tp.terminal_id = '" + profileId + "'";
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select                                    "
					+ "  tp.terminal_id terminal_id,              "
					+ "  tp.merchant_number merch_num,            "
					+ "  tp.merchant_name merch_name,             "
					+ "  p.profile_id profile_id,                 "
					+ "  nvl2(p.profile_id,'y','n') aus_flag      "
					+ " from                                      "
					+ "  trident_profile tp,                      "
					+ "  aus_profiles p                           "
					+ " where                                     "
					+ "  tp.terminal_id = '" + profileId + "'     ");
			*/		
			if (rs.next()) {
				return createProfileRef(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}

	/**
	 * Load a list of profile refs. Currently AUS supports one profile record
	 * per merchant. MES profiles found that already have an AUS profile with
	 * the same merchant number will not be allowed to be created in the AUS
	 * profile table.
	 */
	public List lookupProfileRefs(String searchTerm) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String lowerSt = searchTerm.toLowerCase();
			String sqlQuery = "select tp.terminal_id terminal_id, tp.merchant_number merch_num, tp.merchant_name merch_name, p.profile_id profile_id, "
					+ "nvl2(p.profile_id,'y','n') aus_flag from trident_profile tp, aus_profiles p where (lower(tp.merchant_name) like '%" + lowerSt + "%' "
					+ "or lower(tp.terminal_id) like '%" + lowerSt + "%') and tp.merchant_number = p.merch_num(+) ";
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select                                    "
					+ "  tp.terminal_id terminal_id,              "
					+ "  tp.merchant_number merch_num,            "
					+ "  tp.merchant_name merch_name,             "
					+ "  p.profile_id profile_id,                 "
					+ "  nvl2(p.profile_id,'y','n') aus_flag      "
					+ " from                                      "
					+ "  trident_profile tp,                      "
					+ "  aus_profiles p                           "
					+ " where                                     "
					+ "  ( lower(tp.merchant_name)                "
					+ "     like '%" + lowerSt + "%'              "
					+ "    or lower(tp.terminal_id)               "
					+ "     like '%" + lowerSt + "%' )            "
					+ "  and tp.merchant_number = p.merch_num(+)  ");
			*/
			List refs = new ArrayList();
			while (rs.next()) {
				refs.add(createProfileRef(rs));
			}
			return refs;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Insert new AUS profile record.
	 */
	public boolean insertAusProfile(AusProfile profile) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "insert into aus_profiles (profile_id,merch_num,user_name,vau_file_type,abu_file_type,dau_file_type,auto_flag,auto_frequency,auto_day,auto_ausr_id,auto_last_ts,auto_test_flag) values (?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_profiles  "
					+ " ( profile_id,             "
					+ "   merch_num,              "
					+ "   user_name,              "
					+ "   vau_file_type,          "
					+ "   abu_file_type,          "
					+ "   dau_file_type,          "
					+ "   auto_flag,              "
					+ "   auto_frequency,         "
					+ "   auto_day,               "
					+ "   auto_ausr_id,           "
					+ "   auto_last_ts,           "
					+ "   auto_test_flag )        "
					+ " values                    "
					+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
			*/
			int colNum = 1;
			ps.setString(colNum++, profile.getProfileId());
			ps.setString(colNum++, profile.getMerchNum());
			ps.setString(colNum++, profile.getUserName());
			ps.setString(colNum++, profile.getVauFileType());
			ps.setString(colNum++, profile.getAbuFileType());
			ps.setString(colNum++, profile.getDauFileType());
			ps.setString(colNum++, profile.getAutoFlag());
			ps.setString(colNum++, profile.getAutoFrequency());
			ps.setInt(colNum++, profile.getAutoDay());
			ps.setLong(colNum++, profile.getAutoAusrId());
			ps.setTimestamp(colNum++, profile.getAutoLastTs());
			ps.setString(colNum++, profile.getAutoTestFlag());
			ps.executeUpdate();
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	/**
	 * Create new AusProfile from current row of result set.
	 */
	private AusProfile createAusProfile(ResultSet rs) throws Exception {
		AusProfile profile = new AusProfile();
		profile.setProfileId(rs.getString("profile_id"));
		profile.setMerchNum(rs.getString("merch_num"));
		profile.setCreateTs(rs.getTimestamp("create_ts"));
		profile.setUserName(rs.getString("user_name"));
		profile.setMerchName(rs.getString("merch_name"));
		profile.setVauFileType(rs.getString("vau_file_type"));
		profile.setAbuFileType(rs.getString("abu_file_type"));
		profile.setDauFileType(rs.getString("dau_file_type"));
		profile.setAutoFlag(rs.getString("auto_flag"));
		profile.setAutoFrequency(rs.getString("auto_frequency"));
		profile.setAutoDay(rs.getInt("auto_day"));
		profile.setAutoAusrId(rs.getLong("auto_ausr_id"));
		profile.setAutoAusrName(rs.getString("auto_ausr_name"));
		profile.setAutoLastTs(rs.getTimestamp("auto_last_ts"));
		profile.setAutoTestFlag(rs.getString("auto_test_flag"));
		return profile;
	}
	/**
	 * Load a list with AusProfiles matching a search term.
	 */
	public List lookupAusProfiles(String searchTerm) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String lowerSt = searchTerm != null ? searchTerm.toLowerCase() : null;
			rs = s.executeQuery(" select p.*, au.ausr_name auto_ausr_name, mif.dba_name merch_name from aus_profiles p, mif, aus_users au "
					+ " where p.merch_num = mif.merchant_number and p.auto_ausr_id = au.ausr_id(+) "
					+ (lowerSt != null ? " and (lower(mif.dba_name) like '%" + lowerSt + "%' or p.profile_id like '%" + lowerSt + "%')" : ""));
			List profiles = new ArrayList();
			while (rs.next()) {
				profiles.add(createAusProfile(rs));
			}
			return profiles;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Load AusProfile matching profileId.
	 */
	public AusProfile getAusProfile(String profileId) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select p.*, au.ausr_name auto_ausr_name, mif.dba_name merch_name from aus_profiles p, mif, aus_users au where p.auto_ausr_id = au.ausr_id (+) and to_number(p.merch_num) = mif.merchant_number and p.profile_id = '" + profileId + "'";
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select p.*, au.ausr_name auto_ausr_name, mif.dba_name merch_name "
					+ " from aus_profiles p, mif, aus_users au "
					+ " where p.profile_id = '"
					+ profileId
					+ "' "
					+ "  and p.auto_ausr_id = au.ausr_id (+) "
					+ "  and to_number(p.merch_num) = mif.merchant_number ");
			*/
			if (rs.next()) {
				return createAusProfile(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Update existing AUS profile.
	 */
	public boolean updateAusProfile(AusProfile profile) {
		PreparedStatement ps = null;
		try {
			connect(true);
			String sqlQuery = "update aus_profiles set vau_file_type=?,abu_file_type=?,dau_file_type=?,auto_flag=?,auto_frequency=?,auto_day=?,auto_ausr_id=?,auto_last_ts=?,auto_test_flag=? where profile_id=? ";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" update aus_profiles set "
					+ "  vau_file_type  = ?, " + "  abu_file_type  = ?, "
					+ "  dau_file_type  = ?, " + "  auto_flag      = ?, "
					+ "  auto_frequency = ?, " + "  auto_day       = ?, "
					+ "  auto_ausr_id   = ?, " + "  auto_last_ts   = ?, "
					+ "  auto_test_flag = ?  " + " where "
					+ "  profile_id = ? ");
			*/
			int colNum = 1;
			ps.setString(colNum++, profile.getVauFileType());
			ps.setString(colNum++, profile.getAbuFileType());
			ps.setString(colNum++, profile.getDauFileType());
			ps.setString(colNum++, profile.getAutoFlag());
			ps.setString(colNum++, profile.getAutoFrequency());
			ps.setInt(colNum++, profile.getAutoDay());
			ps.setLong(colNum++, profile.getAutoAusrId());
			ps.setTimestamp(colNum++, profile.getAutoLastTs());
			ps.setString(colNum++, profile.getAutoTestFlag());
			ps.setString(colNum++, profile.getProfileId());
			ps.executeUpdate();
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				ps.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	/**
	 * Delete AUS profile.
	 */
	public boolean deleteAusProfile(String profileId) {
		Statement s = null;
		try {
			connect(true);
			s = con.createStatement();
			int rowCount = s.executeUpdate("delete from aus_profiles where profile_id = '" + profileId + "'");
			return rowCount > 0;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	/**
	 * Create a merch ref object from result set row.
	 */
	private MerchRef createMerchRef(ResultSet rs) throws Exception {
		MerchRef ref = new MerchRef();
		ref.setAusrId(rs.getLong("ausr_id"));
		ref.setMerchNum(rs.getString("merch_num"));
		ref.setMerchName(rs.getString("merch_name"));
		ref.setLinkedFlag(rs.getString("linked_flag"));
		return ref;
	}
	/**
	 * Load a list of merchant refs. Takes an ausr id to determine if a link
	 * already exists for the user.
	 */
	public List lookupMerchRefs(long ausrId, String searchTerm) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String lowerSt = searchTerm.toLowerCase();
			String sqlQuery = "select " + ausrId + " ausr_id, mif.merchant_number merch_num, mif.dba_name merch_name, nvl2(ml.ausr_id,'y','n') linked_flag "
					+ "from mif, (select * from aus_merch_links where ausr_id = " + ausrId + ") ml where ( lower(to_char(mif.merchant_number)) like '%" + lowerSt + "%' or lower(mif.dba_name) like '%" + lowerSt + "%' ) and mif.merchant_number = ml.merch_num(+) ";
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select                                      "
					+ "  " + ausrId + "           ausr_id,          "
					+ "  mif.merchant_number      merch_num,        "
					+ "  mif.dba_name             merch_name,       "
					+ "  nvl2(ml.ausr_id,'y','n') linked_flag       "
					+ " from                                        "
					+ "  mif,                                       "
					+ "  ( select * from aus_merch_links            "
					+ "    where ausr_id = " + ausrId + " ) ml      "
					+ " where                                       "
					+ "  ( lower(to_char(mif.merchant_number))      "
					+ "      like '%" + lowerSt + "%'               "
					+ "     or lower(mif.dba_name)                  "
					+ "      like '%" + lowerSt + "%' )             "
					+ "  and mif.merchant_number = ml.merch_num(+)  ");
			*/
			List refs = new ArrayList();
			while (rs.next()) {
				refs.add(createMerchRef(rs));
			}
			return refs;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Create new MerchLink populated with data in given result set
	 */
	private MerchLink createMerchLink(ResultSet rs) throws Exception {
		MerchLink link = new MerchLink();
		link.setLinkId(rs.getLong("link_id"));
		link.setAusrId(rs.getLong("ausr_id"));
		link.setAusrName(rs.getString("ausr_name"));
		link.setMerchNum(rs.getString("merch_num"));
		link.setMerchName(rs.getString("merch_name"));
		return link;
	}
	/**
	 * Load link between ausrId and merchNum.
	 */
	public MerchLink getMerchLinkForUser(long ausrId, String merchNum) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select link_id,ausr_id,ausr_name,merch_num,merch_name from aus_merch_links where merch_num='" + merchNum + "'" + " and ausr_id=" + ausrId;
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select            " + "  link_id,         "
					+ "  ausr_id,         " + "  ausr_name,       "
					+ "  merch_num,       " + "  merch_name       "
					+ " from              " + "  aus_merch_links  "
					+ " where             " + "  merch_num = '" + merchNum
					+ "'" + "  and ausr_id = " + ausrId);
			*/
			if (rs.next()) {
				return createMerchLink(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Load link between ausrName and merchNum.
	 */
	public MerchLink getMerchLinkForUser(String ausrName, String merchNum) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);
			s = con.createStatement();
			String sqlQuery = "select link_id,ausr_id,ausr_name,merch_num,merch_name from aus_merch_links where merch_num='" + merchNum + "' and ausr_name='" + ausrName + "'";
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select            " + "  link_id,         "
					+ "  ausr_id,         " + "  ausr_name,       "
					+ "  merch_num,       " + "  merch_name       "
					+ " from              " + "  aus_merch_links  "
					+ " where             " + "  merch_num = '" + merchNum
					+ "'" + "  and ausr_name = '" + ausrName + "'");
			*/
			if (rs.next()) {
				return createMerchLink(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Create a new merch ref record and returns it.
	 */
	public MerchLink linkUserToMerch(long ausrId, String merchNum) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connect(true);
			// make sure merch number has not already been linked to this aus user
			if (getMerchLinkForUser(ausrId, merchNum) != null) {
				throw new RuntimeException("Merchant number '" + merchNum + "' already exists for aus user " + ausrId);
			}

			// make sure merch number is valid
			ps = con.prepareStatement(" select dba_name from mif where merchant_number = " + merchNum);
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new RuntimeException("Invalid merchant number '" + merchNum + "'");
			}
			String merchName = rs.getString("dba_name");
			rs.close();
			ps.close();

			// fetch aus user
			ps = con.prepareStatement(" select ausr_name from aus_users where ausr_id = " + ausrId);
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new RuntimeException("Invalid AUS user id " + ausrId);
			}
			String ausrName = rs.getString("ausr_name");
			rs.close();
			ps.close();

			// create a new merch link, insert it into db, return it
			MerchLink link = new MerchLink();
			link.setLinkId(getNewId());
			link.setAusrId(ausrId);
			link.setAusrName(ausrName);
			link.setMerchNum(merchNum);
			link.setMerchName(merchName);
			ps = con.prepareStatement("insert into aus_merch_links (link_id,ausr_id,ausr_name,merch_num,merch_name) values (?,?,?,?,?)");
			ps.setLong(1, link.getLinkId());
			ps.setLong(2, link.getAusrId());
			ps.setString(3, link.getAusrName());
			ps.setString(4, link.getMerchNum());
			ps.setString(5, link.getMerchName());
			ps.executeUpdate();
			return link;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
			throw e;
		}
		finally {
			cleanUp(ps, rs);
		}
	}
	/**
	 * Remove record linking AUS user to merchant number.
	 */
	public boolean unlinkUserFromMerch(long ausrId, String merchNum) {
		Statement s = null;
		try {
			connect(true);
			s = con.createStatement();
			int rowCount = s.executeUpdate("delete from aus_merch_links where ausr_id=" + ausrId + " and merch_num='" + merchNum + "'");
			return (rowCount == 1);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	/**
	 * Load a list of merch refs matching an ausrId.
	 */
	public List getAllMerchLinksForUser(long ausrId, String ausrName) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   
			s = con.createStatement();
			rs = s.executeQuery("select link_id,merch_num,merch_name,ausr_id,ausr_name from aus_merch_links where "
					+ (ausrName != null ? " ausr_name = '" + ausrName + "'" : " ausr_id = " + ausrId)
					+ " order by merch_num, lower(merch_name) ");
			List links = new ArrayList();
			while (rs.next()) {
				links.add(createMerchLink(rs));
			}
			return links;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getAllMerchLinksForUser(long ausrId) {
		return getAllMerchLinksForUser(ausrId, null);
	}
	public List getAllMerchLinksForUser(String ausrName) {
		return getAllMerchLinksForUser(0, ausrName);
	}
	public List getAllMerchLinksForMerchNum(String merchNum) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   
			s = con.createStatement();
			rs = s.executeQuery("select link_id,merch_num,merch_name,ausr_id,ausr_name from aus_merch_links where merch_num = '" + merchNum + "'" + " order by merch_num, lower(merch_name) ");
			List links = new ArrayList();
			while (rs.next()) {
				links.add(createMerchLink(rs));
			}
			return links;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Returns the first available merch link associated with a merch num. Used
	 * to assign default auto ausr id during profile creation.
	 */
	public MerchLink getDefaultMerchLinkForMerchNum(String merchNum) {
		List links = getAllMerchLinksForMerchNum(merchNum);
		if (!links.isEmpty()) {
			return (MerchLink) links.get(0);
		}
		return null;
	}
	public DropDownItem[] getAusrMerchNumDdItems(String ausrName) {
		List links = getAllMerchLinksForUser(0L, ausrName);
		List ddList = new ArrayList();
		for (Iterator i = links.iterator(); i.hasNext();) {
			MerchLink link = (MerchLink) i.next();
			String value = link.getMerchNum();
			String desc = link.getMerchNum() + " - " + link.getMerchName();
			ddList.add(new DropDownItem(value, desc));
		}
		return (DropDownItem[]) ddList.toArray(new DropDownItem[]{});
	}
	public DropDownItem[] getAusrNameDdItems() {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   
			s = con.createStatement();
			rs = s.executeQuery(" select ausr_name, ausr_desc from aus_users order by lower(ausr_desc)  ");
			List ddList = new ArrayList();
			while (rs.next()) {
				ddList.add(new DropDownItem(rs.getString("ausr_name"), rs.getString("ausr_desc")));
			}
			return (DropDownItem[]) ddList.toArray(new DropDownItem[]{});
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Insert new RequestRecord record into db.
	 */
	public boolean insertRequestRecords(List requests) {
		PreparedStatement ps = null;
		RequestRecord request = null;
		int batchNum = 0;
		int recordNum = 0;
		long startTime = System.currentTimeMillis();
		try {
			log.debug("insertRequestRecords - startTime: " + startTime);
			connect(true);   
			String sqlQuery = "insert into aus_requests (req_id,reqf_id,create_ts,merch_num,account_type,account_num,account_num_enc,exp_date,disc_data,card_id) values (aus_id_sequence.nextval,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement(sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_requests    "
					+ " ( req_id,                   "
					+ "   reqf_id,                  "
					+ "   create_ts,                "
					+ "   merch_num,                "
					+ "   account_type,             "
					+ "   account_num,              "
					+ "   account_num_enc,          "
					+ "   exp_date,                 "
					+ "   disc_data,                "
					+ "   card_id )                 "
					+ " values                      "
					+ " ( aus_id_sequence.nextval,  "
					+ "   ?, ?, ?, ?,               "
					+ "   ?, ?, ?, ?, ? )           ");
			*/
			
			// add responses in batches of up to 500
			for (Iterator i = requests.iterator(); i.hasNext();) {
				recordNum = 0;
				// ps.clearBatch();
				while (i.hasNext() && recordNum < 1000) {
					request = (RequestRecord) i.next();

					// TODO: any way to reduce this to one statement?
					// if (request.getReqId() <= 0)
					// {
					// request.setReqId(getNewId());
					// }

					int colNum = 1;
					// ps.setLong (colNum++,request.getReqId());
					ps.setLong(colNum++, request.getReqfId());
					ps.setTimestamp(colNum++, request.getCreateTs());
					ps.setString(colNum++, request.getMerchNum());
					ps.setString(colNum++, request.getAccountType());
					ps.setString(colNum++, request.getAccountNumTrunc());
					ps.setBytes(colNum++, request.getAccountNumEnc());
					ps.setString(colNum++, request.getExpDate());
					ps.setString(colNum++, request.getDiscData());
					ps.setString(colNum++, request.getCardId());
					ps.addBatch();
					++recordNum;
				}
				ps.executeBatch();
				++batchNum;
			}
			log.debug("insertRequestRecords - duration: " + (System.currentTimeMillis() - startTime));
			return true;
		}
		catch (Exception e) {
			Notifier.notifyDeveloper("AusDb.insertRequestRecords", "Error: " + e + "\nBatch " + batchNum + ", record " + recordNum + "\nRequest: " + request, directFlag);
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	public boolean insertRequestRecord(RequestRecord request) {
		List requests = new ArrayList();
		requests.add(request);
		return insertRequestRecords(requests);
	}
	public boolean purgeRequestRecords(long reqfId) {
		Statement s = null;
		long startTime = System.currentTimeMillis();	
		try {
			log.debug("purgeRequestRecords - startTime: " + startTime);
			connect(true);   //CALVIN
			s = con.createStatement();
			int rowCount = s.executeUpdate(" delete from aus_requests where reqf_id = " + reqfId);
			log.debug("purgeRequestRecords - duration: " + (System.currentTimeMillis() - startTime));			
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	private RequestRecord createRequestRecord(ResultSet rs) throws Exception {
		//log.debug("createRequestRecord - startTime: " + System.currentTimeMillis());
		
		RequestRecord request = new RequestRecord();
		request.setReqId(rs.getLong("req_id"));
		request.setReqfId(rs.getLong("reqf_id"));
		request.setCreateTs(rs.getTimestamp("create_ts"));
		request.setMerchNum(rs.getString("merch_num"));
		request.setAccountType(rs.getString("account_type"));
		request.setAccountNum(rs.getString("account_num"));
		request.setExpDate(rs.getString("exp_date"));
		request.setDiscData(rs.getString("disc_data"));
		request.setCardId(rs.getString("card_id"));

		//log.debug("createRequestRecord - endTime: " + System.currentTimeMillis());
		return request;
	}
	/**
	 * Retrieve request records associated with the reqfId.
	 */
	public List getRequests(long reqfId) {
		Statement s = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();

		try {
			log.debug("getRequests - startTime: " + startTime);
			connect(true); // timeout exempt due to slow query
			s = con.createStatement();
			String sqlQuery = "select req_id, reqf_id, create_ts, merch_num, account_type, dukpt_decrypt_wrapper(account_num_enc) account_num, exp_date, disc_data, card_id from aus_requests where reqf_id = " + reqfId;
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select "
					+ "  req_id, reqf_id, create_ts, merch_num, account_type, "
					+ "  dukpt_decrypt_wrapper(account_num_enc) account_num, "
					+ "  exp_date, disc_data, card_id "
					+ " from aus_requests where reqf_id = " + reqfId);
			*/
			List requests = new ArrayList();
			while (rs.next()) {
				requests.add(createRequestRecord(rs));
			}
			log.debug("getRequests - duration: " + (System.currentTimeMillis() - startTime));
			return requests;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Fetches V/MC requests associated with given file, calls the handler.
	 */
	private void handleVmcRequests(RequestRecordHandler handler, RequestFile reqf) throws Exception {
		Statement s = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		
		try {
			log.debug("handleVmcRequests for reqf_id: " + reqf.getReqfId() + " - startTime: " + startTime);
			
			connect(true); // timeout exempt due to slow query
			s = con.createStatement();
			String queryStmt = "select req_id, reqf_id, create_ts, merch_num, account_type, dukpt_decrypt_wrapper(account_num_enc) account_num, exp_date, disc_data, card_id from aus_requests where account_type in ('VISA', 'MC') and reqf_id = " + reqf.getReqfId();
			
			log.debug("Query = " + queryStmt);
			
			rs = s.executeQuery(queryStmt);		
			
			log.debug("Query returned.  Start the outbound file generation loop.  Starttime: " + System.currentTimeMillis());
			
			/*
			rs = s.executeQuery(" select "
					+ "  req_id, reqf_id, create_ts, merch_num, account_type, "
					+ "  dukpt_decrypt_wrapper(account_num_enc) account_num, "
					+ "  exp_date, disc_data, card_id "
					+ " from aus_requests where reqf_id = " + reqf.getReqfId()
					+ "  and account_type in ( 'VISA', 'MC' ) ");
			*/
			List requests = new ArrayList();
			while (rs.next()) {
				//log.debug("Start Loop time: " + System.currentTimeMillis());
				
				requests.add(createRequestRecord(rs));
				if (requests.size() >= 100) {
					handler.handleRequests(reqf, requests);
					requests.clear();
				}
				
				//log.debug("End Loop time: " + System.currentTimeMillis());
			}
			log.debug("Outbound file generation loop.  Endtime: " + System.currentTimeMillis());
			
			log.debug("handleVmcRequests - duration: " + (System.currentTimeMillis() - startTime));
			
			if (!requests.isEmpty()) {
				handler.handleRequests(reqf, requests);
			}
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	/**
	 * Fetches discover requests for the given file, calls the handler.
	 */
	private void handleDiscRequests(RequestRecordHandler handler, RequestFile reqf) throws Exception {
		Statement s = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		try {
			log.debug("handleDiscRequests - startTime: " + startTime);
			connect(true); // timeout exempt due to slow query
			s = con.createStatement();
			String sqlQuery = "select req_id, reqf_id, create_ts, merch_num, account_type, dukpt_decrypt_wrapper(account_num_enc) account_num, exp_date, disc_data, card_id from aus_requests where account_type = 'DISC' and reqf_id = " + reqf.getReqfId();
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select "
					+ "  req_id, reqf_id, create_ts, merch_num, account_type, "
					+ "  dukpt_decrypt_wrapper(account_num_enc) account_num, "
					+ "  exp_date, disc_data, card_id "
					+ " from aus_requests where reqf_id = " + reqf.getReqfId()
					+ "  and account_type = 'DISC' ");
			*/
			List requests = new ArrayList();
			while (rs.next()) {
				requests.add(createRequestRecord(rs));
				if (requests.size() >= 100) {
					handler.handleRequests(reqf, requests);
					requests.clear();
				}
			}
			
			log.debug("handleDiscRequests - duration: " + (System.currentTimeMillis() - startTime));
			
			if (!requests.isEmpty()) {
				handler.handleRequests(reqf, requests);
			}
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	/**
	 * Fetch batches of requests for a request file, pass them to a
	 * RequestRecordHandler object for processing.
	 */
	public void handleRequests(RequestRecordHandler handler, RequestFile reqf, String sysType) throws Exception {
		if (SysUtil.SYS_VAU.equals(sysType) || SysUtil.SYS_ABU.equals(sysType)) {
			handleVmcRequests(handler, reqf);
		}
		else if (SysUtil.SYS_DAU.equals(sysType)) {
			handleDiscRequests(handler, reqf);
		}
		else {
			throw new RuntimeException("Invalid system type: " + sysType);
		}
	}
	/**
	 * Request file to outbound file mapping. Mappings should almost always be
	 * inserted as a list associated with a new outbound file. This assumes
	 * connection already established.
	 */
	private void _insertRfObMappings(List mapList) throws Exception {
		PreparedStatement ps = null;
		try {
			String sqlQuery = "insert into aus_out_rf (reqf_id, ob_id, sys_code) values (?, ?, ?)";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_out_rf      "
					+ " ( reqf_id, ob_id, sys_code )  "
					+ " values                      "
					+ " ( ?, ?, ? )                 ");
			*/
			Iterator i = mapList.iterator();
			while (i.hasNext()) {
				// insert in batches of 100
				for (int x = 0; i.hasNext() && x < 100; ++x) {
					RfObMapping map = (RfObMapping) i.next();
					int colNum = 1;
					ps.setLong(colNum++, map.getReqfId());
					ps.setLong(colNum++, map.getObId());
					ps.setString(colNum++, map.getSysCode());
					ps.addBatch();
				}
				ps.executeBatch();
			}
		}
		finally {
			try {
				ps.close();
			}
			catch (Exception e) {}
		}
	}
	/**
	 * OutboundFile
	 *
	 * Inserting outbound file also involves inserting all rf to outbound file
	 * mappings.
	 */
	public boolean insertOutboundFile(OutboundFile obf, OutboundFileType obft, boolean dataFlag) {
		PreparedStatement ps = null;
		boolean acSetting = getAutoCommit();
		try {
			connect(true);   //CALVIN
			setAutoCommit(false);
			String sqlQuery = "insert into aus_outbound_files (ob_id,create_ts,sys_code,file_seq_id,file_name,file_type,test_flag,delivery_flag) values (?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement(sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_outbound_files  "
					+ " ( ob_id,                        "
					+ "   create_ts,                    "
					+ "   sys_code,                     "
					+ "   file_seq_id,                  "
					+ "   file_name,                    "
					+ "   file_type,                    "
					+ "   test_flag,                    "
					+ "   delivery_flag )               "
					+ " values                          "
					+ " ( ?, ?, ?, ?, ?, ?, ?, ? )      ");
			*/
			int colNum = 1;
			ps.setLong(colNum++, obf.getObId());
			ps.setTimestamp(colNum++, obf.getCreateTs());
			ps.setString(colNum++, obf.getSysCode());
			ps.setLong(colNum++, obf.getFileSeqId());
			ps.setString(colNum++, obf.getFileName());
			ps.setString(colNum++, obf.getFileType());
			ps.setString(colNum++, obf.getTestFlag());
			ps.setString(colNum++, obf.getDeliveryFlag());
			ps.executeUpdate();

			// insert done if data flag is false or there is no contained data
			boolean insertOk = !dataFlag || obf.empty();

			// if insert not done, attempt to upload contained data to the
			// record's blob column
			if (!insertOk) {
				insertOk = uploadAusFileData(new AusFileRowId("aus_outbound_files", "ob_id", obf.getObId(), obf));
			}

			// if no problems inserting, finish transaction
			if (insertOk) {
				// generate rf to obf mappings
				_insertRfObMappings(obf.getMapList());

				// increment appropriate system seq num
				if (SysUtil.SYS_VAU.equals(obf.getSysCode())) {
					incrementVauFileSeqNum(obft);
				}
				else if (SysUtil.SYS_ABU.equals(obf.getSysCode())) {
					incrementAbuFileSeqNum(obf.isTest());
				}
				else if (SysUtil.SYS_DAU.equals(obf.getSysCode())) {
					incrementDauFileSeqNum(obf.isTest());
				}
				commit();
			}
			else {
				throw new RuntimeException("Outbound file insert failed");
			}
			return insertOk;
		}
		catch (Exception e) {
			rollback();
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			setAutoCommit(acSetting);
			cleanUp(ps);
		}
		return false;
	}
	private RfObMapping createRfObMapping(ResultSet rs) throws Exception {
		RfObMapping map = new RfObMapping();
		map.setReqfId(rs.getLong("reqf_id"));
		map.setObId(rs.getLong("ob_id"));
		map.setSysCode(rs.getString("sys_code"));
		return map;
	}
	private OutboundFile createOutboundFile(ResultSet rs, boolean dataFlag) throws Exception {
		OutboundFile obf = new OutboundFile();
		obf.setObId(rs.getLong("ob_id"));
		obf.setCreateTs(rs.getTimestamp("create_ts"));
		obf.setSysCode(rs.getString("sys_code"));
		obf.setFileSeqId(rs.getLong("file_seq_id"));
		obf.setFileName(rs.getString("file_name"));
		obf.setFileType(rs.getString("file_type"));
		obf.setDeliveryFlag(rs.getString("delivery_flag"));
		obf.setTestFlag(rs.getString("test_flag"));
		if (dataFlag) {
			setFileContainerData(obf, rs, "file_data");
		}
		Statement s = null;
		ResultSet rs2 = null;
		try {
			s = con.createStatement();
			rs2 = s.executeQuery(" select reqf_id, ob_id, sys_code " + " from aus_out_rf where ob_id = " + obf.getObId());
			while (rs2.next()) {
				obf.addRfMapping(createRfObMapping(rs2));
			}
		}
		finally {
			try {
				rs2.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
		}
		return obf;
	}
	public OutboundFile getOutboundFile(long obId, boolean dataFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_outbound_files where ob_id = " + obId);
			if (rs.next()) {
				return createOutboundFile(rs, dataFlag);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Gets lists or a single instance of OutboundFile based on parameters. If
	 * first only flag turned on, will return the first found record, null if
	 * none found. Otherwise a list is returned, which may be empty.
	 */
	private Object getOutboundFiles(String sysCode, String testFlag, String deliveryFlag, boolean firstOnly, boolean dataFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_outbound_files where 1 = 1 "
					+ (sysCode != null ? " and sys_code = '" + sysCode + "' " : "")
					+ (testFlag != null ? " and test_flag = '" + testFlag + "' " : "")
					+ (deliveryFlag != null ? " and delivery_flag = '" + deliveryFlag + "' " : "")
					+ (firstOnly ? " and rownum = 1 " : "")
					+ " and file_size is not null ");
			List obfList = new ArrayList();
			while (rs.next()) {
				OutboundFile obf = createOutboundFile(rs, dataFlag);
				if (firstOnly) {
					return obf;
				}
				obfList.add(obf);
			}
			// if first only flag turned on then must've been no results, so
			// return null
			// else return the list
			return (firstOnly ? null : obfList);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Get an outbound file for the given system that needs to be transferred.
	 * This returns the first available outbound file record that is not flagged
	 * as test.
	 */
	public OutboundFile getXferOutboundFile(String sysCode) {
		return (OutboundFile) getOutboundFiles(sysCode, "n", "n", true, true);
	}
	/**
	 * Get an outbound file for the given system that has not been delivered and
	 * is flagged as test.
	 */
	public OutboundFile getSimOutboundFile(String sysCode) {
		return (OutboundFile) getOutboundFiles(sysCode, "y", "n", true, true);
	}
	public boolean setOutboundFileDeliveryFlag(long obId, boolean flagValue) {
		Statement s = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			s.executeUpdate(" update aus_outbound_files set delivery_flag = '" + (flagValue ? "y" : "n") + "' where ob_id = " + obId);
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return false;
	}
	/**
	 * InboundFile
	 */
	public boolean insertInboundFile(InboundFile ibf, boolean dataFlag) {
		PreparedStatement ps = null;
		boolean acSetting = getAutoCommit();
		try {
			connect(true);   //CALVIN
			setAutoCommit(false);
			String sqlQuery = "insert into aus_inbound_files (ib_id,create_ts,sys_code,file_type,file_name,file_size,process_flag,test_flag,ob_id) values(?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_inbound_files   "
					+ " ( ib_id,                        "
					+ "   create_ts,                    "
					+ "   sys_code,                     "
					+ "   file_type,                    "
					+ "   file_name,                    "
					+ "   file_size,                    "
					+ "   process_flag,                 "
					+ "   test_flag,                    "
					+ "   ob_id )                       "
					+ " values                          "
					+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ? )   ");
			*/
			int colNum = 1;
			ps.setLong(colNum++, ibf.getIbId());
			ps.setTimestamp(colNum++, ibf.getCreateTs());
			ps.setString(colNum++, ibf.getSysCode());
			ps.setString(colNum++, ibf.getFileType());
			ps.setString(colNum++, ibf.getFileName());
			ps.setLong(colNum++, ibf.size());
			ps.setString(colNum++, ibf.getProcessFlag());
			ps.setString(colNum++, ibf.getTestFlag());
			ps.setLong(colNum++, ibf.getObId());
			ps.executeUpdate();

			// insert done if data flag is false or there is no contained data
			boolean insertOk = !dataFlag || ibf.empty();

			// if insert not done, attempt to upload contained data to the
			// record's blob column
			if (!insertOk) {
				insertOk = uploadAusFileData(new AusFileRowId("aus_inbound_files", "ib_id", ibf.getIbId(), ibf));
			}

			// if no problems inserting, finish transaction
			if (insertOk) {
				commit();
			}
			else {
				rollback();
			}
			return insertOk;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			setAutoCommit(acSetting);
			cleanUp(ps);
		}
		return false;
	}
	private InboundFile createInboundFile(ResultSet rs, boolean dataFlag) throws Exception {
		InboundFile ibf = new InboundFile();
		ibf.setIbId(rs.getLong("ib_id"));
		ibf.setCreateTs(rs.getTimestamp("create_ts"));
		ibf.setObId(rs.getLong("ob_id"));
		ibf.setSysCode(rs.getString("sys_code"));
		ibf.setFileType(rs.getString("file_type"));
		ibf.setFileName(rs.getString("file_name"));
		ibf.setProcessFlag(rs.getString("process_flag"));
		ibf.setTestFlag(rs.getString("test_flag"));
		if (dataFlag) {
			setFileContainerData(ibf, rs, "file_data");
		}
		return ibf;
	}
	public InboundFile getInboundFile(long ibId, boolean dataFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_inbound_files where ib_id = " + ibId);
			if (rs.next()) {
				return createInboundFile(rs, dataFlag);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getUnprocessedInboundFileIds(String sysStr, boolean testFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			// create query string from sysStr
			List sysCodeList = SysUtil.getSysCodeList(sysStr);
			StringBuffer buf = new StringBuffer();
			buf.append(" select ib_id from aus_inbound_files where sys_code in ( ");
			for (Iterator i = sysCodeList.iterator(); i.hasNext();) {
				buf.append("'" + i.next() + "'" + (i.hasNext() ? ", " : ""));
			}
			buf.append(" )");
			buf.append(" and process_flag = 'n' ");
			buf.append(" and test_flag = '" + (testFlag ? "y" : "n") + "' ");
			// fetch ids
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery("" + buf);
			List idList = new ArrayList();
			while (rs.next()) {
				idList.add(rs.getLong("ib_id"));
			}
			return idList;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getUnprocessedInboundFileIds(String sysStr) {
		return getUnprocessedInboundFileIds(sysStr, false);
	}
	public void setInboundFileProcessFlag(InboundFile ibf, boolean isProcessed) {
		Statement s = null;
		try {
			connect(true);   //CALVIN
			// update the process_flag of an inbound file record
			s = con.createStatement();
			String sqlQuery = "update aus_inbound_files set process_flag = '" + (isProcessed ? "y" : "n") + "' where ib_id = " + ibf.getIbId();
			s.executeUpdate(sqlQuery);
			/*
			s.executeUpdate(" update aus_inbound_files "
					+ " set process_flag = '" + (isProcessed ? "y" : "n")
					+ "' " + " where ib_id = " + ibf.getIbId());
			*/
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	/**
	 * ResponseRecord
	 */
	public boolean insertResponseRecords(List responses) {
		PreparedStatement ps = null;
		long startTime = System.currentTimeMillis();
		
		try {
			log.debug("insertResponseRecords - startTime: " + startTime);
			connect(true);
			String sqlQuery = "insert into aus_responses (rsp_id,create_ts,rspf_id,ib_id,sys_code,ob_id,reqf_id,req_id,rsp_code,old_account_type,old_account_num,old_account_num_enc,old_exp_date,new_account_type,new_account_num,new_account_num_enc,new_exp_date,test_flag,card_id,merch_num,disc_data) "
					+ " select aus_id_sequence.nextval,sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, merch_num, disc_data from aus_requests where req_id = ?";
			ps = con.prepareStatement(sqlQuery);
			/*
			ps = con.prepareStatement(" insert into aus_responses "
					+ " ( rsp_id,                 "
					+ "   create_ts,              "
					+ "   rspf_id,                "
					+ "   ib_id,                  "
					+ "   sys_code,               "
					+ "   ob_id,                  "
					+ "   reqf_id,                "
					+ "   req_id,                 "
					+ "   rsp_code,               "
					+ "   old_account_type,       "
					+ "   old_account_num,        "
					+ "   old_account_num_enc,    "
					+ "   old_exp_date,           "
					+ "   new_account_type,       "
					+ "   new_account_num,        "
					+ "   new_account_num_enc,    "
					+ "   new_exp_date,           "
					+ "   test_flag,              "
					+ "   card_id,                "
					+ "   merch_num,              "
					+ "   disc_data )             "
					+ " select                    "
					+ "  aus_id_sequence.nextval, "
					+ "  sysdate,                 "
					+ "  ?, ?, ?, ?,              "
					+ "  ?, ?, ?, ?,              "
					+ "  ?, ?, ?, ?,              "
					+ "  ?, ?, ?, ?,              "
					+ "  ?,                       "
					+ "  merch_num,               "
					+ "  disc_data                "
					+ " from aus_requests         "
					+ " where req_id = ?          ");
			*/
			
			// add responses in batches of up to 100
			int rspCount = 0;
			long ibId = 0;
			for (Iterator i = responses.iterator(); i.hasNext();) {
				int batchCount = 0;
				ps.clearBatch();
				while (i.hasNext() && batchCount < 100) {
					ResponseRecord response = (ResponseRecord) i.next();
					int colNum = 1;
					ps.setLong(colNum++, response.getRspfId());
					ps.setLong(colNum++, response.getIbId());
					ps.setString(colNum++, response.getSysCode());
					ps.setLong(colNum++, response.getObId());
					ps.setLong(colNum++, response.getReqfId());
					ps.setLong(colNum++, response.getReqId());
					ps.setString(colNum++, response.getRspCode());
					ps.setString(colNum++, response.getOldAcctType());
					ps.setString(colNum++, response.getOldAcctNumTrunc());
					ps.setBytes(colNum++, response.getOldAcctNumEnc());
					ps.setString(colNum++, response.getOldExpDate());
					String acctType = response.getNewAcctType();
					acctType = (!"UNKN".equals(acctType) ? acctType : null);
					ps.setString(colNum++, acctType);
					ps.setString(colNum++, response.getNewAcctNumTrunc());
					ps.setBytes(colNum++, response.getNewAcctNumEnc());
					ps.setString(colNum++, response.getNewExpDate());
					ps.setString(colNum++, response.getTestFlag());
					ps.setString(colNum++, response.getCardId());
					// fetch original merch num, disc_data from request
					ps.setLong(colNum++, response.getReqId());
					ps.addBatch();
					++batchCount;
					++rspCount;
					if (rspCount % 1000 == 0) {
						ibId = response.getIbId();
						log.debug("" + rspCount + " responses generated for ibId " + ibId);
					}
				}
				ps.executeBatch();
			}
			log.debug("insertResponseRecords - duration: " + (System.currentTimeMillis() - startTime));
			log.debug("" + rspCount + " total responses generated for ibId " + ibId);
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	public void purgeResponseRecords(long ibId) {
		Statement s = null;
		try {
			connect(true);
			log.debug("Clearing response records from aus_response for ibId " + ibId);

			// update the process_flag of an inbound file record
			s = con.createStatement();
			int rowCount = s.executeUpdate(" delete from aus_responses where ib_id = " + ibId);

			log.debug("" + rowCount + " response records cleared from aus_responses" + " for ibId " + ibId);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	public boolean insertResponseRecord(ResponseRecord response) {
		List responses = new ArrayList();
		responses.add(response);
		return insertResponseRecords(responses);
	}
	private ResponseRecord createResponseRecord(ResultSet rs) throws Exception {
		ResponseRecord response = new ResponseRecord();
		response.setRspId(rs.getLong("rsp_id"));
		response.setCreateTs(rs.getTimestamp("create_ts"));
		response.setRspfId(rs.getLong("rspf_id"));
		response.setIbId(rs.getLong("ib_id"));
		response.setSysCode(rs.getString("sys_code"));
		response.setObId(rs.getLong("ob_id"));
		response.setReqfId(rs.getLong("reqf_id"));
		response.setReqId(rs.getLong("req_id"));
		response.setMerchNum(rs.getString("merch_num"));
		response.setRspCode(rs.getString("rsp_code"));
		response.setOldAcctType(rs.getString("old_account_type"));
		response.setOldAcctNum(rs.getString("old_account_num"));
		response.setOldExpDate(rs.getString("old_exp_date"));
		response.setDiscData(rs.getString("disc_data"));
		response.setTestFlag(rs.getString("test_flag"));
		response.setCardId(rs.getString("card_id"));
		String newAcctType = rs.getString("new_account_type");
		if (newAcctType != null) {
			response.setNewAcctType(newAcctType);
		}
		String newAcctNum = rs.getString("new_account_num");
		if (newAcctNum != null) {
			response.setNewAcctNum(newAcctNum);
		}
		String newExpDate = rs.getString("new_exp_date");
		if (newExpDate != null) {
			response.setNewExpDate(newExpDate);
		}
		return response;
	}
	/**
	 * Get response records associated with the rspId.
	 */
	private ResponseRecord getResponseRecord(long rspId) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			String sqlQuery = "select rsp_id,create_ts,rspf_id,ib_id,sys_code,ob_id,reqf_id,req_id,merch_num,rsp_code,old_account_type,dukpt_decrypt_wrapper(old_account_num_enc) old_account_num,old_exp_date,disc_data,"
					+ "new_account_type,dukpt_decrypt_wrapper(new_account_num_enc) new_account_num,new_exp_date,test_flag,card_id from aus_responses where rsp_id = " + rspId;
			rs = s.executeQuery(sqlQuery);
			/*
			rs = s.executeQuery(" select                "
					+ "  rsp_id,              "
					+ "  create_ts,           "
					+ "  rspf_id,             "
					+ "  ib_id,               "
					+ "  sys_code,            "
					+ "  ob_id,               "
					+ "  reqf_id,             "
					+ "  req_id,              "
					+ "  merch_num,           "
					+ "  rsp_code,            "
					+ "  old_account_type,    "
					+ "  dukpt_decrypt_wrapper(old_account_num_enc) old_account_num,  "
					+ "  old_exp_date,        "
					+ "  disc_data,           "
					+ "  new_account_type,    "
					+ "  dukpt_decrypt_wrapper(new_account_num_enc) new_account_num,  "
					+ "  new_exp_date,        " + "  test_flag,           "
					+ "  card_id              " + " from aus_responses    "
					+ " where rsp_id = " + rspId);
			*/
			if (rs.next()) {
				return createResponseRecord(rs);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Load response records not associated with response files (rspf_id is
	 * currently 0). These will be ordered by merch_num and reqf_id. Response
	 * codes 'GOOD', 'NPBIN', 'NOMATCH' are filtered out, response files will
	 * not include these non-event response records from Visa.
	 */
	private List getNewResponseRecords(boolean testFlag) // obsolete
	{
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			String sqlQuery = "select rsp_id,create_ts,rspf_id,ib_id,sys_code,ob_id,reqf_id,req_id,merch_num,rsp_code,old_account_type,"
					+ "dukpt_decrypt_wrapper(old_account_num_enc) old_account_num,old_exp_date,disc_data,new_account_type,"
					+ "dukpt_decrypt_wrapper(new_account_num_enc) new_account_num,new_exp_date,test_flag,card_id from aus_responses"
					+ "where rspf_id = 0 and rsp_code not in ('GOOD', 'NPBIN', 'NOMATCH') and reqf_id > 0" + " and test_flag = '"
					+ (testFlag ? "y" : "n") + "' order by merch_num, reqf_id ";
			rs = s.executeQuery (sqlQuery);
			/*		
			rs = s.executeQuery(" select                "
					+ "  rsp_id,              "
					+ "  create_ts,           "
					+ "  rspf_id,             "
					+ "  ib_id,               "
					+ "  sys_code,            "
					+ "  ob_id,               "
					+ "  reqf_id,             "
					+ "  req_id,              "
					+ "  merch_num,           "
					+ "  rsp_code,            "
					+ "  old_account_type,    "
					+ "  dukpt_decrypt_wrapper(old_account_num_enc) old_account_num,  "
					+ "  old_exp_date,        "
					+ "  disc_data,           "
					+ "  new_account_type,    "
					+ "  dukpt_decrypt_wrapper(new_account_num_enc) new_account_num,  "
					+ "  new_exp_date,        "
					+ "  test_flag,           "
					+ "  card_id              "
					+ " from aus_responses    "
					+ " where rspf_id = 0 and rsp_code not in ( 'GOOD', 'NPBIN', 'NOMATCH' ) "
					+ "  and reqf_id > 0      " + "  and test_flag = '"
					+ (testFlag ? "y" : "n") + "' "
					+ " order by merch_num, reqf_id ");
			*/		
			List recs = new ArrayList();
			while (rs.next()) {
				recs.add(createResponseRecord(rs));
			}
			return recs;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	private List getNewResponseRecords() // obsolete
	{
		return getNewResponseRecords(false);
	}
	public void handleNewResponseRecords(ResponseRecordHandler handler, boolean testFlag) throws Exception {
		Statement s = null;
		ResultSet rs = null;
		try {
			// connect with timeout exempt to prevent
			// connection pool disconnects on large files
			connect(true);
			s = con.createStatement();
			rs = s.executeQuery(" select                "
					+ "  rsp_id,              "
					+ "  create_ts,           "
					+ "  rspf_id,             "
					+ "  ib_id,               "
					+ "  sys_code,            "
					+ "  ob_id,               "
					+ "  reqf_id,             "
					+ "  req_id,              "
					+ "  merch_num,           "
					+ "  rsp_code,            "
					+ "  old_account_type,    "
					+ "  dukpt_decrypt_wrapper(old_account_num_enc) old_account_num,  "
					+ "  old_exp_date,        "
					+ "  disc_data,           "
					+ "  new_account_type,    "
					+ "  dukpt_decrypt_wrapper(new_account_num_enc) new_account_num,  "
					+ "  new_exp_date,        "
					+ "  test_flag,           "
					+ "  card_id              "
					+ " from aus_responses    "
					+ " where rspf_id = 0 and rsp_code not in ( 'GOOD', 'NPBIN', 'NOMATCH' ) "
					+ "  and reqf_id > 0      " + "  and test_flag = '"
					+ (testFlag ? "y" : "n") + "' "
					+ " order by merch_num, reqf_id ");
								
			List responses = new ArrayList();
			while (rs.next()) {
				responses.add(createResponseRecord(rs));
				if (responses.size() >= 1000) {
					handler.handleResponses(responses);
					responses.clear();
				}
			}
			if (!responses.isEmpty()) {
				handler.handleResponses(responses);
			}
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	/**
	 * Response file
	 */
	public boolean insertResponseFile(ResponseFile rspFile, boolean dataFlag) {
		PreparedStatement ps = null;
		Statement s = null;
		ResultSet rs = null;
		boolean acSetting = getAutoCommit();
		try {
			connect(true);   //CALVIN
			setAutoCommit(false);

			// determine next seq num for response file record
			s = con.createStatement();
			String sqlQuery = "select nvl(max(seq_num),0) + 1 seq_num from aus_response_files where reqf_id=" + rspFile.getReqfId() + " and merch_num ='" + rspFile.getMerchNum() + "'";
			rs = s.executeQuery (sqlQuery);
			/*
			rs = s.executeQuery(" select                                "
					+ "  nvl(max(seq_num),0) + 1 seq_num      "
					+ " from                                  "
					+ "  aus_response_files                   "
					+ " where                                 "
					+ "  reqf_id = " + rspFile.getReqfId() + ""
					+ "  and merch_num =                      " + "   '"
					+ rspFile.getMerchNum() + "'     ");
			*/		
			rs.next();
			rspFile.setSeqNum(rs.getInt("seq_num"));
			String fileName = "rsp-" + rspFile.getReqfId() + "-" + rspFile.getSeqNum() + ".txt";
			rspFile.setFileName(fileName);
			rs.close();
			s.close();

			String sqlQuery2 = "insert into aus_response_files (rspf_id,create_ts,reqf_id,ib_id,seq_num,ausr_name,merch_num,dl_ts,dl_num,dl_user,file_name,file_size,test_flag) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement (sqlQuery2);
			/*
			ps = con.prepareStatement(" insert into aus_response_files  "
					+ " ( rspf_id,                      "
					+ "   create_ts,                    "
					+ "   reqf_id,                      "
					+ "   ib_id,                        "
					+ "   seq_num,                      "
					+ "   ausr_name,                    "
					+ "   merch_num,                    "
					+ "   dl_ts,                        "
					+ "   dl_num,                       "
					+ "   dl_user,                      "
					+ "   file_name,                    "
					+ "   file_size,                    "
					+ "   test_flag )                   "
					+ " values                          "
					+ " ( ?, ?, ?, ?, ?, ?, ?,          "
					+ "   ?, ?, ?, ?, ?, ? )            ");
			*/
			if (rspFile.getRspfId() <= 0) {
				rspFile.setRspfId(getNewId());
			}

			int colNum = 1;
			ps.setLong(colNum++, rspFile.getRspfId());
			ps.setTimestamp(colNum++, rspFile.getCreateTs());
			ps.setLong(colNum++, rspFile.getReqfId());
			ps.setLong(colNum++, rspFile.getIbId());
			ps.setInt(colNum++, rspFile.getSeqNum());
			ps.setString(colNum++, rspFile.getAusrName());
			ps.setString(colNum++, rspFile.getMerchNum());
			ps.setTimestamp(colNum++, rspFile.getDlTs());
			ps.setInt(colNum++, rspFile.getDlNum());
			ps.setString(colNum++, rspFile.getDlUser());
			ps.setString(colNum++, rspFile.getFileName());
			ps.setLong(colNum++, 0L);
			ps.setString(colNum++, rspFile.getTestFlag());
			ps.executeUpdate();

			// update related response records
			s = con.createStatement();
			s.executeUpdate("update aus_responses set rspf_id=" + rspFile.getRspfId() + " where reqf_id=" + rspFile.getReqfId() + " and merch_num='" + rspFile.getMerchNum() + "'");

			// return true if dataFlag not true or if no file data
			// available to upload or the result of uploading the
			// file data
			boolean insertOk = (!dataFlag || rspFile.empty() || uploadAusFileData(new AusFileRowId(
					"aus_response_files", "rspf_id", rspFile.getRspfId(), rspFile)));
			if (insertOk) {
				commit();
			}
			else {
				rollback();
			}
			return insertOk;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			try {
				ps.close();
			}
			catch (Exception e) {}
			setAutoCommit(acSetting);
			cleanUp();
		}
		return false;
	}
	private ResponseFile createResponseFile(ResultSet rs, boolean dataFlag) throws Exception {
		ResponseFile rspFile = new ResponseFile();
		rspFile.setRspfId(rs.getLong("rspf_id"));
		rspFile.setCreateTs(rs.getTimestamp("create_ts"));
		rspFile.setReqfId(rs.getLong("reqf_id"));
		rspFile.setIbId(rs.getLong("ib_id"));
		rspFile.setSeqNum(rs.getInt("seq_num"));
		rspFile.setAusrName(rs.getString("ausr_name"));
		rspFile.setMerchNum(rs.getString("merch_num"));
		rspFile.setDlTs(rs.getTimestamp("dl_ts"));
		rspFile.setDlNum(rs.getInt("dl_num"));
		rspFile.setDlUser(rs.getString("dl_user"));
		rspFile.setFileName(rs.getString("file_name"));
		rspFile.setFileSize(rs.getInt("file_size"));
		rspFile.setTestFlag(rs.getString("test_flag"));
		if (dataFlag) {
			setFileContainerData(rspFile, rs, "file_data");
		}
		return rspFile;
	}
	public ResponseFile getResponseFile(long rspfId, boolean dataFlag) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_response_files where rspf_id = " + rspfId);
			if (rs.next()) {
				return createResponseFile(rs, dataFlag);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List lookupResponseFiles(String ausrName, String merchNum) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			boolean nameFlag = (ausrName != null && !ausrName.equals(""));
			boolean mnFlag = (merchNum != null && !merchNum.equals(""));

			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_response_files where rspf_id = rspf_id "
					+ (nameFlag ? " and ausr_name = '" + ausrName + "' " : "")
					+ (mnFlag ? " and merch_num = '" + merchNum + "' " : "") + " ");

			List files = new ArrayList();
			while (rs.next()) {
				files.add(createResponseFile(rs, false));
			}
			return files;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public boolean updateResponseFile(ResponseFile rspFile, boolean dataFlag) {
		PreparedStatement ps = null;
		try {
			connect(true);   //CALVIN
			String sqlQuery = "update aus_response_files set create_ts=?,reqf_id=?,ib_id=?,seq_num=?,ausr_name=?,merch_num=?,dl_ts=?,dl_num=?,dl_user=?,file_name=?,test_flag=? where rspf_id=? ";
			ps = con.prepareStatement (sqlQuery);
			/*
			ps = con.prepareStatement(" update aus_response_files set "
					+ "   create_ts = ?,              "
					+ "   reqf_id   = ?,              "
					+ "   ib_id     = ?,              "
					+ "   seq_num   = ?,              "
					+ "   ausr_name = ?,              "
					+ "   merch_num = ?,              "
					+ "   dl_ts     = ?,              "
					+ "   dl_num    = ?,              "
					+ "   dl_user   = ?,              "
					+ "   file_name = ?,              "
					+ "   test_flag = ?               "
					+ " where rspf_id = ?             ");
			*/
			int colNum = 1;
			ps.setTimestamp(colNum++, rspFile.getCreateTs());
			ps.setLong(colNum++, rspFile.getReqfId());
			ps.setLong(colNum++, rspFile.getIbId());
			ps.setInt(colNum++, rspFile.getSeqNum());
			ps.setString(colNum++, rspFile.getAusrName());
			ps.setString(colNum++, rspFile.getMerchNum());
			ps.setTimestamp(colNum++, rspFile.getDlTs());
			ps.setInt(colNum++, rspFile.getDlNum());
			ps.setString(colNum++, rspFile.getDlUser());
			ps.setString(colNum++, rspFile.getFileName());
			ps.setString(colNum++, rspFile.getTestFlag());
			ps.setLong(colNum++, rspFile.getRspfId());
			ps.executeUpdate();
			// return true if dataFlag not true or
			// if no file data available to upload
			// or the result of uploading the file data
			return (!dataFlag || rspFile.empty() || uploadAusFileData(new AusFileRowId(
					"aus_response_files", "rspf_id", rspFile.getRspfId(), rspFile)));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	private ActivityRow createActivityRow(ResultSet rs) throws Exception {
		ActivityRow row = new ActivityRow();
		row.setReqfId(rs.getLong("reqf_id"));
		row.setAusrName(rs.getString("ausr_name"));
		row.setReqTs(rs.getTimestamp("req_ts"));
		row.setMerchNum(rs.getString("merch_num"));
		row.setReqStatus(rs.getString("req_status"));
		row.setReqFileName(rs.getString("req_file_name"));
		row.setTestFlag(rs.getString("test_flag"));
		row.setRspfId(rs.getLong("rspf_id"));
		row.setRspTs(rs.getTimestamp("rsp_ts"));
		row.setRspFileName(rs.getString("rsp_file_name"));
		row.setStatus(rs.getString("status"));
		row.setDlCount(rs.getInt("dl_count"));
		return row;
	}
	/**
	 * Lookup request/response activity (web interface version)
	 */
	public List lookupActivity(String ausrName, String merchNum, int daysBack) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			boolean mnFlag = (merchNum != null && !merchNum.equals(""));
			boolean nameFlag = (!mnFlag && ausrName != null && !ausrName.equals(""));

			s = con.createStatement();
			rs = s.executeQuery(" select                                  "
					+ "  reqf.reqf_id           reqf_id,        "
					+ "  reqf.ausr_name         ausr_name,      "
					+ "  reqf.create_ts         req_ts,         "
					+ "  reqf.merch_num         merch_num,      "
					+ "  reqf.status            req_status,     "
					+ "  reqf.file_name         req_file_name,  "
					+ "  reqf.test_flag         test_flag,      "
					+ "  rspf.rspf_id           rspf_id,        "
					+ "  rspf.create_ts         rsp_ts,         "
					+ "  rspf.file_name         rsp_file_name,  "
					+ "  rspf.dl_num            dl_count,       "
					+ "  decode(reqf.status,                    "
					+ "   'UPLOADED','Pending',                 "
					+ "   'ERROR',   'Failed',                  "
					+ "   nvl2(rspf.rspf_id,                    "
					+ "        'Successful',                    "
					+ "        'Pending'))      status          "
					+ " from                                    "
					+ "  aus_request_files  reqf,               "
					+ "  aus_response_files rspf                "
					+ " where                                   "
					+ "  reqf.reqf_id = rspf.reqf_id(+)         "
					+ "  and (trunc(sysdate)                    "
					+ "   - trunc(nvl(rspf.create_ts,sysdate))) < "
					+ daysBack
					+ (nameFlag ? " and reqf.ausr_name = '" + ausrName + "' " : "")
					+ (mnFlag ? " and reqf.merch_num = '" + merchNum + "' " : "") + " order by reqf.reqf_id desc ");

			List rows = new ArrayList();
			while (rs.next()) {
				rows.add(createActivityRow(rs));
			}
			return rows;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List lookupActivity(String ausrName, String merchNum) {
		return lookupActivity(ausrName, merchNum, getActivityDaysBack());
	}
	
	// filter types supported by api lookup activity method
	public static final String FILT_ALL = "ALL";
	public static final String FILT_RESPONSE = "RESPONSE";
	public static final String FILT_NEW = "NEW";
	public static final String FILT_ERROR = "ERROR";

	/**
	 * Lookup request/response activity (API status query version).
	 */
	public List lookupActivity(String merchNum, String filter, long reqfId, int daysBack) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			String filterClause = null;
			if (FILT_RESPONSE.equals(filter)) {
				filterClause = " where status = 'SUCCESS' ";
			}
			else if (FILT_NEW.equals(filter)) {
				filterClause = " where status = 'SUCCESS' and dl_count = 0 ";
			}
			else if (FILT_ERROR.equals(filter)) {
				filterClause = " where status = 'FAILED' ";
			}

			boolean reqfIdFlag = (reqfId != -1L);
			boolean mnFlag = (merchNum != null && !merchNum.equals(""));

			s = con.createStatement();
			rs = s.executeQuery(" select * from (                         "
					+ " select                                  "
					+ "  reqf.reqf_id           reqf_id,        "
					+ "  reqf.ausr_name         ausr_name,      "
					+ "  reqf.create_ts         req_ts,         "
					+ "  reqf.merch_num         merch_num,      "
					+ "  reqf.status            req_status,     "
					+ "  reqf.file_name         req_file_name,  "
					+ "  reqf.test_flag         test_flag,      "
					+ "  rspf.rspf_id           rspf_id,        "
					+ "  rspf.create_ts         rsp_ts,         "
					+ "  rspf.file_name         rsp_file_name,  "
					+ "  decode(reqf.status,                    "
					+ "   'UPLOADED','PENDING',                 "
					+ "   'ERROR',   'FAILED',                  "
					+ "   nvl2(rspf.rspf_id,                    "
					+ "        'SUCCESS',                       "
					+ "        'PENDING'))  status,             "
					+ "  rspf.dl_num            dl_count        "
					+ " from                                    "
					+ "  aus_request_files  reqf,               "
					+ "  aus_response_files rspf                "
					+ " where                                   "
					+ "  reqf.reqf_id = rspf.reqf_id(+)         "
					+ "  and (trunc(sysdate)                    "
					+ "   - trunc(nvl(rspf.create_ts,sysdate))) < "
					+ daysBack
					+ (mnFlag ? " and reqf.merch_num = '" + merchNum + "' " : "")
					+ (reqfIdFlag ? " and reqf.reqf_id = " + reqfId + " " : "")
					+ " order by reqf.reqf_id desc " + " ) "
					+ (filterClause != null ? filterClause : ""));

			List rows = new ArrayList();
			while (rs.next()) {
				rows.add(createActivityRow(rs));
			}
			return rows;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List lookupActivity(String merchNum, String filter, long reqfId) {
		return lookupActivity(merchNum, filter, reqfId, getActivityDaysBack());
	}
	/**
	 * Loader errors
	 */
	public boolean insertProcessErrors(List errors) {
		PreparedStatement ps = null;
		try {
			connect(true);   //CALVIN
			ps = con.prepareStatement("insert into aus_process_errors (err_id,create_ts,file_id,error_msg,severity,line_num,line_pos) values (?,?,?,?,?,?,?)");

			// add responses in batches of up to 100
			for (Iterator i = errors.iterator(); i.hasNext();) {
				int batchCount = 0;
				ps.clearBatch();
				while (i.hasNext() && batchCount < 100) {
					ProcessError pe = (ProcessError) i.next();

					// TODO: any way to reduce this to one statement?
					if (pe.getErrId() <= 0) {
						pe.setErrId(getNewId());
					}

					int colNum = 1;
					ps.setLong(colNum++, pe.getErrId());
					ps.setTimestamp(colNum++, pe.getCreateTs());
					ps.setLong(colNum++, pe.getFileId());
					ps.setString(colNum++, pe.getErrorMsg());
					ps.setInt(colNum++, pe.getSeverity());
					ps.setInt(colNum++, pe.getLineNum());
					ps.setInt(colNum++, pe.getLinePos());
					ps.addBatch();
					++batchCount;
				}
				ps.executeBatch();
			}
			return true;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			cleanUp(ps);
		}
		return false;
	}
	/**
	 * SimAccount handling.
	 */
	private SimAccount createSimAccount(ResultSet rs) throws Exception {
		SimAccount sa = new SimAccount();
		sa.setSysCode(rs.getString("sys_code"));
		sa.setAccountNum(rs.getString("account_num"));
		sa.setExpDate(rs.getString("exp_date"));
		sa.setRespCode(rs.getString("resp_code"));
		sa.setNewAccountNum(rs.getString("new_account_num"));
		sa.setNewExpDate(rs.getString("new_exp_date"));
		return sa;
	}
	public List getSimAccounts(String sysCode) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery(" select * from aus_sim_accounts " + (sysCode != null ? " where sys_code = '" + sysCode + "' " : ""));
			List saList = new ArrayList();
			while (rs.next()) {
				saList.add(createSimAccount(rs));
			}
			return saList;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public List getSimAccounts() {
		return getSimAccounts(null);
	}
	/**
	 * Stored cards
	 */
	public String getStoredAccountNum(String merchNum, String cardId) {
		Statement s = null;
		ResultSet rs = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			rs = s.executeQuery("select dukpt_decrypt_wrapper(card_number_enc) as card_number from trident_api_card_data where card_id='" + cardId + "'" + " and merchant_number=" + merchNum);

			if (rs.next()) {
				return rs.getString(1);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public String deleteStoredAccountNum(String merchNum, String cardId) {
		// safety first
		if (!"941000070941".equals(merchNum)) {
			throw new RuntimeException("Only 941000070941 allowed at this time");
		}

		Statement s = null;
		try {
			connect(true);   //CALVIN
			String updStr = "delete from trident_api_card_data where merchant_number = " + merchNum + " " + " and card_id = '" + cardId + "'";
			s = con.createStatement();
			s.executeUpdate(updStr);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	public void insertStoredAccountNum(String termId, String merchNum, String cardId, String cardNum, String expDate, String cardName) {
		Statement s = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			String sqlQuery = "insert into trident_api_card_data (terminal_id,merchant_number,card_id,card_number,exp_date,cardholder_name) "
					+ "values ('" + termId + "'," + merchNum + ",'" + cardId + "', '" + cardNum + "', '" + expDate + "', '" + cardName + "')";
			s.executeUpdate(sqlQuery);
			
			/*
			s.executeUpdate(" insert into             "
					+ "  trident_api_card_data  " + " ( terminal_id,          "
					+ "   merchant_number,      " + "   card_id,              "
					+ "   card_number,          " + "   exp_date,             "
					+ "   cardholder_name )     " + " values (                "
					+ "   '" + termId + "',     " + "   " + merchNum + ",     "
					+ "   '" + cardId + "',     " + "   '" + cardNum + "',    "
					+ "   '" + expDate + "',    " + "   '" + cardName + "' )  ");
			*/		
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	public DropDownItem[] getOutboundFileTypeDdItems(String sysCode) {
		List ofts = getOutboundFileTypes(sysCode);
		List ddList = new ArrayList();
		for (Iterator i = ofts.iterator(); i.hasNext();) {
			OutboundFileType oft = (OutboundFileType) i.next();
			String value = oft.getFileType();
			String desc = oft.getDescription();
			ddList.add(new DropDownItem(value, desc));
		}
		return (DropDownItem[]) ddList.toArray(new DropDownItem[]{});
	}
	/**
	 * AUTO AUS
	 */

	/**
	 * Generates list of profiles due for auto processing.
	 */
	public List getAutoLoadProfiles(boolean testFlag) {
		Statement s = null;
		ResultSet rs = null;

		try {
			connect(true);   //CALVIN
			String testFlagStr = (testFlag ? "y" : "n");
			s = con.createStatement();
			String sqlQuery = "select profile_id from (select profile_id, to_number(merch_num) merch_num, auto_flag, auto_test_flag, "
					+ "trunc(sysdate,'DAY') + auto_day next_auto_date, nvl(auto_last_ts, to_date('1-jan-1970')) last_auto_ts from aus_profiles), "
					+ "mif where auto_flag = 'y' and auto_test_flag = '" + testFlagStr + "' and merch_num = mif.merchant_number and "
					+ "nvl(mif.dmacctst,'O') <> 'D' and next_auto_date < sysdate and next_auto_date > last_auto_ts";
			rs = s.executeQuery(sqlQuery);
			
			/*
			rs = s.executeQuery(" select                                       "
					+ "  profile_id                                  "
					+ " from                                         "
					+ "  ( select                                    "
					+ "     profile_id,                              "
					+ "     to_number(merch_num) merch_num,          "
					+ "     auto_flag,                               "
					+ "     auto_test_flag,                          "
					+ "     trunc(sysdate,'DAY') + auto_day          "
					+ "      next_auto_date,                         "
					+ "     nvl(auto_last_ts, to_date('1-jan-1970')) "
					+ "      last_auto_ts                            "
					+ "    from                                      "
					+ "     aus_profiles ),                          "
					+ "  mif                                         "
					+ " where                                        "
					+ "  auto_flag = 'y'                             "
					+ "  and auto_test_flag = '"
					+ testFlagStr
					+ "'  "
					+ "  and merch_num = mif.merchant_number         "
					+ "  and nvl(mif.dmacctst,'O') <> 'D'            "
					+ "  and next_auto_date < sysdate                "
					+ "  and next_auto_date > last_auto_ts           ");
			*/
			List profiles = new ArrayList();
			while (rs.next()) {
				System.out.println("found profile to auto load: " + rs.getString("profile_id"));
				profiles.add(rs.getString("profile_id"));
			}
			return profiles;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
		return null;
	}
	/**
	 * Takes exp date from trident_api_card_data in MMYY format, validates it
	 * and returns it in YYMM format that the card update services require. In
	 * case of invalid data certain values will be returned:
	 *
	 * 0101 - contained 0000 (often present in stored card data) 0102 - invalid
	 * exp date length 0103 - invalid month (non-numeric or out of range) 0104 -
	 * invalid year (non-numeric) 0105 - other error
	 *
	 */
	public String checkTacdExpDate(String expDate) {
		try {
			// check for stored card 0000
			if ("0000".equals(expDate)) {
				return "0101";
			}
			// valid length check
			if (expDate.length() != 4) {
				log.warn("Invalid expiration date length: '" + expDate + "', returning 0102");
				return "0102";
			}
			// validate month, numeric and in range
			String mm = expDate.substring(0, 2);
			try {
				int mmNum = Integer.parseInt(mm);
				if (mmNum < 1 || mmNum > 12) {
					throw new Exception("Invalid month range");
				}
			}
			catch (Exception e) {
				log.warn("Invalid expiration month: '" + mm + "', returning 0103", e);
				return "0103";
			}
			// validate year, numeric
			String yy = expDate.substring(2, 4);
			try {
				Integer.parseInt(yy);
			}
			catch (Exception e) {
				log.warn("Invalid expiration year: '" + yy + "', returning 0104", e);
				return "0104";
			}
			// flip the year and month to aus format
			return yy + mm;
		}
		catch (Exception e) {
			// other error
			log.error("Expiration date validation error: '" + expDate
					+ "', returning 0105", e);
			return "0105";
		}
	}
	/**
	 * Generates request file and requests for a profile id. May generate
	 * multiple request files if card data counts exceed max auto card record
	 * config setting.
	 */
	public void autoLoadProfile(String profileId, boolean testFlag) {
		boolean acSetting = getAutoCommit();
		boolean opOk = true;
		Statement s1 = null;
		Statement s2 = null;
		Statement s3 = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		
		try {
			log.debug("autoLoadProfile - startTime: " + startTime);

			log.debug("about to autoLoadProfile " + profileId + ", " + testFlag);
			connect(true);   //CALVIN
			setAutoCommit(false);
			int maxCards = getAutoCardMax();

			// fetch all card record for profile id from trident card data
			s1 = con.createStatement();
			rs = s1.executeQuery(" select * from trident_api_card_data where terminal_id='" + profileId + "'");

			log.debug("auto loading " + profileId);

			// generate card requests in aus
			int cardCount = 0;
			long reqfId = 0L;
			while (rs.next()) {
				// get aus request data
				String cardId = rs.getString("card_id");
				log.debug("auto loading card id " + cardId);
				String accountNumEnc = rs.getString("card_number_enc");
				String accountNum = rs.getString("card_number");
				String expDate = checkTacdExpDate(rs.getString("exp_date"));
				String discData = "AUTO";
				String accountType = AusAccount.AT_UNKNOWN;
				if (accountNum.startsWith("4")) {
					accountType = AusAccount.AT_VISA;
				}
				else if (accountNum.startsWith("5")) {
					accountType = AusAccount.AT_MC;
				}
				else if (accountNum.startsWith("6")) {
					accountType = AusAccount.AT_DISC;
				}
				else {
					// not an aus supported card type
					continue;
				}

				// if starting batch of cards create request file
				if (cardCount == 0) {
					reqfId = getNewId();
					s2 = con.createStatement();
					String sqlQuery = "insert into aus_request_files (reqf_id,create_ts,user_name,ausr_name,merch_num,status,load_num,file_name,file_size,file_data,test_flag,disc_data) "
							+ "select " + reqfId + " reqf_id,sysdate create_ts, u.user_name, u.ausr_name, p.merch_num, 'PROCESSED' status, 1 load_num, 'auto-aus-updates.txt' file_name, "
							+ "0 file_size, null file_data, '" + (testFlag ? "y" : "n") + "', 'AUTO' disc_data from aus_profiles p, aus_users u where p.profile_id = '" + profileId + "' and p.auto_ausr_id = u.ausr_id (+) ";
					s2.executeUpdate(sqlQuery);
					/*
					s2.executeUpdate(" insert into aus_request_files       "
							+ " ( reqf_id,                          "
							+ " 	create_ts,                        "
							+ "   user_name,                        "
							+ "   ausr_name,                        "
							+ " 	merch_num,                        "
							+ " 	status,	                          "
							+ "   load_num,                         "
							+ "   file_name,                        "
							+ " 	file_size,                        "
							+ "   file_data,                        "
							+ "   test_flag,                        "
							+ " 	disc_data )                       "
							+ " select                              " + "  "
							+ reqfId
							+ " reqf_id,            "
							+ "  sysdate create_ts,                 "
							+ "  u.user_name,                       "
							+ "  u.ausr_name,                       "
							+ "  p.merch_num,                       "
							+ "  'PROCESSED' status,                "
							+ "  1 load_num,                        "
							+ "  'auto-aus-updates.txt' file_name,    "
							+ "  0 file_size,                       "
							+ "  null file_data,                    "
							+ "  '"
							+ (testFlag ? "y" : "n")
							+ "',  "
							+ "  'AUTO' disc_data                   "
							+ " from                                "
							+ "  aus_profiles p,                    "
							+ "  aus_users u                        "
							+ " where                               "
							+ "  p.profile_id = '"
							+ profileId
							+ "' "
							+ "  and p.auto_ausr_id = u.ausr_id (+) ");
					*/
					
					s2.close();
				}

				// create request record
				if (s3 == null) {
					s3 = con.createStatement();
				}
				String sqlQuery2 = "insert into aus_requests (req_id,reqf_id,create_ts,merch_num,account_type,account_num,account_num_enc,exp_date,disc_data,card_id) "
                        + "select aus_id_sequence.nextval," + reqfId + ",sysdate,p.merch_num,'"  
                        + accountType + "','" + accountNum + "','" + accountNumEnc + "','" + expDate + "','" + discData + "','" + cardId 
                        + "' from aus_profiles p where p.profile_id='" + profileId + "'";
				s3.executeUpdate (sqlQuery2);
				/*
				s3.executeUpdate(" insert into aus_requests    "
						+ " ( req_id,                   "
						+ "   reqf_id,                  "
						+ "   create_ts,                "
						+ "   merch_num,                "
						+ "   account_type,             "
						+ "   account_num,              "
						+ "   account_num_enc,          "
						+ "   exp_date,                 "
						+ "   disc_data,                "
						+ "   card_id )                 "
						+ " select                      "
						+ "  aus_id_sequence.nextval,   " + "  "
						+ reqfId
						+ ",            "
						+ "  sysdate,                   "
						+ "  p.merch_num,               "
						+ "  '"
						+ accountType
						+ "',     "
						+ "  '"
						+ accountNum
						+ "',      "
						+ "  '"
						+ accountNumEnc
						+ "',   "
						+ "  '"
						+ expDate
						+ "',         "
						+ "  '"
						+ discData
						+ "',        "
						+ "  '"
						+ cardId
						+ "'           "
						+ " from                        "
						+ "  aus_profiles p             "
						+ " where                       "
						+ "  p.profile_id = '" + profileId + "' ");
				*/
				
				// reset card count to start reqf batch if needed
				if (++cardCount >= maxCards) {
					cardCount = 0;
				}
				log.debug("autoLoadProfile - duration: " + (System.currentTimeMillis() - startTime));
			}
		}
		catch (Exception e) {
			opOk = false;
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			if (opOk) {
				commit();
			}
			else {
				rollback();
			}
			try {
				rs.close();
			}
			catch (Exception e) {}
			try {
				s1.close();
			}
			catch (Exception e) {}
			try {
				s2.close();
			}
			catch (Exception e) {}
			try {
				s3.close();
			}
			catch (Exception e) {}
			setAutoCommit(acSetting);
			cleanUp();
		}
	}
	public void setLastAutoLoad(String profileId) {
		Statement s = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			s.executeUpdate(" update aus_profiles set auto_last_ts = sysdate where profile_id = '" + profileId + "'");
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	public void resetLastAutoLoad(String profileId) {
		Statement s = null;
		try {
			connect(true);   //CALVIN
			s = con.createStatement();
			s.executeUpdate(" update aus_profiles set auto_last_ts = null where profile_id = '" + profileId + "'");
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			}
			catch (Exception e) {}
			cleanUp();
		}
	}
	public DropDownItem[] getAutoUserDdItems(String merchNum) {
		List links = getAllMerchLinksForMerchNum(merchNum);
		List ddList = new ArrayList();
		for (Iterator i = links.iterator(); i.hasNext();) {
			MerchLink link = (MerchLink) i.next();
			String value = "" + link.getAusrId();
			String desc = link.getAusrName();
			ddList.add(new DropDownItem(value, desc));
		}
		return (DropDownItem[]) ddList.toArray(new DropDownItem[]{});
	}
	/**
	 * Delete records older than given days back count. Deletes from
	 * AUS_RESPONSES, AUS_REQUESTS, AUS_RESPONSE_FILES and AUS_REQUEST_FILES.
	 * Used by daily timed event.
	 */
	public void cleanupDbBefore(int daysBack) {
		boolean acSetting = getAutoCommit();
		Statement s = null;
		boolean opOk = false;
		long startTime = System.currentTimeMillis();

		try {
			log.debug("cleanupDbBefore - startTime: " + startTime);
			connect(true);   //CALVIN
			setAutoCommit(false);
			s = con.createStatement();
			// for now prevent records newer than 63 days from being deleted
			if (daysBack < 63)
				throw new RuntimeException("Invalid days back: " + daysBack);

			Calendar startCal = Calendar.getInstance();
			int reqCnt = s.executeUpdate("delete from aus_requests where reqf_id in (select reqf_id from aus_request_files where create_ts < trunc(sysdate) - " + daysBack + ")");
			int rspCnt = s.executeUpdate("delete from aus_responses where reqf_id in (select reqf_id from aus_request_files where create_ts < trunc(sysdate) - " + daysBack + ")");
			int rspfCnt = s.executeUpdate("delete from aus_response_files where reqf_id in (select reqf_id from aus_request_files where create_ts < trunc(sysdate) - " + daysBack + " ) ");
			int reqfCnt = s.executeUpdate("delete from aus_request_files where create_ts < trunc(sysdate) - " + daysBack + "");
			long duration = Calendar.getInstance().getTimeInMillis() - startCal.getTimeInMillis();
			Notifier.notifyPurgeDb(isDirect(), daysBack, reqCnt, rspCnt, rspfCnt, reqfCnt, duration);
			opOk = true;
			log.debug("cleanupDbBefore - duration: " + (System.currentTimeMillis() - startTime));
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
		}
		finally {
			if (opOk) {
				commit();
			}
			else {
				rollback();
			}
			try {
				s.close();
			}
			catch (Exception e) {}
			setAutoCommit(acSetting);
			cleanUp();
		}
	}
	/**
	 * Deletes records older than the configured days back setting.
	 */
	public void cleanupDb() {
		cleanupDbBefore(getPurgeDaysBack());
	}
}