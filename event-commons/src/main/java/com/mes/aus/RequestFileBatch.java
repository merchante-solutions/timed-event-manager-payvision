package com.mes.aus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class RequestFileBatch extends AusBase
{
  static Logger log = Logger.getLogger(RequestFileBatch.class);
  
  private OutboundFileType obfType;
  private List reqfList = new ArrayList();
  
  public RequestFileBatch(OutboundFileType obfType)
  {
    this.obfType = obfType;
  }
  
  public OutboundFileType getObfType()
  {
    return obfType;
  }

  public void addRequestFile(RequestFile reqf)
  {
    reqfList.add(reqf);
  }
  
  public List getReqfList()
  {
    return reqfList;
  }
  
  public String toString()
  {
    StringBuffer buf = new StringBuffer("RequestFileBatch [ ");
    buf.append(""+obfType + ", ");
    for (Iterator i = reqfList.iterator(); i.hasNext();)
    {
      buf.append(""+i.next());
      buf.append(i.hasNext() ? ", " : "");
    }
    buf.append(" ]");
    return ""+buf;
  }
}
