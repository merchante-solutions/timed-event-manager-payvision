package com.mes.aus;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class RequestFile extends AusFileContainer implements Serializable
{
  static Logger log = Logger.getLogger(RequestFile.class);

  public static final String ST_UPLOADED  = "UPLOADED";
  public static final String ST_ERROR     = "ERROR";
  public static final String ST_PROCESSED = "PROCESSED";

  private long    reqfId;
  private Date    createDate;
  private String  userName;
  private String  ausrName;
  private String  merchNum;
  private String  fileName;
  private int     fileSize;
  private String  testFlag;
  private String  status;
  private int     parseNum;

  private String  discMerchNum;
  
  public RequestFile()
  {
  }

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }
  
  public void setDiscMerchNum(String discMerchNum)
  {
    this.discMerchNum = discMerchNum;
  }
  public String getDiscMerchNum()
  {
    return discMerchNum;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setAusrName(String  ausrName)
  {
    this.ausrName = ausrName;
  }
  public String  getAusrName()
  {
    return ausrName;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String  getMerchNum()
  {
    return merchNum;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  public String getFileName()
  {
    return fileName;
  }

  public void setStatus(String  status)
  {
    if (!status.equals(ST_PROCESSED) &&
        !status.equals(ST_UPLOADED) &&
        !status.equals(ST_ERROR))
    {
      throw new RuntimeException("Invalid upload status '" + status + "'");
    }
    this.status = status;
  }
  public String  getStatus()
  {
    return status;
  }

  public String getStatusDescription()
  {
    if (status == null)
      return "--";
    else if (status.equals(ST_PROCESSED))
      return "Processed";
    else if (status.equals(ST_UPLOADED))
      return "Uploaded";
    return "Error";
  }
  public boolean hasStatus(String status)
  {
    if (status == null)
    {
      return false;
    }
    return this.status.equals(status);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setParseNum(int parseNum)
  {
    this.parseNum = parseNum;
  }
  public int getParseNum()
  {
    return parseNum;
  }

  public void setFileSize(int fileSize)
  {
    this.fileSize = fileSize;
  }
  public int getFileSize()
  {
    return fileSize;
  }

  public String toString()
  {
    return " RequestFile [ "
      + "reqfId: " + reqfId
      + ", createDate: " + createDate
      + ", userName: " + userName
      + ", ausrName: " + ausrName
      + ", merchNum: " + merchNum
      + ", discMerchNum: " + discMerchNum
      + ", fileName: " + fileName
      + ", fileSize: " + fileSize
      + ", data present: " + !empty()
      + ", status: " + status
      + ", parseNum: " + parseNum
      + ", testFlag: " + testFlag
      + " ]";
  }
}
