package com.mes.aus.file;

import java.io.OutputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.Notifier;
import com.mes.aus.RequestFile;
import com.mes.aus.ResponseFile;
import com.mes.aus.ResponseRecord;
import com.mes.aus.ResponseRecordHandler;
import com.mes.aus.SysUtil;

public class RspfGenerator extends AutomatedProcess implements ResponseRecordHandler {
	static Logger log = Logger.getLogger(RspfGenerator.class);
	
	public final static int SRC_MES = 1;
	public final static int SRC_VAU = 2;
	public final static int SRC_ABU = 3;
	private Notifier notifier;
	private StringBuffer noticeText;
	private ResponseFile rspf;
	private OutputStream out;
	private int detailCount;
	private int rspfCount;
	private boolean testFlag;

	public RspfGenerator(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	public RspfGenerator() {
		this(true, false);
	}
	private void write(String data) throws Exception {
		out.write(data.getBytes());
	}
	private void generateHeader() throws Exception {
		// header identifier
		write("H1");

		// version id
		write("100000");

		// merch numn
		write(GenUtil.padRight(rspf.getMerchNum(), 32));

		// end of line
		write("\n");
	}

	private String getResponseSource(ResponseRecord response) {
		return SysUtil.getResponseSourceCode(response.getSysCode());
	}

	private void generateDetail(ResponseRecord response) throws Exception {
		// card id token may be present
		boolean tokenFlag = response.getCardId() != null;

		// detail identifier
		// D1 - standard
		// D2 - card store id
		write(tokenFlag ? "D2" : "D1");

		// old account type
		write(GenUtil.padRight(response.getOldAcctType(), 4));

		// old account num
		// D1: old account num
		// D2: card id
		write(GenUtil.padRight(tokenFlag ? response.getCardId() : response.getOldAcctNum(), 32));

		// old exp date
		write(GenUtil.padRight(response.getOldExpDate(), 4));

		// new account type
		String acctType = response.getNewAcctType();
		acctType = "UNKN".equals(acctType) ? null : acctType;
		write(GenUtil.padRight(acctType, 4));

		// new account num
		// D1: new account num
		// D2: new account num truncated
		write(GenUtil.padRight(tokenFlag ? response.getNewAcctNumTrunc() : response.getNewAcctNum(), 32));

		// new exp date
		write(GenUtil.padRight(response.getNewExpDate(), 4));

		// response code
		write(GenUtil.padRight(response.getRspCode(), 8));

		// response source
		write(GenUtil.zeroPad(getResponseSource(response), 2));

		// discretionary data
		write(GenUtil.padRight(response.getDiscData(), 32));

		// end of line
		write("\n");
	}
	private void generateTrailer() throws Exception {
		// record indicator, detail = '1'
		write("T1");

		// detail count
		write(GenUtil.zeroPad("" + detailCount, 6));
	}
	/**
	 * Start a new response file based on the response record. Set output
	 * stream, generate a header record, reset the detail count.
	 */
	private void startFile(ResponseRecord response) throws Exception {
		// fetch the request file without loading data
		log.debug("fetching request file with id: " + response.getReqfId());
		RequestFile reqf = db.getRequestFile(response.getReqfId(), false);
		rspf = new ResponseFile();
		rspf.setCreateDate(Calendar.getInstance().getTime());
		rspf.setReqfId(response.getReqfId());
		rspf.setIbId(response.getIbId());
		rspf.setAusrName(reqf.getAusrName());
		rspf.setMerchNum(response.getMerchNum());
		rspf.setTestFlag(reqf.getTestFlag());
		out = rspf.getOutputStream();
		detailCount = 0;
		generateHeader();
	}

	/**
	 * Send notification of new response file to user.
	 */
	private void notifyUser() {
		if (notifier == null) {
			notifier = new Notifier(directFlag);
		}
		notifier.notifyResponseFile(rspf);
	}
	/**
	 * Finish response file. Generate trailer, store to db, notify user, release
	 * data, add to notice text, null response file.
	 */
	private void endFile() throws Exception {
		generateTrailer();
		try {
			out.flush();
		}
		catch (Exception e) {}
		finally {
			try {
				out.close();
			}
			catch (Exception e) {}
		}
		db.insertResponseFile(rspf, true);
		notifyUser();
		++rspfCount;
		noticeText.append("  " + rspf.getMerchNum() + " " + rspf.getFileName() + " " + rspf.size() + " bytes\n");
		rspf.release();
		rspf = null;
	}
	/**
	 * Returns true if a file has been started.
	 */
	private boolean fileStarted() {
		return rspf != null;
	}
	/**
	 * Returns true if no response file currently exists or the response record
	 * originated from a different request file than the current response file
	 * did.
	 */
	private boolean newFileNeeded(ResponseRecord response) {
		return !fileStarted() || rspf.getReqfId() != response.getReqfId();
	}
	/**
	 * Process a batch of response records.
	 */
	public void handleResponses(List responses) throws Exception {
		for (Iterator i = responses.iterator(); i.hasNext();) {
			ResponseRecord response = (ResponseRecord) i.next();
			if (newFileNeeded(response)) {
				if (fileStarted()) {
					endFile();
				}
				startFile(response);
			}
			generateDetail(response);
			++detailCount;
		}
	}
	/**
	 * Initiate response file generation.
	 */
	private void generateFiles() {
		try {
			log.debug("Initiating response file generation...");

			noticeText = new StringBuffer();
			// process all new response records
			db.handleNewResponseRecords(this, testFlag);

			// end last file
			if (fileStarted()) {
				endFile();
			}

			if (rspfCount > 0) {
				noticeText.insert(0, "Response files generated:\n\n");
				noticeText.append("\n");
			}

			noticeText.append("" + rspfCount + " response files generated");
			Notifier.notifyDeveloper("AUS Response File Generation", "" + noticeText, directFlag);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "generateFiles()");
		}
	}
	public static void generate(boolean directFlag, boolean testFlag) {
		(new RspfGenerator(directFlag, testFlag)).generateFiles();
	}
	public static void generate(boolean directFlag) {
		generate(directFlag, false);
	}
	public static void generate() {
		generate(true);
	}
}