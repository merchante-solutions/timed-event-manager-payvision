package com.mes.aus.file;

import java.io.File;
import org.apache.log4j.PropertyConfigurator;
import com.mes.aus.AusDb;
import com.mes.aus.Notifier;

public class AutomatedProcess implements Runnable {
	protected AusDb db;
	protected boolean directFlag;

	public AutomatedProcess(boolean directFlag) {
		this.directFlag = directFlag;
		db = new AusDb(directFlag);
	}
	public boolean isDirect() {
		return directFlag;
	}
	protected void notifyError(Exception e, String clue) {
		Notifier.notifyError(this, e, clue, directFlag);
	}
	public static void initLog4j(String fileName) {
		File log4jConfig = new File(fileName);
		if (log4jConfig.exists()) {
			PropertyConfigurator.configure(fileName);
		}
		else {
			System.out.println("Warning, " + fileName + " not found, log4j not configured...");
		}
	}
	public static void initLog4j() {
		initLog4j("log4j.cfg");
	}
	public void run() {
		throw new RuntimeException("Threading not enabled on process: " + this.getClass().getName());
	}
}