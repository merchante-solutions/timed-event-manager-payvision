package com.mes.aus;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

public class OutboundFile extends AusFileContainer
{
  static Logger log = Logger.getLogger(OutboundFile.class);

  private long      obId;
  private Date      createDate;
  private String    sysCode;
  private long      fileSeqId;
  private String    fileName;
  private List      mapList = new ArrayList();
  private String    deliveryFlag;
  private String    testFlag;
  private String    fileType;

  public void setObId(long obId)
  {
    this.obId = obId;
  }
  public long getObId()
  {
    return obId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  public String getSysCode()
  {
    return sysCode;
  }

  public void setFileSeqId(long fileSeqId)
  {
    this.fileSeqId = fileSeqId;
  }
  public long getFileSeqId()
  {
    return fileSeqId;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  public String getFileName()
  {
    return fileName;
  }

  public void setMapList(List mapList)
  {
    this.mapList = mapList;
  }
  public List getMapList()
  {
    return mapList;
  }

  public void addRfMapping(RequestFile reqf)
  {
    RfObMapping map = new RfObMapping();
    map.setReqfId(reqf.getReqfId());
    map.setObId(getObId());
    map.setSysCode(getSysCode());
    mapList.add(map);
  }
  public void addRfMapping(RfObMapping map)
  {
    mapList.add(map);
  }

  public void setDeliveryFlag(String deliveryFlag)
  {
    validateFlag(deliveryFlag,"delivery");
    this.deliveryFlag = deliveryFlag;
  }
  public String getDeliveryFlag()
  {
    return flagValue(deliveryFlag);
  }
  public boolean isDelivered()
  {
    return flagBoolean(deliveryFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public String getFileType()
  {
    return fileType;
  }
  public void setFileType(String fileType)
  {
    this.fileType = fileType;
  }
  
  public String toString()
  {
    return " OutboundFile [ "
      + "obId: " + obId
      + ", createDate: " + createDate
      + ", sysCode: " + sysCode
      + ", fileType: " + fileType
      + ", fileSeqId: " + fileSeqId
      + ", fileName: " + fileName
      + ", hasFileData: " + !empty()
      + ", test: " + testFlag
      + ", delivered: " + deliveryFlag
      + " ]";
  }
}
