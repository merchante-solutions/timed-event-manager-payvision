package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class InboundFile extends AusFileContainer
{
  static Logger log = Logger.getLogger(InboundFile.class);

  private long    ibId;
  private Date    createDate;
  private String  sysCode;
  private String  fileType;
  private String  fileName;
  private long    obId;
  private String  processFlag;
  private String  testFlag = FLAG_NO;

  public void setIbId(long ibId)
  {
    this.ibId = ibId;
  }
  public long getIbId()
  {
    return ibId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  public String getSysCode()
  {
    return sysCode;
  }

  public void setFileType(String fileType)
  {
    this.fileType = fileType;
  }
  public String getFileType()
  {
    return fileType;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  public String getFileName()
  {
    return fileName;
  }

  public void setObId(long obId)
  {
    this.obId = obId;
  }
  public long getObId()
  {
    return obId;
  }

  public void setProcessFlag(String processFlag)
  {
    validateFlag(processFlag,"process");
    this.processFlag = processFlag;
  }
  public String getProcessFlag()
  {
    return flagValue(processFlag);
  }
  public boolean isDelivered()
  {
    return flagBoolean(processFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public String toString()
  {
    return " InboundFile [ "
      + "ibId: " + ibId
      + ", createDate: " + createDate
      + ", sysCode: " + sysCode
      + ", hasFileData: " + !empty()
      + ", fileType: " + fileType
      + ", fileName: " + fileName
      + ", fileSize: " + size()
      + ", obId: " + obId
      + ", processed: " + processFlag
      + ", test: " + testFlag
      + " ]";
  }
}
