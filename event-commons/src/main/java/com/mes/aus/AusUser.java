package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AusUser extends AusBase
{
  static Logger log = Logger.getLogger(AusUser.class);

  private long    ausrId;
  private Date    createDate;
  private String  userName;
  private String  ausrName;
  private String  ausrDesc;
  private String  enabledFlag;
  private String  tokenFlag;
  private String  testFlag;
  private String  email;
  private String  notifyFlag;

  public void setAusrId(long ausrId)
  {
    this.ausrId = ausrId;
  }
  public long getAusrId()
  {
    return ausrId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setAusrName(String ausrName)
  {
    this.ausrName = ausrName;
  }
  public String getAusrName()
  {
    return ausrName;
  }

  public void setAusrDesc(String ausrDesc)
  {
    this.ausrDesc = ausrDesc;
  }
  public String getAusrDesc()
  {
    return ausrDesc;
  }

  public void setEnabledFlag(String enabledFlag)
  {
    validateFlag(enabledFlag,"enabled");
    this.enabledFlag = enabledFlag;
  }
  public String getEnabledFlag()
  {
    return flagValue(enabledFlag);
  }
  public boolean isEnabled()
  {
    return flagBoolean(enabledFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setEmail(String email)
  {
    this.email = email;
  }
  public String getEmail()
  {
    return email;
  }

  public void setNotifyFlag(String notifyFlag)
  {
    validateFlag(notifyFlag,"notify");
    this.notifyFlag = notifyFlag;
  }
  public String getNotifyFlag()
  {
    return flagValue(notifyFlag);
  }
  public boolean notificationEnabled()
  {
    return flagBoolean(notifyFlag);
  }

  public void setTokenFlag(String tokenFlag)
  {
    validateFlag(tokenFlag,"token");
    this.tokenFlag = tokenFlag;
  }
  public String getTokenFlag()
  {
    return flagValue(tokenFlag);
  }
  public boolean tokenizationEnabled()
  {
    return flagBoolean(tokenFlag);
  }

  public String toString()
  {
    return " AusUser [ "
      + "ausrId: " + ausrId
      + ", createDate: " + createDate
      + ", userName: " + userName
      + ", ausrName: " + ausrName
      + ", enabled: " + isEnabled()
      + ", tokenization: " + tokenizationEnabled()
      + ", testing: " + isTest()
      + ", email: " + (email != null ? email : "--")
      + ", notify enabled: " + notificationEnabled()
      + " ]";
  }
}
