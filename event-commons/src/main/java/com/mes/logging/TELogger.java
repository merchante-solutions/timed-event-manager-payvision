package com.mes.logging;

import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.constants.LoggerConstants;
import com.mes.events.Event;
//import com.mes.startup.EventBase;
import com.mes.events.TimedEvent;

/**
 * 
 * @author ssubbiah
 * @desc   Utility Class for Timed Event logging. 
 * 			 Uses Log4J for logging.
 * 
 */
public class TELogger {
	public static Logger logger=Logger.getLogger(TELogger.class);  
     
   /*
    * Log the event Start 
    * act=beg evtPID=<devName.eventId.uniqueid> evtId=<TE Event ID> evtName=<TE Event Name> evtFrq=<TE Event Freq> mType=INFO param=<TE params>
    */
    public static void logTEStart(TimedEvent te, Date dateTime) {
		logger.info(
				createLogEntry(LoggerConstants.LOGGER_TEM_ACT, LoggerConstants.LOGGER_TEM_BEGIN) +
				getTELogBase(te) + 
				createLogEntry(LoggerConstants.LOGGER_TEM_FREQUENCY, String.valueOf(te.type)) +
				createLogEntry(LoggerConstants.LOGGER_TEM_MTYPE, LoggerConstants.LOGGER_TEM_lOGLEVEL_INFO) +
				createLogEntry(LoggerConstants.LOGGER_TEM_PARAMS, te.eventArgs)
				);				
	}
    
    /*
     * Log the event end time
     * act=end evtPID=<evtPID from begin action> evtId=<TE Event ID> evtName=<TE Event Name> mType=INFO 
     */	
	public static void logTEEnd(TimedEvent te, Date dateTime, long elapsedTime) {
		logger.info(
				createLogEntry(LoggerConstants.LOGGER_TEM_ACT, LoggerConstants.LOGGER_TEM_END) +
				getTELogBase(te) + 
				createLogEntry(LoggerConstants.LOGGER_TEM_MTYPE, LoggerConstants.LOGGER_TEM_lOGLEVEL_INFO)
				);
	}
	
	/*
     * Log the event error triggered from the timed event itself 
     * act=error evtPID=<evtPID from begin action> evtId=<TE Event ID> evtName=<TE Event Name> mType=ERROR msg=<Error Message>
     */	
	public static void logTEError(Event e, String message) {
		logger.info(
				createLogEntry(LoggerConstants.LOGGER_TEM_ACT, LoggerConstants.LOGGER_TEM_ERROR) +
				getELogBase(e) +
				createLogEntry(LoggerConstants.LOGGER_TEM_MTYPE, LoggerConstants.LOGGER_TEM_lOGLEVEL_ERROR) +
				createLogEntry(LoggerConstants.LOGGER_TEM_MSG, message) 
				);				// Log error
	}
	
	/*
	 *  Log the error triggered from the TimedEvent class where the actual trigger and TIMED_EVENT table
	 *  related changes are done.
	 *  act=error evtPID=<evtPID from begin action> evtId=<TE Event ID> evtName=<TE Event Name> mType=ERROR msg=<Error Message>
	 */
	public static void logTEError(TimedEvent te, String message) {
		logger.info(
				createLogEntry(LoggerConstants.LOGGER_TEM_ACT, LoggerConstants.LOGGER_TEM_ERROR) +
				getTELogBase(te) +
				createLogEntry(LoggerConstants.LOGGER_TEM_MTYPE, LoggerConstants.LOGGER_TEM_lOGLEVEL_ERROR) +
				createLogEntry(LoggerConstants.LOGGER_TEM_MSG, message) 
				);				// Log error
	}
	
	public static void logError(String message) {
		logger.info(
				createLogEntry(LoggerConstants.LOGGER_TEM_ERROR, message));				// Log error
	}
	
	/*
	 * Create name value pairs for logging
	 */
	public static String createLogEntry(String name, Object value) {
		if (name != null)
			return name + LoggerConstants.FIELD_ASSIGNER + (value != null ? QUOTES + value.toString() + QUOTES : value) + LoggerConstants.FIELD_SEPARATOR; 
		return "";
	}
	
	/* 
	 *  evtPID=<devName.eventId.uniqueid> evtId=<TE Event ID> evtName=<TE Event Name>	
	 */
	private static String getTELogBase(TimedEvent te) {	
	   String baseLog = "";
	  
	   // Process Id
	   baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_PID, te.evtPID);      
	   // EventId
	   baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_TEM_ID, te.id+"" );                  	   
	   // Event Name
	   baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_TEM_EVENTNAME, te.name);             
	   	  	   
	   return baseLog;
	}

	/*
	 * evtPID=<devName.eventId.uniqueid>  	
	 */
	private static String getELogBase(Event e) {	
	   String baseLog = "";
	   
	   // Process Id
	   baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_PID, e.getEvtPID());      
	   // EventId
	   //baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_TEM_ID, te.id+"" );                  	   
	   // Event Name
	   //baseLog += TELogger.createLogEntry(LoggerConstants.LOGGER_TEM_EVENTNAME, te.name);             
	   	  	   
	   return baseLog;
	}

	private static final String QUOTES = "\"";
}
