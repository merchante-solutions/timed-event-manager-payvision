/*@lineinfo:filename=AmexFileBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/AmexFileBase.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.List;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordAmex;
import com.mes.settlement.SettlementTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.PropertiesFile;
import com.mes.support.TridentTools;
//import com.mes.settlement.InterchangeAmex;
import sqlj.runtime.ResultSetIterator;

public abstract class AmexFileBase extends SettlementFileBase
{
  private long              BatchMerchantId       = 0L;
  private long               BatchNumber           = 0L;
  private long              FileCreditsAmountLong = 0L;
  private long              FileDebitsAmountLong  = 0L;
  

  public AmexFileBase( )
  {
  }

  private void buildFileHeader( BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;

    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CAPN_TFH_V91);
      ffd.setFieldData("create_date_time"     , FileTimestamp);
      ffd.setFieldData("file_ref_num"         , FileReferenceId);
//    ffd.setFieldData("file_seq_num"         , ???);
      ffd.setFieldData("record_number"        , ++FileRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  private void buildObFileHeader( BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;

    try
    {
      
      PropertiesFile props = new PropertiesFile("optblue.properties");
      String sid = props.getString("sub_" + getEventArg(0));
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CAPN_TFH_OPTBLUE);
      ffd.setFieldData("submitter_id", sid);
      ffd.setFieldData("create_date_time"     , FileTimestamp);
      ffd.setFieldData("file_seq_num"         ,  SettlementDb.getOptblueSettleSeq() );
      ffd.setFieldData("file_ref_num"         , FileReferenceId);
      ffd.setFieldData("record_number"        , ++FileRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildObFileHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildFileTrailer( BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;

    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CAPN_TFS_V91);
      ffd.setFieldData("credits_amount"       , FileCreditsAmountLong );
      ffd.setFieldData("credits_count"        , FileCreditsCount );
      ffd.setFieldData("debits_amount"        , FileDebitsAmountLong );
      ffd.setFieldData("debits_count"         , FileDebitsCount );
      ffd.setFieldData("file_hash_amount"     , (FileDebitsAmountLong + FileCreditsAmountLong));
      ffd.setFieldData("record_number"        , ++FileRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }

  public void buildBatchTrailer( BufferedWriter out, String cc )
  {
    FlatFileRecord    ffd             = null;

    try
    {
      if(BatchRecordCount > 0){
          ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CAPN_TBT_V91);
          ffd.setFieldData("batch_number"           , BatchNumber);
          ffd.setFieldData("creation_date"          , FileTimestamp);
          ffd.setFieldData("net_amount"             , TridentTools.encodeAmount((BatchDebitsAmount - BatchCreditsAmount),20,cc));
          ffd.setFieldData("record_number"          , ++FileRecordCount);
          ffd.setFieldData("amex_se_number"         , BatchMerchantId);
          ffd.setFieldData("tab_count"              , BatchRecordCount);
          ffd.setFieldData("currency_code"          , cc);

          // toFractionalAmount will clean up any Java floating point
          // inaccuracy (i.e. when value sb 28.00 and is 27.9999999998)
          // test to see if there is a negative sign
          String netAmountStr = MesMath.toFractionalAmount((BatchDebitsAmount - BatchCreditsAmount),2,2);
          if ( !netAmountStr.equals("-0.00") && netAmountStr.indexOf("-") >= 0 )
          {
            ffd.setFieldData("net_amount_sign"        ,"-");
          }
          else
          {
            ffd.setFieldData("net_amount_sign"        ,"+");
          }
          out.write( ffd.spew() );
          out.newLine();

          ++FileBatchCount;
      }else{
    	  log.error("Not generating batch trailer for empty batch with batch id: " + BatchNumber);
      }
      

      BatchCreditsAmount  = 0.0;
      BatchDebitsAmount   = 0.0;
      BatchRecordCount    = 0;
      BatchCreditsCount   = 0;
      BatchDebitsCount    = 0;
    }
    catch(Exception e)
    {
      logEntry("buildBatchTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }

  private void buildDetailRecords( BufferedWriter out, SettlementRecord rec, boolean reversal )
  {
    long                  batchId         = 0L;
    long                  batchRecId      = 0L;
    FlatFileRecord        ffd             = null;
    List                  ffds            = null;
    int                   retryCount      = 0;

    try
    {
      // store these in case of error
      batchId     = rec.getLong("batch_id");
      batchRecId  = rec.getLong("batch_record_id");

      // if reversal, invert transaction type so flat file builds correctly
      if( reversal )
      {
        boolean makeReturn = "D".equals(rec.getData("debit_credit_indicator"));

        rec.setData("debit_credit_indicator", makeReturn ?  "C"  : "D"  );
      }

      while(true)
      {
        try
        {
          ffds = rec.buildNetworkRecords(SettlementRecMT);
          break;
        }
        catch( Exception e )
        {
          if ( ++retryCount > 10 )
          {
            throw(e);
          }
        }
      }

      log.debug("flatFileCount: " + ffds.size());
      for( int i = 0; i < ffds.size(); ++i )
      {
        ffd = (FlatFileRecord)ffds.get(i);
        ffd.setFieldData("record_number",++FileRecordCount);

        out.write(ffd.spew());
        out.newLine();
      }

      // increment the counters and update the amounts
      double tranAmount       = rec.getDouble("transaction_amount");
      long   tranAmountLong   = Long.parseLong(TridentTools.encodeAmount(tranAmount,10,rec.getString("currency_code")));

      BatchTranCount      ++;
      FileTranCount       ++;
      BatchRecordCount    ++;//= ffds.size(); /////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      if ( "C".equals(rec.getString("debit_credit_indicator")) )
      {
        BatchCreditsCount     ++;
        FileCreditsCount      ++;
        BatchCreditsAmount    += tranAmount;
        FileCreditsAmount     += tranAmount;
        FileCreditsAmountLong += tranAmountLong;
      }
      else
      {
        BatchDebitsCount      ++;
        FileDebitsCount       ++;
        BatchDebitsAmount     += tranAmount;
        FileDebitsAmount      += tranAmount;
        FileDebitsAmountLong  += tranAmountLong;
      }
    }
    catch(Exception e)
    {
      logEntry("buildDetailRecords(" + batchId + "," + batchRecId + ")", e.toString());
    }
  }

  protected void closeFile( BufferedWriter out, String cc )
    throws java.io.IOException
  {
    buildBatchTrailer(out,cc);// end the last batch
    buildFileTrailer(out);    // file trailer

    out.close();
  }
  
  public Date getSettlementDate()
  {
    return( SettlementTools.getSettlementDateAmex() );
  }
  
  protected void handleSelectedRecords( ResultSetIterator it, String workFilename,long workFileId )
    throws Exception
  {
    String                    cc                = "USD";    // default is USD
    SettlementDb              db                = new SettlementDb();
    BufferedWriter            out               = null;
    SettlementRecordAmex      rec               = new SettlementRecordAmex();
    ResultSet                 resultSet         = null;
    HashSet<String>           dtprocessList     = new HashSet<>();

    try
    {
      db.connect(true);

      resultSet = it.getResultSet();

      log.debug("Work Filename : " + workFilename);

      if( resultSet.next() )
      {
        long    batchId   = 0L;
        long    merchId   = 0L;

        out = new BufferedWriter( new FileWriter( workFilename, false ) );

        // build the data file header
        buildFileHeader(out);

        int recCount = 0;
        do
        {
          if ( transactionToSkip(resultSet) )continue;

          if( merchId  != resultSet.getLong("merchant_number" ) ||
              batchId  != resultSet.getLong("batch_id"        ) )
          {
            // if have built any details, end current batch
            if( recCount != 0L )
              buildBatchTrailer(out,cc);
              
            // update local variables to the values from the new row
            merchId     = resultSet.getLong  ("merchant_number");
            batchId     = resultSet.getLong  ("batch_id"       );
            cc          = resultSet.getString("currency_code"  );
            
            // older transactions will have a null for currency code
            // if an older record is detected, default to 840 (USD)
            if ( cc == null || cc.trim().equals("") )
            {
              cc = "840";
            }

            // store the amex account for use in the trailer record
            BatchMerchantId   = resultSet.getLong("amex_se_number");
            BatchNumber       = resultSet.getLong("batch_id");
          }

          rec.clear();
          rec.setIsEmvEnabled(EmvProperties.containsKey("amex") && EmvProperties.get("amex").equals("yes") ? true : false);
          rec.setFields(resultSet, false, false);
          //Decrypt the card number
    	  try {
    	    rec.getField("card_number_full").setData( rec.getCardNumberFull() );
    	  } catch (Exception ex) {
    		log.error(ex);
          }
          if ( rec.hasLevelIIIData() )
          {
            if( ActionCode == null || "X".equals(ActionCode) )
            {
              rec.setLineItems( db._loadSettlementLineItemDetailRecords(rec.getLong("rec_id"),SettlementRecord.SETTLE_REC_AMEX) );
            }
          }
          
//          // check interchange on re-procs
//          if( "X".equals(ActionCode) )    
//          {
//            if ( ! rec.isCashDisbursement() )
//            {
//              String  icCodeOriginal  = rec.getString("ic_cat");
//              new InterchangeAmex().getBestClass(rec,SettlementDb.loadIcpListAmex(rec));
//              if ( icCodeOriginal == null || !icCodeOriginal.equals( rec.getString("ic_cat") ) )
//              {
//                rec.setData("ic_cat_downgrade",rec.getString("ic_cat"));
//                SettlementDb.updateIcCatDowngrade(rec);
//              }
//            }
//          }
          
          if("X".equals(ActionCode)) {
        	  if(! rec.isCashDisbursement()) {
        		  if ("N".equals(rec.getData("external_reject"))) {
        			  rec.setData("ach_flag", "Y");
        			  rec.setData("load_filename",workFilename);
        			  rec.setData("load_file_id", workFileId);
        			  SettlementDb.updateDTRecord(rec);
                      dtprocessList.add(DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"), "dd-MMM-yyyy"));
                  }
        	  }
          }
          
          buildDetailRecords( out, rec, transactionToReverse(resultSet) );

          log.debug("Processed: " + String.valueOf(++recCount));
        } while( resultSet.next() );
     
        for (String batchdate : dtprocessList) {
             SettlementDb.insertDTProcessRecord(1, workFilename, batchdate);
        }

        log.debug("\n\n");

        // write the final batch and file trailers, close file handle
        closeFile(out,cc);
        sendFile(workFilename);
      }
      else
      {
        log.debug("No records selected.\n");
      }

      resultSet.close();
    }
    finally
    {
      try{ db.cleanUp(); } catch(Exception e) {}
    }
  }
  
protected void handleSelectedOBRecords(ResultSet resultSet, String workFilename) throws Exception {
	handleSelectedOBRecords(resultSet, workFilename, 0l);
}
protected void handleSelectedOBRecords(ResultSet resultSet, String workFilename,long workFileId ) throws Exception {
	String cc = "USD"; // default is USD
	SettlementDb db = new SettlementDb();
	BufferedWriter out = null;
    HashSet<String> dtprocessList = new HashSet<>();
	SettlementRecordAmex rec = new SettlementRecordAmex();
	try {
		db.connect(true);
		log.debug("Work Filename : " + workFilename);
 	    if (resultSet.next()) {
 	    	long batchId = 0L;
			long merchId = 0L;
			out = new BufferedWriter(new FileWriter(workFilename, false));
				// build the data file header
			buildObFileHeader(out);
			
			int recCount = 0;
			do {
				if (transactionToSkip(resultSet))
					continue;
				if (merchId != resultSet.getLong("merchant_number")
					        || batchId != resultSet.getLong("batch_id")) {
					// if have built any details, end current batch
					if (recCount != 0L)
						buildBatchTrailer(out, cc);
						// update local variables to the values from the new row
					merchId = resultSet.getLong("merchant_number");
					batchId = resultSet.getLong("batch_id");
					cc = resultSet.getString("currency_code");
					// older transactions will have a null for currency code
					// if an older record is detected, default to 840 (USD)
					if (cc == null || cc.trim().equals("")) {
						cc = "840";
					}
					// store the amex account for use in the trailer record
					BatchMerchantId = resultSet.getLong("amex_se_number");
					BatchNumber = resultSet.getLong("batch_id");
				}

				rec.clear();
				rec.setIsEmvEnabled(EmvProperties.containsKey("amex") && EmvProperties.get("amex").equals("yes") ? true : false);
				rec.setFields(resultSet, false, false);
		        //Decrypt the card number
		    	try {
		    	    rec.getField("card_number_full").setData( rec.getCardNumberFull() );
		    	} catch (Exception ex) {
		    		  log.error(ex);
		        }
				if (rec.hasLevelIIIData()) {
					if (ActionCode == null || "X".equals(ActionCode)) {
						rec.setLineItems(db._loadSettlementLineItemDetailRecords(rec.getLong("rec_id"),SettlementRecord.SETTLE_REC_AMEX));
					}
				}
				
                if("X".equals(ActionCode)) {
                    if(! rec.isCashDisbursement()) {
                        if ("N".equals(rec.getData("external_reject"))) {
                            rec.setData("ach_flag", "Y");
                            rec.setData("load_filename",workFilename);
                            rec.setData("load_file_id", workFileId);
                            SettlementDb.updateDTRecord(rec);
                            dtprocessList.add(DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"), "dd-MMM-yyyy"));
                        }
                    }
                 }
				
	            buildDetailRecords(out, rec, transactionToReverse(resultSet));
                log.debug("Processed: " + String.valueOf(++recCount));
			} while (resultSet.next());
			
            for (String batchdate : dtprocessList) {
                 SettlementDb.insertDTProcessRecord(1, workFilename, batchdate);
            }

			log.debug("\n\n");            
           
			// write the final batch and file trailers, close file handle
			closeFile(out, cc);
			sendFile(workFilename);
        } else {
			log.debug("No records selected.\n");
		}
		resultSet.close();
	} finally {
		try {
			db.cleanUp();} catch (Exception e) { }
		}
	}
 
}/*@lineinfo:generated-code*/