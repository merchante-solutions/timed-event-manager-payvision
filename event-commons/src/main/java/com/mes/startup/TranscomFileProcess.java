/*@lineinfo:filename=TranscomFileProcess*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomFileProcess.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-10-23 17:34:23 -0700 (Fri, 23 Oct 2015) $
  Version            : $Revision: 23901 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.CSVFile;
import com.mes.support.CSVFileDisk;
import com.mes.support.CSVFileMemory;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class TranscomFileProcess extends EventBase
{
  static Logger log = Logger.getLogger(TranscomFileProcess.class);

  public final static int     FILE_TYPE_UNKNOWN   = -1;
  public final static int     FILE_TYPE_ACH       = 1;
  public final static int     FILE_TYPE_EXTRACT   = 2;
  public final static int     FILE_TYPE_MERCHANT  = 3;
  public final static int     FILE_TYPE_EQUIPMENT = 4;
  
  public String  ARCHIVE_HOST        = "";
  public String  ARCHIVE_USER        = "";
  public String  ARCHIVE_PASSWORD    = "";
  public final static String  ARCHIVE_PATH        = "arc/transcom";
  
  protected int       procSequence      = 0;
  protected int       fileType          = 0;
  
  protected CSVFile     csvFile         = new CSVFileMemory();

  protected String    topNode           = "";
  protected boolean   mesFile           = false;
  protected boolean   testMode          = false;
  
  public TranscomFileProcess()
  {
    super(true);
    this.fileType = FILE_TYPE_UNKNOWN;
    
    setFileProps();
  }
  
  public TranscomFileProcess(int fileType)
  {
    // enable auto-commit
    super(true);
    
    this.fileType = fileType;
    
    setFileProps();
  }
  
  public TranscomFileProcess(int fileType, String connectionString)
  {
    // enable auto-commit
    super(connectionString, true);
    
    this.fileType = fileType;
    
    setFileProps();
  }
  
  private void setFileProps()
  {
    try
    {
      ARCHIVE_HOST        = MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST);
      ARCHIVE_USER        = MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER);
      ARCHIVE_PASSWORD    = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS);
    }
    catch(Exception e)
    {
      logEntry("setFileProps()", e.toString());
    }
  }
  
  public String blankIfNull(String test)
  {
    String result = "";
    
    try
    {
      if(test != null)
      {
        // parse and remove quotes or commas because they kill the csv file
        result = test.replace('"', ' ');
        result = result.replace(',', ' ');
      }
    }
    catch(Exception e)
    {
      logEntry("blankIfNull(" + test + ")", e.toString());
      result = "";
    }
    
    return result;
  }
  
  protected String getMonthlyFilenameBase()
  {
    return( DateTimeFormatter.getCurDateString("MMyyyy") );
  }
  
  /*
  ** METHOD getProcSequence
  **
  ** Establishes the transcom process sequence
  */
  private void getProcSequence()
  {
    try
    {
      // get the processSequence
      /*@lineinfo:generated-code*//*@lineinfo:148^7*/

//  ************************************************************
//  #sql [Ctx] { select  transcom_process_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  transcom_process_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TranscomFileProcess",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/
      
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "getProcSequence()", e.toString());
      setError("getProcSequence(" + fileType + "): " + e.toString());
      logEntry("getProcSequence(" + fileType + ")", e.toString());
    }
  }
  
  /*
  ** METHOD preProcess
  **
  ** Can be overloaded by extending class if necessary.  This default version
  ** simply marks the entries in transcom_process that must be accessed by
  ** setting the process_sequence column value.
  **
  */
  protected void preProcess()
  {
    try
    {
      // mark the necessary records in transcom_process
      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process
//          set     process_sequence = :procSequence
//          where   file_type = :fileType and
//                  process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process\n        set     process_sequence =  :1 \n        where   file_type =  :2  and\n                process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TranscomFileProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,fileType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "preProcess()", e.toString());
      setError("preProcess(" + fileType + ", " + procSequence + "): " + e.toString());
      logEntry("preProcess(" + fileType + ", " + procSequence + ")", e.toString());
    }
  }
  
  /*
  ** METHOD loadCSVFile
  **
  ** Loads the CSV file with the data pertaining to the filenames specified
  **
  ** MUST BE OVERLOADED BY EXTENDING CLASS
  */
  protected void loadCSVFile(Vector loadNames)
  {
  }
  
  /*
  ** METHOD sendStatusEmail
  **
  ** Sends email with the success/failure of the transmission
  */
  private void sendStatusEmail(boolean success, String fileName, String message)
  {
    String        fileTypeString  = "";
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      switch(fileType)
      {
        case FILE_TYPE_ACH:
          fileTypeString = "Deposit File";
          break;
        case FILE_TYPE_EXTRACT:
          fileTypeString = "Extract File";
          break;
        case FILE_TYPE_MERCHANT:
          fileTypeString = "Merchant File";
          break;
        case FILE_TYPE_EQUIPMENT:
          fileTypeString = "Equipment File";
          break;
      }
    
      if(success)
      {
        subject.append("Transcom SUCCESS (");
      }
      else
      {
        subject.append("Transcom FAIL (");
      }
      
      subject.append(fileTypeString);
      subject.append(")");
      
      body.append(fileName);
      body.append("\n");
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_TRANSCOM_TRANSMIT_SUCCESS);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_TRANSCOM_TRANSMIT_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  /*
  ** METHOD transmitCSVFile
  **
  ** Transmits the CSV file to the desired destination.
  */
  protected void transmitCSVFile()
  {
    String        fileName        = "";
    CSVFileDisk   csvFileDisk     = null;
    String        dateString      = DateTimeFormatter.getCurDateString("ddMMyyyy");
    int           sequence        = (procSequence % 99999) + 1;
    DecimalFormat df              = new DecimalFormat("00000");
    String        sequenceString  = df.format(sequence);
    
    String        ftpHost         = "";
    String        ftpUser         = "";
    String        ftpPassword     = "";
    String        ftpPath         = "";
    
    String        statusString    = "";
    boolean       success         = false;
    String        progress        = "";

    try
    {
      switch(fileType)
      {
        case FILE_TYPE_ACH:
          fileName    = dateString + ".act";
          ftpHost     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_HOST);
          ftpUser     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_USER);
          ftpPassword = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_PASSWORD);
          ftpPath     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_PATH);
          break;
          
        case FILE_TYPE_EXTRACT:
          fileName    = getMonthlyFilenameBase() + ".res";
          ftpHost     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_HOST);
          ftpUser     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_USER);
          ftpPassword = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_PASSWORD);
          ftpPath     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_ACTIVITY_PATH);
          csvFileDisk = (CSVFileDisk)csvFile;
          break;
                    
        case FILE_TYPE_MERCHANT:
          if(mesFile)
          {
            fileName    = sequenceString + dateString + ".mes";
          }
          else
          {
            fileName    = sequenceString + dateString + ".mer";
          }
          ftpHost     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_HOST);
          ftpUser     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_USER);
          ftpPassword = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_PASSWORD);
          ftpPath     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_PATH);
          break;
          
        case FILE_TYPE_EQUIPMENT:
          fileName    = sequenceString + dateString + ".equip";
          ftpHost     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_HOST);
          ftpUser     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_USER);
          ftpPassword = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_PASSWORD);
          ftpPath     = MesDefaults.getString(MesDefaults.DK_TRANSCOM_MIF_PATH);
          break;
          
        default:
          break;
      }
      
      log.debug("output file is named: " + fileName);
      
      if(fileName != null && ! fileName.equals(""))
      {
        if(testMode)
        {
          log.debug("\n\nFILE OUTPUT:\n\n");
          log.debug(csvFile.toString());
          log.debug("\n");
        }
        else
        {
          // send file to Transcom host
          try
          {
            log.debug("SFTP data file");
            progress = "SFTP data file";
            if ( csvFileDisk != null )    // disk based CSV file
            {
              ftpPut( csvFileDisk.getFilename(), ftpHost, ftpUser, ftpPassword, ftpPath, false);
            }
            else    // default is RAM based CSV file
            {
              ftpPut( fileName, csvFile.toString().getBytes(), ftpHost, ftpUser, ftpPassword, ftpPath, false);
            }
            
            success = true;
            statusString = "success";
          }
          catch(Exception ftpe)
          {
            statusString = ftpe.toString();
            logEntry("transmitCSVFile(" + fileType + ", " + procSequence + ")", progress + ": " + ftpe.toString());
            log.error("transmitCSVFile(" + fileType + ", " + procSequence + ") -- " + progress + ": " + ftpe.toString());
          }
          
          // send the file to archive host
          try
          {
            log.debug("ARCHIVE data file");
            progress = "ARCHIVE data file";
            
            if ( csvFileDisk != null )    // disk based CSV file
            {
              ftpPut( csvFileDisk.getFilename(), ARCHIVE_HOST, ARCHIVE_USER, ARCHIVE_PASSWORD, ARCHIVE_PATH, false);
            }
            else    // default is RAM based CSV file
            {
              ftpPut( fileName, csvFile.toString().getBytes(), ARCHIVE_HOST, ARCHIVE_USER, ARCHIVE_PASSWORD, ARCHIVE_PATH, false);
            }
            
            log.debug("ARCHIVE data file success");
          }
          catch(Exception ftpe2)
          {
            logEntry("ARCHIVE transmitCSVFile(" + fileType + ", " + procSequence + ")", ftpe2.toString());
          }
          
          sendStatusEmail(success, fileName, statusString);
        }
      }
      else
      {
        setError("transmitCSVFile(" + fileType + "): fileName is blank");
      }
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "transmitCSVFile()", e.toString());
      logEntry("transmitCSVFile(" + fileType + ", " + procSequence + ")", e.toString());
    }
  }
  
  /*
  ** METHOD markFilesProcessed
  */
  protected void markFilesProcessed()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:421^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process
//          set     process_date      = sysdate
//          where   file_type         = :fileType and
//                  process_sequence  = :procSequence and
//                  process_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process\n        set     process_date      = sysdate\n        where   file_type         =  :1  and\n                process_sequence  =  :2  and\n                process_date is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.TranscomFileProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,fileType);
   __sJT_st.setInt(2,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "markFilesProcessed()", e.toString());
      setError("markFilesProcessed(" + fileType + "): " + e.toString());
      logEntry("markFilesProcessed(" + fileType + ")", e.toString());
    }
  }
  
  /*
  ** METHOD getAndProcess
  **
  ** Gets a list of the files that must be processed
  */
  private void getAndProcess()
  {
    ResultSetIterator it        = null;
    Vector            loadNames = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:450^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  load_filename
//          from    transcom_process
//          where   file_type = :fileType and
//                  process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  load_filename\n        from    transcom_process\n        where   file_type =  :1  and\n                process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TranscomFileProcess",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,fileType);
   __sJT_st.setInt(2,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TranscomFileProcess",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        loadNames.add(rs.getString("load_filename"));
      }
      
      it.close();
      
      loadCSVFile(loadNames);

      if ( csvFile.rowCount() > 0 )
      {      
        // send the file
        transmitCSVFile();
      }        
      
      // done, mark the files as processed
      if(! getError())
      {
        markFilesProcessed();
      }
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "getAndProcess()", e.toString());
      logEntry("getAndProcess()", e.toString());
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      log.debug("\nExecuting TranscomFileProcess for fileType = " + fileType);
      
      // set up default parameters
      topNode     = "3941" + MesDefaults.getString(MesDefaults.DK_TRANSCOM_TOP_NODE);
      
      // establish process sequence (procSequence)
      getProcSequence();
      
      if(! getError())
      {
        // pre-process (get sequence, mark files for process)
        preProcess();
      }
      
      if(! getError())
      {
        // get all the files necessary for processing and process them
        getAndProcess();
      }
      
      log.debug("Transcom file process complete\n");
      
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute(" + fileType + ", " + procSequence + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return true;
  }
}/*@lineinfo:generated-code*/