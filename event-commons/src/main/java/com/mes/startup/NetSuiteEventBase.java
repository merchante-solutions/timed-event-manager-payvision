package com.mes.startup;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.xml.soap.SOAPException;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MesNetTools;
import com.netsuite.webservices.platform.core_2019_1.DataCenterUrls;
import com.netsuite.webservices.platform.core_2019_1.Passport;
import com.netsuite.webservices.platform.core_2019_1.Record;
import com.netsuite.webservices.platform.core_2019_1.RecordRef;
import com.netsuite.webservices.platform.core_2019_1.Status;
import com.netsuite.webservices.platform.messages_2019_1.ApplicationInfo;
import com.netsuite.webservices.platform.messages_2019_1.Preferences;
import com.netsuite.webservices.platform.messages_2019_1.SessionResponse;
import com.netsuite.webservices.platform.messages_2019_1.WriteResponse;
import com.netsuite.webservices.platform_2019_1.NetSuiteBindingStub;
import com.netsuite.webservices.platform_2019_1.NetSuitePortType;
import com.netsuite.webservices.platform_2019_1.NetSuiteServiceLocator;

public class NetSuiteEventBase extends EventBase
{
  static    Logger   log                 = Logger.getLogger(NetSuiteEventBase.class);
  
  public static final int    PT_DAILY                   = 1;
  public static final int    PT_MONTHLY                 = 2;
  public static final int    REC_TYPE_JE                = 1;
  public static final int    REC_TYPE_IV                = 2;
  public static final String CURRENCY_ID_USD            = "1";
  public static final double CURRENCY_XRATE_USD         = 1.00;
  
  protected int DailyUploadEmailGroup                   = MesEmails.MSG_ADDRS_NS_DAILY_UPLOAD_NOTIFY;   //174
  protected int MonthlyUploadEmailGroup                 = MesEmails.MSG_ADDRS_NS_MONTHLY_UPLOAD_NOTIFY; //175
  
  public static final String[] BANK_NUMBERS =
  {
    "3941",
    "3943",
    "3858",
    "3003"
  };
  
  public static final String[][] JE_CUSTOM_FORMS =
  {
    { "30"  , "Standard Journal Entry" },
    { "102" , "Cielo Merchant Reserve Journal Entry" },
    { "105" , "Cielo Invoice - Merchants" },
  };

  public static final String[][] CUSTOMER =
  {
    { "4561", "MES Month End Customer" },
  };

  public static final String[][] SUBSIDIARY =
  {
    { "2", "Merchant e-Solutions" },
  };
  
  protected boolean  TestMode            = false;
  protected String   WsdlUrl             = null;
  protected String   WebServiceUrl       = null;
  protected String   LoginEmail          = null;
  protected String   LoginPassword       = null;
  protected String   LoginRole           = null;
  protected String   LoginAccount        = null;
  protected String   applicationId		 = null;
  protected NetSuitePortType Port        = null;
  public    Date     ActiveDate          = null;
  public    Vector   LogEntryRows        = new Vector();
  public    Vector   Responses           = new Vector();
  public    Record[]        RecordList   = null;
  public    WriteResponse[] ResponseList = null;

  public NetSuiteEventBase()
  {
    PropertiesFilename     = "ns-client.properties";
  }
  
  public class RowData
  {
    public    String      BankNumber        = null;
    public    String      PortfolioName     = null;
    public    String      PortfolioId       = null;
    public    Vector      Lines             = new Vector();
    
    public RowData( ResultSet rs )
      throws java.sql.SQLException
    {
      try{ BankNumber      = rs.getString("bank_number"); }catch( java.sql.SQLException e ){}
      try{ PortfolioName   = rs.getString("portfolio_name"); }catch( java.sql.SQLException e ){}
      try{ PortfolioId     = rs.getString("portfolio_id"); }catch( java.sql.SQLException e ){}
    }
    
    public String getBankNumber()      { return BankNumber; }
    public String getPortfolioName()   { return PortfolioName; }
    public String getPortfolioId()     { return PortfolioId; }
    public Vector getLines()           { return Lines; }
  }
  
  public class LogEntryRow
  {
    public    Date     ActiveDate;
    public    int      BankNumber;
    public    int      ItemId;
    public    String   ItemName;
    public    int      ProcessType;
    public    int      RecordType;
    public    String   Status;
    
    public LogEntryRow( int processType, int recordType, Date activeDate )
    {
      ProcessType     = processType;
      RecordType      = recordType;
      ActiveDate      = activeDate;
    }
    
    public Date    getActiveDate()    { return ActiveDate; }
    public int     getBankNumber()    { return BankNumber; }
    public String  getItemName()      { return ItemName; }
    public int     getItemId()        { return ItemId; }
    public int     getProcessType()   { return ProcessType; }
    public int     getRecordType()    { return RecordType; }
    public String  getStatus()        { return Status; }
    public void    setBankNumber( int bankNumber )    { BankNumber = bankNumber; }
    public void    setItemName( String itemName )     { ItemName = itemName; }
    public void    setItemId( int itemId )            { ItemId = itemId; }
    public void    setStatus( String status )         { Status = status; }
  }
  
  public class LineItem
  {
    public    String      ItemName          = null;
    public    String      ItemId            = null;
    public    String      Amount            = null;
    
    public LineItem( String itemId, String itemName, String amount )
    {
      ItemId       = itemId;
      ItemName     = itemName;
      Amount       = amount;
    }
    
    public LineItem( String itemId, String itemName, double amount )
    {
      this( itemId, itemName, String.valueOf(amount) );
    }
    
    public LineItem( String itemId, double amount )
    {
      this( itemId, null, String.valueOf(amount) );
    }
    
    public LineItem( String itemId, String amount )
    {
      this( itemId, null, amount );
    }
    
    public String  getItemId()        { return ItemId; }
    public String  getItemName()      { return ItemName; }
    public String  getStringAmount()  { return Amount; }
    public double  getDoubleAmount()  { return Double.parseDouble(Amount); }
    public long    getLongAmount()    { return Long.parseLong(Amount); }
    public void    setAmount( String amount )   { Amount = amount; }
    public void    setAmount( double amount )   { Amount = String.valueOf(amount); }
  }
  
  /*
   * Since 12.2 endpoint accounts are located in multiple datacenters with different domain names.
   * In order to use the correct one, wrap the Locator and get the correct domain first.
   */
  private static class DataCenterAwareNetSuiteServiceLocator extends NetSuiteServiceLocator
  {
    private String account;
    
    public DataCenterAwareNetSuiteServiceLocator(String account)
    {
      this.account = account;
    }
    
    public NetSuitePortType getNetSuitePort(URL defaultWsDomainURL)
    {
      try
      {
        NetSuitePortType nsPort = super.getNetSuitePort(defaultWsDomainURL);
        // Get the webservices domain for user account
        DataCenterUrls urls   = nsPort.getDataCenterUrls(account).getDataCenterUrls();
        String wsDomain       = urls.getWebservicesDomain();
        // Return URL appropriate for the specific account
        return super.getNetSuitePort(new URL(wsDomain.concat(defaultWsDomainURL.getPath())));
      }
      catch (Exception e)
      {
        throw new RuntimeException(e);
      }
    }
  }
  
  public void logResponses()
  {
    PreparedStatement ps           = null;
    String            qs           = null;
    
    try
    {
      connect();
      for( int i = 0; i < LogEntryRows.size();  i++ )
      {
        LogEntryRow response = (LogEntryRow)LogEntryRows.elementAt(i);
        
        qs = " insert into netsuite_process_log " +
             " (                                " + 
             "   process_type,                  " + 
             "   record_type,                   " + 
             "   active_date,                   " + 
             "   bank_number,                   " + 
             "   item_name,                     " + 
             "   message,                       " + 
             "   db_timestamp                   " + 
             " )                                " + 
             " values                           " + 
             " ( ?,?,?,?,?,?,sysdate )";
        ps = con.prepareStatement(qs);
        ps.setInt(    1, response.getProcessType() );
        ps.setInt(    2, response.getRecordType() );
        ps.setDate(   3, response.getActiveDate() );
        ps.setInt(    4, response.getBankNumber() );
        ps.setString( 5, response.getItemName() );
        ps.setString( 6, response.getStatus() );
        ps.executeUpdate();
      }
    }
    catch( SQLException e )
    {
      logEntry( "logResponses()", e.toString() );

    }
    finally
    {
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  public void uploadData()
  {
    try
    {
      if( RecordList != null )
      {
        log.debug("uploadData() RecordList != null");

        login();
        ResponseList    = (Port.addList(RecordList)).getWriteResponse();
        logout();
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "addList()", e.toString() );
    }
  }
  
  public NetSuitePortType getNetSuitePort()
  {
    return Port;
  }
  
  public String getInvoiceLineItem(int itemIdx, int itemName )
  {
    return("");
  }
  
  public String getJournalEntryLineItem(int itemIdx, int itemName )
  {
    return("");
  }
  
  public String getStatus( int index )
  {
    String   status    = null;
    
    try
    {
      WriteResponse response  = ResponseList[index];
      if( response.getStatus().isIsSuccess() )
      {
        status = "success";
      }
      else
      {
        status = getStatusDetails(response.getStatus());
      }
    }
    catch( Exception e )
    {
    }
    
    return status;
  }
  
  public String getStatusDetails(Status status)
  {
    StringBuffer sb = new StringBuffer();
    
    try
    {
      for (int i = 0; i < status.getStatusDetail().length; i++)
      {
        sb.append(status.getStatusDetail()[i].getCode() + " - " + status.getStatusDetail()[i].getMessage() + " ");
      }
    }
    catch( Exception e )
    {
    }
    
    return sb.toString();
  }
  
  public void locateNetSuiteWebService() throws Exception
  {
    WsdlUrl           = MesDefaults.getString(MesDefaults.DK_NETSUITE_WSDL_URL);
    WebServiceUrl     = ( TestMode ? MesDefaults.getString(MesDefaults.DK_NETSUITE_WS_TEST_URL) : MesDefaults.getString(MesDefaults.DK_NETSUITE_WS_PROD_URL) );
    LoginEmail        = MesDefaults.getString(MesDefaults.DK_NETSUITE_LOGIN_EMAIL);
    LoginPassword     = MesDefaults.getString(MesDefaults.DK_NETSUITE_LOGIN_PASSWORD);
    LoginRole         = MesDefaults.getString(MesDefaults.DK_NETSUITE_LOGIN_ROLE);
    LoginAccount      = MesDefaults.getString(MesDefaults.DK_NETSUITE_LOGIN_ACCOUNT);
    applicationId	  = MesDefaults.getString(MesDefaults.DK_NETSUITE_APPLICATION_ID);
    
    MesNetTools.setupSecureSocketFactory();
    
    // Locate the NetSuite web service.
    NetSuiteServiceLocator service = new DataCenterAwareNetSuiteServiceLocator( LoginAccount );
    
    // Enable client cookie management. This is required.
    service.setMaintainSession(true);
    
    // Get the service port (to the correct datacenter)
    Port = service.getNetSuitePort(new URL(WebServiceUrl));
    
    // Setting client timeout to 2 hours for long running operations
    ((NetSuiteBindingStub) Port).setTimeout(1000 * 60 * 60 * 2);
  }
  
  public boolean login()
  {
    boolean retVal = false;
    
    try
    {
      if( Port == null )
      {
        locateNetSuiteWebService();
      }
      
      setPassportHeader();
      SessionResponse response = Port.login(prepareLoginPassport());
      
      if (response.getStatus().isIsSuccess())
      {
        retVal = true;
      }
      else
      {
        logEntry( "login() ", getStatusDetails(response.getStatus()) );
      }
    }
    catch( Exception e )
    {
      logEntry( "login() ", e.toString() );
    }
    
    return retVal;
  }
  
  public void logout()
  {
    try
    {
      SessionResponse response = Port.logout();
      
      if (!response.getStatus().isIsSuccess())
      {
        logEntry( "logout() ", getStatusDetails(response.getStatus()) );
      }
    }
    catch( Exception e )
    {
      logEntry( "logout() ", e.toString() );
    }
  }
  
  private Passport prepareLoginPassport()
  {
    // Populate Passport object with all login information
    Passport passport = new Passport();
    RecordRef role = new RecordRef();
    passport.setEmail(LoginEmail);
    passport.setPassword(LoginPassword);
    passport.setAccount(LoginAccount);
    role.setInternalId(LoginRole);
    passport.setRole(role);
    
    return passport;
  }
  
  public void setActiveDate( Date activeDate )
  {
    ActiveDate = activeDate;
  }
  
  /*
   * Sets the Preferences and search preferences for an operation by adding
   * the preferences to the SOAP header
   */
  private void setPassportHeader() throws SOAPException
  {
    // Cast your login NetSuitePortType variable to a NetSuiteBindingStub
    NetSuiteBindingStub stub = (NetSuiteBindingStub)Port;
    
    // Clear the headers to make sure you know exactly what you are sending.
    // Headers do not overwrite when you are using Axis/Java
    stub.clearHeaders();
    
    //Create request level login user passport header
    SOAPHeaderElement userPassportHeader = new SOAPHeaderElement("urn:messages.platform.webservices.netsuite.com", "passport");
    userPassportHeader.setObjectValue(prepareLoginPassport());
    
    //Create another SOAPHeaderElement to store the preference ignoreReadOnlyFields
    SOAPHeaderElement platformPrefHeader = new SOAPHeaderElement("urn:messages.platform.webservices.netsuite.com", "preferences");
    Preferences pref = new Preferences();
    pref.setIgnoreReadOnlyFields(true);
    platformPrefHeader.setObjectValue(pref);
    
    //Create another SOAPHeaderElement for ApplicationInfo 
    SOAPHeaderElement appInfoHeader = new SOAPHeaderElement("urn:messages.platform.webservices.netsuite.com", "applicationInfo");
    ApplicationInfo appInfo = new ApplicationInfo();
    appInfo.setApplicationId(applicationId);;
    appInfoHeader.setObjectValue(appInfo);
    
    // setHeader applies the Header Element to the stub
    stub.setHeader(userPassportHeader);
    stub.setHeader(platformPrefHeader);
    stub.setHeader(appInfoHeader);
  }
  
  public void setTestMode( boolean testMode )
  {
    TestMode = testMode;
  }
}