/*@lineinfo:filename=EventBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/EventBase.sqlj $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Provider;
import java.security.Security;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.config.ConfigurationManager;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.events.Event;
import com.mes.logging.TELogger;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.LoggingConfigurator;
import com.mes.support.NumberFormatter;
import com.mes.support.PropertiesFile;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;
public class EventBase extends SQLJConnectionBase implements Event
{
  static { LoggingConfigurator.requireConfiguration(); } // initialize log4j
  static Logger log = Logger.getLogger(EventBase.class);
  
  static { DbProperties.requireConfiguration(); } // initialize database properties

  private   boolean         ConsoleTraceEnabled   = false;
  private   boolean         error                 = false;
  private   String          errorDescription      = "";
  protected String          EventArgs             = null;
  protected Vector          EventArgsVector       = null;
  protected String          PropertiesFilename    = null;
  protected PropertiesFile  EventProps            = null;
  protected String			evtPID			  	  = null;

  public final static int               SZ_SOURCE   = 50;
  public final static int               SZ_FUNCTION = 30;
  public final static int               SZ_MSG      = 255;

  public final static int               SFTP_PUT         = 1;
  public final static int               SFTP_GET         = 2;
  public final static int               SFTP_DELETE      = 3;

  public  static final int          ZIP_BUFFER_LEN      = 4096;

  public String  ARCHIVE_PATH_DAILY    = null;
  public String  ARCHIVE_PATH_MONTHLY  = null;
  
  public EventBase()
  {
    super(SQLJConnectionBase.getDirectConnectString());
    setArchivePath();
  }

  public EventBase(boolean autoCommit)
  {
    super(SQLJConnectionBase.getDirectConnectString(), autoCommit);
    setArchivePath();
  }

  public EventBase(String connectionString)
  {
    super(connectionString);
    setArchivePath();
  }

  public EventBase(String connectionString, boolean autoCommit)
  {
    super(connectionString, autoCommit);
    setArchivePath();
  }  
  
  public void setArchivePath()
  {
    try {
      ARCHIVE_PATH_DAILY = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PATH_DAILY);
      ARCHIVE_PATH_MONTHLY = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PATH_MONTHLY);
    } catch (Exception e) {
      logEntry("setArchivePath() - set archive path daily/monthly", e.toString());
    }
  }  
  
  protected boolean archiveMonthlyFile( String dataFilename )
  {
    return( archiveDataFile( dataFilename, ARCHIVE_PATH_MONTHLY ) );
  }

  protected boolean archiveDailyFile( String dataFilename )
  {
    return( archiveDataFile( dataFilename, ARCHIVE_PATH_DAILY ) );
  }

  protected boolean archiveDataFile( String dataFilename, String arcPath )
  {
    String        archiveHost     = null;
    String        archivePass     = null;
    String        archiveUser     = null;
    int           bytesRead       = 0;
    String        flagFilename    = null;
    int           offset          = -1;
    String        progress        = "";
    boolean       success         = false;
    byte[]        zipData         = new byte[ZIP_BUFFER_LEN];
    String        zipFilename     = null;

    try
    {
      // chop the extension from the data filename to build the flag filename
      offset = dataFilename.lastIndexOf('.');
      if ( offset < 0 )
      {
        flagFilename  = dataFilename + ".flg";
        zipFilename   = dataFilename + ".zip";
      }
      else
      {
        flagFilename  = dataFilename.substring(0,offset) + ".flg";
        zipFilename   = dataFilename.substring(0,offset) + ".zip";
      }

      // create the archive .zip file
      progress = "preparing files for archive";
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFilename));
      zos.setMethod(ZipOutputStream.DEFLATED);

      // zip up the .dat file
      FileInputStream zipInFile = new FileInputStream(dataFilename);
      ZipEntry  dataZipEntry = new ZipEntry(dataFilename);
      dataZipEntry.setMethod(ZipEntry.DEFLATED);
      dataZipEntry.setComment("Outgoing file from MES");

      zos.putNextEntry(dataZipEntry);
      while(bytesRead != -1)
      {
        bytesRead = zipInFile.read(zipData);

        if(bytesRead != -1)
        {
          // write these bytes to the zip file
          zos.write(zipData, 0, bytesRead);
        }
      }
      zos.closeEntry();
      zipInFile.close();

      // close the zip file
      zos.close();

      if ( EventProps != null ) // have a timed event properties file
      {
        archiveHost = EventProps.getString("com.mes.kirin.host",MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST));
        archiveUser = EventProps.getString("com.mes.kirin.user",MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER));
        archivePass = EventProps.getString("com.mes.kirin.password",MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS));
      }
      else    // use MesDefaults values
      {
        archiveHost = MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST);
        archiveUser = MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER);
        archivePass = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS);
      }

      progress = "archiving files";
      ftpPut( zipFilename,
              archiveHost,
              archiveUser,
              archivePass,
              arcPath,
              true);

      ftpPut( flagFilename,
              dataFilename.getBytes(),
              archiveHost,
              archiveUser,
              archivePass,
              arcPath,
              true);

      new File(dataFilename).delete();
      new File(flagFilename).delete();
      new File(zipFilename).delete();

      success = true;
    }
    catch( Exception e )
    {
      logEntry("archiveDataFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }

  public String buildFilename( String fileType )
  {
    return( buildFilename( fileType, ".dat" ) );
  }

  public String buildFilename( String fileType, String ext )
  {
    StringBuffer  buffer      = new StringBuffer();
    String        dateString  = null;

    dateString = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyy");
    buffer.setLength(0);
    buffer.append(fileType);
    buffer.append("_");
    buffer.append(dateString);
    buffer.append("_");
    buffer.append( NumberFormatter.getPaddedInt( getFileId(fileType,dateString), 3 ) );
    buffer.append(ext);

    return( buffer.toString() );
  }
  
  /***
   * Download file from given URL to local file system
   * @param url
   * @param filename
   */
    public void downloadFile(String url, String filename) {

      try {
        InputStream in = new URL(url).openStream();
        BufferedInputStream bin = new BufferedInputStream(in);
        FileOutputStream fos = new FileOutputStream(filename);

        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte data[] = new byte[1024];

        int i = 0;
        while ((i = bin.read(data, 0, 1024)) >= 0) {
          bout.write(data, 0, i);
        }
        bout.close();
        in.close();

      } catch (MalformedURLException e) {
        logEntry("downloadFile() ", e.getMessage());

      } catch (IOException e) {
        logEntry("downloadFile() ", e.getMessage());
      }
    }

  public boolean execute()
  {
    return true;
  }

  //
  // extractFileId requires the std mes filename format
  //
  // prefixBBBB_mmddyy_nnn.xxx
  // where prefix = file prefix (ex: ddf)
  //       BBBB   = bank number (ex: 3941)
  //       mmddyy = date file was created/processed (ex: May 1, 2006 050106)
  //       nnn    = file id (ex: 001)
  //       xxx    = file extension (ex: dat)
  //
  public static int extractFileId( String loadFilename )
  {
    int fileSepOffset = loadFilename.lastIndexOf(".");
    int offset        = loadFilename.lastIndexOf("_");
    return( Integer.parseInt( loadFilename.substring( (offset+1), ((fileSepOffset >= 0) ? fileSepOffset : loadFilename.length()) ) ) );
  }

  public boolean getError()
  {
    return error;
  }

  public String getErrorDescription()
  {
    return this.errorDescription;
  }

  public String getEventArg( int argIdx )
  {
    String      retVal      = null;

    if ( EventArgsVector != null && argIdx < EventArgsVector.size() )
    {
      retVal = (String)EventArgsVector.elementAt(argIdx);
    }
    return( retVal );
  }

  public int getEventArgCount()
  {
    return((EventArgsVector == null) ? 0 : EventArgsVector.size());
  }

  public String getEventArgs()
  {
    return( EventArgs );
  }

  protected int getFileBankNumber( String loadFilename )
  {
    ResultSetIterator   it                  = null;
    int                 offset              = 0;
    int                 prefixLength        = 0;
    ResultSet           resultSet           = null;
    int                 retVal              = 0;

    try
    {
      if ( loadFilename != null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:360^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lft.file_prefix           as prefix,
//                    length(lft.file_prefix)   as prefix_length
//            from    mes_load_file_types     lft
//            where   substr(:loadFilename,1,length(lft.file_prefix)) = lft.file_prefix
//            order by prefix_length desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lft.file_prefix           as prefix,\n                  length(lft.file_prefix)   as prefix_length\n          from    mes_load_file_types     lft\n          where   substr( :1 ,1,length(lft.file_prefix)) = lft.file_prefix\n          order by prefix_length desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.EventBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:367^9*/
        resultSet = it.getResultSet();

        if( resultSet.next() )
        {
          prefixLength = resultSet.getInt("prefix_length");
          offset = loadFilename.substring(prefixLength).indexOf("_");
          if ( offset < 0 )
          {
            offset = 4;
          }
          retVal = Integer.parseInt(loadFilename.substring(prefixLength,(prefixLength+offset)));
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }

  protected Date getFileDate( String loadFilename )
  {
    ResultSetIterator   it                  = null;
    int                 prefixLength        = 0;
    ResultSet           resultSet           = null;
    Date                retVal              = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:403^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  lft.file_prefix           as prefix,
//                  length(lft.file_prefix)   as prefix_length
//          from    mes_load_file_types     lft
//          where   substr(:loadFilename,1,length(lft.file_prefix)) = lft.file_prefix
//          order by prefix_length desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lft.file_prefix           as prefix,\n                length(lft.file_prefix)   as prefix_length\n        from    mes_load_file_types     lft\n        where   substr( :1 ,1,length(lft.file_prefix)) = lft.file_prefix\n        order by prefix_length desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.EventBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^7*/
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        prefixLength = resultSet.getInt("prefix_length");
        int offset = loadFilename.substring(prefixLength).indexOf("_");
        if ( offset < 0 )
        {
          offset = 4;   // default to 4 digit bank number
        }
        retVal = DateTimeFormatter.parseDate(loadFilename.substring((prefixLength+offset+1),(prefixLength+offset+1+6)),"MMddyy");
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }

  public int getFileId( String dateString )
  {
    return( getFileId( null, dateString ) );
  }

  public synchronized int getFileId( String fileType, String dateString )
  {
    int               fileId    = 0;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;

    try
    {
      connect();
      
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:453^7*/

//  ************************************************************
//  #sql [Ctx] { lock table filename_postfix in exclusive mode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "lock table filename_postfix in exclusive mode";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.EventBase",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^7*/
      
      if ( fileType != null && !fileType.equals("") )
      {
        // see if file type already exists in database
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:463^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    filename_postfix
//            where   file_type = :fileType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    filename_postfix\n          where   file_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.EventBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fileType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^9*/
        
        if( recCount == 0 )
        {
          // fileId set to 1, add record to database
          fileId = 1;
          
          /*@lineinfo:generated-code*//*@lineinfo:476^11*/

//  ************************************************************
//  #sql [Ctx] { insert into filename_postfix
//              (
//                file_type,
//                last_date,
//                last_count
//              )
//              values
//              (
//                :fileType,
//                :dateString,
//                :fileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into filename_postfix\n            (\n              file_type,\n              last_date,\n              last_count\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fileType);
   __sJT_st.setString(2,dateString);
   __sJT_st.setInt(3,fileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:490^11*/
        }
        else
        {
          // see if count has already happened for this date
          /*@lineinfo:generated-code*//*@lineinfo:495^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  last_date,
//                      last_count
//              from    filename_postfix
//              where   file_type = :fileType                    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  last_date,\n                    last_count\n            from    filename_postfix\n            where   file_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fileType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.EventBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^11*/
          
          rs = it.getResultSet();
          
          if(rs.next())
          {
            if( dateString.equals(rs.getString("last_date")) )
            {
              fileId = rs.getInt("last_count") + 1;
            }
            else
            {
              fileId = 1;
            }
          }
          else
          {
            fileId = 212;
          }
          
          rs.close();
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:524^11*/

//  ************************************************************
//  #sql [Ctx] { update  filename_postfix
//              set     last_date = :dateString,
//                      last_count = :fileId
//              where   file_type = :fileType                    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  filename_postfix\n            set     last_date =  :1 ,\n                    last_count =  :2 \n            where   file_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dateString);
   __sJT_st.setInt(2,fileId);
   __sJT_st.setString(3,fileType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:530^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getFileId(" + fileType + ", " + dateString + ")", e.toString());
      fileId = 666;
    }
    finally
    {
      try { setAutoCommit(true); } catch(Exception e) {}
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      commit();
      cleanUp();
    }

    return( fileId );
  }

  public long getParentNode( long nodeId )
  {
    long        retVal      = 0L;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:557^7*/

//  ************************************************************
//  #sql [Ctx] { select  orgp.org_group  
//          from    organization      orgp,
//                  organization      orgc,
//                  parent_org        po
//          where   orgc.org_group = :nodeId and
//                  po.org_num = orgc.org_num and
//                  orgp.org_num = po.parent_org_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  orgp.org_group   \n        from    organization      orgp,\n                organization      orgc,\n                parent_org        po\n        where   orgc.org_group =  :1  and\n                po.org_num = orgc.org_num and\n                orgp.org_num = po.parent_org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.EventBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^7*/
    }
    catch( java.sql.SQLException e )
    {
      // no rows found, return 0L
    }
    catch( Exception e )
    {
      logEntry( "getParentNode(" + nodeId + ")", e.toString() );
    }
    return( retVal );
  }

  protected String generateFilename( String fileType )
  {
    return( generateFilename( fileType, Calendar.getInstance().getTime() ) );
  }
  
  protected String generateFilename( String fileType, String fileExt )
  {
    return( generateFilename( fileType, Calendar.getInstance().getTime(), fileExt ) );
  }

  protected String generateFilename( String fileType, Date fileDate )
  {
    return( generateFilename( fileType, fileDate, ".dat" ) );
  }
  
  protected String generateFilename( String fileType, Date fileDate, String fileExt )
  {
    String dateString = DateTimeFormatter.getFormattedDate(fileDate,"MMddyy");
    return( generateFilename( fileType, fileDate, getFileId(fileType,dateString), fileExt ) );
  }
  
  protected String generateFilename( String fileType, Date fileDate, int fileId )
  {
    return( generateFilename( fileType, fileDate, fileId, ".dat" ) );
  }
  
  protected String generateFilename( String fileType, Date fileDate, int fileId, String fileExt )
  {
    StringBuffer      buffer          = new StringBuffer();
    String            dateString      = null;

    // setup a work filename
    dateString = DateTimeFormatter.getFormattedDate(fileDate,"MMddyy");
    buffer.setLength(0);
    buffer.append(fileType);
    buffer.append("_");
    buffer.append(dateString);
    buffer.append("_");
    buffer.append( NumberFormatter.getPaddedInt( fileId, 3 ) );
    buffer.append(fileExt);

    return( buffer.toString() );
  }

  public long loadFilenameToLoadFileId( String loadFilename )
  {
    long        loadFileId      = 0L;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:629^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:632^7*/

      /*@lineinfo:generated-code*//*@lineinfo:634^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.EventBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:638^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadFilenameToLoadFileId( " + loadFilename + " )", e.toString() );
    }
    return( loadFileId );
  }

  public void logEvent(String sourceName, String functionName, String message)
  {
    boolean wasStale = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }

      String source     = (sourceName.length() > SZ_SOURCE) ? sourceName.substring(0, SZ_SOURCE) : sourceName;
      String function   = (functionName.length() > SZ_FUNCTION) ? functionName.substring(0, SZ_FUNCTION) : functionName;
      String msg        = (message.length() > SZ_MSG) ? message.substring(0, SZ_MSG) : message;

      /*@lineinfo:generated-code*//*@lineinfo:663^7*/

//  ************************************************************
//  #sql [Ctx] { insert into startup_event_log
//          (
//            log_date,
//            source_name,
//            function_name,
//            event_message
//          )
//          values
//          (
//            sysdate,
//            :source,
//            :function,
//            :msg
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into startup_event_log\n        (\n          log_date,\n          source_name,\n          function_name,\n          event_message\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,source);
   __sJT_st.setString(2,function);
   __sJT_st.setString(3,msg);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:679^7*/
    }
    catch(Exception e)
    {
      log.error("EventBase::logEvent() failed! " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  protected void postFileTotals( String loadFilename,
                                 String vendor,
                                 int recCount,
                                 int batchCount,
                                 int debitsCount,
                                 double debitsAmount,
                                 int creditsCount,
                                 double creditsAmount )
  {
    SQLJConnectionBase      ftpdb       = null;
    DefaultContext          ftpCtx      = null;

    try
    {
      // default ftpdb name in dp.properties file which can be
      // used to override db.properties default connection string 
      String dbName = MesDefaults.getString(MesDefaults.DEFAULT_FTP_DB_NAME);
      if ( EventProps != null ) // have a timed event properties file
      {
        dbName = EventProps.getString("com.mes.dbname",dbName);
      }

      ftpdb = new SQLJConnectionBase(dbName);
      ftpdb.connect();                // connect to ftp db
      ftpdb.setAutoCommit(false);     // disable auto-commit
      ftpCtx = ftpdb.getContext();    // store a ref to the context

      /*@lineinfo:generated-code*//*@lineinfo:721^7*/

//  ************************************************************
//  #sql [ftpCtx] { insert into file_upload_info
//          (
//            file_name,
//            vendor,
//            upload_dt,
//            record_count,
//            batch_count,
//            debits_count,
//            debit_amt,
//            credits_count,
//            credit_amt,
//            net_amt
//          )
//          values
//          (
//            :loadFilename,
//            :vendor,
//            sysdate,
//            :recCount,
//            :batchCount,
//            :debitsCount,
//            :debitsAmount,
//            :creditsCount,
//            :creditsAmount,
//            (:debitsAmount - :creditsAmount)
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ftpCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into file_upload_info\n        (\n          file_name,\n          vendor,\n          upload_dt,\n          record_count,\n          batch_count,\n          debits_count,\n          debit_amt,\n          credits_count,\n          credit_amt,\n          net_amt\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          sysdate,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n          ( :9  -  :10 )\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.EventBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,vendor);
   __sJT_st.setInt(3,recCount);
   __sJT_st.setInt(4,batchCount);
   __sJT_st.setInt(5,debitsCount);
   __sJT_st.setDouble(6,debitsAmount);
   __sJT_st.setInt(7,creditsCount);
   __sJT_st.setDouble(8,creditsAmount);
   __sJT_st.setDouble(9,debitsAmount);
   __sJT_st.setDouble(10,creditsAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:749^7*/

      /*@lineinfo:generated-code*//*@lineinfo:751^7*/

//  ************************************************************
//  #sql [ftpCtx] { commit
//         };
//  ************************************************************

  ((ftpCtx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : ftpCtx.getExecutionContext().getOracleContext()).oracleCommit(ftpCtx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:754^7*/
    }
    catch( Exception e )
    {
      log.error("postFileTotals error",e);
      logEntry("postFileTotals(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ ftpdb.cleanUp(); } catch (Exception e) {}
    }
  }

  protected boolean sendDataFile( String dataFilename, String host, String user, String pw )
  {
    return( sendDataFile( dataFilename, host, user, pw, null, false, true ) );
  }

  protected boolean sendDataFile( String dataFilename, String host,
                                  String user, String pw,
                                  String destPath,
                                  boolean useBinary,
                                  boolean sendFlagFile)
  {
    return( sendDataFile( dataFilename, host, user, pw, destPath, useBinary, sendFlagFile, ".flg") );
  }
  
  protected boolean sendDataFile( String dataFilename, String host,
                                  String user, String pw,
                                  String destPath,
                                  boolean useBinary,
                                  boolean sendFlagFile,
                                  String  postFix )
  {
    String        flagFilename    = null;
    int           offset          = -1;
    String        progress        = "";
    boolean       success         = false;

    try
    {
      // chop the extension from the data filename to build the flag filename
      offset = dataFilename.lastIndexOf('.');
      if ( offset < 0 )
      {
        flagFilename  = dataFilename + postFix;
      }
      else
      {
        flagFilename  = dataFilename.substring(0,offset) + postFix;
      }

      // send data file
      ftpPut(dataFilename,host,user,pw,destPath,useBinary);

      if ( sendFlagFile == true )
      {
        // send the flag file
        ftpPut(flagFilename,dataFilename.getBytes(),host,user,pw,destPath,useBinary);
      }
      success = true;
    }
    catch( Exception e )
    {
      logEntry("sendDataFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }

  public void setConsoleTrace( boolean enable )
  {
    ConsoleTraceEnabled = enable;
  }

  public void setError(String errMsg)
  {
    error = true;
    errorDescription = errMsg;

    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_TIMED_EVENT_ERROR);
      msg.setSubject("Timed Event ERROR");
      msg.setText(errMsg);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("setError(" + errMsg + ")", e.toString());
    }
  }

  public void setEventArgs( String args )
  {
    EventArgs = args;

    if ( EventArgs != null )
    {
      EventArgsVector = new Vector();
      StringTokenizer tokens = new StringTokenizer(EventArgs,",");
      while( tokens.hasMoreTokens() )
      {
        EventArgsVector.addElement(tokens.nextToken().trim());
      }
    }
  }
  
  public void setEventArgs(String[] args) {
  
    EventArgsVector = new Vector();
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < args.length; ++i) {
      EventArgsVector.addElement(args[i]);
      buf.append(args[i] + (i < args.length - 1 ? "," : ""));
    }
    EventArgs = ""+buf;
  }

  public void setEventProperties( PropertiesFile props)
  {
    EventProps = props;

  }
  public void setEvtPID( String evtPID ) {
	  this.evtPID = evtPID;
  }
  public String getEvtPID() {
	  return evtPID;
  }

  public void trace( String msg )
  {
    if ( ConsoleTraceEnabled == true )
    {
      log.debug( msg );
    }
  }

  public void logEntry(String method, String message)
  {
    try
    {
      MailMessage.sendSystemErrorEmail("Timed Event " + this.getClass().getName() + " failed", "Server:    " + HttpHelper.getServerName(null) + "\nFunction:  " + method + "\nException: " + message);
    }
    catch(Exception e)
    {
    }
    super.logEntry(method, message);
    logError(method, message);
  }
  public void logError(String method, String message) {	
	  String errMsg = this.getClass().getName()+ "::" + method;
      String truncErrMsg = (errMsg.length() > 80) ? errMsg.substring(0, 80) : errMsg.toString();
	  TELogger.logTEError((Event) this, truncErrMsg + ": " + message);
  }

  /*========= SFTP methods, en masse ========================================*/

  protected Sftp getSftp(String host, String user, String pw,
                         String destPath, boolean useBinary)
    throws Exception
  {
    Security.addProvider((Provider)new BouncyCastleProvider());
    Sftp   sftp          = null;
    String progress      = "instantiating Sftp";

    try
    {
      SshParameters params =
        new SshParameters(host,
                          user,
                          pw);

      sftp = new Sftp(params);

      progress = "connecting to ftp server";
      sftp.connect();

      if ( destPath != null && destPath.length() > 0 )
      {
        progress = "changing directory to " + destPath;
        sftp.setDir(destPath);
      }

      // set transfer mode
      if(useBinary)
      {
        sftp.setBinary();
      }
      else
      {
        sftp.setAscii();
      }
    }
    catch (Exception e)
    {
      //will potentially create double log entry for this exception, here and in calling method
      //but attempting to keep logging consistent
      logEntry("getSftp()", progress + ": " + e.toString());
      throw(e);
    }

    return sftp;
  }

  /**
   * Handles both put and get calls, with the restriction on the get being that
   * it returns the called file in byte[] format
   */
  private byte[] ftpAction(int action, String filename, byte[] fileData,
                         String host, String user, String pw,
                         String destPath, boolean useBinary )
    throws Exception
  {
    Sftp      sftp          = null;
    Exception lastException = null;
    String    progress      = "entry";
    int       attemptCount  = 0;
    int       retryCount    = 5;

    for( attemptCount = 0; attemptCount < retryCount; attemptCount++ )
    {
      try
      {
        progress = "generating Sftp";

        sftp = getSftp(host, user, pw, destPath, useBinary);

        switch(action)
        {
          case SFTP_PUT:

            // sending file
            progress = "transmitting file";

            if ( fileData != null )
            {
              sftp.upload( fileData , filename );
            }
            else
            {
              sftp.upload( filename , filename );
            }
            break;

          case SFTP_GET:

            //getting file
            progress = "retrieving file";

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            sftp.download(baos, filename);

            fileData = baos.toByteArray();

            break;

          case SFTP_DELETE:

            //removing remote file
            progress = "deleting file";

            sftp.deleteFile(filename);

            break;

          default:
            throw new Exception("Invalid sftp action request");

        }

        // close the connection and release the reference
        //
        // at this point the file has been delivered
        // without exception.  if there is an exception
        // while quitting the FTP session then ignore it.
        try{ sftp.disconnect(); } catch(Exception ee) {} finally{ sftp = null; }

        break;    // success, exit the retry loop
      }
      catch( Exception ftpe )
      {
        // close the connection and release the reference
        try{ sftp.disconnect(); } catch(Exception ee){} finally{ sftp = null; }

        // save error message
        lastException = ftpe;

        // delay 1 second before retry attempt
        try{ Thread.sleep(1000); } catch( Exception ee ) {}
      }
    }   // end retry loop

    if ( attemptCount >= retryCount )
    {
      logEntry("ftpAction"+action+"(" + filename + ")", progress + ": " + lastException.toString());
      throw(lastException);
    }
    else if ( attemptCount > 0 )
    {
      logEntry("ftpAction"+action+"(" + filename + ")", "'" + filename + "' required " + String.valueOf(attemptCount) + " additional attempt(s)");
    }

    return fileData;

  }

  protected Iterator getSftpDirList(String host, String user, String pw,
                                        String destPath, boolean useBinary)
    throws Exception
  {
    //get the Sftp connection
    Sftp sftp = getSftp(host, user, pw, destPath, useBinary);

    //conversion to iterator here...
    EnumerationIterator enumi = new EnumerationIterator();
    Iterator it = enumi.iterator(sftp.getNameListing());

    return it;
  }


  protected void ftpDelete(String filename, String host, String user,
                            String pw, String destPath)
    throws Exception
  {
     ftpAction(SFTP_DELETE, filename, null, host, user, pw, destPath, false);
  }


  protected byte[] ftpGet(String filename, byte[] fileData,
                         String host, String user, String pw,
                         String destPath, boolean useBinary )
    throws Exception
  {
    return ftpAction(SFTP_GET, filename, fileData, host, user, pw, destPath, useBinary);
  }

  protected void ftpPut( String filename, String host,
                         String user, String pw,
                         String destPath,
                         boolean useBinary)
    throws Exception
  {
    ftpAction(SFTP_PUT, filename, null, host, user, pw, destPath, useBinary );
  }

  protected void ftpPut( String filename, byte[] fileData,
                         String host, String user, String pw,
                         String destPath, boolean useBinary )
    throws Exception
  {
    ftpAction(SFTP_PUT, filename, fileData, host, user, pw, destPath, useBinary);
  }

  public class EnumerationIterator
  {
    public Iterator iterator(final Enumeration myEnum)
    {
      return new Iterator()
      {
       public boolean hasNext()
       {
         return myEnum.hasMoreElements();
       }

       public Object next()
       {
         return myEnum.nextElement();
       }

       public void remove()
       {
         throw new UnsupportedOperationException();
       }
      };
    }
  }

      public static ConfigurationManager loadTEMConfiguration() throws Exception {
          ConfigurationManager configurationManager = ConfigurationManager.getInstance();
          configurationManager.loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);

          return configurationManager;
      }

      public static void printKeyListStatus(String[] keyList) throws Exception {
          System.out.println("Test Log:\n");

          ConfigurationManager configurationManager = loadTEMConfiguration();
          configurationManager.printKeyListStatus(keyList);
          Runtime.getRuntime().exit(0);
      }


  public static void main( String[] args )
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      EventBase eb = new EventBase();
      
      String filename = eb.generateFilename(args[0]);
      
      System.out.println("filename: " + filename);
      
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/