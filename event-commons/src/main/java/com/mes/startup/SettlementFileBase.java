/*@lineinfo:filename=SettlementFileBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/SettlementFileBase.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.mes.clearing.utils.ClearingConstants;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.settlement.SettlementRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.PropertiesFile;

public abstract class SettlementFileBase extends EventBase
{
  static Logger log = Logger.getLogger(SettlementFileBase.class);

  protected String          CardTypeTable;
  protected String          CardTypeTableActivity;
  protected String          CardTypePostTotals;
  protected String          CardTypeFileDesc;
  protected String          DkOutgoingHost;
  protected String          DkOutgoingUser;
  protected String          DkOutgoingPassword;
  protected String          DkOutgoingPath;
  protected boolean         DkOutgoingUseBinary;
  protected boolean         DkOutgoingSendFlagFile;
  protected int             SettlementAddrsNotify;
  protected int             SettlementAddrsFailure;
  protected int             SettlementRecMT;

  protected String          ActionCode            = null;
  protected boolean         CallSendDataFile      = false;
  protected double          BatchCreditsAmount    = 0.0;
  protected int             BatchCreditsCount     = 0;
  protected double          BatchDebitsAmount     = 0.0;
  protected int             BatchDebitsCount      = 0;
  protected int             BatchRecordCount      = 0;
  protected int             BatchTranCount        = 0;
  protected int             FileBatchCount        = 0;
  protected int             FileBatchId           = 0;
  protected double          FileCreditsAmount     = 0.0;
  protected int             FileCreditsCount      = 0;
  protected double          FileDebitsAmount      = 0.0;
  protected int             FileDebitsCount       = 0;
  protected int             FileRecordCount       = 0;
  protected String          FileReferenceId       = null;
  protected Timestamp       FileTimestamp         = null;
  protected int             FileTranCount         = 0;
  protected String          FileReversalInd       = null;
  protected String          TestFilename          = null;
  protected HashMap			EmvProperties		  = new HashMap();

  public SettlementFileBase( )
  {
	  PropertiesFile pf = new PropertiesFile();
      pf.load("emv.properties");
               
      for(Enumeration e = pf.getPropertyNames();e.hasMoreElements();) {
      	String cardType = (String)e.nextElement();
        String emvEnabled = pf.getString(cardType);
        log.debug(cardType + ":" + emvEnabled);
        EmvProperties.put(cardType, emvEnabled);            	    
      }
  }

  protected void addChargebackAdjustmentProcessEntry( String workFilename )
  {
    try
    {
      // do not queue the ACH entries when rebuilding a file
      if ( TestFilename == null )
      {
        // queue an entry in the MES ach process table to
        // handle the merchant DDA adjustments
        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_process
//            (
//              process_type, process_sequence, load_filename
//            )
//            values
//            (
//              :AchEvent.PT_CB_ADJ, 0, :workFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_process\n          (\n            process_type, process_sequence, load_filename\n          )\n          values\n          (\n             :1 , 0,  :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.SettlementFileBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ClearingConstants.PT_CB_ADJ.getValue());
   __sJT_st.setString(2,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^9*/

        // if there were no second presentments issued, then
        // sendFile(..) will not be called.  As a result, the
        // notification email will not be generated.  When this occurs
        // we still want an email to be generated for the cb department.
        if ( FileRecordCount == 0 )
        {
          sendStatusEmail(true,workFilename);
        }
      }
      else
      {
        logEntry("addChargebackAdjustmentProcessEntry(" + workFilename + ")",
                 "WARNING:  Test mode detected, chargeback adjustments skipped for " + workFilename);
      }
    }
    catch( Exception e )
    {
      logEntry("addCbAdjProcessEntry(" + workFilename + ")",e.toString());
    }
  }

  protected void addMbsProcessEntry( int processType, String workFilename )
  {
    try
    {
      // do not queue the MBS billing entries when rebuilding a file
      if ( TestFilename == null )
      {
        // queue an entry in the mbs_process table to
        // handle the loading of the billing summary data
        /*@lineinfo:generated-code*//*@lineinfo:149^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//            (
//              process_type, process_sequence, load_filename
//            )
//            values
//            (
//              :processType, 0, :workFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n          (\n            process_type, process_sequence, load_filename\n          )\n          values\n          (\n             :1 , 0,  :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.SettlementFileBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processType);
   __sJT_st.setString(2,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^9*/
      }
      else
      {
        logEntry("addMbsProcessEntry(" + processType + "," + workFilename + ")",
                 "WARNING:  Test mode detected, billing process skipped for " + workFilename);
      }
    }
    catch( Exception e )
    {
      logEntry("addMbsProcessEntry(" + processType + "," + workFilename + ")",e.toString());
    }
  }

  protected void sendFile( String workFilename )
  {
    try
    {
      // disarm send when running in test mode
      // UPDATE: allowing CallSendDataFile to override test mode
      // to all regenerating and resending files from command line
      if ( TestFilename == null || CallSendDataFile )
      {
        boolean success = false;

        if( CallSendDataFile )
        {
          log.debug("Sending data file: " + workFilename + " to "
            + MesDefaults.getString(DkOutgoingHost));
          success = sendDataFile( workFilename,
                                  MesDefaults.getString(DkOutgoingHost),
                                  MesDefaults.getString(DkOutgoingUser),
                                  MesDefaults.getString(DkOutgoingPassword),
                                  MesDefaults.getString(DkOutgoingPath),
                                  DkOutgoingUseBinary,
                                  DkOutgoingSendFlagFile );
          if ( success == true )
          {
            log.debug("Data file successfully sent, archiving file.");
            archiveDailyFile(workFilename);
          }
        }
        else {
          log.debug("CallSendDataFile false, file not actually sent.");
        }
        sendStatusEmail( success, workFilename );
      }
      else
      {
        log.debug("\n\nTest Filename '" + TestFilename + "' complete (no transmit)");
      }
    }
    catch( Exception e )
    {
      logEntry("sendFile(" + workFilename + ")", e.toString());
    }
    finally
    {
    }
  }

  private void sendStatusEmail(boolean success, String fileName)
  {
    String        subject         = null;
    StringBuffer  body            = new StringBuffer("");
    MailMessage   msg             = null;

    try
    {
      // post the file totals for the systems group to use for confirmation
      postFileTotals( fileName,CardTypePostTotals,
                      FileRecordCount,
                      FileBatchCount,
                      FileDebitsCount,
                      FileDebitsAmount,
                      FileCreditsCount,
                      FileCreditsAmount );

      msg = new MailMessage();
      if( success )
      {
        body.append("[");
        body.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        body.append("] Successfully processed outgoing " + CardTypeFileDesc + " file: ");
        body.append(fileName);
        body.append("\n\n");

        subject = " Success - ";
        msg.setAddresses(SettlementAddrsNotify);
      }
      else
      {
        // make sure the filename is in the message text for error research
        body.append("[");
        body.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        body.append("] Failed to process outgoing " + CardTypeFileDesc + " file: ");
        body.append(fileName);

        subject = " Error - ";
        msg.setAddresses(SettlementAddrsFailure);
      }

      body.append("\n\n");
      body.append("File Statistics\n");
      body.append("================================================\n");
      body.append("Record Count  : " + FileRecordCount);
      body.append("\n");
      body.append("Debits Count  : " + FileDebitsCount );
      body.append("\n");
      body.append("Debits Amount : " + MesMath.toCurrency(FileDebitsAmount) );
      body.append("\n");
      body.append("Credits Count : " + FileCreditsCount );
      body.append("\n");
      body.append("Credits Amount: " + MesMath.toCurrency(FileCreditsAmount) );
      body.append("\n");
      body.append("Net Amount    : " + MesMath.toCurrency(FileDebitsAmount - FileCreditsAmount) );
      body.append("\n\n");

      msg.setSubject(CardTypeFileDesc + subject + fileName);
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }

  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      CallSendDataFile = true;
      setActionCode( getEventArg(1) );    // null or action code
      processTransactions();
    }
    catch( Exception e )
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }

  protected Date getOriginalCpd( long recId )
  {
    Date      retVal      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:320^7*/

//  ************************************************************
//  #sql [Ctx] { select  max( decode(sa.settlement_date,
//                              null,nvl(s.settlement_date,s.batch_date),
//                              sa.settlement_date) )
//          
//          from    :CardTypeTable          s,
//                  :CardTypeTableActivity  sa
//          where   s.rec_id              = :recId
//                  and sa.rec_id(+)      = s.rec_id
//                  and sa.action_code(+) = 'X'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  max( decode(sa.settlement_date,\n                            null,nvl(s.settlement_date,s.batch_date),\n                            sa.settlement_date) )\n         \n        from     ");
   __sjT_sb.append(CardTypeTable);
   __sjT_sb.append("           s,\n                 ");
   __sjT_sb.append(CardTypeTableActivity);
   __sjT_sb.append("   sa\n        where   s.rec_id              =  ? \n                and sa.rec_id(+)      = s.rec_id\n                and sa.action_code(+) = 'X'");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.startup.SettlementFileBase:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^7*/
    }
    catch( Exception e )
    {
      logEntry("getOriginalCpd(" + recId + ")", e.toString());
    }
    finally
    {
    }
    return( retVal );
  }

  public abstract Date getSettlementDate();

  protected void markFileForProcessing( String loadFilename, String workFilename )
  {
    try
    {
      long  loadFileId  = loadFilenameToLoadFileId(loadFilename);
      long  workFileId  = loadFilenameToLoadFileId(workFilename);

      /*@lineinfo:generated-code*//*@lineinfo:352^7*/

//  ************************************************************
//  #sql [Ctx] { update  :CardTypeTable
//          set     output_filename = :workFilename,
//                  output_file_id  = :workFileId
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(CardTypeTable);
   __sjT_sb.append(" \n        set     output_filename =  ? ,\n                output_file_id  =  ? \n        where   load_file_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.startup.SettlementFileBase:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setLong(3,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^7*/

      /*@lineinfo:generated-code*//*@lineinfo:360^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:363^7*/
    }
    catch( Exception e )
    {
      logEntry("markFileForProcessing(" + loadFilename + "," + workFilename + ")", e.toString());
    }
    finally
    {
    }
  }

  protected abstract boolean processTransactions( );

  protected boolean transactionToReverse( ResultSet resultSet )
    throws Exception
  {
    switch( SettlementRecMT )
    {
      case SettlementRecord.MT_FIRST_PRESENTMENT:
        return( "R".equals(ActionCode) ? true : false );

      case SettlementRecord.MT_SECOND_PRESENTMENT:
        return( "U".equals(resultSet.getString("action_code")) ? true : false );

      case SettlementRecord.MT_FEE_COLLECTION:
        return( "R".equals(resultSet.getString("action_code")) ? true : false );

      default:
        return( false );
    }
  }

  protected boolean transactionToSkip( ResultSet resultSet )
    throws Exception
  {
    switch( SettlementRecMT )
    {
      case SettlementRecord.MT_FIRST_PRESENTMENT:
        return( false );

      case SettlementRecord.MT_SECOND_PRESENTMENT:
        if ( "U".equals(resultSet.getString("action_code")) ) // reversal
        {
          if( resultSet.getDate("batch_date") == null )
          {
            logEntry( "buildDetailRecords(" + resultSet.getLong("batch_record_id") + ")",
                      "Unabled to locate original Second Presentment message");
            return( true );   // skip because there was not representment to reverse
          }
        }
        return( false );

      case SettlementRecord.MT_FEE_COLLECTION:
        if ( "R".equals(resultSet.getString("action_code")) ) // reversal
        {
          if( resultSet.getDate("batch_date") == null )
          {
            logEntry( "buildDetailRecords(" + resultSet.getLong("batch_record_id") + ")",
                      "Unabled to locate original Fee Collection message");
            return( true );   // skip because there was not original fee to reverse
          }
        }
        return( false );

      default:
        return( false );
    }
  }

  protected void setActionCode        ( String ac             ) { ActionCode = ac;                  }
  protected void setFileReversalInd   ( String reversalInd    ) { FileReversalInd = reversalInd;    }
  protected void setTestFilename      ( String filename       ) { TestFilename = ("null".equals(filename) ? null : filename); }

  public void executeCommandLine( String[] args )
    throws Exception
  {
    int       baseIdx;
    Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());

    SQLJConnectionBase.initStandalonePool("DEBUG");

    // get an automated timeout exempt connection
    connect(true);

    try
    {
      int bank = Integer.parseInt(args[0]);
      baseIdx = 1;
      setEventArgs(args[0]);
    }
    catch( Exception e )
    {
      baseIdx = 0;
      setEventArgs("9999");
    }

    if ( args.length > baseIdx+0 )
    {
      if ("resend".equals(args[baseIdx+0])) {
        markFileForProcessing(args[baseIdx+1],args[baseIdx+2]);           // regenerate settlement file and actually
        setTestFilename(args[baseIdx+2]);                                 // transmit it to EP
        CallSendDataFile = true;
      }
      else if ( "markAndProcess".equals(args[baseIdx+0]) )                // first presentment: resend entire file
      {
        markFileForProcessing(args[baseIdx+1],args[baseIdx+2]);
        setTestFilename(args[baseIdx+2]);
      }
      else if ( "procResubmits".equals(args[baseIdx+0]) )                 // first presentment: resubmissions
      {
        setActionCode( "X" );
        if ( args.length > baseIdx+1 )setTestFilename( args[baseIdx+1] );
      }
      else if ( "procReversals".equals(args[baseIdx+0]) )                 // first presentment/fee collection: reversals
      {
        setActionCode( "R" );
        if ( args.length > baseIdx+1 )setTestFilename( args[baseIdx+1] );
      }
      else if ( "procReversalsCB".equals(args[baseIdx+0]) )               // chargeback: only reversals
      {
        setActionCode( "U" );
        if ( args.length > baseIdx+1 )setTestFilename( args[baseIdx+1] );
      }
      else if ( "feeCollection".equals(args[baseIdx+0]) )                 // fee collection: fees only
      {
        setActionCode( "F" );
        if ( args.length > baseIdx+1 )setTestFilename( args[baseIdx+1] );
      }
      else if ( "reversalFile".equals(args[baseIdx+0]) )                  // first presentment: reverse entire file
      {
        java.util.Date cpd = DateTimeFormatter.parseDate(args[baseIdx+2],"MM/dd/yyyy");
        String reversalInd = "R" + DateTimeFormatter.getFormattedDate(cpd,"yyMMdd");
        setFileReversalInd(reversalInd);
        setTestFilename(args[baseIdx+1]);
      }
      else
      {
        if ( args.length > baseIdx+1 && "sendFile".equals(args[baseIdx+1]) )CallSendDataFile = true;
        setTestFilename( args[baseIdx+0] );
      }
    }
    processTransactions();

    Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    long elapsed = (endTime.getTime() - beginTime.getTime());

    log.debug("Begin Time: " + String.valueOf(beginTime));
    log.debug("End Time  : " + String.valueOf(endTime));
    log.debug("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
  }
}/*@lineinfo:generated-code*/