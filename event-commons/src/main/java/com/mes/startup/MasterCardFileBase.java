/*@lineinfo:filename=MasterCardFileBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MasterCardFileBase.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.List;
import com.mes.config.MesDefaults;
import com.mes.settlement.InterchangeMasterCard;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordMC;
import com.mes.settlement.SettlementTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import masthead.mastercard.clearingiso.IpmOfflineFileWriter;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.mastercard.clearingiso.IpmOfflineMessages;
import sqlj.runtime.ResultSetIterator;

public abstract class MasterCardFileBase extends SettlementFileBase
{
  public MasterCardFileBase( )
  {
  }
  
  private void buildDetailRecords( IpmOfflineFileWriter ipmFile, SettlementRecord rec, boolean reversal )
  {
    long                  batchId         = 0L;
    long                  batchRecId      = 0L;
    int                   fpNum           = 0;
    IpmOfflineMessage     ipmMsg          = null;
    List                  ipmMsgs         = null;
    String                mti             = null;
    int                   retryCount      = 0;
    
    
    try
    {
      // store these in case of error
      batchId     = rec.getLong("batch_id");
      batchRecId  = rec.getLong("batch_record_id");
      
      while(true)
      {
        try
        {
          ipmMsgs = rec.buildNetworkRecords(SettlementRecMT);
          break;
        }
        catch( Exception e )
        {
          if ( ++retryCount > 10 )
          {
            throw(e);
          }
        }
      }
      
      log.debug("ipmMsgCount: " + ipmMsgs.size());
      for( int i = 0; i < ipmMsgs.size(); ++i )
      {
        ipmMsg = (IpmOfflineMessage)ipmMsgs.get(i);
        if ( reversal )
        {
          String reversalInd = "R" + DateTimeFormatter.getFormattedDate( rec.getDate("settlement_date"),"yyMMdd" );
          ipmMsg.setPrivateDataSubelement(25,reversalInd);
        }
        
        // for first presentments, set the associated first presentment 
        // number in all the necessary financial detail addendum records
        if( SettlementRecMT == SettlementRecord.MT_FIRST_PRESENTMENT )
        {
          mti = ipmMsg.getMTI();
          if ( "1240".equals(mti) )
          {
            fpNum = ipmFile.getTotalMessagesWritten()+1;  // store first presentment number
          }
          else if ( "1644".equals(mti) && ipmMsg.hasPds(501) )
          {
            ipmMsg.setPds(501, (ipmMsg.getPds(501).substring(0,8) + StringUtilities.rightJustify(String.valueOf(fpNum),8,'0')));
          }
        }
        ipmFile.writeMessage(ipmMsg);
      }
      
      // increment the counters and update the amounts
      double tranAmount = rec.getDouble("transaction_amount");

      FileRecordCount     += ipmMsgs.size();
      
      if ( "C".equals(rec.getString("debit_credit_indicator")) )
      {
        FileCreditsCount    ++;
        FileCreditsAmount   += tranAmount;
      }
      else
      {
        FileDebitsCount     ++;
        FileDebitsAmount    += tranAmount;
      }
    }
    catch(Exception e)
    {
      logEntry("buildDetailRecords(" + batchId + "," + batchRecId + ")", e.toString());
    }
    finally
    {
    }
  }
  
  public void buildFileHeader( IpmOfflineFileWriter ipmFile )
  {
    IpmOfflineMessage   hdr   = null;
    
    try
    {
      PropertiesFile propsFile = new PropertiesFile( PropertiesFilename );
      String         testOrProduction = propsFile.getString("test.or.production", "P");
      
      hdr = IpmOfflineMessages.getFileHeader();
      
      // overload header defaults
      hdr.setPds(105,FileReferenceId);
      hdr.setPds(122,testOrProduction);   // mode:  T'est/P'roduction
      
      if ( FileReversalInd != null )
      {
        hdr.setPds(26,FileReversalInd);
      }
      ipmFile.writeHeader(hdr);
    
      ++FileRecordCount;  // include file header record in counts
    }
    catch(Exception e)
    {
      logEntry("buildFileHeader()", e.toString());
    }
    finally
    {
    }
  }
  
  public void buildFileTrailer( IpmOfflineFileWriter ipmFile )
  {
    try
    {
      IpmOfflineMessage trl = IpmOfflineMessages.getFileTrailer();
      
      trl.setPds(105, FileReferenceId);    // set the file id for this file
      ipmFile.writeTrailer(trl);  // writeTrailer(..) will set file totals
      
      ++FileRecordCount;          // include file trailer record in counts
    }
    catch(Exception e)
    {
      logEntry("buildFileTrailer()", e.toString());
    }
    finally
    {
    }
  }
  
  public String buildFileReferenceId( String workFilename, String fileCode )
    throws Exception
  {
    // Establish the FileId (PDS105)
    return(
        "002"                                                                                               // s01 - File Type (002 = Member Generated)
      + DateTimeFormatter.getFormattedDate(FileTimestamp,"yyMMdd")                                          // s02 - File Date
      + StringUtilities.rightJustify(MesDefaults.getString(MesDefaults.DK_MASTERCARD_PROCESSOR_ID),11,'0')  // s03 - Processor ID
      + fileCode                                                                                            // s04 - File Seq Num -- varies by file type
      + StringUtilities.rightJustify(String.valueOf( extractFileId(workFilename) ),4,'0')                   // s04 - File Seq Num
    );
  }
  
  public Date getSettlementDate()
  {
    return( SettlementTools.getSettlementDateMC() );
  }

  protected void handleSelectedRecords(ResultSetIterator it, String workFilename) throws Exception {
		handleSelectedRecords(it, workFilename, 0L);
  }
	
  protected void handleSelectedRecords( ResultSetIterator it, String workFilename, long workFileid )
    throws Exception
  {
    SettlementDb              db                = new SettlementDb();
    IpmOfflineFileWriter      ipmFile           = null;
    SettlementRecordMC        rec               = new SettlementRecordMC();
    ResultSet                 resultSet         = null;
    HashSet<String>           dtprocessList     = new HashSet<>();

    try
    {
      db.connect(true);

      resultSet = it.getResultSet();

      log.debug("Work Filename : " + workFilename);

      if( resultSet.next() )
      {
        // create a handle to the IPM file, delete any existing files
        File f = new File(workFilename);
        if ( f.exists() ) { f.delete(); }
        
        // create the new IPM offline file writer
        ipmFile = new IpmOfflineFileWriter(f);
        buildFileHeader(ipmFile);

        int recCount = 0;
        do
        {
          if ( transactionToSkip(resultSet) )continue;

          rec.clear();
          rec.setIsEmvEnabled(EmvProperties.containsKey("mastercard") && EmvProperties.get("mastercard").equals("yes") ? true : false);
          rec.setFields(resultSet, false, false);
          //Decrypt the card number
    	  try {
      	    rec.getField("card_number_full").setData( rec.getCardNumberFull() );
    	  } catch (Exception ex) {
    		log.error(ex);
          }
          if ( rec.hasLevelIIIData() )
          {
            if( ActionCode == null || "X".equals(ActionCode) )
            {
              rec.setLineItems( db._loadMasterCardSettlementLineItemDetailRecords(rec.getLong("rec_id")) );
            }
          }
          
          // check interchange on re-procs
          if( "X".equals(ActionCode) )    
          {
            if ( ! rec.isCashDisbursement() )
            {
              String icCodeOriginal = rec.getString("ic_cat");
              new InterchangeMasterCard().getBestClass(rec,SettlementDb.loadIrdListMC(rec));
              if ( icCodeOriginal == null || !icCodeOriginal.equals( rec.getString("ic_cat") ) )
              {
                rec.setData("ic_cat_downgrade",rec.getString("ic_cat"));
                SettlementDb.updateIcCatDowngrade(rec);
              }
              if ("N".equals(rec.getData("external_reject"))) {
                rec.setData("load_file_id", workFileid);
                rec.setData("load_filename", workFilename);
                rec.setData("ach_flag", "Y");
                db.updateDTRecord(rec);
                dtprocessList.add(DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"), "dd-MMM-yyyy"));
              }
            } 
          }
          
          buildDetailRecords( ipmFile, rec, transactionToReverse(resultSet) );

          log.debug("Processed: " + String.valueOf(++recCount));
        } while( resultSet.next() );
        
        for (String batchdate : dtprocessList) {
             SettlementDb.insertDTProcessRecord(1, workFilename, batchdate);
        }
       
        log.debug("\n\n");

        buildFileTrailer(ipmFile);

        sendFile(workFilename);
      }
      else
      {
        log.debug("No records selected.\n");
      }

      resultSet.close();
    }
    finally
    {
      try{ db.cleanUp(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/