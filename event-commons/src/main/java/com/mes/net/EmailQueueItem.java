/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/EmailQueueItem.sqlj $

  Description:  
  
    EmailQueueItem

    Creates an email queue item which may be placed in the queue via
    EmailQueue.  This class is intended to be serialized for storage
    in the queue table as a blob, so care must be taken when modifying
    it.  Basically, you can only add new members, never remove them.
    The same rules apply to the Address and Attachment subclasses.
    
    Supports multi-part messages (allows file attachments) and allows
    multiple recipients (of TO, CC and BCC types) to be specified.
    
    A set of sender/recipient addresses may be defined in the db table
    EMAIL_ADDRESSES.  These sets may be referenced with the setAddresses()
    call, or may be specified via the constructor.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-06-02 11:47:37 -0700 (Tue, 02 Jun 2009) $
  Version            : $Revision: 16194 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import com.mes.support.WebLogicPropertiesFile;

public class EmailQueueItem implements Serializable
{
  // serialization version management
  private static final long serialVersionUID  = 0xffffffffffff0001L;
  private static final int  currentVersion    = 3;
  private int objectVersion = currentVersion;
  
  private void readObject(ObjectInputStream ois)
    throws IOException, ClassNotFoundException
  {
    ois.defaultReadObject();
    switch(objectVersion)
    {
      case currentVersion - 2:
        // version 1, convert from 1 to 2
      case currentVersion - 1:
        // version 2, convert from 2 to 3
      case currentVersion:
        // version 3, current version, do nothing
        break;
        
      default:
        throw new IOException(this.getClass().getName() 
          + ": unable to read future version of class, " 
          + "current: " + currentVersion + ", object: " + objectVersion);
    }
    objectVersion = currentVersion;
  }

  public static final int AT_FROM = 0;
  public static final int AT_TO   = 1;
  public static final int AT_CC   = 2;
  public static final int AT_BCC  = 3;
  
  public String[] addressTypes    = { "FROM", "TO", "CC", "BCC" };
    
  /*
  ** public class Address
  **
  ** An email address object containing a string email address and an address
  ** type that specifies what type of address it is: from, to, cc or bcc.
  ** The checkAddresses method is called on creation to validate the address
  ** data, and to make sure it does not conflict with addresses already 
  ** contained by the parent class addresses vector.
  */
  public class Address implements Serializable
  {
    private static final long serialVersionUID  = 0xffffffffffff0002L;
    private static final int  currentVersion    = 3;
    private int objectVersion = currentVersion;
  
    private void readObject(ObjectInputStream ois)
      throws IOException, ClassNotFoundException
    {
      ois.defaultReadObject();
      switch(objectVersion)
      {
        case currentVersion - 2:
          // version 1, convert from 1 to 2
        case currentVersion - 1:
          // version 2, convert from 2 to 3
        case currentVersion:
          // version 3, current version, do nothing
          break;

        default:
          throw new IOException(this.getClass().getName() 
            + ": unable to read future version of class, " 
            + "current: " + currentVersion + ", object: " + objectVersion);
      }
      objectVersion = currentVersion;
    }
    
    private String address;
    private int addressType;
    
    public Address(String address, int addressType) throws Exception
    {
      this.address = address;
      this.addressType = addressType;
      checkAddress();
    }

    private void checkAddress() throws Exception
    {    
      if (addressType < AT_FROM || addressType > AT_BCC)
     {
        throw new Exception("Invalid address type specified (" + addressType + ")");
      }
      
      if (address == null || address.length() == 0)
      {
        throw new Exception("Empty or null address not allowed");
      }
      
      if (addresses != null)
      {
        for (Iterator i = addresses.iterator(); i.hasNext(); )
        {
          Address addr = (Address)i.next();
          if (addr.getType() == AT_FROM && addressType == AT_FROM)
          {
            throw new Exception("Unable to add second from address");
          }
        }
      }
    }

    public int    getType()     { return addressType; }
    public String getAddress()  { return address; }

    public void   setAddress(String address)
    {
      this.address = address;
    }
    
    public String toString()
    {
      return addressTypes[addressType] + ": " + address;
    }
  }
  
  /*
  ** public class Attachment
  **
  ** An email attachment object containing a filename and the contents
  ** of the file as a byte buffer.
  */
  public class Attachment implements Serializable
  {
    private static final long serialVersionUID  = 0xffffffffffff0003L;
    private static final int  currentVersion    = 3;
    private int objectVersion = currentVersion;
  
    private void readObject(ObjectInputStream ois)
      throws IOException, ClassNotFoundException
    {
      ois.defaultReadObject();
      switch(objectVersion)
      {
        case currentVersion - 2:
          // version 1, convert from 1 to 2
        case currentVersion - 1:
          // version 2, convert from 2 to 3
        case currentVersion:
          // version 3, current version, do nothing
          break;
        
        default:
          throw new IOException(this.getClass().getName() 
            + ": unable to read future version of class, " 
            + "current: " + currentVersion + ", object: " + objectVersion);
      }
      objectVersion = currentVersion;
    }
    
    private String filename;
    private byte[] filedata;
    
    public Attachment(String pathname) throws IOException
    {
      this.filedata = loadFile(pathname);
      this.filename = stripPath(pathname);
    }
    public Attachment(String pathname, byte[] filedata) throws IOException
    {
      this.filedata = filedata;
      this.filename = stripPath(pathname);
    }
    public Attachment(String pathname, String filedata) throws IOException
    {
      this.filedata = filedata.getBytes();
      this.filename = stripPath(pathname);
    }
    
    public String getFilename() { return filename; }
    public byte[] getFiledata() { return filedata; }
    
    /*
    ** private byte[] loadFile(String pathname) throws IOException
    **
    ** Loads a file into a byte array.
    **
    ** RETURNS: byte array containing the contents of the file.
    */
    private byte[] loadFile(String pathname) throws IOException
    {
      FileInputStream fis = new FileInputStream(pathname);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] readBuf = new byte[1024];
      int bytesRead = 0;
      while ((bytesRead = fis.read(readBuf)) != -1)
      {
        baos.write(readBuf,0,bytesRead);
      }
      fis.close();
      return baos.toByteArray();
    }
    
    private String stripPath(String pathname)
    {
      try
      {
        return (new File(pathname)).getName();
      }
      catch(Exception e)
      {
        return pathname;
      }
    }
    
    public String toString()
    {
      StringBuffer buf = new StringBuffer();
      buf.append("FILE: '" + filename + "', ");
      buf.append("SIZE: " + filedata.length + " bytes, ");
      buf.append("DATA: '");
      String data;
      if (filedata.length > 30)
      {
        data = new String(filedata,0,27) + "...";
      }
      else
      {
        data = new String(filedata);
      }
      buf.append(data + "'");
      return buf.toString();
    }
  }
  
  // email message data
  private Vector  addresses   = new Vector();
  private Vector  attachments = new Vector();
  private String  subject;
  private String  text;
  
  private String  jobName;
  private String  procName;
  private long    genericId   = -1L;
  private boolean doDirect;
  
  /*
  ** Constructors
  **
  ** If procName is not given then it must be loaded from mes.properties
  ** (exception will be thrown if mes.properties not available).  The
  ** genericId is optional (probably would be used for appSeqNum type
  ** values), and an addressId is optional, will cause loading of
  ** email address set if present.
  */
  private void initializeItem(String jobName, String procName, long genericId,
    int addressId, boolean doDirect)
    throws Exception
  {
    this.doDirect = doDirect;
    if (addressId != -1)
    {
      setAddresses(addressId);
    }
    if (jobName != null)
    {
      this.jobName    = jobName;
    }
    if (procName != null)
    {
      this.procName   = procName;
    }
    if (genericId != -1L)
    {
      this.genericId  = genericId;
    }
  }
  public EmailQueueItem(String jobName)
    throws Exception
  {
    initializeItem(jobName,null,-1L,-1,false);
  }
  public EmailQueueItem(String jobName, boolean doDirect)
    throws Exception
  {
    initializeItem(jobName,null,-1L,-1,doDirect);
  }
  public EmailQueueItem(String jobName, int addressId) 
    throws Exception
  {
    initializeItem(jobName,null,-1L,addressId,false);
  }
  public EmailQueueItem(String jobName, int addressId, boolean doDirect) 
    throws Exception
  {
    initializeItem(jobName,null,-1L,addressId,doDirect);
  }
  public EmailQueueItem(String jobName, long genericId)
    throws Exception
  {
    initializeItem(jobName,null,genericId,-1,false);
  }
  public EmailQueueItem(String jobName, long genericId, boolean doDirect)
    throws Exception
  {
    initializeItem(jobName,null,genericId,-1,doDirect);
  }
  public EmailQueueItem(String jobName, long genericId, int addressId) 
    throws Exception
  {
    initializeItem(jobName,null,genericId,addressId,false);
  }
  public EmailQueueItem(String jobName, long genericId, int addressId, boolean doDirect) 
    throws Exception
  {
    initializeItem(jobName,null,genericId,addressId,doDirect);
  }
  public EmailQueueItem(String jobName, String procName)
    throws Exception
  {
    initializeItem(jobName,procName,-1L,-1,false);
  }
  public EmailQueueItem(String jobName, String procName, boolean doDirect)
    throws Exception
  {
    initializeItem(jobName,procName,-1L,-1,doDirect);
  }
  public EmailQueueItem(String jobName, String procName, int addressId) 
    throws Exception
  {
    initializeItem(jobName,procName,-1L,addressId,false);
  }
  public EmailQueueItem(String jobName, String procName, int addressId, boolean doDirect) 
    throws Exception
  {
    initializeItem(jobName,procName,-1L,addressId,doDirect);
  }
  public EmailQueueItem(String jobName, String procName, long genericId)
    throws Exception
  {
    initializeItem(jobName,procName,genericId,-1,false);
  }
  public EmailQueueItem(String jobName, String procName, long genericId, boolean doDirect)
    throws Exception
  {
    initializeItem(jobName,procName,genericId,-1,doDirect);
  }
  public EmailQueueItem(String jobName, String procName, long genericId, int addressId) 
    throws Exception
  {
    initializeItem(jobName,procName,genericId,addressId,false);
  }
  public EmailQueueItem(String jobName, String procName, long genericId, int addressId, boolean doDirect) 
    throws Exception
  {
    initializeItem(jobName,procName,genericId,addressId,doDirect);
  }
  
  public String getJobName()    { return jobName; }
  public String getProcName()   { return procName; }
  public long   getGenericId()  { return genericId; }
  
  /*
  ** private void loadProcName() throws Exception
  **
  ** Attempts to load a process name from mes.properties file, throws
  ** exception of file not found.
  */
  private void loadProcName() throws Exception
  {
    WebLogicPropertiesFile propFile 
      = new WebLogicPropertiesFile("mes.properties");
    procName = propFile.getString("com.mes.hostname",null);
    if (procName == null)
    {
      throw new Exception("No process name given and mes.properties hostname not found.");
    }
  }
    

  /*
  ** public void setAddresses(int addressId) throws Exception
  **
  ** Loads a set of addresses from the email_addresses table and places
  ** them into the addresses vector.
  */
  public void setAddresses(int addressId) throws Exception
  {
    List addrs = EmailQueueHelper.getAddressesByType(addressId,doDirect);
    for (Iterator i = addrs.iterator(); i.hasNext();)
    {
      EmailQueueHelper.HelperAddress ha = 
        (EmailQueueHelper.HelperAddress)i.next();
        
      addresses.add(new Address(ha.address,ha.addressType));
    }
  }
  
  /*
  ** public Address getFrom()
  **
  ** Scans through the addresses vector for the from address.
  **
  ** RETURNS: Address if found, else null if no from address exists.
  */
  public Address getFrom()
  {
    if (addresses != null)
    {
      for (Iterator i = addresses.iterator(); i.hasNext();)
      {
        Address address = (Address)i.next();
        if (address.getType() == AT_FROM)
        {
          return address;
        }
      }
    }
    return null;
  }
  
  /*
  ** public String getFromString()
  ** 
  ** Returns the FROM email address
  */
  public String getFromString()
  {
    Address addr = getFrom();
    
    if(addr != null)
    {
      return addr.getAddress();
    }
    else
    {
      return "";
    }
  }
  
  public String getAddrList(int type)
  {
    StringBuffer  list = new StringBuffer("");
    
    if(addresses != null)
    {
      for(Iterator i = addresses.iterator(); i.hasNext();)
      {
        Address address = (Address)i.next();
        if(address.getType() == type)
        {
          list.append(address.getAddress());
          list.append(";");
        }
      }
    }
    
    return list.toString();
  }
  
  /*
  ** public String getToString()
  **
  ** Returns the list of TO email addresses 
  */
  public String getToString()
  {
    return getAddrList(AT_TO);
  }
  
  /*
  ** public String getCCString()
  **
  ** Returns the list of CC email addresses
  */
  public String getCCString()
  {
    return getAddrList(AT_CC);
  }
  
  public String getBCCString()
  {
    return getAddrList(AT_BCC);
  }
  
  /*
  ** public Iterator addressesIterator()
  **
  ** RETURNS: addresses iterator.
  */
  public Iterator addressesIterator()
  {
    return addresses.iterator();
  }
  
  /*
  ** public Iterator attachmentsIterator()
  **
  ** RETURNS: attachments iterator.
  */
  public Iterator attachmentsIterator()
  {
    return attachments.iterator();
  }

  /*
  ** public void setFrom(String address) throws Exception
  **
  ** If a from address exists it is replaced with the new address, otherwise
  ** a new from address is added to the addresses vector.
  */
  public void setFrom(String addressStr) throws Exception
  {
    Address address = getFrom();
    if (address != null)
    {
      address.setAddress(addressStr);
    }
    else
    {
      addresses.add(new Address(addressStr,AT_FROM));
    }
  }
  
  /*
  ** Adders to add any type of address and specific address types
  */
  public void addAddress(String address, int addressType) throws Exception
  {
    StringTokenizer tok = new StringTokenizer(address,";");
    while (tok.hasMoreElements())
    {
      addresses.add(new Address((String)tok.nextElement(),addressType));
    }
  }
  public void addTo(String address) throws Exception
  {
    addAddress(address,AT_TO);
  }
  public void addCc(String address) throws Exception
  {
    addAddress(address,AT_CC);
  }
  public void addBcc(String address) throws Exception
  {
    addAddress(address,AT_BCC);
  }
  
  public String getSubject()
  {
    return subject;
  }
  public void setSubject(String subject)
  {
    this.subject = subject;
  }
  public String getText()
  {
    return text;
  }
  public void setText(String text)
  {
    this.text = text;
  }

  public void addStringAsTextFile(String filename, String text)
    throws IOException
  {
    attachments.add(new Attachment(filename,text));
  }  

  public void addFile(String filename)
    throws IOException
  {
    attachments.add(new Attachment(filename));
  }
  
  public void addFile(String fileName, byte[] fileBody)
    throws Exception
  {
    attachments.add(new Attachment(fileName,fileBody));
  }

  /*
  ** public void ValidateAddresses() throws Exception
  **
  ** Determines if it will be possible to send a message with the current
  ** set of addresses.
  */
  public void validateAddresses() throws Exception
  {
    int fromCnt = 0;
    int toCnt = 0;
    for (Iterator i = addresses.iterator(); i.hasNext();)
    {
      Address address = (Address)i.next();
      if (address.getType() == AT_FROM)
      {
        ++fromCnt;
      }
      else if (address.getType() == AT_TO)
      {
        ++toCnt;
      }
    }
    if (fromCnt < 1)
    {
      throw new Exception("Email item must have a from address");
    }
    if (toCnt < 1)
    {
      throw new Exception("Email item must have at least one to address");
    }
  }
  
  public String toString()
  {
    StringBuffer myData = new StringBuffer();
    myData.append("EmailQueueItem contents:\n");
    myData.append("  subject:     " + subject + "\n");
    myData.append("  text:        " + text + "\n");
    myData.append("  addresses:   " + addresses + "\n");
    myData.append("  attachments: " + attachments + "\n");
    return myData.toString();
  }
}
