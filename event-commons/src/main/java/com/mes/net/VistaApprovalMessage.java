/*@lineinfo:filename=VistaApprovalMessage*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VistaApprovalMessage.sqlj $

  Description:  
  
    VistaApprovalMessage

    Message used to notify Vista that a merchant application has been
    approved by credit.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:21p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.net.URL;
import org.jdom.Document;
import org.jdom.Element;

public class VistaApprovalMessage extends XMLMessage
{
  private long appSeqNum;
  private String vistaId;
  private String partnerId;
  private String userName;
  private String passcode;
  private String returnUrl;
  
  public VistaApprovalMessage(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
    loadData();
    buildDocument();
  }
  
  /*
  ** private void loadData(long appSeqNum)
  **
  ** Loads message data (vista id, partner id, user name, passcode).
  */
  protected void loadData()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] { select  vl.verisign_id,
//                  vl.pid,
//                  vl.return_url,
//                  m.merch_number,
//                  a.address_phone
//                  
//          
//                  
//          from    vs_linking_table vl,
//                  merchant m,
//                  address a
//                  
//          where   vl.app_seq_num = :appSeqNum
//                  and m.app_seq_num = vl.app_seq_num
//                  and a.app_seq_num = vl.app_seq_num
//                  and a.addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vl.verisign_id,\n                vl.pid,\n                vl.return_url,\n                m.merch_number,\n                a.address_phone\n                \n         \n                \n        from    vs_linking_table vl,\n                merchant m,\n                address a\n                \n        where   vl.app_seq_num =  :1 \n                and m.app_seq_num = vl.app_seq_num\n                and a.app_seq_num = vl.app_seq_num\n                and a.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VistaApprovalMessage",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vistaId = (String)__sJT_rs.getString(1);
   partnerId = (String)__sJT_rs.getString(2);
   returnUrl = (String)__sJT_rs.getString(3);
   userName = (String)__sJT_rs.getString(4);
   passcode = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/
      
      
      try
      {
        if (returnUrl != null)
        {
          URL url = new URL(returnUrl);
          setTargetUrl("https://" + url.getHost() 
            + "/verisignresponse/xmlacceptor.php3");
        }
      }
      catch (Exception e) {}
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "::loadData(appSeqNum=" + appSeqNum + ", db) " + e.toString());
      logEntry("loadData(appSeqNum=" + appSeqNum + ", db)",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** protected void buildDocument()
  **
  ** Generates the xml message:
  **
  **  <VerisignResponse>
  **    <Status>Approved</Status>
  **    <VistaId>vista_id</VistaId>
  **    <PartnerId>partner_id</PartnerId>
  **    <UserName>verisign_user_name</UserName>
  **    <Passcode>verisign_passcode</Passcode>
  **  </VerisignResponse>
  */
  protected void buildDocument()
  {
    doc = new Document(buildDocBody());
  }
  protected Element buildDocBody()
  {
    Element body = new Element("VerisignResponse");
    body.addContent(new Element("Status").setText("Approved"));
    body.addContent(new Element("VistaId").setText(vistaId));
    body.addContent(new Element("PartnerId").setText("merchantes"));
    body.addContent(new Element("UserName").setText(userName));
    body.addContent(new Element("Passcode").setText(passcode));
    return body;
  }
  
  public String getMessageType()
  {
    return "VISTA: APPROVED (" + appSeqNum + ")";
  }
}/*@lineinfo:generated-code*/