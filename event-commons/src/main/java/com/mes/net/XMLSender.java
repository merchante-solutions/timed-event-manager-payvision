/*@lineinfo:filename=XMLSender*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/XMLSender.sqlj $

  Description:  
  
    XMLSender

    Sends XML messages, receives responses.  Communications are logged.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/07/03 3:51p $
  Version            : $Revision: 13 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.PrintWriter;
import java.net.Socket;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.mes.database.SQLJConnectionBase;

public class XMLSender extends SQLJConnectionBase
{
  public static final int         DEFAULT_SOCKET_TIMEOUT      = 0; // i.e.: infinite

  String      jobDescriptor       = null;
  int         socket_timeout      = DEFAULT_SOCKET_TIMEOUT;
  
  public XMLSender(String newJobDescriptor)
  {
    jobDescriptor = newJobDescriptor;
  }

  public int getSocketTimeout()
  {
    return socket_timeout;
  }

  public void setSocketTimeout(int socket_timeout)
  {
    this.socket_timeout=socket_timeout;
  }
    
  /*
  ** LOGGING
  */
  private void log(XMLMessage message, String details)
  {
    String messageType = "XML message";
    if (message != null)
    {
      messageType = message.getMessageType();
    }
    if (details == null)
    {
      details = "No details";
    }
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:81^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_log
//          (
//            log_time,
//            job_name,
//            msg_type,
//            details
//          )
//          values
//          (
//            sysdate,
//            :jobDescriptor,
//            :messageType,
//            :details
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_log\n        (\n          log_time,\n          job_name,\n          msg_type,\n          details\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.net.XMLSender",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,jobDescriptor);
   __sJT_st.setString(2,messageType);
   __sJT_st.setString(3,details);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^7*/
    }
    catch (Exception e)
    {
      logEntry("log",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  private void logConnectError(String details)
  {
    log(null,details);
  }

  /*
  ** public VSResponse send(XMLMessage outgoingMsg)
  **
  ** Opens a connection to the local Redirector server and sends the
  ** xml message to the message's target.  Creates an xml message
  ** from the response returned.
  **
  ** RETURNS: XMLMessage containing the response returned from the target.
  */
  public XMLMessage send(XMLMessage outgoingMsg)
  {
    XMLMessage returnedMsg = null;
    
    try
    {
      // connect to redirector
      String host = outgoingMsg.getRedirectorHost();
      int port = outgoingMsg.getRedirectorPort();
      Socket rSocket = new Socket(host,port);
      rSocket.setSoTimeout(socket_timeout);
      PrintWriter rpw = new PrintWriter(rSocket.getOutputStream(),true);
    
      // send the target url and content type
      rpw.println(outgoingMsg.getTargetUrl());
      rpw.println(outgoingMsg.getContentType());
    
      // send the xml message data
      XMLOutputter fmt = new XMLOutputter();
      fmt.output(outgoingMsg.getDocument(),rpw);
      log(outgoingMsg,"Request sent");
    
      // send end_post_data
      rpw.println("end_post_data");
    
      // receive response
      SAXBuilder builder = new SAXBuilder();
      returnedMsg = new XMLMessage(builder.build(rSocket.getInputStream()));
      log(returnedMsg,"Response received");
    }
    catch (Exception e)
    {
      logEntry("send",e.toString());
    }
        
    return returnedMsg;
  }
  
  /*
  ** public static XMLMessage sendMessage(XMLMessage requestMsg)
  **
  ** Instantiates an instance of XMLSender, sends the request message.
  **
  ** RETURNS: the response message that was returned.
  */  
  public static XMLMessage sendMessage(XMLMessage outgoingMsg, String jobName)
  {
    return (new XMLSender(jobName)).send(outgoingMsg);
  }

  public static XMLMessage sendMessage(XMLMessage outgoingMsg, String jobName, int socket_timeout)
  {
    XMLSender sndr = new XMLSender(jobName);
    sndr.setSocketTimeout(socket_timeout);
    return sndr.send(outgoingMsg);
  }

}/*@lineinfo:generated-code*/