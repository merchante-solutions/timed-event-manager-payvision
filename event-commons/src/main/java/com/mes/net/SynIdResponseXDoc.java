/*@lineinfo:filename=SynIdResponseXDoc*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/SynIdResponseXDoc.sqlj $

  Description:  
  
    SynIdResponseXDoc

    XML document in response received from Synergistic Software.  Has the
    ability to store it's xml document in a compressed form as a record
    in a db table.
        
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-12-10 16:38:57 -0800 (Tue, 10 Dec 2013) $
  Version            : $Revision: 21935 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import oracle.sql.BLOB;

public class SynIdResponseXDoc extends XDoc
{
  public SynIdResponseXDoc(XMLMessage xmlMsg)
  {
    super(xmlMsg.getDocument());
    extractDataFields();
  }

  private String outputId = null;

  private Element getOutputs()
  {
    return doc.getRootElement()
              .getChild("request")
              .getChild("products")
              .getChild("product")
              .getChild("outputs");
  }              

  /*
  ** private void extractDataFields()
  **
  ** Extracts PDF output id from immediate response.
  */
  private void extractDataFields()
  {
    // get output id
    try
    {
      List outputList = getOutputs().getChildren("output");
      for (Iterator i = outputList.iterator(); i.hasNext();)
      {
        Element output = (Element)i.next();
        if (output.getChild("description").getText().indexOf("PDF") == 0)
        {
          outputId = output.getChild("outputid").getText();
          break;
        }
      }
    }
    catch (Exception e)
    {
      logEntry("extractDataFields",e.toString());
    }
  }
  
  /*
  ** public void submitData(long appSeqNum)
  **
  ** Given an application seq num key this will store the xml document
  ** as a record in the credit_requests table.
  */
  public void submitData(long appSeqNum)
  {
    // storage for the compressed document
    byte docBuf[] = null;
    
    // compress the xml document into a byte buffer
    try
    {
      // dump the xml doc into a byte array buffer
      XMLOutputter fmt = new XMLOutputter();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      fmt.output(doc,baos);
      docBuf = baos.toByteArray();
      
      // compress it
      baos.reset();
      DeflaterOutputStream dos = new DeflaterOutputStream(baos);
      dos.write(docBuf,0,docBuf.length);
      dos.finish();
      docBuf = baos.toByteArray();
    }
    catch (Exception e)
    {
      logEntry("::submitData (compressing)",e.toString());
    }
    
    try
    {
      connect();
    
      // can't auto commit since we need a lock on the record
      // to do blob writing
      setAutoCommit(false);
      
      // clear out previous requests
      /*@lineinfo:generated-code*//*@lineinfo:128^7*/

//  ************************************************************
//  #sql [Ctx] { delete from credit_requests
//          where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from credit_requests\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.net.SynIdResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      // create a new record with an empty blob in credit_requests
      /*@lineinfo:generated-code*//*@lineinfo:135^7*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_requests
//          (
//            app_seq_num,
//            output_id,
//            request_data
//          )
//          values
//          (
//            :appSeqNum,
//            :outputId,
//            empty_blob()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into credit_requests\n        (\n          app_seq_num,\n          output_id,\n          request_data\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          empty_blob()\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.net.SynIdResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,outputId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^7*/
    
      // get a stream handler to the blob
      BLOB blob;
      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] { select  request_data 
//          from    credit_requests
//          where   app_seq_num = :appSeqNum
//          for update
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  request_data  \n        from    credit_requests\n        where   app_seq_num =  :1 \n        for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.net.SynIdResponseXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blob = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^7*/
      //Changed offset to 1. Earlier it was 0. JDBC offsets starts with 1
      OutputStream os = blob.setBinaryStream(1L);
    
      // write the xml data buffer to the blob
      os.write(docBuf,0,docBuf.length);
      os.flush();
      os.close();
      
      // commit
      /*@lineinfo:generated-code*//*@lineinfo:169^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^7*/
    }
    catch (Exception e)
    {
      logEntry("::submitData (db blob)",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/