/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VistaResponseMessage.java $

  Description:  
  
    VistaResponseMessage

    Contains an xml response message from Vista.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:21p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Document;
import org.jdom.Element;

public class VistaResponseMessage extends XMLMessage
{
  public VistaResponseMessage(XMLMessage msg)
  {
    super(msg.getDocument());
  }
  public VistaResponseMessage(Document newDoc)
  {
    super(newDoc);
  }
  
  public void buildDocument()
  {
  }
  
  public Element getMessageBody()
  {
    Element messageBody = null;
    try
    {
      messageBody = doc.getRootElement();
    }
    catch (Exception e)
    {
    }
    
    return messageBody;
  }
  
  private Element getResultElement()
  {
    Element resultElement = null;
    
    try
    {
      resultElement = getMessageBody().getChild("Response");
    }
    catch (Exception e)
    {
    }
    
    return resultElement;
  }
  
  public String getResult()
  {
    String result = "";
    
    try
    {
      result = getResultElement().getText();
    }
    catch (Exception e)
    {
    }
    
    return result;
  }
  
  private Element getDescriptionElement()
  {
    Element descriptionElement = null;
    
    try
    {
      descriptionElement = getMessageBody().getChild("Description");
    }
    catch (Exception e)
    {
      descriptionElement = new Element("Description").setText("No message body");
    }
    
    if (descriptionElement == null)
    {
      descriptionElement = new Element("Description").setText("No Description element found");
    }
    
    return descriptionElement;
  }
  
  public String getDescription()
  {
    return getDescriptionElement().getText();
  }
  
  public String getDetails()
  {
    return "result code: " + getResult() + ", result Description: " + getDescription();
  }
  
  public String getMessageType()
  {
    return getResult() + ": " + getDescription();
  }
}
