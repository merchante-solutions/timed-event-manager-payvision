/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/MailMessage.java $

  Description:

    MailMessage

    Uses the Java Mail API to create an email message and send it.
    Supports multi-part messages (allows file attachments) and allows
    multiple recipients (of TO, CC and BCC types) to be specified.
    A set of sender/recipient addresses may be defined in the db table
    EMAIL_ADDRESSES.  These sets may be referenced with the setAddresses()
    call.

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.net;

import javax.mail.Message.RecipientType;
import com.mes.constants.MesEmails;

public class MailMessage
{
  private EmailQueueItem      queueItem     = null;
  private boolean             doDirect      = false;

  public static final int     EA_FROM       = 1;
  public static final int     EA_TO         = 2;
  public static final int     EA_CC         = 3;
  public static final int     EA_BCC        = 4;

  public MailMessage()
    throws Exception
  {
    queueItem = new EmailQueueItem("MailMessage");
  }

  public MailMessage(String smtpHost)
    throws Exception
  {
    queueItem = new EmailQueueItem("MailMessage");
  }

  public MailMessage(boolean doDirect)
    throws Exception
  {
    queueItem = new EmailQueueItem("MailMessage", doDirect);
    this.doDirect = doDirect;
  }

  public void setAddresses(int emailId)
    throws Exception
  {
    queueItem.setAddresses(emailId);
  }

  public void setFrom(String address)
    throws Exception
  {
    queueItem.setFrom(address);
  }

  public void addRecipient(RecipientType type, String address)
    throws Exception
  {

    if(type.equals(RecipientType.BCC))
    {
      queueItem.addBcc(address);
    }
    else if(type.equals(RecipientType.CC))
    {
      queueItem.addCc(address);
    }
    else
    {
      queueItem.addTo(address);
    }
  }
  public void addTo(String address)
    throws Exception
  {
    addRecipient(RecipientType.TO, address);
  }
  public void addCC(String address)
    throws Exception
  {
    addRecipient(RecipientType.CC, address);
  }
  public void addBCC(String address)
    throws Exception
  {
    addRecipient(RecipientType.BCC, address);
  }

  public void setSubject(String subject)
    throws Exception
  {
    queueItem.setSubject(subject);
  }

  public void setText(String text)
    throws Exception
  {
    queueItem.setText(text);
  }

  public void addStringAsTextFile(String filename, String fileData)
    throws Exception
  {
    queueItem.addStringAsTextFile(filename, fileData);
  }

  public void addFile(String filename)
    throws Exception
  {
    queueItem.addFile(filename);
  }

  public void addFile(String fileName, byte[] fileBody)
    throws Exception
  {
    queueItem.addFile(fileName, fileBody);
  }

  public void send()
    throws Exception
  {
    EmailQueue.push(queueItem, this.doDirect);
  }

  public static void sendSystemErrorEmail(String subject, String message)
  {
    MailMessage msg = null;

    try
    {
      msg = new MailMessage();

      msg.setAddresses(MesEmails.MSG_ADDRS_SYSTEM_ERROR);

      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("sendSystemErrorEmail()", e.toString());
    }
  }
  
  public static void sendErrorEmail(String subject, String message, int emailId)
  {
    MailMessage msg = null;
    
    try
    {
      msg = new MailMessage();

      msg.setAddresses(emailId);

      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("sendErrorEmail(" + emailId + ")", e.toString());
    }
  }
}
