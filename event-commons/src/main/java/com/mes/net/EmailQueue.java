/*@lineinfo:filename=EmailQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/EmailQueue.sqlj $

  Description:  
  
    EmailQueue

    Manages the email queue.  Allows EmailQueueItem objects to be pushed
    onto the email queue and also handles the sending of unsent queue
    items.
    
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import javax.mail.Message.RecipientType;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueueItem.Address;
import com.mes.net.EmailQueueItem.Attachment;
import com.mes.support.HttpHelper;
import com.mes.support.SMTPMail;
import com.mes.support.StringUtilities;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;

public class EmailQueue extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(EmailQueue.class);
  private boolean doDirect = false;
  
  /*
  ** public EmailQueue()
  **
  ** Constructor.
  **
  ** Default constructor.
  */
  public EmailQueue()
  {
    doDirect = false;
  }
  
  /*
  ** public EmailQueue(boolean doDirect)
  **
  ** Constructor.
  **
  ** Flag indicates wheather or not to do direct connection to database.
  */
  public EmailQueue(boolean doDirect)
  {
    this.doDirect = doDirect;
    if (doDirect)
    {
      initialize( getDirectConnectString() );
    }
  }
  
  /*
  ** private void queueLog(long emailSeqNum, String event, String details)
  **   throws Exception
  **
  ** Inserts a log entry into email_log db table.
  */
  private void queueLog(long emailSeqNum, String event, String details)
    throws Exception
  {
    if (event.length() > 20)
    {
      event = event.substring(0,20);
    }
    if (details.length() > 240)
    {
      details = details.substring(0,237) + "...";
    }
    /*@lineinfo:generated-code*//*@lineinfo:104^5*/

//  ************************************************************
//  #sql [Ctx] { insert into email_log
//        ( email_seq_num,
//          log_date,
//          event,
//          details )
//        values
//        ( :emailSeqNum,
//          sysdate,
//          :event,
//          :details )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into email_log\n      ( email_seq_num,\n        log_date,\n        event,\n        details )\n      values\n      (  :1  ,\n        sysdate,\n         :2  ,\n         :3   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
   __sJT_st.setString(2,event);
   __sJT_st.setString(3,details);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^5*/
  }
  
  /*
  ** private EmailQueueItem loadItem(ResultSet rs) throws IOException
  **
  ** Takes a ResultSet and loads an EmailQueueItem from it.  The result
  ** set must contain a blob reference named "message" in order for the
  ** load to occur.
  **
  ** RETURNS: loaded EmailQueueItem.
  */
  private EmailQueueItem loadItem(ResultSet rs) throws Exception
  {
    EmailQueueItem result = null;
    int emailSeqNum       = -1;
    
    try
    {
      // use the blob ref to load the queue item data into a byte array
      emailSeqNum = rs.getInt("email_seq_num");
      Blob b = rs.getBlob("message");
      int dataLen = (int)b.length();
      int chunkSize = 0;
      int dataRead = 0;
      InputStream is = b.getBinaryStream();
      byte[] itemData = new byte[dataLen];
      while (chunkSize != -1)
      {
        chunkSize = is.read(itemData,dataRead,dataLen - dataRead);
        dataRead += chunkSize;
      }
      is.close();
    
      // create an object input stream to inflate/unserialize the data
      ByteArrayInputStream bais = new ByteArrayInputStream(itemData);
      InflaterInputStream iis = new InflaterInputStream(bais);
      ObjectInputStream ois = new ObjectInputStream(iis);
      
      result = (EmailQueueItem)ois.readObject();
    }
    catch(Exception e)
    {
      logEntry("loadItem("+emailSeqNum+")", e.toString());
    }
    
    // return the queue item from the object input stream
    return( result );
  }
  
  private EmailQueueItem getQueueItem(long emailSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    EmailQueueItem      item    = null;
    
    try
    {
      connect();
      
      // run a query to get the BLOB for the email queue item
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  message,
//                  email_seq_num
//          from    email_messages
//          where   email_seq_num = :emailSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  message,\n                email_seq_num\n        from    email_messages\n        where   email_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.net.EmailQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        item = loadItem(rs);
      }
    }
    catch(Exception e)
    {
      logEntry("getQueueItem(" + emailSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return item;
  }
  
  /*
  **
  ** private void sendItem(EmailQueueItem item, long emailSeqNum, int sendTries)
  **
  ** Attempts to send an email queue item.  Creates an SMTPMailer object,
  ** configures it with the item data and sends it.  The results are
  ** logged.  The queue item record's send tries field is incremented.
  ** If the send fails and the max number of retries has been exceeded
  ** the queue item record is flagged as being on hold.
  */
  private void sendItem(EmailQueueItem item, long emailSeqNum, int sendTries)
    throws Exception
  {
    // attempt to send the queue item as an email message
    try
    {
      // increment the send tries
      ++sendTries;
      
      // create a mailer object with the host parameters in MesDefaults
      String emailHost = MesDefaults.getString(MesDefaults.DK_SMTP_HOST);
      SMTPMail mailer = new SMTPMail(emailHost);

      // load the mailer with the queue item data
      mailer.setSubject(item.getSubject());
      mailer.setText(item.getText());
      for (Iterator i = item.addressesIterator(); i.hasNext();)
      {
        Address address = (Address)i.next();
        switch (address.getType())
        {
          case EmailQueueItem.AT_FROM:
            mailer.setFrom(address.getAddress());
            break;
          
          case EmailQueueItem.AT_TO:
            mailer.addRecipient(RecipientType.TO,address.getAddress());
            break;
          
          case EmailQueueItem.AT_CC:
            mailer.addRecipient(RecipientType.CC,address.getAddress());
            break;
          
          case EmailQueueItem.AT_BCC:
            mailer.addRecipient(RecipientType.BCC,address.getAddress());
            break;
          
          default:
            throw new Exception("Invalid address type: " + address.getType());
        }
      }
      
      for (Iterator i = item.attachmentsIterator(); i.hasNext();)
      {
        Attachment attachment = (Attachment)i.next();
        mailer.addByteArrayAsFile(attachment.getFilename(),
          attachment.getFiledata());
      }
    
      // send the email
      mailer.send();
    
      // tag the queue item as sent
      /*@lineinfo:generated-code*//*@lineinfo:269^7*/

//  ************************************************************
//  #sql [Ctx] { update  email_queue
//          set     sent = 'y',
//                  send_tries = :sendTries
//          where   email_seq_num = :emailSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  email_queue\n        set     sent = 'y',\n                send_tries =  :1  \n        where   email_seq_num =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sendTries);
   __sJT_st.setLong(2,emailSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:275^7*/
    
      // log the email as successfully sent
      queueLog(emailSeqNum,"SEND","SUCCESS");
    }
    catch (Exception e)
    {
      // update the queue item record to show a failed attempt
      /*@lineinfo:generated-code*//*@lineinfo:283^7*/

//  ************************************************************
//  #sql [Ctx] { update  email_queue
//          set     send_tries =  :sendTries
//          where   email_seq_num = :emailSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  email_queue\n        set     send_tries =   :1  \n        where   email_seq_num =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sendTries);
   __sJT_st.setLong(2,emailSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:288^7*/
  
      // trouble, log the send as failed
      queueLog(emailSeqNum,"SEND","FAILURE: " + e);

      // if too many failed attempts, put the item on hold
      if (sendTries > 3 /*MesDefaults.getInt(MesDefaults.DK_EMAIL_QUEUE_SEND_MAX)*/)
      {
        // update the queue item record to hold
        /*@lineinfo:generated-code*//*@lineinfo:297^9*/

//  ************************************************************
//  #sql [Ctx] { update  email_queue
//            set     sent = 'h'
//            where   email_seq_num = :emailSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  email_queue\n          set     sent = 'h'\n          where   email_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^9*/
        
        queueLog(emailSeqNum,"HOLD","Too many failed send attempts");
      }
    }
  }
  
  /*
  ** private void sendItems()
  **
  ** Loads all items in the queue that have not been sent and attempts to
  ** send them.  (Doesn't try to send items that are on hold.)
  */
  private void sendItems()
  {
    ResultSetIterator it = null;
    ResultSet rs = null;
    
    try
    {
      connect();
      
      // load the record, including a blob reference
      /*@lineinfo:generated-code*//*@lineinfo:325^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ INDEX ("M" pk_email_messages) */ 
//                  q.email_seq_num, 
//                  q.job_name, 
//                  q.proc_name, 
//                  q.generic_id, 
//                  q.send_tries, 
//                  "M".message
//          from    email_queue q, 
//                  email_messages "M"
//          where   q.sent = 'n'
//                  and q.email_seq_num = "M".email_seq_num      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ INDEX (\"M\" pk_email_messages) */ \n                q.email_seq_num, \n                q.job_name, \n                q.proc_name, \n                q.generic_id, \n                q.send_tries, \n                \"M\".message\n        from    email_queue q, \n                email_messages \"M\"\n        where   q.sent = 'n'\n                and q.email_seq_num = \"M\".email_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.net.EmailQueue",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.net.EmailQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^7*/
      rs = it.getResultSet();
      
      while (rs.next())
      {
        //System.out.println("processing esn: " + rs.getString("email_seq_num") 
        //  + ", job: "  + rs.getString("job_name") + ", proc: " 
        //  + rs.getString("proc_name"));
          
        // send the queue item
        sendItem(loadItem(rs),rs.getLong("email_seq_num"),rs.getInt("send_tries"));
      }
    }
    catch (Exception e)
    {
      String func = "sendItems()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** public static void send()
  **
  ** Entry point to cause all unsent messages in the queue to be sent.
  */
  public static void send()
  {
    EmailQueue queue = new EmailQueue();
    queue.sendItems();
  }
  
  /*
  ** public static void send(boolean doDirect)
  **
  ** Entry point for sending messages that allows doDirect flag to
  ** be set.
  */
  public static void send(boolean doDirect)
  {
    EmailQueue queue = new EmailQueue(doDirect);
    queue.sendItems();
  }

  /*
  ** public byte[] storeItem(EmailQueueItem item) throws Exception
  **
  ** Serialize and compress an EmailQueueItem into a byte buffer.
  **
  ** RETURNS: byte buffer containing the data.
  */
  public byte[] storeItem(EmailQueueItem item) throws Exception
  {
    // serialize item data
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(baos);
    oos.writeObject(item);

    byte[] itemData = baos.toByteArray();
    
    // compress the serialized item data
    baos.reset();
    DeflaterOutputStream dos = new DeflaterOutputStream(baos);
    dos.write(itemData,0,itemData.length);
    dos.finish();
    
    return baos.toByteArray();
  }
      
  /*
  ** public static void pushItem(EmailQueueItem item)
  **
  ** Pushes an email queue item onto the email queue.
  */
  private void pushItem(EmailQueueItem item)
  {
    try
    {
      connect();
      
      // get a new queue seq num
      long emailSeqNum;
      /*@lineinfo:generated-code*//*@lineinfo:427^7*/

//  ************************************************************
//  #sql [Ctx] { select  email_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  email_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.net.EmailQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   emailSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:432^7*/
      
      log.debug("emailSeqNum: "+ emailSeqNum);
      
      // update the email tracking table
      updateTrackingTable(emailSeqNum, item);
      
      // validate the item's addresses
      item.validateAddresses();
    
      // serialize and compress the queue item into a byte buffer
      byte[] itemData = storeItem(item);      

      // can't auto commit since we need a lock on the record
      // to do blob writing
      setAutoCommit(false);
      
      // insert new queue record
      /*@lineinfo:generated-code*//*@lineinfo:450^7*/

//  ************************************************************
//  #sql [Ctx] { insert into email_queue
//          ( email_seq_num,
//            create_date,
//            job_name,
//            proc_name,
//            generic_id )
//          values
//          ( :emailSeqNum,
//            sysdate,
//            :item.getJobName(),
//            :item.getProcName(),
//            :item.getGenericId() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_0 = item.getJobName();
 String __sJT_1 = item.getProcName();
 long __sJT_2 = item.getGenericId();
   String theSqlTS = "insert into email_queue\n        ( email_seq_num,\n          create_date,\n          job_name,\n          proc_name,\n          generic_id )\n        values\n        (  :1  ,\n          sysdate,\n           :2  ,\n           :3  ,\n           :4   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
   __sJT_st.setString(2,__sJT_0);
   __sJT_st.setString(3,__sJT_1);
   __sJT_st.setLong(4,__sJT_2);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:464^7*/
      
      // insert empty blob into email_messages
      /*@lineinfo:generated-code*//*@lineinfo:467^7*/

//  ************************************************************
//  #sql [Ctx] { insert into email_messages
//          ( email_seq_num,
//            message )
//          values
//          ( :emailSeqNum,
//            empty_blob() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into email_messages\n        ( email_seq_num,\n          message )\n        values\n        (  :1  ,\n          empty_blob() )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^7*/
      
      // get stream handler to blob
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:479^7*/

//  ************************************************************
//  #sql [Ctx] { select  message 
//          from    email_messages
//          where   email_seq_num = :emailSeqNum
//          for update
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  message  \n        from    email_messages\n        where   email_seq_num =  :1  \n        for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.net.EmailQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/
      
      //Changed offset to 1. Earlier it was 0. JDBC offsets starts with 1
      OutputStream bOut = b.setBinaryStream(1L);
      
      // write the statement data to the blob
      bOut.write(itemData,0,itemData.length);
      bOut.flush();
      bOut.close();
      
      // log the push
      queueLog(emailSeqNum,"PUSH","SUCCESS");
      
      // commit
      /*@lineinfo:generated-code*//*@lineinfo:499^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^7*/
    }
    catch (Exception e)
    {
      String func = "push(" + item.getSubject() + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      e.printStackTrace();
      System.out.println(item.getText());
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** updateTrackingTable(long emailSeqNum, EmailQueueItem item)
  **
  ** Makes an entry in the email tracking table that will help match seq num to actual email
  */
  private void updateTrackingTable(long emailSeqNum, EmailQueueItem item)
  {
    try
    {
      String  fromAddr  = StringUtilities.getStringMax(50, item.getFromString());
      String  toAddr    = StringUtilities.getStringMax(100, item.getToString());
      String  subject   = StringUtilities.getStringMax(100, item.getSubject());
      String  ccAddr    = StringUtilities.getStringMax(100, item.getCCString());
      String  bccAddr   = StringUtilities.getStringMax(100, item.getBCCString());
      String  body      = StringUtilities.getStringMax(512, item.getText());
      
      /*@lineinfo:generated-code*//*@lineinfo:535^7*/

//  ************************************************************
//  #sql [Ctx] { insert into email_tracking
//          (
//            email_seq_num,
//            from_addr,
//            to_addr,
//            subject,
//            cc_addr,
//            bcc_addr,
//            body,
//            server_name
//          )
//          values
//          (
//            :emailSeqNum,
//            :fromAddr,
//            :toAddr,
//            :subject,
//            :ccAddr,
//            :bccAddr,
//            :body,
//            :HttpHelper.getServerName(null)
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3 = HttpHelper.getServerName(null);
   String theSqlTS = "insert into email_tracking\n        (\n          email_seq_num,\n          from_addr,\n          to_addr,\n          subject,\n          cc_addr,\n          bcc_addr,\n          body,\n          server_name\n        )\n        values\n        (\n           :1  ,\n           :2  ,\n           :3  ,\n           :4  ,\n           :5  ,\n           :6  ,\n           :7  ,\n           :8  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.net.EmailQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,emailSeqNum);
   __sJT_st.setString(2,fromAddr);
   __sJT_st.setString(3,toAddr);
   __sJT_st.setString(4,subject);
   __sJT_st.setString(5,ccAddr);
   __sJT_st.setString(6,bccAddr);
   __sJT_st.setString(7,body);
   __sJT_st.setString(8,__sJT_3);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:559^7*/
    }
    catch(Exception e)
    {
      logEntry("updateTrackingTable(" + emailSeqNum + ")", e.toString());
    }
  }

  /*
  ** public static void push()
  **
  ** Entry point to push a email queue item onto the email queue.
  */
  public static void push(EmailQueueItem item)
  {
    EmailQueue queue = new EmailQueue();
    queue.pushItem(item);
  }   
  
  /*
  ** public static void push()
  **
  ** Entry point to push a email queue item onto the email queue. Allows you to select direct connection
  */
  public static void push(EmailQueueItem item, boolean doDirect)
  {
    EmailQueue queue = new EmailQueue(doDirect);
    queue.pushItem(item);
  }   


  /*
  ** public static EmailQueueItem getItem()
  **
  ** Retrieves an EmailQueueItem from the database for easy manipulation 
  ** by outside processes
  */
  public static EmailQueueItem getItem(long emailSeqNum)
  {
    EmailQueueItem  item = null;
    
    EmailQueue queue = new EmailQueue();
    item = queue.getQueueItem(emailSeqNum);
    
    return item;
  }
}/*@lineinfo:generated-code*/