/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/XMLMessage.java $

  Description:  
  
    XMLMessage

    Generic base for xml messages.  Contains an xml document and an
    accessor.  Extends SQLJConnectionBase so that child classes can
    access database.
    
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.WebLogicPropertiesFile;

public class XMLMessage extends SQLJConnectionBase
{
    public static final String REDIRECTOR_HOST = "com.mes.redirectorHost";
    public static final String REDIRECTOR_PORT = "com.mes.redirectorPort";
  protected Document  doc             = null;
  
  private   String    targetUrl       = "";
  private   String    contentType     = "text/xml";
  private   String    redirectorHost;
  private   int       redirectorPort;
  
  private void initialize()
  {
    WebLogicPropertiesFile propFile = null;
    
    try
    {
      propFile = new WebLogicPropertiesFile( "mes.properties" );
    }
    catch( Exception e )
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new WebLogicPropertiesFile( inputStream );
      
      try { inputStream.close(); } catch(Exception re) {}
    }
    redirectorHost = propFile.getString(REDIRECTOR_HOST);
    redirectorPort = propFile.getInt(REDIRECTOR_PORT);
  }
  
  /*
  ** CONSTRUCTORS
  */
  public XMLMessage()
  {
    initialize();
  }
  public XMLMessage(Document newDoc)
  {
    setDocument(newDoc);
    initialize();
  }
  public XMLMessage(XMLMessage thatMsg)
  {
    setDocument(thatMsg.getDocument());
    initialize();
  }
  public XMLMessage(XDoc xDoc)
  {
    setDocument(xDoc.getDoc());
    initialize();
  }
  
  protected void buildDocument()
  {
  }
  
  /*
  ** ACCESSORS
  */
  public Document getDocument()
  {
    if (doc == null)
    {
      buildDocument();
    }
    return doc;
  }
  public void setDocument(Document newDoc)
  {
    doc = newDoc;
  }

  public String getTargetUrl()
  {
    return targetUrl;
  }
  public void setTargetUrl(String url)
  {
    targetUrl = url;
  }
  
  public String getContentType()
  {
    return contentType;
  }
  public void setContentType(String contentType)
  {
    this.contentType = contentType;
  }
 
  public String getRedirectorHost()
  {
    return redirectorHost;
  }
  public void setRedirectorHost(String redirectorHost)
  {
    this.redirectorHost = redirectorHost;
  }
  
  public int getRedirectorPort()
  {
    return redirectorPort;
  }
  public void setRedirectorPort(int redirectorPort)
  {
    this.redirectorPort = redirectorPort;
  }
  
  /*
  ** public String getMessageType()
  **
  ** Used in logging routines for one, this basic type should be overriden in
  ** child classes to indicate the actual specialized message type.
  **
  ** RETURNS: null.
  */
  public String getMessageType()
  {
    return null;
  }
  public String toString()
  {
    String  host        = getRedirectorHost();
    int     port        = getRedirectorPort();
    String  target      = getTargetUrl();
    String  contentType = getContentType();

    String  xml         = null;
    
    try
    {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(getDocument(),baos);
      xml = baos.toString();
    }
    catch (Exception e)
    {
    }
    
    return this.getClass().getName() + " (XMLMessage):"
      + "\n  host - " + host
      + "\n  port - " + port
      + "\n  target - " + target
      + "\n  contentType - " + contentType
      + "\n  xml - \n" + xml;
  }
}
