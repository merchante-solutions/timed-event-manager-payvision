/*@lineinfo:filename=EmailQueueHelper*//*@lineinfo:user-code*//*@lineinfo:1^1*//**
 * This class is used by EmailQueueItem to load addresses from the database.
 * It allows SQLJConnectionBase dependence to be removed from the item class,
 * which prevents serialization problems when SQLJConnectionBase is modified.
 */
package com.mes.net;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class EmailQueueHelper extends SQLJConnectionBase
{
  public static class HelperAddress
  {
    public String address;
    public int addressType;
  }

  public EmailQueueHelper(boolean directFlag)
  {
    if (directFlag)
    {
      initialize( getDirectConnectString() );
    }
  }

  private List _getAddressesByType(int addressId) throws Exception
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:40^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    address_type-1 address_type,
//                    address
//          from      email_addresses
//          where     email_id = :addressId
//                    and enabled = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    address_type-1 address_type,\n                  address\n        from      email_addresses\n        where     email_id =  :1 \n                  and enabled = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.EmailQueueHelper",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,addressId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.net.EmailQueueHelper",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:47^7*/
      rs = it.getResultSet();

      int addressCount = 0;      
      List addrList = new ArrayList();
      while (rs.next())
      {
        HelperAddress addr = new HelperAddress();
        addr.address = rs.getString("address");
        addr.addressType = rs.getInt("address_type");
        addrList.add(addr);
        ++addressCount;
      }
      
      rs.close();
      it.close();
      
      if (addressCount < 1)
      {
        throw new Exception("Invalid address set ID");
      }
      return addrList;
    }
    catch (Exception e)
    {
      String func = "getAddressesByType(addressId = " + addressId + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
      throw e;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public static List getAddressesByType(int addressId, boolean directFlag) 
    throws Exception
  {
    return (new EmailQueueHelper(directFlag))._getAddressesByType(addressId);
  }
}/*@lineinfo:generated-code*/