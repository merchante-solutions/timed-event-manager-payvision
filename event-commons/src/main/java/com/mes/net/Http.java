/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/Http.java $

  Description:

    Http

    Send Http requests.

  Last Modified By   : $Author: brice $
  Last Modified Date : $Date: 2013-03-22 11:59:46 -0800 (Fri, 22 Mar 2013) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Send HTTP requests via GET/POST.
 * @author brice
 *
 */
public class Http {
	
	public static int METHOD_POST = 0;
	public static int METHOD_GET = 1;
	
	// Parameters
	protected String requestString = "";
	protected String hostUrl = "";
	protected int method = METHOD_POST;
	protected boolean verbose = false;
	protected float timeout = 20.0f;
	protected String contentType = "application/x-www-form-urlencoded;charset=UTF-8";
	// Results
	protected int httpCode;
	protected boolean connected = false;
	protected String httpResponse = "";
	protected String rawResponse = "";
	protected float duration;
	
	/**
	 * Create a new HTTP object.
	 */
	public Http() {
		
	}
	
	/**
	 * Processes the HTTP request.
	 */
	public void run() throws SocketTimeoutException, IOException {
		HttpURLConnection connection = null;
		String resp = new String();
		
		long start = System.currentTimeMillis();
		try {
			if(verbose)
				System.out.print("Connecting to: "+hostUrl+" ... ");
			
			if(method == METHOD_GET)
				connection = (HttpURLConnection) new URL(hostUrl.concat("?").concat(requestString)).openConnection();
			else {
				connection = (HttpURLConnection) new URL(hostUrl).openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", contentType);
				connection.setRequestProperty("Accept-Charset", "UTF-8");
//				connection.setReadTimeout(timeout); // JDK 1.5+ only
				connection.setDoInput(true);
				
				
				OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
				writer.write(requestString);
				writer.flush();
				writer.close();
				
				
				this.httpCode = connection.getResponseCode();
				this.httpResponse = connection.getResponseMessage();
				
				if(verbose)
					System.out.println(httpCode + ":" + httpResponse);
			}
			
			if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(
					new InputStreamReader(connection.getInputStream())
				);
				
				String inputLine = new String();
				while( (inputLine = reader.readLine()) != null) {
					resp += inputLine;
				}
				reader.close();
				if(verbose)
					System.out.println(resp);
			}
			connected = true;
		} catch (SocketTimeoutException e) {
			throw new SocketTimeoutException("Request timed out after "+timeout+"ms.");
		} finally {
			if(connection != null)
				connection.disconnect();
			this.duration = System.currentTimeMillis() - start;
			this.rawResponse = resp;
		}
	}
	
	public Http setMethod(int method) {
		this.method = method;
		return this;
	}
	
	public Http setTimeout(float timeout) {
		this.timeout = timeout;
		return this;
	}
	
	public Http setHostUrl(String url) {
		this.hostUrl = url;
		return this;
	}
	
	public Http setRequest(String request) {
		this.requestString = request;
		return this;
	}
	
	public Http setVerbose(boolean verbose) {
		this.verbose = verbose;
		return this;
	}
	
	public Http setContentType(String type) {
		this.contentType = type;
		return this;
	}
	
	public static Http getInstance() {
		return new Http();
	}
	
	public String getRequestString() {
		return requestString;
	}
	
	/**
	 * Checks whether a connection was made with the host URL.
	 * @return
	 */
	public boolean connectOk() {
		return connected;
	}

	/**
	 * Returns the HTTP status code (eg. 404).
	 * @return
	 */
	public int getHttpCode() {
		return httpCode;
	}

	/**
	 * Returns a textual response of the HTTP status code.
	 * @return
	 */
	public String getHttpText() {
		return httpResponse;
	}

	/**
	 * Returns the response body.
	 * @return
	 */
	public String getResponse() {
		return rawResponse;
	}

	/**
	 * Gets the duration of the entire request.
	 * @return
	 */
	public float getDuration() {
		return duration;
	}
	
}
