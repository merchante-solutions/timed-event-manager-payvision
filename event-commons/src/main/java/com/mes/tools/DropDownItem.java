package com.mes.tools;

public class DropDownItem
{
  private String value;
  private String description;
  private String style;

  public DropDownItem(String value, String description, String style)
  {
    this.value = value;
    this.description = description;
    this.style = style;
  }
  public DropDownItem(String value, String description)
  {
    this(value,description,null);
  }
  public DropDownItem(int value, String description)
  {
    this(String.valueOf(value),description);
  }

  public void setValue(String value)
  {
    this.value = value;
  }
  public String getValue()
  {
    return value;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setStyle(String style)
  {
    this.style = style;
  }
  public String getStyle()
  {
    return style;
  }

  public boolean isEnabled()
  {
    return true;
  }

  public String toString()
  {
    return "DropDownItem [ "
     + "value: " + value
     + ", description: " + description
     + ", style: " + style
     + (isEnabled() ? "" : " (disabled)")
     + " ]";
  }
}