/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/FileUtils.java $

  Description:  FileUtility.java

  Designed to handle generic and mes-specific File-related processes

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;

public class FileUtils
{

  // create class log category
  static Logger log = Logger.getLogger(FileUtils.class);

  /**
   * getFileType - finds the extension and provides
   * an int reference for switching - will ignore
   * any non-like extension
   * @param String - fileName
   **/
  public static int getFileType(String fileName)
  {

    if(null==fileName)
    {
      return 0;
    }

    int type = 0;
    String ext = FileUtils.getExtension(fileName);

    //look for match
    if(ext.equalsIgnoreCase("pdf"))
    {
      type = mesConstants.FILETYPE_PDF;
    }
    else if (ext.equalsIgnoreCase("doc"))
    {
      type = mesConstants.FILETYPE_DOC;
    }
    else if (ext.equalsIgnoreCase("xls"))
    {
      type = mesConstants.FILETYPE_XLS;
    }
    else if (ext.equalsIgnoreCase("txt"))
    {
      type = mesConstants.FILETYPE_TXT;
    }
    else if (ext.equalsIgnoreCase("csv"))
    {
      type = mesConstants.FILETYPE_CSV;
    }
    else if (ext.equalsIgnoreCase("tif") || ext.equalsIgnoreCase("tiff"))
    {
      type = mesConstants.FILETYPE_TIF;
    }
    else if (ext.equalsIgnoreCase("gif"))
    {
      type = mesConstants.FILETYPE_GIF;
    }
    else if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg"))
    {
      type = mesConstants.FILETYPE_JPG;
    }
    else if (ext.equalsIgnoreCase("bmp"))
    {
      type = mesConstants.FILETYPE_BMP;
    }
    else if (ext.equalsIgnoreCase("pgp"))
    {
      type = mesConstants.FILETYPE_PGP;
    }
    else if (ext.equalsIgnoreCase("enc"))
    {
      type = mesConstants.FILETYPE_ENC;
    }
    return type;

  }


  /**
   * getBytesFromFile - Returns the contents of the file in a byte array.
   * @param File
   **/
  public static byte[] getBytesFromFile(File file)
  throws IOException
  {

    InputStream is = new FileInputStream(file);

    // Get the size of the file
    long length = file.length();

    // You cannot create an array using a long type.
    // It needs to be an int type.
    // Before converting to an int type, check
    // to ensure that file is not larger than Integer.MAX_VALUE.
    if (length > Integer.MAX_VALUE) {
        // File is too large
        throw new IOException("File too large: Could not read file "+file.getName());
    }

    // Create the byte array to hold the data
    byte[] bytes = new byte[(int)length];

    // Read in the bytes
    int offset = 0;
    int numRead = 0;
    while (offset < bytes.length
           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
        offset += numRead;
    }

    // Ensure all the bytes have been read in
    if (offset < bytes.length) {
        throw new IOException("Could not completely read file "+file.getName());
    }

    // Close the input stream and return bytes
    is.close();
    return bytes;

  }//getBytesFromFile


  //generic process to get byte[] from given path
  public static byte[] getBytesFromFile(String filename)
  throws Exception
  {
    byte[] data = null;

    if(filename != null)
    {
      try
      {
        data = FileUtils.getBytesFromFile(new File(filename));
      }
      catch(Exception nff)
      {
        nff.printStackTrace();
        log.debug("unable to handle file:: " + filename);
        throw nff;
      }
    }

    return data;

  }



  //TODO: Exception handling
  public static boolean writeByteArrayToFile(byte[] data, String fileName)
  {
    boolean result = false;

    try{

      FileOutputStream fos = new FileOutputStream(fileName);
      fos.write(data,0,data.length);
      fos.close();
      result=true;
    }
    catch (Exception e)
    {
      //System.out.println("outputFile() exception = "+e.getMessage());
    }

    return result;

  }

  public static String getNameFromFullPath(String filePathAndName)
  {
    if (null==filePathAndName || filePathAndName.equals(""))
    {
      //you get what you give
      return filePathAndName;
    }

    int lastSeparator = filePathAndName.lastIndexOf(File.separator);

    if(lastSeparator>-1)
    {

      //only get the filename, if it exists after the seperator
      try
      {
        return filePathAndName.substring(lastSeparator+1);
      }
      catch(Exception e)
      {
        return "";
      }
    }
    else
    {
      //not a full path... return whatever was given
      return filePathAndName;
    }
  }

  public static String getExtension(String fileName)
  {
    String ext;
    int index;

    if(fileName == null)
    {
      return "";
    }

    index = fileName.lastIndexOf(".");

    if( index > -1 )
    {
      //get extension - will return whatever is after the last '.'
      ext = fileName.substring(index + 1);
    }
    else
    {
      ext = "";
    }

    return ext.toLowerCase();

  }

  public static String removeExtension(String fileName)
  {

    fileName = getNameFromFullPath(fileName);

    if (null==fileName || fileName.equals(""))
    {
      return fileName;
    }

    if(fileName.indexOf(".")>-1)
    {
      return fileName.substring(0,fileName.lastIndexOf("."));
    }
    else
    {
      return fileName;
    }
  }

  public static String revertExtension(String newFileName, String oldFileName)
  {
    if( newFileName==null ||
        newFileName.trim().equals("") )
    {
      return oldFileName;
    }
    else
    {
      //always use the oldFileName's extension
      StringBuffer sb = new StringBuffer(newFileName.length()+4);
      sb.append(FileUtils.removeExtension(newFileName));
      sb.append(".");
      sb.append(FileUtils.getExtension(FileUtils.getNameFromFullPath(oldFileName)));
      return sb.toString();
    }
  }

  //quick(?) validation of accepted extensions
  public static boolean validateExtension(String fileName, String[] exts)
  {
    if(fileName == null || exts == null)
    {
      return false;
    }

    String ext = (FileUtils.getExtension(fileName)).toLowerCase();

    boolean result = false;

    for (int i = 0; i < exts.length; i++)
    {
      if(exts[i].equals(ext))
      {
        result = true;
        break;
      }
    }

    return result;
  }

  public static boolean validateExtension(String fileName)
  {
    return validateExtension(fileName, DEFAULT_EXTENSIONS);
  }

  public static final String[] DEFAULT_EXTENSIONS =
  {
    "pdf",
    "doc",
    "xls",
    "xml",
    "txt",
    "csv",
    "tif",
    "tiff",
    "jpg",
    "jpeg",
    "gif",
    "bmp",
    "pgp",
    "ipm",
    "dat"
  };


}