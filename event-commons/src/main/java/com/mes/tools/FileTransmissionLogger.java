/*@lineinfo:filename=FileTransmissionLogger*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/FileTransmissionLogger.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import  com.mes.database.SQLJConnectionBase;

public class FileTransmissionLogger extends SQLJConnectionBase
{
  public static final int     STAGE_RECEIVED      = 1;
  public static final int     STAGE_LOADED        = 2;
  public static final int     STAGE_COPIED        = 3;
  public static final int     STAGE_TRAN_SUMMARY  = 4;
  public static final int     STAGE_RISK_SUMMARY  = 5;
  
  private void logReceived(String loadFilename, long fileSize, int bankNumber)
  {
    try
    {
      // update the existing record
      /*@lineinfo:generated-code*//*@lineinfo:44^7*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmissions
//          set     received = sysdate,
//                  file_size = :fileSize,
//                  bank_number = :bankNumber
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmissions\n        set     received = sysdate,\n                file_size =  :1 ,\n                bank_number =  :2 \n        where   load_filename =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,fileSize);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:51^7*/
    }
    catch(Exception e)
    {
      //@ System.out.println("logReceived(" + loadFilename + "): " + e.toString());
      logEntry("logReceived(" + loadFilename + ")", e.toString());
    }
  }
  
  private void logLoaded(String loadFilename)
  {
    try
    {
      // update existing record
      /*@lineinfo:generated-code*//*@lineinfo:65^7*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmissions
//          set     loaded = sysdate
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmissions\n        set     loaded = sysdate\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:70^7*/
    }
    catch(Exception e)
    {
      logEntry("logLoaded(" + loadFilename + ")", e.toString());
    }
  }
  
  private void logCopied(String loadFilename)
  {
    try
    {
      // update existing record
      /*@lineinfo:generated-code*//*@lineinfo:83^7*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmissions
//          set     copied = sysdate
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmissions\n        set     copied = sysdate\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
    }
    catch(Exception e)
    {
      logEntry("logCopied(" + loadFilename + ")", e.toString());
    }
  }
  
  private void logTranSummary(String loadFilename)
  {
    try
    {
      // update existing record
      /*@lineinfo:generated-code*//*@lineinfo:101^7*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmissions
//          set     tran_summarized = sysdate
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmissions\n        set     tran_summarized = sysdate\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/
    }
    catch(Exception e)
    {
      logEntry("logTranSummary(" + loadFilename + ")", e.toString());
    }
  }
  
  private void logRiskSummary(String loadFilename)
  {
    try
    {
      // update existing record
      /*@lineinfo:generated-code*//*@lineinfo:119^7*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmissions
//          set     risk_summarized = sysdate
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmissions\n        set     risk_summarized = sysdate\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/
    }
    catch(Exception e)
    {
      logEntry("logRiskSummary(" + loadFilename + ")", e.toString());
    }
  }
  
  public void doLogStage(String loadFilename, int loadStage, long fileSize, int bankNumber)
  {
    try
    {
      connect();
      // determine if this load filename already exists.  if not, insert a 
      // blank record so that it can be updated
      int existCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)
//          
//          from    file_transmissions
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)\n         \n        from    file_transmissions\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.FileTransmissionLogger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   existCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^7*/
      
      if(existCount == 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:151^9*/

//  ************************************************************
//  #sql [Ctx] { insert into file_transmissions
//            (
//              load_filename,
//              received,
//              copied
//            )
//            values
//            (
//              :loadFilename,
//              sysdate,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into file_transmissions\n          (\n            load_filename,\n            received,\n            copied\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.tools.FileTransmissionLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^9*/
      }
      
      switch(loadStage)
      {
        case STAGE_RECEIVED:
          logReceived(loadFilename, fileSize, bankNumber);
          break;
          
        case STAGE_LOADED:
          logLoaded(loadFilename);
          break;
          
        case STAGE_COPIED:
          logCopied(loadFilename);
          break;
          
        case STAGE_TRAN_SUMMARY:
          logTranSummary(loadFilename);
          break;
          
        case STAGE_RISK_SUMMARY:
          logRiskSummary(loadFilename);
          break;
          
        default:
          break;
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("logStage(" + loadFilename + ", " + loadStage + "): " + e.toString());
      logEntry("logStage(" + loadFilename + ", " + loadStage + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static void logStage(String loadFilename, int loadStage, long fileSize, int bankNumber)
  {
    try
    {
      FileTransmissionLogger worker = new FileTransmissionLogger();
      
      worker.doLogStage(loadFilename, loadStage, fileSize, bankNumber);
    }
    catch(Exception e)
    {
      //@ System.out.println("com.mes.tools.FileTransmissionLogger::logStage(" + loadFilename + ", " + loadStage + "): " + e.toString());
      com.mes.support.SyncLog.LogEntry("com.mes.tools.FileTransmissionLogger::logStage(" + loadFilename + ", " + loadStage + ")", e.toString());
    }
  }
  
  public static void logStage(String loadFilename, int loadStage)
  {
    logStage(loadFilename, loadStage, -1, -1);
  }
}/*@lineinfo:generated-code*/