/*@lineinfo:filename=AppNotifyBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/AppNotifyBean.sqlj $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/15/04 3:10p $
  Version            : $Revision: 32 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.net.VistaApprovalMessage;
import com.mes.net.VistaDeclineMessage;
import com.mes.net.VistaResponseMessage;
import com.mes.net.XMLMessage;
import com.mes.net.XMLSender;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;


public class AppNotifyBean extends com.mes.database.SQLJConnectionBase
{
  private   int     appType     = -1;

  public AppNotifyBean()
  {
    super();
  }

  public AppNotifyBean(DefaultContext defCtx)
  {
    super();

    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }

  public int getAppType(long appSeqNum)
  {
    int appType = -1;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:64^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type 
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type  \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:69^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + appSeqNum + ")", e.toString());
    }

    return appType;
  }

  private boolean isVirtualApp(long appSeqNum)
  {
    boolean result = false;
    try
    {
      String loginName = "";
      /*@lineinfo:generated-code*//*@lineinfo:85^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_user_login 
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_user_login  \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loginName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/

      if(loginName.equals("vs-virtualapp"))
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("isVirtualApp(" + appSeqNum + ")", e.toString());
    }

    return result;
  }

  public boolean isValidAppType(int appType)
  {
    boolean result = false;

    switch(appType)
    {
      case mesConstants.APP_TYPE_MES:
      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_CBT_NEW:
      case mesConstants.APP_TYPE_VERISIGN:
      case mesConstants.APP_TYPE_DEMO:
      case mesConstants.APP_TYPE_CEDAR:
      case mesConstants.APP_TYPE_ORANGE:
      case mesConstants.APP_TYPE_SVB:
      case mesConstants.APP_TYPE_NSI:
      case mesConstants.APP_TYPE_TRANSCOM:
      case mesConstants.APP_TYPE_DISCOVER:
      case mesConstants.APP_TYPE_NBSC:
      case mesConstants.APP_TYPE_BBT:
      case mesConstants.APP_TYPE_DISCOVER_IMS:
      case mesConstants.APP_TYPE_VISTA:
      case mesConstants.APP_TYPE_GOLD:
      case mesConstants.APP_TYPE_SUMMIT:
      case mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL:
      case mesConstants.APP_TYPE_WEST_AMERICA:
      case mesConstants.APP_TYPE_VISA:
      case mesConstants.APP_TYPE_MESA:
      case mesConstants.APP_TYPE_AGENT:
      case mesConstants.APP_TYPE_INTAGIO:
      case mesConstants.APP_TYPE_MESB:
      case mesConstants.APP_TYPE_RIVER_CITY:
      case mesConstants.APP_TYPE_FOOTHILL:
      case mesConstants.APP_TYPE_VERISIGN_V2:
      case mesConstants.APP_TYPE_ELM_NON_DEPLOY:
      case mesConstants.APP_TYPE_MES_NEW:
      case mesConstants.APP_TYPE_MOUNTAIN_WEST:
      case mesConstants.APP_TYPE_BANNER:
      case mesConstants.APP_TYPE_MESC:
      case mesConstants.APP_TYPE_NET_SUITE:
      case mesConstants.APP_TYPE_SABRE:
      case mesConstants.APP_TYPE_AUTHNET:
      case mesConstants.APP_TYPE_GBB:
      case mesConstants.APP_TYPE_FMST:
      case mesConstants.APP_TYPE_BCB:
      case mesConstants.APP_TYPE_CSB:
      case mesConstants.APP_TYPE_FMBANK:
      case mesConstants.APP_TYPE_FFB:
      case mesConstants.APP_TYPE_STERLING:
      case mesConstants.APP_TYPE_NBSC2:
      case mesConstants.APP_TYPE_VPS_ISO:
      case mesConstants.APP_TYPE_EXCHANGE:
        result = true;
        break;

      default:
        result = false;
        break;
    }

    return result;
  }

  private int cardType;
  public void setCardType(String cardType)
  {
    try
    {
      setCardType(Integer.parseInt(cardType));
    }
    catch(Exception e)
    {
      logEntry("setCardType(" + cardType + ")", e.toString());
    }
  }
  public void setCardType(int cardType)
  {
    this.cardType = cardType;
  }
  public int getCardType()
  {
    return cardType;
  }

  /*
  ** notifySabreStatus
  */
  private boolean notifySabreStatus(long appSeqNum, int status)
  {
    long        merchantId    = 0L;
    int         recCount      = 0;
    boolean     result        = false;


    try
    {
      // if this is a SETUP_COMPLETE status update,
      // insert an entry in the sabre_process table
      // to queue the pcc prenotification to Sabre.
      if ( status == mesConstants.APP_STATUS_SETUP_COMPLETE )
      {
        /*@lineinfo:generated-code*//*@lineinfo:205^9*/

//  ************************************************************
//  #sql [Ctx] { select  mr.merch_number 
//            from    merchant    mr
//            where   mr.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.merch_number  \n          from    merchant    mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^9*/

        /*@lineinfo:generated-code*//*@lineinfo:212^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(sp.merchant_number) 
//            from    sabre_process sp
//            where   sp.merchant_number = :merchantId and
//                    sp.process_type = 1 and -- pcc pre-notification
//                    sp.process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sp.merchant_number)  \n          from    sabre_process sp\n          where   sp.merchant_number =  :1  and\n                  sp.process_type = 1 and -- pcc pre-notification\n                  sp.process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^9*/

        // only insert a new entry if one does not
        // already exist in the database.
        if ( recCount == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:225^11*/

//  ************************************************************
//  #sql [Ctx] { insert into sabre_process
//              (
//                process_type,
//                merchant_number,
//                load_filename
//              )
//              values
//              (
//                1,            -- pcc prenotification
//                :merchantId,
//                'AppNotifyBean'
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into sabre_process\n            (\n              process_type,\n              merchant_number,\n              load_filename\n            )\n            values\n            (\n              1,            -- pcc prenotification\n               :1 ,\n              'AppNotifyBean'\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.tools.AppNotifyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^11*/
        }
      }

      result = true;
    }
    catch(Exception e)
    {
      logEntry("notifySabreStatus(" + appSeqNum + ", " + status + ")", e.toString());
    }

    return( result );
  }

  /*
  ** notifyTranscomStatus
  **
  ** Puts entries in the TRANSCOM_MERCHANT_UPDATE table which are then used
  ** when the merchant update process fires (twice daily)
  */
  private boolean notifyTranscomStatus(long appSeqNum, int status)
  {
    boolean result = false;

    try
    {
      if(status != mesConstants.APP_STATUS_INCOMPLETE)
      {
        int count;

        String updateTableName = "";
        if(appType == mesConstants.APP_TYPE_MES_NEW)
        {
          updateTableName = "transcom_merchant_update_mes";
        }
        else
        {
          updateTableName = "transcom_merchant_update";
        }

        // see if app already exists
        /*@lineinfo:generated-code*//*@lineinfo:280^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    :updateTableName
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(app_seq_num)\n           \n          from     ");
   __sjT_sb.append(updateTableName);
   __sjT_sb.append(" \n          where   app_seq_num =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.tools.AppNotifyBean:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^9*/

        if(count > 0)
        {
          // update the status
          /*@lineinfo:generated-code*//*@lineinfo:291^11*/

//  ************************************************************
//  #sql [Ctx] { update  :updateTableName
//              set     status = :status,
//                      process_job = -1
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(updateTableName);
   __sjT_sb.append(" \n            set     status =  ? ,\n                    process_job = -1\n            where   app_seq_num =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.tools.AppNotifyBean:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^11*/
        }
        else
        {
          // insert a new record
          /*@lineinfo:generated-code*//*@lineinfo:302^11*/

//  ************************************************************
//  #sql [Ctx] { insert into :updateTableName
//              (
//                app_seq_num,
//                status,
//                process_job
//              )
//              values
//              (
//                :appSeqNum,
//                :status,
//                -1
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into  ");
   __sjT_sb.append(updateTableName);
   __sjT_sb.append(" \n            (\n              app_seq_num,\n              status,\n              process_job\n            )\n            values\n            (\n               ? ,\n               ? ,\n              -1\n            )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "7com.mes.tools.AppNotifyBean:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,status);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^11*/
        }

        // add entry to transcom_equipment_update if necessary
        if(appType == mesConstants.APP_TYPE_MES_NEW && status == mesConstants.APP_STATUS_SETUP_COMPLETE)
        {
          /*@lineinfo:generated-code*//*@lineinfo:322^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//              from    transcom_equipment_update
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n            from    transcom_equipment_update\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:327^11*/

          // only insert into this table if there are no previous entries for this app
          if(count == 0)
          {
            /*@lineinfo:generated-code*//*@lineinfo:332^13*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_equipment_update
//                (
//                  app_seq_num
//                )
//                values
//                (
//                  :appSeqNum
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_equipment_update\n              (\n                app_seq_num\n              )\n              values\n              (\n                 :1 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.tools.AppNotifyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^13*/
          }
        }
      }

      result = true;
    }
    catch(Exception e)
    {
      logEntry("notifyTranscomStatus(" + appSeqNum + ", " + status + ")", e.toString());
      //@System.out.println("notifyTranscomStatus(" + appSeqNum + ", " + status + ") " + e.toString());
    }

    return result;
  }
  
  private void notifyVPSDecline(long appSeqNum)
  {
    StringBuffer        body    = new StringBuffer("");
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_VPS_ISO_CREDIT);
      msg.setSubject("VPS ISO Applicaton Decline notice");
      
      // get app data
      /*@lineinfo:generated-code*//*@lineinfo:371^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  aq.app_status_reason    reason_code,
//                  qr.qreason_short_descr  short_reason,
//                  qr.qreason_long_descr   long_reason,
//                  mr.merch_business_name  merchant_name
//          from    app_queue aq,
//                  merchant  mr,
//                  qreason   qr
//          where   aq.app_seq_num = :appSeqNum and
//                  aq.app_queue_type = 1 and
//                  aq.app_seq_num = mr.app_seq_num and
//                  aq.app_status_reason = qr.qreason_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  aq.app_status_reason    reason_code,\n                qr.qreason_short_descr  short_reason,\n                qr.qreason_long_descr   long_reason,\n                mr.merch_business_name  merchant_name\n        from    app_queue aq,\n                merchant  mr,\n                qreason   qr\n        where   aq.app_seq_num =  :1  and\n                aq.app_queue_type = 1 and\n                aq.app_seq_num = mr.app_seq_num and\n                aq.app_status_reason = qr.qreason_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.tools.AppNotifyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.tools.AppNotifyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:384^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        body.append("A merchant online application has been declined via the MeS Credit process:\n\n ");
        body.append("Merchant Name: ");
        body.append(rs.getString("merchant_name"));
        body.append(" (https://www.merchante-solutions.com/jsp/setup/merchinfo4.jsp?primaryKey=");
        body.append(appSeqNum);
        body.append(")\n");
        body.append("Decline Reason: ");
        body.append(rs.getString("short_reason"));
        body.append(" (");
        body.append(rs.getString("long_reason"));
        body.append(")\n\n");
        body.append("View Notes: https://www.merchante-solutions.com/jsp/credit/mes_tracking_notes.jsp?primaryKey=");
        body.append(appSeqNum);
        body.append("&department=-1&sortorder=1\n\n");
      }
      
      rs.close();
      it.close();
      
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("notifyVPSDecline(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private boolean notifyVerisignStatus(long appSeqNum, int status, int xmlFormat)
  {
    boolean result = false;
    try
    {
      // first notify via XML Registration engine
      switch(status)
      {
        case mesConstants.APP_STATUS_APPROVED:
        case mesConstants.APP_STATUS_INCOMPLETE:
        case mesConstants.APP_STATUS_PENDED:
          // no notification needed
          break;

        case mesConstants.APP_STATUS_COMPLETE:
        case mesConstants.APP_STATUS_DECLINED:
        case mesConstants.APP_STATUS_CANCELLED:
        case mesConstants.APP_STATUS_SETUP_COMPLETE:
        case mesConstants.APP_STATUS_ADDED_CARD:
        case mesConstants.APP_STATUS_REMOVED_CARD:
          // insert record into the vs xml message queue
          long nextQueueSeqNum = 0L;
          /*@lineinfo:generated-code*//*@lineinfo:445^11*/

//  ************************************************************
//  #sql [Ctx] { select  vs_xml_sequence.nextval
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vs_xml_sequence.nextval\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.tools.AppNotifyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nextQueueSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:450^11*/

          /*@lineinfo:generated-code*//*@lineinfo:452^11*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_xml_queue
//                ( queue_seq_num,
//                  app_seq_num,
//                  msg_type,
//                  card_type,
//                  xml_success,
//                  xml_failures,
//                  side_success,
//                  side_failures,
//                  created,
//                  format_type )
//              values
//                ( :nextQueueSeqNum,
//                  :appSeqNum,
//                  :status,
//                  :cardType,
//                  'N',
//                  0,
//                  'N',
//                  0,
//                  sysdate,
//                  :xmlFormat )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into vs_xml_queue\n              ( queue_seq_num,\n                app_seq_num,\n                msg_type,\n                card_type,\n                xml_success,\n                xml_failures,\n                side_success,\n                side_failures,\n                created,\n                format_type )\n            values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                'N',\n                0,\n                'N',\n                0,\n                sysdate,\n                 :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.tools.AppNotifyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nextQueueSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,status);
   __sJT_st.setInt(4,cardType);
   __sJT_st.setInt(5,xmlFormat);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:476^11*/
          break;
      }
      
      // now notify via email for ISO apps
      if(appType == mesConstants.APP_TYPE_VPS_ISO)
      {
        switch(status)
        {
          case mesConstants.APP_STATUS_DECLINED:
            notifyVPSDecline(appSeqNum);
            break;
            
          default:
            // do nothing
            break;
        }
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("notifyVerisignStatus(" + appSeqNum + ", " + status + ")", e.toString());
      //@System.out.println("notifyVerisignStatus(" + appSeqNum + ", " + status + ") " + e.toString());
    }

    return result;
  }

  public boolean notifyVistaStatus(long appSeqNum, int status)
  {
    boolean               result      = false;
    try
    {
      XMLMessage reqMsg = null;
      switch(status)
      {
        // no notification needed
        case mesConstants.APP_STATUS_INCOMPLETE:
        case mesConstants.APP_STATUS_PENDED:
        case mesConstants.APP_STATUS_APPROVED:
        case mesConstants.APP_STATUS_COMPLETE:
        case mesConstants.APP_STATUS_ADDED_CARD:
        case mesConstants.APP_STATUS_REMOVED_CARD:
        default:
          break;

        // declined
        case mesConstants.APP_STATUS_DECLINED:
          reqMsg = new VistaDeclineMessage(appSeqNum,"Declined by MES Credit");
          break;

        // "declined" due to cancellation
        case mesConstants.APP_STATUS_CANCELLED:
          reqMsg = new VistaDeclineMessage(appSeqNum,"Setup cancelled by MES");
          break;

        // "approved"
        case mesConstants.APP_STATUS_SETUP_COMPLETE:
          reqMsg = new VistaApprovalMessage(appSeqNum);
          break;
      }
      if (reqMsg != null)
      {

        // for now, send message directly to vista here
        // send messge, get response
        //@System.out.println("Sending Vista Message " + reqMsg.getMessageType());
        VistaResponseMessage respMsg = new VistaResponseMessage(
          XMLSender.sendMessage(reqMsg,"Vista XML Test"));
        //@System.out.println("Received Vista Response " + respMsg.getMessageType());
      }
      result = true;
    }
    catch(Exception e)
    {
      logEntry("notifyVistaStatus(" + appSeqNum + ", " + status + ")", e.toString());
      //@System.out.println("notifyVistaStatus(" + appSeqNum + ", " + status  + ") " + e.toString());
    }
    return result;
  }

  public boolean notifyNSIStatus(long appSeqNum, int status)
  {
    boolean               result      = false;
    ResultSetIterator     it          = null;
    MailMessage           msg         = null;
    SimpleDateFormat      simpleDate  = null;
    String                comDate     = "";
    StringBuffer          message     = new StringBuffer("");
    StringBuffer          subject     = new StringBuffer("");
    boolean               sendOK      = false;

    try
    {
      simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
      comDate     = simpleDate.format(new Date());

      // get necessary data about the account
      /*@lineinfo:generated-code*//*@lineinfo:576^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_business_name,
//                  m.merch_legal_name,
//                  m.merch_password,
//                  m.merch_email_address,
//                  m.merc_cntrl_number,
//                  m.merch_number,
//                  mc.merchcont_prim_phone
//          from    merchant m,
//                  merchcontact mc
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = mc.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_business_name,\n                m.merch_legal_name,\n                m.merch_password,\n                m.merch_email_address,\n                m.merc_cntrl_number,\n                m.merch_number,\n                mc.merchcont_prim_phone\n        from    merchant m,\n                merchcontact mc\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = mc.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.tools.AppNotifyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.tools.AppNotifyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:589^7*/

      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        switch(status)
        {
          case mesConstants.APP_STATUS_INCOMPLETE:
            // no notification needed
            break;

          case mesConstants.APP_STATUS_COMPLETE:
            subject.append(rs.getString("merch_legal_name"));
            subject.append(" Merchant E Application Completed");
            message.append("Merchant Account Completed by:\n");
            message.append("\nDate Completed: ");
            message.append(comDate);
            message.append("\nMerchant Name: ");
            message.append(rs.getString("merch_legal_name"));
            message.append("\nDBA: ");
            message.append(rs.getString("merch_business_name"));
            message.append("\nPhone: ");
            message.append(rs.getString("merchcont_prim_phone"));
            message.append("\nEmail Address: ");
            message.append(rs.getString("merch_email_address"));
            sendOK = true;
            break;

          case mesConstants.APP_STATUS_APPROVED:
            // send the approval notification to NSI
            subject.append(rs.getString("merch_legal_name"));
            subject.append(" Merchant E Application Approved");
            message.append("Merchant Account Approved for:\n");
            message.append("\nDate Approved: ");
            message.append(comDate);
            message.append("\nMerchant Name: ");
            message.append(rs.getString("merch_legal_name"));
            message.append("\nDBA: ");
            message.append(rs.getString("merch_business_name"));
            message.append("\nPhone: ");
            message.append(rs.getString("merchcont_prim_phone"));
            message.append("\nEmail Address: ");
            message.append(rs.getString("merch_email_address"));
            message.append("\nMerchant Account#: ");
            message.append(rs.getLong("merch_number"));
            message.append("\nMerchant E Control#: ");
            message.append(rs.getLong("merc_cntrl_number"));
            message.append("\n");
            sendOK  = true;
            break;

          case mesConstants.APP_STATUS_DECLINED:
            subject.append(rs.getString("merch_legal_name"));
            subject.append(" Merchant E Application DECLINED");
            message.append("Merchant Account Declined for:\n");
            message.append("\nDate Declined: ");
            message.append(comDate);
            message.append("\nMerchant Name: ");
            message.append(rs.getString("merch_legal_name"));
            message.append("\nDBA: ");
            message.append(rs.getString("merch_business_name"));
            message.append("\nPhone: ");
            message.append(rs.getString("merchcont_prim_phone"));
            message.append("\nEmail Address: ");
            message.append(rs.getString("merch_email_address"));
            message.append("\nMerchant E Control#: ");
            message.append(rs.getLong("merc_cntrl_number"));
            message.append("\n");
            sendOK  = true;
            break;

          case mesConstants.APP_STATUS_PENDED:
            break;

          case mesConstants.APP_STATUS_CANCELLED:
            break;

          case mesConstants.APP_STATUS_SETUP_COMPLETE:
            break;
        }

        if(sendOK)
        {
          msg = new MailMessage();
          msg.setAddresses(MesEmails.MSG_ADDRS_NSI_CREDIT_APPROVE);
          msg.setSubject(subject.toString());
          msg.setText(message.toString());
          msg.send();
        }

        result = true;
      }
      else
      {
        logEntry("notifyNSIStatus(" + appSeqNum + ", " + status + ")", "Empty ResultSet");
      }
    }
    catch(Exception e)
    {
      logEntry("notifyNSIStatus(" + appSeqNum + ", " + status + ")", e.toString());
    }

    return result;
  }

  public boolean notifyStatus(long appSeqNum, int status, int appType)
  {
    boolean success       = false;
    boolean wasStale      = false;

    try
    {
      //@System.out.println("\n\nNotifying 3rd party of application status change:");
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }

      this.appType = getAppType(appSeqNum);

      switch(this.appType)
      {
        case mesConstants.APP_TYPE_CBT:
        case mesConstants.APP_TYPE_CBT_NEW:
        case mesConstants.APP_TYPE_DEMO:
        case mesConstants.APP_TYPE_CEDAR:
        case mesConstants.APP_TYPE_ORANGE:
        case mesConstants.APP_TYPE_SVB:
          // no notification
          success = true;
          break;

        case mesConstants.APP_TYPE_VERISIGN:
          // don't notify verisign if this is not a virtual app
          if(isVirtualApp(appSeqNum))
          {
            success = notifyVerisignStatus(appSeqNum,
                                           status,
                                           mesConstants.VS_XML_FMT_ORIGINAL);
          }
          break;

        case mesConstants.APP_TYPE_VERISIGN_V2:
        case mesConstants.APP_TYPE_VPS_ISO:
          success = notifyVerisignStatus(appSeqNum,
                                         status,
                                         mesConstants.VS_XML_FMT_XML_REG);
          break;

        case mesConstants.APP_TYPE_NSI:
          success = notifyNSIStatus(appSeqNum, status);
          break;

        case mesConstants.APP_TYPE_VISTA:
          success = notifyVistaStatus(appSeqNum, status);
          break;

        case mesConstants.APP_TYPE_MES_NEW:
        case mesConstants.APP_TYPE_TRANSCOM:
          //@System.out.println("notifying Transcom of app status");
          success = notifyTranscomStatus(appSeqNum, status);
          break;

        case mesConstants.APP_TYPE_SABRE:
          success = notifySabreStatus(appSeqNum, status);
          break;

        default:
          // unknown application type
          success = true;
          break;
      }

      success = true;
      //@System.out.println("3rd party notification complete.\n\n");
    }
    catch(Exception e)
    {
      logEntry("notifyStatus(" + appSeqNum + ", " + status + ", " + this.appType + ")", e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }

    return success;
  }
}/*@lineinfo:generated-code*/