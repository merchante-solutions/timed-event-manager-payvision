/*@lineinfo:filename=MasterCardProductCodeConverter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/MasterCardProductCodeConverter.sqlj $

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2012-03-09 12:49:22 -0800 (Fri, 09 Mar 2012) $
  Version            : $Revision: 19945 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011, 2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MasterCardProductCodeConverter extends SQLJConnectionBase
{
  protected static final long       REFRESH_TIME_SECOND = 1000L;
  protected static final long       REFRESH_TIME_MINUTE = 60 * REFRESH_TIME_SECOND;
  protected static final long       REFRESH_TIME_HOUR   = 60 * REFRESH_TIME_MINUTE;
  
  protected static final long       REFRESH_TIME        = 24 * REFRESH_TIME_HOUR;
  
  protected static final String[][] ProductCodeDefaults = 
  {
    {"DLG","H"  },  {"DLH","H"  },  {"DLI","H"  },  {"DLP","H"  },  
    {"DLS","H"  },  {"DLU","H"  },  {"EXC","PD" },  {"HNR","PD" },  
    {"ITT","PD" },  {"JCB","DI" },  {"LNC","PD" },  {"MAB","G1" },  
    {"MAC","G1" },  {"MBD","G2" },  {"MBE","G"  },  {"MBK","C"  },  
    {"MCA","A"  },  {"MCB","G"  },  {"MCC","A"  },  {"MCE","A"  },  
    {"MCF","S1" },  {"MCG","A"  },  {"MCH","A"  },  {"MCM","G"  },  
    {"MCO","G"  },  {"MCP","S"  },  {"MCS","A"  },  {"MCT","A"  },  
    {"MCU","A"  },  {"MCV","A"  },  {"MCW","C"  },  {"MDB","G2" },  
    {"MDG","H"  },  {"MDH","H"  },  {"MDJ","H"  },  {"MDL","G2" },  
    {"MDM","S1" },  {"MDN","S"  },  {"MDO","H"  },  {"MDP","H"  },  
    {"MDQ","G"  },  {"MDR","H"  },  {"MDS","H"  },  {"MDT","G2" },  
    {"MDU","H"  },  {"MEB","G"  },  {"MEC","G"  },  {"MED","H"  },  
    {"MEF","S"  },  {"MEO","G"  },  {"MEP","H"  },  {"MFB","C"  },  
    {"MFD","C"  },  {"MFE","C"  },  {"MFH","C"  },  {"MFL","C"  },  
    {"MFW","C"  },  {"MGF","S1" },  {"MHA","J3" },  {"MHB","H"  },  
    {"MHC","A"  },  {"MHH","H"  },  {"MIA","J1" },  {"MIB","A"  },  
    {"MIC","A"  },  {"MID","H"  },  {"MIG","A"  },  {"MIH","A"  },  
    {"MIJ","H"  },  {"MIK","J1" },  {"MIL","J1" },  {"MIP","J1" },  
    {"MIS","H"  },  {"MIU","H"  },  {"MLA","G"  },  {"MLC","G"  },  
    {"MLD","G"  },  {"MLL","G"  },  {"MNF","S1" },  {"MNW","G1" },  
    {"MOG","PD" },  {"MOP","PD" },  {"MOW","PD" },  {"MPA","J1" },  
    {"MPB","G"  },  {"MPC","G"  },  {"MPF","J2" },  {"MPG","J1" },  
    {"MPJ","H"  },  {"MPK","J4" },  {"MPL","A"  },  {"MPM","J1" },  
    {"MPN","J3" },  {"MPO","J1" },  {"MPR","J1" },  {"MPT","J1" },  
    {"MPV","J3" },  {"MPW","J4" },  {"MPX","J3" },  {"MPY","J3" },  
    {"MPZ","J1" },  {"MRB","J4" },  {"MRC","J1" },  {"MRF","A"  },  
    {"MRG","J1" },  {"MRH","J1" },  {"MRJ","J1" },  {"MRK","J4" },  
    {"MRL","J4" },  {"MRO","J1" },  {"MRP","A"  },  {"MRS","J1" },  
    {"MRW","J4" },  {"MSA","J1" },  {"MSB","J4" },  {"MSD","A"  },  
    {"MSF","J1" },  {"MSG","J1" },  {"MSI","PD" },  {"MSJ","J1" },  
    {"MSM","J1" },  {"MSN","J3" },  {"MSO","J1" },  {"MSQ","J1" },  
    {"MSR","J1" },  {"MSS","PD" },  {"MST","PD" },  {"MSV","J3" },  
    {"MSW","J4" },  {"MSX","J3" },  {"MSY","J1" },  {"MSZ","J1" },  
    {"MUP","H"  },  {"MUS","J1" },  {"MUW","C"  },  {"MWB","G1" },  
    {"MWD","C"  },  {"MWE","C"  },  {"MWO","G1" },  {"MWR","C"  },  
    {"NYC","PD" },  {"OLB","G"  },  {"OLG","A"  },  {"OLI","A"  },  
    {"OLP","A"  },  {"OLS","A"  },  {"OLW","A"  },  {"PLU","PD" },  
    {"PRO","Q"  },  {"PUL","PD" },  {"PVA","Q"  },  {"PVB","Q"  },  
    {"PVC","Q"  },  {"PVD","Q"  },  {"PVE","Q"  },  {"PVF","Q"  },  
    {"PVG","Q"  },  {"PVH","Q"  },  {"PVI","Q"  },  {"PVJ","Q"  },  
    {"PVL","Q"  },  {"STR","PD" },  {"SUR","J1" },  {"TBE","G2" },  
    {"TCB","G2" },  {"TCC","H"  },  {"TCE","H"  },  {"TCF","G2" },  
    {"TCG","H"  },  {"TCO","G2" },  {"TCP","G2" },  {"TCS","H"  },  
    {"TCW","H"  },  {"TDN","G2" },  {"TEB","G2" },  {"TEC","G2" },  
    {"TEO","G2" },  {"TIB","H"  },  {"TIC","H"  },  {"TIU","H"  },  
    {"TLA","G2" },  {"TNF","G2" },  {"TNW","H"  },  {"TPB","G2" },  
    {"TPC","G2" },  {"TPL","H"  },
  };
  
  // singleton
  private static MasterCardProductCodeConverter   TheMasterCardProductCodeConverter  = null;
  
  // instance variables
  private   HashMap     ProductCodeMap    = new HashMap();
  private   long        LoadTimeMillis    = 0L;
  
  private MasterCardProductCodeConverter()
  {
    loadDataDefault();
  }
  
  public String decodeVisaProductCode( String mcProdCode )
  {
    String        retVal  = null;
    
    try
    {
      if ( (retVal = (String)ProductCodeMap.get(mcProdCode)) == null )
      {
        retVal = "M";
      }
    }
    catch( Exception e )
    {
      logEntry("decodeVisaProductCode(" + mcProdCode + ")",e.toString());
    }
    return( retVal );
  }
  
  public static MasterCardProductCodeConverter getInstance()
  {
    if ( TheMasterCardProductCodeConverter == null )
    {
      TheMasterCardProductCodeConverter = new MasterCardProductCodeConverter();
    }
    TheMasterCardProductCodeConverter.refresh();
    return( TheMasterCardProductCodeConverter );
  }
  
  protected synchronized void load( )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:139^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pd.product_code             as mc_product_code,
//                  pd.visa_product_code        as vs_product_code
//          from    tc33_auth_mc_prod_code_desc pd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pd.product_code             as mc_product_code,\n                pd.visa_product_code        as vs_product_code\n        from    tc33_auth_mc_prod_code_desc pd";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.MasterCardProductCodeConverter",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.MasterCardProductCodeConverter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:144^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        ProductCodeMap.clear();
        do
        {
          ProductCodeMap.put( resultSet.getString("mc_product_code"),
                              resultSet.getString("vs_product_code") );  
        }
        while( resultSet.next() );
      }
      resultSet.close();
      
      LoadTimeMillis = System.currentTimeMillis();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadDataDefault()
  {
    ProductCodeMap.clear();
    
    for( int i = 0; i < ProductCodeDefaults.length; ++i )
    {
      ProductCodeMap.put( ProductCodeDefaults[i][0],
                          ProductCodeDefaults[i][1] );  
    }
  }
  
  // synchronize the refresh method so two requests (threads)
  // do not trigger a back-to-back reload of the cached data
  protected synchronized void refresh()
  {
    int time  = Integer.parseInt(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"HHmm"));
    
    // only load once at startup, but not between 10AM and 11:30AM
    if ( LoadTimeMillis == 0L && !(time >= 1000 && time <= 1130) )   
    {
      load();
    }
  }
}/*@lineinfo:generated-code*/