/*@lineinfo:filename=CurrencyCodeConverter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/tools/CurrencyCodeConverter.sqlj $

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-04-21 15:04:26 -0700 (Mon, 21 Apr 2014) $
  Version            : $Revision: 22391 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009, 2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Currency;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class CurrencyCodeConverter extends SQLJConnectionBase
{
  protected static final long       REFRESH_TIME_SECOND = 1000L;
  protected static final long       REFRESH_TIME_MINUTE = 60 * REFRESH_TIME_SECOND;
  protected static final long       REFRESH_TIME_HOUR   = 60 * REFRESH_TIME_MINUTE;
  
  protected static final long       REFRESH_TIME        = 24 * REFRESH_TIME_HOUR;
  
  protected static final String[][] CurrencyCodeDefaults = 
  {
    { "AED", "784" },
    { "AFN", "971" },
    { "ALL", "008" },
    { "AMD", "051" },
    { "ANG", "532" },
    { "AOA", "973" },
    { "ARS", "032" },
    { "AUD", "036" },
    { "AWG", "533" },
    { "AZN", "944" },
    { "BAM", "977" },
    { "BBD", "052" },
    { "BDT", "050" },
    { "BGN", "975" },
    { "BHD", "048" },
    { "BIF", "108" },
    { "BMD", "060" },
    { "BND", "096" },
    { "BOB", "068" },
    { "BOV", "984" },
    { "BRL", "986" },
    { "BSD", "044" },
    { "BTN", "064" },
    { "BWP", "072" },
    { "BYN", "933" },
    { "BYR", "974" },
    { "BZD", "084" },
    { "CAD", "124" },
    { "CDF", "976" },
    { "CHE", "947" },
    { "CHF", "756" },
    { "CHW", "948" },
    { "CLF", "990" },
    { "CLP", "152" },
    { "CNY", "156" },
    { "COP", "170" },
    { "COU", "970" },
    { "CRC", "188" },
    { "CUP", "192" },
    { "CVE", "132" },
    { "CYP", "196" },
    { "CZK", "203" },
    { "DJF", "262" },
    { "DKK", "208" },
    { "DOP", "214" },
    { "DZD", "012" },
    { "EEK", "233" },
    { "EGP", "818" },
    { "ERN", "232" },
    { "ETB", "230" },
    { "EUR", "978" },
    { "FJD", "242" },
    { "FKP", "238" },
    { "GBP", "826" },
    { "GEL", "981" },
    { "GHS", "936" },
    { "GIP", "292" },
    { "GMD", "270" },
    { "GNF", "324" },
    { "GTQ", "320" },
    { "GWP", "624" },
    { "GYD", "328" },
    { "HKD", "344" },
    { "HNL", "340" },
    { "HRK", "191" },
    { "HTG", "332" },
    { "HUF", "348" },
    { "IDR", "360" },
    { "ILS", "376" },
    { "INR", "356" },
    { "IQD", "368" },
    { "IRR", "364" },
    { "ISK", "352" },
    { "JMD", "388" },
    { "JOD", "400" },
    { "JPY", "392" },
    { "KES", "404" },
    { "KGS", "417" },
    { "KHR", "116" },
    { "KMF", "174" },
    { "KPW", "408" },
    { "KRW", "410" },
    { "KWD", "414" },
    { "KYD", "136" },
    { "KZT", "398" },
    { "LAK", "418" },
    { "LBP", "422" },
    { "LKR", "144" },
    { "LRD", "430" },
    { "LSL", "426" },
    { "LTL", "440" },
    { "LYD", "434" },
    { "MAD", "504" },
    { "MDL", "498" },
    { "MGA", "969" },
    { "MKD", "807" },
    { "MMK", "104" },
    { "MNT", "496" },
    { "MOP", "446" },
    { "MRU", "929" },
    { "MTL", "470" },
    { "MUR", "480" },
    { "MVR", "462" },
    { "MWK", "454" },
    { "MXN", "484" },
    { "MXV", "979" },
    { "MYR", "458" },
    { "MZN", "943" },
    { "NAD", "516" },
    { "NGN", "566" },
    { "NIO", "558" },
    { "NOK", "578" },
    { "NPR", "524" },
    { "NZD", "554" },
    { "OMR", "512" },
    { "PAB", "590" },
    { "PEN", "604" },
    { "PGK", "598" },
    { "PHP", "608" },
    { "PKR", "586" },
    { "PLN", "985" },
    { "PYG", "600" },
    { "QAR", "634" },
    { "RON", "946" },
    { "RSD", "941" },
    { "RUB", "643" },
    { "RWF", "646" },
    { "SAR", "682" },
    { "SBD", "090" },
    { "SCR", "690" },
    { "SDG", "938" },
    { "SEK", "752" },
    { "SGD", "702" },
    { "SHP", "654" },
    { "SKK", "703" },
    { "SLL", "694" },
    { "SOS", "706" },
    { "SRD", "968" },
    { "STN", "930" },
    { "SVC", "222" },
    { "SYP", "760" },
    { "SZL", "748" },
    { "THB", "764" },
    { "TJS", "972" },
    { "TMM", "795" },
    { "TND", "788" },
    { "TOP", "776" },
    { "TRY", "949" },
    { "TTD", "780" },
    { "TWD", "901" },
    { "TZS", "834" },
    { "UAH", "980" },
    { "UGX", "800" },
    { "USD", "840" },
    { "USN", "997" },
    { "USS", "998" },
    { "UYI", "940" },
    { "UYU", "858" },
    { "UZS", "860" },
    { "VES", "928" },
    { "VND", "704" },
    { "VUV", "548" },
    { "WST", "882" },
    { "XAF", "950" },
    { "XCD", "951" },
    { "XDR", "960" },
    { "XOF", "952" },
    { "XPF", "953" },
    { "YER", "886" },
    { "ZAR", "710" },
    { "ZMK", "894" },
    { "ZWD", "716" },
  };
  
  public static class CurrencyCode
  {
    private String      CurrencyCodeAlpha   = null;
    private String      CurrencyCodeNumeric = null;
    
    public CurrencyCode( String ccAlpha, String ccNumeric )
    {
      CurrencyCodeAlpha   = (ccAlpha == null ? "" : ccAlpha);
      CurrencyCodeNumeric = (ccNumeric == null ? "" : ccNumeric);
    }
    
    public CurrencyCode( ResultSet resultSet ) 
      throws java.sql.SQLException
    {
      CurrencyCodeAlpha   = resultSet.getString("cc_alpha");
      CurrencyCodeNumeric = resultSet.getString("cc");
      
      if ( CurrencyCodeAlpha == null )
      {
        CurrencyCodeAlpha = "";
      }
      
      if ( CurrencyCodeNumeric == null )
      {
        CurrencyCodeNumeric = "";
      }
    }
    
    public String getCurrencyCodeAlpha()
    {
      return( CurrencyCodeAlpha );
    }
    
    public String getCurrencyCodeNumeric()
    {
      return( CurrencyCodeNumeric );
    }
    
    public boolean matchesAlpha( String ccAlpha )
    {
      return( CurrencyCodeAlpha.equals(ccAlpha) );
    }
    
    public boolean matchesNumeric( String ccNumeric )
    {
      return( CurrencyCodeNumeric.equals(ccNumeric) );
    }
  }
  
  // singleton
  private static CurrencyCodeConverter   TheCurrencyCodeConverter  = null;
  
  // instance variables
  private   Vector      CurrencyCodes     = new Vector();
  private   long        LoadTimeMillis    = 0L;
  
  private CurrencyCodeConverter()
  {
    loadDataDefault();
  }
  
  public String alphaCodeToNumericCode( String alphaCode )
  {
    CurrencyCode  cc      = null;
    String        retVal  = null;
    
    try
    {
      for( int i = 0; i < CurrencyCodes.size(); ++i )
      {
        cc = (CurrencyCode)CurrencyCodes.elementAt(i);
        
        if ( cc.matchesAlpha(alphaCode) )
        {
          retVal = cc.getCurrencyCodeNumeric();
          break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry("alphaCodeToNumericCode(" + alphaCode + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  public static int getFractionDigits( String currencyCode )
  {
    String    currencyCodeAlpha   = currencyCode;
    int       decDigits           = 2;
    
    try
    {
      try
      { 
        // if the currency code is numeric, correct by converting
        // to alpha before attempting to get the default fraction digits
        int cc = Integer.parseInt(currencyCode); 
        currencyCodeAlpha = getInstance().numericCodeToAlphaCode(currencyCode);
      } 
      catch( Exception ee ) {}  // ignore, already alpha
      
      decDigits = Currency.getInstance(currencyCodeAlpha).getDefaultFractionDigits();
    }
    catch( Exception e )
    {
      // return 2-digits default
    }
    return( decDigits );
  }
  
  public static CurrencyCodeConverter getInstance()
  {
    if ( TheCurrencyCodeConverter == null )
    {
      TheCurrencyCodeConverter = new CurrencyCodeConverter();
    }
    TheCurrencyCodeConverter.refresh();
    return( TheCurrencyCodeConverter );
  }
  
  protected synchronized void load( )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:347^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct 
//                  cc.currency_code_alpha      as cc_alpha,
//                  cc.currency_code            as cc
//          from    currency_codes cc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct \n                cc.currency_code_alpha      as cc_alpha,\n                cc.currency_code            as cc\n        from    currency_codes cc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.CurrencyCodeConverter",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.CurrencyCodeConverter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        CurrencyCodes.removeAllElements();
        do
        {
          CurrencyCodes.add( new CurrencyCode(resultSet) );  
        }
        while( resultSet.next() );
      }
      resultSet.close();
      
      LoadTimeMillis = System.currentTimeMillis();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadDataDefault()
  {
    CurrencyCodes.removeAllElements();
    
    for( int i = 0; i < CurrencyCodeDefaults.length; ++i )
    {
      CurrencyCodes.add( new CurrencyCode(CurrencyCodeDefaults[i][0],CurrencyCodeDefaults[i][1]) );  
    }
  }
  
  public String numericCodeToAlphaCode( int numericCode )
  {
    StringBuffer buf = new StringBuffer(""+numericCode);
    while (buf.length() < 3)
    {
      buf.insert(0,"0");
    }
    return numericCodeToAlphaCode(buf.toString());
  }

  public String numericCodeToAlphaCode( String numericCode )
  {
    CurrencyCode  cc      = null;
    String        retVal  = null;
    
    try
    {
      for( int i = 0; i < CurrencyCodes.size(); ++i )
      {
        cc = (CurrencyCode)CurrencyCodes.elementAt(i);
        
        if ( cc.matchesNumeric(numericCode) )
        {
          retVal = cc.getCurrencyCodeAlpha();
          break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry("numericCodeToAlphaCode(" + numericCode + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  // synchronize the refresh method so two requests (threads)
  // do not trigger a back-to-back reload of the cached data
  protected synchronized void refresh()
  {
    if ( LoadTimeMillis == 0L )   // only load once at startup
    {
      load();
    }
  }
}/*@lineinfo:generated-code*/