/*@lineinfo:filename=HierarchyTree*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/HierarchyTree.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/25/02 11:51a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Iterator;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.OrgBean;
import sqlj.runtime.ResultSetIterator;

public class HierarchyTree extends SQLJConnectionBase
{
  private static OrgBean        patriarch           = null;
  private static HierarchyTree  instance            = null;
  
  public static final long  DEFAULT_HIERARCHY_NODE    = 9999999999L;
  
  private HierarchyTree()
  {
  }
  
  public static HierarchyTree getInstance()
  {
    try
    {
      if(instance == null)
      {
        instance = new HierarchyTree();
        
        instance.makeTree();
      }
      else if(instance.needRefresh())
      {
        instance.clearNodes(patriarch);
        
        instance.makeTree();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.support.HierarchyTree::getInstance()", e.toString());
    }
    
    return instance;
  }
  
  public static void destroy()
  {
    System.out.println("Destroying Hierarchy Tree");
    if(instance != null)
    {
      instance.clearNodes(patriarch);
    }
    
    instance = null;
  }
  
  public OrgBean getPatriarch()
  {
    return this.patriarch;
  }
  
  private OrgBean findNode(OrgBean parent, long hierarchyNode)
  {
    OrgBean foundNode = null;
    
    if(parent.getHierarchyNode() == hierarchyNode)
    {
      foundNode = parent;
    }
    else
    {
      Iterator it = parent.getChildren().iterator();
      
      while(it.hasNext())
      {
        OrgBean child = (OrgBean)(it.next());
        
        foundNode = findNode(child, hierarchyNode);
        
        if(foundNode != null)
        {
          break;
        }
      }
    }
    
    return foundNode;
  }
  
  public OrgBean findNode(long hierarchyNode)
  {
    OrgBean node = null;
    
    try
    {
      //@ System.out.println("\n------ FINDING NODE --------");
      if(patriarch.getHierarchyNode() == hierarchyNode)
      {
        //@ System.out.println("  User node is patriarch");
        node = patriarch;
      }
      else
      {
        // get to the bank level node for this user before going forward
        OrgBean bankNode = null;
      
        long bankNumber = hierarchyNode / 1000000L;
      
        if(bankNumber < 1000)
        {
          // user is already at a bank node
          bankNumber = hierarchyNode;
        }
        else
        {
          // add 5 zeroes to create a bank-level node
          bankNumber *= 100000L;
        }
       
        //@ System.out.println("  User bank node is: " + bankNumber);
      
        Iterator it = patriarch.getChildren().iterator();
        //@ System.out.println("  got iterator");
        while(it.hasNext())
        {
          //@ System.out.println("  iterating");
          bankNode = (OrgBean)(it.next());
          if(bankNode.getHierarchyNode() == bankNumber)
          {
            //@ System.out.println("  Found bank-level node: " + bankNode.getHierarchyNode());
            break;
          }
        }
      
        if(bankNode == null)
        {
          //@ System.out.println("  Assigning patriarch because bankNode is null");
          bankNode = patriarch;
        }
      
        node = findNode(bankNode, hierarchyNode);
      }
    }
    catch(Exception e)
    {
      logEntry("findNode(" + hierarchyNode + ")", e.toString());
      //@ System.out.println("findNode: " + e.toString());
    }
    
    //@ System.out.println("------ FOUND NODE (" + node.getHierarchyNode() + ") --------\n");
    return node;
  }
  
  private synchronized boolean needRefresh()
  {
    boolean   result    = false;
    String    refresh   = "";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:190^7*/

//  ************************************************************
//  #sql [Ctx] { select  need_refresh 
//          
//          from    view_hierarchy_refresh
//          where   id = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  need_refresh \n         \n        from    view_hierarchy_refresh\n        where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.HierarchyTree",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   refresh = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^7*/
      
      if(refresh.equals("Y"))
      {
        result = true;
        
        /*@lineinfo:generated-code*//*@lineinfo:202^9*/

//  ************************************************************
//  #sql [Ctx] { update  view_hierarchy_refresh
//            set     need_refresh = 'N'
//            where   id = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  view_hierarchy_refresh\n          set     need_refresh = 'N'\n          where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.HierarchyTree",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("needRefresh()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  private synchronized void makeTree()
  {
    int       orgId         = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:230^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num 
//          from    organization
//          where   org_group = :DEFAULT_HIERARCHY_NODE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num  \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.HierarchyTree",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,DEFAULT_HIERARCHY_NODE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:235^7*/
      
      makeTree(orgId);
      
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] { update  view_hierarchy_refresh
//          set     need_refresh = 'N'
//          where   id = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  view_hierarchy_refresh\n        set     need_refresh = 'N'\n        where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.HierarchyTree",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
      
    }
    catch(Exception e)
    {
      logEntry("makeTree()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private synchronized void clearNodes(OrgBean node)
  {
    try
    {
      node.getChildren().clear();
    }
    catch(Exception e)
    {
      logEntry("clearNodes()", e.toString());
    }
  }
  
  private synchronized void makeTree(int orgId)
  {
    StringBuffer      orgName = new StringBuffer("");
    ResultSetIterator it      = null;
    
    try
    {
      // get the org name and hierarchy node
      /*@lineinfo:generated-code*//*@lineinfo:277^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  org_name, 
//                  org_group
//          from    organization
//          where   org_num = :orgId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  org_name, \n                org_group\n        from    organization\n        where   org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.HierarchyTree",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.tools.HierarchyTree",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^7*/
      
      ResultSet rs = it.getResultSet();
      
      long hierarchyNode = 0L;
      if(rs.next())
      {
        orgName.append(rs.getLong("org_group"));
        orgName.append(" - ");
        orgName.append(rs.getString("org_name"));
        hierarchyNode = rs.getLong("org_group");
      }
      else
      {
        orgName.append("org_num ");
        orgName.append(orgId);
        orgName.append(" - UNKNOWN");
      }
      
      it.close();
      
      patriarch = new OrgBean(orgId, orgName.toString(), 0L, hierarchyNode);
      
      // recursively fill the tree
      procreate(patriarch);
    }
    catch(Exception e)
    {
      logEntry("makeTree(" + orgId + ")", e.toString());
    }
    finally
    {
      //cleanUp();
    }
  }
  
  private void procreate(OrgBean ob)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:325^7*/

//  ************************************************************
//  #sql [Ctx] it = { select o.org_num,
//                 o.org_name,
//                 o.org_type_code,
//                 o.org_group
//          from   organization o,
//                 t_hierarchy th
//          where  o.org_group = th.descendent and
//                 th.relation = 1 and
//                 th.hier_type = 1 and
//                 th.ancestor = :ob.getHierarchyNode()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_69 = ob.getHierarchyNode();
  try {
   String theSqlTS = "select o.org_num,\n               o.org_name,\n               o.org_type_code,\n               o.org_group\n        from   organization o,\n               t_hierarchy th\n        where  o.org_group = th.descendent and\n               th.relation = 1 and\n               th.hier_type = 1 and\n               th.ancestor =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.HierarchyTree",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_69);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.tools.HierarchyTree",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:337^7*/
      
      rs = it.getResultSet();
      
      StringBuffer orgName = new StringBuffer("");
      while(rs.next())
      {
        orgName.setLength(0);
        orgName.append(rs.getLong("org_group"));
        orgName.append(" - ");
        orgName.append(rs.getString("org_name"));
        
        ob.addChild(rs.getInt("org_num"), orgName.toString(), 0L, rs.getLong("org_group"));
      }
      
      it.close();
      rs = null;
      
      // procreate each child
      for(Iterator i = ob.getChildren().iterator(); i.hasNext();)
      {
        procreate((OrgBean)(i.next()));
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("procreate(" + ob.getOrgName() + "):" + e.toString());
      logEntry("procreate(" + ob.getOrgName() + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/