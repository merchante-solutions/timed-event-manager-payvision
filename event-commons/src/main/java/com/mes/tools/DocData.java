/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DocData.java $

  Description:  DocData.java

  Class that defines document data, as broken down to our specifications;
  child classes override the getBody() depending on technique

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 7/26/04 11:20a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

public abstract class DocData
{

  protected long id;
  protected String ext;
  protected String fileName;
  protected String mimeType;

  protected String DEFAULT_MIME = "text/plain";

  public DocData (long id, String ext, String fileName, String mimeType)
  {
    this.id = id;
    this.ext = ext;
    this.fileName = fileName;
    this.mimeType = mimeType;
  }

  /**
   * getBody()
   * abstract - force extending classes to implement
   */
  public abstract byte[] getBody();

  public long getId()
  {
    return id;
  }

  public String getMimeType()
  {
    if(null != mimeType)
    {
      return mimeType;
    }
    else
    {
      return DEFAULT_MIME;
    }
  }

  public String getExtension()
  {
    if(null != ext)
    {
      return ext;
    }
    else
    {
      return "";
    }
  }

  public String getFileName()
  {
    if(null != fileName)
    {
      if(fileName.indexOf(".")==-1){
        fileName = fileName+"."+getExtension();
      }
      return fileName;
    }
    else
    {
      return "";
    }
  }

}