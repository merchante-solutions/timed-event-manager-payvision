/* @lineinfo:filename=DccRateUtil *//* @lineinfo:user-code *//* @lineinfo:1^1 */
/*************************************************************************
 * 
 * FILE: $URL:
 * http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/tools/DccRateUtil.sqlj
 * $
 * 
 * Last Modified By : $Author: hmeng $
 * Last Modified Date : $Date: 2015-06-12 21:33:02 -0700 (Fri, 12 Jun 2015) $
 * Version : $Revision: 23686 $
 * 
 * Change History:
 * See SVN database
 * 
 * Copyright (C) 2000-2012, 2013 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * 
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc. This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 * 
 **************************************************************************/
package com.mes.tools;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.MesQueryHandlerList;
import com.mes.support.StringUtilities;

public class DccRateUtil  {

	private static Logger log = Logger.getLogger(DccRateUtil.class);
	private Integer NUMBER_DAYS = 30;
	private Map<String, List<DccRate>> dccRateMap = new HashMap<>();
	private Date batchDate = null;
	private Date[] cardBrandEffDates = null;

	public enum RateType {
		BUY_RATE(0), SELL_RATE(1);
		private Integer rateType;

		private RateType(Integer type) {
			this.rateType = type;
		}

		public Integer getType() {
			return this.rateType;
		}
	}

	public enum CardBrandType {
		MASTER_CARD("MC", "MC"), VISA("VS", "VS"), DISCOVER("DS", "VS"), AMEX("AM", "MC");
		private String cardBrand;
		private String useCardBrand;

		private CardBrandType(String cardBrand, String useCardBrand) {
			this.cardBrand = cardBrand;
		}

		public String getCardBrand() {
			return this.cardBrand;
		}

		public String getUseCardBrand() {
			return this.useCardBrand;
		}
	}
	
	public enum DccRateColumn{
		ITEM_KEY("item_key"),
		EFFECTIVE_DATE("effective_date"),
		EFF_START_DATE("effective_start_date"),
		EFF_END_DATE("effective_end_date"),
		CARD_TYPE("card_Type"),
		BASE_CURRENCY_CODE("base_currency_code"),
		COUNTER_CURRENCY_CODE("counter_currency_code"),
		BUY_RATE("buy_rate"),
		SELL_RATE("sell_rate");
		String columnName;
		DccRateColumn(String name){
			this.columnName = name;
		}
	}
	
	public static DccRate getDefaultRate(String cardType,String currencyCode) {
		DccRate defaultRate = new DccRate();
		defaultRate.baseCurrency = currencyCode;
		defaultRate.counterCurrency = currencyCode;
		defaultRate.buyRate = 1.0d;
		defaultRate.sellRate = 1.0d;
		defaultRate.cardType = cardType;
		defaultRate.effectiveDate = Calendar.getInstance().getTime();
		defaultRate.effectiveStartDate = Calendar.getInstance().getTime();
		defaultRate.effectiveEndDate = Calendar.getInstance().getTime();
		return defaultRate;
	}

	public static class DccRate {
		public String baseCurrency = null;
		public double buyRate = 0.0;
		public String cardType = null;
		public String counterCurrency = null;
		public Date effectiveDate = null;
		public Date effectiveStartDate = null;
		public Date effectiveEndDate = null;
		public double sellRate = 0.0;

		public DccRate() {
			//noop
		}
		public DccRate(Map<String, Object> dccRow) {
			effectiveDate = (Date) dccRow.get(DccRateColumn.EFFECTIVE_DATE.columnName);
			effectiveStartDate = (Date) dccRow.get(DccRateColumn.EFF_START_DATE.columnName);
			effectiveEndDate = (Date) dccRow.get(DccRateColumn.EFF_END_DATE.columnName);
			baseCurrency = (String) dccRow.get(DccRateColumn.BASE_CURRENCY_CODE.columnName);
			cardType = (String) dccRow.get(DccRateColumn.CARD_TYPE.columnName);
			counterCurrency = (String) dccRow.get(DccRateColumn.COUNTER_CURRENCY_CODE.columnName);
			buyRate = ((BigDecimal) dccRow.get(DccRateColumn.BUY_RATE.columnName)).doubleValue();
			sellRate = ((BigDecimal) dccRow.get(DccRateColumn.SELL_RATE.columnName)).doubleValue();
		}

		public String getBaseCurrency() {
			return (baseCurrency);
		}

		public double getBuyRate() {
			return (buyRate);
		}

		public String getCardType() {
			return (cardType);
		}

		public String getCounterCurrency() {
			return (counterCurrency);
		}

		public Date getEffectiveDate() {
			return (effectiveDate);
		}

		public Date getEffectiveEndDate() {
			return this.effectiveEndDate;
		}

		public Date getEffectiveStartDate() {
			return this.effectiveStartDate;
		}

		public double getSellRate() {
			return (sellRate);
		}
		
		public boolean isRateEffective(Date effectiveDate) {
			return effectiveDate.getTime() >= getEffectiveStartDate().getTime() && effectiveDate.getTime() <= getEffectiveEndDate().getTime();
		}

		public void showData() {
			StringBuilder builder = new StringBuilder();
			builder.append("CardType        : " + getCardType()).append("\r\n");
			builder.append("BaseCurrency    : " + getBaseCurrency()).append("\r\n");
			builder.append("CounterCurrency : " + getCounterCurrency()).append("\r\n");
			builder.append("EffectiveDate   : " + getEffectiveDate()).append("\r\n");
			builder.append("BuyRate         : " + getBuyRate()).append("\r\n");
			builder.append("SellRate        : " + getSellRate()).append("\r\n");
			log.info("[DccRate.showData()] - DccRate=" + builder.toString());
		}
	}

	private DccRateUtil() {
	}

	public static DccRateUtil getInstance() {
		return (getInstance(null));
	}
	
	public static DccRateUtil getInstance(java.util.Date effectiveDate) {
		return getInstance(effectiveDate == null ? null : new java.sql.Date(effectiveDate.getTime()));
	}
	
	public static DccRateUtil getInstance(java.sql.Date effectiveDate) {
		DccRateUtil dccRateUtil = new DccRateUtil();
		dccRateUtil.loadRates(effectiveDate);
		dccRateUtil.batchDate = effectiveDate;
		return (dccRateUtil);
	}
	
	public Date getBatchDate() {
		return this.batchDate;
	}

	public double getBuyRate(String cardType, String baseCurrency, String counterCurrency) {
		return getRate(cardType, baseCurrency, counterCurrency, 0);
	}

	public DccRate getDccRate(String cardType, String baseCurrency, String counterCurrency) {
		String key = cardType + StringUtilities.rightJustify(baseCurrency, 3, '0') + StringUtilities.rightJustify(counterCurrency, 3, '0');
		int dateIndex = cardType.equals(CardBrandType.MASTER_CARD.cardBrand) ? 1 : 0;
		if ( CollectionUtils.isNotEmpty(dccRateMap.get(key))) {
			List<DccRate> rateList = dccRateMap.get(key);
			if ( rateList.size() == 1) {
				return rateList.get(rateList.size() - 1);
			}
			DccRate[] rates = rateList.stream().filter(dcc -> dcc.isRateEffective(cardBrandEffDates[dateIndex])).toArray(DccRate[]::new);
			if ( rates !=null && rates.length > 0 ) {
				return rates[0];
			}
		}
		return null;
	}
	
	public DccRate getDccRate(String cardType, String baseCurrency, String counterCurrency, Date fxDate) {
		String key = cardType + StringUtilities.rightJustify(baseCurrency, 3, '0') + StringUtilities.rightJustify(counterCurrency, 3, '0');
		if ( CollectionUtils.isNotEmpty(dccRateMap.get(key))) {
			List<DccRate> rateList = dccRateMap.get(key);
			DccRate[] rates = rateList.stream().filter(dcc -> dcc.isRateEffective(fxDate)).toArray(DccRate[]::new);
			if ( rates !=null && rates.length > 0 ) {
				return rates[0];
			}
		}
		return null;
	}

	public double getRate(String cardType, String baseCurrency, String counterCurrency, int rateType) {
		DccRate rate = getDccRate(cardType,baseCurrency,counterCurrency);
		double retVal = 0.0;
		if (rate != null) {
			retVal = (rateType == 0) ? rate.getBuyRate() : rate.getSellRate();
		}
		return (retVal);
	}

	public double getSellRate(String cardType, String baseCurrency, String counterCurrency) {
		return getRate(cardType, baseCurrency, counterCurrency, 1);
	}

	public MesQueryHandlerList getQueryHandler() {
		return new MesQueryHandlerList();
	}

	public Date getMaxEffectiveDate(CardBrandType brand) throws SQLException {
		if (log.isDebugEnabled()) {
			log.debug("[getMaxEffectiveDate()] - get max effective date for cardBrand=" + brand);
		}
		String rateTable = (brand == CardBrandType.MASTER_CARD ? "mc_settlement_dcc" : "visa_settlement_dcc");
		String preparedSql = String.format("select max(dcc.effective_date) as effective_date from %s dcc where dcc.effective_date >= sysdate-?", rateTable);

		List<Map<String, Object>> results = getQueryHandler().executePreparedStatement("0com.mes.tools.DccRateUtil", preparedSql, NUMBER_DAYS);
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getMaxEffectiveDate()] - maxEffectiveDate=%s for %s", results, NUMBER_DAYS));
		}
		if (CollectionUtils.isEmpty(results)) {
			throw new SQLException("Max Effective Date not found for " + brand.getCardBrand());
		}

		if (results.get(0).containsKey(DccRateColumn.EFFECTIVE_DATE.columnName)) {
			return (Date) results.get(0).get("effective_date");
		}
		throw new SQLException("effective_date column missing for " + brand.getCardBrand());
	}

	private Date[] getEffectiveDates(Date processingDate) throws SQLException {
		Date[] cpds = new Date[2];
		if (processingDate == null) {
			// if the request cpd is null, return the most
			// recent rates for each card type
			cpds[0] = getMaxEffectiveDate(CardBrandType.VISA);
			cpds[1] = getMaxEffectiveDate(CardBrandType.MASTER_CARD);
		}
		else // use the rates on the specified cpd
		{
			for (int i = 0; i < cpds.length; ++i) {
				cpds[i] = processingDate;
			}
		}
		return cpds;
	}

	public Integer getEffectiveDateCount() throws Exception {
		
		String effectiveDate = MesDefaults.getString(MesDefaults.MC_ENHANCED_FX_ACTIVE_DATE);
		Date effDate = new SimpleDateFormat("yyyy-MM-dd").parse(effectiveDate);
		Date currDate = new Date();
		String dateCountStr = MesDefaults.getString(MesDefaults.MC_FX_AUTH_NUMBER_DAYS);
		
		return StringUtils.isBlank(effectiveDate) || currDate.before(effDate) || StringUtils.isBlank(dateCountStr) ? 0 : Integer.valueOf(dateCountStr);
	}

	public void loadRates(Date processingDate) {
		if (log.isDebugEnabled()) {
			log.debug("[loadRates()] - loading FX conversion rates for " + processingDate);
		}
		long startTime = System.currentTimeMillis();
		try {

			cardBrandEffDates = getEffectiveDates(processingDate);

			//@formatter:off
			String preparedSql = "SELECT "
							+ "'VS' AS card_type, "
							+ "  ('VS' || dcc.base_currency_code || dcc.counter_currency_code) AS item_key, "
			                + "  dcc.effective_date, "
			                + "  nvl(dcc.effective_start_date, trunc(effective_date)) effective_start_date, "
			                + "  nvl(dcc.effective_end_date, ((trunc(effective_date) + 1) - (1/24/60/60))) effective_end_date, "
			                + "  dcc.base_currency_code, "
			                + "  dcc.counter_currency_code, "
			                + "  dcc.buy_rate, "
			                + "  dcc.sell_rate "
			                + "FROM visa_settlement_dcc dcc "
			                + "WHERE dcc.effective_date=? "
			                + "UNION "
			                + "SELECT "
			                + "  'DS' AS card_type, "
			                + "  ('DS' || dcc.base_currency_code || dcc.counter_currency_code) AS item_key, "
			                + "  dcc.effective_date, "
			                + "  nvl(dcc.effective_start_date, trunc(effective_date)) effective_start_date, "
			                + "  nvl(dcc.effective_end_date, ((trunc(effective_date) + 1) - (1/24/60/60))) effective_end_date, "
			                + "  dcc.base_currency_code, "
			                + "  dcc.counter_currency_code, "
			                + "  dcc.sell_rate    AS buy_rate, "
			                + "  dcc.sell_rate    AS sell_rate "
			                + "FROM visa_settlement_dcc dcc "
			                + "WHERE dcc.effective_date=? "
			                + "UNION "
			                + "SELECT "
			                + "  'MC' AS card_type, "
			                + "  ('MC' || dcc.base_currency_code || dcc.counter_currency_code) AS item_key, "
			                + "  dcc.effective_date            AS effective_date, "
			                + "  nvl(dcc.effective_start_date, trunc(effective_date)) effective_start_date, "
			                + "  nvl(dcc.effective_end_date, ((trunc(effective_date) + 1) - (1/24/60/60))) effective_end_date, "
			                + "  dcc.base_currency_code        AS base_currency_code, "
			                + "  dcc.counter_currency_code     AS counter_currency_code, "
			                + "  dcc.sell_rate                 AS buy_rate, "
			                + "  dcc.sell_rate                 AS sell_rate "
			                + "FROM mc_settlement_dcc dcc "
			                + "WHERE dcc.effective_date between (?-?) and ? "
			                + "ORDER BY 1,4";
			//@formatter:on
			String sqlName = "2com.mes.tools.DccRateUtil";
			List<Map<String, Object>> resultList = getQueryHandler().executePreparedStatement(sqlName, preparedSql, cardBrandEffDates[0], cardBrandEffDates[0],
					cardBrandEffDates[1], getEffectiveDateCount(), cardBrandEffDates[1]);

			if (CollectionUtils.isNotEmpty(resultList)) {
				if (dccRateMap == null)
					dccRateMap = new HashMap<>();
				else
					dccRateMap.clear();
				for (Map<String, Object> dccRow : resultList) {
					if (dccRateMap.containsKey(dccRow.get(DccRateColumn.ITEM_KEY.columnName))) {
						dccRateMap.get(dccRow.get(DccRateColumn.ITEM_KEY.columnName)).add(new DccRate(dccRow));
					}
					else {
						List<DccRate> rates = new ArrayList<>();
						rates.add(new DccRate(dccRow));
						dccRateMap.put((String) dccRow.get(DccRateColumn.ITEM_KEY.columnName), rates);
					}
				}
			}
		}
		catch (Exception e) {
			log.error("[loadRates()] Unexpected error loading fx rates", e);
		}
		finally {
			long stopTime = System.currentTimeMillis();
			log.debug("[loadRates()] - loaded FX conversion rates, elapsed time = " + (stopTime - startTime) + " milliseconds");
		}
	}

	public static void main(String[] args) {
		try {
			DccRateUtil instance = null;
			if (args.length > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				instance = DccRateUtil.getInstance(sdf.parse(args[0]));
			}
			else {
				instance = DccRateUtil.getInstance();
			}
			instance.showData();
			System.exit(0);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void showData() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		for (Iterator it = dccRateMap.keySet().iterator(); it.hasNext();) {
			String name = (String) it.next();
			List<DccRate> rates = dccRateMap.get(name);
			for (DccRate rate : rates) {
				log.info(name + " " + sdf.format(rate.getEffectiveDate()) + " " + rate.getBuyRate() + " " + rate.getSellRate());
			}
		}
	}
}
