/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/AsciiToEbcdicConverter.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-06-27 11:02:38 -0700 (Thu, 27 Jun 2013) $
  Version            : $Revision: 21249 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.util.HashMap;

public class AsciiToEbcdicConverter
{
  private static final HashMap AsciiToEbcdicMap = 
    new HashMap()
    {
      {
        //  ascii   ebcdic        char    decimal
        put("00", "00");   // NULL      0     
        put("01", "01");   // SOH       1     
        put("02", "02");   // STX       2     
        put("03", "03");   // ETX       3     
        put("04", "37");   // EOT       4     
        put("05", "2D");   // ENQ       5     
        put("06", "2E");   // ACK       6     
        put("07", "2F");   // BEL       7     
        put("08", "16");   // BS        8     
        put("09", "05");   // HT        9     
        put("0A", "25");   // LF       10     
        put("0B", "0B");   // VT       11     
        put("0C", "0C");   // FF       12     
        put("0D", "0D");   // CR       13     
        put("0E", "0E");   // SO       14     
        put("0F", "0F");   // SI       15     
        put("10", "10");   // DLE      16     
        put("11", "11");   // DC1      17     
        put("12", "12");   // DC2      18     
        put("13", "13");   // DC3      19     
        put("14", "3C");   // DC4      20     
        put("15", "3D");   // NAK      21     
        put("16", "32");   // SYN      22     
        put("17", "26");   // ETB      23     
        put("18", "18");   // CAN      24     
        put("19", "19");   // EM       25     
        put("1A", "3F");   // SUB      26     
        put("1B", "27");   // ESC      27     
        put("1C", "1C");   // FS       28     
        put("1D", "1D");   // GS       29     
        put("1E", "1E");   // RS       30     
        put("1F", "1F");   // US       31     
        put("20", "40");   // SPACE    32     
        put("21", "4F");   // !        33     
        put("22", "7F");   // "        34     
        put("23", "7B");   // #        35     
        put("24", "5B");   // $        36     
        put("25", "6C");   // %        37     
        put("26", "50");   // &        38     
        put("27", "7D");   // '        39     
        put("28", "4D");   // (        40     
        put("29", "5D");   // )        41     
        put("2A", "5C");   // *        42     
        put("2B", "4E");   // +        43     
        put("2C", "6B");   // ,        44     
        put("2D", "60");   // -        45     
        put("2E", "4B");   // .        46     
        put("2F", "61");   // /        47     
        put("30", "F0");   // 0        48     
        put("31", "F1");   // 1        49     
        put("32", "F2");   // 2        50     
        put("33", "F3");   // 3        51     
        put("34", "F4");   // 4        52     
        put("35", "F5");   // 5        53     
        put("36", "F6");   // 6        54     
        put("37", "F7");   // 7        55     
        put("38", "F8");   // 8        56     
        put("39", "F9");   // 9        57     
        put("3A", "7A");   // :        58     
        put("3B", "5E");   // ;        59     
        put("3C", "4C");   // <        60     
        put("3D", "7E");   // =        61     
        put("3E", "6E");   // >        62     
        put("3F", "6F");   // ?        63     
        put("40", "7C");   // @        64     
        put("41", "C1");   // A        65     
        put("42", "C2");   // B        66     
        put("43", "C3");   // C        67     
        put("44", "C4");   // D        68     
        put("45", "C5");   // E        69     
        put("46", "C6");   // F        70     
        put("47", "C7");   // G        71     
        put("48", "C8");   // H        72     
        put("49", "C9");   // I        73     
        put("4A", "D1");   // J        74     
        put("4B", "D2");   // K        75     
        put("4C", "D3");   // L        76     
        put("4D", "D4");   // M        77     
        put("4E", "D5");   // N        78     
        put("4F", "D6");   // O        79     
        put("50", "D7");   // P        80     
        put("51", "D8");   // Q        81     
        put("52", "D9");   // R        82     
        put("53", "E2");   // S        83     
        put("54", "E3");   // T        84     
        put("55", "E4");   // U        85     
        put("56", "E5");   // V        86     
        put("57", "E6");   // W        87     
        put("58", "E7");   // X        88     
        put("59", "E8");   // Y        89     
        put("5A", "E9");   // Z        90     
        put("5B", "4A");   // [        91     
        put("5C", "E0");   // \        92     
        put("5D", "5A");   // ]        93     
        put("5E", "5F");   // ^        94     
        put("5F", "6D");   // _        95     
        put("60", "79");   // `        96     
        put("61", "81");   // a        97     
        put("62", "82");   // b        98     
        put("63", "83");   // c        99     
        put("64", "84");   // d        100    
        put("65", "85");   // e        101    
        put("66", "86");   // f        102    
        put("67", "87");   // g        103    
        put("68", "88");   // h        104    
        put("69", "89");   // i        105    
        put("6A", "91");   // j        106    
        put("6B", "92");   // k        107    
        put("6C", "93");   // l        108    
        put("6D", "94");   // m        109    
        put("6E", "95");   // n        110    
        put("6F", "96");   // o        111    
        put("70", "97");   // p        112    
        put("71", "98");   // q        113    
        put("72", "99");   // r        114    
        put("73", "A2");   // s        115    
        put("74", "A3");   // t        116    
        put("75", "A4");   // u        117    
        put("76", "A5");   // v        118    
        put("77", "A6");   // w        119    
        put("78", "A7");   // x        120    
        put("79", "A8");   // y        121    
        put("7A", "A9");   // z        122    
        put("7B", "C0");   // {        123    
        put("7C", "6A");   // |        124    
        put("7D", "D0");   // }        125    
        put("7E", "A1");   // ~        126    
        put("7F", "07");   //         127    
        put("80", "20");   // â‚¬        128    
        put("81", "21");   //          129    
        put("82", "22");   // â€š        130    
        put("83", "23");   // Æ’        131    
        put("84", "24");   // â€ž        132    
        put("85", "15");   // â€¦        133    
        put("86", "06");   // â€         134    
        put("87", "17");   // â€¡        135    
        put("88", "28");   // Ë†        136    
        put("89", "29");   // â€°        137    
        put("8A", "2A");   // Å         138    
        put("8B", "2B");   // â€¹        139    
        put("8C", "2C");   // Å’        140    
        put("8D", "09");   //          141    
        put("8E", "0A");   // Å½        142    
        put("8F", "1B");   //          143    
        put("90", "30");   //          144    
        put("91", "31");   // â€˜        145    
        put("92", "1A");   // â€™        146    
        put("93", "33");   // â€œ        147    
        put("94", "34");   // â€�        148    
        put("95", "35");   // â€¢        149    
        put("96", "36");   // â€“        150    
        put("97", "08");   // â€”        151    
        put("98", "38");   // Ëœ        152    
        put("99", "39");   // â„¢        153    
        put("9A", "3A");   // Å¡        154    
        put("9B", "3B");   // â€º        155    
        put("9C", "04");   // Å“        156    
        put("9D", "14");   //          157    
        put("9E", "3E");   // Å¾        158    
        put("9F", "E1");   // Å¸        159    
        put("A0", "41");   //          160    
        put("A1", "42");   // Â¡        161    
        put("A2", "43");   // Â¢        162    
        put("A3", "44");   // Â£        163    
        put("A4", "45");   // Â¤        164    
        put("A5", "46");   // Â¥        165    
        put("A6", "47");   // Â¦        166    
        put("A7", "48");   // Â§        167    
        put("A8", "49");   // Â¨        168    
        put("A9", "51");   // Â©        169    
        put("AA", "52");   // Âª        170    
        put("AB", "53");   // Â«        171    
        put("AC", "54");   // Â¬        172    
        put("AD", "55");   // Â­        173    
        put("AE", "56");   // Â®        174    
        put("AF", "57");   // Â¯        175    
        put("B0", "58");   // Â°        176    
        put("B1", "59");   // Â±        177    
        put("B2", "62");   // Â²        178    
        put("B3", "63");   // Â³        179    
        put("B4", "64");   // Â´        180    
        put("B5", "65");   // Âµ        181    
        put("B6", "66");   // Â¶        182    
        put("B7", "67");   // Â·        183    
        put("B8", "68");   // Â¸        184    
        put("B9", "69");   // Â¹        185    
        put("BA", "70");   // Âº        186    
        put("BB", "71");   // Â»        187    
        put("BC", "72");   // Â¼        188    
        put("BD", "73");   // Â½        189    
        put("BE", "74");   // Â¾        190    
        put("BF", "75");   // Â¿        191    
        put("C0", "76");   // Ã€        192    
        put("C1", "77");   // Ã�        193    
        put("C2", "78");   // Ã‚        194    
        put("C3", "80");   // Ãƒ        195    
        put("C4", "8A");   // Ã„        196    
        put("C5", "8B");   // Ã…        197    
        put("C6", "8C");   // Ã†        198    
        put("C7", "8D");   // Ã‡        199    
        put("C8", "8E");   // Ãˆ        200    
        put("C9", "8F");   // Ã‰        201    
        put("CA", "90");   // ÃŠ        202    
        put("CB", "9A");   // Ã‹        203    
        put("CC", "9B");   // ÃŒ        204    
        put("CD", "9C");   // Ã�        205    
        put("CE", "9D");   // ÃŽ        206    
        put("CF", "9E");   // Ã�        207    
        put("D0", "9F");   // Ã�        208    
        put("D1", "A0");   // Ã‘        209    
        put("D2", "AA");   // Ã’        210    
        put("D3", "AB");   // Ã“        211    
        put("D4", "AC");   // Ã”        212    
        put("D5", "AD");   // Ã•        213    
        put("D6", "AE");   // Ã–        214    
        put("D7", "AF");   // Ã—        215    
        put("D8", "B0");   // Ã˜        216    
        put("D9", "B1");   // Ã™        217    
        put("DA", "B2");   // Ãš        218    
        put("DB", "B3");   // Ã›        219    
        put("DC", "B4");   // Ãœ        220    
        put("DD", "B5");   // Ã�        221    
        put("DE", "B6");   // Ãž        222    
        put("DF", "B7");   // ÃŸ        223    
        put("E0", "B8");   // Ã         224    
        put("E1", "B9");   // Ã¡        225    
        put("E2", "BA");   // Ã¢        226    
        put("E3", "BB");   // Ã£        227    
        put("E4", "BC");   // Ã¤        228    
        put("E5", "BD");   // Ã¥        229    
        put("E6", "BE");   // Ã¦        230    
        put("E7", "BF");   // Ã§        231    
        put("E8", "CA");   // Ã¨        232    
        put("E9", "CB");   // Ã©        233    
        put("EA", "CC");   // Ãª        234    
        put("EB", "CD");   // Ã«        235    
        put("EC", "CE");   // Ã¬        236    
        put("ED", "CF");   // Ã­        237    
        put("EE", "DA");   // Ã®        238    
        put("EF", "DB");   // Ã¯        239    
        put("F0", "DC");   // Ã°        240    
        put("F1", "DD");   // Ã±        241    
        put("F2", "DE");   // Ã²        242    
        put("F3", "DF");   // Ã³        243    
        put("F4", "EA");   // Ã´        244    
        put("F5", "EB");   // Ãµ        245    
        put("F6", "EC");   // Ã¶        246    
        put("F7", "ED");   // Ã·        247    
        put("F8", "EE");   // Ã¸        248    
        put("F9", "EF");   // Ã¹        249    
        put("FA", "FA");   // Ãº        250    
        put("FB", "FB");   // Ã»        251    
        put("FC", "FC");   // Ã¼        252    
        put("FD", "FD");   // Ã½        253    
        put("FE", "FE");   // Ã¾        254    
        put("FF", "FF");   // Ã¿        255    
      }
    };
    
  private static final HashMap EbcdicToAsciiMap = 
    new HashMap()
    {
      {
        //  ebcdic ascii          char    decimal
        put("00", "00");   // NULL      0     
        put("01", "01");   // SOH       1     
        put("02", "02");   // STX       2     
        put("03", "03");   // ETX       3     
        put("37", "04");   // EOT       4     
        put("2D", "05");   // ENQ       5     
        put("2E", "06");   // ACK       6     
        put("2F", "07");   // BEL       7     
        put("16", "08");   // BS        8     
        put("05", "09");   // HT        9     
        put("25", "0A");   // LF       10     
        put("0B", "0B");   // VT       11     
        put("0C", "0C");   // FF       12     
        put("0D", "0D");   // CR       13     
        put("0E", "0E");   // SO       14     
        put("0F", "0F");   // SI       15     
        put("10", "10");   // DLE      16     
        put("11", "11");   // DC1      17     
        put("12", "12");   // DC2      18     
        put("13", "13");   // DC3      19     
        put("3C", "14");   // DC4      20     
        put("3D", "15");   // NAK      21     
        put("32", "16");   // SYN      22     
        put("26", "17");   // ETB      23     
        put("18", "18");   // CAN      24     
        put("19", "19");   // EM       25     
        put("3F", "1A");   // SUB      26     
        put("27", "1B");   // ESC      27     
        put("1C", "1C");   // FS       28     
        put("1D", "1D");   // GS       29     
        put("1E", "1E");   // RS       30     
        put("1F", "1F");   // US       31     
        put("40", "20");   // SPACE    32     
        put("4F", "21");   // !        33     
        put("7F", "22");   // "        34     
        put("7B", "23");   // #        35     
        put("5B", "24");   // $        36     
        put("6C", "25");   // %        37     
        put("50", "26");   // &        38     
        put("7D", "27");   // '        39     
        put("4D", "28");   // (        40     
        put("5D", "29");   // )        41     
        put("5C", "2A");   // *        42     
        put("4E", "2B");   // +        43     
        put("6B", "2C");   // ,        44     
        put("60", "2D");   // -        45     
        put("4B", "2E");   // .        46     
        put("61", "2F");   // /        47     
        put("F0", "30");   // 0        48     
        put("F1", "31");   // 1        49     
        put("F2", "32");   // 2        50     
        put("F3", "33");   // 3        51     
        put("F4", "34");   // 4        52     
        put("F5", "35");   // 5        53     
        put("F6", "36");   // 6        54     
        put("F7", "37");   // 7        55     
        put("F8", "38");   // 8        56     
        put("F9", "39");   // 9        57     
        put("7A", "3A");   // :        58     
        put("5E", "3B");   // ;        59     
        put("4C", "3C");   // <        60     
        put("7E", "3D");   // =        61     
        put("6E", "3E");   // >        62     
        put("6F", "3F");   // ?        63     
        put("7C", "40");   // @        64     
        put("C1", "41");   // A        65     
        put("C2", "42");   // B        66     
        put("C3", "43");   // C        67     
        put("C4", "44");   // D        68     
        put("C5", "45");   // E        69     
        put("C6", "46");   // F        70     
        put("C7", "47");   // G        71     
        put("C8", "48");   // H        72     
        put("C9", "49");   // I        73     
        put("D1", "4A");   // J        74     
        put("D2", "4B");   // K        75     
        put("D3", "4C");   // L        76     
        put("D4", "4D");   // M        77     
        put("D5", "4E");   // N        78     
        put("D6", "4F");   // O        79     
        put("D7", "50");   // P        80     
        put("D8", "51");   // Q        81     
        put("D9", "52");   // R        82     
        put("E2", "53");   // S        83     
        put("E3", "54");   // T        84     
        put("E4", "55");   // U        85     
        put("E5", "56");   // V        86     
        put("E6", "57");   // W        87     
        put("E7", "58");   // X        88     
        put("E8", "59");   // Y        89     
        put("E9", "5A");   // Z        90     
        put("4A", "5B");   // [        91     
        put("E0", "5C");   // \        92     
        put("5A", "5D");   // ]        93     
        put("5F", "5E");   // ^        94     
        put("6D", "5F");   // _        95     
        put("79", "60");   // `        96     
        put("81", "61");   // a        97     
        put("82", "62");   // b        98     
        put("83", "63");   // c        99     
        put("84", "64");   // d        100    
        put("85", "65");   // e        101    
        put("86", "66");   // f        102    
        put("87", "67");   // g        103    
        put("88", "68");   // h        104    
        put("89", "69");   // i        105    
        put("91", "6A");   // j        106    
        put("92", "6B");   // k        107    
        put("93", "6C");   // l        108    
        put("94", "6D");   // m        109    
        put("95", "6E");   // n        110    
        put("96", "6F");   // o        111    
        put("97", "70");   // p        112    
        put("98", "71");   // q        113    
        put("99", "72");   // r        114    
        put("A2", "73");   // s        115    
        put("A3", "74");   // t        116    
        put("A4", "75");   // u        117    
        put("A5", "76");   // v        118    
        put("A6", "77");   // w        119    
        put("A7", "78");   // x        120    
        put("A8", "79");   // y        121    
        put("A9", "7A");   // z        122    
        put("C0", "7B");   // {        123    
        put("6A", "7C");   // |        124    
        put("D0", "7D");   // }        125    
        put("A1", "7E");   // ~        126    
        put("07", "7F");   //         127    
        put("20", "80");   // â‚¬        128    
        put("21", "81");   //          129    
        put("22", "82");   // â€š        130    
        put("23", "83");   // Æ’        131    
        put("24", "84");   // â€ž        132    
        put("15", "85");   // â€¦        133    
        put("06", "86");   // â€         134    
        put("17", "87");   // â€¡        135    
        put("28", "88");   // Ë†        136    
        put("29", "89");   // â€°        137    
        put("2A", "8A");   // Å         138    
        put("2B", "8B");   // â€¹        139    
        put("2C", "8C");   // Å’        140    
        put("09", "8D");   //          141    
        put("0A", "8E");   // Å½        142    
        put("1B", "8F");   //          143    
        put("30", "90");   //          144    
        put("31", "91");   // â€˜        145    
        put("1A", "92");   // â€™        146    
        put("33", "93");   // â€œ        147    
        put("34", "94");   // â€�        148    
        put("35", "95");   // â€¢        149    
        put("36", "96");   // â€“        150    
        put("08", "97");   // â€”        151    
        put("38", "98");   // Ëœ        152    
        put("39", "99");   // â„¢        153    
        put("3A", "9A");   // Å¡        154    
        put("3B", "9B");   // â€º        155    
        put("04", "9C");   // Å“        156    
        put("14", "9D");   //          157    
        put("3E", "9E");   // Å¾        158    
        put("E1", "9F");   // Å¸        159    
        put("41", "A0");   //          160    
        put("42", "A1");   // Â¡        161    
        put("43", "A2");   // Â¢        162    
        put("44", "A3");   // Â£        163    
        put("45", "A4");   // Â¤        164    
        put("46", "A5");   // Â¥        165    
        put("47", "A6");   // Â¦        166    
        put("48", "A7");   // Â§        167    
        put("49", "A8");   // Â¨        168    
        put("51", "A9");   // Â©        169    
        put("52", "AA");   // Âª        170    
        put("53", "AB");   // Â«        171    
        put("54", "AC");   // Â¬        172    
        put("55", "AD");   // Â­        173    
        put("56", "AE");   // Â®        174    
        put("57", "AF");   // Â¯        175    
        put("58", "B0");   // Â°        176    
        put("59", "B1");   // Â±        177    
        put("62", "B2");   // Â²        178    
        put("63", "B3");   // Â³        179    
        put("64", "B4");   // Â´        180    
        put("65", "B5");   // Âµ        181    
        put("66", "B6");   // Â¶        182    
        put("67", "B7");   // Â·        183    
        put("68", "B8");   // Â¸        184    
        put("69", "B9");   // Â¹        185    
        put("70", "BA");   // Âº        186    
        put("71", "BB");   // Â»        187    
        put("72", "BC");   // Â¼        188    
        put("73", "BD");   // Â½        189    
        put("74", "BE");   // Â¾        190    
        put("75", "BF");   // Â¿        191    
        put("76", "C0");   // Ã€        192    
        put("77", "C1");   // Ã�        193    
        put("78", "C2");   // Ã‚        194    
        put("80", "C3");   // Ãƒ        195    
        put("8A", "C4");   // Ã„        196    
        put("8B", "C5");   // Ã…        197    
        put("8C", "C6");   // Ã†        198    
        put("8D", "C7");   // Ã‡        199    
        put("8E", "C8");   // Ãˆ        200    
        put("8F", "C9");   // Ã‰        201    
        put("90", "CA");   // ÃŠ        202    
        put("9A", "CB");   // Ã‹        203    
        put("9B", "CC");   // ÃŒ        204    
        put("9C", "CD");   // Ã�        205    
        put("9D", "CE");   // ÃŽ        206    
        put("9E", "CF");   // Ã�        207    
        put("9F", "D0");   // Ã�        208    
        put("A0", "D1");   // Ã‘        209    
        put("AA", "D2");   // Ã’        210    
        put("AB", "D3");   // Ã“        211    
        put("AC", "D4");   // Ã”        212    
        put("AD", "D5");   // Ã•        213    
        put("AE", "D6");   // Ã–        214    
        put("AF", "D7");   // Ã—        215    
        put("B0", "D8");   // Ã˜        216    
        put("B1", "D9");   // Ã™        217    
        put("B2", "DA");   // Ãš        218    
        put("B3", "DB");   // Ã›        219    
        put("B4", "DC");   // Ãœ        220    
        put("B5", "DD");   // Ã�        221    
        put("B6", "DE");   // Ãž        222    
        put("B7", "DF");   // ÃŸ        223    
        put("B8", "E0");   // Ã         224    
        put("B9", "E1");   // Ã¡        225    
        put("BA", "E2");   // Ã¢        226    
        put("BB", "E3");   // Ã£        227    
        put("BC", "E4");   // Ã¤        228    
        put("BD", "E5");   // Ã¥        229    
        put("BE", "E6");   // Ã¦        230    
        put("BF", "E7");   // Ã§        231    
        put("CA", "E8");   // Ã¨        232    
        put("CB", "E9");   // Ã©        233    
        put("CC", "EA");   // Ãª        234    
        put("CD", "EB");   // Ã«        235    
        put("CE", "EC");   // Ã¬        236    
        put("CF", "ED");   // Ã­        237    
        put("DA", "EE");   // Ã®        238    
        put("DB", "EF");   // Ã¯        239    
        put("DC", "F0");   // Ã°        240    
        put("DD", "F1");   // Ã±        241    
        put("DE", "F2");   // Ã²        242    
        put("DF", "F3");   // Ã³        243    
        put("EA", "F4");   // Ã´        244    
        put("EB", "F5");   // Ãµ        245    
        put("EC", "F6");   // Ã¶        246    
        put("ED", "F7");   // Ã·        247    
        put("EE", "F8");   // Ã¸        248    
        put("EF", "F9");   // Ã¹        249    
        put("FA", "FA");   // Ãº        250    
        put("FB", "FB");   // Ã»        251    
        put("FC", "FC");   // Ã¼        252    
        put("FD", "FD");   // Ã½        253    
        put("FE", "FE");   // Ã¾        254    
        put("FF", "FF");   // Ã¿        255    
      }
    };

  private AsciiToEbcdicConverter()
  {	
  }
  
  public static String asciiToEbcdic( String asciiBytes )
    throws Exception
  {
    if ( (asciiBytes.length()%2) != 0 )
    {
      throw new Exception("byte buffer must have even length");
    }
  
    String ebcdicBytes = "";
    for( int i = 0; i < asciiBytes.length(); i += 2 )
    {
      ebcdicBytes += translateAsciiByte(asciiBytes.substring(i,i+2));
    }
    return( ebcdicBytes );
  }
  
  public static String ebcdicToAscii( String ebcdicBytes )
    throws Exception
  {
    if ( (ebcdicBytes.length()%2) != 0 )
    {
      throw new Exception("byte buffer must have even length");
    }
    
    String asciiBytes = "";
    for( int i = 0; i < ebcdicBytes.length(); i += 2 )
    {
      asciiBytes += translateEbcdicByte(ebcdicBytes.substring(i,i+2));
    }
    return( asciiBytes );
  }
  
  public static String translateAsciiByte( String asciiByte )
  {
    return( (String)AsciiToEbcdicMap.get(asciiByte) );
  }
  
  public static String translateEbcdicByte( String ebcdicByte )
  {
    return( (String)EbcdicToAsciiMap.get(ebcdicByte) );
  }
}  
