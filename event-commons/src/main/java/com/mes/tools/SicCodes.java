/*@lineinfo:filename=SicCodes*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/SicCodes.sqlj $

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-04-09 15:16:14 -0700 (Tue, 09 Apr 2013) $
  Version            : $Revision: 21031 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012, 2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.HashMap;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class SicCodes extends SQLJConnectionBase
{
  protected static final long       REFRESH_TIME_SECOND = 1000L;
  protected static final long       REFRESH_TIME_MINUTE = 60 * REFRESH_TIME_SECOND;
  protected static final long       REFRESH_TIME_HOUR   = 60 * REFRESH_TIME_MINUTE;
  
  protected static final long       REFRESH_TIME        = 24 * REFRESH_TIME_HOUR;
  
  public static class SicCodeData
  {
    private   boolean   HighRisk          = false;
    private   String    MerchandiseDesc   = null;
    private   String    MerchantType      = null;
    private   String    SicCode           = null;
    private   boolean   TipsAllowed       = false;
  
    public SicCodeData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HighRisk          = "Y".equals(resultSet.getString("high_risk"));
      MerchandiseDesc   = resultSet.getString("merchandise_desc");
      MerchantType      = resultSet.getString("merchant_type");
      SicCode           = resultSet.getString("sic_code");
      TipsAllowed       = "Y".equals(resultSet.getString("tips_allowed"));
    }
    
    public SicCodeData( String sicCode, String merchantType, boolean highRisk, boolean tipsAllowed, String merchandiseDesc )
    {
      HighRisk          = highRisk;
      MerchandiseDesc   = merchandiseDesc;
      MerchantType      = merchantType;
      SicCode           = sicCode;
      TipsAllowed       = tipsAllowed;
    }
    
    public   boolean   getHighRisk()          { return(HighRisk);        }
    public   String    getMerchandiseDesc()   { return(MerchandiseDesc); }
    public   String    getMerchantType()      { return(MerchantType);    }
    public   String    getSicCode()           { return(SicCode);         }
    public   boolean   getTipsAllowed()       { return(TipsAllowed);     }
  }
  
  protected static final SicCodeData[] SicCodeDefaults = 
  {
    new SicCodeData("0742" ,"Veterinary Services"                                         ,false  ,false ,"Vetrinarian Services"       ),
    new SicCodeData("0763" ,"Agricultural Cooperative"                                    ,false  ,false ,"Co-op Services"             ),
    new SicCodeData("0780" ,"Landscaping and Horticultural Services"                      ,false  ,false ,"Landscaping Services"       ),
    new SicCodeData("1520" ,"General Contractors - Resedential and Commercial"            ,true   ,false ,"Contractor Services"        ),
    new SicCodeData("1711" ,"Heating, Plumbing, and Air Conditioning Contractors"         ,true   ,false ,"HVAC Services"              ),
    new SicCodeData("1731" ,"Electical Contractors"                                       ,false  ,false ,"Electrical Contractor Svcs" ),
    new SicCodeData("1740" ,"Masonry, Stonework, Tile Setting, Plastering and Insulation" ,false  ,false ,"Masonry Services"           ),
    new SicCodeData("1750" ,"Carpentry Contractors"                                       ,false  ,false ,"Contractor Services"        ),
    new SicCodeData("1761" ,"Roofing, Siding, and Sheet Metal Work Contractors"           ,true   ,false ,"Roofing Services"           ),
    new SicCodeData("1771" ,"Concrete Work Contractors"                                   ,false  ,false ,"Concrete Services"          ),
    new SicCodeData("1799" ,"Special Trade Contractors (Not Elsewhere Classified)"        ,true   ,false ,"Contractor Services"        ),
    new SicCodeData("2741" ,"Miscellaneous Publishing and Printing"                       ,false  ,false ,"Printing Services"          ),
    new SicCodeData("2791" ,"Typesetting, Plate Making and Related Services"              ,false  ,false ,"Printing Services"          ),
    new SicCodeData("2842" ,"Specialty Cleaning, Polishing and Sanitation Preparations"   ,false  ,false ,"Cleaning Services"          ),
    new SicCodeData("3000" ,"UNITED AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3001" ,"AMERICAN AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3002" ,"PAN AMERICAN"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3003" ,"EUROFLY AIRLINES EUROFLY AIR"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3004" ,"TRANS WORLD AIRLINES"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3005" ,"BRITISH AIRWAYS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3006" ,"JAPAN AIRLINES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3007" ,"AIR FRANCE"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3008" ,"LUFTHANSA"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3009" ,"AIR CANADA"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3010" ,"KLM ( ROYAL DUTCH AIRLINES)"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3011" ,"AEROFLOT"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3012" ,"QANTAS"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3013" ,"ALITALIA"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3014" ,"SAUDI ARABIAN AIRLINES"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3015" ,"SWISSAIR"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3016" ,"SAS"                                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3017" ,"SOUTH AFRICAN AIRWAYS"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3018" ,"VARIG ( BRAZIL )"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3020" ,"AIR-INDIA"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3021" ,"AIR ALGERIE"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3022" ,"PHILIPPINE AIRLINES"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3023" ,"MEXICANA"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3024" ,"PAKISTAN INTERNATIONAL"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3025" ,"AIR NEW ZEALAND"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3026" ,"EMIRATES AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3027" ,"UTA/INTERAIR"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3028" ,"AIR MALTA"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3029" ,"SABENA"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3030" ,"AEROLINEAS ARGENTINAS"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3031" ,"OLYMPIC AIRWAYS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3032" ,"EL AL"                                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3033" ,"ANSETT AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3034" ,"AUSTRALIAN AIRLINES"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3035" ,"TAP (PORTUGAL)"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3036" ,"VASP (BRAZIL)"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3037" ,"EGYPTAIR"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3038" ,"KUWAIT AIRWAYS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3039" ,"AVIANCA"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3040" ,"GULF AIR (BAHRAIN)"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3041" ,"BALKAN-BULGARIAN AIRLINES"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3042" ,"FINNAIR"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3043" ,"AER LINGUS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3044" ,"AIR LANKA"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3045" ,"NIGERIA AIRWAYS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3046" ,"CRUZERIO DO SUL (BRAZIL)"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3047" ,"THY (TURKEY)"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3048" ,"ROYAL AIR MAROC"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3049" ,"TUNIS AIR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3050" ,"ICELANDAIR"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3051" ,"AUSTRIAN AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3052" ,"LANCHILE"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3053" ,"AVIACO (SPAIN)"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3054" ,"LADECO (CHILE)"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3055" ,"LAB (BOLIVIA)"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3056" ,"QUEBECAIRE"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3057" ,"EAST/WEST AIRLINES (AUSTRALIA)"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3058" ,"DELTA"                                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3060" ,"NORTHWEST"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3061" ,"CONTINENTAL"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3063" ,"U.S. AIRWAYS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3064" ,"ADRIA AIRWAYS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3065" ,"AIRINTER"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3066" ,"SOUTHWEST"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3067" ,"VANGUARD AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3071" ,"AIR BRITISH COLUMBIA"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3075" ,"SINGAPORE AIRLINES"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3076" ,"AEROMEXICO"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3077" ,"THAI AIRWAYS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3078" ,"CHINA AIRLINES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3081" ,"NORDAIR"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3082" ,"KOREAN AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3083" ,"AIR AFRIQUE"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3084" ,"EVA AIRLINES"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3085" ,"MIDWEST EXPRESS AIRLINES, INC."                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3086" ,"CARNIVAL AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3087" ,"METRO AIRLINES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3088" ,"CROATIA AIRLINES"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3089" ,"TRANSAERO"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3090" ,"UNI AIRWAYS CORPORATION"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3094" ,"ZAMBIA AIRWAYS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3096" ,"AIR ZIMBABWE"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3097" ,"SPANAIR"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3098" ,"ASIANA AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3099" ,"CATHAY PACIFIC"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3100" ,"MALAYSIAN AIRLINE SYSTEM"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3102" ,"IBERIA"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3103" ,"GARUDA (INDONESIA)"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3106" ,"BRAATHENS S.A.F.E. (NORWAY)"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3110" ,"WINGS AIRWAYS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3111" ,"BRITISH MIDLAND"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3112" ,"WINDWARD ISLAND"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3115" ,"TOWER AIR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3117" ,"VIASA"                                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3118" ,"VALLEY AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3125" ,"TAN"                                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3126" ,"TALAIR"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3127" ,"TACA INTERNATIONAL"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3129" ,"SURINAM AIRWAYS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3130" ,"SUN WORLD INTERNATIONAL"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3132" ,"FRONTIER AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3133" ,"SUNBELT AIRLINES"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3135" ,"SUDAN AIRWAYS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3136" ,"QATAR AIRWAYS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3137" ,"SINGLETON"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3138" ,"SIMMONS AIRLINES"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3143" ,"SCENIC AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3144" ,"VIRGIN ATLANTIC"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3145" ,"SAN JUAN AIRLINES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3146" ,"LUXAIR"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3151" ,"AIR ZAIRE"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3154" ,"PRINCEVILLE"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3159" ,"PBA"                                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3161" ,"ALL NIPPON AIRWAYS"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3164" ,"NORONTAIR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3165" ,"NEW YORK HELICOPTER"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3170" ,"MOUNT COOK"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3171" ,"CANADIAN AIRLINES INTERNATIONAL"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3172" ,"NATIONAIR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3175" ,"MIDDLE EAST AIR"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3176" ,"METROFLIGHT AIRLINES"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3178" ,"MESA AIR"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3181" ,"MALEV"                                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3182" ,"LOT (POLAND)"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3184" ,"LIAT"                                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3185" ,"LAV (VENEZUELA)"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3186" ,"LAP (PARAGUAY)"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3187" ,"LACSA (COSTA RICA)"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3190" ,"JUGOSLAV AIR"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3191" ,"ISLAND AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3192" ,"IRAN AIR"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3193" ,"INDIAN AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3196" ,"HAWAIIAN AIR"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3197" ,"HAVASU AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3200" ,"GUYANA AIRWAYS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3203" ,"GOLDEN PACIFIC AIR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3204" ,"FREEDOM AIR"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3206" ,"CHINA EASTERN AIRLINES"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3212" ,"DONIMICANA"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3215" ,"DAN AIR SERVICES"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3216" ,"CUMBERLAND AIRLINES"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3217" ,"CSA"                                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3218" ,"CROWN AIR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3219" ,"COPA"                                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3220" ,"COMPANIA FAUCETT"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3221" ,"TRANSPORTES AEROS MILITARES ECUATORIANOS"                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3222" ,"COMMAND AIRWAYS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3223" ,"COMAIR"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3228" ,"CAYMAN AIRWAYS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3229" ,"SAETA - SOCIAEDAD ECUATORIANOS DE TRANSPORTES AEREOS"        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3231" ,"SAHSA - SERVICIO AEREO DE HONDURAS"                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3233" ,"CAPITOL AIR"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3234" ,"BWIA"                                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3235" ,"BROCKWAY AIR"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3238" ,"BEMIDJI AIRLINES"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3239" ,"BAR HARBOR AIRLINES"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3240" ,"BAHAMASAIR"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3241" ,"AVIATECA (GUATEMALA)"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3242" ,"AVENSA"                                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3243" ,"AUSTRAIN AIR SERVICE"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3251" ,"ALOHA AIRLINES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3252" ,"ALM"                                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3253" ,"AMERICA WEST"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3254" ,"U.S. AIR SHUTTLE"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3256" ,"ALASKA AIRLINES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3259" ,"AMERICAN TRANS AIR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3261" ,"AIR CHINA"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3262" ,"RENO AIR, INC."                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3266" ,"AIR SEYCHELLES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3267" ,"AIR PANAMA"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3280" ,"AIR JAMAICA"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3282" ,"AIR DJIBOUTI"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3284" ,"AERO VIRGIN ISLANDS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3285" ,"AEROPERU"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3286" ,"AEROLINEAS NICARAGUENSIS"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3287" ,"AERO COACH AVIATION"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3292" ,"CYPRUS AIRWAYS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3293" ,"ECUATORIANA"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3294" ,"ETHIOPIAN AIRLINES"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3295" ,"KENYA AIRWAYS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3297" ,"TAROM ROMANIAN AIR TRANSPORT"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3298" ,"AIR MAURITIUS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3299" ,"WIDEROE'S FLYVESELSKAP"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3300" ,"AZUL AIR"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3351" ,"AFFILIATED AUTO RENTAL"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3352" ,"AMERICAN INTL RENT-A-CAR"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3353" ,"BROOKS RENT-A-CAR"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3354" ,"ACTION AUTO RENTAL"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3357" ,"HERTZ RENT-A CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3359" ,"PAYLESS CAR RENTAL"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3360" ,"SNAPPY CAR RENTAL"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3361" ,"AIRWAYS RENT-A-CAR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3362" ,"ALTRA AUTO RENTAL"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3364" ,"AGENCY RENT-A-CAR"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3366" ,"BUDGET RENT-A-CAR"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3368" ,"HOLIDAY RENT-A-CAR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3370" ,"RENT-A-WRECK"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3376" ,"AJAX RENT-A-CAR"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3380" ,"TRIANGLE RENT-A-CAR"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3381" ,"EUROP CAR"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3385" ,"TROPICAL RENT-A-CAR"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3386" ,"SHOWCASE RENTAL CARS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3387" ,"ALAMO RENT-A-CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3389" ,"AVIS RENT-A-CAR"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3390" ,"DOLLAR RENT-A-CAR"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3391" ,"EUROPE BY CAR"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3393" ,"NATIONAL CAR RENTAL"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3394" ,"KEMWELL GROUP RENT-A-CAR"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3395" ,"THRIFTY CAR RENTAL"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3396" ,"TILDEN RENT-A-CAR"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3398" ,"ECONO-CAR RENT-A-CAR"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3400" ,"AUTO HOST CAR RENTALS"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3405" ,"ENTERPRISE RENT-A-CAR"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3409" ,"GENERAL RENT-A-CAR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3412" ,"A-1 RENT-A-CAR"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3414" ,"GODFREY NATL RENT-A-CAR"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3420" ,"ANSA INTL RENT-A-CAR"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3421" ,"ALLSTATE RENT-A-CAR"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3423" ,"AVCAR RENT-A-CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3425" ,"AUTOMATE RENT-A-CAR"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3427" ,"AVON RENT-A-CAR"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3428" ,"CAREY RENT-A-CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3429" ,"INSURANCE RENT-A-CAR"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3430" ,"MAJOR RENT-A-CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3431" ,"REPLACEMENT RENT-A-CAR"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3432" ,"RESERVE RENT-A-CAR"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3433" ,"UGLY DUCKLING RENT-A-CAR"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3434" ,"USA RENT-A-CAR"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3435" ,"VALUE RENT-A-CAR"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3436" ,"AUTOHANSA RENT-A-CAR"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3437" ,"CITE RENT-A-CAR"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3438" ,"INTERENT RENT-A-CAR"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3439" ,"MILLEVILLE RENT-A-CAR"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3441" ,"ADVANTAGE RENT-A-CAR"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3501" ,"HOLIDAY INN EXPRESS"                                         ,false  ,false ,"Hotel Stay"                 ),
    new SicCodeData("3502" ,"BEST WESTERN HOTELS"                                         ,false  ,false ,"Hotel Stay"                 ),
    new SicCodeData("3503" ,"SHERATON HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3504" ,"HILTON HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3505" ,"FORTE HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3506" ,"GOLDEN TULIP HOTELS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3507" ,"FRIENDSHIP INNS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3508" ,"QUALITY SUITES"                                              ,false  ,false ,"Hotel Stay"                 ),
    new SicCodeData("3509" ,"MARRIOTT HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3510" ,"DAYS INNS"                                                   ,false  ,false ,"Hotel Stay"                 ),
    new SicCodeData("3511" ,"ARABELLA HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3512" ,"INTER-CONTINENTAL HOTELS"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3513" ,"WESTIN HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3514" ,"AMERISUITES"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3515" ,"RODEWAY INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3516" ,"LA QUINTA MOTOR INNS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3517" ,"AMERICANA HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3518" ,"SOL HOTELS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3519" ,"PULLMAN INTERNATIONAL HOTELS"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3520" ,"MERIDIEN HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3522" ,"TOKYO HOTEL"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3523" ,"PENINSULA HOTEL"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3524" ,"WELCOMGROUP HOTELS"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3525" ,"DUNFEY  HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3526" ,"PRINCE HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3527" ,"DOWNTOWNER-PASSPORT HOTEL"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3528" ,"RED LION HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3529" ,"CP HOTELS"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3530" ,"RENAISSANCE HOTELS"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3533" ,"HOTEL IBIS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3534" ,"SOUTHERN PACIFIC HOTELS"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3535" ,"HILTON INTERNATIONALS"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3536" ,"AMFAC HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3537" ,"ANA HOTEL"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3538" ,"CONCORDE HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3539" ,"SUMMERFIELD SUITES HOTELS"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3540" ,"IBEROTEL HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3541" ,"HOTEL OKURA"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3542" ,"ROYAL HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3543" ,"FOUR SEASONS HOTELS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3544" ,"CIGA HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3545" ,"SHARGRI-LA INTERNATIONAL"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3546" ,"SIERRA SUITES HOTELS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3548" ,"HOTELES MELIA"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3549" ,"AUBERGE DES GOVERNEURS"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3550" ,"REGAL 8 INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3551" ,"MIRAGE HOTEL AND CASINO"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3552" ,"COAST HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3553" ,"PARK INNS INTERNATIONAL"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3555" ,"TREASURE ISLAND HOTEL AND CASINO"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3558" ,"JOLLY HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3559" ,"CANDLEWOOD SUITES CANDLEWOOD SUITES"                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3561" ,"GOLDEN NUGGET"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3562" ,"COMFORT INNS"                                                ,false  ,false ,"Hotel Stay"                 ),
    new SicCodeData("3563" ,"JOURNEY'S END MOTELS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3564" ,"SAM'S TOWN HOTEL AND CASINO"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3565" ,"RELAX INNS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3568" ,"LADBROKE HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3570" ,"FORUM HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3572" ,"MIYAKO HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3573" ,"SANDMAN HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3574" ,"VENTURE INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3575" ,"VAGABOND HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3577" ,"MANDARIN ORIENTAL HOTEL"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3579" ,"HOTEL MERCURE"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3581" ,"DELTA HOTEL"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3582" ,"CALIFORNIA HOTEL AND CASINO"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3583" ,"SAS HOTELS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3584" ,"PRINCESS HOTELS INTERNATIONAL"                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3585" ,"HUNGAR HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3586" ,"SOKOS HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3587" ,"DORAL HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3588" ,"HELMSLEY HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3590" ,"FAIRMONT HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3591" ,"SONESTA HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3592" ,"OMNI HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3593" ,"CUNARD HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3595" ,"HOSPITALITY INNS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3598" ,"REGENT INTERNATIONAL HOTELS"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3599" ,"PANNONIA HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3602" ,"HUDSON HOTEL HUDSON HOTEL"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3603" ,"NOAH'S HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3604" ,"HILTON GARDEN INN HILTON GARDEN INN"                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3607" ,"FONTAINEBLEAU RESORTS FONTAINEBLEAU RESORTS"                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3612" ,"MOVENPICK HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3613" ,"MICROTEL INN & SUITES"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3614" ,"AMERICINN"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3615" ,"TRAVELODGE"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3617" ,"AMERICA'S BEST VALUE INN"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3620" ,"BINION'S HORSESHOE CLUB"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3622" ,"MERLIN HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3623" ,"DORINT HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3624" ,"LADY LUCK HOTEL AND CASINO"                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3625" ,"HOTEL UNIVERSALE"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3628" ,"EXCALIBUR HOTEL AND CASINO"                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3629" ,"DAN HOTELS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3631" ,"SLEEP INN"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3632" ,"THE PHOENICIAN"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3633" ,"RANK HOTEL"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3634" ,"SWISSOTEL"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3635" ,"RESO HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3636" ,"SAROVA HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3637" ,"RAMADA INNS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3638" ,"HOWARD JOHNSON"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3639" ,"MOUNT CHARLOTTE THISTLE"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3640" ,"HYATT HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3641" ,"SOFITEL HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3642" ,"NOVOTEL HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3643" ,"STEIGENBERGER HOTELS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3644" ,"ECONO LODGES"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3645" ,"QUEENS MOAT HOUSES"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3646" ,"SWALLOW HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3647" ,"HUSA HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3648" ,"DE VERE HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3649" ,"RADISSON HOTELS"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3650" ,"RED ROOF INNS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3651" ,"IMPERIAL LONDON HOTEL"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3652" ,"EMBASSY HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3653" ,"PENTA HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3654" ,"LOEWS HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3655" ,"SCANDIC HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3656" ,"SARA HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3657" ,"OBEROI HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3658" ,"OTANI HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3659" ,"TAJ HOTELS INTERNATIONAL"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3660" ,"KNIGHTS INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3661" ,"METROPOLE HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3662" ,"CIRCUS CIRCUS HOTEL AND CASINO"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3663" ,"HOTELES EL PRESIDENTE"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3664" ,"FLAG INN"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3665" ,"HAMPTON INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3666" ,"STAKIS HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3667" ,"LUXOR HOTEL AND CASINO"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3668" ,"MARITIM HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3669" ,"ELDORADO HOTEL AND CASINO"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3670" ,"ARCADE HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3671" ,"ARCTIA HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3672" ,"CAMPANILE HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3673" ,"IBUSZ HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3674" ,"RANTASIPI HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3675" ,"INTERHOTEL CEDOK"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3676" ,"MONTE CARLO HOTEL AND CASINO"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3677" ,"CLIMAT DE FRANCE HOTELS"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3678" ,"CUMULUS HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3679" ,"SILVER LEGACY HOTEL AND CASINO"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3680" ,"HOTEIS OTHAN"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3681" ,"ADAMS MARK HOTELS"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3682" ,"SAHARA HOTEL AND CASINO"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3683" ,"BRADBURY SUITES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3684" ,"BUDGET HOST INN"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3685" ,"BUDGETEL INNS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3686" ,"SUISSE CHALETS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3687" ,"CLARION HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3688" ,"COMPRI HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3689" ,"CONSORT HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3690" ,"COURTYARD BY MARRIOTT"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3691" ,"DILLON INNS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3692" ,"DOULBETREE HOTELS"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3693" ,"DRURY INNS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3694" ,"ECONOMY INNS OF AMERICAN"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3695" ,"EMBASSY SUITES"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3696" ,"EXEL INNS"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3697" ,"FAIRFIELD HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3698" ,"HARLEY HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3699" ,"MIDWAY MOTOR LODGE"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3700" ,"MOTEL 6"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3701" ,"LA MANSION DEL RIO"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3702" ,"THE REGISTRY HOTEL"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3703" ,"RESIDENCE INNS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3704" ,"ROYCE HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3705" ,"SANDMAN INNS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3706" ,"SHILO INNS"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3707" ,"SHONEY'S INNS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3708" ,"VIRGIN RIVER HOTEL AND CASINO"                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3709" ,"SUPER 8 MOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3710" ,"THE RITZ CARLTON"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3711" ,"FLAG INNS (AUSTRALIA)"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3712" ,"BUFFALO BILL'S HOTEL AND CASINO"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3713" ,"QUALITY PACIFIC HOTEL"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3714" ,"FOUR SEASONS HOTEL (AUSTRALIA)"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3715" ,"FAIRFIELD INN"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3716" ,"CARLTON HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3717" ,"CITY LODGE HOTELS"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3718" ,"KAROS HOTELS"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3719" ,"PROTEA HOTELS"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3720" ,"SOUTHERN SUN HOTELS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3721" ,"HILTON CONRAD"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3722" ,"WYNDHAM HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3723" ,"RICA HOTELS"                                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3724" ,"INTER NOR HOTELS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3725" ,"SEAPINES PLANTATION"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3726" ,"RIO SUITES"                                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3727" ,"BROADMOOR HOTEL"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3728" ,"BALLY'S HOTEL AND CASINO"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3729" ,"JOHN ASCUAGA'S NUGGET"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3730" ,"MGM GRAND HOTEL"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3731" ,"HARRAH'S HOTELS AND CASINOS"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3732" ,"OPRYLAND HOTEL"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3733" ,"BOCA RATON RESORT"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3734" ,"HARVEY/BRISTOL HOTELS"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3735" ,"MASTERS ECONOMY INNS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3736" ,"COLORADO BELLE/EDGEWATER RESORT"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3737" ,"RIVIERA HOTEL AND CASINO"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3738" ,"TROPICANA RESORT AND CASINO"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3739" ,"WOODSIDE HOTELS AND RESORTS"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3740" ,"TOWNEPLACE SUITES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3741" ,"MILLENNIUM BROADWAY HOTEL"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3742" ,"CLUB MED"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3743" ,"BILTMORE HOTEL AND SUITES"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3744" ,"CAREFREE RESORTS"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3745" ,"ST. REGIS HOTEL"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3746" ,"THE ELIOT HOTEL"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3747" ,"CLUBCORP/CLUBRESORTS"                                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3748" ,"WELLESLEY INNS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3749" ,"THE BEVERLY HILLS HOTEL"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3750" ,"CROWNE PLAZA HOTELS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3751" ,"HOMEWOOD SUITES"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3752" ,"PEABODY HOTELS"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3753" ,"GREENBRIAR RESORTS"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3754" ,"AMELIA ISLAND PLANTATION"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3755" ,"THE HOMESTEAD"                                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3756" ,"SOUTH SEAS RESORTS"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3757" ,"CANYON RANCH"                                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3758" ,"KAHALA MANDARIN ORIENTAL HOTEL"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3759" ,"THE ORCHID AT MAUNA LANI"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3760" ,"HALEKULANI HOTEL/WAIKIKI PARC"                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3761" ,"PRIMADONNA HOTEL AND CASINO"                                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3762" ,"WHISKEY PETE'S HOTEL AND CASINO"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3763" ,"CHATEAU ELAN WINERY AND RESORT"                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3764" ,"BEAU RIVAGE HOTEL AND CASINO"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3765" ,"BELLAGIO"                                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3766" ,"FREMONT HOTEL AND CASINO"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3767" ,"MAIN STREET STATION HOTEL AND CASINO"                        ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3768" ,"SILVER STAR HOTEL AND CASINO"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3769" ,"STRATOSPHERE HOTEL AND CASINO"                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3770" ,"SPRINGHILL SUITES"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3771" ,"CAESAR'S HOTEL AND CASINO"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3772" ,"NEMACOLIN WOODLANDS"                                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3773" ,"THE VENETION RESORT HOTEL CASINO"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3774" ,"NEW YORK-NEW YORK HOTEL AND CASINO"                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("3781" ,"UNKNOWN"                                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4011" ,"Railroads"                                                   ,false  ,false ,"Shipping Services"          ),
    new SicCodeData("4111" ,"Local and Suburban Commuter Passenger Transportation, Inclu" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4112" ,"Passenger Railways (T in U.S. only)"                         ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4119" ,"Ambulance Services"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4121" ,"Taxicabs and Limousines"                                     ,true   ,true ,"Transportation Services"    ),
    new SicCodeData("4131" ,"Bus Lines"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4214" ,"Motor Freight Carriers and Trucking-Local and Long Distance" ,false  ,false ,"Freight Services"           ),
    new SicCodeData("4215" ,"Courier Services-Air and Ground, and Freight Forwarders"     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4225" ,"Public Warehousing and Storage-Farm Products, Refrigerated"  ,false  ,false ,"Warehouse Services"         ),
    new SicCodeData("4411" ,"Steamship and Cruise Lines (T in U.S. only effective thro"   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4457" ,"Boat Rentals and Leasing"                                    ,false  ,false ,"Boat Rental"                ),
    new SicCodeData("4468" ,"Marinas, Marine Service, and Supplies"                       ,false  ,false ,"Marine Service"             ),
    new SicCodeData("4511" ,"Airlines and Air Carriers-Not Elsewhere Classified     Amex" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4582" ,"Airports, Flying Fields, and Airport Terminals"              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4722" ,"Travel Agencies and Tour Operators"                          ,true   ,false ,"Travel Services"            ),
    new SicCodeData("4723" ,"Package Tour Operators - For Use in Germany Only"            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4784" ,"Tolls and Bridge Fees"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4789" ,"Transportation Services (Not Elsewhere Classified)"          ,false  ,false ,"Transportation Services"    ),
    new SicCodeData("4812" ,"Telecommunication Equipment and Telephone Sales   Amex Excl" ,false  ,false ,"Telecommunication Services" ),
    new SicCodeData("4814" ,"Telecommunicatioon Services, including Local and Long Dista" ,true   ,false ,"Telecommunication Services" ),
    new SicCodeData("4815" ,"VisaPhone"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4816" ,"Computer Network/Information Services   Amex Excluded"       ,true   ,false ,"Computer Networking Svcs"   ),
    new SicCodeData("4821" ,"Telegraph Services"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4829" ,"Wire Transfer Money Orders"                                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("4899" ,"Cable and Other Pay Television Services    Amex Excluded"    ,false  ,false ,"Cable Services"             ),
    new SicCodeData("4900" ,"Utilities - Electric, Gas, Water Sanitary"                   ,false  ,false ,"Utility Payment"            ),
    new SicCodeData("5013" ,"Motor Vehicle Supplies and New Parts"                        ,false  ,false ,"Auto Parts"                 ),
    new SicCodeData("5021" ,"Office and Commercial Furniture"                             ,false  ,false ,"Office Furniture"           ),
    new SicCodeData("5039" ,"Construction Materials (Not Elsewhere Classified)"           ,false  ,false ,"Building Supplies"          ),
    new SicCodeData("5044" ,"Photographic, Photocopy, Microfilm Equipment and Supplies"   ,false  ,false ,"Photography Equipment"      ),
    new SicCodeData("5045" ,"Computers and Computer Peripheral Equipment and Software"    ,false  ,false ,"Computer Equipment"         ),
    new SicCodeData("5046" ,"Commercial Equipment (Not Elsewhere Classified)"             ,false  ,false ,"Commercial Equipment"       ),
    new SicCodeData("5047" ,"Medical, Dental, Ophthalmic and Hospital Equipment and Supp" ,false  ,false ,"Medical Equipment"          ),
    new SicCodeData("5051" ,"Metal Service Centers and Offices"                           ,false  ,false ,"Raw Materials"              ),
    new SicCodeData("5065" ,"Electrical Parts and Equipment"                              ,false  ,false ,"Electrical Parts"           ),
    new SicCodeData("5072" ,"Hardware, Equipment and Supplies"                            ,false  ,false ,"Hardware Items"             ),
    new SicCodeData("5074" ,"Plumbing and Heating Equipment and Supplies"                 ,false  ,false ,"Plumbing Supplies"          ),
    new SicCodeData("5085" ,"Industrial Supplies (Not Elsewhere Classified)"              ,false  ,false ,"Industrial Supplies"        ),
    new SicCodeData("5094" ,"Preciouse Stones and Metals, Watches and Jewelry"            ,false  ,false ,"Jewelry"                    ),
    new SicCodeData("5099" ,"Durable Goods (Not Elsewhere Classified)"                    ,false  ,false ,"Retail Products"            ),
    new SicCodeData("5111" ,"Stationery, Office Supplies, Printing and Writing Paper"     ,false  ,false ,"Office Supplies"            ),
    new SicCodeData("5122" ,"Drugs, Drug Proprietaries, and Druggist Sundries"            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5131" ,"Piece Goods, Notions, and Other Dry Goods"                   ,false  ,false ,"Retail Products"            ),
    new SicCodeData("5137" ,"Men's, Women's, and Children's Uniforms and Commercial C"    ,false  ,false ,"Uniforms"                   ),
    new SicCodeData("5139" ,"Commercial Footwear"                                         ,false  ,false ,"Shoes"                      ),
    new SicCodeData("5169" ,"Chemicals and Allied Products (Not Elsewhere Classified)"    ,false  ,false ,"Chemicals"                  ),
    new SicCodeData("5172" ,"Petroleum and Petroleum Products"                            ,false  ,false ,"Fuel"                       ),
    new SicCodeData("5192" ,"Books, Periodicals and Newspapers"                           ,false  ,false ,"Books"                      ),
    new SicCodeData("5193" ,"Florists Supplies, Nursery Stock and Flowers"                ,false  ,false ,"Nursery Products"           ),
    new SicCodeData("5198" ,"Paints, Varnishes and Supplies"                              ,false  ,false ,"Paint Supplies"             ),
    new SicCodeData("5199" ,"Nondurable Goods (Not Elsewhere Classified)"                 ,false  ,false ,"Services"                   ),
    new SicCodeData("5200" ,"Home Supply Warehouse Stores"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5211" ,"Lumber and Building Materials Stores"                        ,false  ,false ,"Building Supplies"          ),
    new SicCodeData("5231" ,"Glass, Paint, and Wallpaper Stores"                          ,false  ,false ,"Hardware Items"             ),
    new SicCodeData("5251" ,"Hardware Stores"                                             ,false  ,false ,"Hardware Items"             ),
    new SicCodeData("5261" ,"Nurseries and Lawn and Garden Supply Stores"                 ,false  ,false ,"Nursery Products"           ),
    new SicCodeData("5271" ,"Mobile Home Dealers"                                         ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5300" ,"Wholesale Clubs"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5309" ,"Duty Free Stores"                                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5310" ,"Discount Stores"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5311" ,"Department Stores"                                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5331" ,"Variety Stores"                                              ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5399" ,"Miscellaneous General Merchandise    Amex Excluded"          ,false  ,false ,"Retail Products"            ),
    new SicCodeData("5411" ,"Grocery Stores and Supermarkets"                             ,false  ,false ,"Groceries"                  ),
    new SicCodeData("5422" ,"Freezer and Locker Meat Provisioners"                        ,false  ,false ,"Meat"                       ),
    new SicCodeData("5441" ,"Candy, Nut and Confectionery Stores"                         ,false  ,false ,"Candy"                      ),
    new SicCodeData("5451" ,"Dairy Products Stores"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5462" ,"Bakeries"                                                    ,false  ,false ,"Baked Goods"                ),
    new SicCodeData("5499" ,"Miscellaneous Food Stores - Convenience Stores and Specialt" ,false  ,false ,"Convenience Store Purchase" ),
    new SicCodeData("5511" ,"Car and Truck Dealers (New and Used) Sales, Service, Repair" ,true   ,false ,"Autos"                      ),
    new SicCodeData("5521" ,"Car and Truck Dealers (Used Only) Sales, Service, Repairs"   ,true   ,false ,"Autos"                      ),
    new SicCodeData("5531" ,"Auto and Home Supply Stores (No longer valid for first pres" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5532" ,"Automotive Tire Stores"                                      ,false  ,false ,"Tires"                      ),
    new SicCodeData("5533" ,"Automotive Parts and Accessories Stores"                     ,false  ,false ,"Automotive Parts"           ),
    new SicCodeData("5541" ,"Service Stations (with or without Ancillary Services)"       ,false  ,false ,"Fuel"                       ),
    new SicCodeData("5542" ,"Automated Fuel Dispensers"                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5551" ,"Boat Dealers"                                                ,false  ,false ,"Boat  "                     ),
    new SicCodeData("5561" ,"Camper, Recreational and Utility Trailer Dealers"            ,false  ,false ,"Recreation Equipment"       ),
    new SicCodeData("5571" ,"Motorcycle Shops and Dealers"                                ,false  ,false ,"Motorcycles"                ),
    new SicCodeData("5592" ,"Motor Home Dealers"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5598" ,"Snowmoblie Dealers"                                          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5599" ,"Miscellaneous Automotive, Aircraft, and Farm Equipment Deal" ,false  ,false ,"Auto Parts"                 ),
    new SicCodeData("5611" ,"Men's and Boys' Clothing and Accessories Stores"             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5621" ,"Women's Ready-To-Wear Stores"                                ,false  ,false ,"Clothing"                   ),
    new SicCodeData("5631" ,"Women's Accessory and Specialty Shops"                       ,false  ,false ,"Boutique Goods"             ),
    new SicCodeData("5641" ,"Children's and Infants' Wear Stores"                         ,false  ,false ,"Clothing"                   ),
    new SicCodeData("5651" ,"Family Clothing Stores"                                      ,false  ,false ,"Clothing"                   ),
    new SicCodeData("5655" ,"Sports and Riding Apparel Stores"                            ,false  ,false ,"Sporting Goods"             ),
    new SicCodeData("5661" ,"Shoe Stores"                                                 ,false  ,false ,"Shoes"                      ),
    new SicCodeData("5681" ,"Furriers and Fur Shops"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5691" ,"Men's and Women's Clothing Stores"                           ,false  ,false ,"Clothing"                   ),
    new SicCodeData("5697" ,"Tailors, Seamstresses, Mending, and Alterations"             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5698" ,"Wig and Toupee Stores"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5699" ,"Miscellaneous Apparel and Accessory Shops"                   ,false  ,false ,"Clothing"                   ),
    new SicCodeData("5712" ,"Furniture, Home Furnishing and Equipment Stores, Except App" ,true   ,false ,"Furniture"                  ),
    new SicCodeData("5713" ,"Floor Covering Stores"                                       ,false  ,false ,"Floor Coverings"            ),
    new SicCodeData("5714" ,"Drapery, Window Covering, and Upholstery Stores"             ,false  ,false ,"Upholstery"                 ),
    new SicCodeData("5718" ,"Fireplace, Fireplace Screens and Accessories Stores"         ,false  ,false ,"Fireplace Supplies"         ),
    new SicCodeData("5719" ,"Miscellaneous Home Furnishing Specialty Stores"              ,false  ,false ,"Home Furnishings"           ),
    new SicCodeData("5722" ,"Household Appliance Stores"                                  ,false  ,false ,"Appliances"                 ),
    new SicCodeData("5732" ,"Electronics Stores"                                          ,true   ,false ,"Electronics"                ),
    new SicCodeData("5733" ,"Music Stores - Musical Instruments, Pianos and Sheet Music"  ,false  ,false ,"Musical Instruments"        ),
    new SicCodeData("5734" ,"Computer Software Stores"                                    ,false  ,false ,"Computer Software"          ),
    new SicCodeData("5735" ,"Record Stores"                                               ,false  ,false ,"Music  "                    ),
    new SicCodeData("5811" ,"Caterers"                                                    ,true   ,false ,"Food"                       ),
    new SicCodeData("5812" ,"Eating Places and Restaurants"                               ,false  ,true ,"Food"                       ),
    new SicCodeData("5813" ,"Drinking Places (Alcoholic Beverages) - Bars, Taverns, Nigh" ,false  ,true ,"Drinks"                     ),
    new SicCodeData("5814" ,"Fast Food Restaurants -conv/petro surcharge"                 ,false  ,true ,"Fast Food"                  ),
    new SicCodeData("5912" ,"Drug Stores and Pharmacies"                                  ,false  ,false ,"Medicine"                   ),
    new SicCodeData("5921" ,"Package Stores - Beer, Wine, and Liquor"                     ,false  ,false ,"Alcohol "                   ),
    new SicCodeData("5931" ,"Used Merchandise and Secondhand Stores"                      ,false  ,false ,"Second Hand Merchandise"    ),
    new SicCodeData("5932" ,"Antique Shops - Sales, Repairs, and Restoration Services"    ,false  ,false ,"Antiques"                   ),
    new SicCodeData("5933" ,"Pawn Shops"                                                  ,false  ,false ,"Retail Products"            ),
    new SicCodeData("5935" ,"Wrecking and Salvage Yards"                                  ,false  ,false ,"Salvage Services"           ),
    new SicCodeData("5937" ,"Antique Reproductions"                                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5940" ,"Bicycle Shops - Sales and Service"                           ,false  ,false ,"Bicycles"                   ),
    new SicCodeData("5941" ,"Sporting Goods Stores"                                       ,false  ,false ,"Sporting Goods"             ),
    new SicCodeData("5942" ,"Book Stores"                                                 ,false  ,false ,"Books"                      ),
    new SicCodeData("5943" ,"Stationery Stores, Office and School Supply Stores"          ,false  ,false ,"Stationary"                 ),
    new SicCodeData("5944" ,"Jewelry Stores, Watches, Clocks, and Siverware Stores"       ,true   ,false ,"Jewelry"                    ),
    new SicCodeData("5945" ,"Hobby, Toy and Game Shops"                                   ,false  ,false ,"Games"                      ),
    new SicCodeData("5946" ,"Camera and Photographic Supply Stores"                       ,false  ,false ,"Cameras"                    ),
    new SicCodeData("5947" ,"Gift, Card, Novelty and Souvenir Shops"                      ,false  ,false ,"Retail Products"            ),
    new SicCodeData("5948" ,"Luggage and Leather Goods Stores"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5949" ,"Sewing, Needlework, Fabric and Piece Goods Stores"           ,false  ,false ,"Sewing Supplies"            ),
    new SicCodeData("5950" ,"Glassware/Crystal Stores"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5960" ,"Direct Marketing - Insurance Services"                       ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5962" ,"Direct Marketing -  Travel-Related Arrangement Services   A" ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5963" ,"Door-to-Door Sales     Amex Excluded"                        ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5964" ,"Direct Marketing - Catalog Merchant"                         ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5965" ,"Direct Marketing - Combination Catalog and Retail Merchant"  ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5966" ,"Direct Marketing - Outbound Telemarketing Merchant"          ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5967" ,"Direct Marketing - Inbound Teleservices Merchant"            ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5968" ,"Direct Marketing - Continuity/Subscription Merchant"         ,true   ,false ,"Installment Payment"        ),
    new SicCodeData("5969" ,"Direct Marketing - Other Direct Marketers (Not Elsewhere Cl" ,true   ,false ,"Promotional Items"          ),
    new SicCodeData("5970" ,"Artist's Supply and Craft Shops"                             ,false  ,false ,"Art Supplies"               ),
    new SicCodeData("5971" ,"Art Dealers and Galleries"                                   ,false  ,false ,"Art"                        ),
    new SicCodeData("5972" ,"Stamp and Coin Stores"                                       ,false  ,false ,"Stamps/Coins"               ),
    new SicCodeData("5973" ,"Religious Goods Stores"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5975" ,"Hearing Aids-Sales, Service, and Supplies"                   ,false  ,false ,"Hearing Aids"               ),
    new SicCodeData("5976" ,"Orthopedic Goods - Prosthetic Devices"                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5977" ,"Cosmetic Stores"                                             ,false  ,false ,"Cosmetics"                  ),
    new SicCodeData("5978" ,"Typewriter Stores - Sales, Rentals, Service"                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5983" ,"Fuel Dealers-Fuel Oil, Wood, coal, and Liquefied Petroleum"  ,false  ,false ,"Fuel"                       ),
    new SicCodeData("5992" ,"Florists"                                                    ,false  ,false ,"Flowers"                    ),
    new SicCodeData("5993" ,"Cigar Stores and Stands"                                     ,false  ,false ,"Cigars"                     ),
    new SicCodeData("5994" ,"News Dealers and Newsstands"                                 ,false  ,false ,"News"                       ),
    new SicCodeData("5995" ,"Pet Shops, Pet Foods and Supplies Stores"                    ,false  ,false ,"Pet Supplies"               ),
    new SicCodeData("5996" ,"Swimming Pools - Sales and Service"                          ,false  ,false ,"Pool Supplies"              ),
    new SicCodeData("5997" ,"Electric Razor Stores - Sales and Service"                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("5998" ,"Tent and Awning Shops"                                       ,false  ,false ,"Tent/Awning Supplies"       ),
    new SicCodeData("5999" ,"Miscellaneous and Specialty Retail Stores"                   ,false  ,false ,"Boutique Goods"             ),
    new SicCodeData("6010" ,"Financial Institutions - Manual Cash Disbursements"          ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("6011" ,"Financial Institutions - Automated Cash Disbursements"       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("6012" ,"Financial Institutions - Merchandise and Services"           ,false  ,false ,"Bank Products"              ),
    new SicCodeData("6051" ,"Non-Financial Institutions-Foreign Currency, Money Orders (" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("6211" ,"Security Brokers/Dealers    Amex Excluded"                   ,false  ,false ,"Securities"                 ),
    new SicCodeData("6300" ,"Insurance Sales, Underwriting, and Premiums    Amex Exclude" ,false  ,false ,"Insurance Policy"           ),
    new SicCodeData("6381" ,"Insurance Premiums (No longer valid for first presentments)" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("6399" ,"Insurance (Not Elsewhere Classified) (No longer valid for f" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("6513" ,"REAL ESTATE AGENTS, MANAGERS, RENTALS"                       ,false  ,false ,"Real Estate Payment"        ),
    new SicCodeData("6540" ,"Stored value card/purchase/load(non-financial institutions)" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7011" ,"Lodging - Hotels, Motels, and Resorts, Central Reservation"  ,false  ,false ,"Lodging"                    ),
    new SicCodeData("7012" ,"Timeshares    Amex Excluded"                                 ,true   ,false ,"Timeshare"                  ),
    new SicCodeData("7032" ,"Sporting and Recreational Camps"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7033" ,"Trailer Parks and Campgrounds"                               ,false  ,false ,"Park Services"              ),
    new SicCodeData("7210" ,"Laundry, Cleaning, and Garment Services"                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7211" ,"Laundries - Family and Commercial"                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7216" ,"Dry Cleaners"                                                ,false  ,false ,"Dry Cleaning Services"      ),
    new SicCodeData("7217" ,"Carpet and Upholstery Cleaning"                              ,false  ,false ,"Upholstery Cleaning"        ),
    new SicCodeData("7221" ,"Photographic Studios"                                        ,false  ,false ,"Photography Services"       ),
    new SicCodeData("7230" ,"Beauty and Barber Shops"                                     ,false  ,true ,"Beauty Shop Services"       ),
    new SicCodeData("7251" ,"Shoe Repair Shops, Shoe Shine Parlors, and Hat Cleaning Sho" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7261" ,"Funeral Service and Crematories"                             ,false  ,false ,"Funeral Services"           ),
    new SicCodeData("7273" ,"Dating and Escort Services"                                  ,true   ,false ,"Dating Services"            ),
    new SicCodeData("7276" ,"Tax Preparation Services"                                    ,false  ,false ,"Tax Assistance Services"    ),
    new SicCodeData("7277" ,"Counseling Services-Debt, Marriage, and Personal"            ,false  ,false ,"Counseling Services"        ),
    new SicCodeData("7278" ,"Buying and Shopping Services and Clubs"                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7296" ,"Clothing Rental - Costumes, Uniforms, Formal Wear"           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7297" ,"Massage Parlors"                                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7298" ,"Health and Beauty Spas"                                      ,false  ,true ,"Spa Services"               ),
    new SicCodeData("7299" ,"Miscellaneous Personal Services-Not Elsewhere Classified"    ,false  ,false ,"Services"                   ),
    new SicCodeData("7311" ,"Advertising Services"                                        ,false  ,false ,"Advertising Services"       ),
    new SicCodeData("7321" ,"Consumer Credit Reporting Agencies"                          ,false  ,false ,"Reporting Agency Services"  ),
    new SicCodeData("7332" ,"Blueprinting and Photocopying Services (Not longer valid fo" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7333" ,"Commercial Photography, Art, and Graphics"                   ,false  ,false ,"Photography Services"       ),
    new SicCodeData("7338" ,"Quick Copy, Reproduction, and Blueprinting Services"         ,false  ,false ,"Copy Services"              ),
    new SicCodeData("7339" ,"Stenographic and Secretarial Services"                       ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7342" ,"Exterminating and Disinfecting Services"                     ,false  ,false ,"Exterminator Services"      ),
    new SicCodeData("7349" ,"Cleaning, Mainenance, and Janitorial Services"               ,false  ,false ,"Cleaning Services"          ),
    new SicCodeData("7361" ,"Employment Agencies and Temporary Help Services"             ,false  ,false ,"Employment Services"        ),
    new SicCodeData("7372" ,"Computer Programming, Data Processing, and Integrated Syste" ,false  ,false ,"Computer Programming"       ),
    new SicCodeData("7375" ,"Information Retrieval Services     Amex Excluded"            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7379" ,"Computer Maintenance, Repair and Services (Not Elsewhere Cl" ,false  ,false ,"Computer Repair"            ),
    new SicCodeData("7392" ,"Management, Consulting, and Public Relations Services"       ,false  ,false ,"Consulting Services"        ),
    new SicCodeData("7393" ,"Detective Agencies, Protective Agencies, and Security Servi" ,false  ,false ,"Investigatory Services"     ),
    new SicCodeData("7394" ,"Equipment, Tool, Furniture, and Appliance Rental and Leasin" ,false  ,false ,"Equipment Rentals"          ),
    new SicCodeData("7395" ,"Photofinishing Laboratories and Photo Developing"            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7399" ,"Business Services (Not Elsewhere Classified)"                ,false  ,false ,"Consulting Services"        ),
    new SicCodeData("7512" ,"Automobile Rental Agency    Amex Excluded"                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7513" ,"Truck and Utility Trailer Rentals   Amex Excluded"           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7519" ,"Motor Home and Recreational Vehicle Rentals"                 ,false  ,false ,"Recreation Equipment"       ),
    new SicCodeData("7523" ,"Parking Lots and Garages"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7531" ,"Automotive Body Repair Shops"                                ,false  ,false ,"Auto Repair"                ),
    new SicCodeData("7534" ,"Tire Retreading and Repair Shops"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7535" ,"Automotive Paint Shops"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7538" ,"Automotive Service Shops (Non-Dealer)"                       ,false  ,false ,"Auto Repair"                ),
    new SicCodeData("7542" ,"Car Washes"                                                  ,false  ,false ,"Car Wash"                   ),
    new SicCodeData("7549" ,"Towing Services"                                             ,false  ,false ,"Towing Services"            ),
    new SicCodeData("7622" ,"Electronic Repair Shops"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7623" ,"Air Conditioning and Refrigeration Repair Shops"             ,false  ,false ,"HVAC Services"              ),
    new SicCodeData("7629" ,"Electrical and Small Appliance Repair Shops"                 ,false  ,false ,"Appliance Repair"           ),
    new SicCodeData("7631" ,"Watch, Clock and Jewelry Repair"                             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7641" ,"Furniture - Reupholstery, Repair, Refinishing"               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7692" ,"Welding Services"                                            ,false  ,false ,"Welding Services"           ),
    new SicCodeData("7699" ,"Miscellaneous Repair Shops and Related Services"             ,false  ,false ,"Repair Services"            ),
    new SicCodeData("7829" ,"Motion Picture and Video Tape Production and Distribution"   ,false  ,false ,"Video Tape Production"      ),
    new SicCodeData("7832" ,"Motion Picture Theaters"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7841" ,"Video Tape Rental Stores"                                    ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7911" ,"Dance Halls, Studios and Schools"                            ,false  ,false ,"Dance Education"            ),
    new SicCodeData("7922" ,"Theatrical Producers (except Motion Pictures) and Ticket Ag" ,true   ,false ,"Theatre Tickets"            ),
    new SicCodeData("7929" ,"Bands, Orchestras and Miscellaneous Entertainers-Not Elsewh" ,false  ,false ,"Entertainment"              ),
    new SicCodeData("7932" ,"Billiard and Pool Establishments"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7933" ,"Bowling Alleys"                                              ,false  ,false ,"Bowling"                    ),
    new SicCodeData("7941" ,"Commercial Sports, Professional Sports Clubs, Athletic Fiel" ,false  ,false ,"Membership Payment"         ),
    new SicCodeData("7991" ,"Tourist Attractions and Exhibits"                            ,false  ,false ,"Attraction Tickets"         ),
    new SicCodeData("7992" ,"Public Golf Courses"                                         ,false  ,false ,"Golf Fees"                  ),
    new SicCodeData("7993" ,"Video Amusement Game Supplies"                               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7994" ,"Video Game Arcades/Establishments"                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7995" ,"Betting, including Lottery Tickets, Casino Gaming Chips, Of" ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7996" ,"Amusement Parks, Circuses, Carnivals, and Fortune Tellers"   ,false  ,false ,"Amusement"                  ),
    new SicCodeData("7997" ,"Membership Clubs (Sports, Recreation, Athletic), Country Cl" ,false  ,false ,"Membership Payment"         ),
    new SicCodeData("7998" ,"Aquariums"                                                   ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("7999" ,"Recreation Services-Not Elsewhere Classified"                ,false  ,false ,"Recreation Services"        ),
    new SicCodeData("8011" ,"Doctors and Physicians-Not Elsewhere Classified"             ,false  ,false ,"Physician Services"         ),
    new SicCodeData("8021" ,"Dentists and Orthodontists"                                  ,false  ,false ,"Dental Services"            ),
    new SicCodeData("8031" ,"Osteopaths"                                                  ,false  ,false ,"Osteopath Services"         ),
    new SicCodeData("8041" ,"Chiropractors"                                               ,false  ,false ,"Chiropractic Services"      ),
    new SicCodeData("8042" ,"Optometrists and Ophthalmologists"                           ,false  ,false ,"Eye Physician Services"     ),
    new SicCodeData("8043" ,"Opticians, Optical Goods and Eyeglasses"                     ,false  ,false ,"Optical Services"           ),
    new SicCodeData("8044" ,"Opticians, Optical Goods, and Eyeglasses (No longer valid f" ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8049" ,"Podiatrists and Chiropodists"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8050" ,"Nursing and Personal Care Facilites"                         ,false  ,false ,"Nursing Services"           ),
    new SicCodeData("8062" ,"Hospitals"                                                   ,false  ,false ,"Hospital Services"          ),
    new SicCodeData("8071" ,"Medical and Dental Laboratories"                             ,false  ,false ,"Lab Testing Services"       ),
    new SicCodeData("8099" ,"Medical Services and Health Practitioners-Not Elsewhere Cla" ,false  ,false ,"Medical Services"           ),
    new SicCodeData("8111" ,"Legal Services and Attorneys"                                ,false  ,false ,"Legal Services"             ),
    new SicCodeData("8211" ,"Elementary and Secondary Schools"                            ,false  ,false ,"Educational Services"       ),
    new SicCodeData("8220" ,"Colleges, Universities, Professional Schools, and Junior Co" ,false  ,false ,"Educational Services"       ),
    new SicCodeData("8241" ,"Correspondence Schools"                                      ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8244" ,"Business and Secretarial Schools"                            ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8249" ,"Vocational and Trade Schools"                                ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8299" ,"Schools and Educational Services (not Elsewhere Classified)" ,false  ,false ,"Educational Services"       ),
    new SicCodeData("8351" ,"Child Care Services"                                         ,false  ,false ,"Child Care"                 ),
    new SicCodeData("8398" ,"Charitable and Social Service Organizations"                 ,false  ,false ,"Donations"                  ),
    new SicCodeData("8641" ,"Civic, Social, and Fraternal Associations"                   ,false  ,false ,"Membership Payment"         ),
    new SicCodeData("8651" ,"Political Organizations"                                     ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8661" ,"Religious Organizations"                                     ,false  ,false ,"Religious Contributions"    ),
    new SicCodeData("8675" ,"Automobile Associations"                                     ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("8699" ,"Membership Organizations (Not Elsewhere Classified)"         ,false  ,false ,"Membership Payment"         ),
    new SicCodeData("8734" ,"Testing Laboratories (Non-Medical Testing)"                  ,false  ,false ,"Testing Services"           ),
    new SicCodeData("8911" ,"Architectural, Engineering, and Surveying Services"          ,false  ,false ,"Engineering Services"       ),
    new SicCodeData("8931" ,"Accounting, Auditing, and Bookkeeping Services"              ,false  ,false ,"Accounting Services"        ),
    new SicCodeData("8999" ,"Professional Services (Not Elsewhere Classified)"            ,false  ,false ,"Professional Services"      ),
    new SicCodeData("9211" ,"Court Costs, Including Alimony and Child Support"            ,true   ,false ,"Court Fees"                 ),
    new SicCodeData("9222" ,"Fines"                                                       ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9223" ,"Bail and Bond Payments"                                      ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9311" ,"Tax Payments"                                                ,true   ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9399" ,"Government Services (Not Elsewhere Classified)"              ,false  ,false ,"Government Payments"        ),
    new SicCodeData("9402" ,"Postal Services - Government Only"                           ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9405" ,"U.S. Federal Government Agencies or Departments"             ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9700" ,"Automated Referal Service (FOR VISA USE ONLY)"               ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9701" ,"Visa Credential Service (FOR VISA USE ONY)"                  ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9702" ,"GCAS Emergency Services (FOR VISA USE ONLY)"                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
    new SicCodeData("9950" ,"Intra-Company Purchases (FOR VISA USE ONLY)"                 ,false  ,false ,"Miscellaneous Goods/Svcs"   ),
  };
  
  // singleton
  private static SicCodes   TheSicCodes   = null;
  
  // instance variables
  private   long        LoadTimeMillis    = 0L;
  private   HashMap     SicCodes          = new HashMap();
  
  private SicCodes()
  {
    loadDataDefault();
  }
  
  public boolean areTipsAllowed( String sicCode )
  {
    boolean       retVal      = false;
    SicCodeData   sicData     = (SicCodeData)SicCodes.get(sicCode);
    
    if ( sicData != null )
    {
      retVal = sicData.getTipsAllowed();
    }
    return( retVal );
  }

  public static SicCodes getInstance()
  {
    if ( TheSicCodes == null )
    {
      TheSicCodes = new SicCodes();
    }
    TheSicCodes.refresh();
    return( TheSicCodes );
  }
  
  public String getMerchandiseDesc( String sicCode )
  {
    String        retVal      = "Miscellaneous Goods/Svcs";
    SicCodeData   sicData     = (SicCodeData)SicCodes.get(sicCode);
    
    if ( sicData != null )
    {
      retVal = sicData.getMerchandiseDesc();
    }
    return( retVal );
  }
  
  public String getMerchantType( String sicCode )
  {
    String        retVal      = "SIC Code: " + sicCode;
    SicCodeData   sicData     = (SicCodeData)SicCodes.get(sicCode);
    
    if ( sicData != null )
    {
      retVal = sicData.getMerchantType();
    }
    return( retVal );
  }
  
  public boolean isHighRisk( String sicCode )
  {
    boolean       retVal      = false;
    SicCodeData   sicData     = (SicCodeData)SicCodes.get(sicCode);
    
    if ( sicData != null )
    {
      retVal = sicData.getHighRisk();
    }
    return( retVal );
  }
  
  protected synchronized void load( )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:918^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code                                          as sic_code,
//                  merchant_type                                     as merchant_type,
//                  nvl(high_risk,'N')                                as high_risk,
//                  nvl(tips_allowed,'N')                             as tips_allowed,
//                  nvl(merchandise_desc,'Miscellaneous Goods/Svcs')  as merchandise_desc 
//          from    sic_codes   sc
//          order by sic_code      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code                                          as sic_code,\n                merchant_type                                     as merchant_type,\n                nvl(high_risk,'N')                                as high_risk,\n                nvl(tips_allowed,'N')                             as tips_allowed,\n                nvl(merchandise_desc,'Miscellaneous Goods/Svcs')  as merchandise_desc \n        from    sic_codes   sc\n        order by sic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.SicCodes",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.SicCodes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:927^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        SicCodes.clear();
        do
        {
          SicCodes.put( resultSet.getString("sic_code"),new SicCodeData(resultSet) );  
        }
        while( resultSet.next() );
      }
      resultSet.close();
      
      LoadTimeMillis = System.currentTimeMillis();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadDataDefault()
  {
    SicCodes.clear();
    
    for( int i = 0; i < SicCodeDefaults.length; ++i )
    {
      SicCodes.put( SicCodeDefaults[i].getSicCode(),SicCodeDefaults[i] );  
    }
  }
  
  // synchronize the refresh method so two requests (threads)
  // do not trigger a back-to-back reload of the cached data
  protected synchronized void refresh()
  {
    if ( LoadTimeMillis == 0L )   // only load once at startup
    {
      load();
    }
  }
}/*@lineinfo:generated-code*/