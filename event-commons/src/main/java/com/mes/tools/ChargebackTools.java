/*@lineinfo:filename=ChargebackTools*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/ChargebackTools.sqlj $

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ref.DefaultContext;

public class ChargebackTools
{
  static { DbProperties.requireConfiguration(); } // initialize database properties
  
  public static int getPendingCBActivity(DefaultContext ctx, long itemId)
    throws java.sql.SQLException
  {
    int recCount = 0;

    /*@lineinfo:generated-code*//*@lineinfo:41^5*/

//  ************************************************************
//  #sql [ctx] { select  count(cba.cb_load_sec) 
//        from    network_chargeback_activity   cba
//        where   cba.cb_load_sec = :itemId and
//                cba.load_filename is null
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cba.cb_load_sec)  \n      from    network_chargeback_activity   cba\n      where   cba.cb_load_sec =  :1   and\n              cba.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.ChargebackTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:47^5*/

    return( recCount );
  }

  public static void deletePendingCBActivity(DefaultContext ctx, long itemId)
    throws java.sql.SQLException
  {
    /*@lineinfo:generated-code*//*@lineinfo:55^5*/

//  ************************************************************
//  #sql [ctx] { delete
//        from    network_chargeback_activity cba
//        where   cba.cb_load_sec = :itemId and
//                cba.load_filename is null and
//                exists(
//                       select qd.id from q_data qd 
//                       where cba.cb_load_sec = qd.id and
//                       qd.item_type = 18 and
//                       qd.type != :MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL
//                       )
//                
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n      from    network_chargeback_activity cba\n      where   cba.cb_load_sec =  :1   and\n              cba.load_filename is null and\n              exists(\n                     select qd.id from q_data qd \n                     where cba.cb_load_sec = qd.id and\n                     qd.item_type = 18 and\n                     qd.type !=  :2  \n                     )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.ChargebackTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^5*/
  }
  
  public static boolean pendChargeback( long loadSec )
  {
    DefaultContext          ctx         = null;
    SQLJConnectionBase      mesdb       = null;
    int                     recCount    = 0;
    boolean                 retVal      = false;
    int                     toQueue     = MesQueues.Q_CHARGEBACKS_ALL;
  
    try
    {
      mesdb = new SQLJConnectionBase(SQLJConnectionBase.getDirectConnectString());
      mesdb.connect();             // connect to db
      mesdb.setAutoCommit(false);  // disable auto-commit
      ctx = mesdb.getContext();    // store a ref to the context
      retVal = pendChargeback(ctx,loadSec);
    }
    catch( Exception e )
    {
      try{mesdb.logEntry( "ChargebackTools::pendChargeback()", e.toString());}catch(Exception ee){}
    }      
    finally
    {
      try{ mesdb.cleanUp(); }catch(Exception ee){}
    }
    return( retVal );
  }

  public static boolean pendChargeback( DefaultContext ctx, long loadSec )
    throws java.sql.SQLException
  {
    int                     recCount    = 0;
    boolean                 retVal      = false;
    int                     toQueue     = MesQueues.Q_CHARGEBACKS_ALL;
  
    // look for a worked chargeback that
    // has not been sent to TSYS (null filename)
    recCount = getPendingCBActivity(ctx,loadSec);

    // has not been settled, erase the pending item and
    // move the items back to the appropriate pending queue
    if ( recCount > 0 )
    {
      // removing the item waiting to be sent
      deletePendingCBActivity(ctx,loadSec);

      // get the queue assignment for this incoming item
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [ctx] { -- get_cb_queue_assignment(bankNumber, merchantId, cardNum, reasonCode, usageCode, actionCode );
//          select  nvl( get_cb_queue_assignment( cb.bank_number,
//                                                cb.merchant_number,
//                                                cb.card_number,
//                                                cb.reason_code,
//                                                decode(cb.first_time_chargeback,'Y',1,3),
//                                                null ), :MesQueues.Q_CHARGEBACKS_ALL )
//          
//          from    network_chargebacks   cb
//          where   cb.cb_load_sec = :loadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- get_cb_queue_assignment(bankNumber, merchantId, cardNum, reasonCode, usageCode, actionCode );\n        select  nvl( get_cb_queue_assignment( cb.bank_number,\n                                              cb.merchant_number,\n                                              cb.card_number,\n                                              cb.reason_code,\n                                              decode(cb.first_time_chargeback,'Y',1,3),\n                                              null ),  :1   )\n         \n        from    network_chargebacks   cb\n        where   cb.cb_load_sec =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.ChargebackTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_CHARGEBACKS_ALL);
   __sJT_st.setLong(2,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   toQueue = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/

      // update with the new queue id
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [ctx] { update  q_data qd
//          set     qd.type = :toQueue
//          where   qd.item_type = 18 and -- chargebacks
//                  qd.type != :MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL and
//                  qd.id = :loadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data qd\n        set     qd.type =  :1  \n        where   qd.item_type = 18 and -- chargebacks\n                qd.type !=  :2   and\n                qd.id =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.ChargebackTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,toQueue);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL);
   __sJT_st.setLong(3,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [ctx] { commit
//         };
//  ************************************************************

  ((ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : ctx.getExecutionContext().getOracleContext()).oracleCommit(ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:144^7*/
      
      retVal = true;
    }
    return( retVal );
  }
  
  public static long getNewLoadSec( )
  {
    SQLJConnectionBase      ftpdb       = null;
    DefaultContext          ftpCtx      = null;
    long                    id          = 0L;
  
    try
    {
      /*
       * NOTE: prior to dc move this method connected to the mes ftpdb schema,
       * after the DC move this was changed to the default ftpdb schema in
       * db.properties which was mesload.  After dc move:
       *
       *   ftpdb = new SQLJConnectionBase(DbProperties.ftpdbName());
       *
       * Business web platform is still using mes schema to generate ids 
       * for web functions.  We are going to continue using mesload schema 
       * here, but this is an outstanding issue that needs to be addressed 
       * in coordination with web platform developers.  The mesload sequence 
       * was bumped up to a much higher number around DC move so likelhhood 
       * of duplicate ids in system is minimal but possible.
       *
       * Background: ftpdb has two schemas, mes and mesload, both of which
       * have duplicate tables including charge_back_pk.  This has lead to problems
       * where different business systems are grabbing ids from different schemas
       * with the possibility of duplicate ids being generated in the system.
       *
       * This code affects EP and TE servers.  We are explicitly specifying the
       * mesload schema connection here as the DbProperties generic ftpdb name 
       * incorrectly assumed there was only one valid schema in ftpdb.
       */
      ftpdb = new SQLJConnectionBase(MesDefaults.getString(MesDefaults.CHARGEBACK_TOOLS_FTPDB));
      ftpdb.connect();                // connect to ftp db
      ftpdb.setAutoCommit(false);     // disable auto-commit
      ftpCtx = ftpdb.getContext();    // store a ref to the context

      /*@lineinfo:generated-code*//*@lineinfo:187^7*/

//  ************************************************************
//  #sql [ftpCtx] { select  pk.sec 
//          
//          from    charge_back_pk pk
//          for update
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ftpCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pk.sec \n         \n        from    charge_back_pk pk\n        for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.ChargebackTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^7*/
  
      id++;   // increment to the next value
  
      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [ftpCtx] { update charge_back_pk
//          set sec = :id, DT = sysdate, load_filename = 'system'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ftpCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update charge_back_pk\n        set sec =  :1  , DT = sysdate, load_filename = 'system'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.tools.ChargebackTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^7*/
  
      /*@lineinfo:generated-code*//*@lineinfo:203^7*/

//  ************************************************************
//  #sql [ftpCtx] { commit
//         };
//  ************************************************************

  ((ftpCtx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : ftpCtx.getExecutionContext().getOracleContext()).oracleCommit(ftpCtx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^7*/
    }
    catch( Exception e )
    {
      id = -1L;
      try{ftpdb.logEntry( "ChargebackTools::getNewLoadSec()", e.toString());}catch(Exception ee){}
    }      
    finally
    {
      try{ ftpdb.cleanUp(); }catch(Exception ee){}
    }
    return(id);
  }
  
  public static void reverseChargeback(DefaultContext ctx, String ct, long loadSec, java.sql.Date reversalDate, double reversalAmount)
			throws java.sql.SQLException {

		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[reverseChargeback()] - reversing chargeback loadSec=" + loadSec + " on " + reversalDate);
		}
		reverseCardbrandChargeback(ctx, ct, loadSec, reversalDate, reversalAmount);

		updateChargebackForReversal(ctx, loadSec, reversalDate, reversalAmount);

		Integer rowCount = createMerchantCredit(ctx, loadSec);

		if (rowCount == 0) {
			createChargebackActivity(ctx, loadSec, "K","Reversal - No Credit");
		}
		updateQueue(ctx, MesQueues.Q_CHARGEBACKS_COMPLETED_REVERSAL, MesQueues.Q_ITEM_TYPE_CHARGEBACK, loadSec);

	}

	private static void createChargebackActivity(DefaultContext ctx, long loadSec, String actionCode, String userMsg) throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[createChargebackActivity()] - create cb activity "+userMsg+" for localSec=" + loadSec);
		}
		// @formatter:off
		String sqlString = "insert into network_chargeback_activity "
				+ " (cb_load_sec, file_load_sec, action_code, action_date, "
				+ "  user_message, user_login, action_source, load_filename) "
				+ "select "
				+ "  cb_load_sec, "
				+ "  1 as file_load_sec, "
				+ "  :1 as action_code, "
				+ "  trunc(sysdate) as action_date,"
				+ "  :2 as user_message, "
				+ "  :3 as user_login, "
				+ "  7 as action_source,"
				+ "  load_filename "
				+ "from "
				+ "  network_chargebacks cb "
				+ "where "
				+ "  cb.cb_load_sec=:4";
		// @formatter:on
		Integer updateCount = executeUpdate(ctx,"11com.mes.tools.ChargebackTools", sqlString, actionCode, userMsg, "system",loadSec);
		if (log.isDebugEnabled()) {
			log.debug("[createChargebackActivity()] - cb activity created " + (updateCount > 0 ? "successful" : "failed") + " for loadSec=" + loadSec);
		}
	}

	private static Integer createMerchantCredit(DefaultContext ctx, long loadSec) throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[createMerchantCredit()] - creating cb reversal credit entry for localSec=" + loadSec);
		}
		//@formatter:off
		String theSqlTS = "insert into network_chargeback_activity\n"
				+ " (cb_load_sec, file_load_sec, action_code, action_date, user_message, user_login, action_source) " 
				+ " select  "
				+ "    cba.cb_load_sec           as load_sec," 
				+ "    max(cba.file_load_sec)+1  as file_load_sec,"
				+ "    'J'                       as action_code," 
				+ "    trunc(sysdate)            as action_date,"
				+ "    'Reversal'                as user_message," 
				+ "    'system'                  as user_login,"
				+ "    7                         as action_source" 
				+ " from    " 
				+ "     network_chargeback_activity     cba,"
				+ "     chargeback_action_codes         ac" 
				+ " where   " + "     cba.cb_load_sec=:1 " 
				+ "     and ac.short_action_code = cba.action_code"
				+ "     and ac.adj_tran_code = 9071  " 
				+ "     and not exists (" + "       select cbai.cb_load_sec " 
				+ "       from "
				+ "         network_chargeback_activity   cbai," 
				+ "         chargeback_action_codes       aci" 
				+ "       where   "
				+ "         cbai.cb_load_sec = cba.cb_load_sec" 
				+ "         and aci.short_action_code = cbai.action_code"
				+ "         and aci.adj_tran_code = 9077)" + " group by cba.cb_load_sec";
		//@formatter:on
		return executeUpdate(ctx, "9com.mes.tools.ChargebackTools", theSqlTS, loadSec);
	}

	private static void reverseCardbrandChargeback(DefaultContext ctx, String ct, long loadSec, java.sql.Date reversalDate, double reversalAmount)
			throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[reverseCardbrandChargeback()] - reversing chargeback loadSec=" + loadSec + " on " + reversalDate);
		}
		String tableName = getChargebackTableName(ct);
		if (StringUtils.isBlank(tableName)) {
			return;
		}

		// mark this entry as reversed in the issuer specific chargeback tables
		String chargebackUpdateSql = String.format("update %s set reversal_date = nvl(? ,trunc(sysdate)), reversal_amount=?  where load_sec=?", tableName);
		Integer updateCount =
				executeUpdate(ctx, "6com.mes.tools.ChargebackTools:" + chargebackUpdateSql, chargebackUpdateSql, reversalDate, reversalAmount, loadSec);

		if (log.isDebugEnabled()) {
			log.debug("[reverseCardbrandChargeback()] - " + tableName + " chargeback updated " + (updateCount > 0 ? "successful" : "failed") + " for loadSec="
					+ loadSec);
		}

		// In case of Amex run updates on OPTB table too
		if ("AM".equals(ct)) {
			if (log.isDebugEnabled()) {
				log.debug("[reverseCardbrandChargeback()] - reversing amex optBlue chargeback loadSec=" + loadSec + " on " + reversalDate);
			}
			String theSqlTS = "update network_chargeback_amex_optb set reversal_date=nvl( :1, trunc(sysdate)), reversal_amount=:2 where load_sec=:3 ";
			updateCount = executeUpdate(ctx, "7com.mes.tools.ChargebackTools", theSqlTS, reversalDate, reversalAmount, loadSec);

			if (log.isDebugEnabled()) {
				log.debug("[reverseCardbrandChargeback()] - optBlue chargeback updated " + (updateCount > 0 ? "successful" : "failed") + " for loadSec="
						+ loadSec);
			}
		}
	}

	private static void updateChargebackForReversal(DefaultContext ctx, long loadSec, java.sql.Date reversalDate, double reversalAmount)
			throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[updateChargebackForReversal()] - loadSec=" + loadSec + ", reversalDate=" + reversalDate + ", reversalAmount=" + reversalAmount);
		}

		String theSqlTS = "update network_chargebacks set reversal_date = nvl( :1  ,trunc(sysdate)), reversal_amount =:2  where cb_load_sec = :3 ";
		Integer updateCount = executeUpdate(ctx, "8com.mes.tools.ChargebackTools", theSqlTS, reversalDate, reversalAmount, loadSec);

		if (log.isDebugEnabled()) {
			log.debug("[updateChargebackForReversal()] - chargeback updated " + (updateCount > 0 ? "successful" : "failed") + " for loadSec=" + loadSec);
		}
	}
	
	private static void updateQueue(DefaultContext ctx, Integer queueType, Integer itemType, Long loadSec) throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[updateQueue()] - queueType=" + queueType + ", itemType=" + itemType + ", localSec=" + loadSec);
		}

		String queueUpdateSql = "update q_data set type=:1, last_changed=sysdate, last_user='system',is_rush=0 where id=:2 and item_type=:3 ";
		Integer updateCount = executeUpdate(ctx, "10com.mes.tools.ChargebackTools", queueUpdateSql, queueType, loadSec, itemType);

		if (log.isDebugEnabled()) {
			log.debug("[updateQueue()] - queue updated " + (updateCount > 0 ? "successful" : "failed") + " for loadSec=" + loadSec);
		}
	}

	private static String getChargebackTableName(String cardType) {
		String tableName = "";
		if ("VS".equals(cardType)) {
			tableName = "network_chargeback_visa";
		}
		else if ("MC".equals(cardType)) {
			tableName = "network_chargeback_mc";
		}
		else if ("AM".equals(cardType)) {
			tableName = "network_chargeback_amex";
		}
		else if ("DS".equals(cardType)) {
			tableName = "network_chargeback_discover";
		}
		return tableName;
	}

	private static Integer executeUpdate(DefaultContext defaultContext, String key, String updateSql, Object... params) throws java.sql.SQLException {
		Logger log = Logger.getLogger(ChargebackTools.class);
		if (log.isDebugEnabled()) {
			log.debug("[executeUpdate()] - key=" + key + ", sql=" + updateSql);
		}
		if (defaultContext == null) {
			sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
		}

		sqlj.runtime.ExecutionContext.OracleContext oracleContext = ((defaultContext.getExecutionContext() == null)
				? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : defaultContext.getExecutionContext().getOracleContext());
		try {
			oracle.jdbc.OraclePreparedStatement statement = oracleContext.prepareOracleBatchableStatement(defaultContext, key, updateSql);
			// set IN parameters
			int paramIndex = 0;
			for (Object param : params) {
				paramIndex++;
				if (param instanceof java.util.Date) {
					statement.setDate(paramIndex, new java.sql.Date(((java.util.Date) param).getTime()));
				}
				else if (param instanceof java.sql.Date) {
					statement.setDate(paramIndex, (java.sql.Date) param);
				}
				else if (param instanceof Integer) {
					statement.setInt(paramIndex, (Integer) param);
				}
				else if (param instanceof Long) {
					statement.setLong(paramIndex, (Long) param);
				}
				else if (param instanceof Double) {
					statement.setDouble(paramIndex, (Double) param);
				}
				else if (param instanceof String) {
					statement.setString(paramIndex, (String) param);
				}
			}
			// execute statement
			oracleContext.oracleExecuteBatchableUpdate();
			return statement.getUpdateCount();
		}catch( java.sql.SQLException ex) {
			log.error("[executeUpdate()] - SQLException executing sqlKey="+key, ex);
			throw ex;
		}
	}
}