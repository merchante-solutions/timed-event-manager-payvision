/*@lineinfo:filename=ApiRequestLogEntry*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/ApiRequestLogEntry.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.Serializable;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.support.TridentTools;

public class ApiRequestLogEntry
  implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  private static final long       MAX_WAIT_TIME       = 1000 * 60 * 5;  // 5 minutes
  
  private   String      CurrencyCode        = TridentApiConstants.FV_CURRENCY_CODE_USD;
  private   String      ClientIpAddress     = null;
  private   String      RequestMethod       = null;
  private   String      RequestParams       = null;
  private   long        RequestTimeMillis   = 0L;
  private   Timestamp   RequestTimestamp    = null;
  private   String      Response            = null;
  private   long        ResponseTimeMillis  = 0L;
  private   String      RetrievalRefNum     = null;
  private   String      RetryId             = TridentApiConstants.RETRY_ID_NONE;
  private   String      ServerName          = "Unknown";
  private   long        SwitchDuration      = 0L;
  private   String      TranType            = null;
  
  public ApiRequestLogEntry( String requestParams, String serverName, String requestMethod, String ipAddress )
  {
    StringBuffer  buffer    = new StringBuffer(requestParams);
    String        tempStr   = null;
    int           endIndex  = 0;
    int           index     = 0;
    
    ClientIpAddress   = ipAddress;
    RequestTimestamp  = new Timestamp( Calendar.getInstance().getTime().getTime() );
    RequestTimeMillis = System.currentTimeMillis();
    RequestMethod     = requestMethod;
    
    if ( (index = buffer.indexOf(TridentApiConstants.FN_CARD_NUMBER)) >= 0 )
    {
      index += TridentApiConstants.FN_CARD_NUMBER.length()+1;   // move to the first digit
      endIndex = index;
      
      while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      if ( (endIndex - index) > 10 )
      {
        tempStr = TridentTools.encodeCardNumber(buffer.substring(index,endIndex));
      }
      else if ( (endIndex - index) > 0 )
      {
        tempStr = buffer.substring(index,endIndex);
      }
      buffer.replace( index, endIndex, tempStr );
    }
    
    if ( (index = buffer.indexOf(TridentApiConstants.FN_CARD_SWIPE)) >= 0 )
    {
      index += TridentApiConstants.FN_CARD_SWIPE.length()+1;   // move to the first character
      endIndex = index;
      
      while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      // only display the portion of the card swipe (truncated) used
      // in the authorization request.  all other track data is discarded
      try
      {
        String cardSwipeRaw = URLDecoder.decode(buffer.substring(index,endIndex),"UTF-8");
        String cardSwipeTrack = TridentTools.decodeCardSwipeTrackData(cardSwipeRaw);
        buffer.replace( index, endIndex, TridentTools.encodeCardSwipe(cardSwipeTrack) );
      }
      catch( Exception e )
      {
        buffer.replace( index, endIndex, "invalid-track-data-found" );
      }        
    }
    
    if ( (index = buffer.indexOf(TridentApiConstants.FN_CVV2)) >= 0 )
    {
      index += TridentApiConstants.FN_CVV2.length()+1;   // move to the first character
      endIndex = index;
      
      while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      buffer.replace( index, endIndex, "xxx" );
    }
    
    if ( (index = buffer.indexOf(TridentApiConstants.FN_AN_CARD_NUMBER)) >= 0 )
    {
      index += TridentApiConstants.FN_AN_CARD_NUMBER.length()+1;   // move to the first character
      endIndex = index;
      
      while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      
      if ( (endIndex - index) > 10 )
      {
        tempStr = TridentTools.encodeCardNumber(buffer.substring(index,endIndex));
      }
      else if ( (endIndex - index) > 0 )
      {
        tempStr = buffer.substring(index,endIndex);
      }
      buffer.replace( index, endIndex, tempStr );
    }
    
    if ( (index = buffer.indexOf(TridentApiConstants.FN_AN_CVV2)) >= 0 )
    {
      index += TridentApiConstants.FN_AN_CVV2.length()+1;   // move to the first character
      endIndex = index;
      
      while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      buffer.replace( index, endIndex, "xxx" );
    }
    
    // store the truncated request params and server name
    RequestParams     = buffer.toString();
    ServerName        = serverName;
    
    // extract fields that have their own colums
    setRetryId( extractRequestParam(TridentApiConstants.FN_RETRY_ID,TridentApiConstants.FLM_RETRY_ID) );
    setTranType( extractRequestParam(TridentApiConstants.FN_TRAN_TYPE,64) );
  }
  
  public String extractParam( String inputString, String name, int maxLength )
  {
    int     endIndex  = 0;
    int     index     = -1;
    String  retVal    = null;
    
    if ( inputString != null )
    {
      index = inputString.toLowerCase().indexOf(name);
    }
    
    if ( index >= 0 )
    {
      index += name.length() + 1;    // move to the first digit
      endIndex = index;
      
      while( endIndex < inputString.length() && inputString.charAt(endIndex) != '&' )
      {
        ++endIndex;
      }
      retVal = inputString.substring(index,endIndex);
      
      if ( maxLength > 0 && retVal.length() > maxLength )
      {
        retVal = retVal.substring(0,maxLength);
      }
    }
    return( retVal );
  }
  
  public String extractRequestParam( String name )
  {
    return( extractRequestParam( name, -1 ) );
  }
  
  public String extractRequestParam( String name, int maxLength )
  {
    return( extractParam( RequestParams, name, maxLength ) );
  }
  
  public String extractResponseParam( String name )
  {
    return( extractResponseParam( name, -1 ) );
  }
  
  public String extractResponseParam( String name, int maxLength )
  {
    return( extractParam( Response, name, maxLength ) );
  }
  
  public String     getClientIpAddress()    { return( ((ClientIpAddress == null) ? "" : ClientIpAddress) ); }
  public String     getCurrencyCode()       { return( CurrencyCode ); }
  public String     getRequestMethod()      { return( RequestMethod ); }
  public String     getRequestParams()      { return( RequestParams ); }
  public long       getRequestTimeMillis()  { return( RequestTimeMillis ); }
  public Timestamp  getRequestTimestamp()   { return( RequestTimestamp ); }
  public String     getResponse()           { return( Response ); }
  public long       getResponseTimeMillis() { return( ResponseTimeMillis ); }
  public String     getRetrievalRefNum()    { return( RetrievalRefNum ); }
  public String     getRetryId()            { return( RetryId ); }
  public String     getServerName()         { return( ServerName ); }
  public long       getSwitchDuration()     { return( SwitchDuration ); }
  public long       getTranDuration()       { return(ResponseTimeMillis - RequestTimeMillis); }
  public String     getTranType()           { return( TranType ); }
 
  public boolean isStoreReady()
  {
    long        now     = Calendar.getInstance().getTime().getTime();
    boolean     retVal  = false;
    
    if ( Response != null || 
         ((now - RequestTimestamp.getTime()) > MAX_WAIT_TIME) )
    {
      retVal = true;
    }         
    return( retVal );
  }
  
  public void setCurrencyCode( String currencyCode )
  {
    CurrencyCode = currencyCode;
    
    if ( CurrencyCode == null )
    {
      CurrencyCode = TridentApiConstants.FV_CURRENCY_CODE_USD;
    }
    else if ( CurrencyCode.length() > 3 )   // prevent column size exceptions
    {
      CurrencyCode = CurrencyCode.substring(0,3);  
    }
  }
  
  public void setProperties( TridentApiTransaction apiTran )
  {
    // set the timestamp to be logged for tracking purposes
    setSwitchDuration(apiTran.getSwitchDuration());
    setRetrievalRefNum(apiTran.getAuthRetrievalRefNum());
    setCurrencyCode(apiTran.getCurrencyCode());
  }
  
  public void setResponse( String resp )
  {
    Response           = resp;
    ResponseTimeMillis = System.currentTimeMillis();
  }
  
  public void setRetrievalRefNum( String retrievalRefNum )
  {
    RetrievalRefNum = retrievalRefNum;
    
    // prevent database column size exceptions
    if( RetrievalRefNum != null && RetrievalRefNum.length() > 12 )
    {
      RetrievalRefNum = RetrievalRefNum.substring(0,12);
    }
  }
  
  public void setRetryId( String retryId )
  {
    RetryId = retryId;
  }
  
  public void setSwitchDuration( long switchDuration )
  {
    SwitchDuration = switchDuration;
  }
  
  public void setTranType( String tranType )
  {
    TranType = tranType;
  }
  
  public void showData( )
  {
    String  profileId   = null;
  
    // check for auth.net emulation
    if ( extractRequestParam(TridentApiConstants.FN_AN_VERSION) != null )
    {
      profileId = extractRequestParam(TridentApiConstants.FN_AN_TID); 
    }
    else  // trident payment gateway request
    { 
      profileId = extractRequestParam(TridentApiConstants.FN_TID);
    }
  
    System.out.println("ProfileId         : " + profileId);
    System.out.println("RequestTimestamp  : " + RequestTimestamp);
    System.out.println("ClientIpAddress   : " + ClientIpAddress);
    System.out.println("RequestParams     : " + RequestParams);
    System.out.println("Response          : " + Response);
    System.out.println("ServerName        : " + ServerName);
    System.out.println("Retrieval Ref Num : " + RetrievalRefNum);
    System.out.println("RetryId           : " + RetryId);
    System.out.println("TranType          : " + TranType);
    System.out.println("TranDuration      : " + (ResponseTimeMillis - RequestTimeMillis));
    System.out.println("SwitchDuration    : " + SwitchDuration);
  }
}/*@lineinfo:generated-code*/