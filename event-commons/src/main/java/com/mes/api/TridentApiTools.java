/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/TridentApiTools.java $

  Description:
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-04-10 15:49:34 -0700 (Fri, 10 Apr 2009) $
  Version            : $Revision: 15978 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;

public class TridentApiTools
{
  private static final String   TranIdSignature = 
    "00000285050000020000001B935B2D4806BDF5380526BF372E6D91335C49B0D3F19FC93617159DE114E912F0";
  
  public static String generateDummyAuthCode()
  {
    String    retVal    = "";
    
    for(int i = 0; i < 5; i++) 
    {
      retVal += ""+((int) (Math.random() * 10));
    }
    return( "T"+retVal );
  }
  
  public static String generateTranId(Object requestObj)
  {
    Object  obj   = ((requestObj == null) ? new Object() : requestObj);
    UUID    uuid  = UUIDGenerator.getInstance().generateNameBasedUUID(null, ("" + TranIdSignature + String.valueOf(obj) + System.currentTimeMillis()) );
    return( uuid.toString().replaceAll("-", "") );
  }
}