/*************************************************************************
 * 
 * FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/ApiVisaLineItemDetailRecord.java $
 * 
 * Description:
 * 
 * 
 * Last Modified By : $Author: jduncan $
 * Last Modified Date : $Date: 2009-08-26 15:03:29 -0700 (Wed, 26 Aug 2009) $
 * Version : $Revision: 16428 $
 * 
 * Change History:
 * See SVN database
 * 
 * Copyright (C) 2009 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * 
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc. This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 * 
 **************************************************************************/
package com.mes.api;

import org.apache.commons.lang.StringUtils;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import dto.VisaLineItem;
import masthead.formats.visak.VisaLineItemDetailRecord;

public class ApiVisaLineItemDetailRecord extends VisaLineItemDetailRecord implements ApiLineItemDetailRecord {
	public ApiVisaLineItemDetailRecord() {
	}

	public ApiVisaLineItemDetailRecord(String rawData, Object bo) throws Exception {
		super();
		setRawLineItemData(rawData, bo);
	}

	public void setRawLineItemData(String rawData, Object bo) throws Exception {

		VisaLineItem visaBO = (VisaLineItem) bo;
		
		String[] items = null;

		if (StringUtils.isBlank(rawData))
			items = new String[11];
		else
			items = (rawData+" ").split(TridentApiConstants.LINE_ITEM_DETAIL_FS_REGEX);

		setItemCommodityCode(StringUtilities.leftJustify(StringUtilities.dataSwap(items[0], visaBO.getItemCommodityCode()), 12, ' '));
		setItemDescriptor(StringUtilities.leftJustify(StringUtilities.dataSwap(items[1], visaBO.getItemDescriptor()), 35, ' '));
		setProductCode(StringUtilities.leftJustify(StringUtilities.dataSwap(items[2], visaBO.getProductCode()), 12, ' '));
		setQuantity(StringUtilities.rightJustify(StringUtilities.dataSwap(items[3], visaBO.getQuantity()), 12, '0'));
		setUnitOfMeasure(StringUtilities.leftJustify(StringUtilities.dataSwap(items[4], visaBO.getUnitOfMeasure()), 12, ' '));

		setUnitCost(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[5], visaBO.getUnitCost())) * 100, "000000000000"));
		setVatTaxAmount(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[6], visaBO.getVatTaxAmount())) * 100, "000000000000"));
		setVatTaxRate(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[7], visaBO.getVatTaxRate())) * 100, "0000"));
		setDiscountPerLineItem(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[8], visaBO.getDiscountPerLine())) * 100, "000000000000"));
		setLineItemTotal(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[9], visaBO.getLineItemTotal())) * 100, "000000000000"));
		setDebitOrCreditIndicator(StringUtilities.dataSwap(items[10], visaBO.getDebitOrCreditIndicator()).substring(0, 1));
	}

	@Override
	public void setRawLineItemData(String rawData) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
