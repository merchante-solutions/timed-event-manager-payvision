/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/TridentApiTranBase.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.Currency;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.support.HttpHelper;
import com.mes.support.MesEncryption;
import com.mes.support.MesMath;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.CountryCodeConverter;
import com.mes.tools.CurrencyCodeConverter;
import masthead.util.ByteUtil;

public class TridentApiTranBase implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  private static final long serialVersionUID  = TridentApiConstants.ApiVersionId;

  // HTTP POST field names
  
  // fields in the response in the order the are presented to the user
  public static final String[] ResponseFields = 
  {
    TridentApiConstants.FN_TRAN_ID,
    TridentApiConstants.FN_ERROR_CODE,
    TridentApiConstants.FN_AUTH_RESP_TEXT,
    TridentApiConstants.FN_AVS_RESULT,
    TridentApiConstants.FN_CVV2_RESULT,
    TridentApiConstants.FN_AUTH_CODE,
    TridentApiConstants.FN_PRODUCT_LEVEL,
    TridentApiConstants.FN_COMMERCIAL_CARD,
    TridentApiConstants.FN_FX_RATE,
    TridentApiConstants.FN_FX_RATE_TABLE,
    TridentApiConstants.FN_FX_RATE_ID,
    TridentApiConstants.FN_FX_EXP_DATE,
    TridentApiConstants.FN_AMOUNT,
    TridentApiConstants.FN_CURRENCY_CODE,
    TridentApiConstants.FN_CURRENCY_CODE_ALPHA,
    TridentApiConstants.FN_COUNTRY_CODE,
    TridentApiConstants.FN_COUNTRY_NAME,
    TridentApiConstants.FN_MOTO_IND,
    TridentApiConstants.FN_3D_ENROLLED,
    TridentApiConstants.FN_3D_TRAN_ID,
    TridentApiConstants.FN_3D_ORDER_ID,
    TridentApiConstants.FN_3D_ACS_URL,
    TridentApiConstants.FN_3D_PAYLOAD,
    TridentApiConstants.FN_EXTENDED_AVS,
    TridentApiConstants.FN_ACCOUNT_BALANCE,
    TridentApiConstants.FN_PARTIAL_AUTH,
    TridentApiConstants.FN_FRAUD_RESULT,
    TridentApiConstants.FN_RESP_HASH,
    TridentApiConstants.FN_CARD_NUMBER_TRUNC,
  };
  
  public static final String    TT_INVALID            = "tapi-invalid-tran-type";
  public static final String    TT_CARD_VERIFY        = "A";
  public static final String    TT_BML_REQUEST        = "B";
  public static final String    TT_CREDIT             = "C";
  public static final String    TT_DEBIT              = "D";
  public static final String    TT_3D_ENROLL_CHECK    = "E";
  public static final String    TT_FX_CONVERT_AMOUNT  = "F";
  public static final String    TT_FX_GET_RATE        = "G";
  public static final String    TT_ACH_REQUEST        = "H";
  public static final String    TT_RESPONSE_INQUIRY   = "I";
  public static final String    TT_RE_AUTH            = "J";
  public static final String    TT_CURRENCY_LOOKUP    = "L";
  public static final String    TT_CASH_ADVANCE       = "M";
  public static final String    TT_FORCE              = "O";
  public static final String    TT_PRE_AUTH           = "P";
  public static final String    TT_AUTH_REVERSAL      = "R";
  public static final String    TT_SETTLE             = "S";
  public static final String    TT_STORE_CARD         = "T";
  public static final String    TT_REFUND             = "U";
  public static final String    TT_VOID               = "V";
  public static final String    TT_DELETE_CARD_ID     = "X";
  public static final String    TT_BATCH_CLOSE        = "Z";

  
  protected static final String[][] TranTypeDescriptions =
  {
    { TT_INVALID      , "Invalid" },
    { TT_PRE_AUTH     , "Pre Auth" },
    { TT_AUTH_REVERSAL, "Reversal" },
    { TT_DEBIT        , "Sale" },
    { TT_CREDIT       , "Credit" },
    { TT_VOID         , "Void" },
    { TT_SETTLE       , "Settle" },
    { TT_CARD_VERIFY  , "Card Verify" },
    { TT_FORCE        , "Offline" },
    { TT_REFUND       , "Refund" },
    { TT_RE_AUTH      , "Re-auth" },
  };
  
  protected double            AuthAmount            = 0.0;
  protected String            AuthCode              = null;
  protected Date              AuthDate              = null;
  protected String            AuthResponseCode      = null;
  protected String            AuthReturnedACI       = null;
  protected String            AuthSourceCode        = null;
  protected String            AuthAvsResponse       = null;
  protected String            AuthCvv2Response      = null;
  protected String            AuthRefNum            = null;
  protected String            AuthTranId            = null;
  protected String            AuthValCode           = null;
  protected Date              BatchDate             = null;
  protected int               BankNumber            = 0;
  private   String            CardNumber            = null;
  private   byte[]            CardNumberEnc         = null;
  protected String            ClientReferenceNumber = null;
  protected String            CountryCode           = null;
  protected String            CurrencyCode          = null;
  protected String            CurrencyCodeAlpha     = null;
  protected String            CustomerHostName      = null;
  protected String            CustomerBrowserType   = null;
  protected String            CustomerANI           = null;
  protected String            CustomerANI_II        = null;
  protected String            CustServicePhone      = null;
  protected String            DbaName               = null;
  protected String            DbaCity               = null;
  protected String            DbaState              = null;
  protected String            DbaZip                = null;
  protected String            DebitCreditInd        = null;
  protected String            DebitNetworkId        = null;
  protected String            DMContactInfo         = null;
  protected double            FxAmount              = 0.0;
  protected int               FxRateId              = 0;
  protected String            FxTranId              = null;
  protected String            FxRateTableXml        = null;
  protected String            FxRateXml             = null;
  protected String            IpAddress             = null;
  protected long              MerchantId            = 0L;
  protected String            MotoEcommInd          = TridentApiConstants.FV_MOTO_ECOMM_DEFAULT;
  protected String            PosDataCode           = null;
  protected String            PosEntryMode          = TridentApiConstants.FV_ENTRY_MODE_DEFAULT;
  protected String            ProductSku            = null;
  protected String            ProfileId             = null;
  protected String            PurchaseId            = null;
  protected long              RecId                 = 0L;
  protected int               RecurringPaymentCount = 0;
  protected String            RecurringPaymentInd   = null;
  protected int               RecurringPaymentNumber= 0;
  protected String            ReferenceNumber       = null;
  protected String            ReimburseAttribute    = null;
  protected HashMap           ResponseData3D        = null;
  protected String            RetryId               = TridentApiConstants.RETRY_ID_NONE;
  protected String            ServerName            = null;
  protected String            ShippingMethod        = null;
  protected String            ShipToAddress         = null;
  protected String            ShipToCountryCode     = null;
  protected String            ShipToFirstName       = null;
  protected String            ShipToLastName        = null;
  protected String            ShipToPhone           = null;
  protected String            ShipToZip             = null;
  protected String            Sic                   = null;
  protected double            TaxAmount             = 0.0;
  protected String            TestFlag              = null;
  protected double            TranAmount            = 0.0;
  protected String            TranAmountRaw         = null;
  protected Date              TranDate              = null;
  protected String            TranType              = TT_INVALID;

  // Hotel / Cruise / Car Rental
  protected Date              TravelDateBegin           = null;
  protected Date              TravelDateEnd          = null;
  protected String            TravelCityBegin;
  protected String            TravelCityEnd;
  protected String            TravelRegionBegin;
  protected String            TravelRegionEnd;
  protected String            TravelCountryBegin;
  protected String            TravelCountryEnd;
  protected String            TravelCheckInName;
  protected String            TravelCheckInLocation;
  protected double            TravelCheckInAmount          = 0.0;
  protected long              TravelStayDuration          = -1;
  
  public TridentApiTranBase( )
  {
  }
  
  public String currencyCodeToCountryCode( String currencyCodeAlpha )
  {
    HashMap  currencyCodeToCountryCodeMap  =
      new HashMap()
      {
        {
          put("USD", "US" );
          put("GBP", "GB" );
          put("CAD", "CA" );
        }
      };
    return( (String)currencyCodeToCountryCodeMap.get(currencyCodeAlpha) );
  }
  
  /**
   * Additional data edits are performed here. Profile and parameters should all be set by this point.
   */
  public void doEdits() {
    // Amex requests cannot have non-numeric ship_to_zip (PMG-128)
    if(hasShipToZip())
      if(getCardType().equals("AM")) 
        setShipToZip(getShipToZip().replaceAll("[^0-9]", ""));

    // Edits on ship_to_phone (PNG-148)
    if(hasShipToPhone()) {
      String ph = getShipToPhone().replaceAll("[^0-9]", "");
      if(ph.length() > 10)
        ph = ph.substring(ph.length()-10, ph.length());
      setShipToPhone(ph);
    }
    
    // Enforce field truncation
    TravelCityBegin = truncateString(TravelCityBegin, TridentApiConstants.FLM_TRAVEL_CITY);
    TravelCityEnd = truncateString(TravelCityEnd, TridentApiConstants.FLM_TRAVEL_CITY);
    TravelRegionBegin = truncateString(TravelRegionBegin, TridentApiConstants.FLM_TRAVEL_REGION);
    TravelRegionEnd = truncateString(TravelRegionEnd, TridentApiConstants.FLM_TRAVEL_REGION);
    TravelCountryBegin = truncateString(TravelCountryBegin, TridentApiConstants.FLM_TRAVEL_COUNTRY);
    TravelCountryEnd = truncateString(TravelCountryEnd, TridentApiConstants.FLM_TRAVEL_COUNTRY);
  }
  
  public static String encodeServerName( String serverName )
  {
    StringBuffer  buffer    = new StringBuffer();
    String        hostAddr  = null;
    
    if ( serverName != null && !serverName.trim().equals("") )
    {
      buffer.append(serverName);
    }
    else
    {
      buffer.append("tapi");
    }
    buffer.append("-");
    
    try
    {
      // append the last octet of the host IP address
      hostAddr  = InetAddress.getLocalHost().toString();
      int     idx       = hostAddr.lastIndexOf(".");
      if ( idx >= 0 )
      {
        buffer.append(hostAddr.substring(idx+1));
      }
      else
      {
        buffer.append(hostAddr);
      }
    }
    catch( Exception e )
    {
      buffer.append("no-addr");
    }
    return( buffer.toString() );
  }
  
  public HashMap get3DResponseData()
  {
    return(ResponseData3D);
  }

  public String getAmountFormatted( double amount )
  {
    int       decDigits   = 2;
    String    retVal      = null;
    
    try
    {
      decDigits = Currency.getInstance(getCurrencyCodeAlpha()).getDefaultFractionDigits();
      retVal    = MesMath.toFractionalAmount(amount,decDigits,decDigits,false);
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + ".getAmountFormatted()", e.toString());
    }      
    return( retVal );
  }

  public double getAuthAmount( )
  {
    return( AuthAmount );
  }

  public String getAuthAvsResponse( )
  {
    return(processString(AuthAvsResponse));
  }

  public String getAuthCode()
  {
    return(processString(AuthCode));
  }
  
  public Date getAuthDate( )
  {
    return( AuthDate );
  }
  
  public String getAuthRefNum()
  {
    return(processString(AuthRefNum));
  }
  
  public String getAuthResponseCode( )
  {
    return(processString(AuthResponseCode));
  }
  
  public String getAuthReturnedACI( )
  {
    return(processString(AuthReturnedACI));
  }
  
  public String getAuthSourceCode( )
  {
    return(processString(AuthSourceCode));
  }
  
  public String getAuthTranId()
  {
    return(processString(AuthTranId));
  }
  
  public String getAuthValCode()
  {
    return(processString(AuthValCode));
  }
  
  public Date getBatchDate()
  {
    return( BatchDate );
  }

  public int getBankNumber( )
  {
    return( BankNumber );
  }
  
  public String getCardNumber()
  {
    return(processString(CardNumber));
  }
  
  public String getCardNumberEnc()
  {
    return(ByteUtil.hexStringN(CardNumberEnc));
  }
  
  public String getCardNumberFull()
  {
    String      retVal    =   null;
    
    try
    {
      retVal = new String(MesEncryption.getClient().decrypt(CardNumberEnc));
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + "::" + "getCardNumberFull()", e.toString());
    }
    return( retVal );
  }
  
  public String getCardType()
  {
    String    debitNetId  = getDebitNetworkId();
    String    retVal      = "";
    
    if ( debitNetId != null && !debitNetId.trim().equals("") )
    {
      retVal = "DB";  // has debit data, for to debit
    }
    else
    {
      retVal = TridentTools.decodeVisakCardType(getCardNumber());
    }
    return( retVal );
  }
  
  public String getClientReferenceNumber()
  {
    return(processString(ClientReferenceNumber));
  }
  
  public String getCountryCode()
  {
    return(processString(CountryCode));
  }
  
  public String getCountryName()
  {
    return( CountryCodeConverter.getInstance().getCountryName(getCountryCode()) );
  }    
  
  public String getCurrencyCode( )
  {
    return( processString(CurrencyCode) );
  }
  
  public String getCurrencyCodeAlpha( )
  {
    return( processString(CurrencyCodeAlpha) );
  }
  
  public String getCustomerANI()
  {
    return( processString(CustomerANI) );
  }
  
  public String getCustomerANI_II()
  {
    return( processString(CustomerANI_II) );
  }
  
  public String getCustomerBrowserType()
  {
    return( CustomerBrowserType );
  }
  
  public String getCustomerHostName()
  {
    return( CustomerHostName );
  }
  
  public String getCustServicePhone()
  {
    return(processString(CustServicePhone));
  }
  
  public String getCustServicePhoneFormatted()
  {
    String    retVal = getCustServicePhone();
    
    if ( retVal.length() == 10 )
    {
      retVal =  retVal.substring(0,3) + "-" + 
                retVal.substring(3,10);
    }
    return(retVal);
  }
  
  public String getDbaName()
  {
    return(processString(DbaName));
  }
  
  public String getDbaCity()
  {
    return(processString(DbaCity));
  }
  
  public String getDbaState()
  {
    return(processString(DbaState));
  }
  
  public String getDbaZip()
  {
    return(processString(DbaZip));
  }
  
  public String getDebitCreditInd()
  {
    return(processString(DebitCreditInd));
  }
  
  public String getDebitNetworkId()
  {
    return(DebitNetworkId);
  }
  
  public String getDMContactInfo()
  {
    return(DMContactInfo);
  }
  
  public double getFxAmount()
  {
    return( FxAmount );
  }
  
  public String getFxAmountFormatted()
  {
    return( getAmountFormatted( getFxAmount() ) );
  }
  
  public int getFxRateId( )
  {
    return( FxRateId );
  }
  
  public String getFxRateTableXml()
  {
    return( FxRateTableXml );
  }
  
  public String getFxRateXml()
  {
    return( FxRateXml );
  }
  
  public String getFxTranId()
  {
    return( FxTranId );
  }
  
  public String getPosDataCode()
  {
    return( processString(PosDataCode));
  }
  
  public String getPosEntryMode()
  {
    String  retVal    = processString(PosEntryMode);
    
    // overload the "keyed" value for MasterCard e-commerce transactions
    if ( retVal.equals(TridentApiConstants.FV_ENTRY_MODE_KEYED) &&
         getCardType().equals("MC") && isEcommerce() )
    {
      retVal = TridentApiConstants.FV_ENTRY_MODE_MC_ECOMMERCE;  // MC only
    }
    return(retVal);
  }
  
  public String getProductSku()
  {
    return( ProductSku );
  }
  
  public String getPurchaseId()
  {
    return(processString(PurchaseId));
  }
  
  public long getMerchantId()
  {
    return( MerchantId );
  }
  
  public String getMotoEcommIndicator( )
  {
    return( MotoEcommInd );
  }

  public String getProfileId()
  {
    return( processString(ProfileId) );
  }
  
  public long getRecId( )
  {
    return( RecId );
  }

  public int getRecurringPaymentCount( )
  {
    return( RecurringPaymentCount );
  }

  public String getRecurringPaymentInd( )
  {
    return( RecurringPaymentInd );
  }

  public int getRecurringPaymentNumber( )
  {
    return( RecurringPaymentNumber );
  }

  public String getReferenceNumber()
  {
    return(processString(ReferenceNumber));
  }

  public String getReimburseAttribute( )
  {
    return( ReimburseAttribute );
  }
  
  public String getRetryId( )
  {
    return( RetryId );
  }
  
  public String getServerName( )
  {
    return( ServerName );
  }
  
  public String getServerNameEncoded( )
  {
    return( encodeServerName( getServerName() ) );
  }
  
  public String getShippingMethod()
  {
    return( ShippingMethod );
  }
  
  public String getShipToAddress( )
  {
    return( ShipToAddress );
  }
  
  public String getShipToCountryCode( )
  {
    return( ShipToCountryCode );
  }
  
  public String getShipToFirstName( )
  {
    return( ShipToFirstName );
  }
  
  public String getShipToLastName( )
  {
    return( ShipToLastName );
  }
  
  public String getShipToPhone( )
  {
    return( ShipToPhone );
  }
  
  public String getShipToZip( )
  {
    return( ShipToZip );
  }
  
  public String getSicCode()
  {
    return(processString(Sic));
  }
  
  public double getTaxAmount( )
  {
    return( TaxAmount );
  }

  public String getTestFlag( )
  {
    return( TestFlag );
  }

  public double getTranAmount( )
  {
    return( TranAmount );
  }

  /**
   * Returns the tran amount as it is found in the request string.
   * @return
   */
  public String getTranAmountRaw( )
  {
	  return( TranAmountRaw );
  }
  
  public long getTranAmountAsLong( )
  {
    int       decDigits       = 2;
    
    try 
    { 
      decDigits = Currency.getInstance(getCurrencyCodeAlpha()).getDefaultFractionDigits(); 
    }
    catch( Exception e ) 
    {
      // default to 2 decimal places
    }       
    return( MesMath.doubleToLong(TranAmount,decDigits) );
  }
  
  public String getTranAmountFormatted( )
  {
    return( getAmountFormatted( getTranAmount() ) );
  }

  public Date getTranDate( )
  {
    return( TranDate );
  }
  
  public String getTranType()
  {
    return(processString(TranType));
  }
  
  public Date getTravelDateBegin() {
    return TravelDateBegin;
  }

  public Date getTravelDateEnd() {
    return TravelDateEnd;
  }
  
  public Date getTravelCheckInDate() {
    return TravelDateBegin;
  }

  public Date getTravelCheckOutDate() {
    return TravelDateEnd;
  }

  public String getTravelCheckInName() {
    return TravelCheckInName;
  }

  public String getTravelCheckInLocation() {
    return TravelCheckInLocation;
  }
  
  public double getTravelCheckInAmount() {
    return TravelCheckInAmount;
  }
  
  public String getTravelCityBegin() {
    return TravelCityBegin;
  }
  
  public String getTravelCityEnd() {
    return TravelCityEnd;
  }
  
  public String getTravelRegionBegin() {
    return TravelRegionBegin;
  }
  
  public String getTravelRegionEnd() {
    return TravelRegionEnd;
  }
  
  public String getTravelCountryBegin() {
    return TravelCountryBegin;
  }
  
  public String getTravelCountryEnd() {
    return TravelCountryEnd;
  }
  
  public String getTranTypeDesc()
  {
    String      retVal      = getTranType();
    for( int i = 0; i < TranTypeDescriptions.length; ++i )
    {
      if ( retVal.equals(TranTypeDescriptions[i][0]) )
      {
        retVal = TranTypeDescriptions[i][1];
        break;
      }
    }
    return(retVal);
  }

  public long getTravelStayDuration() {
    return TravelStayDuration;
  }
  
  public boolean hasAuthCode()
  {
    return( !isBlank( AuthCode ) );
  }
  
  public boolean hasCustomerHostName()     
  {
    return( !isBlank( CustomerHostName ) );
  }
  
  public boolean hasCustomerBrowserType()  
  {
    return( !isBlank( CustomerBrowserType ) );
  }
  
  public boolean hasCustomerANI()          
  {
    return( !isBlank( CustomerANI ) );
  }
  
  public boolean hasCustomerANI_II()       
  {
    return( !isBlank( CustomerANI_II ) );
  }
  
  public boolean hasFxRateId()
  {
    return( getFxRateId() != 0 );
  }
  
  public boolean hasProductSku()           
  {
    return( !isBlank( ProductSku ) );
  }
  
  public boolean hasRetryId()
  {
    return( !isBlank( getRetryId() ) );
  }
  
  public boolean hasShippingMethod()       
  {
    return( !isBlank( ShippingMethod ) );
  }
  
  public boolean hasShipToAddress()
  {
    return( !isBlank( getShipToAddress() ) );
  }
  
  public boolean hasShipToCountryCode()
  {
    return( !isBlank( getShipToCountryCode() ) );
  }
  
  public boolean hasShipToFirstName()
  {
    return( !isBlank( getShipToFirstName() ) );
  }
  
  public boolean hasShipToLastName()
  {
    return( !isBlank( getShipToLastName() ) );
  }
  
  public boolean hasShipToPhone()
  {
    return( !isBlank( getShipToPhone() ) );
  }
  
  public boolean hasShipToZip()
  {
    return( !isBlank( getShipToZip() ) );
  }
  
  /**
   * @return Whether the request is valid for travel data. Both check in and check out dates are required to be considered valid.
   */
  public boolean hasTravelData() {
    return this.TravelDateBegin != null && this.TravelDateEnd != null;
  }
  
  public static boolean isBlank( String value )
  {
    return( (value == null) || value.trim().equals("") );
  }
  
  public boolean isCardTypeAmex() {
    return( "AM".equals(getCardType()));
  }
  
  public boolean isCardTypeDiscover() {
    return( "DS".equals(getCardType()));
  }
  
  public boolean isCardTypeVMC()
  {
    String ct = getCardType();
    return( "MC".equals(ct) || "VS".equals(ct) );
  }
  
  public boolean isEcommerce()
  {
    String  ecommerce = ( TridentApiConstants.FV_MOTO_SECURE_ECOMMERCE +
                          TridentApiConstants.FV_MOTO_NON_AUTH_SECURE_ECOMMERCE +
                          TridentApiConstants.FV_MOTO_ECOMMERCE +
                          TridentApiConstants.FV_MOTO_NON_SECURE_ECOMMERCE );
    return( ecommerce.indexOf( getMotoEcommIndicator() ) >= 0 );
  }
  
  public boolean isOffline()
  {
    return( getTranType().equals(TT_FORCE) );
  }
  
  public boolean isRecurringPayment()
  {
    String  recurring   = ( TridentApiConstants.FV_MOTO_RECURRING_MOTO +
                            TridentApiConstants.FV_MOTO_INSTALLMENT_PAYMENT );
    return( recurring.indexOf( getMotoEcommIndicator() ) >= 0 );
  }
  
  public boolean isSettled( )
  {
    boolean   retVal    = false;
    String    type      = getTranType();    
    
    if ( type.equals(TT_DEBIT) || type.equals(TT_CREDIT) || type.equals(TT_FORCE) )
    {
      retVal = true;
    }
    return( retVal );      
  }
  
  protected String processString( String input )
  {
    return( processString(input,"") );
  }

  protected String processString( String input, String defaultValue )
  {
    return( (input == null) ? defaultValue : input );
  }
  
  public void set3DResponseData( CentinelResponse centinelResp )
  {
    if ( ResponseData3D == null )
    {
      ResponseData3D = new HashMap();
    }
    ResponseData3D.clear();
    ResponseData3D.put( TridentApiConstants.FN_MOTO_IND,    centinelResp.getValue("EciFlag") );
    ResponseData3D.put( TridentApiConstants.FN_3D_ENROLLED, centinelResp.getValue("Enrolled") );
    ResponseData3D.put( TridentApiConstants.FN_3D_TRAN_ID,  centinelResp.getValue("TransactionId") );
    ResponseData3D.put( TridentApiConstants.FN_3D_ACS_URL,  centinelResp.getValue("ACSUrl") );
    ResponseData3D.put( TridentApiConstants.FN_3D_PAYLOAD,  centinelResp.getValue("Payload") );
    ResponseData3D.put( TridentApiConstants.FN_3D_ORDER_ID, centinelResp.getValue("OrderId") );
  }

  public void setAuthAmount( double authAmount )
  {
    AuthAmount = authAmount;
  }
  
  public void setAuthAvsResponse( String avsResponse )
  {
    AuthAvsResponse = avsResponse;
  }

  public void setAuthCode( String authCode )
  {
    AuthCode = authCode;
  }
  
  public void setAuthRefNum( String refNumber )
  {
    AuthRefNum = refNumber;
  }

  public void setAuthResponseCode( String responseCode )
  {
    AuthResponseCode = responseCode;
  }
  
  public void setAuthSourceCode( String authSrcCode )
  {
    AuthSourceCode = authSrcCode;
  }
  
  public void setAuthTranId( String authTranId )
  {
    AuthTranId = authTranId;
  }

  public void setAuthValCode( String valCode )
  {
    AuthValCode = valCode;
  }
  
  public void setBatchDate( Date batchDate )
  {
    BatchDate = batchDate;
  }

  public void setCardNumber( String cardNumber )
  {
    // only accept truncated values
    if ( cardNumber.indexOf("xxxxxx") >= 0 )
    {
      CardNumber = cardNumber;
    }
  }
  
  public void setCardNumberEnc( String cardNumberEnc )
  {
    try
    {
      CardNumberEnc = ByteUtil.parseHexString(cardNumberEnc);
    }
    catch( Exception e )
    {
      // ignore, input value was null or invalid
    }
  }
  
  public void setCardNumberFull( String cardNumber )
  {
    try
    {
      if ( cardNumber != null )
      {
        // remove any non-numeric values from the card number
        StringBuffer  cnFull  = new StringBuffer();
        for( int i = 0; i < cardNumber.length(); ++i )
        {
          char ch = cardNumber.charAt(i);
          if ( Character.isDigit(ch) )
          {
            cnFull.append(ch);
          }
        }
        cardNumber = cnFull.toString();
        
        CardNumberEnc = MesEncryption.getClient().encrypt(cardNumber.getBytes());
        CardNumber    = TridentTools.encodeCardNumber(cardNumber);
      }
      else    // input was null, clear class values
      {
        CardNumberEnc = null;
        CardNumber    = null;
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + ".setCardNumberFull()", e.toString() );
      
      // default to null values
      CardNumberEnc = null;
      CardNumber    = null;
    }      
  }
  
  public void setClientReferenceNumber( String clientRefNum )
  {
    ClientReferenceNumber = clientRefNum;
  }
  
  public void setCountryCode( String countryCode )
  {
    CountryCode = countryCode;
  }
  
  public void setCustServicePhone( String phoneNumber )
  {
    CustServicePhone = phoneNumber;
  }
  
  public void setDbaCity( String dbaCity )
  {
    DbaCity = dbaCity;
  }
  
  public void setDbaName( String dbaName )
  {
    DbaName = dbaName;
  }
  
  public void setDbaState( String dbaState )
  {
    DbaState = dbaState;
  }
  
  public void setDbaZip( String dbaZip )
  {
    DbaZip = dbaZip;
  }
  
  public void setDebitCreditInd( String ind)
  {
    DebitCreditInd = ind;
  }
  
  public void setDMContactInfo(String dmContactInfo)
  {
    DMContactInfo = dmContactInfo;
  }

  public void setCurrencyCode( String currencyCode )
  {
    boolean   isNumber  = false;
    
    try
    {
      int tempInt = Integer.parseInt(currencyCode);
      isNumber = true;
    }
    catch( Exception e )
    {
    }
    
    CurrencyCodeConverter converter = CurrencyCodeConverter.getInstance();
    if ( isNumber )
    {
      CurrencyCode        = currencyCode;
      CurrencyCodeAlpha   = converter.numericCodeToAlphaCode( currencyCode );
    }
    else
    {
      CurrencyCode        = converter.alphaCodeToNumericCode( currencyCode );
      CurrencyCodeAlpha   = currencyCode;
    }
  }
  
  public void setCustomerANI( String value )
  {
    CustomerANI = value;
  }
  
  public void setCustomerANI_II( String value )
  {
    CustomerANI_II = value;
  }
  
  public void setCustomerBrowserType( String value )
  {
    CustomerBrowserType = value;
  }
  
  public void setCustomerHostName( String value )
  {
    CustomerHostName = value;
  }
  
  public void setDebitNetworkId( String networkId )
  {
    DebitNetworkId = networkId;
  }
  
  public void setFxAmount( double amount )
  {
    FxAmount = amount;
  }
  
  public void setFxRateId( int rateId )
  {
    try
    {
      FxRateId = rateId;
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + ".setFxRateId(" + rateId + ")", e.toString());
    }
  }
  
  public void setFxRateTableXml( String tableData )
  {
    FxRateTableXml = tableData;
  }
  
  public void setFxRateXml( String rateData )
  {
    FxRateXml = rateData;
  }
  
  public void setFxTranId( String tranId )
  {
    FxTranId = tranId;
  }
  
  public void setMotoEcommIndicator( String motoEcommInd )
  {
    String  newValue  = (motoEcommInd == null ? null : motoEcommInd.trim());

    try
    {
      // if the value is a number, strip any leading zeroes
      int intValue = Integer.parseInt(motoEcommInd);
      newValue = String.valueOf(intValue);
    }
    catch( Exception e )
    { 
      // not a number, store the full value
    }
    MotoEcommInd = newValue;      
  }
  
  public void setMotoEcommIndicatorToSpace()
  {
    MotoEcommInd = " ";
  }

  public void setPosDataCode( String posDataCode )
  {
    PosDataCode = posDataCode;
  }

  public void setPosEntryMode( String entryMode )
  {
    PosEntryMode = entryMode;
  }
  
  public void setProductSku( String value )
  {
    ProductSku = value;
  }
  
  public void setProfileId( String profileId )
  {
    ProfileId = profileId;
  }
  
  public void setPurchaseId ( String purchaseId )
  {
    PurchaseId = purchaseId;
  }
  
  public void setRecurringPaymentCount( int paymentCount )
  {
    RecurringPaymentCount = paymentCount;
  }

  public void setRecurringPaymentInd( String paymentInd )
  {
    RecurringPaymentInd = paymentInd;
  }

  public void setRecurringPaymentNumber( int paymentNumber )
  {
    RecurringPaymentNumber = paymentNumber;
  }
  
  public void setRetryId( String retryId )
  {
    RetryId = retryId;
  }
  
  public void setServerName( String serverName )
  {
    ServerName = serverName;
  }
  
  public void setShippingMethod( String value )
  {
    ShippingMethod = value;
  }
  
  public void setShipToAddress( String value )
  {
    ShipToAddress = value;
  }
  
  public void setShipToCountryCode( String value )
  {
    ShipToCountryCode = value;
  }
  
  public void setShipToFirstName( String value )
  {
    ShipToFirstName = value;
  }
  
  public void setShipToLastName( String value )
  {
    ShipToLastName = value;
  }
  
  public void setShipToPhone( String value )
  {
    ShipToPhone = value;
  }
  
  public void setShipToZip( String value )
  {
    ShipToZip = value;
  }
  
  public void setTaxAmount( double value )
  {
    TaxAmount = value;
  }
  
  public void setTestFlag( String flag )
  {
    TestFlag = ("N".equals(flag) ? "N" : "Y");
  }
  
  public void setTranAmount( double tranAmount )
  {
    TranAmount = tranAmount;
  }
  
  public void setTranDate( Date tranDate )
  {
    TranDate = tranDate;
  }

  public void setTransactionType( String tranType )
  {
    TranType = tranType;
  }
  
  public String truncateString(String field, int maxLen) {
    if(field != null && field.length() > maxLen)
      return field.substring(0, maxLen);
    else
      return field;
  }
  
  /**
   * Sets the fields related to Hotels, Cruise Lines, and Car Rentals
   * @param request
   */
  protected void setTravelData(HttpServletRequest request) {
    java.util.Date inDate = HttpHelper.getDateAttribute(request, TridentApiConstants.FN_TRAVEL_CHECK_IN_DATE, "MM/dd/yyyy", null);
    java.util.Date outDate = HttpHelper.getDateAttribute(request, TridentApiConstants.FN_TRAVEL_CHECK_OUT_DATE, "MM/dd/yyyy", null);
    
    if(inDate != null)
      this.TravelDateBegin = new java.sql.Date(inDate.getTime());
    if(outDate != null)
      this.TravelDateEnd = new java.sql.Date(outDate.getTime());
    
    this.TravelCheckInLocation = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_CHECK_IN_LOCATION, null);
    this.TravelCheckInName = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_REQUESTER_NAME, null);
    this.TravelCheckInAmount = Math.abs(MesMath.round(HttpHelper.getDoubleAttribute(request,TridentApiConstants.FN_TRAVEL_CHECK_IN_RATE, 0.0), 2));
    // Added city, region, and country departure and return fields (PMG-186).
    this.TravelCityBegin = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_CITY_BEGIN, null);
    this.TravelCityEnd = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_CITY_END, null);
    this.TravelRegionBegin = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_REGION_BEGIN, null);
    this.TravelRegionEnd = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_REGION_END, null);
    this.TravelCountryBegin = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_COUNTRY_BEGIN, null);
    this.TravelCountryEnd = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_TRAVEL_COUNTRY_END, null);
    
    if(TravelDateBegin != null && TravelDateEnd != null) {
      TravelStayDuration = (TravelDateEnd.getTime() - TravelDateBegin.getTime());
      TravelStayDuration /= 86400000; // ms in 1 day
      if(TravelStayDuration >= 0 && TravelStayDuration < 1)
        TravelStayDuration = 1; // minimum 1 day
      if(TravelStayDuration > 99) // Cap duration at 99 days (no error code) - PMG-179
        TravelStayDuration = 99; 
    }
  }
  
  public String urlEncode( String input )
  {
    String    retVal    = input;
    
    try
    {
      retVal = URLEncoder.encode(input,"UTF-8");
    }
    catch( Exception e )
    {
      // just use value non-encoded
    }
    return( retVal );
  }
  
}
