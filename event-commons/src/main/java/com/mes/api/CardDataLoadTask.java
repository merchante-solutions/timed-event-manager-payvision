/*@lineinfo:filename=CardDataLoadTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/CardDataLoadTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class CardDataLoadTask extends TridentApiTransactionTask
{
  public CardDataLoadTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    boolean             retVal        = false;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:41^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id,
//                  terminal_id,
//                  merchant_number,
//                  card_id,
//                  dukpt_decrypt_wrapper(card_number_enc)  as card_number_full,
//                  exp_date
//          from    trident_api_card_data cd
//          where   cd.merchant_number = :Transaction.getMerchantId() and
//                  cd.card_id = :Transaction.getCardId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_129 = Transaction.getMerchantId();
 String __sJT_130 = Transaction.getCardId();
  try {
   String theSqlTS = "select  rec_id,\n                terminal_id,\n                merchant_number,\n                card_id,\n                dukpt_decrypt_wrapper(card_number_enc)  as card_number_full,\n                exp_date\n        from    trident_api_card_data cd\n        where   cd.merchant_number =  :1  and\n                cd.card_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.CardDataLoadTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_129);
   __sJT_st.setString(2,__sJT_130);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.CardDataLoadTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:52^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        Transaction.setCardNumberFull( resultSet.getString("card_number_full") );
        updateAccessDate( resultSet.getLong("rec_id") );
        retVal = true;
      }
      else
      {
        setError(TridentApiConstants.ER_INVALID_CARD_ID);
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      setError(TridentApiConstants.ER_CARD_DATA_LOAD_FAILURE);
      logEntry("doTask()",e.toString());
      System.out.println("doTask(): " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){}
      cleanUp(); 
    }
    return( retVal );
  }
  
  private void updateAccessDate( long recId )
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:87^7*/

//  ************************************************************
//  #sql [Ctx] { update trident_api_card_data
//          set     last_access_time = sysdate,
//                  last_access_date = trunc(sysdate)
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update trident_api_card_data\n        set     last_access_time = sysdate,\n                last_access_date = trunc(sysdate)\n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.api.CardDataLoadTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^7*/
    }
    catch( Exception e )
    {
      // log but do not raise, allow the system to work
      // even if we miss an access update due to an Oracle error
      logEntry("updateAccessData(" + recId + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/