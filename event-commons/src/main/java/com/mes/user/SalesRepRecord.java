/*@lineinfo:filename=SalesRepRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/SalesRepRecord.sqlj $

  Description:  
  
    SalesRepRecord.
    
    Contains a sales_rep record.  Allows insertion, modification and
    deletion of records.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class SalesRepRecord extends DBBean
{
  // record fields
  private String            repId;
  private int               repType;
  private int               repRegion;
  private int               repPlan;
  private long              repMerchantId     = 0L;
  private long              repSSN;
  private String            repReportTo;
  private String            repTitle;
  private Date              repHireDate;
  private Date              repTerminateDate;
  private long              userId;
  
  private boolean           getByRepId = true;
  
  /*
  ** CONSTRUCTOR SalesRepRecord()
  */
  public SalesRepRecord(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
  }
  public SalesRepRecord()
  {
  }
  
  /*
  ** CONSTRUCTOR SalesRepRecord(String newRepId)
  **
  ** Loads record with sales rep record corresponding with newRepId
  */
  public SalesRepRecord(String newRepId)
  {
    getData(newRepId);
  }
  
  /*
  ** CONSTRUCTOR SalesRepRecord()
  **
  ** Loads record with sales rep record corresponding with newUserId.
  */
  public SalesRepRecord(long newUserId)
  {
    getData(newUserId);
  }
  public SalesRepRecord(long newUserId, DefaultContext Ctx)
  {
    this.Ctx = Ctx;
    getData(newUserId);
  }
  
  /*
  ** METHOD public void loadResultSet(ResultSet rs)
  **
  ** Loads the contents of a ResultSet into internal field storage.
  */
  public void loadResultSet(ResultSet rs)
  {
    try
    {
      repMerchantId     = rs.getLong  ("rep_merchant_number");
      repId             = processString(rs.getString("rep_id"));
      repType           = rs.getInt   ("rep_type");
      repRegion         = rs.getInt   ("rep_region");
      repPlan           = rs.getInt   ("rep_plan_id");
      repSSN            = rs.getLong  ("rep_ssn");
      repReportTo       = processString(rs.getString("rep_report_to"));
      repTitle          = processString(rs.getString("rep_title"));
      repHireDate       = rs.getDate  ("rep_hire_date");
      userId            = rs.getLong  ("user_id");
      repTerminateDate  = rs.getDate  ("rep_terminate_date");
    }
    catch (Exception e)
    {
      logEntry("loadResultSet()", e.toString());
    }
  }
  
  /*
  ** METHOD protected boolean get()
  **
  ** Loads a right record from the user_rights table.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  protected boolean get()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      // look up user record in users table by login name
      if (getByRepId)
      {
        /*@lineinfo:generated-code*//*@lineinfo:140^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    sales_rep
//            where   rep_id = :repId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    sales_rep\n          where   rep_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.SalesRepRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.SalesRepRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:145^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:149^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    sales_rep
//            where   user_id = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    sales_rep\n          where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.SalesRepRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.SalesRepRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^9*/
      }
      
      rs = it.getResultSet();
      if (rs.next())
      {
        // load the record into internal storage
        loadResultSet(rs);
        getOk = true;
      }
      
      it.close();
    }
    catch (Exception e)
    {
      logEntry("get()", e.toString());
    }
    
    return getOk;
  }
  
  /*
  ** METHOD public boolean getData(String newRepId)
  **
  ** Loads the rep record corresponding with newRepId.
  **
  ** RETURNS: true if rep record successfully loaded, else false.
  */
  public boolean getData(String newRepId)
  {
    setRepId(newRepId);
    getByRepId = true;
    return getData();
  }
  
  /*
  ** METHOD public boolean getData(long newUserId)
  **
  ** Loads the rep record corresponding with newUserId.
  **
  ** RETURNS: true if rep record successfully loaded, else false.
  */
  public boolean getData(long newUserId)
  {
    setUserId(newUserId);
    getByRepId = false;
    return getData();
  }
  
  /*
  ** METHOD protected boolean submit()
  **
  ** Inserts or updates a right record.
  **
  ** RETURNS: true if submit successful, else false.
  */
  protected boolean submit()
  {
    boolean submitOk = false;
    java.sql.Date realTerminateDate = null;

    try
    {
      int count;
      
      if (repHireDate == null)
      {
        repHireDate = new Date();
      }
      
      if(repTerminateDate != null)
      {
        realTerminateDate = new java.sql.Date(repTerminateDate.getTime());
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:229^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rep_id) 
//          from    sales_rep
//          where   user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rep_id)  \n        from    sales_rep\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.SalesRepRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^7*/
      
      if (count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:238^9*/

//  ************************************************************
//  #sql [Ctx] { update  sales_rep
//            set     rep_type      = :repType,
//                    rep_region    = :repRegion,
//                    rep_plan_id   = :repPlan,
//                    rep_ssn       = :repSSN,
//                    rep_report_to = :repReportTo,
//                    rep_title     = :repTitle,
//                    rep_hire_date = :new java.sql.Date(repHireDate.getTime()),
//                    rep_id        = :repId,
//                    rep_merchant_number = :repMerchantId,
//                    rep_terminate_date = :realTerminateDate
//            where   user_id       = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_101 = new java.sql.Date(repHireDate.getTime());
   String theSqlTS = "update  sales_rep\n          set     rep_type      =  :1 ,\n                  rep_region    =  :2 ,\n                  rep_plan_id   =  :3 ,\n                  rep_ssn       =  :4 ,\n                  rep_report_to =  :5 ,\n                  rep_title     =  :6 ,\n                  rep_hire_date =  :7 ,\n                  rep_id        =  :8 ,\n                  rep_merchant_number =  :9 ,\n                  rep_terminate_date =  :10 \n          where   user_id       =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.user.SalesRepRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,repType);
   __sJT_st.setInt(2,repRegion);
   __sJT_st.setInt(3,repPlan);
   __sJT_st.setLong(4,repSSN);
   __sJT_st.setString(5,repReportTo);
   __sJT_st.setString(6,repTitle);
   __sJT_st.setDate(7,__sJT_101);
   __sJT_st.setString(8,repId);
   __sJT_st.setLong(9,repMerchantId);
   __sJT_st.setDate(10,realTerminateDate);
   __sJT_st.setLong(11,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:256^9*/

//  ************************************************************
//  #sql [Ctx] { insert into sales_rep
//            (
//              rep_type,
//              rep_region,
//              rep_plan_id,
//              rep_ssn,
//              rep_report_to,
//              rep_title,
//              rep_hire_date,
//              rep_id,
//              rep_merchant_number,
//              user_id
//            )
//            values
//            (
//              :repType,
//              :repRegion,
//              :repPlan,
//              :repSSN,
//              :repReportTo,
//              :repTitle,
//              :new java.sql.Date(repHireDate.getTime()),
//              :repId,
//              :repMerchantId,
//              :userId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_102 = new java.sql.Date(repHireDate.getTime());
   String theSqlTS = "insert into sales_rep\n          (\n            rep_type,\n            rep_region,\n            rep_plan_id,\n            rep_ssn,\n            rep_report_to,\n            rep_title,\n            rep_hire_date,\n            rep_id,\n            rep_merchant_number,\n            user_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.user.SalesRepRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,repType);
   __sJT_st.setInt(2,repRegion);
   __sJT_st.setInt(3,repPlan);
   __sJT_st.setLong(4,repSSN);
   __sJT_st.setString(5,repReportTo);
   __sJT_st.setString(6,repTitle);
   __sJT_st.setDate(7,__sJT_102);
   __sJT_st.setString(8,repId);
   __sJT_st.setLong(9,repMerchantId);
   __sJT_st.setLong(10,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:284^9*/
      }

      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
    }
    
    return submitOk;
  }
  
  /*
  ** METHOD protected boolean validate()
  **
  ** Validates right data.
  **
  ** RETURNS: true if data is valid, else false.
  */
  protected boolean validate()
  {
    return (!hasErrors());
  }
  
  /*
  ** METHOD public boolean delete()
  **
  ** Deletes the current record from the database.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean delete()
  {
    boolean deleteOk = false;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:322^7*/

//  ************************************************************
//  #sql [Ctx] { delete from sales_rep
//          where       rep_id = :repId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from sales_rep\n        where       rep_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.user.SalesRepRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,repId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^7*/
      
      deleteOk = true;
    }
    catch (Exception e)
    {
      logEntry("delete()", e.toString());
    }

    return deleteOk;
  }
  
  /*
  ** ACCESSORS
  */
  public long getRepMerchantId()
  {
    return(repMerchantId);
  }
  public void setRepMerchantId(String newRepId)
  {
    try
    {
      repMerchantId = Long.parseLong(newRepId);
    }
    catch( NumberFormatException e )
    {
      repMerchantId = 0L;
    }      
  }
  
  public String getRepId()
  {
    return repId;
  }
  public void setRepId(String newRepId)
  {
    repId = newRepId;
  }
  
  public int getReprType()
  {
    return repType;
  }
  public void setReprType(String newRepType)
  {
    try
    {
      setReprType(Integer.parseInt(newRepType));
    }
    catch(Exception e)
    {
      logEntry("setReprType(" + newRepType + ")", e.toString());
    }
  }
  public void setReprType(int newRepType)
  {
    repType = newRepType;
  }
  
  public int getRepRegion()
  {
    return repRegion;
  }
  public void setRepRegion(String newRepRegion)
  {
    try
    {
      setRepRegion(Integer.parseInt(newRepRegion));
    }
    catch(Exception e)
    {
      logEntry("setRepRegion(" + newRepRegion + ")", e.toString());
    }
  }
  public void setRepRegion(int newRepRegion)
  {
    repRegion = newRepRegion;
  }
  
  public int getRepPlan()
  {
    return repPlan;
  }
  public void setRepPlan(String newRepPlan)
  {
    try
    {
      setRepPlan(Integer.parseInt(newRepPlan));
    }
    catch(Exception e)
    {
      logEntry("setRepPlan(" + newRepPlan + ")", e.toString());
    }
  }
  public void setRepPlan(int newRepPlan)
  {
    repPlan = newRepPlan;
  }
  
  public String getRepSSN()
  {
    String retStr = "";
    if (repSSN != 0L)
    {
      retStr = Long.toString(repSSN);
    }
    return retStr;
  }
  public void setRepSSN(String newRepSSN)
  {
    try
    {
      setRepSSN(Long.parseLong(newRepSSN));
    }
    catch(Exception e)
    {
      logEntry("setRepSSN(" + newRepSSN + ")", e.toString());
    }
  }
  public void setRepSSN(long newRepSSN)
  {
    repSSN = newRepSSN;
  }
  
  public String getRepReportTo()
  {
    return repReportTo;
  }
  public void setRepReportTo(String newRepReportTo)
  {
    repReportTo = newRepReportTo;
  }
    
  public String getRepTitle()
  {
    return repTitle;
  }
  public void setRepTitle(String newRepTitle)
  {
    repTitle = newRepTitle;
  }
  
  public String getRepHireDate()
  {
    String retDate = "";
    try
    {
      retDate = new SimpleDateFormat("MM/dd/yyyy").format(repHireDate);
    }
    catch (NullPointerException e)
    {
    }
    catch (Exception e)
    {
      logEntry("getRepHireDate()", e.toString());
    }
    return retDate;
  }
  public void setRepHireDate(String newDateString)
  {
    try
    {
      repHireDate = new SimpleDateFormat("MM/dd/yyyy").parse(newDateString);
    }
    catch (NullPointerException e)
    {
    }
    catch (Exception e)
    {
      addError("Invalid hire date");
      logEntry("setRepHireDate()", e.toString());
    }
  }
  
  public String getRepTerminateDate()
  {
    String retDate = "";
    try
    {
      retDate = new SimpleDateFormat("MM/dd/yyyy").format(repTerminateDate);
    }
    catch (NullPointerException e)
    {
    }
    catch (Exception e)
    {
      logEntry("getRepTerminateDate()", e.toString());
    }
    return retDate;
  }
  public void setRepTerminateDate(String newDateString)
  {
    try
    {
      repTerminateDate = new SimpleDateFormat("MM/dd/yyyy").parse(newDateString);
    }
    catch (NullPointerException e)
    {
    }
    catch (Exception e)
    {
      addError("Invalid terminate date");
      logEntry("setRepTerminateDate()", e.toString());
    }
  }
  
  public long getUserId()
  {
    return userId;
  }
  public void setUserId(String newUserId)
  {
    try
    {
      setUserId(Long.parseLong(newUserId));
    }
    catch(Exception e)
    {
      logEntry("setUserId(" + newUserId + ")", e.toString());
    }
  }
  public void setUserId(long newUserId)
  {
    userId = newUserId;
  }
}/*@lineinfo:generated-code*/