/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/GroupSet.java $

  Description:  
  
    GroupSet.
    
    Contains a set of Group objects.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copygroup (C) 2000,2001 by Merchant e-Solutions Inc.
  All groups reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


public class GroupSet
  implements Serializable
{
  public class GroupComparator implements Comparator, Serializable
  {
    public int compare(Object o1, Object o2)
    {
      if (o1.getClass() != Group.class || o2.getClass() != Group.class)
      {
        throw new ClassCastException(
          "Attempted to compare class type other than GroupRecord");
      }
    
      long r1 = ((Group)o1).getGroupId();
      long r2 = ((Group)o2).getGroupId();
      int result = 0;
    
      if (r1 < r2)
      {
        result = -1;
      }
      else if (r1 > r2)
      {
        result = 1;
      }
    
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }

  protected Set   groupSet  = new TreeSet(new GroupComparator());
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    groupSet.clear();
  }

  /*
  ** MERHOD public boolean add(Group addGroup)
  **
  ** Adds the Group to the set.
  **
  ** RETURNS: true if successfully added, else false (group already in set)
  */
  public boolean add(Group addGroup)
  {
    return groupSet.add(addGroup);
  }
  public boolean add(long addGroupId)
  {
    return add(new Group(addGroupId));
  }
  public boolean add(GroupSet addSet)
  {
    return groupSet.addAll(addSet.groupSet);
  }
  
  /*
  ** METHOD public boolean contains(Group conGroup)
  **
  ** RETURNS: true if conGroup is in the set, else false.
  */
  public boolean contains(Group conGroup)
  {
    return groupSet.contains(conGroup);
  }
  public boolean contains(long conGroupId)
  {
    return contains(new Group(conGroupId));
  }
  
  /*
  ** METHOD public boolean remove(Group remGroup)
  **
  ** Removes the remGroup from the set.
  **
  ** RETURNS: true if group was found and removed, else false.
  */
  public boolean remove(Group remGroup)
  {
    return groupSet.remove(remGroup);
  }
  public boolean remove(long remGroupId)
  {
    return remove(new Group(remGroupId));
  }
  
  /*
  ** METHOD public Iterator getGroupIterator()
  **
  ** RETURNS: Iterator on groupSet
  */ 
  public Iterator getGroupIterator()
  {
    return groupSet.iterator();
  }
  
  public String toString()
  {
    StringBuffer buf = new StringBuffer("Group Set = { ");
    int cnt = 0;
    for (Iterator i = groupSet.iterator(); i.hasNext(); ++cnt)
    {
      buf.append((cnt > 0 ? ", " : "") + i.next());
    }
    buf.append(" }");
    return buf.toString();
  }
}
