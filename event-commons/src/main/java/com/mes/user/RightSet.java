/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/RightSet.java $

  Description:  
  
    RightSet.
    
    Contains a set of Right objects.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class RightSet
  implements Serializable
{
  public class RightComparator implements Comparator, Serializable
  {
    public int compare(Object o1, Object o2)
    {
      if (o1.getClass() != Right.class || o2.getClass() != Right.class)
      {
        throw new ClassCastException(
          "Attempted to compare class type other than RightRecord");
      }
    
      long r1 = ((Right)o1).getRightId();
      long r2 = ((Right)o2).getRightId();
      int result = 0;
    
      if (r1 < r2)
      {
        result = -1;
      }
      else if (r1 > r2)
      {
        result = 1;
      }
    
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }

  protected Set             rightSet        = new TreeSet(new RightComparator());
//  protected Iterator        iter            = rightSet.iterator();
//  protected Right           currentRight    = null;
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    rightSet.clear();
  }

  /*
  ** MERHOD public boolean add(Right addRight)
  **
  ** Adds the Right to the set.
  **
  ** RETURNS: true if successfully added, else false (right already in set)
  */
  public boolean add(Right addRight)
  {
    return rightSet.add(addRight);
  }
  public boolean add(long addRightId)
  {
    return add(new Right(addRightId));
  }
  public boolean add(RightSet addSet)
  {
    return rightSet.addAll(addSet.rightSet);
  }
  
  /*
  ** METHOD public boolean contains(Right conRight)
  **
  ** RETURNS: true if conRight is in the set, else false.
  */
  public boolean contains(Right conRight)
  {
    return rightSet.contains(conRight);
  }
  public boolean contains(long conRightId)
  {
    return contains(new Right(conRightId));
  }
  
  /*
  ** METHOD public boolean remove(Right remRight)
  **
  ** Removes the remRight from the set.
  **
  ** RETURNS: true if right was found and removed, else false.
  */
  public boolean remove(Right remRight)
  {
    return rightSet.remove(remRight);
  }
  public boolean remove(long remRightId)
  {
    return remove(new Right(remRightId));
  }
  
  /*
  ** METHOD public Iterator getRightIterator()
  **
  ** RETURNS: Iterator
  */
  public Iterator getRightIterator()
  {
    return rightSet.iterator();
  }
  
  /*
  ** METHOD public Right getFirst()
  **
  ** Initializes internal iterator to the first item in the ordered set then
  ** sets the currentRight equal to the first Right.
  **
  ** RETURNS: the first Right in the set, or null if empty set.
  */
  /*
  public Right getFirst()
  {
    iter = rightSet.iterator();
    currentRight = null;
    if (iter.hasNext())
    {
      currentRight = (Right)iter.next();
    }
    
    return currentRight;
  }
  */
  
  /*
  ** METHOD public Right getCurrent()
  **
  ** RETURNS: currentRight (which may be null).
  */
  /*
  public Right getCurrent()
  {
    return currentRight;
  }
  */
  
  /*
  ** METHOD public Right getNext()
  **
  ** Attempts to get the next available Right from the set.
  **
  ** RETURNS: the next Right if found, else null.
  */
  /*
  public Right getNext()
  {
    currentRight = null;
    if (iter != null && iter.hasNext())
    {
      currentRight = (Right)iter.next();
    }
    return currentRight;
  }
  */
  
  public String toString()
  {
    StringBuffer buf = new StringBuffer("Right Set = { ");
    int cnt = 0;
    for (Iterator i = rightSet.iterator(); i.hasNext(); ++cnt)
    {
      buf.append((cnt > 0 ? ", " : "") + i.next());
    }
    buf.append(" }");
    return buf.toString();
  }
}
