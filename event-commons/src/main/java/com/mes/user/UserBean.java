/*@lineinfo:filename=UserBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserBean.sqlj $

  Description:

  UserBean

  Allows validation of user id's and passwords and gives access to various
  user attributes such as user type, user group membership and user rights.

  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.crypt.MD5;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.CardTruncator;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.PasswordRedactor;
import com.mes.support.ServerTools;
import sqlj.runtime.ResultSetIterator;

public class UserBean extends SQLJConnectionBase
  implements HttpSessionBindingListener, Serializable
{
  static Logger log = Logger.getLogger(UserBean.class);

  public static final int     DEFAULT_EXPIRATION_DAYS         = 10000;
  public static final int     DEFAULT_PASSWORD_CHANGE_REASON  = 1;
  public static final long    DEFAULT_LOGOFF_MINUTES          = 999999L;
  public static final boolean DEFAULT_APPLY_TO_MERCHANTS      = false;
  public static final boolean DEFAULT_FORCE_USER_MERCHANT     = false;
  public static final boolean DEFAULT_FORCE_USER_USER         = false;
  public static final int     DEFAULT_MAX_FAILED_ATTEMPTS     = 6;
  
  // password expiration, etc.
  private int     expirationDays          = DEFAULT_EXPIRATION_DAYS;
  private int     passwordChangeReason    = DEFAULT_PASSWORD_CHANGE_REASON;
  private long    autoLogoffExpireMinutes = DEFAULT_LOGOFF_MINUTES;
  private boolean applyToMerchants        = DEFAULT_APPLY_TO_MERCHANTS;
  private boolean forceUserResetMerchant  = DEFAULT_FORCE_USER_MERCHANT;
  private boolean forceUserResetUser      = DEFAULT_FORCE_USER_USER;
  private int     maxFailedAttempts       = DEFAULT_MAX_FAILED_ATTEMPTS;

  public static final String     SESSION_COOKIE_NAME     = "JSESSIONID";

  // user data
  private long        userId;
  private String      loginName;
  private String      userName;
  private String      email;
  private String      phone;
  private String      password;
  private String      passwordEnc;
  private String      ipAddress;
  private String      sessionId;
  private long        hierarchyNode;
  private long        reportHierarchyNode;
  private UserType    userType;
  private RightSet    rights;
  private GroupSet    groups;
  private boolean     enabled;
  private boolean     ignoreStatus;

  // organization data
  private int         bankId;
  private int         orgId;
  private String      orgName;
  private long        merchId;
  private String      merchName;
  private int         appType;
  private String      appDesc;
  private long        sequenceType;
  private boolean     isValid;

  private OrgProfile  profile             = null;

  private MD5         md5 = null;

  private Date        lastAccessed        = null;
  private String      lastResource        = "";
  private String      lastResourceParams  = "";
  private String      serverName          = "";

  private boolean     newSession          = false;
  private HttpSession loginSession        = null;

  private UserQueueLockItem   queueLock   = null;

  private Vector      redactFieldNames    = null;
  
  private void loadRedactFieldNames()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:148^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  field_name
//          from    access_details_redact_fields
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  field_name\n        from    access_details_redact_fields";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.UserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/
      
      rs = it.getResultSet();
      
      redactFieldNames = new Vector();
      
      while( rs.next() )
      {
        redactFieldNames.add(rs.getString("field_name"));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadRedactFieldNames()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void init()
  {
    // initialize internals
    clear();

    // instantiate MD5 hashing class
    md5 = new MD5();

    // instantiate new QueueLockItem
    queueLock = new UserQueueLockItem();

    // determine server name for use in logging
    serverName = HttpHelper.getServerName(null);

    // default behavior is to heed status (enabled/disabled)
    ignoreStatus = false;
    
    // load password redact field names
    loadRedactFieldNames();
  }

  /*
  ** CONSTRUCTOR public UserBean()
  */
  public UserBean()
  {
    init();
  }

  public UserBean(String connectionString)
  {
    super(connectionString);
    init();
  }

  /*
  ** METHODS TO SUPPORT HttpSessionBindingListener
  */
  public synchronized void valueBound(HttpSessionBindingEvent event)
  {
    try
    {
      newSession = true;
      loginSession = event.getSession();
    }
    catch(Exception e)
    {
      logEntry("valueBound()", e.toString());
    }
  }

  public synchronized void valueUnbound(HttpSessionBindingEvent event)
  {
    try
    {
      /* LoginManager does not work in clustered environment
      LoginManager.removeLogin(loginSession.getId().substring(0, loginSession.getId().indexOf('!')));
      */
    }
    catch(Exception e)
    {
      logEntry("valueUnbound()", e.toString());
    }
  }

  /*
  ** METHOD public synchronized void clear()
  **
  ** Clears out internal data.
  */
  public synchronized void clear()
  {
    userId = 0L;
    loginName = "";
    ipAddress = "";
    sessionId = "";
    userName  = "";
    password = "";
    email     = "";
    hierarchyNode = 0L;
    reportHierarchyNode = 0L;
    userType = new UserType();
    rights = new RightSet();
    groups = new GroupSet();
    enabled = false;

    orgId = 0;
    bankId = 0;
    orgName = "";
    merchId = -1L;
    merchName = "";
    appType = -1;
    appDesc = "";
    sequenceType = -1L;
    isValid = false;
  }

  /*
  ** METHOD private boolean get()
  **
  ** Loads user data using either userId or loginName.
  **
  ** RETURNS: true if successful, else false.
  */
  private boolean get()
  {
    ResultSetIterator     it        = null;
    ResultSet             rs        = null;
    UserRecord            userData  = new UserRecord(Ctx);
    UserAppType           appBean   = new UserAppType(Ctx);

    boolean               getOk     = false;

    try
    {
      // load a user record by user id
      if (userId != 0L)
      {
        getOk = userData.getData(userId);
      }
      // or login name
      else
      {
        getOk = userData.getData(loginName);
      }

      if (getOk)
      {
        // extract the needed data out of the user record
        userId              = userData.getUserId();
        loginName           = userData.getLoginName();
        userName            = userData.getUserName();
        password            = userData.getPassword();
        passwordEnc         = userData.getPasswordEnc();
        email               = userData.getEmail();
        phone               = userData.getPhone();
        hierarchyNode       = userData.getHNode();
        userType            = userData.getUserType();
        rights              = userData.getRightSet();
        groups              = userData.getGroupSet();
        enabled             = (ignoreStatus ? true : userData.isEnabled());
        reportHierarchyNode = userData.getReportHierarchyNode();

        if(profile == null)
        {
          //profile = new OrgProfile(Ctx);
          profile = new OrgProfile();
        }

        profile.setHierarchyNode(hierarchyNode);

        try
        {
          connect();

          // load org info
          /*@lineinfo:generated-code*//*@lineinfo:334^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  org_num,
//                      org_type_code,
//                      org_group,
//                      org_name
//              from    organization
//              where   org_group = :hierarchyNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  org_num,\n                    org_type_code,\n                    org_group,\n                    org_name\n            from    organization\n            where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.UserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^11*/

          rs = it.getResultSet();
          if (rs.next())
          {
            orgId     = rs.getInt   ("org_num");
            orgName   = rs.getString("org_name");
            if (rs.getString("org_type_code").equals("m"))
            {
              merchId   = rs.getLong  ("org_group");
              merchName = rs.getString("org_name");
            }
          }
          rs.close();
          it.close();

          // load the bank for this user
          if ( merchId > 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:361^13*/

//  ************************************************************
//  #sql [Ctx] { select  mf.bank_number 
//                from    mif   mf
//                where   mf.merchant_number = :merchId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.bank_number  \n              from    mif   mf\n              where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:366^13*/
          }
          else
          {
            bankId = Integer.parseInt( Long.toString(hierarchyNode).substring(0,4) );
          }

          // usertype is passed incase no apptype is found using orgid..
          if(appBean.getAppType(loginName, orgId, (int)userType.getUserTypeId()))
          {
            appType       = appBean.getAppType();
            sequenceType  = appBean.getSequenceType();
            appDesc       = appBean.getAppDesc();
          }
          else //default to mes app
          {
            appType       = com.mes.constants.mesConstants.APP_TYPE_MES;
            appDesc       = "MES";
            sequenceType  = com.mes.constants.mesConstants.SEQUENCE_APPLICATION;
          }
          
          // load password expiration and auto logoff parameters
          /*@lineinfo:generated-code*//*@lineinfo:388^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  case 
//                        when upe.expiration_days is null
//                          then 9999999999
//                        when upe.expiration_days = -1
//                          then 9999999999
//                          else upe.expiration_days
//                      end                             as expiration_days,
//                      nvl(upe.reason,1)               as password_change_reason,
//                      case 
//                        when upe.auto_logoff_expire_minutes is null
//                          then 999999
//                        when upe.auto_logoff_expire_minutes = -1
//                          then 999999
//                        else upe.auto_logoff_expire_minutes
//                      end*1000*60                     as auto_logoff_expire_minutes,
//                      nvl(upe.apply_to_merchants,'Y') as apply_to_merchants,
//                      nvl(upe.force_user_reset_merchant,'N')  as force_user_reset_merchant,
//                      nvl(upe.force_user_reset_user,'N')      as force_user_reset_user,
//                      nvl(upe.max_failed_attempts, :DEFAULT_MAX_FAILED_ATTEMPTS) as max_failed_attempts
//              from    users u,
//                      mif mf,
//                      t_hierarchy th,
//                      users_password_expire upe
//              where   u.login_name = :loginName
//                      and u.hierarchy_node = mf.merchant_number(+)
//                      and nvl(mf.association_node, u.hierarchy_node) = th.descendent
//                      and th.hier_type = 1
//                      and th.ancestor = upe.hierarchy_node
//              order by th.relation                            
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  case \n                      when upe.expiration_days is null\n                        then 9999999999\n                      when upe.expiration_days = -1\n                        then 9999999999\n                        else upe.expiration_days\n                    end                             as expiration_days,\n                    nvl(upe.reason,1)               as password_change_reason,\n                    case \n                      when upe.auto_logoff_expire_minutes is null\n                        then 999999\n                      when upe.auto_logoff_expire_minutes = -1\n                        then 999999\n                      else upe.auto_logoff_expire_minutes\n                    end*1000*60                     as auto_logoff_expire_minutes,\n                    nvl(upe.apply_to_merchants,'Y') as apply_to_merchants,\n                    nvl(upe.force_user_reset_merchant,'N')  as force_user_reset_merchant,\n                    nvl(upe.force_user_reset_user,'N')      as force_user_reset_user,\n                    nvl(upe.max_failed_attempts,  :1 ) as max_failed_attempts\n            from    users u,\n                    mif mf,\n                    t_hierarchy th,\n                    users_password_expire upe\n            where   u.login_name =  :2 \n                    and u.hierarchy_node = mf.merchant_number(+)\n                    and nvl(mf.association_node, u.hierarchy_node) = th.descendent\n                    and th.hier_type = 1\n                    and th.ancestor = upe.hierarchy_node\n            order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DEFAULT_MAX_FAILED_ATTEMPTS);
   __sJT_st.setString(2,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.user.UserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:419^11*/
          
          rs = it.getResultSet();
          
          if( rs.next() )
          {
            expirationDays = rs.getInt("expiration_days");
            passwordChangeReason = rs.getInt("password_change_reason");
            autoLogoffExpireMinutes = rs.getLong("auto_logoff_expire_minutes");
            applyToMerchants = ("Y").equals(rs.getString("apply_to_merchants"));
            forceUserResetMerchant = ("Y").equals(rs.getString("force_user_reset_merchant"));
            forceUserResetUser = ("Y").equals(rs.getString("force_user_reset_user"));
            maxFailedAttempts = rs.getInt("max_failed_attempts");
          }
          
          rs.close();
          it.close();
        }
        catch (Exception e)
        {
          logEntry("get()", e.toString());
        }
        finally
        {
          try { rs.close(); } catch(Exception e) {}
          try { it.close(); } catch(Exception e) {}

          cleanUp();
        }
      }
    }
    catch(Exception e)
    {
      logEntry("get()", e.toString());
    }

    return getOk;
  }

  /*
  ** METHOD public boolean getData(long getId)
  **
  ** Loads user data using a user id.
  **
  ** RETURNS: true if successful.
  */
  public synchronized boolean getData(long getId)
  {
    userId = getId;
    loginName = "";
    isValid = (get() && enabled);
    if (!isValid)
    {
      clear();
    }
    return isValid;
  }

  // HACK ALERT -- this method is to ensure that disabled Verisign Long app
  // ids do not crash the freaking server
  public synchronized boolean getVSData(long getId)
  {
    userId = getId;
    loginName = "";
    isValid = get();

    if(!isValid)
    {
      clear();
    }
    return isValid;
  }

  /*
  ** METHOD public boolean getData(String getName)
  **
  ** Loads user data using a user's login name.
  **
  ** RETURNS: true if successful.
  */
  public synchronized boolean getData(String getName)
  {
    loginName = getName;
    userId = 0L;
    isValid = (get() && enabled);
    if (!isValid)
    {
      clear();
    }
    return isValid;
  }

  /*
  ** METHOD protected void updateLastLogin()
  **
  ** Updates the current user's last login date to be now.
  */
  protected void updateLastLogin()
  {
    try
    {
      connect();

      int count = 0;
      //System.out.println("getting user count");
      /*@lineinfo:generated-code*//*@lineinfo:524^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_id) 
//          from    users
//          where   user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_id)  \n        from    users\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:529^7*/

      if(count > 0)
      {
        //System.out.println("updating user table with last login");
        /*@lineinfo:generated-code*//*@lineinfo:534^9*/

//  ************************************************************
//  #sql [Ctx] { update  users
//            set     last_login_date = sysdate,
//                    failed_attempts = 0
//            where   user_id = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n          set     last_login_date = sysdate,\n                  failed_attempts = 0\n          where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^9*/
      }
    }
    catch (Exception e)
    {
      logEntry("updateLastLogin()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    //System.out.println("returning from updateLastLogin");
  }

  public void addFailedAttempt(String loginName, String password)
  {
    try
    {
      connect();

      // log the failed attempt
      /*@lineinfo:generated-code*//*@lineinfo:561^7*/

//  ************************************************************
//  #sql [Ctx] { insert into access_failed_attempts
//          (
//            login_name,
//            password_hash
//          )
//          values
//          (
//            :loginName,
//            :password
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into access_failed_attempts\n        (\n          login_name,\n          password_hash\n        )\n        values\n        (\n           :1 ,\n           :2 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,password);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:573^7*/

      // only do the following failed attempts if the login name is valid
      int userCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:578^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//          
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:584^7*/

      if(userCount > 0)
      {
        // increment the FAILED_ATTEMPTS column
        /*@lineinfo:generated-code*//*@lineinfo:589^9*/

//  ************************************************************
//  #sql [Ctx] { update  users
//            set     failed_attempts = failed_attempts + 1
//            where   login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n          set     failed_attempts = failed_attempts + 1\n          where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:594^9*/

        // determine if account should be locked out
        int failedAttempts;

        /*@lineinfo:generated-code*//*@lineinfo:599^9*/

//  ************************************************************
//  #sql [Ctx] { select  failed_attempts
//            
//            from    users
//            where   login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  failed_attempts\n           \n          from    users\n          where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   failedAttempts = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^9*/

        if( failedAttempts >= maxFailedAttempts )
        {
          // lock out this user
          /*@lineinfo:generated-code*//*@lineinfo:610^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     enabled = 'N'
//              where   login_name = :loginName
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n            set     enabled = 'N'\n            where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("addFailedAttempt(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD public boolean getData(String getName, String getPass)
  **
  ** Loads user data using a login name then checks to see if the password
  ** given matches that user's password.  If password matches the user's
  ** last login date is updated.
  **
  ** RETURNS: true if user found with matching password, else false.
  */
  public synchronized boolean getData(String getName, String getPass)
  {
    isValid = false;
    loginName = getName;
    userId = 0L;

    // get hash of password
    md5.Init();
    md5.Update(getPass);

    String passHash = md5.asHex();

    //System.out.println("getting base class data");
    isValid = (get() && passHash.equals(passwordEnc) && enabled);

    //System.out.println("checking isValid");
    if (!isValid)
    {
      clear();

      // update failed_attempts and lockout if necessary
      //System.out.println("adding failed attempt ");
      addFailedAttempt(getName, getPass);
    }
    else
    {
      //System.out.println("updating last login");
      updateLastLogin();
    }

    //System.out.println("returning from getData");
    return isValid;
  }

  /*
  ** METHOD boolean isType(UserType type)
  **
  ** RETURNS: true if user is of the type specified.
  */
  public synchronized boolean isType(UserType type)
  {
    return userType.equals(type);
  }
  public synchronized boolean isType(long typeId)
  {
    return isType(new UserType(typeId));
  }

  /*
  ** METHOD boolean isMember(Group group)
  **
  ** RETURNS: true if user is a member of group.
  */
  public synchronized boolean isMember(Group group)
  {
    return groups.contains(group);
  }
  public synchronized boolean isMember(long groupId)
  {
    return isMember(new Group(groupId));
  }

  /*
  ** METHOD boolean hasRight(Right right)
  **
  ** RETURNS: true if user has given right.
  */
  public synchronized boolean hasRight(Right right)
  {
    return rights.contains(right);
  }
  public synchronized boolean hasRight(long rightId)
  {
    return hasRight(new Right(rightId));
  }
  public synchronized boolean hasRight(String rightString)
  {
    if(rightString!= null)
    {
      //check the String to ensure that it doesn't have
      //a comma (standard DB syntax) seperated list of rights
      String comma = ",";

      if(rightString.indexOf(comma)>0)
      {
        return hasRight(rightString, comma);
      }
      else
      {
        return hasRight(rightString, null);
      }
    }
    else
    {
      return true;
    }
  }

  public synchronized boolean hasRight(String rightString, String regex)
  {
    try
    {
      ArrayList rights = new ArrayList();

      if(regex!=null)
      {
        //once we move to 1.4 we should go to .split()
        //rights.addAll(Arrays.asList(rightString.split(regex)));
        StringTokenizer tok = new StringTokenizer(rightString,regex);
        while (tok.hasMoreTokens())
        {
          rights.add(tok.nextToken().trim());
        }
      }
      else
      {
        rights.add(rightString);
      }

      long right;

      //just need to find one...
      for(int i=0; i<rights.size();i++)
      {
        right = Long.parseLong((String)rights.get(i));
        if(hasRight(right))
        {
          return true;
        }
      }
    }
    catch (Exception e)
    {}

    return false;
  }


  /*
  ** ACCESSORS
  */
  public synchronized OrgProfile getProfile()
  {
    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }
    }
    catch(Exception e)
    {
      logEntry("getProfile()", e.toString());
    }

    return this.profile;
  }

  public synchronized String getProfileValue(String key)
  {
    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }
    }
    catch(Exception e)
    {
      logEntry("getProfileValue(" + key + ")", e.toString());
    }

    return profile.getElement(key);
  }

  public synchronized String getHtmlTitle()
  {
    String result = "Merchant Services Online Reporting";

    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }

      result = profile.getElement(OrgProfile.EK_HTML_TITLE);
    }
    catch(Exception e)
    {
      logEntry("getHtmlTitle()", e.toString());
    }

    return result;
  }

  public synchronized String getStyleSheet()
  {
    String result = "/ss/mes_default.css";
    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }

      result = profile.getElement(OrgProfile.EK_STYLE_SHEET);
    }
    catch(Exception e)
    {
      logEntry("getStyleSheet()", e.toString());
    }

    return result;
  }

  public synchronized String getSecureHeader()
  {
    String result = "/jsp/include/secure_navigation_top.jsp";
    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }

      result = profile.getElement(OrgProfile.EK_HEADER_NAME);
    }
    catch(Exception e)
    {
      logEntry("geSecureHeader()", e.toString());
    }

    return result;
  }

  public synchronized String getSecureFooter()
  {
    String result = "/jsp/include/secure_navigation_bot.jsp";

    try
    {
      if(profile == null)
      {
        //profile = new OrgProfile(Ctx);
        profile = new OrgProfile();
        profile.setHierarchyNode(hierarchyNode);
      }

      result = profile.getElement(OrgProfile.EK_FOOTER_NAME);
    }
    catch(Exception e)
    {
      logEntry("getStyleSheet()", e.toString());
    }

    return result;
  }

  public synchronized void setQueueLockData(long id, int type, Timestamp lockDate)
  {
    try
    {
      queueLock.setData(id, type, lockDate);
    }
    catch(Exception e)
    {
      logEntry("setQueueLockItem(" + id + ", " + type + ")", e.toString());
    }
  }

  public UserQueueLockItem getQueueLockData()
  {
    return queueLock;
  }

  public String getSecureImage()
  {
    return profile.getElement(OrgProfile.EK_IMAGE_NAME);
  }

  public String getSecureImageWidth()
  {
    return profile.getElement(OrgProfile.EK_IMAGE_WIDTH);
  }

  public String getSecureImageHeight()
  {
    return profile.getElement(OrgProfile.EK_IMAGE_HEIGHT);
  }

  public long getUserId()
  {
    return userId;
  }

  public long getSequenceType()
  {
    return this.sequenceType;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public String getAppDesc()
  {
    return this.appDesc;
  }

  public String getPasswordEnc()
  {
    return( passwordEnc );
  }
  public String getLoginName()
  {
    return loginName;
  }
  public String getUserName()
  {
    return userName;
  }
  public String getEmail()
  {
    return email;
  }
  public String getPhone()
  {
    return phone;
  }
  public int getBankNumber()
  {
    int result = 0;
    
    if( getHierarchyNode() == 9999999999L )
    {
      result = 3941;
    }
    else
    {
      result = Integer.parseInt( Long.toString(getHierarchyNode()).substring(0, 4) );
    }
    
    return( result );
  }
  public long getHierarchyNode()
  {
    return( getHierarchyNode( MesHierarchy.HT_BANK_PORTFOLIOS ) );
  }

  public synchronized long getHierarchyNode( int hierType )
  {
    long      retVal      = 0L;
    long      userId      = getUserId();

    try
    {
      // always connect for cleanUp() in finally block
      connect();

      if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
      {
        // hierarchy is pre-loaded
        retVal = hierarchyNode;
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1016^9*/

//  ************************************************************
//  #sql [Ctx] { select  hierarchy_node 
//            from    user_hierarchy
//            where   user_id = :userId and
//                    hier_type = :hierType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  hierarchy_node  \n          from    user_hierarchy\n          where   user_id =  :1  and\n                  hier_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setInt(2,hierType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1022^9*/
      }
    }
    catch(Exception e)
    {
      // ignore, just return 0L
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  public synchronized void setHierarchyNode(String newNode)
  {
    try
    {
      setHierarchyNode(Long.parseLong(newNode));
    }
    catch(Exception e)
    {
      logEntry("setHierarchyNode(" + newNode + ")", e.toString());
    }
  }
  public void setHierarchyNode(long newNode)
  {
    hierarchyNode = newNode;
  }
  public long getReportHierarchyNode()
  {
    return reportHierarchyNode;
  }
  private synchronized void setLastResource(String resource)
  {
    this.lastResource = resource;
  }
  public synchronized String getLastResource()
  {
    return this.lastResource;
  }
  private synchronized void setLastResourceParams(String resourceParams)
  {
    this.lastResourceParams = resourceParams;
  }
  public synchronized String getLastResourceParams()
  {
    return this.lastResourceParams;
  }
  private void setLastAccessed()
  {
    this.lastAccessed = Calendar.getInstance().getTime();
  }
  public synchronized String getLastAccessedString()
  {
    return DateTimeFormatter.getFormattedDate(lastAccessed, DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
  }
  public synchronized Date getLastAccessed()
  {
    return this.lastAccessed;
  }
  private void logResourceAccess()
  {
    try
    {
      connect();

      String paramStr = getLastResourceParams();

      if(paramStr == null)
      {
        paramStr = "";
      }
      else
      {
        paramStr = paramStr.substring(0, paramStr.length() > 4000 ? 4000 : paramStr.length());
      }
      
      paramStr = PasswordRedactor.redactPassword(paramStr, redactFieldNames);
      
      if( sessionId != null )
      {
        sessionId = (sessionId.length() > 75 ? sessionId.substring(0, 75) : sessionId);
      }
      
      // strip off potential proxy addresses from ip address
      /*@lineinfo:generated-code*//*@lineinfo:1108^7*/

//  ************************************************************
//  #sql [Ctx] { select  substr(:ipAddress, 1, decode(instr(:ipAddress, ','), 0, length(:ipAddress), instr(:ipAddress, ',')-1))
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  substr( :1 , 1, decode(instr( :2 , ','), 0, length( :3 ), instr( :4 , ',')-1))\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,ipAddress);
   __sJT_st.setString(2,ipAddress);
   __sJT_st.setString(3,ipAddress);
   __sJT_st.setString(4,ipAddress);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ipAddress = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1113^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1115^7*/

//  ************************************************************
//  #sql [Ctx] { insert into access_details
//          (
//            login_name,
//            server_name,
//            access_date,
//            jsp_name,
//            jsp_params,
//            ip_address,
//            session_id
//          )
//          values
//          (
//            substr(:loginName, 1, 75),
//            substr(:serverName, 1, 40),
//            sysdate,
//            substr(:lastResource, 1, 100),
//            :paramStr,
//            :ipAddress,
//            :sessionId
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into access_details\n        (\n          login_name,\n          server_name,\n          access_date,\n          jsp_name,\n          jsp_params,\n          ip_address,\n          session_id\n        )\n        values\n        (\n          substr( :1 , 1, 75),\n          substr( :2 , 1, 40),\n          sysdate,\n          substr( :3 , 1, 100),\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,serverName);
   __sJT_st.setString(3,lastResource);
   __sJT_st.setString(4,paramStr);
   __sJT_st.setString(5,ipAddress);
   __sJT_st.setString(6,sessionId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1137^7*/
    }
    catch(Exception e)
    {
      logEntry("logResourceAccess()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public synchronized void logAccessDetails(HttpServletRequest request)
  {
    try
    {
      connect();

      setLastAccessed();
      setLastResource(request.getServletPath());
      
      // retrieve IP address and session id
      try
      {
        ipAddress = request.getHeader("X-Forwarded-For");
      }
      catch(Exception e)
      {
        ipAddress = "UNKNOWN";
      }
      
      try
      {
        Cookie[] cookies = request.getCookies();
        
        if( null != cookies )
        {
          for( int i = 0; i < cookies.length; ++i )
          {
            Cookie c = cookies [i];
            
            if( c.getName().equals( SESSION_COOKIE_NAME ) )
            {
              sessionId = c.getValue();
            }
          }
        }
        
      }
      catch(Exception e)
      {
        sessionId = "UNKNOWN";
      }
      
      StringBuffer temp = new StringBuffer("");
      Enumeration myEnum = request.getParameterNames();
      while(myEnum.hasMoreElements())
      {
        String name = (String)myEnum.nextElement();
        temp.append(name);
        temp.append("=");
        String values[] = request.getParameterValues(name);
        for(int i=0; i<values.length; ++i)
        {
          if(i > 0)
          {
            temp.append(",");
          }

          temp.append( CardTruncator.truncate(values[i]) );
        }
        temp.append(";");
      }

      setLastResourceParams( temp.toString() );
      logResourceAccess();
    }
    catch(Exception e)
    {
      logEntry("logAccessDetails()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  // this method returns the 4 digit TSYS bank number
  // for the current login node.
  public int getOrgBankId()
  {
    return( bankId );
  }
  public int getOrgId()
  {
    return orgId;
  }
  public String getOrgName()
  {
    StringBuffer        retVal = new StringBuffer("");

    // create a "# - name" string
    retVal.append( getHierarchyNode() );
    retVal.append( " - " );
    retVal.append( orgName );

    return( retVal.toString() );
  }
  public long getMerchId()
  {
    return merchId;
  }
  public String getMerchName()
  {
    return merchName;
  }
  public UserType getUserType()
  {
    return userType;
  }
  public long getUserTypeId()
  {
    return userType.getUserTypeId();
  }
  public RightSet getRights()
  {
    return rights;
  }
  public GroupSet getGroups()
  {
    return groups;
  }
  public boolean isValid()
  {
    return isValid;
  }

  /*
  ** public boolean isUnderHierarchyNode(long parentNode)
  **
  ** Determines if the user's hierarchy node falls under the hierarchy node given.
  **
  ** RETURNS: true if user falls under the parent node, else false.
  */
  public synchronized boolean isUnderHierarchyNode(long parentNode)
  {
    boolean isUnder = false;

    try
    {
      connect();

      int count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1290^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          from    t_hierarchy
//          where   ancestor = :parentNode
//                  and descendent = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        from    t_hierarchy\n        where   ancestor =  :1 \n                and descendent =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,parentNode);
   __sJT_st.setLong(2,hierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1296^7*/

      isUnder = count > 0;
    }
    catch (Exception e)
    {
      logEntry("isUnderHierarchyNode(parentNode=" + parentNode + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return isUnder;
  }

  /*
  ** public boolean isMerchantUser
  **
  ** Retrns true if user is assigned to a merchant number level of the hierarchy,
  ** otherwise false
  */
  public boolean isMerchantUser()
  {
    boolean result = false;

    try
    {
      connect();

      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:1328^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)
//          
//          from    users u,
//                  mif mf
//          where   u.login_name = :loginName and
//                  u.hierarchy_node = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)\n         \n        from    users u,\n                mif mf\n        where   u.login_name =  :1  and\n                u.hierarchy_node = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1336^7*/

      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("isMerchantUser(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return( result );
  }

  /*
  ** METHOD public void logAccess(HttpServletRequest request, String result)
  **
  ** Logs access attempts.
  */
  public void logAccess(String userId, HttpServletRequest request, String result)
  {
    if (request != null && ! ServerTools.isDevelopmentServer())
    {
      try
      {
        //System.out.println(" ** - getRemoteHost()");
        // try to get location from special header
        String location = "unknown";

        location = request.getHeader(com.mes.constants.mesConstants.REQUEST_HEADER_CLIENT_IP);

        if(location == null || location.equals(""))
        {
          location = request.getRemoteAddr();
        }

        //System.out.println(" ** - getServletPath()");
        String resource = request.getServletPath();

        //System.out.println(" ** - checking for null servletPath, getting query string");
        if(resource == null)
        {
          //System.out.println(" ** - getServletPath() returned null");
          resource = "Direct Access";
        }

        //System.out.println(" ** - connecting to database");
        // insert the entry into the access_log table
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1388^9*/

//  ************************************************************
//  #sql [Ctx] { insert into access_log
//            (
//              access_id,
//              access_date,
//              access_location,
//              access_resource,
//              access_result
//            )
//            values
//            (
//              :userId,
//              sysdate,
//              :location,
//              :resource,
//              :result
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into access_log\n          (\n            access_id,\n            access_date,\n            access_location,\n            access_resource,\n            access_result\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userId);
   __sJT_st.setString(2,location);
   __sJT_st.setString(3,resource);
   __sJT_st.setString(4,result);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1406^9*/

        // check for an outside ip address to update browser type stats
        String ipAddr = request.getHeader(com.mes.constants.mesConstants.REQUEST_HEADER_CLIENT_IP);

        if(ipAddr != null && !ipAddr.substring(0, 3).equals("10."))
        {
          // get browser info
          String browser = request.getHeader("User-Agent");

          if(browser != null && !browser.equals(""))
          {
            int existingCount = 0;

            /*@lineinfo:generated-code*//*@lineinfo:1420^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_agent)
//                
//                from    client_browser_stats
//                where   user_agent = :browser
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_agent)\n               \n              from    client_browser_stats\n              where   user_agent =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.user.UserBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,browser);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   existingCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1426^13*/

            if(existingCount == 0)
            {
              // insert new record
              /*@lineinfo:generated-code*//*@lineinfo:1431^15*/

//  ************************************************************
//  #sql [Ctx] { insert into client_browser_stats
//                  (
//                    user_agent,
//                    access_count
//                  )
//                  values
//                  (
//                    :browser,
//                    1
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into client_browser_stats\n                (\n                  user_agent,\n                  access_count\n                )\n                values\n                (\n                   :1 ,\n                  1\n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,browser);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1443^15*/
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:1447^15*/

//  ************************************************************
//  #sql [Ctx] { update  client_browser_stats
//                  set     access_count = access_count + 1
//                  where   user_agent = :browser
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  client_browser_stats\n                set     access_count = access_count + 1\n                where   user_agent =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,browser);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1452^15*/
            }
          }
        }
      }
      catch (Exception e)
      {
        logEntry("logAccess()", e.toString());
      }
      finally
      {
        //System.out.println(" ** - disconnecting");
        cleanUp();
      }
    }
  }

  public static void _logAccess(String userId, HttpServletRequest request, String result)
  {
    try
    {
      UserBean worker = new UserBean();

      worker.logAccess(userId, request, result);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("static UserBean._logAccess()", e.toString());
    }
  }

  /*
  ** Legacy routines
  */
  public String getUserLoginId()
  {
    return loginName;
  }
  public boolean isUserMerchant()
  {
    return isMember(MesUsers.GROUP_MERCHANT);
  }
  public boolean isUserBank()
  {
    return isMember(MesUsers.GROUP_BANK);
  }
  public boolean isVerisignUser()
  {
    return isMember(MesUsers.GROUP_VERISIGN);
  }
  public boolean isDemoUser()
  {
    return isMember(MesUsers.GROUP_DEMO);
  }
  public boolean isCbtUser()
  {
    return isMember(MesUsers.GROUP_CBT);
  }
  public boolean isCedarUser()
  {
    return isMember(MesUsers.GROUP_CEDAR);
  }

  public void invalidate( )
  {
    clear();
  }

  public synchronized boolean autoLogoff()
  {
    boolean             result      = false;

    if(lastAccessed != null)
    {
      try
      {
        // determine if it has been longer than X minutes since last access
        long last = lastAccessed.getTime();
        long cur = Calendar.getInstance().getTime().getTime();

        if( (cur - last) > autoLogoffExpireMinutes )
        {
          result = true;
        }
      }
      catch(Exception e)
      {
        logEntry("autoLogoff()", e.toString());
      }
    }

    return( result );
  }

  public synchronized boolean passwordExpired()
  {
    boolean             result      = false;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      connect();

      // see if this user is subject to password expiration
      /*@lineinfo:generated-code*//*@lineinfo:1557^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate - u.pswd_change_date) days_since
//          from    users  u
//          where   u.login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate - u.pswd_change_date) days_since\n        from    users  u\n        where   u.login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.user.UserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1562^7*/

      rs = it.getResultSet();

      if( rs.next() && (rs.getInt("days_since") > expirationDays) )
      {
        result = true;
      }
      rs.close();
      it.close();

      //
      // did not expire using the days method, check to see if
      // this is a first time login situation and it the user is
      // required to comply with a first time login pw change
      //
      // Example:  Comdata wants to have no expiration date but
      //           wants the user to change their password on the
      //           first login.  Therefore the user password expire
      //           for Comdata disabled (enabled = N) and therefore
      //           another mechanism was required to cause the first
      //           time login password change.
      //
      if ( result == false )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1587^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  upr.reason            as reason
//            from    users_password_ft_reset upr,
//                    t_hierarchy             th,
//                    users                   u,
//                    mif                     m
//            where   th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                    th.ancestor = upr.hierarchy_node and
//                    upr.enabled = 'Y' and
//                    u.hierarchy_node = m.merchant_number(+) and
//                    (
//                      (
//                        m.merchant_number is null and
//                        th.descendent = u.hierarchy_node
//                      ) or
//                      (
//                        th.descendent = m.association_node and
//                        m.merchant_number = u.hierarchy_node
//                      )
//                    ) and
//                    u.login_name = :loginName and
//                    u.pswd_change_date = u.create_date -- new user
//            order by th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upr.reason            as reason\n          from    users_password_ft_reset upr,\n                  t_hierarchy             th,\n                  users                   u,\n                  mif                     m\n          where   th.hier_type =  :1  and\n                  th.ancestor = upr.hierarchy_node and\n                  upr.enabled = 'Y' and\n                  u.hierarchy_node = m.merchant_number(+) and\n                  (\n                    (\n                      m.merchant_number is null and\n                      th.descendent = u.hierarchy_node\n                    ) or\n                    (\n                      th.descendent = m.association_node and\n                      m.merchant_number = u.hierarchy_node\n                    )\n                  ) and\n                  u.login_name =  :2  and\n                  u.pswd_change_date = u.create_date -- new user\n          order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.user.UserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setString(2,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.user.UserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1611^9*/
        rs = it.getResultSet();

        if(rs.next())
        {
          result = true;
        }
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("passwordExpired()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}

      cleanUp();
    }

    return( result );
  }

  public String getPasswordChangeReason()
  {
    return Integer.toString(passwordChangeReason);
  }

  private boolean isValidated(HttpServletRequest request)
  {
    return isValid;
  }
  public boolean validated(HttpServletRequest request)
  {
    return(isValidated(request));
  }

  public synchronized boolean validate(String userId, String password, HttpServletRequest request)
  {
    boolean result  = false;

    try
    {
      connect();

      result = getData(userId, password);

      if(result)
      {
        logAccess(userId, request, "Successful Login");

        commit();

        setLastAccessed();

        if(newSession)
        {
          String ipAddr = request.getHeader(com.mes.constants.mesConstants.REQUEST_HEADER_CLIENT_IP);
          if(ipAddr == null || ipAddr.equals(""))
          {
            ipAddr = request.getRemoteAddr();
          }

          /* LoginManager does not work in clustered environment
          // ignore testing user id
          if(userId != null && !userId.equals("bandwidthtest"))
          {
            LoginManager.addLogin(this, this.loginSession, ipAddr);
          }
          */
        }
      }
      else
      {
        //System.out.println(" ** - invalid user login");
        if(userId != null && !userId.equals(""))
        {
          //System.out.println(" ** - logging invalid access");
          logAccess(userId, request, "Invalid User ID or Password");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("validate(" + userId + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public synchronized void addLoginCookie(HttpServletResponse response, int clientId)
  {
    try
    {
      // simple -- just add the cookie to the browser
      ClientLoginCookie   loginCookie = new ClientLoginCookie(clientId);

      response.addCookie(loginCookie.getCookie());
    }
    catch(Exception e)
    {
      logEntry("addLoginCookie(" + clientId + ")", e.toString());
    }
  }

  public HttpSession getSession()
  {
    return this.loginSession;
  }

  public synchronized boolean forceValidate(String getName, HttpServletRequest request)
  {
    loginName = getName;
    userId = 0L;

    ignoreStatus = true;

    isValid = get();

    if( isValid )
    {
      updateLastLogin();
      logAccess(getName, request, "Successful Login");
    }
    else
    {
      logAccess(getName, request, "Invalid user name (force)");
    }

    return isValid;
  }

  public synchronized boolean forceValidate(String getName)
  {
    return forceValidate(getName, null);
  }

  ///once tested this will be replace by setappuser
  public synchronized boolean setVerisignUser(long vsUserId)
  {
    if (!getData(vsUserId) || !isMember(MesUsers.GROUP_VERISIGN))
    {
      clear();
    }
    return isValid;
  }

  //this is a more generic version of setVerisignUser and setDemoUser.. used for all users
  //it checks to see if the id is valid and that they have application proxy rights...
  //if so, they are allowed to see the application
  //*****this will also validate to see that they are allowed to view the 2 page application****
  public synchronized boolean setAppUser(long userId)
  {
    if (!getVSData(userId) || !functionValid(mesConstants.FUNC_APPLICATION_PROXY) || this.sequenceType != mesConstants.SEQUENCE_ID_APPLICATION)
    {
      clear();
    }
    return isValid;
  }

  //once tested this will be replaced by setappuser
  public  synchronized boolean setDemoUser(long demoUserId)
  {
    if (!getData(demoUserId) || !isMember(MesUsers.GROUP_DEMO))
    {
      clear();
    }
    return isValid;
  }

 /*
  ** METHOD private boolean isFunctionValid(long funcNum, HttpServletRequest request)
  **
  ** This legacy routine supports older jsp's that use FUNC_* constants to
  ** determine if a user has rights to access the page.
  */
  private boolean isFunctionValid(long funcNum, HttpServletRequest request)
  {
    // scan through the funcNum bitfield and look for rights that correspond
    // with the old FUNC_* style constants
    boolean hasRight = false;
    for (long i = 1; i < mesConstants.FUNC_APPLICATION_DEMO << 1 && !hasRight; i <<= 1)
    {
      if ((i & funcNum) != 0L)
      {
        switch ((int)i)
        {
          // basic permissions
          case (int)mesConstants.FUNC_PROFILE_CHANGE:
            hasRight = rights.contains(MesUsers.RIGHT_PROFILE_CHANGE);
            break;

          // mes permissions
          case (int)mesConstants.FUNC_ACCOUNT_SERVICING:
            hasRight = rights.contains(MesUsers.RIGHT_ACCOUNT_SERVICING);
            break;

          case (int)mesConstants.FUNC_SALES_ADMINISTRATION:
            hasRight = rights.contains(MesUsers.RIGHT_SALES_ADMIN);
            break;

          case (int)mesConstants.FUNC_CUSTOMER_SERVICE:
            hasRight = rights.contains(MesUsers.RIGHT_USER_ADMIN_PEON);
            break;

          case (int)mesConstants.FUNC_SYSTEM_ADMIN:
            hasRight = rights.contains(MesUsers.RIGHT_SYSTEM_ADMIN);
            break;

          // application permissions
          case (int)mesConstants.FUNC_APPLICATION_PROXY:
            hasRight = rights.contains(MesUsers.RIGHT_APPLICATION_PROXY);
            break;

          case (int)mesConstants.FUNC_APPLICATION_VIEW:
            hasRight = rights.contains(MesUsers.RIGHT_APPLICATION_MGR);
            break;

          case (int)mesConstants.FUNC_APPLICATION_DEMO:
            hasRight = rights.contains(MesUsers.RIGHT_APPLICATION_SUPER);
            break;

          // reporting permissions
          case (int)mesConstants.FUNC_REPORT_MERCH:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_MERCH);
            break;

          case (int)mesConstants.FUNC_REPORT_ADMIN:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_ADMIN);
            break;

          case (int)mesConstants.FUNC_REPORT_BANK:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_BANK);
            break;

          case (int)mesConstants.FUNC_REPORT_SALES:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_COMMISSION);
            break;

          case (int)mesConstants.FUNC_REPORT_HR:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_HR);
            break;

          case (int)mesConstants.FUNC_REPORT_MIS:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_MIS);
            break;

          case (int)mesConstants.FUNC_REPORT_ACCOUNTING:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_ACCOUNTING);
            break;

          case (int)mesConstants.FUNC_REPORT_MANAGEMENT:
            hasRight = rights.contains(MesUsers.RIGHT_REPORT_MANAGEMENT);
            break;
        }
      }
    }

    //logAccess(request,(hasRight ? "Success" : "Insufficient Access"));

    return hasRight;
  }
  public synchronized boolean functionValid(long funcNum)
  {
    return(isFunctionValid(funcNum, null));
  }
  public synchronized boolean functionValid(long funcNum, HttpServletRequest request)
  {
    return isFunctionValid(funcNum, request);
  }
  public synchronized boolean isEnabled()
  {
    return enabled;
  }
}/*@lineinfo:generated-code*/