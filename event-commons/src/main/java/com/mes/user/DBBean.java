/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/DBBean.java $

  Description:  
  
    DbBean

    Provides some common built in db connectivity to child page beans.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/06/02 4:51p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;

public abstract class DBBean extends SQLJConnectionBase
{
  private   Vector              errors            = new Vector();
  private   String              submitVal         = "";

  /*
  ** METHOD public final boolean getData()
  **
  ** Calls the child class defined get().
  **
  ** RETURNS: result of get.
  */
  public final boolean getData()
  {
    boolean getOk = false;
    
    try
    {
      connect();
      getOk = get();
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return getOk;
  }
  protected abstract boolean get();
  
  /*
  ** METHOD public final boolean submitData()
  **
  ** Calls the child class defined submit().
  **
  ** RETURNS: result of submit.
  */
  public final boolean submitData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
    
      submitOk = submit();
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }  
    
    return submitOk;
  }
  protected abstract boolean submit();
  
  /*
  ** METHOD public final boolean validateData()
  **
  ** Calls the child class defined validate().
  **
  ** RETURNS: result of validate.
  */
  public final boolean validateData()
  {
    boolean validateOk = false;
    
    try
    {
      connect();
    
      validateOk = validate();
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return validateOk;
  }
  protected abstract boolean validate();
  
  /*
  ** METHOD public boolean deleteData()
  **
  ** Calls the child class deined delete().
  **
  ** RETURNS: result of delete.
  */
  public boolean deleteData()
  {
    boolean deleteOk = false;
    
    try
    {
      connect();
      
      deleteOk = delete();
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return deleteOk;
  }
  protected abstract boolean delete();
  
  /*
  ** METHOD Vector getErrors()
  **
  ** RETURNS: the errors vector.
  */
  public Vector getErrors()
  {
    return errors;
  }
  
  /*
  ** METHOD void addError(String error)
  **
  ** Adds an error string to the error vector.
  */
  protected void addError(String error)
  {
    try
    {
      errors.add(error);
    }
    catch(Exception e)
    {
      logEntry("addError()", e.toString());
    }
  }
  
  /*
  ** METHOD boolean hasErrors()
  **
  ** RETURNS: true if error vector is not empty.
  */
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  
  /*
  ** HELPERS
  */

  /*
  ** METHOD protected Date getSqlDate(String dateString)
  **
  ** Converts a string representing a date (must be in the format of mm/dd/yy or 
  ** mm/dd/yyyy) into a java.sql.Date.
  **
  ** RETURNS: a Date.
  */
  protected Date getSqlDate(String dateString)
  {
    java.util.Date              utilDate      = null;
    java.sql.Date               sqlDate       = null;
    String                      month         = "";
    String                      day           = "";
    String                      year          = "";
    StringBuffer                massagedDate  = new StringBuffer("");
    DateFormat                  dateFormat    = 
      new SimpleDateFormat("mm/dd/yyyy");
    java.util.StringTokenizer   st            = 
      new java.util.StringTokenizer(dateString,"/");
    
    // build a formatted date string from given dateString
    if(st.hasMoreTokens())
    {
      month = st.nextToken();
    }
    if(st.hasMoreTokens())
    {
      day = st.nextToken();
    }
    if(st.hasMoreTokens())
    {
      year = st.nextToken();
      if(year.length() == 2)
      {
        year = new String("20" + year);
      }
    }
    
    massagedDate.append(month);
    massagedDate.append("/");
    massagedDate.append(day);
    massagedDate.append("/");
    massagedDate.append(year);
    
    // convert the formatted date string to a java.util.date (millisecs since 1-1-1970)
    try
    {
      if (massagedDate == null || massagedDate.length() != 10)
      {
        utilDate = new java.util.Date();
      }
      else
      {
        utilDate = dateFormat.parse(massagedDate.toString());
      }
    }
    catch(Exception e)
    {
      logEntry("getSqlDate()", e.toString());
    }
    
    // convert java.util.date to java.sql.date
    if(utilDate != null)
    {
      sqlDate = new java.sql.Date(utilDate.getTime());
    }
    
    return sqlDate;
  }

  /*
  ** METHOD public long parseLong(String val)
  **
  ** Converts a string into a long.
  **
  ** RETURNS: the converted long value.
  */
  public long parseLong(String val)
  {
    long rv;
    try
    {
      rv = Long.parseLong(val);
    }
    catch (Exception e)
    {
      rv = 0;
    }
    return rv;
  }

  /*
  ** METHOD public double parseDouble(String val)
  **
  ** Converts a string into a double.
  **
  ** RETURNS: the converted double value.
  */
  public double parseDouble(String val)
  {
    double rv;
    try
    {
      rv = Double.parseDouble(val);
    }
    catch (Exception e)
    {
      rv = 0.0;
    }
    return rv;
  }
  
  /*
  ** METHOD public int parseInt(String val)
  **
  ** Converts a string into an int.
  **
  ** RETURNS: the converted int value.
  */
  public int parseInt(String val)
  {
    int rv;
    try
    {
      rv = Integer.parseInt(val);
    }
    catch (Exception e)
    {
      rv = 0;
    }
    return rv;
  }
  
  /*
  ** ACCESSORS
  */
  public String getSubmitVal()
  {
    return submitVal;
  }
  public void setSubmitVal(String newVal)
  {
    submitVal = newVal;
  }
}
