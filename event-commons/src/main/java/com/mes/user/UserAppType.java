/*@lineinfo:filename=UserAppType*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserAppType.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/03/04 2:42p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class UserAppType extends com.mes.database.SQLJConnectionBase
{
  private int     appType         = 0;
  private int     sequenceType    = 0;
  private String  appDesc         = "";
  
  public UserAppType()
  {
  }
  
  public UserAppType(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
  }
  
  /*
  ** METHOD orgExists
  **
  ** Returns true if the specified org_num exists in the organization table
  */
  public boolean orgExists(int orgNum)
  {
    int       count       = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:60^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(org_num)  
//          from    organization
//          where   org_num = :orgNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(org_num)   \n        from    organization\n        where   org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserAppType",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,orgNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:65^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::orgExists()", orgNum + ": " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return (count > 0);
  }
  
  public long getHierarchyNode(int orgNum)
  {
    long    hierarchyNode = 0L;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:87^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_group 
//          from    organization
//          where   org_num = :orgNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_group  \n        from    organization\n        where   org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserAppType",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,orgNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   hierarchyNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getHierarchyNode()", orgNum + ": " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return hierarchyNode;
  }

  public long getUsableNode(int orgNum)
  {
    long                usableNode  = 0L;

    try
    {
      connect();

      String  orgType = "";
      long    tempNode = 0L;

      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_type_code,
//                  o.org_group
//          
//          from    organization o
//          where   o.org_num = :orgNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_type_code,\n                o.org_group\n         \n        from    organization o\n        where   o.org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserAppType",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,orgNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgType = (String)__sJT_rs.getString(1);
   tempNode = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^7*/

      if(orgType.equals("m"))
      {
        // merchant, get association node from mif table
        /*@lineinfo:generated-code*//*@lineinfo:130^9*/

//  ************************************************************
//  #sql [Ctx] { select  m.association_node
//            
//            from    mif m
//            where   m.merchant_number = :tempNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  m.association_node\n           \n          from    mif m\n          where   m.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.UserAppType",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,tempNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   usableNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
      }
      else
      {
        usableNode = tempNode;
      }
    }
    catch(Exception e)
    {
      logEntry("getUsableNode(" + orgNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return usableNode;
  }
  
  public long getClosestParent(int orgNum, String tableName)
  {
    long                  resultNode      = 9999999999L;
    ResultSetIterator     it              = null;
    ResultSet             rs              = null;
    
    try
    {
      connect();
      
      if(orgExists(orgNum))
      {
        long  hierarchyNode = getUsableNode(orgNum);

        // get closest hierarchy entry in requested table
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  th.ancestor   ancestor
//            from    t_hierarchy th,
//                    :tableName req
//            where   th.descendent = :hierarchyNode and
//                    th.ancestor = req.hierarchy_node
//            order by th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  th.ancestor   ancestor\n          from    t_hierarchy th,\n                   ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("  req\n          where   th.descendent =  ?  and\n                  th.ancestor = req.hierarchy_node\n          order by th.relation asc");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.user.UserAppType:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^9*/

        rs = it.getResultSet();

        // item will be first in result set
        if(rs.next())
        {
          resultNode = rs.getLong("ancestor");
        }
        
        rs.close();
        it.close();
      }
      else
      {
        // org did not exist, error
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "getAppType()", orgNum + " not found");
        resultNode = 9999999999L;
      }
    }
    catch(Exception e)
    {
      logEntry("findClosestParent(" + orgNum + ", " + tableName + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return resultNode;
  }
  
  public boolean getAppType(String loginName, int orgNum, int userType)
  {
    boolean result = false;
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    try
    {
      connect();
      
      // see if override app type exists for this user
      int   appTypeOverride = -1;
      
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(app_type_override, -1)
//          
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(app_type_override, -1)\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.user.UserAppType",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appTypeOverride = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
      
      if(appTypeOverride == -1)
      {
        result = getAppType(orgNum, userType);
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:238^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct app_type,
//                    appsrctype_code,
//                    screen_sequence_id
//            from    org_app
//            where   app_type = :appTypeOverride
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct app_type,\n                  appsrctype_code,\n                  screen_sequence_id\n          from    org_app\n          where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.user.UserAppType",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appTypeOverride);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.user.UserAppType",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          this.appType      = rs.getInt("app_type");
          this.appDesc      = rs.getString("appsrctype_code");
          this.sequenceType = rs.getInt("screen_sequence_id");
          result = true;
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + loginName + ", " + orgNum + ", " + userType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public boolean getAppType(int orgNum, int userType)
  {
    boolean             result        = false;
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    long                hierarchyNode = 0L;
    boolean             wasStale      = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      if(orgExists(orgNum))
      {
        // get closest parent's hierarchyNode
        hierarchyNode = getClosestParent(orgNum, "org_app");
      
        // get data from org_app
        /*@lineinfo:generated-code*//*@lineinfo:297^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type,
//                    appsrctype_code,
//                    screen_sequence_id 
//            from    org_app 
//            where   hierarchy_node = :hierarchyNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type,\n                  appsrctype_code,\n                  screen_sequence_id \n          from    org_app \n          where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.user.UserAppType",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.user.UserAppType",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:304^9*/
      
        rs = it.getResultSet();
      
        if(rs.next())
        {
          this.appType      = rs.getInt("app_type");
          this.appDesc      = rs.getString("appsrctype_code");
          this.sequenceType = rs.getInt("screen_sequence_id");
          result            = true;
        }
        
        rs.close();
        it.close();
      }
      else
      {
        // org did not exist, error
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "getAppType()", orgNum + " not found");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getAppType()", orgNum + ", " + userType + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return result;
  }
  
  public int getAppType()
  {
    return this.appType;
  }
  
  public int getSequenceType()
  {
    return this.sequenceType;
  }
  
  public String getAppDesc()
  {
    return this.appDesc;
  }
}/*@lineinfo:generated-code*/