/*@lineinfo:filename=TCUserManager*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:  
  
  TCUserManager
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.io.InputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
//import com.tricipher.B2F.*;
//import TRC.api.API;
import com.mes.b2f.API;
import com.mes.b2f.B2F;
//import com.mes.tricipher.mes.B2FApplianceException;
import com.mes.b2f.B2FApplianceException;
import com.mes.b2f.B2FConfiguration;
import com.mes.b2f.B2FData;
import com.mes.b2f.B2FDeviceIdNotRegisteredException;
import com.mes.b2f.B2FInitializationException;
import com.mes.b2f.B2FSessionCache;
//import com.mes.tricipher.B2FSingleton;
import com.mes.b2f.B2FSingleton;
import com.mes.b2f.B2FUserException;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class TCUserManager extends SQLJConnectionBase
{
  public static final int   TC_US_MISSING   = 1001;
  public static final int   TC_US_EXISTS    = 1002;
  public static final int   TC_US_ACTIVATED = 1003;
  public static final int   TC_US_ERROR     = 9999;
  
  static Logger log = Logger.getLogger(TCUserManager.class);
  
  public TCUserManager()
  {
  }
  
  /*
  ** public static int getTCErrorCode
  **
  ** Extracts error code from TriCipher exception details
  */
  public static int getTCErrorCode(String exceptionMsg)
  {
    int errCode = -1;
    try
    {
      int rcIndex = exceptionMsg.lastIndexOf("rc=");
      
      if(rcIndex > 0)
      {
        errCode = Integer.parseInt(exceptionMsg.substring(rcIndex+3,exceptionMsg.length()));
      }
    }
    catch(Exception e)
    {
      System.out.println("TCUserManager::getTCErrorCode(" + exceptionMsg + "): " + e.toString());
    }
    
    return errCode;
  }
  
  /*
  ** protected boolean userExistsInMes(String loginName)
  **
  ** TRUE if user exists in MES user database (and is enabled)
  ** FALSE otherwise
  */
  private boolean userExistsInMes(String loginName)
  {
    boolean result = false;
    try
    {
      connect();
      
      int userCount;
      /*@lineinfo:generated-code*//*@lineinfo:113^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_id)
//          
//          from    users
//          where   login_name = :loginName and
//                  upper(enabled) = 'Y' 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_id)\n         \n        from    users\n        where   login_name =  :1  and\n                upper(enabled) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^7*/
      
      result = (userCount > 0);
    }
    catch(Exception e)
    {
      logEntry("userExistsInMes("+loginName+")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
  
  /****
  ***** implementation of static methods
  ****/
  
  /*
  ** private boolean _shouldUseB2f(MESB2FUser user)
  **
  ** True if user exsists under a hierarchy that requires B2F or
  ** has been singularly configured to do so, False otherwise.
  */
  private boolean _shouldUseB2f(String loginName)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;
    
    try
    {
      connect();
      
      int recCount = 0;
      
      // is this even a user in our database?
      /*@lineinfo:generated-code*//*@lineinfo:159^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//          
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^7*/
      
      if( recCount > 0 )
      {
        // user table overrides all other configurations, so check that first
        String b2fStatus = "";
        /*@lineinfo:generated-code*//*@lineinfo:171^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(upper(use_b2f), 'U') use_b2f
//            
//            from    users
//            where   login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(upper(use_b2f), 'U') use_b2f\n           \n          from    users\n          where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b2fStatus = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^9*/
      
        if(b2fStatus.equals("U"))
        {
          // check user type configuration - this overrides hierarchy if present
          /*@lineinfo:generated-code*//*@lineinfo:182^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(buc.use_b2f)  use_b2f
//              from    users u,
//                      b2f_user_type_config buc
//              where   u.login_name = :loginName and
//                      u.type_id = buc.type_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(buc.use_b2f)  use_b2f\n            from    users u,\n                    b2f_user_type_config buc\n            where   u.login_name =  :1  and\n                    u.type_id = buc.type_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.TCUserManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.user.TCUserManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^11*/
        
          rs = it.getResultSet();
        
          if(rs.next())
          {
            if(rs.getString("use_b2f").equals("Y"))
            {
              result = true;
            }
          }
          else
          {
            // is user a merchant?
            int merchCount = 0;
      
            /*@lineinfo:generated-code*//*@lineinfo:205^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)
//                
//                from    users u,
//                        mif mf
//                where   u.login_name = :loginName and
//                        u.hierarchy_node = mf.merchant_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)\n               \n              from    users u,\n                      mif mf\n              where   u.login_name =  :1  and\n                      u.hierarchy_node = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^13*/
      
            // see if user falls under a hierarchy node that requires b2f
            if(merchCount > 0)
            {
              /*@lineinfo:generated-code*//*@lineinfo:218^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(b2f.use_b2f) use_b2f
//                  from    users u,
//                          mif mf,
//                          t_hierarchy th,
//                          b2f_config b2f
//                  where   u.login_name = :loginName and
//                          u.hierarchy_node = mf.merchant_number and
//                          mf.association_node = th.descendent and
//                          th.ancestor = b2f.hierarchy_node
//                  order by th.relation
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(b2f.use_b2f) use_b2f\n                from    users u,\n                        mif mf,\n                        t_hierarchy th,\n                        b2f_config b2f\n                where   u.login_name =  :1  and\n                        u.hierarchy_node = mf.merchant_number and\n                        mf.association_node = th.descendent and\n                        th.ancestor = b2f.hierarchy_node\n                order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.user.TCUserManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.user.TCUserManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^15*/
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:234^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(b2f.use_b2f)  use_b2f
//                  from    users u,
//                          t_hierarchy th,
//                          b2f_config b2f
//                  where   u.login_name = :loginName and
//                          u.hierarchy_node = th.descendent and
//                          th.ancestor = b2f.hierarchy_node
//                  order by th.relation
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(b2f.use_b2f)  use_b2f\n                from    users u,\n                        t_hierarchy th,\n                        b2f_config b2f\n                where   u.login_name =  :1  and\n                        u.hierarchy_node = th.descendent and\n                        th.ancestor = b2f.hierarchy_node\n                order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.user.TCUserManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.user.TCUserManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^15*/
            }
      
            rs = it.getResultSet();
      
            if(rs.next())
            {
              if(rs.getString("use_b2f").equals("Y"))
              {
                result = true;
              }
            }
        
            rs.close();
            it.close();
          }
        }
        else
        {
          if(b2fStatus.equals("Y"))
          {
            result = true;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("_shouldUseB2f(" + loginName + ")", e.toString());
    }                           
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** private String getSelectedImage(String loginName)
  **
  ** returns image id from user
  */
  private String getSelectedImage(String loginName)
  {
    String result = "";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:297^7*/

//  ************************************************************
//  #sql [Ctx] { select  b2f_img
//          
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  b2f_img\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^7*/
    }
    catch(Exception e)
    {
      logEntry("getSelectedImage(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** private int _getTCUserStatus(MESB2FUser user)
  **
  ** Returns status of user with respect to TriCipher database
  */
  private int _getTCUserStatus(MESB2FUser user, HttpServletRequest request)
  {
    int   userStatus = MESB2FUser.TC_STATUS_MES_NOT_EXIST;
    
    try
    {
      if( userExistsInMes(user.getLoginName()) )
      {
        if( user.doesUseB2f() )
        {
          // see if user exists in TriCipher Database
          B2F b2f = B2FSingleton.getB2fInstance();
        
          try
          {
            B2FData bd = b2f.getUserInformation(B2FSingleton.getRealm(), user.getLoginName());
          
            // set MESB2FUser object data from B2FData
            if(bd.getChallengeResponse() != null)
            {
              user.setChallengeResponse(bd.getChallengeResponse());
              user.setRandomQuestion();
              user.setImg(getSelectedImage(user.getLoginName()));
            }
          
            if(bd.getWelcomeText() != null && !bd.getWelcomeText().equals(""))
            {
              user.setWelcomeMsg(bd.getWelcomeText());
            }
          
            if(bd.getWelcomeBlob() != null)
            {
              user.setWelcomeBlob(bd.getWelcomeBlob());
            }
          
            user.setWelcomeBlobContentType( bd.getWelcomeBlobContentType() );
          
            // if no exception was thrown then this user exists in the TC database
            if(bd.isUserIdActivated())
            {
              // see if user has valid cookie
              try
              {
                B2FData bdc = b2f.validate(B2FSingleton.getRealm(), user.getLoginName(), request.getCookies(), request);
              
                userStatus = MESB2FUser.TC_STATUS_ACTIVATED;
              }
              catch(B2FDeviceIdNotRegisteredException bnre)
              {
                // this means that the user has not registered this computer yet (no matching cookie)
                // need to register devide
                userStatus = MESB2FUser.TC_STATUS_UNREGISTERED;
              }
              catch(Exception e)
              {
                log.error("Validating user with cookie: " + e.toString());
              }
            }
            else
            {
              userStatus = MESB2FUser.TC_STATUS_UNACTIVATED;
            }
          }
          catch(B2FApplianceException bae)
          {
            if(getTCErrorCode(bae.toString()) == API.TRC_INVALID_USER)
            {
              userStatus = MESB2FUser.TC_STATUS_TC_NOT_EXIST;
            }
            else
            {
              // this means there was a problem connecting to the appliance.
              // disable b2f for now
              //user.setUseB2f(false);
              userStatus = MESB2FUser.TC_STATUS_TC_NOT_EXIST;
            }
          }
        }
        else
        {
          // user exists in database but is not set up to require B2F
          userStatus = MESB2FUser.TC_STATUS_TC_NOT_EXIST;
        }
      }
    }
    catch(B2FInitializationException bie)
    {
      userStatus = MESB2FUser.TC_STATUS_ERROR;
      user.setError(bie.toString());
      logEntry("_getTCUserStatus(" + user.getLoginName() + ")", bie.toString());
    }
    catch(Exception e)
    {
      userStatus = MESB2FUser.TC_STATUS_ERROR;
      user.setError(e.toString());
      logEntry("_getTCUserStatus(" + user.getLoginName() + ")", e.toString());
    }
    
    return( userStatus );
  }
  
  /*
  ** private boolean _resetUser(String loginName, String activationCode)
  **
  ** Resets a user in the current realm and sets the activation code equal
  ** to the passed code
  */
  private boolean _resetUser(String loginName, String activationCode)
  {
    boolean result = false;
    try
    {
      if(_shouldUseB2f(loginName))
      {
        B2F b2f = B2FSingleton.getB2fInstance();
        B2FSessionCache sessCache = b2f.getSessionCache();
        
        b2f.resetUser(loginName);
      
        //String resetVal = sessCache.resetid(b2f.getB2fManager(), B2FSingleton.getRealm(), loginName, activationCode);
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("_resetUser(" + loginName + ")", e.toString());
    }
    
    return( result );
  }
  
  /*
  ** private boolean _deleteUser(String loginName)
  **
  ** Deletes user from TriCipher database
  */
  private boolean _deleteUser(String loginName)
  {
    boolean result = false;
    
    try
    {
      if(_shouldUseB2f(loginName))
      {
        B2F b2f = B2FSingleton.getB2fInstance();
        B2FSessionCache sessCache = b2f.getSessionCache();
      
        sessCache.deleteid(b2f.getB2fManager(), B2FSingleton.getRealm(), loginName);
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("_deleteUser(" + loginName + ")", e.toString());
    }
    
    return( result );
  }
  
  /*
  ** private void _displayUser(String loginName)
  **
  ** Displays B2F User data
  */
  private void _displayUser(String loginName)
  {
    try
    {
      B2F b2f = B2FSingleton.getB2fInstance();
      
      B2FData b2fData = b2f.getUserInformation(B2FSingleton.getRealm(), loginName);
    }
    catch(Exception e)
    {
      logEntry("_displayUser(" + loginName + ")", e.toString());
    }
  }
  
  /*
  ** private boolean _convertMESUser(MESB2FUser user)
  **
  ** Takes a user that already exists in the MES User database and creates
  ** a corresponding user in the current server's realm on the TriCipher
  ** security appliance, with the current password as the activation code.
  **
  ** If the password does not exist in clear text in the existing user database
  ** then the activation code is set to a default of:
  **  a) the business phone number of the merchant if this is a merchant-level user
  **  b) the login name of the user
  **
  ** The password not existing should be a rare occurrence.
  */
  private boolean _convertMESUser(String loginName, String activationCode)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:526^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.login_name                              login_name,
//                  nvl(u.city, 'NOWHERE')                    city,
//                  nvl(u.email,u.login_name||'@noemail.com') email_address,
//                  decode(u.name,
//                    null, 'NOFIRSTNAME',
//                    ltrim(rtrim(
//                      substr(u.name, 1, decode(instr(u.name, ' '), 0, length(u.name), instr(u.name, ' ')))))) first_name,
//                  decode(u.name,
//                    null, 'NOLASTNAME',
//                    nvl(ltrim(substr(u.name, decode(instr(u.name, ' '), 0, length(u.name)+1, instr(u.name, ' ')), length(u.name))), ' ')) last_name,
//                  nvl(u.name, 'MESUSER')                    org_name,
//                  nvl(u.state, 'CA')                        state        
//          from    users u
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.login_name                              login_name,\n                nvl(u.city, 'NOWHERE')                    city,\n                nvl(u.email,u.login_name||'@noemail.com') email_address,\n                decode(u.name,\n                  null, 'NOFIRSTNAME',\n                  ltrim(rtrim(\n                    substr(u.name, 1, decode(instr(u.name, ' '), 0, length(u.name), instr(u.name, ' ')))))) first_name,\n                decode(u.name,\n                  null, 'NOLASTNAME',\n                  nvl(ltrim(substr(u.name, decode(instr(u.name, ' '), 0, length(u.name)+1, instr(u.name, ' ')), length(u.name))), ' ')) last_name,\n                nvl(u.name, 'MESUSER')                    org_name,\n                nvl(u.state, 'CA')                        state        \n        from    users u\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.user.TCUserManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.user.TCUserManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:542^7*/
      
      rs = it.getResultSet();
      
      if( rs.next() )
      {
        Hashtable ht = new Hashtable();
        
        ht.put( "Client ID",                rs.getString("login_name") );
        ht.put( "Activation Code",          activationCode );
        ht.put( "Role",                     "Consumer" );
        ht.put( "First Name",               rs.getString("first_name") );
        ht.put( "Last Name",                rs.getString("last_name") );
        ht.put( "B2fUser",                  "T" );
        ht.put( "City Name",                rs.getString("city") );
        ht.put( "Country Name",             "US" );
        ht.put( "Email Address",            rs.getString("email_address") );
        ht.put( "Organization Name",        rs.getString("org_name") );
        ht.put( "Organizational Unit Name", "MES" );
        ht.put( "Publish Cert",             "F" );
        ht.put( "Roam Once",                "F" );
        ht.put( "State Name",               rs.getString("state") );
        ht.put( "Tether",                   "F" );
        ht.put( "TwoFactor",                "None" );
       
        for(Enumeration keys = ht.keys(); keys.hasMoreElements(); )
        {
          Object key = keys.nextElement();
        
          String keyStr = (String)key;
          String valStr = (String)(ht.get(key));
        }                            
          
        // create user
        B2F b2f = B2FSingleton.getB2fInstance();
        B2FConfiguration config = b2f.getConfig();
        B2FSessionCache sessCache = b2f.getSessionCache();
        B2FSessionCache.CacheInfo b2fMgr = b2f.getB2fManager();
        B2FSessionCache.CacheInfo cache = sessCache.getCachedSession("", config.getCsrManagerUserId(), config.getCsrManagerPassword(), false, b2fMgr.getAppliance(), b2fMgr.getPort());
        Hashtable rcHt = sessCache.createid(cache, B2FSingleton.getRealm(), rs.getString("login_name"), ht);
      }
      
      rs.close();
      it.close();
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("convertMESUser(" + loginName + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return ( result );
  }
  
  /*
  ** private boolean _activationCodeCorrect(String loginName, String password)
  ** 
  ** Returns true if the provided activation code is correct for this user.
  ** The activation code is the same as the users's current MES password so 
  ** the validation basically just entails checking against the MES user database.
  */
  private boolean _activationCodeCorrect(String loginName, String password)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      int recCount;
      /*@lineinfo:generated-code*//*@lineinfo:619^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//          
//          from    users
//          where   login_name = :loginName and
//                  password_enc = encrypt_password(:password)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n         \n        from    users\n        where   login_name =  :1  and\n                password_enc = encrypt_password( :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.user.TCUserManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,password);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:626^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("_activationCodeCorrect(" + loginName + ", " + password + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
  
  private void addCookiesToResponse(Cookie[] cookies, HttpServletResponse response)
  {
    try
    {
      if(cookies != null)
      {
        for(int i=0; i<cookies.length; ++i)
        {
          response.addCookie(cookies[i]);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("addCookiesToResponse()", e.toString());
    }
  }
  
  /*
  ** private boolean _activateUser(MESB2FUSer user)
  **
  ** Returns true if the user was activated succesfully on the TriCipher device.
  */
  private boolean _activateUser(MESB2FUser user, HttpServletRequest request, HttpServletResponse response)
  {
    boolean   result = false;
    Hashtable hTable = new Hashtable();
    Cookie[] b2FCookies = null;
    Cookie[] oldB2FCookies = null;
    long registerTime = 182*60*60*24;
    String realm = B2FSingleton.getRealm();
    
    try
    {
      connect();
      
      // fill hashtable with questions and answers from user object
      hTable.put( user.getQuestion1(), user.getAnswer1() );
      hTable.put( user.getQuestion2(), user.getAnswer2() );
      hTable.put( user.getQuestion3(), user.getAnswer3() );
      
      // get B2F instance for interacting with appliance
      B2F b2f = B2FSingleton.getB2fInstance();

      if(b2f != null)
      {
        try
        {
          // activate user
          b2f.activateUser(realm, user.getLoginName(), user.getActivationCode(), user.getPassword(), false);
          
          // get welcome image
          StringBuffer img = new StringBuffer("");
          img.append("http://");
          img.append(HttpHelper.getServerURLNaked(null));
          img.append(user.getImgLoc());
          
          URL imgURL = new URL(img.toString());
          InputStream is = imgURL.openStream();
          byte[] welcomeImage = new byte[is.available()];
          is.read(welcomeImage);
          
          b2f.setWelcomeData(realm, user.getLoginName(), user.getPassword(), user.getWelcomeMsg(), welcomeImage, "image/jpeg");
          b2f.setChallenge(realm, user.getLoginName(), user.getPassword(), hTable);
          
          // set up cookies and log user in to appliance
          b2FCookies = b2f.registerDevice(realm, user.getLoginName(), user.getPassword(), registerTime, "", request.getCookies(), request);
          oldB2FCookies = b2f.deleteCookies(realm, user.getLoginName(), request.getCookies());
        
          b2f.login(realm, user.getLoginName(), user.getPassword(), b2FCookies, null);
        
          addCookiesToResponse(oldB2FCookies, response);
          addCookiesToResponse(b2FCookies, response);
          
          // now set mes password to match along with selected image
          /*@lineinfo:generated-code*//*@lineinfo:717^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     b2f_img = :user.getImg(),
//                      password_enc = encrypt_password(:user.getPassword())
//              where   login_name = :user.getLoginName()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_103 = user.getImg();
 String __sJT_104 = user.getPassword();
 String __sJT_105 = user.getLoginName();
   String theSqlTS = "update  users\n            set     b2f_img =  :1 ,\n                    password_enc = encrypt_password( :2 )\n            where   login_name =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.user.TCUserManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_103);
   __sJT_st.setString(2,__sJT_104);
   __sJT_st.setString(3,__sJT_105);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:723^11*/
          
          result = true;
        }
        catch(B2FUserException bue)
        {
          log.error("**ERROR: " + bue.toString());
          user.setError("Invalid Password: Your password must be at least 8 characters and contain at least one uppercase letter, one lowercase letter, and one number.");
        }
        catch(Exception ex)
        {
          user.setError(ex.toString());
          logEntry("_activateUser("+user.getLoginName()+")", ex.toString());
        }
      }
    }
    catch(Exception e)
    {
      user.setError(e.toString());
      logEntry("_activateUser(" + user.getLoginName() + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
  
  private boolean _loginTricipher(MESB2FUser user, HttpServletRequest request, HttpServletResponse response)
  {
    boolean result = false;
    Cookie[] b2FCookies = null;
    Cookie[] oldB2FCookies = null;
    long registerTime = 182*60*60*24;
    String realm = B2FSingleton.getRealm();

    try
    {    
      B2F b2f = B2FSingleton.getB2fInstance();
      if(b2f != null)
      {
        try
        {
          // set up cookies and log user in to appliance
          b2FCookies = b2f.registerDevice(realm, user.getLoginName(), user.getPassword(), registerTime, "", request.getCookies(), request);
          oldB2FCookies = b2f.deleteCookies(realm, user.getLoginName(), request.getCookies());
    
          b2f.login(realm, user.getLoginName(), user.getPassword(), b2FCookies, null);
    
          addCookiesToResponse(oldB2FCookies, response);
          addCookiesToResponse(b2FCookies, response);
    
          result = true;  
        }
        catch(B2FUserException bue)
        {
          // password incorrect
          log.error("_loginTricipher(): " + bue.toString());
          user.setError("Incorrect Password");
        }
        catch(Exception e)
        {
          logEntry("_loginTricipher(" + user.getLoginName() + ")", e.toString());
        }
      }
    }
    catch(Exception e)
    {
      user.setError(e.toString());
      logEntry("_loginTricipher(" + user.getLoginName() + ")", e.toString());
    }
    
    return( result );
  }
  
  /****
  ***** STATIC SINGLETON ACESS METHODS
  ****/
  
  /*
  ** public static boolean shouldUseB2f(MESB2FUser user)
  **
  ** Determines whether user has been set up to require TriCipher 2-factor
  ** authentication.  This is determined by the B2F_USER_
  */
  public static boolean shouldUseB2f(MESB2FUser user)
    throws Exception
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._shouldUseB2f(user.getLoginName()) );
  }
  
  public static boolean shouldUseB2f(String loginName)
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._shouldUseB2f(loginName) );
  }
  
  /*
  ** public static void getTCUserStatus(MESB2FUser user)
  **
  ** determines current status of user with regards to TriCipher Database
  */
  public static int getTCUserStatus(MESB2FUser user, HttpServletRequest request)
    throws Exception
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._getTCUserStatus(user, request) );
  }
  
  /*
  ** public static void convertMESUser(String loginName, String activationCode)
  **
  ** Adds a user that currently exists in the MES User database 
  */
  public static boolean convertMESUser(String loginName, String activationCode)
    throws Exception
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._convertMESUser(loginName, activationCode) );
  }
  
  /*
  ** public static boolean activationCodeCorrect(String loginName, String password)
  **
  ** True if provided activation code is correct for this user
  */
  public static boolean activationCodeCorrect(String loginName, String password)
    throws Exception
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._activationCodeCorrect(loginName, password) );
  }
  
  /*
  ** public static boolean activateUser(MESB2FUser user, HttpServletRequest request, HttpServletResponse response)
  **
  ** returns true if user was activated successfully
  */
  public static boolean activateUser(MESB2FUser user, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
    TCUserManager manager = new TCUserManager();
    
    return( manager._activateUser(user, request, response) );
  }
  
  /*
  ** public static boolean loginTriciper(MESB2FUser user)
  **
  ** logs user in to Tricipher appliance.  Returns false if password incorrect
  */
  public static boolean loginTricipher(MESB2FUser user, HttpServletRequest request, HttpServletResponse response)
  {
    TCUserManager manager = new TCUserManager();
    return( manager._loginTricipher(user, request, response) );
  }
  
  /*
  ** public static boolean resetUser(MESB2FUSer user, String activationCode)
  **
  ** Returns true if user was successfully reset
  */
  public static boolean resetUser(String loginName, String activationCode)
  {
    TCUserManager manager = new TCUserManager();
    return( manager._resetUser(loginName, activationCode) );
  }
  
  /*
  ** public static boolean deleteUser(String loginName)
  **
  ** Deletes user from TriCipher database
  */
  public static boolean deleteUser(String loginName)
  {
    TCUserManager manager = new TCUserManager();
    return( manager._deleteUser(loginName) );
  }
  
  /*
  ** public static void displayUser(String loginName)
  **
  ** Displays B2FUser data (only useful for diagnostics)
  */
  public static void displayUser(String loginName)
  {
    TCUserManager manager = new TCUserManager();
    manager._displayUser(loginName);
  }
}/*@lineinfo:generated-code*/