/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/Group.java $

  Description:  
  
    Group
    
    Represents a user group.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copygroup (C) 2000,2001 by Merchant e-Solutions Inc.
  All groups reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;

public class Group
  implements Serializable
{
  private long groupId = 0L;
  
  /*
  ** CONSTRUCTOR public Group(long newId)
  **
  ** Creates a Group object with the new group value.
  */
  public Group(long newId)
  {
    groupId = newId;
  }
  
  /*
  ** METHOD public boolean equals(Object o)
  **
  ** Determines if another object equals this one.
  */
  public boolean equals(Object o)
  {
    boolean isEqual = false;
    
    if (o.getClass() == this.getClass())
    {
      isEqual = ((Group)o).getGroupId() == groupId;
    }
    
    return isEqual;
  }
  
  /*
  ** METHOD public long getGroupId()
  **
  ** RETURNS: the group value.
  */
  public long getGroupId()
  {
    return groupId;
  }
  
  public String toString()
  {
    return "Group: " + groupId;
  }
}
