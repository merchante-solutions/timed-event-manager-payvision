/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserQueueLockItem.java $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/20/03 5:01p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserQueueLockItem
  implements Serializable
{
  public long       id        = 0L;
  public int        type      = 0;
  public Timestamp  lockDate  = null;
  
  public UserQueueLockItem()
  {
  }
  
  public UserQueueLockItem(long id, int type, Timestamp lockDate)
  {
    setData(id, type, lockDate);
  }
  
  public void setData(long id, int type, Timestamp lockDate)
  {
    try
    {
      this.id       = id;
      this.type     = type;
      this.lockDate = lockDate;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setData(" + id + ")", e.toString());
    }
  }
}
