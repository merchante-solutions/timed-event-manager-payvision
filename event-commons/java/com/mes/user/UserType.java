/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserType.java $

  Description:  
  
    UserType
    
    Represents a user type.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copygroup (C) 2000,2001 by Merchant e-Solutions Inc.
  All groups reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;

public class UserType
  implements Serializable
{
  private long typeId = 0L;
  
  /*
  ** CONSTRUCTOR public UserType()
  */
  public UserType()
  {
  }

  /*
  ** CONSTRUCTOR public UserType(long newId)
  **
  ** Creates a UserType object with the new type value.
  */
  public UserType(long newId)
  {
    typeId = newId;
  }
  
  /*
  ** METHOD public boolean equals(Object o)
  **
  ** Determines if another object equals this one.
  */
  public boolean equals(Object o)
  {
    boolean isEqual = false;
    
    if (o.getClass() == this.getClass())
    {
      isEqual = ((UserType)o).getUserTypeId() == typeId;
    }
    
    return isEqual;
  }
  
  /*
  ** METHOD public long getUserTypeId()
  **
  ** RETURNS: the type id.
  */
  public long getUserTypeId()
  {
    return typeId;
  }
  
  public String toString()
  {
    return "User Type: " + typeId;
  }
}
