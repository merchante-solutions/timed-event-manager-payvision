/*@lineinfo:filename=OrgProfile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/OrgProfile.sqlj $

  Description:  

  UserBean
  
  Allows validation of user id's and passwords and gives access to various 
  user attributes such as user type, user group membership and user rights.
  
  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Hashtable;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class OrgProfile extends SQLJConnectionBase
  implements Serializable
{
  // element keys
  public static final String        EK_STYLE_SHEET    = "styleSheet";
  public static final String        EK_PROFILE_NODE   = "profileNode";
  public static final String        EK_HEADER_NAME    = "headerName";
  public static final String        EK_FOOTER_NAME    = "footerName";
  public static final String        EK_IMAGE_NAME     = "imageName";
  public static final String        EK_IMAGE_WIDTH    = "imageWidth";
  public static final String        EK_IMAGE_HEIGHT   = "imageHeight";
  public static final String        EK_HTML_TITLE     = "htmlTitle";
  
  
  private Hashtable   elements      = new Hashtable();
  
  private long        hierarchyNode = 0L;
  private boolean     isDevServer   = false;
  
  public OrgProfile()
  {
  }
  
  public OrgProfile(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
  }
  
  public OrgProfile(long hierarchyNode)
  {
    setHierarchyNode(hierarchyNode);
  }
  
  private long getClosestOrgProfileParent(long hierarchyNode)
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    boolean             keepGoing     = true;
    long                curNode       = hierarchyNode;
    
    try
    {
      connect();
      
      if(isDevServer)
      {
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  op.hierarchy_node
//            from    org_profile op,
//                    t_hierarchy th,
//                    mif mf
//            where   (
//                      (th.descendent = :hierarchyNode and mf.merchant_number = 941000000002) or
//                      (mf.merchant_number = :hierarchyNode and mf.association_node = th.descendent)
//                    ) and
//                    th.ancestor = op.hierarchy_node and
//                    nvl(op.enabled, 'N') = 'Y'
//            order by relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  op.hierarchy_node\n          from    org_profile op,\n                  t_hierarchy th,\n                  mif mf\n          where   (\n                    (th.descendent =  :1  and mf.merchant_number = 941000000002) or\n                    (mf.merchant_number =  :2  and mf.association_node = th.descendent)\n                  ) and\n                  th.ancestor = op.hierarchy_node and\n                  nvl(op.enabled, 'N') = 'Y'\n          order by relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setLong(2,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:106^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  op.hierarchy_node
//            from    org_profile op,
//                    t_hierarchy th,
//                    mif mf
//            where   (
//                      (th.descendent = :hierarchyNode and mf.merchant_number = 941000000002) or
//                      (mf.merchant_number = :hierarchyNode and mf.association_node = th.descendent)
//                    ) and
//                    th.ancestor = op.hierarchy_node
//            order by relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  op.hierarchy_node\n          from    org_profile op,\n                  t_hierarchy th,\n                  mif mf\n          where   (\n                    (th.descendent =  :1  and mf.merchant_number = 941000000002) or\n                    (mf.merchant_number =  :2  and mf.association_node = th.descendent)\n                  ) and\n                  th.ancestor = op.hierarchy_node\n          order by relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setLong(2,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^9*/
      }
      
      // closest parent is first item in result set
      rs = it.getResultSet();
      
      if(rs.next())
      {
        curNode = rs.getLong("hierarchy_node");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getClosesetOrgProfileParent(" + hierarchyNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return curNode;
  }
  
  private long getClosestOrgGifParent()
  {
    ResultSetIterator     it          = null;
    ResultSet             rs          = null;
    boolean               keepGoing   = true;
    long                  curNode     = this.hierarchyNode;
    long                  baseNode    = this.hierarchyNode;
    
    try
    {
      connect();
      
      // get base node (association node if merchant)
      /*@lineinfo:generated-code*//*@lineinfo:159^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  association_node
//          from    mif
//          where   merchant_number = :baseNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  association_node\n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,baseNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        baseNode = rs.getLong("association_node");
      }
      
      rs.close();
      it.close();
      
      if(isDevServer)
      {
        /*@lineinfo:generated-code*//*@lineinfo:178^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  og.hierarchy_node,
//                    th.relation
//            from    org_gif2 og,
//                    t_hierarchy th
//            where   th.descendent = :baseNode and
//                    th.ancestor = og.hierarchy_node
//            order by relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  og.hierarchy_node,\n                  th.relation\n          from    org_gif2 og,\n                  t_hierarchy th\n          where   th.descendent =  :1  and\n                  th.ancestor = og.hierarchy_node\n          order by relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,baseNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:191^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  og.hierarchy_node,
//                    th.relation
//            from    org_gif2 og,
//                    t_hierarchy th
//            where   th.descendent = :baseNode and
//                    th.ancestor = og.hierarchy_node and
//                    nvl(og.enabled, 'N') = 'Y'
//            order by relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  og.hierarchy_node,\n                  th.relation\n          from    org_gif2 og,\n                  t_hierarchy th\n          where   th.descendent =  :1  and\n                  th.ancestor = og.hierarchy_node and\n                  nvl(og.enabled, 'N') = 'Y'\n          order by relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,baseNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^9*/
      }
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        curNode = rs.getLong("hierarchy_node");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getClosesetParent(" + hierarchyNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return curNode;
  }
  
  private void getSecureHeaderInfo()
  {
    ResultSetIterator     it          = null;
    ResultSet             rs          = null;
    long                  parentNode  = 0L;
    
    try
    {
      connect();
      
      // find the parent co-branding page
      parentNode = getClosestOrgGifParent();
      
      if(parentNode > 0L)
      {
        // get the gif info from the table for this node
        /*@lineinfo:generated-code*//*@lineinfo:244^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  template_file,
//                    gif_name,
//                    width,
//                    height,
//                    footer_file
//            from    org_gif2
//            where   hierarchy_node = :parentNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  template_file,\n                  gif_name,\n                  width,\n                  height,\n                  footer_file\n          from    org_gif2\n          where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          elements.put(EK_HEADER_NAME, rs.getString("template_file"));
          elements.put(EK_FOOTER_NAME, rs.getString("footer_file"));
          elements.put(EK_IMAGE_NAME,  rs.getString("gif_name"));
          elements.put(EK_IMAGE_WIDTH, rs.getString("width"));
          elements.put(EK_IMAGE_HEIGHT,rs.getString("height"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getSecureHeaderInfo()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public String getElement(String key)
  {
    String result = "";
    
    try
    {
      if(elements.containsKey(key))
      {
        result = (String)(elements.get(key));
      }
    }
    catch(Exception e)
    {
      logEntry("getElement(" + key + ")", e.toString());
    }
    
    return result;
  }
  
  public void setHierarchyNode(String hierarchyNode)
  {
    try
    {
      setHierarchyNode(Long.parseLong(hierarchyNode));
    }
    catch(Exception e)
    {
      logEntry("setHierarchyNode(" + hierarchyNode + ")", e.toString());
    }
  }
  public void setHierarchyNode(long hierarchyNode)
  {
    ResultSetIterator       it          = null;
    ResultSet               rs          = null;
    long                    parentNode  = 0L;
    
    // get profile data for this node
    try
    {
      connect();
      
      isDevServer = HttpHelper.isDevServer(null);
      
      this.hierarchyNode = (hierarchyNode == 0 ? 9999999999L : hierarchyNode);
      
      elements.clear();
      
      parentNode = getClosestOrgProfileParent(hierarchyNode);
      
      // add parentNode to elements
      elements.put(EK_PROFILE_NODE, Long.toString(parentNode));
      
      // get the style sheet and html title
      /*@lineinfo:generated-code*//*@lineinfo:335^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  style_sheet,
//                  title
//          from    org_profile
//          where   hierarchy_node = :parentNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  style_sheet,\n                title\n        from    org_profile\n        where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.user.OrgProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.user.OrgProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // add the style sheet to the elements
        elements.put(EK_STYLE_SHEET, processString(rs.getString("style_sheet")));
        elements.put(EK_HTML_TITLE, processString(rs.getString("title")));
      }
      
      rs.close();
      it.close();
      
      // get the co-branding stuff
      getSecureHeaderInfo();
    }
    catch(Exception e)
    {
      logEntry("setHierarchyNode(" + hierarchyNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/