/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/Right.java $

  Description:  
  
    Right
    
    Represents a user right.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;

public class Right
  implements Serializable
{
  private long rightId = 0L;
  
  /*
  ** CONSTRUCTOR public Right(long newId)
  **
  ** Creates a Right object with the new right value.
  */
  public Right(long newId)
  {
    rightId = newId;
  }
  
  /*
  ** METHOD public boolean equals(Object o)
  **
  ** Determines if another object equals this one.
  */
  public boolean equals(Object o)
  {
    boolean isEqual = false;
    
    if (o.getClass() == this.getClass())
    {
      isEqual = (((Right)o).getRightId() == rightId);
    }

    return isEqual;
  }
  
  /*
  ** METHOD public long getRightId()
  **
  ** RETURNS: the right value.
  */
  public long getRightId()
  {
    return rightId;
  }
  
  /*
  ** METHOD public String toString()
  **
  ** RETURNS: String representation of the right.
  */
  public String toString()
  {
    return "Right: " + rightId;
  }
}
