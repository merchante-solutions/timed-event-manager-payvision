/*@lineinfo:filename=UserTypeRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserTypeRecord.sqlj $

  Description:  
  
    UserTypeRecord.
    
    Contains a user_types record.  Allows insertion, modification and
    deletion of a record.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class UserTypeRecord extends DBBean
  implements Serializable
{
  // record fields
  private UserType            userType          = new UserType();
  private String              name              = "";
  private String              description       = "";
  private long                dfltHierarchyNode = 0L;
  private GroupSet            groupSet          = new GroupSet();
  private Right               reqRight          = new Right(0L);
  
  /*
  ** CONSTRUCTOR UserTypeRecord()
  */
  public UserTypeRecord(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
  }
  public UserTypeRecord()
  {
  }

  /*
  ** CONSTRUCTOR UserTypeRecord(ResultSet rs)
  **
  ** Initializes a type record with the contents of a result set.
  */
  public UserTypeRecord(ResultSet rs)
  {
    loadResultSet(rs);
  }
  
  /*
  ** METHOD public void loadResultSet(ResultSet rs)
  **
  ** Loads the contents of a ResultSet into internal field storage.
  */
  public void loadResultSet(ResultSet rs)
  {
    try
    {
      userType          = new UserType(rs.getLong("type_id"));
      name              = processString(rs.getString("name"));
      description       = processString(rs.getString("description"));
      dfltHierarchyNode = rs.getLong  ("dflt_hierarchy_node");
      reqRight          = new Right(rs.getLong("right_id"));
    }
    catch (Exception e)
    {
      logEntry("loadResultSet()", e.toString());
    }
  }
  
  /*
  ** METHOD protected boolean get()
  **
  ** Loads a type record from the user_types table.
  **
  ** RETURNS: true if type record successfully loaded, else false.
  */
  protected boolean get()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             found   = false;
    
    try
    {
      // look up user type record
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_types
//          where   type_id = :userType.getUserTypeId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_110 = userType.getUserTypeId();
  try {
   String theSqlTS = "select  *\n        from    user_types\n        where   type_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_110);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.UserTypeRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        // load the record into internal storage
        loadResultSet(rs);
        
        found = true;
      }                                    
      
      rs.close();
      it.close();
      
      if(found)
      {
        // look up group associations
        /*@lineinfo:generated-code*//*@lineinfo:137^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  group_id
//            from    user_to_group
//            where   user_id = :userType.getUserTypeId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_111 = userType.getUserTypeId();
  try {
   String theSqlTS = "select  group_id\n          from    user_to_group\n          where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_111);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.UserTypeRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^9*/
        
        groupSet.clear();
        
        rs = it.getResultSet();
        
        while (rs.next())
        {
          groupSet.add(new Group(rs.getLong("group_id")));
        }
        
        it.close();
          
        getOk = true;
      }
    }
    catch (Exception e)
    {
      logEntry("get()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return getOk;
  }
  
  /*
  ** METHOD public boolean getData(long getId)
  **
  ** Loads a type record with the given id from the user_types table.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  public boolean getData(long getId)
  {
    userType = new UserType(getId);
    return getData();
  }
  
  /*
  ** METHOD protected boolean submit()
  **
  ** Inserts or updates a type record.
  **
  ** RETURNS: true if submit successful, else false.
  */
  protected boolean submit()
  {
    boolean submitOk = false;
    
    try
    {
      int count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:198^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(type_id) 
//          from    user_types
//          where   type_id = :userType.getUserTypeId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_112 = userType.getUserTypeId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(type_id)  \n        from    user_types\n        where   type_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserTypeRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_112);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^7*/
      
      if(count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:207^9*/

//  ************************************************************
//  #sql [Ctx] { update  user_types
//            set     name                = :name,
//                    description         = :description,
//                    dflt_hierarchy_node = :dfltHierarchyNode,
//                    right_id            = :reqRight.getRightId()
//            where   type_id             = :userType.getUserTypeId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_113 = reqRight.getRightId();
 long __sJT_114 = userType.getUserTypeId();
   String theSqlTS = "update  user_types\n          set     name                =  :1 ,\n                  description         =  :2 ,\n                  dflt_hierarchy_node =  :3 ,\n                  right_id            =  :4 \n          where   type_id             =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,dfltHierarchyNode);
   __sJT_st.setLong(4,__sJT_113);
   __sJT_st.setLong(5,__sJT_114);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:219^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_types
//            (
//              name,
//              description,
//              dflt_hierarchy_node,
//              right_id,
//              type_id
//            )
//            values
//            (
//              :name,
//              :description,
//              :dfltHierarchyNode,
//              :reqRight.getRightId(),
//              :userType.getUserTypeId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_115 = reqRight.getRightId();
 long __sJT_116 = userType.getUserTypeId();
   String theSqlTS = "insert into user_types\n          (\n            name,\n            description,\n            dflt_hierarchy_node,\n            right_id,\n            type_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,dfltHierarchyNode);
   __sJT_st.setLong(4,__sJT_115);
   __sJT_st.setLong(5,__sJT_116);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }
      
      // clear old group associations
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_to_group
//          where       user_id = :userType.getUserTypeId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_117 = userType.getUserTypeId();
   String theSqlTS = "delete from user_to_group\n        where       user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_117);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^7*/
      
      // insert new group associations
      Iterator gIter = groupSet.getGroupIterator();
      Group group = null;
      
      while(gIter.hasNext())
      {
        group = (Group)gIter.next();
        
        /*@lineinfo:generated-code*//*@lineinfo:255^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_to_group
//            (
//              user_id,
//              group_id
//            )
//            values
//            (
//              :userType.getUserTypeId(),
//              :group.getGroupId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_118 = userType.getUserTypeId();
 long __sJT_119 = group.getGroupId();
   String theSqlTS = "insert into user_to_group\n          (\n            user_id,\n            group_id\n          )\n          values\n          (\n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_118);
   __sJT_st.setLong(2,__sJT_119);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^9*/
      }
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
    }
    
    return submitOk;
  }
  
  /*
  ** METHOD protected boolean validate()
  **
  ** Validates user type data.
  **
  ** RETURNS: true if data is valid, else false.
  */
  protected boolean validate()
  {
    return true;
  }
  
  /*
  ** METHOD public boolean delete()
  **
  ** Deletes the current record from the database.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean delete()
  {
    boolean deleteOk = false;
    
    try
    {
      // delete user type to group records
      /*@lineinfo:generated-code*//*@lineinfo:306^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_to_group
//          where       user_id = :userType.getUserTypeId();
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_120 = userType.getUserTypeId();
   String theSqlTS = "delete from user_to_group\n        where       user_id =  :1 ;";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_120);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^7*/
      
      // delete type record
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_types
//          where       type_id = :userType.getUserTypeId();
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_121 = userType.getUserTypeId();
   String theSqlTS = "delete from user_types\n        where       type_id =  :1 ;";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.user.UserTypeRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_121);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:317^7*/
      
      deleteOk = true;
    }
    catch (Exception e)
    {
      logEntry("delete()", e.toString());
    }

    return deleteOk;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    userType          = new UserType(HttpHelper.getLong(request,"userTypeId",0));
    name              = HttpHelper.getString(request,"name","");
    description       = HttpHelper.getString(request,"description","");
    dfltHierarchyNode = HttpHelper.getLong(request,"dfltHierarchyNode",0);
    reqRight          = new Right(HttpHelper.getLong(request,"rightRec",0));
    String[] groupIds = request.getParameterValues("groupCheck");
    if (groupIds != null)
    {
      for (int i = 0; i < groupIds.length; ++i)
      {
        groupSet.add(new Group(Long.parseLong(groupIds[i])));
      }
    }
    setSubmitVal(HttpHelper.getString(request,"submitVal",""));
  }
  
  /*
  ** ACCESSORS
  */
  public UserType getUserType()
  {
    return userType;
  }
  public void setUserType(UserType newUserType)
  {
    userType = newUserType;
  }
  
  public long getUserTypeId()
  {
    if (userType != null)
    {
      return userType.getUserTypeId();
    }
    return 0L;
  }
  public void setUserTypeId(String userTypeId)
  {
    try
    {
      setUserTypeId(Long.parseLong(userTypeId));
    }
    catch(Exception e)
    {
      logEntry("setUserTypeId(" + userTypeId + ")", e.toString());
    }
  }
  public void setUserTypeId(long newUserTypeId)
  {
    userType = new UserType(newUserTypeId);
  }
  
  public String getName()
  {
    return name;
  }
  public void setName(String newName)
  {
    name = newName;
  }
  
  public void setDescription(String newDescription)
  {
    description = newDescription;
  }
  public String getDescription()
  {
    return description;
  }
  
  public String getDfltHierarchyNode()
  {
    String retStr = "";
    if (dfltHierarchyNode != 0L)
    {
      retStr = Long.toString(dfltHierarchyNode);
    }
    return retStr;
  }
  public void setDfltHierarchyNode(String newDfltHierarchyNodeStr)
  {
    dfltHierarchyNode = parseLong(newDfltHierarchyNodeStr);
  }
  
  public GroupSet getGroupSet()
  {
    return groupSet;
  }
  public void setGroupCheck(String[] groupIds)
  {
    for (int i = 0; i < java.lang.reflect.Array.getLength(groupIds); ++i)
    {
      groupSet.add(new Group(Long.parseLong(groupIds[i])));
    }
  }
  
  public Right getReqRight()
  {
    return reqRight;
  }
  public void setReqRight(String newRightIdStr)
  {
    reqRight = new Right(parseLong(newRightIdStr));
  }
}/*@lineinfo:generated-code*/