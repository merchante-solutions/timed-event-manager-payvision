/*************************************************************************

  FILE: $URL: $

  Description:
  
  MESB2FUser
  
  Stores B2F user details including login state

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import com.mes.support.SimpleDataBean;
import com.mes.support.SyncLog;
import com.mes.support.XSSFilter;

public class MESB2FUser extends SimpleDataBean
  implements HttpSessionBindingListener, Serializable
{
  // user states define where the user is regarding login/activation/registration
  public static final int     USER_STATE_UNINITIALIZED      = -1;     // nothing from user yet
  public static final int     USER_STATE_STANDARD_AUTH      = 10;
  public static final int     USER_STATE_GET_ACTIVATION     = 20;
  public static final int     USER_STATE_REGISTER_IMG       = 30;
  public static final int     USER_STATE_REGISTER_MSG       = 40;
  public static final int     USER_STATE_REGISTER_QUESTIONS = 50;
  public static final int     USER_STATE_CREATE_PASSWORD    = 60;
  public static final int     USER_STATE_AUTHENTICATE       = 70;
  public static final int     USER_STATE_AUTHENTICATED      = 80;
  public static final int     USER_STATE_REGISTER_IP        = 90;
  
  // tricipher statuses define the current status of the user with regards to 
  // the TriCipher database
  public static final int     TC_STATUS_MES_NOT_EXIST   = 0;
  public static final int     TC_STATUS_TC_NOT_EXIST    = 100;
  public static final int     TC_STATUS_UNACTIVATED     = 200;
  public static final int     TC_STATUS_ACTIVATED       = 300;
  public static final int     TC_STATUS_UNREGISTERED    = 400;
  public static final int     TC_STATUS_ERROR           = 9999;
  
  public static final String  WELCOME_MSG_START         = "Log In Here";
  public static final String  WELCOME_MSG_ACTIVATION    = "First Time Activation";
  public static final String  WELCOME_MSG_REGISTER      = "Select Your Welcome Image";
  public static final String  WELCOME_MSG_WELCOME_MSG   = "Enter Welcome Message";
  public static final String  WELCOME_MSG_QUESTIONS     = "Select Questions and Answers";
  public static final String  WELCOME_MSG_PASSWORD      = "Create Password";
  public static final String  WELCOME_MSG_BROWSER_CFG   = "New Location";
  public static final String  WELCOME_MSG_AUTHENTICATE  = "Confirm Your Image and Phrase";
  public static final String  WELCOME_MSG_REGISTER_IP   = "Answer Registration Question";
  
  public static final String  SUBMIT_STANDARD           = "<input type=\"submit\" name=\"submit\" value=\"Submit\">";
  public static final String  SUBMIT_EMPTY              = "";
  
  public static final String  MSG_EMPTY                 = "";
  public static final String  MSG_UNINITIALIZED         = 
    "<b>Please Note:</b> We are implementing new online security features.  You may notice a slight modification to the login process as we make these changes. <br><br>Please enter your user ID to start the login process.";
  public static final String  MSG_GET_ACTIVATION_CODE   = 
    "<b>Enhanced Login Security</b><br> " + 
    "The login process has been changed to provide greater information security.  As part of this process, you are asked for additional verification information and provided with tools to help you verify access to this website.  When asked for an Activation Code, please enter your existing password.  Once the initial login process is completed, only your User ID and Password are needed for future logins.  If you log in from a different computer or use the Profile Change option on the Main Menu, you may be asked to repeat the initial login process or answer a verification question.";

  public static final String  MSG_GET_PASSWORD          = 
    "Please provide your password to complete the login process.";
  public static final String  MSG_REGISTER_IMG          = 
    "Select new image by clicking on the appropriate \"Select This Image\" link or by clicking on the image itself.<br><br>The picture you "+
    "select will be something you should look for each time you log in.  You "+
    "should see the picture you select before you enter your password thereby "+
    "providing you confidence you are at our legitimate web site.  This simple "+
    "confidence component functionality will help thwart anyone attempting to "+
    "fool you into giving your password to a fake site.";
  public static final String  MSG_REGISTER_MSG          =
    "The welcome message you select will be something you should look for each "+
    "time you log in.  You should see the welcome message before you enter your "+
    "password, thereby providing you confidence that you are at our legitimate "+
    "web site.  This simple confidence component functionality will help thwart "+
    "anyone attempting to fool you into giving your password to a fake site.  "+
    "You will only see this picture and welcome message when you are at our "+
    "legitimate web site.";
  public static final String  MSG_QUESTIONS             =
    "Select questions and provide answers.  We will use these questions to verify "+
    "your identity for some transactions in the future.";  
  public static final String  MSG_CREATE_PASSWORD       =
    "You need to create a new password.  Enter your password only if you see "+
    "your personal welcome image and welcome message below.  Your new password "+
    "must be at least 8 characters and must contain at least one uppercase letter, "+
    "one lowercase letter, and one number";
  public static final String  MSG_AUTHENTICATE          = 
    "If you recognize your selected image, you'll know for sure that you are "+
    "at the valid web site and that it is safe to enter your password. If you "+
    "don't recognize the image or the welcome message, do not enter your password.";
  public static final String  MSG_REGISTER_IP           =
    "We haven't seen you log in from this computer before.  You must answer "+
    "one of the questions you provided earlier in order to proceed and register "+
    "this computer as an acceptable location.";
  
                                         
  public String loginName       = "";
  public String activationCode  = "";
  public String password        = "";
  public String confirmPassword = "";
  public String submitButton    = SUBMIT_STANDARD;
  public String submit          = "";
  public String img             = "";
  public String imgLoc          = "";
  public String welcomeMsg      = "";
  
  public String question1       = "";
  public String question2       = "";
  public String question3       = "";
  
  public String answer1         = "";
  public String answer2         = "";
  public String answer3         = "";
  
  public String userAnswer      = "";
  
  public String focusField      = "loginName";
  
  public int currentState;
  
  public boolean  userExistsInMES = false;
  
  private String errorString    = "";
  private String message        = MSG_UNINITIALIZED;
  private String welcomeMessage = WELCOME_MSG_START;
  private boolean usesB2f       = false;
  private boolean doConvert     = false;
  private boolean mustRegister  = false;
  
  private byte[] welcomeBlob    = null;
  private String welcomeBlobContentType = "";
  private String randomQuestion  = "";
  private String randomAnswer    = "";
  
  /*
  ** Methods to support HttpSessionBindingListener
  */
  public synchronized void valueBound(HttpSessionBindingEvent event)
  {
    // set state of user to uninitialized to start the process
    //System.out.println("MESB2FUser object bound to session " + DateTimeFormatter.getCurDateTimeString());
  }
  
  public synchronized void valueUnbound(HttpSessionBindingEvent event)
  {
    // reset state -- although this object should be cleaned up after so 
    // this should be unnecessary
    currentState  = USER_STATE_UNINITIALIZED;
    //System.out.println("MESB2FUser object UNbound from session " + DateTimeFormatter.getCurDateTimeString());
  }
  
  public MESB2FUser()
  {
    currentState  = USER_STATE_UNINITIALIZED;
  }
  
  public void doProcess(HttpServletRequest request, HttpServletResponse response, UserBean user)
  {
    try
    {
      // reset error message to start
      setError("");
      
      //this.request  = request;
      //this.response = response;
      
      // process current state, move to next state and take appropriate action
      processUserState(request, response, user);
    }
    catch(Exception e)
    {
      setError("doProcess(): " + e.toString());
      SyncLog.LogEntry(this.getClass().getName() + "::doProcess()", e.toString());
    }
  }
  
  /*
  ** public void processLoginName
  **
  ** Validates loginName against the MES user database as well as the TriCipher
  ** database and sets the user state accordingly
  */
  public void processLoginName(HttpServletRequest request)
  {
    try
    {
      switch(TCUserManager.getTCUserStatus(this, request))
      {
        case TC_STATUS_MES_NOT_EXIST:
          // user does not exist in MES database -- start over
          setError("Invalid User");
          setCurrentState(USER_STATE_UNINITIALIZED);
          break;
          
        case TC_STATUS_TC_NOT_EXIST:
          // user doesn't yet exist in TC database.  Get password to start registration
          if(usesB2f)
          {
            // first create user in TC database, then set to get activation code
            doConvert = true;
            setCurrentState(USER_STATE_GET_ACTIVATION);
          }
          else
          {
            // don't bother with tricipher at all -- just show password field
            setCurrentState(USER_STATE_STANDARD_AUTH);
          }
          break;
          
        case TC_STATUS_UNACTIVATED:
          // user needs to register by putting in their activation code
          if(usesB2f)
          {
            doConvert = false;
            setCurrentState(USER_STATE_GET_ACTIVATION);
          }
          else
          {
            setCurrentState(USER_STATE_STANDARD_AUTH);
          }
          break;
        
        case TC_STATUS_ACTIVATED:
          // user needs to enter password 
          if(usesB2f)
          {
            setCurrentState(USER_STATE_AUTHENTICATE);
          }
          else
          {
            setCurrentState(USER_STATE_STANDARD_AUTH);
          }
          break;
          
        case TC_STATUS_UNREGISTERED:
          // user needs to answer one of his questions
          if(usesB2f)
          {
            setCurrentState(USER_STATE_REGISTER_IP);
          }
          else
          {
            setCurrentState(USER_STATE_STANDARD_AUTH);
          }
          break;
        
        case TC_STATUS_ERROR:
        default:
          setCurrentState(USER_STATE_UNINITIALIZED);
          setError("Invalid user");
          break;
      }
    }
    catch(Exception e)
    {
      setError(e.toString());
      System.out.println("processLoginName(): " + e.toString());
    }
  }
  
  private void processUserState(HttpServletRequest request, HttpServletResponse response, UserBean user)
  {
    try
    {
      switch(getCurrentState())
      {
        case USER_STATE_UNINITIALIZED:
          if(getLoginName() == null || getLoginName().equals(""))
          {
            // must provide a login name
            setError("Please provide a User ID");
          }
          else
          {
            // allow new current state to be set depending on user
            processLoginName(request);
          }
          break;
          
        case USER_STATE_GET_ACTIVATION:
          // user has entered activation code.  if correct user moves to
          // registration step (choosing welcome message, questions/answers, and image
          if(TCUserManager.activationCodeCorrect(loginName, activationCode))
          {
            if(doConvert)
            {
              // create user in database
              if(TCUserManager.convertMESUser(loginName, activationCode))
              {
                setError("");
                setCurrentState(USER_STATE_REGISTER_IMG);
              }
              else
              {
                setError("Unable to convert MES User");
                setCurrentState(USER_STATE_GET_ACTIVATION);
              }
            }
            else
            {
              setError("");
              setCurrentState(USER_STATE_REGISTER_IMG);
            }
          }
          else
          {
            // incorrect activation code -- remain in this state
            setError("Incorrect Activation Code");
            setCurrentState(USER_STATE_GET_ACTIVATION);
          }
          break;
          
        case USER_STATE_REGISTER_IMG:
          setError("");
          
          if( getImg().equals("") )
          {
            setError("Please select welcome image");
          }
          else
          {
            setError("");
            setCurrentState(USER_STATE_REGISTER_MSG);
          }
          break;
          
        case USER_STATE_REGISTER_MSG:
          if( getWelcomeMsg().equals("") )
          {
            setError("Please enter a welcome message");
          }
          else
          {
            setError("");
            setCurrentState(USER_STATE_REGISTER_QUESTIONS);
          }
          break;
          
        case USER_STATE_REGISTER_QUESTIONS:
          if( getQuestion1().equals("") || getQuestion2().equals("") || getQuestion3().equals("") ||
              (getQuestion1().equals(getQuestion2()) || getQuestion2().equals(getQuestion3()) || 
                getQuestion1().equals(getQuestion3())) )
          {
            setError("Please select three unique questions.");
          }
          else if( getAnswer1().equals("") || getAnswer2().equals("") || getAnswer3().equals("") )
          {
            setError("Please provide an answer to each question.");
          }
          else
          {
            // questions and answers were selected successfully
            setCurrentState(USER_STATE_CREATE_PASSWORD);
          }
          break;
          
        case USER_STATE_CREATE_PASSWORD:
          if( getPassword().equals("") || getConfirmPassword().equals("") ||
              (! getPassword().equals(getConfirmPassword() ) ) )
          {
            setError("Passwords do not match");
          }
          else
          {
            // we have everything we need to activate the user now
            if(TCUserManager.activateUser(this, request, response))
            {
              setCurrentState(USER_STATE_AUTHENTICATED);
            }
          }
          break;
          
        case USER_STATE_AUTHENTICATE:
          // user is an activated B2F User 
          // this means the user has successfully entered their user name and
          // has now entered a password - TC user
          if(TCUserManager.loginTricipher(this, request, response))
          {
            setCurrentState(USER_STATE_AUTHENTICATED);
          }
          else
          {
            setError("Incorrect Password");
          }
          break;
          
        case USER_STATE_STANDARD_AUTH:
          // process password that has been entered 
          if(password.equals(""))
          {
            setError("Please enter a password");
            setCurrentState(USER_STATE_STANDARD_AUTH);
          }
          else if( user.validate(loginName, password, request) )
          {
            setCurrentState(USER_STATE_AUTHENTICATED);
          }
          else
          {
            setError("Invalid password");
          }
          break;
          
        case USER_STATE_AUTHENTICATED:
          break;
          
        case USER_STATE_REGISTER_IP:
          // retrieve user data from TriCipher appliance
          if( randomAnswer.toLowerCase().equals(userAnswer.toLowerCase()) )
          {
            mustRegister = true;
            setCurrentState(USER_STATE_AUTHENTICATE);
          }
          else
          {
            setError("Incorrect answer");
          }
          break;
          
        default:
          setCurrentState(USER_STATE_UNINITIALIZED);
          break;
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::processUserState()", e.toString());
    }
  }
  
  public int getCurrentState()
  {
    return( currentState );
  }
  public void setCurrentState(int newState)
  {
    // set correct message based on new state
    switch(newState)
    {
      case USER_STATE_UNINITIALIZED:
        setMessage(MSG_UNINITIALIZED);
        setWelcomeMessage(WELCOME_MSG_START);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "loginName";
        break;
     
      case USER_STATE_STANDARD_AUTH:
        setMessage(MSG_GET_PASSWORD);
        setWelcomeMessage(WELCOME_MSG_START);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "password";
        break;
        
      case USER_STATE_GET_ACTIVATION:
        setMessage(MSG_GET_ACTIVATION_CODE);
        setWelcomeMessage(WELCOME_MSG_ACTIVATION);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "activationCode";
        break;
        
      case USER_STATE_REGISTER_IMG:
        setMessage(MSG_EMPTY);
        setWelcomeMessage(WELCOME_MSG_REGISTER);
        setSubmitButton(SUBMIT_EMPTY);
        focusField = "loginName";
        break;
        
      case USER_STATE_REGISTER_MSG:
        setMessage(MSG_EMPTY);
        setWelcomeMessage(WELCOME_MSG_WELCOME_MSG);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "welcomeMsg";
        break;
        
      case USER_STATE_REGISTER_QUESTIONS:
        setMessage(MSG_QUESTIONS);
        setWelcomeMessage(WELCOME_MSG_QUESTIONS);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "answer1";
        break;
        
      case USER_STATE_CREATE_PASSWORD:
        setMessage(MSG_CREATE_PASSWORD);
        setWelcomeMessage(WELCOME_MSG_PASSWORD);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "password";
        break;
        
      case USER_STATE_AUTHENTICATE:
        setMessage(MSG_AUTHENTICATE);
        setWelcomeMessage(WELCOME_MSG_AUTHENTICATE);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "password";
        break;
        
      case USER_STATE_AUTHENTICATED:
        setMessage("");
        setWelcomeMessage("");
        setSubmitButton(SUBMIT_STANDARD);
        break;
        
      case USER_STATE_REGISTER_IP:
        setMessage(MSG_REGISTER_IP);
        setWelcomeMessage(WELCOME_MSG_REGISTER_IP);
        setSubmitButton(SUBMIT_STANDARD);
        focusField = "userAnswer";
        break;
        
      default:
        setMessage(MSG_UNINITIALIZED);
        setWelcomeMessage(WELCOME_MSG_START);
        setSubmitButton(SUBMIT_STANDARD);
        break;
    };
    
    currentState = newState;
  }
  
  public void invalidate()
  {
    setLoginName("");
    setPassword("");
    setError("");
    
    if(getCurrentState() != USER_STATE_UNINITIALIZED)
    {
      setCurrentState(USER_STATE_UNINITIALIZED);
    }
  }
  
  public void setLoginName(String loginName)
  {
    this.loginName = loginName;
    
    if(!loginName.equals(""))
    {
      try
      {
        usesB2f = TCUserManager.shouldUseB2f(this);
      }
      catch(Exception e)
      {
      }
    }
  }
  public String getLoginName()
  {
    return( loginName );
  }
  
  public void setActivationCode(String activationCode)
  {
    this.activationCode = activationCode;
  }
  public String getActivationCode()
  {
    if(activationCode == null)
    {
      activationCode = "";
    }
    return( activationCode );
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  public String getPassword()
  {
    if(password == null)
    {
      password = "";
    }
    return( password );
  }
  
  public void setConfirmPassword(String confirmPassword)
  {
    this.confirmPassword = confirmPassword;
  }
  public String getConfirmPassword()
  {
    if(confirmPassword == null)
    {
      confirmPassword = "";
    }
    return( confirmPassword );
  }
  
  public void setError(String errMsg)
  {
    errorString = errMsg;
  }
  public String getError()
  {
    if(errorString == null)
    {
      errorString = "";
    }
    return( errorString );
  }
  
  private void setMessage(String msg)
  {
    message = msg;
  }
  public String getMessage()
  {
    if(message == null)
    {
      message = "";
    }
    return( message );
  }
  
  public boolean doesUseB2f()
  {
    return( usesB2f );
  }
  public void setUseB2f(boolean val)
  {
    usesB2f = val;
  }
  
  private void setWelcomeMessage(String msg)
  {
    welcomeMessage = msg;
  }
  public String getWelcomeMessage()
  {
    if(welcomeMessage == null)
    {
      welcomeMessage = "";
    }
    return( welcomeMessage );
  }
  
  public void setSubmitButton(String submitButton)
  {
    this.submitButton = submitButton;
  }
  public String getSubmitButton()
  {
    if(submitButton == null)
    {
      submitButton = "";
    }
    return( submitButton );
  }
  
  public void setImg(String img)
  {
    this.img = img;
    
    int imgNum = 0;
    try
    {
      imgNum = Integer.parseInt(img);
    }
    catch(Exception e)
    {
    }
    
    imgLoc = "/images/b2f/";
    if(imgNum < 10)
    {
      imgLoc += "00" + imgNum + ".jpg";
    }
    else if(imgNum < 100)
    {
      imgLoc += "0" + imgNum + ".jpg";
    }
    else
    {
      imgLoc += imgNum + ".jpg";
    }
  }
  public String getImg()
  {
    if(img == null)
    {
      img = "";
    }
    
    return( img );
  }
  
  public String getImgLoc()
  {
    return imgLoc;
  }
  
  public void setWelcomeMsg(String welcomeMsg)
  {
    this.welcomeMsg = XSSFilter.encodeXSS(welcomeMsg);
  }
  public String getWelcomeMsg()
  {
    if( welcomeMsg == null )
    {
      welcomeMsg = "";
    }
    
    return welcomeMsg;
  }
  
  public void setQuestion1(String question1)
  {
    this.question1 = XSSFilter.encodeXSS(question1);
  }
  public String getQuestion1()
  {
    if(question1 == null)
    {
      question1 = "";
    }
    return( question1 );
  }
  public void setQuestion2(String question2)
  {
    this.question2 = XSSFilter.encodeXSS(question2);
  }
  public String getQuestion2()
  {
    if(question2 == null)
    {
      question2 = "";
    }
    return( question2 );
  }
  public void setQuestion3(String question3)
  {
    this.question3 = XSSFilter.encodeXSS(question3);
  }
  public String getQuestion3()
  {
    if(question3 == null)
    {
      question3 = "";
    }
    return( question3 );
  }
  
  public void setAnswer1(String answer1)
  {
    this.answer1 = XSSFilter.encodeXSS(answer1);
  }
  public String getAnswer1()
  {
    if(answer1 == null)
    {
      answer1 = "";
    }
    return( answer1 );
  }
  public void setAnswer2(String answer2)
  {
    this.answer2 = XSSFilter.encodeXSS(answer2);
  }
  public String getAnswer2()
  {
    if(answer2 == null)
    {
      answer2 = "";
    }
    return( answer2 );
  }
  public void setAnswer3(String answer3)
  {
    this.answer3 = XSSFilter.encodeXSS(answer3);
  }
  public String getAnswer3()
  {
    if(answer3 == null)
    {
      answer3 = "";
    }
    return( answer3 );
  }

  public void setChallengeResponse(Hashtable ht)
  {
    try
    {
      // set questions and answers from hashtable
      Enumeration keys = ht.keys();
    
      int idx = 0;
      while(keys.hasMoreElements())
      {
        String question = (String)keys.nextElement();
        String answer = (String)ht.get(question);
      
        switch(idx)
        {
          case 0:
            setQuestion1(question);
            setAnswer1(answer);
            ++idx;
            break;
          
          case 1:
            setQuestion2(question);
            setAnswer2(answer);
            ++idx;
            break;
          
          case 2:
            setQuestion3(question);
            setAnswer3(answer);
            ++idx;
            break;
        
          default:
            break;
        }
      }
    }
    catch(Exception e)
    {
    }
  }
  
  public void setWelcomeBlob(byte[] blob)
  {
    welcomeBlob = blob;
  }
  public byte[] getWelcomeBlob()
  {
    return( welcomeBlob );
  }
  public void setWelcomeBlobContentType(String contentType)
  {
    welcomeBlobContentType = contentType;
  }
  public String getWelcomeBlobContentType()
  {
    return( welcomeBlobContentType );
  }
  
  public void setRandomQuestion()
  {
    try
    {
      // get random number from 1 to 3 and set the random question and answer to that value
      Random generator = new Random(System.currentTimeMillis());
      
      int randNum = generator.nextInt();
      
      randNum = (randNum < 0 ? randNum * -1 : randNum);
      
      switch(randNum % 3)
      {
        case 0:
          randomQuestion  = question1;
          randomAnswer    = answer1;
          break;
          
        case 1:
          randomQuestion  = question2;
          randomAnswer    = answer2;
          break;
          
        case 2:
          randomQuestion  = question3;
          randomAnswer    = answer3;
          break;
          
        default:
          // default to question 2
          randomQuestion  = question2;
          randomAnswer    = answer2;
          break;
      }
    }
    catch(Exception e)
    {
    }
  }
  
  public String getRandomQuestion()
  {
    return( randomQuestion );
  }
  
  public void setRandomAnswer(String answer)
  {
    randomAnswer = answer;
  }
  public String getRandomAnswer()
  {
    return( randomAnswer );
  }
  
  public void setUserAnswer(String answer)
  {
    userAnswer = answer;
  }
  public String getUserAnswer()
  {
    return( userAnswer );
  }
}
  
