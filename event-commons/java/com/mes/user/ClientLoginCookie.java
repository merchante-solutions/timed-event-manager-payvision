/*@lineinfo:filename=ClientLoginCookie*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/ClientLoginCookie.sqlj $

  Description:  

  UserBean
  
  Allows validation of user id's and passwords and gives access to various 
  user attributes such as user type, user group membership and user rights.
  
  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-06-16 10:54:10 -0700 (Mon, 16 Jun 2008) $
  Version            : $Revision: 14969 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.sql.ResultSet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ClientLoginCookie extends SQLJConnectionBase
{
  public static final int     CLIENT_DEFAULT          = 1000;
  //public static final int     CLIENT_UBOC             = 1001;  -- obsoleted
  public static final int     CLIENT_SVB              = 1002;
  public static final int     CLIENT_BBT              = 1003;
  public static final int     CLIENT_GOLD_BANK        = 1004;
  public static final int     CLIENT_CERTEGY          = 1005;
  public static final int     CLIENT_BOC              = 1006;
  public static final int     CLIENT_WAM              = 1007;
  public static final int     CLIENT_OCEAN            = 1008;
  public static final int     CLIENT_MBS              = 1009;
  public static final int     CLIENT_CBT              = 1010;
  public static final int     CLIENT_CCB              = 1011;
  public static final int     CLIENT_CERTEGY_GENERIC  = 1012;
  public static final int     CLIENT_RIVERCITY        = 1013;
  public static final int     CLIENT_FOOTHILL         = 1014;
  public static final int     CLIENT_ELMHURST         = 1015;
  public static final int     CLIENT_MTWEST           = 1017;
  public static final int     CLIENT_BANNER           = 1018;
  public static final int     CLIENT_ELMHURST_CLIENT  = 1019;
  public static final int     CLIENT_SABRE            = 1020;
  public static final int     CLIENT_BUTTE            = 1021;
  public static final int     CLIENT_BANKSERV         = 1022;
  public static final int     CLIENT_CSB              = 1023;
  public static final int     CLIENT_FMBANK           = 1024;
  public static final int     CLIENT_FFB              = 1025;
  public static final int     CLIENT_COMDATA          = 1026;
  public static final int     CLIENT_STERLING         = 1027;
  public static final int     CLIENT_EXCHANGE         = 1028;
  public static final int     CLIENT_VPS_ISO          = 1029;
  public static final int     CLIENT_ARMSTRONG        = 1030;
  public static final int     CLIENT_CENTURY          = 1031;
  public static final int     CLIENT_STERLING_3942    = 1032;
  
  public static final int     MAX_COOKIE_AGE          = 7776000;   // 7776000 secs = 90 days
  
  public static final String  CLIENT_COOKIE_NAME      = "mesClient";
  public static final String  DEFAULT_LOGIN_PAGE      = "/jsp/secure/Login.jsp";
  public static final String  DEFAULT_LOGOUT_PAGE     = "/jsp/secure/Logout.jsp";
  public static final String  DEFAULT_TOP_NAV         = "/jsp/include/mes_standard_top.jsp";
  public static final String  DEFAULT_BOTTOM_NAV      = "/jsp/include/mes_standard_bot.jsp";
  public static final String  DEFAULT_STYLE_SHEET     = "/ss/mes_default.css";
  
  private   int       clientId      = 0;
  private   Cookie    clientCookie  = null;
  
  private   String    loginPage     = "";
  private   String    logoutPage    = "";
  private   String    topNav        = "";
  private   String    bottomNav     = "";
  private   String    styleSheet    = "";
  
  public ClientLoginCookie(int clientId)
  {
    initialize(clientId);
  }
  
  public int getClientId()
  {
    return( clientId );
  }
  
  private void initialize(int clientId)
  {
    this.clientId = clientId;
    
    // instantiate the cookie
    clientCookie = new Cookie(CLIENT_COOKIE_NAME, Integer.toString(clientId));
    
    // initialize the cookie
    //clientCookie.setDomain(".merchante-solutions.com");
    clientCookie.setMaxAge(MAX_COOKIE_AGE);
    clientCookie.setPath("/jsp");
    clientCookie.setVersion(0); // original Netscape specification
    clientCookie.setComment("Merchant e-Solutions client identification");
    
    // get attributes (login page, top/bottom nav, and style sheet
    getData();
    
  }
  
  private void getData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:130^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(login_url, :DEFAULT_LOGIN_PAGE)           login_page,
//                  nvl(client_top_nav, :DEFAULT_TOP_NAV)         top_nav,
//                  nvl(client_bottom_nav, :DEFAULT_BOTTOM_NAV)   bottom_nav,
//                  nvl(client_style_sheet, :DEFAULT_STYLE_SHEET) style_sheet,
//                  nvl(logout_url, :DEFAULT_LOGOUT_PAGE)         logout_page
//          from    client_login_page
//          where   client_id = :clientId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(login_url,  :1 )           login_page,\n                nvl(client_top_nav,  :2 )         top_nav,\n                nvl(client_bottom_nav,  :3 )   bottom_nav,\n                nvl(client_style_sheet,  :4 ) style_sheet,\n                nvl(logout_url,  :5 )         logout_page\n        from    client_login_page\n        where   client_id =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.ClientLoginCookie",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,DEFAULT_LOGIN_PAGE);
   __sJT_st.setString(2,DEFAULT_TOP_NAV);
   __sJT_st.setString(3,DEFAULT_BOTTOM_NAV);
   __sJT_st.setString(4,DEFAULT_STYLE_SHEET);
   __sJT_st.setString(5,DEFAULT_LOGOUT_PAGE);
   __sJT_st.setInt(6,clientId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.ClientLoginCookie",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        loginPage   = rs.getString("login_page");
        logoutPage  = rs.getString("logout_page");
        topNav      = rs.getString("top_nav");
        bottomNav   = rs.getString("bottom_nav");
        styleSheet  = rs.getString("style_sheet");
      }
      else
      {
        loginPage   = DEFAULT_LOGIN_PAGE;
        logoutPage  = DEFAULT_LOGOUT_PAGE;
        topNav      = DEFAULT_TOP_NAV;
        bottomNav   = DEFAULT_BOTTOM_NAV;
        styleSheet  = DEFAULT_STYLE_SHEET;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
      loginPage   = DEFAULT_LOGIN_PAGE;
      logoutPage  = DEFAULT_LOGOUT_PAGE;
      topNav      = DEFAULT_TOP_NAV;
      bottomNav   = DEFAULT_BOTTOM_NAV;
      styleSheet  = DEFAULT_STYLE_SHEET;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public String getTopNav()
  {
    return( topNav );
  }
  
  public String getBottomNav()
  {
    return( bottomNav );
  }
  
  public String getStyleSheet()
  {
    return( styleSheet );
  }
  
  public String getLoginPage()
  {
    if(loginPage == null || loginPage.equals(""))
    {
      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:203^9*/

//  ************************************************************
//  #sql [Ctx] { select  login_url 
//            from    client_login_page
//            where   client_id = :clientId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  login_url  \n          from    client_login_page\n          where   client_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.ClientLoginCookie",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,clientId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loginPage = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^9*/
      }
      catch(Exception e)
      {
        loginPage = DEFAULT_LOGIN_PAGE;
      }
      finally
      {
        cleanUp();
      }
    }
    
    return( loginPage );
  }
  
  public String getLogoutPage()
  {
    if(logoutPage == null || logoutPage.equals(""))
    {
      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:231^9*/

//  ************************************************************
//  #sql [Ctx] { select  logout_url 
//            from    client_login_page
//            where   client_id = :clientId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  logout_url  \n          from    client_login_page\n          where   client_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.ClientLoginCookie",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,clientId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   logoutPage = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^9*/
      }
      catch(Exception e)
      {
        logoutPage = "/jsp/secure/Logout.jsp";
      }
      finally
      {
        cleanUp();
      }
    }
    
    return( logoutPage );
  }
  
  public Cookie getCookie()
  {
    return clientCookie;
  }
  
  public static ClientLoginCookie getLoginCookie(HttpServletRequest request)
  {
    ClientLoginCookie result = null;
    try
    {
      // find this login cookie
      Cookie[] cookies = request.getCookies();
      
      for(int i=0; cookies != null && i < cookies.length; ++i)
      {
        Cookie thisCookie = cookies[i];
        
        if(thisCookie.getName().equals(CLIENT_COOKIE_NAME))
        {
          // found
          result = new ClientLoginCookie(Integer.parseInt(thisCookie.getValue()));
          break;
        }
      }
      
      if(result == null)
      {
        // couldn't find cookie in browser so create a default cookie
        result = new ClientLoginCookie(CLIENT_DEFAULT);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.user.ClientLoginCookie::getLoginCookie()", e.toString());
    }
    
    return result;
  }
  
  
}/*@lineinfo:generated-code*/