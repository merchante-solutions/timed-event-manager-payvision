/*@lineinfo:filename=UserRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserRecord.sqlj $

  Description:  
  
    User record object.
    
    Contains a users record.  Allows insertion, modification and
    deletion of records.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-06 11:02:28 -0700 (Mon, 06 Aug 2007) $
  Version            : $Revision: 13944 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.crypt.MD5;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class UserRecord extends DBBean
{
  // users table record fields
  private long              userId              = 0L;
  private String            loginName           = "";
  private String            password            = "";
  private String            passwordEnc         = "";
  private String            userName            = "";
  private String            address1            = "";
  private String            address2            = "";
  private String            city                = "";
  private String            state               = "";
  private String            zip                 = "";
  private String            email               = "";
  private long              phone;
  private long              fax;
  private long              mobile;
  private long              pager;
  private long              pagerPin;
  private long              affiliateId;
  private long              hierarchyNode;
  private String            createDate          = "";
  private String            accessUser          = "";
  private String            createTime          = "";
  private String            pswdChangeDate      = "";
  private String            pswdChangeTime      = "";
  private String            lastLoginDate       = "";
  private String            lastLoginTime       = "";
  private int               bankNum;
  private int               assoc;
  private int               bGroup1;
  private int               bGroup2;
  private int               bGroup3;
  private int               bGroup4;
  private int               bGroup5;
  private int               bGroup6;
  private String            thirdPartyId        = "";
  private String            comments            = "";
  private String            enabled             = "";
  private long              reportHierarchyNode = 0L;

  private String            confirmPassword     = "";
  private GroupSet          groups              = new GroupSet();
  private RightSet          rights              = new RightSet();
  private SalesRepRecord    repRec              = null;
  private UserTypeRecord    typeRec             = null;
  
  private boolean           isNew               = false;
  
  private  MD5               md5                = null;
  
  /*
  ** CONSTRUCTORS
  */
  public UserRecord()
  {
    initialize();
  }
  public UserRecord(DefaultContext Ctx)
  {
    this.Ctx  = Ctx;
    
    repRec    = new SalesRepRecord(Ctx);
    typeRec   = new UserTypeRecord(Ctx);
    md5       = new MD5();
  }
  public UserRecord(ResultSet rs)
  {
    initialize();
    loadResultSet(rs);
  }
  public UserRecord(long newId, DefaultContext Ctx)
  {
    this.Ctx = Ctx;
    
    repRec    = new SalesRepRecord(Ctx);
    typeRec   = new UserTypeRecord(Ctx);
    md5       = new MD5();
    
    userId = newId;
  }
  private void initialize()
  {
    // do standard setup
    md5       = new MD5();
    repRec    = new SalesRepRecord();
    typeRec   = new UserTypeRecord();
  }
  
  /*
  ** METHOD public boolean equals(Object o)
  **
  ** Determines if another object equals this one.
  */
  public boolean equals(Object o)
  {
    boolean isEqual = false;
    
    if (o.getClass() == this.getClass())
    {
      isEqual = (((UserRecord)o).getUserId() == userId);
    }

    return isEqual;
  }
  
  /*
  ** METHOD userExists
  **
  ** Returns true if this login name exists in the database already
  */
  public boolean userExists(String loginName)
  {
    boolean result = false;
    
    try
    {
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:165^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name) 
//          from    users
//          where   lower(login_name) = lower(:loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)  \n        from    users\n        where   lower(login_name) = lower( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^7*/
      
      result = (count > 0);
    }
    catch(Exception e)
    {
      logEntry("userExists(" + loginName + ")", e.toString());
    }
    
    return result;
  }
  
  /*
  ** METHOD hierarchyNodeExists
  **
  ** Returns true if this hierarchy node exists in the organization table
  */
  public boolean hierarchyNodeExists(long hierarchyNode)
  {
    boolean result = false;
    
    try
    {
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:195^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(org_group) 
//          from    organization
//          where   org_group = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(org_group)  \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/
      
      result = (count > 0);
    }
    catch(Exception e)
    {
      logEntry("hierarchyNodeExists(" + hierarchyNode + ")", e.toString());
    }
    
    return result;
  }
  
  /*
  ** METHOD public void loadResultSet(ResultSet rs)
  **
  ** Loads the contents of a ResultSet into internal field storage.
  */
  public void loadResultSet(ResultSet rs)
  {
    try
    {
      userId              = rs.getLong  ("user_id");
      loginName           = processString(rs.getString("login_name"));
      password            = processString(rs.getString("password"));
      passwordEnc         = processString(rs.getString("password_enc"));
      userName            = processString(rs.getString("name"));
      address1            = processString(rs.getString("address1"));
      address2            = processString(rs.getString("address2"));
      city                = processString(rs.getString("city"));
      state               = processString(rs.getString("state"));
      zip                 = processString(rs.getString("zip"));
      email               = processString(rs.getString("email"));
      phone               = rs.getLong  ("phone");
      fax                 = rs.getLong  ("fax");
      mobile              = rs.getLong  ("mobile");
      pager               = rs.getLong  ("pager");
      pagerPin            = rs.getLong  ("pager_pin");
      affiliateId         = rs.getLong  ("affiliate_id");
      hierarchyNode       = rs.getLong  ("hierarchy_node");
      createDate          = DateTimeFormatter.getFormattedDate(rs.getDate("create_date"), "MM/dd/yyyy");
      createTime          = DateTimeFormatter.getFormattedDate(rs.getTime("create_date"), "hh:mm:ss");
      pswdChangeDate      = DateTimeFormatter.getFormattedDate(rs.getDate("pswd_change_date"), "MM/dd/yyyy");
      pswdChangeTime      = DateTimeFormatter.getFormattedDate(rs.getTime("pswd_change_date"), "hh:mm:ss");
      lastLoginDate       = DateTimeFormatter.getFormattedDate(rs.getDate("last_login_date"), "MM/dd/yyyy");
      lastLoginTime       = DateTimeFormatter.getFormattedDate(rs.getTime("last_login_date"), "hh:mm:ss");
      typeRec.setUserTypeId(rs.getLong("type_id"));
      bankNum             = rs.getInt   ("bank_num");
      assoc               = rs.getInt   ("bank_assoc");
      bGroup1             = rs.getInt   ("bank_group1");
      bGroup2             = rs.getInt   ("bank_group2");
      bGroup3             = rs.getInt   ("bank_group3");
      bGroup4             = rs.getInt   ("bank_group4");
      bGroup5             = rs.getInt   ("bank_group5");
      bGroup6             = rs.getInt   ("bank_group6");
      thirdPartyId        = processString(rs.getString("third_party_user_id"));
      comments            = processString(rs.getString("comments"));
      enabled             = processString(rs.getString("enabled"));
      reportHierarchyNode = rs.getLong("report_hierarchy_node");
      
      String  createUser = rs.getString("create_user");
      accessUser          = createUser == null ? "SYSTEM" : createUser;
      
      confirmPassword   = password;
    }
    catch (Exception e)
    {
      logEntry("loadResultSet()", e.toString());
    }
  }
  
  /*
  ** METHOD private void refreshRights()
  **
  ** Reloads right set based on the current group set.
  */
  private void refreshRights()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    Group               group   = null;
    
    try
    {
      rights.clear();
      
      Iterator gIter = groups.getGroupIterator();
      
      while(gIter.hasNext())
      {
        group = (Group)gIter.next();
        
        /*@lineinfo:generated-code*//*@lineinfo:292^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  right_id
//            from    user_group_to_right
//            where   group_id = :group.getGroupId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_106 = group.getGroupId();
  try {
   String theSqlTS = "select  right_id\n          from    user_group_to_right\n          where   group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_106);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^9*/
        rs = it.getResultSet();
        
        while (rs.next())
        {
          rights.add(new Right(rs.getLong("right_id")));
        }
        rs.close();
        it.close();
      }
    }
    catch (Exception e)
    {
      logEntry("refreshRights()", e.toString());
    }
    finally
    {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  /*
  ** METHOD public refreshRightsData()
  **
  ** Wraps internal refreshRights() call with connect()...disconnect().
  */
  public void refreshRightsData()
  {
    try
    {
      connect();
      refreshRights();
    }
    catch(Exception e)
    {
      logEntry("refreshRightsData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
      
  /*
  ** METHOD private void refreshGroups()
  **
  ** Reloads group and right sets based on the current userId.
  */
  private void refreshGroups()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:353^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  group_id
//          from    user_to_group
//          where   user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  group_id\n        from    user_to_group\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^7*/
      
      groups.clear();
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        groups.add(new Group(rs.getLong("group_id")));
      }
      rs.close();  
      it.close();
      
      // add in groups from the user type record
      groups.add(typeRec.getGroupSet());
    
      // load user's rights
      refreshRights();
    }
    catch (Exception e)
    {
      logEntry("refreshGroups()", e.toString());
    }
    finally
    {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  /*
  ** METHOD public refreshGroupsData()
  **
  ** Wraps internal refreshGroups() call with connect()...disconnect().
  */
  public void refreshGroupsData()
  {
    try
    {
      connect();
      refreshGroups();
    }
    catch(Exception e)
    {
      logEntry("refreshGroupsData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
  }
      
  /*
  ** METHOD protected boolean get()
  **
  ** Loads the user record from users via the userId or loginName.
  **
  ** RETURNS: true if record found.
  */
  protected boolean get()
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    boolean             getOk     = false;
    String              whichGet  = "";
    
    try
    {
      // look up user record in users table by user id
      if(userId == 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:430^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    users
//            where   login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    users\n          where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:435^9*/
        
        whichGet = "by loginName";
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:441^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    users
//            where   user_id = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    users\n          where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^9*/
        
        whichGet = "by userId";
      }
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        // load the record into internal storage
        loadResultSet(rs);
        
        // load user type record
        typeRec.getData();

        // load user's groups & rights
        refreshGroups();
        
        // load the sales rep record
        if ((groups.contains(MesUsers.GROUP_MES) && 
             groups.contains(MesUsers.GROUP_SALES)) ||
            groups.contains(MesUsers.GROUP_DISCOVER))
        {
          repRec = new SalesRepRecord(userId, Ctx);
        }
        
        getOk = true;
        isNew = false;
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("get(" + whichGet + ")", e.toString());
    }
    finally
    {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
    }

    return getOk;
  }

  /*
  ** METHOD public boolean getData(String loadName)
  **
  ** Loads the user record in users table with the given login name.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  public boolean getData(String loadName)
  {
    loginName = loadName;
    userId = 0L;
    return getData();
  }
  
  /*
  ** METHOD public boolean getData(long loadId)
  **
  ** Loads the user record in users table with the given user id.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  public boolean getData(long loadId)
  {
    userId = loadId;
    return getData();
  }
  
  /*
  ** METHOD protected boolean validate()
  **
  ** Determines if all field values are valid.
  **
  ** RETURNS: true if all valid, else false.
  */
  protected boolean validate()
  {
    if(password == null)
    {
      password = "";
    }
    
    if(confirmPassword == null)
    {
      confirmPassword = "";
    }
    
    if (!confirmPassword.equals(password))
    {
      addError("Password does not match confirmation password.");
    }
    
    if(isNew && this.password.equals(""))
    {
      addError("Password cannot be blank.");
    }
    
    if((isNew || this.password.length() > 0) && this.password.length() < 6)
    {
      addError("Password must be at least 6 characters and/or digits.");
    }
    
    if(!repRec.validate())
    {
      Vector repErrs = repRec.getErrors();
      for (int i=0; i<repErrs.size(); ++i)
      {
        addError((String)repErrs.elementAt(i));
      }
    }
    
    // check to make sure this user id doesn't exist already if inserting a new record
    if(isNew && userExists(this.loginName))
    {
      addError("Login name \"" + this.loginName + "\" is already taken.  Please choose another login name.");
    }
    
    // check to make sure that this hierarchy node exists in the organization table
    if(this.hierarchyNode == 0L)
    {
      addError("You must provide a hierarchy node for this user.");
    }
    else if( ! hierarchyNodeExists(this.hierarchyNode))
    {
      addError("Hierarchy node " + this.hierarchyNode + " is invalid.");
    }
    
    return (!hasErrors());
  }
  
  /*
  ** METHOD protected java.sql.Date getPasswordChangeDate
  **
  ** Returns what the password change date should be set to for this particular
  ** user.  This is for users that should be forced to change their
  ** password after customer service resets it for them.
  */                                                      
  public java.sql.Date getPasswordChangeDate()
  {
    java.sql.Date result = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      // determine if this is a merchant user
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:601^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif
//          where   merchant_number = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.user.UserRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^7*/
      
      boolean isMerchant = (recCount > 0);
      
      if(isMerchant)
      {
        /*@lineinfo:generated-code*//*@lineinfo:613^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate-(upe.expiration_days+1))  new_date
//            from    users_password_expire upe, 
//                    mif m, 
//                    t_hierarchy th 
//            where   m.merchant_number = :hierarchyNode and 
//                    m.association_node = th.descendent and 
//                    th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and 
//                    th.ancestor = upe.hierarchy_node and 
//                    upper(nvl(upe.force_user_reset_merchant, 'N')) = 'Y' 
//            order by th.relation        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate-(upe.expiration_days+1))  new_date\n          from    users_password_expire upe, \n                  mif m, \n                  t_hierarchy th \n          where   m.merchant_number =  :1  and \n                  m.association_node = th.descendent and \n                  th.hier_type =  :2  and \n                  th.ancestor = upe.hierarchy_node and \n                  upper(nvl(upe.force_user_reset_merchant, 'N')) = 'Y' \n          order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          result = rs.getDate("new_date");
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:636^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate-(upe.expiration_days+1))  new_date
//            from    users_password_expire upe,
//                    t_hierarchy th
//            where   th.hier_type = 1 and
//                    th.descendent = :hierarchyNode and
//                    th.ancestor = upe.hierarchy_node and
//                    upper(nvl(upe.force_user_reset_user, 'N')) = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate-(upe.expiration_days+1))  new_date\n          from    users_password_expire upe,\n                  t_hierarchy th\n          where   th.hier_type = 1 and\n                  th.descendent =  :1  and\n                  th.ancestor = upe.hierarchy_node and\n                  upper(nvl(upe.force_user_reset_user, 'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.user.UserRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:645^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          result = rs.getDate("new_date");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getPasswordChangeDate("+hierarchyNode+")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** METHOD protected boolean submit()
  **
  ** Inserts or updates a user record.
  **
  ** RETURNS: true if submit successful, else false.
  */
  protected boolean submit()
  {
    boolean submitOk = false;
    
    try
    {
      // set the encrypted password
      if(this.password != null && ! this.password.equals(""))
      {
        setPasswordEnc(this.password);
      }
      
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:690^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_id) 
//          from    users
//          where   user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_id)  \n        from    users\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.user.UserRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:695^7*/
      
      if(count > 0)
      {
        // update existing user record
        /*@lineinfo:generated-code*//*@lineinfo:700^9*/

//  ************************************************************
//  #sql [Ctx] { update  users
//            set     login_name            = :loginName,
//                    name                  = :userName,
//                    address1              = :address1,
//                    address2              = :address2,
//                    city                  = :city,
//                    state                 = :state,
//                    zip                   = :zip,
//                    email                 = :email,
//                    phone                 = :phone,
//                    fax                   = :fax,
//                    mobile                = :mobile,
//                    pager                 = :pager,
//                    pager_pin             = :pagerPin,
//                    type_id               = :typeRec.getUserTypeId(),
//                    affiliate_id          = :affiliateId,
//                    hierarchy_node        = :hierarchyNode,
//                    bank_num              = :bankNum,
//                    bank_assoc            = :assoc,
//                    bank_group1           = :bGroup1,
//                    bank_group2           = :bGroup2,
//                    bank_group3           = :bGroup3,
//                    bank_group4           = :bGroup4,
//                    bank_group5           = :bGroup5,
//                    bank_group6           = :bGroup6,
//                    third_party_user_id   = :thirdPartyId,
//                    comments              = :comments,
//                    report_hierarchy_node = :reportHierarchyNode
//            where   user_id               = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_107 = typeRec.getUserTypeId();
   String theSqlTS = "update  users\n          set     login_name            =  :1 ,\n                  name                  =  :2 ,\n                  address1              =  :3 ,\n                  address2              =  :4 ,\n                  city                  =  :5 ,\n                  state                 =  :6 ,\n                  zip                   =  :7 ,\n                  email                 =  :8 ,\n                  phone                 =  :9 ,\n                  fax                   =  :10 ,\n                  mobile                =  :11 ,\n                  pager                 =  :12 ,\n                  pager_pin             =  :13 ,\n                  type_id               =  :14 ,\n                  affiliate_id          =  :15 ,\n                  hierarchy_node        =  :16 ,\n                  bank_num              =  :17 ,\n                  bank_assoc            =  :18 ,\n                  bank_group1           =  :19 ,\n                  bank_group2           =  :20 ,\n                  bank_group3           =  :21 ,\n                  bank_group4           =  :22 ,\n                  bank_group5           =  :23 ,\n                  bank_group6           =  :24 ,\n                  third_party_user_id   =  :25 ,\n                  comments              =  :26 ,\n                  report_hierarchy_node =  :27 \n          where   user_id               =  :28";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,userName);
   __sJT_st.setString(3,address1);
   __sJT_st.setString(4,address2);
   __sJT_st.setString(5,city);
   __sJT_st.setString(6,state);
   __sJT_st.setString(7,zip);
   __sJT_st.setString(8,email);
   __sJT_st.setLong(9,phone);
   __sJT_st.setLong(10,fax);
   __sJT_st.setLong(11,mobile);
   __sJT_st.setLong(12,pager);
   __sJT_st.setLong(13,pagerPin);
   __sJT_st.setLong(14,__sJT_107);
   __sJT_st.setLong(15,affiliateId);
   __sJT_st.setLong(16,hierarchyNode);
   __sJT_st.setInt(17,bankNum);
   __sJT_st.setInt(18,assoc);
   __sJT_st.setInt(19,bGroup1);
   __sJT_st.setInt(20,bGroup2);
   __sJT_st.setInt(21,bGroup3);
   __sJT_st.setInt(22,bGroup4);
   __sJT_st.setInt(23,bGroup5);
   __sJT_st.setInt(24,bGroup6);
   __sJT_st.setString(25,thirdPartyId);
   __sJT_st.setString(26,comments);
   __sJT_st.setLong(27,reportHierarchyNode);
   __sJT_st.setLong(28,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:731^9*/
      }
      else
      {
        // get a new user id (next value in users_sequence)
        long newUserId = 0L;
        
        /*@lineinfo:generated-code*//*@lineinfo:738^9*/

//  ************************************************************
//  #sql [Ctx] { select  users_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  users_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.user.UserRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newUserId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:742^9*/
        
        setUserId(newUserId);
        
        // insert a new user record
        /*@lineinfo:generated-code*//*@lineinfo:747^9*/

//  ************************************************************
//  #sql [Ctx] { insert into users
//            (
//              login_name,
//              name,
//              address1,
//              address2,
//              city,
//              state,
//              zip,
//              email,
//              phone,
//              fax,
//              mobile,
//              pager,
//              pager_pin,
//              type_id,
//              affiliate_id,
//              hierarchy_node,
//              bank_num,
//              bank_assoc,
//              bank_group1,
//              bank_group2,
//              bank_group3,
//              bank_group4,
//              bank_group5,
//              bank_group6,
//              third_party_user_id,
//              comments,
//              user_id,
//              create_date,
//              create_user,
//              pswd_change_date,
//              enabled,
//              report_hierarchy_node
//            )
//            values
//            (
//              :loginName,
//              :userName,
//              :address1,
//              :address2,
//              :city,
//              :state,
//              :zip,
//              :email,
//              :phone,
//              :fax,
//              :mobile,
//              :pager,
//              :pagerPin,
//              :typeRec.getUserTypeId(),
//              :affiliateId,
//              :hierarchyNode,
//              :bankNum,
//              :assoc,
//              :bGroup1,
//              :bGroup2,
//              :bGroup3,
//              :bGroup4,
//              :bGroup5,
//              :bGroup6,
//              :thirdPartyId,
//              :comments,
//              :userId,
//              sysdate,
//              :accessUser,
//              sysdate,
//              'Y',
//              :reportHierarchyNode
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_108 = typeRec.getUserTypeId();
   String theSqlTS = "insert into users\n          (\n            login_name,\n            name,\n            address1,\n            address2,\n            city,\n            state,\n            zip,\n            email,\n            phone,\n            fax,\n            mobile,\n            pager,\n            pager_pin,\n            type_id,\n            affiliate_id,\n            hierarchy_node,\n            bank_num,\n            bank_assoc,\n            bank_group1,\n            bank_group2,\n            bank_group3,\n            bank_group4,\n            bank_group5,\n            bank_group6,\n            third_party_user_id,\n            comments,\n            user_id,\n            create_date,\n            create_user,\n            pswd_change_date,\n            enabled,\n            report_hierarchy_node\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n            sysdate,\n             :28 ,\n            sysdate,\n            'Y',\n             :29 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,userName);
   __sJT_st.setString(3,address1);
   __sJT_st.setString(4,address2);
   __sJT_st.setString(5,city);
   __sJT_st.setString(6,state);
   __sJT_st.setString(7,zip);
   __sJT_st.setString(8,email);
   __sJT_st.setLong(9,phone);
   __sJT_st.setLong(10,fax);
   __sJT_st.setLong(11,mobile);
   __sJT_st.setLong(12,pager);
   __sJT_st.setLong(13,pagerPin);
   __sJT_st.setLong(14,__sJT_108);
   __sJT_st.setLong(15,affiliateId);
   __sJT_st.setLong(16,hierarchyNode);
   __sJT_st.setInt(17,bankNum);
   __sJT_st.setInt(18,assoc);
   __sJT_st.setInt(19,bGroup1);
   __sJT_st.setInt(20,bGroup2);
   __sJT_st.setInt(21,bGroup3);
   __sJT_st.setInt(22,bGroup4);
   __sJT_st.setInt(23,bGroup5);
   __sJT_st.setInt(24,bGroup6);
   __sJT_st.setString(25,thirdPartyId);
   __sJT_st.setString(26,comments);
   __sJT_st.setLong(27,userId);
   __sJT_st.setString(28,accessUser);
   __sJT_st.setLong(29,reportHierarchyNode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^9*/
        
        isNew = true;
      }
      
      // set the password
      java.sql.Date changeDate = getPasswordChangeDate();
      
      if(! this.password.equals(""))
      {
        /*@lineinfo:generated-code*//*@lineinfo:829^9*/

//  ************************************************************
//  #sql [Ctx] { update  users
//            set     password_enc = :passwordEnc,
//                    pswd_change_date = :changeDate
//            where   user_id = :userId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n          set     password_enc =  :1 ,\n                  pswd_change_date =  :2 \n          where   user_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,passwordEnc);
   __sJT_st.setDate(2,changeDate);
   __sJT_st.setLong(3,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:835^9*/
        
        // reset this user on the TriCipher appliance
        TCUserManager.resetUser(loginName, this.password);
      }
      
      // clear old group associations
      /*@lineinfo:generated-code*//*@lineinfo:842^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_to_group
//          where       user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from user_to_group\n        where       user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:846^7*/
        
      // insert new group associations
      Iterator gIter = groups.getGroupIterator();
      Group group = null;
      
      while(gIter.hasNext())
      {
        group = (Group)gIter.next();
        
        if (!typeRec.getGroupSet().contains(group))
        {
          /*@lineinfo:generated-code*//*@lineinfo:858^11*/

//  ************************************************************
//  #sql [Ctx] { insert into user_to_group
//              (
//                user_id,
//                group_id
//              )
//              values
//              (
//                :userId,
//                :group.getGroupId()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_109 = group.getGroupId();
   String theSqlTS = "insert into user_to_group\n            (\n              user_id,\n              group_id\n            )\n            values\n            (\n               :1 ,\n               :2 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setLong(2,__sJT_109);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:870^11*/
        }
      }
      
      // submit sales rep record if applicable
      if ((groups.contains(MesUsers.GROUP_MES) &&
           groups.contains(MesUsers.GROUP_SALES)) ||
          groups.contains(MesUsers.GROUP_DISCOVER))
      {
        if (groups.contains(MesUsers.GROUP_DISCOVER))
        {
          repRec.setRepId(Long.toString(userId));
        }
        repRec.submitData();
      }

      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
      addError(e.toString());
    }
    
    return submitOk;
  }
  
  /*
  ** METHOD protected boolean delete()
  **
  ** Deletes a user record and associated records.
  **
  ** RETURNS: true if successful.
  */
  protected boolean delete()
  {
    // disable deletion of users for the time being
    return true;
  }
  
  /*
  ** METHOD public boolean enableUser(boolean enable)
  **
  ** Enables or disables a user record
  **
  ** RETURNS: true if successful
  */
  public boolean enableUser(boolean enable)
  {
    boolean   result        = false;
    String    enableString  = enable ? "Y" : "N";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:926^7*/

//  ************************************************************
//  #sql [Ctx] { update  users
//          set     enabled = :enableString
//          where   user_id = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n        set     enabled =  :1 \n        where   user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.user.UserRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,enableString);
   __sJT_st.setLong(2,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:931^7*/
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("enableUser()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** METHOD publie boolean inGroup(Group group)
  **
  ** RETURNS: true if group is in this user's group set.
  */
  public boolean inGroup(Group group)
  {
    return groups.contains(group);
  }
  public boolean inGroup(long groupId)
  {
    return groups.contains(groupId);
  }
  
  /*
  ** METHOD public boolean hasRight(Right right)
  **
  ** RETURNS: true if user has given right.
  */
  public boolean hasRight(Right right)
  {
    return rights.contains(right);
  }
  public boolean hasRight(long rightId)
  {
    return rights.contains(rightId);
  }
  
  /*
  ** ACCESSORS
  */
  public long getUserId()
  {
    return userId;
  }
  public void setUserId(long newUserId)
  {
    userId = newUserId;
    repRec.setUserId(userId);
  }
  public void setUserId(String newUserId)
  {
    setUserId(parseLong(newUserId));
  }
  
  public boolean isEnabled()
  {
    boolean result = false;
    
    if(enabled != null && enabled.equals("Y"))
    {
      result = true;
    }
    
    return result;
  }
  
  public String getLoginName()
  {
    return loginName;
  }
  public void setLoginName(String newLoginName)
  {
    loginName = newLoginName;
  }
  
  public String getPassword()
  {
    return password;
  }
  public void setPassword(String newPassword)
  {
    password = newPassword;
  }
  
  public String getConfirmPassword()
  {
    return confirmPassword;
  }
  public void setConfirmPassword(String newConfirmPassword)
  {
    confirmPassword = newConfirmPassword;
  }
  
  public String getUserName()
  {
    return userName;
  }
  public void setUserName(String newUserName)
  {
    userName = newUserName;
  }
  
  public String getAddress1()
  {
    return address1;
  }
  public void setAddress1(String newAddress1)
  {
    address1 = newAddress1;
  }
  
  public String getAddress2()
  {
    return address2;
  }
  public void setAddress2(String newAddress2)
  {
    address2 = newAddress2;
  }
  
  public String getCity()
  {
    return city;
  }
  public void setCity(String newCity)
  {
    city = newCity;
  }
  
  public String getState()
  {
    return state;
  }
  public void setState(String newState)
  {
    state = newState;
  }
  
  public String getZip()
  {
    return zip;
  }
  public void setZip(String newZip)
  {
    zip = newZip;
  }
  
  public String getEmail()
  {
    return email;
  }
  public void setEmail(String newEmail)
  {
    email = newEmail;
  }
  
  public String getPhone()
  {
    String retStr = "";
    if (phone != 0L)
    {
      retStr = Long.toString(phone);
    }
    return retStr;
  }
  public void setPhone(long newPhone)
  {
    phone = newPhone;
  }
  public void setPhone(String newPhone)
  {
    setPhone(parseLong(newPhone));
  }
  
  public String getFax()
  {
    String retStr = "";
    if (fax != 0L)
    {
      retStr = Long.toString(fax);
    }
    return retStr;
  }
  public void setFax(long newFax)
  {
    fax = newFax;
  }
  public void setFax(String newFax)
  {
    setFax(parseLong(newFax));
  }
  
  public String getMobile()
  {
    String retStr = "";
    if (mobile != 0L)
    {
      retStr = Long.toString(mobile);
    }
    return retStr;
  }
  public void setMobile(long newMobile)
  {
    mobile = newMobile;
  }
  public void setMobile(String newMobile)
  {
    setMobile(parseLong(newMobile));
  }
  
  public String getPager()
  {
    String retStr = "";
    if (pager != 0L)
    {
      retStr = Long.toString(pager);
    }
    return retStr;
  }
  public void setPager(long newPager)
  {
    pager = newPager;
  }
  public void setPager(String newPager)
  {
    setPager(parseLong(newPager));
  }
  
  public String getPagerPin()
  {
    String retStr = "";
    if (pagerPin != 0L)
    {
      retStr = Long.toString(pagerPin);
    }
    return retStr;
  }
  public void setPagerPin(long newPagerPin)
  {
    pagerPin = newPagerPin;
  }
  public void setPagerPin(String newPagerPin)
  {
    setPagerPin(parseLong(newPagerPin));
  }
  
  public String getBankGroup1()
  {
    String retStr = "";
    if (bGroup1 != 0)
    {
      retStr = Integer.toString(bGroup1);
    }
    return retStr;
  }
  public void setBankGroup1(int newBankGroup1)
  {
    bGroup1 = newBankGroup1;
  }
  public void setBankGroup1(String newBankGroup1)
  {
    setBankGroup1(parseInt(newBankGroup1));
  }
  
  public String getBankGroup2()
  {
    String retStr = "";
    if (bGroup2 != 0)
    {
      retStr = Integer.toString(bGroup2);
    }
    return retStr;
  }
  public void setBankGroup2(String newBankGroupStr)
  {
    bGroup2 = parseInt(newBankGroupStr);
  }
  
  public String getBankGroup3()
  {
    String retStr = "";
    if (bGroup3 != 0)
    {
      retStr = Integer.toString(bGroup3);
    }
    return retStr;
  }
  public void setBankGroup3(String newBankGroupStr)
  {
    bGroup3 = parseInt(newBankGroupStr);
  }
  
  public String getBankGroup4()
  {
    String retStr = "";
    if (bGroup4 != 0)
    {
      retStr = Integer.toString(bGroup4);
    }
    return retStr;
  }
  public void setBankGroup4(String newBankGroupStr)
  {
    bGroup4 = parseInt(newBankGroupStr);
  }
  
  public String getBankGroup5()
  {
    String retStr = "";
    if (bGroup5 != 0)
    {
      retStr = Integer.toString(bGroup5);
    }
    return retStr;
  }
  public void setBankGroup5(String newBankGroupStr)
  {
    bGroup5 = parseInt(newBankGroupStr);
  }
  
  public String getBankGroup6()
  {
    String retStr = "";
    if (bGroup6 != 0)
    {
      retStr = Integer.toString(bGroup6);
    }
    return retStr;
  }
  public void setBankGroup6(String newBankGroupStr)
  {
    bGroup6 = parseInt(newBankGroupStr);
  }
  
  public String getAssoc()
  {
    String retStr = "";
    if (assoc != 0)
    {
      retStr = Integer.toString(assoc);
    }
    return retStr;
  }
  public void setAssoc(String newAssoc)
  {
    assoc = parseInt(newAssoc);
  }
  
  public String getBankNum()
  {
    String retStr = "";
    if (bankNum != 0)
    {
      retStr = Integer.toString(bankNum);
    }
    return retStr;
  }
  public void setBankNum(String newBankNum)
  {
    bankNum = parseInt(newBankNum);
  }
  
  public String getThirdPartyId()
  {
    return thirdPartyId;
  }
  public void setThirdPartyId(String newId)
  {
    thirdPartyId = newId;
  }
  
  public String getComments()
  {
    return comments;
  }
  public void setComments(String newComments)
  {
    comments = newComments;
  }
  
  public UserType getUserType()
  {
    return typeRec.getUserType();
  }

  public long getTypeId()
  {
    return typeRec.getUserTypeId();
  }
  public void setTypeId(long newTypeId)
  {
    typeRec.getData(newTypeId);
    groups.add(typeRec.getGroupSet());
  }
  public void setTypeId(String newTypeId)
  {
    setTypeId(parseLong(newTypeId));
  }
  
  public long getAId()
  {
    return affiliateId;
  }
  public String getAffiliateId()
  {
    String retStr = "";
    if (affiliateId != 0L)
    {
      retStr = Long.toString(affiliateId);
    }
    return retStr;
  }
  public void setAffiliateId(long newAffiliateId)
  {
    affiliateId = newAffiliateId;
  }
  public void setAffiliateId(String newAffiliateId)
  {
    setAffiliateId(parseLong(newAffiliateId));
  }
  
  public long getHNode()
  {
    return hierarchyNode;
  }
  public String getHierarchyNode()
  {
    String retStr = "";
    if (hierarchyNode != 0L)
    {
      retStr = Long.toString(hierarchyNode);
    }
    return retStr;
  }
  public void setHierarchyNode(long newHierarchyNode)
  {
    hierarchyNode = newHierarchyNode;
  }
  public void setHierarchyNode(String newHierarchyNode)
  {
    setHierarchyNode(parseLong(newHierarchyNode));
  }
  
  public String getCreateDate()
  {
    return createDate;
  }
  public void setCreateDate(String newCreateDate)
  {
    createDate = newCreateDate;
  }
  
  public String getAccessUser()
  {
    return accessUser;
  }
  public void setAccessUser(String accessUser)
  {
    this.accessUser = accessUser;
  }
  
  public String getCreateTime()
  {
    return createTime;
  }
  public void setCreateTime(String newCreateTime)
  {
    createTime = newCreateTime;
  }
  
  public String getPswdChangeDate()
  {
    return pswdChangeDate;
  }
  public void setPswdChangeDate(String newPswdChangeDate)
  {
    pswdChangeDate = newPswdChangeDate;
  }
  
  public String getPswdChangeTime()
  {
    return pswdChangeTime;
  }
  public void setPswdChangeTime(String newPswdChangeTime)
  {
    pswdChangeTime = newPswdChangeTime;
  }
  
  public String getLastLoginDate()
  {
    return lastLoginDate;
  }
  public void setLastLoginDate(String newLastLoginDate)
  {
    lastLoginDate = newLastLoginDate;
  }
  
  public String getLastLoginTime()
  {
    return lastLoginTime;
  }
  public void setLastLoginTime(String newLastLoginTime)
  {
    lastLoginTime = newLastLoginTime;
  }
  
  public String getRepId()
  {
    return repRec.getRepId();
  }
  public void setRepId(String newRepId)
  {
    repRec.setRepId(newRepId);
  }
  
  public String getRepMerchantId()
  {
    long    temp  = repRec.getRepMerchantId();
   
    // return the merchant number as a String.  if
    // the value is 0, then return blank 
    return( ((temp == 0L) ? "" : Long.toString(temp)) );
  }
  public void setRepMerchantId(String newRepMerchantId)
  {
    repRec.setRepMerchantId(newRepMerchantId);
  }
  
  public int getReprType()
  {
    return repRec.getReprType();
  }
  public void setReprType(int newRepType)
  {
    repRec.setReprType(newRepType);
  }
  public void setReprType(String newRepType)
  {
    setReprType(parseInt(newRepType));
  }
  
  public int getRepRegion()
  {
    return repRec.getRepRegion();
  }
  public void setRepRegion(int newRepRegion)
  {
    repRec.setRepRegion(newRepRegion);
  }
  public void setRepRegion(String newRepRegion)
  {
    setRepRegion(parseInt(newRepRegion));
  }
  
  public int getRepPlan()
  {
    return repRec.getRepPlan();
  }
  public void setRepPlan(int newRepPlan)
  {
    repRec.setRepPlan(newRepPlan);
  }
  public void setRepPlan(String newRepPlan)
  {
    setRepPlan(parseInt(newRepPlan));
  }
  
  public String getRepSSN()
  {
    return repRec.getRepSSN();
  }
  public void setRepSSN(long newRepSSN)
  {
    repRec.setRepSSN(newRepSSN);
  }
  public void setRepSSN(String newRepSSN)
  {
    setRepSSN(parseLong(newRepSSN));
  }
  
  public String getRepReportTo()
  {
    return repRec.getRepReportTo();
  }
  public void setRepReportTo(String newRepReportTo)
  {
    repRec.setRepReportTo(newRepReportTo);
  }
    
  public String getRepTitle()
  {
    return repRec.getRepTitle();
  }
  public void setRepTitle(String newRepTitle)
  {
    repRec.setRepTitle(newRepTitle);
  }
  
  public String getRepHireDate()
  {
    return repRec.getRepHireDate();
  }
  public void setRepHireDate(String newDateString)
  {
    repRec.setRepHireDate(newDateString);
  }
  
  public String getRepTerminateDate()
  {
    return(repRec.getRepTerminateDate());
  }
  public void setRepTerminateDate(String newDateString)
  {
    repRec.setRepTerminateDate(newDateString);
  }
  
  public GroupSet getGroupSet()
  {
    return groups;
  }
  public void setGroupSet(GroupSet newSet)
  {
    groups = newSet;
  }
  public void setGroupCheck(String[] groupIds)
  {
    for (int i = 0; i < java.lang.reflect.Array.getLength(groupIds); ++i)
    {
      groups.add(new Group(Long.parseLong(groupIds[i])));
    }
  }

  public RightSet getRightSet()
  {
    return rights;
  }
  public void setRightSet(RightSet newSet)
  {
    rights = newSet;
  }
  public void setRights(String[] rightIds)
  {
    for (int i = 0; i < java.lang.reflect.Array.getLength(rightIds); ++i)
    {
      rights.add(new Right(Long.parseLong(rightIds[i])));
    }
  }
  
  public void setReportHierarchyNode(String newReportNode)
  {
    try
    {
      setReportHierarchyNode(Long.parseLong(newReportNode));
    }
    catch(Exception e)
    {
      logEntry("setHierarchyNode(" + newReportNode + ")", e.toString());
    }
  }
  public void setReportHierarchyNode(long newReportNode)
  {
    reportHierarchyNode = newReportNode;
  }
  public long getReportHierarchyNode()
  {
    return reportHierarchyNode;
  }

  public boolean getIsNew()
  {
    return isNew;
  }
  public void setIsNew(boolean newVal)
  {
    isNew = newVal;
  }
  
  public String getPasswordEnc()
  {
    return this.passwordEnc;
  }
  public void setPasswordEnc(String password)
  {
    // encrypt password
    md5.Update(password);
    this.passwordEnc = md5.asHex();
  }
}/*@lineinfo:generated-code*/