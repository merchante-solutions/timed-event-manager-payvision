/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ach/AchDb.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2012-11-28 11:41:59 -0800 (Wed, 28 Nov 2012) $
  Version            : $Revision: 20730 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.function.Predicate;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.mes.data.payouts.PayoutsFundingProcess;
import com.mes.data.payouts.PayoutsFundingProcessService;
import com.mes.database.SQLJConnectionBase;

public class AchDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(AchDb.class);

  // ach statements insert
  static String AchStatementEntryInsertCommonQs = 
    " (                                 " +
    "   bank_number,                    " +
    "   merchant_number,                " +
    "   post_date,                      " +
    "   batch_date,                     " +
    "   ach_amount,                     " +
    "   transaction_code,               " +
    "   entry_description,              " +
    "   sales_count,                    " +
    "   sales_amount,                   " +
    "   credits_count,                  " +
    "   credits_amount,                 " +
    "   daily_discount_amount,          " +
    "   daily_ic_amount,                " +
    "   sponsored_amount,               " +
    "   non_sponsored_amount,           " +
    "   load_filename,                  " +
    "   rec_id,                         " +
    "   batch_number,                   " +
    "   ach_sequence                   " +
    " )                                 " +
    " values                            " +
    " ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )   ";
  
//ach ext statements insert 
  static String AchExtStatementEntryInsertCommonQs = 
    " (                                 " +
    "   post_date,                      " +
    "   ach_amount,                     " +
    "   transaction_code,               " +
    "   entry_description,              " +
    "   batch_date,                     " +
    "   batch_id,                       " +
    "   batch_number,                   " +
    "   load_filename,                  " +
    "   load_file_id,                   " +
    "   sponsored_amount,               " +
    "   non_sponsored_amount,           " +
    "   vs_amount,           			" +
    "   mc_amount,                  	" +
    "   am_amount,                      " +
    "   ds_amount,                    	" +
    "   pl_amount,                    	" +
    "   db_amount,                    	" +
    "   bank_number,                    " +
    "   merchant_number                	" +
    " )                                 " +
    " values                            " +
    " ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )   ";
    
  public static boolean storeAchStatementRecord( AchStatementRecord rec )
  {
    return( storeAchStatementRecord(rec,"ach_trident_statement") );
  }

  public static boolean storeAchStatementRecord( AchStatementRecord rec, String tableName )
  {
    return( (new AchDb())._storeAchStatementRecord(rec,tableName) );
  }
  
  public static boolean storeAchStatementVector( Vector stmts, String tableName, int commitCount )
  {
    return( (new AchDb())._storeAchStatementVector(stmts,tableName,commitCount) );
  }
  
  public static boolean storeAchStatementVector( Vector stmts, String tableName)
  {
    return( (new AchDb())._storeAchStatementVector(stmts,tableName,1000) );
  }
  
  public static boolean storeAchExtStatementVector( Vector stmts, String tableName)
  {
    return( (new AchDb())._storeAchExtStatementVector(stmts,tableName,1000) );
  }
  
  public boolean _storeAchStatementRecord( AchStatementRecord rec, String tableName )
  {
    PreparedStatement ps          = null;
    String            qs          = null;
    boolean           retVal      = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      qs = "insert into " + tableName + AchStatementEntryInsertCommonQs;
      ps = con.prepareStatement(qs);
      
      setParams(ps, rec);
      
      ps.executeUpdate();
      ps.close();
      con.commit();
      retVal = true;
    }
    catch( Exception e )
    {
      try{ con.rollback(); } catch( Exception ee ) {}
      logEntry("_storeAchStatementEntry(" + rec.getMerchantId() + ", " + rec.getPostDate() + ")",e.toString());
      retVal = false;
    }
    finally
    {
      setAutoCommit(true);
      try { ps.close(); } catch(Exception e) {}
      cleanUp(); 
    }
    return( retVal );
  }
  
  public boolean _storeAchStatementVector( Vector stmts, String tableName, int commitCount )
  {
    PreparedStatement ps      = null;
    String            qs      = null;
    boolean           retVal  = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      qs = "insert into " + tableName + AchStatementEntryInsertCommonQs;
      ps = con.prepareStatement(qs);
      
      AchStatementRecord rec = null;
      
      // insert one row for each record
      for( int i=0; i<stmts.size(); ++i)
      {
        rec = (AchStatementRecord)stmts.elementAt(i);
        
        setParams(ps, rec);
        
        ps.executeUpdate();
        
        if( i>0 && i%commitCount == 0)
        {
          con.commit();
        }
      }
      
      ps.close();
      con.commit();
      retVal = true;
    }
    catch(Exception e)
    {
      try{ con.rollback(); } catch( Exception ee ) {}
      logEntry("_storeAchStatementVector()",e.toString());
      retVal = false;
    }
    finally
    {
      setAutoCommit(true);
      try { ps.close(); } catch(Exception e) {}
      cleanUp(); 
    }
    return( retVal );
  }
  
    public static void createPayoutsFundingProcessEntries(Vector stmts) {
        
        AchStatementRecord rec = null;
        List<PayoutsFundingProcess> payoutsFundingProcessRecs = new ArrayList<>();
        PayoutsFundingProcessService payoutsFundingProcessService = new PayoutsFundingProcessService();
        try {
            for (int i = 0; i < stmts.size(); ++i) {
                rec = (AchStatementRecord) stmts.elementAt(i);
                
                if(BooleanUtils.toBoolean(rec.getSameDayInd(), "Y", "N")
                        || BooleanUtils.toBoolean(rec.getPreFundInd(), "Y", "N")) {
                    PayoutsFundingProcess payoutsFundingProcess = null;
                    
                    if (BooleanUtils.toBoolean(rec.getSameDayInd(), "Y", "N")
                            && !BooleanUtils.toBoolean(rec.getPreFundInd(), "Y", "N")) {
                         payoutsFundingProcess = new PayoutsFundingProcess(rec.getMerchantId(),rec.getPostDate(), 0L, 2, "DDF", new DateTime(), rec.getRecId());
                    } else if (BooleanUtils.toBoolean(rec.getPreFundInd(), "Y", "N")
                            && !BooleanUtils.toBoolean(rec.getSameDayInd(), "Y", "N")) {
                         payoutsFundingProcess = new PayoutsFundingProcess(rec.getMerchantId(),rec.getPostDate(), 0L, 1, "DDF", new DateTime(), rec.getRecId());
                    } else if (BooleanUtils.toBoolean(rec.getPreFundInd(), "Y", "N")
                            && BooleanUtils.toBoolean(rec.getSameDayInd(), "Y", "N")) {
                         payoutsFundingProcess = new PayoutsFundingProcess(rec.getMerchantId(),rec.getPostDate(), 0L, 3, "DDF", new DateTime(), rec.getRecId());
                    }
                    
                    if(payoutsFundingProcess != null) {
                        payoutsFundingProcessRecs.add(payoutsFundingProcess);
                    }
                }
            }
            
            if(!payoutsFundingProcessRecs.isEmpty()) {
                payoutsFundingProcessService.insert(payoutsFundingProcessRecs);
            }
            
        } catch (Exception e) {
            log.error("Exception while trying to create a payouts process record entry", e);
        }
    }

  public boolean _storeAchExtStatementVector( Vector stmts, String tableName, int commitCount )
  {
    PreparedStatement ps      = null;
    String            qs      = null;
    boolean           retVal  = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      qs = "insert into " + tableName + AchExtStatementEntryInsertCommonQs;
      ps = con.prepareStatement(qs);
      
      AchStatementRecord rec = null;
      
      // insert one row for each record
      for( int i=0; i<stmts.size(); ++i)
      {
        rec = (AchStatementRecord)stmts.elementAt(i);
        
        setExtParams(ps, rec);
        
        ps.executeUpdate();
        
        if( i>0 && i%commitCount == 0)
        {
          con.commit();
        }
      }
      
      ps.close();
      con.commit();
      
      retVal = true;
    }
    catch(Exception e)
    {
      try{ con.rollback(); } catch( Exception ee ) {}
      logEntry("_storeAchExtStatementVector()",e.toString());
      retVal = false;
    }
    finally
    {
      setAutoCommit(true);
      try { ps.close(); } catch(Exception e) {}
      cleanUp(); 
    }
    return( retVal );
  }
  
  private void setParams(PreparedStatement ps, AchStatementRecord rec)
    throws Exception
  {
    int idx = 0;
    Predicate<AchStatementRecord> isDepositRecord = s -> AchEntryData.ED_MERCH_DEP.equals(s.getEntryDesc()) 
            || AchEntryData.ED_AMEX_DEP.equals(s.getEntryDesc());
    Predicate<AchStatementRecord> isEnhancedFundingMid = s -> BooleanUtils.toBoolean(s.getSameDayInd(), "Y", "N") 
            || BooleanUtils.toBoolean(s.getPreFundInd(), "Y", "N");
    ps.setInt   (++idx,rec.getBankNumber());
    ps.setLong  (++idx,rec.getMerchantId());
    ps.setDate  (++idx,rec.getPostDate());
    ps.setDate  (++idx,rec.getBatchDate());
    ps.setDouble(++idx,rec.getAchAmount());
    ps.setInt   (++idx,rec.getTransactionCode());
    ps.setString(++idx,rec.getEntryDesc());
    ps.setInt   (++idx,rec.getSalesCount());
    ps.setDouble(++idx,rec.getSalesAmount());
    ps.setInt   (++idx,rec.getCreditsCount());
    ps.setDouble(++idx,rec.getCreditsAmount());
    ps.setDouble(++idx,rec.getDailyDiscountAmount());
    ps.setDouble(++idx,rec.getDailyICAmount());
    ps.setDouble(++idx,rec.getSponsoredAmount());
    ps.setDouble(++idx,rec.getNonSponsoredAmount());
    ps.setString(++idx,rec.getLoadFilename());
    ps.setLong  (++idx,rec.getRecId());
    ps.setLong  (++idx, rec.getBatchNumber());
    if (isDepositRecord.test(rec) && isEnhancedFundingMid.test(rec)) {
        ps.setLong(++idx, -1L);
   } else {
       ps.setLong(++idx, 0L);
   }
  }
  
  private void setExtParams(PreparedStatement ps, AchStatementRecord rec)
	throws Exception
  {
    int idx = 0;
    ps.setDate   (++idx,rec.getPostDate());
    ps.setDouble (++idx,rec.getAchAmount());
    ps.setInt    (++idx,rec.getTransactionCode());
    ps.setString (++idx,rec.getEntryDesc());
    ps.setDate   (++idx,rec.getBatchDate());
    ps.setLong   (++idx,rec.getRecId());
    ps.setLong   (++idx,rec.getBatchNumber());
    ps.setString (++idx,rec.getLoadFilename());
    ps.setLong   (++idx,rec.getLoadFileId());
    ps.setDouble (++idx,rec.getSponsoredAmount());
    ps.setDouble (++idx,rec.getNonSponsoredAmount());
    ps.setDouble (++idx,rec.getVsAmount());
    ps.setDouble (++idx,rec.getMcAmount());
    ps.setDouble (++idx,rec.getAmAmount());
    ps.setDouble (++idx,rec.getDsAmount());
    ps.setDouble (++idx,rec.getPlAmount());
    ps.setDouble (++idx,rec.getDbAmount());
    ps.setInt   (++idx,rec.getBankNumber());
    ps.setLong  (++idx,rec.getMerchantId());  
  }
}
