/*@lineinfo:filename=RiskScoreDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskScoreDataBean.sqlj $

  Description:


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-02-17 09:20:44 -0800 (Tue, 17 Feb 2015) $
  Version            : $Revision: 23325 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class RiskScoreDataBean extends ReportSQLJBean
{
  public static final int         TT_AUTH_CAPTURE_RATIO     = 1;
  public static final int         TT_DECL_AUTH_RATIO        = 2;
  public static final int         TT_DECL_REASON_CODES      = 3;
  public static final int         TT_SMALL_TICKET_AUTH      = 4;
  public static final int         TT_AVG_TICKET             = 5;
  public static final int         TT_DUPLICATE_CARD         = 6;
  public static final int         TT_UNMATCHED_CREDITS      = 7;
  public static final int         TT_CHARGEBACK_VOLUME      = 8;
  public static final int         TT_HIGH_RISK_CHARGEBACK   = 9;
  public static final int         TT_VOLUME_EXCEPTIONS      = 10;

  public static final int         TT_SEQUENTIAL_AUTHS       = 21;
  public static final int         TT_ZERO_DOLLAR_BATCH      = 22;
  public static final int         TT_LARGE_TICKET_CREDITS   = 23;
  public static final int         TT_CREDIT_VOLUME          = 24;
  public static final int         TT_LARGE_TICKET_SALES     = 25;
  public static final int         TT_KEYED_VOLUME           = 26;
  public static final int         TT_RETRIEVAL_VOLUME       = 27;
  public static final int         TT_CREDIT_SALES_RATIO     = 28;
  public static final int         TT_SUSPICIOUS_ACCOUNT     = 29;
  public static final int         TT_FRAUD_CARD_NUMBER      = 30;
  public static final int         TT_FOREIGN_CARD_NUMBER    = 31;
  public static final int         TT_TE_LARGE_TICKET_SALES  = 32;
  public static final int         TT_LARGE_TICKET_CHARGEBACK= 33;
  public static final int         TT_AUTH_AVS_RESULT        = 34;
  public static final int         TT_ACH_REJECT             = 35;
  public static final int         TT_AVG_DAILY_SALES_COUNT  = 36;
  
  
  //new risk categories PRF-3156
  public static final int         TT_ACHP_RETURNS  = 37;
  public static final int         TT_ACHP_MONTHLY_PROCESSING_AMT  = 38;
  public static final int         TT_ACHP_DUPLICATE_DDA = 39;
  public static final int         TT_ACHP_DUPLICATE_CUSTOMER_NAME = 40;
  public static final int         TT_DAILY_SALES_COUNT = 41;
  public static final int         TT_INDIVIDUAL_SALES_AMT = 42;   
  public static final int         TT_INDIVIDUAL_CREDIT_AMT = 43;
  public static final int         TT_DAILY_SALES_VOLUME = 44;
  public static final int         TT_DAILY_CREDIT_COUNT = 45;
  public static final int         TT_AVERAGE_TICKET_AMOUNT = 46;
  public static final int         TT_DAILY_CREDIT_VOLUME = 47;


  public static final int         QS_NEW                    = 0;
  public static final int         QS_PENDING                = 1;
  public static final int         QS_AUTO_CLEARED           = 2;
  public static final int         QS_CLEARED                = 3;
  public static final int         QS_APPROVED               = 4;

  public static final int         POINT_TOT_AUTOMATIC       = 999999;
  public static final int         RISK_THRESHOLD_DEFAULT    = 200;

  public static final long        RISK_ANALYST_UNASSIGNED   = -1L;
  public static final long        RISK_ANALYST_ALL          = 0L;

  public static final int         HISTORIC_DAY_COUNT        = 3;
  public static final int         HISTORIC_ROW_COUNT        = HISTORIC_DAY_COUNT + 1;
  public static final int         HIGH_SCORE_COUNT          = 5;
  public static final int         HIGH_SCORE_ROW_COUNT      = HISTORIC_ROW_COUNT + 2;

  public static final int         NT_UNKNOWN                = -1;

  public static final int         SHORT_NOTE_COUNT          = 2;

  public static final int         VOLUME_MONTH_COUNT        = 3;

  public static final int         SB_DEFAULT                = 0;
  public static final int         SB_CAT_DESC               = 1;
  public static final int         SB_HIT_COUNT              = 2;
  public static final int         SB_HIT_POINTS             = 3;

  public static final int         RT_RISK_ASSIGNMENTS       = (RT_USER + 0);  // 2
  public static final int         RT_CAT_STATS              = (RT_USER + 1);  // 3

  protected static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };

  protected long transactionCount = 0L;
  protected int  merchLevel       = 0;
  protected String pciName        = "";
  protected String pciEmail       = "";
  protected String pciPhone       = "";
  protected List pciHistoryList   = new LinkedList();

  public class CategoryStatistics implements Comparable
  {
    public String     CategoryDesc        = null;
    public long       HitCount            = 0L;
    public long       TotalPoints         = 0L;

    public CategoryStatistics( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CategoryDesc  = resultSet.getString("risk_category");
      HitCount      = resultSet.getLong("hit_count");
      TotalPoints   = resultSet.getLong("hit_points");
    }

    public int compareTo( Object obj )
    {
      CategoryStatistics  compareObj  = (CategoryStatistics)obj;
      int                 retVal      = 0;

      switch( Math.abs(SortOrder) )
      {
        case SB_HIT_COUNT:
          if ( (retVal = (int)(HitCount - compareObj.HitCount)) == 0 )
          {
            if ( (retVal = (int)(TotalPoints - compareObj.TotalPoints)) == 0 )
            {
              retVal = CategoryDesc.compareTo( compareObj.CategoryDesc );
            }
          }
          break;

        case SB_HIT_POINTS:
          if ( (retVal = (int)(TotalPoints - compareObj.TotalPoints)) == 0 )
          {
            if ( (retVal = (int)(HitCount - compareObj.HitCount)) == 0 )
            {
              retVal = CategoryDesc.compareTo( compareObj.CategoryDesc );
            }
          }
          break;

        //case SB_CAT_DESC:
        default:
          retVal = CategoryDesc.compareTo( compareObj.CategoryDesc );
          break;
      }

      if ( SortOrder < 0 )
      {
        retVal *= -1;
      }
      return( retVal );
    }
  }

  public class HighScoreEntry
  {
    public Date       ActivityDate        = null;
    public int        Points              = 0;

    public HighScoreEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActivityDate  = resultSet.getDate("activity_date");
      Points        = resultSet.getInt("points");
    }
  }

  public static class PCIOtherValidation implements Validation
  {
    private String  ErrorMessage  = null;
    private Field   VendorCode    = null;

    public PCIOtherValidation( Field vendorCode )
    {
      VendorCode = vendorCode;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      String      vendorCode    = VendorCode.getData();

      ErrorMessage = null;
      if ( vendorCode.equals("OTHER") &&
           (fdata == null || fdata.equals("")) )
      {
        ErrorMessage = "Must specify other PCI vendor name";
      }
      return( ErrorMessage == null );
    }
  }

  public static class PCILevelTable extends DropDownTable
  {
    public PCILevelTable( long merchantId )
    {
      ResultSetIterator       it = null;
      ResultSet               rs = null;

      try
      {
        connect();

        addElement("",  "select");

        /*@lineinfo:generated-code*//*@lineinfo:265^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  level_id,
//                    level_desc
//            from    t_hierarchy           th,
//                    risk_pci_levels       pcl,
//                    mif                   mf
//            where   th.hier_type = 1 and
//                    th.ancestor = pcl.hierarchy_node and
//                    th.entity_type = 4 and
//                    th.relation =
//                    (
//                      select min(to_number(thi.relation))
//                      from   t_hierarchy thi,
//                             mif         mfi
//                      where  thi.hier_type = 1 and
//                             thi.ancestor in
//                             (
//                               select pcli.hierarchy_node
//                               from   risk_pci_levels pcli
//                             ) and
//                             thi.entity_type = 4 and
//                             mfi.association_node = thi.descendent and
//                             mfi.merchant_number = :merchantId
//                    ) and
//                    mf.association_node = th.descendent and
//                    mf.merchant_number = :merchantId
//            order by pcl.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  level_id,\n                  level_desc\n          from    t_hierarchy           th,\n                  risk_pci_levels       pcl,\n                  mif                   mf\n          where   th.hier_type = 1 and\n                  th.ancestor = pcl.hierarchy_node and\n                  th.entity_type = 4 and\n                  th.relation =\n                  (\n                    select min(to_number(thi.relation))\n                    from   t_hierarchy thi,\n                           mif         mfi\n                    where  thi.hier_type = 1 and\n                           thi.ancestor in\n                           (\n                             select pcli.hierarchy_node\n                             from   risk_pci_levels pcli\n                           ) and\n                           thi.entity_type = 4 and\n                           mfi.association_node = thi.descendent and\n                           mfi.merchant_number =  :1 \n                  ) and\n                  mf.association_node = th.descendent and\n                  mf.merchant_number =  :2 \n          order by pcl.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }

        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  public static class YesNoTable extends DropDownTable
  {
    public YesNoTable( )
    {
      addElement("",  "select");
      addElement("Y", "Yes");
      addElement("N", "No");
      addElement("P", "In Progress");
      addElement("U", "Unresponsive");
    }
  }

  public static class PCIVendorTable extends DropDownTable
  {
    public PCIVendorTable( long merchantId )
    {
      ResultSetIterator     it          = null;
      ResultSet             resultSet   = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:339^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pv.vendor_code,
//                    pv.vendor_name
//            from    t_hierarchy           th,
//                    risk_pci_vendors      pv,
//                    mif                   mf
//            where   th.hier_type = 1 and
//                    th.ancestor = pv.hierarchy_node and
//                    th.entity_type = 4 and
//                    th.relation =
//                    (
//                      select min(to_number(thi.relation))
//                      from   t_hierarchy thi,
//                             mif         mfi
//                      where  thi.hier_type = 1 and
//                             thi.ancestor in
//                             (
//                               select pvi.hierarchy_node
//                               from   risk_pci_vendors pvi
//                             ) and
//                             thi.entity_type = 4 and
//                             mfi.association_node = thi.descendent and
//                             mfi.merchant_number = :merchantId
//                    ) and
//                    mf.association_node = th.descendent and
//                    mf.merchant_number = :merchantId
//            order by pv.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pv.vendor_code,\n                  pv.vendor_name\n          from    t_hierarchy           th,\n                  risk_pci_vendors      pv,\n                  mif                   mf\n          where   th.hier_type = 1 and\n                  th.ancestor = pv.hierarchy_node and\n                  th.entity_type = 4 and\n                  th.relation =\n                  (\n                    select min(to_number(thi.relation))\n                    from   t_hierarchy thi,\n                           mif         mfi\n                    where  thi.hier_type = 1 and\n                           thi.ancestor in\n                           (\n                             select pvi.hierarchy_node\n                             from   risk_pci_vendors pvi\n                           ) and\n                           thi.entity_type = 4 and\n                           mfi.association_node = thi.descendent and\n                           mfi.merchant_number =  :1 \n                  ) and\n                  mf.association_node = th.descendent and\n                  mf.merchant_number =  :2 \n          order by pv.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:367^9*/
        resultSet = it.getResultSet();

        addElement("","select");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("PCIVendorTable()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch(Exception e){}
        cleanUp();
      }
    }
  }

  public static class RiskAnalyst
  {
    public  String    LoginName           = null;
    public  long      UserId              = 0L;

    public RiskAnalyst( ResultSet resultSet )
      throws java.sql.SQLException
    {
      UserId      = resultSet.getLong("user_id");
      LoginName   = resultSet.getString("login_name");
    }
  }

  public static class RiskAnalystsTable extends DropDownTable
  {
    public RiskAnalystsTable( long nodeId, boolean allowAll )
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      ResultSet               resultSet       = null;

      try
      {
        connect();

        // if the user logged in at the top of the hierarchy tree,
        // then force them down to the default bank level.
        if ( nodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE )
        {
          //nodeId = 394100000L;
        }

        /*@lineinfo:generated-code*//*@lineinfo:422^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (ug XPKUSER_TO_GROUP) */
//                    distinct
//                    u.user_id                       as user_id,
//                    u.login_name                    as login_name
//            from    t_hierarchy         th,
//                    users               u,
//                    user_to_group       ug,
//                    user_group_to_right gr
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.descendent = u.hierarchy_node and
//                    --th.descendent = decode(u.hierarchy_node, 9999999999, 394100000, u.hierarchy_node) and
//                    (u.user_id = ug.user_id or u.type_id = ug.user_id) and
//                    u.enabled = 'Y' and
//                    ug.group_id = gr.group_id and
//                    gr.right_id = :MesUsers.RIGHT_REPORT_RISK_ANALYST   -- 356
//            order by login_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (ug XPKUSER_TO_GROUP) */\n                  distinct\n                  u.user_id                       as user_id,\n                  u.login_name                    as login_name\n          from    t_hierarchy         th,\n                  users               u,\n                  user_to_group       ug,\n                  user_group_to_right gr\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.descendent = u.hierarchy_node and\n                  --th.descendent = decode(u.hierarchy_node, 9999999999, 394100000, u.hierarchy_node) and\n                  (u.user_id = ug.user_id or u.type_id = ug.user_id) and\n                  u.enabled = 'Y' and\n                  ug.group_id = gr.group_id and\n                  gr.right_id =  :2    -- 356\n          order by login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,MesUsers.RIGHT_REPORT_RISK_ANALYST);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:441^9*/
        resultSet = it.getResultSet();

        if( allowAll == true )
        {
          addElement(Long.toString(RISK_ANALYST_ALL),"All Analysts");
        }
        addElement(Long.toString(RISK_ANALYST_UNASSIGNED),"Unassigned");

        while( resultSet.next() )
        {
          addElement( resultSet );
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  public class RiskAssignment
  {
    public long             HierarchyNode         = 0L;
    public int              LoadPercent           = 0;
    public String           LoginName             = null;
    public long             MerchantIdLower       = 0L;
    public long             MerchantIdUpper       = 0L;
    public String           Name                  = null;
    public String           OrgName               = null;
    public long             UserId                = 0L;
    public long             RecId                 = 0L;

    public RiskAssignment( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RecId             = resultSet.getLong("rec_id");
      UserId            = resultSet.getLong("user_id");
      LoginName         = resultSet.getString("login_name");
      Name              = resultSet.getString("name");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      LoadPercent       = resultSet.getInt("load_percent");
      MerchantIdLower   = resultSet.getLong("merchant_number_lower");
      MerchantIdUpper   = resultSet.getLong("merchant_number_upper");
      OrgName           = resultSet.getString("org_name");
    }
  };

  public class RiskScoreDetail
  {
    public  Date        ActivityDate  = null;
    public  String      ExceptionDesc = null;
    public  long        MerchantId    = 0L;
    public  String      MutedBy       = null;
    public  Date        MutedDate     = null;
    public  int         Points        = 0;
    public  String      RiskCatDesc   = null;
    public  int         RiskCatId     = -1;

    public RiskScoreDetail( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActivityDate  = resultSet.getDate("activity_date");
      ExceptionDesc = resultSet.getString("exception_desc");
      MerchantId    = resultSet.getLong("merchant_number");
      MutedBy       = resultSet.getString("muted_by");
      MutedDate     = resultSet.getDate("muted_date");
      Points        = resultSet.getInt("points");
      RiskCatId     = resultSet.getInt("risk_cat_id");
      RiskCatDesc   = resultSet.getString("risk_cat_desc");
    }

    public String getPointsAsString( )
    {
      StringBuffer      retVal  = new StringBuffer(Integer.toString(Points));

      if ( Points >= RISK_THRESHOLD_DEFAULT )
      {
        retVal.append("*");
      }
      return( retVal.toString() );
    }
  }

  public static class MerchantVolumeData
  {
    public double         AverageTicket   = 0.0;
    public double         SalesAmount     = 0.0;
    public int            SalesCount      = 0;
    public Date           VolumeMonth     = null;

    public MerchantVolumeData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AverageTicket = resultSet.getDouble("avg_ticket");
      SalesAmount   = resultSet.getDouble("sales_amount");
      SalesCount    = resultSet.getInt("sales_count");
      VolumeMonth   = resultSet.getDate("vol_month");
    }
  }

  public class NoteSummary
  {
    public String     DbaName           = null;
    public String     LoginName         = null;
    public long       MerchantId        = 0L;
    public Timestamp  NoteDate          = null;
    public String     Notes             = null;
    public String     NoteType          = null;
    public String     OtherDesc         = null;

    public NoteSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId    = resultSet.getLong("merchant_number");
      DbaName       = resultSet.getString("dba_name");
      NoteDate      = resultSet.getTimestamp("note_date");
      NoteType      = resultSet.getString("note_type");
      OtherDesc     = resultSet.getString("other_desc");
      LoginName     = resultSet.getString("login_name");
      Notes         = resultSet.getString("notes");
    }
  }

  public class NoteType
  {
    public String       Desc        = null;
    public int          Type        = NT_UNKNOWN;

    public NoteType( ResultSet resultSet )
      throws java.sql.SQLException
    {
      Desc  = resultSet.getString("note_desc");
      Type  = resultSet.getInt("note_type");
    }
  }

  protected class NewPendingActionsTable extends DropDownTable
  {
    public NewPendingActionsTable()
    {
      addElement(Integer.toString(QS_NEW),    "New");
      addElement(Integer.toString(QS_PENDING),"Pending");
      addElement(Integer.toString(QS_CLEARED),"Clear");
    }
  }
  protected class ClearedActionsTable extends DropDownTable
  {
    public ClearedActionsTable()
    {
      addElement(Integer.toString(QS_CLEARED),  "Cleared");
      addElement(Integer.toString(QS_APPROVED), "Approve");
      addElement(Integer.toString(QS_PENDING),  "Pend");
    }
  }

  protected class ApprovedActionsTable extends DropDownTable
  {
    public ApprovedActionsTable()
    {
      addElement(Integer.toString(QS_APPROVED), "Approved");
      addElement(Integer.toString(QS_PENDING),  "Pend");
    }
  }

  public class RowData implements Comparable
  {
    public  int                   ActiveDays          = 0;
    public  Date                  ActivationDate      = null;
    public  Date                  ActivityDate        = null;
    public  long                  AppSeqNum           = 0L;
    public  String                AssignedTo          = null;
    public  long                  AssignedUserId      = -1L;
    public  int                   BankNumber          = 0;
    public  String                DbaName             = null;
    public  long                  MerchantId          = 0L;
    public  int                   MutedPoints         = 0;
    public  String                PortfolioName       = null;
    public  long                  PortfolioNode       = 0L;
    public  Date                  RiskActivationDate  = null;
    public  int                   SicCode             = 0;
    public  String                SicDesc             = null;
    public  int                   StatusCode          = QS_NEW;
    public  DropDownField         StatusField         = null;
    public  String                Status              = null;
    public  int                   TotalPoints         = 0;

    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      DropDownTable     dropDownItems = null;
      StringBuffer      fname         = new StringBuffer("queueOp_");

      // extract the merchant data
      ActivationDate      = resultSet.getDate("activation_date");
      ActiveDays          = resultSet.getInt("active_days");
      ActivityDate        = resultSet.getDate("activity_date");
      AppSeqNum           = resultSet.getLong("app_seq_num");
      AssignedTo          = resultSet.getString("assigned_to");
      AssignedUserId      = resultSet.getLong("assigned_user_id");
      BankNumber          = resultSet.getInt("bank_number");
      DbaName             = resultSet.getString("dba_name");
      MerchantId          = resultSet.getLong("merchant_number");
      MutedPoints         = resultSet.getInt("muted_points");
      PortfolioName       = resultSet.getString("portfolio_name");
      PortfolioNode       = resultSet.getLong("portfolio_node");
      RiskActivationDate  = resultSet.getDate("risk_activation_date");
      SicCode             = resultSet.getInt("sic_code");
      SicDesc             = resultSet.getString("sic_desc");
      StatusCode          = resultSet.getInt("status_code");
      Status              = resultSet.getString("status");
      TotalPoints         = resultSet.getInt("total_points");

      fname.append( NumberFormatter.getPaddedInt( StatusCode, 2 ) );
      fname.append( DateTimeFormatter.getFormattedDate(ActivityDate,"MMddyyyy") );
      fname.append( MerchantId );

      switch( StatusCode )
      {
        case QS_NEW:
        case QS_PENDING:
          dropDownItems = new NewPendingActionsTable();
          break;

        case QS_AUTO_CLEARED:
        case QS_CLEARED:
          dropDownItems = new ClearedActionsTable();
          break;

        case QS_APPROVED:
          dropDownItems = new ApprovedActionsTable();
          break;
      }

      if ( dropDownItems != null )
      {
        StatusField = new DropDownField( fname.toString(), dropDownItems, true );
        StatusField.setData( Integer.toString(StatusCode) );
      }
    }

    public int compareTo( Object obj )
    {
      RowData         compareObj        = (RowData) obj;
      int             retVal            = 0;


      if ( BankNumber == mesConstants.BANK_ID_BBT )
      {
        if ( ( retVal = (getActiveSortValue() - compareObj.getActiveSortValue()) ) == 0 )
        {
          if ( ( retVal = -(TotalPoints - compareObj.TotalPoints) ) == 0 )
          {
            if ( ( retVal = PortfolioName.compareTo(compareObj.PortfolioName) ) == 0 )
            {
              if ( ( retVal = (int)(PortfolioNode - compareObj.PortfolioNode) ) == 0 )
              {
                if ( ( retVal = DbaName.compareTo(compareObj.DbaName) ) == 0 )
                {
                  if ( ( retVal = (int)(MerchantId - compareObj.MerchantId) ) == 0 )
                  {
                    retVal = -( ActivityDate.compareTo( compareObj.ActivityDate ) );
                  }
                }
              }
            }
          }
        }
      }
      else if( BankNumber == mesConstants.BANK_ID_UBOC )
      {
        if ( ( retVal = -(TotalPoints - compareObj.TotalPoints) ) == 0 )
        {
          if ( ( retVal = PortfolioName.compareTo(compareObj.PortfolioName) ) == 0 )
          {
            if ( ( retVal = (int)(PortfolioNode - compareObj.PortfolioNode) ) == 0 )
            {
              if ( ( retVal = (getActiveSortValue() - compareObj.getActiveSortValue()) ) == 0 )
              {
                if ( ( retVal = DbaName.compareTo(compareObj.DbaName) ) == 0 )
                {
                  if ( ( retVal = (int)(MerchantId - compareObj.MerchantId) ) == 0 )
                  {
                    retVal = -( ActivityDate.compareTo( compareObj.ActivityDate ) );
                  }
                }
              }
            }
          }
        }
      }
      else    // default
      {
        if ( ( retVal = PortfolioName.compareTo(compareObj.PortfolioName) ) == 0 )
        {
          if ( ( retVal = (int)(PortfolioNode - compareObj.PortfolioNode) ) == 0 )
          {
            if ( ( retVal = (getActiveSortValue() - compareObj.getActiveSortValue()) ) == 0 )
            {
              if ( ( retVal = -(TotalPoints - compareObj.TotalPoints) ) == 0 )
              {
                if ( ( retVal = DbaName.compareTo(compareObj.DbaName) ) == 0 )
                {
                  if ( ( retVal = (int)(MerchantId - compareObj.MerchantId) ) == 0 )
                  {
                    retVal = -( ActivityDate.compareTo( compareObj.ActivityDate ) );
                  }
                }
              }
            }
          }
        }
      }
      return( retVal );
    }

    public int getActiveSortValue()
    {
      return( (ActiveDays < 90) ? 0 : 1 );
    }

    public String getActiveSortName()
    {
      return( (ActiveDays < 90) ? "Less than 90 days" : "90 or more days" );
    }
  }

  private     boolean         FullNotes           = false;
  protected   boolean         HideClearedItems    = false;
  protected   boolean         MuteEnabled         = true;
  protected   long            RiskAnalystUserId   = 0L;
  protected   int             RiskThreshold       = RISK_THRESHOLD_DEFAULT;
  protected   int             SortOrder           = SB_CAT_DESC;

  public RiskScoreDataBean( )
  {
    setUseXSSFilter(true);
    setUseCardTruncator(true);
  }

  public void assignMerchant( long merchantId, java.util.Date assignDate, long userId )
  {
    Date              activityDate    = null;
    int               rowCount        = 0;

    try
    {
      if ( merchantId != 0L && assignDate != null && userId != 0L )
      {
        activityDate = new java.sql.Date( assignDate.getTime() );

        if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN ) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:797^11*/

//  ************************************************************
//  #sql [Ctx] { select  count( merchant_number ) 
//              from    risk_queue    rq
//              where   rq.merchant_number = :merchantId and
//                      rq.activity_date = :activityDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( merchant_number )  \n            from    risk_queue    rq\n            where   rq.merchant_number =  :1  and\n                    rq.activity_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:803^11*/

          if ( rowCount == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:807^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_queue
//                (
//                  merchant_number,
//                  activity_date,
//                  user_id,
//                  queue_status,
//                  queue_status_date,
//                  queue_last_user_id
//                )
//                values
//                (
//                  :merchantId,
//                  :activityDate,
//                  :userId,
//                  :QS_PENDING,
//                  sysdate,
//                  :ReportUserBean.getUserId()
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1257 = ReportUserBean.getUserId();
   String theSqlTS = "insert into risk_queue\n              (\n                merchant_number,\n                activity_date,\n                user_id,\n                queue_status,\n                queue_status_date,\n                queue_last_user_id\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                sysdate,\n                 :5 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setLong(3,userId);
   __sJT_st.setInt(4,QS_PENDING);
   __sJT_st.setLong(5,__sJT_1257);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:827^13*/
          }
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:831^13*/

//  ************************************************************
//  #sql [Ctx] { update  risk_queue
//                set     user_id = decode(:userId,-1,null,:userId),
//                        queue_last_user_id = :ReportUserBean.getUserId()
//                where   activity_date = :activityDate and
//                        merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1258 = ReportUserBean.getUserId();
   String theSqlTS = "update  risk_queue\n              set     user_id = decode( :1 ,-1,null, :2 ),\n                      queue_last_user_id =  :3 \n              where   activity_date =  :4  and\n                      merchant_number =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setLong(2,userId);
   __sJT_st.setLong(3,__sJT_1258);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setLong(5,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:838^13*/
          }
        }
        else
        {
          addError( "User does not have right to assign risk queue entries" );
          logEntry("assignMerchant( " + merchantId + " )",
                   "User does not have right to assign risk queue entries");
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("assignMerchant( " + merchantId + ", " + activityDate + ", " + userId + " )", e.toString() );
    }
    finally
    {
    }
  }

  protected boolean autoLoad()
  {
    loadPciData(getReportHierarchyNode());
    return( true );
  }

  protected boolean autoSubmit( )
  {
    String        fname       = getAutoSubmitName();
    boolean       retVal      = true;

    if ( fname.equals("submitPci") )
    {
      storePciData();
    }

    return( retVal );
  }

  protected void deleteRiskAssignment( long id )
  {
    try
    {
      setAutoCommit(false);

      /*@lineinfo:generated-code*//*@lineinfo:883^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_assignments    ra
//          where   ra.rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_assignments    ra\n        where   ra.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:888^7*/

      /*@lineinfo:generated-code*//*@lineinfo:890^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:893^7*/

      setAutoCommit(true);
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteRiskAssignment()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:900^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:900^34*/ } catch( Exception ee ){}
    }
    finally
    {
    }
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    switch( ReportType )
    {
      case RT_RISK_ASSIGNMENTS:
        line.append("\"Login Name\",");
        line.append("\"Name\",");
        line.append("\"Hierarchy Node\",");
        line.append("\"Org Name\",");
        line.append("\"Load Percent\",");
        line.append("\"Merchant Number Lower\",");
        line.append("\"Merchant Number Upper\"");
        break;

      case RT_CAT_STATS:
        line.append("\"Risk Category\",");
        line.append("\"Hit Count\",");
        line.append("\"Total Points\"");
        break;

      default:
        line.append("\"Activity Date\",");
        line.append("\"Merchant ID\",");
        line.append("\"DBA Name\",");
        line.append("\"Activation Date\",");
        line.append("\"SIC\",");
        line.append("\"Assigned To\",");
        line.append("\"Status\",");
        line.append("\"Total Points\"");
        break;
    }
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {

    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);

    switch( ReportType )
    {
      case RT_RISK_ASSIGNMENTS:
      {
        RiskAssignment         ra    = (RiskAssignment)obj;

        line.append("\"");
        line.append( ra.LoginName );
        line.append("\",\"");
        line.append( ra.Name );
        line.append("\",");
        line.append(encodeHierarchyNode(ra.HierarchyNode));
        line.append(",\"");
        line.append( ra.OrgName );
        line.append("\",");
        line.append(ra.LoadPercent);
        line.append(",");
        line.append(encodeHierarchyNode(ra.MerchantIdLower));
        line.append(",");
        line.append(encodeHierarchyNode(ra.MerchantIdUpper));
        break;
      }

      case RT_CAT_STATS:
      {
        CategoryStatistics     cs    = (CategoryStatistics)obj;

        line.append("\"");
        line.append( cs.CategoryDesc );
        line.append("\",");
        line.append( cs.HitCount );
        line.append(",");
        line.append( cs.TotalPoints );
        break;
      }

      default:
      {
        RowData         record    = (RowData)obj;

        line.append( DateTimeFormatter.getFormattedDate(record.ActivityDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( encodeHierarchyNode( record.MerchantId ) );
        line.append( ",\"" );
        line.append( record.DbaName );
        line.append( "\"," );
        line.append( DateTimeFormatter.getFormattedDate(record.ActivationDate,"MM/dd/yyyy") );
        line.append( ",\"" );
        line.append( record.SicCode );
        line.append( " " );
        line.append( record.SicDesc );
        line.append( "\",\"" );
        line.append( record.AssignedTo );
        line.append( "\",\"" );
        line.append( record.Status );
        line.append( "\"," );
        line.append( record.TotalPoints );
        break;
      }
    }
  }

  public void encodeQueueOpUrl( StringBuffer buffer, long merchantId, Date activityDate, long lastMerchantId, int queueOp )
  {
    buffer.append("&queueOp=");
    buffer.append(queueOp);
    buffer.append("&queueMerchant=");
    buffer.append(merchantId);
    buffer.append("&queueDate=");
    buffer.append(DateTimeFormatter.getFormattedDate(activityDate,"MM/dd/yyyy"));
    buffer.append("#goto");
    buffer.append(lastMerchantId);
  }

  public int encodeSortOrder( int selectedOrder )
  {
    int     retVal = selectedOrder;

    // if the selected order is same as current order
    // the invert the current order
    if ( Math.abs(SortOrder) == Math.abs(selectedOrder) )
    {
      retVal = (SortOrder * -1);
    }
    return( retVal );
  }

  public int getDaysOverThreshold( )
  {
    int           days        = 0;
    long          merchantId  = getReportMerchantId();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1042^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(distinct activity_date) 
//          from    risk_score_summary    rss
//          where   rss.merchant_number = :merchantId and
//                  rss.activity_date between (:ReportDateEnd - 365) and :ReportDateEnd and
//                  rss.total_points >= :RiskThreshold
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(distinct activity_date)  \n        from    risk_score_summary    rss\n        where   rss.merchant_number =  :1  and\n                rss.activity_date between ( :2  - 365) and  :3  and\n                rss.total_points >=  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateEnd);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setInt(4,RiskThreshold);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   days = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1049^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("getDaysOverThreshold()",e.toString());
    }
    return( days );
  }

  public String getDetailUrl( long merchantId, int riskCatId, Date activityDate )
  {
    Date                beginDate     = null;
    Calendar            cal           = Calendar.getInstance();
    StringBuffer        detailRef     = new StringBuffer();
    Date                endDate       = null;
    long                merchantOrgId = hierarchyNodeToOrgId( merchantId );
    String              temp          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1069^7*/

//  ************************************************************
//  #sql [Ctx] { select  rc.detail_url   
//          from    risk_categories     rc
//          where   rc.risk_cat_id = :riskCatId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rc.detail_url    \n        from    risk_categories     rc\n        where   rc.risk_cat_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,riskCatId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   temp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1074^7*/
    }
    catch( java.sql.SQLException e )
    {
    }

    if( temp != null )
    {
      detailRef.append( temp );

      cal.setTime( activityDate );

      endDate = new java.sql.Date( cal.getTime().getTime() );

      switch( riskCatId )
      {
        case TT_ZERO_DOLLAR_BATCH:
        case TT_VOLUME_EXCEPTIONS:
        case TT_CREDIT_VOLUME:
        case TT_LARGE_TICKET_SALES:
        case TT_CREDIT_SALES_RATIO:
        case TT_FRAUD_CARD_NUMBER:
        case TT_TE_LARGE_TICKET_SALES:
        case TT_AVG_DAILY_SALES_COUNT:
          detailRef.append( ( ( detailRef.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
          detailRef.append( "dataType=0" );
          break;

        case TT_FOREIGN_CARD_NUMBER:
          detailRef.append( ( ( detailRef.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
          detailRef.append( "dataType=0" );
          break;

        case TT_AUTH_CAPTURE_RATIO:
          cal.add(Calendar.DAY_OF_MONTH,-3);
          break;

        case TT_DECL_AUTH_RATIO:
        case TT_CHARGEBACK_VOLUME:
        case TT_DUPLICATE_CARD:
          cal.add(Calendar.DAY_OF_MONTH,-7);
          break;

        default:
          break;
      }
      beginDate = new java.sql.Date( cal.getTime().getTime() );
      encodeOrgUrl( detailRef, merchantOrgId, beginDate, endDate );
    }

    return( detailRef.toString() );
  }

  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Date                    endDate   = getReportDateEnd();
    StringBuffer            filename  = new StringBuffer("");

    if ( ReportType == RT_CAT_STATS )
    {
      filename.append( getReportHierarchyNode() );
      filename.append("_risk_cat_stats_");
      filename.append( DateTimeFormatter.getFormattedDate( beginDate, "MMddyy" ) );
      if ( !beginDate.equals(endDate) )
      {
        filename.append("_to_");
        filename.append( DateTimeFormatter.getFormattedDate( endDate, "MMddyy" ) );
      }
    }
    else if ( ReportType == RT_RISK_ASSIGNMENTS )
    {
      filename.append( getReportHierarchyNode() );
      filename.append("_risk_assignments");
    }
    else
    {
      filename.append( getReportHierarchyNode() );
      filename.append("_risk_score_system");
    }
    return ( filename.toString() );
  }

  public boolean getHideClearedItemsFlag()
  {
    return( HideClearedItems );
  }

  public Date getHighScoreCutoffDate( )
  {
    Date        retVal    = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1167^7*/

//  ************************************************************
//  #sql [Ctx] { select (:ReportDateEnd-365)    
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select ( :1 -365)     \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1171^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("getHighScoreCutoffDate()",e.toString());
    }

    return( retVal );
  }

  public Vector getNoteTypes()
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    Vector              retVal      = new Vector();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1189^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  type          as note_type,
//                  description   as note_desc
//          from    service_call_types
//          where   nvl(risk_type,'N') = 'Y'
//          order by display_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type          as note_type,\n                description   as note_desc\n        from    service_call_types\n        where   nvl(risk_type,'N') = 'Y'\n        order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.RiskScoreDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1196^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        retVal.addElement( new NoteType( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getNoteTypes()",e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( retVal );
  }

  public long getRiskAnalystUserId()
  {
    return( RiskAnalystUserId );
  }

  public int getRiskThreshold()
  {
    return( RiskThreshold );
  }

  public int getSortOrder()
  {
    return( SortOrder );
  }

  public boolean hasQueueOpRight( int queueOp )
  {
    boolean             retVal        = false;

    switch( queueOp )
    {
      case QS_CLEARED:
        retVal = ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_ANALYST ) ||
                   ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN ) );
        break;

      default:
        retVal = ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN );
        break;
    }
    return(retVal);
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup      fgroup        = null;
    long            merchantId    = getReportHierarchyNode();

    fgroup = new FieldGroup("pciFields");
    fgroup.add(new DropDownField( "pciLevel", "PCI Level", new PCILevelTable(merchantId), true ));
    fgroup.add(new DropDownField( "pciVendor", "PCI Vendor", new PCIVendorTable(merchantId), true ));
    fgroup.add(new Field        ( "pciVendorOther",  "Other Vendor Name", 40,40, true ));
    fgroup.getField("pciVendorOther").addValidation(new PCIOtherValidation(fgroup.getField("pciVendor")));
    fgroup.add(new DateField    ( "pciLastScanDate", "Last Scan Date", true ));
    fgroup.add(new DateField    ( "pciNextScanDate", "Next Scan Date", true ));
    fgroup.add(new DropDownField( "pciCompliant","Compliant", new YesNoTable(), true ));
    fgroup.add(new ButtonField  ( "submitPci","Update") );
    ((DateField)fgroup.getField("pciLastScanDate")).setDateFormat("MM/dd/yyyy");
    ((DateField)fgroup.getField("pciNextScanDate")).setDateFormat("MM/dd/yyyy");

    fields.add(fgroup);

    // BB&T specific fields (for now at least)
    if(getReportBankId() == 3867)
    {
      FieldGroup bbtPci = new FieldGroup("bbtPciFields");
      bbtPci.add(new RadioButtonField ("pciValRequiredInd",     yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new RadioButtonField ("thirdPartySwInd",       yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new Field            ("thirdPartySwName",      75,30,true));
      bbtPci.add(new Field            ("thirdPartySwVer",       75,30,true));
      bbtPci.add(new Field            ("thirdPartyName",        75,30,true));
      bbtPci.add(new RadioButtonField ("paymentAppValConfInd",  yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new RadioButtonField ("thirdPartyAgentInd",    yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new Field            ("thirdPartyAgentName1",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentName2",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentDesc1",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentDesc2",  75,15,true));

      // override existing PCI Compliant field
      bbtPci.add(new RadioButtonField ("pciCompliant",          yesNoRadioList,-1,true,"Required"));

      fields.add(bbtPci);
    }

    fields.setHtmlExtra("class=\"formFields\"");
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }

  public boolean isSortedByPortfolio( )
  {
    boolean     retVal        = true;

    switch( getReportBankId() )
    {
      case mesConstants.BANK_ID_BBT:
        retVal = false;
        break;
    }
    return( retVal );
  }

  public void loadCategoryStatistics( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it                = null;
    long                nodeId            = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet         = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1327^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  :nodeId                                   as hierarchy_node,
//                  nvl(rc.risk_cat_desc,
//                      ('Risk Category ' || rs.risk_cat_id)) as risk_category,
//                  count(rs.merchant_number)                 as hit_count,
//                  sum(rs.points)                            as hit_points
//          from    organization        o,
//                  group_merchant      gm,
//                  risk_score          rs,
//                  risk_categories     rc
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  rs.merchant_number = gm.merchant_number and
//                  rs.activity_date between :beginDate and :endDate and
//                  rc.risk_cat_id(+) = rs.risk_cat_id
//          group by nvl(rc.risk_cat_desc,('Risk Category ' || rs.risk_cat_id))
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                    as hierarchy_node,\n                nvl(rc.risk_cat_desc,\n                    ('Risk Category ' || rs.risk_cat_id)) as risk_category,\n                count(rs.merchant_number)                 as hit_count,\n                sum(rs.points)                            as hit_points\n        from    organization        o,\n                group_merchant      gm,\n                risk_score          rs,\n                risk_categories     rc\n        where   o.org_group =  :2  and\n                gm.org_num = o.org_num and\n                rs.merchant_number = gm.merchant_number and\n                rs.activity_date between  :3  and  :4  and\n                rc.risk_cat_id(+) = rs.risk_cat_id\n        group by nvl(rc.risk_cat_desc,('Risk Category ' || rs.risk_cat_id))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1344^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new CategoryStatistics( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadCategoryStatistics(" + nodeId + "," + beginDate + "," + endDate + ")",
               e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
  }

  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    switch( ReportType )
    {
      case RT_RISK_ASSIGNMENTS:
        loadRiskAssignments();
        break;

      case RT_DETAILS:
        loadDetailData(orgId,beginDate,endDate);
        break;

      case RT_CAT_STATS:
        loadCategoryStatistics(orgId,beginDate,endDate);
        break;

      default:
        loadSummaryData(orgId,beginDate,endDate);
        break;
    }
  }

  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator       it              = null;
    ResultSet               resultSet       = null;

    try
    {
      ReportRows.removeAllElements();

      /*@lineinfo:generated-code*//*@lineinfo:1396^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant)
//                      index (rs idx_risk_score_merch_date) */
//                  rs.merchant_number                    as merchant_number,
//                  rs.activity_date                      as activity_date,
//                  rs.risk_cat_id                        as risk_cat_id,
//                  nvl(rc.risk_cat_desc,rs.risk_cat_id)  as risk_cat_desc,
//                  rs.score_desc                         as exception_desc,
//                  rs.muted_date                         as muted_date,
//                  u.name                                as muted_by,
//                  rs.points                             as points
//          from    group_merchant      gm,
//                  risk_score          rs,
//                  risk_categories     rc,
//                  users               u
//          where   gm.org_num = :orgId and
//                  rs.merchant_number = gm.merchant_number and
//                  rs.activity_date between :beginDate and :endDate and
//                  rc.risk_cat_id(+) = rs.risk_cat_id and
//                  u.user_id(+) = rs.muted_by_user_id
//          order by  rs.merchant_number,
//                    rc.risk_cat_desc,
//                    rs.activity_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant)\n                    index (rs idx_risk_score_merch_date) */\n                rs.merchant_number                    as merchant_number,\n                rs.activity_date                      as activity_date,\n                rs.risk_cat_id                        as risk_cat_id,\n                nvl(rc.risk_cat_desc,rs.risk_cat_id)  as risk_cat_desc,\n                rs.score_desc                         as exception_desc,\n                rs.muted_date                         as muted_date,\n                u.name                                as muted_by,\n                rs.points                             as points\n        from    group_merchant      gm,\n                risk_score          rs,\n                risk_categories     rc,\n                users               u\n        where   gm.org_num =  :1  and\n                rs.merchant_number = gm.merchant_number and\n                rs.activity_date between  :2  and  :3  and\n                rc.risk_cat_id(+) = rs.risk_cat_id and\n                u.user_id(+) = rs.muted_by_user_id\n        order by  rs.merchant_number,\n                  rc.risk_cat_desc,\n                  rs.activity_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1420^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RiskScoreDetail( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadHighScores( )
  {
    loadHighScores( getReportMerchantId(), ReportDateEnd );
  }

  public void loadHighScores( long merchantId, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    int                           rowCount          = 0;

    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:1456^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant) */
//                  rss.activity_date       as activity_date,
//                  rss.total_points        as points
//          from    risk_score_summary  rss
//          where   rss.merchant_number = :merchantId and
//                  rss.activity_date between (:endDate - 365) and :endDate
//          order by rss.total_points desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant) */\n                rss.activity_date       as activity_date,\n                rss.total_points        as points\n        from    risk_score_summary  rss\n        where   rss.merchant_number =  :1  and\n                rss.activity_date between ( :2  - 365) and  :3 \n        order by rss.total_points desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1465^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new HighScoreEntry(resultSet) );

        if ( ++rowCount >= HIGH_SCORE_COUNT )
        {
          break;
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadHighScores()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadMerchantVolume( )
  {
    loadMerchantVolume( getReportMerchantId() );
  }

  public void loadMerchantVolume( long merchantId )
  {
    Date                      beginDate     = null;
    Calendar                  cal           = null;
    Date                      endDate       = null;
    ResultSetIterator         it            = null;
    ResultSet                 resultSet     = null;

    try
    {
      ReportRows.removeAllElements();

      cal = Calendar.getInstance();
      endDate = new java.sql.Date( cal.getTime().getTime() );

      // set the begin date to 2 months prior
      cal.add(Calendar.MONTH,(1-VOLUME_MONTH_COUNT));
      cal.set(Calendar.DAY_OF_MONTH,1);

      beginDate = new java.sql.Date( cal.getTime().getTime() );

      /*@lineinfo:generated-code*//*@lineinfo:1516^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sm.batch_date,'month')                          as vol_month,
//                  sum(sm.bank_sales_amount + sm.nonbank_sales_amount)   as sales_amount,
//                  sum(sm.bank_sales_count  + sm.nonbank_sales_count )   as sales_count,
//                  round( decode( sum(sm.bank_sales_count + sm.nonbank_sales_count),
//                                 0,0,
//                                 (sum(sm.bank_sales_amount + sm.nonbank_sales_amount)/
//                                  sum(sm.bank_sales_count  + sm.nonbank_sales_count ))
//                               ),2 )                                    as avg_ticket
//          from    daily_detail_file_summary sm
//          where   sm.merchant_number = :merchantId and
//                  sm.batch_date between :beginDate and :endDate
//          group by trunc(sm.batch_date,'month')
//          order by vol_month desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sm.batch_date,'month')                          as vol_month,\n                sum(sm.bank_sales_amount + sm.nonbank_sales_amount)   as sales_amount,\n                sum(sm.bank_sales_count  + sm.nonbank_sales_count )   as sales_count,\n                round( decode( sum(sm.bank_sales_count + sm.nonbank_sales_count),\n                               0,0,\n                               (sum(sm.bank_sales_amount + sm.nonbank_sales_amount)/\n                                sum(sm.bank_sales_count  + sm.nonbank_sales_count ))\n                             ),2 )                                    as avg_ticket\n        from    daily_detail_file_summary sm\n        where   sm.merchant_number =  :1  and\n                sm.batch_date between  :2  and  :3 \n        group by trunc(sm.batch_date,'month')\n        order by vol_month desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1531^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new MerchantVolumeData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  public void loadNotes( )
  {
    loadNotes( getReportOrgId() );
  }

  public void loadNotes( long orgId )
  {
    long                  merchantId    = getReportMerchantId();
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    int                   rowCount      = 0;

    try
    {
      ReportRows.removeAllElements();

      /*@lineinfo:generated-code*//*@lineinfo:1566^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rn.merchant_number        as merchant_number,
//                  mf.dba_name               as dba_name,
//                  rn.note_date              as note_date,
//                  nvl(sct.description,
//                      'Type: ' || rn.type)  as note_type,
//                  rn.other_description      as other_desc,
//                  u.login_name              as login_name,
//                  rn.notes                  as notes
//          from    group_merchant          gm,
//                  group_rep_merchant      grm,
//                  risk_notes              rn,
//                  service_call_types      sct,
//                  mif                     mf,
//                  users                   u
//          where   gm.org_num = :orgId and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                  rn.merchant_number = gm.merchant_number and
//                  -- the old risk_note_types table defined 0 as other
//                  sct.type(+) = decode(rn.type,0,99,rn.type) and
//                  mf.merchant_number = rn.merchant_number and
//                  u.user_id = rn.user_id
//          order by rn.note_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rn.merchant_number        as merchant_number,\n                mf.dba_name               as dba_name,\n                rn.note_date              as note_date,\n                nvl(sct.description,\n                    'Type: ' || rn.type)  as note_type,\n                rn.other_description      as other_desc,\n                u.login_name              as login_name,\n                rn.notes                  as notes\n        from    group_merchant          gm,\n                group_rep_merchant      grm,\n                risk_notes              rn,\n                service_call_types      sct,\n                mif                     mf,\n                users                   u\n        where   gm.org_num =  :1  and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and\n                rn.merchant_number = gm.merchant_number and\n                -- the old risk_note_types table defined 0 as other\n                sct.type(+) = decode(rn.type,0,99,rn.type) and\n                mf.merchant_number = rn.merchant_number and\n                u.user_id = rn.user_id\n        order by rn.note_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1592^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new NoteSummary( resultSet ) );

        if ( FullNotes == false )
        {
          // only take the most recent calls
          if ( ++rowCount > SHORT_NOTE_COUNT )
          {
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadNotes()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  public class PCIHistory
  {
    private Date quizDate;
    private String dateStr;
    private String result;

    public PCIHistory()
    {
      quizDate = null;
      dateStr = "";
      result = "";
    }

    public PCIHistory(Date d, String r)
    {
      quizDate = d;
      if(d!=null)
      {
        SimpleDateFormat df = new SimpleDateFormat("EEE, MMM d, ''yy");
        dateStr = df.format(quizDate);
      }
      else
      {
        dateStr = "n/a";
      }
      result = r==null?"":r;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append(result.toUpperCase()).append(" taken ").append(dateStr);
      return sb.toString();
    }
  }

  public void loadPciData( long merchantId )
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1662^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    risk_pci_level               as pci_level,
//                    risk_pci_vendor_code         as pci_vendor,
//                    risk_pci_vendor_other        as pci_vendor_other,
//                    risk_pci_last_scan_date      as pci_last_scan_date,
//                    risk_pci_next_scan_date      as pci_next_scan_date,
//                    risk_pci_compliant           as pci_compliant
//          from      merchant
//          where     merch_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    risk_pci_level               as pci_level,\n                  risk_pci_vendor_code         as pci_vendor,\n                  risk_pci_vendor_other        as pci_vendor_other,\n                  risk_pci_last_scan_date      as pci_last_scan_date,\n                  risk_pci_next_scan_date      as pci_next_scan_date,\n                  risk_pci_compliant           as pci_compliant\n        from      merchant\n        where     merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1672^7*/
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        setFields(resultSet,false);  // set fields, do not scan result set
      }
      resultSet.close();
      it.close();

      //Load PCI Quiz History
      /*@lineinfo:generated-code*//*@lineinfo:1683^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    date_taken        as quiz_date,
//                    result            as result
//          from      quiz_history
//          where     merch_number = :merchantId
//            and     quiz_id      = 1
//          order by  date_taken desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    date_taken        as quiz_date,\n                  result            as result\n        from      quiz_history\n        where     merch_number =  :1 \n          and     quiz_id      = 1\n        order by  date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1691^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        //populate quiz history
        pciHistoryList.add(new PCIHistory(resultSet.getDate("quiz_date"),
                                          resultSet.getString("result")));
      }
      resultSet.close();
      it.close();

      //Set Merchant Level and transaction count
      //same as in PCIQuestionnaire
      long count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1706^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(round(((sum(sm.visa_sales_count)/count(sm.active_date))*12)),0)
//          
//          from    monthly_extract_summary sm
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(round(((sum(sm.visa_sales_count)/count(sm.active_date))*12)),0)\n         \n        from    monthly_extract_summary sm\n        where   sm.merchant_number =  :1  and\n                sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1713^7*/

      this.transactionCount = count;

      if(this.transactionCount > 6000000L)
      {
        merchLevel = 1;
      }
      else if(6000000L >= this.transactionCount && this.transactionCount >= 1000000L)
      {
        merchLevel = 2;
      }
      else
      {
        //check only ecommerce transactions
        /*@lineinfo:generated-code*//*@lineinfo:1728^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(round(((sum(sm.vmc_sales_count)/count(sm.active_date))*12)),0)
//            
//            from    monthly_extract_summary   sm,
//                    merch_pos mp,
//                    pos_category pc,
//                    merchant m
//            where   sm.merchant_number = :merchantId and
//                    sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and
//                    sm.merchant_number = m.merch_number and
//                    m.app_seq_num = mp.app_seq_num and
//                    mp.pos_code = pc.pos_code and
//                    pc.pos_type not in (1, 4) --everything else could be considered e-commerce
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(round(((sum(sm.vmc_sales_count)/count(sm.active_date))*12)),0)\n           \n          from    monthly_extract_summary   sm,\n                  merch_pos mp,\n                  pos_category pc,\n                  merchant m\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and\n                  sm.merchant_number = m.merch_number and\n                  m.app_seq_num = mp.app_seq_num and\n                  mp.pos_code = pc.pos_code and\n                  pc.pos_type not in (1, 4) --everything else could be considered e-commerce";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1742^9*/

        if(count < 20000)
        {
         merchLevel = 4;
        }
        else
        {
         merchLevel = 3;
        }
      }

      //load PCI POS merchant contact information
      /*@lineinfo:generated-code*//*@lineinfo:1755^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pci_name,
//                  pci_email,
//                  pci_phone
//          from    pos_surveys
//          where   merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pci_name,\n                pci_email,\n                pci_phone\n        from    pos_surveys\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1762^7*/
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        pciName   = resultSet.getString("pci_name");
        pciEmail  = resultSet.getString("pci_email");
        if(resultSet.getString("pci_phone")!=null)
        {
          pciPhone  = PhoneField.format(resultSet.getString("pci_phone"));
        }
      }
      resultSet.close();
      it.close();


      //Establish extra BBT data
      if(getReportBankId() == 3867)
      {
        // retrieve other PCI data fields
        /*@lineinfo:generated-code*//*@lineinfo:1782^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bbt.pci_val_required_ind      pci_val_required_ind,
//                    bbt.third_party_sw_ind        third_party_sw_ind,
//                    bbt.third_party_sw_name       third_party_sw_name,
//                    bbt.third_party_name          third_party_name,
//                    bbt.third_party_sw_ver        third_party_sw_ver,
//                    bbt.payment_app_val_conf_ind  payment_app_val_conf_ind,
//                    bbt.third_party_agent_ind     third_party_agent_ind,
//                    bbt.third_party_agent_name_1  third_party_agent_name_1,
//                    bbt.third_party_agent_name_2  third_party_agent_name_2,
//                    bbt.third_party_agent_desc_1  third_party_agent_desc_1,
//                    bbt.third_party_agent_desc_2  third_party_agent_desc_2
//            from    app_merch_bbt bbt,
//                    merchant      mr
//            where   mr.merch_number = :merchantId and
//                    mr.app_seq_num = bbt.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bbt.pci_val_required_ind      pci_val_required_ind,\n                  bbt.third_party_sw_ind        third_party_sw_ind,\n                  bbt.third_party_sw_name       third_party_sw_name,\n                  bbt.third_party_name          third_party_name,\n                  bbt.third_party_sw_ver        third_party_sw_ver,\n                  bbt.payment_app_val_conf_ind  payment_app_val_conf_ind,\n                  bbt.third_party_agent_ind     third_party_agent_ind,\n                  bbt.third_party_agent_name_1  third_party_agent_name_1,\n                  bbt.third_party_agent_name_2  third_party_agent_name_2,\n                  bbt.third_party_agent_desc_1  third_party_agent_desc_1,\n                  bbt.third_party_agent_desc_2  third_party_agent_desc_2\n          from    app_merch_bbt bbt,\n                  merchant      mr\n          where   mr.merch_number =  :1  and\n                  mr.app_seq_num = bbt.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1799^9*/

        resultSet = it.getResultSet();

        if(resultSet.next())
        {
          setFields(resultSet,false);
        }

        resultSet.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      //e.printStackTrace();
      logEntry("loadPciData()", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  public void loadRiskAssignments( )
  {
    int                   bankNumber    = getReportBankId();
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;

    try
    {
      ReportRows.removeAllElements();

      /*@lineinfo:generated-code*//*@lineinfo:1834^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ra.rec_id                       as rec_id,
//                    ra.user_id                      as user_id,
//                    u.login_name                    as login_name,
//                    u.name                          as name,
//                    ra.hierarchy_node               as hierarchy_node,
//                    o.org_name                      as org_name,
//                    nvl(ra.load_percent,100)        as load_percent,
//                    nvl(merchant_number_lower,-1)   as merchant_number_lower,
//                    nvl(merchant_number_upper,-1)   as merchant_number_upper
//          from      t_hierarchy             th,
//                    risk_assignments        ra,
//                    users                   u,
//                    organization            o
//          where     th.hier_type = 1 and
//                    ( ( :bankNumber = 9999 and
//                        th.ancestor in ( select (bank_number || '00000') from mbs_banks where owner = 1 ) ) or
//                      ( th.ancestor = (:bankNumber || '00000') ) ) and
//                    decode(u.hierarchy_node,9999999999,394100000,u.hierarchy_node) = th.descendent and
//                    ra.user_id = u.user_id and
//                    o.org_group(+) = ra.hierarchy_node
//          order by  ra.hierarchy_node, nvl(ra.load_percent,100), ra.login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ra.rec_id                       as rec_id,\n                  ra.user_id                      as user_id,\n                  u.login_name                    as login_name,\n                  u.name                          as name,\n                  ra.hierarchy_node               as hierarchy_node,\n                  o.org_name                      as org_name,\n                  nvl(ra.load_percent,100)        as load_percent,\n                  nvl(merchant_number_lower,-1)   as merchant_number_lower,\n                  nvl(merchant_number_upper,-1)   as merchant_number_upper\n        from      t_hierarchy             th,\n                  risk_assignments        ra,\n                  users                   u,\n                  organization            o\n        where     th.hier_type = 1 and\n                  ( (  :1  = 9999 and\n                      th.ancestor in ( select (bank_number || '00000') from mbs_banks where owner = 1 ) ) or\n                    ( th.ancestor = ( :2  || '00000') ) ) and\n                  decode(u.hierarchy_node,9999999999,394100000,u.hierarchy_node) = th.descendent and\n                  ra.user_id = u.user_id and\n                  o.org_group(+) = ra.hierarchy_node\n        order by  ra.hierarchy_node, nvl(ra.load_percent,100), ra.login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1857^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RiskAssignment( resultSet ) );
      }
    }
    catch(Exception e)
    {
      logEntry("loadRiskAssignments()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  public void loadScoresByDay( )
  {
    loadScoresByDay( getReportMerchantId(), ReportDateEnd );
  }

  public void loadScoresByDay( long merchantId, Date endDate )
  {
    int                 excludeMuted      = ((MuteEnabled == true) ? 1 : 0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:1891^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant)
//                      index (rs idx_risk_score_merch_date) */
//                  op.org_group                          as portfolio_node,
//                  op.org_name                           as portfolio_name,
//                  rss.merchant_number                   as merchant_number,
//                  nvl(mr.app_seq_num,0)                 as app_seq_num,
//                  mf.dba_name                           as dba_name,
//                  mf.bank_number                        as bank_number,
//                  mf.sic_code                           as sic_code,
//                  nvl(sc.merchant_type,'Unavailable')   as sic_desc,
//                  mf.activation_date                    as activation_date,
//                  mf.risk_activation_date               as risk_activation_date,
//                  (trunc(sysdate)-mf.risk_activation_date)
//                                                        as active_days,
//                  rss.activity_date                     as activity_date,
//                  rss.total_points                      as total_points,
//                  rss.muted_points                      as muted_points,
//                  nvl(ru.login_name,'Unassigned')       as assigned_to,
//                  nvl(ru.user_id,-1)                    as assigned_user_id,
//                  nvl(rq.queue_status,:QS_NEW)          as status_code,
//                  nvl(rstat.status_desc,'New')          as status
//          from    risk_score_summary  rss,
//                  risk_queue          rq,
//                  risk_status         rstat,
//                  users               ru,
//                  mif                 mf,
//                  merchant            mr,
//                  groups              g,
//                  organization        op,
//                  sic_codes           sc
//          where   rss.merchant_number = :merchantId and
//                  rss.activity_date between (:endDate - :HISTORIC_DAY_COUNT) and :endDate and
//                  (
//                    (:excludeMuted = 0) or
//                    not exists
//                    (
//                      select  rm.merchant_number
//                      from    risk_score_muted rm
//                      where   rm.merchant_number = rss.merchant_number and
//                              rm.activity_date = rss.activity_date
//                    )
//                  ) and
//                  (rss.total_points - (nvl(rss.muted_points,0) * :excludeMuted)) >= :RiskThreshold and
//                  rq.activity_date(+) = rss.activity_date and
//                  rq.merchant_number(+) = rss.merchant_number and
//                  ru.user_id(+) = rq.user_id and
//                  rstat.status_code(+) = rq.queue_status and
//                  mf.merchant_number = rss.merchant_number and
//                  mr.merch_number(+) = mf.merchant_number and
//                  g.assoc_number(+) = (mf.bank_number || mf.dmagent) and
//                  op.org_group = g.group_2 and
//                  sc.sic_code(+) = mf.sic_code
//          order by rss.activity_date, rss.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant)\n                    index (rs idx_risk_score_merch_date) */\n                op.org_group                          as portfolio_node,\n                op.org_name                           as portfolio_name,\n                rss.merchant_number                   as merchant_number,\n                nvl(mr.app_seq_num,0)                 as app_seq_num,\n                mf.dba_name                           as dba_name,\n                mf.bank_number                        as bank_number,\n                mf.sic_code                           as sic_code,\n                nvl(sc.merchant_type,'Unavailable')   as sic_desc,\n                mf.activation_date                    as activation_date,\n                mf.risk_activation_date               as risk_activation_date,\n                (trunc(sysdate)-mf.risk_activation_date)\n                                                      as active_days,\n                rss.activity_date                     as activity_date,\n                rss.total_points                      as total_points,\n                rss.muted_points                      as muted_points,\n                nvl(ru.login_name,'Unassigned')       as assigned_to,\n                nvl(ru.user_id,-1)                    as assigned_user_id,\n                nvl(rq.queue_status, :1 )          as status_code,\n                nvl(rstat.status_desc,'New')          as status\n        from    risk_score_summary  rss,\n                risk_queue          rq,\n                risk_status         rstat,\n                users               ru,\n                mif                 mf,\n                merchant            mr,\n                groups              g,\n                organization        op,\n                sic_codes           sc\n        where   rss.merchant_number =  :2  and\n                rss.activity_date between ( :3  -  :4 ) and  :5  and\n                (\n                  ( :6  = 0) or\n                  not exists\n                  (\n                    select  rm.merchant_number\n                    from    risk_score_muted rm\n                    where   rm.merchant_number = rss.merchant_number and\n                            rm.activity_date = rss.activity_date\n                  )\n                ) and\n                (rss.total_points - (nvl(rss.muted_points,0) *  :7 )) >=  :8  and\n                rq.activity_date(+) = rss.activity_date and\n                rq.merchant_number(+) = rss.merchant_number and\n                ru.user_id(+) = rq.user_id and\n                rstat.status_code(+) = rq.queue_status and\n                mf.merchant_number = rss.merchant_number and\n                mr.merch_number(+) = mf.merchant_number and\n                g.assoc_number(+) = (mf.bank_number || mf.dmagent) and\n                op.org_group = g.group_2 and\n                sc.sic_code(+) = mf.sic_code\n        order by rss.activity_date, rss.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QS_NEW);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,HISTORIC_DAY_COUNT);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setInt(6,excludeMuted);
   __sJT_st.setInt(7,excludeMuted);
   __sJT_st.setInt(8,RiskThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1946^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadScoresByDay()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                 excludeMuted      = ((MuteEnabled == true) ? 1 : 0);
    int                 hideClearedItems  = ((HideClearedItems == true) ? 1 : 0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:1978^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant)
//                      index (rs idx_risk_score_merch_date) */
//                  op.org_group                          as portfolio_node,
//                  op.org_name                           as portfolio_name,
//                  rss.merchant_number                   as merchant_number,
//                  nvl(mr.app_seq_num,0)                 as app_seq_num,
//                  mf.bank_number                        as bank_number,
//                  mf.dba_name                           as dba_name,
//                  mf.sic_code                           as sic_code,
//                  nvl(sc.merchant_type,'Unavailable')   as sic_desc,
//                  mf.activation_date                    as activation_date,
//                  mf.risk_activation_date               as risk_activation_date,
//                  (trunc(sysdate)-mf.risk_activation_date)  as active_days,
//                  rss.activity_date                     as activity_date,
//                  rss.total_points                      as total_points,
//                  rss.muted_points                      as muted_points,
//                  nvl(ru.login_name,'Unassigned')       as assigned_to,
//                  nvl(ru.user_id, -1)                   as assigned_user_id,
//                  nvl(rq.queue_status,:QS_NEW)          as status_code,
//                  nvl(rstat.status_desc,'New')          as status
//          from    group_merchant      gm,
//                  group_rep_merchant  grm,
//                  risk_score_summary  rss,
//                  risk_queue          rq,
//                  risk_status         rstat,
//                  users               ru,
//                  mif                 mf,
//                  merchant            mr,
//                  groups              g,
//                  organization        op,
//                  sic_codes           sc
//          where   gm.org_num = :orgId and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                  rss.merchant_number = gm.merchant_number and
//                  rss.activity_date between :beginDate and :endDate and
//                  (
//                    (:excludeMuted = 0) or
//                    not exists
//                    (
//                      select  rm.merchant_number
//                      from    risk_score_muted rm
//                      where   rm.merchant_number = rss.merchant_number and
//                              rm.activity_date = rss.activity_date
//                    )
//                  ) and
//                  (rss.total_points - (nvl(rss.muted_points,0) * :excludeMuted)) >= :RiskThreshold and
//                  rq.merchant_number(+) = rss.merchant_number and
//                  rq.activity_date(+) = rss.activity_date and
//                  (
//                    nvl(rq.user_id,0) <= 0 or
//                    rq.user_id = decode( :RiskAnalystUserId, :RISK_ANALYST_ALL, rq.user_id, :RiskAnalystUserId )
//                  ) and
//                  (
//                    :hideClearedItems = 0 or
//                    nvl(rq.queue_status,1) in ( 0, 1 )      -- new, pending
//                  ) and
//                  ru.user_id(+) = rq.user_id and
//                  rstat.status_code(+) = rq.queue_status and
//                  mf.merchant_number = rss.merchant_number and
//                  mr.merch_number(+) = mf.merchant_number and
//                  g.assoc_number(+) = (mf.bank_number || mf.dmagent) and
//                  op.org_group = g.group_2 and
//                  sc.sic_code(+) = mf.sic_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant)\n                    index (rs idx_risk_score_merch_date) */\n                op.org_group                          as portfolio_node,\n                op.org_name                           as portfolio_name,\n                rss.merchant_number                   as merchant_number,\n                nvl(mr.app_seq_num,0)                 as app_seq_num,\n                mf.bank_number                        as bank_number,\n                mf.dba_name                           as dba_name,\n                mf.sic_code                           as sic_code,\n                nvl(sc.merchant_type,'Unavailable')   as sic_desc,\n                mf.activation_date                    as activation_date,\n                mf.risk_activation_date               as risk_activation_date,\n                (trunc(sysdate)-mf.risk_activation_date)  as active_days,\n                rss.activity_date                     as activity_date,\n                rss.total_points                      as total_points,\n                rss.muted_points                      as muted_points,\n                nvl(ru.login_name,'Unassigned')       as assigned_to,\n                nvl(ru.user_id, -1)                   as assigned_user_id,\n                nvl(rq.queue_status, :1 )          as status_code,\n                nvl(rstat.status_desc,'New')          as status\n        from    group_merchant      gm,\n                group_rep_merchant  grm,\n                risk_score_summary  rss,\n                risk_queue          rq,\n                risk_status         rstat,\n                users               ru,\n                mif                 mf,\n                merchant            mr,\n                groups              g,\n                organization        op,\n                sic_codes           sc\n        where   gm.org_num =  :2  and\n                grm.user_id(+) =  :3  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :4  = -1 ) and\n                rss.merchant_number = gm.merchant_number and\n                rss.activity_date between  :5  and  :6  and\n                (\n                  ( :7  = 0) or\n                  not exists\n                  (\n                    select  rm.merchant_number\n                    from    risk_score_muted rm\n                    where   rm.merchant_number = rss.merchant_number and\n                            rm.activity_date = rss.activity_date\n                  )\n                ) and\n                (rss.total_points - (nvl(rss.muted_points,0) *  :8 )) >=  :9  and\n                rq.merchant_number(+) = rss.merchant_number and\n                rq.activity_date(+) = rss.activity_date and\n                (\n                  nvl(rq.user_id,0) <= 0 or\n                  rq.user_id = decode(  :10 ,  :11 , rq.user_id,  :12  )\n                ) and\n                (\n                   :13  = 0 or\n                  nvl(rq.queue_status,1) in ( 0, 1 )      -- new, pending\n                ) and\n                ru.user_id(+) = rq.user_id and\n                rstat.status_code(+) = rq.queue_status and\n                mf.merchant_number = rss.merchant_number and\n                mr.merch_number(+) = mf.merchant_number and\n                g.assoc_number(+) = (mf.bank_number || mf.dmagent) and\n                op.org_group = g.group_2 and\n                sc.sic_code(+) = mf.sic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QS_NEW);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setInt(7,excludeMuted);
   __sJT_st.setInt(8,excludeMuted);
   __sJT_st.setInt(9,RiskThreshold);
   __sJT_st.setLong(10,RiskAnalystUserId);
   __sJT_st.setLong(11,RISK_ANALYST_ALL);
   __sJT_st.setLong(12,RiskAnalystUserId);
   __sJT_st.setInt(13,hideClearedItems);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.reports.RiskScoreDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2045^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement(new RowData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void muteDetail( int detailRecId )
  {
    int         rowCount      = 0;

    try
    {
      if ( ( detailRecId != -1 ) &&
           ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_ANALYST ) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2074^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(muted_by_user_id)    
//            from    risk_score
//            where   rec_id = :detailRecId and
//                    muted_by_user_id is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(muted_by_user_id)     \n          from    risk_score\n          where   rec_id =  :1  and\n                  muted_by_user_id is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,detailRecId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2080^9*/

        if ( rowCount == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2084^11*/

//  ************************************************************
//  #sql [Ctx] { update risk_score
//              set muted_by_user_id = :ReportUserBean.getUserId(),
//                  muted_date = sysdate
//              where rec_id = :detailRecId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1259 = ReportUserBean.getUserId();
   String theSqlTS = "update risk_score\n            set muted_by_user_id =  :1 ,\n                muted_date = sysdate\n            where rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1259);
   __sJT_st.setInt(2,detailRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2090^11*/
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("muteDetail( " + detailRecId + " )", e.toString() );
    }
    finally
    {
    }
  }

  public void muteMerchant( long merchantId )
  {
    Date              activityDate    = null;
    Calendar          cal             = Calendar.getInstance();
    long              userId          = ReportUserBean.getUserId();

    try
    {
      if ( merchantId != 0L )
      {
        if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_ANALYST ) )
        {
          cal.setTime( ReportDateBegin );

          do
          {
            activityDate = new java.sql.Date( cal.getTime().getTime() );

            /*@lineinfo:generated-code*//*@lineinfo:2121^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score_muted
//                  ( merchant_number, activity_date, muted_by_user_id, muted_date )
//                values
//                  ( :merchantId, :activityDate, :userId, trunc(sysdate) )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_score_muted\n                ( merchant_number, activity_date, muted_by_user_id, muted_date )\n              values\n                (  :1 ,  :2 ,  :3 , trunc(sysdate) )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setLong(3,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2127^13*/

            cal.add( Calendar.DAY_OF_MONTH, 1 );
            activityDate = null;
          }
          while( !cal.getTime().after( ReportDateEnd ) );
        }
        else
        {
          logEntry("muteMerchant( " + merchantId + " )",
                   "User does not have right to mute risk score");
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("muteMerchant( " + merchantId + " )", e.toString() );
    }
    finally
    {
    }
  }

  public void setProperties(HttpServletRequest request)
  {
    Calendar        cal           = Calendar.getInstance();
    int             queueOp       = 0;
    Date            sqlDate       = null;
    long            temp          = 0L;

    // load the default report properties
    super.setProperties( request );

    SortOrder         = HttpHelper.getInt(request,"sortOrder",SB_DEFAULT);
    RiskThreshold     = HttpHelper.getInt(request,"riskThreshold",RISK_THRESHOLD_DEFAULT);
    MuteEnabled       = HttpHelper.getBoolean(request,"muteEnable",true);
    HideClearedItems  = HttpHelper.getBoolean(request,"hideCleared",false);

    RiskAnalystUserId = HttpHelper.getLong(request,"analystUserId", ReportUserBean.getUserId());

    if ( ( RiskAnalystUserId == ReportUserBean.getUserId() ) &&
         !ReportUserBean.hasRight(MesUsers.RIGHT_REPORT_RISK_ANALYST) )
    {
      RiskAnalystUserId = RISK_ANALYST_ALL;
    }

    if ( usingDefaultReportDates() )
    {
      int         inc           = -1;
      int         bankNumber    = getReportBankIdDefault();

      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;
      }

      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );

//      // 3941 and all default the end date to the current date, all
//      // other banks default the end date to the previous day
//      if ( bankNumber == mesConstants.BANK_ID_STERLING )
//      {
//        cal.setTime( ReportDateEnd );
//        cal.add( Calendar.DAY_OF_MONTH, -1 );
//        setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
//      }
    }

    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }

    // assign the queue entry for this merchant (if necessary)
    assignMerchant( HttpHelper.getLong( request, "assignMerchant", 0L ),
                    HttpHelper.getDate( request, "assignDate", "MM/dd/yyyy", null ),
                    HttpHelper.getLong( request, "assignUserId", 0L ) );

    // see if the user requested this entry to be cleared
    if ( (queueOp = HttpHelper.getInt(request,"queueOp",-1)) != -1 )
    {
      // update the queue entry for this merchant
      updateRiskQueue( HttpHelper.getLong( request, "queueMerchant", 0L ),
                       HttpHelper.getDate( request, "queueDate", "MM/dd/yyyy", null ),
                       queueOp );
    }


    // check for the multi-cleared interface
    for (Enumeration names = request.getParameterNames();
         names.hasMoreElements(); )
    {
      String name = (String)names.nextElement();

      if ( name.startsWith("queueOp_") &&
           (queueOp = HttpHelper.getInt(request,name,-1)) != -1 )
      {
        updateRiskQueue( name, queueOp );
      }
    }

    // if the user had requested this row muted, then mute it
    muteMerchant( HttpHelper.getLong(request, "muteMerchant", 0L) );

    // must the detail.  if -1, muteDetail does nothing
    muteDetail( HttpHelper.getInt(request,"muteDetailId",-1) );

    // check for full note report
    FullNotes = HttpHelper.getBoolean(request,"fullNotes",false);

    temp = HttpHelper.getLong(request,"deleteId",0L);

    if ( temp > 0L )
    {
      deleteRiskAssignment(temp);
    }
  }

  public void showData( java.io.PrintStream out )
  {
  }

  public void storeNote( HttpServletRequest request )
  {
    long         merchantId          = 0L;
    int          noteType            = NT_UNKNOWN;
    String       notes               = null;
    String       otherDesc           = null;

    // use the current merchant number
    merchantId    = getReportMerchantId();

    // extract the params from the request
    noteType      = HttpHelper.getInt(request,"noteType",NT_UNKNOWN);
    notes         = HttpHelper.getString(request,"notes",null);
    otherDesc     = HttpHelper.getString(request,"otherDesc",null);

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2273^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_notes
//          (
//            merchant_number,
//            note_date,
//            type,
//            other_description,
//            user_id,
//            notes
//          )
//          values
//          (
//            :merchantId,
//            sysdate,
//            :noteType,
//            :otherDesc,
//            :ReportUserBean.getUserId(),
//            substr( :notes, 1, 1500 )
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1260 = ReportUserBean.getUserId();
   String theSqlTS = "insert into risk_notes\n        (\n          merchant_number,\n          note_date,\n          type,\n          other_description,\n          user_id,\n          notes\n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 ,\n          substr(  :5 , 1, 1500 )\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,noteType);
   __sJT_st.setString(3,otherDesc);
   __sJT_st.setLong(4,__sJT_1260);
   __sJT_st.setString(5,notes);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2293^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2295^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2298^7*/
    }
    catch(Exception e)
    {
      logEntry("storeNote( )", e.toString());
    }
    finally
    {
    }
  }

  protected void storePciData( )
  {
    int           appCount    = 0;
    int           mifCount    = 0;
    long          merchantId  = getReportHierarchyNode();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2319^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)               as mf_count,
//                  sum( decode(mr.merch_number,null,0,1) ) as mr_count
//          
//          from    mif             mf,
//                  merchant        mr
//          where   mf.merchant_number = :merchantId and
//                  mr.merch_number(+) = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)               as mf_count,\n                sum( decode(mr.merch_number,null,0,1) ) as mr_count\n         \n        from    mif             mf,\n                merchant        mr\n        where   mf.merchant_number =  :1  and\n                mr.merch_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2328^7*/

      if ( mifCount > 0 )
      {
        Date        lastScanDate      = null;
        Date        nextScanDate      = null;

        if ( appCount == 0 )
        {
          int     appType = 0;

          /*@lineinfo:generated-code*//*@lineinfo:2339^11*/

//  ************************************************************
//  #sql [Ctx] { select  oa.app_type 
//              from    t_hierarchy           th,
//                      org_app               oa,
//                      mif                   mf
//              where   th.hier_type = 1 and
//                      th.ancestor = oa.hierarchy_node and
//                      th.entity_type = 4 and
//                      th.relation =
//                      (
//                        select min(thi.relation)
//                        from   t_hierarchy thi,
//                               mif         mfi
//                        where  thi.hier_type = 1 and
//                               thi.ancestor in
//                               (
//                                  select oai.hierarchy_node
//                                  from    org_app   oai
//                               ) and
//                               thi.entity_type = 4 and
//                               mfi.association_node = thi.descendent and
//                               mfi.merchant_number = :merchantId
//                      ) and
//                      mf.association_node = th.descendent and
//                      mf.merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  oa.app_type  \n            from    t_hierarchy           th,\n                    org_app               oa,\n                    mif                   mf\n            where   th.hier_type = 1 and\n                    th.ancestor = oa.hierarchy_node and\n                    th.entity_type = 4 and\n                    th.relation =\n                    (\n                      select min(thi.relation)\n                      from   t_hierarchy thi,\n                             mif         mfi\n                      where  thi.hier_type = 1 and\n                             thi.ancestor in\n                             (\n                                select oai.hierarchy_node\n                                from    org_app   oai\n                             ) and\n                             thi.entity_type = 4 and\n                             mfi.association_node = thi.descendent and\n                             mfi.merchant_number =  :1 \n                    ) and\n                    mf.association_node = th.descendent and\n                    mf.merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2365^11*/

          // this procedure will only create the app
          // if one does not already exist
          /*@lineinfo:generated-code*//*@lineinfo:2369^11*/

//  ************************************************************
//  #sql [Ctx] { call create_shadow_app(:merchantId, :appType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN create_shadow_app( :1 ,  :2  )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,appType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2372^11*/
        }

        lastScanDate = ((DateField)fields.getField("pciLastScanDate")).getSqlDate();
        nextScanDate = ((DateField)fields.getField("pciNextScanDate")).getSqlDate();

        /*@lineinfo:generated-code*//*@lineinfo:2378^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant
//            set     risk_pci_level          = :fields.getData("pciLevel"),
//                    risk_pci_vendor_code    = :fields.getData("pciVendor"),
//                    risk_pci_vendor_other   = :fields.getData("pciVendorOther"),
//                    risk_pci_last_scan_date = :lastScanDate,
//                    risk_pci_next_scan_date = :nextScanDate,
//                    risk_pci_compliant      = :fields.getData("pciCompliant")
//            where   merch_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1261 = fields.getData("pciLevel");
 String __sJT_1262 = fields.getData("pciVendor");
 String __sJT_1263 = fields.getData("pciVendorOther");
 String __sJT_1264 = fields.getData("pciCompliant");
   String theSqlTS = "update merchant\n          set     risk_pci_level          =  :1 ,\n                  risk_pci_vendor_code    =  :2 ,\n                  risk_pci_vendor_other   =  :3 ,\n                  risk_pci_last_scan_date =  :4 ,\n                  risk_pci_next_scan_date =  :5 ,\n                  risk_pci_compliant      =  :6 \n          where   merch_number =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1261);
   __sJT_st.setString(2,__sJT_1262);
   __sJT_st.setString(3,__sJT_1263);
   __sJT_st.setDate(4,lastScanDate);
   __sJT_st.setDate(5,nextScanDate);
   __sJT_st.setString(6,__sJT_1264);
   __sJT_st.setLong(7,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2388^9*/

        /*@lineinfo:generated-code*//*@lineinfo:2390^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2393^9*/

        // store BB&T-specific PCI data
        if(getReportBankId() == 3867)
        {
          int appSeqNum   = 0;
          int pciCount    = 0;

          /*@lineinfo:generated-code*//*@lineinfo:2401^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//              
//              from    merchant
//              where   merch_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n             \n            from    merchant\n            where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2407^11*/

          /*@lineinfo:generated-code*//*@lineinfo:2409^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//              
//              from    app_merch_bbt
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n             \n            from    app_merch_bbt\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pciCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2415^11*/

          if(pciCount == 0)
          {
            // insert new record
            /*@lineinfo:generated-code*//*@lineinfo:2420^13*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_bbt
//                (
//                  app_seq_num,
//                  pci_val_required_ind,
//                  third_party_name,
//                  third_party_sw_ind,
//                  third_party_sw_name,
//                  third_party_sw_ver,
//                  payment_app_val_conf_ind,
//                  third_party_agent_ind,
//                  third_party_agent_name_1,
//                  third_party_agent_name_2,
//                  third_party_agent_desc_1,
//                  third_party_agent_desc_2
//                )
//                values
//                (
//                  :appSeqNum,
//                  :getData("pciValRequiredInd"),
//                  :getData("thirdPartyName"),
//                  :getData("thirdPartySwInd"),
//                  :getData("thirdPartySwName"),
//                  :getData("thirdPartySwVer"),
//                  :getData("paymentAppValConfInd"),
//                  :getData("thirdPartyAgentInd"),
//                  :getData("thirdPartyAgentName1"),
//                  :getData("thirdPartyAgentName2"),
//                  :getData("thirdPartyAgentDesc1"),
//                  :getData("thirdPartyAgentDesc2")
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1265 = getData("pciValRequiredInd");
 String __sJT_1266 = getData("thirdPartyName");
 String __sJT_1267 = getData("thirdPartySwInd");
 String __sJT_1268 = getData("thirdPartySwName");
 String __sJT_1269 = getData("thirdPartySwVer");
 String __sJT_1270 = getData("paymentAppValConfInd");
 String __sJT_1271 = getData("thirdPartyAgentInd");
 String __sJT_1272 = getData("thirdPartyAgentName1");
 String __sJT_1273 = getData("thirdPartyAgentName2");
 String __sJT_1274 = getData("thirdPartyAgentDesc1");
 String __sJT_1275 = getData("thirdPartyAgentDesc2");
   String theSqlTS = "insert into app_merch_bbt\n              (\n                app_seq_num,\n                pci_val_required_ind,\n                third_party_name,\n                third_party_sw_ind,\n                third_party_sw_name,\n                third_party_sw_ver,\n                payment_app_val_conf_ind,\n                third_party_agent_ind,\n                third_party_agent_name_1,\n                third_party_agent_name_2,\n                third_party_agent_desc_1,\n                third_party_agent_desc_2\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                 :9 ,\n                 :10 ,\n                 :11 ,\n                 :12 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setString(2,__sJT_1265);
   __sJT_st.setString(3,__sJT_1266);
   __sJT_st.setString(4,__sJT_1267);
   __sJT_st.setString(5,__sJT_1268);
   __sJT_st.setString(6,__sJT_1269);
   __sJT_st.setString(7,__sJT_1270);
   __sJT_st.setString(8,__sJT_1271);
   __sJT_st.setString(9,__sJT_1272);
   __sJT_st.setString(10,__sJT_1273);
   __sJT_st.setString(11,__sJT_1274);
   __sJT_st.setString(12,__sJT_1275);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2452^13*/
          }
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:2456^13*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//                set     pci_val_required_ind      = :getData("pciValRequiredInd"),
//                        third_party_name          = :getData("thirdPartyName"),
//                        third_party_sw_ind        = :getData("thirdPartySwInd"),
//                        third_party_sw_name       = :getData("thirdPartySwName"),
//                        third_party_sw_ver        = :getData("thirdPartySwVer"),
//                        payment_app_val_conf_ind  = :getData("paymentAppValConfInd"),
//                        third_party_agent_ind     = :getData("thirdPartyAgentInd"),
//                        third_party_agent_name_1  = :getData("thirdPartyAgentName1"),
//                        third_party_agent_name_2  = :getData("thirdPartyAgentName2"),
//                        third_party_agent_desc_1  = :getData("thirdPartyAgentDesc1"),
//                        third_party_agent_desc_2  = :getData("thirdPartyAgentDesc2")
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1276 = getData("pciValRequiredInd");
 String __sJT_1277 = getData("thirdPartyName");
 String __sJT_1278 = getData("thirdPartySwInd");
 String __sJT_1279 = getData("thirdPartySwName");
 String __sJT_1280 = getData("thirdPartySwVer");
 String __sJT_1281 = getData("paymentAppValConfInd");
 String __sJT_1282 = getData("thirdPartyAgentInd");
 String __sJT_1283 = getData("thirdPartyAgentName1");
 String __sJT_1284 = getData("thirdPartyAgentName2");
 String __sJT_1285 = getData("thirdPartyAgentDesc1");
 String __sJT_1286 = getData("thirdPartyAgentDesc2");
   String theSqlTS = "update  app_merch_bbt\n              set     pci_val_required_ind      =  :1 ,\n                      third_party_name          =  :2 ,\n                      third_party_sw_ind        =  :3 ,\n                      third_party_sw_name       =  :4 ,\n                      third_party_sw_ver        =  :5 ,\n                      payment_app_val_conf_ind  =  :6 ,\n                      third_party_agent_ind     =  :7 ,\n                      third_party_agent_name_1  =  :8 ,\n                      third_party_agent_name_2  =  :9 ,\n                      third_party_agent_desc_1  =  :10 ,\n                      third_party_agent_desc_2  =  :11 \n              where   app_seq_num =  :12";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1276);
   __sJT_st.setString(2,__sJT_1277);
   __sJT_st.setString(3,__sJT_1278);
   __sJT_st.setString(4,__sJT_1279);
   __sJT_st.setString(5,__sJT_1280);
   __sJT_st.setString(6,__sJT_1281);
   __sJT_st.setString(7,__sJT_1282);
   __sJT_st.setString(8,__sJT_1283);
   __sJT_st.setString(9,__sJT_1284);
   __sJT_st.setString(10,__sJT_1285);
   __sJT_st.setString(11,__sJT_1286);
   __sJT_st.setInt(12,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2471^13*/
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("storePciData()",e.toString());
    }
    finally
    {
      cleanUp();
      //reload so as to include non-field display data
      autoLoad();
    }
  }

  public void updateRiskQueue( String recIdStr, int queueOp )
  {
    java.util.Date    clearDate   = null;
    int               curStatus   = -1;
    SimpleDateFormat  dateFormat  = new SimpleDateFormat( "MMddyyyy" );
    long              merchantId  = 0L;

    try
    {
      //
      // recIdString is queueOp_SSMMDDYYYY<merchant id>
      //
      // SS             = current status code padded to 2 chars
      // MMDDYYYY       = activity date
      // <merchant id>  = Merchant account number
      //
      //           012345678901234567890123456789
      //  Sample:  queueOp_0006012003941000000002
      //
      curStatus   = Integer.parseInt( recIdStr.substring(8,10) );
      clearDate   = dateFormat.parse( recIdStr.substring(10,18 ) );
      merchantId  = Long.parseLong( recIdStr.substring(18) );

      // ignore unless the status has changed
      if ( curStatus != queueOp )
      {
        updateRiskQueue( merchantId, clearDate, queueOp );
      }
    }
    catch( Exception e )
    {
      logEntry( "updateRiskQueue( " + recIdStr + ", " + queueOp + " )", e.toString() );
    }
  }

  public void updateRiskQueue( long merchantId, java.util.Date clearDate, int statusCode )
  {
    Date              activityDate    = null;
    long              assignedUserId  = 0L;
    int               rowCount        = 0;
    long              userId          = ReportUserBean.getUserId();

    try
    {
      if ( hasQueueOpRight( statusCode ) )
      {
        activityDate = new java.sql.Date( clearDate.getTime() );

        /*@lineinfo:generated-code*//*@lineinfo:2536^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( merchant_number ) 
//            from    risk_queue    rq
//            where   rq.merchant_number = :merchantId and
//                    rq.activity_date = :activityDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( merchant_number )  \n          from    risk_queue    rq\n          where   rq.merchant_number =  :1  and\n                  rq.activity_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2542^9*/

        if ( rowCount == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2546^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_queue
//              (
//                merchant_number,
//                activity_date,
//                user_id,
//                queue_status,
//                queue_status_date,
//                queue_last_user_id
//              )
//              values
//              (
//                :merchantId,
//                :activityDate,
//                :userId,
//                :statusCode,
//                sysdate,
//                :ReportUserBean.getUserId()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1287 = ReportUserBean.getUserId();
   String theSqlTS = "insert into risk_queue\n            (\n              merchant_number,\n              activity_date,\n              user_id,\n              queue_status,\n              queue_status_date,\n              queue_last_user_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n              sysdate,\n               :5 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setLong(3,userId);
   __sJT_st.setInt(4,statusCode);
   __sJT_st.setLong(5,__sJT_1287);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2566^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2570^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(user_id,:RISK_ANALYST_UNASSIGNED) 
//              from    risk_queue    rq
//              where   rq.merchant_number = :merchantId and
//                      rq.activity_date = :activityDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(user_id, :1 )  \n            from    risk_queue    rq\n            where   rq.merchant_number =  :2  and\n                    rq.activity_date =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.reports.RiskScoreDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,RISK_ANALYST_UNASSIGNED);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   assignedUserId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2576^11*/

          // cannot clear an entry assigned to another user unless
          // you are a queue admin.
          if ( assignedUserId == userId ||
               assignedUserId == RISK_ANALYST_UNASSIGNED ||
               ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN ) )
          {
            // if there is no user assigned to this queue entry
            // and it is being cleared, then assign the current
            // user to the queue entry.
            if ( ( assignedUserId == RISK_ANALYST_UNASSIGNED ) &&
                 ( statusCode == QS_CLEARED ) )
            {
              assignedUserId = ReportUserBean.getUserId();
            }

            /*@lineinfo:generated-code*//*@lineinfo:2593^13*/

//  ************************************************************
//  #sql [Ctx] { update risk_queue
//                set user_id = :assignedUserId,
//                    queue_status = :statusCode,
//                    queue_status_date = sysdate,
//                    queue_last_user_id = :ReportUserBean.getUserId()
//                where activity_date = :activityDate and
//                      merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1288 = ReportUserBean.getUserId();
   String theSqlTS = "update risk_queue\n              set user_id =  :1 ,\n                  queue_status =  :2 ,\n                  queue_status_date = sysdate,\n                  queue_last_user_id =  :3 \n              where activity_date =  :4  and\n                    merchant_number =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.reports.RiskScoreDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,assignedUserId);
   __sJT_st.setInt(2,statusCode);
   __sJT_st.setLong(3,__sJT_1288);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setLong(5,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2602^13*/
          }
          else
          {
            addError( "User does not have rights to update accounts assigned to other users." );
          }
        }
      }
      else
      {
        addError( "User does not have rights to update risk queue status" );
        logEntry("updateRiskQueue( " + merchantId + " )",
                 "User does not have rights to update risk queue status.");
      }
    }
    catch( Exception e )
    {
      addError("Queue update failed: " + e.toString() );
      logEntry("updateRiskQueue( " + merchantId + ", " + activityDate + " )", e.toString() );
    }
    finally
    {
    }
  }

  //hijacked - need this for simplicity in PCI field description
  public String renderHtml(String fieldName)
  {
    if(fieldName.equals("pciQuizResults"))
    {
      PCIHistory history;
      StringBuffer sb = new StringBuffer();
      for(Iterator i = pciHistoryList.iterator();i.hasNext();)
      {
        history = (PCIHistory)i.next();
        sb.append(history.toString()).append("<br>");
      }
      //ditch the last <br>
      if(sb.length()>4)
      {
        sb.setLength(sb.length()-4);
      }
      return sb.toString();
    }
    else if(fieldName.equals("pciLevelRec"))
    {
      return ""+merchLevel;
    }
    else if(fieldName.equals("pciLevelTrans"))
    {
      return ""+ transactionCount;
    }
    else if(fieldName.equals("pciName"))
    {
      return pciName;
    }
    else if(fieldName.equals("pciEmail"))
    {
      return pciEmail;
    }
    else if(fieldName.equals("pciPhone"))
    {
      return pciPhone;
    }
    else
    {
      return super.renderHtml(fieldName);
    }
  }
}/*@lineinfo:generated-code*/