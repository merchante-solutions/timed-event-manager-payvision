/*************************************************************************

  FILE: $URL: $

  Description:  
  
  TCUserManager
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.b2f;

import java.util.Hashtable;

public class B2FSessionCache
{
  public B2FSessionCache()
  {
  }
  
  public String resetid(Object unused, String unused2, String loginName, String activationCode)
  {
    String result = "";
    
    return( result );
  }
  
  public void deleteid(Object unused, String unsused2, String loginName)
  {
  }
  
  public CacheInfo getCachedSession(String realm, 
                                    String mgrId, 
                                    String mgrPwd, 
                                    boolean unused, 
                                    String ipaddr,
                                    String port)
  {
    return( new CacheInfo() );
  }
  
  public CacheInfo getCachedSession()
  {
    return( new CacheInfo() );
  }
  
  public Hashtable createid(CacheInfo cache, String realm, String loginName, Hashtable ht)
  {
    // doesn't need to do anything
    return( null );
  }
  
  public class CacheInfo
  {
    public CacheInfo()
    {
    }
    
    public String getAppliance()
    {
      return( "" );
    }
    
    public String getPort()
    {
      return( "" );
    }
  }
}