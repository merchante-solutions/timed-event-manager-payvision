/*************************************************************************

  FILE: $URL: $

  Description:  
  
  TCUserManager
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.b2f;

public class B2FConfiguration
{
  public B2FConfiguration()
  {
  }
  
  public String getCsrManagerUserId()
  {
    return( "" );
  }
  
  public String getCsrManagerPassword()
  {
    return( "" );
  }
}