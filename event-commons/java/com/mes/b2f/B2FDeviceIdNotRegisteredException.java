package com.mes.b2f;

public class B2FDeviceIdNotRegisteredException extends Exception
{
  public B2FDeviceIdNotRegisteredException(String message)
  {
    super(message);
  }
}
