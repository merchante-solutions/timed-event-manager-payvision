/*@lineinfo:filename=B2F*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:  
  
  TCUserManager
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.b2f;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class B2F extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(B2F.class);
  
  private B2FSessionCache           cache     = null;
  private B2FSessionCache.CacheInfo cacheInfo = null;
  
  public B2F()
  {
    super();
    
    setAutoCommit( false );
    
    cache = new B2FSessionCache();
    cacheInfo = cache.getCachedSession();
  }
  
  public B2FData getUserInformation(String realm, String loginName)
    throws B2FApplianceException
  {
    B2FData           data  = null;
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rtrim(dukpt_decrypt_wrapper(ub.welcome_msg))   welcome_msg,
//                  rtrim(dukpt_decrypt_wrapper(ub.question_1))    question_1,
//                  rtrim(dukpt_decrypt_wrapper(ub.question_2))    question_2,
//                  rtrim(dukpt_decrypt_wrapper(ub.question_3))    question_3,
//                  rtrim(dukpt_decrypt_wrapper(ub.answer_1))      answer_1,
//                  rtrim(dukpt_decrypt_wrapper(ub.answer_2))      answer_2,
//                  rtrim(dukpt_decrypt_wrapper(ub.answer_3))      answer_3
//          from    users u,
//                  users_b2f ub
//          where   u.login_name = :loginName and
//                  u.user_id = ub.user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rtrim(dukpt_decrypt_wrapper(ub.welcome_msg))   welcome_msg,\n                rtrim(dukpt_decrypt_wrapper(ub.question_1))    question_1,\n                rtrim(dukpt_decrypt_wrapper(ub.question_2))    question_2,\n                rtrim(dukpt_decrypt_wrapper(ub.question_3))    question_3,\n                rtrim(dukpt_decrypt_wrapper(ub.answer_1))      answer_1,\n                rtrim(dukpt_decrypt_wrapper(ub.answer_2))      answer_2,\n                rtrim(dukpt_decrypt_wrapper(ub.answer_3))      answer_3\n        from    users u,\n                users_b2f ub\n        where   u.login_name =  :1  and\n                u.user_id = ub.user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.b2f.B2F",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        data = new B2FData(rs);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getUserInformation(" + loginName + ")", e.toString());
      data = null;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    if( data == null )
    {
      throw new B2FApplianceException("Unable to retrieve user information (" + loginName + ")");
    }
    
    return( data );
  }
  
  private String getRequestIPVal(HttpServletRequest request)
  {
    String retVal = "GSgasdgasgLIGJUSTLIGSGSAG";
    
    try
    {
      String ipVal = request.getHeader("WL-Proxy-Client-IP");
    
      if(ipVal == null || ipVal.equals(""))
      {
        ipVal = request.getHeader("Proxy-Client-IP");
      
      }
  
      if( ipVal == null || ipVal.equals("") )
      {
        // just get some valid value from the request
        ipVal = request.getRequestedSessionId();
      }
    
      if( ipVal == null || ipVal.equals("") )
      {
        // default to horsecrap
        ipVal = "GSgasdgasgLIGJUSTLIGSGSAG";
      }
      
      if( ipVal != null && ! ipVal.equals("") )
      {
        retVal = ipVal;
      }
    }
    catch(Exception e)
    {
      retVal = "GSgasdgasgLIGJUSTLIGSGSAG";
    }
    
    return( retVal );
    
  }
  
  public B2FData validate(String realm, String loginName, Cookie[] cookies, HttpServletRequest request)
    throws B2FDeviceIdNotRegisteredException
  {
    boolean cookieFound = false;
    
    try
    {
      connect();
      
      String cookieName = "";
      
      // get cookie name and user data
      /*@lineinfo:generated-code*//*@lineinfo:167^7*/

//  ************************************************************
//  #sql [Ctx] { select  'MESB2F_'||encrypt_password(:loginName)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  'MESB2F_'||encrypt_password( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.b2f.B2F",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cookieName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^7*/
      
      // find cookie with this name
      if( cookies != null )
      {
        for(int i=0; i<cookies.length; ++i)
        {
          Cookie c = cookies[i];
          
          if( c.getName().equals(cookieName) )
          {
            // found cookie - check value
            String ipVal = getRequestIPVal(request);
            
            int recCount = 0;
            /*@lineinfo:generated-code*//*@lineinfo:187^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_id)
//                
//                from    users_b2f_cookies
//                where   user_id in (select user_id from users where login_name = :loginName) and
//                        cookie_val = encrypt_password(:ipVal)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_id)\n               \n              from    users_b2f_cookies\n              where   user_id in (select user_id from users where login_name =  :1 ) and\n                      cookie_val = encrypt_password( :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.b2f.B2F",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,ipVal);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^13*/
          
            if( recCount > 0 )
            {
              cookieFound = true;
              break;
            }
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("validate(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    if( cookieFound == false )
    {
      throw new B2FDeviceIdNotRegisteredException("Device not found in cookie array");
    }
    
    return( null );
  }
  
  private boolean checkPassword(String password)
  {
    boolean retVal = false;
    
    // check length
    if(password.length() >= 8)
    {
      boolean hasUpperCase  = false;
      boolean hasLowerCase  = false;
      boolean hasDigit      = false;
    
      for(int i=0; i < password.length(); ++i)
      {
        if( Character.isDigit(password.charAt(i)) )
        {
          hasDigit = true;
        }
      
        if( Character.isUpperCase(password.charAt(i)) )
        {
          hasUpperCase = true;
        }
      
        if( Character.isLowerCase(password.charAt(i)) )
        {
          hasLowerCase = true;
        }
      }
    
      retVal = (hasUpperCase && hasLowerCase && hasDigit);
    }
    
    
    return( retVal );
  }
  
  public void activateUser( String realm,
                            String loginName,
                            String activationCode,
                            String password,
                            boolean unused )
    throws B2FUserException
  {
    boolean passwordOk = true;
    
    // if password is ok then activate this user
    if(password == null)
    {
      passwordOk = false;
    }
    else
    {
      passwordOk = checkPassword(password);
    }
    
    if(passwordOk)
    {
      try
      {
        connect();
        
        // remove any existing entries in users_b2f
        /*@lineinfo:generated-code*//*@lineinfo:284^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    users_b2f
//            where   user_id in (select user_id from users where login_name = :loginName)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    users_b2f\n          where   user_id in (select user_id from users where login_name =  :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^9*/
        
        commit();
        
        // insert new record for this user
        /*@lineinfo:generated-code*//*@lineinfo:294^9*/

//  ************************************************************
//  #sql [Ctx] { insert into users_b2f
//            (
//              user_id
//            )
//            select user_id from users where login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into users_b2f\n          (\n            user_id\n          )\n          select user_id from users where login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:301^9*/
        
        // update user password
        /*@lineinfo:generated-code*//*@lineinfo:304^9*/

//  ************************************************************
//  #sql [Ctx] { update  users
//            set     password_enc = encrypt_password(:password)
//            where   login_name = :loginName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n          set     password_enc = encrypt_password( :1 )\n          where   login_name =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,password);
   __sJT_st.setString(2,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:309^9*/
        
        commit();
      }
      catch(Exception e)
      {
        logEntry("activateUser(" + loginName + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    else
    {
      throw new B2FUserException("Password strength test failed");
    }
  }
  
  private String encryptPad(String val)
  {
    StringBuffer result = new StringBuffer(val);
    
    for(int i=0; i<10-val.length(); ++i)
    {
      result.append(" ");
    }
    
    return( result.toString() );
  }
  
  public void setWelcomeData( String realm,
                              String loginName,
                              String password,
                              String welcomeMsg,
                              byte[] welcomeImg,
                              String imgType )
  {
    try
    {
      connect();
      
      // update users_b2f to include new welcome message
      /*@lineinfo:generated-code*//*@lineinfo:352^7*/

//  ************************************************************
//  #sql [Ctx] { update  users_b2f
//          set     welcome_msg = dukpt_encrypt_wrapper( :encryptPad(welcomeMsg) )
//          where   user_id in (select user_id from users where login_name = :loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_73 = encryptPad(welcomeMsg);
   String theSqlTS = "update  users_b2f\n        set     welcome_msg = dukpt_encrypt_wrapper(  :1  )\n        where   user_id in (select user_id from users where login_name =  :2 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_73);
   __sJT_st.setString(2,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("setWelcomeData(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setChallenge( String realm,
                            String loginName,
                            String password,
                            Hashtable challenges )
    throws Exception
  {
    String[]  questions = new String[3];
    String[]  answers   = new String[3];
    try
    {
      connect();
      
      // update users_b2f with challenge questions and answers
      int i = 0;
      for(Enumeration e = challenges.keys(); e.hasMoreElements();)
      {
        String question = (String)(e.nextElement());
        questions[i]    = question;
        answers[i]      = (String)(challenges.get(question));
        ++i;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:393^7*/

//  ************************************************************
//  #sql [Ctx] { update  users_b2f
//          set     question_1 = dukpt_encrypt_wrapper( :encryptPad(questions[0]) ),
//                  question_2 = dukpt_encrypt_wrapper( :encryptPad(questions[1]) ),
//                  question_3 = dukpt_encrypt_wrapper( :encryptPad(questions[2]) ),
//                  answer_1 = dukpt_encrypt_wrapper( :encryptPad(answers[0]) ),
//                  answer_2 = dukpt_encrypt_wrapper( :encryptPad(answers[1]) ),
//                  answer_3 = dukpt_encrypt_wrapper( :encryptPad(answers[2]) )
//          where   user_id in (select user_id from users where login_name = :loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_74 = encryptPad(questions[0]);
 String __sJT_75 = encryptPad(questions[1]);
 String __sJT_76 = encryptPad(questions[2]);
 String __sJT_77 = encryptPad(answers[0]);
 String __sJT_78 = encryptPad(answers[1]);
 String __sJT_79 = encryptPad(answers[2]);
   String theSqlTS = "update  users_b2f\n        set     question_1 = dukpt_encrypt_wrapper(  :1  ),\n                question_2 = dukpt_encrypt_wrapper(  :2  ),\n                question_3 = dukpt_encrypt_wrapper(  :3  ),\n                answer_1 = dukpt_encrypt_wrapper(  :4  ),\n                answer_2 = dukpt_encrypt_wrapper(  :5  ),\n                answer_3 = dukpt_encrypt_wrapper(  :6  )\n        where   user_id in (select user_id from users where login_name =  :7 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_74);
   __sJT_st.setString(2,__sJT_75);
   __sJT_st.setString(3,__sJT_76);
   __sJT_st.setString(4,__sJT_77);
   __sJT_st.setString(5,__sJT_78);
   __sJT_st.setString(6,__sJT_79);
   __sJT_st.setString(7,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:403^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("setChallenge(" + loginName + ")", e.toString());
      throw(new Exception("Error persisting data to database: " + e.toString()));
    }
    finally
    {
      cleanUp();
    }
  }
  
  public Cookie[] registerDevice( String realm,
                              String loginName,
                              String password,
                              long registerTime,
                              String blah,
                              Cookie[] existingCookies,
                              HttpServletRequest request )
    throws B2FUserException
  {
    Cookie[] retVal = new Cookie[1];
    
    try
    {
      connect();
      
      // determine int value of register time
      int expireTime = (int) (registerTime);
      
      String cookieName = "";
      String cookieVal  = "";
      
      String ipVal      = getRequestIPVal(request);
      
      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] { select  'MESB2F_'||encrypt_password(:loginName),
//                  encrypt_password(:ipVal)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  'MESB2F_'||encrypt_password( :1 ),\n                encrypt_password( :2 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.b2f.B2F",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,ipVal);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cookieName = (String)__sJT_rs.getString(1);
   cookieVal = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^7*/
      
      // see if cookie exists in array
      boolean cookieFound = false;
      
      if(existingCookies != null)
      {
        for(int i=0; i<existingCookies.length; ++i)
        {
          Cookie c = existingCookies[i];
          
          if( c.getName().equals(cookieName) && 
              c.getValue().equals(cookieVal) )
          {
            cookieFound = true;
            
            // reset age of cookie
            c.setMaxAge(expireTime);
            
            // establish this cookie as the return value
            retVal[0] = c;
            
            break;
          }
        }
      }
      
      if(! cookieFound )
      {
        retVal[0] = new Cookie(cookieName, cookieVal);
        
        retVal[0].setPath("/jsp/secure/b2f");
        retVal[0].setMaxAge(expireTime);
        retVal[0].setVersion(0);
      }
        
      // insert cookie into database for future retrieval
      /*@lineinfo:generated-code*//*@lineinfo:485^7*/

//  ************************************************************
//  #sql [Ctx] { insert into users_b2f_cookies
//          (
//            user_id,
//            cookie_val
//          )
//          select  u.user_id, encrypt_password(:ipVal)
//          from    users u
//          where   u.login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into users_b2f_cookies\n        (\n          user_id,\n          cookie_val\n        )\n        select  u.user_id, encrypt_password( :1 )\n        from    users u\n        where   u.login_name =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ipVal);
   __sJT_st.setString(2,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^7*/
      
      commit();
    }
    catch(java.sql.SQLException se)
    {
      if(se.getErrorCode() == 1)
      {
        // cookie already exists, no big deal
      }
      else
      {
        throw new B2FUserException(se.toString());
      }
    }
    catch(Exception e)
    {
      logEntry("registerDevice(" + loginName + ")", e.toString());
      throw new B2FUserException(e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( retVal );
  }
  
  public void resetUser(String loginName)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:529^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    users_b2f
//          where   user_id in (select user_id from users where login_name = :loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    users_b2f\n        where   user_id in (select user_id from users where login_name =  :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:534^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:536^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    users_b2f_cookies
//          where   user_id in (select user_id from users where login_name = :loginName)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    users_b2f_cookies\n        where   user_id in (select user_id from users where login_name =  :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.b2f.B2F",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("resetUser(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public Cookie[] deleteCookies(String realm,
                                String loginName,
                                Cookie[] existingCookies )
  {
    return( null );
  }
  
  public void login(String realm,
                    String loginName,
                    String password,
                    Cookie[] cookies,
                    Object blah)
    throws B2FUserException
  {
    boolean pass = false;
    try
    {
      // validate that password matches 
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:577^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//          
//          from    users
//          where   login_name = :loginName and
//                  password_enc = encrypt_password(:password)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n         \n        from    users\n        where   login_name =  :1  and\n                password_enc = encrypt_password( :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.b2f.B2F",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,password);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:584^7*/
      
      pass = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("login(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    if(pass == false)
    {
      throw new B2FUserException("Incorrect Password");
    }
  }
  
  public B2FSessionCache getSessionCache()
  {
    return( cache );
  }
  
  public B2FSessionCache.CacheInfo getB2fManager()
  {
    return( cacheInfo );
  }
  
  public B2FConfiguration getConfig()
  {
    return( new B2FConfiguration() );
  }
  
  public static boolean checkStrongPassword(String password)
  {
    B2F b2f = new B2F();
    
    return( b2f.checkPassword(password) );
  }
  
  public static void main(String[] args)
  {
    B2F work = new B2F();
    
    System.out.println("->"+work.encryptPad(args[0])+"<-");
  }
}/*@lineinfo:generated-code*/