/*@lineinfo:filename=TimedEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/events/TimedEvent.sqlj $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.events;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.logging.TELogger;
import com.mes.net.MailMessage;
import com.mes.support.LoggingConfigurator;
import com.mes.support.MesMath;
import com.mes.support.PropertiesFile;

public class TimedEvent extends SQLJConnectionBase
  implements java.io.Serializable
{
  static { LoggingConfigurator.configure(); }
  static Logger log = Logger.getLogger(TimedEvent.class);
  
  // establish constant serialVersionUID to avoid problems with mismatched versions
  static final long serialVersionUID    = 16L;
  
  // Event types
  public static final int       EVENT_FREQUENTLY        = 1;
  public static final int       EVENT_DAILY             = 2;
  public static final int       EVENT_MONTHLY           = 3;

  public static final long      MINUTE_MILLIS           = 60000;

  public static final int       TE_STATUS_IDLE          = 0;
  public static final int       TE_STATUS_RUNNING       = 1;
  
  public static String[]    eventStatuses = new String[] {"IDLE", "RUNNING"};
  
  public  String            className;
  public  int               dayOfMonth;
  public  String            daysOfWeek;
  public  String            devName;
  public  boolean           enabled;
  public  int               endTime;
  public  String            eventArgs;
  public  boolean           eventMonitored    = false;
  private EventRunner       eventThread;
  public  int               frequencyMinutes;
  public  int               id;
  public  Timestamp         lastAttempt;
  private long              lastEmailTS       = 0L;
  public  Timestamp         lastTriggered;
  private boolean           modified          = false;
  public  String            monthsOfYear;
  public  String            name;
  public  long              processStartTS;
  public  int               startTime;
  public  int               status;
  public  int               timeoutMinutes;
  public  int               type;
  public  String            eventDesc;
  protected PropertiesFile  eventProps    = null;
  public  String			evtPID 		  = null;
  
  public TimedEvent(ResultSet rs, PropertiesFile props)
  {
    eventProps = props;
    setData(rs);
    
    status      = TE_STATUS_IDLE;
  }
  
   /*
    * PID should be unique for a process, so it is generated using the event name, event id and the timestamp
    */   
   private String getProcId(){
	   return devName+"."+id+"."+new Date().getTime(); 
   }
   
  
  public boolean eventModified()
  {
    return( modified );
  }
  
  public void setData( ResultSet rs )
  {
    try
    {
      id                = rs.getInt("event_id");
      type              = rs.getInt("event_type");
      name              = rs.getString("event_name");
      enabled           = rs.getString("event_enabled").toUpperCase().equals("Y");
      className         = rs.getString("event_classname");
      lastTriggered     = rs.getTimestamp("last_triggered");
      lastAttempt       = rs.getTimestamp("last_triggered");
      startTime         = rs.getInt("start_time");
      endTime           = rs.getInt("end_time");
      frequencyMinutes  = rs.getInt("frequency_minutes");
      daysOfWeek        = rs.getString("days_of_week");
      dayOfMonth        = rs.getInt("day_of_month");
      monthsOfYear      = rs.getString("months_of_year");
      devName           = rs.getString("dev_name");
      timeoutMinutes    = rs.getInt("timeout_minutes");
      eventArgs         = rs.getString("event_args");
      eventDesc         = rs.getString("event_desc");   
      //eventMonitored    = "Y".equals(rs.getString("monitor"));
    }
    catch(Exception e)
    {	                                 
      TELogger.logTEError(this, "Error in setData()" + e.getMessage());
      //log.error("Error in setData()",e);
    }
  }
  
  /*
  ** public boolean pastTimeToProcess
  **
  ** Determies whether this event has been spinning its wheels
  */
  public Calendar pastTimeToProcess( )
  {
    boolean       result          = false;
    Calendar      curDate         = null;
    Calendar      lastDate        = null;
    
    try
    {
      connect();
      
      // check to see if database indicates that this event is due to fire
      if( timeToProcess() )
      {
        // if so then check to see how long it has been since it processed.
        Timestamp curDBTime;
        /*@lineinfo:generated-code*//*@lineinfo:162^9*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate \n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.events.TimedEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curDBTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^9*/
    
        curDate = Calendar.getInstance();
        curDate.setTime(curDBTime);
        lastDate = Calendar.getInstance();
        lastDate.setTime(lastAttempt);
        
        long  diffMinuteMillis = curDate.getTime().getTime() - lastDate.getTime().getTime();
        long  frequencyMinuteMillis = frequencyMinutes * MINUTE_MILLIS;
        
        // threshold depends on issue type
        int weedsMultiplier = MesDefaults.getInt(MesDefaults.DK_TE_WEEDS_MULTIPLIER);
        int weedsAddon      = MesDefaults.getInt(MesDefaults.DK_TE_WEEDS_ADDON);
        switch( type )
        {
          case EVENT_FREQUENTLY:
            // if this event is still running (frequencyMinutes*2) + 10 then it is in the weeds
            if( diffMinuteMillis > ((frequencyMinuteMillis * weedsMultiplier) + (weedsAddon * MINUTE_MILLIS)) )
            {
              result = true;
            }
            break;
            
          case EVENT_DAILY:
            break;
      
          case EVENT_MONTHLY:
            break;
        
          default:
            result = false;
            break;
        }
        
      }
    }
    catch(Exception e)
    {
    	TELogger.logTEError(this, "Error in pastTimeToProcess()" + e.getMessage());
      //log.error("Error in pastTimeToProcess()",e);
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    if( result == true )
    {
      return( curDate );
    }
    else
    {
      return( null );
    }
  }
  
  public boolean timeToProcess( )
  {
    boolean       result          = true;
    Calendar      curDate         = null;
    Calendar      lastDate        = null;
  
    try
    {
      connect();
      
      // get db time
      Timestamp curDBTime;
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.events.TimedEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curDBTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/
    
      curDate = Calendar.getInstance();
      curDate.setTime(curDBTime);
      lastDate = Calendar.getInstance();
      lastDate.setTime(lastAttempt);
    
      // days and months
      int curDayOfWeek    = curDate.get(Calendar.DAY_OF_WEEK);
      int curDayOfMonth   = curDate.get(Calendar.DAY_OF_MONTH);
      int curMonthOfYear  = curDate.get(Calendar.MONTH);
      int curDayOfYear    = curDate.get(Calendar.DAY_OF_YEAR);
    
      int lastDayOfWeek   = lastDate.get(Calendar.DAY_OF_WEEK);
      int lastDayOfMonth  = lastDate.get(Calendar.DAY_OF_MONTH);
      int lastMonthOfYear = lastDate.get(Calendar.MONTH);
      int lastDayOfYear   = lastDate.get(Calendar.DAY_OF_YEAR);
    
      // minutes
      long lastTime = lastDate.getTime().getTime();
      long curTime = curDate.getTime().getTime();
    
      // hours
      int  curHour = curDate.get(Calendar.HOUR_OF_DAY) * 100;
      curHour += curDate.get(Calendar.MINUTE);
    
      int lastHour = lastDate.get(Calendar.HOUR_OF_DAY) * 100;
      lastHour += lastDate.get(Calendar.MINUTE);
    
      // check day of week or month
      switch(type)
      {
        case EVENT_FREQUENTLY:
        case EVENT_DAILY:
          if(daysOfWeek.charAt(curDayOfWeek - 1) != 'Y')
          {
            result = false;
          }
          break;
      
        case EVENT_MONTHLY:
          if(monthsOfYear.charAt(curMonthOfYear) != 'Y' ||
             dayOfMonth != curDayOfMonth - 1)
          {
            result = false;
          }
          break;
        
        default:
          result = false;
          break;
      }
    
      if(result)
      {
        switch(type)
        {
          case EVENT_FREQUENTLY:
            // check to see if has been at least frequency minutes since last trigger
            long diffTime = curTime - lastTime;
          
            if( diffTime < (frequencyMinutes * MINUTE_MILLIS) || 
                curHour < startTime ||
                curHour > endTime)
            {
              result = false;
            }
            break;
          
          case EVENT_DAILY:
            if(curHour < startTime || curHour > endTime)
            {
              // not time yet today
              result = false;
            }
            else if(curDayOfYear == lastDayOfYear && lastHour >= startTime)
            {
              // already processed today
              result = false;
            }
            break;
          
          case EVENT_MONTHLY:
            if(curHour < startTime)
            {
              result = false;
            }
            else if(curDayOfMonth == lastDayOfMonth && curMonthOfYear == lastMonthOfYear)
            {
              // already processed today
              result = false;
            }
            break;
          
          default:
            result = false;
            break;
        }
      }
    }
    catch(Exception e)
    {
    	TELogger.logTEError(this, "Error in timeToProcess()" + e.getMessage());
      //log.error("Error in timeToProcess()",e);
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    if(result)
    {
      // show why this guy is processing
      //@System.out.println("\n\nlast: " + DateTimeFormatter.getFormattedDate(lastDate.getTime(), "hh:mm:ss"));
      //@System.out.println("cur:  " + DateTimeFormatter.getFormattedDate(curDate.getTime(), "hh:mm:ss"));
    }
    
    return( result );
  }
  
  public void process()
  {
    // make sure that this event isn't already trying to execute
    try
    {
      connect();
      
      if(status == TE_STATUS_IDLE || eventThread == null)
      {
        // get a timestamp of the thread start time
        processStartTS = System.currentTimeMillis();
      
        // change status to reflect what is happening
        status = TE_STATUS_RUNNING;
      
        // update the lastAttempt so that we don't unnecessarily get back into this function
        /*@lineinfo:generated-code*//*@lineinfo:378^9*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.events.TimedEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastAttempt = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^9*/
      
        // create the event thread
        this.evtPID = getProcId();
        eventThread = new EventRunner(this);
      
        eventThread.start();
      }
      else  // thread is still running
      {
        //@System.out.println("Event " + name + " is already running...");
        if (  timeoutMinutes > 0 &&
             (System.currentTimeMillis() - processStartTS) > (timeoutMinutes * MINUTE_MILLIS) )
        {
          // only send the warning message once per hour
          if ( (System.currentTimeMillis() - lastEmailTS) > (60 * MINUTE_MILLIS) ) 
          {
            StringBuffer  body      = new StringBuffer();
            boolean       isAlive   = ((eventThread==null) ? false : eventThread.isAlive());
            MailMessage   msg       = new MailMessage();
      
            body.setLength(0);
            body.append(name);
            body.append(" appears to have stalled.\n\n");
            body.append("Thread Information:\n");
            body.append("  Time Running: ");
            body.append( MesMath.round(((System.currentTimeMillis() - processStartTS)/MINUTE_MILLIS),2) );
            body.append(" minutes\n");
            body.append("  Is Alive    : ");
            body.append( isAlive );
            if ( isAlive == false )
            {
              body.append(" (forcing reset)");
              eventThread = null;     // allow another to be created
            }
            body.append("\n");
      
            msg.setAddresses(MesEmails.MSG_ADDRS_TIMED_EVENT_MGR);
            msg.setSubject("Event Failure: " + name);
            msg.setText( body.toString() );
            msg.send();
            
            lastEmailTS = System.currentTimeMillis();
          }            
        }
      }
    }
    catch(Exception e)
    {
    	TELogger.logTEError(this, "Error in process()" + e.getMessage());
      //log.error("Error in process()",e);
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setEventModified( boolean newValue )
  {
    modified = newValue;
  }
  
  public void setTriggered()
  {
    try
    {
      connect();
      
      // update this item in memory
      /*@lineinfo:generated-code*//*@lineinfo:453^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.events.TimedEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastTriggered = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:458^7*/
      
      lastAttempt = lastTriggered;
      
      // update database
      /*@lineinfo:generated-code*//*@lineinfo:463^7*/

//  ************************************************************
//  #sql [Ctx] { update  timed_events
//          set     last_triggered = :lastTriggered
//          where   event_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  timed_events\n        set     last_triggered =  :1 \n        where   event_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.events.TimedEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,lastTriggered);
   __sJT_st.setInt(2,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:470^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:473^7*/
    }
    catch(Exception e)
    {
    	TELogger.logTEError(this, "Error in setTriggered()" + e.getMessage());
      //log.error("Error in setTriggered() (className: " + className + ")",e);
    }
    finally
    {
      // close the db connection
      cleanUp();
    
      // this thread is still idle 
      // even if db updates fail
      status = TE_STATUS_IDLE;
    }
  }
  
  public class EventRunner extends Thread
    implements java.io.Serializable
  {
    private   TimedEvent      te = null;
    
    public EventRunner(TimedEvent event)
    {
      // establish a local reference to the event that is starting this thread
      te = event;
    }
    
    public void run()
    {
    	Date evtStartTime = null;
      try
      {
        Event event = null;
        event = (Event)Class.forName(te.className).newInstance();
                
        event.setEventArgs(te.eventArgs);
        event.setEventProperties(te.eventProps);
        event.setEvtPID(te.evtPID);
                        
        evtStartTime = new Date();
        TELogger.logTEStart(te, evtStartTime);         
        event.execute();
                     
      }
      catch(Exception e)
      {
    	  e.printStackTrace();
    	  TELogger.logTEError(te, "EventRunner.run() error " + e.getMessage());
        //log.error("EventRunner.run() error (te.className: " + te.className + ")",e);
      }
      finally
      { 
    	// thread is finished.  update database and set thread state back to idle
          te.setTriggered();
          
          Date evtEndTime = new Date();
          TELogger.logTEEnd(te, evtEndTime, evtEndTime.getTime() - evtStartTime.getTime());
      }
    }
  }
}/*@lineinfo:generated-code*/