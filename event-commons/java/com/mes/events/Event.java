package com.mes.events;

import com.mes.support.PropertiesFile;

public interface Event {

  public void setEventArgs(String args);
  public void setEventProperties(PropertiesFile propertiesFile);
  public boolean execute();
  public void setEvtPID(String evtPID);
  public String getEvtPID();
}