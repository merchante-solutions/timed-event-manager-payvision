/*@lineinfo:filename=FlatFileRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/FlatFileRecord.sqlj $

  Description:

    FlatFileRecordField

    Implementation of a single field in a FlatFileRecord.  Contains 
    information necessary to spit out the field into a flat text file.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.commons.lang.StringUtils;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class FlatFileRecord extends SQLJConnectionBase
{
  public static final int           TC_MIXED      =  0;
  public static final int           TC_LOWER      =  1;
  public static final int           TC_UPPER      =  2;

  private   boolean           debugMode   = false;
  private   String            DefName     = "";
  private   int               defType;
  private   int               FieldIndex  = -1;
  private   Vector            fields      = new Vector();
  private   boolean           initialized = false;
  private   DefaultContext    ExternalCtx = null;
  
  public FlatFileRecord()
  {
  }
  
  public FlatFileRecord(DefaultContext newCtx)
  {
    if(newCtx != null)
    {
      ExternalCtx = newCtx;
    }
  }
  
  public FlatFileRecord(DefaultContext newCtx, int defType )
  {
    if(newCtx != null)
    {
      ExternalCtx = newCtx;
    }
    
    this.defType = defType;
    
    initialize();
  }
  
  public FlatFileRecord(int defType)
  {
    this.defType = defType;
    
    initialize();
  }
  
  public void setDefType(int defType)
  {
    this.defType = defType;
    initialize();
  }
  
  public void setAllFieldData(ResultSet rs)
  {
    String                columnName  = null;
    FlatFileRecordField   field       = null;
    ResultSetMetaData     md          = null;
    Date                  tempDate    = null;
    long                  tempTime    = 0L;
  
    try
    {
      md = rs.getMetaData();
      
      boolean foundField = false;
      
      // set data in template for any fields specified by the ResultSet
      for(int i=1; i <= md.getColumnCount(); ++i)
      {
        columnName = md.getColumnName(i).toLowerCase();
        // set the data on all instances of column name in the vector
        foundField = false;
        for(int j = 0; j < fields.size(); ++j)
        {
          field = (FlatFileRecordField)fields.elementAt(j);
          if( field.getName().equals(columnName) )
          {
            foundField = true;
            switch(field.getType())
            {
              case FlatFileRecordField.FIELD_TYPE_NUMERIC:
                field.setData(rs.getDouble(columnName));
                break;
              
              case FlatFileRecordField.FIELD_TYPE_DATE:
                if(rs.getTimestamp(columnName) == null)
                {
                  field.setData("");
                }
                else
                {
                  field.setData(new Date(rs.getTimestamp(columnName).getTime()));
                }
                break;
                
              //case FlatFileRecordField.FIELD_TYPE_ALPHA:
              //case FlatFileRecordField.FIELD_TYPE_ALPHA_NUMERIC:
              //case FlatFileRecordField.FIELD_TYPE_ALPHA_ONLY:
              //case FlatFileRecordField.FIELD_TYPE_ALPHA_NUMERIC_NO_PAD:
              //case FlatFileRecordField.FIELD_TYPE_ALPHA_NUMERIC_LPAD:
              //case FlatFileRecordField.FIELD_TYPE_ALPHA_NUMERIC_MIN_ONE:
              default:
                field.setData(rs.getString(columnName));
                break;
            }
          }
        }
        
        if(!foundField && debugMode)
        {
          System.out.println("Couldn't find field: " + columnName);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setAllFieldData("+this.defType+","+columnName+")", e.toString());
    }
  }
  
    public boolean setAllSDRFieldData(FlatFileRecord inputFlatFile, String formatType, int additionalInfo) {
        FlatFileRecordField outputField = null;
        String inputFieldName = null;
        boolean populatedFlag = false;

        try {
            boolean foundField = false;

            String[] inputFormFieldNames = inputFlatFile.getFieldNames().split("\n");

            // set data in template for any fields specified by the FieldBean
            for (int i = 0; i < inputFormFieldNames.length; ++i) {
                inputFieldName = inputFormFieldNames[i];

                String outputFieldName = null;

                if ("AIRLINES".equals(formatType) && additionalInfo < 0) {
                    outputFieldName = SDRMappings.SDR05AirlineMap.getSdrMappingForCdfField(inputFieldName);
                } else if ("AIRLINES".equals(formatType) && additionalInfo >= 0) {
                    String tempOutputFieldName = SDRMappings.SDR05AirlineLegMap
                            .getSdrMappingForCdfField(inputFieldName);
                    outputFieldName = getOutputLegFieldName(tempOutputFieldName, this.defType);
                } else if ("CARRENTALS".equals(formatType)) {
                    outputFieldName = SDRMappings.SDR06CarRentalMap.getSdrMappingForCdfField(inputFieldName);
                } else {
                    outputFieldName = SDRMappings.SDR09HotelMap.getSdrMappingForCdfField(inputFieldName);
                }

                // set the data on all instances of column name in the vector
                foundField = false;
                String fieldValue = inputFlatFile.getFieldData(inputFieldName);
                for (int j = 0; j < fields.size() && isValid(outputFieldName, fieldValue); ++j) {
                    outputField = (FlatFileRecordField) fields.elementAt(j);

                    if (outputField.getName().equals(outputFieldName)) {
                        foundField = true;

                        int type = outputField.getType();
                        if (type == 2) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
                            Date d1 = sdf.parse(fieldValue);
                            String format = outputField.getMask();
                            sdf.applyPattern(format);
                            outputField.setData(sdf.format(d1));
                        } else {
                            outputField.setData(fieldValue);
                        }
                        populatedFlag = true;
                    }
                }
            }

            if (!foundField && debugMode) {
                System.out.println("Couldn't find field: " + inputFieldName);
            }
        } catch (Exception e) {
            populatedFlag = false;
            logEntry("setAllSDRFieldData(" + this.defType + "," + inputFieldName + ")", e.toString());
        }
        return populatedFlag;
    }

    private boolean isValid(String outputFieldName, String fieldValue) {
        return !StringUtils.isBlank(outputFieldName) && !StringUtils.isBlank(fieldValue);
    }
  
  public void setAllFieldData(FieldBean fieldBean)
  {
    FlatFileRecordField   field       = null;
    String                fieldName   = null;
    Field                 formField   = null;
    Date                  tempDate    = null;
    long                  tempTime    = 0L;
  
    try
    {
      boolean foundField = false;
      
      String[] formFieldNames = fieldBean.getFieldNames().split("\n");
      
      // set data in template for any fields specified by the FieldBean
      for(int i=0; i < formFieldNames.length; ++i)
      {
        fieldName = formFieldNames[i];
        
        // set the data on all instances of column name in the vector
        foundField = false;
        for(int j = 0; j < fields.size(); ++j)
        {
          field = (FlatFileRecordField)fields.elementAt(j);
          
          if( field.getName().equals(fieldName) )
          {
            foundField  = true;
            formField   = fieldBean.getField(fieldName);
            
            if ( formField instanceof DateField )
            {
              Date dateValue = ((DateField)formField).getUtilDate();
              if ( dateValue == null )
              {
                field.setData("");
              }
              else
              {
                field.setData(dateValue);
              }
            }
            else if ( formField instanceof NumberField ||
                      formField instanceof CurrencyField )
            {
              field.setData(formField.asDouble());
            }
            else
            {
              field.setData( formField.getData() );
            }              
          }
        }
        
        if(!foundField && debugMode)
        {
          System.out.println("Couldn't find field: " + fieldName);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setAllFieldData("+this.defType+","+fieldName+")", e.toString());
    }
  }
  
  public String spew(String delimiter,int textCase)
  {
    return spew(delimiter,textCase,true);
  }
  
  public String spew(String delimiter,int textCase, boolean scrubUnprintable)
  {
    String        formattedData = null;
    boolean       hasDelimiter  = (delimiter.length()>0);
    StringBuffer  record        = new StringBuffer("");
    
    try
    {
      // formatted values to the stringbuffer
      for(int i=0; i<fields.size(); ++i)
      {
        FlatFileRecordField field = (FlatFileRecordField)fields.elementAt(i);
        formattedData = ((FlatFileRecordField)fields.elementAt(i)).getFormattedData();
        
        // adjust for the case setting
        switch( textCase )
        {
          case TC_UPPER:
            if ( formattedData != null && formattedData.length() > 0 )
            {
              formattedData = formattedData.toUpperCase();
            }              
            break;
            
          case TC_LOWER:
            if ( formattedData != null && formattedData.length() > 0 )
            {
              formattedData = formattedData.toLowerCase();
            }              
            break;
            
          default:
            break;
        }
        
        record.append( formattedData );
        if(hasDelimiter)
        {
          record.append(delimiter);
        }
      }

      if(scrubUnprintable)
      {
        // replace non-printable characters with a space

        int len = record.length();
        char[] recordChars = new char[len];
        record.getChars(0, len, recordChars, 0);

        for( int i = 0; i < len; ++i )
        {
          char ch = recordChars[i];
          if( ch < ' ' || ch > '~' )
          {
            record.setCharAt(i,' ');
          }
        }
        formattedData = record.toString();
      }
      else
      {
        formattedData = StringUtilities.removeExtendedAsciiChars(record.toString());
      }
    }
    catch(Exception e)
    {
      logEntry("spew(" + defType + ")", e.toString());
    }
    
    return( formattedData );
  }
   
  public String spew(int textCase)
  {
    return spew("",textCase,true);
  }
  
  public String spew(String delimiter)
  {
    return spew(delimiter,TC_MIXED,true);
  }
  
  public String spew()
  {
    return spew("",TC_MIXED,true);
  }
  
  public String spewJSON()
  {
    return( spewJSON(0) );
  }
  
  public String spewJSON( int indentSize )
  {
    String        indent        = "";
    String        lf            = "\n";
    String        rawData       = null;
    StringBuffer  record        = new StringBuffer("");
    
    try
    {
      for( int i = 0; i < indentSize; ++i )
      {
        indent += " ";
      }
      record.append(indent + "{" + lf);
      
      // formatted values to the stringbuffer
      for(int i=0; i < fields.size(); ++i)
      {
        FlatFileRecordField field = (FlatFileRecordField)fields.elementAt(i);
        rawData = field.getDisplayData();

        if ( field.getType() != FlatFileRecordField.FIELD_TYPE_NUMERIC )
        {
          rawData = "\"" + rawData + "\"";  
        }
        record.append(indent 
                      + "  \"" 
                      + field.getName() 
                      + "\":" 
                      + rawData 
                      + ((i+1 >= fields.size()) ? "" : ",")
                      + lf);
      }
      record.append(indent + "}");
    }
    catch(Exception e)
    {
      logEntry("spewJSON(" + defType + ")", e.toString());
    }
    return( StringUtilities.removeExtendedAsciiChars(record.toString()) );
  }
  
  public FlatFileRecordField findFieldByName(String name)
  {
    FlatFileRecordField result = null;
    boolean found = false;
    
    try
    {
      for (int i=0; i < fields.size(); ++i)
      {
        if(((FlatFileRecordField)fields.elementAt(i)).getName().equals(name))
        {
          result = (FlatFileRecordField)fields.elementAt(i);
          found = true;
          break;
        }
      }
      
      if(!found && debugMode)
      {
        System.out.println("Couldn't find field: " + name);
      }
    }
    catch(Exception e)
    {
      logEntry("findFieldByName(" + name + ")", e.toString());
    }
    
    return result;
  }
  
  public FlatFileRecordField findNextFieldByName( FlatFileRecordField currField )
  {
    FlatFileRecordField     field       = null;
    FlatFileRecordField     result      = null;
    boolean                 searching   = false;
    
    try
    {
      for (int i=0; i < fields.size(); ++i)
      {
        field = (FlatFileRecordField)fields.elementAt(i);
        if( field.getNumber() == currField.getNumber() )
        {
          searching = true; // found the the current instance, 
          continue;         // set the search flag and move
                            // to the next entry
        }
        
        // if we have found the current record, then look
        // for the next one.
        if ( searching == true && 
             field.getName().equals(currField.getName()) )
        {
          result = (FlatFileRecordField)fields.elementAt(i);
          break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("findNextFieldByName(" + currField.getName() + "," +
               currField.getNumber() + ")", e.toString());
    }
    
    return( result );
  }
  
  public FlatFileRecordField findFieldByNumber(int number)
  {
    FlatFileRecordField result = null;
    
    try
    {
      for (int i=0; i < fields.size(); ++i)
      {
        if(((FlatFileRecordField)fields.elementAt(i)).getNumber() == number)
        {
          result = (FlatFileRecordField)fields.elementAt(i);
          break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("findFieldByNumber(" + number + ")", e.toString());
    }
    
    return result;
  }
  
  public int getDefType( )
  {
    return( this.defType );
  }
  
  public String getDefName( )
  {
    return( DefName );
  }
  
  public FlatFileRecordField getFirstField( )
  {
    FieldIndex = -1;
    return( getNextField() );
  }
  
  public FlatFileRecordField getNextField( )
  {
    FlatFileRecordField       retVal      = null;
    
    ++FieldIndex;
    if ( fields != null && fields.size() > FieldIndex )
    {
      retVal = (FlatFileRecordField)fields.elementAt(FieldIndex);
    }
    return( retVal );
  }
  
  public void setDebug( boolean isOn )
  {
    debugMode = isOn;
  }

  public int getFieldAsInt( String fieldName )
  {
    return( getFieldAsInt( fieldName, 0 ) );
  }
  
  public int getFieldAsInt( String fieldName, int defaultValue )
  {
    int     retVal      = defaultValue;
    
    try
    {
      retVal = Integer.parseInt( getFieldRawData(fieldName).trim() );
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public long getFieldAsLong( String fieldName )
  {
    return( getFieldAsLong( fieldName, 0L ) );
  }
  
  public long getFieldAsLong( String fieldName, long defaultValue )
  {
    long    retVal      = defaultValue;
    
    try
    {
      retVal = Long.parseLong( getFieldRawData(fieldName).trim() );
    }
    catch( Exception e )
    {
    }
    return( retVal );
  
  }
  
  public double getFieldAsDouble( String fieldName )
  {
    return( getFieldAsDouble( fieldName, 0.0 ) );
  }
  
  public double getFieldAsDouble( String fieldName, double defaultValue )
  {
    long    longVal     = 0L;
    double  retVal      = defaultValue;
    
    try
    {
      FlatFileRecordField f = findFieldByName(fieldName);
      
      if( f != null )
      {
        String        rawData = f.getRawData().trim();
        int           padLen  = Math.max(rawData.length(),f.decimalPlaces);
        StringBuffer  buf     = new StringBuffer( StringUtilities.rightJustify(rawData,padLen,'0') );
        buf.insert( (buf.length() - f.decimalPlaces), '.');
        retVal = Double.parseDouble(buf.toString());
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public Date getFieldAsDate( String fieldName )
  {
    Date    retVal      = null;
    
    try
    {
      FlatFileRecordField f = findFieldByName(fieldName);
      if( f != null && !StringUtilities.isAllZeroes(f.getRawData()) )
      {
        retVal = DateTimeFormatter.parseDate( f.getRawData().trim(), f.mask );
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public java.sql.Date getFieldAsSqlDate( String fieldName )
  {
    java.util.Date    javaDate    = null;
    java.sql.Date     sqlDate     = null;
    
    javaDate = getFieldAsDate(fieldName);
    if ( javaDate != null )
    {
       sqlDate = new java.sql.Date( javaDate.getTime() );
    }      
    return( sqlDate );
  }
  
  public int getFieldLength(String fieldName)
  {
    int   retVal  = -1;

    try
    {
      FlatFileRecordField f = findFieldByName(fieldName);
      
      if(f != null)
      {
        retVal = f.getLength();
      }
    }
    catch(Exception e)
    {
      logEntry("getFieldLength(" + fieldName + ")", e.toString());
    }
    
    return( retVal );
  }

  public String getFieldRawData(String fieldName)
  {
    String result = "";

    try
    {
      FlatFileRecordField f = findFieldByName(fieldName);
      
      if(f != null)
      {
        result = f.getRawData();
      }
    }
    catch(Exception e)
    {
      logEntry("getFieldRawData(" + fieldName + ")", e.toString());
    }

    return result;
  }
  
  public int getRecordLength()
  {
    FlatFileRecordField   f             =   null;
    int                   recordLength  = 0;
    
    for( int i = 0; i < fields.size(); ++i )
    {    
      f = (FlatFileRecordField)fields.elementAt(i);
      recordLength += f.getLength();
    }
    return( recordLength );
  }
  
  public String getFieldData(String fieldName)
  {
    return( getFieldData(fieldName,"") );
  }
  
  public String getFieldData(String fieldName, String defaultValue)
  {
    String  result = defaultValue;
    try
    {
      FlatFileRecordField f = findFieldByName(fieldName);
      if ( f != null )
      {
        result = f.getFormattedData();
      }        
    }
    catch(Exception e)
    {
      logEntry("getFieldData(" + fieldName + ")", e.toString());
    }
    
    return ( result );
  }
  
  public String getFieldNames()
  {
    FlatFileRecordField   f         =   null;
    StringBuffer          result    = new StringBuffer("");

    try
    {
      for( int i = 0; i < fields.size(); ++i )
      {
        f = (FlatFileRecordField)fields.elementAt(i);
        result.append(f.getName());
        result.append("\n");
      }
    }
    catch(Exception e)
    {
      logEntry("getFieldNames()", e.toString());
    }
    return ( result.toString() );
  }
  
  public void scrubFiller(char fillerChar)
  {
    String fdata = "";
    
    for(int fldIdx = 0; fldIdx < fields.size(); ++fldIdx)
    {
      FlatFileRecordField field       = (FlatFileRecordField)fields.elementAt(fldIdx);
      String              filledData  = "";
      
      for( int i = 0; i < field.getLength(); ++i )
      {
        filledData += fillerChar;
      }
      if ( filledData.equals(field.getFormattedData()) )
      {
        field.setData(filledData.replace(fillerChar,' '));
      }
    }
  }
  
  public void setFieldData(String field, String data)
  {
    try
    {
      FlatFileRecordField f = findFieldByName(field);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + field +", " + data + ")", e.toString());
    }
  }
  public void setFieldData(String field, int data)
  {
    try
    {
      FlatFileRecordField f = findFieldByName(field);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + field +", " + data + ")", e.toString());
    }
  }
  public void setFieldData(String field, long data)
  {
    try
    {
      FlatFileRecordField f = findFieldByName(field);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + field + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(String field, double data)
  {
    try
    {
      FlatFileRecordField f = findFieldByName(field);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + field + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(String field, Date data)
  {
    try
    {
      FlatFileRecordField f = findFieldByName(field);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + field + ", " + data + ")", e.toString());
    }
  }
  
  public void setFieldData(int fieldNumber, String data)
  {
    try
    {
      FlatFileRecordField f = findFieldByNumber(fieldNumber);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + fieldNumber + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(int fieldNumber, int data)
  {
    try
    {
      FlatFileRecordField f = findFieldByNumber(fieldNumber);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + fieldNumber + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(int fieldNumber, long data)
  {
    try
    {
      FlatFileRecordField f = findFieldByNumber(fieldNumber);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + fieldNumber + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(int fieldNumber, double data)
  {
    try
    {
      FlatFileRecordField f = findFieldByNumber(fieldNumber);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + fieldNumber + ", " + data + ")", e.toString());
    }
  }
  public void setFieldData(int fieldNumber, Date data)
  {
    try
    {
      FlatFileRecordField f = findFieldByNumber(fieldNumber);
      
      while(f != null)
      {
        f.setData(data);
        f = findNextFieldByName(f);
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldData(" + fieldNumber + ", " + data + ")", e.toString());
    }
  }
  
  public void setFieldBeanData( FieldBean fieldBean )
  {
    double                doubleValue = 0.0;
    String                fieldName   = null;
    Field                 formField   = null;
  
    try
    {
      String[] ffdFieldNames = getFieldNames().split("\n");
      
      // set data in template for any fields specified by the FieldBean
      for(int i=0; i < ffdFieldNames.length; ++i)
      {
        fieldName = ffdFieldNames[i];
        formField = fieldBean.getField(fieldName);

        if ( formField != null )
        {
          String rawData = getFieldRawData(fieldName);
          
          if ( rawData == null || rawData.trim().equals("") )
          {
            // do not overwrite FieldBean fields 
            // with missing, blank or all zeros 
            // in the FlatFileRecord data
            continue;   
          }
          else if ( formField instanceof DateField )
          {
            Date    dateValue = null;
            String  fieldData = getFieldData(fieldName);
            
            if ( !StringUtilities.isAllZeroes(fieldData) )
            {
              dateValue = getFieldAsDate(fieldName);
            }
            
            if ( dateValue != null )
            {
              fieldBean.setData(fieldName,dateValue);
            }
          }
          else if ( formField instanceof NumberField ||
                    formField instanceof CurrencyField )
          {
            // do not set numeric fields when the flat file data is all zeros
            if ( !rawData.replace('0',' ').trim().equals("") )
            {
              fieldBean.setData( fieldName, getFieldAsDouble(fieldName) );
            }
          }
          else
          {
            fieldBean.setData( fieldName, getFieldData(fieldName) );
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setFieldBeanData(" + fieldName + ")", e.toString());
    }
  }
  
  private void initialize()
  {
    DefaultContext        ctx           = null;
    ResultSetIterator     it            = null;
    ResultSet             rs            = null;
    
    try
    {
      initialized = false;
    
      // empty out fields vector
      fields.clear();
      
      if ( ExternalCtx == null )
      {
        connect();
        ctx = this.Ctx;
      }
      else
      {
        // do not manipulate the external connection
        ctx = ExternalCtx;
      }        
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:977^9*/

//  ************************************************************
//  #sql [ctx] { select  name 
//            from    flat_file_def_types 
//            where   def_type = :defType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  name  \n          from    flat_file_def_types \n          where   def_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.FlatFileRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,defType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   DefName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:982^9*/
      }
      catch( Exception e )
      {
        DefName = "Record Type " + String.valueOf(defType);
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:989^7*/

//  ************************************************************
//  #sql [ctx] it = { select  field_order,
//                  name,
//                  type,
//                  length,
//                  decimal_places,
//                  mask,
//                  default_value
//          from    flat_file_def
//          where   def_type = :defType
//          order by field_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  field_order,\n                name,\n                type,\n                length,\n                decimal_places,\n                mask,\n                default_value\n        from    flat_file_def\n        where   def_type =  :1 \n        order by field_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.flatfile.FlatFileRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,defType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.flatfile.FlatFileRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1001^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        fields.add(new FlatFileRecordField( rs.getInt("field_order"),
                                            rs.getString("name"), 
                                            rs.getInt("type"),
                                            rs.getInt("length"),
                                            rs.getInt("decimal_places"),
                                            rs.getString("mask"),
                                            rs.getString("default_value")));
      }
      
      rs.close();
      it.close();
      
      initialized = true;
    }
    catch(Exception e)
    {
      logEntry("createTemplate(" + defType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      
      if ( ExternalCtx == null )
      {
        // do not manipulate the external connection
        cleanUp();
      }        
    }
  }
  
  public boolean isInitialized()
  {
    return( this.initialized );
  }
  
  public void resetAllFields( )
  {
    for ( int i = 0; i < fields.size(); ++i )
    {
      ((FlatFileRecordField)fields.elementAt(i)).reset();
    }
  }
  
  public void suck(String line, String delimiter)
  {
    String              data          = null;
    int                 fldIdx        = 0;
    boolean             hasDelimiter  = delimiter.length()>0;
    int                 offset        = 0;
    StringTokenizer     tokenizer     = null;
    
    try
    {
      if ( hasDelimiter )
      {
        tokenizer = new StringTokenizer( line, delimiter );
      }        
    
      // formatted values to the stringbuffer
      offset = 0;
      for( fldIdx = 0; fldIdx < fields.size(); ++fldIdx )
      {
        FlatFileRecordField field = (FlatFileRecordField)fields.elementAt(fldIdx);
        
        if ( hasDelimiter )
        {
          data = tokenizer.nextToken();
        }
        else    // flat
        {
          data = line.substring( offset, offset + field.getLength() );
          offset += field.getLength();
        }
        
        try
        {
          field.setFormattedData(data);
        }
        catch( Exception dataFormat_e )    
        {
          // bad data, load it as a raw string
          field.setData(data);
        }          
      }
    }
    catch(Exception e)
    {
      logEntry("suck(" + defType + "," + fldIdx + ")", e.toString());
    }
  }
    
  
  public void suck( String line )
  {
    suck( line, "" );
  }
  
  public void showData()
  {
    String formattedData = "";
    
    for(int i=0; i<fields.size(); ++i)
    {
      FlatFileRecordField field = (FlatFileRecordField)fields.elementAt(i);
      formattedData = ((FlatFileRecordField)fields.elementAt(i)).getFormattedData();
      System.out.println( field.getName() + " : " + formattedData);
    }
  }

  
    private String getOutputLegFieldName(String tempOutputFieldName, int defType) {
        if (!StringUtils.isBlank(tempOutputFieldName)) {
            switch (defType) {
            case MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_3:
                return tempOutputFieldName.replace("~", "_1");
            case MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_4:
                return tempOutputFieldName.replace("~", "_2");
            case MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_5:
                return tempOutputFieldName.replace("~", "_3");
            case MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_6:
                return tempOutputFieldName.replace("~", "");
            default:
                return tempOutputFieldName.replace("~", "");
            }
        }
        return null;
    }
}

