package com.mes.clearing.utils;

public enum ClearingConstants {
	PT_CB_ADJ(2),PT_MOJAVE_SUMMARY(14),SUM_TYPE_DDF(1), PROC_TYPE_RESPN(2),PROC_TYPE_CBDIS(1);
	
	private int value;	
	
	public int getValue() {
		return value;
	}
	
	private ClearingConstants(int value){
		this.value = value;
	}
}
