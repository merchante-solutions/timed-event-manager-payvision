package com.mes.fasterfunding;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class FasterFundingDao {
    
    private Logger log = Logger.getLogger(FasterFundingDao.class);
    private Connection con;
    private static final String SQL_GET_FASTER_FUNDING_CONFIG = "SELECT ELIGIBLE_BANK, ELIGIBLE_ENTRY_DESCRIPTION FROM FASTER_FUNDING_CONFIG";
    
    public FasterFundingDao(Connection con) {
        this.con = con;
    }

    public Map<Long,String> loadFasterFundingConfig() {
        
        log.debug("Inside AchEvent::loadFasterFundingConfig() method");
        Map<Long,String> fasterFundingConfig = new HashMap<>();

        try (PreparedStatement ps = con.prepareStatement(SQL_GET_FASTER_FUNDING_CONFIG)) {

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    fasterFundingConfig.put(rs.getLong("ELIGIBLE_BANK"), rs.getString("ELIGIBLE_ENTRY_DESCRIPTION"));
                }
            }
        } catch (Exception e) {
            log.error("Could not load Faster Funding Configuration from DB:", e);
        }
        
        return fasterFundingConfig;
    }

}
