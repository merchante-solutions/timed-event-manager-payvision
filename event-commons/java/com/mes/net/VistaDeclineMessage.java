/*@lineinfo:filename=VistaDeclineMessage*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VistaDeclineMessage.sqlj $

  Description:  
  
    VistaDeclineMessage

    Message used to notify Vista that a merchant application has been
    declined by credit.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:21p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.net.URL;
import org.jdom.Document;
import org.jdom.Element;

public class VistaDeclineMessage extends XMLMessage
{
  private long appSeqNum;
  private String vistaId;
  private String returnUrl;
  private String details;
  
  public VistaDeclineMessage(long appSeqNum, String details)
  {
    this.details = details;
    this.appSeqNum = appSeqNum;
    loadData();
    buildDocument();
  }
  
  /*
  ** private void loadData(long appSeqNum)
  **
  ** Loads message data (vista id).
  */
  protected void loadData()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] { select  vl.verisign_id,
//                  vl.business_url
//          
//          from    vs_linking_table vl
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vl.verisign_id,\n                vl.business_url\n         \n        from    vs_linking_table vl\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VistaDeclineMessage",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vistaId = (String)__sJT_rs.getString(1);
   returnUrl = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/
      
      try
      {
        if (returnUrl != null)
        {
          URL url = new URL(returnUrl);
          setTargetUrl("https://" + url.getHost() 
            + "/verisignresponse/xmlacceptor.php3");
        }
      }
      catch (Exception e) {}
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "::loadData(appSeqNum=" + appSeqNum + ", db) " + e.toString());
      logEntry("loadData(appSeqNum=" + appSeqNum + ", db)",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  
  /*
  ** protected void buildDocument()
  **
  ** Generates the xml message:
  **
  **  <VerisignResponse>
  **    <Status>Declined</Status>
  **    <Message>Stinky credit</Message>
  **    <VistaId>vista_id</VistaId>
  **  </VerisignResponse>
  */
  protected void buildDocument()
  {
    doc = new Document(buildDocBody());
  }
  protected Element buildDocBody()
  {
    Element body = new Element("VerisignResponse");
    body.addContent(new Element("Status").setText("Denied"));
    body.addContent(new Element("Message").setText(details));
    body.addContent(new Element("VistaId").setText(vistaId));
    return body;
  }
  
  public String getMessageType()
  {
    return "VISTA: DECLINED (" + appSeqNum + ")";
  }
}/*@lineinfo:generated-code*/