/*@lineinfo:filename=SynDataRequestXDoc*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/SynDataRequestXDoc.sqlj $

  Description:  
  
    SynDataRequestXDoc

    Synergistic request message, used to request the credit report data.
    The request sent is the same as the response to an id request sent
    earlier so this class will load a stored version of that response
    from a db table.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-12-10 16:38:57 -0800 (Tue, 10 Dec 2013) $
  Version            : $Revision: 21935 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.util.List;
import java.util.zip.InflaterInputStream;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import sqlj.runtime.ResultSetIterator;

public class SynDataRequestXDoc extends XDoc
{
  boolean requestReady = false;
  
  public SynDataRequestXDoc(long appSeqNum)
  {
    loadRequestData(appSeqNum);
  }
  
  private Element getOutputs()
  {
    return doc.getRootElement()
              .getChild("request")
              .getChild("products")
              .getChild("product")
              .getChild("outputs");
  }              
  
  /*
  ** private void loadAppData(long appSeqNum)
  **
  ** Retrieves the request data from the credit_requests table.
  */
  protected void loadRequestData(long appSeqNum)
  {
    // load the db record data
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    byte docBuf[] = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  request_data
//          from    credit_requests
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  request_data\n        from    credit_requests\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.SynDataRequestXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.net.SynDataRequestXDoc",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        Blob b = rs.getBlob("request_data");
        int dataLen = (int)b.length();
        int chunkSize = 0;
        int dataRead = 0;
        InputStream is = b.getBinaryStream();
        docBuf = new byte[dataLen];
      
        while(chunkSize != -1)
        {
          chunkSize = is.read(docBuf,dataRead,dataLen - dataRead);
          dataRead += chunkSize;
        }
        is.close();
      }
      rs.close();
      it.close();
      
      /*
      String outFileName = Long.toString(appSeqNum) + "_request.zip";
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outFileName));
      bos.write(docBuf);
      bos.flush();
      bos.close();
      */
      
      /* CHANGED 12/01/13
      // load a blob reference
      Blob blob;
      #sql [Ctx]
      {
        select  request_data into :blob
        from    credit_requests
        where   app_seq_num = :appSeqNum
      };
      
      // retrieve request data via blob reference
      int dataLen = (int)blob.length();
      int chunkSize = 0;
      int dataRead = 0;
      InputStream is = blob.getBinaryStream();
      docBuf = new byte[dataLen];
      while (chunkSize != -1)
      {
        chunkSize = is.read(docBuf,dataRead,dataLen - dataRead);
        dataRead += chunkSize;
      }
      is.close();
      */
    }
    catch (Exception e)
    {
      logEntry("loadRequestData(appSeqNum=" + appSeqNum + ", db)",e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    // decompress into xml document storage
    // and trim out the non-xml output id
    if (docBuf != null)
    {
      try
      {
        // decompress
        ByteArrayInputStream bais = new ByteArrayInputStream(docBuf);
        InflaterInputStream iis = new InflaterInputStream(bais);
        SAXBuilder builder = new SAXBuilder();
        doc = builder.build(iis);
        
        // trim non-xml output id's
        List outputList = getOutputs().getChildren("output");
        for (int i = 0; i < outputList.size(); ++i)
        {
          Element output = (Element)outputList.get(i);
          if (output.getChild("description").getText().indexOf("XML") != 0)
          {
            outputList.remove(i--);
          }
        }
        requestReady = true;
      }
      catch (Exception e)
      {
        logEntry("loadRequestData(appSeqNum=" + appSeqNum + ", decomp)",e.toString());
      }
    }
  }
  
  public boolean isReady()
  {
    return requestReady;
  }
}/*@lineinfo:generated-code*/