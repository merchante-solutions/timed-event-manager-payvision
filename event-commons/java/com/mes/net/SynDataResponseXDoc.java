/*@lineinfo:filename=SynDataResponseXDoc*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/SynDataResponseXDoc.sqlj $

  Description:  
  
    SynDataResponseXDoc

    XML document in response to request for credit report data received 
    from Synergistic Software.  Provides accessors to key data fields
    in response and allows fields to be submitted and recalled from 
    a db table.
        
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-12-10 16:38:57 -0800 (Tue, 10 Dec 2013) $
  Version            : $Revision: 21935 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.jdom.Element;

public class SynDataResponseXDoc extends XDoc
{
  public SynDataResponseXDoc(XMLMessage xmlMsg)
  {
    super(xmlMsg.getDocument());
  }
  
  protected class Applicant
  {
    private String segmentId;
    private String name;
    private String ssn;
    private String score;
    
    private int bkCount;
    private int mortCount;
    private int derogCount;
    
    public Applicant(Element applicant)
    {
      segmentId = applicant.getChild("segmentid").getText();
      
      // NOTE: some credit scores have been received without ssn so we
      // are going to treat names and ssn elements as optional here
      
      // combine name elements into one name
      Element fnEl = applicant.getChild("firstname");
      Element lnEl = applicant.getChild("lastname");
      if (fnEl != null || lnEl != null)
      {
        name = fnEl != null ? fnEl.getText() : null;
        if (lnEl != null)
        {
          name = (name != null ? name + " " : "") + lnEl.getText();
        }
      }
        
      // look for ssn
      Element ssnEl = applicant.getChild("ssn");
      ssn = ssnEl != null ? ssnEl.getText() : null;
    }
    
    public String getSegmentId()
    {
      return segmentId;
    }
    public String getName()
    {
      return name;
    }
    public String getSsn()
    {
      return ssn;
    }
    public String getScore()
    {
      return score;
    }
    public void setScore(String score)
    {
      this.score = score;
    }
    public int getBkCount()
    {
      return bkCount;
    }
    public void setBkCount(int bkCount)
    {
      this.bkCount = bkCount;
    }
    public int getMortCount()
    {
      return mortCount;
    }
    public void setMortCount(int mortCount)
    {
      this.mortCount = mortCount;
    }
    public int getDerogCount()
    {
      return derogCount;
    }
    public void setDerogCount(int derogCount)
    {
      this.derogCount = derogCount;
    }
    public String toString()
    {
      return "segId: " + segmentId + ", name: " + name + 
             ", ssn: " + ssn + ", score: " + score + ", bkCount: " +
             bkCount + ", mortCount: " + mortCount + ", derogCount: " +
             derogCount;
    }
  };
  private Vector applicants = null;
  
  /*
  ** public class ExtractException extends Exception
  **
  ** Keeps track of an extract state when a data extract exception gets thrown.
  */
  public class ExtractException extends Exception
  {
    private int exState;
    
    public ExtractException(String message, int exState)
    {
      super("Extraction failed (" + exState + ") due to '" + message + "'");
      this.exState = exState;
    }
    
    public int getExState()
    {
      return exState;
    }
  }
  
  /*
  ** private List getList(String childName)
  **
  ** Scans through the document request element's children, locates the first
  ** request child that starts with the child name (i.e. would find 'addresses'
  ** element if childName were 'address').  If found, it's children are returned
  ** in a list.
  **
  ** RETURNS: List object containing the elements named by childName, NULL if
  **          no match found.
  */
  private List getList(String childName)
  {
    // get list of elements below request
    List parentList = doc.getRootElement()
                         .getChild("requests")
                         .getChild("request")
                         .getChildren();

    // scan for a parent to a list of elements named by childName                         
    for (Iterator i = parentList.iterator(); i.hasNext();)
    {
      Element child = (Element)i.next();
      if (child.getName().startsWith(childName))
      {
        return child.getChildren();
      }
    }
    
    // no match found
    return null;
  }
  
  // accumulate method states
  public static final int EX_ACCUM_START      = 100;
  public static final int EX_ACCUM_APP_SET    = 101;
  public static final int EX_ACCUM_ITEMS      = 102;
  public static final int EX_ACCUM_ITEM_LIST  = 103;
  public static final int EX_ACCUM_SEG_MATCH  = 104;
  public static final int EX_ACCUM_ITEM_BK    = 105;
  public static final int EX_ACCUM_ITEM_MORT  = 106;
  public static final int EX_ACCUM_ITEM_DEROG = 107;
  
  /*
  ** private void accumulateIndicators(Applicant applicant)
  **   throws ExtractException
  **
  ** Scans xml for bankrupty, mortgage and derogatory indicators that match
  ** a given applicant.  The accumulated counts for each indicator are stored 
  ** in the applicant.
  */
  private void accumulateIndicators(Applicant applicant)
    throws ExtractException
  {
    int exState = EX_ACCUM_START;
    try
    {
      // a list of element name prefixes that contain the indicators
      String[] items =
      {
        "trade", "address", "aka", "employment", "inquir", "score", "public record"
      };
      
      String segmentId = applicant.getSegmentId();
      
      // get a list of each item type, accumulate indicators in each item
      int bkCount     = 0;
      int mortCount   = 0;
      int derogCount  = 0;
      exState         = EX_ACCUM_ITEMS;
      for (int i = 0; i < items.length; ++i)
      {
        // scan through each item list
        exState       = EX_ACCUM_ITEM_LIST;
        List itemList = getList(items[i]);
        if (itemList != null)
        {
          for (Iterator itemIter = itemList.iterator(); itemIter.hasNext();)
          {
            Element item = (Element)itemIter.next();
          
            // make sure the segmentId of an item corresponds with the applicant
            if (segmentId.equals(item.getChild("segmentid").getText()))
            {
              exState               = EX_ACCUM_SEG_MATCH;
              Element bkElement     = item.getChild("bankruptcyind");
              Element mortElement   = item.getChild("mortgageind");
              Element derogElement  = item.getChild("derogind");
              exState               = EX_ACCUM_ITEM_BK;
              if (bkElement != null
                  && bkElement.getText().toLowerCase().equals("true"))
              {
                ++bkCount;
              }
              exState       = EX_ACCUM_ITEM_MORT;
              if (mortElement != null
                  && mortElement.getText().toLowerCase().equals("true"))
              {
                ++mortCount;
              }
              exState       = EX_ACCUM_ITEM_DEROG;
              if (derogElement != null
                  && derogElement.getText().toLowerCase().equals("true"))
              {
                ++derogCount;
              }
            }
          }
        }
      }
      exState = EX_ACCUM_APP_SET;
      applicant.setBkCount(bkCount);
      applicant.setMortCount(mortCount);
      applicant.setDerogCount(derogCount);
    }
    catch (Exception e)
    {
      throw new ExtractException(e.toString(),exState);
    }
  }
  
  // extract method states
  public static final int EX_START          = 0;
  public static final int EX_GET_LISTS      = 1;
  public static final int EX_APP_SCAN       = 2;
  public static final int EX_APP_INFO       = 3;
  public static final int EX_SCORE_SCAN     = 4;
  public static final int EX_SCORE_INFO     = 5;
  public static final int EX_SCORE_MATCH    = 6;
  public static final int EX_NEW_APP        = 7;
  public static final int EX_SCORE_EXTRACT  = 8;
  public static final int EX_ADD_APP        = 9;
  
  /*
  ** private void extractDataFields() throws ExtractException
  **
  ** Extracts credit scores and other key fields from the xml doc
  ** containing the credit report data.
  */
  private void extractDataFields() throws ExtractException
  {
    int exState = EX_START;
    
    try
    {
      // generate an applicant with indicator counts for each score
      // that matches each applicant's segmentid
      exState         = EX_GET_LISTS;
      applicants      = new Vector();
      List appList    = getList("applicant");
      List scoreList  = getList("score");
      exState         = EX_APP_SCAN;
      for (Iterator appIter = appList.iterator(); appIter.hasNext();)
      {
        // get applicant info
        exState             = EX_APP_INFO;
        Element appElement  = (Element)appIter.next();
        String appSegId     = appElement.getChild("segmentid").getText();
        
        // scan scores for matches with applicants
        exState = EX_SCORE_SCAN;
        for (Iterator scoreIter = scoreList.iterator(); scoreIter.hasNext();)
        {
          exState               = EX_SCORE_INFO;
          Element scoreElement  = (Element)scoreIter.next();
          String scoreSegId     = scoreElement.getChild("segmentid").getText();
          
          // see if a score matches an applicant (segment id's link them)
          exState = EX_SCORE_MATCH;
          if (appSegId.equals(scoreSegId))
          {
            // create an applicant based on an applicant element
            exState = EX_NEW_APP;
            Applicant applicant = new Applicant(appElement);

            // accumulate indicator info for the applicant
            accumulateIndicators(applicant);
        
            // get the actual score data
            exState = EX_SCORE_EXTRACT;
            String scoreStr = "-1";
            try
            {
              scoreStr = scoreElement.getChild("score").getText();
            }
            catch (Exception e) { }
            applicant.setScore(scoreStr);

            // add the applicant
            exState = EX_ADD_APP;
            applicants.add(applicant);
            //System.out.println("applicant added: " + applicant.toString());
          }
        }
      }
    }
    catch (ExtractException ee)
    {
      throw ee;
    }
    catch (Exception e)
    {
      throw new ExtractException(e.toString(),exState);
    }
  }

  /*
  ** public void submitData(long appSeqNum)
  **
  ** Given an application seq num key this will store the important
  ** data fields in the credit_scores table.
  */
  private boolean isExtracted = false;
  public void submitData(long appSeqNum)
  {
    int       scoreCount    = 0;
    double    averageScore  = 0.0;
    
    // write all scores to db
    try
    {
      // get data to submit from the xml document
      if (!isExtracted)
      {
        extractDataFields();
        isExtracted = true;
      }
      
      connect();

      // update/insert a record for each applicant record extracted      
      for (Iterator i = applicants.iterator(); i.hasNext();)
      {
        Applicant app = (Applicant)i.next();
        
        try
        {
          long tempAppSeqNum = 0L;
          /*@lineinfo:generated-code*//*@lineinfo:398^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//              
//              from    credit_scores
//              where   app_seq_num = :appSeqNum
//                      and segment_id = :app.getSegmentId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_10 = app.getSegmentId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n             \n            from    credit_scores\n            where   app_seq_num =  :1 \n                    and segment_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.SynDataResponseXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_10);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempAppSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:407^11*/

//  ************************************************************
//  #sql [Ctx] { update  credit_scores
//              set     name        = :app.getName(),
//                      ssn         = :app.getSsn(),
//                      score       = :app.getScore(),
//                      bk_count    = :app.getBkCount(),
//                      mort_count  = :app.getMortCount(),
//                      derog_count = :app.getDerogCount()
//              where   app_seq_num = :appSeqNum
//                      and segment_id = :app.getSegmentId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_11 = app.getName();
 String __sJT_12 = app.getSsn();
 String __sJT_13 = app.getScore();
 int __sJT_14 = app.getBkCount();
 int __sJT_15 = app.getMortCount();
 int __sJT_16 = app.getDerogCount();
 String __sJT_17 = app.getSegmentId();
   String theSqlTS = "update  credit_scores\n            set     name        =  :1 ,\n                    ssn         =  :2 ,\n                    score       =  :3 ,\n                    bk_count    =  :4 ,\n                    mort_count  =  :5 ,\n                    derog_count =  :6 \n            where   app_seq_num =  :7 \n                    and segment_id =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.net.SynDataResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_11);
   __sJT_st.setString(2,__sJT_12);
   __sJT_st.setString(3,__sJT_13);
   __sJT_st.setInt(4,__sJT_14);
   __sJT_st.setInt(5,__sJT_15);
   __sJT_st.setInt(6,__sJT_16);
   __sJT_st.setLong(7,appSeqNum);
   __sJT_st.setString(8,__sJT_17);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:418^11*/
          //System.out.println("updated " + appSeqNum + ", " + app.getSegmentId());
        }
        catch (SQLException se)
        {
          /*@lineinfo:generated-code*//*@lineinfo:423^11*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_scores
//              (
//                app_seq_num,
//                segment_id,
//                name,
//                ssn,
//                score,
//                bk_count,
//                mort_count,
//                derog_count
//              )
//              values
//              (
//                :appSeqNum,
//                :app.getSegmentId(),
//                :app.getName(),
//                :app.getSsn(),
//                :app.getScore(),
//                :app.getBkCount(),
//                :app.getMortCount(),
//                :app.getDerogCount()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_18 = app.getSegmentId();
 String __sJT_19 = app.getName();
 String __sJT_20 = app.getSsn();
 String __sJT_21 = app.getScore();
 int __sJT_22 = app.getBkCount();
 int __sJT_23 = app.getMortCount();
 int __sJT_24 = app.getDerogCount();
   String theSqlTS = "insert into credit_scores\n            (\n              app_seq_num,\n              segment_id,\n              name,\n              ssn,\n              score,\n              bk_count,\n              mort_count,\n              derog_count\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.net.SynDataResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_18);
   __sJT_st.setString(3,__sJT_19);
   __sJT_st.setString(4,__sJT_20);
   __sJT_st.setString(5,__sJT_21);
   __sJT_st.setInt(6,__sJT_22);
   __sJT_st.setInt(7,__sJT_23);
   __sJT_st.setInt(8,__sJT_24);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:447^11*/
          //System.out.println("inserted " + appSeqNum + ", " + app.getSegmentId());
        }
        
        try
        {
          if(Double.parseDouble(app.getScore()) > 0)
          {
            ++scoreCount;
            averageScore += Double.parseDouble(app.getScore());
          }
        }
        catch(Exception de)
        {
          logEntry("submitData(" + appSeqNum + "): parsing score: " + app.getScore(), de.toString());
        }
      }
      
      // add average score to table
      if(scoreCount > 0)
      {
        averageScore = averageScore / scoreCount;
      }
      
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:472^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    credit_score_average
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    credit_score_average\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.net.SynDataResponseXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:478^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:482^9*/

//  ************************************************************
//  #sql [Ctx] { update  credit_score_average
//            set     average_score = round(:averageScore, 2)
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_score_average\n          set     average_score = round( :1 , 2)\n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.net.SynDataResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,averageScore);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:491^9*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_score_average
//            (
//              app_seq_num,
//              average_score
//            )
//            values
//            (
//              :appSeqNum,
//              round(:averageScore, 2)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into credit_score_average\n          (\n            app_seq_num,\n            average_score\n          )\n          values\n          (\n             :1 ,\n            round( :2 , 2)\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.net.SynDataResponseXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setDouble(2,averageScore);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^9*/
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "::submitData(appSeqNum=" + appSeqNum +") " + e.toString());
      logEntry("submitData(appSeqNum=" + appSeqNum +")",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/