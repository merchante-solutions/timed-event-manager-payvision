/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/net/MesNetTools.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-03-13 15:28:27 -0700 (Fri, 13 Mar 2009) $
  Version            : $Revision: 15862 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.net;

import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.axis.AxisProperties;

public class MesNetTools
{
  private static boolean    SSLCertificationValidationEnabled   = true;
  
  public static void disableSSLCertificateValidation()
  {
    if ( SSLCertificationValidationEnabled )
    {
      try
      {
        // disable SSL certificate validation for SOAP calls
        AxisProperties.setProperty("axis.socketSecureFactory","org.apache.axis.components.net.SunFakeTrustSocketFactory");
    
        // disable SSL certificate validation for HTTPS connections
        TrustManager[] trustAllCerts = new TrustManager[]
        {
          // create a trust manager that does not validate certificate chains
          new X509TrustManager() 
          {
            public X509Certificate[] getAcceptedIssuers() { return( null ); }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
          }
        };
    
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        
        // update the internal boolean so we only 
        // do the disable once per JRE session
        SSLCertificationValidationEnabled = false;
      }
      catch( Exception e )
      {
      }
    }
  }
  
	public static void setupSecureSocketFactory() {
		AxisProperties.setProperty("axis.socketSecureFactory","masthead.net.TLSSocketSecureFactory");
	}
}

