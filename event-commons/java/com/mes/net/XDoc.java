/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/XDoc.java $

  Description:  
  
    XDoc

    Base message for XML document types.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 11/26/01 4:54p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Document;
import com.mes.database.SQLJConnectionBase;

public class XDoc extends SQLJConnectionBase
{
  protected Document doc;
  
  public XDoc()
  {
  }
  public XDoc(boolean autoCommit)
  {
    super(autoCommit);
  }
  public XDoc(String connectionString)
  {
    super(connectionString);
  }
  public XDoc(String connectionString, boolean autoCommit)
  {
    super(connectionString,autoCommit);
  }
  public XDoc(Document doc)
  {
    this.doc = doc;
  }
  public XDoc(String connectionString, Document doc)
  {
    super(connectionString);
    this.doc = doc;
  }
  public XDoc(String connectionString, boolean autoCommit, Document doc)
  {
    super(connectionString,autoCommit);
    this.doc = doc;
  }
  
  public Document getDoc()
  {
    return doc;
  }
  public void setDoc(Document doc)
  {
    this.doc = doc;
  }
}
