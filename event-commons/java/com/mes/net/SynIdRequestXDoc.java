/*@lineinfo:filename=SynIdRequestXDoc*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/SynIdRequestXDoc.sqlj $

  Description:  
  
    SynIdRequestXDoc

    Synergistic request message xml payload, used to initiate a credit report.
    Should expect a SynIdResponseXDoc message back containing request and 
    report id's.
    
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Document;
import org.jdom.Element;
import com.mes.config.MesDefaults;

public class SynIdRequestXDoc extends XDoc
{
  public SynIdRequestXDoc(long appSeqNum)
  {
    loadSynergisticData(appSeqNum);
    loadAppData(appSeqNum);
    buildDoc();
  }
  
  // fixed data fields
  private String    requestType     = "";
  private String    providerId      = "";
  private String    subscriberId    = "";
  private String    userId          = "";
  private String    password        = "";
  private String    productId       = "";
  
  /*
  ** private void loadSynergisticData()
  **
  ** Loads synergistic account information.  These values are stored in the
  ** defaults table.  Currently there are two sets, one for CB&T apps
  ** and the other for all others.
  */
  protected void loadSynergisticData(long appSeqNum)
  {
    try
    {
      subscriberId  = MesDefaults.getString(MesDefaults.DK_SYN_XML_SUBSCRIBER_ID);
      userId        = MesDefaults.getString(MesDefaults.DK_SYN_XML_USER_ID);
      password      = MesDefaults.getString(MesDefaults.DK_SYN_XML_PASSWORD);

      requestType   = MesDefaults.getString(MesDefaults.DK_SYN_XML_REQUEST_TYPE);
      providerId    = MesDefaults.getString(MesDefaults.DK_SYN_XML_PROVIDER_ID);
      productId     = MesDefaults.getString(MesDefaults.DK_SYN_XML_PRODUCT_ID);
    }
    catch (Exception e)
    {
      logEntry("loadFixedData)",e.toString());
    }
    
  }
  
  // app data fields
  private String    owner1FirstName = "";
  private String    owner1LastName  = "";
  private String    owner1Ssn       = "";
  private String    owner1Address   = "";
  private String    owner1City      = "";
  private String    owner1State     = "";
  private String    owner1Zip       = "";
  private String    applicantType   = "";
  private String    addressType     = "";
  
  /*
  ** private String removeNonAlphaNums(String rawData)
  **
  ** Removes any character that is not a letter number or space.
  **
  ** RETURNS: the original String without non alpha-numerics, null if original 
  **          String is null.
  */
  private String removeNonAlphaNums(String rawData)
  {
    String filteredData = null;
    if (rawData != null)
    {
      StringBuffer filteredBuf = new StringBuffer(rawData);
      for (int i = filteredBuf.length() - 1; i >= 0; --i)
      {
        char ch = filteredBuf.charAt(i);
        if (!Character.isLetterOrDigit(ch) && !Character.isSpaceChar(ch))
        {
          filteredBuf.deleteCharAt(i);
        }
      }
      filteredData = filteredBuf.toString();
    }
    return filteredData;
  }
  
  /*
  ** private void loadAppData(long appSeqNum)
  **
  ** Loads the application fields that buildDocument() will need.
  */
  protected void loadAppData(long appSeqNum)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] { select  busowner_first_name,
//                  busowner_last_name,
//                  busowner_ssn
//                  
//          
//                  
//          from    businessowner
//          
//          where   app_seq_num = :appSeqNum and
//                  busowner_num(+) = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  busowner_first_name,\n                busowner_last_name,\n                busowner_ssn\n                \n         \n                \n        from    businessowner\n        \n        where   app_seq_num =  :1  and\n                busowner_num(+) = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.SynIdRequestXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   owner1FirstName = (String)__sJT_rs.getString(1);
   owner1LastName = (String)__sJT_rs.getString(2);
   owner1Ssn = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      if (owner1Ssn.length() < 9)
      {
        owner1Ssn = (new String("000000000"))
                      .substring(owner1Ssn.length()) + owner1Ssn;
      }
      
      String address1, address2;
      /*@lineinfo:generated-code*//*@lineinfo:149^7*/

//  ************************************************************
//  #sql [Ctx] { select  address_line1,
//                  address_line2,
//                  address_city,
//                  countrystate_code,
//                  address_zip
//  
//          
//          
//          from    address
//          
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = 4
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  address_line1,\n                address_line2,\n                address_city,\n                countrystate_code,\n                address_zip\n\n         \n        \n        from    address\n        \n        where   app_seq_num =  :1  and\n                addresstype_code = 4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.net.SynIdRequestXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   address1 = (String)__sJT_rs.getString(1);
   address2 = (String)__sJT_rs.getString(2);
   owner1City = (String)__sJT_rs.getString(3);
   owner1State = (String)__sJT_rs.getString(4);
   owner1Zip = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
      if (address2 != null && address2.length() > 0)
      {
        owner1Address = address1 + " " + address2;
      }
      else
      {
        owner1Address = address1;
      }
      
      owner1FirstName = removeNonAlphaNums(owner1FirstName);
      owner1LastName  = removeNonAlphaNums(owner1LastName);
      owner1Ssn       = removeNonAlphaNums(owner1Ssn);
      owner1Address   = removeNonAlphaNums(owner1Address);
      owner1City      = removeNonAlphaNums(owner1City);
      owner1State     = removeNonAlphaNums(owner1State);
      owner1Zip       = removeNonAlphaNums(owner1Zip);
    }
    catch (Exception e)
    {
      logEntry("loadAppData(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }

    // for now set these to the following constant values
    // but these may need to vary by application in the future

    applicantType = "A";            // applicant type A (applicant)
    addressType   = "CURRENT";
  }

  /*
  ** private void buildDoc()
  **
  ** Generates a Synergistic Softare request for id:
  **
  **  <requests>
  **    <request>
  **      <requesttype>I</requesttype>
  **      <subscribers>
  **        <subscriber>
  **          <providerid>123</providerid>
  **          <subscriberid>123</subscriberid>
  **          <userid>test</userid>
  **          <password>password</password>
  **        </subscriber>
  **      </subscribers>
  **      <products>
  **        <product>
  **          <productid>{productid}</productid>
  **        </product>
  **      </products>
  **      <applicants>
  **        <applicant>
  **          <lastname>Public</lastname>
  **          <firstname>John</firstname>
  **          <middlename/>
  **          <suffix/>
  **          <ssn>123-45-6789</ssn>
  **          <type>A</type>
  **          <addresses>
  **            <address>
  **              <streetaddress>123 Street Ave</streetaddress>
  **              <city>Big City</city>
  **              <state>CA</state>
  **              <zip>94123</zip>
  **              <type>CURRENT</type>
  **            </address>
  **          </addresses>
  **        </applicant>
  **      </applicants>
  **    </request>
  **  </requests>
  */
  protected void buildDoc()
  {
    doc = new Document(buildDocBody());
  }
  protected Element buildDocBody()
  {
    return new Element("requests").addContent(buildRequest());
  }
  protected Element buildRequest()
  {
    Element request = new Element("request");
    request.addContent(buildRequestType());
    request.addContent(buildSubscribers());
    request.addContent(buildProducts());
    request.addContent(buildApplicants());
    return request;
  }
  protected Element buildRequestType()
  {
    return new Element("requesttype").setText(requestType);
  }
  protected Element buildSubscribers()
  {
    Element subscriber = new Element("subscriber");
    subscriber.addContent(new Element("providerid").setText(providerId));
    subscriber.addContent(new Element("subscriberid").setText(subscriberId));
    subscriber.addContent(new Element("userid").setText(userId));
    subscriber.addContent(new Element("password").setText(password));
    return new Element("subscribers").addContent(subscriber);
  }
  protected Element buildProducts()
  {
    Element product = new Element("product");
    product.addContent(new Element("productid").setText(productId));
    return new Element("products").addContent(product);
  }
  protected Element buildApplicants()
  {
    Element applicant = new Element("applicant");
    applicant.addContent(new Element("lastname").setText(owner1LastName));
    applicant.addContent(new Element("firstname").setText(owner1FirstName));
    applicant.addContent(new Element("middlename").setText(""));
    applicant.addContent(new Element("suffix").setText(""));
    applicant.addContent(new Element("ssn").setText(owner1Ssn));
    applicant.addContent(new Element("type").setText(applicantType));
    applicant.addContent(buildAddresses());

    return new Element("applicants").addContent(applicant);
  }
  protected Element buildAddresses()
  {
    Element address = new Element("address");
    address.addContent(new Element("streetaddress").setText(owner1Address));
    address.addContent(new Element("city").setText(owner1City));
    address.addContent(new Element("state").setText(owner1State));
    address.addContent(new Element("zip").setText(owner1Zip));
    address.addContent(new Element("type").setText(addressType));

    return new Element("addresses").addContent(address);
  }

}/*@lineinfo:generated-code*/