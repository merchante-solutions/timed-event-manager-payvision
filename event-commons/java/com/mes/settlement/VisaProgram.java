package com.mes.settlement;

import org.apache.commons.lang3.StringUtils;

public class VisaProgram {

	private String name;
	private String posEntryMode;
	private String aci;
	private String aciDB;
	private String cid;
	private String ecpi;
	private String eligibleSicCodes;
	private String aciList;
	private String purchaseIdFormatList;
	private int auth;
	private int clearing;
	private boolean generalRetail;
	private boolean cardNotPresentEcomm;
	private boolean largeTicket;
	private boolean largeTicketGsa;
	private boolean fleet;
	private boolean levelXDataOk;
	private String reimbursementAttribute;
	private String rpsNonAuto;
	private String rpsAuto;

	public String getName() {
		return name;
	}

	public String getPosEntryMode() {
		return posEntryMode;
	}

	public String getAci() {
		return aci;
	}

	public String getAciDB() {
		return aciDB;
	}

	public String getCid() {
		return cid;
	}

	public String getEcpi() {
		return ecpi;
	}

	public int getAuthCutOffTime() {
		return auth;
	}

	public int getAuth() {
		return auth;
	}

	public int getClearingCutoffTime() {
		return clearing;
	}

	public boolean isGeneralRetail() {
		return generalRetail;
	}

	public boolean isCardNotPresentEcomm() {
		return cardNotPresentEcomm;
	}

	public boolean isLargeTicket() {
		return largeTicket;
	}

	public boolean isLargeTicketGsa() {
		return largeTicketGsa;
	}

	public boolean isFleet() {
		return fleet;
	}

	public boolean isLevelXDataOk() {
		return levelXDataOk;
	}

	public String getReimbursementAttribute() {
		return reimbursementAttribute;
	}

	public String getRpsNonAuto() {
		return rpsNonAuto;
	}

	public String getEligibleSicCodes() {
		return eligibleSicCodes;
	}

	public String getAciList() {
		return aciList;
	}

	public String getPurchaseIdFormatList() {
		return purchaseIdFormatList;
	}

	public String getRpsAuto() {
		return rpsAuto;
	}

	public static VisaProgramBuilder builder() {
		return new VisaProgramBuilder();
	}

	public static class VisaProgramBuilder {
		private VisaProgram managedInstance = new VisaProgram();

		public VisaProgramBuilder withClassList(String programName) {
			managedInstance.name = programName;
			return this;
		}

		public VisaProgramBuilder withEligibleSicCodes(String eligibleSicCodes) {
			managedInstance.eligibleSicCodes = eligibleSicCodes;
			return this;
		}

		public VisaProgramBuilder withAciList(String aciList) {
			managedInstance.aciList = aciList;
			return this;
		}

		public VisaProgramBuilder withPurchaseIdFormatList(String pidFormatList) {
			managedInstance.purchaseIdFormatList = pidFormatList;
			return this;
		}

		public VisaProgramBuilder withPosEntryMode(String posEntryMode) {
			managedInstance.posEntryMode = posEntryMode;
			return this;
		}

		public VisaProgramBuilder withAuthorizationCharIndicatorDB(String aciDB) {
			managedInstance.aciDB = aciDB;
			return this;
		}

		public VisaProgramBuilder withAuthorizationCharIndicator(String aci) {
			managedInstance.aci = aci;
			return this;
		}

		public VisaProgramBuilder withCardHolderIdMethod(String cid) {
			managedInstance.cid = cid;
			return this;
		}

		public VisaProgramBuilder withEcommerceMoto(String ecpi) {
			managedInstance.ecpi = ecpi;
			return this;
		}

		public VisaProgramBuilder withTimelinessAuth(int timelinessAuth) {
			managedInstance.auth = timelinessAuth;
			return this;
		}

		public VisaProgramBuilder withTimelinessClear(int timelinessClear) {
			managedInstance.clearing = timelinessClear;
			return this;
		}

		public VisaProgramBuilder withGeneralRetail(String rtl) {
			managedInstance.generalRetail = StringUtils.equals(rtl, "Y");
			return this;
		}

		public VisaProgramBuilder withCardNotPresentEcomm(String cnpEc) {
			managedInstance.cardNotPresentEcomm = StringUtils.equals(cnpEc, "Y");
			return this;
		}

		public VisaProgramBuilder withLargeTicket(String ltkt) {
			managedInstance.largeTicket = StringUtils.equals(ltkt, "Y");
			return this;
		}

		public VisaProgramBuilder withLargeTicketGsa(String ltg) {
			managedInstance.largeTicketGsa = StringUtils.equals(ltg, "Y");
			return this;
		}

		public VisaProgramBuilder withFleet(String flt) {
			managedInstance.fleet = StringUtils.equals(flt, "Y");
			return this;
		}

		public VisaProgramBuilder withLevelXDataOk(String lvlx) {
			managedInstance.levelXDataOk = StringUtils.equals(lvlx, "Y");
			return this;
		}

		public VisaProgramBuilder withReimbursementAttribute(String ra) {
			managedInstance.reimbursementAttribute = ra;
			return this;
		}

		public VisaProgramBuilder withRequestedPaymentServiceNonAuto(String rPSNonAuto) {
			managedInstance.rpsNonAuto = rPSNonAuto;
			return this;
		}

		public VisaProgramBuilder withRequestedPaymentServiceAuto(String rpsAuto) {
			managedInstance.rpsAuto = rpsAuto;
			return this;
		}

		public VisaProgram build() {
			return managedInstance;
		}
	}

	@Override
	public String toString() {
		return "VisaProgram{" + "name='" + name + '\'' + ", posEntryMode='" + posEntryMode + '\'' + ", aci='" + aci + '\'' + ", aciDB='" + aciDB + '\''
				+ ", cid='" + cid + '\'' + ", ecpi='" + ecpi + '\'' + ", eligibleSicCodes='" + eligibleSicCodes + '\'' + ", aciList='" + aciList + '\''
				+ ", purchaseIdFormatList='" + purchaseIdFormatList + '\'' + ", auth=" + auth + ", clearing=" + clearing + ", generalRetail=" + generalRetail
				+ ", cardNotPresentEcomm=" + cardNotPresentEcomm + ", largeTicket=" + largeTicket + ", largeTicketGsa=" + largeTicketGsa + ", fleet=" + fleet
				+ ", levelXDataOk=" + levelXDataOk + ", reimbursementAttribute='" + reimbursementAttribute + '\'' + ", rpsNonAuto='" + rpsNonAuto + '\''
				+ ", rpsAuto='" + rpsAuto + '\'' + '}';
	}
}
