/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordAmex.java $

  Description:

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-05-13 13:05:08 -0700 (Wed, 13 May 2015) $
  Version            : $Revision: 23622 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.CurrencyCodeConverter;
//TLV tools to extract necessary information from ICC data
import masthead.formats.iso.TLVMsg;
import masthead.formats.iso.TlvFieldData;
import masthead.util.ByteUtil;

public class SettlementRecordAmex extends SettlementRecord
{
  public SettlementRecordAmex()
  {
    super(mesConstants.MBS_BT_VISAK, "AM", SettlementRecord.SETTLE_REC_AMEX);
  }

  private   int       CurrencyDecimalPlaces         = -1;

  public List buildFirstPresentmentRecords()
    throws Exception
  {
    List            recs      = new ArrayList(1);
    FlatFileRecord  ffd       = null;


    // order of records: 
    //    required - TAB
    //    optional - TAD
    //    optional - TAA (industry-specific or CPS -- not both!)
    //    required - TAA (location)

    // Detail V91
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAB_V91);
    ffd.setAllFieldData(this);
    
    // amex requires the amount to be encoded with the number of
    // decimal places set to the currency code decimal places
    // eg:  123.00 USD == 000000012300
    //      123.00 YEN == 000000000123
    ffd.setFieldData("transaction_amount",TridentTools.encodeAmount(getDouble("transaction_amount"),12,CurrencyDecimalPlaces));

    // amex_settlement does not have a field "transaction_type_id"
    // SettlementRecordAmex does not have a field "processing_code"
    if( "C".equals(getString("debit_credit_indicator") ) )
    {
      ffd.setFieldData("processing_code"      , 200000                                    );
      ffd.setFieldData("transaction_type_id"  , "06"                                      );
    }

    if( !isFieldBlank("roc_number") )
      ffd.setFieldData("reference_number"     , getString("roc_number")                   );
    recs.add(ffd);


    // Detail Addendums V91
    int       recType           = -1;
    boolean   isLevel3          = "Y".equals(getData("level_iii_data_present"));
    switch( getInt("format_code") )
    {
      case  1:  // airline
        recType = MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_AIR_V91;
        break;

      case 11:  // hotel
        recType = MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_HOTEL_V91;
        break;

      case 14:  // travel/cruise
        recType = MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_TRAVEL_V91;
        break;

      default:  // no industry-specific TAA
        if( isLevel3 || getString("cardholder_reference_number") != null )
        {
          recType = MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_CPS_L2_V91;
        }
        break;
    }

    String amtSign      = "C".equals(getData("debit_credit_indicator")) ? "-" : "+";
    String amtInfo[][]  = {
	    { "048" , "tran_fee_amount" }, // Surcharge
	    { "058" , "tip_amount" },
	    { "056" , "tax_amount" } // Leave tax at end; we don't always send it
    };
    ffd = null;
    for( int arrayLoop = 0, fieldLoop = 0; arrayLoop < amtInfo.length; ++arrayLoop  )
    {
      boolean isSurcharge = amtInfo[arrayLoop][1].equals("tran_fee_amount");
      double amount = getDouble(amtInfo[arrayLoop][1]);
      if( amount != 0.0 )
      {
        if( ffd == null )
        {
          // if have no other amounts and this is tax amount and going to put tax in cps taa, then continue;
          if( arrayLoop == (amtInfo.length-1) && 
              recType   == MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_CPS_L2_V91 )
          {
            continue;
          }

          ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAD_V91);
          ffd.setAllFieldData(this);
        }
        ++fieldLoop;
        ffd.setFieldData(("amount_type_" + fieldLoop), amtInfo[arrayLoop][0]);
        ffd.setFieldData(("amount_" + fieldLoop), TridentTools.encodeAmount(amount,12,CurrencyDecimalPlaces));
        ffd.setFieldData(("amount_sign_" + fieldLoop), isSurcharge ? (amount < 0 ? "-" : "+") : amtSign);
      }
    }
    if( ffd != null )
    {
      recs.add(ffd);
    }

    if ( recType > 0 )
    {
      ffd = getFlatFileRecord(recType);
      ffd.setAllFieldData(this);

      switch( recType )
      {
        case MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_CPS_L2_V91:
          ffd.setFieldData("tax_amount"                   , TridentTools.encodeAmount(getDouble("tax_amount"                 ),12,CurrencyDecimalPlaces));
          break;

        case MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_AUTO_V91:
          ffd.setFieldData("audit_adjustment_amount"      , TridentTools.encodeAmount(getDouble("audit_adjustment_amount"    ),12,CurrencyDecimalPlaces));
          break;

        case MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_AIR_V91:
          ffd.setFieldData("original_transaction_amount"  , TridentTools.encodeAmount(getDouble("original_transaction_amount"),12,CurrencyDecimalPlaces));
          break;

        case MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_TRAVEL_V91:
          ffd.setFieldData("rate_daily"                   , TridentTools.encodeAmount(getDouble("rate_daily"                 ),12,CurrencyDecimalPlaces));
          break;
      }

      if( isLevel3 && LineItems != null )
      {
        int idx = 0;
        for( Iterator i = LineItems.iterator(); i.hasNext(); )
        {
          if ( ++idx > 4 ) { break; }

          LineItem item = (LineItem)i.next();

          ffd.setFieldData(("charge_item_amount_"   + idx)  , TridentTools.encodeAmount(item.getDouble ("extended_item_amount"),12,CurrencyDecimalPlaces) );
          ffd.setFieldData(("charge_description_"   + idx)  ,                           item.getString ("item_description")                               );
          ffd.setFieldData(("charge_item_quantity_" + idx)  ,                           item.getInt    ("item_quantity")                                  );
        }
      }
      recs.add(ffd);
    }

    // Location Detail V91
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_LOC_DTL_V91);
    ffd.setAllFieldData(this);
    ffd.setFieldData("country_code"         , "PR".equals(getData("dba_state")) ? "630" : "840");
    if( getData("dba_name").startsWith("MES*") )
    {
      String sellerID = ("0000000000" + getData("merchant_number"));
      sellerID = sellerID.substring(sellerID.length() - 10) + getData("dba_name").substring(3).replaceAll(" ","");
      if( sellerID.length() > 20 ) sellerID = sellerID.substring(0,20);
      ffd.setFieldData("seller_id"            , sellerID );
    }
    
    // for optblue merchants
    if(isOptMerchant()){
    	// for Optblue, seller id has to be left justified and character space filled.
    	ffd.setFieldData("seller_id", getData("merchant_number") );
    }
    
    recs.add(ffd);

    try{
        if( isEmvEnabled && !isFieldBlank("icc_data") )
        {
          // ICC / EMV data
          ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_CAPN_TAA_ICC_EMV);
          
          String emvData = getData("icc_data");
          TlvFieldData input = new TlvFieldData();   	 
          input.unpack(ByteUtil.parseHexString(emvData), 0, true);
             
          //Parse data
          // tags is a collection of TLVMsg 
          Enumeration tags = input.elements();
          while (tags.hasMoreElements()) {
            try{
        	  TLVMsg tlvMsg = (TLVMsg) tags.nextElement();          
              // extract the current tag from the TLV
              int tag = tlvMsg.getTag();
              String value = input.getValue(tag);
              //Write tag values to appropriate field
              switch (tag){
                case(mesConstants.EMV_TAG_APP_CRYPTOGRAM):
                  ffd.setFieldData("application_cryptogram", input.getValue(tag));
                  break;
                case(mesConstants.EMV_TAG_ISSUER_APP_DATA):
                  ffd.setFieldData("issuer_application_data", StringUtilities.leftJustify(input.getValue(tag),64,' '));
                  break;
                case(mesConstants.EMV_TAG_UNPREDICTABLE_NUM):
                    ffd.setFieldData("unpredictable_number", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_APP_TRAN_CNTR):
                    ffd.setFieldData("application_transaction_counter", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_TVR):
                    ffd.setFieldData("terminal_verification_results", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_TERMINAL_TRAN_DATE):
                    ffd.setFieldData("transaction_dt", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_TRAN_TYPE_REC):
                    ffd.setFieldData("transaction_type", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_CRYPTO_AMOUNT):
                    ffd.setFieldData("amount_authorized", StringUtilities.rightJustify(input.getValue(tag),12,'0'));
                    break;
                case(mesConstants.EMV_TAG_CURRENCY_CODE):
                    ffd.setFieldData("terminal_transaction_currency_code",input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_COUNTRY_CODE):
                    ffd.setFieldData("terminal_country_code",input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_APP_INTERCHG_PROFILE):
                    ffd.setFieldData("application_interchange_profile", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_AMT_OTHER):
                    ffd.setFieldData("amount_other", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_APP_PAN_SEQ_NUM):
        	        ffd.setFieldData("application_pan_sequence_number", input.getValue(tag));
                    break;
                case(mesConstants.EMV_TAG_CRYPTO_INFO_DATA):
                    ffd.setFieldData("cryptogram_information_data", input.getValue(tag));
                    break;
              }   		  
            
              } catch (Exception ex){
                  //Continue processing even if unable to read specific tags
                  SyncLog.LogEntry("com.mes.settlement.SettlementRecordAmex::buildFirstPresentmentRecords() - unable to read tag", ex.toString());
              } 
          }
          
          ffd.setAllFieldData(this);
          recs.add(ffd);
        }
    	
    }catch(Exception e){
    	log.error("Not generating EMV TAA inside SettlementRecordAmex - buildFirstPresentmentRecords() receiving invalid icc data " 
				 + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
    }
    

    return( recs );
  }
  
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    List            recs      = null;
    
    // format fields correctly
    CurrencyDecimalPlaces     = CurrencyCodeConverter.getFractionDigits(getString("currency_code","840"));
    
    switch(messageType)
    {
      case MT_FIRST_PRESENTMENT:
        recs = buildFirstPresentmentRecords();
        break;

//      case MT_SECOND_PRESENTMENT:
//        recs = buildSecondPresentmentRecords();
//        break;

//      case MT_FEE_COLLECTION:
//        recs = buildFeeCollectionRecords();
//        break;
    }

    return( recs );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("amexData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
          // duplicate fields, different names
    fgroup.add( new Field         ( "approval_code"             , "Approval Code"           , 6   , 8 , true  ) );          // "auth_code"
    fgroup.add( new Field         ( "cardholder_reference_number","Cardholder Ref Number"   ,17   ,19 , true  ) );          // "customer_code"
    fgroup.add( new Field         ( "roc_number"                , "ROC Number"              ,30   ,32 , true  ) );          // "purchase_id"
    fgroup.add( new Field         ( "shipped_to_zip"            , "Shipped To ZIP Code"     ,15   ,17 , true  ) );          // "ship_to_zip"
    fgroup.add( new NumberField   ( "transaction_id"            , "Auth Tran ID"            ,15   ,17 , true  , 0 ) );      // "auth_tran_id"

          // special amex fields
    fgroup.add( new NumberField   ( "amex_se_number"            , "Amex SE Number"          ,15   ,17 , false , 0 ) );
    fgroup.add( new Field         ( "format_code"               , "Format Code"             , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "media_code"                , "Media Code"              , 2   , 4 , false ) );
    fgroup.add( new Field         ( "special_program_code"      , "Special Program Code"    , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "submission_method"         , "Submission Method"       , 2   , 4 , false ) );
    fgroup.add( new Field         ( "tax_type_code"             , "Tax Type Code"           , 3   , 5 , true  ) );

          // special amex travel fields
    fgroup.add( new Field         ( "airline_process_id"        , "Airline Process ID"      , 3   , 5 , false ) );
    fgroup.add( new Field         ( "carrier_code"              , "Carrier Code"            , 2   , 4 , false ) );
    fgroup.add( new Field         ( "carrier_name"              , "Carrier Name"            ,25   ,27 , false ) );
    fgroup.add( new DateField     ( "departure_date"            , "Departure Date"          ,           true  ) );
    fgroup.add( new NumberField   ( "doc_type"                  , "Doc Type"                , 2   , 4 , false , 0 ) );
    fgroup.add( new Field         ( "fare_basis"                , "Fare Basis"              ,24   ,26 , false ) );
    fgroup.add( new Field         ( "issue_city"                , "Issue City"              ,18   ,20 , false ) );
    fgroup.add( new Field         ( "passenger_name"            , "Passenger Name"          ,25   ,27 , false ) );
    fgroup.add( new Field         ( "statement_city_begin"      , "Statement City Begin"    ,18   ,20 , false ) );
    fgroup.add( new Field         ( "statement_city_end"        , "Statement City End"      ,18   ,20 , false ) );
    fgroup.add( new Field         ( "statement_region_begin"    , "Statement Region Begin"  , 3   , 5 , false ) );
    fgroup.add( new Field         ( "statement_region_end"      , "Statement Region End"    , 3   , 5 , false ) );
    fgroup.add( new Field         ( "statement_country_begin"   , "Statement Country Begin" , 3   , 5 , false ) );
    fgroup.add( new Field         ( "statement_country_end"     , "Statement Country End"   , 3   , 5 , false ) );
    fgroup.add( new NumberField   ( "ticket_number"             , "Ticket #"                ,14   ,16 , true  , 0 ) );
    fgroup.add( new Field         ( "tran_type"                 , "Tran Type"               , 3   , 5 , false ) );
    
    //Amex Tokenization field
    fgroup.add( new Field         ( "iso_ecommerce_indicator"   , "Iso Ecommerce Indicator" , 2   , 4 , false ) );
    // for ALL DateField items: if don't ensure date format and date format input are the same, will lose dates if use WRS "Reject Admin"
    ((DateField)getField("departure_date"       )).setDateFormat("MM/dd/yyyy");
    
    // fields for ICC
    fgroup.add( new Field         ( "icc_data"         , "EMV data (icc)"       , 255   , 255 , true  ) );

 }    
  
  public boolean isCardNumberValid()
  {
    boolean retVal  = false;
    try
    {
      String  cardNumber  = getString("card_number_full");
      if( cardNumber.length() == 15 )
      {
        int sum = 0;

        for( int i = (15-2); i >= 0; i-=2 )
        {
          int tempInt  = Character.digit(cardNumber.charAt(i),10) * 2;
          sum         += (tempInt/10) + (tempInt%10);
          sum         += Character.digit(cardNumber.charAt(i-1),10);
        }
        sum = (10-(sum%10)) % 10;

        if( sum == Character.digit(cardNumber.charAt(15-1),10) )
        {
          retVal = true;    // correct length & mod-10 check digit, so card number is valid
        }
      }
    }
    catch( Exception ee )
    {
    }
    return( retVal );
  }
  
  public boolean isMerchantNumberValid()
  {
    return( TridentTools.isValidSeNumber(getData("amex_se_number")) );
  }

 public boolean isOptMerchant(){
	 PropertiesFile props = new PropertiesFile("optblue.properties");
	 String sid = "";
	 if("3943".equals(getData("bank_number")))
		 sid = props.getString("se_list_"+ 3943);
	 else
		 sid = props.getString("se_list_" + 9999);
	 return(  sid.indexOf(getData("amex_se_number")) >= 0   );
 }
 
  public void setFieldsBatchData(VisakBatchData batchData)
    throws java.sql.SQLException
  {
    if( batchData != null )
    {
      super.setFieldsBatchData(batchData);

      setData( "amex_se_number"           , batchData.getAmexSeNumber());
      MerchantIsProcessedByMeS            = batchData.getAmexSeIsAggregator();
    }
  }

  public void setFields_Amex()
    throws java.sql.SQLException
  {
	ArdefDataAmex ardef = SettlementDb.loadArdefAmex(getString("card_number_full").substring(0,6));
	    // set fields that depend on the ARDEF data
    if (ardef != null && ardef.getProductCode() != null && ardef.getProductCode().equalsIgnoreCase("IB"))
    {
      setArdefData(ardef);
    }
    else
    {
    // set fields that depend on the ARDEF data (or simple card-type defaults)
      setArdefData();
    }

    // cannot call until after batch_id and batch_record_id are set
    setAuthData();
  }

  public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType)
    throws java.sql.SQLException
  {
    RecBatchType = batchType;
    super.setFieldsBase(batchData, resultSet, batchType);

    boolean   isReturn = "C".equals(getData("debit_credit_indicator"));

    String  mediaCode  = null;
    switch( (getData("moto_ecommerce_ind") + " ").charAt(0) )
    {
      case ' ': mediaCode = "01";   break;    // not a moto tran        = signature on file
      case '1': mediaCode = "02";   break;    // moto                   = phone order
      case '2': mediaCode = "05";   break;    // recurring tran         = recurring billing
      case '3': mediaCode = "05";   break;    // installment payment    = recurring billing
      case '4': mediaCode = "03";   break;    // unknown/other moto     = mail order
      case '5': mediaCode = "04";   break;    // secure ecomm           = internet order
      case '6': mediaCode = "04";   break;    // non-auth secure, merch = internet order
      case '7': mediaCode = "04";   break;    // non-auth secure tran   = internet order
      case '8': mediaCode = "04";   break;    // non-secure tran        = internet order
    }
    setData( "media_code"               , mediaCode  );
    setData( "submission_method"        , "03"       );

    if( "256s".equals( getData("load_filename").substring(0,4) ) )    // if Sabre, then travel info
    {
      setData( "airline_process_id"       , "890"                         );
      setData( "departure_date"           , getDate("transaction_date")   );
      setData( "doc_type"                 , "18"                          );
      setData( "fare_basis"               , "SERVICE FEE"                 );

      String  dbaCity = getData("dba_city");
      setData( "issue_city"               , dbaCity.substring(0,7) + "-" + dbaCity.substring(7) );
      setData( "tran_type"                , isReturn ? "REF" : "MSC"      );

      if( getData("auth_tran_id").equals(getData("acq_reference_number")) )
        setData( "auth_tran_id"             , ""                            );

      // format of dba_name: 12 char name, 13 more chars
      //  (13 could be could be '000' + acq-ref-num, 3-char carrier code + ticket #, 'void' + more info, passenger name, ??)
      // -- if don't have at least the 12 + the 3, or if the 3 are '000' then ignore
      String[]  carrierInfo   = { "" , "SERVICE FEE" };
      String    ticketingInfo = getData("dba_name");
      if( ticketingInfo.length() >= 15 &&
          !"000".equals(ticketingInfo.substring(12,15)) )
      {
        if( Character.isDigit( ticketingInfo.charAt(12) ) &&
            Character.isDigit( ticketingInfo.charAt(13) ) &&
            Character.isDigit( ticketingInfo.charAt(14) ) )
        {
          carrierInfo   = (SettlementDb.loadAirlineCarrierCodes( ticketingInfo.substring(12,15) )).split(",");
        }
        setData( "ticket_number"            , ticketingInfo.substring(12)   );
      }
      else
      {
        setData( "ticket_number"            , String.valueOf( getLong("acq_reference_number")%10000000000L ) );
      }
      setData( "carrier_code"             , carrierInfo[0]                );
      setData( "carrier_name"             , carrierInfo[1]                );
    }

    setFields_Amex();
  }
  
  public void setFieldsVisak(VisakBatchData batchData, int recIdx)
    throws java.sql.SQLException
  {
    super.setFieldsVisak(batchData, recIdx);

    setData( "media_code"               , SettlementDb.decodeVisakValue("AMEX",FT_ID_METHOD, getData( "cardholder_id_code" )) );
    setData( "submission_method"        , SettlementDb.decodeVisakValue("AMEX",FT_INPUT_CAP, getData( "device_code"        )) );

    setFields_Amex();
  }

  public void fixBadData()
  {
    super.fixBadData();

    if( "256s".equals(getString("load_filename").substring(0,4)) )  setData( "format_code"  , "01" ); // airline-specific TAA
    else if( getDate("statement_date_begin") != null )
    {
      if( getInt("sic_code") == 4411 )                              setData( "format_code"  , "14" ); // travel/cruise-specific TAA
      else                                                          setData( "format_code"  , "11" ); // lodging-specific TAA
    }
    else                                                            setData( "format_code"  , "02" ); //  no ind-specific TAA

      // where fields names/lengths differ between amex_settlement table and SettlementRecord class, set amex fields
    setData( "approval_code"                  , getData( "auth_code"              ) );
    if( isFieldBlank("cardholder_reference_number") )
      setData( "cardholder_reference_number"    , getData( "customer_code"          ) );
    setData( "reference_number"               , getData( "acq_reference_number"   ) );
    setData( "roc_number"                     , getData( "purchase_id"            ) );
    if( isFieldBlank("ship_to_zip") )
      setData( "shipped_to_zip"                 , getData( "dba_zip"                ) );
    else
      setData( "shipped_to_zip"                 , getData( "ship_to_zip"            ) );
    if( isFieldBlank("tax_type_code") )
      setData( "tax_type_code"                  ,("840".equals(getData("currency_code")) ? "056" : "TX " ) );

    // prevent corrupted auth data from passing to settlement
    try {
        setData( "transaction_id"                 , getLong( "auth_tran_id"           ) );
     } catch( Exception badTranId_e ) {  }

    if( MerchantIsProcessedByMeS && getLong("amex_se_number") != 5049892912L )
    {
      setData( "dba_name"                       , "MES*" + getData( "dba_name"      ) );
    }

    //@showData();//@
  }

  public void fixBadDataAfterIcAssignment()
  {
    super.fixBadDataAfterIcAssignment();
  }
}
