/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordDiscover.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-10-18 10:18:17 -0700 (Fri, 18 Oct 2013) $
  Version            : $Revision: 21636 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.mes.constants.FormatTypes;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.utils.TLVUtil;
import masthead.formats.iso.TLVMsg;
import masthead.formats.iso.TlvFieldData;
import masthead.util.ByteUtil;

public class SettlementRecordDiscover extends SettlementRecord
{
    // region codes for the US and US territories =
    // USA,ASM,FSM,GUM,MHL,MNP,PLW,PRI,UMI,VIR
    public static final String US_AND_US_TERRITORIES = "840,016,583,316,584,580,585,630,581,850";
     // map of Vital extension record format to the flat file record definition
    private static final HashMap CdfExtRecToFfdType = new HashMap() {
        {
            put("AIRL1", MesFlatFiles.DEF_TYPE_CDF_PT1_REC);
            put("AIRL2", MesFlatFiles.DEF_TYPE_CDF_PT2_REC);
            put("CARNT", MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC);
            put("OPTIN_C", MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC_OPT);
            put("LODGE", MesFlatFiles.DEF_TYPE_CDF_VISA_HOTEL_REC);
            put("OPTIN_L", MesFlatFiles.DEF_TYPE_CDF_HOTEL_REC_OPT);
        }
    };

    private static final ArrayList<Integer> AirlineMapExtRecToFfdType = new ArrayList<Integer>(){
        {
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_1);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_2);
        }
    };

    private static final ArrayList<Integer> CarRentalMapExtRecToFfdType = new ArrayList<Integer>(){
        {
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR06_SEQ_1);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR06_SEQ_2);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR06_SEQ_3);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR06_SEQ_4);
        }
    };
    private static final ArrayList<Integer> HotelMapExtRecToFfdType = new ArrayList<Integer>(){
        {
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR09_SEQ_1);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR09_SEQ_2);
            add(MesFlatFiles.DEF_TYPE_DISCOVER_SDR09_SEQ_3);
        }
    };

    private FormatTypes sicGroup = FormatTypes.OTHER;     // default to "other"
  
  public SettlementRecordDiscover()
  {
    super(mesConstants.MBS_BT_VISAK, "DS", SettlementRecord.SETTLE_REC_DISC);
  }

  public List buildFirstPresentmentRecords()
    throws Exception
  {
    List            recs      = new ArrayList(1);
    FlatFileRecord  ffd       = null;
    
    // Detail V101
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_DETAIL_V101);
    ffd.setAllFieldData(this);

    // fix fields where the data in discover_settlement needs to be manipulated for the flat file
    //    1) if auth date and/or time is blank, stuff with transaction date and/or time
    //    2) need to specifically get the time component for tran & auth times
    if( isFieldBlank("auth_date") ) ffd.setFieldData("auth_date"        ,getDate("transaction_date"                       ) );
    if( isFieldBlank("auth_time") ) ffd.setFieldData("auth_time"        ,getDate("transaction_time" ,"MM/dd/yyyy HH:mm:ss") );
    else                            ffd.setFieldData("auth_time"        ,getDate("auth_time"        ,"MM/dd/yyyy HH:mm:ss") );
                                    ffd.setFieldData("transaction_time" ,getDate("transaction_time" ,"MM/dd/yyyy HH:mm:ss") );
    //    3) add four trailing zeroes to process code
    ffd.setFieldData("process_code",getString("process_code") + "0000");  // unspecified "from" and "to" accounts
    
    //    4) decrypt and set card number from card_number_enc if full card is not available
    if( isFieldBlank("card_number_full") ) ffd.setFieldData("card_number_full", getCardNumberFull());
    
    recs.add(ffd);

    // check for extension record data
    if ( getString("client_data") != null )
    {
      // Detail Ext V101
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_DETAIL_EXT_V101);
      ffd.setAllFieldData(this);
      recs.add(ffd);
    }

    //check for surcharge amount
    if(getDouble("tran_fee_amount") != 0.0 ){
    	String tranFeeAmount = TridentTools.encodeCobolAmount(getDouble("tran_fee_amount"), 10);
    	ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_DETAIL_SDR19_V191);
    	ffd.setAllFieldData(this);
    	ffd.setFieldData("surcharge_amount", tranFeeAmount);
    	recs.add(ffd);
    }
   /* Blocking the creation of SDR 05, 06, 09 as it's descoped for April-13-2018 release  
    * if (isQualifiedSICForSDRBuild()) {
        buildSDRFromD256ErData(recs);
    }*/

    /*
     * RS Software - EMV modification - start 
     * Fixed icc_data2 record and duplicated record
     * Keep existing logic intact
     */
    if (isEmvEnabled && !isFieldBlank("icc_data")) {
        Map<Integer,Integer> tagSpaceMap = new HashMap<>();

    	// ICC / EMV data
    	String emvData = getData("icc_data");
    	
    	// sequence 1
    	ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_DETAIL_ICC1_V101);
    	// sequence 2
    	FlatFileRecord ffd2 = getFlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_DETAIL_ICC2_V101);

    	TlvFieldData input = new TlvFieldData();

			if (emvData.matches(TLVUtil.IS_HEX_STRING_WITH_SPACE) && emvData.contains(" ")) {
					tagSpaceMap = TLVUtil.tlvSpaceCalculation(emvData);
					input.unpack(TLVUtil.hexStringToByteArray(emvData), 0, true);
			}
			else {
				input.unpack(ByteUtil.parseHexString(emvData), 0, true);
			}
    	// Parse data
    	// tags is a collection of TLVMsg
    	Enumeration tags = input.elements();
    	while (tags.hasMoreElements()) {
    		try {
    			TLVMsg tlvMsg = (TLVMsg) tags.nextElement();
    			// extract the current tag from the TLV
    			int tag = tlvMsg.getTag();
    			// Write tag values to appropriate field
    			switch (tag) {
    			case (mesConstants.EMV_TAG_APP_INTERCHG_PROFILE):
    				ffd.setFieldData("application_interchange_profile", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag),' '));
    			break;
    			case (mesConstants.EMV_TAG_APP_TRAN_CNTR):
    				ffd.setFieldData("application_transaction_counter", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag),' '));
    			break;
    			case (mesConstants.EMV_TAG_APP_CRYPTOGRAM):
    				ffd.setFieldData("application_cryptogram", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag),' '));
    			break;
    			case (mesConstants.EMV_TAG_CRYPTO_AMOUNT):
    				ffd.setFieldData("amount_authorized", input.getValue(tag));
    			break;
    			case (mesConstants.EMV_TAG_CRYPTO_INFO_DATA):
    				ffd.setFieldData("cryptogram_information_data", input.getValue(tag));
    			break;
    			case (mesConstants.EMV_TAG_COUNTRY_CODE):
    				// Added for EMV by RS software--Start
    				// fix for numeric data too long exception
    				String countryCode = input.getValue(tag);
    			if (countryCode != null && countryCode.length() == 4) {
    				countryCode = countryCode.substring(1);
    			}
    			ffd.setFieldData("terminal_country_code", countryCode);
    			// Added for EMV by RS software--End
    			break;
    			case (mesConstants.EMV_TAG_TERMINAL_CAPABILITIES):
    				ffd.setFieldData("terminal_capabilities", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag),' '));
    			break;
    			case (mesConstants.EMV_TAG_TERMINAL_TYPE):
    				ffd.setFieldData("terminal_type", input.getValue(tag));
    			break;
    			case (mesConstants.EMV_TAG_TVR):
    				ffd.setFieldData("terminal_verification_results", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' '));
    			break;
    			case (mesConstants.EMV_TAG_TERMINAL_TRAN_DATE):
    				ffd.setFieldData("transaction_date", input.getValue(tag));
    			break;
    			case (mesConstants.EMV_TAG_TRAN_TYPE_REC):
    				ffd.setFieldData("transaction_type", input.getValue(tag));
    			break;
    			case (mesConstants.EMV_TAG_CURRENCY_CODE):
    				// Added for EMV by RS software--Start
    				// fix for numeric data too long exception
    				String currencyCode = input.getValue(tag);
    			if (currencyCode != null && currencyCode.length() == 4) {
    				currencyCode = currencyCode.substring(1);
    			}
    			ffd.setFieldData("transaction_currency_code", currencyCode);
    			// Added for EMV by RS software--End
    			break;
    			case (mesConstants.EMV_TAG_ISSUER_APP_DATA):
    				// first 32 bytes go to sequence 1, the rest if any to sequence 2
    				String appData = TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' ');
    			if (appData.length() > 32) {
    				ffd.setFieldData("issuer_application_data_1", appData.substring(0, 32));
    				ffd2.setFieldData("issuer_application_data_2", appData.substring(33, appData.length()));
    			} else {
    				ffd.setFieldData("issuer_application_data_1", StringUtilities.leftJustify(appData, 32, ' '));
    			}
    			break;
    			case (mesConstants.EMV_TAG_UNPREDICTABLE_NUM):
    				ffd.setFieldData("unpredictable_number", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' '));
    			break;
    			case (mesConstants.EMV_TAG_ISSUER_AUTH_DATA):
    				String issuerAuthData =  TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' ');

    				ffd.setFieldData("issuer_authentication_data", issuerAuthData);
    			break;
    			case (mesConstants.EMV_TAG_APP_USAGE):
    				ffd.setFieldData("application_usage_control", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' '));
    			break;
    			case (mesConstants.EMV_TAG_CARDHOLDER_VERIF):
    				ffd.setFieldData("cardholder_verification_method_results", TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' '));
    			break;
    			}

    		} catch (Exception ex) {
    			// Continue processing even if unable to read specific tags
    			SyncLog.LogEntry("com.mes.settlement.SettlementRecordDiscover::buildFirstPresentmentRecords() - unable to read tag",
    					ex.toString());
    		}
    	}
    	ffd.setAllFieldData(this);
    	recs.add(ffd);
    	String emvData2 = getData("icc_data_2");

    	// Add ICC_DATA_2 in settlement file
    	// ICC / EMV data
    	input = new TlvFieldData();
    	tagSpaceMap =  new HashMap<>();
    	
			if (emvData2.matches(TLVUtil.IS_HEX_STRING_WITH_SPACE) && emvData2.contains(" ")) {
					tagSpaceMap = TLVUtil.tlvSpaceCalculation(emvData2);
					input.unpack(TLVUtil.hexStringToByteArray(emvData2), 0, true);
			}
			else {
				input.unpack(ByteUtil.parseHexString(emvData2), 0, true);
			}
     	
    	// Parse data
    	// tags is a collection of TLVMsg
    	tags = input.elements();
    	while (tags.hasMoreElements()) {
    		try {
    			TLVMsg tlvMsg = (TLVMsg) tags.nextElement();
    			// extract the current tag from the TLV
    			int tag = tlvMsg.getTag();
    			// Write tag values to appropriate field
    			switch (tag) {
    			case (mesConstants.EMV_TAG_ISSUER_APP_DATA):
    			    String issuerAppData = TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' ');
    			
    			  if(StringUtilities.isAllZeroes(issuerAppData))
    				  ffd2.setFieldData("issuer_application_data_2", issuerAppData.replace('0', ' '));
    			  else
    			      ffd2.setFieldData("issuer_application_data_2", StringUtilities.leftJustify(issuerAppData, 32,' '));
    			break;
    			case (mesConstants.EMV_TAG_ISSUER_SCRIPT):
    			  String issuerScript =  TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' ');
    			if(StringUtilities.isAllZeroes(issuerScript))
    			  ffd2.setFieldData("issuer_script_results", issuerScript.replace('0', ' '));
    			else
            ffd2.setFieldData("issuer_script_results", StringUtilities.leftJustify(issuerScript, 50,' '));
    			break;
    			case(mesConstants.EMV_TAG_DEDICATED_FILE_NAME):
    				String dedicatedFileName = TLVUtil.tlvDataLeftPadding(tagSpaceMap, tag, input.getValue(tag), ' ');
    			    ffd2.setFieldData("dedicated_file_name", StringUtils.stripStart(dedicatedFileName, "0"));
    			break;
    			}
    		} catch (Exception ex) {
    			// Continue processing even if unable to read specific tags
    			SyncLog.LogEntry("com.mes.settlement.SettlementRecordDiscover::buildFirstPresentmentRecords() - unable to read tag for icc_data_2",
    					ex.toString());
    		}
    	}

    	ffd2.setAllFieldData(this);
    	recs.add(ffd2);
    }
    // RS Software - EMV modification - end

    return( recs );
  }

    private boolean isQualifiedSICForSDRBuild() {
        boolean returnValue = false;

        final String[][] sicGroups = {
                // 0 , 1
                // group , sic list
                { "AIRLINES", "3000-3300,4112,4411,4511" }, // Airlines 05
                { "CARRENTALS", "3351-3441,7512,7513,7519" }, // Car Rentals 06
                { "HOTELS", "3501-3999,7011,7012" }, // Hotels 09
        };

        for (int i = 0; i < sicGroups.length; ++i) {
            if (isSicInList(this.getString("sic_code"), sicGroups[i][1])) {
                sicGroup = FormatTypes.getFormatType(sicGroups[i][0]);
                break;
            }
        }

        if ((sicGroup != FormatTypes.OTHER) && (this.getString("load_filename") != null
                && this.getLong("batch_record_id") > 0 && this.getDate("batch_date") != null)) {
            returnValue = true;
        }
        return returnValue;
    }

    private void buildSDRFromD256ErData(List recs) {

        final String optin = "OPTIN";
        final String optinCarRental = "OPTIN_C";
        final String optinLodge = "OPTIN_L";
        final String airl2 = "AIRL2";
        FlatFileRecord ffd = null;
        try {

            List<DailyDetailFileD256ER> rsExtraLst = SettlementDb.getD256ErData(this.getString("load_filename"),
                    this.getLong("batch_record_id"), new java.sql.Date(this.getDate("batch_date").getTime()), sicGroup);
            List<FlatFileRecord> cdfFileRecords = new ArrayList<FlatFileRecord>();
            List<FlatFileRecord> cdfAirline2Records = new ArrayList<FlatFileRecord>();
            int airline2Count = 0;

            if (rsExtraLst != null && !rsExtraLst.isEmpty()) {
                for (DailyDetailFileD256ER rsExtra : rsExtraLst) {
                    String formatIndicator = optin.equals(rsExtra.getFormatIndicator())
                            ? ((sicGroup == FormatTypes.CARRENTALS) ? optinCarRental
                                    : (sicGroup == FormatTypes.HOTELS) ? optinLodge : rsExtra.getFormatIndicator())
                            : rsExtra.getFormatIndicator();
                    String rawData = fixDataLength(rsExtra.getData());
                    if (formatIndicator != null && rawData != null) {
                        Integer deftype = (Integer) CdfExtRecToFfdType.get(formatIndicator);
                        if (deftype != null && deftype.intValue() > 0) {
                            FlatFileRecord additionalffd = new FlatFileRecord(deftype);
                            additionalffd.suck(rawData);
                            cdfFileRecords.add(additionalffd);
                            if (airl2.equals(formatIndicator)) {
                                airline2Count++;
                                cdfAirline2Records.add(additionalffd);
                            }
                        }
                    }
                }
            }

            if (!cdfFileRecords.isEmpty()) {
                switch (sicGroup) {
                case AIRLINES:
                    for (Integer outgoingFormatType : AirlineMapExtRecToFfdType) {
                        List<Boolean> populatedFlagList = new ArrayList<Boolean>();
                        ffd = getFlatFileRecord(outgoingFormatType.intValue());
                        for (FlatFileRecord flatFile : cdfFileRecords) {
                            populatedFlagList.add(ffd.setAllSDRFieldData(flatFile, sicGroup.getFormatType(), -1));
                        }
                        if (!populatedFlagList.isEmpty() && populatedFlagList.contains(true)) {
                            recs.add(ffd);
                        }
                    }
                    for (int i = 0; i < airline2Count; i++) {
                        List<Boolean> populatedFlagList = new ArrayList<Boolean>();
                        ffd = getFlatFileRecord(getSeqVal(i));
                        for (FlatFileRecord flatFile : cdfAirline2Records) {
                            populatedFlagList.add(ffd.setAllSDRFieldData(flatFile, sicGroup.getFormatType(), i));
                        }
                        if (!populatedFlagList.isEmpty() && populatedFlagList.contains(true)) {
                            recs.add(ffd);
                        }
                    }
                    break;
                case CARRENTALS:
                case HOTELS:
                    List<Integer> outgoingFileTypes = (sicGroup == FormatTypes.CARRENTALS) ? CarRentalMapExtRecToFfdType
                            : HotelMapExtRecToFfdType;
                    for (Integer outgoingFormatType : outgoingFileTypes) {
                        List<Boolean> populatedFlagList = new ArrayList<Boolean>();
                        ffd = getFlatFileRecord(outgoingFormatType.intValue());
                        for (FlatFileRecord flatFile : cdfFileRecords) {
                            populatedFlagList.add(ffd.setAllSDRFieldData(flatFile, sicGroup.getFormatType(), -1));
                        }
                        if (!populatedFlagList.isEmpty() && populatedFlagList.contains(true)) {
                            recs.add(ffd);
                        }
                    }
                    break;
                default:
                    log.info("not a valid sicgroup");
                    break;
                }
            }

        } catch (Exception e) {
            SyncLog.LogEntry(
                    "com.mes.settlement.SettlementRecordDiscover::buildSDRFromD256ErData() - Exception occured while constructing SDR ",
                    e.toString());
        }
    }
  
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    List            recs      = null;
    
    switch(messageType)
    {
      case MT_FIRST_PRESENTMENT:
        recs = buildFirstPresentmentRecords();
        break;
        
//      case MT_SECOND_PRESENTMENT:
//        recs = buildSecondPresentmentRecords();
//        break;
        
//      case MT_FEE_COLLECTION:
//        recs = buildFeeCollectionRecords();
//        break;
        
//      case MT_VISA_MPS:
//        recs = buildMerchantProfileRecords();
//        break;
    }
    
    return( recs );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("discData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
          // duplicate fields, different names OR field info based on info from a standard field
    fgroup.add( new Field         ( "approval_code"             , "Approval Code"           , 6   , 8 , true  ) );          // "auth_code"
    fgroup.add( new Field         ( "network_reference_id"      , "Network Ref ID"          ,15   ,17 , false ) );          // "auth_tran_id"
//  fgroup.add( new Field         ( "pos_entry_mode"            , "POS Entry Mode"          , 3   , 5 , true  ) );          // "pos_entry_mode" converted to discover + '0'
//  fgroup.add( new Field         ( "purchase_id"               , "Purchase ID"             ,16   ,18 , true  ) );          // "purchase_id"'s last 16 chars
    fgroup.add( new Field         ( "response_code"             , "Response Code"           , 2   , 4 , true  ) );          // "auth_response_code"
//  fgroup.add( new NumberField   ( "reference_number"          , "Reference Number"        ,15   ,17 , false , 0 ) );      // "acq_reference_number"

          // special Discover fields
    fgroup.add( new NumberField   ( "acquirer_id"               , "Acquirer ID"             ,11   ,13 , true  , 0 ) );
    fgroup.add( new Field         ( "client_code"               , "Client Code"             , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "client_data"               , "Client Data"             ,40   ,42 , true  ) );
    fgroup.add( new Field         ( "client_prefix"             , "Client Prefix"           , 2   , 4 , true  ) );
    fgroup.add( new NumberField   ( "discover_merchant_number"  , "Discover Merchant Number",15   ,17 , false , 0 ) );
    fgroup.add( new Field         ( "passenger_name"            , "Passenger Name"          ,20   ,22 , true  ) );
    fgroup.add( new Field         ( "pos_data"                  , "POS Data Code"           ,13   ,15 , true  ) );
    fgroup.add( new Field         ( "process_code"              , "Process Code"            , 2   , 4 , false ) );
    fgroup.add( new Field         ( "track_condition_code"      , "Track Condition Code"    , 2   , 4 , true  ) );
    fgroup.add( new Field         ( "tran_type"                 , "Tran Type"               , 2   , 4 , false ) );
    fgroup.add( new HiddenField   ( "icc_data_2"                ) );

    fgroup.add( new Field         ( "card_program_id"           , "Card Program ID"         , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "icp_column"                , "ICP Column"              ,16   ,18 , true  ) );
    fgroup.add( new Field         ( "interchange_program"       , "Interchange Program"     , 3   , 5 , true  ) );
    fgroup.add( new Field		  ( "disc_partial_auth_ind" 	, "Discover Partial Auth Ind"		  , true  ) );
    
    // fields for ICC
    fgroup.add( new Field         ( "icc_data"         , "EMV data first part (icc)"  , 167   , 167 , true  ) );
    fgroup.add( new Field         ( "icc_data_2"       , "EMV data second part (icc)" , 158   , 158 , true  ) );
  }    
  
  public boolean isMerchantNumberValid()
  {
    String  merchNum = getData("discover_merchant_number");
    return( merchNum.length() == 15 && TridentTools.mod10Check(merchNum.substring(4)) );
  }
  
	protected void setAuthData(TridentAuthData authData) {
		super.setAuthData(authData);
		setData("disc_partial_auth_ind", getDiscoverPartialAuthInd(RecBatchType, authData));
	}

	protected String getDiscoverPartialAuthInd(int recBatchType, TridentAuthData authData) {
		if (recBatchType == mesConstants.MBS_BT_PG) {
			if (DbBooleanType.TRUE.strValue.equals(getData("is_partial_auth_enabled"))) {
				return DISC_PARTIAL_AUTH_ENABLED;
			}
			return DISC_PARTIAL_AUTH_DISABLED;
		}
		if (authData != null && recBatchType == mesConstants.MBS_BT_VISAK) {
			if (DbBooleanType.TRUE.strValue.equals(authData.getPartialAuthInd())) {
				return DISC_PARTIAL_AUTH_ENABLED;
			}
			return DISC_PARTIAL_AUTH_DISABLED;
		}
		if (authData != null && recBatchType == mesConstants.MBS_BT_VITAL) {
			return authData.getDiscPartialAuthInd();
		}
		return null;
	}

	public void setFieldsBatchData(VisakBatchData batchData) throws java.sql.SQLException {
		if (batchData != null) {
			super.setFieldsBatchData(batchData);

			setData("acquirer_id", batchData.getDiscoverAcquirerID());
			setData("discover_merchant_number", batchData.getDiscoverMID());
			MerchantIsProcessedByMeS = batchData.getDiscoverMIDIsAggregator();
		}
	}

  public void setFields_Discover()
    throws java.sql.SQLException
  {
    String            cardNumber  = getData("card_number_full");
    ArdefDataDiscover ardef       = SettlementDb.loadArdefDiscover(cardNumber);

    // set fields that depend on the ARDEF data
    setArdefData(ardef);
    setData( "region_issuer"        , ardef.getIssuerCountryCode() );
    if( isFieldBlank("currency_code"      ) )
      setData( "region_merchant"      , getData("funding_currency_code") );
    else
      setData( "region_merchant"      , getData("currency_code") );

    // cannot call until after batch_id and batch_record_id are set
    setAuthData();


    String almCode = "Z";
    if( ardef.getAccountLevelInd() == 1 && !isFieldBlank("auth_tran_id") )
    {
      String authCode = getData("auth_code");
      if( authCode.length() == 6 )
      {
        char sixthChar = (authCode).charAt(5);
        if( Character.isLetter(sixthChar) ) almCode = String.valueOf(sixthChar);
      }
    }
    setData( "alm_code"               , almCode                         );
    setData( "card_program_id"        , ardef.getCardProductId()        );


    // find the correct icp column in discover_ic_program_rules
    String icpColumn = null;
    // card type might be international, so check merchant & issuer regions
          if( US_AND_US_TERRITORIES.indexOf(getData("region_merchant")) < 0 ) { /* log error?? */   }
    else  if( US_AND_US_TERRITORIES.indexOf(getData("region_issuer"  )) < 0 ) { 
    	switch( (getData("alm_code")+".").charAt(0) )
    	{
    	  case 'C':                                                // International Consumer Credit - Core
    	  case 'R':                                                // International Consumer Credit - Rewards
    	  case 'P':                                                // International Consumer Credit - Premium
    	  case 'Q': icpColumn = "intl"  ; break;                   // International Consumer Credit - Premium Plus
    	  case 'B':                                                // International Commercial Credit
    	  case 'E': icpColumn = "intl_comml" ; break;              // International Commercial Credit - Executive Business
    	  default:                                                 // Unspecified Product Type / 'Z'
    	    switch( getInt("card_program_id") )
    	    {
    	      case   3:                                            // International Consumer Debit
    	      case   5:                                            // International Consumer Prepaid - Gift
    	      case   6: icpColumn = "intl_debit"    ; break;       // International Consumer Prepaid - ID Known
    	      case   8:                                            // International Consumer Credit - Core
    	      case   1:                                            // International Consumer Credit - Rewards
    	      case   7:                                            // International Consumer Credit - Premiuim
    	      case  11: icpColumn = "intl"     ; break;            // International Consumer Credit - Premium Plus
    	      case   4:                                            // International Commercial Debit
    	      case   2:                                            // International Commercial Credit
    	      case  10:                                            // International Commercial Credit - Executive Business
			  case  15:                                            // International Commercial Prepaid - Reloadable
    	      case  12: icpColumn = "intl_comml"  ; break;         // International Commercial Prepaid - Non-Reloadable
    	      case  13: icpColumn = "intl"        ; break;         // International PayPal Payment Card

    	      //case   9:                                          // Private label Credit Card
    	    }
    	    break;
    	}
    }
    if( icpColumn == null )
    {
      switch( (getData("alm_code")+".").charAt(0) )
      {
        case 'C': icpColumn = "core"       ; break;     // Consumer Credit - Core
        case 'R': icpColumn = "rewards"    ; break;     // Consumer Credit - Rewards
        case 'P': icpColumn = "premium"    ; break;     // Consumer Credit - Premium
        case 'Q': icpColumn = "prem_plus"  ; break;     // Consumer Credit - Premium Plus
        case 'B': icpColumn = "commercial" ; break;     // Commercial Credit
        case 'E': icpColumn = "commercial" ; break;     // Commercial Credit - Executive Business

        default:                                        // Unspecified Product Type / 'Z'
          switch( getInt("card_program_id") )
          {
            case   3: icpColumn = "debit"         ; break;    // Consumer Debit
            case   5: icpColumn = "prepaid"       ; break;    // Consumer Prepaid - Gift
            case   6: icpColumn = "prepaid"       ; break;    // Consumer Prepaid - ID Known
            case   8: icpColumn = "core"          ; break;    // Consumer Credit - Core
            case   1: icpColumn = "rewards"       ; break;    // Consumer Credit - Rewards
            case   7: icpColumn = "premium"       ; break;    // Consumer Credit - Premiuim
            case  11: icpColumn = "prem_plus"     ; break;    // Consumer Credit - Premium Plus

            case   4: icpColumn = "comm_debit"    ; break;    // Commercial Debit
            case   2: icpColumn = "commercial"    ; break;    // Commercial Credit
            case  10: icpColumn = "commercial"    ; break;    // Commercial Credit - Executive Business
		    case  15:                                         // Commercial Prepaid - Reloadable
            case  12: icpColumn = "comm_prepaid"  ; break;    // Commercial Prepaid - Non-Reloadable

            case  13: icpColumn = "paypal"        ; break;    // PayPal Payment Card


          //case   9:                                         // Private label Credit Card
          }
          break;
      }
    }
    setData( "icp_column"               , icpColumn         );
  }
  
	/**
	 * POS Data Subfields....
	 * 167: 1: Terminal: Attendance Indicator 0=attended, 1=unattended, 2=no POS device, 9=unknown
	 * 168: 2: Merchant: Partial Approval of Merchandise & Cashback 0=not supported, 1=both m&c, 2=only m, 3=only c, 4=neither m nor c
	 * 169: 3: Terminal: Location Indicator 0=at merchant, 1=merchant remote, 2=at cardholder, 3=no pos/voice auth, 9=unknown
	 * 170: 4: Cardholder: Presence 0=present, 1=NP/unspecified, 2=NP/mail, 3=NP/phone, 4=NP/recurring, 5=NP/ecomm, 9=unknown
	 * 171: 5: Card: Presence 0=present, 1= NP, 9=unknown
	 * 172: 6: Terminal: Card Capture Capabilities Indicator 0=no capture, 1=capture, 9=unknown
	 * 173: 7: Transaction: Status Indicator 0=normal request/original presentment, 4=pre-authed request
	 * 174: 8: Transaction: Security Indicator 0=no security concern, 1=merch suspects fraud, 2=ID verified, 3=biometrics verified, 9=unknown
	 * 175: 9: ~ Reserved ~ 0=zero-fill
	 * 176: 10: POS Type of Terminal Device M = Mobile POS, 0 = Unspecified, 9 = Unknown
	 * 177: 11: Terminal: Data Input Capability Indicator 0=unspecified, 1=no pos/voice, 2=mag stripe, 3=bar code, 4=OCR, 5=ICC, 6=keyed, 7=mag stripe &
	 * keyed, R=RFID, S=SET w/cert, T=SET w/out cert, U=SSL, V=nonsecure ecomm
	 * 178: 12: ~ Reserved ~ 0=zero-fill
	 * 179: 13: ~ Reserved ~ 0=zero-fill
	 */
    @Override
	protected void setPosData() {

		setData("pos_data", getDefaultPosData(getPaddedString("moto_ecommerce_ind", 1)));

		// PROD-1571 : Field 22 - POS Data, revised the description of Position 175 from 'Reserved (zero-fill)' to 'E-commerce Transaction Indicator'
		int posConditionCode = 0;
		try {
			posConditionCode = Integer.parseInt(getData("pos_condition_code"));
		}
		catch (NumberFormatException nfe) {
		}

		if (posConditionCode != 0) {
			StringBuilder tempPos = new StringBuilder(getData("pos_data"));

			if (posConditionCode == 8) {
				tempPos.setCharAt(8, '1');
			}
			else if (posConditionCode == 59) {
				tempPos.setCharAt(8, '7');
			}
			else {
				tempPos.setCharAt(8, '0');
			}
			setData("pos_data", tempPos.toString());
		}

		if (StringUtils.isNotBlank(getData("disc_partial_auth_ind"))) {
			StringBuilder tempPos = new StringBuilder(getData("pos_data"));
			tempPos.setCharAt(1, getData("disc_partial_auth_ind").charAt(0));
			setData("pos_data", tempPos.toString());
		}

		// US115: TA98 - Refer TODO-1104
		// Updated position 176 from Reserved to 'POS Type of Terminal Device' as follows: M = Mobile POS, 0 = Unspecified, 9 = Unknown
		String deviceCode = getData("device_code");
		if (deviceCode != null && "G".equals(deviceCode)) {
			StringBuilder tempPos = new StringBuilder(getData("pos_data"));
			tempPos.setCharAt(9, 'M');

			setData("pos_data", tempPos.toString());
		}
	}

	private String getDefaultPosData(String motoEcommInd) {
		String posEntryMode = getData("pos_entry_mode");
		if ("90".equals(posEntryMode)) {
			return "0000090000200";
		}
		else if ("85".equals(posEntryMode)) {
			return "0000090000500";
		}
		else if ("05".equals(posEntryMode)) {
			return "0000090000500";
		}
		else if (DbBooleanType.TRUE.strValue.equals(getData("card_present"))) {
			return "0000090000000";
		}
		else if ("8".equals(motoEcommInd)) {
			return "9095190000V00";
		}
		else if ("567".indexOf(motoEcommInd) >= 0) {
			return "9095190000U00";
		}
		else if ("23".indexOf(motoEcommInd) >= 0) {
			return "9094190000000";
		}
		else if ("1".equals(motoEcommInd)) {
			return "9093190000000";
		}
		return "9091190000000";
	}

	@Override
	public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType) throws java.sql.SQLException {
		RecBatchType = batchType;
		super.setFieldsBase(batchData, resultSet, batchType);

		if ("256s".equals(getData("load_filename").substring(0, 4))) // if Sabre, then travel info
		{
			// @@@@@@@@@@ supposed to be putting last 10 of acq_reference_number after "TKT: 000"
			String clientData = "TKT: 000 PN: " + (getData("passenger_name") + "                 ").substring(0, 17);
			setData("client_data", clientData);
		}

		setFields_Discover();
	}
  
	@Override
	public void setFieldsVisak(VisakBatchData batchData, int recIdx) throws java.sql.SQLException {
		super.setFieldsVisak(batchData, recIdx);

		setFields_Discover();
	}

  public void fixBadData()
  {
    super.fixBadData();

      // where fields names/lengths differ between discover_settlement table and SettlementRecord class, set discover fields
    setData( "approval_code"                  , getData( "auth_code"              ) );
    setData( "network_reference_id"           , getData( "auth_tran_id"           ) );
    setData( "response_code"                  , getData( "auth_response_code"     ) );
    setData( "reference_number"               , getData( "acq_reference_number"   ) );

    // note:  purchase_id is 25 chars in SettlementRecord.java and 16 chars in discover_settlement;
    //        this code removes everything except the last 16 chars
    String    purchaseId    = getData( "purchase_id" );
    int       pidExcess     = purchaseId.length() - 16;
    if( pidExcess > 0 )
    {
      setData( "purchase_id"                    , purchaseId.substring( pidExcess )   );
    }

    // note:  pos_entry_mode is 2 chars in SettlementRecord.java and 3 chars in discover_settlement;
    //        there is a trigger on discover_settlement to add a trailing '0' when the length is 2
    String    posEntryMode  = getData("pos_entry_mode");

    if( "01".equals(posEntryMode) ) setData( "pos_entry_mode", "010" );
    else  if( "10".equals(posEntryMode) ) setData( "pos_entry_mode", "100" );
    else  if( "90".equals(posEntryMode) ) setData( "pos_entry_mode", "020" );
    else  if( "04".equals(posEntryMode) ) setData( "pos_entry_mode", "040" ); 
    else  if( "85".equals(posEntryMode) ) setData( "pos_entry_mode", "85"  ); // Chip Fallback Transaction
    /**else  if( "81".equals(posEntryMode) ) setData( "pos_entry_mode", "81"  ); // Contactless Card with RFID Terminal Input Capability
    else  if( "83".equals(posEntryMode) ) setData( "pos_entry_mode", "83"  );*/ // Contactless Chip with RFID-Chip Terminal Input Capability (not supporting these pos_entry_modes)
    else  if( "04".equals(posEntryMode) ) setData( "pos_entry_mode", "040" );
    else  if( "05".equals(posEntryMode) ) setData( "pos_entry_mode", "050" ); 
    // taken from visa k; not sure if correct
    //@ visaK accountDataSource "Q" --> 00 which will be "00" but should be "01"  // Reserved - Manually keyed bar code capable terminal  
    //@ visaK accountDataSource "R" --> 00 which will be "00" but should be "01"  // Reserved - Manually keyed OCR capable terminal  
    else                                  setData( "pos_entry_mode", "000" );


    String[]  procCodeMap   = new String[]  { "00", "01", "20", "14", "15" };
    String[]  tranTypeMap   = new String[]  { "SA", "CA", "RT", "SA", "SA" };
    int       tranType;
          if( "C".equals(getData("debit_credit_indicator")) )   tranType = 2; // return
    else  if( isCashDisbursement() )                            tranType = 1; // cash advance
    else  if( "2".equals(getData("moto_ecommerce_ind"))     )   tranType = 3; // recurring billing
    else  if( "3".equals(getData("moto_ecommerce_ind"))     )   tranType = 4; // installment payment
    else                                                        tranType = 0; // purchase / sale
    setData( "process_code"                   , procCodeMap[tranType] );
    setData( "tran_type"                      , tranTypeMap[tranType] );

    //@showData();//@
  }

  public void fixBadDataAfterIcAssignment()
  {
    super.fixBadDataAfterIcAssignment();

    // if the FPI starts with a '-' then it was created by MeS; Discover will expect all zeroes
    if( "-".equals(getData("interchange_program").substring(0,1) ) )  setData("interchange_program" , "000");
  }
  
    /**
     * This method is to check if the sic code is part of the listed sic code
     * ranges.
     * 
     * @param sicCode
     * @param tokenList
     * @return
     */
    public static final boolean isSicInList(String sicCode, String tokenList) {
        boolean returnValue = false;
        int dataLength = 4;
        StringBuilder builderSIC = new StringBuilder();
        String stringSIC;

        if ((tokenList == null) || (tokenList.length() < dataLength)) {
            System.out.println("isSicInList(): bad token list..." + tokenList);
            return returnValue;
        }
        if ((sicCode == null) || (sicCode.length() > dataLength)) {
            System.out.println("isSicInList(): bad test data..." + sicCode);
            return returnValue;
        }
        builderSIC.append(sicCode);
        while (sicCode.length() < dataLength) {
            builderSIC.append(" ");
        } // pad string

        stringSIC = builderSIC.toString();
        String[] tokens = tokenList.split(",");
        for (int i = 0; i < tokens.length; ++i) {
            String thisToken = tokens[i];
            int thisLength = thisToken.length();
            if (thisLength == dataLength) {
                // token contains one value; test data matches or not
                if (stringSIC.equals(thisToken)) {
                    returnValue = true;
                }
            } else if ((thisLength == (dataLength * 2) + 1) && (thisToken.charAt(dataLength) == '-')) {
                // token contains two values separated by a '-'; SIC Code is in
                // set (inclusive) or not
                String lo = thisToken.substring(0, dataLength);
                String hi = thisToken.substring(dataLength + 1, thisLength);
                if ((stringSIC.compareTo(lo) >= 0) && (stringSIC.compareTo(hi) <= 0)) {
                    returnValue = true;
                }
            } else {
                // token format is not recognized
                System.out.println("isSicInList(): bad token..." + thisToken);
            }
        }
        return returnValue;
    }

/** This method is to pad the provided data
 * @param data
 * @return formattedData
 */
private String fixDataLength(String data) {
	String formatedData = null;
	final int dataLength = 256;
	final int prefixPad = 16;
	formatedData = data.length() == dataLength?data:data.length()+prefixPad == dataLength?String.format("%1$"+(prefixPad+data.length())+"s", data):
		String.format("%1$-"+(((dataLength - (data.length()+prefixPad)))+(data.length()+prefixPad))+"s", String.format("%1$"+(prefixPad+data.length())+"s", data));
	return formatedData;
}

	/**
	 * This method is to return the respective
	 * flat file def types for SDR05
	 * @param airline2Count
	 * @return int
	 */
	private int getSeqVal(int airline2Count) {
		switch (airline2Count) {
		case 0:
			return MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_3;
		case 1:
			return MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_4;
		case 2:
			return MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_5;
		case 3:
			return MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_6;
		default:
			return MesFlatFiles.DEF_TYPE_DISCOVER_SDR05_SEQ_6;
		}
	}

}
