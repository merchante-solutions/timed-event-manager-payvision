/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordVisa.java $

  Description:

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-05-13 13:05:08 -0700 (Wed, 13 May 2015) $
  Version            : $Revision: 23622 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenDateField;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.CurrencyCodeConverter;
import com.mes.tools.SicCodes;
//TLV tools to extract necessary information from ICC data
import masthead.formats.iso.TLVMsg;
import masthead.formats.iso.TlvFieldData;
import masthead.util.ByteUtil;
import masthead.util.Util13;

public class SettlementRecordVisa extends SettlementRecord
{
  public SettlementRecordVisa()
  {
    super(mesConstants.MBS_BT_VISAK, "VS", SettlementRecord.SETTLE_REC_VISA);
  }

  private static final HashMap  CountryCodeToRegionMap  =
    new HashMap()
    {
      {
        put("BR","5");
        put("US","1");
        put("AS","4");
        put("PR","5");
        put("VI","5");
      }
    };
    
  private List buildFeeCollectionRecords()
    throws Exception
  {
    boolean         isCredit  = "C".equals(getData("debit_credit_indicator"));
    FlatFileRecord  ffd       = null;
    List            recs      = new ArrayList(1);
    String          tc        = "10";   // default to fee collect
    
    if ( isCredit ) 
    {
      tc = "20";    // C = credit issuer via TC20 funds disbursement
    }
    else
    {
      tc = "10";    // D = debit issuer via TC10 fee collection
    }
    
    // TCR 0 (TC10 and TC20 have same TCR0 format)
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC10_TCR0);// 410
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
    ffd.setFieldData("source_bin", getData("bin_number") );
    ffd.setFieldData("event_date", DateTimeFormatter.getFormattedDate(getDate("transaction_date"),"MMdd"));
    ffd.setFieldData("card_number", getCardNumberFull() );
    ffd.setFieldData("source_amount", getDouble("transaction_amount") );
    ffd.setFieldData("source_currency_code", getData("currency_code") );
    ffd.setFieldData("settlement_flag","9");  // 9 = Base II selects 
    recs.add(ffd);
    
    return( recs );
  }
    
  public List buildFirstPresentmentRecords()
    throws Exception
  {
    String          cc        = getData("country_code");
    boolean         isCielo   = (ProcessorType == PROC_TYPE_CIELO);
    boolean         isCredit  = "C".equals(getData("debit_credit_indicator"));      // chargback_visa = "debit_credit_ind"
    List            recs      = new ArrayList(1);
    FlatFileRecord  ffd       = null;
    String          tc        = "05";   // default to sale
    boolean         isChipData = false;
    boolean         isFullChip = false;
    HashMap         chipTagValues = new HashMap();
    String standInProcessingAuthChar = "-";

	String emvData;
	String responseCode = null;
	String posEntryMode = null;
	final String USAGE_CODE_FP_DEFAULT="1";
	int decDigits = CurrencyCodeConverter.getFractionDigits(getString("currency_code", "840"));
    
    // override the transaction code
    if ( isCashDisbursement() )
    {
      tc = "07";
    }
    else if ( isCredit )
    {
      tc = "06";
    }
    
    // before starting, see if we need to remove the online
    // auth data due to it being too old for visa to accept
    if ( isFieldBlank("requested_payment_service") && isAuthExpired() )
    {
      setData( "auth_val_code"    , ""  );
      setData( "auth_tran_id"     , ""  );
      setData( "auth_settled_aci" , "N" );
    }      
    

    // if there are chip data - parse them to include into different TCRs later
    if( isEmvEnabled && !isFieldBlank("icc_data") )
    {
      isChipData = true;
      emvData = getData("icc_data");
      
      //check if it full chip data or early chip data based on response_code_emv
      responseCode = getData("response_code_emv");

      isFullChip = (responseCode != null) && responseCode.startsWith("Y");
      
      //parse EMV data
	  TlvFieldData input = new TlvFieldData();   	 
	  input.unpack(ByteUtil.parseHexString(emvData), 0, true);
	  Enumeration tags = input.elements();
	  while (tags.hasMoreElements()) {
		  try{
			  TLVMsg tlvMsg = (TLVMsg) tags.nextElement();          
			  // extract the current tag from the TLV
			  int tag = tlvMsg.getTag();
			  //Write tag and values in HashTable	    		  
			  String value = input.getValue(tag);
			  if (value != null) {
				  chipTagValues.put(tag, value);	    			  
			  }
	    		
		  } catch (Exception ex){
	    		  //Continue processing even if unable to read specific tags
	    		  SyncLog.LogEntry("com.mes.settlement.SettlementRecordVisa::buildFirstPresentmentRecords() - unable to read tag", ex.toString());
		  }          
	  }
          
           
    }

    // TCR 0
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
    ffd.setFieldData("usage_code",USAGE_CODE_FP_DEFAULT);
    ffd.setFieldData("transaction_amount",Double.parseDouble(MesMath.toFractionalAmount(getDouble("transaction_amount"),decDigits,decDigits,false)));
    
    // prevent field length exceptions
    String  cnExt   = "";
    String  cnFull  = getCardNumberFull();
    if ( cnFull != null && cnFull.length() > 16 )
    {
      cnExt   = cnFull.substring(16);
      cnFull  = cnFull.substring(0,16);
    }
    ffd.setFieldData("card_number"    ,StringUtilities.leftJustify(cnFull,16,'0'));
    ffd.setFieldData("card_number_ext",StringUtilities.leftJustify(cnExt , 3,'0'));
    
    if ( !"US".equals(cc) && !"CA".equals(cc) )
    {
      ffd.setFieldData("dba_state",""); // state allowed only in US and Canada  
    }
    
    if ( isCielo )
    {
      if ( getData("region_issuer").startsWith("BR") && getData("currency_code").equals("986") )
      {
        ffd.setFieldData("settlement_flag","8");
      }
      else
      {
        ffd.setFieldData("settlement_flag","0");
      }
    }
    
    //write pos_entry_mode from chip data
    if (isChipData) 
    {
    	ffd.setFieldData("pos_entry_mode", getData("pos_entry_mode"));
    }
    
    recs.add(ffd);
    
    // TCR 1
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR1);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
    
    if(!isBlank(getData("special_conditions_ind"))){
    	ffd.setFieldData("special_conditions_ind",StringUtilities.rightJustify(getData("special_conditions_ind") , 2,' '));
    }
    
    if(!isBlank(getData("pos_environment"))){
    	ffd.setFieldData("pos_environment",getData("pos_environment"));
    }
    else if ( "Y".equals( getData("recurring_payment_ind") ) )
    {
      ffd.setFieldData("pos_environment","R");
    }
    else if ( "R".equals( getData("recurring_payment_ind") ) || "I".equals( getData("recurring_payment_ind") ) )
    {
      ffd.setFieldData("pos_environment", getData("recurring_payment_ind"));
    }
    if ( isCielo )
    {
      int installmentCount = getInt("multiple_clearing_seq_count");
      if( installmentCount >= 50 )installmentCount -= 50;
      ffd.setFieldData("installment_payment_count",StringUtilities.rightJustify(String.valueOf(installmentCount),2,'0'));
      ffd.setFieldData("message_text"," " + StringUtilities.rightJustify(getData("auth_amount").replaceAll("\\.",""),12,'0'));
      
      if ( getData("region_issuer").startsWith("BR") )
      {
        ffd.setFieldData("account_selection",(getData("card_type").equals("VD") ? "2" : "3"));
      }
      else
      {
        ffd.setFieldData("account_selection","0");
      }
      ffd.setFieldData("fee_program_indicator","");   //@ match SEC
    }
    
    /** As part of CAU-OCT2017 (Visa Clearing - 2.7 Changes to Stand-In Processing) - if auth_source_code is "-" replace the "-" with a space 
     *  Values 1,2,4,5 and C are no longer valid for auth_source_code. Only B and - are valid values. **/
   
   if(standInProcessingAuthChar.equals(getData("auth_source_code")))
       ffd.setFieldData("auth_source_code", " ");
   
    
    recs.add(ffd);
    
    // TCR 2 - Brazil Domestic
    if ( "BR".equals(getData("country_code")) && getData("region_merchant").equals(getData("region_issuer")) )
    {
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR2);
      ffd.setAllFieldData(this);
      
      String  installmentPaymentCount = "";
      String  specialMerchantId       = "";
      
      int installmentNumber= getInt("multiple_clearing_seq_num");
      int installmentCount = getInt("multiple_clearing_seq_count");
      if( installmentCount >= 50 )      // emissor
      {
        installmentPaymentCount = StringUtilities.rightJustify(String.valueOf(installmentCount),2,'0');
      }
      else if ( installmentCount > 0 )  // loja
      {
        specialMerchantId       = "990" + StringUtilities.rightJustify(String.valueOf(installmentCount),2,'0');
        installmentPaymentCount = StringUtilities.rightJustify(String.valueOf(installmentNumber+10),2,'0');
      }
      ffd.setFieldData("special_merchant_id",specialMerchantId);
      ffd.setFieldData("installment_payment_count",installmentPaymentCount);
      ffd.setFieldData("tran_code",tc);
      recs.add(ffd);
    }

    // TCR 3
    switch( getChar("market_specific_data_ind") )
    {
      case 'A':   // auto rental
        ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR3_CAR_RENTAL);
        ffd.setAllFieldData(this);
        ffd.setFieldData("tran_code",tc);
        ffd.setFieldData("business_format_code","CA");
        recs.add(ffd);
        break;
    
      case 'H':   // hotel
        ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR3_LODGING);
        ffd.setAllFieldData(this);
        ffd.setFieldData("tran_code",tc);
        ffd.setFieldData("business_format_code","LG");
        recs.add(ffd);
        break;
    }

    // TCR 4 - Supplemental Financial Data
    double surcharge = getDouble("tran_fee_amount"); 
    if(surcharge != 0) {
	    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR4_SD);
	    ffd.setAllFieldData(this);
	    ffd.setFieldData("transaction_code",tc);
	    ffd.setFieldData("business_format_code", "SD");
	    ffd.setFieldData("surcharge_amount", TridentTools.encodeAmount(surcharge, 8));
	    ffd.setFieldData("surcharge_card_currency", TridentTools.encodeAmount(surcharge, 8));
	    ffd.setFieldData("surcharge_credit_debit_indicator", surcharge < 0 ?  "CR" : "DB" );

	    recs.add(ffd);
    }
    
    // TCR 5
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR5);

      // TCR 5 - Payment Service Data
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
      
    String eflags = getData("eligibility_flags");
    String fpi = getData("fee_program_indicator");
    if ( !isBlank(eflags) && !isBlank(fpi) )
    {
      if( ( eflags.indexOf("UTL") >= 0 && fpi.charAt(0) != 'U' ) ||
          ( eflags.indexOf("TAX") >= 0 && fpi.charAt(0) != 'T' ) )
      {
        // if this is a utilities merchant, but the tran did not get a utilities fpi
        // (US Domestic utilties fpi's are currently UTC,UTD,UBC; assume PR merchants won't have MVV's, at least for now)
        ffd.setFieldData("mvv", "");
      }
    }
    

    recs.add(ffd);
    
    if ( "VB".equals(getData("card_type")) && !isCielo )   // brazil does not use TCR6
    {
      String    messageID = String.valueOf(getData("rec_id"));  // msg ID = rec id with trailing spaces

      //** Uncomment this line when doing file comparisions 
      //messageID = getData("renter_name");//@

      // TCR 6 - Commercial Card Data
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR6);
      ffd.setAllFieldData(this);
      ffd.setFieldData("tran_code",tc);
      ffd.setFieldData("message_identifier",messageID);
      recs.add(ffd);
      
      if ( "Y".equals(getData( "level_iii_data_present" )) && LineItems != null )
      {
        Iterator  i         = LineItems.iterator();
        int       idx       = 0;                                  // seq #  = 000 (summary) & 001-999 (line item details)
        int       lid_ind   = i.hasNext() ? 0 : 1;                // ind    = 1 for last line item, 0 for all others
        
        for( ; lid_ind == 0; ++idx )
        {
          // TC50 - Purchasing Additional Data (Summary) ~or~ Purchasing Line Item Data
          if( idx == 0 )
          {
            ffd =  getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_SUMMARY);
            ffd.setAllFieldData(this);
            ffd.setFieldData("account_number"             , getCardNumberFull()   );
          }
          else
          {
            LineItem item = (LineItem)i.next();
            lid_ind = i.hasNext() ? 0 : 1;

            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_LINE_ITEM);
            ffd.setAllFieldData(item);
            ffd.setFieldData("item_sequence_number"       , idx                   );
            ffd.setFieldData("line_item_detail_indicator" , lid_ind               );
          }
          ffd.setFieldData("destination_bin"            , getData("issuer_bin") );
          ffd.setFieldData("source_bin"                 , getData("bin_number") );
          ffd.setFieldData("message_identifier"         , messageID             );
          recs.add(ffd);
        }
      }
      else if ( "E".equals(getData( "level_iii_data_present" )) )
      {
        // TC50 - Purchasing Additional Data (Summary)
        ffd =  getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_SUMMARY);
        ffd.setAllFieldData(this);
        ffd.setFieldData("account_number"             , getCardNumberFull()   );
        ffd.setFieldData("destination_bin"            , getData("issuer_bin") );
        ffd.setFieldData("source_bin"                 , getData("bin_number") );
        ffd.setFieldData("message_identifier"         , messageID             );
        recs.add(ffd);
        
        // TC50 - Purchasing Line Item Data
        LineItem item = generateLineItem();
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_LINE_ITEM);
        ffd.setAllFieldData(item);
        ffd.setFieldData("item_sequence_number"       , "1"                   );
        ffd.setFieldData("line_item_detail_indicator" , "1"                   );
        ffd.setFieldData("destination_bin"            , getData("issuer_bin") );
        ffd.setFieldData("source_bin"                 , getData("bin_number") );
        ffd.setFieldData("message_identifier"         , messageID             );
        recs.add(ffd);
      }
    }
    //RS Software - EMV modification for full chip start
    //validation added for null value
		if (isFullChip) {
			// TCR 7 -- ICC / EMV full-chip data
			 ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR7  ); //RS Software: Added for EMV 
			if (chipTagValues.get(mesConstants.EMV_TAG_TRAN_TYPE_REC) != null) {
				ffd.setFieldData("transaction_type", chipTagValues.get(mesConstants.EMV_TAG_TRAN_TYPE_REC).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_TERMINAL_TRAN_DATE) != null) {
				ffd.setFieldData("transaction_date",
						chipTagValues.get(mesConstants.EMV_TAG_TERMINAL_TRAN_DATE).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_TERMINAL_CAPABILITIES) != null) {
				ffd.setFieldData("capability_profile",
						chipTagValues.get(mesConstants.EMV_TAG_TERMINAL_CAPABILITIES).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_COUNTRY_CODE) != null) {
				//RS Software: Added for EMV ---start 
				String countryCode = chipTagValues.get(mesConstants.EMV_TAG_COUNTRY_CODE).toString();
    			if (countryCode != null && countryCode.length() == 4) {
    				countryCode = countryCode.substring(1);
    			}
    			ffd.setFieldData("terminal_country_code",
						countryCode);
    			//RS Software: Added for EMV ---end
    			
				//ffd.setFieldData("terminal_country_code",
				//		chipTagValues.get(mesConstants.EMV_TAG_COUNTRY_CODE).toString()); Old code
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_UNPREDICTABLE_NUM) != null) {
				ffd.setFieldData("unpredictable_number",
						chipTagValues.get(mesConstants.EMV_TAG_UNPREDICTABLE_NUM).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_APP_TRAN_CNTR) != null) {
				ffd.setFieldData("application_transaction_counter",
						chipTagValues.get(mesConstants.EMV_TAG_APP_TRAN_CNTR).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_APP_INTERCHG_PROFILE) != null) {
				ffd.setFieldData("application_interchange_profile",
						chipTagValues.get(mesConstants.EMV_TAG_APP_INTERCHG_PROFILE).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_APP_CRYPTOGRAM) != null) {
				ffd.setFieldData("cryptogram", chipTagValues.get(mesConstants.EMV_TAG_APP_CRYPTOGRAM).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_ISSUER_APP_DATA) != null) {
				String issuerAppData = chipTagValues.get(mesConstants.EMV_TAG_ISSUER_APP_DATA).toString();
				if (issuerAppData.length() > 2) {
					ffd.setFieldData("issuer_application_data_1", issuerAppData.substring(2, 3));  //Added by RS software
					//ffd.setFieldData("issuer_application_data1", issuerAppData.substring(2, 3)); //Old code
					
					if (issuerAppData.length() > 6) {
						ffd.setFieldData("issuer_application_data_2", issuerAppData.substring(4, 7));  //Added by RS software
						//ffd.setFieldData("issuer_application_data2", issuerAppData.substring(4, 7)); //Old code
						
						if (issuerAppData.length() > 15) {
							ffd.setFieldData("issuer_application_data_3", issuerAppData.substring(8, 16));  //Added by RS software
							//ffd.setFieldData("issuer_application_data3", issuerAppData.substring(8, 16)); //Old code
							
							if (issuerAppData.length() > 31) {
								ffd.setFieldData("issuer_application_data_5", issuerAppData.substring(17, 32));  //Added by RS software
								//ffd.setFieldData("issuer_application_data5", issuerAppData.substring(17, 32)); //Old code
							}
						}
					}

				}
				if (issuerAppData.length() > 0) {
					ffd.setFieldData("issuer_application_data_4", issuerAppData.substring(0, 1));  //Added by RS software
					//ffd.setFieldData("issuer_application_data4", issuerAppData.substring(0, 1)); //Old code
				}
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_TVR) != null) {
				ffd.setFieldData("terminal_verification_results",
						chipTagValues.get(mesConstants.EMV_TAG_TVR).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_CRYPTO_AMOUNT) != null) {
				ffd.setFieldData("cryptogram_amount", chipTagValues.get(mesConstants.EMV_TAG_CRYPTO_AMOUNT).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_FORM_FACTOR) != null) {
				ffd.setFieldData("form_factor_indicator",
						chipTagValues.get(mesConstants.EMV_TAG_FORM_FACTOR).toString());
			}

			if (chipTagValues.get(mesConstants.EMV_TAG_ISSUER_SCRIPT) != null) {
				ffd.setFieldData("issuer_script_result",
						chipTagValues.get(mesConstants.EMV_TAG_ISSUER_SCRIPT).toString());
			}

			ffd.setAllFieldData(this);
			ffd.setFieldData("tran_code", tc);
			if (getInt("card_sequence_number") != 0) {
				ffd.setFieldData("card_sequence_number", tc); // card sequence
																// number
			}
			recs.add(ffd);
		}
		
		//RS Software - EMV modification end

    return( recs );
  }
  
  public List buildSecondPresentmentRecords()
    throws Exception
  {
    boolean         isCielo         = (ProcessorType == PROC_TYPE_CIELO);
    boolean         isCredit        = "C".equals(getData("debit_credit_indicator"));      // chargback_visa = "debit_credit_ind"
    List            recs            = new ArrayList(1);
    FlatFileRecord  ffd             = null;
    double          originalAmount  = getDouble("original_amount");
    String          tc              = "05";   // default to sale
    final String SPL_CHBK_INDCTR_PARTIAL_AMOUNT="P";
    final String SPL_CHBK_INDCTR_FULL_AMOUNT=" ";
    final String USAGE_CODE_NINE="9";
    final String USAGE_CODE_SP_DEFAULT="2";
    
    // override the transaction code
    if ( isCashDisbursement() )
    {
      tc = "07";
    }
    else if ( isCredit )
    {
      tc = "06";
    }
    
    // TCR 0
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
    ffd.setFieldData("card_number", getCardNumberFull());
    if(USAGE_CODE_NINE.equals(getData("usage_code")))
    {
        ffd.setFieldData("usage_code",USAGE_CODE_NINE);
    }else{
        ffd.setFieldData("usage_code",USAGE_CODE_SP_DEFAULT);
    }

    recs.add(ffd);
    
    // TCR 1
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR1);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code",tc);
    if(SPL_CHBK_INDCTR_PARTIAL_AMOUNT.equals(getData("special_chargeback_ind")))
    {
    	ffd.setFieldData("special_chargeback_ind", SPL_CHBK_INDCTR_PARTIAL_AMOUNT);
    }
    else
    {
    	ffd.setFieldData("special_chargeback_ind", SPL_CHBK_INDCTR_FULL_AMOUNT);
    }
    if(USAGE_CODE_NINE.equals(getData("usage_code")))
    {
    	ffd.setFieldData("cb_ref_num","");
    	ffd.setFieldData("documentation_ind", "");
    }
    
    if(!isBlank(getData("special_conditions_ind"))){
    	ffd.setFieldData("special_conditions_ind",StringUtilities.rightJustify(getData("special_conditions_ind") , 2,' '));
    }
    
    recs.add(ffd);

    // TCR 4 Supplementary Visa Claims Resolution Financial Data Record 
    // add for CAU April, 2018
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR4);
    ffd.setFieldData("transaction_code",tc);
    ffd.setFieldData("tran_component_seq_num", "4");

    if(USAGE_CODE_NINE.equals(getData("usage_code")))
    {
    	ffd.setFieldData("business_format_code","DF");
    	ffd.setFieldData("vrol_case_number", getData("vrol_case_number"));

    	recs.add(ffd);
    }
    
    if ( !isCredit )
    {
      // TCR 5 - Payment Service Data
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR5);
      ffd.setAllFieldData(this);
      ffd.setFieldData("tran_code",tc);
      if(USAGE_CODE_NINE.equals(getData("usage_code")))
      {
      	ffd.setFieldData("crs_proc_code","");
      	ffd.setFieldData("cb_rights_ind", "");
      }
      recs.add(ffd);
    }
    
    if ( "VB".equals(getData("card_type")) && !isCielo )   // brazil does not use TCR6
    {
      // TCR 6 - Commercial Card Data
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC05_TCR6);
      ffd.setAllFieldData(this);
      ffd.setFieldData("tran_code",tc);
      recs.add(ffd);
    }
    return( recs );
  }
  
  private List buildMerchantProfileRecords()
    throws Exception
  {
    FlatFileRecord  ffd       = null;
    List            recs      = new ArrayList(1);
    
    // TCR 0
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_MPS_OUT);// 430
    ffd.setAllFieldData(this);
    ffd.setFieldData("dest_bin"     , getData("issuer_bin") );
    ffd.setFieldData("source_bin"   , getData("bin_number") );
    ffd.setFieldData("acquirer_bin" , getData("bin_number") );
    recs.add(ffd);
    
    // TCR 1
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC50_TCR1_MPS_OUT);// 431
    ffd.setAllFieldData(this);
    if ( "1".equals(getData("incorporation_status_code")) )
    {
      ffd.setFieldData("sole_prop_last_name"      , getData("owner_last_name").toUpperCase()      );
      ffd.setFieldData("sole_prop_first_name"     , getData("owner_first_name").toUpperCase()     );
      ffd.setFieldData("sole_prop_middle_initial" , getData("owner_middle_initial").toUpperCase() );
    }
    recs.add(ffd);
    
    return( recs );
  }
  
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    List            recs      = null;
    
    ProcessorType = (getInt("bank_number") == mesConstants.BANK_ID_CIELO) ? PROC_TYPE_CIELO : PROC_TYPE_MES;
    
    switch(messageType)
    {
      case MT_FIRST_PRESENTMENT:
        recs = buildFirstPresentmentRecords();
        break;
        
      case MT_SECOND_PRESENTMENT:
        recs = buildSecondPresentmentRecords();
        break;
        
      case MT_FEE_COLLECTION:
        recs = buildFeeCollectionRecords();
        break;
        
      case MT_VISA_MPS:
        recs = buildMerchantProfileRecords();
        break;
    }
    
    return( recs );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("visaData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
    fgroup.add( new Field         ( "acquirer_business_id"      , "Visa Business ID"        , 8   ,10 , false ) );
    fgroup.add( new Field         ( "ardef_data"                , "ARDEF Data (various)"    ,16   ,18 , true  ) );
    fgroup.add( new Field         ( "merchant_vol_indicator"    , "Merchant Volume Ind"     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "moto_ecommerce_merchant"   , "MOTO/eComm Account"      , 1   , 3 , false ) );
    fgroup.add( new Field         ( "mvv"                       , "MVV"                     ,10   ,12 , false ) );
    fgroup.add( new Field         ( "pos_term_cap"              , "POS Term Cap"            , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "issuer_bin"                , "Issuer BIN"              , 6   , 8 , true  ) );
    fgroup.add( new Field         ( "tech_ind"                  , "Technology Indicator"    , 1   , 3 , true  ) );

    fgroup.add( new Field         ( "auth_settled_aci"          , "Settled ACI"             , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "fee_program_indicator"     , "FPI"                     , 3   , 5 , true  ) );
    fgroup.add( new Field         ( "requested_payment_service" , "RPS"                     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "spend_qualified_ind"       , "Spend Qualified Ind"     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "token_assurance_level"     , "Token Assurance Level"   , 2   , 3 , true  ) );
    fgroup.add( new Field         ( "ecommerce_goods_indicator" , "eCommerce Goods Ind"     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "market_specific_data_ind"  , "Market Specific Data"    , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "information_indicator"     , "Information Ind"         , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "special_conditions_ind"    , "Special Condition Ind"   , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "enhanced_data_1"           , "Enhanced Data 1"         ,64   ,24 , true  ) );
    fgroup.add( new Field         ( "enhanced_data_2"           , "Enhanced Data 2"         ,64   ,24 , true  ) );
    fgroup.add( new Field         ( "enhanced_data_3"           , "Enhanced Data 3"         ,64   ,24 , true  ) );
    fgroup.add( new Field         ( "recurring_payment_ind"     , "Recurring Pmt Ind"       , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "pos_environment"           , "Pos Environment"         , 1   , 1 , true  ) );

    // enhanced field for Hotel and Auto Rental Addendum records
    fgroup.add( new Field         ( "no_show_indicator"         , "No Show Indicator"       , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "extra_charges"             , "Extra Charges"           , 6   , 8 , true  ) );
    fgroup.add( new CurrencyField ( "rate_weekly"               , "Rate Weekly"             ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "one_way_drop_off_charges"  , "One Way Drop Off Charges",12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "insurance_charges"         , "Insurance Charges"       ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "fuel_charges"              , "Fuel Charges"            ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "car_class_code"            , "Car Class Code"          , 2   , 4 , true  ) );
    fgroup.add( new CurrencyField ( "folio_cash_advances"       , "Folio Cash Advances"     ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "food_beverage_charges"     , "Food/Bev Charges"        ,12   ,14 , true  ) );
    fgroup.add( new CurrencyField ( "prepaid_expenses"          , "Prepaid Expenses"        ,12   ,14 , true  ) );
    
    // fields to support Visa Fee Collection
    fgroup.add( new HiddenField   ( "message_text"              ) );
    
    // fields to support Visa Merchant Registration (TC50)
    fgroup.add( new HiddenField   ( "mps_id_number"             ) );
    fgroup.add( new HiddenDateField("close_date"                ) );
    fgroup.add( new HiddenField   ( "incorporation_status_code" ) );
    fgroup.add( new HiddenField   ( "owner_first_name"          ) );
    fgroup.add( new HiddenField   ( "owner_last_name"           ) );
    fgroup.add( new HiddenField   ( "owner_middle_initial"      ) );
    // fields for ICC
    fgroup.add( new Field         ( "icc_data"         , "EMV data (icc)"       , 255   , 255 , true  ) );
    fgroup.add( new HiddenField   ( "card_sequence_number"      ) );
    fgroup.add( new HiddenField   ( "response_code_emv"         ) );
  }    
  
  public String getCardTypeEnhanced()
  {
    return( getString("card_type_enhanced") );
  }
  
  public String getPlanType()
  {
    return( isCashDisbursement() ? "V$" : "VS" );
  }
  
  protected boolean isAuthExpired()
  {
    Date      authDate    = getDate("auth_date");
    Calendar  cal         = Calendar.getInstance();
    boolean   retVal      = false;
    
    try
    {
      if ( authDate != null )
      {
        // the Visa cutoff is 3 am Pacific time:
        //   if the current time is before 3 am, the cpd is today
        //   if the current time is after  3 am, the cpd is tomorrow
        // just add 21 hours -- it will either go over midnight to tomorrow or not.
        cal.add(Calendar.HOUR,21);
      
        // reset all the time components
        cal.set(Calendar.HOUR,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
      
        // subtract 30 days from the cpd
        cal.add(Calendar.DAY_OF_MONTH,-30);
      
        // if the auth date is older than cpd-30, auth is expired
        retVal = authDate.before(cal.getTime());
      }
    }
    catch( Exception e )
    {
      logEntry("isAuthExpired(" + getData("rec_id") + ")",e.toString());
    }
    
    return( retVal );
  }    
  
  public void setFieldsBatchData(VisakBatchData batchData)
    throws java.sql.SQLException
  {
    if( batchData != null )
    {
      super.setFieldsBatchData(batchData);

      setData( "acquirer_business_id"     , batchData.getAcquirerBusinessId());
      setData( "bin_number"               , batchData.getBinNumber());
      setData( "eligibility_flags"        , batchData.getEligibilityFlagsVisa() );
      setData( "moto_ecommerce_merchant"  , batchData.getMotoEcommMerchant() );
      setData( "mvv"                      , batchData.getMVV() );
      setData( "pos_term_cap"             , batchData.getPosTermCap());
    }
  }

  public void setFields_Visa( VisakBatchData batchData )
    throws java.sql.SQLException
  {
	//tem1-2303 : extra logging to identify bad card numbers 
	String cardNumber = getData("card_number_full");
	
	if(null== (cardNumber)){
		log.error("setFields_Visa() receiving empty/null card number " 
				 + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
	}else if (!TridentTools.mod10Check(getData("card_number_full"))){
		 log.error("setFields_Visa() receiving invalid card number for " + (cardNumber.length()>=6?cardNumber.substring(0, 6):cardNumber) + ":" 
	                 + Util13.hexStringN(cardNumber.getBytes())
		             + " for Batch id:" + getData("batch_id") + " for Batch record id:" + getData("batch_record_id"));
	}
	  
    ArdefDataVisa   ardef         = SettlementDb.loadArdefVisa(cardNumber);

    // set fields that depend on the ARDEF data
    setArdefData(ardef);

    // remove data for bad card #s.
    String cte = getData("card_type_enhanced");
    if ( cte != null && "nul".equals((cte+"nul").substring(0,3)) )
    {
      setData( "card_type_enhanced"   , "");
      setData( "region_issuer"        , "");
    }
    else
    {
      setData( "region_issuer"        , StringUtilities.leftJustify(ardef.getArdefCountryCode(),2,' ') + ardef.getArdefRegion() );

      switch( RecBatchType )
      {
        case mesConstants.MBS_BT_CIELO:
          String comboCard    = ardef.getComboCard();               //  Y (credit/debit) or C (credit/prepaid)
          if( comboCard != null && comboCard.trim().length() == 1 && cte.length() == 3 )
          {
            boolean isFundingSrcC = "C".equals( cte.substring(2) );   // the standard funding source should be C or else D/P

            if( "DB0".equals(getData("dbcr_installtype")) )           // if tran arrived in debito file
            {
              if( isFundingSrcC )                                       // if funding source is credit
              {
                setData( "card_type_enhanced"       , cte.substring(0,2) + ("Y".equals(comboCard) ? "D" : "P") );
                if( !"VB".equals(getData("card_type")) )
                {
                  setData( "card_type"                , "VD" );
                }
              }
            }
            else // if( "CRx".equals(getData("dbcr_installtype")) )   // if tran arrived in credito file
            {
              if( !isFundingSrcC )                                      // if funding source is not credit
              {
                setData( "card_type_enhanced"       , cte.substring(0,2) + "C" );
                if( !"VB".equals(getData("card_type")) )
                {
                  setData( "card_type"                , "VS" );
                }
              }
            }
          }
          break;
      }
    }

    setData( "tech_ind"             , ardef.getTechnologyIndicator() );
    setData( "issuer_bin"           , ardef.getIssuerBin() );
    setData( "ardef_data"           , ardef.getArdefData() );

    // build the region data for the merchant
    String countryCode  = getData("country_code");
    String regionCode   = (String) CountryCodeToRegionMap.get(countryCode);
    if( regionCode == null )
    {
      regionCode = SettlementDb.loadVisaRegionCode(countryCode);
    }
    setData( "region_merchant"      , StringUtilities.leftJustify(countryCode,2,' ') + regionCode );

    // set dba_city after the moto_ecommerce_ind is finalized;
    // visa has edits on the city based on the CNP status
    if( RecBatchType != mesConstants.MBS_BT_CIELO && (isMotoEcommIndCNP() || (getInt("sic_code") == 4816)) )
    {
      String[]  dbaCity = null;

      switch( RecBatchType )
      {
        case mesConstants.MBS_BT_VISAK  :
          dbaCity = new String[]  { getString("phone_number"), (batchData.getParameter()).getMerchantCityPhone(), getString("dm_contact_info") };
          break;
        case mesConstants.MBS_BT_PG     :
          dbaCity = new String[]  { getString("phone_number"), getString("dm_contact_info") };
          break;
        case mesConstants.MBS_BT_VITAL  :
          dbaCity = new String[]  { getString("phone_number") };
          break;
      //case mesConstants.MBS_BT_CIELO  :
      //  break;
      }

      // Each batch type has its possible dbaCity's in order of least to most important;
      //  we'll take the first one that has (1) valid ph# or (2) "is sic 4816 or motoecpi 5/6/7" and "seems to have url or email"
      // (Visa says CPS CNP must be phone, and moto/ecpi=5/6/7 or sic=4816 must be url or email.)
      String    data      = "";
      boolean   takeFirst = (getInt("sic_code") == 4816) || ("567".indexOf( getData("moto_ecommerce_ind") ) >= 0);
      int       idx       = (dbaCity == null) ? (-1) : (dbaCity.length-1);
      for( ; idx >= 0; --idx )
      {
        if( dbaCity[idx] == null )continue;

        data = dbaCity[idx].trim();
        
        if( data.length() != 0 )
        {
          if( TridentTools.isValidMerchantPhone(data) ) { data = TridentTools.getFormattedPhone(data);  break;  }
          if( takeFirst && (data.indexOf(".") >= 0 || data.indexOf("@") >= 0) )   {                     break;  }
        }
      }

      if( data.length() != 0 )setData( "dba_city"             , data );
    }

    // cannot call until after batch_id and batch_record_id are set
    setAuthData();
  }

  public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType)
    throws java.sql.SQLException
  {
    RecBatchType = batchType;
    super.setFieldsBase(batchData, resultSet, batchType);

    setFields_Visa(batchData);
  }
  
  public void setFieldsVisak(VisakBatchData batchData, int recIdx)
    throws java.sql.SQLException
  {
    super.setFieldsVisak(batchData, recIdx);

    setFields_Visa(batchData);
  }
  
  protected boolean isMotoEcommIndCNP()
  {
    String  aci             = getData("auth_returned_aci");
    String  motoEcommInd    = getData("moto_ecommerce_ind").trim();

    // make sure that the moto/ecomm matches the ACI to prevent rejects
          if( "U".equals(aci) )                     { motoEcommInd = "5"; }
    else  if( "S".equals(aci) )                     { motoEcommInd = "6"; }
    else  if( "W".equals(aci) )                     { motoEcommInd = "7"; }
    else  if( !isBlank(motoEcommInd) )
    {
            if( "Z".equals(motoEcommInd) )          { motoEcommInd = "";  }   // Z= card present (only sent by PG)
      else  if( "56".indexOf(motoEcommInd) >= 0 )   { motoEcommInd = "7"; }   // if aci != U/S, moto_ecpi != 5/6
      else  if( "23".indexOf(motoEcommInd) >= 0 )   { setData("recurring_payment_ind","Y"); }   // 2= recurring; 3= installment
    }

    if( isBlank(motoEcommInd) )
    {
      if( "N".equals(getData("card_present")) )     { motoEcommInd = "1"; }   // default CNP to one time MOTO
      else
      {
        // fix moto_ecpi for certain sic codes
        //
        // Visa documentation:
        //  "If the Merchant Category Code on an original transaction is
        //   5960, 5962, or 5964 through 5969, the entry cannot be a space."
        //
        // A value of one (1) has special meaning.  Visa documentation:
        //  "For domestic transactions in the U.S.A. Region, this value may
        //   also indicate a single bill payment transaction in either the
        //   card present or card absent environments."
        //
        int       sicCode           = getInt("sic_code");
        int[]     motoRequiredSics  = {5960,5962,5964,5965,5966,5967,5968,5969};

        for( int i = 0; i < motoRequiredSics.length; ++i )
        {
          if( sicCode == motoRequiredSics[i] )      { motoEcommInd = "1";   break;  }
        }
      }
    }

    setData("moto_ecommerce_ind", motoEcommInd);

    return( !isBlank(motoEcommInd) && "123567".indexOf( motoEcommInd ) >= 0 );
  }
  
  public void fixBadData()
  {
    super.fixBadData();

    // a leading space is invalid and must be removed; a trailing space is often valid, but will be added later if necessary
    setData("product_id",getData("product_id").trim());

    // trim the extra_charges (just makes it faster to parse later; zero and space both mean "no extra charges")
    setData("extra_charges",getData("extra_charges").replace('0',' ').trim());

    // '0' is a bad value for auth_avs_response
    if( "0".equals(getData("auth_avs_response")) )setData("auth_avs_response","");

    // if the begin date is older than today-365, date is invalid
    if( !isFieldBlank("statement_date_begin") )
    {
      Date      dateBegin = getDate("statement_date_begin");
      Calendar  cal       = Calendar.getInstance();
      cal.add(Calendar.YEAR,-1);
      if( dateBegin.before(cal.getTime()) )
      {
        setData("statement_date_begin","");
      }
    }

    // always fix (even though auth source can can be space for credits/returns, just not for debits/purchases)
    if ( getData("auth_source_code" ).trim().length() == 0 )
    {
      if( "C".equals(getData("debit_credit_indicator")) )
        setData( "auth_source_code", "9" );
      else
        setData( "auth_source_code", "E" );
    }
    
    if(!"1".equals(getData("no_show_indicator"  ))) setData("no_show_indicator"   , "0"   );  // htl/auto must be 0 or 1; others N/A
    if( isFieldBlank("auth_returned_aci"        ) ) setData("auth_returned_aci"   , "N"   );

    // need to force tax_amount and tax_indicator to make sense together
    if( getDouble("tax_amount") == 0.00 )
    {
      // if tax_amount == 0, then tax_indicator can be " "/not-included or "2"/exempt -- it cannot be "1"/included
      if( !"2".equals(getData("tax_indicator").trim()) )setData("tax_indicator", " ");
      
      if(getData("tax_amount").length() > 0)setData("tax_indicator", "2");
    }
    else
    {
      // if tax_amount != 0, then tax_indicator should be "1"/included
      setData( "tax_indicator", "1" );
    }
    
    //@showData();//@
  }
  
  public void fixBadDataAfterIcAssignment()
  {
    super.fixBadDataAfterIcAssignment();

    // if the FPI starts with a '-' then it was created by MeS; Visa will expect all spaces
    if( "-".equals(getData("fee_program_indicator").substring(0,1) ) )  setData("fee_program_indicator" , "");

    // if the RPS is not a CPS program then the settled ACI must be either 'N' or 'T'; otherwise settled=returned
    String  settled_aci = getData("auth_returned_aci");
    if( isFieldBlank("requested_payment_service") && !"T".equals(settled_aci) )
    {
      if( getData("region_merchant").startsWith("US") &&
          getData("region_issuer"  ).startsWith("US") )
      {
        // if changing ACI from "U"/"S" to "N", change the moto_ecpi from "5"/"6" to "7"
        if( "US".indexOf(settled_aci) >= 0 )
        {
          setData("moto_ecommerce_ind", "7");
        }
        settled_aci = "N";
      }
    }
    setData("auth_settled_aci" , settled_aci);
  }
  
  public LineItem generateLineItem()
  {
    LineItem    li    = null;
    
    li = new LineItem( SettlementRecord.SETTLE_REC_VISA );
    li.setData("item_commodity_code","49500");
    li.setData("item_descriptor",SicCodes.getInstance().getMerchandiseDesc(getData("sic_code")));
    li.setData("product_code","MISC");
    li.setData("quantity","1");
    li.setData("unit_of_measure","EA");
    li.setData("unit_cost",getData("transaction_amount"));
    li.setData("vat_tax_amount",getData("tax_amount"));
    li.setData("vat_tax_rate","0");
    li.setData("discount_per_line","0");
    li.setData("line_item_total",getData("transaction_amount"));
    
    return(li);
  }
}
