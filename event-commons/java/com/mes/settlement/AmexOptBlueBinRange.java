
package com.mes.settlement;

import java.sql.Date;
import org.apache.commons.lang3.StringUtils;

public class AmexOptBlueBinRange 
{
  // Amex Product Codes for Opt Blue Bin ranges
  public static final String  AMEX_PRODUCT_CODE_RELOADABLE      = "RP";  // Reloadable card
  public static final String  AMEX_PRODUCT_CODE_STORED_VALUE    = "SV";  // Stored Value Card
  public static final String  AMEX_PRODUCT_CODE_INTL_RELOADABLE = "IR";  // International Reloadable
  public static final String  AMEX_PRODUCT_CODE_REG_SUBMISSION  = "IS";  // Commercial Debit
  public static final String  AMEX_PRODUCT_CODE_INTL_REG_SUBMISSION = "IB";  // International Regular submission
  public static final String  AMEX_PRODUCT_CODE_VPAYMENT   = "VP";  // vPayment

  protected String amexCmBin;	
  protected String amexProductCode;
  protected Date effectiveDate;
  protected String loadFileName;
  protected long loadFileId;
  protected String enabled;
  
  public String getAmexCmBin(){
	  return amexCmBin;
  }
 
public String getAmexProductCode(){
	  return amexProductCode;
  }
  
  public Date getEffectiveDate(){
	  return effectiveDate;
  }
  
  public void setAmexCmBin(String bin){
	  amexCmBin=bin;
  }
  
  public void setAmexProductCode(String code){
	  amexProductCode=code;
  }
  
  public void setEffectiveDate(Date date){
	  effectiveDate=date;
  }
  
  @Override
  public String toString()
  {
    return ("amex_cm_bin     : " + amexCmBin    + "product_code    : " + amexProductCode   + "effective date  : " + effectiveDate);
  }


  public String getLoadFileName() {
	return loadFileName;
  }

  public void setLoadFileName(String loadFileName) {
	this.loadFileName = loadFileName;
  }

  public long getLoadFileId() {
	return loadFileId;
  }

  public void setLoadFileId(long loadFileId) {
	this.loadFileId = loadFileId;
  }

  public String getEnabled() {
	return enabled;
  }

  public void setEnabled(String enabled) {
	this.enabled = enabled;
  }
  
  public static boolean isPrePaidCard(String productCode){
	return ((null != productCode) && ( 
            (productCode == AMEX_PRODUCT_CODE_RELOADABLE ) || 
            (productCode ==  AMEX_PRODUCT_CODE_STORED_VALUE ) || 
            (productCode ==  AMEX_PRODUCT_CODE_INTL_RELOADABLE) || 
            (productCode ==   AMEX_PRODUCT_CODE_REG_SUBMISSION )));	
  }
  
  public static boolean isVpayment(String productCode) {
	  return StringUtils.equalsIgnoreCase(productCode, AMEX_PRODUCT_CODE_VPAYMENT);
  }
  
}