/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeBase.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2012-03-30 09:58:24 -0700 (Fri, 30 Mar 2012) $
  Version            : $Revision: 20016 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import com.mes.support.SyncLog;

public abstract class InterchangeBase
{
  // ********************************************************************************
  public static abstract class IC_BaseClass
  {
    protected   Date        CentralProcessingDate   = null;
    protected   Vector      DisqualList             = new Vector(0);
    protected   boolean     cpsSmallTicketFlag      = false;


    public                      IC_BaseClass( ){  }

    public boolean isCpsSmallTicketFlag() {
		return cpsSmallTicketFlag;
	}

	public void setCpsSmallTicketFlag(boolean cpsSmallTicketFlag) {
		this.cpsSmallTicketFlag = cpsSmallTicketFlag;
	}

    
	public final void setCentralProcessingDate( Date cpd )
    {
      CentralProcessingDate = cpd;
    }

    public final Vector getDisqualList()
    {
      return( DisqualList );
    }

    public final void showDisqualList()
    {
      for( int i = 0; i < DisqualList.size(); ++i )
      {
        System.out.println((String)DisqualList.elementAt(i));
      }
    }

    public final    boolean   testDataIsInList_NullListReturnsFalse( int dataLength, String testData, String tokenList )
    {
      if( tokenList == null || tokenList.length() == 0 )return( false );
      return( testDataIsInList( dataLength, testData, tokenList ) );
    }

    public final    boolean   testDataIsInList_NullListReturnsTrue( int dataLength, String testData, String tokenList )
    {
      if( tokenList == null || tokenList.length() == 0 )return( true );
      return( testDataIsInList( dataLength, testData, tokenList ) );
    }

    public final    boolean   testDataIsInList( int dataLength, String testData, String tokenList )
    {
      return SettlementTools.testDataIsInList(dataLength,testData,tokenList);
    }

    // returns an irfInfo string formatted for this card-type
    public          String    getIrfInfo(SettlementRecord tranInfo, int bfCardType, String enhRequired, VisaProgram program) { return( null ); }

    // returns null or the enhancements required if the given tranInfo meets all requirements for this class
    public          String    tranQualifies( SettlementRecord tranInfo, int bfCardType )                  { return( null ); }

    public          String    tranQualifies( SettlementRecord tranInfo, int bfCardType,VisaProgram program )                  { return( null ); }
  }   // end of "class IC_BaseClass"


  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  protected Date          CentralProcessingDate   = null;
  protected HashMap       DisqualMap              = new HashMap(0);
  protected String        getBestIcInfoClause     = "";
  public final String     myCardType;
  public final String     myClassPrefix;
  public final int        myIcLevelLength;
  protected   boolean     cpsSmallTicketFlag      = false;


  // constructor
  public InterchangeBase( String thisCardType, String thisClassPrefix, int thisIssuerIcLevelLength )
  {
    myCardType      = thisCardType;
    myClassPrefix   = thisClassPrefix;
    myIcLevelLength = thisIssuerIcLevelLength;

    if ( "MC".equals(myCardType) ) 
    { 
      setCentralProcessingDate( SettlementTools.getSettlementDateMC() );  
    }
    else if ( "VS".equals(myCardType) ) 
    { 
      setCentralProcessingDate( SettlementTools.getSettlementDateVisa() );
    }
    else if ( "DS".equals(myCardType) ) 
    { 
      setCentralProcessingDate( SettlementTools.getSettlementDateDiscover() );
    }
    else if ( "AM".equals(myCardType) ) 
    { 
      setCentralProcessingDate( SettlementTools.getSettlementDateAmex() );
    }
    else    // default to today
    { 
      Calendar  cal   = Calendar.getInstance();
      // clear the time elements
      cal.clear(Calendar.HOUR);
      cal.clear(Calendar.HOUR_OF_DAY);
      cal.clear(Calendar.MINUTE);
      cal.clear(Calendar.SECOND);
      cal.clear(Calendar.MILLISECOND);

      // default the central processing date to current date
      setCentralProcessingDate( cal.getTime() );
    }      
  }

  public Date getCentralProcessingDate()
  {
    return( CentralProcessingDate );
  }

  public void setCentralProcessingDate(Date value)
  {
    CentralProcessingDate = value;
  }
  
   public boolean isCpsSmallTicketFlag() {
		return cpsSmallTicketFlag;
	}

	public void setCpsSmallTicketFlag(boolean cpsSmallTicketFlag) {
		this.cpsSmallTicketFlag = cpsSmallTicketFlag;
	}


  public HashMap getDisqualMap()
  {
    return( DisqualMap );
  }

  public IC_BaseClass getInterchangeClass( String className )
  {
    IC_BaseClass  interchangeClass  = null;

    try
    {
      Class         c                 = null;
      Object        o                 = null;
      String        extendedClass     = myClassPrefix + className;

      c = Class.forName( extendedClass );         // load the class into the JVM
      o = c.newInstance();                        // create an instance of the class
      interchangeClass = (IC_BaseClass)o;         // cast the object as a IC_BaseClass
      interchangeClass.setCentralProcessingDate( getCentralProcessingDate() );
      interchangeClass.setCpsSmallTicketFlag(isCpsSmallTicketFlag());
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.getInterchangeClass(" + className + ")",e.toString());
    }
    return( interchangeClass );
  }

  public IcInfo getBestIrfInfoFromInfoList( SettlementRecord tranInfo, String irfInfoList )
  {
    return( getBestIrfInfoFromInfoList( tranInfo, irfInfoList, myCardType ) );
  }

  public IcInfo getBestIrfInfoFromInfoList( SettlementRecord tranInfo, String irfInfoList, String cardType )
  {
    IcInfo  retVal    = null;
    try
    {
      retVal = SettlementDb.getBestIcInfo( tranInfo, cardType, myIcLevelLength, CentralProcessingDate, irfInfoList, getBestIcInfoClause );
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeBase::getBestIrfInfoFromInfoList()",e.toString());
    }
    return( retVal );
  }

  public IcInfo getBestIrfInfoFromClassList( SettlementRecord tranInfo, int bfCardType, List<VisaProgram> classList )
  {
    String      irfInfoList   = null;
    IcInfo      retVal        = null;

    try
    {
      if( classList != null && classList.size() > 0 )
      {
        for( VisaProgram program: classList)
        {
          String        thisClassName     = program.getName();
          IC_BaseClass  interchangeClass  = getInterchangeClass( thisClassName );

          String  enhRequired = interchangeClass.tranQualifies( tranInfo, bfCardType,program);
          if( enhRequired != null && enhRequired.length() == SettlementRecord.NUM_ENHANCED_IND )
          {
            String  irfInfoThisClass = interchangeClass.getIrfInfo( tranInfo, bfCardType, enhRequired,program );

            if( irfInfoList == null ) { irfInfoList =                     irfInfoThisClass; }
            else                      { irfInfoList = irfInfoList + "," + irfInfoThisClass; }
          }
          else
          {
            DisqualMap.put( thisClassName, interchangeClass.getDisqualList() );
          }
        }
        //  have list of info for all qualifying classes; need to pick the best one
        retVal = getBestIrfInfoFromInfoList( tranInfo, irfInfoList );
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeBase::getBestIrfInfoFromClassList(..)",e.toString());
    }
    return( retVal );
  }

  public abstract boolean getBestClass( SettlementRecord tranInfo, List classList );
}
