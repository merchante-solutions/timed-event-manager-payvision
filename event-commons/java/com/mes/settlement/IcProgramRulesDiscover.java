/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/IcProgramRulesDiscover.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-10-15 14:28:15 -0700 (Tue, 15 Oct 2013) $
  Version            : $Revision: 21623 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import com.mes.support.StringUtilities;

public class IcProgramRulesDiscover
{
  protected   String        Icp                         = null;
  protected   String        IcpReg                      = null;
  protected   String        AmountToleranceFlags        = null;
  protected   String        AuthRequiredFlags           = null;
  protected   int           TimelinessDays              = 0;

  public IcProgramRulesDiscover()
  {
  }

  public String getIcp()                              { return(Icp); }
  public void   setIcp(String value)                  { Icp = value; }

  public String getIcpReg()                           { return(IcpReg); }
  public void   setIcpReg(String value)               { IcpReg = value; }

  public String getAmountToleranceFlags()             { return(AmountToleranceFlags); }
  public void   setAmountToleranceFlags(String value) { AmountToleranceFlags = value; }
  
  public String getAuthRequiredFlags()                { return(AuthRequiredFlags); }
  public void   setAuthRequiredFlags(String value)    { AuthRequiredFlags = value; }
  
  public int    getTimelinessDays()                   { return(TimelinessDays); }
  public void   setTimelinessDays(int value)          { TimelinessDays = value; }

  public void setRuleData(ResultSet resultSet)
    throws java.sql.SQLException
  {
    if( resultSet.getString("icp_code_reg") != null )
      setIcpReg(StringUtilities.rightJustify( resultSet.getString(  "icp_code_reg"      ), 3, '0') );
    else
      setIcpReg(StringUtilities.rightJustify( resultSet.getString(  "icp_code"          ), 3, '0') );
    setIcp (  StringUtilities.rightJustify( resultSet.getString(  "icp_code"          ), 3, '0') );
    setAmountToleranceFlags               ( resultSet.getString(  "amount_tolerance"  ) );
    setAuthRequiredFlags                  ( resultSet.getString(  "auth_reqd"         ) );
    setTimelinessDays                     ( resultSet.getInt   (  "timeliness_days"   ) );
  }
  
  public void showData()
  {
    System.out.println( "Icp Code                 : " + getIcp());
    System.out.println( "Icp Code Reg             : " + getIcpReg());
    System.out.println( "AmountToleranceFlags     : " + getAmountToleranceFlags());
    System.out.println( "AuthRequiredFlags        : " + getAuthRequiredFlags());
    System.out.println( "TimelinessDays           : " + getTimelinessDays());
  }
}
