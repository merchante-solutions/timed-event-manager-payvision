package com.mes.settlement;
public class Fpi {
	public enum CardProduct {
		CONSUMER_PREPAID("fpi_cnsmr_prepaid",0),
		CONSUMER_DEBIT("fpi_cnsmr_debit",1),
		CONSUMER_CREDIT("fpi_cnsmr_credit",2),
		CONSUMER_REWARDS("fpi_cnsmr_rewards",3),
		CONSUMER_SIGNATURE("fpi_cnsmr_signature",4),
		CONSUMER_SIGNATURE_PREFERRED("fpi_cnsmr_sign_pref",5),
		CONSUMER_HI_NET("fpi_cnsmr_hi_net",6),
		COMMERCIAL_BUSINESS_DEBIT("fpi_comm_bus_debit",7),
		COMMERCIAL_BUSINESS_T1("fpi_comm_bus_t1",8),
		COMMERCIAL_BUSINESS_SIGNATURE_T3("fpi_comm_bus_sig_t3",9),
		COMMERCIAL_BUSINESS_ENHANCED_T2("fpi_comm_bus_enh_t2",10),
		COMMERCIAL_CORPORATE("fpi_comm_corp",11),
		COMMERCIAL_PURCHASED("fpi_comm_purch",12),
		COMMERCIAL_PREPAID("fpi_comm_prepaid",13),
		COMMERCIAL_BUSINESS_T4("fpi_comm_bus_t4",14),
		COMMERCIAL_BUSINESS_T5 ("fpi_comm_bus_t5",15);

		private final String value;
		private final Integer intValue;
		CardProduct(String value, Integer intValue) {
			this.value = value;
			this.intValue = intValue;
		}

		public String getValue() {
			return this.value;
		}

		public Integer getIntValue() {
			return this.intValue;
		}

	}
	public enum Program{
		   STANDARD("IC_Visa_Standard",0),
		   ELECTRONIC("IC_Visa_Electronic",1),
		   CPS_RETAIL("IC_Visa_CPS_Retail",2),
		   CPS_RESTAURANT("IC_Visa_CPS_Restaurant",3),
		   CPS_SUPERMARKET("IC_Visa_CPS_Supermarket",4),
		   CPS_RETAIL_SERVICE_STATION("IC_Visa_CPS_RetailSvcStation",5),
		   CPS_SMALL_TICKET("IC_Visa_CPS_SmallTicket",6),
		   CPS_RETAIL_KEY_ENTRY("IC_Visa_CPS_RetailKeyEntry",7),
		   CPS_CARD_NOT_PRESENT("IC_Visa_CPS_CardNotPresent",8),
		   CPS_AUTOMATED_FUEL_DISPENSER("IC_Visa_CPS_AutomatedFuelDisp",9),
		   CPS_ACCOUNT_FUNDING("IC_Visa_CPS_Account_Funding",10),
		   CPS_E_COMMERCE_BASIC("IC_Visa_CPS_ECommerceBasic",11),
		   CPS_E_COMMERCE_PREF_RETAIL("IC_Visa_CPS_ECommercePrefRtl",12),
		   CPS_HOTEL_AUTO("IC_Visa_CPS_Hotel_Auto",13),
		   CPS_HOTEL_AUTO_CNP("IC_Visa_CPS_Hotel_Auto_CNP",14),
		   CPS_E_COMMERCE_PREF_HTL_AUTO("IC_Visa_CPS_ECommercePrefHtlAuto",15),
		   CPS_PASSENGER_TRANSPORT("IC_Visa_CPS_PassengerTransport",16),
		   CPS_PASSENGER_TRANSPORT_CNP("IC_Visa_CPS_PassengerTransport_CNP",17),
		   CPS_E_COMMERCE_PREF_PASS_TRANS("IC_Visa_CPS_ECommercePrefPassTrans",18),
		   PROGRAM_CPS_RECU_PMT ("IC_Visa_CPS_RecurrBillPmt",19),
		   RETAIL_2("fpi row retail 2",19),
		   UTILITY("fpi row utility",20),
		   TRAVEL_SERVICE("fpi row travel service",21),
		   BUSINESS_TO_BUSINESS("fpi row business-to-business",22),
		   LEVEL_2_DATA("fpi row level 2 data",23),
		   LEVEL_3_DATA("fpi row level 3 data",24),
		   ELECTRONIC_W_DATA("fpi row electronic w/data",25),
		   US_INTER_REGIONAL_MERCHANT("fpi row US inter-regional merchant",26),
		   PUERTO_RICO_DOMESTIC_STANDARD("fpi row Puerto Rico Domestic-Standard",27),
		   PUERTO_RICO_DOMESTIC_ELECTRONIC("fpi row Puerto Rico Domestic - Electronic",28),
		   US_GOVERNMENT_FEE("fpi row US Domestic - Government Fee",29),
		   PROGRAM_CASH_DISBURSEMENT("Cash Disbursement",97),
		   PROGRAM_NOT_US_DOMESTIC("Not US Domestic",98),
		   PROGRAM_CREDIT_VOUCHER("Credit Voucher",99),
		   NO_PROGRAM("",-1);

		private final String value;
		private final Integer intValue;
		Program(String value, Integer intValue) {
			this.value = value;
			this.intValue = intValue;
		}

		public String getValue() {
			return this.value;
		}
		public Integer getIntValue() {
			return this.intValue;
		}

	}

	public enum SicCodeGroup{
		HIGH_RISK_TELEMARKET,
		RETAIL_TIPPERS,
		SERVICE_STATION,
		FUEL,
		CASH_DISBURSEMENT,
		DEBT_REPAYMENT,
		UTILITY,
		AIRLINE,
		PASSENGER_TRANSPORT,
		TRAVEL_SERVICE,
		HOTEL_AUTO_RENTAL,
		HEALTH_DEV_MKT_UTILITY,
		DEVELOPING_MARKET,
		DEVELOPING_MARKET_DEBIT,
		CHARITY,
		BUSINESS_TO_BUSINESS,
		GOVERNMENT_TO_GOVERNMENT,
		RECURRING_BILL_PAYMENT,
		GLOBAL_B2B_VIRTUAL_PAYMENT,
		GOVERNMENT_FEE
	}
}

