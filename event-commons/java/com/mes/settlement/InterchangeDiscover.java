/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeDiscover.java $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $LastChangedDate: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import com.mes.support.SyncLog;

public class InterchangeDiscover  extends InterchangeBase
{
  public static final String      MY_CARD_TYPE          = "DS";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeDiscover$";


  // Discover irfInfo string format: "eeaaabbb,eeaaabbb,eeaaabbb" where ee=enhancements, aaa=icp, bbb=icp regulated
  public static final int     LOC_ICP           = SettlementRecord.NUM_ENHANCED_IND + 0;        // "aaa"= icp         = 3 chars
  public static final int     LOC_ICP_REG       = SettlementRecord.NUM_ENHANCED_IND + 3;        // "bbb"= icp reg     = 3 chars
  public static final int     LEN_IRF_INFO      = SettlementRecord.NUM_ENHANCED_IND + 6;        // enh + (icp + icp reg)
  public static final int     LEN_ISSUER_IC_LVL = 3;                                            // (icp)
  public static final String  IRF_INFO_UNKNOWN  = SettlementRecord.DEF_ENHANCED_IND + "-00-00"; // use when fail to qualify any Discover ICP


  // ********************************************************************************
  // ********************************************************************************
  public static          class IC_Discover                                                          extends IC_BaseClass
  {
    public                      IC_Discover()                     { }



    ////// sic codes used to check various requirements.....

    public static final String  strSic_AmtTolerance20Percent    = "4121,7230"                           ;       // taxis,salons
    public static final String  strSic_AmtToleranceExemptAlways = "3000-3350,4112,4111,4131,4511"                 + "," + // passenger transport
                                                                  "3351-3500,7512,7513,7519"            + "," + // car rentals; DS rule really "-3441"
                                                                  "3501-3999,7011,7012"                 + "," + // hotels
                                                                  "4411"                                + "," + // steamship/cruise lines
                                                                  "5541,5542"                           + "," + // service stations & afd's
                                                                  "5812-5814"                           ;       // restaurants & bars & fast food



    ////// Discover provided values.....

    // Discover Card Program IDs & ALM Codes (aka "Card Product" and "Card Product Code")
    //    003   =         Consumer   Debit
    //    005   =         Prepaid           - Gift
    //    006   =         Prepaid           - ID Known
    //    008   = 'C'   = Consumer   Credit – Core
    //    001   = 'R'   = Consumer   Credit - Rewards
    //    007   = 'P'   = Consumer   Credit - Premium
    //    011   = 'Q'   = Consumer   Credit - Premium Plus
    //    004   =         Commercial Debit
    //    002   = 'B'   = Commercial Credit
    //    010   = 'E'   = Commercial Credit - Executive Business
    //    009   =         Prvt Label Credit
    //            'Z'   =    none

    // Discover Processing Codes
    //    '00'  = purchase
    //    '01'  = cash disbursement
    //    '09'  = purchase with cashback
    //    '13'  = recurring - auto pay w/AVS  (eg monthly)
    //    '14'  = recurring - auto pay        (eg monthly)
    //    '15'  = recurring - installment     (eg monthly)
    //    '16'  = recurring - subscription    (eg yearly)
    //    '20'  = credit voucher / return



    public String   tranQualifies( SettlementRecord tranInfo, IcProgramRulesDiscover thisRuleSet )
    {
      String      retVal      = null;

      try
      {
        // These requirements are not checked here because enough information is in Oracle
        // ****** in the MeS "Discover IC Program Rules" Table ******
        // * Sic Code
        // * POS Entry Mode
        // * Processing Code
        // * Amount Maximum
        //


        DisqualList.removeAllElements();    // clear the disqual list

        // ** check requirements met: authorization
        if( DisqualList.size() == 0 )
        {
          if( "Y".equals(thisRuleSet.getAuthRequiredFlags()) )
          {
            if( tranInfo.isFieldBlank("auth_tran_id") )
            {
              DisqualList.add("Failed to meet authorization requirements");
            }
          }
        } // if( DisqualList.size() == 0 )

        // ** check requirements met: timeliness
        if( DisqualList.size() == 0 )
        {
          int intDays = thisRuleSet.getTimelinessDays();
          if( intDays != 0 )
          {
            if( !tranMeetsDateTimeliness(tranInfo, intDays) )
            {
              DisqualList.add("Failed to meet timeliness requirements");
            }
          }
        } // if( DisqualList.size() == 0 )

        // ** check requirements met: amount tolerance
        if( DisqualList.size() == 0 )
        {
          if( "Y".equals(thisRuleSet.getAmountToleranceFlags()) )
          {
            double  amountLimit;
            String  tran_sic_code             = tranInfo.getString("sic_code");

                  if( testDataIsInList( 4, tran_sic_code, strSic_AmtTolerance20Percent    ) ) amountLimit = 0.2;
            else  if( testDataIsInList( 4, tran_sic_code, strSic_AmtToleranceExemptAlways ) ) amountLimit = 0.0;
            else                                                                              amountLimit = 0.1;

            if( amountLimit > 0.0 )
            {
              double  tran_transaction_amount   = tranInfo.getDouble("transaction_amount");
              double  tran_auth_amount_total    = tranInfo.getDouble("auth_amount_total");

              if( ( tran_auth_amount_total  < ( tran_transaction_amount * (1.0 - amountLimit) ) ) ||
                  ( tran_auth_amount_total  > ( tran_transaction_amount * (1.0 + amountLimit) ) ) )
              {
                DisqualList.add("Tran Amount / Total Auth Amount not within tolerance");
              }
            }
          }
        } // if( DisqualList.size() == 0 )

        if( DisqualList.size() == 0 )retVal = SettlementRecord.DEF_ENHANCED_IND;
      }
      catch( Exception e )
      {
        String recInfo = ((tranInfo == null) ? "null" : (tranInfo.getString("batch_id") + "," + tranInfo.getString("batch_record_id")));
        SyncLog.LogEntry("com.mes.settlement.InterchangeDiscover::tranQualifies(" + recInfo + ")",e.toString());
      }
      return( retVal );
    }

    public boolean  tranMeetsDateTimeliness( SettlementRecord tranInfo, int numDays )
      throws Exception
    {
      boolean     retVal    = false;
      
      if( tranInfo.getDate("transaction_date") != null )
      {           
        int     nsfType = 0;//(myProgram == PROGRAM_NOT_US_DOMESTIC) ? 1 : 0; // SettlementDb.NSF_SUNDAYS_ONLY : SettlementDb.NSF_NONE;

        Calendar calOne = Calendar.getInstance();
        Calendar calTwo = Calendar.getInstance();

        calOne.clear();
        calTwo.clear();

        calOne.setTime( tranInfo.getDate("transaction_date") );
        calTwo.setTime( CentralProcessingDate );

        calOne.add(Calendar.DAY_OF_MONTH, 1);   // "+1" so don't check whether tran date is a nonSettlementDay
        calTwo.add(Calendar.DAY_OF_MONTH,-1);   // "-1" so don't check whether CPD       is a nonSettlementDay
        calOne.add(Calendar.DAY_OF_MONTH, numDays-2+SettlementDb.getNonSettlementDayCount(tranInfo.RecSettleRecType,calOne,calTwo,nsfType));

        retVal = !calTwo.after(calOne);
      }
      return( retVal );
    }
  }






  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  // constructor
  public InterchangeDiscover()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  public boolean getBestClass( SettlementRecord tranInfo, List listOfRuleSets )
  {
    boolean       retVal          = false;
    try
    {
      DisqualMap.clear();   // dump the disqual map


      IcInfo  icInfo      = null;
      String  irfInfoList = null;

      // sabre should be 100% old ic_cat for non-MAP mid, but be 100% MAP for MAP mid
      if( !tranInfo.MerchantIsProcessedByMeS && "256s".equals(  tranInfo.getData("load_filename").substring(0,4) )  )
      {
        if(  "C".equals(  tranInfo.getData("debit_credit_indicator") )  ) irfInfoList = SettlementRecord.DEF_ENHANCED_IND + "RTNRTN";
        else                                                              irfInfoList = SettlementRecord.DEF_ENHANCED_IND + "SBRSBR";
      }
      else
      if( listOfRuleSets != null && listOfRuleSets.size() > 0 )
      {
        IC_BaseClass  interchangeClass  = getInterchangeClass( "IC_Discover" );

        for( int i = 0; i < listOfRuleSets.size(); ++i )
        {
          IcProgramRulesDiscover      thisRuleSet  = (IcProgramRulesDiscover)listOfRuleSets.get(i);

          String  enhRequired = ((IC_Discover)interchangeClass).tranQualifies( tranInfo, thisRuleSet );
          if( enhRequired != null && enhRequired.length() == SettlementRecord.NUM_ENHANCED_IND )
          {
            String  irfInfoThisClass = enhRequired + thisRuleSet.getIcp() + thisRuleSet.getIcpReg();

            if( irfInfoList == null ) { irfInfoList =                     irfInfoThisClass; }
            else                      { irfInfoList = irfInfoList + "," + irfInfoThisClass; }
          }
          else
          {
            DisqualMap.put( thisRuleSet.getIcp(), new Vector( interchangeClass.getDisqualList() ) );
          }
        }
      }

      if( irfInfoList == null ) { irfInfoList = IRF_INFO_UNKNOWN; }
      else                      { retVal = true;                  }

      //  have list of info for all qualifying classes; need to pick the best one
      getBestIcInfoClause = " and icd.debit_type is null";
      icInfo = getBestIrfInfoFromInfoList( tranInfo, irfInfoList );

      int debitType = tranInfo.getInt("debit_type");
      if( debitType != 0 )
      {
        //  check again, but only allow the regulated row(s) for this debit_type and the already-selected irf-info
        IcInfo  wouldHaveGotten = icInfo;
        String  whgIrfInfo      = wouldHaveGotten.getIcString();
        getBestIcInfoClause = " and nvl(icd.debit_type," + debitType + ") = " + debitType;

        if(  "C".equals(  tranInfo.getData("debit_credit_indicator") )  )
          icInfo = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + "RGRRGR," + whgIrfInfo);
        else
          icInfo = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + "REGREG," + whgIrfInfo);

        if( whgIrfInfo.equals(icInfo.getIcString()) ) // if no regulated selected, revert to "would have gotten"
        {
          icInfo = wouldHaveGotten;
        }
        else                                          // if regulated selected, use some regulated and some "would have gotten" info
        {
          // store the whg IcString
          icInfo.setIcString( whgIrfInfo );

          // check for "would have gotten" lookup ic_code (if there isn't one, then ic_billing will be regulated ic_cat)
          String  regIc   = wouldHaveGotten.getIcCodeRegulated();
          if( regIc != null && regIc.length() == 4 )
          {
            icInfo.setIcCodeBilling(debitType + regIc);
          }
        }
      }

      String irfInfo = icInfo.getIcString();
      if( debitType != 0 )
        tranInfo.setData( "interchange_program" , irfInfo.substring( LOC_ICP_REG, LOC_ICP_REG + 3 ) );
      else
        tranInfo.setData( "interchange_program" , irfInfo.substring( LOC_ICP    , LOC_ICP     + 3 ) );
      tranInfo.setData( "ic_cat"              , icInfo.getIcCode()                        );
      tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling()                 );

      tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeDiscover.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }
}
