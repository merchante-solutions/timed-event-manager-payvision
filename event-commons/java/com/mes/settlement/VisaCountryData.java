/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/settlement/VisaCountryData.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-12-08 15:16:51 -0800 (Tue, 08 Dec 2009) $
  Version            : $Revision: 16788 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;

public class VisaCountryData
{
  protected String AreaNetIndicator             = null;
  protected String CountryCode                  = null;
  protected String CpsIndicator                 = null;
  protected String CurrencyCodeDefault          = null;
  protected String EpayIndicator                = null;
  protected String ForeignIndicator             = null;
  
  public VisaCountryData( )
  {
  }
  
  public VisaCountryData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setAreaNetIndicator   ( resultSet.getString("area_net_indicator") );
    setCountryCode        ( resultSet.getString("country_code") );
    setCpsIndicator       ( resultSet.getString("cps_indicator") );
    setCurrencyCodeDefault( resultSet.getString("currency_code_default") );
    setEpayIndicator      ( resultSet.getString("epay_indicator") );
    setForeignIndicator   ( resultSet.getString("foreign_indicator") );
  }
  
  public String getAreaNetIndicator()               { return( AreaNetIndicator ); } 
  public void   setAreaNetIndicator( String value ) { AreaNetIndicator = value; }
  
  public String getCountryCode()                    { return( CountryCode ); } 
  public void   setCountryCode( String value )      { CountryCode = value; }
  
  public String getCpsIndicator()                   { return( CpsIndicator ); } 
  public void   setCpsIndicator( String value )     { CpsIndicator = value; }
  
  public String getCurrencyCodeDefault()            { return( CurrencyCodeDefault ); } 
  public void   setCurrencyCodeDefault(String value){ CurrencyCodeDefault = value; }
  
  public String getEpayIndicator()                  { return( EpayIndicator ); } 
  public void   setEpayIndicator( String value )    { EpayIndicator = value; }
  
  public String getForeignIndicator()               { return( ForeignIndicator ); } 
  public void   setForeignIndicator( String value ) { ForeignIndicator = value; }
  
  public void showData()
  {
    System.out.println("AreaNetIndicator    : " + AreaNetIndicator    );
    System.out.println("CountryCode         : " + CountryCode         );
    System.out.println("CpsIndicator        : " + CpsIndicator        );
    System.out.println("CurrencyCodeDefault : " + CurrencyCodeDefault );
    System.out.println("EpayIndicator       : " + EpayIndicator       );
    System.out.println("ForeignIndicator    : " + ForeignIndicator    );
  }
}
