package com.mes.settlement;

import java.sql.Date;

public class DailyDetailFileD256ER {

    private long trnSeqNum;
    private long btcSeqNum;
    private long batchNum;
    private Date batchDate;
    private long merchantNumber;
    private long finSeqNum;
    private long sequenceNumber;
    private int transactionCode;
    private String formatIndicator;
    private String data;
    private String loadFileName;

    /**
     * This method returns the transaction sequence number
     * 
     * @return trnSeqNum
     */
    public long getTrnSeqNum() {
        return trnSeqNum;
    }

    /**
     * This method is to set the transaction sequence number
     * 
     * @param trnSeqNum
     */
    public void setTrnSeqNum(long trnSeqNum) {
        this.trnSeqNum = trnSeqNum;
    }

    /**
     * This method returns the btcSeqnum
     * 
     * @return btcSeqNum
     */
    public long getBtcSeqNum() {
        return btcSeqNum;
    }

    /**
     * This method is to set the btcSeqNum
     * 
     * @param btcSeqNum
     */
    public void setBtcSeqNum(long btcSeqNum) {
        this.btcSeqNum = btcSeqNum;
    }

    /**
     * This method returns the batch number
     * 
     * @return batchNum
     */
    public long getBatchNum() {
        return batchNum;
    }

    /**
     * This method is to set the batch number
     * 
     * @param batchNum
     */
    public void setBatchNum(long batchNum) {
        this.batchNum = batchNum;
    }

    /**
     * This method returns the batch date
     * 
     * @return batchDate
     */
    public Date getBatchDate() {
        return batchDate;
    }

    /**
     * This method is to set the batch date
     * 
     * @param batchDate
     */
    public void setBatchDate(Date batchDate) {
        this.batchDate = batchDate;
    }

    /**
     * This method returns the merchant number
     * 
     * @return merchantNumber
     */
    public long getMerchantNumber() {
        return merchantNumber;
    }

    /**
     * This method is to set the merchant number
     * 
     * @param merchantNumber
     */
    public void setMerchantNumber(long merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    /**
     * This method returns the finSeqNumber
     * 
     * @return finSeqNum
     */
    public long getFinSeqNum() {
        return finSeqNum;
    }

    /**
     * This method is to set the finSeqNumber
     * 
     * @param finSeqNum
     */
    public void setFinSeqNum(long finSeqNum) {
        this.finSeqNum = finSeqNum;
    }

    /**
     * This method returns the sequence number for the data entered
     * 
     * @return sequenceNumber
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * This method is used to set the sequence number for the data
     * 
     * @param sequenceNumber
     */
    public void setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * This method returns the transaction code
     * 
     * @return transactionCode
     */
    public int getTransactionCode() {
        return transactionCode;
    }

    /**
     * This method is to set the transaction code
     * 
     * @param transactionCode
     */
    public void setTransactionCode(int transactionCode) {
        this.transactionCode = transactionCode;
    }

    /**
     * This method returns the format indicator
     * 
     * @return formatIndicator
     */
    public String getFormatIndicator() {
        return formatIndicator;
    }

    /**
     * This method is to set the format Indicator
     * 
     * @param formatIndicator
     */
    public void setFormatIndicator(String formatIndicator) {
        this.formatIndicator = formatIndicator;
    }

    /**
     * This method returns the raw data parsed
     * 
     * @return data
     */
    public String getData() {
        return data;
    }

    /**
     * This method is to set the raw data
     * 
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * This method returns the loadFielName
     * 
     * @return loadFileName
     */
    public String getLoadFileName() {
        return loadFileName;
    }

    /**
     * This method is to set the loadFileName
     * 
     * @param loadFileName
     */
    public void setLoadFileName(String loadFileName) {
        this.loadFileName = loadFileName;
    }

}
