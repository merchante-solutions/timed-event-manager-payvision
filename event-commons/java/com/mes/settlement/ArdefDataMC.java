/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/ArdefDataMC.java $

  Description:

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-04-16 12:22:33 -0700 (Thu, 16 Apr 2015) $
  Version            : $Revision: 23579 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.Date;
import com.mes.constants.mesConstants;
import com.mes.support.StringUtilities;

public class ArdefDataMC
  extends ArdefDataBase
{
  protected Date      EffectiveTs                       = null;
  protected String    TableId                           = null;
  protected long      RangeHigh                         = 0L;
  protected long      RangeLow                          = 0L;
  protected String    ActiveInactiveCode                = null;
  protected String    GCMSProductId                     = null;
  protected String    CardProgramId                     = null;
  protected int       DebitType                         = 0;
  protected int       PriorityCode                      = -1;
  protected long      MemberId                          = -1L;
  protected String    ProductTypeId                     = null;
  protected String    Endpoint                          = null;
  protected String    CountryCodeAlpha                  = null;
  protected String    CountryCodeNumeric                = null;
  protected String    Region                            = null;
  protected String    ProductClass                      = null;
  protected String    TransactionRoutingInd             = null;
  protected String    FirstPresentmentReassignSwitch    = null;
  protected String    ProductReassignSwitch             = null;
  protected String    PWCBOptInSwitch                   = null;
  protected String    LicensedProductId                 = null;
  protected String    MappingServiceInd                 = null;
  protected String    AccountLevelInd                   = null;
  protected Date      AccountLevelActivationDate        = null;
  protected String    BillingCurrencyDefault            = null;
  protected int       BillingCurrencyExpDefault         = 2;
  protected String    BillingPrimaryTranCurrency_1      = null;
  protected String    BillingPrimaryCurrency_1          = null;
  protected int       BillingPrimaryCurrencyExp_1       = 2;
  protected String    BillingPrimaryTranCurrency_2      = null;
  protected String    BillingPrimaryCurrency_2          = null;
  protected int       BillingPrimaryCurrencyExp_2       = 2;
  protected String    BillingPrimaryTranCurrency_3      = null;
  protected String    BillingPrimaryCurrency_3          = null;
  protected int       BillingPrimaryCurrencyExp_3       = 2;
  protected String    BillingPrimaryTranCurrency_4      = null;
  protected String    BillingPrimaryCurrency_4          = null;
  protected int       BillingPrimaryCurrencyExp_4       = 2;
  protected String    ChipToMagConvServiceInd           = null;
  protected Date      FloorExpDate                      = null;
  protected String    CoBrandParticipationSwitch        = null;
  protected String    SpendControlSwitch                = null;
  protected String    MoneySendInd                      = null;
  protected String    DurbinRegulatedRateInd            = null;

  public boolean isInRange( String cardNumber )
  {
    boolean retVal = false;
    try
    {
      long cardBin = Long.parseLong( StringUtilities.leftJustify(cardNumber,19,'0') );
      if ( cardBin >= RangeLow && cardBin <= RangeHigh )
      {
        retVal = true;
      }
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public boolean isActive()
  {
    return( "A".equals(getActiveInactiveCode().trim()) );
  }
  
  public boolean isForeignIssuer( int recBatchType ) 
  { 
    boolean retVal = false;

    switch( recBatchType )
    {
      case mesConstants.MBS_BT_CIELO:     retVal = !"076".equals(CountryCodeNumeric); break;  // Brazil
      default:                            retVal = !"840".equals(CountryCodeNumeric); break;  // United States
    }
    return( retVal );
  }

  public String getCardTypeEnhanced()
  {
    return( ProductClass );
  }
  
  public String getCardTypeFull()
  {
    String    cardTypeFull;
    
    if ( "2".equals(ProductTypeId) || "3".equals(ProductTypeId) )
    {
      cardTypeFull  = "MB";   // commercial credit or debit
    }
    else if ( "DMC".equals(CardProgramId) )
    {
      cardTypeFull  = "MD";   // consumer debit
    }
    else
    {
      cardTypeFull  = "MC";   // consumer credit
    }
    return( cardTypeFull );
  }
  
  
  public Date getEffectiveTs()                                { return(EffectiveTs); }
  public void setEffectiveTs( Date value )                    { EffectiveTs = value; }
  
  public String getTableId()                                  { return(TableId); }
  public void setTableId( String value )                      { TableId = value; }
  
  public long getRangeHigh()                                  { return(RangeHigh); }
  public void setRangeHigh( long value )                      { RangeHigh = value; }
  
  public long getRangeLow()                                   { return(RangeLow); }
  public void setRangeLow( long value )                       { RangeLow = value; }
  
  public String getActiveInactiveCode()                       { return(ActiveInactiveCode); }
  public void setActiveInactiveCode( String value )           { ActiveInactiveCode = value; }
  
  public String getGCMSProductId()                            { return(GCMSProductId); }
  public void setGCMSProductId( String value )                { GCMSProductId = value; }
  
  public String getCardProgramId()                            { return(CardProgramId); }
  public void setCardProgramId( String value )                { CardProgramId = value; }
  
  public int getDebitType()                                   { return(DebitType); }
  public void setDebitType( int value )                       { DebitType = value; }
  public void setDebitType( String value )
  {
    int  debitType = 0;
    try{ debitType = Integer.parseInt(value); } catch( Exception ee ) {}
    setDebitType( debitType ); 
  }
  
  public int getPriorityCode()                                { return(PriorityCode); }
  public void setPriorityCode( int value )                    { PriorityCode = value; }
  
  public long getMemberId()                                   { return(MemberId); }
  public void setMemberId( long value )                       { MemberId = value; }
  
  public String getProductTypeId()                            { return(ProductTypeId); }
  public void setProductTypeId( String value )                { ProductTypeId = value; }
  
  public String getEndpoint()                                 { return(Endpoint); }
  public void setEndpoint( String value )                     { Endpoint = value; }
  
  public String getCountryCodeAlpha()                         { return(CountryCodeAlpha); }
  public void setCountryCodeAlpha( String value )             { CountryCodeAlpha = value; }
  
  public String getCountryCodeNumeric()                       { return(CountryCodeNumeric); }
  public void setCountryCodeNumeric( String value )           { CountryCodeNumeric = value; }
  
  public String getRegion()                                   { return(Region); }
  public void setRegion( String value )                       { Region = value; }
  
  public String getProductClass()                             { return(ProductClass); }
  public void setProductClass( String value )                 { ProductClass = value; }
  
  public String getTransactionRoutingInd()                    { return(TransactionRoutingInd); }
  public void setTransactionRoutingInd( String value )        { TransactionRoutingInd = value; }
  
  public String getFirstPresentmentReassignSwitch()           { return(FirstPresentmentReassignSwitch); }
  public void setFirstPresentmentReassignSwitch( String value ) { FirstPresentmentReassignSwitch = value; }
  
  public String getProductReassignSwitch()                    { return(ProductReassignSwitch); }
  public void setProductReassignSwitch( String value )        { ProductReassignSwitch = value; }
  
  public String getPWCBOptInSwitch()                          { return(PWCBOptInSwitch); }
  public void setPWCBOptInSwitch( String value )              { PWCBOptInSwitch = value; }
  
  public String getLicensedProductId()                        { return(LicensedProductId); }
  public void setLicensedProductId( String value )            { LicensedProductId = value; }
  
  public String getMappingServiceInd()                        { return(MappingServiceInd); }
  public void setMappingServiceInd( String value )            { MappingServiceInd = value; }
  
  public String getAccountLevelInd()                          { return(AccountLevelInd); }
  public void setAccountLevelInd( String value )              { AccountLevelInd = value; }
  
  public Date getAccountLevelActivationDate()                 { return(AccountLevelActivationDate); }
  public void setAccountLevelActivationDate( Date value )     { AccountLevelActivationDate = value; }
  
  public String getBillingCurrencyDefault()                   { return(BillingCurrencyDefault); }
  public void setBillingCurrencyDefault( String value )       { BillingCurrencyDefault = value; }
  
  public int getBillingCurrencyExpDefault()                   { return(BillingCurrencyExpDefault); }
  public void setBillingCurrencyExpDefault( int value )       { BillingCurrencyExpDefault = value; }
  
  public String getBillingPrimaryTranCurrency_1()             { return(BillingPrimaryTranCurrency_1); }
  public void setBillingPrimaryTranCurrency_1( String value ) { BillingPrimaryTranCurrency_1 = value; }
  
  public String getBillingPrimaryCurrency_1()                 { return(BillingPrimaryCurrency_1); }
  public void setBillingPrimaryCurrency_1( String value )     { BillingPrimaryCurrency_1 = value; }
  
  public int getBillingPrimaryCurrencyExp_1()                 { return(BillingPrimaryCurrencyExp_1); }
  public void setBillingPrimaryCurrencyExp_1( int value )     { BillingPrimaryCurrencyExp_1 = value; }
  
  public String getBillingPrimaryTranCurrency_2()             { return(BillingPrimaryTranCurrency_2); }
  public void setBillingPrimaryTranCurrency_2( String value ) { BillingPrimaryTranCurrency_2 = value; }
  
  public String getBillingPrimaryCurrency_2()                 { return(BillingPrimaryCurrency_2); }
  public void setBillingPrimaryCurrency_2( String value )     { BillingPrimaryCurrency_2 = value; }
  
  public int getBillingPrimaryCurrencyExp_2()                 { return(BillingPrimaryCurrencyExp_2); }
  public void setBillingPrimaryCurrencyExp_2( int value )     { BillingPrimaryCurrencyExp_2 = value; }
  
  public String getBillingPrimaryTranCurrency_3()             { return(BillingPrimaryTranCurrency_3); }
  public void setBillingPrimaryTranCurrency_3( String value ) { BillingPrimaryTranCurrency_3 = value; }
  
  public String getBillingPrimaryCurrency_3()                 { return(BillingPrimaryCurrency_3); }
  public void setBillingPrimaryCurrency_3( String value )     { BillingPrimaryCurrency_3 = value; }
  
  public int getBillingPrimaryCurrencyExp_3()                 { return(BillingPrimaryCurrencyExp_3); }
  public void setBillingPrimaryCurrencyExp_3( int value )     { BillingPrimaryCurrencyExp_3 = value; }
  
  public String getBillingPrimaryTranCurrency_4()             { return(BillingPrimaryTranCurrency_4); }
  public void setBillingPrimaryTranCurrency_4( String value ) { BillingPrimaryTranCurrency_4 = value; }
  
  public String getBillingPrimaryCurrency_4()                 { return(BillingPrimaryCurrency_4); }
  public void setBillingPrimaryCurrency_4( String value )     { BillingPrimaryCurrency_4 = value; }
  
  public int getBillingPrimaryCurrencyExp_4()                 { return(BillingPrimaryCurrencyExp_4); }
  public void setBillingPrimaryCurrencyExp_4( int value )     { BillingPrimaryCurrencyExp_4 = value; }
  
  public String getChipToMagConvServiceInd()                  { return(ChipToMagConvServiceInd); }
  public void setChipToMagConvServiceInd( String value )      { ChipToMagConvServiceInd = value; }
  
  public Date getFloorExpDate()                               { return(FloorExpDate); }
  public void setFloorExpDate( Date value )                   { FloorExpDate = value; }
  
  public String getCoBrandParticipationSwitch()               { return(CoBrandParticipationSwitch); }
  public void setCoBrandParticipationSwitch( String value )   { CoBrandParticipationSwitch = value; }
  
  public String getSpendControlSwitch()                       { return(SpendControlSwitch); }
  public void setSpendControlSwitch( String value )           { SpendControlSwitch = value; }
  
   
  public String getMoneySendIndicator()                  { return(MoneySendInd); }
  public void setMoneySendIndicator( String value )      { MoneySendInd = value; }
  
  public String getDurbinRegulatedRateInd()                  { return(DurbinRegulatedRateInd); }
  public void setDurbinRegulatedRateInd( String value )      { DurbinRegulatedRateInd = value; }
  
  
  public void showData()
  {
    System.out.println("CardTypeFull                  = " + getCardTypeFull());
    System.out.println("EffectiveTs                   = " + getEffectiveTs());
    System.out.println("TableId                       = " + getTableId());
    System.out.println("RangeHigh                     = " + getRangeHigh());
    System.out.println("RangeLow                      = " + getRangeLow());
    System.out.println("ActiveInactiveCode            = " + getActiveInactiveCode());
    System.out.println("GCMSProductId                 = " + getGCMSProductId());
    System.out.println("CardProgramId                 = " + getCardProgramId());
    System.out.println("DebitType                     = " + getDebitType());
    System.out.println("PriorityCode                  = " + getPriorityCode());
    System.out.println("MemberId                      = " + getMemberId());
    System.out.println("ProductTypeId                 = " + getProductTypeId());
    System.out.println("Endpoint                      = " + getEndpoint());
    System.out.println("CountryCodeAlpha              = " + getCountryCodeAlpha());
    System.out.println("CountryCodeNumeric            = " + getCountryCodeNumeric());
    System.out.println("Region                        = " + getRegion());
    System.out.println("ProductClass                  = " + getProductClass());
    System.out.println("TransactionRoutingInd         = " + getTransactionRoutingInd());
    System.out.println("FirstPresentmentReassignSwitch= " + getFirstPresentmentReassignSwitch());
    System.out.println("ProductReassignSwitch         = " + getProductReassignSwitch());
    System.out.println("PWCBOptInSwitch               = " + getPWCBOptInSwitch());
    System.out.println("LicensedProductId             = " + getLicensedProductId());
    System.out.println("MappingServiceInd             = " + getMappingServiceInd());
    System.out.println("AccountLevelInd               = " + getAccountLevelInd());
    System.out.println("AccountLevelActivationDate    = " + getAccountLevelActivationDate());
    System.out.println("BillingCurrencyDefault        = " + getBillingCurrencyDefault());
    System.out.println("BillingCurrencyExpDefault     = " + getBillingCurrencyExpDefault());
    System.out.println("BillingPrimaryTranCurrency_1  = " + getBillingPrimaryTranCurrency_1());
    System.out.println("BillingPrimaryCurrency_1      = " + getBillingPrimaryCurrency_1());
    System.out.println("BillingPrimaryCurrencyExp_1   = " + getBillingPrimaryCurrencyExp_1());
    System.out.println("BillingPrimaryTranCurrency_2  = " + getBillingPrimaryTranCurrency_2());
    System.out.println("BillingPrimaryCurrency_2      = " + getBillingPrimaryCurrency_2());
    System.out.println("BillingPrimaryCurrencyExp_2   = " + getBillingPrimaryCurrencyExp_2());
    System.out.println("BillingPrimaryTranCurrency_3  = " + getBillingPrimaryTranCurrency_3());
    System.out.println("BillingPrimaryCurrency_3      = " + getBillingPrimaryCurrency_3());
    System.out.println("BillingPrimaryCurrencyExp_3   = " + getBillingPrimaryCurrencyExp_3());
    System.out.println("BillingPrimaryTranCurrency_4  = " + getBillingPrimaryTranCurrency_4());
    System.out.println("BillingPrimaryCurrency_4      = " + getBillingPrimaryCurrency_4());
    System.out.println("BillingPrimaryCurrencyExp_4   = " + getBillingPrimaryCurrencyExp_4());
    System.out.println("ChipToMagConvServiceInd       = " + getChipToMagConvServiceInd());
    System.out.println("FloorExpDate                  = " + getFloorExpDate());
    System.out.println("CoBrandParticipationSwitch    = " + getCoBrandParticipationSwitch());
    System.out.println("SpendControlSwitch            = " + getSpendControlSwitch());
    System.out.println("LoadFilename                  = " + getLoadFilename());
    System.out.println("LoadFileId                    = " + getLoadFileId());
    System.out.println("MoneySendInd                  = " + getMoneySendIndicator());
    System.out.println("DurbinRegulatedRateInd        = " + getDurbinRegulatedRateInd());
  }
}
