/*************************************************************************

  FILE: $URL: http://10.1.61.151/svn/mesweb/branches/te/src/main/com/mes/settlement/SettlementRecordModBII.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-01-23 15:10:34 -0800 (Thu, 23 Jan 2014) $
  Version            : $Revision: 22205 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenDateField;
import com.mes.forms.HiddenField;

public class SettlementRecordModBII extends SettlementRecord
{
  public SettlementRecordModBII()
  {
    super(mesConstants.MBS_BT_CIELO, "B2", SettlementRecord.SETTLE_REC_MODBII);
    DbaZipMaxLength = 9;
  }

  public List buildPresentmentRecords( int whichPresentment )
    throws Exception
  {
    boolean         isCredit  = "C".equals(getData("debit_credit_indicator"));      // chargback_visa = "debit_credit_ind"
    List            recs      = new ArrayList(1);
    FlatFileRecord  ffd       = null;
    String          tc        = isCredit ? "06" : "05";   // no cash disbursement
    
    
    // TCR 0
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_TC05_TCR0);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code"          , tc);
    ffd.setFieldData("card_number"        , getCardNumberFull());
    
    if( isCredit )
    {
      ffd.setFieldData("tran_source"        , " ");     // default=1
      ffd.setFieldData("pos_entry_mode"     , "  ");    // default=sRec.pos_entry_mode
    }
    if( whichPresentment != 1 )
    {
      ffd.setFieldData("tran_type"          , "2");     // default/1st=1; 2nd = "2", partial 2nd = "3", reprocessed = "6"
      ffd.setFieldData("financial_tran_id"  , "N");     // default=S
    }
    recs.add(ffd);
    
    // TCR 1
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_TC05_TCR1);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code"          , tc);

//    ffd.setFieldData("issuer_free_text"   , tc);

//    if( getData("operation_type").equals("995") )
//    {
//      ffd.setFieldData("995_transaction_amount" , getDouble("transaction_amount"));
//    }

    int installmentCount  = getInt("multiple_clearing_seq_count");
    if( installmentCount > 0 )
    {
      if( installmentCount < 50 )     // parcelado loja
      {
        ffd.setFieldData("multiple_clearing_seq_total", getDouble("auth_amount"));
      }
      else                            // parcelado emissor
      {
        ffd.setFieldData("multiple_clearing_seq_count", (installmentCount - 50));
      }
    }
    recs.add(ffd);

    // TCR 2
    ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_TC05_TCR2);
    ffd.setAllFieldData(this);
    ffd.setFieldData("tran_code"          , tc);

    int fileFormat = getInt("file_format");
    switch( fileFormat )
    {
      case   9: // Diners
        ffd.setFieldData("diners_clearing_nsu"      , getData("clearing_nsu"));
        ffd.setFieldData("diners_dba_address"       , getData("dba_address"));
        ffd.setFieldData("diners_dba_zip"           , getData("dba_zip"));
        break;

//    case   7: // ELO
      default : // etc etc etc
        break;
    }
    if( "05".equals(tc) && whichPresentment == 1 )
    {
      ffd.setFieldData("transaction_date"   , "00000000");
    }
    if( installmentCount > 0 && installmentCount < 50 )     // parcelado loja
    {
      ffd.setFieldData("operation_type"     , "990");         // if airline, then more complicated ~~ but no airline in phase 1
    }
    recs.add(ffd);

    if ( !isCredit )
    {
      // TCR 5 - Payment Service Data
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_TC05_TCR5);
      ffd.setAllFieldData(this);
      ffd.setFieldData("tran_code"          , tc);
      
      recs.add(ffd);
    }
    
    if ( !isFieldBlank("icc_data") )
    {
      // TCR 7 -- icc data
      ffd = getFlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_TC05_TCR7);
      ffd.setAllFieldData(this);
      ffd.setFieldData("tran_code"          , tc);
      
      recs.add(ffd);
    }
    
    return( recs );
  }
  
  public List buildNetworkRecords(int messageType)
    throws Exception
  {
    List            recs      = null;
    
    switch(messageType)
    {
      case MT_FIRST_PRESENTMENT:
        recs = buildPresentmentRecords(1);
        break;
        
      case MT_SECOND_PRESENTMENT:
        recs = buildPresentmentRecords(2);
        break;
    }
    
    return( recs );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("modbiiData");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                     len  size   null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
    fgroup.add( new Field         ( "acquirer_business_id"      , "Visa Business ID"        , 8   ,10 , false ) );
    fgroup.add( new Field         ( "pos_term_cap"              , "POS Term Cap"            , 1   , 3 , true  ) );

    fgroup.add( new Field         ( "ecommerce_goods_indicator" , "eCommerce Goods Ind"     , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "market_specific_data_ind"  , "Market Specific Data"    , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "information_indicator"     , "Information Ind"         , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "special_conditions_ind"    , "Special Condition Ind"   , 1   , 3 , true  ) );
    fgroup.add( new Field         ( "recurring_payment_ind"     , "Recurring Pmt Ind"       , 1   , 3 , true  ) );

    fgroup.add( new Field         ( "acquirer_code"             , "Acquirer Code"           , 8   ,10 , true  ) );
    fgroup.add( new Field         ( "clearing_nsu"              , "Clearing NSU"            ,12   ,14 , true  ) );
    fgroup.add( new Field         ( "dest_id"                   , "Destination ID"          , 5   , 7 , true  ) );
    fgroup.add( new Field         ( "issuer_bank_id"            , "Issuer Bank ID"          , 5   , 7 , true  ) );
    fgroup.add( new HiddenField   ( "file_format"        ) );
    fgroup.add( new HiddenDateField("movement_date"      ) );
    fgroup.add( new Field         ( "settlement_type"           , "Settlement Type"         , 1   , 3 , true  ) );
    fgroup.add( new CurrencyField ( "ic_expense"                , "IC Expense"              ,12   ,14 , false ) );
  }    
  
  public String getCardTypeEnhanced()
  {
    return( getString("card_type_enhanced") );
  }
  
  public boolean isCardNumberValid()
  {
    return( ( getInt("dest_id") == 0 ) ? false : super.isCardNumberValid() );
  }
  
  public void setFieldsBatchData(VisakBatchData batchData)
    throws java.sql.SQLException
  {
    if( batchData != null )
    {
      super.setFieldsBatchData(batchData);

      setData( "acquirer_business_id"     , batchData.getAcquirerBusinessId());
      setData( "bin_number"               , batchData.getBinNumber());
      setData( "eligibility_flags"        , batchData.getEligibilityFlagsVisa() );
      setData( "pos_term_cap"             , batchData.getPosTermCap());
    }
  }

  public void setFields_ModBII()
    throws java.sql.SQLException
  {
    ArdefDataModBII ardef         = SettlementDb.loadArdefModBII(getData("card_number_full"));

    // set fields that depend on the ARDEF data
    setArdefData(ardef);
    setData( "card_type_enhanced"   , getData("cielo_card_association") );    // override in case card# not in modbii_ardef_table

    if( "DB0".equals(getData("dbcr_installtype")) )         // if tran arrived in  debito file
    {
      setData( "dest_id"              , ardef.getDestIdDebit()             );
      setData( "issuer_bank_id"       , ardef.getIssuerBankIdDebit()       );
    }
    else                                                    // if tran arrived in credito file
    {
      setData( "dest_id"              , ardef.getDestIdCredit()            );
      setData( "issuer_bank_id"       , ardef.getIssuerBankIdCredit()      );
    }

    // cannot call until after batch_id and batch_record_id are set
    setAuthData();
  }

  public void setFieldsBase(VisakBatchData batchData, ResultSet resultSet, int batchType)
    throws java.sql.SQLException
  {
    RecBatchType = batchType;
    super.setFieldsBase(batchData, resultSet, batchType);
    setFields_ModBII();
  }
}
