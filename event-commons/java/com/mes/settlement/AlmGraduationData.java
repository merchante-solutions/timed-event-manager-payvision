/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/AlmGraduationData.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;

public class AlmGraduationData
{
  protected String    GCMSProductId                     = null;
  protected String    ProductClass                      = null;

  public AlmGraduationData( )
  {
  }
  
  public AlmGraduationData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setGCMSProductId( resultSet.getString("gcms_product_id") );
    setProductClass( resultSet.getString("product_class") );
  }
  
  public String getGCMSProductId()              { return(GCMSProductId); }
  public void setGCMSProductId( String value )  { GCMSProductId = value; }
  
  public String getProductClass()               { return(ProductClass); }
  public void setProductClass( String value )   { ProductClass = value; }
  
  public void showData()
  {
    System.out.println("GCMSProductId                 = " + getGCMSProductId());
    System.out.println("ProductClass                  = " + getProductClass());
  }
}
