/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeVisa.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $LastChangedDate: 2015-10-23 18:06:40 -0700 (Fri, 23 Oct 2015) $
  Version            : $Revision: 23904 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.google.common.collect.Table;
import com.mes.settlement.Fpi.Program;
import com.mes.support.MesMath;
import com.mes.support.SyncLog;

public class InterchangeVisa        extends InterchangeBase
{
  static Logger log = Logger.getLogger(InterchangeVisa.class);

  public static final String      MY_CARD_TYPE          = "VS";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeVisa$";


  public static class CardTypeVisa
  {
    // internally created value (all card type info in one integer bit field)
    public static final int   UNKNOWN                       = 0;

    public static final int   CONSUMER                      = 0x0001;
    public static final int   COMMERCIAL                    = 0x0002;

    public static final int   CREDIT                        = 0x0004;
    public static final int   DEBIT                         = 0x0008;

    public static final int   PREPAID                       = 0x0010;   // "DEBIT" bit should also be set
//  public static final int   spare                         = 0x0020;   // currently unused

    public static final int   ANY_CONSUMER_OR_COMMERCIAL    = CONSUMER  | COMMERCIAL;
    public static final int   ANY_CREDIT_OR_DEBIT           = CREDIT    | DEBIT;
    //                        ANY_CARD                      = 0x003F;   // consumer/commercial, credit/debit, prepaid, spare
    public static final int   ANY_PROGRAM                   = 0x0FC0;
    public static final int   ANY_SUB_PROGRAM               = 0xF000;

    public static final int   ANY_CNSM_OR_COMM_PROGRAM      = ANY_CONSUMER_OR_COMMERCIAL | ANY_PROGRAM;

    public static final int   CONSUMER_CREDIT               = CONSUMER    | CREDIT;
    public static final int   CONSUMER_DEBIT                = CONSUMER    | DEBIT;

    public static final int   CONSUMER_TRADITIONAL          = CONSUMER    | 0x0100;
    public static final int   CONSUMER_TRADITIONAL_REWARDS  = CONSUMER    | 0x0200;
    public static final int   CONSUMER_SIGNATURE            = CONSUMER    | 0x0400;
    public static final int   CONSUMER_SIG_PREFERRED        = CONSUMER    | 0x0440; // US-issued "signature preferred"
    public static final int   CONSUMER_HIGH_NET_WORTH       = CONSUMER    | 0x0480; // US-issued "??" & non-US "infinite"

    public static final int   CNSM_ELECTRON                 = CONSUMER    |         0x1000;
    public static final int   CNSM_PREMIUM                  = CONSUMER    |         0x2000;

    public static final int   COMMERCIAL_BUSINESS           = COMMERCIAL  | 0x0100;
    public static final int   COMMERCIAL_BUS_SIG            = COMMERCIAL  | 0x0140;
    public static final int   COMMERCIAL_BUS_ENH            = COMMERCIAL  | 0x0180;
    public static final int   COMMERCIAL_BUS_T4             = COMMERCIAL  | 0x0440;
    public static final int   COMMERCIAL_CORPORATE          = COMMERCIAL  | 0x0200;
    public static final int   COMMERCIAL_PURCHASING         = COMMERCIAL  | 0x0400;
    public static final int   COMMERCIAL_PURCH_VLPA         = COMMERCIAL  | 0x0480;
    public static final int   COMMERCIAL_BUS_T5             = COMMERCIAL  | 0x0280;

    public static final int   COMM_GSA                      = COMMERCIAL  |         0x1000;
    public static final int   COMM_FLEET                    = COMMERCIAL  |         0x2000;
    public static final int   COMM_LRGTKT                   = COMMERCIAL  |         0x4000;
    public static final int   COMM_B2B                      = COMMERCIAL  |         0x8000;
    public static final int   COMM_GSA_FLEET                = COMM_GSA | COMM_FLEET;


    public static boolean isVisaCommercialProduct( String strPid )
    {
      boolean retVal = false;
      switch( (strPid + " ").charAt(0) )
      {
        case 'G': // business
        case 'K': // corporate
        case 'S': // purchasing
          retVal = true;
          break;
      }
      return( retVal );
    }


    public static final int getBitField( SettlementRecord tranInfo )
    {
      int     retVal            = UNKNOWN;
      String  strArdefData      = tranInfo.getPaddedString   ( "ardef_data"           , 16);
      String  strCardTypeEnh    = tranInfo.getPaddedString   ( "card_type_enhanced"   ,  3);
      String  strProdId         = tranInfo.getPaddedString   ( "product_id"           ,  2);

      // first         decode  auth pid = tranInfo.product_id
      // if fail, then decode ardef pid = tranInfo.card_type_enhanced.substring(0,2)
      for( int idx = 0; idx < 2; ++idx )
      {
        String  thisProdId = (idx == 0) ? strProdId : strCardTypeEnh.substring(0,2);
        if( thisProdId.trim().length() == 0 )continue;

        switch( (thisProdId.charAt(0) * 0x100) + thisProdId.charAt(1) )
        {
          /* "A "  VISA_TRADITIONAL          */ case  (('A'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
//        /* "AX"  AMERICAN_EXPRESS          */ case  (('A'*0x100)+'X') :    MeS does not use
          /* "B "  VISA_TRADITIONAL_REWARDS  */ case  (('B'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL_REWARDS             ; break;
          /* "C "  VISA_SIGNATURE            */ case  (('C'*0x100)+' ') : retVal = CONSUMER_SIGNATURE                       ; break;
          /* "D "  VISA_SIGNATURE_PREFERRED  */ case  (('D'*0x100)+' ') : retVal = CONSUMER_SIG_PREFERRED                   ; break;
//        /* "DI"  DISCOVER                  */ case  (('D'*0x100)+'I') :    MeS does not use
//        /* "DN"  DINERS                    */ case  (('D'*0x100)+'N') :    MeS does not use
//        /* "E "  PROPRIETARY_ATM           */ case  (('E'*0x100)+' ') :    MeS does not use
          /* "F "  VISA_CLASSIC              */ case  (('F'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
          /* "G "  VISA_BUSINESS             */ case  (('G'*0x100)+' ') : retVal = COMMERCIAL_BUSINESS                      ; break;
          /* "G1"  VISA_BUSINESS_SIGNATURE   */ case  (('G'*0x100)+'1') : retVal = COMMERCIAL_BUS_SIG                       ; break;
          /* "G3"  VISA_BUSINESS_ENHANCED    */ case  (('G'*0x100)+'3') : retVal = COMMERCIAL_BUS_ENH                       ; break;
          /* "G4"  VISA_BUSINESS_INF_PRIV    */ case  (('G'*0x100)+'4') : retVal = COMMERCIAL_BUS_T4                        ; break;
          /* "G5"  VISA_BUSINESS_TIER_5      */ case  (('G'*0x100)+'5') : retVal = COMMERCIAL_BUS_T5                        ; break;
          /* "I "  VISA_INFINITE             */ case  (('I'*0x100)+' ') : if( !"US1".equals(tranInfo.getPaddedString( "region_issuer"       , 3)) ||
                                                                                 "Q".equals(tranInfo.getPaddedString( "spend_qualified_ind" , 1)) )
                                                                            retVal = CONSUMER_HIGH_NET_WORTH                  ;
                                                                          else
                                                                            retVal = CONSUMER_SIGNATURE                       ;
                                                                                                                              break;
          /* "I1"  VISA_INFINITE_PRIVILEGE   */ case  (('I'*0x100)+'1') : retVal = CONSUMER_SIGNATURE                       ; break;
          /* "I2"  VISA_ULTRA_HIGH_NET_WORTH */ case  (('I'*0x100)+'2') : retVal = CONSUMER_SIGNATURE                       ; break;
          /* "J3"  VISA_HEALTHCARE           */ case  (('J'*0x100)+'3') : retVal = CONSUMER_TRADITIONAL                     ; break;
//        /* "JC"  JCB                       */ case  (('J'*0x100)+'C') :    MeS does not use
          /* "K "  VISA_CORPORATE_T_E        */ case  (('K'*0x100)+' ') : retVal = COMMERCIAL_CORPORATE                     ; break;
          /* "K1"  VISA_CORPORATE_T_E_GSA    */ case  (('K'*0x100)+'1') : retVal = COMMERCIAL_CORPORATE | COMM_GSA          ; break;
          /* "L "  VISA_ELECTRON             */ case  (('L'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL | CNSM_ELECTRON     ; break;
//        /* "M "  MASTERCARD                */ case  (('M'*0x100)+' ') :    MeS does not use
          /* "N "  VISA_PLATINUM             */ case  (('N'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL | CNSM_PREMIUM      ; break;
          /* "N1"  VISA_REWARDS              */ case  (('N'*0x100)+'1') : retVal = CONSUMER_TRADITIONAL | CNSM_PREMIUM      ; break;
          /* "N2"  VISA_SELECT               */ case  (('N'*0x100)+'2') : retVal = CONSUMER_TRADITIONAL | CNSM_PREMIUM      ; break;
          /* "P "  VISA_GOLD                 */ case  (('P'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
          /* "Q "  PRIVATE_LABEL             */ case  (('Q'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
//        /* "R "  PROPRIETARY               */ case  (('R'*0x100)+' ') :    MeS does not use
          /* "S "  VISA_PURCHASING           */ case  (('S'*0x100)+' ') : retVal = COMMERCIAL_PURCHASING                    ; break;
          /* "S1"  VISA_PURCHASING_FLEET     */ case  (('S'*0x100)+'1') : retVal = COMMERCIAL_PURCHASING | COMM_FLEET       ; break;
          /* "S2"  VISA_PURCHASING_GSA       */ case  (('S'*0x100)+'2') : retVal = COMMERCIAL_PURCHASING | COMM_GSA         ; break;
          /* "S3"  VISA_PURCHASING_GSA_FLEET */ case  (('S'*0x100)+'3') : retVal = COMMERCIAL_PURCHASING | COMM_GSA_FLEET   ; break;
          /* "S4"  VISA_GOVT_SERVICES_LOAN   */ case  (('S'*0x100)+'4') : retVal = COMMERCIAL_PURCHASING                    ; break;
          /* "S5"  VISA_COMM_TRANSPORT_EBT   */ case  (('S'*0x100)+'5') : retVal = COMMERCIAL_BUSINESS                      ; break;
          /* "S6"  VISA_BUSINESS_LOAN        */ case  (('S'*0x100)+'6') : retVal = COMMERCIAL_PURCHASING                    ; break;
          /* "S7"  VISA_DISTRIBUTION         */ case  (('S'*0x100)+'7') : retVal = COMMERCIAL_PURCHASING                    ; break;  // retired in Oct 2013
          /* "U "  VISA_TRAVEL_MONEY         */ case  (('U'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
          /* "V "  VISA_V_PAY                */ case  (('V'*0x100)+' ') : retVal = CONSUMER_TRADITIONAL                     ; break;
        }

        if( retVal != UNKNOWN )break;
      }

    // the "ardef_data" string is set in ArdefDataVisa.java; must match this position formatting
    // (and cannot change format without considering strings already saved in visa_settlement)

//    if( strArdefData.charAt ( 0 ) == 'Y' ) is account level processing
      if( strArdefData.charAt ( 1 ) == 'L' ) retVal |= COMM_LRGTKT;
      if( strArdefData.charAt ( 2 ) == 'L' &&
          strArdefData.charAt ( 3 ) == 'P' ) retVal |= COMMERCIAL_PURCH_VLPA;
//    if( strArdefData.charAt ( 2 ) == 'M' )
//    {
//      if( strArdefData.charAt ( 3 ) == 'A' ) is mobile prepaid agent
//      if( strArdefData.charAt ( 3 ) == 'B' ) is interoperable mobile branchless banking
//      if( strArdefData.charAt ( 3 ) == 'G' ) is mobile prepaid general
//    }
      if( strArdefData.charAt ( 4 ) == 'A' ) retVal  = UNKNOWN;   // is ATM card, need to reject
//    if( strArdefData.charAt ( 5 ) == 'B' ) retVal |= COMM_B2B;
//    if( strArdefData.charAt ( 6 ) == 'A' ) is travel account "air"
//    if( strArdefData.charAt ( 6 ) == 'H' ) is travel account "hotel"


      switch( strCardTypeEnh.charAt(2) )
      {
                            // NOTE: if add any debit funding sources here, also add to ArdefDataVisa.getCardTypeFull()
        /* credit   */  case 'C'  : retVal |= CREDIT          ; break;
        /* debit    */  case 'D'  : retVal |= DEBIT           ; break;
        /* prepaid  */  case 'P'  : retVal |= DEBIT | PREPAID ; break;
        /* charge   */  case 'H'  : retVal |= CREDIT          ; break;
        /* deferd db*/  case 'R'  : retVal |= CREDIT          ; break;
        /* ???      */  default   : retVal  = UNKNOWN         ; break;
      }

      // make sure we gathered enough info about the card to know how to handle it
      if( ( ( retVal & (ANY_CONSUMER_OR_COMMERCIAL) ) == UNKNOWN ) || // must be consumer or commercial
          ( ( retVal & (ANY_PROGRAM               ) ) == UNKNOWN ) || // must be some program
          ( ( retVal & (ANY_CREDIT_OR_DEBIT       ) ) == UNKNOWN ) )  // must be credit or debit
      {
        log.info("cannot create tran card type bitfield: pid=" + strProdId + " and cte=" + strCardTypeEnh + " and ardefData=" + strArdefData );

        retVal = UNKNOWN;
      }
      log.debug("[getBitField] - bit value for card = " + retVal);
      return( retVal );
    }
  }   // end of "class CardTypeVisa"

                                                                    // card-type  -- irf category
//==================================================================//============================================
  public static final String  FPI_MeS_FAILED_TO_QUALIFY   = "-00";  //  MeS       -- failed to pick a Visa FPI
  public static final String  FPI_MeS_CASH_DISB_DOMESTIC  = "-CD";  //  MeS       -- cash disbursement - issuer & merchant in same country
  public static final String  FPI_MeS_CASH_DISB_FOREIGN   = "-CF";  //  MeS       -- cash disbursement - issuer & merchant in different countries

  // Brazil Domestic NNSS transactions have no fpi's; we are using "-BR" to find rows in ic_desc but will always transmit spaces.
  public static final String  FPI_MeS_BR_NNSS             = "-BR";  //  MeS       -- Brazil NNSS Domestic - all differentiation in ic_desc

  // Puerto Rico Domestic transactions have no fpi's; we are using "-P?" to find rows in ic_desc but will always transmit spaces.
  // Transactions qualify for either electronic or standard in one of the four card_type categories.  Electronic rates vary by sic_code,
  // but that's handled in ic_desc. (Note: Since retail 1 is the only sic-group with worse rates than rtl 2, rtl 2 sics = all sics not in rtl 1.)
  public static final String  FPI_MeS_PR_REG_WITH_FRAUD   = "-PF";  //  MeS       -- Puerto Rico Domestic - Regulated w/Fraud

  public static final String  FPI_US_IR_REG_WITH_FRAUD    = "603";  //  Any Reg'd -- Inter-Regional Merchant - Regulated w/Fraud

  public static final String  FPI_US_IR_CON_STD           = "901";  //  Consumer  -- Inter-Regional Merchant -- Standard
  public static final String  FPI_US_IR_CON_ELEC          = "903";  //  Consumer  -- Inter-Regional Merchant -- Electronic (pre-PS2K)
  public static final String  FPI_US_IR_CON_ECOMM         = "918";  //  Consumer  -- Inter-Regional Merchant -- eComm non-authenticated
  public static final String  FPI_US_IR_CON_ECOMMS        = "919";  //  Consumer  -- Inter-Regional Merchant -- eComm secure
  public static final String  FPI_US_IR_CON_AIR           = "920";  //  Consumer  -- Inter-Regional Merchant -- airline
  public static final String  FPI_US_IR_CON_CHIP_I        = "917";  //  Consumer  -- Inter-Regional Merchant -- issuer chip
  public static final String  FPI_US_IR_CON_CHIP_A        = "915";  //  Consumer  -- Inter-Regional Merchant -- acquirer chip

  public static final String  FPI_US_IR_PREMIUM           = "947";  //  "premium" -- Inter-Regional Merchant -- non-north-Am Platinum, Canadian Infinite, US Signature


  public static final String  FPI_US_CV_PASS_TRANS        = "165";  //  (all)     -- Credit Voucher -- Passenger Transport
  public static final String  FPI_US_CV_COMMERCIAL        = "167";  //  Commercial-- Credit Voucher -- Non-PT, Non-Purchasing
  public static final String  FPI_US_CV_PURCH_GSA1        = "352";  //  Purchasing-- Credit Voucher -- Non-PT, GSA 1
  public static final String  FPI_US_CV_PURCH_GSA2        = "353";  //  Purchasing-- Credit Voucher -- Non-PT, GSA 2
  public static final String  FPI_US_CV_PURCH_GSA3        = "354";  //  Purchasing-- Credit Voucher -- Non-PT, GSA 3
  public static final String  FPI_US_CV_PURCH_GSA4        = "355";  //  Purchasing-- Credit Voucher -- Non-PT, GSA 4
  public static final String  FPI_US_CV_PURCH_GSA5        = "356";  //  Purchasing-- Credit Voucher -- Non-PT, GSA 5
  public static final String  FPI_US_CV_PURCH_1           = "357";  //  Purchasing-- Credit Voucher -- Non-PT, Non-GSA 1
  public static final String  FPI_US_CV_PURCH_2           = "358";  //  Purchasing-- Credit Voucher -- Non-PT, Non-GSA 2
  public static final String  FPI_US_CV_PURCH_3           = "359";  //  Purchasing-- Credit Voucher -- Non-PT, Non-GSA 3
  public static final String  FPI_US_CV_PURCH_4           = "360";  //  Purchasing-- Credit Voucher -- Non-PT, Non-GSA 4
  public static final String  FPI_US_CV_PURCH_5           = "361";  //  Purchasing-- Credit Voucher -- Non-PT, Non-GSA 5
  public static final String  FPI_US_CV_CNSR_CR           = "166";  //  Cnsmr CR  -- Credit Voucher -- Non-PT
  public static final String  FPI_US_CV_CNSR_CR_MOTO      = "164";  //  Cnsmr CR  -- Credit Voucher -- Non-PT MOTO/e-commerce
  public static final String  FPI_US_CV_DEBIT             = "369";  //  any Debit -- Credit Voucher --

  public static final String  FPI_US_REG_WITH_FRAUD       = "338";  //  Any Reg'd -- US Domestic -- Regulated w/Fraud
  public static final String  FPI_US_REG_WITH_FRAUD_STKT  = "331";  //  Any Reg'd -- US Domestic -- Regulated w/Fraud -- Small Ticket


  public static final String  FPI_US_CR_CPS_RTL_TR        = "RCT";  //  Cnsmr CR  -- CPS/Retail - Threshold
  public static final String  FPI_US_CR_CPS_SPMKT_TR      = "SCT";  //  Cnsmr CR  -- CPS/Supermarket - Threshold
  public static final String  FPI_US_CR_CPS_SML_TKT       = "179";  //  Cnsmr CR  -- CPS/Small Ticket

  public static final String  FPI_US_CR_CPS_CHARITY       = "379";  //  Cnsmr CR  -- CPS/Charity

  public static final String  FPI_US_DB_CPS_DEBT_REPAY    = "DPT";  //  Cnsmr DB  -- CPS/Debit Debt Repayment

  public static final String  FPI_US_STP_TR1              = "392";  // Purchasing/Corporate -- US STP Tier 1 (amount <= 6999.99)
  public static final String  FPI_US_STP_TR2              = "393";  // Purchasing/Corporate -- US STP Tier 2 (amount between 7k and  14,999.99)
  public static final String  FPI_US_STP_TR3              = "394";  // Purchasing/Corporate -- US STP Tier 3 (amount between 15k and 49,999.99)
  public static final String  FPI_US_STP_TR4              = "395";  // Purchasing/Corporate -- US STP Tier 4 (amount between 50k and 99,999.99)
  public static final String  FPI_US_STP_TR5              = "396";  // Purchasing/Corporate -- US STP Tier 5 (amount >= 100k)

  public static final String  FPI_US_CORP_CP              = "235";  //  Corporate -- Retail
  public static final String  FPI_US_CORP_CNP             = "234";  //  Corporate -- Card Not Present

  public static final String  FPI_US_PURCH_CP             = "238";  //  Purchasing-- Retail
  public static final String  FPI_US_PURCH_CNP            = "237";  //  Purchasing-- Card Not Present

  public static final String  FPI_US_PURCH_LRG_TKT        = "156";  //  Purchasing-- Large Ticket
  public static final String  FPI_US_PURCH_LRG_TKT_PP     = "367";  //  Purchasing-- Large Ticket -- Prepaid
  public static final String  FPI_US_PURCH_LRG_TKT_GSA_0  = "147";  //  Purchasing-- Large Ticket -- GSA
  public static final String  FPI_US_PURCH_GSA_G2G        = "G2G";  //  Purchasing-- Government-to-Govt -- GSA
  public static final String  FPI_US_PURCH_VLPA1          = "267";  //  Purchasing-- Visa Large Purchase Advantage 1
  public static final String  FPI_US_PURCH_VLPA2          = "268";  //  Purchasing-- Visa Large Purchase Advantage 2
  public static final String  FPI_US_PURCH_VLPA3          = "269";  //  Purchasing-- Visa Large Purchase Advantage 3
  public static final String  FPI_US_PURCH_VLPA4          = "270";  //  Purchasing-- Visa Large Purchase Advantage 4

  public static final String  FPI_CPS_RECUR_BILL_PMT      = "160";  //  CPS/Recurring Payment
  public static final String FPI_US_GLB_B2B_VTL_PYMT      = "961";  //  Global B2B Virtual Payments

  // reimbursement attributes (RA)
  public static final char    RA_STANDARD                         = '0';
  public static final char    RA_SUPERMARKET                      = '4';
  public static final char    RA_CPS_EMERGING_MARKET              = '6';
//public static final char    RA_PIN_PIN_DEBIT                    = '8';
  public static final char    RA_CPS                              = 'A';
  public static final char    RA_INTERREGIONAL_PRE_PS2K           = 'B';
  public static final char    RA_INTERREGIONAL_AIRLINE            = 'C';
  public static final char    RA_ELECTRONIC                       = 'J';
  public static final char    RA_GSA_LARGE_TKT_NON_T_E            = 'N';
  public static final char    RA_CONSUMER_MOTO_ECOMM_RETURN       = 'T';
//public static final char    RA_PIN_SMS_SUPERMARKET              = 'Y';


  // requested payment services (RPS)
  public static final char    RPS_NONE                            = ' ';
  public static final char    RPS_SMALL_TICKET                    = 'F';


  // Visa irfInfo string format: "eeaaabc,eeaaabc,eeaaabc"  where ee=enhancements, aaa=fpi, b=RA, c=RPS
  public static final int     LOC_FPI           = SettlementRecord.NUM_ENHANCED_IND + 0;    // "aaa" = fpi = 3 chars
  public static final int     LOC_RA            = SettlementRecord.NUM_ENHANCED_IND + 3;    // "b"   = ra  = 1 char
  public static final int     LOC_RPS           = SettlementRecord.NUM_ENHANCED_IND + 4;    // "c"   = rps = 1 char
  public static final int     LEN_IRF_INFO      = SettlementRecord.NUM_ENHANCED_IND + 5;    // enh + (fpi + ra + rps)
  public static final int     LEN_ISSUER_IC_LVL = 3;                                        // fpi
  public static final String  IRF_INFO_UNKNOWN  = SettlementRecord.DEF_ENHANCED_IND + FPI_MeS_FAILED_TO_QUALIFY + String.valueOf(RA_STANDARD) + String.valueOf(RPS_NONE);
  public static final String  IRF_INFO_REG_PR   = SettlementRecord.DEF_ENHANCED_IND + FPI_MeS_PR_REG_WITH_FRAUD + String.valueOf(RA_STANDARD) + String.valueOf(RPS_NONE);
  public static final String  IRF_INFO_REG_IR   = SettlementRecord.DEF_ENHANCED_IND + FPI_US_IR_REG_WITH_FRAUD  + String.valueOf(RA_STANDARD) + String.valueOf(RPS_NONE);
  public static final String  IRF_INFO_REG_US   = SettlementRecord.DEF_ENHANCED_IND + FPI_US_REG_WITH_FRAUD     + String.valueOf(RA_STANDARD) + String.valueOf(RPS_NONE);
  public static final String  IRF_INFO_REG_STKT = SettlementRecord.DEF_ENHANCED_IND + FPI_US_REG_WITH_FRAUD_STKT+ String.valueOf(RA_STANDARD) + String.valueOf(RPS_NONE);

  //Visa mandate for Oct-2018 
  public static final List<String> SIC_RELIGIOUS_CHARITY     = Arrays.asList("8398","8661");
  public static final List<String> FPI_CHARITY_RETAIL2_DB_PP = Arrays.asList("379","193","315");
  public static final String       RA_RELIGIOUS_CHARITY      = "D";
  public static final String       US_MERCHANT               = "US";

  private static  Map<String,VisaProgram> nonCpsProgramMap;
  protected  static  Map<String,VisaProgram> cpsProgramMap;
  private static  Table<String, Fpi.CardProduct, String> fpiTable;
  private static  Integer programListCount;
  private static  Map<Fpi.SicCodeGroup,String> sicCodeGroupMap;


  // ********************************************************************************
  // ********************************************************************************
  public static abstract class IC_Visa                                                              extends IC_BaseClass
  {
    public        final Fpi.Program     myProgram;
    public                      IC_Visa( Fpi.Program  thisProgram )  { myProgram = thisProgram ; }
    
    // member eligibility flags (whichever ones have been set up for this merchant; defined by MeS)
    public static final String  ELIG_FLAG_GOVT_2_GOVT     = "G2G";    // Government To Government
    public static final String  ELIG_FLAG_DEBT_REPAYMENT  = "RPY";    // Debt Repayment
    public static final String  ELIG_FLAG_DEBIT_TAX_FEE   = "TAX";    // Debit Tax Fee
    public static final String  ELIG_FLAG_THRESHOLD_1     = "TH1";    // Threshold 1 (retail or supermarket) (warehouse, merit III, supermarket)
    public static final String  ELIG_FLAG_THRESHOLD_2     = "TH2";    // Threshold 2 (retail or supermarket) (warehouse, merit III, supermarket)
    public static final String  ELIG_FLAG_THRESHOLD_3     = "TH3";    // Threshold 3 (retail or supermarket) (warehouse, merit III, supermarket)
    public static final String  ELIG_FLAG_UTILITY         = "UTL";    // Utility

    //Constant for B2B Virtual Payment Product
    public static final String CONST_GLB_VTL_PYMT			= "X";


    // column numbers for the "Response Strings" array
    public static final int   RESP_STR_FPI_CONSUMER_PREPAID          =  0;
    public static final int   RESP_STR_FPI_CONSUMER_DEBIT            =  1;
    public static final int   RESP_STR_FPI_CONSUMER_CREDIT           =  2;
    public static final int   RESP_STR_FPI_CONSUMER_REWARDS          =  3;
    public static final int   RESP_STR_FPI_CONSUMER_SIGNATURE        =  4;
    public static final int   RESP_STR_FPI_CONSUMER_SIG_PREFERRED    =  5;
    public static final int   RESP_STR_FPI_CONSUMER_HIGH_NET_WORTH   =  6;
    public static final int   RESP_STR_FPI_COMMERCIAL_CORPORATE      = 11;
    public static final int   RESP_STR_FPI_COMMERCIAL_PURCHASING     = 12;

    public static final double GOVNMT_FEE_CPS_SML_TKT_AMOUNT_LIMIT = 15.00;


    public String   getIrfInfo(SettlementRecord tranInfo, int bfCardType, String enhRequired, VisaProgram program)
    {
      try
      {
        boolean isDebit           = ( (bfCardType & CardTypeVisa.DEBIT)           == CardTypeVisa.DEBIT           );
        boolean isPrepaid         = ( (bfCardType & CardTypeVisa.PREPAID)         == CardTypeVisa.PREPAID         );
        boolean checkEnhancements = false;
        String  strIrfInfo        = "";
        Byte    authSrc           = tranInfo.getByte("auth_source_cde");
        boolean isSTP             = (authSrc == '1' || authSrc == '2' || authSrc == '3' || authSrc == '4');
        String  prodId             = tranInfo.getPaddedString("product_id",2);//"S6".equals(tranInfo.getPaddedString("product_id",2))
        boolean checkProd          = ( prodId.equals("S ") || prodId.equals("S1") ||
                                       prodId.equals("S2") || prodId.equals("S3") ||
                                       prodId.equals("K ") || prodId.equals("K1") );
        final String JAPAN = "JP";
        
        do
        {
          Program row_program  = (myProgram.getIntValue() > programListCount) ? Program.STANDARD : myProgram ;
          Fpi.CardProduct col_card_type ;
          String  strFPI          = null;
          String  strRA           = convertProgramToVisaProgram(row_program).getReimbursementAttribute();
          String  strRPS          = (tranInfo.getByte("market_specific_data_ind") == 'A') ? convertProgramToVisaProgram(row_program).getRpsAuto():
                  convertProgramToVisaProgram(row_program).getRpsNonAuto();
          String  strEtcFinal     = strRA + strRPS;
          String  strEtc          = strRA + strRPS + ",";

          // select a column in aRespStrs
          switch( bfCardType & CardTypeVisa.ANY_CNSM_OR_COMM_PROGRAM )
          {
            case CardTypeVisa.CONSUMER_TRADITIONAL          :
                   if( isPrepaid )                            col_card_type = Fpi.CardProduct.CONSUMER_PREPAID;
              else if( isDebit )                              col_card_type = Fpi.CardProduct.CONSUMER_DEBIT;
              else                                            col_card_type = Fpi.CardProduct.CONSUMER_CREDIT; break;
            case CardTypeVisa.CONSUMER_TRADITIONAL_REWARDS  : col_card_type = Fpi.CardProduct.CONSUMER_REWARDS; break;
            case CardTypeVisa.CONSUMER_SIGNATURE            : col_card_type = Fpi.CardProduct.CONSUMER_SIGNATURE; break;
            case CardTypeVisa.CONSUMER_SIG_PREFERRED        : col_card_type = Fpi.CardProduct.CONSUMER_SIGNATURE_PREFERRED; break;
            case CardTypeVisa.CONSUMER_HIGH_NET_WORTH       : col_card_type = Fpi.CardProduct.CONSUMER_HI_NET; break;

            case CardTypeVisa.COMMERCIAL_BUSINESS           :
                   if( isDebit )                              col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_DEBIT;
              else                                            col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_T1; break;
            case CardTypeVisa.COMMERCIAL_BUS_SIG            : col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_SIGNATURE_T3; break;
            case CardTypeVisa.COMMERCIAL_BUS_ENH            : col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_ENHANCED_T2; break;
            case CardTypeVisa.COMMERCIAL_BUS_T4             : col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_T4; break;
            case CardTypeVisa.COMMERCIAL_BUS_T5             : col_card_type = Fpi.CardProduct.COMMERCIAL_BUSINESS_T5; break;
            case CardTypeVisa.COMMERCIAL_CORPORATE          : col_card_type = Fpi.CardProduct.COMMERCIAL_CORPORATE; break;
            case CardTypeVisa.COMMERCIAL_PURCHASING         :
            case CardTypeVisa.COMMERCIAL_PURCH_VLPA         :
                                                              col_card_type = Fpi.CardProduct.COMMERCIAL_PURCHASED; break;

          default:
              System.out.println("can't calc fpi for card type: " + bfCardType );
              return( null );
          }

          if( myProgram == Program.PROGRAM_CASH_DISBURSEMENT )
          {
            // if tran_region_issuer == tran_region_merchant then "domestic" else "foreign"
            if( tranInfo.getPaddedString("region_issuer",3).equals(tranInfo.getPaddedString("region_merchant",3)) )
                  strFPI = FPI_MeS_CASH_DISB_DOMESTIC;
            else  strFPI = FPI_MeS_CASH_DISB_FOREIGN;
          }
          else if( myProgram == Program.PROGRAM_NOT_US_DOMESTIC )
          {
                // tran_region_XXX = two-char country code followed by one-char region code; for merchant & issuer regions....
                //      if regions don't match        , inter-regional
                //      if regions match              , intra-regional
                //      if countries & regions match  , domestic
            String    tran_region_issuer    = tranInfo.getPaddedString("region_issuer"  ,3);
            String    tran_region_merchant  = tranInfo.getPaddedString("region_merchant",3);
            boolean   isDomestic            = tran_region_merchant.equals(tran_region_issuer);
            boolean   isRegional            = tran_region_merchant.charAt(2) == tran_region_issuer.charAt(2);

            if( !testDataIsInList( 2, tran_region_merchant.substring(0,2), "BR,US,PR,VI" ) )
            {
              strFPI = FPI_MeS_FAILED_TO_QUALIFY;   // we have no code for whatever country this is
            }
            else if( isDomestic &&  "BR".equals(tran_region_merchant.substring(0,2))
                                &&   "Y".equals(tranInfo.getPaddedString("ardef_data",16).substring(7,8))
                                && "986".equals(tranInfo.getString("currency_code")) )   // Brazil Domestic
            {
              strFPI = FPI_MeS_BR_NNSS;     // one "FPI" for all Brazilian NNSS transactions; ic_desc does most of the work


                    String        tran_sic_code = tranInfo.getString("sic_code");
                    String        sicGroup      = "~OTH";     // default to "other"
              final String[][]    sicGroups     =
              {
              //  0     , 1
              //  group , sic list
                { "~GAS", "5122,5172,5309,5311,5541,5542,5912,5983,7511"                                                                                      },  // Petroleum, Pharmacy, Dept Store
                { "~SMK", "5411,5422,5441,5451,5462,5499,5921"                                                                                                },  // Supermarket
                { "~TVL", "3351-3441,3501-3799,4411,4722,4723,5021,5094,5712,5714,5718,5719,5932,5937,5944,5950,5962-5969,5971,7011,7012,7512,7513,7519,7641" },  // Travel, Jewelry, Telemarketing
                { "~EM1", "0763,4784,4814,4899,5039,5051,5072,5074,5198,5300,5960,6211,6300,8211,8220,8241,8244,8249,8299"                                    },  // Emerging 1
                { "~EM2", "7276,8351,9211,9222,9223,9311,9399,9402,9405"                                                                                      },  // Emerging 2
                { "~EM3", "4900"                                                                                                                              },  // Emerging 3
                { "~B2B", "9950"                                                                                                                              },  // Business 2 Business
              };
              for( int i = 0; i < sicGroups.length; ++i )
              {
                if( testDataIsInList( 4, tran_sic_code, sicGroups[i][1] ) )
                {
                  sicGroup = sicGroups[i][0];
                  break;
                }
              }
              tranInfo.setData( "sic_group" , sicGroup );


              // set eligibility_flags to help in ic assignment;
              //   relies on mif.visa_eligibility_flags being unnecessary for Brazil NNSS;
              //   can't wipe out existing flags (eg if clearing reject) because cielo_product & cielo_tran_type not stored in visa_settlement
              if( !tranInfo.isFieldBlank("dbcr_installtype") )    // set in SettlementDb during extract
              {
                String  eFlags        = tranInfo.getPaddedString("ardef_data",4).substring(2,4) + "x";
                if( "DSx,VAx,VFx,VRx".indexOf(eFlags) >= 0 )
                {
                //eFlags = tran_ardef_data_product_subtype + "x";
                }
                else if( // "S6".equals(tranInfo.getPaddedString("product_id",2)) || --- as long as no BR NNSS ALP, ardef's Product ID is all we need to check
                         "S6".equals(tranInfo.getPaddedString("card_type_enhanced",3).substring(0,2)) )
                {
                  eFlags = "S6x";
                }
                else
                {
                  String  char3     = tranInfo.getData("dbcr_installtype").substring(2,3);
                  String  char2     = ( "5".equals(tranInfo.getPaddedString("moto_ecommerce_ind",1))) ? "V" : "x";
                  String  char1     = ( tranInfo.getInt("cielo_product"  )  ==  79  ) ? "3" : // Carne
                                     (( tranInfo.getInt("cielo_tran_type")  == 145 ||
                                        tranInfo.getInt("cielo_tran_type")  == 146  ) ? "1" : // vendas comiss.
                                                                                        "x");

                  eFlags = char1 + char2 + char3;
                }
                tranInfo.setData( "eligibility_flags" , eFlags );
              }
            }
            else if( isDomestic && "PR".equals(tran_region_merchant.substring(0,2)) )    // Puerto Rico Domestic
            {
              // except for premium cards and non-premium rewards cards, the card type column is already correct
              if( (bfCardType & CardTypeVisa.CNSM_PREMIUM) == CardTypeVisa.CNSM_PREMIUM )
              {
                col_card_type = Fpi.CardProduct.CONSUMER_REWARDS; // RESP_STR_FPI_CONSUMER_REWARDS;
              }
              else if( col_card_type == Fpi.CardProduct.CONSUMER_REWARDS )  // RESP_STR_FPI_CONSUMER_REWARDS )
              {
                col_card_type = ( isDebit ) ? Fpi.CardProduct.CONSUMER_DEBIT: Fpi.CardProduct.CONSUMER_CREDIT; //RESP_STR_FPI_CONSUMER_DEBIT : RESP_STR_FPI_CONSUMER_CREDIT;
              }

              // if it is a return OR a purchase with no more than 3 days from TranToClearing && a valid auth && pos entry mode in "05,07,90,91"
              if( "C".equals( tranInfo.getString("debit_credit_indicator") )                              ||
                  ( tranMeetsDateTimeliness_TranToClearing( tranInfo, 3 )                               &&
                    testDataIsInList( 1, tranInfo.getString("auth_source_code").equals("-")?"Z":tranInfo.getString("auth_source_code"), "Z,B,1-8,D-G" )            &&
                    testDataIsInList( 2, tranInfo.getPaddedString("pos_entry_mode",2), "05,07,90,91" )  ) )
              {
                strEtcFinal = String.valueOf(RA_INTERREGIONAL_PRE_PS2K) + strRPS;   // electronic for foreign merchants
                row_program = Program.PUERTO_RICO_DOMESTIC_ELECTRONIC; //FPI_ROW_PR_DOMESTIC_ELECTRONIC;
              }
              else
              {
              //strEtcFinal = standard ra & rps
                row_program = Program.PUERTO_RICO_DOMESTIC_STANDARD; //FPI_ROW_PR_DOMESTIC_STANDARD;
              }
            }
            else
            {
              // in the US inter-regional merchant row, some consumer columns are not used for their usual card types:
              //    column:     cnsmr prepaid   cnsmr debit     cnsmr credit    rewards         sig/vsp/hnw               bus*4/corp/purch
              //    used for:   consumer std    electron std                    premium         super premium             as usual
              //
              // in addition, basic & electron consumer cards (not signature or infinte consumer or any commercial) have several possible fpi's.

              boolean  isPremium = false;
              switch( tran_region_issuer.charAt(2) )
              {
                case '1':     // region: US
                  if( col_card_type == Fpi.CardProduct.CONSUMER_SIGNATURE )
                  {
                    isPremium = true;
                  }
                  break;
                case '2':     // region: Canada
                  if( col_card_type == Fpi.CardProduct.CONSUMER_HI_NET )
                  {
                    isPremium = true;
                  }
                  break;
                default:      // region: Asia Pacific, CEMEA, Visa Europe, LAC
                  if( (bfCardType & CardTypeVisa.CNSM_PREMIUM) == CardTypeVisa.CNSM_PREMIUM )
                  {
                    isPremium = true;
                  }
                  break;
              }
              
              if( isPremium && !isPrepaid )
              {
                col_card_type = Fpi.CardProduct.CONSUMER_REWARDS;
              }
              else if( col_card_type.getIntValue() <= Fpi.CardProduct.CONSUMER_REWARDS.getIntValue())
              {
                // set column for "consumer" or "electron"
                if( (bfCardType & CardTypeVisa.CNSM_ELECTRON) == CardTypeVisa.CNSM_ELECTRON &&
                    !isPrepaid                                                              ) col_card_type = Fpi.CardProduct.CONSUMER_DEBIT;
                else                                                                          col_card_type = Fpi.CardProduct.CONSUMER_PREPAID;
 
                /*
                 * As per article 2.8 Oct2015 CAU updates document, Visa platinum prepaid card (product id= N, funding source = P)
                 * from AP, CEMEA and LAC region receives FPI 947.
                 */
                if(isPremium && isPrepaid && (prodId.equals("N ")))
                {
                    strFPI = FPI_US_IR_PREMIUM;
                }


//     PIN note: as long as with & without PIN are the same rate && we wouldn't assign to countries that need FPI,
//                 don't need to differentiate w & w/out PIN
//   Other note: there are other rows where these "FPI"s aren't assigned quite accurately, but where we don't send FPI and the rate is the same
//                 or where it's about airline (since we don't have US airline merchants), we aren't bothering to differentiate them either
//                 (Especially because the docs seem incomplete; hard to be sure of rules; perhaps different rules for US and LAC interregional.)

                // check if qualify for any electronic fpi's
                char    indMotoEcomm  = tranInfo.getPaddedString("moto_ecommerce_ind",1).charAt(0);
                char    isAuthed      = (testDataIsInList( 1, tranInfo.getString("auth_source_code").equals("-")?"Z":tranInfo.getString("auth_source_code"), "Z,B,1-8,D-G" )) ? 'Y' : 'N';
                char    isAuthedY1Y3  = (testDataIsInList( 2, tranInfo.getPaddedString("auth_response_code",2), "Y1,Y3" )) ? 'Y' : isAuthed;
                char    isChipAcq     = ("5".equals( tranInfo.getPaddedString("pos_term_cap",1) )) ? 'Y' : 'N';
                char    isChipIssuer  = ("A".equals( tranInfo.getPaddedString("tech_ind",1) )) ? 'Y' : 'N';
// see PIN note.char    isChipPIN     = ( ??? ) ? 'Y' : 'N';
                boolean isPurchase    = "D".equals( tranInfo.getString("debit_credit_indicator") );
                int     offset        = (testDataIsInList( 4, tranInfo.getString("sic_code"), sicCodeGroupMap.get(Fpi.SicCodeGroup.AIRLINE)) &&
                                            !tranInfo.isFieldBlank("enhanced_data_1")) ? 3 : 0;
                String  posEntryMode  = tranInfo.getPaddedString("pos_entry_mode",2);
                String  strEtcAirline = String.valueOf(RA_INTERREGIONAL_AIRLINE  ) + strRPS + ",";
                String  strEtcPrePS2K = String.valueOf(RA_INTERREGIONAL_PRE_PS2K ) + strRPS + ",";

                String[][] irQualAndResp  =
                {
                //         #7/various: (0-1) clearing days, (2) separator, (3) moto/ecomm, (4) auth required, (5) auth or auth response Y1/Y3 required,
                //                     (6) separator, (7) issuer chip, (8) acquirer chip, (9) w/PIN

                //  ~~~~~~~~~~~~~~~~~~ non-airline (offset = 0) ~~~~~~~~~~~~~~~~~ , ~~~~~~~~~~~~~~~~~~~~ airline (offset = 3) ~~~~~~~~~~~~~~~~~~~ , ~~~~~~~~~~~~~ all ~~~~~~~~~~~~
                //  0                     , 1                     , 2             , 3                     , 4                     , 5             , 6               , 7
                //  fpi ir consumer       , fpi ir electron       , strEtc        , fpi ir consumer       , fpi ir electron       , strEtc        , pos entry mode  , various
                //  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ , ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ , ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// see PIN note.  { FPI_US_IR_CON_CHIP_F_P, FPI_US_IR_ELC_CHIP_F_P, strEtcPrePS2K , FPI_US_IR_CON_CHIP_FAP, FPI_US_IR_ELC_CHIP_FAP, strEtcAirline , "05"            , "03|..Y|YYY" },  // full chip w/PIN
                  { FPI_US_IR_CON_ELEC    , FPI_US_IR_CON_ELEC    , strEtcPrePS2K , FPI_US_IR_CON_AIR     , FPI_US_IR_CON_AIR     , strEtcAirline , "05"            , "03|..Y|YYN" },  // full chip
                  { FPI_US_IR_CON_CHIP_A  , FPI_US_IR_CON_CHIP_A  , strEtcPrePS2K , FPI_US_IR_CON_AIR     , FPI_US_IR_CON_AIR     , strEtcAirline , "90,91"         , "03|..Y|NY." },  // acquirer chip
                  { null                  , null                  , null          , FPI_US_IR_CON_AIR     , FPI_US_IR_CON_AIR     , strEtcAirline ,  null            , "00|...|..." },  // airline
                  { FPI_US_IR_CON_CHIP_I  , FPI_US_IR_CON_CHIP_I  , strEtcPrePS2K , FPI_US_IR_CON_CHIP_I  , FPI_US_IR_CON_CHIP_I  , strEtcPrePS2K , "90,91"         , "03|.Y.|YN." },  // issuer chip
                  { FPI_US_IR_CON_ELEC    , FPI_US_IR_CON_ELEC    , strEtcPrePS2K , FPI_US_IR_CON_ELEC    , FPI_US_IR_CON_ELEC    , strEtcPrePS2K , "05,07,90,91"   , "03|.YY|N.." },  // electronic
                  { FPI_US_IR_CON_ECOMM   , FPI_US_IR_CON_ECOMM   , strEtc        , FPI_US_IR_CON_ECOMM   , FPI_US_IR_CON_ECOMM   , strEtc        , "01"            , "00|6Y.|..." },  // e-commerce non-authenticated
                  { FPI_US_IR_CON_ECOMMS  , FPI_US_IR_CON_ECOMMS  , strEtc        , FPI_US_IR_CON_ECOMMS  , FPI_US_IR_CON_ECOMMS  , strEtc        , "01"            , "00|5Y.|..." },  // e-commerce secure
                };

                for( int i = 0; i < irQualAndResp.length; ++i )
                {
                  if( irQualAndResp[i][offset] == null )continue;   // must have a non-airline or airline FPI (whichever is needed)

                  if( ( irQualAndResp[i][7].charAt(3) != '.' ) && ( irQualAndResp[i][7].charAt(3) != indMotoEcomm ) )continue;
                  if( ( irQualAndResp[i][7].charAt(7) != '.' ) && ( irQualAndResp[i][7].charAt(7) != isChipIssuer ) )continue;
                  if( ( irQualAndResp[i][7].charAt(8) != '.' ) && ( irQualAndResp[i][7].charAt(8) != isChipAcq    ) )continue;
// see PIN note.  if( ( irQualAndResp[i][7].charAt(9) != '.' ) && ( irQualAndResp[i][7].charAt(9) != isChipPIN    ) )continue;

                  if( isPurchase )
                  {
                    if( ( irQualAndResp[i][7].charAt(4) != '.' ) && ( irQualAndResp[i][7].charAt(4) != isAuthed     ) )continue;
                    if( ( irQualAndResp[i][7].charAt(5) != '.' ) && ( irQualAndResp[i][7].charAt(5) != isAuthedY1Y3 ) )continue;
                    if( ( irQualAndResp[i][6] != null ) &&  !testDataIsInList( 2, posEntryMode, irQualAndResp[i][6] ) )continue;

                    int numDays = ( irQualAndResp[i][7].charAt(0) - '0' ) * 10  +
                                  ( irQualAndResp[i][7].charAt(1) - '0' )       ;
                    if( numDays != 0 && !tranMeetsDateTimeliness_TranToClearing( tranInfo, numDays ) )continue;
                  }

                  strIrfInfo += enhRequired + irQualAndResp[i][col_card_type.getIntValue()+offset] + irQualAndResp[i][2+offset];
                  break;    // irQualAndResp contains a hierarchy (at least for LAC); once the first match is found, stop traversing the hierarchy.
                }
              }

              row_program = Program.US_INTER_REGIONAL_MERCHANT;
            }
          }
          else if( myProgram == Program.PROGRAM_CREDIT_VOUCHER )
          {
            String  tran_sic_code               = tranInfo.getString("sic_code");
            String  tran_special_conditions_ind = tranInfo.getPaddedString("special_conditions_ind",2);

            if( isDebit || tranInfo.getInt("debit_type") != 0 )
            {
              strFPI = FPI_US_CV_DEBIT;                                   // any debit card
            }
            else if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.PASSENGER_TRANSPORT ) ) )
            {
              strFPI = FPI_US_CV_PASS_TRANS;                              // any credit card, passenger transport
            }
            else if( (bfCardType & CardTypeVisa.COMMERCIAL_PURCHASING) == CardTypeVisa.COMMERCIAL_PURCHASING )
            {
              String  fpis[][]  = { { FPI_US_CV_PURCH_GSA1  , FPI_US_CV_PURCH_1 } ,
                                    { FPI_US_CV_PURCH_GSA2  , FPI_US_CV_PURCH_2 } ,
                                    { FPI_US_CV_PURCH_GSA3  , FPI_US_CV_PURCH_3 } ,
                                    { FPI_US_CV_PURCH_GSA4  , FPI_US_CV_PURCH_4 } ,
                                    { FPI_US_CV_PURCH_GSA5  , FPI_US_CV_PURCH_5 } };

              double  amts[]    =   {   10000.00  ,
                                        25000.00  ,
                                       100000.00  ,
                                       500000.00  };

              double  tran_transaction_amount   = tranInfo.getDouble("transaction_amount");
              int     col       = ((bfCardType & CardTypeVisa.COMM_GSA) == CardTypeVisa.COMM_GSA) ? 0 : 1;
              int     row       = 0;
              for( ; row < amts.length; ++row )
              {
                if( tran_transaction_amount <= amts[row] )break;
              }
              strFPI = fpis[row][col];                                    // any purchasing credit card, not pt, depending on gsa/non-gsa & amt
            }
            else if( (bfCardType & CardTypeVisa.COMMERCIAL) == CardTypeVisa.COMMERCIAL )
            {
              strFPI = FPI_US_CV_COMMERCIAL;                              // any commercial (except purchasing) credit card, not pt
            }
            else if( "Y".equals(tranInfo.getString("moto_ecommerce_merchant")) &&
                      ( tran_special_conditions_ind.charAt(1) != '8' )         &&                 // can't be quasi-cash
                      !testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.HIGH_RISK_TELEMARKET)))
            {
              strEtcFinal = String.valueOf(RA_CONSUMER_MOTO_ECOMM_RETURN) + strRPS;       // credit voucher for moto merchants

              strFPI = FPI_US_CV_CNSR_CR_MOTO;                            // any consumer credit card, not pt, moto/ecommerce
            } 
            else
            {
              strFPI = FPI_US_CV_CNSR_CR;                                 // any consumer credit card, not pt, not moto/ecommerce
            }
          }
          
          else if( myProgram != Program.STANDARD )
          {
            String  tran_eligibility_flags  = tranInfo.getString("eligibility_flags");
            String  tran_sic_code           = tranInfo.getString("sic_code");
            boolean isSicTravelSvc          = testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.TRAVEL_SERVICE));
            boolean isSicBusiness2B         = testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.BUSINESS_TO_BUSINESS ));
            boolean isQualCharYes           = ( isDebit ) ? convertProgramToVisaProgram(row_program).isCardNotPresentEcomm() :
                    convertProgramToVisaProgram(row_program).isGeneralRetail();
            boolean isSicGovernmentFee          = testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.GOVERNMENT_FEE ));
            
            // check for "utility"
            if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.UTILITY )))
            {
              if( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_UTILITY, tran_eligibility_flags ) )
              {
                if( isQualCharYes )
                {
                  if( StringUtils.isNotEmpty(fpiTable.get(Program.UTILITY.toString(),col_card_type)))
                  {
                    strIrfInfo += enhRequired + fpiTable.get(Program.UTILITY.toString(),col_card_type) + strEtc;
                  }
                }
              }
            }


            if( (bfCardType & CardTypeVisa.CONSUMER) == CardTypeVisa.CONSUMER )
            {
              if( isQualCharYes )
              {
                boolean isSicDevelopingMarket   = testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.DEVELOPING_MARKET ));


                if( isDebit )
                {
                  // check for "debit-only developing market"
                  if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.DEVELOPING_MARKET_DEBIT)))
                  {
                    isSicDevelopingMarket   = true;
                  }
                  // check for "debit tax payment"
                  /*else if( testDataIsInList( 4, tran_sic_code, strSicTaxPayment ) )
                  {
                    if( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_DEBIT_TAX_FEE, tran_eligibility_flags ) )
                    {
                      strIrfInfo += enhRequired + FPI_US_DB_CPS_TAX_PMT1 + strEtc;
                    }
                  }*/
                  // check for "debit debt repayment"
                  else if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.DEBT_REPAYMENT)))
                  {
                    if( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_DEBT_REPAYMENT, tran_eligibility_flags ) )
                    {
                      if( tranInfo.getByte("market_specific_data_ind") == 'B' )
                      {
                        String tran_special_conditions_ind = tranInfo.getPaddedString("special_conditions_ind",2);
                        if( tran_special_conditions_ind.charAt(1) == '9' )
                        {
                          strIrfInfo += enhRequired + FPI_US_DB_CPS_DEBT_REPAY + strEtc;
                        }
                      }
                    }
                  }
                }
                else  // if( isCredit )
                {
                  // check for "charity"
                  if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.CHARITY)))
                  {
                    strIrfInfo += enhRequired + FPI_US_CR_CPS_CHARITY + strEtc;
                  }
                }

                // check for "retail 2"
                if( isSicDevelopingMarket )
                {
                  if(StringUtils.isNotEmpty(fpiTable.get(Program.RETAIL_2.toString(),col_card_type)))
                  {
                    strIrfInfo += enhRequired +  fpiTable.get(Program.RETAIL_2.toString(),col_card_type)+ strEtc;
                  }
                }
              }
              /* this block is added to support new FPI(160) - CPS/Recurring Bill payment program as part Oct 2014 changes
               * This FPI is applicable to
               * Product id = A (Visa traditional)
               * Product id = B (Visa rewards)
               * Product id = C (Visa Signature)
               * Product id = I (Visa Infinate with SQI value 'N')
               * MCC must be 4814 or 4899
               * RA = A and RPS = P
               * ACI = 'J' and MOTO/ECI = 2 and POS Entry mode = 1, market specific data indicator = 'B'
               */
              else{
                  if(!isDebit){
                      if(myProgram == Program.PROGRAM_CPS_RECU_PMT)
                      {
                           if(testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.RECURRING_BILL_PAYMENT)) && strEtcFinal.equals("AP")
                                     &&  tranInfo.getByte("market_specific_data_ind") == 'B')
                           {
                               switch (col_card_type)
                               {
                               case CONSUMER_CREDIT:
                               case CONSUMER_REWARDS:
                                   strFPI =  FPI_CPS_RECUR_BILL_PMT;
                                   break;
                               case CONSUMER_SIGNATURE:

                                   if(tranInfo.getString("card_type_enhanced").startsWith("I") &&
                                           tranInfo.getPaddedString( "spend_qualified_ind" , 1).equals(" "))
                                       strFPI = fpiTable.get(Program.STANDARD.toString(),col_card_type);
                                   else
                                       strFPI =  FPI_CPS_RECUR_BILL_PMT;
                                   break;
                               default:
                                   strFPI = fpiTable.get(Program.STANDARD.toString(),col_card_type);
                                   break;
                               }
                           }
                           else
                             strFPI =  fpiTable.get(Program.STANDARD.toString(),col_card_type);
                     }
                  }
              }

              switch( col_card_type.getIntValue() )
              {
                case RESP_STR_FPI_CONSUMER_PREPAID        :
                case RESP_STR_FPI_CONSUMER_DEBIT          :
                case RESP_STR_FPI_CONSUMER_CREDIT         :
                case RESP_STR_FPI_CONSUMER_REWARDS        :
                  if( !isDebit )
                  {
                    // check for "Threshold" (either retail or supermarket)
                    if( ( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_THRESHOLD_1, tran_eligibility_flags ) ) ||
                        ( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_THRESHOLD_2, tran_eligibility_flags ) ) ||
                        ( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_THRESHOLD_3, tran_eligibility_flags ) ) )
                    {
                            if( row_program == Program.CPS_RETAIL ) { strFPI = FPI_US_CR_CPS_RTL_TR;    }
                      else  if( row_program == Program.CPS_SUPERMARKET ) { strFPI = FPI_US_CR_CPS_SPMKT_TR;  }
                    }
                  }
                  break;

                case RESP_STR_FPI_CONSUMER_SIGNATURE      :
                  if( isSicTravelSvc )
                  {
                    if( row_program == Program.ELECTRONIC )
                    {
                      row_program = Program.STANDARD;     // travel service: downgrade to standard   (from Electronic)
                    }
                    else if( row_program != Program.CPS_SMALL_TICKET )
                    {
                      row_program = Program.TRAVEL_SERVICE;   // upgrade to travel service  (from CPS/anything-but-small-ticket)
                    }
                  }
                  break;

                case RESP_STR_FPI_CONSUMER_SIG_PREFERRED  :
                case RESP_STR_FPI_CONSUMER_HIGH_NET_WORTH :
                        if( row_program == Program.ELECTRONIC) { row_program = Program.STANDARD ;                   } // downgrade to standard
                  else  if( isSicTravelSvc                          ) { row_program = Program.ELECTRONIC ;                 } // downgrade to electronic
                  else  if( isSicBusiness2B                         ) { row_program = Program.BUSINESS_TO_BUSINESS ;             } // upgrade to business-to-business
                  else  if( testDataIsInList( 4, tran_sic_code,sicCodeGroupMap.get(Fpi.SicCodeGroup.SERVICE_STATION)) &&
                            row_program == Program.CPS_SMALL_TICKET ) { row_program = Program.CPS_RETAIL_SERVICE_STATION; } // change to "fuel" row
                  break;
              }
              if (isSicGovernmentFee) {

                if (isDebit || isPrepaid) {

                  switch (myProgram) {

                  case CPS_CARD_NOT_PRESENT:
                  case CPS_E_COMMERCE_BASIC:
                  case CPS_E_COMMERCE_PREF_RETAIL:

                    String strCardTypeEnh = tranInfo.getPaddedString("card_type_enhanced", 3);
                    String strProdId = tranInfo.getPaddedString("product_id", 2);

                    // first decode auth pid =
                    // tranInfo.product_id
                    // if fail, then decode ardef pid =
                    // tranInfo.card_type_enhanced.substring(0,2)
                    for (int idx = 0; idx < 2; ++idx) {
                      String thisProdId = (idx == 0) ? strProdId : strCardTypeEnh.substring(0, 2);
                      if (thisProdId.trim().length() == 0)
                        continue;

                      switch ((thisProdId.charAt(0) * 0x100) + thisProdId.charAt(1)) {
                      /* "F " VISA_CLASSIC */ case (('F' * 0x100) + ' '):
                        strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                        break;
                      /* "J3" VISA_HEALTH_CARE */ case (('J' * 0x100) + '3'):
                        if (isPrepaid)
                          strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                        break;
                      }
                    }
                    break;
                  }
                } else {
                	
                  double  transactionAmt   = tranInfo.getDouble("transaction_amount");

                  switch (myProgram) {

                  case CPS_RETAIL:
                  case CPS_RESTAURANT:
                  case CPS_SUPERMARKET:
                  case CPS_RETAIL_SERVICE_STATION:
                  case CPS_SMALL_TICKET:
                  case CPS_RETAIL_KEY_ENTRY:
                  case CPS_CARD_NOT_PRESENT:
                  case CPS_AUTOMATED_FUEL_DISPENSER:
                  case CPS_ACCOUNT_FUNDING:
                  case CPS_E_COMMERCE_BASIC:
                  case CPS_E_COMMERCE_PREF_RETAIL:
                  case CPS_HOTEL_AUTO:
                  case CPS_HOTEL_AUTO_CNP:
                  case CPS_E_COMMERCE_PREF_HTL_AUTO:
                  case CPS_PASSENGER_TRANSPORT:
                  case CPS_PASSENGER_TRANSPORT_CNP:
                  case CPS_E_COMMERCE_PREF_PASS_TRANS:
                  case PROGRAM_CPS_RECU_PMT:

                    String strCardTypeEnh = tranInfo.getPaddedString("card_type_enhanced", 3);
                    String strProdId = tranInfo.getPaddedString("product_id", 2);

                    // first decode auth pid =
                    // tranInfo.product_id
                    // if fail, then decode ardef pid =
                    // tranInfo.card_type_enhanced.substring(0,2)
                    for (int idx = 0; idx < 2; ++idx) {
                      String thisProdId = (idx == 0) ? strProdId : strCardTypeEnh.substring(0, 2);
                      if (thisProdId.trim().length() == 0)
                        continue;

                      switch ((thisProdId.charAt(0) * 0x100) + thisProdId.charAt(1)) {
                        /* "A " VISA_TRADITIONAL */ 
                        case (('A' * 0x100) + ' '):
                        /* "B " VISA_TRADITIONAL_REWARDS */
                        case (('B' * 0x100) + ' '):
                        /* "C " VISA_SIGNATURE */ 
                        case (('C' * 0x100) + ' '):
                        	if(transactionAmt<=GOVNMT_FEE_CPS_SML_TKT_AMOUNT_LIMIT){
                        		if(myProgram == Program.CPS_SMALL_TICKET){
                        			if(cpsSmallTicketFlag){
                                        strFPI = FPI_US_CR_CPS_SML_TKT;
                                    } else {
                                        strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                                    }
                        		} else {
                        			if(!cpsSmallTicketFlag){
                                        strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                        			}
                        			else {
                        				break;
                        			}
                        		}
                        	} else {
                                strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                        	}
                        break;
                        /* "D " VISA_SIGNATURE_PREFERRED */ 
                        case (('D' * 0x100) + ' '):
                              strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                        break;
                        /* "I " VISA_INFINITE */ 
                        case (('I' * 0x100) + ' '):
                        	 if (!"US1".equals(tranInfo.getPaddedString("region_issuer", 3))
                               || "Q".equals(tranInfo.getPaddedString("spend_qualified_ind", 1))){
                        		 
                        		 if("N".equals(tranInfo.getPaddedString("spend_qualified_ind", 1)) &&
                        				 transactionAmt<=GOVNMT_FEE_CPS_SML_TKT_AMOUNT_LIMIT){
                             		if(myProgram == Program.CPS_SMALL_TICKET){
                             			if(cpsSmallTicketFlag){
                                             strFPI = FPI_US_CR_CPS_SML_TKT;
                                         } else {
                                             strFPI = fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                                         }
                             		} else {
                             			if(!cpsSmallTicketFlag){
                                             strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                             			} else {
                             				break;
                             			}
                             		}
                             	} else {
                                     strFPI =  fpiTable.get(Program.US_GOVERNMENT_FEE.toString(),col_card_type);
                             	}
                        	 }  
                        break;
                      }
                    }
                    break;
                  }
                }
              }
            }
            else  // if( (bfCardType & CardTypeVisa.COMMERCIAL) == CardTypeVisa.COMMERCIAL )
            {
              boolean isSicFuel               = testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.FUEL));
                  boolean   isCommGSA       = ( (bfCardType & CardTypeVisa.COMM_GSA  ) == CardTypeVisa.COMM_GSA   );

                  final int LEVEL2_CUST     = (1<<0);
                  final int LEVEL2_TAX      = (1<<1);
                  final int LEVEL2          = (1<<2);
                  final int LEVEL3          = (1<<3);
                  final int LEVEL3_ENH      = (1<<4);
                  final int LEVEL_LRG_TKT   = (LEVEL2_CUST | LEVEL2_TAX | LEVEL3);
                  final int LEVEL_LRG_TKT_E = (LEVEL2_CUST | LEVEL2_TAX | LEVEL3_ENH);
                        int dataLevel       = 0;


                  // if( (!account funding/travel service) and (!travel service sic) ) then (maybe dataLevel != 0)
                  if( convertProgramToVisaProgram(row_program).isLevelXDataOk() && !isSicTravelSvc )
                  {
                    double  tran_tax_amount           = tranInfo.getDouble("enh_tax_amount");
                    double  tran_transaction_amount   = tranInfo.getDouble("transaction_amount");
                    if( checkEnhancements )   // if currently checking enhancements
                    {
                      checkEnhancements   = false;                              // don't check again
                      enhRequired         = tranInfo.setEnhIndTax(enhRequired); // indicate that fpi's we get now need enh tax
                    }
                    else                      // if not currently checking enhancements
                    {
                      if( !isCommGSA && tran_tax_amount != 0.00 )               // if not GSA card and enh tax amt is interesting
                        checkEnhancements = true;                                 // then set up to check again with the enh tax amt
                      tran_tax_amount = tranInfo.getDouble("tax_amount");       // use regular tax amt this time
                    }

                    // all commercial cards: check for level 2 tax data
                    if( tran_tax_amount > 0 )
                    {
                      // fuel merchants:      tax amount > zero
                      // non-fuel merchants:  tax amount between 0.1% and 22% of tran amt
                      //  ("tax must be within 0.1% to 22% of the source amount; generally the tax is included in the source amount")
                      if( isSicFuel )
                      {
                        if( col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_PURCHASING ) { dataLevel |= LEVEL2_TAX;            }
                        else                                                      { dataLevel |= LEVEL2_TAX | LEVEL2;   }
                      }
                      else
                      {
                        if ( tran_tax_amount >= MesMath.round((0.001 * tran_transaction_amount),2) &&
                             tran_tax_amount <= MesMath.round((0.22  * tran_transaction_amount),2) ) { dataLevel |= LEVEL2_TAX | LEVEL2;   }
                      }
                    }

                    // only purchasing & corporate cards:  check for level 3 data
                    if( col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_PURCHASING ||
                        col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_CORPORATE  )
                    {
                      if( tranInfo.getByte("level_iii_data_present") == 'Y' )     { dataLevel |= LEVEL3;                }
                      else
                      if( tranInfo.getByte("enh_level_iii_data_present") == 'Y' ) { dataLevel |= LEVEL3_ENH;            }
                    }

                    // only purchasing cards: check for level 2 customer code data, more options on level 2 tax data; check for special programs
                    if( col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_PURCHASING || col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_CORPORATE)
                    {
                      if( tranInfo.getByte("tax_indicator") == '2' )              { dataLevel |= LEVEL2_TAX;            }
                      if( !tranInfo.isFieldBlank("customer_code") )
                      {
                        if( isSicFuel )                                           { dataLevel |= LEVEL2_CUST | LEVEL2;  }
                        else                                                      { dataLevel |= LEVEL2_CUST;           }
                      }

                      // check for "large ticket" (either GSA or non-GSA)
                      if( ( (dataLevel & LEVEL_LRG_TKT  ) == LEVEL_LRG_TKT   ) ||
                          ( (dataLevel & LEVEL_LRG_TKT_E) == LEVEL_LRG_TKT_E ) )
                      {
                        if( tran_transaction_amount <= 10000000 )
                        {
                          String enhRequiredTemp = ( (dataLevel & LEVEL_LRG_TKT) == LEVEL_LRG_TKT ) ? enhRequired : tranInfo.setEnhIndL3(enhRequired);
                          if( isCommGSA )
                          {
                            if(convertProgramToVisaProgram(row_program).isLargeTicketGsa())
                            {
                              if( (bfCardType & CardTypeVisa.COMM_LRGTKT) == CardTypeVisa.COMM_LRGTKT )
                              {
                                strIrfInfo += enhRequiredTemp + FPI_US_PURCH_LRG_TKT_GSA_0 + String.valueOf(RA_GSA_LARGE_TKT_NON_T_E) + strRPS + ",";
                              }
                            }
                          }
                          else if( isPrepaid )
                          {
                            if(convertProgramToVisaProgram(row_program).isCardNotPresentEcomm() )
                            {
                              // tran must also meet 8 day clearing timeliness, but all these CPS programs have less < 8 day clearing requirement
                              strIrfInfo += enhRequiredTemp + FPI_US_PURCH_LRG_TKT_PP + String.valueOf(RA_CPS_EMERGING_MARKET) + strRPS + ",";
                            }
                          }
                          else
                          {
                            if(convertProgramToVisaProgram(row_program).isLargeTicket() )
                            {
                              // tran must also meet 8 day clearing timeliness, but all these CPS programs have less < 8 day clearing requirement
                              strIrfInfo += enhRequiredTemp + FPI_US_PURCH_LRG_TKT + String.valueOf(RA_CPS_EMERGING_MARKET) + strRPS + ",";
                            }
                          }
                        }
                      }

                      // check for "Visa Large Purchase Advantage"
                      if( (bfCardType & CardTypeVisa.COMMERCIAL_PURCH_VLPA) == CardTypeVisa.COMMERCIAL_PURCH_VLPA )
                      {
                        if( convertProgramToVisaProgram(row_program).isCardNotPresentEcomm())
                        {
                                if( tran_transaction_amount > 500000 ) strIrfInfo += enhRequired + FPI_US_PURCH_VLPA4 + strEtc;
                          else  if( tran_transaction_amount > 100000 ) strIrfInfo += enhRequired + FPI_US_PURCH_VLPA3 + strEtc;
                          else  if( tran_transaction_amount >  25000 ) strIrfInfo += enhRequired + FPI_US_PURCH_VLPA2 + strEtc;
                          else  if( tran_transaction_amount >  10000 ) strIrfInfo += enhRequired + FPI_US_PURCH_VLPA1 + strEtc;
                        }
                      }

                      // check for "government-to-government"
                      if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.GOVERNMENT_TO_GOVERNMENT)))
                      {
                        if( testDataIsInList_NullListReturnsFalse( 3, ELIG_FLAG_GOVT_2_GOVT, tran_eligibility_flags ) )
                        {
                          if( isCommGSA )
                          {
                            if( row_program != Program.ELECTRONIC )
                            {
                              strIrfInfo += enhRequired + FPI_US_PURCH_GSA_G2G + strEtc;
                            }
                          }
                        }
                      }
                      // check for "fleet"
                      else if( isSicFuel )
                      {
                        if( (bfCardType & CardTypeVisa.COMM_FLEET  ) == CardTypeVisa.COMM_FLEET )
                        {
                          byte tran_type_of_purchase = tranInfo.getByte("type_of_purchase");
                          switch( tran_type_of_purchase )
                          {
                            case '1': // fuel only
                              dataLevel |= LEVEL3;      // if fuel only, pretend we have level 3 data
                            case '3': // fuel and non-fuel
                              if( ( !testDataIsInList( 1, tranInfo.getPaddedString("fuel_units",1), "L,G,I,K,P" ) ) ||
                                  ( tranInfo.getDouble("fuel_quantity")    <= 0.00      ) ||
                                  ( tranInfo.getDouble("fuel_gross_price") <= 0.00      ) ||
                                  ( tranInfo.getDataLength("fuel_type") != 2            ) )
                              {
                                // if don't meet "additional" fuel reqs, don't meet Level 2 or Level 3 reqs
                                dataLevel &= ~(LEVEL2 | LEVEL3);
                              }
                              else if( (dataLevel & LEVEL2) != LEVEL2 )
                              {
                                // if don't meet Level 2 reqs, don't meet Level 3 reqs
                                dataLevel &= ~(LEVEL3);
                              }

                              if( convertProgramToVisaProgram(row_program).isFleet())
                              {
                                // make sure get "retail" not "cnp"
                                row_program = Program.CPS_RETAIL;
                              }
                              else
                              {
                                // if not one of the right CPS programs, can still get cps if have level 2 data
                                if( (dataLevel & LEVEL2) == LEVEL2 )
                                  row_program =  Program.CPS_RETAIL;
                                else
                                  row_program = Program.ELECTRONIC ;
                              }
                              break;
                          }
                        }
                      }
                    }
                  }

                  // change column for commercial prepaid (but not too soon, not before utility)
                  if( isPrepaid )
                  {
                    col_card_type = Fpi.CardProduct.COMMERCIAL_PREPAID;
                  }

                  if( (dataLevel & LEVEL3_ENH) == LEVEL3_ENH )
                  {
                    // as long as level 3 is the first "if" for upgrades/downgrades below,
                    // it is simple to check what to do for level 3 enhanced
                    Program temp_program = Program.NO_PROGRAM;
                          if( row_program != Program.ELECTRONIC  ) temp_program = Program.LEVEL_3_DATA;  // upgrade to level 3
                    else  if( !isCommGSA )                        temp_program = Program.ELECTRONIC_W_DATA ;   // upgrade to electronic w/data (if !GSA)
                    if( temp_program.getIntValue() > Program.NO_PROGRAM.getIntValue())
                    {
                      if( StringUtils.isNotEmpty(fpiTable.get(temp_program.toString(),col_card_type)))
                      {
                        strIrfInfo += tranInfo.setEnhIndL3(enhRequired) + fpiTable.get(temp_program.toString(),col_card_type) + strEtc;
                      }
                    }
                  }
                  Program new_program = Program.NO_PROGRAM;
                  if( row_program == Program.ELECTRONIC /*PROGRAM_ELECTRONIC*/ )
                  {
                          if( !isCommGSA &&
                              (dataLevel & LEVEL3) == LEVEL3 ){ new_program = Program.ELECTRONIC_W_DATA ;    } // upgrade to electronic w/data (if !GSA)
                    else  if( (dataLevel & LEVEL2) == LEVEL2 ){                                       } // leave at electronic
                    else                                      { new_program = Program.STANDARD;       } // downgrade to standard
                  }
                  else // if( row_program == PROGRAM_CPS_... )
                  {
                          if( (dataLevel & LEVEL3) == LEVEL3 ){ new_program = Program.LEVEL_3_DATA ;   } // upgrade to level 3
                    else  if( (dataLevel & LEVEL2) == LEVEL2 ){ new_program = Program.LEVEL_2_DATA;   } // upgrade to level 2
                    else  if( isSicBusiness2B                ){ new_program = Program.BUSINESS_TO_BUSINESS ; } // upgrade to business-to-business
                    else  if( isSicTravelSvc                 ){ new_program = Program.TRAVEL_SERVICE ; } // upgrade to travel service (or downgrade to electronic)
                  //else  if( something                      ){ strFPI      = FPI_US_something;       } // is OK to set strFPI here
                  }
                  if(!Program.NO_PROGRAM.equals(new_program))
                  {
                    if( StringUtils.isNotEmpty(fpiTable.get(new_program.toString(),col_card_type)))
                    {
                      row_program = new_program;
                    }
                    if(new_program == Program.BUSINESS_TO_BUSINESS && col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_CORPORATE ){
                        if(!isSicTravelSvc){
                            if(tranInfo.getByte("card_present")=='Y')
                                strFPI = FPI_US_CORP_CP;
                            else
                                strFPI = FPI_US_CORP_CNP;
                        }else{
                            row_program = Program.TRAVEL_SERVICE;
                        }
                        
                    }
                    if(new_program == Program.BUSINESS_TO_BUSINESS && col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_PURCHASING) {
                        if(!isSicTravelSvc){
                            if(tranInfo.getByte("card_present")=='Y')
                                strFPI = FPI_US_PURCH_CP;
                            else
                                strFPI = FPI_US_PURCH_CNP;
                        }else {
                            row_program = Program.TRAVEL_SERVICE;
                        }
                        
                    }

                    //Assign new FPI's 392,393,394,395,396 based on transaction amount for Straight through processing transactions
                    if( col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_PURCHASING || col_card_type.getIntValue() == RESP_STR_FPI_COMMERCIAL_CORPORATE){
                        if(tranInfo.getByte("market_specific_data_ind") == 'J' && strRA == "A" && isSTP && checkProd){
                            switch(new_program){
                                    case CPS_CARD_NOT_PRESENT:
                                    case CPS_E_COMMERCE_BASIC:
                                    case CPS_E_COMMERCE_PREF_RETAIL:
                                    case CPS_HOTEL_AUTO_CNP:
                                    case CPS_E_COMMERCE_PREF_HTL_AUTO:
                                    case CPS_PASSENGER_TRANSPORT_CNP:
                                    case CPS_E_COMMERCE_PREF_PASS_TRANS:
                                          if(tranInfo.getDouble("transaction_amount") <  7000   )     strFPI = FPI_US_STP_TR1;
                                     else if(tranInfo.getDouble("transaction_amount") <  15000  )     strFPI = FPI_US_STP_TR2;
                                     else if(tranInfo.getDouble("transaction_amount") <  50000  )     strFPI = FPI_US_STP_TR3;
                                     else if(tranInfo.getDouble("transaction_amount") <  100000 )     strFPI = FPI_US_STP_TR4;
                                     else if(tranInfo.getDouble("transaction_amount") >= 100000 )     strFPI = FPI_US_STP_TR5;
                            }
                        }
                    }
                  }
            //    break;
            //}
            }
          }
          
        //As per CAU 2017, New fee program has been added for all B2B Virtual Payments - For all Purchase & Credit Voucher Transactions
          boolean isPurchase    = "D".equals( tranInfo.getString("debit_credit_indicator") );
          String  tran_sicCode = tranInfo.getString("sic_code");
    	  if(prodId.contains(CONST_GLB_VTL_PYMT)
                  && testDataIsInList( 4, tran_sicCode, sicCodeGroupMap.get(Fpi.SicCodeGroup.GLOBAL_B2B_VIRTUAL_PAYMENT))){
              if(isPurchase || myProgram == Program.PROGRAM_CREDIT_VOUCHER){
              	  strFPI = FPI_US_GLB_B2B_VTL_PYMT;							// Credit Voucher -- B2B Virtual Payment
              } 
          }
          
          if( strFPI == null ) 
          {
            strFPI = fpiTable.get(row_program.toString(),col_card_type);

          }
          
          /*
           * As per the document Visa 2.9 Changes to Visa Gold International IRF for Japan Issuers
           * Version 2.0 12-15-16.pdf assigning premium FPI for Japan issued Visa Gold cards
           */
          String card_region_issuer = tranInfo.getPaddedString("region_issuer",3);
          
          if (!isPrepaid  && card_region_issuer.substring(0,2).equalsIgnoreCase(JAPAN) 
        		  && (prodId.equals("P ") ) )
          { 
            strFPI = updateJapanFPI(strFPI);
            strIrfInfo = "";
          } 
          
          strIrfInfo += enhRequired + strFPI + strEtcFinal;
          
          if( checkEnhancements )
              strIrfInfo += ",";    // if not SURE going to add more, would need to delete trailing comma when done.
        }while( checkEnhancements );

        return( strIrfInfo );
      }
      catch( Exception e )
      {
        System.out.println(e.toString());
        SyncLog.LogEntry("com.mes.settlement.InterchangeVisa$IC_Visa::getIrfInfo()",e.toString());
        return( null );
      }
    }

    private  VisaProgram convertProgramToVisaProgram(Program program) {
      if (cpsProgramMap.get(program.getValue()) != null) {
        return cpsProgramMap.get(program.getValue());
      }
      else if(nonCpsProgramMap.get(program.getValue()) != null){
        return nonCpsProgramMap.get(program.getValue());
      }
      else return new VisaProgram();
    }


    /**
     * This method is to get the check the FPI issued over the Japan cards and updated them
     * based on the document Visa 2.9 Changes to Visa Gold International IRF for Japan Issuers
     * Version 2.0 12-15-16.pdf assigning premium FPI for Japan issued Visa Gold cards
     *
     * @param String - estimated FPI for Japan cards
     *
     * @return String - enhanced FPI for Japan cards
     */
    private String updateJapanFPI (String fpi) 
    {
      String strFPI = fpi;
      if ( strFPI.equals(FPI_US_IR_CON_STD) 
    		  || strFPI.equals(FPI_US_IR_CON_ELEC) || strFPI.equals(FPI_US_IR_CON_ECOMM) 
    		  || strFPI.equals(FPI_US_IR_CON_ECOMMS) || strFPI.equals(FPI_US_IR_CON_CHIP_I) 
    		  || strFPI.equals(FPI_US_IR_CON_CHIP_A) ) 
      {
        strFPI = FPI_US_IR_PREMIUM;
      }
      return strFPI;
    }

    public String   tranQualifies( SettlementRecord tranInfo, int bfCardType,VisaProgram program )
    {
      String      retVal      = null;
      String      enhRequired = SettlementRecord.DEF_ENHANCED_IND;
      String      progress    = "entry";//@

      try
      {
        DisqualList.removeAllElements();    // clear the disqual list

        // must be Visa card: getBitField should ensure that it is


        if( myProgram == Program.PROGRAM_CASH_DISBURSEMENT )
        {
          if( !testDataIsInList( 4, tranInfo.getString("sic_code"), sicCodeGroupMap.get(Fpi.SicCodeGroup.CASH_DISBURSEMENT)))
          {
            DisqualList.add("Invalid sic code for cash disbursement");
          }
          else if( !"Y".equals( tranInfo.getString("cash_disbursement") ) )
          {
            DisqualList.add("Must be cash disbursement");
          }
        }
        else if( "Y".equals( tranInfo.getString("cash_disbursement") ) )
        {
          DisqualList.add("Must not be cash disbursement");
        }
        else if( myProgram == Program.PROGRAM_NOT_US_DOMESTIC )
        {
          if( tranInfo.getString("region_merchant").startsWith("US") &&
              tranInfo.getString("region_issuer"  ).startsWith("US") )
          {
            DisqualList.add("Must not be a US Domestic transaction");
          }
        }
        else if( !tranInfo.getString("region_merchant").startsWith("US") ||
                 !tranInfo.getString("region_issuer"  ).startsWith("US") )
        {
          DisqualList.add("Must be a US Domestic transaction");
        }
        else if( myProgram == Program.PROGRAM_CREDIT_VOUCHER )
        {
          if( !"C".equals( tranInfo.getString("debit_credit_indicator") ) )
          {
            DisqualList.add("Must be credit/return (not debit/purchase)");
          }
        }
        else if( !"D".equals( tranInfo.getString("debit_credit_indicator") ) )
        {
          DisqualList.add("Must be debit/purchase (not credit/return)");
        }

        else if( myProgram.getIntValue() >= programListCount )
        {
          // program identifier must have rows in the tables
          DisqualList.add("Invalid program identifier: " + String.valueOf(myProgram) );
        }
        else if( (myProgram == Program.PROGRAM_CPS_RECU_PMT)
                && ( testDataIsInList( 4, tranInfo.getString("sic_code"), sicCodeGroupMap.get(Fpi.SicCodeGroup.RECURRING_BILL_PAYMENT)))){
            if(tranInfo.getByte("market_specific_data_ind") != 'B'){
                DisqualList.add("Market Specific data should be B");
            }
        }
        // all Visa purchases at US merchants with US issued cards qualify for Visa US Standard
        else if( myProgram != Program.STANDARD )
        {
          // the following tests only apply to Electronic & CPS programs

          boolean isConsumerDebit                   = ( (bfCardType & CardTypeVisa.CONSUMER_DEBIT) == CardTypeVisa.CONSUMER_DEBIT );

          char    tran_market_specific_data_ind     = tranInfo.getChar        ( "market_specific_data_ind"        );
          String  tran_moto_ecommerce_ind           = tranInfo.getPaddedString( "moto_ecommerce_ind",1            );
          String  tran_sic_code                     = tranInfo.getString      ( "sic_code"                        );
          String  tran_special_conditions_ind       = tranInfo.getPaddedString( "special_conditions_ind",2        );

          if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.HIGH_RISK_TELEMARKET)))
          {
            DisqualList.add("Must not be a high-risk telemarketing merchant");
          }
          else if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.PASSENGER_TRANSPORT)) &&
                   (tranInfo.isFieldBlank("enhanced_data_1") || tranInfo.isFieldBlank("enhanced_data_2")) )
          {
                  // enhanced_data_1 & _2 = "ticket number" & "itinerary data"
            DisqualList.add("Passenger transport merchant must have market specific data");
          }
          else if( tran_special_conditions_ind.charAt(1) == '9' && !isConsumerDebit )
          {
            DisqualList.add("Debt repayment only valid for consumer debit cards");
          }
          else if( !testDataIsInList( 2, tranInfo.getPaddedString("pos_entry_mode",2), program.getPosEntryMode()))
          {
            DisqualList.add("Invalid POS Entry Mode");
          }
          else if( !testDataIsInList( 1, tranInfo.getPaddedString("cardholder_id_method",1), program.getCid()))
          {
            DisqualList.add("Invalid Cardholder ID Method");
          }
          else if( !testDataIsInList( 1, tran_moto_ecommerce_ind, program.getEcpi() ) )
          {
            DisqualList.add("Invalid MOTO/Ecommerce Indicator");
          }
          else if( tran_market_specific_data_ind == 'B' && " ".equals( tran_moto_ecommerce_ind ) )
          {
            DisqualList.add("Invalid MOTO/ECPI for Bill Payment Transaction");
          }
          else if( tranInfo.isFieldBlank("auth_code") )
          {
            DisqualList.add("Missing auth code");
          }
          else if( myProgram == Program.ELECTRONIC )
          {
            // the following tests only apply to Electronic program

            if( (bfCardType & CardTypeVisa.COMMERCIAL) == CardTypeVisa.COMMERCIAL )
            {
              // no more tests for commercial card transactions
            }
            else if( tranInfo.getString("auth_code").endsWith("0000Y") )
            {
              DisqualList.add("Invalid authorization code");
            }
            else if( !testDataIsInList( 1, tranInfo.getString("auth_source_code").equals("-")?"Z":tranInfo.getString("auth_source_code"), "Z,B,1-8,D-G" ) )
            {
              DisqualList.add("Invalid authorization source code");
            }
            else if( !tranMeetsDateTimeliness_TranToClearing( tranInfo, program.getClearingCutoffTime() ) )
            {
              DisqualList.add("Failed to meet clearing timeliness requirements");
            }
          }
          // the following tests only apply to CPS programs
          else if( !tranMeetsDateTimeliness_AuthToTran( tranInfo, program.getAuthCutOffTime() ) )
          {
            DisqualList.add("Failed to meet auth date to tran date timeliness requirements");
          }
          else if( !tranMeetsDateTimeliness_TranToClearing( tranInfo, program.getClearingCutoffTime() ) )
          {
            DisqualList.add("Failed to meet clearing timeliness requirements");
          }
          else if( !testDataIsInList( 1, tranInfo.getString("auth_source_code").equals("-")?"Z":tranInfo.getString("auth_source_code"), "Z,B,1-5,F,G" ) )
          {
            DisqualList.add("Invalid authorization source code");
          }
          else if( tranInfo.isFieldBlank("auth_tran_id") )
          {
            DisqualList.add("Missing auth transaction id");
          }
          else if( tranInfo.isFieldBlank("auth_val_code") )
          {
            DisqualList.add("Missing auth validation code");
          }
          else
          {
            String  tran_auth_returned_aci = tranInfo.getString("auth_returned_aci");

            if( !testDataIsInList( 1, tran_auth_returned_aci, program.getAci() ) )
            {
              DisqualList.add("Invalid ACI for this program");
            }
            else if( isConsumerDebit && !testDataIsInList_NullListReturnsTrue( 1, tran_auth_returned_aci, program.getAciDB() ) )
            {
              DisqualList.add("Invalid ACI for debit card");
            }
            // check ACI matches other data
            else
            {
              switch( tran_auth_returned_aci.charAt(0) )
              {
              // note: don't need to check for 'N' or 'T' because all CPS programs check for allowed aci (never null in aQualStrs[])
              //case 'N':   // not a cps transaction
              //case 'T':   // not eligible to participate in cps
              //  DisqualList.add("Invalid ACI for any CPS program");
              //  break;

                case 'R':
                  if( tran_market_specific_data_ind == 'B' )
                  {
                    DisqualList.add("Invalid ACI for Bill Payment Transaction");
                  }
                  else if( !testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.HEALTH_DEV_MKT_UTILITY )))
                  {
                    DisqualList.add("Invalid ACI for non-Health/DevelopingMrkt/Utility SIC");
                  }
                  break;

                case 'S':
                  if( !"6".equals( tran_moto_ecommerce_ind ) )
                  {
                    DisqualList.add("Moto/Ecommerce Indicator must be 6 when ACI is S");
                  }
                  break;

                case 'U':
                  if( !"5".equals( tran_moto_ecommerce_ind ) )
                  {
                    DisqualList.add("Moto/Ecommerce Indicator must be 5 when ACI is U");
                  }
                  break;
              }
            }

            // check market specific data
            if( DisqualList.size() == 0 )
            {
              boolean   tranPurchaseIdIsInvalid   = tranInfo.isFieldBlank("purchase_id");
              byte      tran_purchase_id_format   = tranInfo.getByte("purchase_id_format");
              String    goodExtraChargesValues    = null;
              String    goodMarketSpecificData    = "";

              switch( myProgram )
              {
                case CPS_RETAIL                   :
                case CPS_RETAIL_KEY_ENTRY         :
                  if( !testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.HOTEL_AUTO_RENTAL)) &&
                      !testDataIsInList( 1, String.valueOf(tran_market_specific_data_ind), "A,H" ) )
                  {
                    tranPurchaseIdIsInvalid = false;
                    break;
                  }
                  goodMarketSpecificData = "N,B,";    // retail programs allow more msd options
                // fall through...
                case CPS_HOTEL_AUTO               :
                case CPS_HOTEL_AUTO_CNP           :
                case CPS_E_COMMERCE_PREF_HTL_AUTO   :
                  if( tranInfo.isFieldBlank("statement_date_begin") )
                  {
                    DisqualList.add("Invalid statement start date");
                  }
                  else
                  {
                    switch( tran_purchase_id_format )
                    {
                      case '3': // car rental
                        goodMarketSpecificData += "A";
                        goodExtraChargesValues  = " ,0-5";
                        break;

                      case '4': // hotel
                        goodMarketSpecificData += "H";
                        goodExtraChargesValues  = " ,0,2-7";
                        break;
                    }

                    if( goodExtraChargesValues == null )
                    {
                      DisqualList.add("Purchase identifier format must be '3' or '4'");
                    }
                    else if( !testDataIsInList( 1, String.valueOf(tran_market_specific_data_ind), goodMarketSpecificData ) )
                    {
                      DisqualList.add("Invalid market specific data indicator");
                    }
                    else
                    {
                      String tran_extra_charges_values = tranInfo.getString("extra_charges");
                      for( int i = 0; tran_extra_charges_values != null && i < tran_extra_charges_values.length(); ++i )
                      {
                        if( !testDataIsInList( 1, tran_extra_charges_values.substring(i,i+1), goodExtraChargesValues ) )
                        {
                          DisqualList.add("Invalid extra charge value");
                        }
                      }
                    }
                  }
                  break;

                case CPS_CARD_NOT_PRESENT        :
                case CPS_E_COMMERCE_BASIC        :
                case CPS_E_COMMERCE_PREF_RETAIL  :
                  if( tran_purchase_id_format != '1' )
                  {
                    if( tranInfo.isFieldBlank("enh_purchase_id") )
                    {
                      DisqualList.add("Purchase identifier format must be '1'");
                    }
                    else
                    {
                      tranPurchaseIdIsInvalid   = false;
                      enhRequired               = tranInfo.setEnhIndPID(enhRequired);   // indicate that fpi's we get now need enh pid
                    }
                  }
                  break;

                default:
                  tranPurchaseIdIsInvalid = false;
                  break;
              }
              if( tranPurchaseIdIsInvalid )
              {
                // purchase identifiers: 1=order # / 3=rental agreement # / 4=folio #
                DisqualList.add("Purchase identifier required");
              }
            } // if( DisqualList.size() == 0 )

            // check amount tolerance requirements met
            if( DisqualList.size() == 0 )
            {
              double tran_transaction_amount    = tranInfo.getDouble("transaction_amount");
              double tran_auth_amount           = tranInfo.getDouble("auth_amount");
              double tran_auth_amount_total     = tranInfo.getDouble("auth_amount_total");
              switch( myProgram )
              {
                case CPS_SUPERMARKET              :
                case CPS_RETAIL                   :
                  if( !isConsumerDebit )                                          break;  // don't chk amt if not consumer debit
                  if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.RETAIL_TIPPERS))) break;  // don't chk amt if retail merchant adds tips
                  if( testDataIsInList( 4, tran_sic_code, sicCodeGroupMap.get(Fpi.SicCodeGroup.TRAVEL_SERVICE)))         // no cps_retail for travel svc consumer debit
                  {
                    DisqualList.add("Invalid sic code for retail debit");
                    break;
                  }
                // fall through...
                case CPS_ACCOUNT_FUNDING               :
                case CPS_PASSENGER_TRANSPORT           :
                case CPS_PASSENGER_TRANSPORT_CNP       :
                case CPS_E_COMMERCE_PREF_PASS_TRANS    :
                  if( tran_transaction_amount != tran_auth_amount_total )
                  {
                    DisqualList.add("Tran amount must match total auth amount");
                  }
                  break;

                case CPS_SMALL_TICKET             :
                  if( tran_transaction_amount > 15.00 )
                  {
                    DisqualList.add("Invalid Small Ticket tran amount");
                  }
                  break;

                case CPS_AUTOMATED_FUEL_DISPENSER :
                  if( "10".equals( tranInfo.getString("auth_response_code") ) )
                  {
                    if( tran_transaction_amount > tran_auth_amount )
                    {
                      DisqualList.add("Tran amount cannot be more than auth amount");
                    }
                  }
                  else
                  {
                    if( tran_auth_amount != 1.00 )
                    {
                      DisqualList.add("Tran auth amount must be $1");
                    }
                  }
                  if( tran_transaction_amount > 125.00 )
                  {
                    DisqualList.add("Tran amount must less than $125");
                  }
                  break;

                case CPS_E_COMMERCE_BASIC              :
                case CPS_E_COMMERCE_PREF_RETAIL        :
                  if( tran_market_specific_data_ind == 'E' )  // transaction aggregation
                  {
                    if( !tranMeetsDateTimeliness_AuthToTran( tranInfo, 3 ) )
                    {
                      DisqualList.add("Failed to meet auth date to tran date timeliness requirements");
                    }
                    else if( tran_auth_amount_total  > 15.00 )
                    {
                      DisqualList.add("Total authorized amount must be less than $15");
                    }
                    else if( tran_transaction_amount > 15.00 )
                    {
                      DisqualList.add("Tran amount must be less than $15");
                    }
                    else if( tran_transaction_amount > tran_auth_amount_total )
                    {
                      DisqualList.add("Tran amount cannot exceed total auth amount");
                    }
                  }
                  break;

                default:
                  break;
              }

              switch( myProgram )
              {
                case CPS_RETAIL                       :
                case CPS_RESTAURANT                   :
                case CPS_SUPERMARKET                  :
                case CPS_RETAIL_SERVICE_STATION       :
                case CPS_SMALL_TICKET                 :
                case CPS_RETAIL_KEY_ENTRY             :
                case CPS_AUTOMATED_FUEL_DISPENSER     :
                case CPS_ACCOUNT_FUNDING              :
                case CPS_PASSENGER_TRANSPORT          :
                case CPS_PASSENGER_TRANSPORT_CNP      :
                case CPS_E_COMMERCE_PREF_PASS_TRANS   :
                  if( tran_auth_amount != tran_auth_amount_total )
                  {
                    DisqualList.add("Tran only allowed one auth, no reversals");
                  }
                  break;

                default:
                  break;
              }
            } // if( DisqualList.size() == 0 )
          }
        } // if( myProgram != PROGRAM_STANDARD )

        if( DisqualList.size() == 0 ) {
          retVal = enhRequired;
        }else{
          log.debug("Program: " + program.getName() + " Disqualifications: "+ DisqualList.toString());
        }
      }
      catch( Exception e )
      {
        String recInfo = ((tranInfo == null) ? "null" : (tranInfo.getString("batch_id") + "," + tranInfo.getString("batch_record_id")));
        String errMsg  = e.toString() + "\r\n" +
                          "myProgram: " + myProgram +
                          ", bfCardType: " + bfCardType +
                          ", progress: " + progress;
        SyncLog.LogEntry("com.mes.settlement.InterchangeVisa$IC_Visa::tranQualifies(" + recInfo + ")",errMsg);
      }
      return( retVal );
    }

    public boolean  tranMeetsDateTimeliness_AuthToTran( SettlementRecord tranInfo, int numDays )
      throws Exception
    {
      boolean     retVal    = false;

      if( tranInfo.getDate("auth_date"       ) != null &&
          tranInfo.getDate("transaction_date") != null )
      {
        Calendar calOne = Calendar.getInstance();
        Calendar calTwo = Calendar.getInstance();

        calOne.clear();
        calTwo.clear();

        calOne.setTime( tranInfo.getDate("auth_date"       ) );
        calTwo.setTime( tranInfo.getDate("transaction_date") );

        // note: for numDays = 7, actually supposed to be "tran date within 7 days after or 1 day before auth date"
        calOne.add(Calendar.DAY_OF_MONTH, numDays);

        retVal = !calTwo.after(calOne);
      }
      return( retVal );
    }

    public boolean  tranMeetsDateTimeliness_TranToClearing( SettlementRecord tranInfo, int numDays )
      throws Exception
    {
      boolean     retVal    = false;

      if( tranInfo.getDate("transaction_date") != null )
      {
        int     nsfType = (myProgram == Program.PROGRAM_NOT_US_DOMESTIC) ? 1 : 0; // SettlementDb.NSF_SUNDAYS_ONLY : SettlementDb.NSF_NONE;

        Calendar calOne = Calendar.getInstance();
        Calendar calTwo = Calendar.getInstance();

        calOne.clear();
        calTwo.clear();

        calOne.setTime( tranInfo.getDate("transaction_date") );
        calTwo.setTime( CentralProcessingDate );

        calOne.add(Calendar.DAY_OF_MONTH, 1);   // "+1" so don't check whether tran date is a nonSettlementDay
        calTwo.add(Calendar.DAY_OF_MONTH,-1);   // "-1" so don't check whether CPD       is a nonSettlementDay
        calOne.add(Calendar.DAY_OF_MONTH, numDays-1+SettlementDb.getNonSettlementDayCount(tranInfo.RecSettleRecType,calOne,calTwo,nsfType));

        retVal = !calTwo.after(calOne);
      }
      return( retVal );
    }
  }
  // ********************************************************************************
  public static final class IC_Visa_Standard                                                                extends IC_Visa
  { public                  IC_Visa_Standard()                  { super( Program.STANDARD                      );  } }
  public static final class IC_Visa_Electronic                                                              extends IC_Visa
  { public                  IC_Visa_Electronic()                { super( Program.ELECTRONIC                    );  } }
  public static final class IC_Visa_CPS_Retail                                                              extends IC_Visa
  { public                  IC_Visa_CPS_Retail()                { super( Program.CPS_RETAIL                    );  } }
  public static final class IC_Visa_CPS_Restaurant                                                          extends IC_Visa
  { public                  IC_Visa_CPS_Restaurant()            { super( Program.CPS_RESTAURANT                );  } }
  public static final class IC_Visa_CPS_Supermarket                                                         extends IC_Visa
  { public                  IC_Visa_CPS_Supermarket()           { super( Program.CPS_SUPERMARKET                );  } }
  public static final class IC_Visa_CPS_RetailSvcStation                                                    extends IC_Visa
  { public                  IC_Visa_CPS_RetailSvcStation()      { super( Program.CPS_RETAIL_SERVICE_STATION     );  } }
  public static final class IC_Visa_CPS_SmallTicket                                                         extends IC_Visa
  { public                  IC_Visa_CPS_SmallTicket()           { super( Program.CPS_SMALL_TICKET              );  } }
  public static final class IC_Visa_CPS_RetailKeyEntry                                                      extends IC_Visa
  { public                  IC_Visa_CPS_RetailKeyEntry()        { super( Program.CPS_RETAIL_KEY_ENTRY          );  } }
  public static final class IC_Visa_CPS_CardNotPresent                                                      extends IC_Visa
  { public                  IC_Visa_CPS_CardNotPresent()        { super( Program.CPS_CARD_NOT_PRESENT           );  } }
  public static final class IC_Visa_CPS_AutomatedFuelDisp                                                   extends IC_Visa
  { public                  IC_Visa_CPS_AutomatedFuelDisp()     { super( Program.CPS_AUTOMATED_FUEL_DISPENSER   );  } }
  public static final class IC_Visa_CPS_AccountFunding                                                      extends IC_Visa
  { public                  IC_Visa_CPS_AccountFunding()        { super( Program.CPS_ACCOUNT_FUNDING            );  } }
  public static final class IC_Visa_CPS_ECommerceBasic                                                      extends IC_Visa
  { public                  IC_Visa_CPS_ECommerceBasic()        { super( Program.CPS_E_COMMERCE_BASIC          );  } }
  public static final class IC_Visa_CPS_ECommercePrefRtl                                                    extends IC_Visa
  { public                  IC_Visa_CPS_ECommercePrefRtl()      { super( Program.CPS_E_COMMERCE_PREF_RETAIL     );  } }
  public static final class IC_Visa_CPS_Hotel_Auto                                                          extends IC_Visa
  { public                  IC_Visa_CPS_Hotel_Auto()            { super( Program.CPS_HOTEL_AUTO                 );  } }
  public static final class IC_Visa_CPS_Hotel_Auto_CNP                                                      extends IC_Visa
  { public                  IC_Visa_CPS_Hotel_Auto_CNP()        { super( Program.CPS_HOTEL_AUTO_CNP            );  } }
  public static final class IC_Visa_CPS_ECommercePrefHtlAuto                                                extends IC_Visa
  { public                  IC_Visa_CPS_ECommercePrefHtlAuto()  { super( Program.CPS_E_COMMERCE_PREF_HTL_AUTO  );  } }
  public static final class IC_Visa_CPS_PassengerTransport                                                  extends IC_Visa
  { public                  IC_Visa_CPS_PassengerTransport()    { super( Program.CPS_PASSENGER_TRANSPORT      );  } }
  public static final class IC_Visa_CPS_PassengerTransport_CNP                                              extends IC_Visa
  { public                  IC_Visa_CPS_PassengerTransport_CNP(){ super( Program.CPS_PASSENGER_TRANSPORT_CNP   );  } }
  public static final class IC_Visa_CPS_ECommercePrefPassTrans                                              extends IC_Visa
  { public                  IC_Visa_CPS_ECommercePrefPassTrans(){ super( Program.CPS_E_COMMERCE_PREF_PASS_TRANS );  } }
  public static final class IC_Visa_CashDisbursement                                                        extends IC_Visa
  { public                  IC_Visa_CashDisbursement()          { super( Program.PROGRAM_CASH_DISBURSEMENT    );  } }
  public static final class IC_Visa_NotUSDomestic                                                           extends IC_Visa
  { public                  IC_Visa_NotUSDomestic()             { super( Program.PROGRAM_NOT_US_DOMESTIC      );  } }
  public static final class IC_Visa_CreditVoucher                                                           extends IC_Visa
  { public                  IC_Visa_CreditVoucher()             { super( Program.PROGRAM_CREDIT_VOUCHER       );  } }
  public static final class IC_Visa_CPS_RecurrBillPmt                                                           extends IC_Visa
  { public                  IC_Visa_CPS_RecurrBillPmt()         { super( Program.PROGRAM_CPS_RECU_PMT          );  } }

  // ********************************************************************************


  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  static{
    loadConfiguration();
  }

  public static void loadConfiguration() {
    log.debug(" Loading Visa Interchange Configurations ");
    nonCpsProgramMap = SettlementDb.loadNonCpsVisaProgramMap();
    cpsProgramMap = SettlementDb.loadCpsVisaProgramMap();
    fpiTable = SettlementDb.loadFpiProductTable();
    sicCodeGroupMap = SettlementDb.loadSicCodeGroupMap();
    programListCount = cpsProgramMap.size() + nonCpsProgramMap.size();
  }

  // constructor
  public InterchangeVisa()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  public boolean getBestClass( SettlementRecord tranInfo, String classname )
  {
    return getBestClass(tranInfo, Arrays.asList(classname));
  }

  public boolean getBestClass( SettlementRecord tranInfo, List classList )
  {
    boolean       retVal          = false;

    try
    {
      DisqualMap.clear();   // dump the disqual map

      IcInfo  icInfo = null;
      int bfCardType = CardTypeVisa.getBitField( tranInfo );
      getBestIcInfoClause = " and icd.debit_type is null";
      if( bfCardType != CardTypeVisa.UNKNOWN )
      {
        icInfo = getBestIrfInfoFromClassList( tranInfo, bfCardType, classList );
        if( icInfo == null )  {
          icInfo = getBestIrfInfoFromClassList( tranInfo, bfCardType, Arrays.asList(nonCpsProgramMap.get(Program.ELECTRONIC.getValue())));
        }
        if( icInfo == null ){

          icInfo = getBestIrfInfoFromClassList( tranInfo, bfCardType,  Arrays.asList(nonCpsProgramMap.get(Program.STANDARD.getValue())));
        }
      }
      if( icInfo == null )
      {
        icInfo = getBestIrfInfoFromInfoList( tranInfo, IRF_INFO_UNKNOWN );
      }
      else
      {
        retVal = true;
      }

      int debitType = tranInfo.getInt("debit_type");
      if( debitType != 0 )
      {
        //  check again, but only allow the regulated row(s) for this debit_type
        IcInfo  wouldHaveGotten = icInfo;
        String  whgIrfInfo      = wouldHaveGotten.getIcString();
        getBestIcInfoClause = " and nvl(icd.debit_type,0) = " + debitType;

        String    tran_region_issuer    = tranInfo.getPaddedString("region_issuer"  ,3).substring(0,2);
        String    tran_region_merchant  = tranInfo.getPaddedString("region_merchant",3).substring(0,2);
              if( !tran_region_merchant.equals(tran_region_issuer)  ) icInfo = getBestIrfInfoFromInfoList( tranInfo, IRF_INFO_REG_IR );
        else  if(  tran_region_merchant.equals("PR")                ) icInfo = getBestIrfInfoFromInfoList( tranInfo, IRF_INFO_REG_PR );
        else  if(  tran_region_merchant.equals("US") &&
           "D".equals(tranInfo.getString("debit_credit_indicator")) )
        {
              if( (bfCardType & CardTypeVisa.CONSUMER) == CardTypeVisa.CONSUMER &&
                   whgIrfInfo.charAt(LOC_RPS) == RPS_SMALL_TICKET )   icInfo = getBestIrfInfoFromInfoList( tranInfo, IRF_INFO_REG_STKT );
              else                                                    icInfo = getBestIrfInfoFromInfoList( tranInfo, IRF_INFO_REG_US );
        }
        else                                                          icInfo = new IcInfo();

        if( "---".equals(icInfo.getIcCode()) )    // if no regulated found, revert to "would have gotten"
        {
          icInfo = wouldHaveGotten;
        }
        else                                      // if regulated found, use some regulated and some "would have gotten" info
        {
          // get whg enh/fpi/ra/rps and the regulated fpi; store the whg enh/ra/rps and the regulated fpi
          String regFpi     = icInfo.getIcString().substring( LOC_FPI, LOC_FPI + 3 );
          icInfo.setIcString( whgIrfInfo.substring( 0,LOC_FPI ) + regFpi + whgIrfInfo.substring( LOC_FPI + 3 ) );

          // check for "would have gotten" lookup ic_code (if there isn't one, then ic_billing will be regulated ic_cat)
          String  regIc   = wouldHaveGotten.getIcCodeRegulated();
          if( regIc != null && regIc.length() == 4 )
          {
            icInfo.setIcCodeBilling(debitType + regIc);
          }
        }
      }

      String irfInfo = icInfo.getIcString();
      tranInfo.setData("fee_program_indicator"      , irfInfo.substring( LOC_FPI  , LOC_FPI + 3 ) );
      //Visa mandate for Oct-2018 assign Reimbursement attribute 'D' to 8661,8398 when FPI 379,193,31
      boolean isUSMerchant = tranInfo.getString("region_merchant") != null 
    		  && tranInfo.getString("region_merchant").toUpperCase().startsWith(US_MERCHANT);
      if(SIC_RELIGIOUS_CHARITY.contains(tranInfo.getData("sic_code")) && 
    		  FPI_CHARITY_RETAIL2_DB_PP.contains(tranInfo.getData("fee_program_indicator")) && isUSMerchant) {
        tranInfo.setData("reimbursement_attribute"    ,  RA_RELIGIOUS_CHARITY);  
      }else {
        tranInfo.setData("reimbursement_attribute"    , irfInfo.substring( LOC_RA   , LOC_RA  + 1 ) );
      }
      tranInfo.setData("requested_payment_service"  , irfInfo.substring( LOC_RPS  , LOC_RPS + 1 ) );
      tranInfo.setData("ic_cat"                     , icInfo.getIcCode());
      tranInfo.setData("ic_cat_billing"             , icInfo.getIcCodeBilling());
      if( "---".equals(icInfo.getIcCodeBilling()) )
      {
        icInfo = getBestIrfInfoFromClassList( tranInfo, bfCardType, Arrays.asList(nonCpsProgramMap.get(Program.ELECTRONIC.getValue())));
        if( icInfo != null )
        {
          tranInfo.setData("ic_cat_billing"             , icInfo.getIcCodeBilling());
        }
      }

      tranInfo.saveEnhDataAfterIcAssignment( irfInfo );
      tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeVisa.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }

}