/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeMasterCard.java $

  Description:

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $LastChangedDate: 2014-10-16 14:19:21 -0700 (Thu, 16 Oct 2014) $
  Version            : $Revision: 23192 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.support.SyncLog;
import com.mes.tools.DccRateUtil;

public class InterchangeMasterCard  extends InterchangeBase
{
  public static final Logger log = Logger.getLogger(InterchangeMasterCard.class);
  public static final String      MY_CARD_TYPE          = "MC";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeMasterCard$";


  // MasterCard irfInfo string format: "eeaa-bbbbbbbcdd,eeaa-bbbbbbbcdd,eeaa-bbbbbbbcdd" where ee=enhancements, aa=ird, bbbbbbb=bsa-arrangement, c=card-type, dd=addendum(s)-to-send
  public static final int     LOC_IRD           = SettlementRecord.NUM_ENHANCED_IND +  0;             // "aa" = ird         = 2 chars
  public static final int     LOC_SEPARATOR     = SettlementRecord.NUM_ENHANCED_IND +  2;             // "-"  = separator   = 1 char
  public static final int     LOC_BSA_TYPE_ID   = SettlementRecord.NUM_ENHANCED_IND +  3;             // "b"  = bsa arrange = 7 chars (1 char bsa type + 6 chars bsa id code)
  public static final int     LOC_CARD_TYPE     = SettlementRecord.NUM_ENHANCED_IND + 10;             // "c"  = card-type   = 1 char ('B' business, 'C' consumer credit, 'D' consumer debit)
  public static final int     LOC_ADDENDUM      = SettlementRecord.NUM_ENHANCED_IND + 11;             // "dd" = addendum(s) = 0-8 chars ("" up to "a1b2c3d4")
  public static final int     LEN_IRF_INFO      = SettlementRecord.NUM_ENHANCED_IND + 11 + 8;         // enh + (ird + separator + bsa + card-type) + addendum(s)
  public static final int     LEN_ISSUER_IC_LVL = 11;                                                 // (ird + separator + bsa + card-type)
  public static final String  IRF_INFO_UNKNOWN  = SettlementRecord.DEF_ENHANCED_IND + "-0---------";  // use when fail to qualify any MasterCard IRD


  // ********************************************************************************
  // ********************************************************************************
  public static          class IC_MasterCard                                                        extends IC_BaseClass
  {
    public                      IC_MasterCard()                  {  }



    ////// sic codes used to check various requirements.....

    public static final String  strSic_A001_Airline             = "3000-3350,4511"                      ;
    public static final String  strSic_B001_CruiseSteamship     = "4411"                                ;
    public static final String  strSic_F001_RestaurantEtc       = "5812-5814"                           ;
    public static final String  strSic_H001_Lodging             = "3501-3999,7011"                      ;
    public static final String  strSic_PET1_PetroleumCAT_AFD    = "5542"                                ;
  //public static final String  strSic_PET2_PetroleumSvcStn     = "5541"                                ;
    public static final String  strSic_R001_Railway             = "4112"                                ;
    public static final String  strSic_TA01_TravelAgency        = "4722"                                ;
    public static final String  strSic_V001_VehicleRental       = "3351-3500,7512,7513,7519"            ;
    public static final String  strSic_GW01_globalWholeSale     = "4131,4582,4722,5962,6513,7032,7033,7012,7298,7991,7997,7999";
    
    public static final String  strSic_EmploymentAgency         = "7361"                                ;
    public static final String  strSic_FuelLocation             = "4468,5499,5541,5542,5983,7511,9752"  ;
    public static final String  strSic_ShippingCourier          = "4214,4215"                           ;
    public static final String  strSic_T1TaxNotRequired         = "4111,4131,4215,4468,4784,5499,5541,5542,5983,7511,8211,8220,8398,8661,9211,9222,9311,9399,9402,9752"  ;
   
    public static final String strSic_C001_CashDisbursement		= "6010";
    public static final String strSic_D001_PaymentTransactions  = "6532,6533";
    public static final String strSic_Z001_ATM					= "6011";
    
    
    public static final String strSic_M001_MailOrder_TelephoneOrder
    															= "5960,5962,5964,5965,5966,5967,5968,5969";
    public static final String strSic_P001_BeautySalons			= "7230";
    public static final String strSic_S001_Supermarket			= "5411";
    public static final String strSic_T001_Telephone			= "4813,4814";
    public static final String strSic_U001_Unique				= "4829,6050,6051,7995,9754";    
    public static final String strSic_W001_WarehouseClub		= "5300";
    
    public static final String  strSic_AmtTolerance25Percent    = "7230"                                ;     // P001: Beauty Salon
  //public static final String  strSic_AmtToleranceExempt_9402  = "9402"                                ;
    public static final String  strSic_AmtToleranceExemptAlways = strSic_A001_Airline                   + "," +
                                                                  strSic_B001_CruiseSteamship           + "," +
                                                                  strSic_F001_RestaurantEtc             + "," +
                                                                  strSic_H001_Lodging                   + "," +
                                                                  strSic_PET1_PetroleumCAT_AFD          + "," +
                                                                  strSic_R001_Railway                   + "," +
                                                                  strSic_V001_VehicleRental             ;

    public static final String  strSic_TimelinessExemptAlways   = strSic_B001_CruiseSteamship           + "," +
                                                                  strSic_H001_Lodging                   + "," +
                                                                  strSic_V001_VehicleRental             ;

    public static final String  strSic_EligibleForBB           =  strSic_GW01_globalWholeSale           + "," +
                                                                  strSic_A001_Airline                   + "," +
                                                                  strSic_V001_VehicleRental             + "," +
                                                                  strSic_B001_CruiseSteamship           + "," +
                                                                  strSic_H001_Lodging                   + "," +
                                                                  strSic_R001_Railway                   ;
      
    public static final String  strSic_EligibleForZX		   =  strSic_A001_Airline 			+ "," +             
    		  													  strSic_B001_CruiseSteamship 	+ "," + 
													    		  strSic_F001_RestaurantEtc     + "," +  
													    		  strSic_H001_Lodging           + "," +  
													    		  strSic_M001_MailOrder_TelephoneOrder + "," +
													    		  strSic_P001_BeautySalons		+ "," +
													    		  strSic_R001_Railway			+ "," +
													    		  strSic_S001_Supermarket		+ "," +
													    		  strSic_T001_Telephone			+ "," +	
													    		  strSic_U001_Unique			+ "," +	
													    		  strSic_V001_VehicleRental		+ "," +
													    		  strSic_W001_WarehouseClub;  
    
    /*OTH1-Other : Not In A001, B001, C001, D001, F001, H001, M001, P001, R001, S001, T001, U001, V001, W001, Z001*/
    public static final String  strSic_NotInOTH1 	   			= 	strSic_A001_Airline 			+ "," +             
			  														strSic_B001_CruiseSteamship 	+ "," + 
			  														strSic_C001_CashDisbursement	+ "," +
																    strSic_D001_PaymentTransactions	+ "," +
																    strSic_F001_RestaurantEtc     	+ "," +  
																    strSic_H001_Lodging           	+ "," +  
																    strSic_M001_MailOrder_TelephoneOrder + "," +
																    strSic_P001_BeautySalons		+ "," +
																    strSic_R001_Railway				+ "," +
																    strSic_S001_Supermarket			+ "," +
																    strSic_T001_Telephone			+ "," +	
																    strSic_U001_Unique				+ "," +	
																    strSic_V001_VehicleRental		+ "," +
																    strSic_W001_WarehouseClub		+ "," +
																    strSic_Z001_ATM;
    		 

    ////// IcProgramRulesMC contents.....

    // length of strings for "auth_required" and "timelines"; four per rule set (non-airline-then-airline for gcms-then-icc)
    public static final int     IC_RULE_LENGTH_AUTH_REQD                = 1;        // Y or N
    public static final int     IC_RULE_LENGTH_TIMELINESS               = 8;        // "ddddeeee" = 4-digit number-of-days & 4 Y/N exclusions

    // position of exemption flags for "amount_tolerance" and "timeliness"
    public static final int     IC_RULE_POSITION_EXEMPT_CNP             = 0;        // Y or .
    public static final int     IC_RULE_POSITION_EXEMPT_ECOMM5          = 1;        // Y or .

    // characters used to define input mode requirements
    public static final char    IC_RULE_INPUT_APPLIES_TO_ALL            = 'A';      // applies to all
    public static final char    IC_RULE_INPUT_APPLIES_TO_FLEET_AT_FUEL  = 'F';      // applies to fleet cards at fuel locations

    public static final char    IC_RULE_INPUT_MUST_BE_RECURRING         = 'R';      // must be card-not-present recurring transaction
    public static final char    IC_RULE_INPUT_MUST_BE_KEYED             = 'K';      // must be face-to-face key-entered
    public static final char    IC_RULE_INPUT_MUST_BE_SWIPED            = 'S';      // must be face-to-face swiped
    public static final char    IC_RULE_INPUT_MUST_BE_SWIPED_FULL_MERCH = 'Z';      // must be face-to-face swiped ~ OR ~ must be CNP UCAF Full or UCAF Merchant  
    public static final char    IC_RULE_INPUT_MUST_BE_UCAF_FULL         = 'F';      // must be card-not-present UCAF Full
    public static final char    IC_RULE_INPUT_MUST_BE_UCAF_MERCH        = 'M';      // must be card-not-present UCAF Merchant
    public static final char    IC_RULE_INPUT_MUST_BE_CHIP_ACQ          = 'A';      // must be acquirer chip
    public static final char    IC_RULE_INPUT_MUST_BE_CHIP_ISS          = 'I';      // must be issuer chip
    public static final char    IC_RULE_INPUT_MUST_BE_NON_UCAF_MERCH    = 'U';      // ECommerce security Level Indicator not 1, 2

    // acceptable values for the possible input modes
    public static final String  strInputModeRecurring                   = "406,401";
    public static final String  strInputModeKeyed                       = "016,916,011";
    public static final String  strInputModeSwiped                      = "012,01A,01B,01C,01M,01N,912,91A,91B,91C,91M,91N";
    public static final String  strInputModeChip                        = "010,011,012,016,01A,01B,01S";
    public static final String  strInputModeUCAF                        = "50S";    // plus check UCAF Collection Ind = 1/Merchant or 2/Full


    ////// MasterCard provided values.....

    // MasterCard Product Class (only two of many; don't use any others)
    public static final String  strProductClass_Fleet                   = "MCF,MPK";

    // MasterCard GCMS Product ID (only some of many; don't use any others)
    public static final String  strGcmsProductId_ElectronicPaymentAccnt = "MEF";
    public static final String  strGcmsProductId_TaxExempt              = "MGF,MNF,MPK";

    // MasterCard ALM Codes
    //    'C'   =        level 1
    //    'D'   =        level 1 & product graduation
    //    'E'   =        level 2
    //    'F'   =        level 2 & product graduation
    //    'G'   =        level 3
    //    'H'   =        level 3 & product graduation
    //    'J'   =        level 4
    //    'K'   =        level 4 & product graduation
    //    'B'   = enhanced value
    //    'M'   = enhanced value & product graduation
    //    'P'   =                  product graduation
    //    'S'   =     high value
    //    'T'   =     high value & product graduation
    //    'W'   =spend shortfall
    //    'Y'   =spend shortfall & product graduation
    //    'Z'   = business-as-usual (neither enhanced value nor product graduation)

    // MasterCard Processing Codes
    //    '00'  = purchase
    //    '09'  = purchase with cashback
    //    '18'  = unique (purchase with specific sic codes)
    //    '20'  = credit voucher / return
    //    '12'  = cash disbursement
    //    '28'  = payment transaction



    public String   tranQualifies( SettlementRecord tranInfo, IcProgramRulesMC thisRuleSet )
    {
      String      retVal      = null;
      String      enhRequired = SettlementRecord.DEF_ENHANCED_IND;

      try
      {
        // These requirements are not checked here because enough information is in Oracle
        // ****** in the MasterCard MPE Tables ******
        // * Sic Code
        // * Processing Code
        // * GCMS Product ID
        // * MasterCard Assigned ID
        //
        // ****** in the MeS "MC IC Program Rules" Table ******
        // * Program Registration ID
        //      Payment Tran    (IRD 20,21)                 = "C01"
        //      Rebate          (IRD EZ)                    = "C02"
        //      MoneySend       (IRD MS)                    = "C07"
        // * Eligibility Flag
        //      Warehouse, Supermarket, Merit III           = "THn" (where n=1/2/3 for Tier 1/2/3)
        //      Various Brazil Domestic (4-076000) IRD's    = SMK (supermarket) and/or CR0/CR1/CR2 (installment type)
        //

	      Date batchDate = tranInfo.getDate("batch_date");
	      String fundingCurrencyCode = tranInfo.getData("funding_currency_code");
	      String originalCurrencyCode = tranInfo.getData("currency_code");

	      double amountLimit = 0;
	      double fxRate = tranInfo.getDouble("fx_rate");
	      double tran_transaction_amount = tranInfo.getDouble("transaction_amount") * fxRate;

        String  tran_sic_code             = tranInfo.getString("sic_code");
        String  tran_product_class        = tranInfo.getString("product_class");
        String  tran_PresenceAndInput     = tranInfo.getPaddedString("pos_data_code",12).substring(4, 4+3);

        boolean isCardCommercial          = tranInfo.getString("card_type").charAt(1) == 'B';   // card_type  = MB or MC or MD
        boolean isCardFleet               = testDataIsInList( 3, tran_product_class, strProductClass_Fleet );

        boolean isSicFuelLocation         = testDataIsInList( 4, tran_sic_code, strSic_FuelLocation );

        boolean isTranPurchReturn         = "C".equals(tranInfo.getString("debit_credit_indicator"));
        boolean isTranExemptFromICC       = "N".equals(thisRuleSet.getIcComplianceSwitch());
        boolean isTranFleetAtFuel         = isCardFleet && isSicFuelLocation;
        boolean isTranCardNotPresent      = tran_PresenceAndInput.charAt(1) == '0';
        boolean isTranECommWith5Ind       = testDataIsInList( 3, tran_PresenceAndInput, strInputModeUCAF );
        boolean isTranAcquirerChip        = ( "5CDEM".indexOf(tranInfo.getPaddedString("pos_data_code",1).substring(0, 0+1)) >= 0 );
        boolean isTranIssuerChip          = ( tranInfo.getInt("service_code") % 100 == 2 ||
                                              tranInfo.getInt("service_code") % 100 == 6 );
        boolean isTranUSDomestic          = tranInfo.getString("bsa_type" ).charAt(0) == '2' &&
                                            "01".equals(tranInfo.getString("bsa_id_code").substring(0,2));
        boolean isTranGlobalTravel        =  tranInfo.getString("bsa_type" ).charAt(0) == '8' &&
        		                             "000900".equals(tranInfo.getString("bsa_id_code"));


        DisqualList.removeAllElements();    // clear the disqual list

        // ** check requirements met: Fleet Card at Fuel Location eligibility
        if( isTranFleetAtFuel )
        {
          if( !thisRuleSet.getFleetAtFuel() )
          {
            DisqualList.add("Tran cannot be fleet card at fuel location");
          }
        }
        
        if( !testDataIsInList( 4, tran_sic_code, strSic_EligibleForBB )){
        	if("MBS".equals(tranInfo.getString("gcms_product_id")) && isTranGlobalTravel){
        		 DisqualList.add("Tran cannot be eligible for BB");
        	}
        	
        }

        // If the transaction sic code is in strSic_EligibleForZX or transaction sic code is not in strSic_NotInOTH1 transaction is eligible for IRD ZX 
        // Transaction mcc in (A001, B001, F001, H001, M001, P001, R001, S001, T001, U001, V001, W001) 
        // or Transaction mcc not in(A001, B001, C001, D001, F001, H001, M001, P001, R001, S001, T001, U001, V001, W001, Z001)
        if( ( "MWF".equals(tranInfo.getString("gcms_product_id")) || "DWF".equals(tranInfo.getString("gcms_product_id")) ) ) {
        	
        	if (!( testDataIsInList( 4, tranInfo.getString("sic_code"), strSic_EligibleForZX ) || !testDataIsInList( 4, tranInfo.getString("sic_code"), strSic_NotInOTH1 ) ) ) {
        		DisqualList.add("Tran cannot be eligible for ZX");
        	}
        }

        // ** check requirements met: amount minimum
        amountLimit = thisRuleSet.getAmountMin();
        if( amountLimit != 0.00 && tran_transaction_amount < amountLimit )
        {
          {
            DisqualList.add("Tran amount less than minimum: $" + String.valueOf(amountLimit));
          }
        }

        // ** check requirements met: amount maximum
        amountLimit = thisRuleSet.getAmountMax();
        if( amountLimit != 0.00 && tran_transaction_amount > amountLimit )
        {
          boolean isExempt = false;
          String  strExemptions = thisRuleSet.getAmountMaxExemptSics();
          if( strExemptions != null )
          {
            if( "!:".equals(strExemptions.substring(0,2) ) )
            {
              if( !testDataIsInList( 4, tran_sic_code, strExemptions.substring(2) ) )isExempt = true;
            }
            else
            {
              if( testDataIsInList( 4, tran_sic_code, strExemptions  ) )isExempt = true;
            }
          }
          if( !isExempt )
          {
            DisqualList.add("Tran amount greater than maximum: $" + String.valueOf(amountLimit));
          }
        }

        // ** check requirements met: MC Assigned ID required
        if( !"NN".equals(thisRuleSet.getMcAssignedIdFlags()) )
        {
          if( tranInfo.isFieldBlank( "mc_assigned_id" ) )
          {
            DisqualList.add("IRD requires MC Assigned ID");
          }
        }

        if( !isTranExemptFromICC )
        {
          // ** check requirements met: amount tolerance
          if( DisqualList.size() == 0 )
          {
            amountLimit = thisRuleSet.getAmountTolerance();
            if( amountLimit != 0.00 )
            {
              if( testDataIsInList( 4, tran_sic_code, strSic_AmtTolerance25Percent ) )  amountLimit = 0.25;

              if( tran_transaction_amount > (amountLimit * 100.0) )
              {
                double tran_auth_amount_total = tranInfo.getDouble("auth_amount_total") * fxRate;

                if( tran_auth_amount_total <= (amountLimit * 100.0) )
                {
                  // do nothing if auth amt < limit
                }
                else
                if( ( tran_auth_amount_total  < ( tran_transaction_amount * (1.0 - amountLimit) ) ) ||
                    ( tran_auth_amount_total  > ( tran_transaction_amount * (1.0 + amountLimit) ) ) )
                {
                  boolean isExempt = testDataIsInList( 4, tran_sic_code, strSic_AmtToleranceExemptAlways );

                  if( !isExempt )
                  {
                    String  strExemptions = thisRuleSet.getAmountToleranceExemptions();
                    if( ( strExemptions != null ) &&
                        ( ( strExemptions.charAt(IC_RULE_POSITION_EXEMPT_CNP    ) == 'Y' && isTranCardNotPresent ) ||
                          ( strExemptions.charAt(IC_RULE_POSITION_EXEMPT_ECOMM5 ) == 'Y' && isTranECommWith5Ind  ) ) )
                    {
                      isExempt = true;
                    }
                    else
                    {
                      strExemptions = thisRuleSet.getAmountToleranceExemptSics();
                      if( strExemptions != null )
                      {
                        if( testDataIsInList( 4, tran_sic_code, strExemptions ) ) isExempt = true;
                      }

                    //// sic code 9402 with terminal type CT2 is exempt
                    //if( testDataIsInList( 4, tran_sic_code, strSic_AmtToleranceExempt_9402 ) )
                    //{
                    //  if( "CT2".equals(tranInfo.getString("cat_indicator") )    isExempt = true;
                    //}
                    }
                  }

                  if( !isExempt )
                  {
                    DisqualList.add("Tran Amount / Total Auth Amount not within tolerance");
                  }
                }
              }
            }
          } // if( DisqualList.size() == 0 )

          // ** check requirements met: magnetic stripe data (using card/cardholder presence & input mode & e-commerce indicators)
          if( DisqualList.size() == 0 )
          {
            String    requiredInputMode = thisRuleSet.getPosEntryModeAllowed();
            if( requiredInputMode != null )
            {
              int status  = ( requiredInputMode.length() == 2 ) ? 0 : 2;    // 0 = failed, 1 = passed, 2 = format error

              if( status == 0 )
              {
                switch( requiredInputMode.charAt(0) )
                {
                  case IC_RULE_INPUT_APPLIES_TO_ALL:                                                  break;  // check mode
                  case IC_RULE_INPUT_APPLIES_TO_FLEET_AT_FUEL:  if( !isTranFleetAtFuel  ) status = 1; break;  // check mode only if fleet at fuel
                  default:                                                                status = 2; break;  // report error
                }
              }

              if( status == 0 )
              {
                char  myCollectionInd = tranInfo.getPaddedString("ecomm_security_level_ind",3).charAt(2);

                switch( requiredInputMode.charAt(1) )
                {
                  case IC_RULE_INPUT_MUST_BE_RECURRING:   if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeRecurring ) ) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_KEYED:       if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeKeyed     ) ) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_SWIPED:      if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeSwiped    ) ) status = 1; break;

                  case IC_RULE_INPUT_MUST_BE_SWIPED_FULL_MERCH: if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeSwiped    ) ) status = 1;
                                                    else  if( isTranECommWith5Ind && (myCollectionInd == '1' || myCollectionInd == '2')) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_UCAF_FULL:   if( isTranECommWith5Ind && myCollectionInd == '2'                       ) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_UCAF_MERCH:  if( isTranECommWith5Ind && myCollectionInd == '1'                       ) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_NON_UCAF_MERCH:  if(!(myCollectionInd == '1' || myCollectionInd == '2')) status = 1; break; 
                  case IC_RULE_INPUT_MUST_BE_CHIP_ACQ:    if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeChip      ) &&
                                                              isTranAcquirerChip  && !isTranIssuerChip                            ) status = 1; break;
                  case IC_RULE_INPUT_MUST_BE_CHIP_ISS:    if( testDataIsInList( 3, tran_PresenceAndInput, strInputModeChip      ) &&
                                                             !isTranAcquirerChip  &&  isTranIssuerChip                            ) status = 1; break;


                  default:                                                                                                          status = 2; break;
                }
              }

            //// sic code 5542 must be CT1 or CT2 to pass
            //if( testDataIsInList( 4, tran_sic_code, strSic_PET1_PetroleumCAT_AFD ) &&
            //    !testDataIsInList( 3, tranInfo.getPaddedString("cat_indicator",3), "CT1,CT2" ) )
            //{
            //  status = 0;
            //}

              if( status == 2 ) System.out.println("tranQualifies(): bad input mode string..." + requiredInputMode);
              if( status != 1 ) DisqualList.add("Tran failed input mode test: " + requiredInputMode);
            } // if( requiredInputMode != null )
          } // if( DisqualList.size() == 0 )
        } // if( !isTranExemptFromICC )

        // ** check requirements met: authorization & timeliness
        if( DisqualList.size() == 0 )
        {
          int   offsetAuthRequired;
          int   offsetTimeliness;

          if( testDataIsInList( 4, tran_sic_code, strSic_A001_Airline ) )
          {
            offsetAuthRequired  = IC_RULE_LENGTH_AUTH_REQD;
            offsetTimeliness    = IC_RULE_LENGTH_TIMELINESS;
          }
          else
          {
            offsetAuthRequired  = 0;
            offsetTimeliness    = 0;
          }

          // ** ** authorization -- I/C Compliance only (not sure what GCMS auth requirement could be)
          String strAuthRequiredFlags = thisRuleSet.getAuthRequiredFlags();
          if( !isTranExemptFromICC )
          {
            offsetAuthRequired += (2 * IC_RULE_LENGTH_AUTH_REQD);
            if( strAuthRequiredFlags.charAt(offsetAuthRequired) == 'Y' )
            {
              if( tranInfo.isFieldBlank( "auth_banknet_ref_num" ) ||
                  tranInfo.isFieldBlank( "auth_banknet_date"    ) )
              {
                DisqualList.add("Failed to meet authorization requirements (ICC)");
              }
            }
          } // if( !isTranExemptFromICC )


          // ** ** timeliness -- first GCMS then I/C Compliance
          String strTimelinessFlags = thisRuleSet.getTimelinessFlags();
          int intDays = Integer.valueOf( strTimelinessFlags.substring(offsetTimeliness, offsetTimeliness + 4) ).intValue();
          if( intDays != 0 )
          {
            if( !tranMeetsDateTimeliness(tranInfo, false, intDays, strTimelinessFlags.substring(offsetTimeliness + 4,offsetTimeliness + 8)) )
            {
              DisqualList.add("Failed to meet timeliness requirements (GCMS)");
            }
            if( !isTranExemptFromICC )
            {
              if( !testDataIsInList( 4, tran_sic_code, strSic_TimelinessExemptAlways ) )
              {
                offsetTimeliness += (2 * IC_RULE_LENGTH_TIMELINESS);
                intDays = Integer.valueOf( strTimelinessFlags.substring(offsetTimeliness, offsetTimeliness + 4) ).intValue();
                if( intDays != 0 )
                {
                  String  strExemptions = thisRuleSet.getTimelinessExemptions();
                  if( ( strExemptions == null ) ||
                      ! ( ( strExemptions.charAt(IC_RULE_POSITION_EXEMPT_CNP    ) == 'Y' && isTranCardNotPresent ) ||
                          ( strExemptions.charAt(IC_RULE_POSITION_EXEMPT_ECOMM5 ) == 'Y' && isTranECommWith5Ind  ) ) )
                  {
                    if( !tranMeetsDateTimeliness(tranInfo, true, intDays, strTimelinessFlags.substring(offsetTimeliness + 4,offsetTimeliness + 8)) )
                    {
                      DisqualList.add("Failed to meet timeliness requirements (ICC)");
                    }
                  }
                }
              }
            } // if( !isTranExemptFromICC )
          }
        } // if( DisqualList.size() == 0 )

        // ** check requirements met: addendum
        if( DisqualList.size() == 0 )
        {
          String    addendReqsPassed = "";
          String    addendReqsList = thisRuleSet.getAddendumRequired();
          if( addendReqsList != null )
          {
            String[]  addendReqsArray = addendReqsList.split(",");

            for( int i = 0; i < addendReqsArray.length; ++i )
            {
              String    addendReq = addendReqsArray[i];

              int status  = ( addendReq.length() == 2 ) ? 0 : 2;    // 0 = failed, 1 = passed, 2 = format error

              if( status == 0 )
              {
                switch( addendReq.charAt(0) )
                {
                  case 'I':   // ID      ... PDS 0596 (Card Acceptor Tax ID)
                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "D" ) )             { status = 2; break; }

                    // if not a commercial card, do not need a tax id
                    if( !isCardCommercial )                                               { continue; }

                    if( !tranInfo.isFieldBlank( "merchant_tax_id" ) )
                    {
                      status = 1;       // met "ID"
                    }
                    break;

                  case '5':   // 57 - 57 ... PDS 0057
                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "7" ) )             { status = 2; break; }

                    status = 1;       // met "57" -- for Large Ticket MPG, must send PDS 0057=01
                    break;

                  case 'M':   // M1 - M2 ... MoneySend / Payment Transaction
                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-2" ) )           { status = 2; break; }


                    // For MoneySend/Payment Transaction Merchants, MeS will always supply...
                    //    Customer Service 800# & Card Acceptor URL

                    if( !tranInfo.isFieldBlank( "payer_user_info"    ) &&
                        !tranInfo.isFieldBlank( "date_of_funds_reqd" ) &&
                        !tranInfo.isFieldBlank( "addl_trace_num"     ) )
                    {
                      addendReq = "M2";
                      status = 1;       // met "M2"
                    }
                    else if( addendReq.charAt(1) == '1' )
                    {
                      continue;
                    }
                    break;

                  case 'L':   // L1 - L2 ... Lodging
                    // if not lodging sic code, go to next requirement
                    if( !testDataIsInList( 4, tran_sic_code, strSic_H001_Lodging ) )      { continue; }

                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-2" ) )           { status = 2; break; }


                    // For Lodging Merchants, MeS will always supply...
                    //    Customer Service 800# & Property Phone #

                    if( !tranInfo.isFieldBlank( "statement_date_begin"  ) &&
                        !tranInfo.isFieldBlank( "statement_date_end"    ) &&
                        !tranInfo.isFieldBlank( "purchase_id"           ) )           // folio #
                    {
                      if( !tranInfo.isFieldBlank( "rate_daily"                ) &&
                          !tranInfo.isFieldBlank( "total_room_tax"            ) &&
                          !tranInfo.isFieldBlank( "hotel_rental_days"         ) &&
                          !tranInfo.isFieldBlank( "hotel_fire_safety_act_ind" ) )
                      {
                        addendReq = "L2";
                        status = 1;       // met "L2"
                      }
                      else if( addendReq.charAt(1) == '1' )
                      {
                        status = 1;       // met "L1"
                      }
                    }

                    // if no addenda data for "consumer interregional card", go to next requirement
                    if( status != 1 && !isTranUSDomestic & !isCardCommercial )            { continue; }

                    break;

                  case 'V':   // V1 - V2 ... Vehicle Rental
                    // if not vehicle rental sic code, go to next requirement
                    if( !testDataIsInList( 4, tran_sic_code, strSic_V001_VehicleRental ) ){ continue; }

                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-2" ) )           { status = 2; break; }


                    // For Vehicle Rental Merchants, MeS will always supply...
                    //    Customer Service 800#

                    if( !tranInfo.isFieldBlank( "statement_date_begin"      ) &&
                        !tranInfo.isFieldBlank( "statement_date_end"        ) &&
                        !tranInfo.isFieldBlank( "purchase_id"               ) &&          // rental agreement #
                        !tranInfo.isFieldBlank( "renter_name"               ) &&
                        !tranInfo.isFieldBlank( "rental_return_city"        ) &&
                        !tranInfo.isFieldBlank( "rental_return_state"       ) &&
                        !tranInfo.isFieldBlank( "rental_return_country"     ) &&
                        !tranInfo.isFieldBlank( "rental_return_location_id" ) )
                    {
                      if( !tranInfo.isFieldBlank( "rate_daily"                      ) &&
                          !tranInfo.isFieldBlank( "rate_daily_weekly_ind"           ) &&
                          !tranInfo.isFieldBlank( "rental_class_id"                 ) &&
                          !tranInfo.isFieldBlank( "hotel_rental_days"               ) &&
                          !tranInfo.isFieldBlank( "rental_location_city"            ) &&
                          !tranInfo.isFieldBlank( "rental_location_state"           ) &&
                          !tranInfo.isFieldBlank( "rental_location_country"         ) )
                      {
                        addendReq = "V2";
                        status = 1;       // met "V2"
                      }
                      else if( addendReq.charAt(1) == '1' )
                      {
                        status = 1;       // met "V1"
                      }
                    }

                    // if no addenda data for "consumer interregional card", go to next requirement
                    if( status != 1 && !isTranUSDomestic & !isCardCommercial )            { continue; }

                    break;

                  case 'P':   // P1 - P3 ... Passenger Transport
                    boolean   isSicAirline      = testDataIsInList( 4, tran_sic_code, strSic_A001_Airline       );
                    boolean   isSicRailway      = testDataIsInList( 4, tran_sic_code, strSic_R001_Railway       );
                    boolean   isSicTravelAgency = testDataIsInList( 4, tran_sic_code, strSic_TA01_TravelAgency  );

                    // if not passenger transport sic code, go to next requirement
                    if( !isSicAirline && !isSicRailway && !isSicTravelAgency )            { continue; }

                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-3" ) )           { status = 2; break; }


                    if( !tranInfo.isFieldBlank( "passenger_name"    ) &&
                        !tranInfo.isFieldBlank( "ticket_number"     ) &&
                        !tranInfo.isFieldBlank( "issuing_carrier"   ) )
                    {
                      int metLevel = 0;    // no extra passenger transport data
                      if( !tranInfo.isFieldBlank( "travel_date"                     ) &&
                          !tranInfo.isFieldBlank( "carrier_code"                    ) &&
                          !tranInfo.isFieldBlank( "svc_class_code"                  ) &&
                          !tranInfo.isFieldBlank( "origin_city"                     ) &&
                          !tranInfo.isFieldBlank( "destination_city"                ) )
                      {
                        metLevel |= 0x0001;    // pt trip leg basic

                        if( !tranInfo.isFieldBlank( "total_fare"                      ) &&
                            !tranInfo.isFieldBlank( "stop_over_code"                  ) &&
                            !tranInfo.isFieldBlank( "fare_basis_code"                 ) &&
                            !tranInfo.isFieldBlank( "flight_number"                   ) &&
                            !tranInfo.isFieldBlank( "departure_time"                  ) )
                        {
                          metLevel |= 0x0010;    // pt trip leg enhanced
                        }
                      }

                      if( isSicRailway || isSicTravelAgency )
                      {
                        if( !tranInfo.isFieldBlank( "travel_date"                     ) &&
                            !tranInfo.isFieldBlank( "start_station"                   ) &&
                            !tranInfo.isFieldBlank( "dest_station"                    ) &&
                            !tranInfo.isFieldBlank( "passenger_description"           ) )
                        {
                          metLevel |= 0x0100;    // rail trip leg basic

                          if( !tranInfo.isFieldBlank( "total_fare"                      ) &&
                              !tranInfo.isFieldBlank( "service_type"                    ) )
                          {
                            metLevel |= 0x1000;    // rail trip leg enhanced
                          }
                        }
                      }
                      if( (metLevel & 0x1010) != 0x0000 )
                      {
                        status = 1;       // met "P3"
                      }
                      else
                      if( (metLevel & 0x0101) != 0x0000 && addendReq.charAt(1) <= '2' )
                      {
                        status = 1;       // met "P2"
                      }
                      else
                      if( addendReq.charAt(1) == '1' )
                      {
                        status = 1;       // met "P1"
                      }
                      if( status == 1 )
                      {
                        addendReq = "P" + String.valueOf(metLevel + '0');   // store bitfield as "P0" thru "P?"
                      }
                    }

                    // if no addenda data for travel agency sic, go to next requirement
                    if( status != 1 && isSicTravelAgency )                                { continue; }

                    // if no addenda data for "consumer interregional card", go to next requirement
                    if( status != 1 && !isTranUSDomestic & !isCardCommercial )            { continue; }

                    break;

                  case 'T':   // T1 - T4 ... Total Tax Amt
                    boolean exactTaxRangeOnly   = addendReq.charAt(1) == '1'; // T1 has more requirements than others
                    boolean isTaxExemptCard     = testDataIsInList( 3, tranInfo.getString("gcms_product_id"), strGcmsProductId_TaxExempt );
                    double  tran_tax_amount     = tranInfo.getDouble("tax_amount");
                    boolean checkEnhancements   = false;

                    do
                    {
                      // unless exempt, must be 0.1-30% of tran amount
                      if( exactTaxRangeOnly )
                      {
                        if( tran_tax_amount < (tran_transaction_amount * 0.001) ||
                            tran_tax_amount > (tran_transaction_amount * 0.300) )
                        {
                          tran_tax_amount = 0.00;
                        }
                      }
                      if( tran_tax_amount != 0.00 )
                      {
                        addendReq = "TT";
                        status = 1;       // met "TT"
                      }
                      else
                      {
                        // if card and/or merchant doesn't need, go to next requirement
                        // if invalid modifier, post error
                        switch( addendReq.charAt(1) )
                        {
                          case '1': if( testDataIsInList( 4, tran_sic_code, strSic_T1TaxNotRequired ) )
                                                            status = -1;  break;      // required unless one of the T1 tax-not-required sic's
                          case '2': if( isSicFuelLocation ) status = -1;  break;      // required unless fuel location
                          case '3': if( isTranFleetAtFuel ) status = -1;  break;      // required unless fleet card at fuel location
                          case '4': if( isTranPurchReturn ) status = -1;  break;      // required unless purchse return (credit)

                          default:                          status = 2; break;
                        }
                        if( status == 0 && (!exactTaxRangeOnly || isTaxExemptCard) )
                        {
                          addendReq = "T0";
                          status = 1;       // met "T0" (we will send $0 tax & set tax exempt indicator)
                        }
                      }

                      if( checkEnhancements )   // if currently checking enhancements
                      {
                        if( status == 1 ) enhRequired = tranInfo.setEnhIndTax(enhRequired); // indicate that the ird needs enh tax
                        checkEnhancements   = false;                                        // don't check again
                      }
                      else                      // if not currently checking enhancements
                      {
                        if( status == 0 )         // if failed tax requirement, check again if have enh tax amt
                        {
                          tran_tax_amount = tranInfo.getDouble("enh_tax_amount");
                          if( tran_tax_amount != 0.00 ) checkEnhancements = true;
                        }
                      }
                    }while( checkEnhancements && status >= 0 );
                    if( status < 0 )continue;
                    break;

                  case 'F':   // F1 - F2 ... Fleet Card at Fuel Location Sic
                    // if not US-issued fleet card at fuel location, go to next requirement
                    if( !isTranUSDomestic || !isTranFleetAtFuel )                         { continue; }

                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-2" ) )           { status = 2; break; }


                    // For commercial cards acq'd in the US, MeS will always supply...
                    //    Card Acceptor Type & Card Acceptor Tax ID & Legal Corporate Name
                    //    Customer Code if present (rule = "Customer Code must be sent if provided by the customer.  Do not send all spaces.")

                    if( !tranInfo.isFieldBlank( "oil_co_brand_name" ) &&
                        !tranInfo.isFieldBlank( "purchase_time"     ) &&
                        !tranInfo.isFieldBlank( "motor_fuel_info"   ) )
                    {
                      if( !tranInfo.isFieldBlank( "odometer_reading"            ) &&
                          !tranInfo.isFieldBlank( "vehicle_number"              ) &&
                          !tranInfo.isFieldBlank( "driver_number_or_id_number"  ) &&
                          !tranInfo.isFieldBlank( "product_type_code"           ) )
                      {
                        addendReq = "F2";
                        status = 1;       // met "F2"
                      }
                      else if( addendReq.charAt(1) == '1' )
                      {
                        status = 1;       // met "F1"
                      }
                    }
                    break;

                  case 'C':   // C1 - C3 ... Commercial Card
                    // if not US-issued or not commercial card or is fleet card at fuel location, go to next requirement
                    if( !isTranUSDomestic || !isCardCommercial || isTranFleetAtFuel )     { continue; }

                    // if invalid modifier, post error
                    if( !testDataIsInList( 1, addendReq.substring(1), "1-3" ) )           { status = 2; break; }


                    // For commercial cards acq'd in the US, MeS will always supply...
                    //    Card Acceptor Type & Card Acceptor Tax ID & Legal Corporate Name
                    //    Customer Code if present (rule = "Customer Code must be sent if provided by the customer.  Do not send all spaces.")

                    switch( addendReq.charAt(1) )
                    {
                      case '2':
                        // if not Electronic Payment Account card, go to next requirement
                        if( !testDataIsInList( 3, tranInfo.getString("gcms_product_id"), strGcmsProductId_ElectronicPaymentAccnt ) )
                                                                                              { continue; }

                        // For Electronic Payment Account transactions, MeS will always supply...
                        //    Customer Identifier Type = PMTREF (payment reference number)
                        if( tranInfo.isFieldBlank( "custom_id_detail" ) )
                        {
                          break;            // failed to meet requirement
                        }
                        if( isTranPurchReturn )
                        {
                          status = 1;       // met "C2"
                          break;
                        }
                      // fall through...
                      case '1':
                        if( !tranInfo.isFieldBlank( "purchase_id" ) )         // invoice #
                        {
                          status = 1;       // met "C1" or "C2"
                        }
                        else if( !tranInfo.isFieldBlank( "enh_purchase_id" ) )         // invoice #
                        {
                          enhRequired = tranInfo.setEnhIndPID(enhRequired);   // indicate that the ird needs enh pid
                          status = 1;       // met "C1" or "C2"
                        }
                        break;

                      case '3':
                        if( testDataIsInList( 4, tran_sic_code, strSic_EmploymentAgency ) )
                        {
                          if( !tranInfo.isFieldBlank( "employee_name"    ) &&
                              !tranInfo.isFieldBlank( "job_description"  ) &&
                              !tranInfo.isFieldBlank( "temp_start_date"  ) &&
                              !tranInfo.isFieldBlank( "temp_week_ending" ) &&
                              !tranInfo.isFieldBlank( "requestor_name"   ) )
                          {
                            if( ( !tranInfo.isFieldBlank( "misc_expenses"   ) ) ||
                                ( !tranInfo.isFieldBlank( "reg_hrs_worked"  ) &&
                                  !tranInfo.isFieldBlank( "reg_hrs_rate"    ) ) ||
                                ( !tranInfo.isFieldBlank( "ot_hrs_worked"   ) &&
                                  !tranInfo.isFieldBlank( "ot_hrs_rate"     ) ) )
                            {
                              addendReq = "C6";
                              status = 1;       // met "C6" for Employment Agency  (do we want three different emp agency flags?)
                            }
                          }
                        }
                        else
                        {
//@                          if( !tranInfo.isFieldBlank( "product_code"          ) &&
//@                              !tranInfo.isFieldBlank( "item_description"      ) &&
//@                              !tranInfo.isFieldBlank( "item_quantity"         ) &&
//@                              !tranInfo.isFieldBlank( "item_unit_of_measure"  ) &&
//@                              !tranInfo.isFieldBlank( "extended_item_amt"     ) &&
//@                              !tranInfo.isFieldBlank( "debit_credit_indicator") )
                          if ( "Y".equals(tranInfo.getData("level_iii_data_present")) )
                          {
                            addendReq = "C3";
                            status = 1;       // met "C3" for general commercial
                          }
                          else if( "Y".equals(tranInfo.getData("enh_level_iii_data_present")) )
                          {
                            enhRequired = tranInfo.setEnhIndL3(enhRequired);    // indicate that the ird needs enh level 3
                            addendReq = "C3";
                            status = 1;       // met "C3" for general commercial
                          }
                          if( testDataIsInList( 4, tran_sic_code, strSic_ShippingCourier ) )
                          {
                            if( !tranInfo.isFieldBlank( "tax_amount"                ) &&
                                !tranInfo.isFieldBlank( "service_descriptor_code"   ) &&
                                !tranInfo.isFieldBlank( "tracking_number"           ) &&
                                !tranInfo.isFieldBlank( "shipping_net_amt"          ) &&
                                !tranInfo.isFieldBlank( "pickup_date"               ) &&
                                !tranInfo.isFieldBlank( "package_count"             ) &&
                                !tranInfo.isFieldBlank( "package_weight"            ) &&
                                !tranInfo.isFieldBlank( "unit_of_measure"           ) &&
                                !tranInfo.isFieldBlank( "shipping_party_info"       ) &&
                                !tranInfo.isFieldBlank( "shipping_party_addr"       ) &&
                                !tranInfo.isFieldBlank( "shipping_party_postal"     ) &&
                                !tranInfo.isFieldBlank( "shipping_party_contact"    ) &&
                                !tranInfo.isFieldBlank( "delivery_party_info"       ) &&
                                !tranInfo.isFieldBlank( "delivery_party_addr"       ) &&
                                !tranInfo.isFieldBlank( "delivery_party_postal"     ) &&
                                !tranInfo.isFieldBlank( "delivery_party_contact"    ) )
                            {
                              if( status == 1 )
                              {
                                addendReq = "C5";
                                status = 1;       // met "C5" for general commercial + shipping/courier
                              }
                              else
                              {
                                addendReq = "C4";
                                status = 1;       // met "C4" for shipping/courier
                              }
                            }
                          }
                        }
                        break;
                    }
                    break;

                  default:
                    status = 2;         // report error
                    break;
                }
              }

              if( status == 2 ) System.out.println("tranQualifies(): bad addendum requirement string...'" + addendReq + "'");
              if( status != 1 ) DisqualList.add("Tran failed addendum requirement test: '" + addendReq + "'");
              else              addendReqsPassed += addendReq;
            } // for()
          } // if( addendReqsList != null )

          thisRuleSet.setAddendumRequired( addendReqsPassed );    // will be either "" or non-empty string
        } // if( DisqualList.size() == 0 )

        if( DisqualList.size() == 0 ) {
		retVal = enhRequired;
	}
      }
      catch( Exception e )
      {
        String recInfo = ((tranInfo == null) ? "null" : (tranInfo.getString("batch_id") + "," + tranInfo.getString("batch_record_id")));
        SyncLog.LogEntry("com.mes.settlement.InterchangeMasterCard::tranQualifies(" + recInfo + ")",e.toString());
	log.error("Error.", e);
      }
      
      return( retVal );
    }

    public boolean  tranMeetsDateTimeliness( SettlementRecord tranInfo, boolean bIsIccNotGcms, int numDays, String excludedDays )
      throws Exception
    {
      boolean     retVal    = false;
      
      if( tranInfo.getDate("auth_date"       ) != null &&
          tranInfo.getDate("transaction_date") != null )
      {           
        Calendar calAuth  = Calendar.getInstance();
        Calendar calTran  = Calendar.getInstance();
        Calendar calOne   = Calendar.getInstance();
        Calendar calTwo   = Calendar.getInstance();

        calAuth.clear();
        calTran.clear();
        calOne.clear();
        calTwo.clear();

        calAuth.setTime( tranInfo.getDate("auth_date"       ) );
        calTran.setTime( tranInfo.getDate("transaction_date") );

        // MasterCard assumes the tran date is St. Louis time, and says the rules are....
        //    if tran date before auth date, use tran date
        //    if tran date equal  auth date, use tran date
        //    if tran date equal  auth date + 1
        //        if auth time 21:00 to 23:59, use tran date
        //        if auth time 00:00 to 20:59, use auth date
        //    if tran date after   auth + 1, use auth date
        // which can be restated as...
        //    if "auth date/time plus three hours" < "tran date", then use auth date
        //    else use tran date
        // BUT if we store an "auth date" with the tran which is three hours less than the one in the auth log, we can simplify with...
        //    if auth date before tran date, use auth date
        //    else use tran date
        if( bIsIccNotGcms && calAuth.before(calTran) )
        {
          calOne = calAuth;
        }
        else
        {
          calOne = calTran;
        }
        calTwo.setTime( CentralProcessingDate );

        if( bIsIccNotGcms )numDays -= 1;    // assume the icc timeliness test will happen one day later than this ic assignment

        // excludedDays.charAt(3) = header processing date
        // Note: not in use; perhaps would ++calOne or --calTwo before getNonSettlementDayCount()
      //      if( excludedDays.charAt(3) == 'Y' )numDays += ???;                                                                  // header processing date

        // excludedDays.charAt(2) = transaction date
        // Note: in calOne/calTwo comparison, adding one to numDays has the same effect as adding one to calOne, but if excluding tran date
        //   must ++calOne before getNonSettlementDayCount() so the tran date won't be double excluded if it is also a Sunday/Holiday
              if( excludedDays.charAt(2) == 'Y' )calOne.add(Calendar.DAY_OF_MONTH, +1);                                           // transaction date

        // excludedDays.charAt(0) = non-processing days (Sundays); excludedDays.charAt(1) = holidays
              if( excludedDays.startsWith("YN") )numDays += SettlementDb.getNonSettlementDayCount(tranInfo.RecSettleRecType,calOne,calTwo,1);  // Sundays
        else  if( excludedDays.startsWith("YY") )numDays += SettlementDb.getNonSettlementDayCount(tranInfo.RecSettleRecType,calOne,calTwo,2);  // Sundays + holidays
        else  if( excludedDays.startsWith("NY") )numDays += SettlementDb.getNonSettlementDayCount(tranInfo.RecSettleRecType,calOne,calTwo,3);  // US holidays

        // calOne         = comparison date (either the auth date or the transaction date, plus one if excluding tran/auth date)
        // calTwo         = Central Processing Date (CPD) -- expected file header date
        // numDays        = allowed days + excluded days
        // calTwo-numDays = the day before the Last Valid Transaction Date (LVTD)
        // retVal         = is calOne (auth/tran date) after calTwo (day before LVTD)
        //                = is auth/tran date on or after LVTD
        calTwo.add(Calendar.DAY_OF_MONTH, -numDays);
        retVal = calOne.after(calTwo);
      }
      return( retVal );
    }
  }


  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  // constructor
  public InterchangeMasterCard()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  public boolean getBestClass( SettlementRecord tranInfo, List listOfRuleSets )
  {
    boolean       retVal          = false;
    try
    {
      DisqualMap.clear();   // dump the disqual map


      IcInfo  icInfo      = null;
      String  irfInfoList = null;

      Map<String, String> cardTypeMap = new HashMap<>();

      String vs = "VS", mc = "MC", ds = "DS";
      cardTypeMap.put("VS", vs);
      cardTypeMap.put("VB", vs);
      cardTypeMap.put("VD", vs);
      cardTypeMap.put("MC", mc);
      cardTypeMap.put("MB", mc);
      cardTypeMap.put("MD", mc);
      cardTypeMap.put("DC", ds);
      cardTypeMap.put("DS", ds);
      cardTypeMap.put("JC", ds);

      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
      Date batchDate = tranInfo.getDate("batch_date");
      String cardType = cardTypeMap.get(tranInfo.getString("card_type"));
      String fundingCurrencyCode = tranInfo.getData("funding_currency_code");
      String originalCurrencyCode = tranInfo.getData("currency_code");
      double tranAmount = tranInfo.getDouble("transaction_amount");

      String txLog = "[getBestClass()] - Getting FX rate for:\n";
      txLog += "Batch date: " + df.format(batchDate) + "\n";
      txLog += "Card type: " + cardType + "\n";
      txLog += "originalCurrencyCode: " + originalCurrencyCode + "\n";
      txLog += "fundingCurrencyCode: " + fundingCurrencyCode;
      log.debug(txLog);

      double buyRate = 0;
      double sellRate = 0;

      try {
	      DccRateUtil rateUtil  = DccRateUtil.getInstance(batchDate);

	      buyRate = rateUtil.getBuyRate(cardType, fundingCurrencyCode, originalCurrencyCode);
	      sellRate = rateUtil.getSellRate(cardType, fundingCurrencyCode, originalCurrencyCode);
      } catch(Exception e) {
	      log.warn("[getBestClass()] - Error getting FX rate.", e);
      }

      if ( buyRate == 0 || sellRate == 0 )
      {
	      // See: com.mes.startup.DailyDetailFileProcessEvent.
	      // The same condition is checked and considered "missing rate data".
	      buyRate = 1.0;
	      sellRate = 1.0;
      }

      double fxRate = "C".equals(tranInfo.getString("debit_credit_indicator")) ? buyRate : sellRate;
      tranInfo.setData("fx_rate", fxRate);

      BigDecimal originalTranAmount = BigDecimal.valueOf(tranAmount).setScale(2, RoundingMode.HALF_EVEN);
      BigDecimal fundingTranAmount = BigDecimal.valueOf(tranAmount * fxRate).setScale(2, RoundingMode.HALF_EVEN);

      log.debug("Using FX rate: " + fxRate + ". Transaction amount conversion: " + originalTranAmount.toString() + " -> " + fundingTranAmount.toString());

      if( listOfRuleSets != null && listOfRuleSets.size() > 0 )
      {
        IC_BaseClass  interchangeClass  = getInterchangeClass( "IC_MasterCard" );

        String strEtc = "-"; // separator  = '-'
	try {
		strEtc += tranInfo.getString("bsa_type").substring(0,1); // bsa type   = 1,2,3,4,8
		strEtc += tranInfo.getString("bsa_id_code").substring(0,6); // bsa id code= 6 chars
		strEtc += tranInfo.getString("card_type").substring(1,2);   // card type  = 'B'usiness or 'C'redit or 'D'ebit
	} catch(Exception e) {
		log.warn("Warning.", e);
	}

        for( int i = 0; i < listOfRuleSets.size(); ++i )
        {
          IcProgramRulesMC    thisRuleSet  = (IcProgramRulesMC)listOfRuleSets.get(i);

          String  enhRequired = ((IC_MasterCard)interchangeClass).tranQualifies( tranInfo, thisRuleSet );
          if( enhRequired != null && enhRequired.length() == SettlementRecord.NUM_ENHANCED_IND )
          {
            String  irfInfoThisClass = enhRequired + thisRuleSet.getIrd() + strEtc + thisRuleSet.getAddendumRequired();

            if( irfInfoList == null ) { irfInfoList =                     irfInfoThisClass; }
            else                      { irfInfoList = irfInfoList + "," + irfInfoThisClass; }
          }
          else
          {
            DisqualMap.put( thisRuleSet.getIrd(), new Vector( interchangeClass.getDisqualList() ) );
          }
        }
      }

      if( irfInfoList == null ) { irfInfoList = IRF_INFO_UNKNOWN; }
      else                      { retVal = true;                  }

      //  have list of info for all qualifying classes; need to pick the best one
      getBestIcInfoClause = " and icd.debit_type is null";
      icInfo = getBestIrfInfoFromInfoList( tranInfo, irfInfoList );
      
      int debitType = tranInfo.getInt("debit_type");
      if( debitType != 0 )  //  && "USA,PRI,GUM,ASM,VIR,MNP".indexOf(getData("region_merchant")) >= 0 ) // US_AND_US_TERRITORIES; perhaps use table 0128??
      {
        //  check again, but only allow the regulated row(s) for this debit_type
        IcInfo  wouldHaveGotten = icInfo;
        String  whgIrfInfo      = wouldHaveGotten.getIcString();
        getBestIcInfoClause = " and nvl(icd.debit_type,0) = " + debitType;

        icInfo = getBestIrfInfoFromInfoList( tranInfo, irfInfoList );

        if( "---".equals(icInfo.getIcCode()) )    // if no regulated found, revert to "would have gotten"
        {
          icInfo = wouldHaveGotten;
        }
        else                                      // if regulated found, use some regulated and some "would have gotten" info
        {
          // get whg enh/addendum(s) and the regulated ird; store the whg enh/addendum(s) and the regulated ird
          String regIrd     = icInfo.getIcString().substring( LOC_IRD, LOC_IRD + 2 );
          icInfo.setIcString( whgIrfInfo.substring( 0,LOC_IRD ) + regIrd + whgIrfInfo.substring( LOC_IRD + 2 ) );

          // check for "would have gotten" lookup ic_code (if there isn't one, then ic_billing will be regulated ic_cat)
          String  regIc   = wouldHaveGotten.getIcCodeRegulated();
          if( regIc != null && regIc.length() == 4 )
          {
            icInfo.setIcCodeBilling(debitType + regIc);
          }
        }
      }

      String irfInfo = icInfo.getIcString();
      tranInfo.setData( "ird"                 , irfInfo.substring( LOC_IRD, LOC_IRD + 2 ) );
      tranInfo.setData( "addendum_required"   , irfInfo.substring( LOC_ADDENDUM         ) );
      tranInfo.setData( "ic_cat"              , icInfo.getIcCode()                        );
      tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling()                 );

      tranInfo.saveEnhDataAfterIcAssignment( irfInfo );
      tranInfo.fixBadDataAfterIcAssignment();

      log.info("Using ird: " + tranInfo.getString("ird") + ", ic_cat: " + tranInfo.getString("ic_cat") + ", ic_cat_billing: " + tranInfo.getString("ic_cat_billing"));
    }
    catch( Exception e )
    {
	    log.error("Error.", e);
	    SyncLog.LogEntry("com.mes.settlement.InterchangeMasterCard.getBestClass()", e.toString());
    }
    return( retVal );
  }
}
