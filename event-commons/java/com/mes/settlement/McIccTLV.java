/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/settlement/McIccTLV.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-06-26 18:55:51 -0700 (Wed, 26 Jun 2013) $
  Version            : $Revision: 21248 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

public class McIccTLV
{
  public  String        Tag   = null;
  public  String        Len   = null;
  public  String        Val   = null;
  
  public McIccTLV( String tag, String len, String val )
  {
    Tag   = tag;
    Len   = len;
    Val   = val;
  }
  
  public String getTag()              { return( Tag ); }
  public void   setTag( String tag )  { Tag = tag;      }
  
  public String getLen()              { return( Len ); }
  public void   setLen( String len )  { Len = len;      }
  
  public String getVal()              { return( Val );  }
  public void   setVal( String val )  { Val = val;      }
}
