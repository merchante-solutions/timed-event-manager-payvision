/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/ArdefLoader.java $

  Description:

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-10-16 14:19:21 -0700 (Thu, 16 Oct 2014) $
  Version            : $Revision: 23192 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Date;
import com.mes.constants.MesFlatFiles;
import com.mes.database.OracleConnectionPool;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import com.mes.tools.FileUtils;

public class ArdefLoader
{
	
  public void loadArdefFile( String inputFilename ) throws Exception
  {
    String          ct              = "XX";
    SettlementDb    db              = null;
    Date            effectiveDate   = null;
    FlatFileRecord  ffd             = null;
    long            loadFileId      = 0L;
    String          loadFilename    = null;
    int             recordLength    = 0;
    int             recCount        = 0;
    String          tableName       = null;
    int             inputFileRecCount = 0;

    try
    {
      BufferedReader in = new BufferedReader( new FileReader(inputFilename) );
      String line = null;
      db = new SettlementDb();
      db.connect(true);
      db.setAutoCommit(false);

      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      loadFileId    = db._loadFilenameToLoadFileId(loadFilename);

      if ( loadFilename.indexOf("mc_ardef") >= 0 )
      {
        System.out.println("Processing MasterCard ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MC_ARDEF);
        recordLength = 277;
        tableName = "mc_ardef_table";
        ct = "MC";
      }
      else if ( loadFilename.indexOf("vs_ardef") >= 0 )
      {
        System.out.println("Processing Visa ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_ARDEF);
        recordLength = 140;
        tableName = "visa_ardef_table";
        ct = "VS";
      }
      else if ( loadFilename.indexOf("vs_reg_ard") >= 0 )
      {
        System.out.println("Processing Visa Regulated ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_REGARDEF);
        recordLength = 140;
        tableName = "visa_ardef_table_regulated";
        ct = "VS_REG";
      }
      else if ( loadFilename.indexOf("ds_ardef") >= 0 )
      {
        System.out.println("Processing Discover ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_ARDEF);
        recordLength = 80;
        tableName = "discover_ardef_table";
        ct = "DS";
      }
      else if ( loadFilename.indexOf("b2_ardef") >= 0 )
      {
        System.out.println("Processing Modified Base II ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MODBII_ARDEF);
        recordLength = 188;
        tableName = "modbii_ardef_table";
        ct = "MODBII";
      }
      else if ( loadFilename.indexOf("amex_bin") >= 0 )
      {
        System.out.println("Processing Amex OptBlue ARDEF File - " + loadFilename);
        ffd  = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_ARDEF);
        recordLength = 200;
        tableName = "amex_ardef_optblue_table";
        ct = "AMEX";
      }
      else
      {
        throw( new Exception("Invalid file type " + loadFilename) );
      }

      while( (line = in.readLine()) != null )
      {
        if ( line.length() < recordLength )
        {
          line = StringUtilities.leftJustify(line,recordLength,' ');  // pad with trailing spaces
        }
        if ( "MC".equals(ct) )
        {
          ArdefDataMC data = db._parseArdefDataMC(line,ffd);
          data.setLoadFilename(loadFilename);
          data.setLoadFileId(loadFileId);

          // only insert the Debit MC (DMC) and MasterCard Credit (MCC) and Maestro (MSI)
          // entries into the ARDEF table.  this limits the data in the
          // ARDEF table to card programs that we participate in.  the
          // full table 40 data is still available in MC_EP_TABLE_DATA.
          String cpid = data.getCardProgramId();
          if ( ("DMC".equals(cpid) || "MCC".equals(cpid) || "MSI".equals(cpid)) && data.isActive() )
          {
            db._storeArdefMC(data);
          }
        }
        else if ( "VS".equals(ct) )
        {
          ArdefDataVisa data = db._parseArdefDataVisa(line,ffd);
          data.setLoadFilename(loadFilename);
          data.setLoadFileId(loadFileId);
          db._storeArdefVisa(data);
        }
        else if ( "VS_REG".equals(ct) )
        {
          RegArdefDataVisa data = db._parseRegArdefDataVisa(line,ffd);
          data.setLoadFilename(loadFilename);
          data.setLoadFileId(loadFileId);
          db._storeRegArdefVisa(data);
        }
        else if ( "DS".equals(ct) )
        {
          if ( line.startsWith("HDR") )
          {
            java.util.Date    javaDate = DateTimeFormatter.parseDate(line.substring(42,54),"yyyyMMddHHmm");
            if ( javaDate == null )
            {
              javaDate = db._getFileDate(loadFilename);
            }
            effectiveDate = new java.sql.Date(javaDate.getTime());
          }
          else if ( line.startsWith("RCD") )
          {
            ArdefDataDiscover data = db._parseArdefDataDiscover(line,ffd);
            data.setLoadFilename(loadFilename);
            data.setLoadFileId(loadFileId);
            data.setEffectiveDate(effectiveDate);
            db._storeArdefDiscover(data);
          }
        }
        else if ( "MODBII".equals(ct) )
        {
          if (   line.startsWith("TBESBAND")
              || line.startsWith(" CDBAND ")
              || line.startsWith("--------") )
          {
            continue;   // skip header information
          }
          ArdefDataModBII data = db._parseArdefDataModBII(line,ffd);
          data.setLoadFilename(loadFilename);
          data.setLoadFileId(loadFileId);
          db._storeArdefModBII(data);
        }
        else if ( "AMEX".equals(ct) )
        {
          //Get effective date from Header record
          if ( line.startsWith("DFHDR") )
          {
        	++inputFileRecCount;
            java.util.Date    javaDate = DateTimeFormatter.parseDate(line.substring(5,13),"MMddyyyy");
            if ( javaDate == null )
            {
              javaDate = db._getFileDate(loadFilename);
            }
            effectiveDate = new java.sql.Date(javaDate.getTime());
          } 
          else if (line.startsWith("DFTRL")) //ignore trailer
          {
         	  ++inputFileRecCount;
        	 /* int trailerRecCount =  Integer.parseInt(line.substring(83,90));
        	  if (trailerRecCount != inputFileRecCount){
        		  System.out.println("Invalid count  - Expected count = " + trailerRecCount + " Actual Count=" + inputFileRecCount);
        	  }*/        	  
          }
          else  //Store the detail record
          {
            AmexOptBlueBinRange data = db._parseArdefDataAmexOptBlue(line,ffd);
            data.setLoadFileName(loadFilename);
            data.setLoadFileId(loadFileId);
            data.setEffectiveDate(effectiveDate);
            data.setEnabled("N");
            db._storeArdefAmexOptBlue(data);
          }
        }
        if ( (recCount % 250) == 0 ) { db.commit(); }
      }
      System.out.println();
      in.close();
      db.commit();

      // toggle the enabled flag
      db._toggleArdefData(tableName);
      db.commit();

      // delete the old data
      db._clearArdefData(tableName,false);
      db.commit();
    }
    catch (FileNotFoundException fEx){
    	System.out.println("Input File Not found " + inputFilename);
    	throw fEx;
    }
    catch( Exception e )
    {
      System.out.println();
      System.out.println("Failed - " + e.toString());
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try{ffd.cleanUp();}catch(Exception ee){}
      try{db.cleanUp();}catch(Exception ee){}
    }
  }

  public static void main( String[] args )
  {
    ArdefLoader     loader      = null;

    try
    {
      loader = new ArdefLoader();
      loader.loadArdefFile(args[0]);
    }
    catch( Exception e )
    {
      System.out.println();
      System.out.println("Failed - " + e.toString());
      e.printStackTrace();
    }
    finally
    {
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}