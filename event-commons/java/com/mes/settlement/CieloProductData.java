/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/settlement/CieloProductData.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-11-08 08:32:15 -0800 (Fri, 08 Nov 2013) $
  Version            : $Revision: 21747 $

  Change History:
     See SVN database

  Copyright (C) 2000-2013,2014 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.ResultSet;

public class CieloProductData
{
  private   int             CieloProduct        = -1;
  private   int             InstallmentMin      = 1;
  private   int             InstallmentMax      = 1;
  
  public CieloProductData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    CieloProduct    = resultSet.getInt("cielo_product");
    setInstallmentRange(resultSet);
  }

  public boolean acceptsProduct( int installmentCount )
  {
    boolean   retVal    = false;
    
    if ( installmentCount >= 50 )   // emissor are accepted if product exists
    {
      retVal = true;
    }
    else if ( installmentCount >= InstallmentMin && installmentCount <= InstallmentMax )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public void setInstallmentRange( ResultSet resultSet )
    throws java.sql.SQLException
  {
    if ( InstallmentMin > resultSet.getInt("installment_min") )
    {
      InstallmentMin  = resultSet.getInt("installment_min");
    }
    
    if ( InstallmentMax < resultSet.getInt("installment_max") )
    {
      InstallmentMax  = resultSet.getInt("installment_max");
    }
  }
}