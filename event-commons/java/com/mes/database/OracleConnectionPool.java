/*@lineinfo:filename=OracleConnectionPool*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/OracleConnectionPool.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesSystem;
import com.mes.support.PropertiesFile;
import com.mes.support.SMTPMail;
import oracle.sqlj.runtime.Oracle;
import sqlj.runtime.ref.DefaultContext;

public class OracleConnectionPool
  implements Runnable
{
  static Logger log = Logger.getLogger(OracleConnectionPool.class);
  
  public static final int     POOL_INCREMENT_DEFAULT        = 5;
  public static final int     POOL_SIZE_MIN                 = 5;
  public static final int     POOL_SIZE_MAX                 = 100;
  public static final int     POOL_SHRINK_NEVER             = 0;
  
  private static final int    ST_CON_UNLOCKED               = 0;
  private static final int    ST_CON_LOCKED                 = 1;
  private static final int    ST_CON_PENDING_REFRESH        = 2;

  private static final int    DEFAULT_USE_COUNT_THRESHOLD   = 1000;     // max number of uses of a connection before reset
  private static final int    REFRESH_SIGNAL_TIMEOUT        = 1000;     // watch dog timer for the pool cleaner thread
  private static final long   DEAD_CONNECTION_LOOP_TIME     = 60000L;   // time in ms before a big brother goes through the connection list
  private static final long   DEAD_CONNECTION_TIME          = 120000L;  // time in ms before a connection is considered dead
  private static final long   TERMINATE_CONNECTION_TIME     = 300000L;  // time in ms before a connection is killed
                                                              // 180000 = 3 minutes
                                                              
  private static final int    TOTAL_CONNECTS                = 0;
  private static final int    TOTAL_RELEASES                = 1;
  private static final int    TOTAL_RESETS                  = 2;
  private static final int    TOTAL_BB_KILLS                = 3;
  private static final int    TOTAL_COUNT                   = 4;
                                                              
  private static final int    ST_IDLE                       = 0;                                                              
  private static final int    ST_WAIT                       = 1;
  private static final int    ST_REFRESH_ENTRY              = 2;
  private static final int    ST_POP                        = 3;
  private static final int    ST_REMOVE                     = 4;
  private static final int    ST_RESET                      = 5;
  private static final int    ST_UNLOCK                     = 6;
  private static final int    ST_BB_POP                     = 7;
  private static final int    ST_BB_RESET                   = 8;
  private static final int    ST_BB_UNLOCK                  = 9;
  private static final int    ST_BB_TERMINATE_SESSION       = 10;
  private static final int    ST_BB_TEST_CONNECTION         = 11;
  private static final int    ST_BB_GET_SERIAL_NUMBER       = 12;
  private static final int    ST_BB_CHECK_STATUS            = 13;
  private static final int    ST_LOOP                       = 14;
  
  // Maximum length of time a thread waits for connection before returning null
  // see getFreeConnection()
  public static final long    TIMEOUT_MAX                   = 10000L; // 10 seconds

  private   static Integer              PoolLock          = 1;
  private   static OracleConnectionPool TheConnectionPool = null;
  
  private   DefaultContext  AdminCtx                    = null;
  private   int             AdminSessionId              = 0;
  private   boolean         AutoCommit                  = true;
  private   int             CloseThreadCount            = 0;
  private   Vector          Connections                 = new Vector();
  private   Vector          ConnectionRefreshList       = new Vector();
  private   Date            CreationTime                = null;
  private   long            CreationTS                  = System.currentTimeMillis();
  private   Properties      DbProps                     = null;
  private   boolean         DebugOutput                 = false;
  private   boolean         FastModeEnabled             = false;
  private   boolean         Initialized                 = false;
  private   String          JdbcConnectionString        = null;
  private   String          JdbcDriverClassName         = null;
  private   boolean         Logging                     = true;
  private   boolean         PoolCleanerRunning          = true;
  private   int             PoolCleanerHeartbeat        = 0;
  private   int             PoolSizeIncrement           = 1;
  private   int             PoolSizeMax                 = 10;
  private   int             PoolSizeMin                 = 1;
  private   int             PoolShrinkSecs              = POOL_SHRINK_NEVER;
  private   Thread          PoolCleaner                 = null;
  private   int             PoolCleanerState            = ST_IDLE;
  private   int             UseCountThreshold           = DEFAULT_USE_COUNT_THRESHOLD;
  private   int             AdminUseCount               = 0;
  private   int[]           Totals                      = new int[TOTAL_COUNT];
  private   boolean         Tracing                     = false;
  private   long            lastDeadConnectionAudit     = 0L;
  
  private class CleanerThread implements Runnable
  {
    private Thread                CleanupThread   = null;
    private PoolConnection        PoolCon         = null;
    
    public CleanerThread( PoolConnection poolCon )
    {
      CleanupThread  = new Thread(this);  // initialize thread
      PoolCon = poolCon;                  // store the context
      CleanupThread.start();              // start the thread
      adjustCloseThreadCount(1);
    }
    
    public void run( )
    {
      try
      {
        trace( "CleanerThread: Calling DefaultContext::close()" );
        
        PoolCon.close();
        PoolCon.openConnection();
        PoolCon.unlock();
        
        trace( "CleanerThread: DefaultContext::close() complete" );
      }
      catch( Exception e )
      {
        // ignore all exceptions
        logEntry("CleanerThread::run():  " + e.toString());
      }
      finally
      {
        adjustCloseThreadCount(-1);
        
        // please garbage collect me
        PoolCon         = null;
        CleanupThread   = null;
      }
    }
  }
  
  private class CloseThread implements Runnable
  {
    private Thread                CloserThread    = null;
    private DefaultContext        CtxToClose      = null;
    
    public CloseThread( DefaultContext ctx )
    {
      CloserThread  = new Thread(this); // initialize thread
      CtxToClose    = ctx;              // store the context
      CloserThread.start();             // start the thread
      adjustCloseThreadCount(1);
    }
    
    public void run( )
    {
      try
      {
        trace( "CloseThread: Calling DefaultContext::close()" );
        
        CtxToClose.close();
        
        trace( "CloseThread: DefaultContext::close() complete" );
      }
      catch( Exception e )
      {
        // ignore all exceptions
        logEntry("CloseThread::run():  " + e.toString());
      }
      finally
      {
        adjustCloseThreadCount(-1);
        
        // please garbage collect me
        CtxToClose    = null;
        CloserThread  = null;
      }
    }
  }
  
  public class PoolConnection
    implements Comparable
  {
    private DefaultContext      Ctx               = null;
    private int                 LockState         = ST_CON_UNLOCKED;
    private Object              LockedBy          = null;
    private String              LockedByUser      = null;
    private int                 LockCount         = 0;
    private long                LockTS            = 0L;
    private boolean             MarkedForReset    = false;
    private int                 UseCount          = 0;
    private int                 OracleSessionId   = 0;
    private Date                OpenTime          = null;
    private int                 ReleaseCount      = 0;
    private String              ReleaseStatus     = "";
    private boolean             TimeoutExempt     = false;
    
    public PoolConnection( Object whoLocked )
    {
      openConnection();
      if ( whoLocked != null )
      {
        lock(whoLocked);
      }
    }
    
    public synchronized void close( )
    {
      try
      {
        Ctx.close();
        trace("Pool Connection " + this + " closed");
      }
      catch( Exception e )
      {
        logEntry("PoolConnection::close():  " + e.toString());
      }        
    }
    
    public int compareTo( Object object )
    {
      PoolConnection        entry   = (PoolConnection)object;
      int                   retVal  = 0;
      
      if ( ( retVal = (int)(entry.getLockTime() - getLockTime()) ) == 0 )
      {
        retVal = this.toString().compareTo( entry.toString() );
      }
      
      // sort in descending order based on lock time
      return( retVal );
    }
    
    public DefaultContext getContext()
    {
      return(Ctx);
    }
    
    public Connection getConnection()
    {
      return(Ctx.getConnection());
    }
    
    public int getLockCount()
    {
      return( LockCount );
    }
    
    public long getLockTime()
    {
      long      retVal = 0L;
      if ( isLocked() )
      {
        retVal = (System.currentTimeMillis() - LockTS);
      }
      return( retVal );
    }
    
    public String getLockedByLoginName()
    {
      return( MesSystem.getRequestLoginName(LockedBy) );
    }
    
    public String getLockedByName()
    {
      String        retVal    = "Not Available";
      
      try
      {
        if ( LockState == ST_CON_UNLOCKED )
        {
          retVal = "Not Locked";
        }
        else if ( LockState == ST_CON_PENDING_REFRESH )
        {
          retVal = "Pending Refresh";
        }
        else
        {
          retVal = LockedBy.getClass().getName();
        }          
      }
      catch( Exception e )
      {
        logEntry("PoolConnection::getLockedByName() synchronization error: " + e.toString());
      }
      return( retVal );
    }
    
    public int getUseCount()
    {
      return( UseCount );
    }
  
    public Date getOpenTime()
    {
      return( OpenTime );
    }
    
    public int getOracleSessionId()
    {
      return(OracleSessionId);
    }
    
    public int getReleaseCount()
    {
      return( ReleaseCount );
    }
    
    public String getReleaseStatus()
    {
      return( ReleaseStatus );
    }
    
    public boolean isLocked()
    {
      return( LockState != ST_CON_UNLOCKED );
    }
    
    public boolean isMarkedForReset()
    {
      return(MarkedForReset);
    }
    
    public boolean isTimeoutExempt()
    {
      return( TimeoutExempt );
    }
    
    public boolean lock( )
    {
      return( lock(null) );
    }
    
		public synchronized boolean lock(Object whoLocked) {
			if (log.isTraceEnabled()) {
				log.trace("[lock( Object whoLocked )] - lock connection OracleSessionId=" + OracleSessionId);
			}
			boolean retVal = false;
			if (LockState == ST_CON_UNLOCKED) {
				LockState = ST_CON_LOCKED;
				LockedBy = whoLocked;
				LockTS = System.currentTimeMillis();
				retVal = true;

				// do not refresh the connection here when in
				// fast mode (e.g. the API connection pool)
				if (!FastModeEnabled) {
					refreshConnection();
				}

				++LockCount;
				Totals[TOTAL_CONNECTS]++;
			}
			if (log.isTraceEnabled()) {
				log.trace("[lock()] - connection OracleSessionId=" + OracleSessionId + " locked=" + retVal+", LockCount="+LockCount);
			}
			return (retVal);
		}
    
    public void markForReset()
    {
      MarkedForReset = true;
    }
    
    private void openConnection( )
    {
			if (log.isTraceEnabled()) {
				log.trace("[openConnection()] - attempting to open and validate a connection");
			}
      try
      {
        long base = System.currentTimeMillis();
        Ctx = Oracle.getConnection( JdbcConnectionString, DbProps, AutoCommit );
        /*@lineinfo:generated-code*//*@lineinfo:407^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct(sid) 
//            from    v$mystat 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct(sid)  \n          from    v$mystat";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.database.OracleConnectionPool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   OracleSessionId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
						if (log.isTraceEnabled()) {
							log.trace("[openConnection()] - retrieved OracleSessionId=" + OracleSessionId);
						}
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^9*/
        OpenTime = Calendar.getInstance().getTime();
      }
      catch( java.sql.SQLException e )
      {
    	  log.error("[openConnection()] - Unexpected exception opening connection",e);
        logEntry("openConnection():  " + e.toString());
      }
    }        
    
		public void release(Object releaseObj) {
			if (log.isTraceEnabled()) {
				log.trace("[release()] - ");
			}

			int cursorCount = 0;
			int cursorCountReal = 0;

			ReleaseStatus = "entry";

			// only allow the connection pool and the original lock
			// object to release this connection. this prevents
			// threads that have had their connection closed by
			// the connection pool from closing another threads
			// connection.
			if ((releaseObj == OracleConnectionPool.getInstance()) || (releaseObj == LockedBy)) {
				ReleaseStatus = "ownership verified";

				// synchronize the modification of the lock status
				synchronized (this) {
					ReleaseStatus = "synch block entry";
					if (LockState == ST_CON_LOCKED) {
						ReleaseStatus = "lock state verified";
						try {
							// unregister from big brother
							MesSystem.unregisterRequest(LockedBy);
							ReleaseStatus = "request unregistered";
							if (log.isTraceEnabled()) {
								log.trace("[release()] - OracleSessionId=" + OracleSessionId + ", UseCount=" + UseCount + ", UseCountThreshold="
										+ UseCountThreshold);
							}
							if (UseCount >= UseCountThreshold) {
								ReleaseStatus = "use count exception";

								// add this connection to the list to
								// be cleaned up by the pool cleaner
								synchronized (ConnectionRefreshList) {
									if (log.isTraceEnabled()) {
										log.trace("[release()] - adding released connection to cleanup pool OracleSessionId=" + OracleSessionId);
									}
									ReleaseStatus = "refresh list lock";
									ConnectionRefreshList.addElement(this);
									ReleaseStatus = "refresh list updated";
									if (log.isTraceEnabled()) {
										log.trace("[release()] - refreshed list updated, OracleSessionId=" + OracleSessionId);
									}
									ConnectionRefreshList.notify();
									ReleaseStatus = "refresh list notified";
									if (log.isTraceEnabled()) {
										log.trace("[release()] - refersh list notified, OracleSessionId=" + OracleSessionId);
									}
								}
								// update the state to pending refresh
								LockState = ST_CON_PENDING_REFRESH;
								ReleaseStatus = "lock state refresh";
								if (log.isTraceEnabled()) {
									log.trace("[release()] - ReleaseStatus="+ReleaseStatus+", OracleSessionId=" + OracleSessionId);
								}
							}
							else // don't need to reset, just unlock this
							{
								ReleaseStatus = "unlocking";
								// update use count
								setUseCount(++UseCount);
								ReleaseStatus = "use count set";
								unlock();
								if (log.isTraceEnabled()) {
									log.trace("[release()] - connection unlocked, OracleSessionId=" + OracleSessionId);
								}
								ReleaseStatus = "connection unlocked";
							}
						}
						catch (Exception e) {
							ReleaseStatus = (ReleaseStatus + " - " + e.toString());
							logEntry("release(): " + e.toString());
							log.error("[release()] - Exception Occured. ReleaseStatus="+ReleaseStatus,e);
						}
					}
				}
				if (log.isTraceEnabled()) {
					log.trace("[release()] - connection released, ReleaseStatus=" + ReleaseStatus);
				}
			}
			ReleaseStatus = "";
		}
    
		private void refreshConnection() {
			if (log.isTraceEnabled()) {
				log.trace("[refreshConnection()] - OracleSessionId=" + OracleSessionId);
			}
			// this one is got closed somehow, open a new one
			if (Ctx.isClosed() == true) {
				openConnection();
				trace("Pool Connection " + this + " refreshed");
				if (log.isTraceEnabled()) {
					log.trace("[refreshConnection()] - connection refreshed, OracleSessionId=" + OracleSessionId);
				}
			}
		}
    
		private synchronized void resetConnection() {
			if (log.isTraceEnabled()) {
				log.trace("[resetConnection()] - ");
			}
			// start a new thread to close the connection
			CloseThread closer = new CloseThread(Ctx);

			openConnection();

			// reset use count
			setUseCount(0);

			trace("Pool Connection " + this + " reset");
		}

    public synchronized void setLockedByUser( String loginName )
    {
      LockedByUser = loginName;
    }
    
    public synchronized void setUseCount( int useCount )
    {
      UseCount = useCount;
    }
    
    public synchronized void setTimeoutExempt( boolean timeoutExempt )
    {
      TimeoutExempt = timeoutExempt;
    }
    
    private synchronized void unlock( )
    {
    	if ( log.isTraceEnabled()) {
    		log.trace("[unlock()] - ");
    	}
      if ( isLocked() )
      {
        MarkedForReset = false;
        TimeoutExempt = false;
        LockedBy = null;
        LockState = ST_CON_UNLOCKED;
        
        ++ReleaseCount;
        
        trace("Pool Connection " + this + " unlocked");
        
        Totals[TOTAL_RELEASES]++;
      }        
    }
  }
  
  public OracleConnectionPool( )
  {
	  log.info("[OracleConnectionPool()] - Instantiating OracleConnectionPool");
    PropertiesFile    propsFile   = new PropertiesFile();
    
    // attempt to read the mes.properties file
    try
    {
      propsFile.load("mes.properties");
    }
    catch(Exception e)
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
      
      try
      {
        propsFile.load(inputStream);
        inputStream.close();
      }
      catch(Exception se)
      {
      }
      
    }
    
    initialize( SQLJConnectionBase.getPoolConnectionString(),
                propsFile.getInt("com.mes.PoolSizeMin",POOL_SIZE_MIN),
                propsFile.getInt("com.mes.PoolSizeMax",POOL_SIZE_MAX), 
                propsFile.getInt("com.mes.PoolIncSize",POOL_INCREMENT_DEFAULT),
                propsFile.getInt("com.mes.PoolShrinkSecs",POOL_SHRINK_NEVER),
                propsFile.getInt("com.mes.UseCountThreshold",DEFAULT_USE_COUNT_THRESHOLD) );
  }
  
  public OracleConnectionPool( String connectString )
  {
    PropertiesFile    propsFile   = new PropertiesFile();
    
    // attempt to read the mes.properties file
    try
    {
      propsFile.load("mes.properties");
    }
    catch(Exception e)
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
      
      try
      {
        propsFile.load(inputStream);
        inputStream.close();
      }
      catch(Exception se)
      {
      }
    }
  
    initialize( connectString, 
                propsFile.getInt("com.mes.PoolSizeMin",POOL_SIZE_MIN),
                propsFile.getInt("com.mes.PoolSizeMax",POOL_SIZE_MAX), 
                propsFile.getInt("com.mes.PoolIncSize",POOL_INCREMENT_DEFAULT),
                propsFile.getInt("com.mes.PoolShrinkSecs",POOL_SHRINK_NEVER),
                propsFile.getInt("com.mes.UseCountThreshold",DEFAULT_USE_COUNT_THRESHOLD) );
  }
  
  public OracleConnectionPool( String connectString, int min, int max, int incSize, int poolShrinkSecs )
  {
    initialize( connectString, min, max, incSize, poolShrinkSecs, DEFAULT_USE_COUNT_THRESHOLD);
  }
  
  private PoolConnection addConnection( Object whoLocked )
  {
	  if(log.isTraceEnabled()) {
		  log.trace("[addConnection()] - add connection to the pool");
	  }
    PoolConnection      poolCon   = null;
    
    trace("Adding connection pool connection...");
    poolCon = new PoolConnection(whoLocked);
    Connections.addElement(poolCon);
    return( poolCon );
  }
  
  private synchronized void adjustCloseThreadCount( int value )
  {
    CloseThreadCount += value;
  }
  
  public void alertAdmins( )
  {
	  if(log.isTraceEnabled()) {
		  log.trace("[alertAdmins()] - ");
	  }
    StringTokenizer     addrs           = null;
    long                delayMS         = 0L;
    Date                lastDate        = null;
    SMTPMail            mailer          = null;
    StringBuffer        message         = new StringBuffer("");
    PropertiesFile      propsFile       = new PropertiesFile();
    
    try
    {
      propsFile.load("mes.properties");
      
      lastDate = propsFile.getDate("last.admin.alert", "yyyyMMddhhmmss", null);
      delayMS  = (propsFile.getInt("pool.warning.delay", 60) * 1000);
      
      if ( ( lastDate == null ) ||
           ( Calendar.getInstance().getTime().getTime() > (lastDate.getTime() + delayMS) ) )
      {
    	  
    	// set the message
          message.append("The Connection Pool demand on server '");
          message.append( propsFile.getString("com.mes.hostname","Unknown") );
          message.append("' exceeded capacity at ");
          message.append( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yy HH:mm:ss") );
          message.append(".\n\n");
          message.append(MesDefaults.MESSAGE_CONNECTION_POOL_URL);
          message.append("\n\n");
        
          // add data about who may be killing the pool
          message.append("Bean(s) responsible for lock is:\n");
        
          TreeSet sortedSet = new TreeSet(getConnections());
        
          Iterator it = sortedSet.iterator();
        
          int idx = 0;
          while(it.hasNext())
          {
            PoolConnection poolCon = (PoolConnection)it.next();
            message.append("  ");
            message.append(poolCon.getLockedByName());
            message.append(" - ");
            message.append(poolCon.getLockedByLoginName());
            message.append("  (");
            message.append(Long.toString(poolCon.getLockTime() / 1000));
            message.append(" seconds)\n");
          
            if(++idx >= 5)
            {
              break;
            }
          }
          
          if(log.isTraceEnabled()) {
    		  log.trace("[alertAdmins()] - message to admins:\r\n"+message.toString());
    	  }
    	  
        // get email addresses
        addrs = new StringTokenizer(propsFile.getProperty("pool.admins"), ";");
      
        // create the email 
        mailer = new SMTPMail( propsFile.getString("email.host",null) );
      
        // set the from address
        mailer.setFrom( propsFile.getString("email.fromaddr",null) );
      
        // set the email addresses
        while(addrs.hasMoreTokens())
        {
          mailer.addRecipient( addrs.nextToken() );
        }
      
        // setup the data in the mailer
        mailer.setSubject( "Connection Pool Alert" );
        mailer.setText( message.toString() );
      
        // send the email
        mailer.send();
        
        propsFile.setProperty("last.admin.alert", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "yyyyMMddhhmmss") );
        propsFile.store("mes.properties");
      }        
    }
    catch(Exception e)
    {
      System.out.println("OracleConnectionPool.alertAdmins(): " + e.toString());
    }
  }
  
  public void cleanUp( )
  {
	  log.info("[cleanUp()] - Draining OracleConnectionPool");
    
    PoolCleanerRunning = false;
    synchronized( ConnectionRefreshList )
    {
      ConnectionRefreshList.notifyAll();
    }        
    emptyPool();
    TheConnectionPool = null;
  }
  
  private void emptyPool( )
  {
	  if(log.isTraceEnabled()) {
		  log.trace("[emptyPool()] - ");
	  }
    PoolConnection      poolCon   = null;
    
    for ( int i = 0; i < Connections.size(); ++i )
    {
      poolCon = (PoolConnection) Connections.elementAt(i);
      try
      {
        poolCon.close();
      }
      catch( Exception e )
      {
      }
    }
    Connections.removeAllElements();
  }
  //Note: This will get fixed/removed in TEM part II of elimination SQLJ.
  @SuppressWarnings({"deprecation"})
  protected void finalize()
    throws Throwable 
  {
    emptyPool();
    super.finalize();
  }
  
  public int getCloseThreadCount()
  {
    return( CloseThreadCount );
  }
  
  public int getConnectionCountGiven()
  {
    PoolConnection        poolCon = null;
    int                   retVal  = 0;
    
    for ( int i = 0; i < Connections.size(); ++i )
    {
      poolCon = (PoolConnection) Connections.elementAt(i);
      retVal += poolCon.getLockCount();
    }
    return( retVal );
  }
  
  public int getConnectionCountInUse()
  {
    PoolConnection        poolCon = null;
    int                   retVal  = 0;
    
    for ( int i = 0; i < Connections.size(); ++i )
    {
      poolCon = (PoolConnection) Connections.elementAt(i);
      if ( poolCon.isLocked() )
      {
        retVal++;
      }
    }
    return( retVal );
  }
  
  public int getConnectionCountReleased()
  {
    PoolConnection        poolCon = null;
    int                   retVal  = 0;
    
    for ( int i = 0; i < Connections.size(); ++i )
    {
      poolCon = (PoolConnection) Connections.elementAt(i);
      retVal += poolCon.getReleaseCount();
    }
    return( retVal );
  }
  
  public Vector getConnections( )
  {
    return(Connections);
  }
  
  public Date getCreationTime( )
  {
    return( CreationTime );
  }
  
  public static OracleConnectionPool getInstance()
  {
    return( getInstance(null) );
  }
  
	public static OracleConnectionPool getInstance(String connectString) {
		if (log.isTraceEnabled()) {
			log.trace("[getInstance()] - connectString-" + DbProperties.getLoggableConnection(connectString));
		}
		synchronized (PoolLock) {
			if (TheConnectionPool == null) {
				if (connectString == null) {
					TheConnectionPool = new OracleConnectionPool();
				}
				else {
					TheConnectionPool = new OracleConnectionPool(connectString);
				}
			}
		}
		return (TheConnectionPool);
	}

  public static void destroy()
  {
    System.out.println("Destroying OracleConnectionPool");
    synchronized( PoolLock )
    {
      if ( TheConnectionPool != null )
      {
        TheConnectionPool.cleanUp();
      }
    }
  }
  
  public PoolConnection getFreeConnection( Object whoLocked ) 
  {
    return( getFreeConnection( whoLocked, false ) );
  }
  
  public PoolConnection getFreeConnection( Object whoLocked, boolean timeoutExempt ) 
  {
	  if ( log.isTraceEnabled()) {
		  log.trace("[getFreeConnection()] - timeoutExempt="+timeoutExempt);
	  }
    PoolConnection      conIt   = null;   // connection iterator
    PoolConnection      poolCon = null;   // returned connection
    long                start   = System.currentTimeMillis();

    // update the requestor count
    // JRF Don't bother with this any more
    //Requester.updateRequester(whoLocked);
  
    while( ( poolCon == null ) &&
           ( (System.currentTimeMillis() - start) < TIMEOUT_MAX ) )
    {
      for ( int i = 0; i < Connections.size(); ++i )
      {
        conIt = (PoolConnection)Connections.elementAt(i);

        // test to see if the connection is locked
        // before calling synchronized lock method.
        if ( conIt.isLocked() == false )
        {
          if ( conIt.lock(whoLocked) == true )
          {
            poolCon = conIt;
            if ( log.isTraceEnabled()) {
      		  	log.trace("[getFreeConnection()] - returning pooled connection");
      	  	}
            break;    // exit for loop
          }
        }          
      }
      if ( poolCon == null )
      {
        if ( Connections.size() < PoolSizeMax )
        {
          // always add at least one, locked connection
          // for the current requester to have.
          poolCon = addConnection(whoLocked);
          
          // add the remaining (if any) to complete
          // the increment size.
          for( int i = 1; i < PoolSizeIncrement; ++i )
          {
            addConnection(null);   // these are not locked
          }
          	if ( log.isTraceEnabled()) {
    		  	log.trace("[getFreeConnection()] - created new connection and adding to pool, new poolsize="+this.Connections.size());
    	  	}
        }
        else if ( FastModeEnabled )   // no wait, return null connection
        {
          break;    // exit while loop
        }
        else
        {
          try
          {
            // wait 10ms before trying again
            Thread.sleep(10);     
          }
          catch( InterruptedException e )
          {
            break;    // exit while loop
          }            
        }          
      }
    }      
    trace("Pool Connection " + poolCon + " locked");

    if ( poolCon == null )
    {
      // failed to get a connection, we are in trouble
      alertAdmins();
	  log.error("[getFreeConnection()] - failed to retrieve pooled connection");
    }
    else    
    {
      // have a valid pool connection
      poolCon.setTimeoutExempt( timeoutExempt );
    }
    
    return( poolCon );
  }

  public int getPoolHeartbeat( )
  {
    return( PoolCleanerHeartbeat );
  }
  
  public int getPoolSize( )
  {
    return( Connections.size() );
  }
  
  public int getPoolSizeMax( )
  {
    return( PoolSizeMax );
  }
  
  public int getPoolSizeMin( )
  {
    return( PoolSizeMin );
  }
  
  public String getPoolCleanerState( )
  {
    String      retVal        = null;
    
    switch( PoolCleanerState )
    {
      case ST_IDLE:
        retVal = "Idle";
        break;
      case ST_WAIT:
        retVal = "Wait";
        break;
      case ST_REFRESH_ENTRY:
        retVal = "Refresh Entry";
        break;
      case ST_POP:
        retVal = "Pop";
        break;
      case ST_REMOVE:
        retVal = "Remove";
        break;
      case ST_RESET:
        retVal = "Reset";
        break;
      case ST_UNLOCK:
        retVal = "Unlock";
        break;
      case ST_BB_POP:
        retVal = "Big Brother Pop";
        break;
      case ST_BB_RESET:
        retVal = "Big Brother Reset";
        break;
      case ST_BB_UNLOCK:
        retVal = "Big Brother Unlock";
        break;
      case ST_BB_CHECK_STATUS:
        retVal = "Big Brother Check Status";
        break;
      case ST_BB_TEST_CONNECTION:
        retVal = "Big Brother Test Connection";
        break;
      case ST_BB_TERMINATE_SESSION:
        retVal = "Big Brother Terminate Session";
        break;
      case ST_BB_GET_SERIAL_NUMBER:
        retVal = "Big Brother Get Serial Number";
        break;
      case ST_LOOP:
        retVal = "Loop";
        break;
      default:
        retVal = "State: " + Integer.toString(PoolCleanerState);
        break;
    }
    return( retVal );
  }
  
  public int getTotalBigBrotherKills( )
  {
    return( Totals[TOTAL_BB_KILLS] );
  }
  
  public int getTotalConnects( )
  {
    return( Totals[TOTAL_CONNECTS] );
  }
  
  public int getTotalReleases( )
  {
    return( Totals[TOTAL_RELEASES] );
  }
  
  public int getTotalResets( )
  {
    return( Totals[TOTAL_RESETS] );
  }
  
  public long getUpTime( )
  {
    return( ( System.currentTimeMillis() - CreationTS ) );
  }
  
  public int getUseCountThreshold( )
  {
    return( UseCountThreshold );
  }
  
  protected void initialize( String connectString, int min, int max, int incSize, int shrinkSecs, int useCountThreshold )
  {
    String            driverClassName         = null;
    MessageFormat     messageParser           = new MessageFormat("{0}={1}");
    Object[]          nameValuePair           = null;
    int               tokenCount              = 0;
    StringTokenizer   tokenizer               = new StringTokenizer( connectString, ";" );

    tokenCount = tokenizer.countTokens();
    
    log.debug("[initialize()] - Initializing connection pool with connection string: " + DbProperties.getLoggableConnection(connectString));

    try
    {
      emptyPool();
      
      // store the new pool params
      
      PoolSizeMax         = max;
      PoolSizeMin         = min;
      PoolSizeIncrement   = incSize;
      PoolShrinkSecs      = shrinkSecs;
      UseCountThreshold   = useCountThreshold;
      
      // cap pool startup for dev/test servers to prevent over-usage of database sessions
      if( ! HttpHelper.isProdServer(null) )
      {
        PoolSizeMin       = 2;
        PoolSizeMax       = 10;
        PoolSizeIncrement = 1;
      }
      if ( log.isTraceEnabled()) {
		  	log.trace("[initialize()] - PoolSizeMin="+PoolSizeMin+", PoolSizeMax="+PoolSizeMax+", PoolSizeIncrement="+PoolSizeIncrement+", shrinkSecs="+shrinkSecs+", useCountThreashold="+useCountThreshold);
	  	}
      // set java DNS cache to 0 (no caching)
      try
      {
        java.security.Security.setProperty("networkaddress.cache.ttl" , "0");
      }
      catch(Exception ex)
      {
      }
      catch(Error er)
      {
      }
      
      //@ TODO - Add code to start a shrinker thread
      
      for( int i = 0; i < tokenCount; ++i )
      {
        switch( i )
        {
          case 0:     // jdbc name
            JdbcConnectionString = tokenizer.nextToken();
            break;
          
          case 1:     // driver class name
            JdbcDriverClassName = tokenizer.nextToken();
            break;
          
          default:    // rest are db properties
          {
            if ( DbProps == null )
            {
              DbProps = new Properties();
            }            
            try
            {
              nameValuePair = messageParser.parse( tokenizer.nextToken() );
              DbProps.put( nameValuePair[0].toString(), nameValuePair[1].toString() );
            }
            catch( java.text.ParseException e )
            {
              // improperly formatted, ignore
            }
            break;
          }        
        }          
      }        
    
      if ( JdbcDriverClassName != null )
      {
        //
        // Dynamically load the driver class
        //
        // The forName call will dynamically load the named class
        // into the JVM.  This call is safe even if the class has 
        // already been loaded.
        //
        Class.forName( JdbcDriverClassName );
      }
      
      for( int i = 0; i < PoolSizeMin; ++i )
      {
        addConnection(null);    // add an unlocked connection
      }
      
      if ( PoolCleaner == null )
      {
        // start the pool cleaning thread
        PoolCleaner = new Thread(this);
        PoolCleaner.start();
      }        
      
      // timestamp that the connection pool was initialize
      CreationTime = Calendar.getInstance().getTime();
    }
    catch( java.lang.ClassNotFoundException e )
    {
    }
    if ( log.isTraceEnabled()) {
	  	log.trace("[initialize()] - completed");
  	}
  }
  
  private boolean isAdminConnectionStale()
  {
    boolean retVal = true;
    try
    {
      if ( !AdminCtx.isClosed() )
      {
        int isAlive = 0;
        /*@lineinfo:generated-code*//*@lineinfo:1157^9*/

//  ************************************************************
//  #sql [AdminCtx] { select 1 
//            from  dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 1  \n          from  dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.database.OracleConnectionPool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   isAlive = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1161^9*/
        // stale if isAlive does not equal 1
        retVal = (isAlive != 1);
      }
    }
    catch( Exception e )
    {
      // ignore, assume connection is stale
    }
    return( retVal );
  }
  
  public synchronized void logEntry( String msg )
  {
    BufferedWriter        logFile       = null;
    File                  tmpFile       = null;
    
    if ( DebugOutput )
    {
      System.out.println(msg); 
    }
    if ( Logging )
    {
      try
      {
        tmpFile = new File("OracleConnectionPool.log");
        
        if ( tmpFile.length() > (1024*1024) )
        {
          File backupFile = new File("OracleConnectionPool.log.bak");
          backupFile.delete();
          tmpFile.renameTo(backupFile);
        }
        
        // output the the msg to the log file
        logFile  = new BufferedWriter( new FileWriter( "OracleConnectionPool.log", true ) );
        logFile.write( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yyyy HH:mm:ss - " ) );
        logFile.write( msg );
        logFile.newLine();
      }
      catch( Exception e )
      {
        System.out.println("Log Entry Failed: " + e.toString() );
      }
      finally
      {
        try{ logFile.close(); } catch( Exception ee ) {}
      }        
    }
  }

  private void resetAdminConnection( )
  {
    try
    {
      try{ AdminCtx.close(); } catch(Exception e) {}

      AdminCtx = Oracle.getConnection( JdbcConnectionString, DbProps, AutoCommit );
      /*@lineinfo:generated-code*//*@lineinfo:1219^7*/

//  ************************************************************
//  #sql [AdminCtx] { select  distinct(sid) 
//          from    v$mystat 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct(sid)  \n        from    v$mystat";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.database.OracleConnectionPool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AdminSessionId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1223^7*/
    }
    catch( Exception e )
    {
      logEntry("resetAdminConnection(): " + e.toString());
    }
  }
  
  public void resetPoolConnection( int index )
  {
    PoolConnection          poolCon     = null;
    
    try
    {
      poolCon = (PoolConnection)Connections.elementAt(index);
      poolCon.resetConnection();
      poolCon.unlock();
    }
    catch( Exception e )
    {
    }
  }
  
  public void run( )
  {
    int                     killCon           = 0;
    PoolConnection          poolCon           = null;
    String                  refString         = String.valueOf(this);
    int                     serialNumber      = 0;
    String                  serverName        = HttpHelper.getServerName(null);
    
    try
    {
      System.out.println("starting pool cleaner");
      
      while( PoolCleanerRunning )
      {
        synchronized( ConnectionRefreshList )
        {
          PoolCleanerState = ST_WAIT;
          ConnectionRefreshList.wait(REFRESH_SIGNAL_TIMEOUT);
          ++PoolCleanerHeartbeat;
        }
        
        //******************************************************
        // Connection Refresh List Loop Begin
        //******************************************************
        PoolCleanerState = ST_REFRESH_ENTRY;
        while( ConnectionRefreshList.size() > 0 )
        {
          try
          {
            // pop the connection 
            PoolCleanerState = ST_POP;
            poolCon = (PoolConnection)ConnectionRefreshList.elementAt(0);
            
            // log reset in big_brother_kills
            logBigBrother(poolCon, true, "R");
            
            // remove the connection from the vector
            PoolCleanerState = ST_REMOVE;
            ConnectionRefreshList.removeElementAt(0);
            
            // reset the connection (close and re-open)
            PoolCleanerState = ST_RESET;
            poolCon.resetConnection();
            
            // unlock the connection which will return
            // it to the free connection list
            PoolCleanerState = ST_UNLOCK;
            poolCon.unlock();
            
            // statistics
            Totals[TOTAL_RESETS]++;
          }
          catch( Exception e )
          {
            logEntry("POOL ERROR:  " + e.toString());
            
            // protect against unwanted thread
            // exit.  just ignore any exceptions.
            trace("******************************************");
            trace("POOL ERROR");
            trace("  " + e.toString());
            trace("******************************************");
          }            
        }
        //******************************************************
        // Connection Refresh List Loop End
        //******************************************************
        
        //******************************************************
        // Big Brother Loop Begin
        //******************************************************
        if((System.currentTimeMillis() - lastDeadConnectionAudit) > DEAD_CONNECTION_LOOP_TIME)
        {
          try
          {
            Vector           cons = getConnections();

            // make sure the connection is not closed
            if ( isAdminConnectionStale() )
            {
              resetAdminConnection();
            }
            else
            {
              if ( ++AdminUseCount > UseCountThreshold )
              {
                resetAdminConnection();
              }
            }
    
            for(int i=0; i < cons.size(); ++i)
            {
              PoolCleanerState = ST_BB_POP;
              poolCon = (PoolConnection)cons.elementAt(i);

              PoolCleanerState = ST_BB_TEST_CONNECTION;            
              if( poolCon.isLocked() && 
                  !poolCon.isTimeoutExempt() &&
                  poolCon.getLockTime() > DEAD_CONNECTION_TIME )
              {
                PoolCleanerState = ST_BB_CHECK_STATUS;
                /*@lineinfo:generated-code*//*@lineinfo:1347^17*/

//  ************************************************************
//  #sql [AdminCtx] { select  decode(nvl(status,'INACTIVE'),
//                                   'INACTIVE',1,0) 
//                    from    v$session
//                    where   sid(+) = :poolCon.getOracleSessionId()
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_0 = poolCon.getOracleSessionId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(nvl(status,'INACTIVE'),\n                                 'INACTIVE',1,0)  \n                  from    v$session\n                  where   sid(+) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.database.OracleConnectionPool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_0);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   killCon = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1353^17*/

                // if the session is inactive or it has been active
                // for too long, then return it to the pool.
                boolean wasActive = false;
                if ( (killCon == 1) || 
                     (poolCon.getLockTime() > TERMINATE_CONNECTION_TIME) )
                {
                  // if killCon is 0 then the connection is still
                  // active, but has been active for longer than
                  // the allowable time.  nuke it!
                  if ( killCon == 0 )
                  {
                    wasActive = true;
                    PoolCleanerState = ST_BB_GET_SERIAL_NUMBER;

                    // get the serial number for this session
                    /*@lineinfo:generated-code*//*@lineinfo:1370^21*/

//  ************************************************************
//  #sql [AdminCtx] { select  serial# 
//                        from    v$session
//                        where   sid = :poolCon.getOracleSessionId()
//                       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1 = poolCon.getOracleSessionId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  serial#  \n                      from    v$session\n                      where   sid =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.database.OracleConnectionPool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   serialNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1375^21*/

                    PoolCleanerState = ST_BB_TERMINATE_SESSION;

                    try
                    {
                      // establish the query string
                      StringBuffer qs = new StringBuffer("");
                      
                      qs.append("alter system kill session '");
                      qs.append(poolCon.getOracleSessionId());
                      qs.append(",");
                      qs.append(serialNumber);
                      qs.append("'");
                      
                      // get a prepared statement
                      PreparedStatement ps = AdminCtx.getConnection().prepareStatement(qs.toString());
                      
                      // run it
                      ps.executeUpdate();
                      
                      // make sure prepared statement is closed
                      ps.close();
                    }
                    catch(Exception killException)
                    {
                      System.out.println("Session killer: " + killException.toString());
                      logEntry("Session Killer: " + killException.toString());
                    }
                  }

                  try
                  {
                    logBigBrother(poolCon, wasActive, "K");
                  }
                  catch(Exception e)
                  {
                    System.out.println("Error logging session kill: " + e.toString());
                  }

                  // this connection was abandoned so
                  // reset it to be sure it is clean 
                  // before returning it to the pool
                  PoolCleanerState = ST_BB_RESET;
                  poolCon.resetConnection();      // reset this connection
                  PoolCleanerState = ST_BB_UNLOCK;   
                  poolCon.unlock();               // return connection to pool
                  ++Totals[TOTAL_BB_KILLS];
                }
                else    // just mark it
                {
                  poolCon.markForReset();
                }                
              }
            }
          }
          catch( Exception e )
          {
            System.out.println("Big Brother Failed: " + e.toString());
            logEntry("Big Brother Failed: " + e.toString());
          }

          // reset lastDeadConnectionAudit so this won't happen again for a while
          lastDeadConnectionAudit = System.currentTimeMillis();
        }
        
        //******************************************************
        // Big Brother Loop End
        //******************************************************
        
        PoolCleanerState = ST_LOOP;
      }
    }
    catch( InterruptedException e )
    {
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
      logEntry(e.toString());
    }
    finally
    {
      if( ! HttpHelper.isCluster(null))
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1462^11*/

//  ************************************************************
//  #sql [AdminCtx] { delete
//              from    mesweb_connection_pools
//              where   server_name = :serverName and
//                      reference = :refString
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    mesweb_connection_pools\n            where   server_name =  :1  and\n                    reference =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.database.OracleConnectionPool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serverName);
   __sJT_st.setString(2,refString);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1468^11*/
          /*@lineinfo:generated-code*//*@lineinfo:1469^11*/

//  ************************************************************
//  #sql [AdminCtx] { commit
//             };
//  ************************************************************

  ((AdminCtx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : AdminCtx.getExecutionContext().getOracleContext()).oracleCommit(AdminCtx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1472^11*/
        }
        catch( Exception e )
        {
        }
      }
    
      try{ AdminCtx.close(); } catch( Exception e ) {}
      AdminCtx = null;
      
      PoolCleaner = null;
    }
  }
  
  private void logBigBrother(PoolConnection pc, boolean wasActive, String killType)
  {
    try
    {
      // add log entry to big_brother_kills
      String wasActiveStr = (wasActive ? "Y" : "N");
      /*@lineinfo:generated-code*//*@lineinfo:1492^7*/

//  ************************************************************
//  #sql [AdminCtx] { insert into big_brother_kills
//          (
//            locked_by,
//            lock_user,
//            lock_time,
//            server_name,
//            was_active,
//            kill_type
//          )
//          values
//          (
//            :pc.getLockedByName(),
//            :pc.getLockedByLoginName(),
//            :pc.getLockTime(),
//            :HttpHelper.getServerName(null),
//            :wasActiveStr,
//            :killType
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = AdminCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2 = pc.getLockedByName();
 String __sJT_3 = pc.getLockedByLoginName();
 long __sJT_4 = pc.getLockTime();
 String __sJT_5 = HttpHelper.getServerName(null);
   String theSqlTS = "insert into big_brother_kills\n        (\n          locked_by,\n          lock_user,\n          lock_time,\n          server_name,\n          was_active,\n          kill_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.database.OracleConnectionPool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2);
   __sJT_st.setString(2,__sJT_3);
   __sJT_st.setLong(3,__sJT_4);
   __sJT_st.setString(4,__sJT_5);
   __sJT_st.setString(5,wasActiveStr);
   __sJT_st.setString(6,killType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1512^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1514^7*/

//  ************************************************************
//  #sql [AdminCtx] { commit
//         };
//  ************************************************************

  ((AdminCtx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : AdminCtx.getExecutionContext().getOracleContext()).oracleCommit(AdminCtx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1517^7*/
    }
    catch(Exception e)
    {
      System.out.println("logBigBrother failed: " + e.toString());
      logEntry("logBigBrother Failed: " + e.toString());
    }
  }
  
  public void setDebugOutput( boolean newValue )
  {
    DebugOutput = newValue;
  }
  
  public void setFastModeEnabled( boolean enabled )
  {
    FastModeEnabled = enabled;
  }
  
  public void setLogging( boolean newValue )
  {
    Logging = newValue;
  }
  
  public void setTracing( boolean newValue )
  {
    Tracing = newValue;
  }
  
  public synchronized void trace( String msg )
  {
    BufferedWriter        logFile       = null;
    File                  tmpFile       = null;
    
    if ( DebugOutput )
    {
      log.debug(msg);
    }
    if ( Tracing )
    {
      try
      {
        tmpFile = new File("OracleConnectionPool.trace");
        
        if ( tmpFile.length() > (1024*1024) )
        {
          File backupFile = new File("OracleConnectionPool.trace.bak");
          backupFile.delete();
          tmpFile.renameTo(backupFile);
        }
        
        // output the the msg to the log file
        logFile  = new BufferedWriter( new FileWriter( "OracleConnectionPool.trace", true ) );
        logFile.write( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yyyy HH:mm:ss - " ) );
        logFile.write( msg );
        logFile.newLine();
      }
      catch( Exception e )
      {
        logEntry("Trace Log Failed: " + e.toString() );
      }
      finally
      {
        try{ logFile.close(); } catch( Exception ee ) {}
      }        
    }
  }
}/*@lineinfo:generated-code*/