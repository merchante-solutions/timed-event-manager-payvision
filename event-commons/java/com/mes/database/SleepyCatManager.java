/**
 * Manages sleepycat database environments.
 *
 * Useage notes:
 *
 * getInstance(dbPathStr) - fetches an instance of the manager for the 
 * specified location in the file system.  A sleepy cat environment is
 * setup.
 *
 * createStoredMap(idxName) - creates a database table with index name and
 * returns a StoredMap on the table.
 *
 * createSecondaryStoredMap(primaryName,secondaryName,SerialKeyCreator) -
 * creates a primary database if needed, creates a secondary database
 * associated with the primary and returns a StoredSortedMap on the secondary
 * table that uses the serial key creator to map the desired secondary index
 * key to database items.  The secondary map is sorted by the key, and allows
 * ranges of the database object to be fetched, removed, etc.
 *
 * Don't forget to close(), close(pathStr) or closeAll() when done.
 *
 * Example:
 *
 *  SleepyCatManager manager = SleepyCatManager.getInstance("db");
 *
 *  // create stored map for order entries
 *  StoredMap snMap = manager.createStoredMap("order_entries");
 *
 *  // configure secondary stored map using tran date as key
 *  StoredSortedMap dateMap = manager.createSecondaryStoredMap(
 *   "order_entries",
 *   "order_entries_by_date",
 *   new SerialSerialKeyCreator(manager.getClassCatalog(),String.class,
 *     BmlOrderIdEntry.class,Date.class)
 *   {
 *     public Object createSecondaryKey(Object primKeyInput, Object valueInput)
 *     {
 *       BmlOrderIdEntry entry = (BmlOrderIdEntry)valueInput;
 *       return entry.getTranDate();
 *     }
 *   });
 * 
 */
package com.mes.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;
import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.SecondaryConfig;
import com.sleepycat.je.SecondaryDatabase;
import com.sleepycat.je.SecondaryKeyCreator;

public class SleepyCatManager
{
  static Logger log = Logger.getLogger(SleepyCatManager.class);

  private static final String IDX_CLASS_CATALOG = "ClassCatalogDB";

  // map of file locations to manager instances
  private static HashMap managers = new HashMap();

  // location of database files
  private File dbLocation;

  // the containing environment for all db tables
  private Environment env;

  // default db configs
  private DatabaseConfig dfltPrimConfig;
  private SecondaryConfig dfltSecConfig;

  // stored class catalog for serialized object class descriptions
  private StoredClassCatalog  classCatalog;

  // index name to primary database map
  private Map primDbMap = new HashMap(); 

  // index name to secondary database map
  private Map secDbMap = new HashMap(); 

  // index name to primary database StoredMaps
  private Map primIdxMap = new HashMap();

  // index name to secondary database StoredSortedMaps
  private Map secIdxMap = new HashMap(); 
  
  public SleepyCatManager(File dbPath) 
    throws DatabaseException, FileNotFoundException
  {
    open(dbPath);

    // setup default primary db config with
    // duplicates = false (unique primary keys)
    dfltPrimConfig = new DatabaseConfig();
    dfltPrimConfig.setReadOnly(false);
    dfltPrimConfig.setAllowCreate(true);
    dfltPrimConfig.setTransactional(true);
    dfltPrimConfig.setSortedDuplicates(false);

    // setup default secondary config
    // duplicates = true (non-unique secondary indexes)
    dfltSecConfig = new SecondaryConfig();
    dfltSecConfig.setTransactional(true);
    dfltSecConfig.setAllowCreate(true);
    dfltSecConfig.setSortedDuplicates(true);
  }

  /**
   * Opens a sleepycat environment at a specified location on the 
   * local disk and opens a class catalog db in it.
   */
  private void open(File dbPath)
    throws DatabaseException, FileNotFoundException
  {
    dbPath.mkdirs();

    // configure and create the main sleepycat db environment
    EnvironmentConfig envConfig = new EnvironmentConfig();
    envConfig.setReadOnly(false);
    envConfig.setAllowCreate(true);
    envConfig.setTransactional(true);

    env = new Environment(dbPath,envConfig);

    // configure and create the class catalog
    // (stores class descriptions for serialized objects stored in the db)
    DatabaseConfig catConfig = new DatabaseConfig();
    catConfig.setReadOnly(false);
    catConfig.setAllowCreate(true);
    catConfig.setTransactional(true);
    catConfig.setSortedDuplicates(false);

    classCatalog = new StoredClassCatalog(
      env.openDatabase(null,IDX_CLASS_CATALOG,catConfig));
  }

  public StoredClassCatalog getClassCatalog()
  {
    return classCatalog;
  }

  /**
   * Creates a primary database with the specified configuration.
   */
  public Database createDatabase(String indexName, DatabaseConfig dbConfig)
    throws DatabaseException
  {
    Database db = env.openDatabase(null,indexName,dbConfig);
    primDbMap.put(indexName,db);
    return db;
  }

  /**
   * Creates primary database with default configuration.
   */
  public Database createDatabase(String indexName) throws DatabaseException
  {
    return createDatabase(indexName,dfltPrimConfig);
  }

  /**
   * Fetches a database.
   */
  public Database getDatabase(String indexName)
  {
    return (Database)primDbMap.get(indexName);
  }

  /**
   * Creates a secondary database associated with a primary database, useful for
   * creating secondary indexes.
   */
  public SecondaryDatabase createSecondaryDatabase(String primIndexName, 
    String secIndexName, SecondaryConfig secConfig) throws DatabaseException
  {
    // fetch the primary database
    Database primDb = getDatabase(primIndexName);
    if (primDb == null)
    {
      throw new NullPointerException("Primary database '" + primIndexName
        + "' not found");
    }

    SecondaryDatabase secDb = env.openSecondaryDatabase(null,secIndexName,
      primDb,secConfig);
    secDbMap.put(secIndexName,secDb);
    return secDb;
  }

  /**
   * Creates a secondary database with default configuration.
   */
  public SecondaryDatabase createSecondaryDatabase(String primIndexName,
    String secIndexName, SecondaryKeyCreator kc) throws DatabaseException
  {
    dfltSecConfig.setKeyCreator(kc);
    return createSecondaryDatabase(primIndexName,secIndexName,dfltSecConfig);
  }

  /**
   * Fetches a secondary database.
   */
  public SecondaryDatabase getSecondaryDatabase(String indexName)
  {
    return (SecondaryDatabase)secDbMap.get(indexName);
  }

  /**
   * Creates a primary key stored map associated with the named database.
   */
  public StoredMap createStoredMap(String indexName, Class keyClass,
    Class valueClass) throws DatabaseException
  {
    // if already exists, return it
    StoredMap storedMap = (StoredMap)primIdxMap.get(indexName);
    if (storedMap == null)
    {
      // fetch/create the db
      Database db = getDatabase(indexName);
      if (db == null)
      {
        db = createDatabase(indexName);
      }

      // create and store the stored map
      EntryBinding keyBinding = new SerialBinding(classCatalog,keyClass);
      EntryBinding valueBinding = new SerialBinding(classCatalog,valueClass);
      storedMap = new StoredMap(db,keyBinding,valueBinding,true);
      primIdxMap.put(indexName,storedMap);
    }

    return storedMap;
  }

  /**
   * Creates a primary key stored map with generic key/value classes
   * (Object.class).
   */
  public StoredMap createStoredMap(String indexName) throws DatabaseException
  {
    return createStoredMap(indexName,Object.class,Object.class);
  }

  /**
   * Create stored map associated with the given secondary database.
   */
  public StoredSortedMap createSecondaryStoredMap(String secIndexName, 
    Class keyClass, Class valueClass) throws DatabaseException
  {
    // make sure secondary stored map doesn't already exist
    if (primIdxMap.get(secIndexName) != null)
    {
      throw new RuntimeException("Stored map already exists"
        + " for index '" + secIndexName + "'");
    }
    
    // fetch the secondary db
    SecondaryDatabase secDb = getSecondaryDatabase(secIndexName);
    if (secDb == null)
    {
      throw new NullPointerException("Secondary database with index '" 
        + secIndexName + "' not found");
    }

    EntryBinding keyBinding = new SerialBinding(classCatalog,keyClass);
    EntryBinding valueBinding = new SerialBinding(classCatalog,valueClass);
    StoredSortedMap ssMap = new StoredSortedMap(secDb,keyBinding,valueBinding,true);
    primIdxMap.put(secIndexName,ssMap);

    return ssMap;
  }

  /**
   * Creates a stored map associated with a secondary database.  This method
   * requires a secondary key creator which is used to create a secondary
   * database.  The primary database will be created if needed using
   * default values, including default key/value classes.
   */
  public StoredSortedMap createSecondaryStoredMap(String primIndexName,
    String secIndexName, Class keyClass, Class valueClass,
    SecondaryKeyCreator kc) throws DatabaseException
  {
    // fetch/create primary database
    Database primDb = getDatabase(primIndexName);
    if (primDb == null)
    {
      primDb = createDatabase(primIndexName);
    }

    // create secondary database
    createSecondaryDatabase(primIndexName,secIndexName,kc);

    // create secondary stored map
    return createSecondaryStoredMap(secIndexName,keyClass,valueClass);
  }

  /**
   * Creates a stored map associated with a secondary databse.  The key
   * creator is used to create a secondary database associated with the
   * primary.  The primary will be created with default values if it
   * does not exist.  The secondary stored map will use generic key/value
   * class types (Object.class).
   */
  public StoredSortedMap createSecondaryStoredMap(String primIndexName,
    String secIndexName, SecondaryKeyCreator kc) throws DatabaseException
  {
    return createSecondaryStoredMap(primIndexName,secIndexName,
      Object.class,Object.class,kc);
  }

  /**
   *  Fetches a stored map.  If map does not exist this will create the
   * the underlying database object and create the stored map.
   */
  public StoredMap getStoredMap(String indexName) throws DatabaseException
  {
    StoredMap map = (StoredMap)primIdxMap.get(indexName);
    if (map == null)
    {
      map = createStoredMap(indexName);
    }
    return map;
  }

  /**
   * Fetches a secondary stored sorted map.  If the map does not exist
   * it cannot be created due to specific secondary key creators needing
   * to be explicitly specified.  Returns null if none found.
   */
  public StoredSortedMap getSecondarydMap(String indexName)
  {
    StoredSortedMap map = (StoredSortedMap)secIdxMap.get(indexName);
    if (map == null)
    {
      log.warn("No secondary database exists named '" + indexName + "'");
    }
    return map;
  }

  /**
   * Closes all databases in the sleepycat environment, the class catalog
   * and the environment itself.
   */
  public void close()
  {
    // close secondary databases
    for (Iterator i = secDbMap.values().iterator(); i.hasNext();)
    {
      Database db = (Database)i.next();
      try
      {
        db.close();
      }
      catch (Exception e)
      {
        log.error("Error closing secondary database: " + e);
      }
    }

    // close primary databases
    for (Iterator i = primDbMap.values().iterator(); i.hasNext();)
    {
      Database db = (Database)i.next();
      try
      {
        db.close();
      }
      catch (Exception e)
      {
        log.error("Error closing database: " + e);
      }
    }

    // close class catalog
    try
    {
      classCatalog.close();
    }
    catch (Exception e)
    {
      log.error("Error closing class catalog: " + e);
    }

    // finally, close the environment
    try
    {
      env.close();
    }
    catch (Exception e)
    {
      log.error("Error closing sleepy cat environment: " + e);
    }
  }

  /**
   * Create a new or fetch an existing manager instance for the given
   * location string.
   */
  public static SleepyCatManager getInstance(String dbPathStr) 
    throws Exception
  {
    File dbPath = (new File(dbPathStr)).getCanonicalFile();
    SleepyCatManager manager = (SleepyCatManager)managers.get(dbPath);
    if (manager == null)
    {
      manager = new SleepyCatManager(dbPath);
      managers.put(dbPath,manager);
    }
    return manager;
  }

  /**
   * Close each manager instance and remove it from the managers map.
   */
  public static void closeAll()
  {
    for (Iterator i = managers.values().iterator(); i.hasNext(); i.remove())
    {
      SleepyCatManager manager = (SleepyCatManager)i.next();
      manager.close();
    }
  }

  /**
   * Close a specific manager instance.
   */
  public static void close(String dbPathStr)
  {
    try
    {
      File dbPath = (new File(dbPathStr)).getCanonicalFile();
      SleepyCatManager manager = (SleepyCatManager)managers.get(dbPath);
      if (manager != null)
      {
        manager.close();
      }
      else
      {
        log.warn("No manager instance found to close for path '" 
          + dbPathStr + "'");
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
}