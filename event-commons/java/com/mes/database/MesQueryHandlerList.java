package com.mes.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MesQueryHandlerList extends MeSQueryHandler<List<Map<String, Object>>> {

	protected List<Map<String, Object>> copyQueryResults(ResultSet resultSet) throws SQLException {
		List<Map<String, Object>> queryResults = new ArrayList<>();
		while (resultSet.next()) {
			ResultSetMetaData metaData = resultSet.getMetaData();
			int totalColumnCount = metaData.getColumnCount();
			Map<String, Object> row = new HashMap<>();
			queryResults.add(row);

			for (int i = 0; i < totalColumnCount; i++) {
				int columnIndex = i + 1;
				Object obj = resultSet.getObject(columnIndex);
				String columnName = metaData.getColumnName(columnIndex).toLowerCase();
				if (obj instanceof java.sql.Timestamp) {
					row.put(columnName, new java.util.Date(((Timestamp) obj).getTime()));
				}
				else {
					row.put(columnName, obj);
				}
			}
		}
		return queryResults;
	}
}
