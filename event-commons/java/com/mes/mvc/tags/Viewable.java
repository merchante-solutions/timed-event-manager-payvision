package com.mes.mvc.tags;

import java.util.List;

public interface Viewable
{
  public List getRows();
  public boolean isReversed();
  public int getLeadCount();
  public int getTrailCount();
  public boolean exceedsLimits();
  public boolean isExpanded();
}