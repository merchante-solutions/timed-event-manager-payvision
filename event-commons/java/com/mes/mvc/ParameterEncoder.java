/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/mvc/ParameterEncoder.java $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-11-08 19:00:21 -0800 (Tue, 08 Nov 2011) $
  Version            : $Revision: 19519 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mvc;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ParameterEncoder
{
  private List parmList = new ArrayList();

  public void add(String name, String value)
  {
    parmList.add(new String[] { name, value });
  }

  public void replace(String name, String value)
  {
    for (Iterator i = parmList.iterator(); i.hasNext();)
    {
      String[] data = (String[])i.next();
      if (data[0].equals(name))
      {
        data[1] = value;
        return;
      }
    }
  }

  public String encode() 
  {
    return encode(parmList);
  }

  public String encode(Map parmMap)
  {
    List parmList = new ArrayList();
    for (Iterator i = parmMap.keySet().iterator(); i.hasNext();)
    {
      String name = (String)i.next();
      String value = (String)parmMap.get(name);
      parmList.add(new String[] { name, value });
    }
    return encode(parmList);
  }

  public String encode(List parmList)
  {
    try
    {
      StringBuffer buf = new StringBuffer();
      for (Iterator i = parmList.iterator(); i.hasNext();)
      {
        if (buf.length() > 0)
        {
          buf.append("&");
        }
        String[] data = (String[])i.next();
        buf.append(URLEncoder.encode(data[0],"UTF-8") + "=");
        buf.append(URLEncoder.encode(data[1],"UTF-8"));
      }
      return buf.toString();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  /**
   * Allows you to specify the order of parameters in a map using a
   * separate list.
   */
  public String encode(List parmOrder, Map parmMap)
  {
    List parmList = new ArrayList();
    for (Iterator i = parmOrder.iterator(); i.hasNext();)
    {
      String name = (String)i.next();
      String value = (String)parmMap.get(name);
      parmList.add(new String[] { name, value });
    }
    return encode(parmList);
  }
  
  public void put(String name, String value)
  {
    String[] data = null;
  
    for (Iterator i = parmList.iterator(); i.hasNext();)
    {
      String[] tempData = (String[])i.next();
      if (tempData[0].equals(name))
      {
        data = tempData;
        break;
      }
    }
    if ( data == null ) 
    {
      parmList.add(new String[] { name, value });
    }
    else
    {
      data[1] = value;
    }
  }
}

