/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVFile.java $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/04/02 9:24a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.sql.ResultSet;

public abstract class CSVFile
{
  public CSVFile()
  {
  }
  
  public CSVFile(ResultSet rs, boolean withHeader)
  {
    try
    {
      // add the header record if necessary
      if(withHeader)
      {
        setHeader(rs);
      }
      addRows(rs);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::CSVFile(rs, withheader)", e.toString());
    }
  }
  
  public void addRows(ResultSet rs)
  {
    addRows(rs,CSVRow.DATE_MMDDYY);
  }
  
  public abstract void addRows(ResultSet rs, String dateFormat);
  public abstract void clear();
  public abstract int rowCount();
  public abstract void setHeader(ResultSet rs);
  public abstract void setHeader(CSVRow row);
}
