package com.mes.support.config;

import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.support.LoggingConfigurator;

/**
 *
 * Retrieves property file strings and converts them to String arrays suitable
 * for system callouts.
 *
 * Property file naming convention: <function>.<name>
 *
 *  i.e. decrypt.aus-discover
 *
 * Callout strings are delimited by double colons (::).  Strings will be split
 * into an array of strings to be passed to Runtime.exec() method.
 *
 * Tokens: functions may support replacement tokens using angle bracket
 * convention.
 *
 *  i.e. <in-file> would be replaced by an input filename
 *
 * This class is generic, intended to be extended for specific use cases.
 *
 *  i.e. CryptCalloutConfig manages encryption/decryption system callouts.
 */

public class CalloutConfig extends Config {

  static { LoggingConfigurator.requireConfiguration(); }
  static Logger log = Logger.getLogger(CalloutConfig.class);
  
  private static final String SYM_RT_SPLITTER = "::";
  
  public CalloutConfig(String filename) {
    super(new FileConfigSource(filename));
  }
    
  public CalloutConfig() {
    this("callout.properties");
  }
  
  /**
   * Fetches property by combining function and name.  If tokenMap 
   * provided will replace tokens in property string.  Returns String
   * array by splitting property string with the splitter symbol.
   */
  public String[] getRuntimeStrs(String function, String name, 
    Map tokenMap)
  {
    String[] strs = getTokenProperty(function + "." + name,tokenMap).split(SYM_RT_SPLITTER);
    for (int i = 0; i < strs.length; ++i) strs[i] = strs[i].trim();
    return strs;
  }

  /**
   * Passes null for token map.
   */  
  public String[] getRuntimeStrs(String function, String name) {
  
    return getRuntimeStrs(function,name,null);
  }
}
