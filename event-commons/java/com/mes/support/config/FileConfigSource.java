package com.mes.support.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;

/** 
 * Keeps a reference to a File and produces input streams for reading
 * data from it.  Also keeps track of last loaded time and provides a
 * check to determine if the file has been modified since.
 */
public class FileConfigSource implements ConfigSource {

  static Logger log = Logger.getLogger(FileConfigSource.class);
  
  // how long to wait before checking file date
  private static long CHECK_FREQUENCY = 1000;
  
  private File file = null;
  private Date lastLoadDate = null;
  private long lastCheckTs = 0;

  public FileConfigSource(File file) {
  
    if (!isValidFile(file)) {
    
      throw new RuntimeException("Invalid file: " + file);
    }
    this.file = file;
  }

  public FileConfigSource(String filename) {
  
    this(new File(filename));
  }
  
  private boolean isValidFile(File file) {
  
    boolean isValid = file != null && file.exists() && !file.isDirectory();

    if (!isValid) {
    
      log.warn("Config source file " + file + " is not valid");
    }
    
    return isValid;
  }

  public InputStream getInputStream() {
  
    try {
  
      FileInputStream fis = new FileInputStream(file);
      lastLoadDate = Calendar.getInstance().getTime();
      return fis;
    }
    catch (Exception e) {
    
      throw new RuntimeException(e);
    }
  }
  
  public boolean isUpdated() {
  
    if (!isValidFile(file)) {
    
      return false;
    }
    
    // limit the frequency of file checks
    long curTs = Calendar.getInstance().getTimeInMillis();
    if (curTs - lastCheckTs < CHECK_FREQUENCY) {
    
      return false;
    }
    lastCheckTs = Calendar.getInstance().getTimeInMillis();
  
    Date lastModDate = new Date(file.lastModified());
    return lastModDate.after(lastLoadDate);
  }
  
  public String toString() {
  
    return "FileConfigSource [ file: " + file + " ]";
  }
}
  
    