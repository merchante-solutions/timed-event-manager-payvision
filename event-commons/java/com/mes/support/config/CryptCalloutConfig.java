package com.mes.support.config;

import java.util.Map;

public class CryptCalloutConfig extends CalloutConfig {
  
  private static final String FUNC_ENCRYPT    = "encrypt";
  private static final String FUNC_DECRYPT    = "decrypt";
  
  private static final String TOK_IN_FILE     = "<in-file>";
  private static final String TOK_OUT_FILE    = "<out-file>";
  
  public CryptCalloutConfig() {
  }
  
  public CryptCalloutConfig(String filename) {
    super(filename);
  }
  
  public String[] getEncryptRuntimeStrs(String name, String inFilename, String outFilename) {
  
    Map tokenMap = getTokenMap(
      new String[] { TOK_IN_FILE, inFilename, TOK_OUT_FILE, outFilename });
    return getRuntimeStrs(FUNC_ENCRYPT,name,tokenMap);
  }
  
  public String[] getDecryptRuntimeStrs(String name, String inFilename, String outFilename) {
  
    Map tokenMap = getTokenMap(
      new String[] { TOK_IN_FILE, inFilename, TOK_OUT_FILE, outFilename });
    return getRuntimeStrs(FUNC_DECRYPT,name,tokenMap);
  }
}
