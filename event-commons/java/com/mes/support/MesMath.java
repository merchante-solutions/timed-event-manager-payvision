/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesMath.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Currency;

public class MesMath
{
  public static long doubleToLong(double dVal )
  {
    return( doubleToLong( dVal, 2 ) );
  }

  // input a double value and number of decimal places,
  // output value as a long shifted up dec places.
  public static long doubleToLong(double dVal, int dec )
  {
    NumberFormat      nFormat   = NumberFormat.getInstance();
    long              longVal   = 0L;
    StringBuffer      temp      = null;
    
    nFormat.setMaximumFractionDigits(dec);
    nFormat.setMinimumFractionDigits(dec);
    
    // load the floating point value into a string
    temp = new StringBuffer( nFormat.format(dVal) );
    
    // convert the string to a long by 
    // removing the decimal point.
    for(int i = 0; i < temp.length(); ++i )
    {
      switch( temp.charAt(i) )
      {
        case ',':   // remove any ',' added by the number formatter
        case '.':
          temp.deleteCharAt(i);
          break;
      }
    }
    longVal = Long.parseLong(temp.toString());
    return( longVal );
  }
  
  public static double round( double value, int decimalCount )
  {
    BigDecimal bd = new BigDecimal( Double.toString(value) );
    bd = bd.setScale(decimalCount, RoundingMode.HALF_UP);
    return( bd.doubleValue() );
  }

  public static double roundUp( double value )
  {
    return( roundUp( value, 2 ) );
  }
  
  public static double roundUp( double value, int decimalCount )
  {
    double      change;
    double      temp;
    
    // seperate the whole number from 
    // the fraction.  store fraction in change
    change  = ( value - ((long)value) );
    
    // move two decimal places into the 
    // whole number portion.
    temp    = ( change * Math.pow(10,decimalCount) );
    change  = ( (long)temp * Math.pow(10,-decimalCount) );
    
    // using the whole number portion of temp,
    // create the exact change (rounded down).
    change  = ((double)((long) temp) * Math.pow(10,-decimalCount));
    
    // check to see if there are valid decimal
    // places beyond decimal count.
    if ( temp > ((long) temp) )
    {
      change += Math.pow(10,-decimalCount);
    }
    return( (double)((long)value) + change );
  }

  /*
    convert string type to double type and get currency format.. if not valid double.. 
    unformatted string is returned
  */
  public static String toCurrency( String value )
  {
    String result = value;

    try
    {
      double tempDouble = Double.parseDouble(value.trim());
      result = toCurrency(tempDouble);
    }
    catch(Exception e)
    {
    }
    
    return result;
  }

  public static String toCurrency( double value )
  {
    return( toCurrency(value, "USD") );
  }

  public static String toCurrency( double value, String currencyCodeAlpha )
  {
    double    amount    = ( Math.abs( value ) < 0.01 ? 0.0 : value );
    String    retVal    = "";
    
    if ( currencyCodeAlpha == null || currencyCodeAlpha.equals("USD") )
    {
      retVal = toFractionalCurrency( amount, 2 );
    }
    else
    {
      int decDigits = Currency.getInstance(currencyCodeAlpha).getDefaultFractionDigits();
      retVal = toFractionalAmount(amount,decDigits,decDigits,true);
      if ( "BRL".equals(currencyCodeAlpha) )
      {
        retVal = "R$" + retVal.replaceAll(",",";").replaceAll("\\.",",").replaceAll(";",".");
      }
    }
    return( retVal );
  }
  
  public static String toCurrencyCompressed( double value )
  {
    double    amount    = ( Math.abs( value ) < 0.01 ? 0.0 : value );
    
    return( toFractionalCurrency( amount, 2, false ) );
  }
  
  public static String toFractionalCurrency( double value )
  {
    return( toFractionalCurrency( value, 6, true ) );
  }
  
  public static String toFractionalCurrency( double value, int maxFractionDigits )
  {
    return( toFractionalCurrency( value, maxFractionDigits, true ) );
  }
  
  public static String toFractionalCurrency( double value, int maxFractionDigits, boolean useGrouping )
  {
    NumberFormat        currencyFormat  = NumberFormat.getCurrencyInstance( java.util.Locale.US );
    
    currencyFormat.setGroupingUsed( useGrouping );
    currencyFormat.setMaximumFractionDigits(maxFractionDigits);
    return( currencyFormat.format( value ) );
  }
  
  public static String toFractionalCurrencyCompressed( double value )
  {
    return( toFractionalCurrency( value, 6, false ) );
  }
  
  public static String toFractionalCurrencyCompressed( double value, int maxFractionDigits )
  {
    return( toFractionalCurrency( value, maxFractionDigits, false ) );
  }
  
  public static String toPercent(double value, int maxFractionDigits, int minFractionDigits)
  {
    NumberFormat percentFormat = NumberFormat.getPercentInstance(java.util.Locale.US);
    percentFormat.setMaximumFractionDigits(maxFractionDigits);
    percentFormat.setMinimumFractionDigits(minFractionDigits);
    return percentFormat.format(value);
  }
  public static String toPercent(double value, int maxFractionDigits)
  {
    return toPercent(value,maxFractionDigits,0);
  }
  public static String toPercent(double value)
  {
    return toPercent(value,1);
  }
  
  public static String toFractionalAmount(double value, int maxFractionDigits, int minFractionDigits, boolean useGrouping)
  {
    NumberFormat format = NumberFormat.getInstance();
    format.setGroupingUsed( useGrouping );
    format.setMaximumFractionDigits(maxFractionDigits);
    format.setMinimumFractionDigits(minFractionDigits);
    return format.format(value);
  }
  public static String toFractionalAmount(double value, int maxFractionDigits, int minFractionDigits)
  {
    return toFractionalAmount(value,maxFractionDigits,minFractionDigits,true);
  }
  public static String toFractionalAmount(double value, int maxFractionDigits)
  {
    return toFractionalAmount(value,maxFractionDigits,0);
  }
  public static String toFractionalAmount(double value)
  {
    return toFractionalAmount(value,1,0);
  }
}


