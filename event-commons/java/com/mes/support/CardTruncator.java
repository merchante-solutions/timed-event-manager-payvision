package com.mes.support;

import java.util.StringTokenizer;

public class CardTruncator
{
  public static String truncTokens = " \t\n\r\f,=;&[]<>";
  
  public static String truncate(String data)
  {
    StringBuffer value = new StringBuffer("");
    
    if(data != null)
    {
      StringTokenizer st = new StringTokenizer(data, truncTokens, true);
    
      String next = "";
    
      while( st.hasMoreTokens() )
      {
        next = st.nextToken();
      
        if( next.length() <= 16 && next.length() >= 14 && isDigits(next) && TridentTools.mod10Check(next) == true )
        {
          // probably a card number
          value.append( TridentTools.encodeCardNumber(next) );
        }
        else
        {
          value.append(next);
        }
      }
    }
    
    return( value.toString() );
  }
  
  public static boolean containsCard(String data)
  {
    boolean result = false;
    
    StringTokenizer st = new StringTokenizer(data, truncTokens, true);
    
    String next = "";
    
    while( st.hasMoreTokens() )
    {
      next = st.nextToken();
      
      if( next.length() <= 16 && next.length() >= 14 && isDigits(next) && TridentTools.mod10Check(next) == true )
      {
        // probably a card number
        result = true;
      }
    }
    
    return( result );
  }
  
  private static boolean isDigits(String test)
  {
    boolean result = true;
    
    for( int i=0; i < test.length(); ++i )
    {
      if( ! Character.isDigit(test.charAt(i)) )
      {
        result = false;
      }
    }
    
    return( result );
  }
  
  public static void main(String[] args)
    throws Exception
  {
    if( containsCard(args[0]) )
    {
      System.out.println( truncate(args[0]) );
    }
    else
    {
      System.out.println("no card number detected");
    }
  }
}