/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVFileMemory.java $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/04/02 9:24a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.sql.ResultSet;
import java.util.Vector;

public class CSVFileMemory extends CSVFile
{
  private boolean hasHeader     = false;
  private CSVRow  header        = null;
  
  private Vector  rows          = new Vector();
  
  public CSVFileMemory()
  {
  }
  
  public CSVFileMemory(ResultSet rs, boolean withHeader)
  {
    try
    {
      // add the header record if necessary
      if(withHeader)
      {
        header = new CSVRow(rs, true);
        hasHeader = true;
      }
    
      while(rs.next())
      {
        rows.add(new CSVRow(rs, false));
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::CSVFileMemory(rs, withheader)", e.toString());
    }
  }
  
  public void addRows(ResultSet rs, String dateFormat)
  {
    try
    {
      while(rs.next())
      {
        rows.add(new CSVRow(rs, false, dateFormat));
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::addRows(rs, dateFormat)", e.toString());
    }
  }
  
  public void clear()
  {
    rows.clear();
  }
  
  public int rowCount()
  {
    return(rows.size());
  }
  
  public void setHeader(ResultSet rs)
  {
    header = new CSVRow(rs, true);
    hasHeader = true;
  }
  
  public void setHeader(CSVRow row)
  {
    header = row;
    hasHeader = true;
  }
  
  public void showData( java.io.PrintStream out )
  {
    if(hasHeader)
    {
      out.print( header.toString() );
    }
    
    for(int i=0; i < rows.size(); ++i)
    {
      out.print( ((CSVRow)rows.elementAt(i)).toString() );
    }
  }
  
  public String toString()
  {
    StringBuffer result = new StringBuffer("");
    
    if(hasHeader)
    {
      result.append(header.toString());
    }
    
    for(int i=0; i < rows.size(); ++i)
    {
      result.append(((CSVRow)rows.elementAt(i)).toString());
    }
    
    return result.toString();
  }
}
