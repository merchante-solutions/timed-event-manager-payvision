/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/support/ThreadPool.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-07-30 15:35:07 -0700 (Fri, 30 Jul 2010) $
  Version            : $Revision: 17651 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Vector;
import org.apache.log4j.Logger;

public class ThreadPool
{
  private   int                         ThreadCount         = 0;
  private   int                         ThreadCountMax      = 7;
  private   Vector                      Workers             = null;
  private static Logger log = Logger.getLogger(ThreadPool.class);
  
  public ThreadPool( int threadCountMax )
  {
    ThreadCountMax  = threadCountMax;
    Workers = new Vector(ThreadCountMax);
  }  

  private synchronized void adjustThreadCount( int value )
  {
    ThreadCount += value;
  }

  public Thread getThread( Runnable job )
  {
    return( getThread(job,-1L) );
  }

  public Thread getThread( Runnable job, long timeout )
  {
    Thread      retVal      = null;
    long        startTime   = System.currentTimeMillis();
    Thread      worker      = null;
    
    // returns a thread from the pool attached to the specified job
    // if timeout is specified and no thread become available then
    // getThread(..) returns null.
    while( retVal == null )
    {
      if ( Workers.size() < ThreadCountMax )
      {
        retVal = new Thread(job);
        Workers.add(retVal);
      }
      else
      {
        for( int i = 0; i < Workers.size(); ++i )
        {
          worker = (Thread)Workers.elementAt(i);
          if ( !worker.isAlive() )
          {
            Workers.removeElementAt(i);
            retVal = new Thread(job);
            Workers.add(retVal);
            break;
          }
        }
        
        if ( retVal == null )
        {
          // check for timeout and return null
          if ( timeout > 0 && ((System.currentTimeMillis() - startTime) > timeout) )
          {
            break;
          }
        
          try
          {
            Thread.sleep(10);    // wait 10ms
          }
          catch( java.lang.InterruptedException ie )
          {
            // thread killed, exit loop
            ThreadPool.log.debug(ie.getMessage());
            break;
          }
        }
      }
    }
    return( retVal );
  }
  
  public void waitForAllThreads()
  {
    for ( int i = 0; i < Workers.size(); ++i )
    {
      Thread worker = (Thread)Workers.elementAt(i);
      try
      {
        worker.join();
      }
      catch( java.lang.InterruptedException ee )
      {
        ThreadPool.log.debug(ee.getMessage());
      }
    }
  }
}
