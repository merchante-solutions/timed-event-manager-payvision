/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/TridentTools.sqlj $

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Calendar;
import java.util.Date;
import com.mes.constants.mesConstants;
import masthead.formats.visak.DetailRecord;

public class TridentTools
{
  public  static final int  VISA_K_GROUP_BASE                           = 0;
  public  static final int  VISA_K_GROUP_CASHBACK                       = 1;
  public  static final int  VISA_K_GROUP_REST                           = 2;
  public  static final int  VISA_K_GROUP_DIRECT_MKT                     = 3;
  public  static final int  VISA_K_GROUP_AUTO_RENT                      = 4;
  public  static final int  VISA_K_GROUP_HOTEL                          = 5;
  public  static final int  VISA_K_GROUP_HOTEL_NON_LODGING              = 6;
  public  static final int  VISA_K_GROUP_HOTEL_AMEX                     = 7;
  public  static final int  VISA_K_GROUP_VMC_PURCH_CARD                 = 8;
  public  static final int  VISA_K_GROUP_PT                             = 9;
  public  static final int  VISA_K_GROUP_DIRECT_DEBIT                   = 10;
  public  static final int  VISA_K_GROUP_MULTI_TERM_STORES              = 11;
  public  static final int  VISA_K_GROUP_DM_RECURRING_PMTS              = 12;
  public  static final int  VISA_K_GROUP_AUTO_RENTAL_MC                 = 13;
  public  static final int  VISA_K_GROUP_SERVICE_DEV_IND                = 14;
  public  static final int  VISA_K_GROUP_POS_CHECK_SERVICE              = 15;
  public  static final int  VISA_K_GROUP_EXISTING_DEBT_IND              = 16;
  public  static final int  VISA_K_GROUP_UNIVERSAL_CARD_AUTH            = 17;
  public  static final int  VISA_K_GROUP_ECOMM_GOODS_IND                = 18;
  public  static final int  VISA_K_GROUP_CCS_PRIV_LABEL                 = 19;
  public  static final int  VISA_K_GROUP_MERCH_VERIFY_VALUE             = 20;
  public  static final int  VISA_K_GROUP_AMEX_PURCH_CARD                = 21;
  public  static final int  VISA_K_GROUP_US_ONLY_NON_TE_COMM_CARD_LII   = 22;
  public  static final int  VISA_K_GROUP_MAP_EXTENSION                  = 23;
  public  static final int  VISA_K_GROUP_RESERVED2                      = 24;

  // groups defined, but record layout not specified as of EIS-1081 v7.1
  public  static final int  VISA_K_GROUP_NON_TE_COMM_CARD_LII           = 25;
  public  static final int  VISA_K_GROUP_VISA_ENHANCED_LIII             = 26;
  public  static final int  VISA_K_GROUP_MC_ENHANCED_LIII               = 27;
  public  static final int  VISA_K_GROUP_VISA_FLEET                     = 28;
  public  static final int  VISA_K_GROUP_MC_FLEET                       = 29;
  public  static final int  VISA_K_GROUP_AMEX_PC_CHARGE_DESC            = 30;
  public  static final int  VISA_K_GROUP_MKT_SPEC_DATA_IND              = 31;
  
  // groups defined by MES
  public  static final int  VISA_K_GROUP_DYNAMIC_DBA                    = 47;
  
  public  static final int  VISA_K_GROUP_COUNT                          = 48;
  
  
  // Visa K Capture transaction code mapping
  public static final String [][]       VisakTranCodeMap = 
  {
//   visak    credit?  EBT/DB?
    { "54",   null,    null   }, // Purchase (Online or Offline)
    { "55",   null,    null   }, // Cash Advance  
    { "56",   null,    null   }, // Purchase/Card Not Present (Online or Offline)  
    { "5B",   null,    null   }, // Bill Payment
    { "5G",   "Y" ,    null   }, // Account Funding Purchase (Online or Offline)  
    { "5H",   "Y" ,    null   }, // Account Funding Purchase/Card Not Present (Online or Off  
    { "CR",   "Y" ,    null   }, // Credit/Return (Offline)  
    
    { "5J",   null,    null   }, // Cardholder Funds Transfer (Online or Offline)  
    { "5K",   null,    null   }, // Cardholder Funds Transfer/Card Not Present (Online Offline)  
    { "76",   null,    null   }, // Check Guarantee with Conversion - Sale  
    { "77",   null,    null   }, // Check Verification with Conversion - Sale  
    { "78",   null,    null   }, // Check Conversion only - Sale  
    { "92",   "Y",     "EB"   }, // EBT/Food Stamps - Return  
    { "93",   null,    "DB"   }, // Debit Purchase (Online)  
    { "94",   "Y",     "DB"   }, // Debit Return (Online)  
    { "96",   null,    "EB"   }, // EBT/Cash Benefits - Cash withdrawal  
    { "98",   null,    "EB"   }, // EBT/Food Stamps - Purchase  
    { "9E",   null,    "EB"   }, // EBT/Food Stamps - Electronic voucher  
    { "9F",   null,    "EB"   }, // EBT/Cash Benefits - Purchase or purchase with cash  
    { "9G",   null,    "DB"   }, // Debit Account Funding Purchase (Online)  
    { "9H",   "Y",     "DB"   }, // Debit Account Funding Return(Online)  
    { "9J",   null,    "DB"   }, // Debit Cardholder Funds Transfer (Online)  
    { "9K",   "Y",     "DB"   }, // Debit Funds Transfer Return (Online)  
    { "A0",   null,    null   }, // Chip Card Transaction Advice Record (Limited Availab  
    { "FR",   null,    null   }, // Account Funding Credit/Return (Offline)  
    { "TR",   null,    null   }, // Cardholder Funds Transfer Credit/Return (Offline)  
  };
  
  protected static final String ValidPhoneNumberCharacters = "0123456789-.() ";
  
  public TridentTools()
  {
  }
  
  public static int calcMod10CheckDigit( String data )
  {
    int     retVal    = 0;
    int     sum       = 0;
    
    try
    {
      boolean dbl = true;
      
      for( int i = (data.length()-1); i >= 0; --i )
      {
        int digit = Character.digit(data.charAt(i), 10);
        if(dbl)
        {
          digit = ((digit*2) % 10) + ((digit*2) / 10);
        }
        sum += digit;
        dbl = !dbl;
      }
      retVal = ((10 - (sum % 10))%10);
    }
    catch(Exception e)
    {
      System.out.println("calcMod10CheckDigit(" + data + "): " + e.toString());
    }
    return( retVal );
  }
  
  public static double decodeAmount( String amountString )
  {
    double        retVal      = 0.0;
    StringBuffer  temp        = new StringBuffer();
    
    try
    {
      // avoiding java floating point math
      temp.append(amountString);
      temp.insert(amountString.length()-2,".");
      retVal = Double.parseDouble(temp.toString());
    }
    catch( Exception e )
    {
      System.out.println("decodeAmount(" + amountString + "): " + e.toString());
    }
    return( retVal );
  }
  
  public static String decodeCardSwipeTrackData( String input )
  {
    String          cardSwipe     = null;
    int             offsetBegin   = 0;
    int             offsetEnd     = 0;
  
    try
    {
      // remove sentinels if they are present
      switch( input.charAt(0) )
      {
        case '%':   // track 1 start sentinel
        case ';':   // track 2 start sentinel
          offsetBegin = 1;
          break;
      
        default:
          offsetBegin = 0;
          break;
      }
  
      if( (offsetEnd = input.indexOf("?")) < 0 )
      {
        offsetEnd = input.length();
      }
      cardSwipe = input.substring(offsetBegin,offsetEnd);
    }      
    catch( Exception e )
    {
      SyncLog.LogEntry("decodeCardSwipeTrackData()",e.toString());
    }
    return( cardSwipe );
  }
  
  public static String decodeBankNetRefNum  ( String tranId ) { return decodeBankNetInfo( tranId, 2 );  }
  public static String decodeBankNetDate    ( String tranId ) { return decodeBankNetInfo( tranId, 1 );  }
  public static String decodeBankNetInfo    ( String tranId ) { return decodeBankNetInfo( tranId, 0 );  }
  public static String decodeBankNetInfo    ( String tranId, int which )
  {
    String      retVal      = null;
    
    if ( tranId != null )
    {
      // MasterCard encodes the transaction 
      // identifer field with two fields
      // <BankNet Date><BankNet Reference Number>
      //
      // example: 1102MDSVVVYX2
      //          1102        -> BankNet Date
      //          MDSVVVYX2   -> BankNet Reference Number

      tranId = tranId.trim();
      if ( tranId.length() == 13 && tranId.indexOf(' ') < 0 )
      {
        int bankNetMonth  = -1;
        int bankNetDay    = -1;   
        int min           = 1;
        int max           = 0;
      
        try{ bankNetMonth = Integer.parseInt( tranId.substring(0,2) ); } catch( Exception nfe ) {}
        try{ bankNetDay   = Integer.parseInt( tranId.substring(2,4) ); } catch( Exception nfe ) {}
    
        switch( bankNetMonth )
        {
          case  2:    // february  
            max = (isLeapYear(Calendar.getInstance().get(Calendar.YEAR)) ? 29 : 28);
            break;
        
          case  4:    // april  
          case  6:    // june
          case  9:    // september
          case 11:    // november
            max = 30;
            break;
        
          case  1:    // january
          case  3:    // march
          case  5:    // may
          case  7:    // july
          case  8:    // august
          case 10:    // october
          case 12:    // december
            max = 31; 
            break;
        }
        if ( bankNetDay >= min && bankNetDay <= max )
        {
          String    tempDate    = tranId.substring(0,4);
          String    tempRefNum  = tranId.substring(4);

          if (  ! ("000000000".equals(tempRefNum))  &&
                ! ("999999999".equals(tempRefNum))  )
          {
            switch( which )
            {
              case 0: retVal  = tempDate + tempRefNum;  break;    // 0 = date & ref# (same 13 chars it should have been coming in)
              case 1: retVal  = tempDate;               break;    // 1 = date
              case 2: retVal  = tempRefNum;             break;    // 2 = reference number
            }
          }
        }
      }
    }      
    return( retVal );
  }
  
  public static String decodeCardNumber( String cardNumberFull )
  {
    String    retVal    = null;
    
    if ( cardNumberFull != null )
    {
      // remove any non-numeric values from the card number
      StringBuffer  cnFull  = new StringBuffer();
      for( int i = 0; i < cardNumberFull.length(); ++i )
      {
        char ch = cardNumberFull.charAt(i);
        if ( Character.isDigit(ch) )
        {
          cnFull.append(ch);
        }
      }
      retVal = cnFull.toString();
    }
    return( retVal );
  }
  
  public static double decodeCobolAmount( String value, int decimals )
  {
    StringBuffer          buffer    = new StringBuffer();
    char                  ch        = '\0';
    char[][]              lookup    = null;
    double                retVal    = 0.0;

    if ( value != null && value.length() > 0 )
    {
      ch = value.charAt(value.length()-1);
    
      for( int loop = 0; loop < 2 && buffer.length() == 0; ++loop )
      {
        lookup = (loop == 0) ? mesConstants.CobolPosValueEncoding :
                               mesConstants.CobolNegValueEncoding;
                          
        for( int i = 0; i < lookup.length; ++i )
        {
          if ( lookup[i][1] == ch )
          {
            buffer.append( (loop == 1) ? "-" : "" );
            buffer.append(value.substring(0,value.length()-1));
            buffer.append(lookup[i][0]);
            break;
          }
        }
      }
    
      if ( buffer.length() > 0 )
      {
        if ( decimals > 0 )
        {
          buffer.insert(buffer.length()-decimals,'.'); 
        }
        retVal = Double.parseDouble(buffer.toString());
      }
    }
    return( retVal );
  }
  
  public static String decodeVisakCardType( DetailRecord detailRec )
  {
    String    cardType  = null;
    String    tranCode  = detailRec.getTransactionCode();

    // check for pin debit / ebt (first using debit network id, then using transaction codes)
    if( detailRec.hasGroup(TridentTools.VISA_K_GROUP_DIRECT_DEBIT) )
    {
      if( "K".equals(detailRec.getNetworkIdentificationCode() ) )   cardType = "EB";
      else                                                          cardType = "DB";
    }
    else
    {
      for( int i = 0; i < VisakTranCodeMap.length; ++i )
      {
        if( tranCode.equals( VisakTranCodeMap[i][0] ) )
        {
          cardType = VisakTranCodeMap[i][2];    // values: null, "EB", "DB"
          break;
        }
      }

      if( cardType == null )
      {
        cardType = decodeVisakCardType(decodeCardNumber(detailRec.getCardholderAccountNumber()));
      }
    }

    return( cardType );
  }
  
  public static String decodeVisakCardType( String cardNumber )
  {
    int       cardBin   = -1;
    String    cardType  = null; 
    
    try
    {
      cardBin = Integer.parseInt(cardNumber.substring(0,6));
    
      if ( cardBin >= 400000 && cardBin <= 499999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_VISA; 
      }
      else if ( cardBin >= 222100 && cardBin <= 272099 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_MC; 
      }
      else if ( cardBin >= 510000 && cardBin <= 559999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_MC; 
      }
      else if ( cardBin >= 340000 && cardBin <= 349999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_AMEX; 
      }
      else if ( cardBin >= 370000 && cardBin <= 379999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_AMEX; 
      }
      else if ( cardBin >= 601100 && cardBin <= 601109 ||   // DN
                cardBin >= 601120 && cardBin <= 601149 ||   // DN
                cardBin >= 601174 && cardBin <= 601174 ||   // DN
                cardBin >= 601177 && cardBin <= 601179 ||   // DN
                cardBin >= 601186 && cardBin <= 601199 ||   // DN
                cardBin >= 644000 && cardBin <= 659999 ||   // DN
                cardBin >= 622126 && cardBin <= 622925 ||   // CUP
                cardBin >= 624000 && cardBin <= 626999 ||   // CUP
                cardBin >= 628200 && cardBin <= 628899 ||   // CUP
                cardBin >= 622926 && cardBin <= 623796 ||
                //Add the New IIN Ranges for JCB and Union Pay - Start
                cardBin >= 810000 && cardBin <= 810999 ||   // CUP 
                cardBin >= 811000 && cardBin <= 813199 ||   // CUP 
                cardBin >= 813200 && cardBin <= 815199 ||   // CUP 
                cardBin >= 815200 && cardBin <= 816399 ||   // CUP 
                cardBin >= 816400 && cardBin <= 817199 ||   // CUP 
                //Add the New IIN Ranges for JCB and Union Pay - End
                cardBin >= 300000 && cardBin <= 305999 ||   // DCI
                cardBin >= 309500 && cardBin <= 309599 ||   // DCI
                cardBin >= 360000 && cardBin <= 369999 ||   // DCI
                cardBin >= 380000 && cardBin <= 399999 )    // DCI
      {
        cardType = mesConstants.VITAL_ME_AP_DISC; 
      }
      else if ( cardBin >= 300000 && cardBin <= 305999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_DINERS; 
      }
      else if ( cardBin >= 380000 && cardBin <= 389999 ) 
      { 
        cardType = mesConstants.VITAL_ME_AP_DINERS; 
      }
      //Add the New IIN Ranges for JCB and Union Pay 
      else if (
    		  (cardBin >= 308800 && cardBin <= 309499) || //JCB
    		  (cardBin >= 309600 && cardBin <= 310299) || //JCB
    		  (cardBin >= 311200 && cardBin <= 312099) || //JCB
    		  (cardBin >= 315800 && cardBin <= 315999) || //JCB
    		  (cardBin >= 333700 && cardBin <= 334999) || //JCB
    		  (cardBin >= 352800 && cardBin <= 358999 )
    		  )
      { 
        cardType = mesConstants.VITAL_ME_AP_JCB; 
      }
      else 
      {
        cardType = mesConstants.VITAL_ME_AP_PRIVATE_LABEL; 
      }
    }
    catch( Exception e )
    {
      cardType = mesConstants.VITAL_ME_AP_ERROR;
    }
    
    return( cardType );
  }
  
  public static String decodeVisakTranDate( String visakTranDate )
  {
    return( DateTimeFormatter.getFormattedDate( decodeVisakTranDateAsDate(visakTranDate), "MMddyy" ) );
  }
  
  public static Date decodeVisakTranDateAsDate( String visakTranDate )
  {
    Calendar        cal         = Calendar.getInstance();
    String          retVal      = null;
    java.util.Date  tomorrow    = null;
    
    // establish tomorrows date
    cal.add( Calendar.DAY_OF_MONTH, 1 );
    tomorrow = cal.getTime();
    
    cal = Calendar.getInstance();
    cal.set( Calendar.MONTH, Integer.parseInt( visakTranDate.substring(0,2) ) - 1 );
    cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt( visakTranDate.substring(2,4) ) );

    if ( cal.getTime().after( tomorrow ) )
    {
      cal.add( Calendar.YEAR, -1 );
    }
    
    return( cal.getTime() );
  }
  
  public static String decodeVisakTranDateTime( String visakTranDate, String visakTranTime )
  {
    return( DateTimeFormatter.getFormattedDate(decodeVisakTranDateTimeAsDate(visakTranDate,visakTranTime), "MMddyyHHmmss") );
  }
  
  public static Date decodeVisakTranDateTimeAsDate( String visakTranDate, String visakTranTime )
  {
    Calendar    cal       = Calendar.getInstance();
    
    cal.setTime( decodeVisakTranDateAsDate(visakTranDate) );
    cal.set( Calendar.HOUR_OF_DAY, Integer.parseInt(visakTranTime.substring(0,2)) );
    cal.set( Calendar.MINUTE, Integer.parseInt(visakTranTime.substring(2,4)) );
    cal.set( Calendar.SECOND, Integer.parseInt(visakTranTime.substring(4,6)) );
    cal.set( Calendar.MILLISECOND, 0 );
    
    return( cal.getTime() );
  }
  
  public static String encodeString( String value, int fieldLength )
  {
    StringBuffer          temp      = new StringBuffer("");
    
    temp.append(value);
    
    while( temp.length() < fieldLength )
    {
      temp.append(" ");
    }
    
    return( temp.toString() );
  }
  
  public static String encodeDouble( double number, int fieldLength )
  {
    StringBuffer          temp      = new StringBuffer();
    
    temp.append(Double.toString(number));
    
    while( temp.length() < fieldLength )
    {
      temp.insert(0, "0");
    }
    
    return( temp.toString() );
  }
  
  public static String encodeInteger( int number, int fieldLength )
  {
    StringBuffer          temp      = new StringBuffer();
    
    temp.append(Integer.toString(number));
    
    while( temp.length() < fieldLength )
    {
      temp.insert(0, "0");
    }
    
    return( temp.toString() );
  }
  
  public static String encodeDate( Date val, String format )
  {
    return( DateTimeFormatter.getFormattedDate(val, format) );
  }
  
  /**
   * Encodes a USD amount to it's whole number VisaD equivalent. <br />
   * For example $5.00 USD is converted to 500.<br />
   * For situations where multiple currencies may be used, call encodeAmount(amount, fieldLength, currencyCode) instead.
   * @param amount
   * @param fieldLength The minimum width of the field.  The amount will be zero padded to the width.
   * @return The converted VisaD amount.
   */
  public static String encodeAmount( double amount, int fieldLength )
  {
    return( encodeAmount(amount,fieldLength,2) );   // default to 2 decimals
  }
  
  /**
   * Encodes an amount to it's whole number VisaD equivalent. <br />
   * For example $5.00 USD is converted to 500.<br />
   * Zero-place decimal currencies such as Yen, have anything past the decimal place removed: $500.00 becomes 500 (not 50000). <br />
   * <b>Be sure to enforce that zero-place decimal currencies have zero 'cents' or is a whole number before calling this function.</b>
   * @param amount
   * @param fieldLength The minimum width of the field.  The amount will be zero padded to the width.
   * @param currencyCode
   * @return The converted VisaD amount.
   */
  public static String encodeAmount(double amount, int fieldLength, String currencyCode)
  {
    int     decDigits     = 2;
    int     numericCode   = -1;
    
    try 
    {
      numericCode = Integer.parseInt(currencyCode);
    }
    catch( NumberFormatException e ) { }
    
    switch(numericCode) 
    {
      // Zero decimal place currencies
      case 174:   // KMF
      case 262:   // DJF
      case 360:   // IDR
      case 392:   // JPY
      case 310:   // KRW
      case 548:   // VUV
      case 600:   // PYG
      case 646:   // RWF
      case 950:   // XAF
      case 952:   // XOF
      case 953:   // XPF
      case 152:   // CLP
      case 800:   // UGX
        decDigits = 0;
        break;
        
      case 352:   // ISK
        decDigits = 2;  
        break;
    
      // Three decimal place currencies
      case 368:   // IQD
      case 414:   // KWD
      case 512:   // OMR
      case 788:   // TND
        decDigits = 3;
        break;
    }
    return( encodeAmount(amount,fieldLength,decDigits) );
  }
   
  public static String encodeAmount(double amount, int fieldLength, int decDigits)
  {
    StringBuffer          temp      = new StringBuffer();
    
    // toFractionalAmount will clean up any Java floating point 
    // inaccuracy (i.e. when value sb 28.00 and is 27.9999999998)
    temp.append(MesMath.toFractionalAmount(Math.abs(amount),decDigits,decDigits));
    
    // remove any formatting characters 
    for( int i = 0; i < temp.length(); ++i )
    {
      switch( temp.charAt(i) )
      {
        case '$':
        case ',':
        case '.':
        case '(':
        case ')':
          temp.deleteCharAt(i);
          break;
          
        default:
          break;
      }
    }
    
    while( temp.length() < fieldLength )
    {
      temp.insert(0,"0");
    }
    
    return( temp.toString() );
  }
  
  public static String encodeCobolAmount( double amount, int fieldLength )
  {
    char                  ch        = '\0';
    char[][]              lookup    = null;
    StringBuffer          temp      = new StringBuffer();
    
    temp.append( encodeAmount(amount,fieldLength) );

    if( amount < 0.0 )
    {
      lookup = mesConstants.CobolNegValueEncoding;
    }
    else
    {
      lookup = mesConstants.CobolPosValueEncoding;
    }
    ch    = temp.charAt(temp.length()-1);
    for( int i = 0; i < lookup.length; ++i )
    {
      if ( lookup[i][0] == ch )
      {
        temp.setCharAt(temp.length()-1,lookup[i][1]);
        break;
      }
    }
    return( temp.toString() );        
  }
  
  public static String encodeCardNumber( String input )
  {
    String          cardNumber  = input.trim();
    StringBuffer    ecard       = new StringBuffer();
    
    try
    {
      ecard.append( cardNumber.substring(0,6) );
      ecard.append( "xxxxxx" );
      ecard.append( cardNumber.substring( (cardNumber.length()-4) ) );
    }
    catch( Exception e )
    {
      return( cardNumber );
    }
    
    return( ecard.toString() );
  }
  
  public static String encodeCardSwipe( String input )
  {
    String          cardNumber  = null;
    StringBuffer    eswipe      = new StringBuffer(input);
    int             end         = 0;
    int             fill        = 0;
    int             start       = 0;
    
    // extract the full card number
    if ( input.charAt(0) == 'B' )
    {
      start = 1;
      end   = input.indexOf("^");
      fill  = input.indexOf("^",end+1);
    }
    else
    {
      start = 0;
      end   = input.indexOf("=");
      fill  = end;
    }
    
    cardNumber  = removeSpaces(input.substring(start,end));
    eswipe.replace(start,end,encodeCardNumber(cardNumber));
    
    for( int i = (fill+5); i < eswipe.length(); ++i )
    {
      eswipe.setCharAt(i,'X');
    }
    return( eswipe.toString() );
  }
  
  public static String getCardNumberFull( String cardNumberEnc )
  {
    String retVal;
    
    try
    {
      retVal = new String(MesEncryption.getClient().decrypt(cardNumberEnc));
    }
    catch( Exception e )
    {
      retVal = "";
    }
    return( retVal );
  }
  
  public static String getFormattedPhone( String phoneData )
  {
    return( getFormattedPhone(phoneData,true) );
  }
  
  public static String getFormattedPhone( String phoneData, boolean includeHyphen )
  {
    StringBuffer    retVal    = new StringBuffer("");
    
    for( int i = 0; i < phoneData.length(); ++i )
    {
      char ch = phoneData.charAt(i);
      if ( Character.isDigit(ch) )
      {
        if( i != 0 || ch != '1' )retVal.append(ch);
      }
    }
    if ( includeHyphen )
    {
      retVal.insert(3,"-");
    }
    return( retVal.toString() );
  }
  
  public static boolean mod10Check(String data)
  {
    int     checkDigit  = 0;
    boolean result      = false;
    
    try
    {
      checkDigit  = calcMod10CheckDigit( data.substring(0,data.length()-1) );
      result      = (Character.digit(data.charAt(data.length()-1), 10) == checkDigit);
    }
    catch(Exception e)
    {
      System.out.println("mod10Check(" + data + "): " + e.toString());
    }
    return( result );
    
  }
  
  public static boolean isDisplayGroup( int groupId )
  {
    boolean   retVal    = true;
    
    switch( groupId )
    {
      case VISA_K_GROUP_MAP_EXTENSION:
      case VISA_K_GROUP_RESERVED2:
      case VISA_K_GROUP_NON_TE_COMM_CARD_LII:
      case VISA_K_GROUP_VISA_FLEET:
      case VISA_K_GROUP_MC_FLEET:
        retVal = false;   // hide these groups from JSP display
        break;
    }
    return( retVal );
  }
  
  public static boolean isLeapYear( int year )
  {
    boolean     retVal      = false;
    
    // check for a 2 digit year and adjust if necessary
    if (year < 100) 
    {
      // if the year is greater than 40 assume it
      // is from 1900's.  if the year is less than
      // 40 assume it is from 2000's.
      year += ((year > 40) ? 1900 : 2000);
    }

    // is year divisible by 4?
    if (year % 4 == 0) 
    {
      if (year % 100 != 0)      // is year divisible by 4 but not 100?  
      {
        retVal = true;
      }
      else if (year % 400 == 0) // is year divisible by 4 and 100 and 400?
      {
        retVal = true;
      }
    }
    return( retVal );
  }
  
  public static boolean isValidMerchantPhone( String cityPhone )
  {
    boolean       retVal    = false;
    String        testValue = null;
    
    if ( cityPhone != null )
    {
      testValue = cityPhone.trim();
      retVal = true;    // assume true
      
      // scan the string and verify that all chars are valid for a phone #
      for( int i = 0; i < testValue.length(); ++i )
      {
        if ( ValidPhoneNumberCharacters.indexOf( testValue.charAt(i) ) < 0 )
        {
          retVal = false;
          break;
        }
      }
    }
    
    if ( retVal )
    {
      StringBuffer buffer = new StringBuffer();
      
      for ( int i = 0; i < testValue.length(); ++i )
      {
        char ch = testValue.charAt(i);
        if ( Character.isDigit( ch ) )
        {
          buffer.append(ch);
        }
      }
      retVal = (buffer.length() == 10) || (buffer.length() == 11 && buffer.charAt(0) == '1');
    }
    
    return( retVal );
  }
  
  public static boolean isValidSeNumber( String seNumber )
  {
    int                 checkDigit  = -1;
    StringBuffer        buffer      = new StringBuffer(seNumber);
    boolean             retVal      = true;
    int                 sum1        = 0;
    int                 sum2        = 0;
    int                 tempInt     = 0;
    
    try
    {
      if ( seNumber.length() != 10 )    // incorrect length
      {
        retVal = false;
      }
      else
      {
        tempInt = Integer.parseInt(seNumber.substring(0,3));
        
        if ( tempInt < 930 || tempInt > 939 )
        {
          buffer.setCharAt(0,'0');    // replace first digit with 0
        }
        
        retVal = mod10Check(buffer.toString());
      }
    }
    catch( Exception ee )
    {
      retVal = false;
    }
    
    return( retVal );
  }
  
  public static boolean isVisakCardPresent( DetailRecord detailRec )
  {
    String      idMethod    = detailRec.getCardholderIdentificationCode();
    boolean     retVal      = true;   // assume card present
    
    // N = Address Verification, CPS/Card Not Present or e-Commerce
    if ( idMethod.equals("N") )
    {
      retVal = false;
    }
    return( retVal );
  }
  
  public static boolean isVisakCashAdvance( String tranCode )
  {
    boolean     retVal      = false;
    
    if ( tranCode != null && tranCode.equals("55") )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public static boolean isVisakCredit( String tranCode )
  {
    boolean     retVal      = false;
    
    for ( int i = 0; i < VisakTranCodeMap.length; ++i )
    {
      if ( tranCode.equals( VisakTranCodeMap[i][0] ) )
      {
        retVal =  ( VisakTranCodeMap[i][1] != null &&
                    VisakTranCodeMap[i][1].equals("Y") );
        break;
      }
    }
    return( retVal );
  }
  
  public static boolean isVisakEntryModeSwiped( String dataSource )
  {
    boolean     retVal = false;
    
    if ( dataSource.equals("D") ||  // Full Track 2 Magnetic-stripe read 
         dataSource.equals("H") ||  // Full Track 1 Magnetic-stripe read
         dataSource.equals("S") ||  // Full Magnetic strip read Chip Card capable terminal (Visa MasterCard and JCB Transactions Only)
         dataSource.equals("W") ||  // Chip Card processed as MSR due to terminal and card not having common AID
         dataSource.equals("Z") )	// Chip Card processed as MSR due to terminal or card failure
    {
      retVal = true;
    }         
    return( retVal );
  }
  
  public static String removeSpaces( String input )
  {
    StringBuffer    buffer    = new StringBuffer();
    char            ch;
    
    if ( input != null )
    {
      for( int i = 0; i < input.length(); ++i )
      {
        if ( (ch = input.charAt(i)) != ' ' ) 
        {
          buffer.append(ch);
        }
      }
    }
    return( buffer.toString() );
  }
  
  public static String toCurrency( String amountString )
  {
    return( MesMath.toCurrency(decodeAmount(amountString)) );
  }
};
