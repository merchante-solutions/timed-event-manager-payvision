/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesSystem.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/15/03 3:03p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

public class MesSystem
{
  public static long    DEAD_REQUEST_TIMEOUT              = 480000L;
  public static long    WATCHDOG_SLEEP_TIMER              = 60000L;
  
  public static String  MES_SYSTEM_VERSION                = "1.0";
  
  public static class SystemWatchdogThread implements Runnable
  {
    Thread          WatchDogThread      = null;
    
    public SystemWatchdogThread( )
    {
      WatchDogThread = new Thread(this); 
      WatchDogThread.start();
    }
    
    public void run( )
    {
      MesHttpRequest          req       = null;
      
      try
      {
        while(true)
        {
          Thread.sleep(WATCHDOG_SLEEP_TIMER);
          
          if ( ActiveRequests != null )
          {
            for( int i = 0; i < ActiveRequests.size(); ++i )
            {
              req = (MesHttpRequest) ActiveRequests.elementAt(i);
              if ( req.getActiveTime() > DEAD_REQUEST_TIMEOUT )
              {
                ActiveRequests.removeElement(req);
              }
            }
          }            
        }
      }
      catch( InterruptedException e )
      {
      }
      catch( Exception e )
      {
        System.out.println( e.toString() );
      }        
    }
  }
  public static class MesHttpRequest
  {
    String                      LoginName         = null;
    Object                      RequestingObject  = null;
    long                        RequestTS         = 0L;
    HttpServletRequest          ServletRequest    = null;
    
    public MesHttpRequest( Object ref, String loginName, HttpServletRequest request )
    {
      RequestingObject  = ref;
      LoginName         = loginName;
      ServletRequest    = request;
      RequestTS         = System.currentTimeMillis();
    }
    
    public long getActiveTime( )
    {
      return( (System.currentTimeMillis() - RequestTS) );
    }
    
    public String getLoginName( )
    {
      return( (LoginName == null) ? "N/A" : LoginName );
    }
    
    public Object getRequestObject( )
    {
      return( RequestingObject );
    }
    
    public HttpServletRequest getServletRequest()
    {
      return( ServletRequest );
    }
  }

  private static  Vector                ActiveRequests    = new Vector();
//  private static  MesAgentServer        AgentServer       = new MesAgentServer();
  private static  SystemWatchdogThread  WatchDog          = new SystemWatchdogThread();
  
  // cannot instantiate this object, just a collection of static methods
	private MesSystem( )    
  {
    
  }
  
  public static void clearActiveRequests()
  {
    ActiveRequests.removeAllElements();
  }
  
  public static MesHttpRequest findRequest( Object ref )
  {
    MesHttpRequest          retVal  = null;
    MesHttpRequest          temp    = null;
    
    for( int i = 0; i < ActiveRequests.size(); ++i )
    {
      temp = (MesHttpRequest)ActiveRequests.elementAt(i);
      if ( temp.getRequestObject() == ref )
      {
        retVal = temp;
        break;
      }
    }
    return( retVal );
  }
  
  public static int getActiveRequestCount()
  {
    return( ActiveRequests.size() );
  }
  
  public static String getRequestLoginName( Object ref )
  { 
    MesHttpRequest            mesRequest  = findRequest(ref);
    String                    retVal      = "No Matching Request";
    
    if ( mesRequest != null )
    {
      retVal = mesRequest.getLoginName();
    }
    return( retVal );
  }
  
  public static void registerRequest(Object ref, String loginName, HttpServletRequest request )
  {
    MesHttpRequest      mesRequest = findRequest(ref);
    
    if ( mesRequest == null )
    {
      ActiveRequests.addElement(new MesHttpRequest(ref,loginName,request));
    }
  }
  
  public static void unregisterRequest(Object ref)
  {
    MesHttpRequest        temp = null;
    
    while( ( temp = findRequest(ref) ) != null )
    {
      ActiveRequests.removeElement(temp);
    }
  }
}

