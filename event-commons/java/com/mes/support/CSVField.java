/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVField.java $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/12/01 3:49p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

public class CSVField
{
  public static final int   FIELD_TYPE_QUOTED       = 1;
  public static final int   FIELD_TYPE_UNQUOTED     = 2;
  
  private int     fieldType;
  private String  fieldData;
  
  public CSVField()
  {
  }
  
  public CSVField(int fieldType, String fieldData)
  {
    this.fieldType = fieldType;
    this.fieldData = fieldData;
  }
  
  public void setData(int fieldType, String fieldData)
  {
    this.fieldType = fieldType;
    this.fieldData = fieldData;
  }
  
  public String toString()
  {
    StringBuffer    result = new StringBuffer("");
    
    switch(fieldType)
    {
      case FIELD_TYPE_QUOTED:
        result.append("\"");
        break;
        
      case FIELD_TYPE_UNQUOTED:
      default:
        break;
    }
    
    result.append(fieldData);
    
    switch(fieldType)
    {
      case FIELD_TYPE_QUOTED:
        result.append("\"");
        break;
        
      case FIELD_TYPE_UNQUOTED:
      default:
        break;
    }
    
    return result.toString();
  }
}
