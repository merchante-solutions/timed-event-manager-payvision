/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesEncryption.java $

  Description:  

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-28 16:16:49 -0700 (Tue, 28 Jul 2015) $
  Version            : $Revision: 23754 $

  Change History:
     See SVN Databse

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import masthead.encrypt.EncryptionClient;
import masthead.encrypt.LargeDataEncryptionClient;
import masthead.util.Util13;

public class MesEncryption
{
  static { LoggingConfigurator.configure(); }
  static Logger log = Logger.getLogger(MesEncryption.class);
  
  private static final String USE_RESTAPI = "encryption.new.flag";
  private static final String HEX_ENCODING = "hex";
  
  private static final String USE_SESSION_KEY = "encryption.sessionKey.flag";
  
  static private HashMap clients = new HashMap();
  static private String             PropertiesFilename  = "mes.properties";
  
  public static byte[] decryptLargeData( byte[] data )
    throws Exception
  {
    return( getLargeClient().decrypt(data) );
  }
  
  public static String decryptString( String ksnHex, String encryptedString )
    throws Exception
  {
    byte[] ksn            = Util13.parseHexString(ksnHex);
    byte[] encryptedBytes = Util13.parseHexString(encryptedString);
    return( new String(getClient("idt.encryption.host.name").decrypt(ksn,encryptedBytes)) );
  }        
  
  public static String decryptString( String encryptedString )
    throws Exception
  {
    byte[] encryptedBytes = Util13.parseHexString(encryptedString);
    return( new String(getClient().decrypt(encryptedBytes)) );
  }
  
  public static String decryptString(String encryptedString, boolean logBytes,boolean useSessionKeyFlag) throws Exception {
	  byte[] encryptedBytes = Util13.parseHexString(encryptedString);

	  EncryptionClient client = getClient();
	  
	  if(useSessionKeyFlag){
		  PropertiesFile  propsFile = new PropertiesFile( PropertiesFilename );
      	  client.setEnableSessionKey(propsFile.getBoolean(USE_SESSION_KEY));
      }
	  
	  byte[] bytes = client.decrypt(encryptedBytes);
	  String retVal = new String(bytes);
	  if (logBytes) {
		  if (retVal == null || retVal.length() == 0) {
			  log.error("decryptString() returning empty card number for " + encryptedString);
		  }
		  else if (!TridentTools.mod10Check(retVal)) {
			  log.error("decryptString() returning invalid card number for " + encryptedString + ":" + Util13.hexStringN(bytes));
		  }
	  }
	  return retVal;
  }

  public static void destroy()
  {
    clients.clear();
  }
  
  public static byte[] encryptLargeData( byte[] data )
    throws Exception
  {
    return( getLargeClient().encrypt(data) );
  }
  
  public static EncryptionClient getClient()
  {
    return( getClient( PropertiesFilename,"encryption.host.name" ) );
  }
  
  public static EncryptionClient getClient( String hostParam )
  {
    return( getClient( PropertiesFilename,hostParam ) );
  }
  
  public static EncryptionClient getClient( String propsFilename, String hostParam )
  {
    EncryptionClient client = (EncryptionClient) clients.get(hostParam);
    
    if ( client == null )
    {
      try
      {
        String       hostDefault = "";
        String       portDefault = "";
        PropertiesFile  propsFile = new PropertiesFile( propsFilename );
        try {
          hostDefault = MesDefaults.getString(MesDefaults.MES_ENCRYPTION_HOST_DEFAULT);
          portDefault = MesDefaults.getString(MesDefaults.MES_ENCRYPTION_PORT_DEFAULT);
        } catch (Exception ex) {
          //nothing should be changed
        }
        
        client = new masthead.encrypt.EncryptionClient(propsFile.getString(hostParam,hostDefault),
                                                       Integer.parseInt( propsFile.getString("encryption.host.port",portDefault) ));
        client.setEncryptionFlag(propsFile.getBoolean(USE_RESTAPI));
        clients.put(hostParam, client);
      }
      catch(Exception e)
      {
      }
    }
    return( client );
  }
  
  public static LargeDataEncryptionClient getLargeClient()
  {
	  EncryptionClient client = getClient();
		client.setEncoding(HEX_ENCODING);
		return (new masthead.encrypt.LargeDataEncryptionClient(client));
  }
  
  public static void setPropertiesFilename( String propsFilename )
  {
    PropertiesFilename = propsFilename;
  }
  
  public static void main( String[] args )
  {
    byte[]  value = null;
    
    try
    {
      if( args[0].equals("encrypt") )
      {
        System.out.println("original value: " + args[1]);
        value = MesEncryption.getClient().encrypt(args[1].getBytes());
        System.out.println("encrypted value: " + value);
        value = MesEncryption.getClient().decrypt(value);
        System.out.println("decrypted value: " + new String(value));
      }
      else if ( args[0].equals("decrypt") )
      {
        value = MesEncryption.getClient().decrypt(args[1].getBytes());
      }
      else if ( args[0].equals("largedata") )
      {
        String testStr = "this is not a very long test string";
        byte[] retVal = MesEncryption.encryptLargeData( testStr.getBytes() );
        System.out.println("enc value: " + new String(retVal));
        System.out.println("dec value: " + new String( MesEncryption.decryptLargeData( retVal )));
      }
      System.out.println(value);
    }
    catch( Exception e )
    {
    }
    finally
    {
    }      
  }
}