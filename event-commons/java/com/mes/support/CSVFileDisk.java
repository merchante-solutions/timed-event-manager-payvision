/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVFileDisk.java $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/31/03 4:49p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;

public class CSVFileDisk extends CSVFile
{
  String            Filename          = null;
  int               RowCount          = 0;

  public CSVFileDisk( String filename )
  {
    super();
    Filename = filename;
  }
  
  public CSVFileDisk(String filename, ResultSet rs, boolean withHeader)
  {
    super(rs,withHeader);
    Filename = filename;
  }
  
  public void addRow(ResultSet rs)
  {
    addRow(rs,CSVRow.DATE_MMDDYY);
  }
  
  public void addRow(ResultSet rs, String dateFormat)
  {
    BufferedWriter        out     = null;
    CSVRow                row     = new CSVRow();
    
    try
    {
      out = new BufferedWriter( new FileWriter( Filename, true ) );
      row.clear();
      row.addFields(rs, dateFormat );
      out.write(row.toString());
      ++RowCount;
      row = null;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::addRow()", e.toString());
    }
    finally
    {
      try{ out.close(); } catch(Exception e){}
    }
  }
  
  public void addRows(ResultSet rs, String dateFormat)
  {
    BufferedWriter        out     = null;
    CSVRow                row     = new CSVRow();
    
    try
    {
      out = new BufferedWriter( new FileWriter( Filename, true ) );
      while(rs.next())
      {
        row.clear();
        row.addFields(rs, dateFormat );
        out.write(row.toString());
        ++RowCount;
      }
      row = null;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::addRows()", e.toString());
    }
    finally
    {
      try{ out.close(); } catch(Exception e){}
    }
  }
  
  public void clear()
  {
    File      file    = new File(Filename);
    
    try
    {
      file.delete();
      RowCount = 0;
    }
    catch( Exception e )
    {
      // ignore
    }
    file = null;
  }
  
  public String getFtpFilename( )
  {
    StringBuffer        fullPath = new StringBuffer( System.getProperty("user.dir") );
    fullPath.append( File.separator  );
    fullPath.append( getFilename() );
    return( fullPath.toString() );
  }
  
  public String getFilename( )
  {
    return(Filename);
  }
  
  public int rowCount()
  {
    return( RowCount );
  }
  
  public void setHeader(ResultSet rs)
  {
    setHeader( new CSVRow(rs, true) );
  }
  
  public void setHeader(CSVRow row)
  {
    BufferedWriter    out     = null;
    
    try
    {
      // overwrite existing file
      out = new BufferedWriter( new FileWriter( Filename, false ) );
      out.write(row.toString());
      row = null;
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setHeader()", e.toString());
    }
    finally
    {
      try{ out.close(); } catch(Exception e){}
    }
  }
}
