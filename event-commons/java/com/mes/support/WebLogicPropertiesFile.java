/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/WebLogicPropertiesFile.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-06-01 12:23:38 -0700 (Fri, 01 Jun 2012) $
  Version            : $Revision: 20223 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.InputStream;

public class WebLogicPropertiesFile extends PropertiesFile
{
  public WebLogicPropertiesFile( ) 
  {
    super();
  }
  
  public WebLogicPropertiesFile( String filename )
  {
    super( filename );
  }
  
  public WebLogicPropertiesFile( InputStream inputStream )
  {
    super( inputStream );
  }
  
  public void load( String filename )
  {
    String        weblogicPath    = null;
    try
    {
      weblogicPath = System.getProperties().getProperty("user.dir");
    }
    catch( Exception e )
    {
    }
    
    // if the property is not found, then user the
    // current directory.
    if ( weblogicPath == null )
    {
      weblogicPath = ".";
    }
    super.load( weblogicPath + java.io.File.separator + filename );
  }
}