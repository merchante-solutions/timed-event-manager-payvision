/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVRow.java $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/04/01 2:52p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Time;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

public class CSVRow
{
  public static final   String    DATE_MMDDYY       = "MM/dd/yy";
  public static final   String    DATE_MMDDYYYY     = "MM/dd/yyyy";
  
  public static final   String    TIME_HHMMSS       = "hh:mm:ss";
  public static final   String    TIME_HHMM         = "hh:mm";
  
  private static final  String    defaultDateFormat = DATE_MMDDYY;
  
  private               Vector    fields            = new Vector();
  
  public CSVRow()
  {
  }
  
  public CSVRow(ResultSet rs, boolean isHeader)
  {
    if(isHeader)
    {
      addHeaderFields(rs);
    }
    else
    {
      addFields(rs, defaultDateFormat);
    }
  }
  
  public CSVRow(ResultSet rs, boolean isHeader, String dateFormat)
  {
    if(isHeader)
    {
      addHeaderFields(rs);
    }
    else
    {
      addFields(rs, dateFormat);
    }
  }
  
  public String toString()
  {
    StringBuffer    result = new StringBuffer("");
      
    for(int i=0; i < fields.size(); ++i)
    {
      CSVField field = (CSVField)fields.elementAt(i);
      result.append(field.toString());
      
      if(i < (fields.size() - 1))
      {
        // add a comma
        result.append(",");
      }
    }
    
    result.append("\n");
    
    return result.toString();
  }
  
  public void clear()
  {
    fields.clear();
  }
  
  public void addField(CSVField field)
  {
    fields.add(field);
  }
  
  public void addField(int fieldType, String fieldData)
  {
    fields.add(new CSVField(fieldType, fieldData));
  }
  
  public void addField(int fieldType, double fieldData)
  {
    fields.add(new CSVField(fieldType, Double.toString(fieldData)));
  }
  
  public void addField(int fieldType, int fieldData)
  {
    fields.add(new CSVField(fieldType, Integer.toString(fieldData)));
  }
  
  public void addField(int fieldType, long fieldData)
  {
    fields.add(new CSVField(fieldType, Long.toString(fieldData)));
  }
  
  public void addField(int fieldType, Date fieldData)
  {
    addField( fieldType, fieldData, "MM/dd/yyyy" );
  }
  
  public void addField(int fieldType, Date fieldData, String format)
  {
    fields.add(new CSVField(fieldType, DateTimeFormatter.getFormattedDate(fieldData,format) ));
  }
  
  public void addHeaderFields(ResultSet rs)
  {
    try
    {
      ResultSetMetaData md      = rs.getMetaData();
      
      for(int col = 1; col <= md.getColumnCount(); ++col)
      {
        addField(CSVField.FIELD_TYPE_QUOTED, md.getColumnName(col));
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::addHeaderFields()", e.toString());
    }
  }
  
  public void addFields(ResultSet rs, String dateFormat)
  {
    try
    {
      ResultSetMetaData   md        = rs.getMetaData();
      DateFormat          df        = new SimpleDateFormat(dateFormat);
      DateFormat          tf        = new SimpleDateFormat("hh:mm:ss");
      Date                tempDate  = null;
      Time                tempTime  = null;
      
      // add one field for each column of the result set
      for(int col = 1; col <= md.getColumnCount(); ++col)
      {
        int     fieldType;
        String  fieldData;
        
        // set the field type
        switch(md.getColumnType(col))
        {
          case Types.CHAR:
          case Types.VARCHAR:
          case Types.LONGVARCHAR:
            fieldType = CSVField.FIELD_TYPE_QUOTED;
            fieldData = rs.getString(col);
            break;
            
          case Types.NUMERIC:
          case Types.DECIMAL:
          case Types.BIT:
          case Types.TINYINT:
          case Types.SMALLINT:
          case Types.INTEGER:
          case Types.BIGINT:
          case Types.REAL:
          case Types.FLOAT:
          case Types.DOUBLE:
            fieldType = CSVField.FIELD_TYPE_UNQUOTED;
            fieldData = rs.getString(col);
            break;
          
          case Types.DATE:
          case Types.TIMESTAMP:
            fieldType = CSVField.FIELD_TYPE_UNQUOTED;
            tempDate = rs.getDate(col);
            
            if(tempDate == null)
            {
              fieldData = "";
            }
            else
            {
              fieldData = df.format(rs.getDate(col));
            }
            break;
          
          case Types.TIME:
            fieldType = CSVField.FIELD_TYPE_UNQUOTED;
            tempTime = rs.getTime(col);
            
            if(tempTime == null)
            {
              fieldData = "";
            }
            else
            {
              fieldData = tf.format(rs.getTime(col));
            }
            break;
          
          default:
            fieldType = CSVField.FIELD_TYPE_UNQUOTED;
            fieldData = rs.getString(col);
            break;
        }
        
        if(fieldData == null)
        {
          fieldData = "";
        }
        
        // add the field
        addField(fieldType, fieldData);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::addFields()", e.toString());
    }
  }
}
