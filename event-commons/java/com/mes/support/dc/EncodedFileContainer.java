package com.mes.support.dc;

import java.io.InputStream;
import java.io.OutputStream;

public interface EncodedFileContainer extends FileContainer
{
  public void setEncodedData(InputStream in) throws Exception;
  public void setEncodedData(byte[] data) throws Exception;
  public InputStream getEncodedInputStream();
  public OutputStream getEncodedOutputStream();
  public int writeEncodedData(OutputStream out);
  public int encodeStream(InputStream in, OutputStream out);
  public int decodeStream(InputStream in, OutputStream out);
}