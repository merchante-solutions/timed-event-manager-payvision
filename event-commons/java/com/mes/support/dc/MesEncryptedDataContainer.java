package com.mes.support.dc;

/**
 * Data container object that provides encryption.
 *
 * Useful for storing small amounts of sensitive data such as a credit card 
 * account number.
 */
public class MesEncryptedDataContainer extends MesDataContainer
{
  public MesEncryptedDataContainer()
  {
    // create encryption encoder
    super(new EncryptionEncoder(new MesEncryptionHandler()));
  }
}