package com.mes.support.dc;

import java.io.InputStream;

/**
 * Data container object that provides compression + encryption.
 *
 * Useful for storing large amounts of sensitive data in database blobs.
 */
public abstract class MesDataContainer implements EncodedDataContainer
{
  private EncodedDataContainer dc;

  public MesDataContainer()
  {
    throw new RuntimeException(
      "MesDataContainer instance must initialize internal data container");
  }
  public MesDataContainer(Encoder e)
  {
    dc = new EncodedDataContainerImpl(e);
  }

  public void setData(InputStream in) throws Exception
  {
    dc.setData(in);
  }

  public void setData(byte[] data)
  {
    dc.setData(data);
  }
    
  public void setData(String dataStr)
  {
    dc.setData(dataStr);
  }

  public byte[] getData()
  {
    return dc.getData();
  }

  public boolean empty()
  {
    return dc.empty();
  }

  public int size()
  {
    return dc.size();
  }

  public void setEncodedData(byte[] encData)
  {
    dc.setEncodedData(encData);
  }

  public void setEncodedData(InputStream in) throws Exception
  {
    dc.setEncodedData(in);
  }

  public byte[] getEncodedData()
  {
    return dc.getEncodedData();
  }
}