package com.mes.support.dc;

import java.io.InputStream;
import java.io.OutputStream;

public interface FileContainer
{
  public void setData(InputStream in);
  public void setData(byte[] data);
  public void setData(String data);
  public InputStream getInputStream();
  public OutputStream getOutputStream();
  public boolean empty();
  public long size();
  public void release();
  public int writeData(OutputStream out);
}