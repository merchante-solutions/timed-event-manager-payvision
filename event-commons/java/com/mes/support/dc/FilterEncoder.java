package com.mes.support.dc;

import org.apache.log4j.Logger;

public abstract class FilterEncoder implements Encoder
{
  static Logger log = Logger.getLogger(FilterEncoder.class);

  private Encoder encoder;

  public FilterEncoder(Encoder encoder)
  {
    this.encoder = encoder;
  }
  public FilterEncoder()
  {
    this(new Encoder() { 
      public byte[] encode(byte[] data) { return data; }
      public byte[] decode(byte[] data) { return data; }
    });
  }

  public abstract byte[] enfilter(byte[] data) throws Exception;

  public abstract byte[] defilter(byte[] data) throws Exception;

  /**
   * unfiltered -> loc filter -> chain encode -> unfiltered
   */
  public byte[] encode(byte[] data) throws Exception
  {
    // encode locally first, then pass to chained encoder
    return encoder.encode(enfilter(data));
  }

  /**
   * filtered -> chain decode -> loc defilter -> unfiltered
   */
  public byte[] decode(byte[] data) throws Exception
  {
    // decode with chained encoder first, then defilter locally
    // (reverse of encoding order to correctly decode in order)
    return defilter(encoder.decode(data));
  }
}