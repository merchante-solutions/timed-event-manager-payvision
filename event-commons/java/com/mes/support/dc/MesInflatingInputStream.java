package com.mes.support.dc;

import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;

public class MesInflatingInputStream extends MesFilterInputStream
{
  static Logger log = Logger.getLogger(MesInflatingInputStream.class);
  
  public static final int     DFLT_INF_READ_SIZE  = 500020;
  
  private CompressionHandler compressor;
  
  public MesInflatingInputStream(InputStream in, int readSize)
  {
    super(in,readSize);
    this.compressor = new MesCompressionHandler();
  }
  public MesInflatingInputStream(InputStream in)
  {
    this(in,DFLT_INF_READ_SIZE);
  }
  
  protected boolean isBlockEncoded()
  {
    return true;
  }
  
  protected EncodingType getEncodingType()
  {
    return EncodingType.MES_BLK_COMPRESS_1;
  }
  
  /**
   * Takes byte buffer with data to be encrypted and a length.  Encrypts
   * data and returns byte array containing an encrypted data block length 
   * prefix followed by encrypted data: <length><SEPARATOR><encrypted data>
   */
  protected byte[] filterData(byte[] data, int dataCount) throws Exception
  {
    // data to be encrypted needs to be in exactly sized buffer
    // should already be in correctly sized buffer 
    if (data.length != dataCount)
    {
      throw new IOException("Data block incorrect size");
    }
      
    // encrypt the data
    return compressor.decompress(data);
  }
} 

