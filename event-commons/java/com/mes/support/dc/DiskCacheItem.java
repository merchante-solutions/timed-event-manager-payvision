package com.mes.support.dc;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.log4j.Logger;

public class DiskCacheItem
{
  static Logger log = Logger.getLogger(DiskCacheItem.class);
  
  private DiskCache dc;
  
  public DiskCacheItem(DiskCache dc)
  {
    this.dc = dc;
  }
  
  private void validate()
  {
    dc.validate(this);
  }
  
  public boolean empty()
  {
    validate();
    return dc.empty(this);
  }
  
  public long size()
  {
    validate();
    return dc.size(this);
  }
  
  public String filename()
  {
    validate();
    return dc.filename(this);
  }
  
  public void delete()
  {
    validate();
    dc.delete(this);
  }
  
  public InputStream getInputStream()
  {
    validate();
    try
    {
      return dc.getInputStream(this);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  public OutputStream getOutputStream()
  {
    validate();
    try
    {
      return dc.getOutputStream(this);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  // expose this for testing...
  public File file()
  {
    return dc.file(this);
  }
  
  public String toString()
  {
    return "DiskCacheItem [ "
      + "fileName: " + filename()
      + " ]";
  }
}