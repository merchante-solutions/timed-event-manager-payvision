package com.mes.support.dc;

import java.io.InputStream;
import org.apache.log4j.Logger;
import com.mes.support.MesEncryption;
import masthead.encrypt.EncryptionClient;

public class MesEncryptingInputStream extends MesFilterInputStream
{
  static Logger log = Logger.getLogger(MesEncryptingInputStream.class);
  
  public static final int     DFLT_ENC_READ_SIZE  = 500000;
  public static final String  SEPARATOR           = "@";
  
  private EncryptionClient client;

  public MesEncryptingInputStream(InputStream in, int readSize)
  {
    super(in,readSize);
  }
  public MesEncryptingInputStream(InputStream in)
  {
    this(in,DFLT_ENC_READ_SIZE);
  }
  
  private EncryptionClient getClient()
  {
    if (client == null)
    {
      client = MesEncryption.getClient();
    }
    return client;
  }
  
  protected EncodingType getEncodingType()
  {
    return EncodingType.MES_BLK_ENCRYPT_1;
  }
  
  /**
   * Takes byte buffer with data to be encrypted and a length.  Encrypts
   * data and returns byte array containing an encrypted data block length 
   * prefix followed by encrypted data: <length><SEPARATOR><encrypted data>
   */
  protected byte[] filterData(byte[] data, int dataCount) throws Exception
  {
    // data to be encrypted needs to be in exactly sized buffer
    if (data.length > dataCount)
    {
      byte[] resized = new byte[dataCount];
      System.arraycopy(data,0,resized,0,dataCount);
      data = resized;
    }
      
    // return block encoded encrypted data
    return encodeBlock(getClient().encrypt(data));
  }
} 

