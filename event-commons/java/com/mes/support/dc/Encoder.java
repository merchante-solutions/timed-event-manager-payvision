package com.mes.support.dc;

public interface Encoder
{
  public byte[] encode(byte[] data) throws Exception;
  public byte[] decode(byte[] data) throws Exception;
}