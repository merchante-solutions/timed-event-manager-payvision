package com.mes.support.dc;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.apache.log4j.Logger;

public class MesCompressionHandler implements CompressionHandler
{
  static Logger log = Logger.getLogger(MesCompressionHandler.class);
  
  public byte[] compress(byte[] data, int off, int len) throws Exception
  {
    Deflater deflater = new Deflater();
    deflater.setInput(data,off,len);
    deflater.finish();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte[] tempBuf = new byte[50000];
    int byteCount = 0;
    while ((byteCount = deflater.deflate(tempBuf)) > 0)
    {
      baos.write(tempBuf,0,byteCount);
    }
    deflater.end();
    return baos.toByteArray();
  }
  public byte[] compress(byte[] data) throws Exception
  {
    return compress(data,0,data.length);
  }

  public byte[] decompress(byte[] data) throws Exception
  {
    Inflater inflater = new Inflater();
    inflater.setInput(data);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte tempBuf[] = new byte[50000];
    int byteCount = 0;
    while ((byteCount = inflater.inflate(tempBuf)) > 0)
    {
      baos.write(tempBuf,0,byteCount);
    }
    inflater.end();
    return baos.toByteArray();
  }
}