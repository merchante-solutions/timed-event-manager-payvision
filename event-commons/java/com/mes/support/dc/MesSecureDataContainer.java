package com.mes.support.dc;

/**
 * Data container object that provides compression + encryption.
 *
 * Useful for storing large amounts of sensitive data in database blobs.
 */
public class MesSecureDataContainer extends MesDataContainer
{
  public MesSecureDataContainer()
  {
    // create compression encoder wrapping an encryption encoder
    // compression will happen first, followed by encryption
    // this should create most compact data for storage
    super(new CompressionEncoder(
            new EncryptionEncoder(new MesEncryptionHandler()),
            new MesCompressionHandler()));
  }
}