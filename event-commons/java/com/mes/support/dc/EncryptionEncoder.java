package com.mes.support.dc;

public class EncryptionEncoder extends FilterEncoder
{
  private EncryptionHandler encHnd;

  public EncryptionEncoder(Encoder encoder, EncryptionHandler encHnd)
  {
    super(encoder);
    this.encHnd = encHnd;
  }
  public EncryptionEncoder(EncryptionHandler encHnd)
  {
    this.encHnd = encHnd;
  }

  public byte[] enfilter(byte[] data) throws Exception
  {
    return encHnd.encrypt(data);
  }

  public byte[] defilter(byte[] data) throws Exception
  {
    return encHnd.decrypt(data);
  }
}