package com.mes.support.dc;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.log4j.Logger;

public class FileContainerImpl implements FileContainer
{
  static Logger log = Logger.getLogger(FileContainerImpl.class);
  
  public static final int SEND_BUFFER_SIZE = 500000;
  
  protected DiskCacheItem item;
  
  /**
   * Sets disk cache item with data received from input stream.
   * Releases old disk cache item if present.
   */
  public void setData(InputStream in) 
  {
    // release old item if present to free up disk cache resources
    if (item != null)
    {
      release();
    }
    item = DiskCache.createItem(in);
  }
  
  /**
   * Sets data from an array of bytes.  May have memory limitations...
   */
  public void setData(byte[] data)
  {
    try
    {
      setData(new ByteArrayInputStream(data));
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  public void setData(String data) 
  {
    setData(data.getBytes());
  }

  public InputStream getInputStream()
  {
    return item != null ? item.getInputStream() : null;
  }
  
  /**
   * Fetches an output stream from the disk cache item to allow writing
   * to the cache file.
   */
  public OutputStream getOutputStream()
  {
    if (item == null)
    {
      item = DiskCache.createItem();
    }
    return item.getOutputStream();
  }

  /**
   * True if no disk cache item or it is empty.
   */
  public boolean empty()
  {
    return item == null || item.empty();
  }
  
  /**
   * Returns amount of space used in disk cache.
   */
  public long size()
  {
    return item != null ? item.size() : 0;
  }
  
  /**
   * Deletes the disk cache item.
   */
  public void release()
  {
    if (item != null)
    {
      item.delete();
      item = null;
    }
  }
  
  /**
   * Passes all available data from source stream to destination stream.
   * Supports pass through streaming and convenience functions that will
   * write contained file data to an output stream. Returns number of
   * bytes written to output stream.
   */
  protected int sendStream(InputStream in, OutputStream out)
  {
    try
    {
      byte[] buf = new byte[SEND_BUFFER_SIZE];
      int readCount = 0;
      int writeCount = 0;
      while ((readCount = in.read(buf)) != -1)
      {
        out.write(buf,0,readCount);
        writeCount += readCount;
      }
      out.flush();
      return writeCount;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  public int writeData(OutputStream out)
  {
    InputStream in = null;
    
    try
    {
      in = getInputStream();
      return sendStream(in,out);
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
}