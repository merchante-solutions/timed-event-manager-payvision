package com.mes.support.dc;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

public class DiskCache
{
  static Logger log = Logger.getLogger(DiskCache.class);
  
  public static final String  PREFIX                = "dci-";
  public static final String  TEMP_DIR              = ".";
  
  // expires after an hour
  public static final long    EXPIRE_MS             = 1000L * 60L * 60L;
  
  // cleanup every minute
  public static final long    CLEANUP_FREQUENCY_MS  = 1000L * 60L;
  
  private static DiskCache cache = new DiskCache();
  private static File tempDir;

  
  private Map files = new HashMap();
  private Set items = new HashSet();
  private Calendar lastCleanup = null;
  private Calendar started;
  
  private DiskCache()
  {
    tempDir = new File(TEMP_DIR);
    started = Calendar.getInstance();
    
    // run a cleanup thread 
    (new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            while (true)
            {
              DiskCache.this.cleanup();
              Thread.sleep(CLEANUP_FREQUENCY_MS);
            }
          }
          catch (Exception e)
          {
            log.error("Error in cleanup thread: " + e);
            e.printStackTrace();
            throw new RuntimeException(e);
          }
        }
      })).start();
  }
  
  private void cleanup()
  {
    // clean up temp files that have expired
    final Calendar expired = Calendar.getInstance();
    expired.setTimeInMillis(expired.getTimeInMillis() - EXPIRE_MS);
    final Calendar modded = Calendar.getInstance();
    File[] tempFiles = tempDir.listFiles(
      new FileFilter()
      {
        public boolean accept(File f)
        {
          // don't delete non-cache item files
          String name = f.getName();
          if (!name.startsWith(PREFIX) || !name.endsWith(".tmp"))
          {
            return false;
          }
          
          // see if file is expired
          modded.setTimeInMillis(f.lastModified());
          if (modded.before(expired))
          {
            return true;
          }
          
          // clear files from prior caches
          // DISABLED due to multiple instances
          //if (modded.before(started))
          //{
          //  return true;
          //}
          
          return false;
        }
      });
    
    // delete files found
    for (int i = 0; i < tempFiles.length; ++i)
    {
      tempFiles[i].delete();
      // TODO: clean up item as well
      // make it thread safe
    }
  }
  
  public boolean valid(DiskCacheItem item)
  {
    return items.contains(item);
  }
  
  public void validate(DiskCacheItem item)
  {
    if (!valid(item))
    {
      throw new NullPointerException("Invalid disk cache item");
    }
  }
  
  /**
   * Fetches the underlying item file for an item.
   */
  public File file(DiskCacheItem item)
  {
    return (File)files.get(item);
  }

  /**
   * Returns the item file size.
   */
  public long size(DiskCacheItem item)
  {
    File f = file(item);
    return f != null ? f.length() : 0;
  }
  
  /**
   * Returns true if file is empty.
   */
  public boolean empty(DiskCacheItem item)
  {
    return size(item) == 0;
  }
  
  /**
   * Delete cached item file, remove from files map, remove from item set.
   */
  public void delete(DiskCacheItem item)
  {
    File f = file(item);
    if (f != null)
    {
      log.debug("Delete " + f + ": " + f.delete());
    }
    files.remove(item);
    items.remove(item);
  }
  
  /**
   * Fetch file name associated with item if exists.
   */
  public String filename(DiskCacheItem item)
  {
    try
    {
      File f = file(item);
      return f != null ? f.getCanonicalPath() : null;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }
    return null;
  }
  
  /**
   * Returns an input stream for the item file.
   */
  public InputStream getInputStream(DiskCacheItem item) throws IOException
  {
    return new FileInputStream(file(item));
  }
  
  /**
   * Return an output stream for the item file, will overwrite
   * any content currently in the file.  If no file exists a
   * new file is created.
   */
  public OutputStream getOutputStream(DiskCacheItem item) throws IOException
  {
    cache.validate(item);
    File file = cache.file(item);
    if (file == null)
    {
      file = File.createTempFile(PREFIX,null,tempDir);
      cache.files.put(item,file);
    }
    return new FileOutputStream(file);
  }
    
  /**
   * Creates new cache item, stores it in the items set.
   */
  public static DiskCacheItem createItem()
  {
    // create item, initialize in files map with null
    DiskCacheItem item = new DiskCacheItem(cache);
    cache.items.add(item);
    return item;
  }

  
  /**
   * Stores data from input stream in the disk cache.  Generates and 
   * returns a disk cache key item.
   */
  public static DiskCacheItem createItem(InputStream in)
  {
    OutputStream out = null;
    
    try
    {
      // create item 
      DiskCacheItem item = createItem();
      
      // write input to item file
      out = item.getOutputStream();
      byte[] inBuf = new byte[5000];
      int readCount = 0;
      while ((readCount = in.read(inBuf)) != -1)
      {
        out.write(inBuf,0,readCount);
      }
      out.flush();
      
      return item;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    finally
    {
      try { out.close(); } catch (Exception e) { }
    }
  }
}
  
  
