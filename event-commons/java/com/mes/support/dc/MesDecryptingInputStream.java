package com.mes.support.dc;

import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import com.mes.support.MesEncryption;
import masthead.encrypt.EncryptionClient;

public class MesDecryptingInputStream extends MesFilterInputStream
{
  static Logger log = Logger.getLogger(MesDecryptingInputStream.class);
  
  public static final int     DFLT_DEC_READ_SIZE  = 500020;
  
  private EncryptionClient client;

  public MesDecryptingInputStream(InputStream in, int readSize)
  {
    super(in,readSize);
  }
  public MesDecryptingInputStream(InputStream in)
  {
    this(in,DFLT_DEC_READ_SIZE);
  }
  
  private EncryptionClient getClient()
  {
    if (client == null)
    {
      client = MesEncryption.getClient();
    }
    return client;
  }
  
  protected boolean isBlockEncoded()
  {
    return true;
  }
  
  protected EncodingType getEncodingType()
  {
    return EncodingType.MES_BLK_ENCRYPT_1;
  }
  
  /**
   * Takes byte buffer with data to be encrypted and a length.  Encrypts
   * data and returns byte array containing an encrypted data block length 
   * prefix followed by encrypted data: <length><SEPARATOR><encrypted data>
   */
  protected byte[] filterData(byte[] data, int dataCount) throws Exception
  {
    // data to be encrypted needs to be in exactly sized buffer
    // should already be in correctly sized buffer 
    if (data.length != dataCount)
    {
      throw new IOException("Data block incorrect size");
    }
      
    // encrypt the data
    return getClient().decrypt(data);
  }
} 

