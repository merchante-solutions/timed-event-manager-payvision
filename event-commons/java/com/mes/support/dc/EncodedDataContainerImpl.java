package com.mes.support.dc;

import java.io.InputStream;
import org.apache.log4j.Logger;

public class EncodedDataContainerImpl extends DataContainerImpl 
  implements EncodedDataContainer
{
  static Logger log = Logger.getLogger(EncodedDataContainerImpl.class);

  protected Encoder encoder;
  protected boolean encodedFlag;

  public EncodedDataContainerImpl(Encoder encoder)
  {
    this.encoder = encoder;
  }

  /**
   * Encoding/decoding, use the encoder object to encode and decode data.
   */

  private byte[] decode(byte[] encData)
  {
    try
    {
      return encoder.decode(encData);
    }
    catch (Exception e)
    { 
      log.error("Decode error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private byte[] encode(byte[] data)
  {
    try
    {
      return encoder.encode(data);
    }
    catch (Exception e)
    {
      log.error("Encode error: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  /**
   * Encoded data accessors. These will encode data
   * if necessary and maintain the encoded flag.
   */

  public byte[] getEncodedData()
  {
    // encode data if data is unencoded
    if (!encodedFlag && data != null)
    {
      setEncodedData(encode(data));
    }
    return data;
  }

  public void setEncodedData(byte[] data)
  {
    encodedFlag = true;
    this.data = data;
  }

  public void setEncodedData(InputStream in) throws Exception
  {
    setEncodedData(bytesFromInputStream(in));
  }

  /**
   * Override data accessors.  These will decode data
   * if necessary and maintain the encoded flag.
   */

  public void setData(byte[] data)
  {
    encodedFlag = false;
    this.data = data;
  }

  public byte[] getData()
  {
    // decode data if data is encoded
    if (encodedFlag && data != null)
    {
      setData(decode(data));
    }
    return data;
  }
}