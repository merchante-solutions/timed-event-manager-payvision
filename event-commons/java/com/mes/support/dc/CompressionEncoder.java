package com.mes.support.dc;

public class CompressionEncoder extends FilterEncoder
{
  private CompressionHandler cmpHnd;

  public CompressionEncoder(Encoder encoder, CompressionHandler cmpHnd)
  {
    super(encoder);
    this.cmpHnd = cmpHnd;
  }
  public CompressionEncoder(CompressionHandler cmpHnd)
  {
    this.cmpHnd = cmpHnd;
  }

  public byte[] enfilter(byte[] data) throws Exception
  {
    return cmpHnd.compress(data);
  }

  public byte[] defilter(byte[] data) throws Exception
  {
    return cmpHnd.decompress(data);
  }
}