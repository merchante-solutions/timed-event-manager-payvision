package com.mes.support.dc;

import org.apache.log4j.Logger;
import com.mes.support.MesEncryption;
import masthead.encrypt.EncryptionClient;

public class MesEncryptionHandler implements EncryptionHandler
{
  static Logger log = Logger.getLogger(MesEncryptionHandler.class);
  
  private EncryptionClient encClient;

  private EncryptionClient getEncClient()
  {
    if (encClient == null)
    {
      encClient = MesEncryption.getClient();
    }
    return encClient;
  }

  public byte[] encrypt(byte[] data) throws Exception
  {
    return getEncClient().encrypt(data);
  }

  public byte[] decrypt(byte[] data) throws Exception
  {
    return getEncClient().decrypt(data);
  }
}