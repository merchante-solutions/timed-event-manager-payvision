/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/DateTimeFormatter.java $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeFormatter
{
  public static final String      DEFAULT_DATE_FORMAT       = "MM/dd/yy";
  public static final String      DEFAULT_TIME_FORMAT       = "hh:mm a";
  public static final String      DEFAULT_DATE_TIME_FORMAT  = "MM/dd/yy hh:mm a";
  
  public static String getFormattedDate(Date dt, String format)
  {
    String result = "";
    
    // blank string gets returned if the date object is null
    if(dt != null)
    {
      try
      {
        SimpleDateFormat    df = new SimpleDateFormat(format);
        result = df.format(dt);
      }
      catch(Exception e)
      {
        System.out.println("Invalid format: " + e);
//@        com.mes.support.SyncLog.LogEntry("com.mes.support.DateTimeFormatter::getFormattedDate()", e.toString());
      }
    }
    
    return result;
  }
  
  public static String getFormattedDate_YDDD(Date dt)
  {
    String    retVal    = getFormattedDate(dt,"yyDDD");
    
    if ( retVal.length() == 5 )
    {
      retVal = retVal.substring(1);
    }
    return( retVal );
  }
  
  public static String getFormattedTimestamp( long elapsed )
  {
    Calendar      cal       = Calendar.getInstance();
    int           days      = 0;
    long          ms        = elapsed;
    StringBuffer  retVal    = new StringBuffer("");
    
    // remove the days from the millisecond value
    days  = (int)(ms/MesCalendar.ONE_DAY_MS);
    ms   -= (days * MesCalendar.ONE_DAY_MS);

    // setup the calendar with the adjusted millisecond value
    cal.clear();
    cal.set( Calendar.MILLISECOND, (int)ms );
    
    // build the display string
    if ( days > 0 )
    {
      retVal.append(days);
      retVal.append(" d ");
    }
    retVal.append( DateTimeFormatter.getFormattedDate(cal.getTime(),"HH:mm:ss") );
    return( retVal.toString() );
  }
  
  public static Date parseDate(String dateString, String format)
  {
    return( parseDate(dateString, format, true) );
  }
  
  public static Date parseDate(String dateString, String format, boolean lenient)
  {
    Date result = null;
    
    try
    {
      if( dateString != null && !StringUtilities.isAllZeroes(dateString) )
      {
        SimpleDateFormat      df = new SimpleDateFormat(format);
        df.setLenient(lenient);      
        result = df.parse(dateString);
      }        
    }
    catch(Exception e)
    {
//@      com.mes.support.SyncLog.LogEntry("com.mes.support.DateTimeFormatter::parseDate(" + dateString + ", " + format + ")", e.toString());
    }
    
    return result;
  }
  
  public static java.sql.Date parseSQLDate(String dateString, String format)
  {
    java.sql.Date   result  = null;
    
    try
    {
      SimpleDateFormat      df = new SimpleDateFormat(format);
      
      result = new java.sql.Date(df.parse(dateString).getTime());
    }
    catch(Exception e)
    {
//@      com.mes.support.SyncLog.LogEntry("com.mes.support.DateTimeFormatter::parseSQLDate(" + dateString + ", " + format + ")", e.toString());
    }
    
    return result;
  }
  
  public static Date getCurDate()
  {
    Calendar  cal     = Calendar.getInstance();
    return cal.getTime();
  }
  
  public static String getCurDateTimeString()
  {
    return getFormattedDate(getCurDate(), DEFAULT_DATE_TIME_FORMAT);
  }
  public static String getCurDateString()
  {
    return getFormattedDate(getCurDate(), DEFAULT_DATE_FORMAT);
  }
  public static String getCurTimeString()
  {
    return getFormattedDate(getCurDate(), DEFAULT_TIME_FORMAT);
  }
  public static String getCurDateString(String format)
  {
    return getFormattedDate(getCurDate(), format);
  }
  public static String getCurTimeString(String format)
  {
    return getFormattedDate(getCurDate(), format);
  }
  public static String getCurDateTimeString(String format)
  {
    return getFormattedDate(getCurDate(), format);
  }
  
  //
  // Calculation provided by:
  //  Astronomical Applications Department of the U.S Naval Observatory
  //  http://aa.usno.navy.mil/
  //
  public static Date getEasterSundayInYear( int year )
  {
    int   c;            
    int   n;
    int   k;
    int   i;
    int   j;
    int   l;
    int   m;
    int   d;

    c = (year / 100);
    n = year - 19 * (year / 19);
    k = (c - 17) / 25;
    i = c - c / 4 - ( c - k) / 3 + 19 * n + 15; 
    i = i - 30 * ( i / 30 ); 
    i = i - (i / 28) * (1 - (i / 28) * (29 / (i + 1)) * ((21 - n) / 11)); 
    j = year + year / 4 + i + 2 - c + c / 4; 
    j = j - 7 * (j / 7); 
    l = i - j; 
    m = 3 + (l + 40) / 44; 
    d = l + 28 - 31 * ( m / 4 ); 
    
    Calendar cal = Calendar.getInstance();
    cal.clear();
    cal.set( year, (m-1), d );
  
    return( cal.getTime() );
  }
  
  public static Date getGoodFridayInYear(int year)
  {
    Calendar    cal   = Calendar.getInstance();
    
    cal.clear();
    cal.setTime( getEasterSundayInYear(year) );
    cal.add( Calendar.DAY_OF_MONTH, -2 );
    return( cal.getTime() );
  }
  
  public static Date getNthWeekdayInMonthYear(int year, int month, int dayOfMonth, int occurNum)
  {
    Calendar    cal   = Calendar.getInstance();
    
    cal.clear();
    cal.set( Calendar.DAY_OF_MONTH, 1 );
    cal.set( Calendar.MONTH, month );
    cal.set( Calendar.YEAR, year );
    
    while( cal.get( Calendar.DAY_OF_WEEK ) != dayOfMonth )
    {
      cal.add( Calendar.DAY_OF_MONTH, 1 );
    }
    cal.add( Calendar.DAY_OF_MONTH, (7 * (occurNum-1)) );
    
    return( cal.getTime() );
  }
  
  public static Date getLastWeekdayInMonthYear(int year, int month, int dayOfMonth )
  {
    Calendar    cal   = Calendar.getInstance();
    
    cal.clear();
    cal.set( Calendar.DAY_OF_MONTH, 1 );
    cal.set( Calendar.MONTH, month );
    cal.set( Calendar.YEAR, year );
    
    cal.add( Calendar.MONTH, 1);
    cal.add( Calendar.DAY_OF_MONTH, -1);
    
    while( cal.get( Calendar.DAY_OF_WEEK ) != dayOfMonth )
    {
      cal.add( Calendar.DAY_OF_MONTH, -1 );
    }
    return( cal.getTime() );
  }
  
  public static Date getLastDateOfMonth(int month)
  {
    Calendar cal = Calendar.getInstance();

    cal.set( Calendar.MONTH, month );
    cal.set( Calendar.DATE, cal.getActualMaximum(Calendar.DATE) );

    return( cal.getTime() );
  }
  
  public static String getLastDateOfMonth(int month, String format)
  {
    Calendar cal = Calendar.getInstance();

    cal.set( Calendar.MONTH, month );
    cal.set( Calendar.DATE, cal.getActualMaximum(Calendar.DATE) );

    return( getFormattedDate(cal.getTime(), format) );
  }
}
