package com.mes.support;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import  org.apache.commons.lang.StringUtils;
/**
 * StringUtilities 
 * 
 * Provides a set of commonly used String parsing and
 * processing routines.
 * 
 * NOTE: This class represents a modified version of Apache's StringUtilities.java class 
 *       so the copyright and conditions statement below is required
 */

/**
 * Copyright (c) 1999 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Xindice" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation and was
 * originally based on software copyright (c) 1999-2001, The dbXML
 * Group, L.L.C., http://www.dbxmlgroup.com.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * $Id: StringUtilities.java,v 1.1.1.1 2001/12/06 19:33:58 bradford Exp $
 */

public final class StringUtilities {
   private StringUtilities() {
   }

   public static String fmtstr(String s,String noValString)
   {
     return (s==null || s.length()<1)? noValString : s;
   }
   
   /**
    * replace()
    * 
    * Replaces all instances of a specified string with another in a given source string.
    * 
    */
   public static String replace(String src,String fnd,String rpl)
   {
     if(src==null || src.length()<1 || fnd==null || rpl==null || fnd.length()<1 || rpl.length()<1)
       return src;
     
     int b,i;
     
     b=i=0;
     
     StringBuffer sb = new StringBuffer(src.length());
     
     while((i=src.indexOf(fnd,b))>0) {
       sb.append(src.substring(b,i));
       sb.append(rpl);
       b=i+1;
     }
     if(b<src.length())
       sb.append(src.substring(b));
     
     return sb.toString();
   }
   

   /**
    * replace()
    * 
    * Replaces character at specified position. 1st position = 0
    * 
    */
   public static String replaceAtPos(String src, char rpl, int pos)
   {
     if(src==null || src.length()<1 || pos < 0 || pos > src.length())
       return src;
     
     StringBuffer sb = new StringBuffer(src);

     sb.setCharAt(pos,rpl);
     
     return sb.toString();
   }


   /**
    * strip()
    * 
    * Removes all instances of a specified char from the given string.
    * 
    * @param    s   String in question.
    * @param    c   char to "strip" from string in question.
    */
   public static String strip(String s,char c)
   {
     if(s==null)
       return null;
     
     if(s.length()<1 || s.indexOf(c)<0)
       return s;
     
     StringBuffer sb = new StringBuffer(s.length());
     char d;
     
     for(int i=0;i<s.length();i++) {
       if((d=s.charAt(i))!=c)
         sb.append(d);
     }
     
     return sb.toString();
   }
   
   /**
    * dateStamp() Returns string representing date of YYYYMMDD format.
    */
   public static String dateStamp(Date date)
   {
     final Calendar clndr = Calendar.getInstance();
     clndr.setTime(date);
     return 
      Integer.toString(clndr.get(Calendar.YEAR))
      +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.MONTH)+1), 2, '0')
      +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.DAY_OF_MONTH)), 2, '0');
   }
   
   /**
    * cleanNumericString()
    * 
    * Removes any surrounding whitespace chars then any preceeding '0' chars.
    */
   public static String cleanNumericString(String s)
   {
     if(s==null || s.length()==0)
       return s;
     
     StringBuffer sb = new StringBuffer(s.length());
     
     // keep only chars: 0-9, '+', '-', '.'
     char[] chars = s.toCharArray();
     for(int i=0;i<chars.length;i++) {
       if((chars[i]>=48 && chars[i]<=57) || chars[i]==43 || chars[i]==45 || chars[i]==46)
         sb.append((char)chars[i]);
     }
     
     s=sb.toString();
     chars = s.toCharArray();
     
     int i=0;
     final int len = s.length();
     
     while(chars[i]=='0' && i<(len-1))
       i++;
     if(i>0)
       s=sb.substring(i);
     
     return s;
   }
   
   public static long stringToLong(String value, long defvalue)
   {
      try {
        return Long.parseLong(cleanNumericString(value));
      }
      catch ( Exception e ) {
        return defvalue;
      }
   }
   
   public static long stringToLong(String value)
   {
      return stringToLong(value,0);
   }
   
   /**
    * stringToInt quickly converts a String value to an int, returning
    * a default value if a conversion error occurs.
    *
    * @param value The value to convert
    * @param defvalue The default value to return
    * @return The converted value
    */
   public static int stringToInt(String value, int defvalue) {
      try {
        return Integer.parseInt(cleanNumericString(value));
      }
      catch ( Exception e ) {
        return defvalue;
      }
   }

   /**
    * stringToInt quickly converts a String value to an int, returning
    * 0 if a conversion error occurs.
    *
    * @param value The value to convert
    * @return The converted value
    */
   public static int stringToInt(String value) {
       return stringToInt(value, 0);
   }

   /**
    * leftJustify left-justifies a String value, space padding to width
    * characters if the string is less than width.
    *
    * @param value The value to left-justify
    * @param width The width to left-justify to
    * @return The left-justified value
    */
   public static String leftJustify(String value, int width, char padChar) {
      StringBuffer sb = new StringBuffer(width);
      sb.append(value);
      for ( int i = sb.length(); i < width; i++ )
         sb.append(padChar);
      if ( sb.length()>width )
         sb.setLength(width);
      return sb.toString();
   }

   /**
    * rightJustify right-justifies a String value, space padding to
    * width characters if the string is less than width.
    *
    * @param value The value to right-justify
    * @param width The width to right-justify to
    * @return The right-justified value
    */
   public static String rightJustify(String value, int width, char padChar) {
     
     width = (width < 0)? 0 : width;
      
     StringBuffer sb = new StringBuffer(width);
     if (value != null) 
        sb.append(value);
     for ( int i = sb.length(); i < width; i++ )
        sb.insert(0, padChar);
     if ( sb.length()>width )
        return sb.toString().substring(sb.length()-width);
     else
        return sb.toString();
  }

   /**
    * parseBuffer parses through a String buffer and produces a
    * Map table using the provided delimiters.  This method can also
    * urldecode values and produce newline-delimited multi-value properties if
    * multiple values for the same key are detected in the buffer.
    *
    * @param retprops The Map to populate
    * @param inbuffer The String buffer to parse
    * @param delimiters The delimiters to use
    * @param urldecode Whether or not to URL-decode the values
    * @param multivalue Whether or not to parse multiple values
    */
   public static void parseBuffer(Map retprops, String inbuffer, String delimiters, boolean urldecode, boolean multivalue) {
      if ( inbuffer.length() == 0 )
         return;
      int idx = 0;
      String line;
      String key;
      String value;
      String prop;
      String temp;
      StringTokenizer st = new StringTokenizer(inbuffer, delimiters, false);
      while ( st.hasMoreTokens() ) {
         line = st.nextToken();
         idx = line.indexOf('=');
         if ( idx != -1 ) {
            key = line.substring(0, idx).trim();
            try {
               value = urldecode ? HttpHelper.urlDecode(line.substring(idx+1).trim())
                                 : line.substring(idx+1).trim();
            }
            catch ( Exception e ) {
               value = line.substring(idx+1).trim();
            }
            if ( multivalue ) {
               temp = (String)retprops.get(key);
               if ( temp != null && temp.length() > 0 )
                  value = temp + "\u0001" + value;
               retprops.put(key, value);
            }
            else
               retprops.put(key, value);
         }
      }
   }

   /**
    * findWhiteSpace scans a String value for whitespace characters
    * from a specified starting position and returns the location of the first
    * whitespace character found.
    *
    * @param value The value to scan
    * @param start The starting position
    * @return The first whitespace location
    */
   public static int findWhiteSpace(String value, int start) {
      char[] chars = value.toCharArray();
      for ( int i = 0; i < chars.length; i++ )
         if ( chars[i] <= 32 )
            return i;
      return -1;
   }

   /**
    * findWhiteSpace scans a String value for whitespace characters
    * and returns the location of the first whitespace character found.
    *
    * @param value The value to scan
    * @return The first whitespace location
    */
   public static int findWhiteSpace(String value) {
      return findWhiteSpace(value, 0);
   }

   /**
    * javaEncode converts a String value to a Java-printable String.
    * All non-printable characters, such as newline and backspace are converted
    * to their respective escape sequences.  This method does not convert
    * UNICODE characters.
    *
    * @param value The value to convert
    * @return The converted value
    */
   public static String javaEncode(String value) {
      StringTokenizer st = new StringTokenizer(value, "\b\t\n\f\r\"\'\\", true);
      StringBuffer sb = new StringBuffer(value.length());
      String token;
      while ( st.hasMoreTokens() ) {
         token = st.nextToken();
         if ( token.equals("\b") )
            sb.append("\\b");
         else if ( token.equals("\t") )
            sb.append("\\t");
         if ( token.equals("\n") )
            sb.append("\\n");
         else if ( token.equals("\f") )
            sb.append("\\f");
         else if ( token.equals("\r") )
            sb.append("\\r");
         else if ( token.equals("\"") )
            sb.append("\\\"");
         else if ( token.equals("\'") )
            sb.append("\\\'");
         else if ( token.equals("\\") )
            sb.append("\\\\");
         else
            sb.append(token);
      }
      return sb.toString();
   }
   
  /*
  ** Returns the string itself or the string truncated to the max length
  */
  public static String getStringMax(int max, String value)
  {
    if(value == null)
    {
      return "";
    }
    else
    {
      return ( (value.length() > max) ? value.substring(0, max) : value );
    }
  }

  public static String removeExtendedAsciiChars( String rawData )
  {
    StringBuffer      buffer    = new StringBuffer("");
    String            retVal    = null;
    
    if ( rawData != null )
    {
      // strip the non-printable characters from the output string
      for( int i = 0; i < rawData.length(); ++i )
      {
        int ch = (int)rawData.charAt(i);
      
        if( ch > 127 )
        {
          switch( ch )
          {
            case 150: // MS Word special hyphen
              buffer.append('-');
              break;
            
            case 146: // MS Word special apostrophe
              buffer.append('\'');
              break;
            
            default:
              com.mes.support.SyncLog.LogEntry("com.mes.support.StringUtilities::removeExtendedAsciiChars()", "unknown bad char: " + ch);
              buffer.append(' ');
              break;
          }
        }
        else
        {
          buffer.append(rawData.charAt(i));
        }
      }
      retVal = buffer.toString();
    }
    return( retVal );
  }
  
  public static boolean isAllZeroes( String rawData )
  {
    boolean     allZeroes     = false;
    
    if ( rawData != null && !"".equals(rawData.trim()) )
    {
      allZeroes = true;   // assume true until proven otherwise
      for( int i = 0; i < rawData.length(); ++i )
      {
        if ( rawData.charAt(i) != '0' )
        {
          allZeroes = false;
          break;
        }
      }
    }
    return( allZeroes );
  }
  public static String dataSwap(String str1,String str2) {
	  	return !(StringUtils.isBlank(str1) || StringUtils.isBlank(str1.replace("-", ""))) || StringUtils.isBlank(str2)  ? str1 : str2;
  }
}
