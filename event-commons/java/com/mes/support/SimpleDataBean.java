/*************************************************************************

  FILE: $URL: $

  Description:
  
  SimpleDataBean
  
  Very simple bean for storing data that is easily interfaced with an
  HttpServletRequest and is serializable so by default allowed to replicate
  across a WebLogic cluster
  
  NOTE: this class only works for objects with fields of type:
    

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;

public class SimpleDataBean
  implements Serializable
{
  public SimpleDataBean()
  {
  }
  
  public void setAttributes(HttpServletRequest request)
  {
    for (Enumeration names = request.getParameterNames();
         names.hasMoreElements(); )
    {
      try
      {
        String name = (String)names.nextElement();
        String value = URLDecoder.decode(request.getParameter(name), "UTF-8");
        
        setField(name,value);
      }
      catch(Exception e)
      {
      }
    }
  }
  
  protected void setField(String name, String value)
  {
    Method[] methods = this.getClass().getMethods();
    
    // find named method in class
    for(int i=0; i < methods.length; ++i)
    {
      Method m = (Method)(methods[i]);
      
      // see if this is a "set" method
      if(m.getName().length() > 3 && m.getName().substring(0, 3).equals("set"))
      {
        // see if set method matches the name of the field
        if(m.getName().substring(3, m.getName().length()).toLowerCase().equals(name.toLowerCase()))
        {
          try
          {
            // invoke the set method with the value specified
            m.invoke(this, new String[] { XSSFilter.encodeXSS(value) });
          }
          catch(Exception e)
          {
            System.out.println("setField(" + name+ ", " + value + "): " + e.toString());
          }
        }
      }
    }
  }
}