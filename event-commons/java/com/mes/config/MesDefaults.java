/*************************************************************************

 FILE: $Archive: /Java/beans/com/mes/net/MesDefaults.sqlj $

 Description:

 MesDefaults

 Contains static methods that give access to default values stored
 in the database table MES_DEFAULTS.  These default values are accessed
 by key values represented by constants also defined within this class.

 Last Modified By   : $Author: vjoshi $
 Last Modified Date : $Date: 2015-01-28 15:01:59 -0800 (Wed, 28 Jan 2015) $
 Version            : $Revision: 23300 $

 Change History:
 See VSS database

 Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
 All rights reserved, Unauthorized distribution prohibited.

 This document contains information which is the proprietary
 property of Merchant e-Solutions, Inc.  This document is received in
 confidence and its contents may not be disclosed without the
 prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/

package com.mes.config;

import java.util.Hashtable;

public class MesDefaults  {
    // default keys
    public static final String DK_STORE_NUMBER = "DK_STORE_NUMBER";
    public static final String DK_LOCATION_NUMBER = "DK_LOCATION_NUMBER";
    public static final String DK_TIME_ZONE = "DK_TIME_ZONE";
    public static final String DK_VS_ACQUIRER_NAME = "DK_VS_ACQUIRER_NAME";
    public static final String DK_VS_ACQUIRER_CONTACT = "DK_VS_ACQUIRER_CONTACT";
    public static final String DK_VS_ACQUIRER_PHONE = "DK_VS_ACQUIRER_PHONE";
    public static final String DK_VS_ACQUIRER_BIN = "DK_VS_ACQUIRER_BIN";
    public static final String DK_VS_AGENT_BIN = "DK_VS_AGENT_BIN";
    public static final String DK_VS_AGENT_CHAIN = "DK_VS_AGENT_CHAIN";
    public static final String DK_VS_XML_TEST_URL = "DK_VS_XML_TEST_URL";
    public static final String DK_VS_XML_LISTENER_URL = DK_VS_XML_TEST_URL;
    public static final String DK_VS_XML_HDR_NAME = "DK_VS_XML_HDR_NAME";
    public static final String DK_VS_XML_HDR_PASSWORD = "DK_VS_XML_HDR_PASSWORD";
    public static final String DK_VS_XML_HDR_PARTNER = "DK_VS_XML_HDR_PARTNER";
    public static final String DK_RISK_OVER_AMOUNT_DEFAULT = "DK_RISK_OVER_AMOUNT_DEFAULT";
    public static final String DK_RISK_OVER_PERCENT_DEFAULT = "DK_RISK_OVER_PERCENT_DEFAULT";
    public static final String DK_SYN_XML_TEST_URL = "DK_SYN_XML_TEST_URL";
    public static final String DK_SYN_XML_REQUEST_TYPE = "DK_SYN_XML_REQUEST_TYPE";
    public static final String DK_SYN_XML_PROVIDER_ID = "DK_SYN_XML_PROVIDER_ID";
    public static final String DK_SYN_XML_SUBSCRIBER_ID = "DK_SYN_XML_SUBSCRIBER_ID";
    public static final String DK_SYN_XML_USER_ID = "DK_SYN_XML_USER_ID";
    public static final String DK_SYN_XML_PASSWORD = "DK_SYN_XML_PASSWORD";
    public static final String DK_SYN_XML_PRODUCT_ID = "DK_SYN_XML_PRODUCT_ID";
    public static final String DK_SYN_XML_DETAIL_LINK = "DK_SYN_XML_DETAIL_LINK";
    public static final String DK_TRANSCOM_TOP_NODE = "DK_TRANSCOM_TOP_NODE";
    public static final String DK_TRANSCOM_ACTIVITY_HOST = "DK_TRANSCOM_ACTIVITY_HOST";
    public static final String DK_TRANSCOM_ACTIVITY_USER = "DK_TRANSCOM_ACTIVITY_USER";
    public static final String DK_TRANSCOM_ACTIVITY_PASSWORD = "DK_TRANSCOM_ACTIVITY_PASSWORD";
    public static final String DK_TRANSCOM_ACTIVITY_PATH = "DK_TRANSCOM_ACTIVITY_PATH";
    public static final String DK_VS_XML_RETRY_MAX = "DK_VS_XML_RETRY_MAX";
    public static final String DK_TRANSCOM_MIF_HOST = "DK_TRANSCOM_MIF_HOST";
    public static final String DK_TRANSCOM_MIF_USER = "DK_TRANSCOM_MIF_USER";
    public static final String DK_TRANSCOM_MIF_PASSWORD = "DK_TRANSCOM_MIF_PASSWORD";
    public static final String DK_TRANSCOM_MIF_PATH = "DK_TRANSCOM_MIF_PATH";
    public static final String DK_SMTP_HOST = "DK_SMTP_HOST";
    public static final String DK_TRANSCOM_FTP_TIMEOUT = "DK_TRANSCOM_FTP_TIMEOUT";
    public static final String DK_MAX_QUEUE_DISPLAY = "DK_MAX_QUEUE_DISPLAY";
    public static final String DK_DEFAULT_LOGO_PATH = "DK_DEFAULT_LOGO_PATH";
    public static final String DK_RISK_CAPT_AUTH_RATIO = "DK_RISK_CAPT_AUTH_RATIO";
    public static final String DK_RISK_DECL_APPR_RATIO = "DK_RISK_DECL_APPR_RATIO";
    public static final String DK_RISK_AVS_AUTH_RATIO = "DK_RISK_AVS_AUTH_RATIO";
    public static final String DK_RISK_AUTH_MIN_COUNT = "DK_RISK_AUTH_MIN_COUNT";
    public static final String DK_DISCOVER_TOP_NODE = "DK_DISCOVER_TOP_NODE";
    public static final String DK_SNA_FTP_ADDRESS = "DK_SNA_FTP_ADDRESS";
    public static final String DK_SNA_FTP_USER = "DK_SNA_FTP_USER";
    public static final String DK_SNA_FTP_PASSWORD = "DK_SNA_FTP_PASSWORD";
    public static final String DK_EVENT_SERVER_LISTEN_PORT = "DK_EVENT_SERVER_LISTEN_PORT";
    public static final String DK_BBT_ENROLL_EMAIL_DELAY = "DK_BBT_ENROLL_EMAIL_DELAY";
    public static final String DK_CBT_CREDIT_EMAIL_FROM = "DK_CBT_CREDIT_EMAIL_FROM";
    public static final String DK_CHANGE_REQUEST_HOST = "DK_CHANGE_REQUEST_HOST";
    public static final String DK_GCF_IP_ADDRESS = "DK_GCF_IP_ADDRESS";
    public static final String DK_GCF_USER = "DK_GCF_USER";
    public static final String DK_GCF_PASSWORD = "DK_GCF_PASSWORD";
    public static final String DK_SABRE_HOST = "DK_SABRE_HOST";
    public static final String DK_SABRE_USER = "DK_SABRE_USER";
    public static final String DK_SABRE_PASSWORD = "DK_SABRE_PASSWORD";
    public static final String DK_SABRE_OUTGOING_PATH = "DK_SABRE_OUTGOING_PATH";
    public static final String DK_VS_XML_REG_USER_NAME = "DK_VS_XML_REG_USER_NAME";
    public static final String DK_VS_XML_REG_USER_DOMAIN = "DK_VS_XML_REG_USER_DOMAIN";
    public static final String DK_VS_XML_REG_PASSWORD = "DK_VS_XML_REG_PASSWORD";
    public static final String DK_VS_XML_REG_LISTENER_URL = "DK_VS_XML_REG_LISTENER_URL";
    public static final String DK_VS_XML_REG_RESELLER = "DK_VS_XML_REG_RESELLER";
    public static final String DK_OUTGOING_CB_HOST = "DK_OUTGOING_CB_HOST";
    public static final String DK_OUTGOING_CB_USER = "DK_OUTGOING_CB_USER";
    public static final String DK_OUTGOING_CB_PASSWORD = "DK_OUTGOING_CB_PASSWORD";
    public static final String DK_OUTGOING_BM_USER = "DK_OUTGOING_BM_USER";
    public static final String DK_OUTGOING_BM_PASSWORD = "DK_OUTGOING_BM_PASSWORD";
    public static final String DK_UATP_OUTGOING_HOST = "DK_UATP_OUTGOING_HOST";
    public static final String DK_UATP_OUTGOING_PATH = "DK_UATP_OUTGOING_PATH";
    public static final String DK_UATP_OUTGOING_USER = "DK_UATP_OUTGOING_USER";
    public static final String DK_UATP_OUTGOING_PASSWORD = "DK_UATP_OUTGOING_PASSWORD";
    public static final String DK_ACR_SHIP_COST_BASE = "DK_ACR_SHIP_COST_BASE";
    public static final String DK_ACR_SHIP_COST_RUSH_BASE = "DK_ACR_SHIP_COST_RUSH_BASE";
    public static final String DK_MMS_PGP_USER = "DK_MMS_PGP_USER";
    public static final String DK_ARCHIVE_HOST = "DK_ARCHIVE_HOST";
    public static final String DK_ARCHIVE_USER = "DK_ARCHIVE_USER";
    public static final String DK_ARCHIVE_PASS = "DK_ARCHIVE_PASS";
    public static final String DK_ARCHIVE_PATH = "DK_ARCHIVE_PATH";
    public static final String DK_ARCHIVE_PATH_DAILY="DK_ARCHIVE_PATH_DAILY";
    public static final String DK_ARCHIVE_PATH_MONTHLY="DK_ARCHIVE_PATH_MONTHLY";
    public static final String DK_AMEX_OUTGOING_HOST = "DK_AMEX_OUTGOING_HOST";
    public static final String DK_AMEX_OUTGOING_PATH = "DK_AMEX_OUTGOING_PATH";    
    public static final String DK_AMEX_SPM_OUTGOING_PATH = "DK_AMEX_SPM_OUTGOING_PATH";
    public static final String DK_AMEX_OPTB_OUTGOING_PATH = "DK_AMEX_OPTB_OUTGOING_PATH";
    public static final String DK_AMEX_OUTGOING_USER = "DK_AMEX_OUTGOING_USER";
    public static final String DK_AMEX_OUTGOING_PASSWORD = "DK_AMEX_OUTGOING_PASSWORD";
    public static final String DK_AMEX_ESA_BATCH_RECOVER_DAYS = "DK_AMEX_ESA_BATCH_RECOVER_DAYS";
    public static final String DK_VPS_ISO_CHECK_DUPLICATES = "DK_VPS_ISO_CHECK_DUPLICATES";
    public static final String DK_VPS_ISO_XML_REG_TEST_URL = "DK_VPS_ISO_XML_REG_TEST_URL";
    public static final String DK_DISC_ME_OUTGOING_HOST = "DK_DISC_ME_OUTGOING_HOST";
    public static final String DK_DISC_ME_OUTGOING_USER = "DK_DISC_ME_OUTGOING_USER";
    public static final String DK_DISC_ME_OUTGOING_PASSWORD = "DK_DISC_ME_OUTGOING_PASSWORD";
    public static final String DK_DISC_ME_OUTGOING_PATH = "DK_DISC_ME_OUTGOING_PATH";
    public static final String DK_DISCOVER_OUTGOING_HOST = "DK_DISCOVER_OUTGOING_HOST";
    public static final String DK_DISCOVER_OUTGOING_USER = "DK_DISCOVER_OUTGOING_USER";
    public static final String DK_DISCOVER_OUTGOING_PASSWORD = "DK_DISCOVER_OUTGOING_PASSWORD";
    public static final String DK_DISCOVER_OUTGOING_PATH = "DK_DISCOVER_OUTGOING_PATH";
    public static final String DK_MATCH_INCOMING_PATH = "DK_MATCH_INCOMING_PATH";
    public static final String DK_MATCH_OUTGOING_PATH = "DK_MATCH_OUTGOING_PATH";
    public static final String DK_MMS_MES_HOST = "DK_MMS_MES_HOST";
    public static final String DK_MMS_MES_USER = "DK_MMS_MES_USER";
    public static final String DK_MMS_MES_PASSWORD = "DK_MMS_MES_PASSWORD";
    public static final String DK_MMS_MES_OUTGOING_PATH = "DK_MMS_MES_OUTGOING_PATH";
    public static final String DK_MMS_MES_INCOMING_PATH = "DK_MMS_MES_INCOMING_PATH";
    public static final String DK_TPG_PREAUTH_EXPIRE_DAYS = "DK_TPG_PREAUTH_EXPIRE_DAYS";
    public static final String DK_TPG_DEFAULT_BATCH_CLOSE_TIME = "DK_TPG_DEFAULT_BATCH_CLOSE_TIME";
    public static final String DK_TOCCATA_USER_ID = "DK_TOCCATA_USER_ID";
    public static final String DK_TOCCATA_ACCESS_KEY = "DK_TOCCATA_ACCESS_KEY";
    public static final String DK_FX_RATE_EXPIRATION_EXTENSION = "DK_FX_RATE_EXPIRATION_EXTENSION";   // in hours
    public static final String DK_FX_RATE_RECURRING_EXTENSION = "DK_FX_RATE_RECURRING_EXTENSION";   // in days
   // public static final String DK_NON_USD_BATCH_NUMBER = "DK_NON_USD_BATCH_NUMBER";   // non-usd batch number
    public static final String DK_PAYVISION_FTP_HOST = "DK_PAYVISION_FTP_HOST";
    public static final String DK_PAYVISION_FTP_USER = "DK_PAYVISION_FTP_USER";
    public static final String DK_PAYVISION_FTP_PASSWORD = "DK_PAYVISION_FTP_PASSWORD";
    public static final String DK_FX_CONVERSION_TOLERANCE = "DK_FX_CONVERSION_TOLERANCE";
    public static final String DK_TPG_PREAUTH_EXPIRE_DAYS_TEST = "DK_TPG_PREAUTH_EXPIRE_DAYS_TEST";
    public static final String DK_TE_WEEDS_MULTIPLIER = "DK_TE_WEEDS_MULTIPLIER";
    public static final String DK_TE_WEEDS_ADDON = "DK_TE_WEEDS_ADDON";
    public static final String DK_VISA_HOST = "DK_VISA_HOST";
    public static final String DK_VISA_USER = "DK_VISA_USER";
    public static final String DK_VISA_PASSWORD = "DK_VISA_PASSWORD";
    public static final String DK_MC_HOST = "DK_MC_HOST";
    public static final String DK_MC_USER = "DK_MC_USER";
    public static final String DK_MC_PASSWORD = "DK_MC_PASSWORD";
    public static final String DK_MC_INCOMING_PATH = "DK_MC_INCOMING_PATH";
    public static final String DK_MC_EP_HOST = "DK_MC_EP_HOST";
    public static final String DK_MC_EP_USER = "DK_MC_EP_USER";
    public static final String DK_MC_EP_PASSWORD = "DK_MC_EP_PASSWORD";
    public static final String DK_MC_EP_OUTGOING_PATH = "DK_MC_EP_OUTGOING_PATH";
    public static final String DK_VISA_EP_HOST = "DK_VISA_EP_HOST";
    public static final String DK_VISA_EP_USER = "DK_VISA_EP_USER";
    public static final String DK_VISA_EP_PASSWORD = "DK_VISA_EP_PASSWORD";
    public static final String DK_VISA_EP_INCOMING_PATH="DK_VISA_EP_INCOMING_PATH";
    public static final String DK_VISA_EP_OUTGOING_PATH = "DK_VISA_EP_OUTGOING_PATH";
    public static final String DK_VISA_INCOMING_PATH = "DK_VISA_INCOMING_PATH";
    public static final String DK_VISA_BASE_II_CUTOFF = "DK_VISA_BASE_II_CUTOFF";
    public static final String DK_VISA_AMMF_OUTGOING_PATH = "DK_VISA_AMMF_OUTGOING_PATH";
    public static final String DK_MC_GCMS_CUTOFF = "DK_MC_GCMS_CUTOFF";
    public static final String DK_RISK_SCAN_NO_ACTIVITY_DAYS = "DK_RISK_SCAN_NO_ACTIVITY_DAYS";
    public static final String DK_GRS_OUTGOING_HOST = "DK_GRS_OUTGOING_HOST";
    public static final String DK_GRS_OUTGOING_USER = "DK_GRS_OUTGOING_USER";
    public static final String DK_GRS_OUTGOING_PASSWORD = "DK_GRS_OUTGOING_PASSWORD";
    public static final String DK_AMEX_HOST = "DK_AMEX_HOST";
    public static final String DK_AMEX_USER = "DK_AMEX_USER";
    public static final String DK_AMEX_PASSWORD = "DK_AMEX_PASSWORD";
    public static final String DK_AMEX_INCOMING_PATH = "DK_AMEX_INCOMING_PATH";
    public static final String DK_AMEX_CLEARING_CUTOFF = "DK_AMEX_CLEARING_CUTOFF";
    public static final String DK_DISCOVER_CLEARING_CUTOFF = "DK_DISCOVER_CLEARING_CUTOFF";
    public static final String DK_GCF_FTPS_IP_ADDRESS = "DK_GCF_FTPS_IP_ADDRESS";
    public static final String DK_GCF_FTPS_PORT = "DK_GCF_FTPS_PORT";

    public static final String DK_PG_REAUTH_DAYS = "DK_PG_REAUTH_DAYS";
    public static final String DK_PV_API_LOADER_DAYS = "DK_PV_API_LOADER_DAYS";

    public static final String DK_DISCOVER_MAP_OUTGOING_PATH = "DK_DISCOVER_MAP_OUTGOING_PATH";

    public static final String DK_FIS_OUTGOING_HOST = "DK_FIS_OUTGOING_HOST";
    public static final String DK_FIS_OUTGOING_USER = "DK_FIS_OUTGOING_USER";
    public static final String DK_FIS_OUTGOING_PASSWORD = "DK_FIS_OUTGOING_PASSWORD";
    public static final String DK_FIS_OUTGOING_PATH = "DK_FIS_OUTGOING_PATH";

    public static final String DK_DISCOVER_MAP_PROFILE_HOST = "DK_DISCOVER_MAP_PROFILE_HOST";
    public static final String DK_DISCOVER_MAP_PROFILE_USER = "DK_DISCOVER_MAP_PROFILE_USER";
    public static final String DK_DISCOVER_MAP_PROFILE_PW = "DK_DISCOVER_MAP_PROFILE_PW";
    public static final String DK_DISCOVER_MAP_PROFILE_PATH = "DK_DISCOVER_MAP_PROFILE_PATH";
    public static final String DK_DISCOVER_MAP_INCOMING_PATH = "DK_DISCOVER_MAP_INCOMING_PATH";
    public static final String DK_DISCOVER_MAP_RESPONSE_FILE_MASK = "DK_DISCOVER_MAP_RESPONSE_FILE_MASK";

    public static final String DK_DISCOVER_MAP_PRESP_USER = "DK_DISCOVER_MAP_PRESP_USER";
    public static final String DK_DISCOVER_MAP_PRESP_PW = "DK_DISCOVER_MAP_PRESP_PW";
    public static final String DK_DISCOVER_MAP_PRESP_PATH = "DK_DISCOVER_MAP_PRESP_PATH";
    public static final String DK_DISCOVER_MAP_ARI_PW = "DK_DISCOVER_MAP_ARI_PW";

    public static final String DK_ENCORE_BP_HOST = "DK_ENCORE_BP_HOST";
    public static final String DK_ENCORE_BP_USER = "DK_ENCORE_BP_USER";
    public static final String DK_ENCORE_BP_PW = "DK_ENCORE_BP_PW";
    public static final String DK_ENCORE_BP_UPLOAD_PATH = "DK_ENCORE_BP_UPLOAD_PATH";
    public static final String DK_ENCORE_BP_DOWNLOAD_PATH = "DK_ENCORE_BP_DOWNLOAD_PATH";

    public static final String DK_ACH_CUTOFF = "DK_ACH_CUTOFF";

    public static final String DK_MODBII_CLEARING_CUTOFF = "DK_MODBII_CLEARING_CUTOFF";

    public static final String DK_RED_IP = "DK_RED_IP";
    public static final String DK_RED_USER = "DK_RED_USER";
    public static final String DK_RED_PASSWORD = "DK_RED_PASSWORD";

    public static final String DK_VERIFI_IMAGE_HOST = "DK_VERIFI_IMAGE_HOST";
    public static final String DK_VERIFI_IMAGE_USER = "DK_VERIFI_IMAGE_USER";
    public static final String DK_VERIFI_IMAGE_PASSWORD = "DK_VERIFI_IMAGE_PASSWORD";
    public static final String DK_VERIFI_IMAGE_PATH = "DK_VERIFI_IMAGE_PATH";
    public static final String DK_SPOKANE_IMAGE_HOST = "DK_SPOKANE_IMAGE_HOST";
    public static final String DK_SPOKANE_IMAGE_USER = "DK_SPOKANE_IMAGE_USER";
    public static final String DK_SPOKANE_IMAGE_PASSWORD = "DK_SPOKANE_IMAGE_PASSWORD";
    public static final String DK_SPOKANE_IMAGE_PATH = "DK_SPOKANE_IMAGE_PATH";

    public static final String DK_WF_ENTITY_ID_PROD = "DK_WF_ENTITY_ID_PROD";
    public static final String DK_WF_ENTITY_ID_UAT = "DK_WF_ENTITY_ID_UAT";
    public static final String DK_WF_AGENT_ID_PROD = "DK_WF_AGENT_ID_PROD";
    public static final String DK_WF_AGENT_ID_UAT = "DK_WF_AGENT_ID_UAT";
    public static final String DK_WF_OUTGOING_HOST = "DK_WF_OUTGOING_HOST";
    public static final String DK_WF_OUTGOING_USER = "DK_WF_OUTGOING_USER";
    public static final String DK_WF_OUTGOING_PASSWORD = "DK_WF_OUTGOING_PASSWORD";
    public static final String DK_WF_OUTGOING_AB_PATH = "DK_WF_OUTGOING_AB_PATH";

    public static final String DK_MASTERCARD_PROCESSOR_ID = "DK_MASTERCARD_PROCESSOR_ID";

    public static final String DK_VITAL_UPLOAD_PATH = "DK_VITAL_UPLOAD_PATH";
    public static final String DK_FED_URL = "DK_FED_URL";

    public static final String SCHEDULED_DAYS = "SCHEDULED_DAYS";
    public static final String DK_TRICIPHER_APPLIANCE_ADDR = "DK_TRICIPHER_APPLIANCE_ADDR";
    public static final String DK_TRICIPHER_APPLIANCE_PORT = "DK_TRICIPHER_APPLIANCE_PORT";
    public static final String DK_TRICIPHER_B2F_USER = "DK_TRICIPHER_B2F_USER";
    public static final String DK_TRICIPHER_B2F_PASSWORD = "DK_TRICIPHER_B2F_PASSWORD";
    public static final String DK_TRICIPHER_B2F_FIELD_NAME = "DK_TRICIPHER_B2F_FIELD_NAME";
    public static final String DK_TRICIPHER_B2F_USER_FIELD_NAME = "DK_TRICIPHER_B2F_USER_FIELD_NAME";
    public static final String DK_TRICIPHER_B2F_COOKIE_PREFIX = "DK_TRICIPHER_B2F_COOKIE_PREFIX";
    public static final String DK_TRICIPHER_B2F_SIGNING_KEY = "DK_TRICIPHER_B2F_SIGNING_KEY";
    public static final String DK_TRICIPHER_B2F_SIGNING_CERT = "DK_TRICIPHER_B2F_SIGNING_CERT";
    public static final String DK_TRICIPHER_B2F_CSR_USER = "DK_TRICIPHER_B2F_CSR_USER";
    public static final String DK_TRICIPHER_B2F_CSR_PASSWORD = "DK_TRICIPHER_B2F_CSR_PASSWORD";

    public static final String MERCH_PSP_FROM_EMAIL_ADDRESS = "MERCH_PSP_FROM_EMAIL_ADDRESS";
    public static final String MERCH_COMM_ENTITY_TO_EMAIL_ADDRESS = "MERCH_COMM_ENTITY_TO_EMAIL_ADDRESS";

    public static final String MERCH_PSP_TO_VISA_EMAIL_ADDRESS = "MERCH_PSP_TO_VISA_EMAIL_ADDRESS";
    public static final String MERCH_PSP_TO_MASTER_CARD_EMAIL_ADDRESS = "MERCH_PSP_TO_MASTER_CARD_EMAIL_ADDRESS";
    public static final String MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS = "MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS";
    public static final String MERCH_PSP_CC_WELLS_FARGO_EMAIL_ADDRESS = "MERCH_PSP_CC_WELLS_FARGO_EMAIL_ADDRESS";
    public static final String MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS = "MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS";
    public static final String MERCH_PSP_TO_VISA_EMAIL_ADDRESS_FOR_WF = "MERCH_PSP_TO_VISA_EMAIL_ADDRESS_FOR_WF";

    public static final String MERCH_PSP_TO_WF_OUTGOING_PATH = "MERCH_PSP_TO_WF_OUTGOING_PATH";

    public static final String MERCH_PSP_CC_COMPLIANCE_MANAGER_EMAIL_ADDRESS = "MERCH_PSP_CC_COMPLIANCE_MANAGER_EMAIL_ADDRESS";
    
    public static final String DK_NETSUITE_WSDL_URL="DK_NETSUITE_WSDL_URL";
    public static final String DK_NETSUITE_WS_TEST_URL="DK_NETSUITE_WS_TEST_URL";
    public static final String DK_NETSUITE_WS_PROD_URL="DK_NETSUITE_WS_PROD_URL";
    public static final String DK_NETSUITE_LOGIN_EMAIL="DK_NETSUITE_LOGIN_EMAIL";
    public static final String DK_NETSUITE_LOGIN_PASSWORD="DK_NETSUITE_LOGIN_PASSWORD";
    public static final String DK_NETSUITE_LOGIN_ROLE="DK_NETSUITE_LOGIN_ROLE";
    public static final String DK_NETSUITE_LOGIN_ACCOUNT="DK_NETSUITE_LOGIN_ACCOUNT";
    public static final String DK_NETSUITE_APPLICATION_ID="DK_NETSUITE_APPLICATION_ID";

    //Avatax defaults
    public static final String AVATAX_USERNAME = "AVATAX_USERNAME";
    public static final String AVATAX_PASSWORD = "AVATAX_PASSWORD";
    public static final String AVATAX_BASEURL = "AVATAX_BASEURL";
    public static final String AVATAX_COMPANY_CODE = "AVATAX_COMPANY_CODE";
    public static final String AVATAX_FROM_EMAIL_ADDRESS = "AVATAX_FROM_EMAIL_ADDRESS";
    public static final String AVATAX_TO_EMAIL_ADDRESS = "AVATAX_TO_EMAIL_ADDRESS";
    public static final String AVATAX_CC_EMAIL_ADDRESS = "AVATAX_CC_EMAIL_ADDRESS";
    
    public final static String  SABRE_ARCHIVE_HOST          = "SABRE_ARCHIVE_HOST";
    public final static String  SABRE_ARCHIVE_USER          = "SABRE_ARCHIVE_USER";
    public final static String  SABRE_ARCHIVE_PASSWORD      = "SABRE_ARCHIVE_PASSWORD";
    public final static String  SABRE_ARCHIVE_PATH          = "SABRE_ARCHIVE_PATH";
    
    // Discover Entitlement
    public final static String  DISC_ENT_ARCHIVE_HOST        = "DISC_ENT_ARCHIVE_HOST";
    public final static String  DISC_ENT_ARCHIVE_USER        = "DISC_ENT_ARCHIVE_USER";
    public final static String  DISC_ENT_ARCHIVE_PASSWORD    = "DISC_ENT_ARCHIVE_PASSWORD";
    public final static String  DISC_ENT_ARCHIVE_PATH        = "DISC_ENT_ARCHIVE_PATH";
    
    //Cielo
    public final static String  CIELO_UNMASKED_MERCHANT_ACCOUNTS = "CIELO_UNMASKED_MERCHANT_ACCOUNTS";
    public final static String  WHERE_CLAUSE_FOR_UPDATE_CIELO_MERCHANTS = "WHERE_CLAUSE_FOR_UPDATE_CIELO_MERCHANTS";
        
    public final static String  DEFAULT_FTP_DB_NAME="DEFAULT_FTP_DB_NAME";
    public final static String  MES_FTP_DB_NAME="MES_FTP_DB_NAME";
    public final static String  MMS_VERSION    = "MMS_VERSION";
    
    public final static String  DISC_SETTLEMENT_FILE_VERSION_IND="DISC_SETTLEMENT_FILE_VERSION_IND";
    
    //Auto Attach Base
    public final static String  DEFAULT_AUTO_ATTACH_BASE_FTP_HOST = "DEFAULT_AUTO_ATTACH_BASE_FTP_HOST";
    public final static String  DEFAULT_AUTO_ATTACH_BASE_FTP_FTP_USER = "DEFAULT_AUTO_ATTACH_BASE_FTP_FTP_USER";
    public final static String  DEFAULT_AUTO_ATTACH_BASE_FTP_FTP_PASSWORD = "DEFAULT_AUTO_ATTACH_BASE_FTP_FTP_PASSWORD";
    public final static String  DEFAULT_AUTO_ATTACH_BASE_FTP_IMAGE_DIRECTORY = "DEFAULT_AUTO_ATTACH_BASE_FTP_IMAGE_DIRECTORY";
    public final static String  AUTO_ATTACH_BASE_NON_PDF_CONVERT_HOST = "AUTO_ATTACH_BASE_NON_PDF_CONVERT_HOST";
    public final static String  TRIDENT_API_REQUEST_URL = "TRIDENT_API_REQUEST_URL";
    public final static String  MES_ENCRYPTION_HOST_DEFAULT="MES_ENCRYPTION_HOST_DEFAULT";
    public final static String  MES_ENCRYPTION_PORT_DEFAULT="MES_ENCRYPTION_PORT_DEFAULT";
    
    public final static String  DEFAULT_AUS_EMAILS="DEFAULT_AUS_EMAILS";
    
    public final static String  NEW_PACKING_SLIP_BEAN_MANGERS_LIST="NEW_PACKING_SLIP_BEAN_MANGERS_LIST";
    
    public final static String  PM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST="PM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST";
    public final static String  PM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST="PM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST";
    
    
    public final static String  NBSC_TO_CBT_SHIPPING_BUSINESS_NAME="NBSC_TO_CBT_SHIPPING_BUSINESS_NAME";
    public final static String  NBSC_TO_CBT_SHIPPING_ADDR_1="NBSC_TO_CBT_SHIPPING_ADDR_1";
    public final static String  NBSC_TO_CBT_SHIPPING_ADDR_2="NBSC_TO_CBT_SHIPPING_ADDR_2";
    public final static String  NBSC_TO_CBT_SHIPPING_CSZ="NBSC_TO_CBT_SHIPPING_CSZ";
    public final static String  NBSC_TO_CBT_SHIPPING_CONTACT_NAME="NBSC_TO_CBT_SHIPPING_CONTACT_NAME";
    public final static String  NBSC_TO_CBT_SHIPPING_PHONE="NBSC_TO_CBT_SHIPPING_PHONE";
    public final static String  NBSC_TO_CBT_EQUIPMENT_COMMENTS="NBSC_TO_CBT_EQUIPMENT_COMMENTS";
    
    public final static String  DAILY_ME_MONITORING_DEVOPS_NOTICES_EMAIL="DAILY_ME_MONITORING_DEVOPS_NOTICES_EMAIL";
    public final static String  DAILY_ME_MONITORING_DEVOPS_ALERTS_EMAIL="DAILY_ME_MONITORING_DEVOPS_ALERTS_EMAIL";
    
    public final static String  AM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST="AM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST";
    public final static String  AM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST="AM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST";
    
    public final static String  ARDEF_MONITORING_DEVOPS_NOTICES_EMAIL_LIST="ARDEF_MONITORING_DEVOPS_NOTICES_EMAIL_LIST";
    public final static String  ARDEF_MONITORING_DEVOPS_ALERTS_EMAIL_LIST="ARDEF_MONITORING_DEVOPS_ALERTS_EMAIL_LIST";
    
    public final static String  GCF_ORDER_FILE_FTP_ARC_HOST="GCF_ORDER_FILE_FTP_ARC_HOST";
    public final static String  GCF_ORDER_FILE_FTP_ARC_PORT="GCF_ORDER_FILE_FTP_ARC_PORT";
    public final static String  GCF_ORDER_FILE_FTP_ARC_USER="GCF_ORDER_FILE_FTP_ARC_USER";
    public final static String  GCF_ORDER_FILE_FTP_ARC_PASSWORD="GCF_ORDER_FILE_FTP_ARC_PASSWORD";
    public final static String  GCF_ORDER_FILE_INTERNAL_USERS="GCF_ORDER_FILE_INTERNAL_USERS";
    
    public final static String  ACH_CONFIRMATION_EMAIL_FROM="ACH_CONFIRMATION_EMAIL_FROM";
    public final static String  ACH_CONFIRMATION_EMAIL_TO="ACH_CONFIRMATION_EMAIL_TO";
    
    public final static String  DEC_FILE_UPLOAD_SMTP_HOST="DEC_FILE_UPLOAD_SMTP_HOST";
    public final static String  DK_WF_OUTGOING_PATH="DK_WF_OUTGOING_PATH";
    
    public final static String  TRIDENT_API_TRANSACTION_GTS_USERNAME="TRIDENT_API_TRANSACTION_GTS_USERNAME";
    public final static String  TRIDENT_API_TRANSACTION_GTS_PASSWORD="TRIDENT_API_TRANSACTION_GTS_PASSWORD";
    
    public final static String  DISCOVER_FILE_VERSION_IND="DISCOVER_FILE_VERSION_IND";
    
    public final static String  AUTO_LOGIN_IP_PREFIX="AUTO_LOGIN_IP_PREFIX";
    
    public final static String  BILLING_DB_FTPDB="BILLING_DB_FTPDB";
    public final static String  ACCULYNK_LOADER_FTPDB="ACCULYNK_LOADER_FTPDB";
    public final static String  CHARGEBACK_TOOLS_FTPDB="CHARGEBACK_TOOLS_FTPDB";
    
    public final static String  TRIDENT_API_TRANSACTION_PROFILE_ID_TEST="TRIDENT_API_TRANSACTION_PROFILE_ID_TEST";
    public final static String  TRIDENT_API_TRANSACTION_PROFILE_KEY_TEST="TRIDENT_API_TRANSACTION_PROFILE_KEY_TEST";
    public final static String  TRIDENT_API_TRANSACTION_CARD_NUMBER_TEST="TRIDENT_API_TRANSACTION_CARD_NUMBER_TEST";
    
    public final static String  TRI_CIPHER_APPLIANCES_IP_ADDRESS_TEST="TRI_CIPHER_APPLIANCES_IP_ADDRESS_TEST";
    public final static String  TRI_CIPHER_APPLIANCES_PORT_TEST      ="TRI_CIPHER_APPLIANCES_PORT_TEST";
    
    public final static String  REPORT_SQLJBEAN_CARD_NUMBER_TEST      ="REPORT_SQLJBEAN_CARD_NUMBER_TEST";
    public final static String  REPORT_SQLJBEAN_CARD_NUMBER_TRUNC_TEST="REPORT_SQLJBEAN_CARD_NUMBER_TRUNC_TEST";
    public final static String  REPORT_SQLJBEAN_CARD_NUMBER_ENC_TEST  ="REPORT_SQLJBEAN_CARD_NUMBER_ENC_TEST";
    public final static String  REPORT_SQLJBEAN_VALID_EMAIL_TEST      ="REPORT_SQLJBEAN_VALID_EMAIL";
    public final static String  REPORT_SQLJBEAN_MERCHANT_ID_TEST      ="REPORT_SQLJBEAN_MERCHANT_ID";
    
    public final static String  DK_API_STIP_TIMEOUT_MINUTES = "DK_API_STIP_TIMEOUT_MINUTES";
    
    //FTP test
    public static final String FTP_TEST_HOST     = "FTP_TEST_HOST";
    public static final String FTP_TEST_USER     = "FTP_TEST_USER";
    public static final String FTP_TEST_PASSWORD = "FTP_TEST_PASSWORD";
    public static final String FTP_TEST_PATH     = "FTP_TEST_PATH";
    
    // TSG
    public static final String DK_TSG_OUTGOING_HOST     = "DK_TSG_OUTGOING_HOST";
    public static final String DK_TSG_OUTGOING_USER     = "DK_TSG_OUTGOING_USER";
    public static final String DK_TSG_OUTGOING_PASSWORD = "DK_TSG_OUTGOING_PASSWORD";
    public static final String DK_TSG_OUTGOING_PATH     = "DK_TSG_OUTGOING_PATH";
    
    // mc test cards
    public static final String SIMULATOR_MC1            = "SIMULATOR_MC1";
    public static final String SIMULATOR_MC2            = "SIMULATOR_MC2";
    public static final String SIMULATOR_MC3            = "SIMULATOR_MC3";
    public static final String SIMULATOR_MC4            = "SIMULATOR_MC4";
    public static final String SIMULATOR_MC5            = "SIMULATOR_MC5";
    public static final String SIMULATOR_MC6            = "SIMULATOR_MC6";
    public static final String SIMULATOR_MC7            = "SIMULATOR_MC7";
    public static final String SIMULATOR_MC8            = "SIMULATOR_MC8";
    public static final String SIMULATOR_MC9            = "SIMULATOR_MC9";

    // mc test card ids (card store testing)
    public static final String SIMULATOR_MC1_CARD_ID    = "SIMULATOR_MC1_CARD_ID";
    public static final String SIMULATOR_MC2_CARD_ID    = "SIMULATOR_MC2_CARD_ID";
    public static final String SIMULATOR_MC3_CARD_ID    = "SIMULATOR_MC3_CARD_ID";
    public static final String SIMULATOR_MC4_CARD_ID    = "SIMULATOR_MC4_CARD_ID";
    public static final String SIMULATOR_MC5_CARD_ID    = "SIMULATOR_MC5_CARD_ID";
    public static final String SIMULATOR_MC6_CARD_ID    = "SIMULATOR_MC6_CARD_ID";
    public static final String SIMULATOR_MC7_CARD_ID    = "SIMULATOR_MC7_CARD_ID";
    public static final String SIMULATOR_MC8_CARD_ID    = "SIMULATOR_MC8_CARD_ID";
    public static final String SIMULATOR_MC9_CARD_ID    = "SIMULATOR_MC9_CARD_ID";

    // visa test cards
    public static final String SIMULATOR_VISA1          = "SIMULATOR_VISA1";
    public static final String SIMULATOR_VISA2          = "SIMULATOR_VISA2";
    public static final String SIMULATOR_VISA3          = "SIMULATOR_VISA3";
    public static final String SIMULATOR_VISA4          = "SIMULATOR_VISA4";
    public static final String SIMULATOR_VISA5          = "SIMULATOR_VISA5";
    public static final String SIMULATOR_VISA6          = "SIMULATOR_VISA6";
    public static final String SIMULATOR_VISA7          = "SIMULATOR_VISA7";
    public static final String SIMULATOR_VISA8          = "SIMULATOR_VISA8";
    public static final String SIMULATOR_VISA9          = "SIMULATOR_VISA9";

    // visa test card ids (card store testing)
    public static final String SIMULATOR_VISA1_CARD_ID  = "SIMULATOR_VISA1_CARD_ID";
    public static final String SIMULATOR_VISA2_CARD_ID  = "SIMULATOR_VISA2_CARD_ID";
    public static final String SIMULATOR_VISA3_CARD_ID  = "SIMULATOR_VISA3_CARD_ID";
    public static final String SIMULATOR_VISA4_CARD_ID  = "SIMULATOR_VISA4_CARD_ID";
    public static final String SIMULATOR_VISA5_CARD_ID  = "SIMULATOR_VISA5_CARD_ID";
    public static final String SIMULATOR_VISA6_CARD_ID  = "SIMULATOR_VISA6_CARD_ID";
    public static final String SIMULATOR_VISA7_CARD_ID  = "SIMULATOR_VISA7_CARD_ID";
    public static final String SIMULATOR_VISA8_CARD_ID  = "SIMULATOR_VISA8_CARD_ID";
    public static final String SIMULATOR_VISA9_CARD_ID  = "SIMULATOR_VISA9_CARD_ID";
    
    // disc test cards
    public static final String SIMULATOR_DISC1          = "SIMULATOR_DISC1";
    public static final String SIMULATOR_DISC2          = "SIMULATOR_DISC2";
    public static final String SIMULATOR_DISC3          = "SIMULATOR_DISC3";
    public static final String SIMULATOR_DISC4          = "SIMULATOR_DISC4";
    public static final String SIMULATOR_DISC5          = "SIMULATOR_DISC5";
    public static final String SIMULATOR_DISC6          = "SIMULATOR_DISC6";
    public static final String SIMULATOR_DISC7          = "SIMULATOR_DISC7";
    public static final String SIMULATOR_DISC8          = "SIMULATOR_DISC8";
    public static final String SIMULATOR_DISC9          = "SIMULATOR_DISC9";

    public static final String MESSAGE_CONNECTION_POOL_URL = "MESSAGE_CONNECTION_POOL_URL";
    public static final String PLATFORM_URL = "PLATFORM_URL";

    public static final String EPICOR_DATABASE_DRIVER = "EPICOR_DATABASE_DRIVER";
    public static final String EPICOR_RAC_BATCH_URL = "EPICOR_RAC_BATCH_URL";
    public static final String EPICOR_JACK_USER = "EPICOR_JACK_USER";
    public static final String EPICOR_JACK_PASSWORD = "EPICOR_JACK_PASSWORD";
    public static final String EPICOR_CONNECTION = "EPICOR_CONNECTION";

    public static final String BATCH_PROCESS_MAX_RETRY = "BATCH_PROCESS_MAX_RETRY";
    public static final String BATCH_EXTRACTOR_NOTIFICATION_FROM = "BATCH_EXTRACTOR_NOTIFICATION_FROM";
    public static final String BATCH_EXTRACTOR_NOTIFICATION_TO = "BATCH_EXTRACTOR_NOTIFICATION_TO";
    public static final String DS_RECON_MAILBOX_3003 = "DS_RECON_MAILBOX_3003";

    public static final String READ_TIMEOUT = "PAY_HERE_POST_EVENT_READ_TIMEOUT";   // in milliseconds
    public static final String CONNECT_TIMEOUT = "PAY_HERE_POST_EVENT_CONNECT_TIMEOUT";   // in milliseconds

    private static Hashtable values = null;
    //  private static Timestamp    curChanged          = null;
    private static MesDefaults singleton = null;
	
    //determine the cut over date for FX
    public static final String MC_ENHANCED_FX_ACTIVE_DATE = "ENHANCED_FX_ACTIVE_DATE";
    public static final String MC_ENHANCED_FX_ACTIVE_TIME = "ENHANCED_FX_ACTIVE_TIME";
    public static final String MC_FX_AUTH_NUMBER_DAYS = "FX_AUTH_NUMBER_DAYS";
    
    //Payvision Constants
    public static final String PAYVISION_HOST = "PAYVISION_HOST";
    public static final String PAYVISION_ECOMM_MEMBER_ID = "PAYVISION_ECOMM_MEMBER_ID";
    public static final String PAYVISION_ECOMM_MEMBER_GUID = "PAYVISION_ECOMM_MEMBER_GUID";
    public static final String PAYVISION_CHARGEBACKS_PATH = "/ReportsApiV1.1/Chargebacks.asmx";
    public static final String PAYVISION_DECLINES_PATH = "/ReportsApiV1.1/Declines.asmx";
    public static final String PAYVISION_TRANSACTIONS_PATH = "/ReportsApiV1.1/Transactions.asmx";
    
    //Faster Funding Constant
    public static final String ACH_FASTER_FUNDING_BANKS = "ACH_FASTER_FUNDING_BANKS";
    
    //MC Edits 10 & 16 config switches
    public static final String MC_EDIT_10E_ROLLBACK_SWITCH = "MC_EDIT_10E_ROLLBACK_SWITCH";
    public static final String MC_EDIT_10F_ROLLBACK_SWITCH = "MC_EDIT_10F_ROLLBACK_SWITCH";
    public static final String MC_EDIT_10G_ROLLBACK_SWITCH = "MC_EDIT_10G_ROLLBACK_SWITCH";
    public static final String MC_EDIT_16_ROLLBACK_SWITCH = "MC_EDIT_16_ROLLBACK_SWITCH";

    public static final String PG_TEST_URL="PG_TEST_URL";
    public static final String PG_PRODUCTION_URL="PG_PRODUCTION_URL";

    /*
    ** METHOD private void loadValues()
    **
    ** Loads the key/value pairs from db table mes_defaults and stores them in
    ** a static hashtable.
    */
    private synchronized void loadValues() {
//    ResultSetIterator it    = null;
//    ResultSet         rs    = null;
        try {
//      connect();
//
//      #sql [Ctx] it =
//      {
//        select  key,
//                value
//        from    mes_defaults
//      };
//
//      rs = it.getResultSet();

            if (values == null) {
                values = new Hashtable();
            } else {
                values.clear();
            }

//      while (rs.next())
//      {
//        try
//        {
//          values.put(new Integer(rs.getInt(1)),rs.getString(2));
//        }
//        catch (Exception e)
//        {
//          values.put(new Integer(rs.getInt(1)),"");
//        }
//      }

//      rs.close();
//      it.close();
//
//      // get the last date changed
//      #sql [Ctx]
//      {
//        select  last_date into :curChanged
//        from    mes_defaults_last_changed
//        where   id = 1
//      };
        } catch (Exception e) {
            // singleton.logEntry("loadValues()", e.toString());
        } finally {
//      try { rs.close(); } catch(Exception e) {}
//      try { it.close(); } catch(Exception e) {}
//      cleanUp();
        }
    }

    /*
    ** METHOD public static void refreshValues()
    **
    ** Creates an instance of MesDefaults so that db access can occur.  Calls
    ** loadValues on the instance to refresh the default values hashtable.
    */
    public synchronized static void refreshValues() {
        if (singleton == null) {
            singleton = new MesDefaults();
        }

        singleton.loadValues();
    }

  /*
  ** METHOD private boolean dateChanged
  **
  ** Returns true if the mes_defaults table has changed since last loaded
  */
//  private synchronized boolean dateChanged()
//  {
//    boolean result = false;
//    ResultSetIterator it  = null;
//    ResultSet         rs  = null;
//
//    try
//    {
//      connect();
//
//      Timestamp lastChanged = null;
//
//      #sql [Ctx] it =
//      {
//        select  last_date
//        from    mes_defaults_last_changed
//        where   id = 1
//      };
//
//      rs = it.getResultSet();
//
//      if(rs.next())
//      {
//        lastChanged = rs.getTimestamp("last_date");
//      }
//
//      rs.close();
//      it.close();
//
//      if(lastChanged.getTime() != curChanged.getTime())
//      {
//        result = true;
//      }
//    }
//    catch(Exception e)
//    {
//      // singleton.logEntry("dateChanged()", e.toString());
//    }
//    finally
//    {
//      try { rs.close(); } catch(Exception e) {}
//      try { it.close(); } catch(Exception e) {}
//      cleanUp();
//    }
//
//    return result;
//  }


    /*
    ** METHOD private static void checkValues()
    **
    ** Checks to see if default values hashtable needs to be loaded.  Causes
    ** a refresh of the values if need be.
    */
    private static void checkValues() {
        if (values == null /* || singleton.dateChanged()*/) {
            refreshValues();
        }
    }

    /*
    ** GETTERS
    **
    ** Various methods to retrieve default values of various types.
    */
    private static String getValueString(String key)
            throws Exception {
        String result = "";

        try {
            result = (String) values.get(key);
        } catch (Exception e) {
            // singleton.logEntry("getValueString()", e.toString());
        }

        if (result == null) {
            throw new Exception("Key " + key + " did not exist in MES_DEFAULTS");
        }

        return result;
    }

    public static String getString(String key)
            throws Exception {
        ConfigurationManager instance = ConfigurationManager.getInstance();
        if (!instance.isLoaded())
            instance.loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);
        return instance.getString(key);
    }


    public static double getDouble(String key)
            throws Exception {
        return (getDouble(key, 0.0));
    }

    private static double getDouble(String key, double defaultValue)
            throws Exception {
        double retVal = defaultValue;
        String keyVal = "";

        //checkValues();

        keyVal = getString(key);

        try {
            retVal = Double.parseDouble(keyVal);
        } catch (Exception e) {
            // singleton.logEntry("getDouble()", e.toString());
        }

        return (retVal);
    }

    public static int getInt(String key)
            throws Exception {
        return (getInt(key, 0));
    }

    private static int getInt(String key, int defaultValue)
            throws Exception {
        int result = defaultValue;
        String keyVal = "";

        //checkValues();

        keyVal = getString(key);

        try {
            result = Integer.parseInt(keyVal);
        } catch (Exception e) {
            // singleton.logEntry("getInt()", e.toString());
        }

        return result;
    }

    public static long getLong(String key)
            throws Exception {
        return (getLong(key, 0L));
    }

    private static long getLong(String key, long defaultValue)
            throws Exception {
        long result = defaultValue;
        String keyVal = "";

        //checkValues();

        keyVal = getString(key);

        try {
            result = Long.parseLong(keyVal);
        } catch (Exception e) {
            // singleton.logEntry("getLong()", e.toString());
        }

        return result;
    }

    public static void destroy() {
        System.out.println("Destroying MesDefaults");
        singleton = null;
    }
}
