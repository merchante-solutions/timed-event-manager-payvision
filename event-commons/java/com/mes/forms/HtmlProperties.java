/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlProperties.java $

  Description:
  
  HtmlProperties
  
  Contains a set of properties that may be associated with an html tag.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-22 10:31:52 -0800 (Thu, 22 Feb 2007) $
  Version            : $Revision: 13480 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Enumeration;
import java.util.Hashtable;

public class HtmlProperties
{
  private Hashtable props = new Hashtable();
  
  public String toString()
  {
    StringBuffer propBuf = new StringBuffer("");
    for (Enumeration e = props.keys(); e.hasMoreElements();)
    {
      String key = (String)e.nextElement();
      String value = (String)props.get(key);
      if (!value.equals("empty"))
      {
        propBuf.append(" " + key + "='" + value + "'");
      }
    }
    return propBuf.toString();
  }
  
  public Enumeration keys()
  {
    return props.keys();
  }
  
  public void add(String key)
  {
    props.put(key,"empty");
  }
  
  public String get(String key)
  {
    return (String)props.get(key);
  }
  
  public void set(String key, String value)
  {
    if (props.get(key) != null)
    {
      props.put(key,value);
    }
  }
  
  public void clear(String key)
  {
    if (props.get(key) != null)
    {
      props.put(key,"empty");
    }
  }
  
  public void setAll(HtmlProperties from)
  {
    for (Enumeration e = from.props.keys(); e.hasMoreElements();)
    {
      String key = (String)e.nextElement();
      if (props.get(key) != null)
      {
        props.put(key,from.props.get(key));
      }
    }
  }
  
  public void remove(String key)
  {
    props.remove(key);
  }
}
