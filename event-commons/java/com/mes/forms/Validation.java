/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/Validation.java $

  Description:
  
  Validation
  
  Defines an interface for validation classes.  Validation objects are
  loaded into Field objects and used to validate the Field's data.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public interface Validation
{
  public boolean validate(String fieldData);
  public String getErrorText();
}