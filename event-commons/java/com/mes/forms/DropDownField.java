/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DropDownField.java $

  Description:

  DropDownField

  Wraps a DropDownTable and provides a renderer for the html select tag.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Iterator;
import com.mes.tools.DropDownItem;
import com.mes.tools.DropDownTable;

public class DropDownField extends Field
{
  protected DropDownTable dropDown;

  public DropDownField(String fname, String label, DropDownTable dropDown,
    boolean nullAllowed, String defValue)
  {
    super(fname,label,0,0,nullAllowed);
    this.dropDown = dropDown;

    if(defValue != null && defValue.length() > 0)
      this.setData(defValue);
  }

  public DropDownField(String fname, String label, DropDownTable dropDown,
    boolean nullAllowed)
  {
    this(fname,label,dropDown,nullAllowed,null);
  }

  public DropDownField(String fname, DropDownTable dropDown, boolean nullAllowed)
  {
    this(fname,fname,dropDown,nullAllowed,null);
  }

  public DropDownField(String fname, DropDownTable dropDown, boolean nullAllowed, String defValue)
  {
    this(fname,fname,dropDown,nullAllowed,defValue);
  }

  public String getDescription()
  {
    return dropDown.getDescription(getData());
  }

  protected String processData(String rawData)
  {
    return rawData;
  }

  private String renderOption(DropDownItem item)
  {
    String value = item.getValue() != null ? item.getValue() : "";
    String style = item.getStyle();
    String description 
                 = item.getDescription() != null ? item.getDescription() : "";

    StringBuffer html = new StringBuffer();
    html.append("      <option value=\"" + value + "\"");
    if (fdata != null && fdata.equals(value))
    {
      html.append(" selected");
    }
    if(style != null && !style.equals(""))
    {
      if (style.startsWith("style"))
      {
        html.append(" " + style);
      }
      else
      {
        html.append(" style=\"" + style + "\"");
      }
    }
    html.append(">");
    html.append(description + "</option>\n");
    return html.toString();
  }

  protected String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();

    html.append("    <select name=\"" + fname + "\" ");
    if (tabIndex > 0)
    {
      html.append("tabindex=\"" + tabIndex + "\" ");
    }
    html.append(getHtmlExtra());
    html.append(">\n");

    for (Iterator i = dropDown.getItems().iterator(); i.hasNext();)
    {
      html.append(renderOption((DropDownItem)i.next()));
    }
    html.append("    </select>");

    return html.toString();
  }
  public String getRenderData()
  {
    String renderData = dropDown.getDescription(getData());
    if (renderData == null)
    {
      return getData();
    }
    return renderData;
  }
  
  public void setSelectedItem( int itemIdx )
  {
    if ( dropDown.setCurIdx(itemIdx) )
    {
      setData( dropDown.getCurrentValue() );
    }
  }
}
