/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/MapperField.java $

  Description:
  
  MapperField
  
  Maps some number of contained fields to the value present in a map field.
  This class wraps methods in Field so that when rendering, getting data, etc.
  a mapped field is substituted based on the current value of the mapping 
  fields value.
  
  Describes a generic data field.  A Field contains data and can validate
  it.  Fields have the ability to render themselves as an html form tag.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Hashtable;
import java.util.Iterator;

public class MapperField extends Field
{
  private Hashtable mapHash = new Hashtable();
  private Field keyField;

  public MapperField(String fname, Field keyField, String defaultValue)
  {
    super(fname,0,0,true);
    
    this.keyField = keyField;
    
    HiddenField defaultField = new HiddenField(fname);
    mapHash.put(fname + ".default",defaultField);
    defaultField.setData(defaultValue);
  }
  public MapperField(String fname, Field keyField)
  {
    this(fname,keyField,"default");
  }
  
  public Field addMap(String mapKey, Field mapField)
  {
    mapField.setName(fname);
    if (mapField instanceof ButtonField)
    {
      ((ButtonField)mapField).setButtonName(fname);
    }
    mapHash.put(mapKey,mapField);
    return mapField;
  }
  
  public Field getMapField()
  {
    Field mapField = (Field)mapHash.get(keyField.getData());
    if (mapField == null)
    {
      mapField = (Field)mapHash.get(fname + ".default");
    }
    return mapField;
  }
  
  public Field getMapField(String mapKey)
  {
    return (Field)mapHash.get(mapKey);
  }
  
  // validation
  public void removeValidation(String name) { getMapField().removeValidation(name); }
  public String addValidation(Validation validation, String name) { return getMapField().addValidation(validation,name); }
  public String addValidation(Validation validation) { return getMapField().addValidation(validation); }
  public void setOptionalCondition(Condition condition) { getMapField().setOptionalCondition(condition); }
  public void makeRequired() { getMapField().makeRequired(); }
  public void makeOptional() { getMapField().setNullAllowed(true); }
  public void setNullAllowed(boolean newValue) { getMapField().setNullAllowed(newValue); }
  protected void validateData() { getMapField().validateData(); }
  public boolean isValid() { return getMapField().isValid(); }
  
  // rendering
  protected String renderErrorIndicator() { return getMapField().renderErrorIndicator(); }
  protected String renderHtmlField() { return getMapField().renderHtmlField(); }
  public String renderAsHidden() { return getMapField().renderAsHidden(); }
  public String renderHtml() { return getMapField().renderHtml(); }
  public String getRenderData() { return getMapField().getRenderData(); }
  
  // general  
  public String getName() { return getMapField().getName(); }
  public String getShowName() { return getMapField().getShowName(); }
  public String getLabel() { return getMapField().getLabel(); }
  public int getLength() { return getMapField().getLength(); }
  public boolean getNullAllowed() { return getMapField().getNullAllowed(); }
  public String getHtmlExtra() { return getMapField().getHtmlExtra(); }
  public String getErrorText() { return getMapField().getErrorText(); }
  public boolean getHasError() { return getMapField().getHasError(); }
  public boolean getShowErrorText() { return getMapField().getShowErrorText(); }
  public String getOriginalData() { return getMapField().getOriginalData(); }
  public String getData() { return getMapField().getData(); }
  public boolean getHasChanged() { return getMapField().getHasChanged(); }
  protected String processData(String rawData) { return getMapField().processData(rawData); }
  
  public void setData(String fdata) { getMapField().setData(fdata); }
  public void setDataTrim(String fdata) { getMapField().setDataTrim(fdata); }
  public void setOriginalData(String odata) { getMapField().setOriginalData(odata); }
  public void setShowName(String sname) { getMapField().setShowName(sname); }
  public void setLabel(String label) { getMapField().setLabel(label); }
  public void setHtmlSize(int s) { getMapField().setHtmlSize(s); }

  public boolean isBlank() { return getMapField().isBlank(); }
  public double asDouble() { return getMapField().asDouble(); }
  public double asFloat() { return getMapField().asFloat(); }
  public int asInteger() { return getMapField().asInteger(); }
  public long asLong() { return getMapField().asLong(); }


  public void setHtmlExtra(String htmlExtra)
  {
    for (Iterator i = mapHash.values().iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).setHtmlExtra(htmlExtra);
      }
      else
      {
        f.setHtmlExtra(htmlExtra);
      }
    }
  }
  
  public void addHtmlExtra(String newValue)
  {
    for (Iterator i = mapHash.values().iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        ((FieldGroup)f).addHtmlExtra(htmlExtra);
      }
      else
      {
        f.addHtmlExtra(htmlExtra);
      }
    }
  }
  
  public void setShowErrorText(boolean showErrorText) { getMapField().setShowErrorText(showErrorText); }
  public void setFixImage(String newPath, int newWidth, int newHeight) { getMapField().setFixImage(newPath,newWidth,newHeight); }
  public void reset() { getMapField().reset(); }  
  
}
  
