/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlText.java $

  Description:

  HtmlText
  
  This is a basic html element that contains a string that will be rendered
  verbatim.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlText implements HtmlRenderable
{
  private String text;
  
  public HtmlText()
  {
    this("");
  }
  public HtmlText(String text)
  {
    this.text = text;
  }
  
  public void setText(String text)
  {
    if (text != null)
    {
      this.text = text;
    }
  }
  
  public String getText()
  {
    return text;
  }
  
  public String renderHtml()
  {
    return text;
  }
}
