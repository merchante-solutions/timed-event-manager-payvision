/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/FieldError.java $

  Description:
  
  FieldError
  
  Encapsulates a validation error in a Field object.  Stores a reference
  to the field and provides methods for accessing information in the
  field that are relevant to the error, including methods to help generate
  a marker useful in making a link that helps a user navigate to the field's
  location on the page.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class FieldError
{
  private Field field;
  
  public FieldError(Field field)
  {
    this.field = field;
  }
  
  public Field getField()
  {
    return field;
  }
  
  public String getErrorText()
  {
    return field.getErrorText();
  }
  
  public String getFieldName()
  {
    return field.getName();
  }
  
  public String getFieldLabel()
  {
    return field.getLabel();
  }
  
  public String getFieldErrorMarker()
  {
    return "marker for " + field.getName();
  }
  
  public String toString()
  {
    return getErrorText();
  }
}

