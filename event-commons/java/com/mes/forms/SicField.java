/*@lineinfo:filename=SicField*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/SicField.sqlj $

  Description:
  
  SicCodeField
  
  Field used to hold sic codes w/ validation.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


public final class SicField extends Field
{
  public SicField(String fname, String label, boolean nullAllowed)
  {
    super(fname, label, 4, 4, nullAllowed);
    
    addValidation( new SicValidation() );
  }
  
  public class SicValidation extends SQLJConnectionBase
    implements Validation
  {
    public boolean validate(String fieldData)
    {
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }
      
      boolean isValid = false;
      
      try
      {
        connect();
        
        int intSic = -1;

        try
        {        
          intSic = Integer.parseInt(fieldData);
        }
        catch (Exception e) {}

        ResultSetIterator it = null;        
        /*@lineinfo:generated-code*//*@lineinfo:68^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code
//            from    sic_codes
//            where   sic_code = :intSic
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code\n          from    sic_codes\n          where   sic_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.SicField",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,intSic);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.SicField",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^9*/
        
        isValid = it.getResultSet().next();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::validate(fieldData = " 
          + fieldData + ")";
        String desc = e.toString();
        System.out.println(func + ": " + desc);
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }
      
      return isValid;
    }
        
    public String getErrorText()
    {
      return "Invalid Sic Code";
    }
  }

}/*@lineinfo:generated-code*/