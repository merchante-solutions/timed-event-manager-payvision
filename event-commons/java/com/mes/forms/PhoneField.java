/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/PhoneField.java $

  Description:
  
  PhoneField
  
  A field containing a phone number.  Validates the number and formats it
  when rendering it.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;

public class PhoneField extends Field
{
  public class PhoneNumberValidation implements Validation
  {
    private String errorText = "";
    
    /*
    ** public boolean validate(String fieldData)
    **
    ** Determines if fieldData is a valid phone number, which is to say
    ** it contains exactly 10 numeric digits.
    **
    ** RETURNS: true if fieldData contains a valid number phone number,
    **          else false.
    */
    public boolean validate(String fieldData)
    {
      boolean isValid = true;
      
      try
      {
        // ignore null fields
        if (fieldData != null && fieldData.length() > 0)
        {
          // see if invalid characters in the mix
          try
          {
            Long.valueOf(fieldData);
          }
          catch (NumberFormatException ne)
          {
            throw new Exception("Not a valid phone number");
          }
      
          // check for invalid phone length
          if (fieldData.length() != 10)
          {
            throw new Exception("Phone number must be 10 digits long");
          }
        }
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() == 0)
        {
          errorText = e.toString();
        }
        isValid = false;
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }

  public PhoneField(String fname,
                    String label,
                    boolean nullAllowed)
  {
    // sizing allows 10 digits numbers with up to 3 formatting chars
    // (xxx)xxx-xxxx, xxx.xxx.xxxx, xxx xxx xxxx, etc.
    super(fname,label,13,13,nullAllowed);
    addValidation(new PhoneNumberValidation());
  }
  public PhoneField(String fname,
                    boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }
  
  public String getRenderData()
  {
    if (!hasError)
    {
      return format(fdata);
    }
    return fdata;
  }
  
  public static String format(String phoneNumber)
  {
    if (phoneNumber.length() == 10)
    {
      return  phoneNumber.substring(0,3) + "-" + 
              phoneNumber.substring(3,6) + "-" + 
              phoneNumber.substring(6,10);
    }
    return phoneNumber;
  }
  
  /*
  ** protected String processData(String rawData)
  **
  ** Strips out valid formatting characters so that valid phone numbers
  ** will be stored internally as just a string of numeric digits.
  **
  ** RETURNS: processed data if valid, else data as is if invalid.
  */
  protected String processData(String rawData)
  {
    try
    {
      // scan through string, strip valid formatting chars
      StringTokenizer tok = new StringTokenizer(rawData," ()-.");
      StringBuffer tokBuf = new StringBuffer();
      while (tok.hasMoreTokens())
      {
        tokBuf.append(tok.nextToken());
      }
    
      // parse the stripped string to make sure it has only numeric data
      Long.parseLong(tokBuf.toString());
    
      // don't filter data that's wrong length
      if (tokBuf.length() != 10)
      {
        throw new Exception("bad length");
      }
    
      // return the filtered data
      return tokBuf.toString();
    }
    catch (Exception e) { }
    
    return rawData;
  }
}
