/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ComboDateField.java $

  Description:
  
  ComboDateField
  
  Specialized field group that contains three drop down fields representing
  the month, day and year of a date.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-10-22 16:10:59 -0700 (Mon, 22 Oct 2007) $
  Version            : $Revision: 14231 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DropDownTable;

public class ComboDateField extends FieldGroup
{
  public static class DayDropDownField extends DropDownField
  {
    private class DayTable extends DropDownTable
    {
      public DayTable()
      {
        for (int i = 1; i <= 31; ++i)
        {
          String dayStr = Integer.toString(i);
          addElement(dayStr,dayStr);
        }
      }
    }
  
    public DayDropDownField(String fname,
                     boolean nullAllowed)
    {
      super(fname,null,nullAllowed);
      dropDown = new DayTable();
    }
  }
  
  public static class MonthDropDownField extends DropDownField
  {
    public static String[] months =
    {
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    };
  
    private class MonthTable extends DropDownTable
    {
      public MonthTable( int monthInc )
      {
        for (int i = 0; i < months.length; ++i)
        {
          addElement(Integer.toString(i + monthInc),months[i]);
        }
      }
    }
  
    public MonthDropDownField(String fname,
                              boolean nullAllowed,
                              int monthInc)
    {
      super(fname,null,nullAllowed);
      dropDown = new MonthTable( monthInc );
    }
    public MonthDropDownField(String fname,
                              boolean nullAllowed)
    {
      this(fname,nullAllowed,1);
    }                              
  }

  public static class YearDropDownField extends DropDownField
  {
    private int currentYear = Calendar.getInstance().get(Calendar.YEAR);

    private class YearTable extends DropDownTable
    {
      private     boolean     DescendingFlag    = false;
      private     int         MinYear           = 2000;
      
      public YearTable( int minYear)
      {
        init(minYear,false);
      }
      
      public YearTable( int minYear, boolean descending )
      {
        init(minYear,descending);
      }
      
      public boolean getDescendingFlag()
      {
        return( DescendingFlag );
      }
      
      public int getMinYear()
      {
        return( MinYear );
      }
      
      public void init( int minYear, boolean descending )
      {
        String yearStr = null;
        
        MinYear         = minYear;
        DescendingFlag  = descending;
        
        if ( descending )
        {
          for (int i = currentYear; i >= minYear; --i)
          {
            yearStr = Integer.toString(i);
            addElement(yearStr,yearStr);
          }
        }
        else  // ascending by default
        {
          for (int i = minYear; i <= currentYear; ++i)
          {
            yearStr = Integer.toString(i);
            addElement(yearStr,yearStr);
          }
        }          
      }
    }
  
    public YearDropDownField(String fname,
                     boolean nullAllowed)
    {
      this(fname,nullAllowed,2000,false);
    }
    
    public YearDropDownField( String fname,
                              boolean nullAllowed,
                              int minYear )
    {
      this(fname,nullAllowed,minYear,false);
    }
    
    public YearDropDownField( String fname,
                              boolean nullAllowed,
                              int minYear,
                              boolean descendingOrder )
    {
      super(fname,null,nullAllowed);
      dropDown = new YearTable(minYear,descendingOrder);
    }
    
    public void setDescendingFlag( boolean descending )
    {
      YearTable     yearDropDown = (YearTable)dropDown;
      
      if ( yearDropDown.getDescendingFlag() != descending )
      {
        // create a new table with the request sort order
        yearDropDown = new YearTable( yearDropDown.getMinYear(), descending );
      }        
    }
  }
  
  private Field     dayField;
  private Field     monthField;
  private Field     yearField;
  
  private boolean   allowExpansion      = true;
  private int       monthIncrement      = 1;
  private boolean   useDay;
  private boolean   showDayOfWeek       = false;
  
  /*
  ** public ComboDateField(String fname)
  **
  ** Constructor.
  */
  public ComboDateField(String fname, String label, boolean useDay, 
                        boolean nullAllowed, int minYear, boolean adjustMonth,
                        boolean yearsInDescendingOrder)
  {
    super(fname,label);
    
    monthIncrement = ( adjustMonth ? 1 : 0 );
    
    // create the date component drop downs
    if (useDay)
    {
      dayField    = new DayDropDownField(fname + ".day",nullAllowed);
    }
    else
    {
      dayField  = new HiddenField(fname + ".day");
    }
    monthField  = new MonthDropDownField(fname + ".month",nullAllowed,monthIncrement);
    yearField   = new YearDropDownField(fname + ".year",nullAllowed,minYear,yearsInDescendingOrder);
    add(dayField);
    add(monthField);
    add(yearField);
    
    this.useDay       = useDay;
    
    // default values to current date
    Calendar  cal = Calendar.getInstance();
    dayField.setData(String.valueOf(useDay ? cal.get(Calendar.DAY_OF_MONTH) : 1 ));
    monthField.setData(String.valueOf(cal.get(Calendar.MONTH) + monthIncrement));
    yearField.setData(String.valueOf(cal.get(Calendar.YEAR)));
  }
  public ComboDateField(String fname, String label, boolean useDay, 
                        boolean nullAllowed, int minYear, boolean adjustMonth)
  {
    this(fname,label,useDay,nullAllowed,minYear,adjustMonth,false);
  }
  public ComboDateField(String fname, String label, boolean useDay, 
                        boolean nullAllowed)
  {
    this(fname,label,useDay,nullAllowed,2000,true);
  }
  public ComboDateField(String fname, boolean useDay, boolean nullAllowed)
  {
    this(fname,fname,useDay,nullAllowed);
  }
  public ComboDateField(String fname,boolean nullAllowed)
  {
    this(fname,true,nullAllowed);
  }
  
  public String getHtmlDate()
  {
    StringBuffer html = new StringBuffer();
    
    html.append(getName());
    html.append(".month=");
    html.append(monthField.getData());
    html.append("&");
    html.append(getName());
    html.append(".day=");
    html.append(dayField.getData());
    html.append("&");
    html.append(getName());
    html.append(".year=");
    html.append(yearField.getData());
    
    return html.toString();
  }

  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();

    html.append(monthField.renderHtml());
    html.append(dayField.renderHtml());
    html.append(yearField.renderHtml());
    
    if ( this.showDayOfWeek == true )
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime( getUtilDate() );
      html.append( "&nbsp;&nbsp;<span class=\"smallGrey\">" );
      html.append( getDayString(cal.get(Calendar.DAY_OF_WEEK)) );
      html.append( "</span>" );
    }
    
    return html.toString();
  }
  
  public String renderAsHidden()
  {
    StringBuffer html = new StringBuffer();
    
    html.append((new HiddenField(dayField)).renderHtml());
    html.append((new HiddenField(monthField)).renderHtml());
    html.append((new HiddenField(yearField)).renderHtml());
    
    return html.toString();
  }
  
  public String getDateString()
  {
    return( DateTimeFormatter.getFormattedDate( getUtilDate(), "MM/dd/yyyy") );
  }            
  
  public String getDayString(int day)
  {
    String result = "";
    
    switch(day)
    {
      case Calendar.MONDAY:
        result = "Mon";
        break;
      case Calendar.TUESDAY:
        result = "Tue";
        break;
      case Calendar.WEDNESDAY:
        result = "Wed";
        break;
      case Calendar.THURSDAY:
        result = "Thu";
        break;
      case Calendar.FRIDAY:
        result = "Fri";
        break;
      case Calendar.SATURDAY:
        result = "Sat";
        break;
      case Calendar.SUNDAY:
        result = "Sun";
        break;
    }
    
    return result;
  }
  
  public Date getUtilDate()
  {
    Calendar      cal       = Calendar.getInstance();
    
    cal.set( Calendar.MONTH, (monthField.asInteger() - monthIncrement) );
    cal.set( Calendar.DAY_OF_MONTH, dayField.asInteger() );
    cal.set( Calendar.YEAR, yearField.asInteger() );
  
    return( cal.getTime() );
  }
  
  public java.sql.Date getSqlDate()
  {
    return new java.sql.Date(getUtilDate().getTime());
  }
  
  public void setUtilDate(Date newDate)
  {
    Calendar  cal = Calendar.getInstance();
    
    cal.setTime(newDate);
    dayField.setData(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
    monthField.setData(String.valueOf(cal.get(Calendar.MONTH) + monthIncrement));
    yearField.setData(String.valueOf(cal.get(Calendar.YEAR)));
  }
  
  public void setExpansionFlag( boolean newValue )
  {
    this.allowExpansion = newValue;
  }
  
  public void setDayOfWeekVisibleFlag( boolean newValue )
  {
    this.showDayOfWeek = newValue;
  }
  
  public void setYearOrderAscending()
  {
    ((YearDropDownField)yearField).setDescendingFlag(false);
  }
  
  public void setYearOrderDescending()
  {
    ((YearDropDownField)yearField).setDescendingFlag(true);
  }

  public void addDays(int days)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(getUtilDate());
    cal.add(Calendar.DATE,days);
    setUtilDate(cal.getTime());
  }
  
  /*
  ** protected String processData(String rawData)
  **
  ** This method is called by setData in Field class, and is used to set
  ** the internal drop downs via a string representation of a date.
  **
  ** RETURNS: empty string (Field's fdata storage is irrelevant to 
  **          this class).
  */
  protected String processData(String rawData)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      ParsePosition pos = new ParsePosition(0);
      Date utilDate = sdf.parse(rawData,pos);
      setUtilDate(utilDate);
    }
    catch (Exception e) {}
    
    return "";
  }
  
  public String getSqlDateString()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
    StringTokenizer tok = new StringTokenizer(sdf.format(getUtilDate()),"-");
    return tok.nextToken() + "-" + 
           tok.nextToken().substring(0,3) + "-" + 
           tok.nextToken();
  }
  
  public Vector getFieldsVector()
  {
    Vector allFields = null;
    
    if ( allowExpansion )
    {
      allFields = super.getFieldsVector();  // default action
    }
    else
    {
      // when expansion is off, return 
      // the ComboDateField (a FieldGroup)
      // as an individual field rather
      // than returning the day, month and
      // year fields individually.
      allFields = new Vector();
      allFields.add(this);
    }
    return( allFields );
  }
}
