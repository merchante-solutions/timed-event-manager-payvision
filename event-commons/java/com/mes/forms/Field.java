/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/Field.java $

  Description:

  Field

  Describes a generic data field.  A Field contains data and can validate
  it.  Fields have the ability to render themselves as an html form tag.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-02-01 11:15:45 -0800 (Wed, 01 Feb 2012) $
  Version            : $Revision: 19750 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;

public class Field extends SQLJConnectionBase implements HtmlField
{
  // create class log category
  static Logger log = Logger.getLogger(Field.class);

  protected String    fixImagePath    = "/images/fixfield.gif";
  protected int       fixImageWidth   = 21;
  protected int       fixImageHeight  = 9;

  protected String    fname;
  protected String    sname;                        // ("show" name)
  protected String    fdata           = "";

  protected String    originalData    = null;
  protected int       length;
  protected int       htmlSize;
  protected boolean   nullAllowed;
  protected String    htmlExtra       = "";
  protected boolean   showErrorText   = false;
  protected boolean   hasChanged      = false;
  protected boolean   readOnly        = false;
  protected String    defaultValue    = "";
  protected int       tabIndex        = 0;

  protected String    errorText;
  protected String    helpHtmlText    = "";
  protected boolean   hasError;
  protected boolean   needsValidation = false;
  private   Hashtable validations     = new Hashtable();
  private   int       unnamedValCount = 0;

  // parameter to provide auto-changelog functionality
  protected boolean   trackChanges    = false;

  // if true this parameter will prevent a field value from being changed
  // after it was originally set
  protected boolean   immutable       = false;

  /*
  ** public Field(String fname, String label, int length, int htmlSize,
  **   boolean nullAllowed)
  **
  ** Constructor.
  **
  ** Includes a label (sname).
  */
  public Field(String fname, String label, int length, int htmlSize,
    boolean nullAllowed)
  {
    this.fname        = fname;
    this.sname        = label;
    this.length       = length;
    this.htmlSize     = htmlSize;
    this.nullAllowed  = nullAllowed;

    if (length > 0)
    {
      addValidation(new MaxLengthValidation(length),"maxlength");
    }

    if (!nullAllowed)
    {
      addValidation(new RequiredValidation(),"required");
    }
  }

  /*
  ** public Field(String fname, int length, int htmlSize, boolean nullAllowed)
  **
  ** Constructor.
  */
  public Field(String fname, int length, int htmlSize, boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }

  public Field(String fname, String label, boolean nullAllowed)
  {
    this(fname,label,20,10,nullAllowed);
  }

  /*
  ** public boolean equals(Object that)
  **
  ** Determines equality based on type and name of field.
  **
  ** RETURNS: true if that is instance of Field and has the same name as this.
  */
  public boolean equals(Object that)
  {
    return (  that != null
              && that instanceof Field
              && ((Field)that).getName().equals(getName()) );
  }

  /*
  ** public void setTrackChanges(boolean)
  **
  ** Enables or disables auto-tracking of field data changes
  */
  public void setTrackChanges(boolean trackChanges)
  {
    this.trackChanges = trackChanges;
  }

  public boolean trackChanges()
  {
    return trackChanges;
  }

  public void setImmutable(boolean immutable)
  {
    this.immutable = immutable;
  }

  public boolean isImmutable()
  {
    return immutable;
  }


  /*************************************************************************
  **
  ** Validation classes
  **
  *************************************************************************/

  public class RequiredValidation implements Validation
  {
    // optional condition is set to be always true by default
    private Condition optionalCondition = new Condition()
    {
      public boolean isMet() { return true; }
    };

    public void setOptionalCondition(Condition optionalCondition)
    {
      this.optionalCondition = optionalCondition;
    }

    public boolean validate(String fieldData)
    {
      // if field is not blank then the required validation is met
      if (fieldData != null && fieldData.length() > 0)
      {
        return true;
      }

      // if the field has an optional condition that is not met it
      // is not considered required, so validation is true
      if (!optionalCondition.isMet())
      {
        return true;
      }

      // field is blank and either the conditional requirement is met
      // or it is unconditionally required, so validation fails
      return false;

      /*
      // if field is in a field group, the group will set it's condition
      // in each of it's fields, so if optional condition is set
      // in a field and the condition is met, then field is not considered
      // required
      if ((optionalCondition != null && !optionalCondition.isMet())
          || (fieldData != null && fieldData.length() > 0))
      {
        return true;
      }
      return false;
      */
    }

    public String getErrorText()
    {
      return "Field required";
    }
  }

  public class MaxLengthValidation implements Validation
  {
    int maxLength;

    public MaxLengthValidation(int maxLength)
    {
      this.maxLength = maxLength;
    }

    public boolean validate(String fieldData)
    {
      if (fieldData == null || fieldData.length() <= maxLength)
      {
        return true;
      }

      log.debug("field " + getLabel() + " length=" + fieldData.length() + ", max = " + maxLength + " ("+fieldData+")");
      return false;
    }

    public String getErrorText()
    {
      String label = getLabel();
      if (label.equals(getName()))
      {
        return "Field too long";
      }
      return label + " too long";
    }
  }

  public class MinLengthValidation implements Validation
  {
    private int       MinLength   = 0;

    public MinLengthValidation(int minLength)
    {
      MinLength = minLength;
    }

    public boolean validate(String fieldData)
    {
      boolean     retVal      = false;

      // pass if the field is empty, blank or over the min length
      if (fieldData == null || fieldData.equals("") ||
           fieldData.length() >= MinLength)
      {
        retVal = true;
      }
      return( retVal );
    }

    public String getErrorText()
    {
      String label = getLabel();
      if (label.equals(getName()))
      {
        return "Field too short";
      }
      return label + " too short";
    }
  }

  /*************************************************************************
  **
  ** Validation handling
  **
  *************************************************************************/

  /*
  ** public void removeAllValidation()
  **
  ** Resets the validation hash to empty.
  */
  public void removeAllValidation()
  {
    validations = new Hashtable();
    nullAllowed = true;
  }

  /*
  ** public void removeValidation(String name)
  **
  ** Removes the named validation.
  */
  public void removeValidation(String name)
  {
    validations.remove(name);

    if(name !=null && name.equals("required"))
      nullAllowed = true;
  }

  /*
  ** public String addValidation(Validation validation,String name)
  **
  ** Adds a validation with the given name to the validations hash table.
  ** If a validation with the same name is already in the table it is removed.
  **
  ** RETURNS: String name of the added validation.
  */
  public String addValidation(Validation validation, String name)
  {
    removeValidation(name);
    validations.put(name,validation);

    if(name !=null && name.equals("required"))
      nullAllowed = false;

    return name;
  }

  /*
  ** public String addValidation(Validation validation)
  **
  ** Generates a unique name and adds the validation using that name.
  **
  ** RETURNS: String name of the added validation.
  */
  public String addValidation(Validation validation)
  {
    String name = validation.getClass().getName() + "_" + (++unnamedValCount);
    return addValidation(validation,name);
  }

  /*
  ** public void setOptionalCondition(Condition optionalC)
  **
  ** If a required validation exists, it's optional condition is set to the
  ** given condition.
  */
  public void setOptionalCondition(Condition condition)
  {
    RequiredValidation rv = (RequiredValidation)validations.get("required");
    if (rv != null)
    {
      rv.setOptionalCondition(condition);
    }
  }

  /*
  ** public void makeRequired()
  **
  ** Adds a required validation (just as in the constructor).  Allows a field
  ** to be made required at some point after construction.  Note, if a required
  ** validation already exists it will be replaced with a new generic version
  ** (optional condition in original required validation will be lost if there
  ** was one).
  */
  public void makeRequired()
  {
    setNullAllowed(false);
  }

  public void makeOptional()
  {
    setNullAllowed(true);
  }

  public void setNullAllowed( boolean newValue )
  {
    this.nullAllowed = newValue;

    if(nullAllowed)
      removeValidation("required");
    else
      addValidation(new RequiredValidation(),"required");
  }

  /**
   * public Validation getValidation(String name)
   *
   * Returns the named validation if it exists.
   */
  public Validation getValidation(String name)
  {
    return (Validation)validations.get(name);
  }

  /*
  ** protected void validateData()
  **
  ** Iterates through validations, if an error condition is detected hasError
  ** is set to true and errorText is set to the validation objects error text.
  */
  protected void validateData()
  {
    // call getData instead of using the fdata
    // member variable to insure that group
    // fields (i.e. CityStateZipField) return
    // the proper value.
    String        fieldData     = getData();

    if (true)
    {
      hasError = false;
      for (Iterator i = validations.values().iterator(); i.hasNext();)
      {
        Validation v = (Validation)i.next();
        if (!v.validate(fieldData))
        {
          hasError = true;
          errorText = v.getErrorText();
          // print error to console
          log.debug(getName() + " failed validation: " + v.getErrorText());
          break;
        }
      }
      needsValidation = false;
    }
  }

  /*
  ** public void resetErrorState()
  **
  ** This can be used by session persistent field beans to reset errors between
  ** requests.
  */
  public void resetErrorState()
  {
    hasError = false;
    errorText = null;
  }

  /*
  ** public boolean isValid()
  **
  ** Validates data.
  **
  ** RETURNS: true if no validation errors, else false.
  */
  public boolean  isValid()
  {
    validateData();
    return !hasError;
  }

  /*************************************************************************
  **
  ** HTML rendering
  **
  *************************************************************************/

  /*
  ** public HtmlImage getFixImage()
  **
  ** RETURNS: html image tag to display an error tag indicator.
  */
  public HtmlImage getFixImage()
  {
    HtmlImage img = new HtmlImage();
    img.setImage(fixImagePath);
    img.setHeight(fixImageHeight);
    img.setWidth(fixImageWidth);
    String errTxt = (errorText == null ? "" : errorText);
    img.setProperty("align","absmiddle");
    img.setProperty("alt",errTxt);
    img.setProperty("title",errTxt);
    img.setProperty("id","errorind_" + getName());

    return img;
//    return  "<img align=\"absmiddle\" alt=\"" + errorText
//         + "\" border=\"0\" height=\"" + fixImageHeight + "\" "
//         + "width=\"" + fixImageWidth + "\" src=\"" + fixImagePath
//         + "\">";
  }

  /*
  ** protected String renderErrorIndicator()
  **
  ** If the field's data is invalid then an error tag is generated.  The tag
  ** contents is either the error text or a graphical error tag defined by
  ** the fixImage... strings.
  **
  ** RETURNS: an error tag if error condition exists, else an empty String.
  */
  protected String renderErrorIndicator()
  {
    String errorHtml = "";

    if (hasError)
    {
      if (showErrorText)
      {
        errorHtml = "<span class=\"fErrorText\"><nobr>&nbsp;"
                    + errorText + "</nobr></span>";
      }
      else
      {
        errorHtml = getFixImage().renderHtml();
      }
    }

    return errorHtml;
  }

  /*
  ** protected String renderHtmlHelpText()
  **
  ** If the field needs further explanation, such as format info etc
  **
  ** RETURNS: help text if present, else an empty String.
  */
  public void setHtmlHelpText(String text)
  {
    //probably add in an int reference for Bold, Italic
    helpHtmlText = "&nbsp;&nbsp;"+text;
  }

  protected String renderHtmlHelpText()
  {
    return helpHtmlText;
  }

  /*
  ** protected String renderHtmlField()
  **
  ** Generates HTML form input field.  This is the method that would normally
  ** be overwritten by child classes to define different HTML form fields.
  **
  ** RETURNS: HTML form field as a String.
  */
  protected String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"text\" ");
    html.append("size=\"" + (htmlSize > 0 ? htmlSize : length) + "\" ");
    html.append("maxlength=\"" + length + "\" ");
    html.append("name=\"" + getName() + "\" ");
    if (tabIndex > 0)
    {
      html.append("tabindex=\"" + tabIndex + "\" ");
    }
    if (fdata != null)
    {
      html.append("value=\"" + getRenderData() + "\" ");
    }
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }

  /*
  ** public String renderAsHidden()
  **
  ** Renders the field data in the form of a hidden html form field.
  **
  ** RETURNS: HTML hidden form field with the fields data and name.
  */
  public String renderAsHidden()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"hidden\" ");
    html.append("name=\"" + getName() + "\" ");
    html.append("value=\"" + getData() + "\" ");
    html.append(">");
    return html.toString();
  }

  /*
  ** public String renderHtml()
  **
  ** Default field behavior is to render an HTML form field followed by
  ** an error tag if error condition exists.  Child classes may modify this
  ** behavior by overriding the individual render routines or by overriding
  ** this method.
  **
  ** RETURNS: HTML field followed by html error indicator as a String.
  */
  public String renderHtml()
  {
    if( readOnly )
    {
      return( getRenderData() + renderAsHidden() );
    }

    return( renderHtmlField() + renderErrorIndicator() + renderHtmlHelpText() );
  }

  /*
  ** public String getRenderData()
  **
  ** This determines how the field's data will appear in rendered HTML form.
  ** Default behavior is to simply return the field data.  Child classes may
  ** want to override this in order to return something other than the default.
  **
  ** RETURNS: a String containing the data as it should appear in the rendered
  **          HTML data.
  */
  public String getRenderData()
  {
    return getData();
  }

  /*
  ** public String toString()
  **
  ** Simple method to show the field name and value
  **
  ** RETURNS: a String containing the field name and value
  */
  public String toString()
  {
    StringBuffer result = new StringBuffer(getShowName());
    result.append(" ("+getName()+")");
    result.append( ": \n  " );
    result.append( ( fdata != null ? fdata : "") );
    result.append("\n");

    return( result.toString() );
  }

  /*************************************************************************
  **
  ** Getters and Setters
  **
  *************************************************************************/

  public String   getName()           { return fname; }
  public String   getShowName()       { return sname; }
  public String   getLabel()          { return sname; }
  public int      getLength()         { return length; }
  public boolean  getNullAllowed()    { return nullAllowed; }
  public String   getHtmlExtra()      { return htmlExtra; }
  public String   getErrorText()      { return errorText; }
  public boolean  getHasError()       { return hasError; }
  public boolean  getShowErrorText()  { return showErrorText; }
  public String   getOriginalData()   { return originalData; }
  public String   getData()           { return (fdata != null ? fdata : ""); }
  public int      getDataLength()     { return getData().length(); }
  public String   getHtmlData()       { return (getData().equals("") ? "&nbsp;" : getData()); }
  public int      getTabIndex()       { return tabIndex; }

  //allows more complex children to override (dropdownfield) so display data is more felxible
  public String   getDescription()    { return (fdata != null ? fdata : ""); }
  /*
  ** public boolean getHasChanged()
  **
  ** Useful for determining if the field's data has been changed from what it
  ** was initially set to.
  **
  ** RETURNS: true if field's initial data has been changed, else false.
  */
  public boolean getHasChanged()
  {
    String  origData  = originalData;
    String  curData   = fdata;

    // ignore differences that are cosmetic only
    if( this instanceof NumberField || this instanceof CurrencyField || this instanceof CurrencyPrettyField )
    {
      try
      {
        // normalize numeric data to same format
        curData = Double.toString(Double.parseDouble(fdata));
        origData = Double.toString(Double.parseDouble(originalData));
      }
      catch(Exception e)
      {
        // ignore
      }
    }

    return ( origData != null && !origData.equals(curData) );
  }

  public boolean isReadOnly()
  {
    return readOnly;
  }

  public void makeReadOnly()
  {
    this.readOnly = true;
  }

  public void unmakeReadOnly()
  {
    this.readOnly = false;
  }

  /*
  ** protected String processData(String rawData)
  **
  ** This method is called by setData prior to setting field data so that any
  ** special handling of the field data can occur. This method may be overidden
  ** by child classes to incorporate specialized processing of data.  This default
  ** version simply returns the data as is.
  **
  ** RETURNS: rawData unchanged.
  */
  protected String processData(String rawData)
  {
    return rawData;
  }

  protected void setName(String fname)
  {
    this.fname = fname;
  }

  /*
  ** public void setData(String fdata)
  **
  ** Used to set a field's data.  Child classes wishing to modify how data gets
  ** set may override processData().  If a null string is passed in field data
  ** is set to an empty string.
  */
  public void setData(String fdata)
  {
    // don't allow this field's value to be changed if it is immutable and has
    // already been set

    if( isImmutable() && getOriginalData() != null )
    {
      // don't do anything
    }
    else
    {
      // do any special pre-processing that may be required
      this.fdata = processData((fdata != null ? fdata : ""));

      // strip trailing white space
      if (this.fdata != null)
      {
        int i = this.fdata.length() - 1;
        for (; i >= 0 && Character.isWhitespace(this.fdata.charAt(i)); --i);
        this.fdata = this.fdata.substring(0,i + 1);
      }

      // if this is the first time the field's data has been set, store
      // it as the field's original data
      if (getOriginalData() == null)
      {
        setOriginalData(this.fdata);
      }

      // turn on the needs validation flag
      needsValidation = true;
    }
  }

  /*
  **
  */
  public void setDataTrim(String fdata)
  {
    if(this.length > 0)
    {
      if(fdata.length() > this.length)
      {
        setData(fdata.substring(0, this.length));
      }
      else
      {
        setData(fdata);
      }
    }
    else
    {
      setData(fdata);
    }
  }

  /*
  ** public void setOriginalData(String originalData)
  **
  ** Stores the data as the field's initial data set.
  */
  public void setOriginalData(String originalData)
  {
    this.originalData = originalData;
  }

  public void setShowName(String sname)
  {
    if(sname!=null)
    {
      this.sname=sname;
    }
  }

  public void setLabel(String label)
  {
    setShowName(label);
  }


  public void setMinLength( int minLength )
  {
    removeValidation("minlength");
    if ( minLength > 0 )
    {
      addValidation(new MinLengthValidation(minLength),"minlength");
    }
  }

  public void setHtmlSize(int s)
  {
    this.htmlSize=s;
  }

  /*
  ** public void addHtmlExtra(String newValue)
  **
  ** Appends newValue to the current value of htmlExtra.
  */
  public void addHtmlExtra(String newValue)
  {
    StringBuffer temp = new StringBuffer();
    if (this.htmlExtra != null && this.htmlExtra.length() > 0)
    {
      temp.append( this.htmlExtra );
      temp.append( " " );
    }
    temp.append(newValue);
    this.htmlExtra = temp.toString();
  }

  public void setHtmlExtra(String htmlExtra)
  {
    this.htmlExtra = htmlExtra;
  }

  public void setShowErrorText(boolean showErrorText)
  {
    this.showErrorText = showErrorText;
  }

  public void setTabIndex(int tabIndex)
  {
    this.tabIndex = tabIndex;
  }

  /*
  ** public void setFixImagePath(String newPath, int newWidth, int newHeight)
  **
  ** Sets the fix image path and dimensions.
  */
  public void setFixImage(String newPath, int newWidth, int newHeight)
  {
    fixImagePath    = newPath;
    fixImageWidth   = newWidth;
    fixImageHeight  = newHeight;
  }

  /*
  ** public void reset()
  **
  ** A reset method that can be overriden by child classes to reset internal
  ** data when appropriate (originally added to support resetting
  ** CheckboxField which require a reset due to the way in which check box
  ** data doesn't get posted if it's not checked).
  */
  public void reset() {}
  {
  }

  /*
  ** public void resetDefault()
  **
  ** Sets field back to default value (if specified)
  */
  public void resetDefault()
  {
    setData(defaultValue);
  }

  /*
  ** public void setDefaultValue (String val)
  **
  ** Establishes the default value for this field
  */
  public void setDefaultValue(String val)
  {
    defaultValue = val;
  }

  /*************************************************************************
  **
  ** Convenience methods
  **
  *************************************************************************/

  /*
  ** public boolean isBlank()
  **
  ** Determines if the field's data is blank or null.
  **
  ** RETURNS: true if blank or null, else false.
  */
  public boolean isBlank()
  {
    return (fdata == null || fdata.length() == 0);
  }

  /*
  ** asXXX conversion methods
  **
  ** These methods will simply parse the String data into requested numeric
  ** format.
  **
  ** Notes:
  **
  **    1) if getData() is blank then these methods will return 0
  **    2) if getData() is a non-numeric value then
  **       these methods will throw a NumberFormatException
  */
  public Date asDate( String format )
  {
    Date result = null;
    try
    {
      result = DateTimeFormatter.parseDate(getData(),format);
    }
    catch(Exception e)
    {
    }

    return result;
  }
  public double asDouble( )
  {
    double result = 0.0;
    try
    {
      result = Double.parseDouble(getData());
    }
    catch(NumberFormatException nfe)
    {
    }

    return result;
  }
  public double asFloat( )
  {
    float result = 0;

    try
    {
      result = Float.parseFloat(getData());
    }
    catch(NumberFormatException nfe)
    {
    }

    return result;
  }
  public int asInteger( )
  {
    int result = 0;

    try
    {
      result = Integer.parseInt(getData());
    }
    catch(NumberFormatException nfe)
    {
    }

    return result;
  }
  public int asInt()
  {
    return asInteger();
  }
  public long asLong( )
  {
    long result = 0L;

    try
    {
      result = Long.parseLong(getData());
    }
    catch(NumberFormatException nfe)
    {
    }

    return result;
  }
}
