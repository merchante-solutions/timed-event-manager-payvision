/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/FieldBean.java $

  Description:

  FieldBean

  Base class for jsp beans that contain form fields.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-02-01 11:15:45 -0800 (Wed, 01 Feb 2012) $
  Version            : $Revision: 19750 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.jdom.Document;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ImageButtonField.ParameterField;
import com.mes.support.CardTruncator;
import com.mes.support.DateTimeFormatter;
import com.mes.support.XSSFilter;
import com.mes.user.UserBean;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import sqlj.runtime.ref.DefaultContext;

public class FieldBean extends SQLJConnectionBase
{
  // Map TYpes for loading XML data into the field bean
  public static final int       MAP_TYPE_VERISIGN       = 1;
  public static final int       MAP_TYPE_VERISIGN_ISO   = 2;

  protected UserBean            user;
  protected FieldGroup          fields        = new FieldGroup("baseGroup");
  protected HtmlForm            form          = new HtmlForm();
  protected Vector              subBeans      = new Vector();
  protected FieldBean           parent        = null;
  protected Vector              errors        = null;
  protected HttpServletRequest  request       = null;
  protected boolean             useXSSFilter  = false;
  protected boolean             useCardTruncator = false;

  static Logger flog = Logger.getLogger(FieldBean.class);

  /*
  ** public FieldBean()
  **
  ** Constructor.
  */
  public FieldBean()
  {
  }

  public FieldBean(boolean autoCommit)
  {
    super(autoCommit);
  }

  public FieldBean(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      this.Ctx = defCtx;
    }
  }

  /*
  ** public FieldBean(UserBean user)
  **
  ** Constructor.
  */
  public FieldBean(UserBean user)
  {
    this.user = user;
  }


  /*************************************************************************
  **
  ** Condition classes
  **
  *************************************************************************/

  public class FieldValueCondition implements Condition
  {
    private Field field;
    private String equalsData;

    public FieldValueCondition(Field field, String equalsData)
    {
      this.field = field;
      this.equalsData = equalsData;
    }

    public boolean isMet()
    {
      return field.getData().equals(equalsData);
    }
  }

  public class OrCondition implements Condition
  {
    Vector conditions = new Vector();

    public void add(Condition condition)
    {
      conditions.add(condition);
    }

    public boolean isMet()
    {
      for (Iterator i = conditions.iterator(); i.hasNext(); )
      {
        Condition c = (Condition)i.next();
        if (c.isMet())
        {
          return true;
        }
      }
      return false;
    }
  }

  public class AndCondition implements Condition
  {
    Vector conditions = new Vector();

    public void add(Condition condition)
    {
      conditions.add(condition);
    }

    public boolean isMet()
    {
      for (Iterator i = conditions.iterator(); i.hasNext(); )
      {
        Condition c = (Condition)i.next();
        if (!c.isMet())
        {
          return false;
        }
      }
      return true;
    }
  }

  public class NotCondition implements Condition
  {
    Condition condition;

    public NotCondition(Condition condition)
    {
      this.condition = condition;
    }

    public boolean isMet()
    {
      return !condition.isMet();
    }
  }

  /*************************************************************************
  **
  ** Validation classes
  **
  *************************************************************************/

  public class ConditionalRequiredValidation implements Validation
  {
    private Condition condition;
    private String errorText = null;

    public ConditionalRequiredValidation(Condition condition)
    {
      this.condition = condition;
    }
    public ConditionalRequiredValidation(Condition condition, String errorText)
    {
      this.condition = condition;
      this.errorText = errorText;
    }

    public boolean validate(String fieldData)
    {
      if (condition != null && condition.isMet()
          && (fieldData == null || fieldData.length() == 0))
      {
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      if (errorText == null)
      {
        return "Field required";
      }

      return errorText;
    }
  }

  public class NotAllowedValidation implements Validation
  {
    private String[]  illegals = null;
    private String    errorText = "Invalid input";

    private void initializeIllegals(String[] illegals)
    {
      for (int i = 0; i < illegals.length; ++i)
      {
        illegals[i] = illegals[i].toLowerCase();
      }
      this.illegals = illegals;
    }

    public NotAllowedValidation(String[] illegals, String errorText)
    {
      initializeIllegals(illegals);
      this.errorText = errorText;
    }
    public NotAllowedValidation(String illegal, String errorText)
    {
      String illegals[] = new String[1];
      illegals[0] = illegal;
      initializeIllegals(illegals);
      this.errorText = errorText;
    }
    public NotAllowedValidation(String[] illegals)
    {
      initializeIllegals(illegals);
    }
    public NotAllowedValidation(String illegal)
    {
      String illegals[] = new String[1];
      illegals[0] = illegal;
      initializeIllegals(illegals);
    }

    public boolean validate(String fieldData)
    {
      for (int i = 0; i < illegals.length; ++i)
      {
        if (fieldData.toLowerCase().indexOf(illegals[i]) >= 0)
        {
          return false;
        }
      }
      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }

  public class RequiredIfNoValidation implements Validation
  {
    private String errorText;
    private Field yesField;

    public RequiredIfNoValidation(Field yesField, String errorText)
    {
      this.yesField = yesField;
      this.errorText = errorText;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fieldData)
    {
      if (yesField.getData().length() > 0 &&
          yesField.getData().toUpperCase().charAt(0) == 'N')
      {
        return fieldData != null && fieldData.length() > 0;
      }

      return true;
    }
  }

  public class RequiredIfYesValidation implements Validation
  {
    private String errorText;
    private Field yesField;

    public RequiredIfYesValidation(Field yesField, String errorText)
    {
      this.yesField = yesField;
      this.errorText = errorText;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fieldData)
    {
      if (yesField.getData().length() > 0 &&
          yesField.getData().toUpperCase().charAt(0) == 'Y')
      {
        return fieldData != null && fieldData.length() > 0;
      }

      return true;
    }
  }

  public class RequiredIfValueValidation implements Validation
  {
    private String  errorText;
    private Field   checkField;
    private String  value;

    public RequiredIfValueValidation(Field checkField, String value, String errorText)
    {
      this.checkField = checkField;
      this.value = value;
      this.errorText = errorText;
    }

    public String getErrorText()
    {
      return( errorText );
    }

    public boolean validate(String fieldData)
    {
      boolean retVal = true;

      // validation fails if checkField has the specified value and fdata is empty
      if( checkField.getData().length() > 0 &&
          checkField.getData().equals(value) )
      {
        if( fieldData == null || fieldData.length() == 0)
        {
          retVal = false;
        }
      }

      return( retVal );
    }
  }

  public class RequiredIfNotBlankValidation implements Validation
  {
    private String errorText = "Field Requred";
    private Field checkField;

    public RequiredIfNotBlankValidation(Field checkField)
    {
      this.checkField = checkField;
    }

    public String getErrorText()
    {
      return( errorText );
    }

    public boolean validate(String fdata)
    {
      boolean retVal = true;

      // valdation fails if checkField is not empty and fdata is empty
      if( checkField.getData().length() > 0 )
      {
        if( fdata == null || fdata.length() == 0)
        {
          retVal = false;
        }
      }

      return( retVal );
    }
  }

  public class OptionalIfValueValidation implements Validation
  {
    private String  errorText;
    private Field   checkField;
    private String  value;

    public OptionalIfValueValidation(Field checkField, String value, String errorText)
    {
      this.checkField = checkField;
      this.value = value;
      this.errorText = errorText;
    }

    public String getErrorText()
    {
      return( errorText );
    }

    public boolean validate(String fieldData)
    {
      // default to valid if field has data
      boolean retVal = (fieldData != null && fieldData.length() > 0);

      if( ! retVal &&
          checkField.getData().length() > 0 &&
          checkField.getData().equals(value) )
      {
        // ok for this field to be empty for the specified value
        retVal = true;
      }

      return( retVal );
    }
  }

  public class CheckboxRequiredValidation implements Validation
  {
    String  errorMessage = "Required";

    public CheckboxRequiredValidation()
    {
    }

    public String getErrorText()
    {
      return( errorMessage );
    }

    public boolean validate(String fieldData)
    {
      boolean isValid = false;

      isValid = (fieldData != null && fieldData.equals("y"));

      return( isValid );
    }
  }

  public class IfYesNotBlankValidation implements Validation
  {
    String          ErrorMessage              = "Field missing value";
    Field           YesNoField                = null;

    public IfYesNotBlankValidation( Field yesNoField, String msg )
    {
      YesNoField      = yesNoField;
      ErrorMessage    = msg;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fieldData)
    {
      boolean             retVal        = true;

      try
      {
        if ( YesNoField.getData().toUpperCase().equals("Y") )
        {
          if ( fieldData == null || fieldData.trim().equals("") )
          {
            retVal = false;
          }
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
      }
      return( retVal );
    }
  }

  public class IfNoBlankValidation implements Validation
  {
    String          ErrorMessage              = "Field missing value";
    Field           YesNoField                = null;

    public IfNoBlankValidation( Field yesNoField, String msg )
    {
      YesNoField      = yesNoField;
      ErrorMessage    = msg;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fieldData)
    {
      boolean             retVal        = true;

      try
      {
        if ( ! YesNoField.getData().toUpperCase().equals("Y") )
        {
          if ( fieldData != null && !fieldData.trim().equals("") )
          {
            retVal = false;
          }
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
      }
      return( retVal );
    }
  }

  public class FieldEqualsFieldValidation implements Validation
  {
    private Field otherField;
    private String otherName;

    public FieldEqualsFieldValidation(Field otherField, String otherName)
    {
      this.otherField = otherField;
      this.otherName = otherName;
    }

    public boolean validate(String fieldData)
    {
      return fieldData.equals(otherField.getData());
    }

    public String getErrorText()
    {
      return "Must match " + otherName;
    }
  }

  public class MaxValueValidation implements Validation
  {
    private double maxValue;

    public MaxValueValidation(double maxValue)
    {
      this.maxValue = maxValue;
    }

    public boolean validate(String fieldData)
    {
      boolean isValid = true;
      try
      {
        double fieldValue = Double.parseDouble(fieldData);
        isValid = fieldValue <= maxValue;
      }
      catch (Exception e) {}
      return isValid;
    }

    public String getErrorText()
    {
      return "May not exceed " + maxValue;
    }
  }

  public class AllOrNoneValidation implements Validation
  {
    private Vector fields = new Vector();
    private String errorText;

    public AllOrNoneValidation(String errorText)
    {
      this.errorText = errorText;
    }
    public AllOrNoneValidation(Vector fields, String errorText)
    {
      this.fields = fields;
      this.errorText = errorText;
    }

    public void addField(Field field)
    {
      fields.add(field);
    }

    public boolean validate(String fieldData)
    {
      boolean fieldIsEmpty = (fieldData == null || fieldData.length() == 0);
      boolean allAreEmpty = true;
      for (Iterator i = fields.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        if (field.getData().length() > 0)
        {
          allAreEmpty = false;
          break;
        }
      }

      if (fieldIsEmpty && !allAreEmpty)
      {
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }

  public class OnlyOneValidation implements Validation
  {
    protected String          ErrorText       = null;
    protected Vector          FieldList       = new Vector();

    public OnlyOneValidation(String errorText)
    {
      ErrorText = errorText;
    }
    public OnlyOneValidation(String errorText, Vector fields)
    {
      ErrorText  = errorText;
      FieldList  = fields;
    }

    public void addField(Field field)
    {
      FieldList.add(field);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      int         hasDataCount          = 0;
      boolean     retVal                = false;
      Field       temp                  = null;

      for (int i = 0; i < FieldList.size(); ++i )
      {
        temp = (Field)FieldList.elementAt(i);
        if (!temp.isBlank())
        {
          ++hasDataCount;
        }
      }

      // allow none or only one field
      // to have data, but not more
      // than one.
      if ( hasDataCount < 2 )
      {
        retVal = true;
      }
      return( retVal );
    }

    public String getErrorText()
    {
      return(ErrorText);
    }
  }

  public class AtLeastOneValidation implements Validation
  {
    protected String          ErrorText       = null;
    protected Vector          FieldList       = new Vector();

    public AtLeastOneValidation(String errorText)
    {
      ErrorText = errorText;
    }
    public AtLeastOneValidation(String errorText, Vector fields)
    {
      ErrorText  = errorText;
      FieldList  = fields;
    }
    public AtLeastOneValidation(String errorText, FieldGroup fg)
    {
      ErrorText  = errorText;

      Iterator i = fg.iterator();
      while(i.hasNext())
        FieldList.add((Field)i.next());
    }

    public void addField(Field field)
    {
      FieldList.add(field);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      // require at least one

      for (int i = 0; i < FieldList.size(); ++i ) {
        if(!((Field)FieldList.elementAt(i)).isBlank())
          return true;
      }

      return false;
    }

    public String getErrorText()
    {
      return(ErrorText);
    }
  }

  public class YearValidation implements Validation
  {
    private String  ErrorText         = "Invalid year";
    private boolean NullAllowed       = false;

    public YearValidation( )
    {
    }

    public YearValidation(String errorText)
    {
      // overload the default error text
      ErrorText = errorText;
    }

    public YearValidation(String errorText, boolean nullAllowed)
    {
      // overload the default error text
      ErrorText   = errorText;
      NullAllowed = nullAllowed;
    }

    public boolean validate(String fieldData)
    {
      boolean     retVal        = false;
      int         yearValue     = 0;

      try
      {
        if ( (fieldData == null) || fieldData.equals("") )
        {
          retVal = NullAllowed;
        }
        else
        {
          yearValue = Integer.parseInt(fieldData);

          if ( yearValue > 1900 && yearValue <= Calendar.getInstance().get(Calendar.YEAR) )
          {
            retVal = true;
          }
        }
      }
      catch( Exception e )
      {
        // ignore, invalid year specified
      }
      return( retVal );
    }

    public String getErrorText()
    {
      return( ErrorText );
    }
  }

  public class PercentValidation implements Validation
  {
    private String  ErrorText         = null;
    private int     MaxValue          = 0;
    private int     MinValue          = 0;
    private boolean NullAllowed       = false;

    public PercentValidation( )
    {
      init( "Invalid percent value", 0, 100, false );
    }

    public PercentValidation( boolean nullAllowed )
    {
      init( "Invalid percent value", 0, 100, nullAllowed );
    }

    public PercentValidation(String errorText)
    {
      init( errorText, 0, 100, false );
    }

    public PercentValidation(String errorText, boolean nullAllowed)
    {
      init( errorText, 0, 100, nullAllowed );
    }

    public PercentValidation(String errorText, int minVal, int maxVal )
    {
      init( errorText, minVal, maxVal, false );
    }

    public PercentValidation(String errorText, int minVal, int maxVal, boolean nullAllowed )
    {
      init( errorText, minVal, maxVal, nullAllowed );
    }

    protected void init( String errorText, int minVal, int maxVal, boolean nullAllowed )
    {
      ErrorText   = errorText;
      MaxValue    = maxVal;
      MinValue    = minVal;
      NullAllowed = nullAllowed;
    }

    public boolean validate(String fieldData)
    {
      boolean     retVal        = false;
      int         val           = 0;

      try
      {
        if ( (fieldData == null) || fieldData.equals("") )
        {
          retVal = NullAllowed;
        }
        else
        {
          val = Integer.parseInt(fieldData);

          if ( val >= MinValue && val <= MaxValue )
          {
            retVal = true;
          }
        }
      }
      catch( Exception e )
      {
        // ignore, invalid number specified
      }
      return( retVal );
    }

    public String getErrorText()
    {
      return( ErrorText );
    }
  }

  public class NumericDataOnlyValidation implements Validation
  {
    private String errorText      = null;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      boolean result = true;

      if(fdata != null)
      {
        for(int i=0; i < fdata.length(); ++i)
        {
          if(!Character.isDigit(fdata.charAt(i)))
          {
            errorText = "Data must be numeric";
            result = false;
            break;
          }
        }
      }

      return result;
    }
  }

  /*************************************************************************
  **
  ** Field Manipulation
  **
  *************************************************************************/

  public FieldGroup getFields()
  {
    return fields;
  }

  /*
  ** protected void createFields(HttpServletRequest request)
  **
  ** Called by setFields(), convenient place to put field creation and
  ** initialization.
  */
  protected void createFields(HttpServletRequest request)
  {
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      FieldBean sb = (FieldBean)i.next();
      if (!sb.fieldsCreated)
      {
        sb.createFields(request);
      }
    }
    fieldsCreated = true;
  }

  /*
  ** private void resolveUserObject()
  **
  ** If the user is null an attempt is made to fetch it from the http session.
  */
  private void resolveUserObject(HttpServletRequest request)
  {
    if (user == null)
    {
      if(request == null)
      {
        user = new UserBean();
        user.forceValidate("system");
      }
      else
      {
        HttpSession session = request.getSession();
        if (session != null)
        {
          setUser((UserBean)session.getAttribute("UserLogin"));
        }
      }
    }
  }

  public void setUseCardTruncator(boolean useCardTruncator)
  {
    this.useCardTruncator = useCardTruncator;
  }

  public void setUseXSSFilter(boolean useXSSFilter)
  {
    this.useXSSFilter = useXSSFilter;
  }

  private void setField(HttpServletRequest request, String name, String value)
  {
    //flog.debug("setField:: name: "+name+" -- value: "+ value);
    // look for special cboxMarker parameter
    if(name.endsWith("_cboxMarker"))
    {
      try
      {
        // look for another parameter in the request containing this (stripped)
        // field name
        String strippedName = name.substring(0, name.length()-11);

        if(request.getParameter(strippedName) == null)
        {
          // set the strippedName field to "n"
          fields.getField(strippedName).setData("n");
        }
      }
      catch(Exception strippedException)
      {
      }
    }
    Field field = fields.getField(name);

    if (field != null)
    {
      //flog.debug("found field = "+ name);
      if( useCardTruncator )
      {
        value = CardTruncator.truncate(value);
      }

      if( useXSSFilter )
      {
        value = XSSFilter.encodeXSS(value);
      }

      field.setData(value);
    }
  }

  private boolean allowMultipart = false;

  protected void enableMultipart()
  {
    allowMultipart = true;
  }
  protected void disableMultipart()
  {
    allowMultipart = false;
  }

  public String toString()
  {
    return( fields.toString() );
  }

  private boolean isMultipartRequest(HttpServletRequest request)
  {
    if(request == null) return false;

    String contentType = request.getContentType();
    return allowMultipart && contentType != null
      && contentType.toLowerCase().startsWith("multipart/form-data");
  }

  // limits uploads to 100MB...increased to 100 10/28/2011 to support large aus files
  public static final int MAX_FILE_UPLOAD_SIZE = 100 * 1024 * 1024;

  private List fileParts = new ArrayList();

  public List getFileParts()
  {
    return fileParts;
  }

  // this needs to be overridden or the file data will be discarded
  protected void receiveFile(FilePart part)
  {
    flog.warn("File part " + part.getName() + " discarded,"
      + " bean does not handle file parts");
  }

  private void setFieldsWithMultipartRequest(HttpServletRequest request)
  {
    try
    {
      MultipartParser parser = new MultipartParser(request,MAX_FILE_UPLOAD_SIZE,true,false);
      Part part = null;
      while ((part = parser.readNextPart()) != null)
      {
        String name = part.getName();
        if (part.isParam())
        {
          String value = ((ParamPart)part).getStringValue();
          setField(request,name,value);
        }
        else if (part.isFile())
        {
          FilePart filePart = (FilePart)part;
          if (filePart.getFileName() != null)
          {
            flog.debug("Receiving file " + filePart.getFileName());
            receiveFile(filePart);
            fileParts.add(filePart);
          }
          else
          {
            flog.debug("File part " + name + " is empty");
          }
        }
        else
        {
          flog.warn("Unhandled multi part: " + name);
        }
      }
    }
    catch (Exception e)
    {
      flog.error("Error setting fields with multipart request: " + e);
      e.printStackTrace();
    }
  }


  protected void setFieldsWithStandardRequest(HttpServletRequest request)
  {
    if(request != null)
    {
      // apply request parameters to the fields
      for (Enumeration names = request.getParameterNames();
           names.hasMoreElements(); )
      {
        String name = (String)names.nextElement();
        String value = request.getParameter(name);
        //flog.debug("name  = "+ name);
        //flog.debug("value = "+ value);
        try
        {
          value = URLDecoder.decode(value, "UTF-8");
        }
        catch(Exception e)
        {
          logEntry("setFieldsWithStandardRequest()", e.toString());
        }
        setField(request,name,value);
      }
    }

  }

  private boolean fieldsCreated = false;

  /*
  ** public void setFields(ServletRequest request)
  **
  ** Takes a servlet request and scans throught it's parameters, loading
  ** any parameters whose names match field names within this bean's field
  ** group.  This allows automation similar to the use-bean/set-property tags,
  ** but is better hopefully due to the fact that individual accessors
  ** for each field are no longer necessary.
  */
  public void setFields(HttpServletRequest request)
  {
    //flog.debug("in setFields(request)...");

    this.request = request;

    resolveUserObject(request);

    // create fields
    if (!fieldsCreated)
    {
      //flog.debug("creating fields(request)...");
      createFields(request);
      fieldsCreated = true;
    }

    // do any custom request handling that needs doing on self
    // and any sub beans that might be present prior to setting
    // field data
    preHandleRequest(request);
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).preHandleRequest(request);
    }

    // reset fields
    fields.reset();

    if (isMultipartRequest(request))
    {
      setFieldsWithMultipartRequest(request);
    }
    else
    {
      setFieldsWithStandardRequest(request);
    }

    // do any custom request handling that needs doing on self
    // and any sub beans that might be present after setting
    // field data
    postHandleRequest(request);
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).postHandleRequest(request);
    }
  }

  /*
  ** public void setFields(Document doc)
  **
  ** Analagous to setFields(ServletRequest), except this version accepts
  ** an XML Document and uses the database table XML_FIELD_MAPPING to attempt
  ** to assign fields in this field bean's field group.
  */
  public void setFields(Document doc, int mapType, DefaultContext Ctx, long virtualSeqNum, HttpServletRequest request)
  {
    // create fields
    if(!fieldsCreated)
    {
      createFields(request);
      fieldsCreated = true;
    }

    preHandleRequest(request);
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).preHandleRequest(request);
    }

    // reset fields
    fields.reset();

    // retrieve field mappings using mapType=>XML_FIELD_MAPPING.MAP_TYPE
    if(XMLFieldMapper.mapToFields(mapType, fields, doc, virtualSeqNum, Ctx))
    {
    }
    else
    {
    }

    // do any custom request handling that needs doing on self
    // and any sub beans that might be present after setting
    // field data
    postHandleRequest(request);

    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).postHandleRequest(request);
    }
  }

  public void setFields(Map fMap, DefaultContext Ctx, HttpServletRequest request)
  {
    Field field = null;

    // create fields
    if( ! fieldsCreated )
    {
      createFields(request);
      fieldsCreated = true;
    }

    preHandleRequest(request);
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).preHandleRequest(request);
    }

    // reset fields
    fields.reset();

    // find fields matching names in map keys
    Iterator iter = fMap.entrySet().iterator();

    while(iter.hasNext())
    {
      Map.Entry entry = (Map.Entry)iter.next();

      field = fields.getField((String)entry.getKey(), true, true); // ignore case of field name

      if( field != null )
      {
        // set field data
        field.setData((String)entry.getValue());
      }
      else
      {
        flog.error("** could not find field named: " + entry.getKey());
      }
    }

    // do any custom request handling that needs doing on self
    // and any sub beans that might be present after setting
    // field data
    postHandleRequest(request);

    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      ((FieldBean)i.next()).postHandleRequest(request);
    }
  }

  /*
  ** protected void preHandlRequest(HttpServletRequest request)
  **
  ** The following methods are called from setFields.  The pre method
  ** is called before field data is set and the post method is called
  ** after.  Child classes should override these methods to do any
  ** special handling that is needed based on information in the request
  ** (pre) or after the field data has been set (post).  In particular
  ** these methods are useful for field beans that are added as sub
  ** beans to another field bean.  Use of these methods supersedes the
  ** practice of overloading the setFields method itself, although
  ** setFields remains non-final for backward compatability.
  */
  protected void preHandleRequest(HttpServletRequest request)
  {
  }
  protected void postHandleRequest(HttpServletRequest request)
  {
  }

  // static method so that non-FieldBean objects can still use
  // the code to set a FieldGroup using a result set (see reports package)
  public static void setFields(FieldGroup fields, ResultSet resultSet, boolean scanResultSet,boolean removeUnderscores)
    throws java.sql.SQLException
  {
    Field                     field       = null;
    StringBuffer              fieldName   = new StringBuffer();
    boolean                   hasNext     = true;
    int                       index       = 0;
    ResultSetMetaData         metaData    = resultSet.getMetaData();

//    flog.debug("entering setFields(FieldGroup, ResultSet, boolean)");
    // scanning the result set, so advance to
    // the first record (if exists)
    if ( scanResultSet == true )
    {
//      flog.debug("getting resultset.next()");
      hasNext = resultSet.next();
    }

    while( hasNext )
    {
      for (int col = 1; col <= metaData.getColumnCount(); col++)
      {
        fieldName.setLength(0);
        fieldName.append( metaData.getColumnName(col) );

        // if necessary convert one_two_three style SQL column names
        // into oneTwoThree style field names.
        if ( removeUnderscores )
        {
          while( (index = fieldName.toString().indexOf("_")) >= 0 )
          {
            fieldName.deleteCharAt(index);
          }
        }
        field = fields.getField(fieldName.toString(),true);

        if ( field != null )
        {
          if ( field instanceof DateField )
          {
//            flog.debug("    setting DateField");
            ((DateField)field).setUtilDate(resultSet.getTimestamp(col));
          }
          else if (field instanceof DateStringField)
          {
//            flog.debug("    setting DateStringField");
            ((DateStringField)field).setUtilDate(resultSet.getDate(col));
          }
          else
          {
//            flog.debug("    setting normal field");
            Object obj = resultSet.getObject(col);
            if(obj != null)
            {
              field.setData(obj.toString());
            }
            //field.setData(resultSet.getString(col));
          }
        }
        else
        {
          //flog.debug("Field not found in FieldGroup: " + fieldName.toString() + " (" + metaData.getColumnName(col) + ")");
        }
      }

      // if the result set is to be scanned, then advance
      // to the next record.  otherwise, we are just to
      // process the current record and exit the loop without
      // changing the current row in the ResultSet.
      if ( scanResultSet == true )
      {
        hasNext = resultSet.next();
      }
      else
      {
        hasNext = false;
      }
    }
  }

  public static void setFields(FieldGroup fields, ResultSet resultSet, boolean scanResultSet )
    throws java.sql.SQLException
  {
    setFields(fields,resultSet,scanResultSet,true);
  }

  public String getFieldNames()
  {
    StringBuffer result = new StringBuffer("");

    try
    {
      if (!fieldsCreated)
      {
        createFields(null);
        fieldsCreated = true;
      }

      Vector allFields = fields.getFieldsVector();

      for(int i=0; i<allFields.size(); ++i)
      {
        result.append(((Field)(allFields.elementAt(i))).getName());
        result.append("\n");
      }
    }
    catch(Exception e)
    {
      logEntry("getFieldNames()", e.toString());
    }

    return result.toString();
  }

  public void showFieldData()
  {
    try
    {
      Vector allFields = fields.getFieldsVector();

      for(int i=0; i<allFields.size(); ++i)
      {
        Field f = (Field)(allFields.elementAt(i));
      }
    }
    catch(Exception e)
    {
      logEntry("showFieldData()", e.toString());
    }
  }

  // local version uses the default field set (fields)
  public void setFields( ResultSet resultSet, boolean scanResultSet,
                         boolean removeUnderscores)
    throws java.sql.SQLException
  {
    setFields( fields, resultSet, scanResultSet, removeUnderscores );
  }

  // local version uses the default field set (fields)
  public void setFields( ResultSet resultSet, boolean scanResultSet)
    throws java.sql.SQLException
  {
    setFields( fields, resultSet, scanResultSet, true );
  }

  // default call automatically scans the entire result set into
  // the default field set (fields)
  public void setFields( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setFields( fields, resultSet, true, true );
  }

  // rendering of errors in HTML is application specific and
  // therefore the base class contains a placeholder only
  public String renderErrors( )
  {
    return(null);
  }

  public String renderHiddenFields( )
  {
    Vector        allFields = fields.getFieldsVector();
    Field         f         = null;
    StringBuffer  retVal    = new StringBuffer();

    for( int i = 0; i < allFields.size(); ++i )
    {
      f = (Field)allFields.elementAt(i);
      if ( f instanceof HiddenField )
      {
        retVal.append("\n");
        retVal.append(f.renderHtml());
      }
    }
    return( retVal.toString() );
  }

  /*
  ** public String renderHtml()
  **
  ** Calls the form's render method.
  **
  ** RETURNS: rendered fields in form sections as defined by the field groups.
  */
  public String renderHtml()
  {
    return form.renderHtml();
  }

  /*
  ** public String renderHtml(String fname)
  **
  ** Looks for the field with the corresponding field name in the field group
  ** and has it render itself as an html form field.
  **
  ** RETURNS: the rendered html in a String.
  */
  public String renderHtml(String fname)
  {
    return( fields.renderHtml(fname) );
  }

  public String renderHtml(String fname, int radioIndex)
  {
    return( fields.renderHtml(fname,radioIndex) );
  }

  public String renderHtml(String fname, int radioIndex, boolean showJavaScript)
  {
    return( fields.renderHtml(fname,radioIndex,showJavaScript,true) );
  }

  public String renderHtml(String fname, int radioIndex, boolean showJavaScript, boolean showLabel)
  {
    return( fields.renderHtml(fname,radioIndex,showJavaScript,showLabel) );
  }

  /*
  ** public Field getField(String fname)
  **
  ** Searches for a field object within the field group with the name given.
  **
  ** RETURNS: Field reference if found, else null.
  */
  public Field getField(String fname)
  {
    return fields.getField(fname);
  }


  /*
  ** public Field fieldHasError(String fname)
  **
  ** Searches for a field object within the field group with the name given.
  ** Then checks to see that all validations pass,
  ** returns true if atleast one validation fails
  **
  ** RETURNS: True or False
  */
  public boolean fieldHasError(String fname)
  {
    return !fields.getField(fname).isValid();
  }

  /*
  ** public Field renderFieldErrorIndicator(String fname)
  **
  ** Searches for a field object within the field group with the name given.
  ** Then checks to see that all validations pass,
  ** if atleast 1 validation fails will render the html error flag
  **
  ** RETURNS: True or False
  */
  public String renderFieldErrorIndicator(String fname)
  {
    if(fieldHasError(fname))
    {
      return fields.getField(fname).getFixImage().renderHtml();
    }
    else
    {
      return "";
    }
  }

  /*
  ** public String getData(String fname)
  **
  ** Searches for a field in the field group matching fname and returns
  ** field's data if found.
  **
  ** RETURNS: String containing field's data, or empty String if not found.
  */
  public String getData(String fname)
  {
    Field field = fields.getField(fname);
    if (field != null)
    {
      return field.getData();
    }
    return "";
  }

  public String getOriginalData(String fname)
  {
    Field field = fields.getField(fname);
    if (field != null)
    {
      return field.getOriginalData();
    }
    return "";
  }

  public int getDataLength(String fname)
  {
    int   retVal    = 0;

    Field field = fields.getField(fname);
    if (field != null)
    {
      retVal = field.getDataLength();
    }
    return( retVal );
  }

  //mimics getData allowing for complex fields to override
  public String getDescription(String fname)
  {
    Field field = fields.getField(fname);
    if (field != null)
    {
      return field.getDescription();
    }
    return "";
  }

  public String getLabel(String fname)
  {
    return fields.getLabel(fname);
  }

  public String listChanges()
  {
    return( fields.listChanges() );
  }
  public boolean hasChanged()
  {
    return( fields.hasChanged() );
  }
  public boolean hasChanged( String fieldName )
  {
    return( fields.hasChanged( fieldName ) );
  }

  /*
  ** public boolean hasData(String fname)
  **
  ** Returns true if the field matching fname is not empty
  */
  public boolean hasData(String fname)
  {
    return (!getData(fname).equals(""));
  }

  /*
  ** Convenience conversion getters.
  */
  public boolean getBoolean(String fname)
  {
    return( getBoolean(fname,false) );
  }

  public boolean getBoolean(String fname, boolean defaultValue)
  {
    boolean     retVal = defaultValue;

    try
    {
      // returns true iff getData(fname) returns "true" (case ignored)
      retVal = (Boolean.valueOf(getData(fname))).booleanValue();
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public int getInt(String fname)
  {
    return( getInt(fname,0) );
  }

  public int getInt(String fname, int defaultValue)
  {
    int     retVal = defaultValue;

    try
    {
      retVal = Integer.parseInt(getData(fname));
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public long getLong(String fname)
  {
    return( getLong(fname,0L) );
  }

  public long getLong(String fname,long defaultValue)
  {
    long    retVal    = defaultValue;

    try
    {
      retVal = Long.parseLong(getData(fname));
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public float getFloat(String fname)
  {
    return( getFloat(fname,0) );
  }

  public float getFloat(String fname, float defaultValue)
  {
    float   retVal    = defaultValue;
    try
    {
      retVal = Float.parseFloat(getData(fname));
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public double getDouble(String fname)
  {
    return( getDouble(fname,0.0) );
  }

  public double getDouble(String fname, double defaultValue)
  {
    double    retVal    = defaultValue;

    try
    {
      retVal = getField(fname).asDouble();
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public Date getDate(String fname)
  {
    return( getDate(fname,"MM/dd/yyyy",null) );
  }

  public Date getDate(String fname, String format)
  {
    return( getDate(fname,format,null) );
  }

  public Date getDate(String fname, String format, Date defaultValue)
  {
    Date    retVal    = defaultValue;

    try
    {
      retVal = getField(fname).asDate(format);
    }
    catch (Exception e)
    {
    }
    return( retVal );
  }

  public java.sql.Date getSqlDate(String fname)
  {
    return( getSqlDate(fname,"MM/dd/yyyy",null) );
  }

  public java.sql.Date getSqlDate(String fname, String format)
  {
    return( getSqlDate(fname,format,null) );
  }

  public java.sql.Date getSqlDate(String fname, String format, Date defaultValue)
  {
    java.util.Date javaDate = getDate(fname,format,defaultValue);
    return( (javaDate == null ? null : new java.sql.Date(javaDate.getTime())) );
  }

  public java.sql.Timestamp getSqlTimestamp(String fname)
  {
    return( getSqlTimestamp(fname,"MM/dd/yyyy HH:mm:ss",null) );
  }

  public java.sql.Timestamp getSqlTimestamp(String fname, String format)
  {
    return( getSqlTimestamp(fname,format,null) );
  }

  public java.sql.Timestamp getSqlTimestamp(String fname, String format, Date defaultValue)
  {
    java.util.Date javaDate = getDate(fname,format,defaultValue);
    return( (javaDate == null ? null : new java.sql.Timestamp(javaDate.getTime())) );
  }

  public String getPaddedString(String fname,int length)
  {
    String retVal = getString(fname,"");
    while( retVal.length() < length ) { retVal += " "; }
    return( retVal );
  }

  public String getString(String fname)
  {
    return( getString(fname,null,false) );
  }

  public String getString(String fname,String defaultValue)
  {
    return( getString(fname,defaultValue,false) );
  }

  public String getString(String fname,String defaultValue,boolean truncate)
  {
    Field   f       = getField(fname);
    String  retVal  = defaultValue;

    if ( f != null )
    {
      retVal = f.getData();

      if ( retVal == null || retVal.equals("") )
      {
        retVal = defaultValue;
      }

      // hidden fields have no defined length
      if ( !(f instanceof HiddenField) && truncate && retVal != null && retVal.length() > f.getLength() )
      {
        retVal = retVal.substring(0,f.getLength()); // truncate
      }
    }
    return( retVal );
  }

  public byte getByte(String fname)
  {
    return( getByte(fname,(byte)0x00) );
  }

  public byte getByte(String fname,byte defaultValue)
  {
    byte    retVal  = defaultValue;
    String  temp    = getData(fname);

    if ( temp != null && temp.length() > 0 )
    {
      retVal = (temp.getBytes())[0];
    }
    return( retVal );
  }

  public char getChar(String fname)
  {
    return( getChar(fname,(char)0x00) );
  }

  public char getChar(String fname,char defaultValue)
  {
    char    retVal  = defaultValue;
    String  temp    = getData(fname);

    if ( temp != null && temp.length() > 0 )
    {
      retVal = temp.charAt(0);
    }
    return( retVal );
  }

  /*
  ** public void setData(String fname, String data)
  **
  ** Searches for a field in the field group matching fname and sets
  ** it's data.
  */
  public void setData(String fname, String data)
  {
    Field field = fields.getField(fname);
    if (field != null)
    {
      field.setData(data);
    }
  }

  public void setData(String fname, boolean data) { setData(fname,String.valueOf(data)); }
  public void setData(String fname, byte data) { setData(fname,String.valueOf(data)); }
  public void setData(String fname, int data) { setData(fname,String.valueOf(data)); }
  public void setData(String fname, long data) { setData(fname,String.valueOf(data)); }
  public void setData(String fname, float data) { setData(fname,String.valueOf(data)); }
  public void setData(String fname, double data) { setData(fname,String.valueOf(data)); }

  public void setData(String fname, Date data) { setData(fname,data,"MM/dd/yyyy"); }
  public void setData(String fname, Date data, String fmt)
  {
    setData(fname,DateTimeFormatter.getFormattedDate(data,fmt));
  }

  /*
  ** public long getUserId()
  **
  ** RETURNS: long user id associated with the bean.
  */
  public long getUserId()
  {
    return user.getUserId();
  }

  /*
  ** public String getUserLoginName()
  **
  ** RETURNS: long user id associated with the bean.
  */
  public String getUserLoginName()
  {
    return user.getLoginName();
  }

  /*
  ** public UserBean getUser()
  **
  ** RETURNS: UserBean reference to user associated with bean.
  */
  public UserBean getUser()
  {
    return user;
  }

  public void setUser(UserBean v)
  {
    if(v!=null)
      user=v;
  }

  /*
  ** public long getAppSeqNum()
  **
  ** Extracts an app seq num field value if it exists, converts it to a long.
  **
  ** RETURNS: long containing the current app seq num, -1L if not found.
  */
  public long getAppSeqNum()
  {
    long appSeqNum;
    try
    {
      appSeqNum  = Long.parseLong(fields.getData("appSeqNum"));
    }
    catch (Exception e)
    {
      appSeqNum = -1L;
    }
    return appSeqNum;
  }

  /*
  ** public FieldGroup getBaseGroup()
  **
  ** RETURNS: FieldGroup reference to the base field group.
  */
  public FieldGroup getBaseGroup()
  {
    return fields;
  }

  /*
  ** public Iterator getGroupIterator(String groupName)
  **
  ** RETURNS: an Iterator over the fields in the indicated group field, or
  **          NULL if the field does not exist or the field is not of type
  **          FieldGroup.
  */
  public Iterator getGroupIterator(String groupName)
  {
    Iterator i = null;

    try
    {
      i = ((FieldGroup)fields.getField(groupName)).iterator();
    }
    catch (Exception e)
    {
    }

    return i;
  }

  /*
  ** public FieldBean add(FieldBean subBeab) throws Exception
  **
  ** Adds a field bean under this field bean.  The sub bean's fields are
  ** linked to a FieldGroup and added into this beans baseGroup field group.
  **
  ** An exception is thrown if an attempt is made to add a bean to itself.
  **
  ** RETURNS: reference to the FieldBean added in.
  */
  public FieldBean add(FieldBean subBean)
  {
    if (subBean == this)
    {
      throw new RuntimeException("Attempt to add field bean to self not allowed.");
    }

    // create a unique name for the new sub group
    String subGroupName = subBean.getClass().getName();
    StringTokenizer tok = new StringTokenizer(subGroupName,".");
    while (tok.hasMoreTokens())
    {
      subGroupName = tok.nextToken();
    }
    int subIdx = 1;
    while (fields.getField(subGroupName + "_" + subIdx) != null)
    {
      ++subIdx;
    }
    FieldGroup subGroup = new FieldGroup(subGroupName + "_" + subIdx);

    // add sub bean's fields into this beans baseGroup within the new sub group
    for (Iterator i = subBean.fields.iterator(); i.hasNext();)
    {
      subGroup.add((Field)i.next());
    }
    fields.add(subGroup);

    // store a ref to the sub bean
    subBeans.add(subBean);

    // repoint subBean's fields reference to shared fields
    subBean.fields = fields;

    // repoint subBean's user reference to shared user
    subBean.user = user;

    // now tell the subBean that he has a parent
    subBean.parent = this;

    // return a reference to the subBean for convenience
    return subBean;
  }

  public void add(Field newField)
  {
    fields.add(newField);
  }

  public void addHtmlExtra( String htmlExtra )
  {
    Vector        allFields = fields.getFieldsVector();
    Field         f         = null;

    for( int i = 0; i < allFields.size(); ++i )
    {
      f = (Field)allFields.elementAt(i);
      f.addHtmlExtra(htmlExtra);
    }
  }

  /**************************************************************************
  **
  ** Validation
  **
  **************************************************************************/

  /*
  ** public Vector getErrors()
  **
  ** RETURNS: the errors Vector.
  */
  public Vector getErrors()
  {
    if (errors == null)
    {
      errors = new Vector();
    }
    return errors;
  }

  /*
  ** public void clearErrors()
  **
  ** Clears any error items from the errors vector (if they are present).
  */
  public void clearErrors()
  {
    getErrors().setSize(0);
  }

  /*
  ** public boolean hasErrors()
  **
  ** RETURNS: true if the errors vector is not empty.
  */
  public boolean hasErrors()
  {
    return getErrors().size() > 0;
  }

  /*
  ** public void addError(String errorText)
  **
  ** Provides a means for externally introducing errors.
  */
  public void addError(String errorText)
  {
    getErrors().add(errorText);
  }

  /*
  ** protected void validateFields()
  **
  ** Subclasses may implement this to validate field data external to
  ** the fields own validation.
  */
  protected void validateFields()
  {
  }

  public boolean isFieldBlank( String fname )
  {
    return( isBlank(getData(fname)) );
  }

  public static boolean isBlank( String strVal )
  {
    boolean     retVal      = false;

    if ( strVal == null || strVal.equals("") )
    {
      retVal = true;
    }

    return( retVal );
  }

  public boolean fieldExists( String fname )
  {
    return( getField(fname) != null );
  }

  /*
  ** public boolean isValid()
  **
  ** Validates every field within the fields field field group.  Any errors
  ** are collected into the errors vector.  Calling this method causes any
  ** existing error items to be cleared from the errors vector.  Also calls
  ** validateFields() which may cause additional error items to be generated.
  **
  ** RETURNS: true if no errors were created during validation, else false.
  */
  public boolean isValid()
  {
    // init errors vector
    clearErrors();

    // use field validator to validate fields, collect any field errors
    FieldValidator validator = new FieldValidator();
    if (!validator.validateFields(fields))
    {
      System.out.println("error: " + validator.getFieldErrors());
      getErrors().addAll(validator.getFieldErrors());
    }

    // do any non-specific validation of fields
    validateFields();

    // return whether or not errors were detected
    return !hasErrors();
  }

  /*
  ** public void resetErrorState()
  **
  ** Clears the error vector and resets error state in fields.
  */
  public void resetErrorState()
  {
    clearErrors();
    fields.resetErrorState();
  }


  /**************************************************************************
  **
  ** Auto Setting
  **
  **************************************************************************/

  protected boolean _isAutoAction   = false;
  protected boolean _isAutoActOk    = false;
  protected boolean _isAutoSubmit   = false;
  protected boolean _isAutoValid    = false;
  protected boolean _isAutoSubmitOk = false;
  protected boolean _isAutoLoadOk   = false;
  protected boolean _isReset        = false;
  protected String  autoSubmitName  = null;
  protected String  autoActionName  = null;

  public boolean  isAutoAction()      { return _isAutoAction;   }
  public boolean  isAutoActOk()       { return _isAutoActOk;    }
  public boolean  isAutoSubmit()      { return _isAutoSubmit;   }
  public boolean  isAutoValid()       { return _isAutoValid;    }
  public boolean  isAutoSubmitOk()    { return _isAutoSubmitOk; }
  public boolean  isAutoLoadOk()      { return _isAutoLoadOk;   }
  public boolean  isReset()           { return _isReset;   }
  public String   getAutoSubmitName() { return autoSubmitName;  }
  public String   getAutoActionName() { return autoActionName;  }

  /*
  ** protected void autoSetAction()
  **
  ** Determines whether a button was pressed and if so if it was an action button
  */
  protected void autoSetAction()
  {
    // determine if a button was pushed, either an action
    // button or a normal submit button
    _isAutoSubmit = false;
    _isAutoAction = false;
    _isAutoSubmitOk = false;
    _isAutoActOk = false;
    _isAutoValid = false;
    _isAutoLoadOk = false;
    _isReset      = false;
    for (Iterator i = fields.getFieldsVector().iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof MapperField)
      {
        f = ((MapperField)f).getMapField();
      }
      if (f.getData().length() > 0)
      {
        if (f.getName().startsWith("action"))
        {
          _isAutoAction = true;
          autoActionName = f.getName();
          break;
        }
        else if (f instanceof ButtonField)
        {
          _isAutoSubmit = true;
          autoSubmitName = f.getName();
          break;
        }
        else if (f instanceof ButtonReset)
        {
          _isReset = true;
          break;
        }
        else if (f instanceof ParameterField && f.getName().endsWith(".x")) // only trigger off of one of the image button fields
        {
          _isAutoSubmit = true;
          autoSubmitName = f.getName();
          break;
        }
        else if (f instanceof HiddenField && f.getName().endsWith("submit"))
        {
          _isAutoSubmit = true;
          autoSubmitName = f.getName();
          break;
        }
      }
    }

    // handle action
    if (_isAutoAction)
    {
      _isAutoActOk = autoAct();
    }
    // handle submit
    else if (_isAutoSubmit)
    {
      _isAutoValid = isValid();
      if (_isAutoValid)
      {
        _isAutoSubmitOk = autoSubmit();
        for (Iterator i = subBeans.iterator(); i.hasNext() && _isAutoSubmitOk;)
        {
          _isAutoSubmitOk = ((FieldBean)i.next()).autoSubmit();
        }
      }
    }
    else if (_isReset )
    {
      resetErrorState();
    }
    // handle load
    else
    {
      _isAutoLoadOk = autoLoad();
      for (Iterator i = subBeans.iterator(); i.hasNext() && _isAutoLoadOk;)
      {
        _isAutoLoadOk = ((FieldBean)i.next()).autoLoad();
      }
    }
  }

  /*
  ** public void autoSetFields(ServletRequest request)
  **
  ** Calls setFields() to load submitted data into bean's fields and then
  ** determines what action should be taken.  If an action button was pressed
  ** then autoAct() is called.  If any other button was pressed and the
  ** field data is all valid, autoSubmit() is called.  If no button was
  ** pressed autoLoad() is called.
  */
  public void autoSetFields(HttpServletRequest request)
  {
    // first, set the field data
    setFields(request);

    autoSetAction();
  }

  /*
  ** public void autoSetFields(ResultSet resultSet)
  **
  ** Performs activities identical to autoSetFields(HttpServletRequest)
  ** except that it utilizes a ResultSet to set field data
  */
  public void autoSetFields(ResultSet resultSet)
  {
    try
    {
      // set fields
      setFields(resultSet);

      autoSetAction();
    }
    catch(Exception e)
    {
      logEntry("autoSetFields(resultSet)", e.toString());
    }
  }

  /*
  ** protected boolean autoAct()
  **
  ** Called from autoSetFields().  This method should be overridden by
  ** subclasses that wish to implement auto actions.
  **
  ** RETURN: default stub always returns true.
  */
  protected boolean autoAct()
  {
    return true;
  }

  /*
  ** protected boolean autoSubmit()
  **
  ** Called from autoSetFields().  This method should be overriden by
  ** subclasses that wish to implement auto submitting.
  **
  ** RETURNS: default stub always returns true.
  */
  protected boolean autoSubmit()
  {
    return true;
  }

  public boolean externalSubmit()
  {
    return autoSubmit();
  }

  /*
  ** protected boolean autoLoad()
  **
  ** Called from autoSetFields().  This method should be overriden by
  ** subclasses that wish to implement auto loading.
  **
  ** RETURNS: default stub always returns true.
  */
  protected boolean autoLoad()
  {
    return true;
  }

  public boolean isSubmitted()
  {
    return fields.isSubmitted();
  }


  /**
   * Given a string and a delimiter string an array is generated containing
   * all substrings separated by the delimiter.
   */
  public static String[] split(String str, String delim)
  {
    StringTokenizer tok = new StringTokenizer(str,delim);
    String[] strs = new String[tok.countTokens()];
    for (int i = 0; i < strs.length; ++i)
    {
      strs[i] = tok.nextToken().trim();
    }
    return strs;
  }

  /**
   * Resets the value of the last used auto submit field to blank.  Useful in
   * session scoped beans that need a way to prevent resubmissions from happening
   * after a successful submit, etc.
   */
  public void resetAutoSubmit()
  {
    fields.getField(getAutoSubmitName()).setData("");
  }

  /*
  ** public void setImmutable(boolean immutable)
  **
  ** Sets all contained fields to be immutable
  */
  public void setImmutable(boolean immutable)
  {
    fields.setImmutable(immutable);
  }

  /*
  ** public void setTrackChanges(boolean trackChanges)
  **
  ** Sets all contained fields trackChanges parameter to specified value
  */
  public void setTrackChanges(boolean trackChanges)
  {
    fields.setTrackChanges(trackChanges);
  }

  public void setHtmlExtra(String val)
  {
    fields.setHtmlExtra(val);
  }

}