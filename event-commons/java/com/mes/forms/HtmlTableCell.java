/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlTableCell.java $

  Description:

  HtmlTableCell

  Extends HtmlContainer to define an html table cell element.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlTableCell extends HtmlContainer
{
  {
    addProperty("align");
    addProperty("bgcolor");
    addProperty("colspan");
    addProperty("rowspan");
    addProperty("valign");
    addProperty("width");
    addProperty("height");
    isTransparent = false;
  }
/*
  public HtmlTableCell()
  {
    super();
  }


  public HtmlTableCell(HtmlRenderable hr)
  {
    super();
    addContent(hr);
  }

  public HtmlTableCell(String contents)
  {
    super();
    add(contents);
  }
*/
  public String getName()
  {
    return "td";
  }

  public HtmlRenderable get(int idx)
  {
    return getContent(idx);
  }

  public HtmlTableCell cloneCell()
  {
    HtmlTableCell cell = new HtmlTableCell();
    cell.copyProperties(this);
    return cell;
  }
}