/*@lineinfo:filename=XMLFieldMapper*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/XMLFieldMapper.sqlj $

  Description:
  
  XMLFieldMapper
  
  Database-enabled helper bean that retrieves an XML=>Field mapping
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class XMLFieldMapper extends SQLJConnectionBase
{
  private int         mapType       = 0;
  private FieldGroup  fields        = null;
  private Document    doc           = null;
  private long        virtualSeqNum = 0L;
  private boolean     usesXPath     = false;
  private String      rootElement   = "";
  
  public XMLFieldMapper()
  {
  }
  
  public XMLFieldMapper(int mapType, FieldGroup fields, Document doc, long virtualSeqNum, DefaultContext dfltCtx)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    if(dfltCtx != null)
    {
      Ctx = dfltCtx;
    }
    
    try
    {
      connect();

      this.mapType = mapType;
      this.fields = fields;
      this.doc = doc;
      this.virtualSeqNum = virtualSeqNum;
      
      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(root_element, ' ')  root_element,
//                  nvl(uses_xpath, 'N')    uses_xpath
//          from    xml_field_map_type
//          where   map_type = :mapType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(root_element, ' ')  root_element,\n                nvl(uses_xpath, 'N')    uses_xpath\n        from    xml_field_map_type\n        where   map_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.XMLFieldMapper",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mapType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.XMLFieldMapper",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        rootElement = rs.getString("root_element");
        usesXPath = rs.getString("uses_xpath").equals("Y");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("XMLFieldMapper constructor(" + mapType + ", " + virtualSeqNum + ")", e.toString());
      System.out.println("XMLFieldMapper constructor(" + mapType + ", " + virtualSeqNum + "): " + e.toString());
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void logMapError(String element, String error)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_field_map_error
//          (
//            log_date,
//            xml_element,
//            log_error,
//            vapp_seq_num
//          )
//          values
//          (
//            sysdate,
//            :element,
//            :error,
//            :virtualSeqNum
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_field_map_error\n        (\n          log_date,\n          xml_element,\n          log_error,\n          vapp_seq_num\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.forms.XMLFieldMapper",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,element);
   __sJT_st.setString(2,error);
   __sJT_st.setLong(3,virtualSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
    }
    catch(Exception e)
    {
      System.out.println("error logging map error: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** private String findValue(String element)
  **
  ** This method attempts to retrieve a value from an XML Document according
  ** to the path specified in the 'element' attribute.  This attribute can 
  ** specify multpile levels of the xml document by separating the individual
  ** elements with a semicolon.  In addition, a particular element can include
  ** a attribute (only one is supported at this time) with a colon separating
  ** the element from the attribute.  The attribute must include an equals sign
  ** to declare the value of the attribute.
  **
  ** For example: Owners;Owner:id=1;FirstName would map to an value as:
  **
  ** <Owners>
  **   <Owner id="1">
  **     <FirstName>value</FirstName>
  **   </Owner>
  ** </Owners>
  */
  private String findValue(String element)
  {
    String value = "";
    try
    {
      // first parse the element into tree
      StringTokenizer tokenizer = new StringTokenizer(element, ";");
      
      // walk through the document according to specified element mapping
      Element curElement = doc.getRootElement();
      
      while(tokenizer.hasMoreTokens())
      {
        // determine if this token includes a attribute
        StringTokenizer params = new StringTokenizer(tokenizer.nextToken(), ":");
        
        if(params.countTokens() == 1)
        {
          // just the element name
          curElement = curElement.getChild(params.nextToken());
        }
        else
        {
          // first token is the element name
          String elementName  = params.nextToken();
          String attr         = params.nextToken(); 
          
          StringTokenizer attrToken = new StringTokenizer(attr, "=");
          
          if(attrToken.countTokens() != 2)
          {
            throw new Exception("Error parsing attribute element (count = " + params.countTokens() + "): " + element);
          }
          else
          {
            String attribute  = attrToken.nextToken();
            String attrValue  = attrToken.nextToken();
            
            // get list of elements under curElement
            Iterator itr = (curElement.getChildren(elementName)).iterator();
            
            while(itr.hasNext())
            {
              Element thisOne = (Element)itr.next();
              if(thisOne.getAttributeValue(attribute).equals(attrValue))
              {
                curElement = thisOne;
                break;
              }
            }
          }
        }
      }
      
      // curElement should now be pointing at the Element that includes the data
      if(curElement != null)
      {
        value = curElement.getText();
      }
      
      //System.out.println("(original)element: " + element + " value = " + value);
    }
    catch(Exception e)
    {
      logMapError(element, e.toString());
      com.mes.support.SyncLog.LogEntry("XMLFieldMapper::findValue(" + element + ")", e.toString());
    }
    
    return value;
  }
  
  private String findXPathValue(XMLField xmlField)
  {
    String value = "";
    try
    {
      Element element = (Element)(XPath.selectSingleNode(doc, rootElement+xmlField.element));

      if(element != null)
      {
        value = element.getText();
        
        if(xmlField.decimalCorrectionFactor > 0)
        {
          if(value.length() < xmlField.decimalCorrectionFactor)
          {
            StringBuffer newVal = new StringBuffer(value);
            // need to add some zeroes
            for(int i=0; i<(xmlField.decimalCorrectionFactor-value.length()); ++i)
            {
              newVal.insert(0, '0');
            }
            value = newVal.insert(0, '.').toString();
          }
          else
          {
            // add a decimal point where necessary
            value = (new StringBuffer(value)).insert(value.length()-xmlField.decimalCorrectionFactor, '.').toString();
          }
        }
        //System.out.println("(xpath)element: " + rootElement+path + " value = " + value);
      }
    }
    catch(Exception e)
    {
      System.out.println("XMLFieldMapper::findXPathValue(" + xmlField.element + "): " + e.toString());
      logEntry("findXPathValue(" + xmlField.element + ")", e.toString());
      logMapError(xmlField.element, e.toString());
    }
    
    return value;
  }
  
  private Vector getXMLFieldMap()
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    Vector              map             = new Vector();
    
    try
    {
      connect();
      
      // retrieve mapping from database
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  xml_element               xml_element,
//                  field_name                field_name,
//                  decimal_correction_factor decimal_correction_factor
//          from    xml_field_mapping
//          where   map_type = :mapType
//          order by display_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xml_element               xml_element,\n                field_name                field_name,\n                decimal_correction_factor decimal_correction_factor\n        from    xml_field_mapping\n        where   map_type =  :1 \n        order by display_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.forms.XMLFieldMapper",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mapType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.forms.XMLFieldMapper",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        map.add(new XMLField(rs));
      }
    }
    catch(Exception e)
    {
      logEntry("getXMLFieldMap(" + mapType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return map;
  }
  
  private boolean _mapToFields()
  {
    boolean result        = false;
    Vector  nonRecurring;
    Vector  recurring;
    try
    {
      connect();
      
      this.virtualSeqNum = virtualSeqNum;
      this.doc = doc;
      
      Vector xmlFields = getXMLFieldMap();
      
      if(usesXPath)
      {
        // use XPath syntax for setting fields
        for(int i=0; i<xmlFields.size(); ++i)
        {
          XMLField  xmlField  = (XMLField)(xmlFields.elementAt(i));
          String    xmlValue  = findXPathValue(xmlField);
          
          if(xmlValue != null)
          {
            Field field = fields.getField(xmlField.fieldName);
            if(field != null)
            {
              field.setDataTrim(xmlValue);
            }
            else
            {
              //@System.out.println("XMLFieldMapper::_mapToFields: couldn't find field " + xmlField.fieldName + " in list of fields");
            }
          }
        }
      }
      else
      {
        // use old style syntax for setting fields
        for(int i=0; i<xmlFields.size(); ++i)
        {
          XMLField xmlField = (XMLField)(xmlFields.elementAt(i));
        
          String xmlValue = findValue(xmlField.element);
        
          if(xmlValue != null)
          {
            Field field = fields.getField(xmlField.fieldName);
            if(field != null)
            {
              field.setDataTrim(xmlValue);
            }
          }
        }
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("_mapToFields(" + mapType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static boolean mapToFields(int mapType, FieldGroup fields, Document doc, long virtualSeqNum, DefaultContext Ctx)
  {
    XMLFieldMapper mapper = new XMLFieldMapper(mapType, fields, doc, virtualSeqNum, Ctx);
    
    return mapper._mapToFields();
  }
  
  
  public class XMLField
  {
    public String   element   = "";
    public String   fieldName = "";
    public int      decimalCorrectionFactor = 0;
    
    public XMLField(ResultSet rs)
    {
      try
      {
        element                 = rs.getString("xml_element");
        fieldName               = rs.getString("field_name");
        decimalCorrectionFactor = rs.getInt("decimal_correction_factor");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry("XMLField::constructor()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/