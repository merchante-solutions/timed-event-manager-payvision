/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlTable.java $

  Description:

  HtmlTable

  Extends HtmlContainer to define an html table element.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlTable extends HtmlContainer
{
  {
    addProperty("align");
    addProperty("bgcolor");
    addProperty("border");
    addProperty("cellpadding");
    addProperty("cellspacing");
    addProperty("height");
    addProperty("width");
    isTransparent = false;
  }

  public String getName()
  {
    return "table";
  }

  protected HtmlRenderable addContent(HtmlRenderable hr, int idx)
  {
    if (hr instanceof HtmlTableRow)
    {
      return super.addContent(hr,idx);
    }
    return hr;
  }
  protected HtmlRenderable addContent(HtmlRenderable hr)
  {
    if (hr instanceof HtmlTableRow)
    {
      return super.addContent(hr);
    }
    return hr;
  }

  public HtmlTableRow add(HtmlTableRow row, int idx)
  {
    return (HtmlTableRow)addContent(row,idx);
  }
  public HtmlTableRow add(HtmlTableRow row)
  {
    return (HtmlTableRow)addContent(row);
  }

  public HtmlTableRow getRow(int rowNum)
  {
    return (HtmlTableRow)getContent(rowNum);
  }

  public HtmlTable cloneTable()
  {
    HtmlTable table = new HtmlTable();
    table.copyProperties(this);
    HtmlTableRow tr1 = (HtmlTableRow)getContent(0);
    if (tr1 != null)
    {
      tr1 = tr1.cloneRow();
      table.add(tr1);
    }
    return table;
  }
}