/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/CheckboxField.java $

  Description:
  
  CheckboxField
  
  Renders a checkbox html field.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class CheckboxField extends Field
{
  private boolean     resetToChecked  = false;
  private boolean     showLabel       = true;
  private String      OnClick         = "";
  
  public CheckboxField(String fname, String label, boolean isChecked)
  {
    super(fname,label,0,0,true);
    fdata = (isChecked ? "y" : "n");
    
    resetToChecked = isChecked;
  }
  
  public CheckboxField(String fname, boolean isChecked)
  {
    super(fname,"",0,0,true);
    fdata = (isChecked ? "y" : "n");
    resetToChecked = isChecked;
  }
  
  public CheckboxField(String fname, String label, boolean isChecked, boolean doShowLabel)
  {
    super(fname,label,0,0,true);
    fdata = (isChecked ? "y" : "n");
    resetToChecked = isChecked;
    showLabel = doShowLabel;
  }
  
  public boolean isBlank()
  {
    boolean     retVal    = false;
    
    // consider not checked ("n") as blank
    if( super.isBlank() || fdata.equals("n") )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public void setShowLabel(boolean showLabel)
  {
    this.showLabel = showLabel;
  }
  
  public boolean isChecked()
  {
    return (fdata != null && fdata.equals("y"));
  }
  
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    
    // create hidden marker field that will tell us what to do when the 
    // checkbox was left unchecked at submit
    HiddenField marker = new HiddenField(getName()+"_cboxMarker", "mark");
    
    html.append("<table cellpadding=\"0\" cellspacing=\"0\"><tr><td ");
    html.append(getHtmlExtra());
    html.append(">");
    html.append(marker.renderHtml());
    html.append("<input type=\"checkbox\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("value=\"y\" ");
    html.append((isChecked() ? "checked " : " "));
    if( OnClick != null && OnClick.length() > 0 )
    {
      html.append("onClick=\"");
      html.append(OnClick);
      html.append("\" ");
    }                    
    html.append(getHtmlExtra());
    html.append(">");
    if( showLabel == true )
    {
      html.append("</td><td valign=\"center\" ");
      html.append(getHtmlExtra());
      html.append(">");
      html.append(getLabelText());
    }
    html.append("</td></tr></table>");
    html.append("\n");
    
    return html.toString();
  }
  
  // 
  // Overloaded HTML rendering method.  
  //
  //  The field rendering method needed to be overloaded to put the 
  //  checkbox in a table so that the label could be centered beside
  //  the checkbox itself.  The table causes the error icon to display
  //  below the checkbox field it is associated with, so it was necessary
  //  to overload the full render method to place both the checkbox table
  //  and the error icon in yet another table.
  //
  public String renderHtml()
  {
    if(readOnly) {
      return getLabelText();
    }
    
    StringBuffer        buf = new StringBuffer();
    
    buf.append("<table cellpadding=\"0\" cellspacing=\"0\">\n");
    buf.append("  <tr>\n    <td>\n");
    buf.append( renderHtmlField() );
    buf.append("    </td>\n    <td>\n");
    buf.append( renderErrorIndicator() );
    buf.append("    </td>\n  </tr>\n</table>\n");
    
    return( buf.toString() );
  }

  // overload to insure that the incoming data is in lower case
  protected String processData(String rawData)
  {
    return (rawData != null ? rawData.toLowerCase() : "");
  }

  public String getLabelText()
  {
    return getLabel();
  }
  
  public void setLabelText(String s)
  {
    setLabel(s);
  }
  
  public void reset()
  {
    fdata = resetToChecked ? "y" : "n";
  }
  
  public void setOnClick(String onClick)
  {
    OnClick = onClick;
  }
}
