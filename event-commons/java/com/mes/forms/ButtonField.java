/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ButtonField.java $

  Description:
  
  ButtonField
  
  Extends field to define an html form submit button.  Buttons submit there
  own name as their value when pressed.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-22 10:31:52 -0800 (Thu, 22 Feb 2007) $
  Version            : $Revision: 13480 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ButtonField extends Field
{
  protected String buttonText = "";
  protected String bname = "";  
    // the html element name to allow for different field names to "point" to the same html button field.
  
  public ButtonField(String fname)
  {
    super(fname,0,0,true);
    this.buttonText = fname;
    this.bname=fname;
  }
  public ButtonField(String fname, String label)
  {
    super(fname,label,0,0,true);
    this.buttonText = label;
    this.bname=fname;
  }
  public ButtonField(String fname, String label, String buttonText)
  {
    super(fname,label,0,0,true);
    this.buttonText = buttonText;
    this.bname=fname;
  }
  public ButtonField(String fname, String label, String buttonText, String buttonName)
  {
    super(fname,label,0,0,true);
    this.buttonText = buttonText;
    this.bname=buttonName;
  }
  
  public String getButtonText()
  {
    return buttonText;
  }
  
  public void setButtonText(String buttonText)
  {
    if(buttonText==null || buttonText.length()<1)
      return;
    
    this.buttonText = buttonText;
  }
  
  public String getButtonName()
  {
    return bname;
  }
  
  public void setButtonName(String buttonName)
  {
    if(buttonName==null || buttonName.length()<1)
      return;
    
    this.bname = buttonName;
  }
  
  protected void validateData()
  {
    hasError = false;
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"submit\" ");
    html.append("name=\"" + bname + "\" ");
    html.append("value=\"" + buttonText + "\" ");
    if (tabIndex > 0)
    {
      html.append("tabindex=\"" + tabIndex + "\" ");
    }
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }
}
