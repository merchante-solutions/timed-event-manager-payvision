/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ImageButtonField.java $

  Description:
  
  ImageButtonField
  
  Extends FieldGroup to define an html form submit image button.  Constructor
  takes an image url with the image height and width.  Two internal parameter
  fields are created and stored inside the field to accomodate the .x and .y
  parameters that are submitted when the image is clicked.  These parameter
  fields cause the data value of their parent image button field to be set to
  it's own name to indicate that the image was clicked similar to how a
  ButtonField submits it's own name when pressed.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ImageButtonField extends FieldGroup
{
  private String  imageUrl;
  private int     height;
  private int     width;
  private String  altText;
  
  public class ParameterField extends Field
  {
    public ParameterField(String fname)
    {
      super(fname,5,5,true);
    }
    
    protected String processData(String rawData)
    {
      ImageButtonField.this.setData(ImageButtonField.this.getName());
      return rawData;
    }
  }
  
  public ImageButtonField(String fname, String label, String imageUrl, 
    int height, int width)
  {
    this(fname, label, imageUrl, height, width, label);
  }
  public ImageButtonField(String fname, String imageUrl, int height, int width)
  {
    this(fname,fname,imageUrl,height,width,fname);
  }
  
  public ImageButtonField(String fname, String label, String imageUrl, int height, int width, String altText)
  {
    super(fname,label);
    
    this.imageUrl = imageUrl;
    this.height   = height;
    this.width    = width;
    this.altText  = altText;
    
    add(new ParameterField(fname + ".x"));
    add(new ParameterField(fname + ".y"));
  }
  
  protected void validateData()
  {
    hasError = false;
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"image\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("src=\"" + imageUrl + "\" ");
    html.append("align=\"middle\" ");
    html.append("border=\"0\" ");
    html.append("height=\"" + height + "\" ");
    html.append("width=\"" + width + "\" ");
    html.append("hspace=\"0\" ");
    html.append("vspace=\"0\" ");
    html.append("alth=\"" + altText + "\" ");
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }
}
