/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlContainer.java $

  Description:

  HtmlContainer
  
  This abstract class is a base for html elements that consist of a begin
  tag, some html content and an end tag.  It provides a rendering scheme.
  Child classes will need to provide a name and methods for adding content.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public abstract class HtmlContainer extends HtmlTag
{
  {
    addProperty("class");
  }
  
  // Transparency determines how this container will be added into
  // other containers and how it is rendered.  If a transparent
  // container is added to a container, the transparent container's
  // contents are added individually (the transparent container is
  // "stripped" off).  When a transparent container is rendered it's
  // contents are rendered without start and end tags (it is invisible
  // when rendered).  Child classes that are not transparent must
  // override the default value here during initialization to make
  // themselves non-transparent.
  protected boolean isTransparent = true;
  
  /*************************************************************************
  **
  ** Indentation handling
  **
  *************************************************************************/

  // base indentation level
  private int startIndentLevel = 0;
  
  /*
  ** public void setStartIndentLevel(int startIndentLevel)
  **
  ** Sets the base indentation level used when formatting rendered html.
  */
  public void setStartIndentLevel(int startIndentLevel)
  {
    this.startIndentLevel = startIndentLevel;
  }
  
  // indentation to use when doing indentation formatting
  private String indent = "  ";
  
  /*
  ** protected void setIndent(String indent)
  **
  ** Sets indentation string to use when indenting.
  */
  protected void setIndent(String indent)
  {
    this.indent = indent;
  }
  
  /*
  ** protected String getIndentation(int indentLevel)
  **
  ** Creates an indentation based on the indent string and the current base
  ** indent level.
  **
  ** RETURNS: an indent String to be placed before a line of html.
  */
  protected String getIndentation(int indentLevel)
  {
    StringBuffer indentBuf = new StringBuffer();
    for (int i = 0; i < indentLevel; ++i)
    {
      indentBuf.append(indent);
    }
    return indentBuf.toString();
  }
  
  /*************************************************************************
  **
  ** Suppress formatting
  **
  *************************************************************************/

  // when true then all contained html is to be rendered without any
  // spacing or carriage returns...this is handy for rendering images
  // and such that need to be flush with each other, etc.
  private boolean suppressFormatting = false;
  
  /*
  ** public void setSuppressFormatting(boolean suppressFormatting)
  **
  ** Sets the format suppress flag.  The value is rolled down into any
  ** containers within this container.
  */
  public void setSuppressFormattingDeep(boolean suppressFormatting)
  {
    this.suppressFormatting = suppressFormatting;
    for (Iterator i = contents.iterator(); i.hasNext();)
    {
      HtmlRenderable hr = (HtmlRenderable)i.next();
      if (hr instanceof HtmlContainer)
      {
        ((HtmlContainer)hr).setSuppressFormattingDeep(suppressFormatting);
      }
    }
  }
  
  public void setSuppressFormatting(boolean suppressFormatting)
  {
    this.suppressFormatting = suppressFormatting;
  }
  
  /*************************************************************************
  **
  ** Container property management
  **
  *************************************************************************/

  // storage for general and tag specific properties
  Hashtable containerProps = new Hashtable();

  /*
  ** private void applyProperty(HtmlRenderable hr, String tag, String name, 
  **  String value)
  **
  ** Applies property with tag, name and value to the given renderable object.
  */
  private void applyProperty(HtmlRenderable hr, String tag, String name, 
    String value)
  {
    // apply to all properties in containers
    if (hr instanceof HtmlContainer)
    {
      ((HtmlContainer)hr).setAllProperties(tag,name,value);
    }
    // apply to property of element
    else
    {
      ((HtmlTag)hr).setProperty(tag,name,value);
    }
  }
  
  /*
  ** private void applyProperty(HtmlRenderable hr, String name, String value)
  **
  ** Applies property with name and value to the given renderable object.
  */
  private void applyProperty(HtmlRenderable hr, String name, String value)
  {
    // apply to all properties in containers
    if (hr instanceof HtmlContainer)
    {
      ((HtmlContainer)hr).setAllProperties(name,value);
    }
    // apply to property of element
    else
    {
      ((HtmlTag)hr).setProperty(name,value);
    }
  }

  /*
  ** public void setAllProperties(String tag, String name, String value)
  **
  ** Sets named property to value in self and all contained items.  Value is
  ** only applied in each case if the tag matches the item's name.
  */
  public void setAllProperties(String tag, String name, String value)
  {
    // apply to self
    setProperty(tag,name,value);
    
    // apply to all contained elements
    for (Iterator i = contents.iterator(); i.hasNext();)
    {
      HtmlRenderable hr = (HtmlRenderable)i.next();
      applyProperty(hr,tag,name,value);      
    }
    
    // store for application to future container content additions
    HtmlProperties props = (HtmlProperties)containerProps.get(tag);
    if (props == null)
    {
      props = new HtmlProperties();
      containerProps.put(tag,props);
    }
    props.set(name,value);
  }
  
  /*
  ** public void setAllProperties(String name, String value)
  **
  ** Applies the property value to property named in this container and all 
  ** its contained elements.
  */
  public void setAllProperties(String name, String value)
  {
    // apply to self
    setProperty(name,value);
    
    // apply to all contained elements
    for (Iterator i = contents.iterator(); i.hasNext();)
    {
      HtmlRenderable hr = (HtmlRenderable)i.next();
      applyProperty(hr,name,value);
    }

    // store for application to future container content additions
    HtmlProperties props = (HtmlProperties)containerProps.get("general");
    if (props == null)
    {
      props = new HtmlProperties();
      containerProps.put("general",props);
    }
    props.set(name,value);
  }
  
  /*
  ** public void setAllProperties(String tag, HtmlProperties props)
  **
  ** Calls setAllProperties for each property contained in props with the
  ** given tag name.
  */
  public void setAllProperties(String tag, HtmlProperties props)
  {
    for (Enumeration e = props.keys(); e.hasMoreElements();)
    {
      String name = (String)e.nextElement();
      String value = (String)props.get(name);
      setAllProperties(tag,name,value);
    }
  }
  
  /*
  ** public void setAllProperties(String tag, HtmlProperties props)
  **
  ** Calls setAllProperties for each property contained in props.
  */
  public void setAllProperties(HtmlProperties props)
  {
    // apply to self
    setProperties(props);
    
    // apply to all contained elements
    for (Enumeration e = props.keys(); e.hasMoreElements();)
    {
      String name = (String)e.nextElement();
      String value = (String)props.get(name);
      setAllProperties(name,value);
    }
  }
  
  /*
  ** private void applyProperties(String tag, HtmlRenderable hr, 
  **   HtmlProperties props)
  **
  ** Applies the given set of properties with the named tag association to
  ** the specified object.
  */
  private void applyProperties(String tag, HtmlRenderable hr, 
    HtmlProperties props)
  {
    for (Enumeration e = props.keys(); e.hasMoreElements();)
    {
      String name   = (String)e.nextElement();
      String value  = (String)props.get(name);
      applyProperty(hr,tag,name,value);
    }
  }

  /*
  ** private void applyProperties(HtmlRenderable hr, HtmlProperties props)
  **
  ** Applies the given set of properties to the specified object.
  */
  private void applyProperties(HtmlRenderable hr, HtmlProperties props)
  {
    for (Enumeration e = props.keys(); e.hasMoreElements();)
    {
      String name   = (String)e.nextElement();
      String value  = (String)props.get(name);
      applyProperty(hr,name,value);
    }
  }
  
  /*
  ** private void passOnProperties(HtmlRenderable hr)
  **
  ** Applies all container props to newly added content.
  */
  private void passOnProperties(HtmlRenderable hr)
  {
    // for each prop list in container props apply prop list to hr
    for (Enumeration e = containerProps.keys(); e.hasMoreElements();)
    {
      String tag            = (String)e.nextElement();
      HtmlProperties props  = (HtmlProperties)containerProps.get(tag);

      // general properties
      if (tag.equals("general"))
      {
        applyProperties(hr,props);
      }
      // tag specific properties
      else
      {
        applyProperties(tag,hr,props);
      }
    }
  }
  
  /*************************************************************************
  **
  ** Contents handling
  **
  *************************************************************************/

  // vector of html elements contained by this container
  protected Vector contents = new Vector();
  
  /*
  ** public int getContentCount()
  **
  ** RETURNS: number of items contained in contents.
  */
  public int getContentSize()
  {
    return contents.size();
  }
  
  /*
  ** protected HtmlRenderable addContent(HtmlRenderable hr)
  **
  ** Adds an HtmlRenderable object to the contents vector.
  **
  ** RETURNS: object added.
  */
  protected HtmlRenderable addContent(HtmlRenderable hr, int idx)
  {
    if (hr == null) throw new NullPointerException();
    passOnProperties(hr);
    contents.add(idx,hr);
    return hr;
  }
  protected HtmlRenderable addContent(HtmlRenderable hr)
  {
    return addContent(hr,contents.size());
  }
  
  /*
  ** public HtmlContainer addContainerContents(HtmlContainer container)
  **
  ** Special handler that adds the contents of a container individually (the
  ** container itself is not added, only its contents).
  **
  ** RETURNS: reference to the container.
  */
  public HtmlContainer addContainerContents(HtmlContainer container, int idx)
  {
    if (container == null) throw new NullPointerException();
    
    int idxOff = idx;
    for (Iterator i = container.contents.iterator(); i.hasNext(); ++idxOff)
    {
      addContent((HtmlRenderable)i.next(),idxOff);
    }
    return container;
  }
  public HtmlContainer addContainerContents(HtmlContainer container)
  {
    return addContainerContents(container,contents.size());
  }
  
  /*
  ** public HtmlRenderable add(HtmlRenderable hr)
  **
  ** Accepts an html renderable object, adds it to container contents.
  ** When adding transparent containers the contents of the container 
  ** are added without the container itself.
  **
  ** RETURNS: the html renderable object added.
  */
  public HtmlRenderable add(HtmlRenderable hr, int idx)
  {
    if (hr instanceof HtmlContainer)
    {
      HtmlContainer hc = (HtmlContainer)hr;
      if (hc.isTransparent)
      {
        addContainerContents(hc,idx);
      }
      else
      {
        addContent(hr,idx);
      }
    }
    else
    {
      addContent(hr,idx);
    }
    
    return hr;
  }
  public HtmlRenderable add(HtmlRenderable hr)
  {
    return add(hr,contents.size());
  }
  
  /*
  ** public HtmlRenderable add(String text)
  **
  ** Default adder allowing String text to be added.  Strings are wrapped
  ** in an HtmlText object and added to contents.
  **
  ** RETURNS: HtmlRenderable reference to the HtmlText object.
  */
  public HtmlRenderable add(String text, int idx)
  {
    HtmlText ht = new HtmlText(text);
    addContent(ht,idx);
    return ht;
  }
  public HtmlRenderable add(String text)
  {
    return add(text,contents.size());
  }
  
  /*
  ** protected HtmlRenderable removeContent(int removeIdx)
  **
  ** Removes the item at the specified index.
  **
  ** RETURNS: removed item, null if does not exist.
  */
  protected HtmlRenderable removeContent(int removeIdx)
  {
    try
    {
      return (HtmlRenderable)contents.remove(removeIdx);
    }
    catch (Exception e)
    {
    }
    return null;
  }
      
  /*
  ** public HtmlRenderable remove(int removeIdx)
  **
  ** Attempts to remove the item with the given index from contents.
  **
  ** RETURNS: the item removed, or null if index is invalid.
  */
  public HtmlRenderable remove(int removeIdx)
  {
    return removeContent(removeIdx);
  }
  
  /*
  ** public void truncate(int truncIdx)
  **
  ** Truncates all items from truncIdx to the end of contents.
  */
  public void truncate(int truncIdx)
  {
    try
    {
      if (truncIdx >= 0)
      {
        while (contents.size() > truncIdx)
        {
          contents.remove(truncIdx);
        }
      }
    }
    catch (Exception e)
    {
    }
  }
  
  /*
  ** protected HtmlRenderable getContent(int idx)
  **
  ** RETURNS: the renderable object at the specified index in the contents
  **          Vector.
  */
  protected HtmlRenderable getContent(int idx)
  {
    if (idx < contents.size())
    {
      return (HtmlRenderable)contents.elementAt(idx);
    }
    return null;
  }
  
  /*************************************************************************
  **
  ** Rendering
  **
  *************************************************************************/

  /*
  ** protected String renderStartTag()
  **
  ** Renders the start tag of the container in the form of "<tag>" (using
  ** renderTag()).
  **
  ** RETURNS: String containing the start tag.
  */
  protected String renderStartTag()
  {
    return renderTag();
  }
  
  /*
  ** protected String renderContentsHtml(int indentLevel)
  **
  ** Renders all contained html renderable contents.  The indent level is
  ** passed in to contained objects that are also containers.  Indentation
  ** is manually padded to non-container objects if formatting is not being
  ** suppressed.  Carriage returns are also added for readability unless
  ** formatting is being suppressed.
  **
  ** RETURNS: String containing the rendered html contents.
  */
  protected String renderContentsHtml(int indentLevel)
  {
    StringBuffer html = new StringBuffer();

    String eol = (suppressFormatting ? "" : "\n");
    
    for (Iterator i = contents.iterator(); i.hasNext();)
    {
      HtmlRenderable hr = (HtmlRenderable)i.next();
      if (hr instanceof HtmlContainer)
      {
        html.append(((HtmlContainer)hr).renderHtml(indentLevel));
      }
      else
      {
        html.append((suppressFormatting ? "" : getIndentation(indentLevel)) 
          + hr.renderHtml() + eol);
      }
    }
    
    return html.toString();
  }
  
  /*
  ** protected String renderEndTag()
  **
  ** Renders the end tag of the container in the form of "</tag>".
  **
  ** RETURNS: String containing the end tag.
  */
  protected String renderEndTag()
  {
    return "</" + getName() + ">";
  }
  
  /*
  ** public String renderHtml(int indentLevel)
  **
  ** Renders this container html tag along with it contents using the specified
  ** indentation level.
  **
  ** RETURNS: this container and its contents as html.
  */
  public String renderHtml(int indentLevel)
  {
    StringBuffer html = new StringBuffer();
    
    String eol = (suppressFormatting ? "" : "\n");
    String indentation = (suppressFormatting ? "" : getIndentation(indentLevel));

    html.append(indentation + renderStartTag() + eol);
    html.append(renderContentsHtml(indentLevel + 1));
    html.append(indentation + renderEndTag() + eol);
    
    return html.toString();
  }
  
  
  /*
  ** public String renderHtml()
  **
  ** Calls the main html rendering routine using the base indent level.
  **
  ** RETURNS: the container and its contents rendered as html.
  */
  public String renderHtml()
  {
    return renderHtml(startIndentLevel);
  }
}
