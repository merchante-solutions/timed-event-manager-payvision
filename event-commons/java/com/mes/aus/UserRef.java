package com.mes.aus;

/**
 * Used to reference MES users.
 */
public class UserRef extends AusBase
{
  private long userId;
  private String fullName;
  private String loginName;
  private String typeName;
  private String ausrName;
  private String ausEnabledFlag;
  private String merchNum;

  public void setUserId(long userId)
  {
    this.userId = userId;
  }
  public long getUserId()
  {
    return userId;
  }

  public void setFullName(String fullName)
  {
    this.fullName = fullName;
  }
  public String getFullName()
  {
    return fullName;
  }

  public void setLoginName(String loginName)
  {
    this.loginName = loginName;
  }
  public String getLoginName()
  {
    return loginName;
  }

  public void setTypeName(String typeName)
  {
    this.typeName = typeName;
  }
  public String getTypeName()
  {
    return typeName;
  }

  public void setAusrName(String ausrName)
  {
    this.ausrName = ausrName;
  }
  public String getAusrName()
  {
    return ausrName;
  }
  public boolean isAusUser()
  {
    return (ausrName != null);
  }

  public void setAusEnabledFlag(String ausEnabledFlag)
  {
    validateFlag(ausEnabledFlag,"ausEnabled");
    this.ausEnabledFlag = ausEnabledFlag;
  }
  public String getAusEnabledFlag()
  {
    return flagValue(ausEnabledFlag);
  }
  public boolean isAusEnabled()
  {
    return flagBoolean(ausEnabledFlag);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public boolean isMerchUser()
  {
    return (merchNum != null);
  }

  public String toString()
  {
    return "UserRef [ "
      + "userId: " + userId
      + ", loginName: " + loginName
      + ", fullName: " + fullName
      + ", typeName: " + typeName
      + ", ausrName: " + (ausrName != null ? ausrName : "--")
      + ", merchNum: " + (merchNum != null ? merchNum : "--")
      + " ]";
  }
}