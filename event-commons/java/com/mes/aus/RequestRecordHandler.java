package com.mes.aus;

import java.util.List;

public interface RequestRecordHandler
{
  public void handleRequests(RequestFile reqf, List requests) 
    throws Exception;
}