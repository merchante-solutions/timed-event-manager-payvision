package com.mes.aus;

import org.apache.log4j.Logger;

/**
 * Profile reference.  Used to find valid MES profiles to create in the
 * AUS profile table.  Currently AUS supports one profile per merchant.
 */
public class ProfileRef extends AusBase
{
  static Logger log = Logger.getLogger(ProfileRef.class);

  private String terminalId;
  private String merchNum;
  private String merchName;
  private String profileId;
  private String ausFlag;

  public String getTerminalId()
  {
    return terminalId;
  }
  public void setTerminalId(String terminalId)
  {
    this.terminalId = terminalId;
  }
  
  public String getMerchNum()
  {
    return merchNum;
  }
  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  
  public String getMerchName()
  {
    return merchName;
  }
  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  
  public String getProfileId()
  {
    return profileId;
  }
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  
  public void setAusFlag(String ausFlag)
  {
    validateFlag(ausFlag,"aus");
    this.ausFlag = ausFlag;
  }
  public String getAusFlag()
  {
    return flagValue(ausFlag);
  }
  public boolean isAus()
  {
    return flagBoolean(ausFlag);
  }

  public String toString()
  {
    return "ProfileRef [ "
      + "terminalId: " + terminalId
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + ", aus profileId: " + profileId
      + ", isAus: " + isAus()
      + " ]";
  }
}
