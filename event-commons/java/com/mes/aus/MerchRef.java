package com.mes.aus;

import org.apache.log4j.Logger;

/**
 * Merchant reference.  These objects refer to MES merchant accounts in the 
 * mif table, but also have an ausrId that allow the reference to indicate
 * whether the merchant account is linked to the ausrId.
 */
public class MerchRef extends AusBase
{
  static Logger log = Logger.getLogger(MerchRef.class);

  private long ausrId;
  private String merchNum;
  private String merchName;
  private String linkedFlag;

  public void setAusrId(long ausrId)
  {
    this.ausrId = ausrId;
  }
  public long getAusrId()
  {
    return ausrId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setLinkedFlag(String linkedFlag)
  {
    validateFlag(linkedFlag,"linked");
    this.linkedFlag = linkedFlag;
  }
  public String getLinkedFlag()
  {
    return flagValue(linkedFlag);
  }
  public boolean isLinked()
  {
    return flagBoolean(linkedFlag);
  }

  public String toString()
  {
    return "MerchRef [ "
      + "ausrId: " + ausrId
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + ", isLinked: " + isLinked()
      + " ]";
  }
}
