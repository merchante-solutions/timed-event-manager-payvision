package com.mes.aus;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import org.apache.log4j.Logger;
import com.mes.support.dc.EncodedFileContainer;
import com.mes.support.dc.MesSecureFileContainer;

public class AusFileContainer extends AusBase implements EncodedFileContainer, Serializable
{
  static Logger log = Logger.getLogger(AusFileContainer.class);

  protected EncodedFileContainer efc = new MesSecureFileContainer();
  
  public void setData(InputStream in)
  {
    efc.setData(in);
  }
  
  public void setData(byte[] data)
  {
    efc.setData(data);
  }
  
  public void setData(String data)
  {
    efc.setData(data);
  }
  
  public InputStream getInputStream()
  {
    return efc.getInputStream();
  }
  
  public OutputStream getOutputStream()
  {
    return efc.getOutputStream();
  }
  
  public boolean empty()
  {
    return efc.empty();
  }
  
  public long size()
  {
    return efc.size();
  }
  
  public void release()
  {
    efc.release();
  }
  
  public int writeData(OutputStream out)
  {
    return efc.writeData(out);
  }
  
  public void setEncodedData(InputStream in) throws Exception
  {
    efc.setEncodedData(in);
  }
  
  public void setEncodedData(byte[] data) throws Exception
  {
    efc.setEncodedData(data);
  }
  
  public InputStream getEncodedInputStream()
  {
    return efc.getEncodedInputStream();
  }
  
  public OutputStream getEncodedOutputStream()
  {
    return efc.getEncodedOutputStream();
  }
  
  public int writeEncodedData(OutputStream out)
  {
    return efc.writeEncodedData(out);
  }
  
  public int encodeStream(InputStream in, OutputStream out)
  {
    return efc.encodeStream(in,out);
  }
  
  public int decodeStream(InputStream in, OutputStream out)
  {
    return efc.decodeStream(in,out);
  }
}