package com.mes.aus;

public class SimAccount extends AusBase
{
  private String sysCode;
  private String accountNum;
  private String expDate;
  private String respCode;
  private String newAccountNum;
  private String newExpDate;

  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  public String getSysCode()
  {
    return sysCode;
  }

  public void setAccountNum(String accountNum)
  {
    this.accountNum = accountNum;
  }
  public String getAccountNum()
  {
    return accountNum;
  }

  public void setExpDate(String expDate)
  {
    this.expDate = expDate;
  }
  public String getExpDate()
  {
    return expDate;
  }

  public void setRespCode(String respCode)
  {
    this.respCode = respCode;
  }
  public String getRespCode()
  {
    return respCode;
  }

  public void setNewAccountNum(String newAccountNum)
  {
    this.newAccountNum = newAccountNum;
  }
  public String getNewAccountNum()
  {
    return newAccountNum;
  }

  public void setNewExpDate(String newExpDate)
  {
    this.newExpDate = newExpDate;
  }
  public String getNewExpDate()
  {
    return newExpDate;
  }

  public String toString()
  {
    return "SimAccount ["
      + ", sysCode: " + sysCode
      + ", accountNum: " + accountNum
      + ", expDate: " + expDate
      + ", respCode: " + respCode
      + ", newAccountNum: " + newAccountNum
      + ", newExpDate: " + newExpDate
      + " ]";
  }
}