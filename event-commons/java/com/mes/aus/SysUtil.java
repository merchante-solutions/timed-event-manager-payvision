package com.mes.aus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * System utility, provides several functions pertaining to systems.
 *
 * System result code mapping.  Maps card association response codes 
 * to internal MES response codes, provides method to convert.
 *
 * System to source code translation.  Generates response file source
 * codes from system descriptor strings.
 */
public class SysUtil
{
  static Logger log = Logger.getLogger(SysUtil.class);

  // system types  
  public static final String SYS_MES = "MES";
  public static final String SYS_ABU = "ABU";
  public static final String SYS_VAU = "VAU";
  public static final String SYS_DAU = "DAU";

  // system source codes
  public static final Integer SRC_MES = 1;
  public static final Integer SRC_VAU = 2;
  public static final Integer SRC_ABU = 3;
  public static final Integer SRC_DAU = 4;

  // internal MES response codes
  public static final String RC_GOOD      = "GOOD";
  public static final String RC_NEWACCT   = "NEWACCT";
  public static final String RC_NEWEXP    = "NEWEXP";
  public static final String RC_CALL      = "CALL";
  public static final String RC_CLOSED    = "CLOSED";
  public static final String RC_NPBIN     = "NPBIN";
  public static final String RC_NOMATCH   = "NOMATCH";
  public static final String RC_EXPERR    = "EXPERR";
  public static final String RC_ACCTERR   = "ACCTERR";
  public static final String RC_MERCHERR  = "MERCHERR";
  public static final String RC_OVERRIDE  = "OVERRIDE"; // dau: issuer override of previous update
  public static final String RC_ERROR     = "ERROR";    // dau: undefined error
  
  public static final String RC_VALID     = "VALID";
  public static final String RC_UNKNWN     = "UNKNWN";

  // system file types
  public static final String FT_ABU_RSP   = "ABURSP";
  public static final String FT_VAU_RSP   = "VAURSP";
  public static final String FT_VAU_RPT1  = "VAURPT1";
  public static final String FT_VAU_RPT2  = "VAURPT2";
  public static final String FT_VAU_RPT3  = "VAURPT3";
  public static final String FT_VAU_RPT4  = "VAURPT4";
  public static final String FT_VAU_RPT5  = "VAURPT5";
  public static final String FT_VAU_RPT6  = "VAURPT6";
  public static final String FT_DAU_RSP   = "DAURSP";
  public static final String FT_DAU_RPT1  = "DAURPT1";

  // top level sys code to sys-specific map (sys code -> sys map)
  private static Map sysRcMaps = new HashMap();

  // sys-specific maps (sys rc -> mes rc)
  private static Map abuRcMap = new HashMap();
  private static Map vauRcMap = new HashMap();
  private static Map dauRcMap = new HashMap();

  // system descriptor -> response source code
  private static Map srcMap = new HashMap();

  // static initializer
  static
  {
    // abu response codes
    abuRcMap.put("UPDATE",RC_NEWACCT);
    abuRcMap.put("CONTAC",RC_CLOSED);
    abuRcMap.put("EXPIRY",RC_NEWEXP);
    abuRcMap.put("000101",RC_ACCTERR);
    abuRcMap.put("000102",RC_ACCTERR);
    abuRcMap.put("000103",RC_EXPERR);
    abuRcMap.put("000104",RC_MERCHERR);
    abuRcMap.put("VALID", RC_VALID);
    abuRcMap.put("UNKNWN",RC_UNKNWN);
    abuRcMap.put("N",RC_NPBIN);
    abuRcMap.put("P",RC_NOMATCH);
    abuRcMap.put("V",RC_GOOD);
    
    // vau response codes
    vauRcMap.put("A",RC_NEWACCT);
    vauRcMap.put("E",RC_NEWEXP);
    vauRcMap.put("Q",RC_CALL);
    vauRcMap.put("C",RC_CLOSED);
    vauRcMap.put("N",RC_NPBIN);
    vauRcMap.put("P",RC_NOMATCH);
    vauRcMap.put("V",RC_GOOD);

    // vau reject codes
    vauRcMap.put("1",RC_ACCTERR);
    vauRcMap.put("2",RC_ACCTERR);
    vauRcMap.put("5",RC_EXPERR);
    vauRcMap.put("M",RC_MERCHERR);

    // dau response codes
    dauRcMap.put("A",RC_NEWACCT);
    dauRcMap.put("E",RC_NEWEXP);
    dauRcMap.put("Q",RC_CALL);
    dauRcMap.put("C",RC_CLOSED);
    dauRcMap.put("O",RC_OVERRIDE);
    
    // dau error codes
    dauRcMap.put("15",RC_ACCTERR);
    dauRcMap.put("16",RC_EXPERR);
    dauRcMap.put("27",RC_ERROR);

    sysRcMaps.put(SYS_ABU,abuRcMap);
    sysRcMaps.put(SYS_VAU,vauRcMap);
    sysRcMaps.put(SYS_DAU,dauRcMap);

    srcMap.put(SYS_MES,SRC_MES);
    srcMap.put(SYS_VAU,SRC_VAU);
    srcMap.put(SYS_ABU,SRC_ABU);
    srcMap.put(SYS_DAU,SRC_DAU);
  }

  /**
   * Returns MES rc corresonding with the given systems rc.
   */
  public static String getResponseCode(String sysCode, String sysRspCode)
  {
    Map rcMap = (Map)sysRcMaps.get(sysCode);
    if (rcMap == null)
    {
      throw new RuntimeException("Invalid system code '" + sysCode + "'");
    }
    String rc = (String)rcMap.get(sysRspCode);
    if (rc == null)
    {
      log.warn("No response code found for '" + sysRspCode 
        + "' for system code '" + sysCode + "'");
    }
    return rc;
  }

  public static String getResponseSourceCode(String sysCode)
  {
    Integer srcCode = (Integer)srcMap.get(sysCode);
    if (srcCode == null)
    {
      throw new RuntimeException("Invalid system code '" + sysCode + "'");
    }
    return ""+srcCode;
  }

  public static void validateSysCode(String sysCode)
  {
    if (!SYS_ABU.equals(sysCode) && 
        !SYS_VAU.equals(sysCode) && 
        !SYS_DAU.equals(sysCode))
    {
      throw new RuntimeException("Invalid system code: '" + sysCode + "'");
    }
  }

  /**
   * Parses sys codes from a delimited string.  Codes may be delimited by
   * comma, semicolon or space.
   */
  public static List getSysCodeList(String sysStr)
  {
    // validate systems, parse into list
    Pattern p = Pattern.compile("([^,; ]+)[,; ]?");
    Matcher m = p.matcher(sysStr);
    List sysCodeList = new ArrayList();
    while(m.find())
    {
      String sysCode = m.group(1);
      validateSysCode(sysCode);
      sysCodeList.add(sysCode);
    }
    return sysCodeList;
  }


}