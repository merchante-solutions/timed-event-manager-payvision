package com.mes.aus;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class ProcessError extends AusBase
{
  private long errId;
  private Date createDate = Calendar.getInstance().getTime();
  private long fileId;
  private int lineNum;
  private int linePos;
  private String errorMsg;
  private int severity;

  public static int WARNING  = 1;
  public static int ERROR    = 2;
  public static int FATAL    = 3;

  public ProcessError(long fileId, int lineNum, int linePos, String errorMsg, 
    int severity)
  {
    this.fileId = fileId;
    this.lineNum = lineNum;
    this.linePos = linePos;
    this.errorMsg = errorMsg;
    this.severity = severity;
  }
  public ProcessError(long fileId, int lineNum, int linePos, String errorMsg)
  {
    this(fileId,lineNum,linePos,errorMsg,ERROR);
  }

  public void setErrId(long errId)
  {
    this.errId = errId;
  }
  public long getErrId()
  {
    return errId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setFileId(long fileId)
  {
    this.fileId = fileId;
  }
  public long getFileId()
  {
    return fileId;
  }

  public void setLineNum(int lineNum)
  {
    this.lineNum = lineNum;
  }
  public int getLineNum()
  {
    return lineNum;
  }

  public void setLinePos(int linePos)
  {
    this.linePos = linePos;
  }
  public int getLinePos()
  {
    return linePos;
  }

  public void setErrorMsg(String errorMsg)
  {
    this.errorMsg = errorMsg;
  }
  public String getErrorMsg()
  {
    return errorMsg;
  }

  public void setSeverity(int severity)
  {
    this.severity = severity;
  }
  public int getSeverity()
  {
    return severity;
  }
  public boolean isFatal()
  {
    return severity == FATAL;
  }
  public boolean isWarning()
  {
    return severity == WARNING;
  }

  public String getSeverityDescription()
  {
    if (severity == WARNING)
    { 
      return "Warning";
    }
    else if (severity == ERROR)
    {
      return "Error";
    }
    else if (severity == FATAL)
    {
      return "Fatal";
    }
    return "Unknown";
  }
  
  public String toString()
  {
    return "ProcessError [ "
      + "error: " + errorMsg
      + ", lineNum: " + lineNum
      + ", linepos: " + linePos
      + ", severity: " + getSeverityDescription()
      + " ]";
  }
}
