package com.mes.aus;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AusBase implements Serializable
{
  public static final String FLAG_YES   = "y";
  public static final String FLAG_NO    = "n";

  public static DateFormat defaultDf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma'</nobr>'");

  protected DateFormat df = defaultDf;

  public void setDf(DateFormat df)
  {
    this.df = df;
  }
  public DateFormat getDf()
  {
    return df;
  }

  public String formatDate(Date date)
  {
    return date != null ? df.format(date) : "--";
  }

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }

  public static void validateFlag(String flag, String val1, String val2, 
    String descriptor)
  {
    if (!flag.equals(val1) &&!flag.equals(val2))
    {
      throw new RuntimeException("Invalid " + descriptor 
        + " flag value '" +  flag + "'" 
        + ", must be '" + val1 + "' or '" + val2 + "'");
    }
  }
  public static void validateFlag(String flag, String descriptor)
  {
    validateFlag(flag,FLAG_YES,FLAG_NO,descriptor);
  }
  protected String flagValue(String flag)
  {
    return flag != null ? flag : FLAG_NO;
  }
  protected boolean flagBoolean(String flag)
  {
    return flag != null && flag.equals(FLAG_YES);
  }
}