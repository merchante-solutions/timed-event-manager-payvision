package com.mes.aus.file;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusDb;
import com.mes.aus.ProcessError;

public class ErrorManager {
	static Logger log = Logger.getLogger(ErrorManager.class);
	private AusDb _db;
	private List errors;
	private boolean fatalFlag;

	public ErrorManager() {
		resetErrors();
	}
	public ErrorManager(AusDb db) {
		_db = db;
		resetErrors();
	}
	private AusDb db() {
		if (_db == null) {
			throw new NullPointerException("Null AusDb");
		}
		return _db;
	}
	public void setDb(AusDb db) {
		_db = db;
	}
	public AusDb getDb() {
		return _db;
	}
	public void addError(ProcessError error) {
		errors.add(error);
		if (error.isFatal()) {
			fatalFlag = true;
		}
	}
	public int getErrorCount() {
		return errors.size();
	}
	public boolean hasError() {
		return getErrorCount() > 0;
	}
	public boolean hasFatalError() {
		return fatalFlag;
	}
	public ProcessError getError(int errorNum) {
		ProcessError error = null;
		if (errorNum < errors.size()) {
			error = (ProcessError) errors.get(errorNum);
		}
		return error;
	}
	public List getErrors() {
		return errors;
	}
	public void resetErrors() {
		errors = new ArrayList();
		fatalFlag = false;
	}
	public void logErrors() {
		if (hasError()) {
			db().insertProcessErrors(errors);
		}
	}
}