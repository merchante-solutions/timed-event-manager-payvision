package com.mes.aus.file;

import org.apache.log4j.Logger;

public class VauFileUtil {
	static Logger log = Logger.getLogger(VauFileUtil.class);
	/**
	 * Converts line data into trimmed array of field strings.
	 */
	public static String[] parseReportLine(String line) {
		return parseReportLine(line, true);
	}
	public static String[] parseReportLine(String line, boolean trimFlag) {
		String[] fields = line.split("[|]");
		for (int i = 0; i < fields.length; ++i) {
			if (trimFlag) {
				fields[i] = fields[i].trim();
			}
		}
		return fields;
	}
}