package com.mes.aus.file.job;

import com.mes.aus.file.AutomatedProcess;

public abstract class Job extends AutomatedProcess implements Runnable {
	public Job(boolean directFlag) {
		super(directFlag);
	}
}