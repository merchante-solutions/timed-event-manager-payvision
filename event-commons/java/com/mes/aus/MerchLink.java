/**
 * Represents a link between an AUS user and a merchant account that
 * allows AUS inquiries files to be processed under the user for the
 * merchant account.
 */
package com.mes.aus;

import org.apache.log4j.Logger;

public class MerchLink extends AusBase
{
  static Logger log = Logger.getLogger(MerchLink.class);

  private long linkId;
  private long ausrId;
  private String ausrName;
  private String merchNum;
  private String merchName;

  public void setLinkId(long linkId)
  {
    this.linkId = linkId;
  }
  public long getLinkId()
  {
    return linkId;
  }

  public void setAusrId(long ausrId)
  {
    this.ausrId = ausrId;
  }
  public long getAusrId()
  {
    return ausrId;
  }

  public void setAusrName(String ausrName)
  {
    this.ausrName = ausrName;
  }
  public String getAusrName()
  {
    return ausrName;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public String toString()
  {
    return "MerchLink [ "
      + "linkId: " + linkId
      + ", ausrId: " + ausrId
      + ", ausrName: " + ausrName
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + " ]";
  }
}