package com.mes.aus;

import java.util.List;

public interface ResponseRecordHandler
{
  public void handleResponses(List responses) throws Exception;
}