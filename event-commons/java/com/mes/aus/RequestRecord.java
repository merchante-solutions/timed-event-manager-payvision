package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class RequestRecord extends AusBase
{
  static Logger log = Logger.getLogger(RequestRecord.class);

  private long reqId;
  private long reqfId;
  private Date createDate;
  private String merchNum;
  private String discData;
  private String cardId;

  private AusAccount acct = new AusAccount();

  public AusAccount getAcct()
  {
    return acct;
  }

  public void setAccountNum(String accountNum)
  {
    acct.setData(accountNum);
  }
  public String getAccountNum()
  {
    return acct.clearText();
  }

  public void setAccountNumEnc(byte[] accountNumEnc)
  {
    acct.setEncodedData(accountNumEnc);
  }
  public byte[] getAccountNumEnc()
  {
    return acct.getEncodedData();
  }

  public String getAccountNumTrunc()
  {
    return acct.truncated();
  }

  public void setReqId(long reqId)
  {
    this.reqId = reqId;
  }
  public long getReqId()
  {
    return reqId;
  }

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setAccountType(String accountType)
  {
    acct.setAccountType(accountType);
  }
  public String getAccountType()
  {
    return acct.getAccountType();
  }

  public void setExpDate(String expDate)
  {
    acct.setExpDate(expDate);
  }
  public String getExpDate()
  {
    return acct.getExpDate();
  }

  public void setDiscData(String discData)
  {
    this.discData = discData;
  }
  public String getDiscData()
  {
    return discData;
  }

  public void setCardId(String cardId)
  {
    this.cardId = cardId;
  }
  public String getCardId()
  {
    return cardId;
  }
  public boolean hasCardId()
  {
    return cardId != null;
  }

  public String toString()
  {
    return "RequestRecord [ "
      + "reqId: " + reqId
      + ", reqfId: " + reqfId
      + ", createDate: " + createDate
      + ", merchNum: " + merchNum
      + ", disc. data: " + (discData != null ? discData : "--")
      + ", accountType: " + getAccountType()
      + ", accountNum: " + getAccountNumTrunc()
      + ", expDate: " + getExpDate()
      + ", cardId: " + (cardId != null ? cardId : "--")
      + " ]";
  }
}