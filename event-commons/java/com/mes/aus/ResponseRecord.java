package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class ResponseRecord extends AusBase
{
  static Logger log = Logger.getLogger(ResponseRecord.class);

  private long rspId;
  private Date createDate;
  private long rspfId;
  private long ibId;
  private long obId;
  private long reqId;
  private long reqfId;
  private String sysCode;
  private String merchNum;
  private String rspCode;
  private AusAccount oldAcct = new AusAccount();
  private AusAccount newAcct = new AusAccount();
  private String discData;
  private String testFlag = FLAG_NO;
  private String cardId;

  public void setRspId(long rspId)
  {
    this.rspId = rspId;
  }
  public long getRspId()
  {
    return rspId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setRspfId(long rspfId)
  {
    this.rspfId = rspfId;
  }
  public long getRspfId()
  {
    return rspfId;
  }

  public void setIbId(long ibId)
  {
    this.ibId = ibId;
  }
  public long getIbId()
  {
    return ibId;
  }

  public void setObId(long obId)
  {
    this.obId = obId;
  }
  public long getObId()
  {
    return obId;
  }

  public void setReqId(long reqId)
  {
    this.reqId = reqId;
  }
  public long getReqId()
  {
    return reqId;
  }

  public void setSysCode(String sysCode)
  {
    this.sysCode = sysCode;
  }
  public String getSysCode()
  {
    return sysCode;
  }

  public void setRspCode(String rspCode)
  {
    this.rspCode = rspCode;
  }
  public String getRspCode()
  {
    return rspCode;
  }

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setOldAcctType(String oldAcctType)
  {
    oldAcct.setAccountType(oldAcctType);
  }
  public String getOldAcctType()
  {
    return oldAcct.getAccountType();
  }

  public void setOldExpDate(String oldExpDate)
  {
    oldAcct.setExpDate(oldExpDate);
  }
  public String getOldExpDate()
  {
    return oldAcct.getExpDate();
  }

  public void setOldAcctNum(String accountNum)
  {
    oldAcct.setData(accountNum);
  }
  public String getOldAcctNum()
  {
    return oldAcct.clearText();
  }

  public void setOldAcctNumEnc(byte[] accountNumEnc)
  {
    oldAcct.setEncodedData(accountNumEnc);
  }
  public byte[] getOldAcctNumEnc()
  {
    return oldAcct.getEncodedData();
  }

  public String getOldAcctNumTrunc()
  {
    return oldAcct.truncated();
  }

  public void setNewAcctType(String newAcctType)
  {
    newAcct.setAccountType(newAcctType);
  }
  public String getNewAcctType()
  {
    return newAcct.getAccountType();
  }

  public void setNewAcctNum(String accountNum)
  {
    newAcct.setData(accountNum);
  }
  public String getNewAcctNum()
  {
    return newAcct.clearText();
  }

  public void setNewAcctNumEnc(byte[] accountNumEnc)
  {
    newAcct.setEncodedData(accountNumEnc);
  }
  public byte[] getNewAcctNumEnc()
  {
    return newAcct.getEncodedData();
  }

  public String getNewAcctNumTrunc()
  {
    return newAcct.truncated();
  }

  public void setNewExpDate(String newExpDate)
  {
    newAcct.setExpDate(newExpDate);
  }
  public String getNewExpDate()
  {
    return newAcct.getExpDate();
  }

  public void setOldAcct(AusAccount oldAcct)
  {
    this.oldAcct = oldAcct;
  }

  public void setNewAcct(AusAccount newAcct)
  {
    this.newAcct = newAcct;
  }

  public void setDiscData(String discData)
  {
    this.discData = discData;
  }
  public String getDiscData()
  {
    return discData;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setCardId(String cardId)
  {
    this.cardId = cardId;
  }
  public String getCardId()
  {
    return cardId;
  }
  public boolean hasCardId()
  {
    return cardId != null;
  }

  public String toString()
  {
    return "ResponseRecord [ "
      + "rspId: " + rspId
      + ", createDate: " + createDate
      + ", rspfId: " + (rspfId > 0 ? ""+rspfId : "--")
      + ", ibId: " + ibId
      + ", obId: " + obId
      + ", reqId: " + reqId
      + ", reqfId: " + reqfId
      + ", sysCode: " + sysCode
      + ", merchNum: " + merchNum
      + ", rspCode: " + rspCode
      + ", oldAcct: " + oldAcct
      + ", newAcct: " + newAcct
      + ", discData: " + (discData != null ? discData : "--")
      + ", test: " + testFlag
      + ", cardId: " + (cardId != null ? cardId : "--")
      + " ]";
  }
}