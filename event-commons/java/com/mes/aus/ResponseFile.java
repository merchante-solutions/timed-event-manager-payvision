package com.mes.aus;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class ResponseFile extends AusFileContainer
{
  static Logger log = Logger.getLogger(ResponseFile.class);

  private long    rspfId;
  private Date    createDate;
  private long    reqfId;
  private long    ibId;
  private int     seqNum;
  private String  ausrName;
  private String  merchNum;
  private Date    dlDate;
  private int     dlNum;
  private String  dlUser;
  private String  fileName;
  private int     fileSize;
  private String  testFlag;

  public void setRspfId(long rspfId)
  {
    this.rspfId = rspfId;
  }
  public long getRspfId()
  {
    return rspfId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setReqfId(long reqfId)
  {
    this.reqfId = reqfId;
  }
  public long getReqfId()
  {
    return reqfId;
  }

  public void setIbId(long ibId)
  {
    this.ibId = ibId;
  }
  public long getIbId()
  {
    return ibId;
  }

  public void setSeqNum(int seqNum)
  {
    this.seqNum = seqNum;
  }
  public int getSeqNum()
  {
    return seqNum;
  }

  public void setAusrName(String  ausrName)
  {
    this.ausrName = ausrName;
  }
  public String  getAusrName()
  {
    return ausrName;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String  getMerchNum()
  {
    return merchNum;
  }

  public void setDlDate(Date dlDate)
  {
    this.dlDate = dlDate;
  }
  public Date getDlDate()
  {
    return dlDate;
  }
  public void setDlTs(Timestamp dlTs)
  {
    dlDate = toDate(dlTs);
  }
  public Timestamp getDlTs()
  {
    return toTimestamp(dlDate);
  }

  public void setDlUser(String dlUser)
  {
    this.dlUser = dlUser;
  }
  public String getDlUser()
  {
    return dlUser;
  }

  public void setDlNum(int dlNum)
  {
    this.dlNum = dlNum;
  }
  public int getDlNum()
  {
    return dlNum;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  public String getFileName()
  {
    return fileName;
  }

  public void setFileSize(int fileSize)
  {
    this.fileSize = fileSize;
  }
  public int getFileSize()
  {
    return fileSize;
  }

  public boolean isNew()
  {
    return dlNum == 0;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return flagValue(testFlag);
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public String toString()
  {
    return " ResponseFile [ "
      + "rspfId: " + rspfId
      + ", createDate: " + createDate
      + ", reqfId: " + reqfId
      + ", ibId: " + ibId
      + ", seqNum: " + seqNum
      + ", ausrName: " + ausrName
      + ", merchNum: " + merchNum
      + ", dlNum: " + dlNum
      + ", dlDate: " + dlDate
      + ", dlUser: " + dlUser
      + ", fileName: " + fileName
      + ", fileSize: " + fileSize
      + ", data present: " + !empty()
      + ", testFlag: " + testFlag
      + " ]";
  }
}
