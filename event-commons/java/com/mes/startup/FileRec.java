package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;

public class FileRec {
	private String loadFilename = "";
	private Date batchDate = null;

	public FileRec(ResultSet rs) throws java.sql.SQLException {
		loadFilename = rs.getString("load_filename");
		batchDate = rs.getDate("batch_date");
	}

	public FileRec(String loadFilename, Date batchDate) {
		this.loadFilename = loadFilename;
		this.batchDate = batchDate;
	}

	public String getLoadFilename() {
		return loadFilename;
	}
	
	public Date getBatchDate() {
		return batchDate;
	}

	@Override
	public String toString() {
		return String.format("FileRec [loadFilename=%s, batchDate=%s]", loadFilename, batchDate);
	}
	
}
