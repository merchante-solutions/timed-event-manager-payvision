/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TsysFileBase.java $

  Description:

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/26/04 4:47p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.sql.Date;
import java.util.Calendar;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;

public class TsysFileBase extends EventBase
{
  protected double            BatchCreditsAmount    = 0.0;
  protected int               BatchCreditsCount     = 0;
  protected double            BatchDebitsAmount     = 0.0;
  protected int               BatchDebitsCount      = 0;
  protected double            BatchPaymentsAmount   = 0.0;
  protected int               BatchRecordCount      = 0;
  protected int               EmailGroupSuccess     = MesEmails.MSG_ADDRS_OUTGOING_CB_NOTIFY;
  protected int               EmailGroupFailure     = MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE;
  
  protected Date              FileActivityDate      = null;
  protected int               FileBankNumber        = mesConstants.BANK_ID_MES;
  protected double            FileCreditsAmount     = 0.0;
  protected int               FileCreditsCount      = 0;
  protected double            FileDebitsAmount      = 0.0;
  protected int               FileDebitsCount       = 0;
  protected double            FilePaymentsAmount    = 0.0;
  protected int               FileRecordCount       = 0;
  protected String            TestFilename          = null;
  
  public TsysFileBase( )
  {
  }
  
  protected synchronized void buildTransmissionHeader( BufferedWriter out, String transName, String transDesc )
  {
    FlatFileRecord    ffd               = null;
    
    try
    {
      // build the transmission header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_TRANS_HDR);
      ffd.connect(true);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("tran_number","1");
      ffd.setFieldData("bank_number",FileBankNumber);
      ffd.setFieldData("trans_date",Calendar.getInstance().getTime());
      ffd.setFieldData("trans_name", transName);
      ffd.setFieldData("trans_desc_code", transDesc);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildTransmissionHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildTransmissionTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // set record count and total
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_TRANS_TRL);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("bank_number", FileBankNumber);
      ffd.setFieldData("trans_number", "1");
      ffd.setFieldData("debits_amount", FileDebitsAmount);
      ffd.setFieldData("credits_amount", FileCreditsAmount);
      ffd.setFieldData("payments_amount", FilePaymentsAmount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildTransmissionTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  protected boolean inTestMode()
  {
    return( TestFilename != null );
  }
  
  protected boolean sendFile( String dataFilename, String host, String user, String pw )
  {
    return( sendFile(dataFilename,host,user,pw,true) );
  }
  
  protected boolean sendFile( String dataFilename, String host, String user, String pw, boolean archiveFile )
  {
    boolean       success         = false;
    StringBuffer  status          = new StringBuffer();
    
    try
    {    
      try
      {
        // first send the data file to the specified host
        success = sendDataFile( dataFilename, host, user, pw );
        
        if ( success == true )
        {
          if ( archiveFile == true )
          {
            success = archiveDailyFile(dataFilename);
          }            
          
          if ( success == true )
          {
            status.append("[");
            status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
            status.append("] Successfully processed TSYS outgoing file: ");
            status.append(dataFilename);
            status.append("\n\n");
            status.append("File Statistics\n");
            status.append("================================================\n");
            status.append("Record Count  : " + FileRecordCount);
            status.append("\n");
            status.append("Debits Count  : " + FileDebitsCount );
            status.append("\n");
            status.append("Debits Amount : " + MesMath.toCurrency(FileDebitsAmount) );
            status.append("\n");
            status.append("Credits Count : " + FileCreditsCount );
            status.append("\n");
            status.append("Credits Amount: " + MesMath.toCurrency(FileCreditsAmount) );
            status.append("\n");
            status.append("Net Amount    : " + MesMath.toCurrency(FileDebitsAmount - FileCreditsAmount) );
            status.append("\n\n");
          }
          else  // data file sent, but archive failed
          {
            status.append("[");
            status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
            status.append("] Successfully processed but failed to archive TSYS outgoing file: ");
            status.append(dataFilename);
          }
        }   
        else    // failed to send the data file
        {
            status.append("[");
            status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
            status.append("] Failed to send '");
            status.append(dataFilename);
            status.append("' to host ");
            status.append(host);
        }
      }
      catch(Exception ftpe)
      {
        logEntry("sendFile(" + dataFilename + ")", ftpe.toString());
        status.append(ftpe.toString());
      }
      
      // send a status email
      sendStatusEmail(success, dataFilename, status.toString() );
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }    
  
  protected void sendStatusEmail(boolean success, String fileName, String message )
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      // post the file totals for the systems group to use for confirmation
      postFileTotals( fileName,"vital",
                      FileRecordCount,
                      0,
                      FileDebitsCount,
                      FileDebitsAmount,
                      FileCreditsCount,
                      FileCreditsAmount );
    
      if(success)
      {
        subject.append("TSYS Outgoing File Success - " + fileName);
      }
      else
      {
        subject.append("TSYS Outgoing File Error - " + fileName);
        
        // make sure the filename is in 
        // the message text for error research
        body.append("Filename: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses( EmailGroupSuccess );
      }
      else
      {
        msg.setAddresses( EmailGroupFailure );
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  protected void setTestFilename( String tfilename )
  {
    TestFilename = tfilename;
  }
}