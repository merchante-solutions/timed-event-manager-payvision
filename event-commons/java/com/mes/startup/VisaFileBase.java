/*@lineinfo:filename=VisaFileBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaFileBase.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.List;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.InterchangeVisa;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordVisa;
import com.mes.settlement.SettlementTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.TridentTools;
import com.mes.tools.CurrencyCodeConverter;
import sqlj.runtime.ResultSetIterator;

public abstract class VisaFileBase extends SettlementFileBase
{
  public VisaFileBase( )
  {
  }
  
  public void buildBatchTrailer( BufferedWriter out )
  {
    FlatFileRecord    ffd             = null;
      
    try
    {
      ++BatchTranCount;   // tran count includes trailers
      ++BatchRecordCount; // rec count includes trailers
      ++FileBatchCount;
      ++FileRecordCount;
      ++FileTranCount;
    
      // create the batch trailer record
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_VISA_TC91_TCR0);

      // merchant registration TC50s do not count as financial transactions
      if ( SettlementRecMT != SettlementRecord.MT_VISA_MPS )
      {
        ffd.setFieldData("monetary_tran_count",(BatchDebitsCount + BatchCreditsCount));
      }
      //@ffd.setFieldData("batch_number",);
      ffd.setFieldData("number_of_tcrs",BatchRecordCount);
      ffd.setFieldData("center_batch_id",FileBatchId);
      ffd.setFieldData("tran_count",BatchTranCount);
      ffd.setFieldData("source_amount",TridentTools.encodeAmount((BatchDebitsAmount + BatchCreditsAmount),15));
      out.write( ffd.spew() );
      out.newLine();
      
      // reset batch totals
      BatchRecordCount    = 0;
      BatchCreditsCount   = 0;
      BatchCreditsAmount  = 0.0;
      BatchDebitsCount    = 0;
      BatchDebitsAmount   = 0.0;
      BatchTranCount      = 0;
    }
    catch(Exception e)
    {
      logEntry("buildBatchTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildDetailRecords( BufferedWriter out, SettlementRecord rec, boolean reversal )
  {
    long                  batchId         = 0L;
    long                  batchRecId      = 0L;
    FlatFileRecord        ffd             = null;
    List                  ffds            = null;
    int                   retryCount      = 0;
    
    try
    {
      // store these in case of error
      batchId     = rec.getLong("batch_id");
      batchRecId  = rec.getLong("batch_record_id");
      
      // disable line item detail on reversals
      if ( reversal )   
      {
        rec.setData("level_iii_data_present","N"); 
      }    
      
      while(true)
      {
        try
        {
          ffds = rec.buildNetworkRecords(SettlementRecMT);
          break;
        }
        catch( Exception e )
        {
          if ( ++retryCount > 10 )
          {
            throw(e);
          }
        }
      }
      
      if ( (BatchRecordCount + ffds.size()) >= 999 )
      {
        buildBatchTrailer(out);
      }
      
      log.debug("flatFileCount: " + ffds.size());
      for( int i = 0; i < ffds.size(); ++i )
      {
        ffd = (FlatFileRecord)ffds.get(i);
        
        // each financial TC50 record needs to be counted as a new transaction
        if( SettlementRecMT != SettlementRecord.MT_VISA_MPS &&
            "50".equals(ffd.getFieldData("tran_code")) &&
             "0".equals(ffd.getFieldData("tran_component_seq_num")) )
        {
          BatchTranCount      ++;
          FileTranCount       ++;
        }
        else if ( reversal )
        {
          char[] tc = ffd.getFieldData("tran_code").toCharArray();
          tc[0] = '2';           // change tranCode "0x" to "2x"
          ffd.setFieldData("tran_code",String.valueOf(tc));
          Date originalCpd = getOriginalCpd(rec.getLong("rec_id"));
          ffd.setFieldData("central_proc_date", DateTimeFormatter.getFormattedDate(originalCpd,"yyDDD").substring(1));
        }
        out.write(ffd.spew());
        out.newLine();
      }
      
      int decDigits = CurrencyCodeConverter.getFractionDigits(rec.getString("currency_code","840"));
      // increment the counters and update the amounts
      double tranAmount = Double.parseDouble(MesMath.toFractionalAmount(rec.getDouble("transaction_amount"),decDigits,decDigits,false));

      BatchTranCount      ++;
      FileTranCount       ++;
      BatchRecordCount    += ffds.size();
      FileRecordCount     += ffds.size();
      
      if ( "C".equals(rec.getString("debit_credit_indicator")) )
      {
        BatchCreditsCount   ++;
        FileCreditsCount    ++;
        BatchCreditsAmount  += tranAmount;
        FileCreditsAmount   += tranAmount;
      }
      else
      {
        BatchDebitsCount    ++;
        FileDebitsCount     ++;
        BatchDebitsAmount   += tranAmount;
        FileDebitsAmount    += tranAmount;
      }
    }
    catch(Exception e)
    {
      logEntry("buildDetailRecords(" + batchId + "," + batchRecId + ")", e.toString());
    }
    finally
    {
    }
  }
  
  public void buildFileTrailer( BufferedWriter out )
  {
    FlatFileRecord    ffd             = null;
      
    try
    {
      ++FileRecordCount;  // include file trailer record in counts
      ++FileTranCount;    // 
      
      // create the file trailer record
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_VISA_TC92_TCR0);
      
      // merchant registration TC50s do not count as financial transactions
      if ( SettlementRecMT != SettlementRecord.MT_VISA_MPS )
      {
        ffd.setFieldData("monetary_tran_count",(FileDebitsCount + FileCreditsCount));
      }        
      //@ffd.setFieldData("batch_number",);
      ffd.setFieldData("number_of_tcrs",FileRecordCount);
      ffd.setFieldData("tran_count",FileTranCount);
      ffd.setFieldData("source_amount",TridentTools.encodeAmount((FileDebitsAmount + FileCreditsAmount),15));
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  protected void closeFile( BufferedWriter out )
    throws java.io.IOException
  {
    buildBatchTrailer(out);   // end the last batch
    buildFileTrailer(out);    // file trailer

    out.close();
  }
  
  public Date getSettlementDate()
  {
    return( SettlementTools.getSettlementDateVisa() );
  }
  
  protected void handleSelectedRecords(ResultSetIterator it, String workFilename) throws Exception {
	  handleSelectedRecords(it, workFilename, 0L);
	 }

  protected void handleSelectedRecords( ResultSetIterator it, String workFilename , Long workFileId)
    throws Exception
  {
    SettlementDb              db                = new SettlementDb();
    BufferedWriter            out               = null;
    SettlementRecordVisa      rec               = new SettlementRecordVisa();
    ResultSet                 resultSet         = null;
    HashSet<String>           dtprocessList     = new HashSet<>();

    try
    {
      db.connect(true);

      resultSet = it.getResultSet();

      log.debug("Work Filename : " + workFilename);

      if( resultSet.next() )
      {
        out = new BufferedWriter( new FileWriter( workFilename, false ) );

        int recCount = 0;
        do
        {
          if ( transactionToSkip(resultSet) )continue;

          rec.clear();
          rec.setIsEmvEnabled(EmvProperties.containsKey("visa") && EmvProperties.get("visa").equals("yes") ? true : false);
          rec.setFields(resultSet, false, false);
          //Decrypt the card number
    	  try {
      	    rec.getField("card_number_full").setData( rec.getCardNumberFull() );
    	  } catch (Exception ex) {
    		log.error(ex);
          }
          if ( rec.hasLevelIIIData() )
          {
            if( ActionCode == null || "X".equals(ActionCode) )
            {
              rec.setLineItems( db._loadVisaSettlementLineItemDetailRecords(rec.getLong("rec_id")) );
            }
          }
          
          // check interchange on re-procs
          if( "X".equals(ActionCode) )    
          {
            if ( ! rec.isCashDisbursement() )
            {
              String  icCodeOriginal  = rec.getString("ic_cat");
              new InterchangeVisa().getBestClass(rec,SettlementTools.getClassListVisa(rec));
              if ( icCodeOriginal == null || !icCodeOriginal.equals( rec.getString("ic_cat") ) )
              {
                rec.setData("ic_cat_downgrade",rec.getString("ic_cat"));
                SettlementDb.updateIcCatDowngrade(rec);
              }
              if ("N".equals(rec.getData("external_reject"))) {
              	  rec.setData("ach_flag", "Y");
              	  rec.setData("load_filename", workFilename);
              	  rec.setData("load_file_id", workFileId);
                  SettlementDb.updateDTRecord(rec);
                  dtprocessList.add(DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"), "dd-MMM-yyyy"));
              }
            }
          }
          
          buildDetailRecords( out, rec, transactionToReverse(resultSet) );

          log.debug("Processed: " + String.valueOf(++recCount));
        } while( resultSet.next() );
        
        for (String batchdate : dtprocessList) {
             SettlementDb.insertDTProcessRecord(1, workFilename, batchdate);
        }
        
        log.debug("\n\n");
        
        // write the final batch and file trailers, close file handle
        closeFile(out);
        sendFile(workFilename);
      }
      
      else
      {
        log.debug("No records selected.\n");
      }

      resultSet.close();
    }
    finally
    {
      try{ db.cleanUp(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/