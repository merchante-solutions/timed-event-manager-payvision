/*@lineinfo:filename=ProcessTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/ProcessTable.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-02-05 15:58:38 -0800 (Tue, 05 Feb 2013) $
  Version            : $Revision: 20894 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ProcessTable extends SQLJConnectionBase
{
  public static final int           PT_ALL      = -1;
  
  public class ProcessTableEntry
  {
    protected     Date      BatchDate     = null;
    protected     String    LoadFilename  = null;
    protected     int       ProcessType   = -1;
    protected     long      RecId         = -1L;
    protected     long      NodeId        = 0L;
    
    public ProcessTableEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RecId         = resultSet.getLong("rec_id");
      ProcessType   = resultSet.getInt("process_type");
      
      // optional params
      try{ BatchDate      = resultSet.getDate("batch_date");      } catch( java.sql.SQLException sqle ) {}
      try{ LoadFilename   = resultSet.getString("load_filename"); } catch( java.sql.SQLException sqle ) {}
      try{ NodeId         = resultSet.getLong("hierarchy_node");         } catch( java.sql.SQLException sqle ) {}
    }
    
    public Date getBatchDate()
    {
      return( BatchDate );
    }
    
    public String getLoadFilename()
    {
      return( LoadFilename );
    }
    
    public long getNodeId()
    {
      return( NodeId );
    }
    
    public int getProcessType()
    {
      return( ProcessType );
    }
    
    public long getRecId()
    {
      return( RecId );
    }
    
    public boolean isValid()
      throws java.sql.SQLException
    {
      boolean     retVal      = false;
      
      try
      {
        connect();
      
        int temp;
        /*@lineinfo:generated-code*//*@lineinfo:95^9*/

//  ************************************************************
//  #sql [Ctx] { select  process_sequence 
//            from    :TableName
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  process_sequence  \n          from     ");
   __sjT_sb.append(TableName);
   __sjT_sb.append(" \n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.ProcessTable:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,RecId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   temp = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^9*/
        retVal = true;
      }
      finally
      {
        cleanUp();
      }
      return( retVal );
    }
    
    public void recordTimestampBegin( )
    {
      Timestamp     beginTime   = null;

      try
      {
        connect();
        beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
        /*@lineinfo:generated-code*//*@lineinfo:119^9*/

//  ************************************************************
//  #sql [Ctx] { update  :TableName
//            set     process_begin_date = :beginTime
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(TableName);
   __sjT_sb.append(" \n          set     process_begin_date =  ? \n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.startup.ProcessTable:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,RecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampBegin(" + TableName + "," + RecId + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }

    public void recordTimestampEnd( )
    {
      Timestamp     beginTime   = null;
      long          elapsed     = 0L;
      Timestamp     endTime     = null;

      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:146^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct least(process_begin_date) 
//            from    :TableName
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  distinct least(process_begin_date)  \n          from     ");
   __sjT_sb.append(TableName);
   __sjT_sb.append(" \n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.startup.ProcessTable:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,RecId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^9*/
        endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
        elapsed = (endTime.getTime() - beginTime.getTime());

        /*@lineinfo:generated-code*//*@lineinfo:155^9*/

//  ************************************************************
//  #sql [Ctx] { update  :TableName
//            set     process_end_date = :endTime,
//                    process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4520 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(TableName);
   __sjT_sb.append(" \n          set     process_end_date =  ? ,\n                  process_elapsed =  ? \n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.startup.ProcessTable:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4520);
   __sJT_st.setLong(3,RecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampEnd(" + TableName + "," + RecId + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    public void reset()
    {
      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:179^9*/

//  ************************************************************
//  #sql [Ctx] { update  :TableName
//            set     process_end_date = null,
//                    process_elapsed = null,
//                    process_begin_date = null,
//                    process_sequence = 0
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(TableName);
   __sjT_sb.append(" \n          set     process_end_date = null,\n                  process_elapsed = null,\n                  process_begin_date = null,\n                  process_sequence = 0\n          where   rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.startup.ProcessTable:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,RecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^9*/
      }
      catch(Exception e)
      {
        logEntry("reset(" + TableName + "," + RecId + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }

  protected     boolean             NodeIdSupported = false;
  protected     int                 ProcessType     = PT_ALL;
  protected     String              SequenceName    = null;
  protected     String              TableName       = null;
  protected     Vector              TableEntries    = new Vector();
  
  public ProcessTable( String tableName, String sequenceName, int processType )
  {
    init(tableName,sequenceName,processType,false);
  }
    
  public ProcessTable( String tableName, String sequenceName, int processType, boolean nodeIdSupported )
  {
    init(tableName,sequenceName,processType,nodeIdSupported);
  }
  
  public boolean hasPendingEntries()
  {
    int   recCount  = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    :TableName    pt
//          where   pt.process_sequence = 0
//                  and :ProcessType in ( :PT_ALL, pt.process_type )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(1)  \n        from     ");
   __sjT_sb.append(TableName);
   __sjT_sb.append("     pt\n        where   pt.process_sequence = 0\n                and  ?  in (  ? , pt.process_type )");
   String __sjT_sql = __sjT_sb.toString();
   if ( log.isDebugEnabled()) {
	   log.debug("[hasPendingEntries()] - ProcessType = "+ProcessType+", SQL - "+__sjT_sql);
   }
   String __sjT_tag = "5com.mes.startup.ProcessTable:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,ProcessType);
   __sJT_st.setInt(2,PT_ALL);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
    }
    catch( Exception e )
    {
      logEntry("hasPendingEntries()",e.toString());
    }
    finally
    {
      cleanUp();
    }      
      
    return( recCount > 0 );      
  }
  
  protected void init( String tableName, String sequenceName, int processType, boolean nodeIdSupported )
  {
    TableName       = tableName;
    SequenceName    = sequenceName;
    ProcessType     = processType;
    NodeIdSupported = nodeIdSupported;
  }
  
  public int preparePendingEntries()
  {
    int                 recCount    = 0;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    long                seqNum      = -1L;
    
    try
    {
      connect();
      
      TableEntries.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:265^7*/

//  ************************************************************
//  #sql [Ctx] { select  :SequenceName.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select   ");
   __sjT_sb.append(SequenceName);
   __sjT_sb.append(" .nextval\n         \n        from    dual");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.startup.ProcessTable:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] { update  :TableName pt
//          set     pt.process_sequence = :seqNum
//          where   pt.process_sequence = 0
//                  and :ProcessType in ( :PT_ALL, pt.process_type )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update   ");
   __sjT_sb.append(TableName);
   __sjT_sb.append("  pt\n        set     pt.process_sequence =  ? \n        where   pt.process_sequence = 0\n                and  ?  in (  ? , pt.process_type )");
   String __sjT_sql = __sjT_sb.toString();
   if ( log.isDebugEnabled()) {
	   log.debug("[preparePendingEntries()] - ProcessType="+ProcessType+", SQL = "+__sjT_sql);
   }
   String __sjT_tag = "7com.mes.startup.ProcessTable:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
   __sJT_st.setInt(2,ProcessType);
   __sJT_st.setInt(3,PT_ALL);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:278^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:280^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pt.*
//          from    :TableName pt
//          where   pt.process_sequence = :seqNum                  
//          order by pt.process_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  pt.*\n        from     ");
   __sjT_sb.append(TableName);
   __sjT_sb.append("  pt\n        where   pt.process_sequence =  ?                   \n        order by pt.process_type");
   String __sjT_sql = __sjT_sb.toString();
   if ( log.isDebugEnabled()) {
	   log.debug("[preparePendingEntries()] - seqNum="+seqNum+", SQL="+__sjT_sql);
   }
   String __sjT_tag = "8com.mes.startup.ProcessTable:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        TableEntries.add( new ProcessTableEntry(resultSet) );
        ++recCount;
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("preparePendingEntries()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( recCount );
  }
  
  public Vector getEntryVector()
  {
    return( TableEntries );
  }
}/*@lineinfo:generated-code*/