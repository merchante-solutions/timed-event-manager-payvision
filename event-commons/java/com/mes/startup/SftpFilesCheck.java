package com.mes.startup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;

public class SftpFilesCheck extends EventBase{
	
	static Logger log = Logger.getLogger(SftpFilesCheck.class);

	public static SftpFilesCheck sftpObject() {
		SftpFilesCheck o = new SftpFilesCheck();
		return o;
	}
	
	public ArrayList displayVisaFiles() {
		Sftp sftp = null;
		String remoteFilename = null;
		ArrayList visaList = new ArrayList();
		
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("MMddyy");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 5);
		if (date.toString() == null || date.toString() == ""){
			// throw exception
			System.out.println("No date detail.");
		}

		final String mask = "undiff_"+ month.format(date) + ".." + year.format(date) +"_.*";
		final String fileMask = "undiff_";

		try {
			SQLJConnectionBase.initStandalonePool();
			//connect(true);
			connect();
			//get the Sftp connection
			sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_HOST),
	                  MesDefaults.getString(MesDefaults.DK_VISA_USER),
	                  MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),
	                  MesDefaults.getString(MesDefaults.DK_VISA_INCOMING_PATH)+"/arc",
	                  false   // !binary
	                );
			
			EnumerationIterator enumi = new EnumerationIterator();
			Iterator it = enumi.iterator(sftp.getNameListing(mask));
//			Iterator it = enumi.iterator(sftp.getNameListing());
			while( it.hasNext() )
		      {
				remoteFilename = (String)it.next();
				if (remoteFilename.length()>14){
					if (sftp.getFileTimestamp(remoteFilename).after(cal.getTime())) {
						if (remoteFilename.substring(0, 7).equals(fileMask)){
							visaList.add(remoteFilename.substring(13, 20));
						}
					}
				}
		      }
//			System.out.println(visaList);
		}
		catch( Exception e ) {
			System.out.println(e.toString());
	      //logEntry("loadFiles(" + fileType + "," + remoteFilename + ")",e.toString());
	    }
	    finally {
	      try{ sftp.disconnect(); } catch( Exception ee ) {}
	      cleanUp();
	    }
		
		Collections.sort(visaList);
		
		return visaList;
	}
	 
	
	public ArrayList displayMCFiles() {
		Sftp sftp = null;
		String remoteFilename = null;
		ArrayList MCList = new ArrayList();
		
		SimpleDateFormat df = new SimpleDateFormat("MMddyy");
		SimpleDateFormat mf = new SimpleDateFormat("MM");
		SimpleDateFormat yf = new SimpleDateFormat("yy");
		
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		
		
		if (date.toString() == null || date.toString() == ""){
			// throw exception
			System.out.println("No date detail.");
		}

		final String fileMask = "mc_t1120.*_"+mf.format(date)+".."+yf.format(date)+"_.*";
		final String cyc1 = "mc_t112013941_"+df.format(cal.getTime())+"_";
		final String cyc2 = "mc_t112023941_"+df.format(cal.getTime())+"_";
		final String cyc3 = "mc_t112033941_"+df.format(cal.getTime())+"_";
		final String cyc5 = "mc_t112053941_"+df.format(date)+"_";
		final String cyc6 = "mc_t112063941_"+df.format(date)+"_";

		try {
			SQLJConnectionBase.initStandalonePool();
			//connect(true);
			connect();
			//get the Sftp connection
			sftp = getSftp( MesDefaults.getString(MesDefaults.DK_MC_HOST),
	                  MesDefaults.getString(MesDefaults.DK_MC_USER),
	                  MesDefaults.getString(MesDefaults.DK_MC_PASSWORD),
	                  MesDefaults.getString(MesDefaults.DK_MC_INCOMING_PATH)+"/arc",
	                  false   // !binary
	                );
			
			EnumerationIterator enumi = new EnumerationIterator();
			Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
//			Iterator it = enumi.iterator(sftp.getNameListing());
			while( it.hasNext() )
		      {
				remoteFilename = (String)it.next();
				if (remoteFilename.length()>20){
					
					String rf = remoteFilename.substring(0, 21);
					
					if (rf.equals(cyc1)){
						MCList.add(remoteFilename.substring(20, 27));
					}
					else if (rf.equals(cyc2)){
						MCList.add(remoteFilename.substring(20, 27));
					}
					else if (rf.equals(cyc3)){
						MCList.add(remoteFilename.substring(20, 27));
					}
					else if (rf.equals(cyc5)){
						MCList.add(remoteFilename.substring(20, 27));
					}
					else if (rf.equals(cyc6)){
						MCList.add(remoteFilename.substring(20, 27));
					}
				}
		      }
//			System.out.println(MCList);
		}
		catch( Exception e ) {
			System.out.println(e.toString());
	      //logEntry("loadFiles(" + fileType + "," + remoteFilename + ")",e.toString());
	    }
	    finally {
	      try{ sftp.disconnect(); } catch( Exception ee ) {}
	      cleanUp();
	    }
		
		Collections.sort(MCList);
		
		return MCList;
	}
    public ArrayList displayProcessedFiles() {
        Sftp sftp = null;
        String remoteFilename = null;
        ArrayList filesList = new ArrayList();
        
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("MMddyy");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yy");
        if (date.toString() == null || date.toString().length() == 0){
               // throw exception
               System.out.println("No date detail.");
        }
        
        try {
               SQLJConnectionBase.initStandalonePool();
               //connect(true);
               connect();
               //get the Sftp connection
               sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_EP_HOST),
                   MesDefaults.getString(MesDefaults.DK_VISA_EP_USER),
                   MesDefaults.getString(MesDefaults.DK_VISA_EP_PASSWORD),
                   MesDefaults.getString(MesDefaults.DK_VISA_EP_INCOMING_PATH),
                   false   // !binary
                 );

               //for EP2 testing
//             System.out.println("getSftp");
//             sftp = getSftp( "10.3.4.22",
//                 "jduncan",
//                 "19thHole",
//                 "/settlement/visa/inbound/arc",
//                 false   // !binary
//               );
//             System.out.println("getSftp done");
               
               EnumerationIterator enumi = new EnumerationIterator();
               Iterator it = enumi.iterator(sftp.getNameListing(".*_" + month.format(date) + ".." + year.format(date) + "_.*"));
               String fileMask = "undiff_";
//             Iterator it = enumi.iterator(sftp.getNameListing());
               while( it.hasNext() )
               {
            	   remoteFilename = (String)it.next();
            	   if (df.format(sftp.getFileTimestamp(remoteFilename)).equals(df.format(date))) {
            		   String fileExt = remoteFilename.substring(remoteFilename.length()-11, remoteFilename.length()-4);
            		   if (!filesList.contains(fileExt) && !remoteFilename.substring(0, 7).equals(fileMask)){
            			   filesList.add(fileExt);
            		   }
            	   }
               }
        } 
        catch( Exception e){
               System.out.println(e.toString());
               logEntry("Sftp Exception(" + remoteFilename + ")",e.toString());
        }
//      catch( Exception e ) {
//             System.out.println(e.toString());
//       logEntry("loadFiles(" + remoteFilename + ")",e.toString());
//   }
     finally {
       try{ sftp.disconnect(); } catch( Exception ee ) {}
       cleanUp();
     }
        
        Collections.sort(filesList);
 
        return filesList;
 }


	/*public static void main(String[] args) {
				
		SftpFilesCheck sftpobject = SftpFilesCheck.sftpObject();
		
		try {
			//SQLJConnectionBase.initStandalonePool("DEBUG");
			SQLJConnectionBase.initStandalonePool();
			sftpobject.connect();
			sftpobject.displayFiles();
		}
		catch( Exception e )
	    {
	      //logEvent(this.getClass().getName(), "execute()", e.toString());
	      //logEntry("execute()", e.toString());
			System.out.println("Failed Oracle DB connection. " + e.toString());
	    }
	    finally
	    {
	    	sftpobject.cleanUp();
	    }
		//sftpobject.displayFiles();
	}*/
}
