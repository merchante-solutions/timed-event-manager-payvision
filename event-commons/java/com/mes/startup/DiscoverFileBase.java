/*@lineinfo:filename=DiscoverFileBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DiscoverFileBase.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.List;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.InterchangeDiscover;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordDiscover;
import com.mes.settlement.SettlementTools;
import com.mes.settlement.VisakBatchData;
import com.mes.support.DateTimeFormatter;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public abstract class DiscoverFileBase extends SettlementFileBase
{
  private   long            BatchMerchantId       = 0L;
  protected int             FileProcessorId       = 610069;
  protected String          FileVersionInd        = "";

  public DiscoverFileBase( )
  {
    try {
      FileVersionInd = MesDefaults.getString(MesDefaults.DISCOVER_FILE_VERSION_IND);
    } catch (Exception e) {
      logEntry("DiscoverFileBase() - constructor: set the file version indicator", e.toString());
    }
  }

  private void buildFileHeader( BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;
    
    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_FILE_HEADER_V101);
      if ( TestFilename != null )
      {
        ffd.setFieldData( "file_type"     , "TEST");
      }
      else
      {
        ffd.setFieldData( "file_type"     , "PROD");
      }
      ffd.setFieldData( "processor_id"  , FileProcessorId);
      ffd.setFieldData( "file_date"     , FileTimestamp);
      ffd.setFieldData( "file_time"     , FileTimestamp);
      ffd.setFieldData( "version_ind"   , FileVersionInd);
      ffd.setFieldData( "file_ref_id"   , FileReferenceId);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildFileTrailer( BufferedWriter out )
  {
    FlatFileRecord    ffd             = null;
      
    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_FILE_TRAILER_V101);
      ffd.setFieldData( "file_count"    , FileRecordCount);
      ffd.setFieldData( "file_amount"   , TridentTools.encodeCobolAmount((FileDebitsAmount - FileCreditsAmount),ffd.getFieldLength("file_amount")));
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildBatchHeader( BufferedWriter out, ResultSet resultSet )
  {
    FlatFileRecord    ffd               = null;

    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_BATCH_HEADER_V101);
      ffd.setAllFieldData(resultSet);
      ffd.setFieldData( "file_date"     , FileTimestamp);
      ffd.setFieldData( "file_time"     , FileTimestamp);
      out.write( ffd.spew() );
      out.newLine();

      ++FileBatchCount;

      // reset the batch totals
      BatchCreditsAmount  = 0.0;
      BatchDebitsAmount   = 0.0;
      BatchRecordCount    = 0;
      BatchCreditsCount   = 0;
      BatchDebitsCount    = 0;
    }
    catch(Exception e)
    {
      logEntry("buildBatchHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }

  public void buildBatchTrailer( BufferedWriter out )
  {
    FlatFileRecord    ffd             = null;
      
    try
    {
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_BATCH_TRAILER_V101);
      ffd.setFieldData( "discover_merchant_number", BatchMerchantId);
      ffd.setFieldData( "batch_count"   , BatchRecordCount);
      ffd.setFieldData( "batch_amount"  , TridentTools.encodeCobolAmount((BatchDebitsAmount - BatchCreditsAmount),ffd.getFieldLength("batch_amount")) );
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildBatchTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildDetailRecords( BufferedWriter out, SettlementRecord rec, boolean reversal )
  {
    long                  batchId         = 0L;
    long                  batchRecId      = 0L;
    FlatFileRecord        ffd             = null;
    List                  ffds            = null;
    int                   retryCount      = 0;
    
    try
    {
      // store these in case of error
      batchId     = rec.getLong("batch_id");
      batchRecId  = rec.getLong("batch_record_id");
      
      // if reversal (and non-MAP), invert transaction type so flat file builds correctly
      if( reversal && rec.getString("acquirer_id") == null )
      {
        boolean makeReturn = "D".equals(rec.getData("debit_credit_indicator"));

        rec.setData("process_code"          , makeReturn ?  "20" : "00" );
        rec.setData("tran_type"             , makeReturn ?  "RT" : "SA" );
        rec.setData("debit_credit_indicator", makeReturn ?  "C"  : "D"  );
        rec.setData("client_data"           , "REVERSAL"  );
      }

      while(true)
      {
        try
        {
          ffds = rec.buildNetworkRecords(SettlementRecMT);
          break;
        }
        catch( Exception e )
        {
          if ( ++retryCount > 10 )
          {
            throw(e);
          }
        }
      }
      
      log.debug("flatFileCount: " + ffds.size());
      for( int i = 0; i < ffds.size(); ++i )
      {
        ffd = (FlatFileRecord)ffds.get(i);
        
        if ( reversal && rec.getString("acquirer_id") != null )
        {
          ffd.setFieldData("tran_type",("R" + ffd.getFieldData("tran_type").substring(0,1)));
          ffd.setFieldData("interchange_program","0000");
          rec.setData("tran_type",ffd.getFieldData("tran_type"));
        }
        out.write(ffd.spew());
        out.newLine();
      }
      
      // increment the counters and update the amounts
      double tranAmount = rec.getDouble("transaction_amount");

      BatchTranCount      ++;
      BatchRecordCount    += ffds.size();
      FileRecordCount     += ffds.size();
      
      // returns, reversal sale, reversal cash advance
      if ( "RT,RS,RC".indexOf(rec.getString("tran_type")) >= 0 )
      {
        BatchCreditsCount   ++;
        FileCreditsCount    ++;
        BatchCreditsAmount  += tranAmount;
        FileCreditsAmount   += tranAmount;
      }
      else
      {
        BatchDebitsCount    ++;
        FileDebitsCount     ++;
        BatchDebitsAmount   += tranAmount;
        FileDebitsAmount    += tranAmount;
      }
    }
    catch(Exception e)
    {
      logEntry("buildDetailRecords(" + batchId + "," + batchRecId + ")", e.toString());
    }
    finally
    {
    }
  }

  protected void closeFile( BufferedWriter out )
    throws java.io.IOException
  {
    buildBatchTrailer(out);   // end the last batch
    buildFileTrailer(out);    // file trailer

    out.close();
  }
  
  public Date getSettlementDate()
  {
    return( SettlementTools.getSettlementDateDiscover() );
  }
 
  
  protected void handleSelectedRecords( ResultSetIterator it, String workFilename,long workFileid )
    throws Exception
  {
    SettlementDb              db                = new SettlementDb();
    BufferedWriter            out               = null;
    SettlementRecordDiscover  rec               = new SettlementRecordDiscover();
    ResultSet                 resultSet         = null;
    HashSet<String>           dtprocessList     = new HashSet<>();

    try
    {
      db.connect(true);

      resultSet = it.getResultSet();

      log.debug("Work Filename : " + workFilename);

      if( resultSet.next() )
      {
        long    batchId   = 0L;
        long    merchId   = 0L;
        int     curId     = 0;
        String  posData   = null;

        out = new BufferedWriter( new FileWriter( workFilename, false ) );

        // build the data file header
        buildFileHeader(out);

        int recCount = 0;
        do
        {
          if ( transactionToSkip(resultSet) )continue;

          if( merchId  != resultSet.getLong("merchant_number" ) ||
              batchId  != resultSet.getLong("batch_number"    ) || 
              curId    != resultSet.getLong("currency_code"    ))
          {
            merchId     = resultSet.getLong("merchant_number" );
            batchId     = resultSet.getLong("batch_number"    );
            curId       = resultSet.getInt("currency_code"    ); 
            
            // if have built any details, end current batch
            if( recCount != 0L )
              buildBatchTrailer(out);
            buildBatchHeader(out,resultSet);

            // store the discover account for use in the trailer record
            BatchMerchantId = resultSet.getLong("discover_merchant_number");
          }

          rec.clear();
          rec.setIsEmvEnabled(EmvProperties.containsKey("discover") && EmvProperties.get("discover").equals("yes") ? true : false);
          rec.setFields(resultSet, false, false);

          //Decrypt the card number
    	  try {
    	    rec.getField("card_number_full").setData( rec.getCardNumberFull() );
    	  } catch (Exception ex) {
    		log.error(ex);
          }
//@@ Discover does not have line item details
//          if ( rec.hasLevelIIIData() )
//          {
//            if( ActionCode == null || "X".equals(ActionCode) )
//            {
//              rec.setLineItems( db._loadDiscoverSettlementLineItemDetailRecords(rec.getLong("rec_id")) );
//            }
//          }
          
          // check interchange on re-procs
          if( "X".equals(ActionCode) )    
          {
            posData     = resultSet.getString("pos_data");  
            VisakBatchData        batchData           = new VisakBatchData();
            switch (posData.charAt(4) )
              {
                case '1': // card not present
                    rec.setData("moto_ecommerce_ind","1" );
                    rec.setData("card_present"      ,"N");
                  break;
                case '0': // card present
                    rec.setData("moto_ecommerce_ind","" );
                    rec.setData("card_present"      ,"Y");
                  break;
            }
              
            rec.setFieldsBase(batchData, resultSet, 0);  
            if ( ! rec.isCashDisbursement() )
            {
              String  icCodeOriginal  = rec.getString("ic_cat");
              new InterchangeDiscover().getBestClass(rec,SettlementDb.loadIcpListDiscover(rec));
              if ( icCodeOriginal == null || !icCodeOriginal.equals( rec.getString("ic_cat") ) )
              {
                rec.setData("ic_cat_downgrade",rec.getString("ic_cat"));
                SettlementDb.updateIcCatDowngrade(rec);
              }
              if ("N".equals(rec.getData("external_reject"))) 
              {
                rec.setData("load_file_id", workFileid);
                rec.setData("load_filename", workFilename);
                rec.setData("ach_flag", "Y");
                db.updateDTRecord(rec);
                dtprocessList.add(DateTimeFormatter.getFormattedDate(rec.getDate("batch_date"), "dd-MMM-yyyy"));
              }
            }
          }
          
          buildDetailRecords( out, rec, transactionToReverse(resultSet) );

          log.debug("Processed: " + String.valueOf(++recCount));
        } while( resultSet.next() );
        
        for (String batchdate : dtprocessList) {
             SettlementDb.insertDTProcessRecord(1, workFilename, batchdate);
        }

        log.debug("\n\n");

        // write the final batch and file trailers, close file handle
        closeFile(out);
        sendFile(workFilename);
      }
      else
      {
        log.debug("No records selected.\n");
      }

      resultSet.close();
    }
    finally
    {
      try{ db.cleanUp(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/