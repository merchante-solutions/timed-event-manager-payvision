/*@lineinfo:filename=InsertCreditQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/InsertCreditQueue.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-09-11 11:48:37 -0700 (Fri, 11 Sep 2009) $
  Version            : $Revision: 16467 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.cbt.TierMccQueueOps;
import com.mes.queues.QueueTools;
import com.mes.tools.AppNotifyBean;
import com.mes.tools.ApplicationXDoc;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class InsertCreditQueue extends SQLJConnectionBase
{
  public InsertCreditQueue(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  public InsertCreditQueue()
  {
  }
  
  private boolean appIsAutoApprove(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      String autoApprove = "";
      
      // see if app type of app is auto-approve
      /*@lineinfo:generated-code*//*@lineinfo:66^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(at.auto_approve, 'N')
//          
//          from    app_type  at,
//                  application app
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_type = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(at.auto_approve, 'N')\n         \n        from    app_type  at,\n                application app\n        where   app.app_seq_num =  :1  and\n                app.app_type = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   autoApprove = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      result = autoApprove.equals("Y");
    }
    catch(Exception e)
    {
      logEntry("appIsAutoApprove(" + appSeqNum + ")", e.toString());
    }
    
    return result;
  }
  
  public String getAppSource(long loginId)
  {
    String      result      = "";
    
    // get the app source based on the login id
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:93^7*/

//  ************************************************************
//  #sql [Ctx] { select  login_name 
//          from    users
//          where   user_id = :loginId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  login_name  \n        from    users\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loginId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppSource(" + loginId + ")", e.toString());
      result = "unknown";
    }
    
    return result;
  }
  
  public void calculateCreditMatrixApplicationScore(long app_seq_num)
  {
    // delegate the scoring to a timed event
    
    try {
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_matrix_process
//          (
//            app_seq_num
//          )
//          values
//          (
//            :app_seq_num
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into credit_matrix_process\n        (\n          app_seq_num\n        )\n        values\n        (\n           :1 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.InsertCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_seq_num);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/
    }
    catch(Exception e) {
      logEntry("calculateCreditMatrixApplicationScore(" + app_seq_num + ")",
        "Error occurred attempting to insert new record in credit_matrix_process table.");
    }
  }
  
  public void pullCreditReport(long appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      // pull credit if:
      // app_type.pull_credit = 'Y' AND
      // (app_type.pull_credit_conversion = 'Y' OR
      //  (app_type.pull_credit_conversion = 'N' AND merchant.account_type = 'N'))
      /*@lineinfo:generated-code*//*@lineinfo:142^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(at.pull_credit||at.pull_credit_conversion||nvl(mr.account_type, 'N'),
//                    'YNN', 'Y',
//                    'YYN', 'Y',
//                    'YYC', 'Y',
//                    'N')  pull_credit
//          from    app_type at,
//                  application app,
//                  merchant mr
//          where   app.app_seq_num = :appSeqNum and
//                  at.app_type_code = app.app_type and
//                  app.app_seq_num = mr.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(at.pull_credit||at.pull_credit_conversion||nvl(mr.account_type, 'N'),\n                  'YNN', 'Y',\n                  'YYN', 'Y',\n                  'YYC', 'Y',\n                  'N')  pull_credit\n        from    app_type at,\n                application app,\n                merchant mr\n        where   app.app_seq_num =  :1  and\n                at.app_type_code = app.app_type and\n                app.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.InsertCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.InsertCreditQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
      
      rs = it.getResultSet();
      
      if(rs.next() && rs.getString("pull_credit").equals("Y"))
      {
        CreditReportBean.sendIdRequest(appSeqNum);
        
        // request sent so defer the wait and acquisition of the actual report to a timed event
        try 
        {
          /*@lineinfo:generated-code*//*@lineinfo:166^11*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_requests_process
//              (
//                app_seq_num
//              )
//              values
//              (
//                :appSeqNum
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into credit_requests_process\n            (\n              app_seq_num\n            )\n            values\n            (\n               :1 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.InsertCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^11*/
        }
        catch(Exception se) 
        {
          logEntry("pullCreditReport(" + appSeqNum  + "): insert into process table", se.toString());
        }
      }
      
      // calculate the credit matrix (internal) score
      calculateCreditMatrixApplicationScore(appSeqNum);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("pullCreditReport(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** private boolean updateMerchCreditStatus(long appSeqNum, int newStatus)
  **
  ** Updates the merch_credit_status field in merchant to a new status.
  **
  ** NOTE: connect() and cleanUp() NOT called.
  **
  ** RETURNS: true if success, else false.
  */
  private boolean updateMerchCreditStatus(long appSeqNum, int newStatus)
  {
    boolean updateOk = false;
    
    try
    {
      connect();

      // update the merch_credit_status column in the merchant table
      /*@lineinfo:generated-code*//*@lineinfo:219^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_credit_status = :newStatus
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_credit_status =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.InsertCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:224^7*/
      updateOk = true;
    }
    catch (Exception e)
    {
      logEntry("updateMerchCreditStatus(" + appSeqNum + ", " + newStatus + ")",
        e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return updateOk;
  }
  
  /*
  ** protected boolean isDemoApp(long appSeqNum)
  **
  ** Returns TRUE If app is of type APP_TYPE_DEMO, false otherwise
  */
  protected boolean isDemoApp(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      int appType = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:253^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^7*/
      
      result = (appType == mesConstants.APP_TYPE_DEMO);
    }
    catch(Exception e)
    {
      logEntry("isDemoApp(" + appSeqNum + ")", e.toString());
    }
    
    return result;
  }

  /*
  ** public boolean addToMesCreditQueue(long appSeqNum, UserBean user)
  **
  ** Adds the application to the MES credit queue and pulls credit.
  ** This is public so that it can be called separately to insert an app
  ** into mes production queues (i.e. after approval in CB&T credit queues).
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean addToMesCreditQueue(long appSeqNum, UserBean user)
  {
    boolean queueAddOk = false;
    boolean useCleanUp = false;

    try
    {
      connect();
      
      // see if this app is subject to auto-approval
      if(appIsAutoApprove(appSeqNum))
      {
        AutoApprove.autoApprove(appSeqNum, Ctx);
      }
      else if(! isDemoApp(appSeqNum))
      {
        AddQueueBean    aqb = new AddQueueBean(Ctx);
        AppTrackingBean atb = new AppTrackingBean(Ctx);
    
        // add the app to the credit queue if it doesn't already exist
        if(!aqb.entryExists(appSeqNum,QueueConstants.QUEUE_CREDIT) )
        {
          if(aqb.insertQueue( appSeqNum, 
                              QueueConstants.QUEUE_CREDIT,
                              QueueConstants.Q_CREDIT_NEW,
                              QueueConstants.Q_STATUS_NEW,
                              getAppSource(user.getUserId()),
                              user.getUserId(),
                              "SYSTEM") )
          {
            // add the credit queue sub-table entry
            aqb.insertCreditQueue(appSeqNum);
          
            // add the application to the app tracking table
            atb.addAllDepartmentTracking(appSeqNum);
      
            // update the merchant credit status in merchant
            updateMerchCreditStatus(appSeqNum,QueueConstants.CREDIT_NEW);
        
            // pull a credit report
            pullCreditReport(appSeqNum);
          }
          else
          {
            logEntry("addToMesCreditQueue(" + appSeqNum + ", " + user.getUserId() 
              + ")","Unable to insert into credit queue, side effects did not happen");
          }
        }
      }
      
      queueAddOk = true;
    }
    catch(Exception e)
    {
      logEntry("addToMesCreditQueue(" + appSeqNum + ", " + user.getUserId()
        + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return queueAddOk;
  }
  

  public boolean addToCbtReviewQueue(long appSeqNum, UserBean user)
  {
    try
    {
      // revert back to the old queue review
      //ReviewQueueOps.doDataAssignAdd(appSeqNum,user);

      // the new tiered queue ops
      TierMccQueueOps.doNewMcc(appSeqNum,user);
      return true;
    }
    catch(Exception e)
    {
      logEntry("addToCbtReviewQueue(" + appSeqNum + ", " + user.getUserId()
        + ")",e.toString());
    }

    return false;
  }

  /*
  ** public boolewn appTypeClientReview(long appSeqNum)
  **
  ** Returns true if this app should go under client review
  */
  public boolean appTypeClientReview(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      String clientReview = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:381^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(upper(at.client_review), 'N')
//          
//          from    app_type  at,
//                  application app
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_type = at.app_type_code      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(upper(at.client_review), 'N')\n         \n        from    app_type  at,\n                application app\n        where   app.app_seq_num =  :1  and\n                app.app_type = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   clientReview = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^7*/
      
      result = clientReview.equals("Y");
    }
    catch(Exception e)
    {
      logEntry("appTypeClientReview(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** public boolean addToClientReviewQueue(long appSeqNum, UserBean user)
  **
  ** Adds app to the Client Review Queue
  **
  ** RETURNS: true if successful, else false.
  **          
  */
  public boolean addToClientReviewQueue(long appSeqNum, UserBean user)
  {
    boolean addOk = false;
    
    try
    {
      connect();
      
      QueueTools.insertQueue(appSeqNum, MesQueues.Q_CLIENT_REVIEW_NEW, user);
       
      // update the merchant credit status in merchant
      updateMerchCreditStatus(appSeqNum,QueueConstants.CLIENT_REVIEW_NEW);

      addOk = true;
    }
    catch (Exception e)
    {
      logEntry("addToClientReviewQueue(" + appSeqNum + ", " + user.getLoginName() 
        + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return addOk;
  }
  
  /*
  ** public boolean addToBbtQaQueue(long appSeqNum, UserBean user)
  **
  ** Determines which BB&T qa queue an app should be added to and then 
  ** adds it.
  **
  ** RETURNS: true if successful, else false.
  **          
  */
  public boolean addToBbtQaQueue(long appSeqNum, UserBean user)
  {
    boolean rval = false;

    try
    {
      connect();

      // add all new apps to the bbt qa setup queue
      int queueType     = MesQueues.Q_BBT_QA_SETUP;
      int crntQueueType = QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_BBT_QA);

      if(crntQueueType == MesQueues.Q_NONE) {
        QueueTools.insertQueue(appSeqNum,queueType,user);
      } else {
        QueueTools.moveQueueItem(appSeqNum, crntQueueType, queueType, user, "insertCreditQueue.addToBbtQaQueue");
      }
      
      // update the merchant credit status in merchant
      updateMerchCreditStatus(appSeqNum,QueueConstants.CREDIT_NEW);
    
      // pull a credit report
      pullCreditReport(appSeqNum);
      
      rval = true;
    }
    catch(Exception e)
    {
      System.out.println("addToBbtQaQueue(" + appSeqNum + ", " + user.getUserId()
        + ") EXCEPTION: "+e.toString());
      logEntry("addToBbtQaQueue(" + appSeqNum + ", " + user.getUserId()
        + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return rval;
  }

  /*
  ** private boolean doCommonSideEffects(long appSeqNum)
  **
  ** Triggers certain side effects that all apps need to have happen when 
  ** completed.
  **
  ** RETURNS: true if successful, else false.
  */
  private boolean doCommonSideEffects(long appSeqNum)
  {
    boolean sideEffectsOk = false;

    try
    {
      connect();
      
      // notify third parties of application completion
      new AppNotifyBean(Ctx).notifyStatus( appSeqNum, 
                                        mesConstants.APP_STATUS_COMPLETE,
                                        -1);
                                    
      // build and store the XML application
      ApplicationXDoc.storeXml(appSeqNum);

      sideEffectsOk = true;
    }
    catch (Exception e)
    {
      logEntry("doCommonSideEffects(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return sideEffectsOk;
  }
  
  /*
  ** public boolean appAlreadyAdded(long appSeqNum)
  **
  ** Determines if queue items with this appSeqNum have already been added.
  **
  ** RETURNS: true if one or more queue items exists, else false.
  */
  public boolean appAlreadyAdded(long appSeqNum)
  {
    boolean alreadyAdded  = false;
    
    try
    {
      connect();
      
      int appCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:546^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    application
//          where   app_seq_num = :appSeqNum and
//                  ( app_user_login = 'mesappgen'
//                    or app_created_date < '11-nov-2001' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    application\n        where   app_seq_num =  :1  and\n                ( app_user_login = 'mesappgen'\n                  or app_created_date < '11-nov-2001' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^7*/
      
      alreadyAdded = (appCount > 0);
      
      if(!alreadyAdded)
      {
        /*@lineinfo:generated-code*//*@lineinfo:560^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    xml_applications
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    xml_applications\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^9*/
      
        alreadyAdded = (appCount > 0);
      }
    }
    catch (Exception e)
    {
      logEntry("appAlreadyAdded(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return alreadyAdded;
  }
  
  /**
   * public boolean isCbtReviewQueuesUser(UserBean user)
   *
   * Uses user hierarchy node to determine if this is a user that submits apps 
   * that need to be routed into CB&T review queues.
   *
   * RETURNS: true if user submits apps to CB&T review queues.
   */
  public boolean isCbtReviewQueuesUser(UserBean user)
  {
    boolean result = false;
    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:600^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(crqn.user_node)
//          
//          from    users u,
//                  t_hierarchy th,
//                  cbt_review_queue_nodes crqn
//          where   u.login_name = :user.getLoginName() and
//                  u.hierarchy_node = th.descendent and
//                  th.ancestor = crqn.user_node and
//                  upper(nvl(crqn.enabled, 'N')) = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_790 = user.getLoginName();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(crqn.user_node)\n         \n        from    users u,\n                t_hierarchy th,\n                cbt_review_queue_nodes crqn\n        where   u.login_name =  :1  and\n                u.hierarchy_node = th.descendent and\n                th.ancestor = crqn.user_node and\n                upper(nvl(crqn.enabled, 'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_790);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("isCbtReviewQueueUser()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void afterClientReviewQueue(long appSeqNum, UserBean appUser, UserBean reviewUser)
  {
    try
    {
      connect();
      
      if (isCbtReviewQueuesUser(appUser))
      {
        // add into new cb&t review queues
        addToCbtReviewQueue(appSeqNum,reviewUser);
      }
      else if (appUser.isUnderHierarchyNode(386700000L)) 
      {
        // add to bb&t qa queues
        addToBbtQaQueue(appSeqNum,appUser);
      }
      else
      {
        // add to mes queues
        addToMesCreditQueue(appSeqNum,appUser);
      }
    }
    catch(Exception e)
    {
      logEntry("afterClientReviewQueue(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** public boolean addApp(long appSeqNum, String sourceType, long loginId)
  **
  ** This routine adds the application into credit queues along with triggering 
  ** various side effects that need to happen when the application is completed.
  ** 
  ** RETURNS: true if all goes well, false if queue addition or side effects fail.
  */
  public boolean addApp(long appSeqNum, String sourceType, long loginId)
  {
    boolean queueAddOk    = false;
    boolean sideEffectsOk = false;
    
    try
    {
      connect();
      
      // don't allow an previously submitted app to be added again
      if (appAlreadyAdded(appSeqNum))
      {
        return false;
      }
      
      // get login name of user that actually created the app
      String  appUserLogin = "";
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_user_login
//          
//          from    application
//          where   app_seq_num  = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_user_login\n         \n        from    application\n        where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.InsertCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appUserLogin = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:690^7*/
      
      UserBean user = new UserBean();
      user.getData(appUserLogin);
      
      if ( appTypeClientReview(appSeqNum) )
      {
        // add to client review queue
        queueAddOk = addToClientReviewQueue(appSeqNum, user);
      }
      else if (isCbtReviewQueuesUser(user))
      {
        // add into cb&t tiered review queues
        queueAddOk = addToCbtReviewQueue(appSeqNum,user);
      }
      else if (user.isUnderHierarchyNode(386700000L)) 
      {
        // add to bb&t qa queues
        queueAddOk = addToBbtQaQueue(appSeqNum,user);
      }
      else
      {
        // add to mes queues
        queueAddOk = addToMesCreditQueue(appSeqNum,user);
      }
    
      // if no problems adding to credit queue, do side effects
      if (queueAddOk)
      {
        sideEffectsOk = doCommonSideEffects(appSeqNum);
      }
    }
    catch (Exception e)
    {
      logEntry("addApp(" + appSeqNum + ", " + sourceType + ", " + loginId 
        + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return queueAddOk && sideEffectsOk;
  }
  
  /*
  ** public boolean addApp(long appSeqNum, long loginId)
  **
  ** Wrapper method that defaults sourceType to "USER".
  **
  ** RETURNS: result of addApp(...).
  */
  public boolean addApp(long appSeqNum, long loginId)
  {
    return addApp(appSeqNum, "USER", loginId);
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      InsertCreditQueue duh = new InsertCreditQueue();
      
      UserBean user = new UserBean();
      
      user.forceValidate("jfirman");
      
      duh.addToMesCreditQueue(Long.parseLong(args[0]), user); 
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
  }
  
}/*@lineinfo:generated-code*/