/*@lineinfo:filename=AssignMidBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AssignMidBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-11-08 12:03:41 -0800 (Thu, 08 Nov 2007) $
  Version            : $Revision: 14308 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import sqlj.runtime.ref.DefaultContext;

/*
** CLASS AssignMidBean
**
** This class is used to assign a unique merchant number to an online
** application.
*/
public class AssignMidBean extends com.mes.database.SQLJConnectionBase
{
  public static final int       MERCHANT_NUMBER_THRESHOLD     = 300;
  
  private long      merchantNumber    = 0L;
  
  public AssignMidBean()
  {
  }
  
  public AssignMidBean(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  /*
  ** METHOD lowWaterMark
  **
  ** Sends an email to interested parties when a client's merchant number pool
  ** is running low
  */
  private static synchronized void lowWaterMark(String client, int count)
  {
    StringBuffer    subject = new StringBuffer("");
    StringBuffer    message = new StringBuffer("");
    MailMessage     msg     = null;
    
    try
    {
      subject.append("WARNING! Merchant Number pool running low for ");
      subject.append(client);
      subject.append("!");
      
      message.append("There are only ");
      message.append(count);
      message.append(" merchant numbers remaining in the ");
      message.append(client);
      message.append(" merchant number pool.  This situation must be rectified ");
      message.append("immediately by getting another block of merchant numbers.");
      
      msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_MERCHANT_NUMBER_POOL);
      msg.setSubject(subject.toString());
      msg.setText(message.toString());
      msg.send();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::lowWaterMark()", e.toString());
    }
  }
  
  /*
  ** METHOD getMESMid
  **
  ** Gets the next MES merchant number.  MES Merchant numbers are sequential,
  ** so this method simply accesses an Oracle sequence called 
  ** "merchant_sequence" to get the merchant number.
  **
  ** This method is synchronized to ensure thread-safe behavior, although
  ** the Oracle sequence is thread-safe already so the syncrhonization is
  ** not really necessary.
  */
  private static synchronized long getMESMid(DefaultContext Ctx)
  {
    long      merchantNumber    = -1L;
    boolean   foundValidNumber  = false;
    
    int       merchCount        = 0;
    int       mifCount          = 0;
    
    try
    {
      while(! foundValidNumber)
      {
        // get the next merchant number in sequence
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] { select merchant_sequence.nextval 
//            from  dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merchant_sequence.nextval  \n          from  dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^9*/
        
        // check to make sure this merchant number does not already exist in 
        // the merchant table
        /*@lineinfo:generated-code*//*@lineinfo:126^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_number) 
//            from    merchant
//            where   merch_number  = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_number)  \n          from    merchant\n          where   merch_number  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^9*/
        
        // check to make sure this merchant number does not already exist in
        // the mif table
        /*@lineinfo:generated-code*//*@lineinfo:135^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//            from    mif
//            where   merchant_number = :merchantNumber and
//                    bank_number     = 3941
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n          from    mif\n          where   merchant_number =  :1  and\n                  bank_number     = 3941";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^9*/
        
        if(merchCount == 0 && mifCount == 0)
        {
          // merchant number is valid so break out of the loop
          foundValidNumber  = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::getMESMid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  private static synchronized long get3942Mid(DefaultContext Ctx)
  {
    long      merchantNumber    = -1L;
    boolean   foundValidNumber  = false;
    
    int       merchCount        = 0;
    int       mifCount          = 0;
    
    try
    {
      while(! foundValidNumber)
      {
        // get the next merchant number in sequence
        /*@lineinfo:generated-code*//*@lineinfo:171^9*/

//  ************************************************************
//  #sql [Ctx] { select merchant_sequence_3942.nextval 
//            from  dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merchant_sequence_3942.nextval  \n          from  dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^9*/
        
        // check to make sure this merchant number does not already exist in 
        // the merchant table
        /*@lineinfo:generated-code*//*@lineinfo:179^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_number) 
//            from    merchant
//            where   merch_number  = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_number)  \n          from    merchant\n          where   merch_number  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^9*/
        
        // check to make sure this merchant number does not already exist in
        // the mif table
        /*@lineinfo:generated-code*//*@lineinfo:188^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//            from    mif
//            where   merchant_number = :merchantNumber and
//                    bank_number     = 3942
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n          from    mif\n          where   merchant_number =  :1  and\n                  bank_number     = 3942";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^9*/
        
        if(merchCount == 0 && mifCount == 0)
        {
          // merchant number is valid so break out of the loop
          foundValidNumber  = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::get3942Mid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  /*
  ** METHOD getBBTMid
  **
  ** Gets next available BB&T merchant number
  */
  private static synchronized long getBBTMid(long appSeqNum, DefaultContext Ctx)
  {
    long merchantNumber = -1L;

    try
    {
      // get the next unused merchant number from the cbt_merchant_numbers table
      /*@lineinfo:generated-code*//*@lineinfo:223^7*/

//  ************************************************************
//  #sql [Ctx] { select  min(merchant_number)  
//          from    bbt_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(merchant_number)   \n        from    bbt_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^7*/
      
      // update this table row to show that it is assigned to this app
      /*@lineinfo:generated-code*//*@lineinfo:231^7*/

//  ************************************************************
//  #sql [Ctx] { update  bbt_merchant_numbers
//          set     app_seq_num = :appSeqNum
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bbt_merchant_numbers\n        set     app_seq_num =  :1 \n        where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^7*/
      
      // check to see if we're running out of merchant numbers
      int mCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//          from    bbt_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n        from    bbt_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^7*/
      
      if(mCount < MERCHANT_NUMBER_THRESHOLD)
      {
        lowWaterMark("BB&T", mCount);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::getBBTMid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  /*
  ** METHOD getCBTMid
  **
  ** Gets the next available CB&T merchant number.  CB&T merchant numbers are
  ** not sequential, so they have to be assigned from a pool of numbers that
  ** are stored in the cbt_merchant_numbers table.
  **
  ** This method is synchronized to guarantee thread-safe access to the
  ** cbt_merchant_numbers table.  This is necessary because assigning the
  ** merchant number is a two-step process so the implicit Oracle thread-safety
  ** doesn't help.
  */
  private static synchronized long getCBTMid(long appSeqNum, DefaultContext Ctx)
  {
    long merchantNumber = -1L;
    
    try
    {
      boolean isUnique = true;
      
      do
      {
        isUnique = true;
        // get the next unused merchant number from the cbt_merchant_numbers table
        /*@lineinfo:generated-code*//*@lineinfo:285^9*/

//  ************************************************************
//  #sql [Ctx] { select  min(merchant_number)  
//            from    cbt_merchant_numbers
//            where   app_seq_num is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(merchant_number)   \n          from    cbt_merchant_numbers\n          where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^9*/
        
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:293^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//            
//            from    mif
//            where   merchant_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n           \n          from    mif\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^9*/                                               
        
        if( recCount > 0 )
        {
          isUnique = false;
        }
        
        if( isUnique == true )
        {
          /*@lineinfo:generated-code*//*@lineinfo:308^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_number)
//              
//              from    merchant
//              where   merch_number = :merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_number)\n             \n            from    merchant\n            where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^11*/
          
          if( recCount > 0 )
          {
            isUnique = false;
          }
        }
        
        if( ! isUnique )
        {
          // this merchant number can't work
          /*@lineinfo:generated-code*//*@lineinfo:325^11*/

//  ************************************************************
//  #sql [Ctx] { update  cbt_merchant_numbers
//              set     app_seq_num = -1
//              where   merchant_number = :merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  cbt_merchant_numbers\n            set     app_seq_num = -1\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^11*/
        }
        
      } while( isUnique == false );
        
      // update this table row to show that it is assigned to this app
      /*@lineinfo:generated-code*//*@lineinfo:336^7*/

//  ************************************************************
//  #sql [Ctx] { update  cbt_merchant_numbers
//          set     app_seq_num = :appSeqNum
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  cbt_merchant_numbers\n        set     app_seq_num =  :1 \n        where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^7*/
      
      // check to see if we're running out of merchant numbers
      int mCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:346^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//          from    cbt_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n        from    cbt_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:351^7*/
      
      if(mCount < MERCHANT_NUMBER_THRESHOLD)
      {
        lowWaterMark("CB&T", mCount);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::getCBTMid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  /*
  ** METHOD getTranscomMid
  **
  ** Gets the next available Transcom merchant number.  Transcom merchant 
  ** numbers are not sequential, so they have to be assigned from a pool 
  ** of numbers that are stored in the transcom_merchant_numbers table.
  **
  ** This method is synchronized to guarantee thread-safe access to the
  ** transcom_merchant_numbers table.  This is necessary because assigning the
  ** merchant number is a two-step process so the implicit Oracle thread-safety
  ** doesn't help.
  */
  private static synchronized long getTranscomMid(long appSeqNum, DefaultContext Ctx)
  {
    long merchantNumber = -1L;
    
    try
    {
      // get the next unused merchant number from transcom_merchant_numbers 
      /*@lineinfo:generated-code*//*@lineinfo:385^7*/

//  ************************************************************
//  #sql [Ctx] { select  min(merchant_number) 
//          from    transcom_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(merchant_number)  \n        from    transcom_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^7*/
      
      // update this table row to show that this merchant number is assigned
      /*@lineinfo:generated-code*//*@lineinfo:393^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_merchant_numbers
//          set     app_Seq_num = :appSeqNum
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_merchant_numbers\n        set     app_Seq_num =  :1 \n        where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:398^7*/
      
      // check to see if we're running out of merchant numbers
      int mCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:403^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//          from    transcom_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n        from    transcom_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^7*/
      
      if(mCount < MERCHANT_NUMBER_THRESHOLD)
      {
        lowWaterMark("Transcom", mCount);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::getTranscomMid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  /*
  ** METHOD getTranscomMid
  **
  ** Gets the next available NBSC merchant number.  NBSC merchant 
  ** numbers are not sequential, so they have to be assigned from a pool 
  ** of numbers that are stored in the nbsc_merchant_numbers table.
  **
  ** This method is synchronized to guarantee thread-safe access to the
  ** nbsc_merchant_numbers table.  This is necessary because assigning the
  ** merchant number is a two-step process so the implicit Oracle thread-safety
  ** doesn't help.
  */
  private static synchronized long getNBSCMid(long appSeqNum, DefaultContext Ctx)
  {
    long merchantNumber = -1L;
    
    try
    {
      // get the next unused merchant number from nbsc_merchant_numbers
      /*@lineinfo:generated-code*//*@lineinfo:442^7*/

//  ************************************************************
//  #sql [Ctx] { select  min(merchant_number) 
//          from    nbsc_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(merchant_number)  \n        from    nbsc_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:447^7*/
      
      // update this table row to show that this merchant number is assigned
      /*@lineinfo:generated-code*//*@lineinfo:450^7*/

//  ************************************************************
//  #sql [Ctx] { update  nbsc_merchant_numbers
//          set     app_Seq_num = :appSeqNum
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  nbsc_merchant_numbers\n        set     app_Seq_num =  :1 \n        where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:455^7*/
      
      // check to see if we're running out of merchant numbers
      int mCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:460^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//          from    nbsc_merchant_numbers
//          where   app_seq_num is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n        from    nbsc_merchant_numbers\n        where   app_seq_num is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/
      
      if(mCount < MERCHANT_NUMBER_THRESHOLD)
      {
        lowWaterMark("NBSC", mCount);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AssignMidBean::getTranscomMid()", e.toString());
    }
    
    return merchantNumber;
  }
  
  /*
  ** METHOD assignMid
  **
  ** Assigns a merchant number to an application.  This is the main method used
  ** by an interested accessor, and basically performs two steps:
  ** 
  ** 1) Establish the merchant number based on the application type
  **
  ** 2) Assign the merchant number to the application in the database
  **
  ** Once this method is completed, the merchant number is stored in the
  ** merchantNumber instance variable so that the caller can access it if
  ** necessary.
  */
  public boolean assignMid(long appSeqNum)
  {
    boolean       result                = false;
    int           appType               = -1;
    
    try
    {
      // establish connection to database
      connect();
      
      // don't assign a merchant number if one already exists for this merchant
      int midCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:506^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum and
//                  merch_number is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1  and\n                merch_number is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   midCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:513^7*/
      
      if(midCount > 0)
      {
        // merchant number already assigned
        result = true;
      }
      else
      {
        // determine which type of application this is
        /*@lineinfo:generated-code*//*@lineinfo:523^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_type 
//            from    application
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type  \n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.AssignMidBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:528^9*/
      
        // get merchant number based on appType
        switch(appType)
        {
          case mesConstants.APP_TYPE_BBT:
            this.merchantNumber = getBBTMid(appSeqNum, Ctx);
            break;
            
          case mesConstants.APP_TYPE_CBT:
          case mesConstants.APP_TYPE_MESA:
          case mesConstants.APP_TYPE_CBT_NEW:
            this.merchantNumber = getCBTMid(appSeqNum, Ctx);
            break;
          
          case mesConstants.APP_TYPE_TRANSCOM:
            this.merchantNumber = getTranscomMid(appSeqNum, Ctx);
            break;
          
          case mesConstants.APP_TYPE_NBSC:
          case mesConstants.APP_TYPE_NBSC2:
            this.merchantNumber = getNBSCMid(appSeqNum, Ctx);
            break;
          
          case mesConstants.APP_TYPE_DEMO:
            this.merchantNumber = 9999999999L;
            break;
          
          case mesConstants.APP_TYPE_MES:
          case mesConstants.APP_TYPE_MES_NEW:
          case mesConstants.APP_TYPE_VERISIGN:
          case mesConstants.APP_TYPE_CEDAR:
          case mesConstants.APP_TYPE_ORANGE:
          case mesConstants.APP_TYPE_SVB:
          case mesConstants.APP_TYPE_NSI:
          case mesConstants.APP_TYPE_DISCOVER:
          case mesConstants.APP_TYPE_DISCOVER_IMS:
          case mesConstants.APP_TYPE_VISTA:
          case mesConstants.APP_TYPE_GOLD:
          case mesConstants.APP_TYPE_SUMMIT:
          case mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL:
          case mesConstants.APP_TYPE_WEST_AMERICA:
          case mesConstants.APP_TYPE_VISA:
          case mesConstants.APP_TYPE_AGENT:
          case mesConstants.APP_TYPE_INTAGIO:
          case mesConstants.APP_TYPE_MESB:
          case mesConstants.APP_TYPE_RIVER_CITY:
          case mesConstants.APP_TYPE_FOOTHILL:
          case mesConstants.APP_TYPE_VERISIGN_V2:
          case mesConstants.APP_TYPE_ELM_NON_DEPLOY:
          case mesConstants.APP_TYPE_MOUNTAIN_WEST:
          case mesConstants.APP_TYPE_BANNER:
          case mesConstants.APP_TYPE_MESC:
          case mesConstants.APP_TYPE_NET_SUITE:
          case mesConstants.APP_TYPE_SABRE:
          case mesConstants.APP_TYPE_AUTHNET:
          case mesConstants.APP_TYPE_GBB:
          case mesConstants.APP_TYPE_FMST:
          case mesConstants.APP_TYPE_BCB:
          case mesConstants.APP_TYPE_CSB:
          case mesConstants.APP_TYPE_STERLING:
            this.merchantNumber = getMESMid(Ctx);
            break;
            
          case mesConstants.APP_TYPE_STERLING_3942:
            this.merchantNumber = get3942Mid(Ctx);
            break;
          
          default:
            // assign an MES merchant id for now
            this.merchantNumber = getMESMid(Ctx);
            //logEntry("assignMid()", "invalid app type (" + appType + ")");
            break;
        }
      
        if(merchantNumber > 0)
        {
          //System.out.println("Assigning " + this.merchantNumber + " to " + appSeqNum);
        
          // update merchant table with new merchant number
          /*@lineinfo:generated-code*//*@lineinfo:608^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     merch_number  = :this.merchantNumber
//              where   app_seq_num   = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n            set     merch_number  =  :1 \n            where   app_seq_num   =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.AssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.merchantNumber);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:613^11*/
      
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("assignMid()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** ACCESSOR getMerchantNumber
  */
  public long getMerchantNumber()
  {
    return this.merchantNumber;
  }
}/*@lineinfo:generated-code*/