/*@lineinfo:filename=CreditReportBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CreditReportBean.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.SynDataRequestXDoc;
import com.mes.net.SynDataResponseXDoc;
import com.mes.net.SynIdRequestXDoc;
import com.mes.net.SynIdResponseXDoc;
import com.mes.net.XMLMessage;
import com.mes.net.XMLSender;
import sqlj.runtime.ResultSetIterator;

public class CreditReportBean extends SQLJConnectionBase
{
  public static final int     MAX_DATAREQUEST_WAIT_MILIS = (60*1000); // wait max 1 minute for the data to come back
  
  private long    appSeqNum;
  
  public CreditReportBean(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }

  /*
  ** private boolean sendIdRequest()
  **
  ** Sends an id request to synergistic servers to initiate a credit
  ** report.  The response is stored for subsequent data requests.
  **
  ** RETURNS: true if successful, else false.
  */  
  private boolean sendIdRequest()
  {
    boolean sendIdOk = false;
    try
    {
      XMLMessage reqMsg = new XMLMessage(new SynIdRequestXDoc(appSeqNum));
      reqMsg.setTargetUrl(MesDefaults.getString(MesDefaults.DK_SYN_XML_TEST_URL));
      XMLMessage respMsg = 
        XMLSender.sendMessage(reqMsg,"CreditReportBean::sendIdRequest");
      SynIdResponseXDoc idRespDoc = new SynIdResponseXDoc(respMsg);
      idRespDoc.submitData(appSeqNum);
      sendIdOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "sendIdRequest(appSeqNum=" + appSeqNum + "), " + e.toString());
      logEntry("sendIdRequest(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    
    return sendIdOk;
  }
  public static boolean sendIdRequest(long appSeqNum)
  {
    CreditReportBean crb  = new CreditReportBean(appSeqNum);
    boolean sendIdOk      = false;
    
    try
    {
      crb.connect();
      sendIdOk = crb.sendIdRequest();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.ops.CreditReportBean::sendIdRequest() STATIC", e.toString());
    }
    finally
    {
      crb.cleanUp();
    }
    
    return sendIdOk;
  }
  
  /*
  ** private boolean sendDataRequest()
  **
  ** Loads a previously received id response and sends it as a data
  ** request.  Credit scores are extracted from the credit report data
  ** response and stored.
  **
  ** RETURNS: true if successful, else false.
  */
  private boolean sendDataRequest()
  {
    boolean sendDataOk = false;
    try
    {
      XMLMessage reqMsg = new XMLMessage(new SynDataRequestXDoc(appSeqNum));
      
      if(reqMsg != null)
      {
        reqMsg.setTargetUrl(MesDefaults.getString(MesDefaults.DK_SYN_XML_TEST_URL));
        XMLMessage respMsg = 
          XMLSender.sendMessage(reqMsg,"CreditReportBean::sendDataRequest",MAX_DATAREQUEST_WAIT_MILIS);
        SynDataResponseXDoc dataRespDoc = new SynDataResponseXDoc(respMsg);
        dataRespDoc.submitData(appSeqNum);
        sendDataOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + 
        "sendDataRequest(appSeqNum=" + appSeqNum + "), " + e.toString());
      logEntry("sendDataRequest(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return sendDataOk;
  }
  public static boolean sendDataRequest(long appSeqNum)
  {
    CreditReportBean  crb         = new CreditReportBean(appSeqNum);
    boolean           sendDataOk  = false;
    
    try
    {
      crb.connect();
      sendDataOk = crb.sendDataRequest();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.ops.CreditReportBean::sendDataRequest() STATIC", e.toString());
    }
    finally
    {
      crb.cleanUp();
    }
    
    return sendDataOk;
  }
  
  /*
  ** public class ScoreSet
  **
  ** Helper class that contains a credit score data set.
  */
  public class ScoreSet
  {
    private String segmentId;
    private String name;
    private String ssn;
    private String score;
    private String linkId;
    
    public ScoreSet(ResultSet rs)
    {
      try
      {
        segmentId = rs.getString("segment_id");
        name      = rs.getString("name");
        ssn       = rs.getString("ssn");
        score     = rs.getString("score");
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + 
          ".ScoreSet, " + e.toString());
        logEntry("ScoreSet",e.toString());
      }
    }
    
    public String getSegmentId()
    {
      return segmentId;
    }
    public String getName()
    {
      return name;
    }
    public String getSsn()
    {
      return ssn;
    }
    public String getScore()
    {
      return score;
    }
  };
  
  /*
  ** private int getScoreCount()
  **
  ** Determines how many scores, if any, are available for current
  ** appSeqNum.
  **
  ** RETURNS: number of scores found.
  */
  private int getScoreCount()
  {
    int count = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(name) 
//          from    credit_scores
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(name)  \n        from    credit_scores\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CreditReportBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".getScoreCount(appSeqNum=" + appSeqNum + ", count), " + e.toString());
      logEntry("getScoreCount(appSeqNum=" + appSeqNum + ", count)",e.toString());
    }
    
    return count;
  }
    

  /*
  ** private int getRequestCount()
  **
  ** Determines how many requests, if any, are available for current
  ** appSeqNum.
  **
  ** RETURNS: number of requests found.
  */
  private int getRequestCount()
  {
    int count = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:247^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    credit_requests
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    credit_requests\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CreditReportBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".getRequestCount(appSeqNum=" + appSeqNum + ", count), " + e.toString());
      logEntry("getRequestCount(appSeqNum=" + appSeqNum + ", count)",e.toString());
    }
    
    return count;
  }
    

  /*
  ** private Vector getScores()
  **
  ** Creates a vector containing a set of scores stored under the appSeqNum
  ** in the database.  If score data hasn't previously been requested, then 
  ** request the score data first.
  **
  ** RETURNS: Vector of found scores.
  */
  private Vector getScores()
  {
    Vector scores = null;
    
    try
    {
      // determine number of scores avaialable
      // (requesting them if necessary)
      int scoreCount = getScoreCount();
      if (scoreCount == 0 && getRequestCount() > 0 && sendDataRequest())
      {
        scoreCount = getScoreCount();
      }
    
      // if scores available, load them into a vector
      if (scoreCount > 0)
      {
        // get the scores from credit_scores
        ResultSetIterator it = null;
        /*@lineinfo:generated-code*//*@lineinfo:293^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  segment_id,
//                    name,
//                    ssn,
//                    score
//              
//            from    credit_scores
//      
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  segment_id,\n                  name,\n                  ssn,\n                  score\n            \n          from    credit_scores\n    \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CreditReportBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.CreditReportBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^9*/
  
        // load the scores into a vector
        ResultSet rs = it.getResultSet();
        scores = new Vector();
        while (rs.next())
        {
          scores.add(new ScoreSet(rs));
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + 
        ".getScores(appSeqNum=" + appSeqNum + ", score), " + e.toString());
      logEntry("getScores(appSeqNum=" + appSeqNum + ", score)",e.toString());
    }
    
    return scores;
  }
  public static Vector getScores(long appSeqNum)
  {
    CreditReportBean crb  = new CreditReportBean(appSeqNum);
    Vector    scores      = null;
    
    try
    {
      crb.connect();
      scores = crb.getScores();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.ops.CreditReportBean::getScores() STATIC", e.toString());
    }
    finally
    {
      crb.cleanUp();
    }
    
    return scores;
  }
  
  /*
  ** private String getOutputId()
  **
  ** RETURNS: the output id associated with appSeqNum.
  */
  private String getOutputId()
  {
    String outputId = null;
    try
    {
      if (getRequestCount() > 0)
      {
        // load the output id from credit_requests
        /*@lineinfo:generated-code*//*@lineinfo:358^9*/

//  ************************************************************
//  #sql [Ctx] { select  output_id 
//            from    credit_requests
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  output_id  \n          from    credit_requests\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.CreditReportBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   outputId = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:363^9*/
      }
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + 
        ".getOutputId(appSeqNum=" + appSeqNum + "), " + e.toString());
      logEntry("getOutputId(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return outputId;
  }
  public static String getOutputId(long appSeqNum)
  {
    CreditReportBean  crb       = new CreditReportBean(appSeqNum);
    String            outputId  = "";
    try
    {
      crb.connect();
      outputId = crb.getOutputId();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.ops.CreditReportBean::getOutputId() STATIC", e.toString());
    }
    finally
    {
      crb.cleanUp();
    }
    
    return outputId;
  }  
}/*@lineinfo:generated-code*/