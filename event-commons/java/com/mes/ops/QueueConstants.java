/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/QueueConstants.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/19/04 2:19p $
  Version            : $Revision: 41 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class QueueConstants
{
  // "credit" status 
  //   status indicating where app is in client review and account setup processes
  //   stored in merchant.merch_credit_status, visible on App Status Lookup screen
  //   manipulated by com.mes.ops.InsertCreditQueue

  // mes statuses
  public static final int     APP_NOT_FINISHED                = 0;
  public static final int     CREDIT_APPROVE                  = 1;
  public static final int     CREDIT_DECLINE                  = 2;
  public static final int     CREDIT_PEND                     = 3;
  public static final int     CREDIT_CANCEL                   = 4;
  public static final int     CREDIT_NEW                      = 5;
  
  // CB&T review queue statuses
  public static final int     CBT_CREDIT_NEW                  = 6;
  public static final int     CBT_CREDIT_PEND                 = 7;
  public static final int     CBT_CREDIT_CANCEL               = 8;
  public static final int     CBT_CREDIT_DECLINE              = 9;
  public static final int     CBT_REVIEW_INCOMPLETE           = 10;
  public static final int     CBT_REVIEW_COMPLETE             = 11;
  public static final int     CBT_REVIEW_CANCEL               = 12;

  // general purpose client review statuses
  public static final int     CLIENT_REVIEW_NEW               = 100;
  public static final int     CLIENT_REVIEW_PEND              = 101;
  public static final int     CLIENT_REVIEW_DECLINED          = 102;
  public static final int     CLIENT_REVIEW_CANCELED          = 103;


  // Queue Types
  public static final int     QUEUE_CREDIT                    = 1;
  public static final int     QUEUE_SETUP                     = 2;
  public static final int     QUEUE_EQUIPMENT                 = 3;
  public static final int     QUEUE_DOCUMENTATION             = 4;
  public static final int     QUEUE_CANCEL                    = 5;
  public static final int     QUEUE_VNUMBER                   = 6;
  public static final int     QUEUE_MMS                       = 7;
  public static final int     QUEUE_PROGRAMMING               = 8;
  public static final int     QUEUE_DEPLOYMENT                = 9;
  public static final int     QUEUE_ACTIVATION                = 10;
  public static final int     QUEUE_MATCH                     = 20;
  public static final int     QUEUE_INTERNALMATCH             = 30;
  
  //match queue stages
  public static final int     Q_MATCH_PEND                    = 200;
  public static final int     Q_MATCH_ERROR                   = 201;

  //internal match queue stages
  public static final int     Q_INTERNALMATCH_PEND            = 300;


  // Credit Queue Stages
  public static final int     Q_CREDIT_NEW                    = 10;
  public static final int     Q_CREDIT_DECLINE                = 11;
  public static final int     Q_CREDIT_PEND                   = 12;
  public static final int     Q_CREDIT_PEND_MATCH             = 13;
  public static final int     Q_CREDIT_OVERRIDE               = 14;
  
  // Documentation queue stages
  public static final int     Q_DOCUMENT_PENDING              = 20;
  public static final int     Q_DOCUMENT_RECEIVED             = 21;
  
  // Setup queue stages
  public static final int     Q_SETUP_NEW                     = 30;
  public static final int     Q_SETUP_VNUMBER                 = 31;
  public static final int     Q_SETUP_MANUAL                  = 32;
  public static final int     Q_SETUP_PENDING                 = 33;
  public static final int     Q_SETUP_COMPLETE                = 34;
  public static final int     Q_SETUP_QA                      = 35;
  public static final int     Q_SETUP_EXPORT                  = 36;
  public static final int     Q_SETUP_DATA_OVERFLOW           = 37;
  public static final int     Q_SETUP_QA_CORRECTION           = 38;


  public static final int     Q_PROGRAMMING_NEW               = 80;
  public static final int     Q_PROGRAMMING_PENDING           = 81;
  public static final int     Q_PROGRAMMING_COMPLETE          = 82;

  public static final int     Q_DEPLOYMENT_NEW                = 90;
  public static final int     Q_DEPLOYMENT_PENDING            = 91;
  public static final int     Q_DEPLOYMENT_COMPLETE           = 92;

  public static final int     Q_ACTIVATION_NEW                = 100;
  public static final int     Q_ACTIVATION_SCHEDULED          = 103;
  public static final int     Q_ACTIVATION_PENDING            = 101;
  public static final int     Q_ACTIVATION_COMPLETE           = 102;


  // Cancel queue stages
  public static final int     Q_CANCEL_NEW                    = 40;

  // Equipment queue stages
  public static final int     Q_EQUIPMENT_NEW                 = 50;
  public static final int     Q_EQUIPMENT_PEND                = 51;
  public static final int     Q_EQUIPMENT_COMPLETE            = 52;
    
  // v-number queue stages
  public static final int     Q_VNUMBER_NEW                   = 60;
  public static final int     Q_VNUMBER_ASSIGNED              = 61;

  // MMS queue stages
  public static final int     Q_MMS_NEW                       = 70;
  public static final int     Q_MMS_PEND                      = 73;
  public static final int     Q_MMS_EXCEPTION                 = 74;
  public static final int     Q_MMS_COMPLETED                 = 77;
  
  // Credit Queue Statuses
  public static final int     Q_STATUS_NONE                   = 9999;
  public static final int     Q_STATUS_UNKNOWN                = 0;
  public static final int     Q_STATUS_NEW                    = 1;
  public static final int     Q_STATUS_APPROVED               = 101;
  public static final int     Q_STATUS_DECLINED               = 102;
  public static final int     Q_STATUS_PENDING                = 103;
  public static final int     Q_STATUS_CANCELLED              = 104;
  
  // Documentation queue statuses
  public static final int     Q_STATUS_DOCUMENT_NEEDED        = 201;
  public static final int     Q_STATUS_DOCUMENT_RECEIVED      = 202;
  
  // Documentation Queue document types
  public static final long    DOC_VOIDED_CHECK                = 0x0001;
  public static final long    DOC_SIGNATURE_SHEET             = 0x0002;
  public static final long    DOC_TERMINATED_MERCHANT         = 0x0004;
  public static final long    DOC_3MONTHS_STATEMENTS          = 0x0008;
  public static final long    DOC_SITE_INSPECTION             = 0x0010;
  public static final long    DOC_PERSONAL_GURANTEE           = 0x0020;
  
  
  // v-number queue statuses
  public static final int     Q_STATUS_VNUMBER_NEEDED         = 601;
  public static final int     Q_STATUS_VNUMBER_ADDED          = 602;

  // Departments
  public static final int     DEPARTMENT_ALL                  = -1;
  public static final int     DEPARTMENT_CREDIT               = 100;
  public static final int     DEPARTMENT_RISK                 = 150;
  public static final int     DEPARTMENT_DISCOVER_RAP         = 175;
  public static final int     DEPARTMENT_TSYS                 = 200;
  public static final int     DEPARTMENT_MMS                  = 300;
  public static final int     DEPARTMENT_3RDPARTY             = 400;
  public static final int     DEPARTMENT_DEPLOYMENT           = 500;
  public static final int     DEPARTMENT_ACTIVATION           = 600;
  public static final int     DEPARTMENT_HELP_DESK            = 800;
  public static final int     DEPARTMENT_SALES                = 1000;
  
  public static final int     DEPARTMENT_LAST                 = DEPARTMENT_ACTIVATION;
  
  // Department statuses  
  public static final int     DEPT_STATUS_ALL                 = -1;
  
  public static final int     DEPT_STATUS_CREDIT_NEW          = 101;
  public static final int     DEPT_STATUS_CREDIT_APPROVED     = 102;
  public static final int     DEPT_STATUS_CREDIT_DECLINED     = 103;
  public static final int     DEPT_STATUS_CREDIT_PEND         = 104;
  public static final int     DEPT_STATUS_CREDIT_CANCEL       = 105;

  //DISCOVER DEPT STATUSES
  public static final int     DEPT_STATUS_DISCOVER_RAP_NA                     = 176;
  public static final int     DEPT_STATUS_DISCOVER_RAP_RECEIVED               = 177;
  public static final int     DEPT_STATUS_DISCOVER_RAP_TRANSMISSION           = 178;
  public static final int     DEPT_STATUS_DISCOVER_RAP_TRANSMISSION_ERRORS    = 179;
  public static final int     DEPT_STATUS_DISCOVER_RAP_LIVE_AUTO              = 180;
  public static final int     DEPT_STATUS_DISCOVER_RAP_FAX_TO_DISCOVER        = 181;
  public static final int     DEPT_STATUS_DISCOVER_RAP_LIVE_MANUAL            = 182;
  public static final int     DEPT_STATUS_DISCOVER_RAP_PENDING                = 183;
  public static final int     DEPT_STATUS_DISCOVER_RAP_DECLINED               = 184;
  public static final int     DEPT_STATUS_DISCOVER_RAP_RETRANSMISSION         = 185;


  public static String getDepartmentDescription(int department)
  {
    String DepartmentDesc = "Unknown Department";
    
    switch(department)
    {
      case DEPARTMENT_ALL:
        DepartmentDesc = "All";
        break;
        
      case DEPARTMENT_CREDIT:
        DepartmentDesc = "Credit";
        break;
        
      case DEPARTMENT_DISCOVER_RAP:
        DepartmentDesc = "Discover Rap";
        break;

      case DEPARTMENT_TSYS:
        DepartmentDesc = "TSYS";
        break;
        
      case DEPARTMENT_MMS:
        DepartmentDesc = "MMS";
        break;
        
      case DEPARTMENT_3RDPARTY:
        DepartmentDesc = "Setup Support";
        break;
      
      case DEPARTMENT_DEPLOYMENT:
        DepartmentDesc = "Deployment";
        break;

      case DEPARTMENT_ACTIVATION:
        DepartmentDesc = "Activation";
        break;

      case DEPARTMENT_RISK:
        DepartmentDesc = "Risk";
        break;

      case DEPARTMENT_HELP_DESK:
        DepartmentDesc = "Help Desk";
        break;

      case DEPARTMENT_SALES:
        DepartmentDesc = "Sales";
        break;

    }
    
    return DepartmentDesc;
  }
    
  public static String getQueueDescription(int QueueType)
  {
    String QueueDesc = "Unknown Queue";
    
    switch(QueueType)
    {
      case QUEUE_CREDIT:
        QueueDesc = "Credit Evaluation";
        break;
        
      case QUEUE_SETUP:
        QueueDesc = "Account Setup";
        break;
        
      case QUEUE_MMS:
        QueueDesc = "POS Setup";
        break;

      case QUEUE_EQUIPMENT:
        QueueDesc = "Equipment Management";
        break;
        
      case QUEUE_DOCUMENTATION:
        QueueDesc = "Documentation";
        break;
      
      case QUEUE_CANCEL:
        QueueDesc = "Cancelled Applications";
        break;
        
      case QUEUE_VNUMBER:
        QueueDesc = "POS ID Assignment";
        break;

      case QUEUE_ACTIVATION:
        QueueDesc = "Activation";
        break;

      case QUEUE_DEPLOYMENT:
        QueueDesc = "Deployment";
        break;

      case QUEUE_MATCH:
        QueueDesc = "Match Queue";
        break;
      
      case QUEUE_INTERNALMATCH:
        QueueDesc = "Internal Match Queue";
        break;
    }
    
    return QueueDesc;
  }
  
  public static String getStageDescription(int QueueStage)
  {
    String StageDesc = "Unknown Stage";
    
    switch(QueueStage)
    {
      case Q_CREDIT_NEW:
        StageDesc = "Credit Queue";
        break;

      case Q_CANCEL_NEW:
        StageDesc = "Cancel Queue";
        break;
        
      case Q_CREDIT_DECLINE:
        StageDesc = "Decline Queue";
        break;
        
      case Q_CREDIT_PEND:
        StageDesc = "Pending Queue";
        break;
      
      case Q_CREDIT_PEND_MATCH:
        StageDesc = "Pending Match Queue";
        break;

      case Q_CREDIT_OVERRIDE:
        StageDesc = "Credit Override Queue";
        break;

      case Q_MATCH_PEND:
        StageDesc = "Pending Match Queue";
        break;

      case Q_MATCH_ERROR:
        StageDesc = "Match Error Queue";
        break;

      case Q_INTERNALMATCH_PEND:
        StageDesc = "Pending Internal Match Queue";
        break;

      case Q_DOCUMENT_PENDING:
      case Q_DOCUMENT_RECEIVED:
        StageDesc = "Documentation Queue";
        break;
      
      case Q_SETUP_NEW:
        StageDesc = "Setup Queue";
        break;
        
      case Q_SETUP_QA:
        StageDesc = "Setup QA Queue";
        break;

      case Q_SETUP_EXPORT:
        StageDesc = "Setup Export Queue";
        break;

      case Q_SETUP_DATA_OVERFLOW:
        StageDesc = "Setup Data Overflow Queue";
        break;
      
      case Q_SETUP_QA_CORRECTION:
        StageDesc = "Setup QA Correction Queue";
        break;

      case Q_SETUP_VNUMBER:
        StageDesc = "POS ID Queue";
        break;
        
      case Q_SETUP_MANUAL:
        StageDesc = "Manual Data Entry Queue";
        break;
        
      case Q_SETUP_PENDING:
        StageDesc = "Setup Pending Queue";
        break;
        
      case Q_SETUP_COMPLETE:
        StageDesc = "Completed Queue";
        break;

      case Q_MMS_NEW:
        StageDesc = "POS New Queue";
        break;

      case Q_MMS_PEND:
        StageDesc = "POS Pend Queue";
        break;

      case Q_MMS_COMPLETED:
        StageDesc = "POS Completed Queue";
        break;

      case Q_MMS_EXCEPTION:
        StageDesc = "POS Exception Queue";
        break;
        
      case Q_VNUMBER_NEW:
        StageDesc = "POS ID Queue";
        break;
        
      case Q_VNUMBER_ASSIGNED:
        StageDesc = "POS ID (Assigned) Queue";
        break;
      
      case Q_EQUIPMENT_NEW:
        StageDesc = "Equipment Deployment New Queue";
        break;

      case Q_EQUIPMENT_PEND:
        StageDesc = "Equipment Deployment Pending Queue";
        break;

      case Q_EQUIPMENT_COMPLETE:
        StageDesc = "Equipment Deployment Complete Queue";
        break;

      case Q_ACTIVATION_NEW:
        StageDesc = "Activation New Queue";
        break;

      case Q_ACTIVATION_PENDING:
        StageDesc = "Activation Pending Queue";
        break;

      case Q_ACTIVATION_COMPLETE:
        StageDesc = "Activation Complete Queue";
        break;

      case Q_DEPLOYMENT_NEW:
        StageDesc = "Deployment New Queue";
        break;

      case Q_DEPLOYMENT_PENDING:
        StageDesc = "Deployment Pending Queue";
        break;

      case Q_DEPLOYMENT_COMPLETE:
        StageDesc = "Deployment Complete Queue";
        break;
    }
    
    return StageDesc;
  }
  
  public static String getStageString(int QueueStage)
  {
    String StageString = "Unknown";
    
    switch(QueueStage)
    {
      case Q_CREDIT_NEW:
        StageString = "Credit";
        break;

      case Q_CANCEL_NEW:
        StageString = "Cancel";
        break;
        
      case Q_CREDIT_DECLINE:
        StageString = "Decline";
        break;
        
      case Q_CREDIT_PEND:
        StageString = "Pending";
        break;

      case Q_CREDIT_PEND_MATCH:
        StageString = "Pending Match";
        break;

      case Q_CREDIT_OVERRIDE:
        StageString = "Credit Override";
        break;

      case Q_MATCH_PEND:
        StageString = "Pending Match";
        break;

      case Q_MATCH_ERROR:
        StageString = "Match Error";
        break;
        
      case Q_DOCUMENT_PENDING:
        StageString = "Pending";
        break;
        
      case Q_VNUMBER_NEW:
        StageString = "V-Number";
        break;
        
      case Q_VNUMBER_ASSIGNED:
        StageString = "V-Number Assigned";
        break;

      case Q_EQUIPMENT_NEW:
        StageString = "New Deployment";
        break;
      
      case Q_EQUIPMENT_PEND:
        StageString = "Pending Deployment";
        break;
      
      case Q_EQUIPMENT_COMPLETE:
        StageString = "Complete Deployment";
        break;

      case Q_MMS_NEW:
        StageString = "MMS";
        break;
    }
    
    return StageString;
  }
}
