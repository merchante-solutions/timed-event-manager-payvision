/*@lineinfo:filename=AutoUploadIndividualMerchant*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadIndividualMerchant.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadIndividualMerchant extends SQLJConnectionBase
{

  private String      cpsFlag           = "";   // merchant.merch_visa_cps_ind          (moto = 1, hotel = 2, default = N)
  private String      psirfFlag         = "";   // merchant.merch_visa_psirf_flag       (retail with terminal = Y, otherwise N)
  private String      smktFlag          = "";   // merchant.merch_visa_smkt_flag        (sic 5411 = Y, otherwise N)
  private String      epsFlag           = "";   // merchant.merch_visa_eps_flag         (sic 5812, 5814, 7523, 7832 = Y otherwise N)
  private String      eirfFlag          = "";   // merchant.merch_visa_eirf_flag        (default to Y.  N is the other option)
  private String      meritFlag         = "";   // merchant.merch_mc_merit_flag         (moto = 1, retail w/terminal = 3, default = N)
  private String      siipFlag          = "";   // merchant.merch_mc_smkt_flag          (sic 5411 = Y, otherwise N)
  private String      prmFlag           = "";   // merchant.merch_mc_premier_flag       (sic between 3300 & 3999 or 4411 = Y, otherwise N)
  private String      catFlag           = "";   // merchant.merch_mc_pertocat_flag      (sic 5542 = Y, otherwise N)
  private String      whsFlag           = "";   // merchant.merch_mc_warehouse_flag     (sic 5300 = Y, otherwise N)

  private String      depositFlag       = "";   // merchant.merch_deposit_flag          (default to Y, other option is N)
  private String      riskCategory      = "";   // 
  private String      exceptionFlag     = "";   // merchant.merch_dly_excpt_flag        (default to Y, other option is N)
  private String      fraudFlag         = "";   // merchant.merch_fraud_flag            (default to Y, other option is N)
  private String      holdFlag          = "";   // merchant.merch_stat_hold_flag        (default to N, other option is Y)
  private String      achFlag           = "";   // merchant.merch_dly_ach_flag          (default to Y, other option is N)
  private String      printFlag         = "";   // merchant.merch_stat_print_flag       (default to Y, other option is N)
  private String      dollarFlag        = "";   // merchant.merch_interchg_dollar_flag  (default to G. other options are N, B, and *)
  private String      numberFlag        = "";   // merchant.merch_interchg_number_flag  (default to G. other options are N, B, and *)

  private String      draftStorageFlag  = "";   // merchant.merch_paper_draft_flag
  private String      vrsFlag           = "";   // merchant.merch_vrs_flag
  private String      plasticFlag       = "N";   // merchant.merch_pos_add_ind
  private int         plasticNum        = 0;    // merchant.merch_pos_add_ind

  private String      dailyDiscountFlag = "";   // merchant.merch_dly_discount_flag
  
  private int         sicCode           = 0;
  private int         indType           = 0;
  private int         appType           = -1;
  private int         prodType          = 0;
  private int         busNature         = 0;
  private int         pricingGrid       = -1;
  
  public AutoUploadIndividualMerchant()
  {
  }

  public AutoUploadIndividualMerchant(long pk)
  {
    getInfo(pk);

    if(appType != mesConstants.APP_TYPE_BBT)
    {
      setFlags(pk);
    }
    
    setDefaults(pk);

    submitData(pk);
    //showit();
  }


  private void showit()
  {
    System.out.println("cpsFlag           = " + cpsFlag                );
    System.out.println("psirfFlag         = " + psirfFlag              );
    System.out.println("smktFlag          = " + smktFlag               );
    System.out.println("epsFlag           = " + epsFlag                );
    System.out.println("eirfFlag          = " + eirfFlag               );
    System.out.println("meritFlag         = " + meritFlag              );
    System.out.println("siipFlag          = " + siipFlag               );
    System.out.println("prmFlag           = " + prmFlag                );
    System.out.println("catFlag           = " + catFlag                );
    System.out.println("whsFlag           = " + whsFlag                );
    System.out.println("depositFlag       = " + depositFlag            );
    System.out.println("riskCategory      = " + riskCategory           );
    System.out.println("exceptionFlag     = " + exceptionFlag          );
    System.out.println("fraudFlag         = " + fraudFlag              );
    System.out.println("holdFlag          = " + holdFlag               );
    System.out.println("achFlag           = " + achFlag                );
    System.out.println("printFlag         = " + printFlag              );
    System.out.println("dollarFlag        = " + dollarFlag             );
    System.out.println("numberFlag        = " + numberFlag             );
    System.out.println("draftStorageFlag  = " + draftStorageFlag       );
    System.out.println("vrsFlag           = " + vrsFlag                );
    System.out.println("plasticFlag       = " + plasticFlag            );
    System.out.println("plasticNum        = " + plasticNum             );
    System.out.println("dailyDiscountFlag = " + dailyDiscountFlag      );
  }


  private void getInfo(long pk)
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;

    try
    {
      
      connect();
   
      /*@lineinfo:generated-code*//*@lineinfo:143^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.sic_code,
//                  merch.industype_code,
//                  merch.pricing_grid,
//                  merch.business_nature,
//                  app.app_type
//          from    merchant    merch,
//                  application app
//          where   merch.app_seq_num   = :pk and 
//                  merch.app_seq_num   = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.sic_code,\n                merch.industype_code,\n                merch.pricing_grid,\n                merch.business_nature,\n                app.app_type\n        from    merchant    merch,\n                application app\n        where   merch.app_seq_num   =  :1  and \n                merch.app_seq_num   = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadIndividualMerchant",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
        
        if(!isBlank(rs.getString("industype_code")))
        {
          this.indType = rs.getInt("industype_code");
        }

        if(!isBlank(rs.getString("pricing_grid")))
        {
          this.pricingGrid = rs.getInt("pricing_grid");
        }

        if(!isBlank(rs.getString("business_nature")))
        {
          this.busNature = rs.getInt("business_nature");
        }

        if(!isBlank(rs.getString("sic_code")))
        {

          this.sicCode = rs.getInt("sic_code");
        }
      }
      
      rs.close();
      it.close();
      
      // get pricing grid override if present
      /*@lineinfo:generated-code*//*@lineinfo:188^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(aib.pricing_grid_type, -1)  pricing_grid
//          from    appo_ic_bets  aib,
//                  tranchrg      tc
//          where   tc.app_seq_num = :pk and
//                  tc.cardtype_code = 1 and
//                  tc.tranchrg_interchangefee_type = aib.bet_type and
//                  tc.tranchrg_interchangefee_fee = aib.combo_id 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(aib.pricing_grid_type, -1)  pricing_grid\n        from    appo_ic_bets  aib,\n                tranchrg      tc\n        where   tc.app_seq_num =  :1  and\n                tc.cardtype_code = 1 and\n                tc.tranchrg_interchangefee_type = aib.bet_type and\n                tc.tranchrg_interchangefee_fee = aib.combo_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AutoUploadIndividualMerchant",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getInt("pricing_grid") != -1)
        {
          this.pricingGrid = rs.getInt("pricing_grid");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getInfo(" + pk + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }


  private void setFlags(long pk)
  {
    try
    {
      connect();
      switch(this.sicCode)
      {
        case 5411:
          //smktFlag        = isBlank(smktFlag)   ? "Y" : smktFlag;
        break;
        case 5542:
          catFlag         = isBlank(catFlag)    ? "Y" : catFlag;
        break;
        case 5814:
        case 7524:
        case 7833:
          epsFlag         = isBlank(epsFlag)    ? "Y" : epsFlag;
        break;
      }

      setFlagsProduct(pk);
    
      if(this.pricingGrid > 0)
      {
        switch(this.pricingGrid)
        {
          case mesConstants.APP_PG_RETAIL:
            cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag           = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
        
          case mesConstants.APP_PG_HOTEL:
            cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag           = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
        
          case mesConstants.APP_PG_MOTO:
            cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag           = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;  

          default:
            cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag           = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;

        }
      }
      else
      {
        //set to retail defaults
        cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
        psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
        smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
        epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
        eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
        meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
        siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
        prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
        catFlag           = isBlank(catFlag)          ? "N" : catFlag;
        whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
        draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
        vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
        plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
      }
      
      // for non-CB&T and non-BB&T apps set defaults
      if( appType != mesConstants.APP_TYPE_CBT && 
          appType != mesConstants.APP_TYPE_CBT_NEW &&
          appType != mesConstants.APP_TYPE_BBT)
      {
        int posCode = 0;
        /*@lineinfo:generated-code*//*@lineinfo:338^9*/

//  ************************************************************
//  #sql [Ctx] { select  pos_code
//            
//            from    merch_pos
//            where   app_seq_num = :pk
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_code\n           \n          from    merch_pos\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posCode = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
      
        // dial terminals
        if(posCode == mesConstants.APP_MPOS_DIAL_TERMINAL)
        {
          psirfFlag = "N";
          meritFlag = "N";
        }
        else
        {
          cpsFlag = "N";
          meritFlag = "N";
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("setFlags(" + pk + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setFlagsProduct(long pk)
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:379^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pc.pos_type   pos_type
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num   = :pk and
//                  mp.pos_code      = pc.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pc.pos_type   pos_type\n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num   =  :1  and\n                mp.pos_code      = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AutoUploadIndividualMerchant",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:386^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        prodType =  rs.getInt("pos_type");

        switch (prodType)
        {
          case mesConstants.APP_PAYSOL_DIAL_PAY:
            cpsFlag         = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag       = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag        = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag         = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag        = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag       = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag        = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag         = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag         = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag         = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag= isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag         = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag     = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
          case mesConstants.APP_PAYSOL_INTERNET:
            cpsFlag         = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag       = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag        = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag         = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag        = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag       = isBlank(meritFlag)        ? "N" : meritFlag;
            siipFlag        = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag         = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag         = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag         = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag= isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag         = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag     = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("setFlagsProduct: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setDefaults(long pk)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    try
    {
      connect();
      
      //only excute this for bbt..then exit out of method
      if(appType == mesConstants.APP_TYPE_BBT)
      {
        cpsFlag           = "N"; 
        psirfFlag         = "N"; 
        smktFlag          = "N"; 
        epsFlag           = "N"; 
        eirfFlag          = "N"; 
        meritFlag         = "N"; 
        siipFlag          = "N"; 
        prmFlag           = "N"; 
        catFlag           = "N"; 
        whsFlag           = "N"; 
        draftStorageFlag  = "N"; 
        vrsFlag           = "N"; 
        plasticFlag       = "N"; 
        depositFlag       = "Y";
        exceptionFlag     = "Y";
        fraudFlag         = "Y";
        holdFlag          = "N";
        achFlag           = "Y";
        printFlag         = "Y";
        dollarFlag        = "G";
        numberFlag        = "G";

        switch(this.busNature)
        {
          case 1:
            psirfFlag = "Y";
            meritFlag = "3";
          break;
          
          case 2:
            psirfFlag = "Y";
            meritFlag = "3";
          break;
          
          case 3:
            psirfFlag = "Y";
            meritFlag = "3";
          break;
          
          case 4:
            eirfFlag  = "Y";
            meritFlag = "1";
          break;
          
          case 5:
            eirfFlag  = "Y";
            meritFlag = "1";
          break;
          
          case 6:
            eirfFlag  = "Y";
            meritFlag = "1";
          break;
          
          case 7:
            eirfFlag  = "Y";
            meritFlag = "1";
          break;
        }
      
        //always erf y and merit 1 when its dialpay... we ignore the business nature..
        if(prodType == mesConstants.APP_PAYSOL_DIAL_PAY)
        {
          eirfFlag  = "Y";
          meritFlag = "1";
        }
      }
      else
      {
        // cpsFlag
        if(isBlank(cpsFlag))
        {
          switch(this.indType)
          {
            case 7:   // moto
            case 14:
              cpsFlag = "N";
              break;
            case 3:
              cpsFlag = "N";
              break;
            default:
              cpsFlag = "N";
              break;
          }
        }
      
        // psirfFlag
        if(isBlank(psirfFlag))
        {
          psirfFlag = (this.indType == 1 || this.indType == 9) ? "N" : "N";
        }
      
        // smktFlag
        if(isBlank(smktFlag))
        {
          smktFlag = (this.sicCode == 5411) ? "N" : "N";
        }
      
        // epsFlag
        if(isBlank(epsFlag))
        {
          switch(this.sicCode)
          {
            case 5812:
            case 5814:
            case 7523:
            case 7832:
              epsFlag = "N";
              break;
          
            default:
              epsFlag = "N";
              break;
          }
        }
      
        // eirfFlag
        if(isBlank(eirfFlag))
        {
          eirfFlag = "N";
        }
      
        // meritFlag
        if(isBlank(meritFlag))
        {
          switch(this.indType)
          {
            case 7:   // moto
            case 14:
              meritFlag = "N";
              break;
            case 1:   // retail
            case 9:
              meritFlag = "N";
              break;
            default:
              meritFlag = "N";
              break;
          }
        }

        long hnode = 0L;

        if(this.appType >= 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:596^11*/

//  ************************************************************
//  #sql [Ctx] it = { select hierarchy_node 
//              from   org_app 
//              where  app_type = :this.appType
//              order by hierarchy_node desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select hierarchy_node \n            from   org_app \n            where  app_type =  :1 \n            order by hierarchy_node desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AutoUploadIndividualMerchant",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:602^11*/
        
          rs = it.getResultSet();
        
          if(rs.next())
          {
            hnode = rs.getLong("hierarchy_node");
          }
        
          rs.close();
          it.close();
        }

    
        if(hnode != 9999999999L)
        {
          int anCount = 0;

          /*@lineinfo:generated-code*//*@lineinfo:620^11*/

//  ************************************************************
//  #sql [Ctx] { select count(ancestor)
//               
//              from   t_hierarchy 
//              where  hier_type  = 1 and 
//                     ancestor   = 3941500001 and 
//                     descendent = :hnode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(ancestor)\n              \n            from   t_hierarchy \n            where  hier_type  = 1 and \n                   ancestor   = 3941500001 and \n                   descendent =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hnode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   anCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:628^11*/
         
          if(anCount > 0)
          {
            meritFlag = "N";
          }  
          else
          {
            switch(this.appType)
            {
              case mesConstants.APP_TYPE_MES:
              case mesConstants.APP_TYPE_MES_NEW:
              case mesConstants.APP_TYPE_DISCOVER:
              case mesConstants.APP_TYPE_DISCOVER_IMS:
              case mesConstants.APP_TYPE_VERISIGN:
              case mesConstants.APP_TYPE_VERISIGN_V2:
              case mesConstants.APP_TYPE_NSI:
                meritFlag = "N";
              break;
            }
          }
      
        }
        else
        {
          switch(this.appType)
          {
            case mesConstants.APP_TYPE_MES:
            case mesConstants.APP_TYPE_MES_NEW:
            case mesConstants.APP_TYPE_DISCOVER:
            case mesConstants.APP_TYPE_DISCOVER_IMS:
            case mesConstants.APP_TYPE_VERISIGN_V2:
            case mesConstants.APP_TYPE_VERISIGN:
            case mesConstants.APP_TYPE_NSI:
              meritFlag = "N";
            break;
          }
        }

        // siipFlag
        if(isBlank(siipFlag))
        {
          siipFlag = (this.sicCode == 5411) ? "N" : "N";
        }
      
        // prmFlag
        if(isBlank(prmFlag))
        {
          /* prmFLag should ALWAYS be 'N' (per Judy Washington, 3/9/05 */
  //        if((this.sicCode >= 3300 && this.sicCode <= 3999) || this.sicCode == 4411)
  //        {
  //          prmFlag = "Y";
  //        }
  //        else
          {
            prmFlag = "N";
          }
        }
      
        // catFlag
        if(isBlank(catFlag))
        {
          catFlag = (this.sicCode == 5542) ? "N" : "N";
        }
      
        // whsFlag
        if(isBlank(whsFlag))
        {
          whsFlag = (this.sicCode == 5300) ? "N" : "N";
        }
      
        // depositFlag
        if(isBlank(depositFlag))
        {
          depositFlag = "Y";
        }
      
        // exceptionFlag
        if(isBlank(exceptionFlag))
        {
          exceptionFlag = "Y";
        }
      
        // fraudFlag
        if(isBlank(fraudFlag))
        {
          fraudFlag = "Y";
        }
      
        // achFlag
        if(isBlank(achFlag))
        {
          achFlag = "Y";
        }
      
        // dollarFlag
        if(isBlank(dollarFlag))
        {
          if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
          {
            dollarFlag = "B";
          }
          else
          {
            dollarFlag = "G";
          }
        }
      
        // numberFlag
        if(isBlank(numberFlag))
        {
          if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
          {
            numberFlag = "B";
          }
          else
          {
            numberFlag = "G";
          }
        }

        if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
        {
          if(prodType == 1 || prodType == 9 || indType == 9)
          {
            psirfFlag = "Y";
            meritFlag = "3";         
          } 
          else
          {
            cpsFlag   = "1";
            meritFlag = "1";         
          }
        }

        if(prodType != mesConstants.APP_PAYSOL_DIAL_TERMINAL)
        {
          psirfFlag = "N";  
          eirfFlag  = "N";
        }
      
        printFlag = _getPrintFlag(appType);
        holdFlag = ( printFlag.equals("Y") ? "N" : "Y" );

        switch(this.appType)
        {
          case mesConstants.APP_TYPE_VERISIGN:
          case mesConstants.APP_TYPE_NSI:
          case mesConstants.APP_TYPE_VERISIGN_V2:
          case mesConstants.APP_TYPE_PAYPAL_REFERRAL:
            dailyDiscountFlag = "D";
            break;
          case mesConstants.APP_TYPE_ELM_NON_DEPLOY:
            dailyDiscountFlag = "N";
            break;
          case mesConstants.APP_TYPE_NET_SUITE:
          case mesConstants.APP_TYPE_NETSUITE_2:
            dailyDiscountFlag = "N";
            break;
          case mesConstants.APP_TYPE_SABRE:
            dailyDiscountFlag = "N";
            break;
          case mesConstants.APP_TYPE_BCB:
            dailyDiscountFlag = "I";
            break;
          default:
            break;  
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setDefaults(" + pk + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public String _getPrintFlag(int appType)
  {
    String pFlag = "Y";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:818^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(print_statements, 'Y')
//          
//          from    app_type
//          where   app_type_code = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(print_statements, 'Y')\n         \n        from    app_type\n        where   app_type_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pFlag = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^7*/
    }
    catch(Exception e)
    {
      logEntry("_getPrintFlag(" + appType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return pFlag;
  }

  /*
  ** METHOD public void submitData()
  **
  */


  public void submitData(long pk)
  {
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:851^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_visa_cps_ind           =    :cpsFlag,
//                  merch_visa_psirf_flag        =    :psirfFlag,
//                  merch_visa_smkt_flag         =    :smktFlag,
//                  merch_visa_eps_flag          =    :epsFlag,
//                  merch_visa_eirf_flag         =    :eirfFlag,
//                  merch_mc_merit_ind           =    :meritFlag,
//                  merch_mc_smkt_flag           =    :siipFlag,
//                  merch_mc_premier_flag        =    :prmFlag,
//                  merch_mc_petrocat_flag       =    :catFlag,
//                  merch_mc_warehouse_flag      =    :whsFlag,
//                  merch_deposit_flag           =    :depositFlag,
//                  merch_dly_excpt_flag         =    :exceptionFlag,
//                  merch_fraud_flag             =    :fraudFlag,
//                  merch_stat_hold_flag         =    :holdFlag,
//                  merch_dly_ach_flag           =    :achFlag,
//                  merch_stat_print_flag        =    :printFlag,
//                  merch_interchg_dollar_flag   =    :dollarFlag,
//                  merch_interchg_number_flag   =    :numberFlag,
//                  merch_paper_draft_flag       =    :draftStorageFlag,
//                  merch_vrs_flag               =    :vrsFlag,
//                  merch_pos_add_ind            =    :plasticFlag,
//                  merch_eqpt_dlv_add_pref      =    :plasticNum,
//                  merch_dly_discount_flag      =    :dailyDiscountFlag
//          where   app_seq_num                  =    :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_visa_cps_ind           =     :1 ,\n                merch_visa_psirf_flag        =     :2 ,\n                merch_visa_smkt_flag         =     :3 ,\n                merch_visa_eps_flag          =     :4 ,\n                merch_visa_eirf_flag         =     :5 ,\n                merch_mc_merit_ind           =     :6 ,\n                merch_mc_smkt_flag           =     :7 ,\n                merch_mc_premier_flag        =     :8 ,\n                merch_mc_petrocat_flag       =     :9 ,\n                merch_mc_warehouse_flag      =     :10 ,\n                merch_deposit_flag           =     :11 ,\n                merch_dly_excpt_flag         =     :12 ,\n                merch_fraud_flag             =     :13 ,\n                merch_stat_hold_flag         =     :14 ,\n                merch_dly_ach_flag           =     :15 ,\n                merch_stat_print_flag        =     :16 ,\n                merch_interchg_dollar_flag   =     :17 ,\n                merch_interchg_number_flag   =     :18 ,\n                merch_paper_draft_flag       =     :19 ,\n                merch_vrs_flag               =     :20 ,\n                merch_pos_add_ind            =     :21 ,\n                merch_eqpt_dlv_add_pref      =     :22 ,\n                merch_dly_discount_flag      =     :23 \n        where   app_seq_num                  =     :24";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cpsFlag);
   __sJT_st.setString(2,psirfFlag);
   __sJT_st.setString(3,smktFlag);
   __sJT_st.setString(4,epsFlag);
   __sJT_st.setString(5,eirfFlag);
   __sJT_st.setString(6,meritFlag);
   __sJT_st.setString(7,siipFlag);
   __sJT_st.setString(8,prmFlag);
   __sJT_st.setString(9,catFlag);
   __sJT_st.setString(10,whsFlag);
   __sJT_st.setString(11,depositFlag);
   __sJT_st.setString(12,exceptionFlag);
   __sJT_st.setString(13,fraudFlag);
   __sJT_st.setString(14,holdFlag);
   __sJT_st.setString(15,achFlag);
   __sJT_st.setString(16,printFlag);
   __sJT_st.setString(17,dollarFlag);
   __sJT_st.setString(18,numberFlag);
   __sJT_st.setString(19,draftStorageFlag);
   __sJT_st.setString(20,vrsFlag);
   __sJT_st.setString(21,plasticFlag);
   __sJT_st.setInt(22,plasticNum);
   __sJT_st.setString(23,dailyDiscountFlag);
   __sJT_st.setLong(24,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:878^7*/
  
      if(appType == mesConstants.APP_TYPE_BBT)
      {
        /*@lineinfo:generated-code*//*@lineinfo:882^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_edc_flag = 'Y'
//            where   app_seq_num    = :pk
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_edc_flag = 'Y'\n          where   app_seq_num    =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.AutoUploadIndividualMerchant",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:887^9*/
      }
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public static String getPrintFlag(int appType)
  {
    String pFlag = "Y";
    
    try
    {
      AutoUploadIndividualMerchant auim = new AutoUploadIndividualMerchant();
      
      pFlag = auim._getPrintFlag(appType);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("getPrintFlag(" + appType + "): " + e.toString());
    }
    
    return( pFlag );
  }
}/*@lineinfo:generated-code*/