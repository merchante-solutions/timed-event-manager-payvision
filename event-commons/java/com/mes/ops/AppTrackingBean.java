/*@lineinfo:filename=AppTrackingBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AppTrackingBean.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/17/03 10:26a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class AppTrackingBean extends com.mes.database.SQLJConnectionBase
{
  public AppTrackingBean(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  public AppTrackingBean()
  {
  }
  
  public AppTrackingBean(String connectString)
  {
    super(connectString);
  }
  
  public boolean trackRecordExists(long appSeqNum, int department)
  {
    int       recordCount     = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    app_tracking
//          where   app_seq_num = :appSeqNum and
//                  dept_code   = :department
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    app_tracking\n        where   app_seq_num =  :1  and\n                dept_code   =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AppTrackingBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,department);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recordCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::trackRecordExists()", e.toString());
    }
    
    return (recordCount > 0);
  }
  
  public void addTrackRecord(long appSeqNum, int department, int status)
  {
    try
    {
      if( ! trackRecordExists(appSeqNum, department) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:79^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_tracking
//            (
//              app_seq_num,
//              dept_code,
//              status_code
//            )
//            values
//            (
//              :appSeqNum,
//              :department,
//              :status
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_tracking\n          (\n            app_seq_num,\n            dept_code,\n            status_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AppTrackingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,department);
   __sJT_st.setInt(3,status);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^9*/
        
        if(department == QueueConstants.DEPARTMENT_CREDIT)
        {
          /*@lineinfo:generated-code*//*@lineinfo:97^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//              set     date_received = sysdate
//              where   app_seq_num   = :appSeqNum and
//                      dept_code     = :department
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n            set     date_received = sysdate\n            where   app_seq_num   =  :1  and\n                    dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.AppTrackingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,department);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^11*/
        }
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::addTrackRecord()", e.toString());
    }
  }
  

  public boolean newDiscoverAccount(long appSeqNum)
  {
    ResultSetIterator it          = null;
    boolean           result      = false;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:122^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    merchpo_rate
//          from      merchpayoption
//          where     app_seq_num = :appSeqNum
//          and       cardtype_code = :com.mes.constants.mesConstants.APP_CT_DISCOVER
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merchpo_rate\n        from      merchpayoption\n        where     app_seq_num =  :1 \n        and       cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AppTrackingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.constants.mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AppTrackingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:128^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getString("merchpo_rate") != null && !rs.getString("merchpo_rate").equals(""))
        {
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + ":newDiscoverAccount()", e.toString());
    }
    finally
    {
      if(it != null)
      {
        try{it.close();}catch(Exception e){}
      }
    }
    return result;
  }

  public void addAllDepartmentTracking(long appSeqNum)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:163^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    dept_code
//          from      departments
//          where     dept_code <> -1
//          order by  dept_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    dept_code\n        from      departments\n        where     dept_code <> -1\n        order by  dept_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AppTrackingBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AppTrackingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int dept = rs.getInt("dept_code");
        int status = -1;
        
        if(dept == QueueConstants.DEPARTMENT_CREDIT)
        {
          status = QueueConstants.DEPT_STATUS_CREDIT_NEW;
        }
        else if(dept == QueueConstants.DEPARTMENT_DISCOVER_RAP)
        {
          if(newDiscoverAccount(appSeqNum))
          {
            status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_RECEIVED;
          }
          else
          {
            status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_NA;
          }
        }
        
        addTrackRecord(appSeqNum, dept, status);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::addAllDepartmentTracking()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/