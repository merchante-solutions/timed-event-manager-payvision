/*@lineinfo:filename=AutoUploadBetInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadBetInfo.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadBetInfo extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(AutoUploadBetInfo.class);
  
  private static final String AMEX_PLAN       = "AM";
  private static final String DISCOVER_PLAN   = "DS";
  private static final String JCB_PLAN        = "JC";
  private static final String DINERS_PLAN     = "DC";
  private static final String CARTE_PLAN      = "CB";
  private static final String EBT_PLAN        = "EB";
  private static final String OVERALL_PLAN    = "OV";
  private static final String VISA_PLAN       = "VS";
  private static final String MASTERCARD_PLAN = "MC";
  private static final String DEBIT_PLAN      = "DB";
  private static final String VIRTERM_PLAN    = "VT";

  private static final String VOICE_MEDIA     = "VO";
  private static final String TERMINAL_MEDIA  = "TE";
  private static final String ARU_MEDIA       = "AR";
  private static final String ECR_MEDIA       = "EC";
  private static final String BATCH_MEDIA     = "BA";
  private static final String FIXED_MEDIA     = "FX";

  private static final String VENDOR_VITAL          = "VP";
  private static final String VENDOR_INHOUSE        = "IN";
  private static final String VENDOR_GLOBAL_CENTRAL = "MC";
  private static final String VENDOR_GLOBAL_EAST    = "ND";
  private static final String VENDOR_PAYTECH        = "GS";
  
  private static final int    BET_FLAVOR_DEFAULT    = 1;
  


  private String betVisa                = "";
  private String betMC                  = "";
  private String betDebit               = "";
  private String betSystem              = "";
  private String indBetVisa             = "";
  private String indBetMC               = "";
  private String betDataCapture1        = "";
  private String vendorDataCapture1     = "";
  private String metTable               = "";

  private Vector bets                   = new Vector();

  
  private boolean betVisaReq            = true;
  private boolean betMCReq              = true;
  private boolean betSystemReq          = true;
  private boolean betDebitReq           = false;

  private Vector  cards                 = new Vector();

  private int     appType               = -1;
  private int     productType           = 0;
  private String  productTypeDesc       = "";
  private int     planType              = 0;
  private int     betTypeCode           = 0;
  private int     pricingGrid           = 0;
  private int     interchangeTypeCode   = 0;
  private int     interchangeFeeCode    = 0;
  private boolean interDefault          = false;
  private String  internetType          = "";

  private boolean hasWireless           = false;

  private double  debitFee              = 0.0;
  private double  internetFee           = 0.0;
  private double  dialPayFee            = 0.0;
  private double  chargebackFee         = 0.0;
  private double  achDepositFee         = 0.0;
  private double  voiceAuthFee          = -1.0;
  private double  aruAuthFee            = 0.0;
  private double  vmcAuthFee            = 0;
  private double  avsFee                = 0.0;
  private int     processor             = -1;
  private String  adminFee              = "";

  public AutoUploadBetInfo(long primaryKey)
  {
    boolean useCleanup = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanup = true;
      }
      
      setPageDefaults(primaryKey);
      setBets(primaryKey);
      submitData(primaryKey);
    }
    catch(Exception e)
    {
      logEntry("AutoUploadBetInfo(" + primaryKey + ")", e.toString());
      System.out.println("AutoUploadBetInfo constructor: " + e.toString());
    }
    finally
    {
      if(useCleanup)
      {
        cleanUp();
      }
    }
  }

  public void setPageDefaults(long primaryKey)  
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    int                 i         = 0;

    try 
    {
      
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type
//          from    application
//          where   app_seq_num   = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type\n        from    application\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
      }

      it.close();

      //get product type and solution from application
      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] it = { select a.pos_code, 
//                 a.pos_type 
//          from   merch_pos    b, 
//                 pos_category a
//          where  b.app_seq_num = :primaryKey and 
//                 b.pos_code    = a.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select a.pos_code, \n               a.pos_type \n        from   merch_pos    b, \n               pos_category a\n        where  b.app_seq_num =  :1  and \n               b.pos_code    = a.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.productType = rs.getInt("pos_type");
        int sol          = rs.getInt("pos_code");

        if(productType == mesConstants.APP_PAYSOL_INTERNET && (sol == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK || sol == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO || sol == mesConstants.APP_MPOS_CYBERCASH_IC_VERIFY || sol == mesConstants.APP_MPOS_CYBERCASH_CASH_REGISTER || sol == mesConstants.APP_MPOS_CYBERCASH_WEB_AUTHORIZE))
        {
          this.interDefault = true;
        }
        
      }
      
      it.close();

      //get chargeback fee from application
      /*@lineinfo:generated-code*//*@lineinfo:210^7*/

//  ************************************************************
//  #sql [Ctx] it = { select misc_code,
//                 misc_chrg_amount 
//          from   miscchrg
//          where  app_seq_num = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select misc_code,\n               misc_chrg_amount \n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("misc_code"))
        {
          case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
            chargebackFee = rs.getDouble("misc_chrg_amount");
          break;
          case mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE:
            achDepositFee = rs.getDouble("misc_chrg_amount");
          break;
          case mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE:
            voiceAuthFee  = rs.getDouble("misc_chrg_amount");
          break;
        }
      }
      
      it.close();

      //get plan type,interchange type code and interchange fee code from application
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] it = { select tranchrg_discrate_type,
//                 tranchrg_interchangefee_type,
//                 tranchrg_interchangefee_fee,
//                 tranchrg_aru_fee,
//                 tranchrg_voice_fee,
//                 tranchrg_avs_fee,
//                 tranchrg_per_auth
//          from   tranchrg
//          where  app_seq_num    = :primaryKey and 
//                 cardtype_code  = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tranchrg_discrate_type,\n               tranchrg_interchangefee_type,\n               tranchrg_interchangefee_fee,\n               tranchrg_aru_fee,\n               tranchrg_voice_fee,\n               tranchrg_avs_fee,\n               tranchrg_per_auth\n        from   tranchrg\n        where  app_seq_num    =  :1  and \n               cardtype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        this.planType             = rs.getInt("tranchrg_discrate_type");
        this.interchangeTypeCode  = isBlank(rs.getString("tranchrg_interchangefee_type")) ? 0 : rs.getInt("tranchrg_interchangefee_type"); //cbt only for now
        this.interchangeFeeCode   = isBlank(rs.getString("tranchrg_interchangefee_fee"))  ? 0 : rs.getInt("tranchrg_interchangefee_fee");  //cbt only for now

        if(this.voiceAuthFee == -1.0 && !isBlank(rs.getString("tranchrg_voice_fee")))
        {
          this.voiceAuthFee = rs.getDouble("tranchrg_voice_fee");
        }
        this.aruAuthFee     = isBlank(rs.getString("tranchrg_aru_fee"))  ? 0.0 : rs.getDouble("tranchrg_aru_fee");

        if(!isBlank(rs.getString("tranchrg_per_auth")))
        {
          this.vmcAuthFee = rs.getDouble("tranchrg_per_auth");
        }
        
        if(!isBlank(rs.getString("tranchrg_avs_fee")))
        {
          this.avsFee = rs.getDouble("tranchrg_avs_fee");
        }
      }

     
      it.close();

      //get plan type from application
      /*@lineinfo:generated-code*//*@lineinfo:282^7*/

//  ************************************************************
//  #sql [Ctx] it = { select bet_type_code,
//                 pricing_grid,
//                 processor
//          from   merchant
//          where  app_seq_num    = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select bet_type_code,\n               pricing_grid,\n               processor\n        from   merchant\n        where  app_seq_num    =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        if(!isBlank(rs.getString("bet_type_code")))
        {
          this.betTypeCode = rs.getInt("bet_type_code");
        }
        
        if(!isBlank(rs.getString("pricing_grid")))
        {
          this.pricingGrid = rs.getInt("pricing_grid");
        }

        if(!isBlank(rs.getString("processor")))
        {
          this.processor = rs.getInt("processor");
        }
      }
     
      it.close();


      // determine if merchant has wireless equipment
      int wirelessCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:316^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          
//          from    merchequipment
//          where   app_seq_num = :primaryKey
//                  and prod_option_id in ( 20, 21, 22 )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num) \n         \n        from    merchequipment\n        where   app_seq_num =  :1 \n                and prod_option_id in ( 20, 21, 22 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   wirelessCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/

      hasWireless = (wirelessCount > 0);

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
      System.out.println("setPageDefaults: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setBets(long primaryKey)
  {
    switch(this.appType)
    {
      case mesConstants.APP_TYPE_VERISIGN:
      case mesConstants.APP_TYPE_NSI:
      case mesConstants.APP_TYPE_VERISIGN_V2:
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,     "8030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3930"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,       "6030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_INHOUSE, FIXED_MEDIA,   "1430"));
        betVisa                 = "4094";
        betMC                   = "5094";
        indBetVisa              = "*";
        indBetMC                = "*";
        betSystem               = "1020";
        betDebit                = "";
      break;

      case mesConstants.APP_TYPE_SABRE:
        betVisa                 = "4000";
        betMC                   = "5000";
        betSystem               = "1020";
        indBetVisa              = "*";
        indBetMC                = "*";
      break;

      case mesConstants.APP_TYPE_NET_SUITE:
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7059"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9059"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2059"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3059"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_INHOUSE, FIXED_MEDIA,   "1459"));
        betVisa                 = "4081";
        betMC                   = "5081";
        indBetVisa              = "*";
        indBetMC                = "*";
        betSystem               = "1020";
        betDebit                = "";
      break;

      case mesConstants.APP_TYPE_TRANSCOM:
        setTranscomSystemBet();
        setTranscomVisaMcBets(primaryKey);
        setTranscomDebitBet(primaryKey);
        setAuthBets(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
      break;

      case mesConstants.APP_TYPE_BBT:
        betSystem = "7999"; //always 7999 generate chargeback through charge record
        setBbtInterchangeBets(primaryKey);
        
        if(hasWireless)
        {
          setBbtVisaMcWirelessAuthBets();
        }
        else
        {
          setBbtVisaMcAuthBets();
        }
        
        setBbtDebitBet(primaryKey);

        if(hasWireless)
        {
          setBbtNonBankCardWirelessBets(primaryKey);
        }
        else
        {
          setBbtNonBankCardBets(primaryKey);
        }

        setBbtVoiceAruBets();
        setDataCaptureBet();
        setBbtMet(primaryKey);
      break;

      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_CBT_NEW:
        setFeeSetInterchangeBets();
        
        // deal with special situation where separate check card rate chosen
        // for passthru interchange choice
        setCbtCheckCardInterchange(primaryKey);
        
        if(productType != mesConstants.APP_PAYSOL_DIAL_PAY)
        {
          setCbtVisaMcAuthBets();
        }
        
        setCbtSystemBet();
        setCbtDebitBet(primaryKey);
        setCbtVoiceAuthFeeBets();
        setCbtAruAuthFeeBets();
        setCbtNonBankCardBets(primaryKey);
      break;
      
      case mesConstants.APP_TYPE_MES_NEW:
        setFeeSetInterchangeBets();
        //setSystemBet();
        setTranscomSystemBet();
        setTranscomDebitBet(primaryKey);
        setAuthBets(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        break;
      
      case mesConstants.APP_TYPE_CSB:
        setFeeSetInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
        break;
      
      case mesConstants.APP_TYPE_BCB:
        setFeeSetInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
        
        // for the plan bets to 0014 for Butte
        indBetVisa  = "0014";
        indBetMC    = "0014";
        break;

      case mesConstants.APP_TYPE_MOUNTAIN_WEST:
        setMountainWestInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        if(!setInternetBet(primaryKey))
        {
          setAuthBets(primaryKey);
        }
      break;

      case mesConstants.APP_TYPE_ELM_NON_DEPLOY:
        setElmNonDeployInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        if(!setInternetBet(primaryKey))
        {
          setAuthBets(primaryKey);
        }
      break;

      case mesConstants.APP_TYPE_BANNER:
        setBannerInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setAuthBets(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
      break;

      case mesConstants.APP_TYPE_DISCOVER_IMS:
        setImsInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        voiceAuthFee = 0.0;
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
      break;
      
      case mesConstants.APP_TYPE_AUTHNET:
        setFeeSetInterchangeBets();
        setSystemBet();
        // set auth bets
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_INHOUSE, FIXED_MEDIA,  "1425"));
        break;
        
      case mesConstants.APP_TYPE_GOLD:
        setGoldInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        if(hasWireless)
        {
          setWirelessBets();
        }
        else
        {
          setVoiceAuthFeeBets(primaryKey);
          setAuthBets(primaryKey);
        }
        break;
      
      case mesConstants.APP_TYPE_FMBANK:
      case mesConstants.APP_TYPE_FFB:
      case mesConstants.APP_TYPE_STERLING:
        setFeeSetInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        if(hasWireless)
        {
          setWirelessBets();
        }
        else
        {
          setVoiceAuthFeeBets(primaryKey);
          setAuthBets(primaryKey);
        }
        break;
        
      case mesConstants.APP_TYPE_COUNTRY_CLUB:
        setFeeSetInterchangeBets();
        setCCBSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
        break;
        
      case mesConstants.APP_TYPE_FNMS:
        setFeeSetInterchangeBets();
        setFNMSSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
        break;
        
      case mesConstants.APP_TYPE_VPS_ISO:
      case mesConstants.APP_TYPE_EXCHANGE:
      default:
        setFeeSetInterchangeBets();
        setSystemBet();
        setDebitBet(primaryKey);
        setVoiceAuthFeeBets(primaryKey);
        setAuthBets(primaryKey);
        break;
    }
  }

  private void setCbtSystemBet()
  {
    if(chargebackFee == 10.0)
    {
      betSystem = "6010";
    }
    else if(chargebackFee == 15.0)
    {
      betSystem = "6015";
    }
    else if(chargebackFee == 20.0)
    {
      betSystem = "6020";
    }
    else if(chargebackFee == 25.0)
    {
      betSystem = "6999";
    }
  }

  private void setTranscomSystemBet()
  {
    if(chargebackFee == 20.0 && achDepositFee == 0.0)
    {
      betSystem = "1020";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.05)
    {
      betSystem = "1205";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.1)
    {
      betSystem = "1210";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.15)
    {
      betSystem = "1215";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.2)
    {
      betSystem = "1220";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.25)
    {
      betSystem = "1225";
    }
    else if(chargebackFee == 20.0 && achDepositFee == 0.3)
    {
      betSystem = "1230";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.0)
    {
      betSystem = "1015";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.05)
    {
      betSystem = "1105";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.1)
    {
      betSystem = "1110";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.15)
    {
      betSystem = "1115";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.2)
    {
      betSystem = "1120";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.25)
    {
      betSystem = "1125";
    }
    else if(chargebackFee == 15.0 && achDepositFee == 0.3)
    {
      betSystem = "1130";
    }
  }
  
  private void setCCBSystemBet()
  {
    if(chargebackFee == 0.0)
    {
      betSystem = "1026";
    }
    else if(chargebackFee == 15.0)
    {
      betSystem = "1126";
    }
    else
    {
      // default to $20
      betSystem = "1226";
    }
  }
  
  private void setFNMSSystemBet()
  {
    if(achDepositFee == 0.10)
    {
      if(chargebackFee == 0.0)
      {
        betSystem = "1106";
      }
      else if(chargebackFee == 10.0)
      {
        betSystem = "1111";
      }
      else if(chargebackFee == 20.0)
      {
        betSystem = "1210";
      }
      else if(chargebackFee == 30.0)
      {
        betSystem = "1116";
      }
      else if(chargebackFee == 35.0)
      {
        betSystem = "1121";
      }
    }
    else if(achDepositFee == 0.15)
    {
      if(chargebackFee == 0.0)
      {
        betSystem = "1107";
      }
      else if(chargebackFee == 10.0)
      {
        betSystem = "1112";
      }
      else if(chargebackFee == 20.0)
      {
        betSystem = "1215";
      }
      else if(chargebackFee == 30.0)
      {
        betSystem = "1117";
      }
      else if(chargebackFee == 35.0)
      {
        betSystem = "1122";
      }
    }
    else if(achDepositFee == 0.20)
    {
      if(chargebackFee == 0.0)
      {
        betSystem = "1108";
      }
      else if(chargebackFee == 10.0)
      {
        betSystem = "1113";
      }
      else if(chargebackFee == 20.0)
      {
        betSystem = "1220";
      }
      else if(chargebackFee == 30.0)
      {
        betSystem = "1118";
      }
      else if(chargebackFee == 35.0)
      {
        betSystem = "1123";
      }
    }
    else if(achDepositFee == 0.25)
    {
      // set special system bets for 1st National Merchant Services
      if(chargebackFee == 0.0)
      {
        betSystem = "1100";
      }
      else if(chargebackFee == 10.0)
      {
        betSystem = "1101";
      }
      else if(chargebackFee == 20.0)
      {
        betSystem = "1225";
      }
      else if(chargebackFee == 30.0)
      {
        betSystem = "1102";
      }
      else if(chargebackFee == 35.0)
      {
        betSystem = "1103";
      }
    }
    else
    {
      // use normal chargeback system bets
      setSystemBet();
    }
  }
  
  private void setSystemBet()
  {
    if(chargebackFee == 0.0)
    {
      betSystem = "1001";
    }
    else if(chargebackFee == 9.0)
    {
      betSystem = "1009";
    }
    else if(chargebackFee == 10.0)
    {
      betSystem = "1010";
    }
    else if(chargebackFee == 12.0)
    {
      betSystem = "1012";
    }
    else if(chargebackFee == 15.0)
    {
      betSystem = "1015";
    }
    else if(chargebackFee == 15.25)
    {
      betSystem = "1125";
    }
    else if(chargebackFee == 20.0)
    {
      betSystem = "1020";
    }
    else if(chargebackFee == 22.0)
    {
      betSystem = "1022";
    }
    else if(chargebackFee == 25.0)
    {
      betSystem = "1025";
    }
    else if(chargebackFee == 29.0)
    {
      betSystem = "1029";
    }
    else if(chargebackFee == 30.0)
    {
      betSystem = "1030";
    }
    else if(chargebackFee == 35.0)
    {
      betSystem = "1035";
    }
  }
  
  private void setCbtNonBankCardBets(long primaryKey)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:839^7*/

//  ************************************************************
//  #sql [Ctx] it = { select cardtype_code,
//                 tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cardtype_code,\n               tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:845^7*/

      rs = it.getResultSet();
     
      while(rs.next())
      {
        double cardFee = rs.getDouble("tranchrg_per_tran");
        
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:

            if(cardFee == 0.0)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9000"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6900"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5900"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4900"));
            }
            else if(cardFee == 0.05)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9005"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6905"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5905"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4905"));
            }
            else if(cardFee == 0.06)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9006"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6906"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5906"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4906"));
            }
            else if(cardFee == 0.07)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9007"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6907"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5907"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4907"));
            }
            else if(cardFee == 0.08)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9008"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6908"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5908"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4908"));
            }
            else if(cardFee == 0.09)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9009"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6909"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5909"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4909"));
            }
            else if(cardFee == 0.10)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9010"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6910"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5910"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4910"));
            }
            else if(cardFee == 0.11)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9011"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6911"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5911"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4911"));
            }
            else if(cardFee == 0.12)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9012"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6912"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5912"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4912"));
            }
            else if(cardFee == 0.13)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9013"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6913"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5913"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4913"));
            }
            else if(cardFee == 0.14)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9014"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6914"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5914"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4914"));
            }
            else if(cardFee == 0.15)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9015"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6915"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5915"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4915"));
            }
            else if(cardFee == 0.16)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9016"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6916"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5916"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4916"));
            }
            else if(cardFee == 0.17)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9017"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6917"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5917"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4917"));
            }
            else if(cardFee == 0.18)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9018"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6918"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5918"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4918"));
            }
            else if(cardFee == 0.19)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9019"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6919"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5919"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4919"));
            }
            else if(cardFee == 0.20)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9020"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6920"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5920"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4920"));
            }
            else if(cardFee == 0.25)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9025"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6925"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5925"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4925"));
            }
            else if(cardFee == 0.30)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9030"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6930"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5930"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4930"));
            }
            else if(cardFee == 0.35)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9035"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6935"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5935"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4935"));
            }
            else if(cardFee == 0.40)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9040"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6940"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5940"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4940"));
            }
            else if(cardFee == 0.45)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9045"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6945"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5945"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4945"));
            }
            else if(cardFee == 0.50)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9050"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6950"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5950"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4950"));
            }
            else if(cardFee == 0.55)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9055"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6955"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5955"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4955"));
            }
            else if(cardFee == 0.60)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9060"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6960"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5960"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4960"));
            }
            else if(cardFee == 0.65)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9065"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6965"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5965"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4965"));
            }
            else if(cardFee == 0.70)
            {
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9070"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6970"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5970"));
              bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4970"));
            }
          break;

          case mesConstants.APP_CT_DISCOVER:
            if(cardFee == 0.05)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9705"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6705"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5705"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4705"));
            }
            else if(cardFee == 0.06)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9706"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6706"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5706"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4706"));
            }
            else if(cardFee == 0.07)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9707"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6707"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5707"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4707"));
            }
            else if(cardFee == 0.08)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9708"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6708"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5708"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4708"));
            }
            else if(cardFee == 0.09)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9709"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6709"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5709"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4709"));
            }
            else if(cardFee == 0.10)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9710"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6710"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5710"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4710"));
            }
            else if(cardFee == 0.11)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9711"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6711"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5711"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4711"));
            }
            else if(cardFee == 0.12)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9712"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6712"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5712"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4712"));
            }
            else if(cardFee == 0.13)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9713"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6713"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5713"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4713"));
            }
            else if(cardFee == 0.14)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9714"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6714"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5714"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4714"));
            }
            else if(cardFee == 0.15)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9715"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6715"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5715"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4715"));
            }
            else if(cardFee == 0.16)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9716"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6716"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5716"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4716"));
            }
            else if(cardFee == 0.17)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9717"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6717"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5717"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4717"));
            }
            else if(cardFee == 0.18)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9718"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6718"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5718"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4718"));
            }
            else if(cardFee == 0.19)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9719"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6719"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5719"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4719"));
            }
            else if(cardFee == 0.20)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9720"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6720"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5720"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4720"));
            }
            else if(cardFee == 0.25)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9725"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6725"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5725"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4725"));
            }
            else if(cardFee == 0.30)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9730"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6730"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5730"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4730"));
            }
            else if(cardFee == 0.35)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9735"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6735"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5735"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4735"));
            }
            else if(cardFee == 0.40)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9740"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6740"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5740"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4740"));
            }
            else if(cardFee == 0.45)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9745"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6745"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5745"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4745"));
            }
            else if(cardFee == 0.50)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9750"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6750"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5750"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4750"));
            }
            else if(cardFee == 0.55)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9755"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6755"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5755"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4755"));
            }
            else if(cardFee == 0.60)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9760"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6760"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5760"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4760"));
            }
            else if(cardFee == 0.65)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9765"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6765"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5765"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4765"));
            }
            else if(cardFee == 0.70)
            {
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9770"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6770"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5770"));
              bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4770"));
            }
          break;

          case mesConstants.APP_CT_JCB:

            if(cardFee == 0.05)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9605"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6605"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5605"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4605"));
            }
            else if(cardFee == 0.06)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9606"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6606"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5606"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4606"));
            }
            else if(cardFee == 0.07)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9607"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6607"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5607"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4607"));
            }
            else if(cardFee == 0.08)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9608"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6608"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5608"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4608"));
            }
            else if(cardFee == 0.09)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9609"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6609"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5609"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4609"));
            }
            else if(cardFee == 0.10)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9610"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6610"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5610"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4610"));
            }
            else if(cardFee == 0.11)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9611"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6611"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5611"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4611"));
            }
            else if(cardFee == 0.12)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9612"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6612"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5612"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4612"));
            }
            else if(cardFee == 0.13)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9613"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6613"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5613"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4613"));
            }
            else if(cardFee == 0.14)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9614"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6614"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5614"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4614"));
            }
            else if(cardFee == 0.15)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9615"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6615"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5615"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4615"));
            }
            else if(cardFee == 0.16)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9616"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6616"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5616"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4616"));
            }
            else if(cardFee == 0.17)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9617"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6617"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5617"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4617"));
            }
            else if(cardFee == 0.18)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9618"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6618"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5618"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4618"));
            }
            else if(cardFee == 0.19)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9619"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6619"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5619"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4619"));
            }
            else if(cardFee == 0.20)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9620"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6620"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5620"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4620"));
            }
            else if(cardFee == 0.25)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9625"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6625"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5625"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4625"));
            }
            else if(cardFee == 0.30)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9630"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6630"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5630"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4630"));
            }
            else if(cardFee == 0.35)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9635"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6635"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5635"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4635"));
            }
            else if(cardFee == 0.40)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9640"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6640"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5640"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4640"));
            }
            else if(cardFee == 0.45)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9645"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6645"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5645"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4645"));
            }
            else if(cardFee == 0.50)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9650"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6650"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5650"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4650"));
            }
            else if(cardFee == 0.55)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9655"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6655"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5655"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4655"));
            }
            else if(cardFee == 0.60)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9660"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6660"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5660"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4660"));
            }
            else if(cardFee == 0.65)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9665"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6665"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5665"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4665"));
            }
            else if(cardFee == 0.70)
            {
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9670"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6670"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5670"));
              bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4670"));
            }
          break;

          case mesConstants.APP_CT_DINERS_CLUB:

            if(cardFee == 0.05)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9805"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6805"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5805"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4805"));
            }
            else if(cardFee == 0.06)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9806"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6806"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5806"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4806"));
            }
            else if(cardFee == 0.07)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9807"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6807"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5807"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4807"));
            }
            else if(cardFee == 0.08)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9808"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6808"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5808"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4808"));
            }
            else if(cardFee == 0.09)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9809"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6809"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5809"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4809"));
            }
            else if(cardFee == 0.10)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9810"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6810"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5810"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4810"));
            }
            else if(cardFee == 0.11)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9811"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6811"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5811"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4811"));
            }
            else if(cardFee == 0.12)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9812"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6812"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5812"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4812"));
            }
            else if(cardFee == 0.13)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9813"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6813"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5813"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4813"));
            }
            else if(cardFee == 0.14)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9814"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6814"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5814"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4814"));
            }
            else if(cardFee == 0.15)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9815"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6815"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5815"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4815"));
            }
            else if(cardFee == 0.16)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9816"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6816"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5816"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4816"));
            }
            else if(cardFee == 0.17)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9817"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6817"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5817"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4817"));
            }
            else if(cardFee == 0.18)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9818"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6818"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5818"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4818"));
            }
            else if(cardFee == 0.19)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9819"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6819"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5819"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4819"));
            }
            else if(cardFee == 0.20)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9820"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6820"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5820"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4820"));
            }
            else if(cardFee == 0.25)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9825"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6825"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5825"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4825"));
            }
            else if(cardFee == 0.30)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9830"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6830"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5830"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4830"));
            }
            else if(cardFee == 0.35)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9835"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6835"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5835"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4835"));
            }
            else if(cardFee == 0.40)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9840"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6840"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5840"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4840"));
            }
            else if(cardFee == 0.45)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9845"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6845"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5845"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4845"));
            }
            else if(cardFee == 0.50)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9850"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6850"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5850"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4850"));
            }
            else if(cardFee == 0.55)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9855"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6855"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5855"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4855"));
            }
            else if(cardFee == 0.60)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9860"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6860"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5860"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4860"));
            }
            else if(cardFee == 0.65)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9865"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6865"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5865"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4865"));
            }
            else if(cardFee == 0.70)
            {
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "9870"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "6870"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,       "5870"));
              bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "4870"));
            }
          break;
        }

      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setCbtNonBankCardBets: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setBbtVoiceAruBets()
  {

    //always use vital voice auth bet
    if(true)//this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
    {
      if(voiceAuthFee == 0.0)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "3902"));
      }
      else if(voiceAuthFee == 0.25)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "3877"));
      }
      else if(voiceAuthFee == 0.35)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4156"));
      }
      else if(voiceAuthFee == 0.45)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "3800"));
      }
      else if(voiceAuthFee == 0.50)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "3987"));
      }
      else if(voiceAuthFee == 0.55)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "3807"));
      }
      else if(voiceAuthFee == 0.62)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4155"));
      }
      else if(voiceAuthFee == 0.65)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4891"));
      }
      else if(voiceAuthFee == 0.75)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4892"));
      }
      else if(voiceAuthFee == 0.95)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4885"));
      }
      else if(voiceAuthFee == 1.0)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "4904"));
      }
    }
    else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
    {
      if(voiceAuthFee == 0.45)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, VOICE_MEDIA,  "3600"));
      }
      else if(voiceAuthFee == 0.50)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, VOICE_MEDIA,  "3100"));
      }
      else if(voiceAuthFee == 0.55)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, VOICE_MEDIA,  "3101"));
      }
      else if(voiceAuthFee == 0.65)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, VOICE_MEDIA,  "3104"));
      }
      else if(voiceAuthFee == 0.75)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, VOICE_MEDIA,  "3102"));
      }
    }
  }


  private void setCbtVoiceAuthFeeBets()
  {
    if(voiceAuthFee == 0.47)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0247"));
    }
    else if(voiceAuthFee == 0.50)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0250"));
    }
    else if(voiceAuthFee == 0.55)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0255"));
    }
    else if(voiceAuthFee == 0.60)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0260"));
    }
    else if(voiceAuthFee == 0.65)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0265"));
    }
    else if(voiceAuthFee == 0.70)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0270"));
    }
    else if(voiceAuthFee == 0.75)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,  "0275"));
    }
  }

  private void setCbtAruAuthFeeBets()
  {
    if(aruAuthFee == 0.32)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0332"));
    }
    else if(aruAuthFee == 0.35)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0335"));
    }
    else if(aruAuthFee == 0.40)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0340"));
    }
    else if(aruAuthFee == 0.45)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0345"));
    }
    else if(aruAuthFee == 0.50)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0350"));
    }
    else if(aruAuthFee == 0.55)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0355"));
    }
    else if(aruAuthFee == 0.60)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0360"));
    }
    else if(aruAuthFee == 0.65)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0365"));
    }
    else if(aruAuthFee == 0.70)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0370"));
    }
    else if(aruAuthFee == 0.75)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,  "0375"));
    }
  }

  private boolean setImsDialPayFeeBets(long primaryKey)
  {
    boolean isDialPay = false;
    
    if(getDialPayFee(primaryKey))
    {
      if(this.dialPayFee == 0.55)
      {
        isDialPay = true;

        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA, "8055"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,   "6055"));
      }
      else if(this.dialPayFee == 0.40)
      {
        isDialPay = true;
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA, "8040"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,   "6040"));
      }
    }
    return isDialPay;
  }

  private boolean setInternetBet(long primaryKey)
  {
    boolean isInternet = false;

    if(getInternetFee(primaryKey))
    {
      isInternet = true;
      
      if(internetFee == 0.00)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3000"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2000"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7000"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9000"));
      }
      else if(internetFee == 0.05)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3005"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2005"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7005"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9005"));
      }
      else if(internetFee == 0.10)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3010"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2010"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7010"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9010"));
      }
      else if(internetFee == 0.15)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3015"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2015"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7015"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9015"));
      }
      else if(internetFee == 0.20)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3020"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2020"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7020"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9020"));
      }
      else if(internetFee == 0.25)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7025"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9025"));
      }
      else if(internetFee == 0.30)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7030"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9030"));
      }
    }
    return isInternet;
  }

  private void setCbtDebitBet(long primaryKey)
  {
    if(getDebitFee(primaryKey))
    {
      if(debitFee == 0.37)
      {
        betDebit = "7032";
      }
      else if(debitFee == 0.40)
      {
        betDebit = "7035";
      }
      else if(debitFee == 0.45)
      {
        betDebit = "7040";
      }
      else if(debitFee == .50)
      {
        betDebit = "7045";
      }
      else if(debitFee == 0.55)
      {
        betDebit = "7050";
      }
      else if(debitFee == 0.60)
      {
        betDebit = "7055";
      }
      else if(debitFee == 0.65)
      {
        betDebit = "7060";
      }
      else if(debitFee == .70)
      {
        betDebit = "7065";
      }
      else if(debitFee == 0.75)
      {
        betDebit = "7070";
      }
      else if(debitFee == 0.80)
      {
        betDebit = "7075";
      }
    }
  }

  private void setMountainWestInterchangeBets()
  {
    if(this.planType == mesConstants.APP_PS_FIXED_PLUS_PER_ITEM)
    {
      if(this.betTypeCode == mesConstants.APP_BET_TYPE_RETAIL)
      {
        betVisa                 = "4050";
        betMC                   = "5050";
      }
      else if(this.betTypeCode == mesConstants.APP_BET_TYPE_MOTO)
      {
        betVisa                 = "4054";
        betMC                   = "5054";
      }
    }
    else if(this.planType == mesConstants.APP_PS_INTERCHANGE)
    {
      betVisa                   = "4001";
      betMC                     = "5001";
    }
    else if(this.planType == mesConstants.APP_PS_MTWEST_4090_5090)
    {
      betVisa                   = "4090";
      betMC                     = "5090";
    }
    else if(this.planType == mesConstants.APP_PS_MTWEST_4178_5178)
    {
      betVisa                   = "4178";
      betMC                     = "5178";
    }
    else if(this.planType == mesConstants.APP_PS_MTWEST_4179_5179)
    {
      betVisa                   = "4179";
      betMC                     = "5179";
    }
    else if(this.planType == mesConstants.APP_PS_MTWEST_4180_5180)
    {
      betVisa                   = "4180";
      betMC                     = "5180";
    }
  }

  private void setBbtInterchangeBets(long primaryKey)
  {
    indBetVisa              = "*";
    indBetMC                = "*";
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
  
    try
    {
      connect();

      if(this.betTypeCode == 999999)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1960^9*/

//  ************************************************************
//  #sql [Ctx] it = { select vs_other_bet,
//                   mc_other_bet 
//            from   app_merch_bbt
//            where  app_seq_num = :primaryKey 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select vs_other_bet,\n                 mc_other_bet \n          from   app_merch_bbt\n          where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1966^9*/

        rs = it.getResultSet();
     
        if(rs.next())
        {
          betVisa = rs.getString("vs_other_bet");
          betMC   = rs.getString("mc_other_bet");
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1978^9*/

//  ************************************************************
//  #sql [Ctx] it = { select vs_bet,
//                   mc_bet 
//            from   bbt_interchange_bets
//            where  bet_type = :betTypeCode 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select vs_bet,\n                 mc_bet \n          from   bbt_interchange_bets\n          where  bet_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1984^9*/

        rs = it.getResultSet();
     
        if(rs.next())
        {
          betVisa = rs.getString("vs_bet");
          betMC   = rs.getString("mc_bet");
        }
      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setBbtInterchangeBets: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void setCbtCheckCardInterchange(long appSeqNum)
  {
    boolean             checkCard = false;
    double              vDiscRate = 0.0;
    double              vPerItem  = 0.0;
    double              cDiscRate = 0.0;
    double              cPerItem  = 0.0;
    
    try
    {
      // see if passthru interchange was chosen
      if(interchangeTypeCode == 1 && (interchangeFeeCode == 1 || interchangeFeeCode == 2))
      {
        int checkCardCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2022^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(cardtype_code)
//            
//            from    tranchrg
//            where   app_seq_num = :appSeqNum and
//                    cardtype_code = :mesConstants.APP_CT_VISA_CHECK_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cardtype_code)\n           \n          from    tranchrg\n          where   app_seq_num =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_CHECK_CARD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   checkCardCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2029^9*/
        
        if(checkCardCount > 0)
        {
          // now see if separate check card rate was chosen
          /*@lineinfo:generated-code*//*@lineinfo:2034^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(tranchrg_disc_rate, 0),
//                      nvl(tranchrg_pass_thru, 0)
//              
//              from    tranchrg
//              where   app_seq_num = :appSeqNum and
//                      cardtype_code = :mesConstants.APP_CT_VISA
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(tranchrg_disc_rate, 0),\n                    nvl(tranchrg_pass_thru, 0)\n             \n            from    tranchrg\n            where   app_seq_num =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vDiscRate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vPerItem = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2043^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:2045^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(tranchrg_disc_rate, 0),
//                      nvl(tranchrg_pass_thru, 0)
//              
//              from    tranchrg
//              where   app_seq_num = :appSeqNum and
//                      cardtype_code = :mesConstants.APP_CT_VISA_CHECK_CARD
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(tranchrg_disc_rate, 0),\n                    nvl(tranchrg_pass_thru, 0)\n             \n            from    tranchrg\n            where   app_seq_num =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_CHECK_CARD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cDiscRate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cPerItem = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2054^11*/
          
          if(vDiscRate != cDiscRate || vPerItem != cPerItem)
          {
            // need separate bet
            if(interchangeFeeCode == 1) // retail
            {
              betVisa = "1310";
              betMC   = "1470";
            }
            else                        // moto
            {
              betVisa = "1311";
              betMC   = "1471";
            }
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setCbtCheckCardInterchange()", e.toString());
    }
  }

  private void setFeeSetInterchangeBets()
  {
    indBetVisa  = "0001";
    indBetMC    = "0001";
    
    try
    {
      // retrieve bet numbers from appo_ic_bets table
      /*@lineinfo:generated-code*//*@lineinfo:2087^7*/

//  ************************************************************
//  #sql [Ctx] { select  lpad(to_char(vs_bet), 4, '0'),
//                  lpad(to_char(mc_bet), 4, '0')
//          
//          from    appo_ic_bets
//          where   bet_type = :interchangeTypeCode and
//                  combo_id = :interchangeFeeCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lpad(to_char(vs_bet), 4, '0'),\n                lpad(to_char(mc_bet), 4, '0')\n         \n        from    appo_ic_bets\n        where   bet_type =  :1  and\n                combo_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,interchangeTypeCode);
   __sJT_st.setInt(2,interchangeFeeCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   betVisa = (String)__sJT_rs.getString(1);
   betMC = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2096^7*/
    }
    catch(Exception e)
    {
      logEntry("setFeeSetInterchangeBets(" + interchangeTypeCode + ", " + interchangeFeeCode + ")", e.toString());
    }
  }

  private void setElmNonDeployInterchangeBets()
  {
    if(this.planType == mesConstants.ELM_APP_PS_RATE_PER_ITEM || this.planType == mesConstants.ELM_APP_PS_RATE_ONLY || this.planType == mesConstants.ELM_APP_PS_TIERED)
    {
      if(this.betTypeCode == 1) //retail or restaurant
      {
        betVisa                 = "4067";
        betMC                   = "5067";
      }
      else if(this.betTypeCode == 2) //moto or dialpay
      {
        betVisa                 = "4066";
        betMC                   = "5066";
      }
      else if(this.betTypeCode == 3) //hotel/lodging
      {
        betVisa                 = "4065";
        betMC                   = "5065";
      }
      else if(this.betTypeCode == 4) //supermarket
      {
        betVisa                 = "4063";
        betMC                   = "5063";
      }

    }
    else if(this.planType == mesConstants.ELM_APP_PS_FLAT_RATE)
    {
      betVisa                   = "4064";
      betMC                     = "5064";
    }
    else if(this.planType == mesConstants.ELM_APP_PS_BUY_RATE_PLUS)
    {
      // special buy rate ic bets
      betVisa                   = "4131";
      betMC                     = "5131";
    }
  }
  
  private void setGoldInterchangeBets()
  {
    if(this.planType == mesConstants.APP_PS_INTERCHANGE)
    {
      // interchange pass-thru
      betVisa         = "4001";
      betMC           = "5001";
    }
    else
    {
      switch(pricingGrid)
      {
        case mesConstants.APP_PG_RETAIL:
        default:
          betVisa     = "4030";
          betMC       = "5030";
          break;
        
        case mesConstants.APP_PG_MOTO:
          betVisa     = "4031";
          betMC       = "5031";
          break;
        
        case mesConstants.APP_PG_HOTEL:
          betVisa     = "4009";
          betMC       = "5009";
          break;
      }
    }
  }

  private void setBannerInterchangeBets()
  {
    if(this.planType == mesConstants.APP_PS_FIXED_PLUS_PER_ITEM)
    {
      if(this.betTypeCode == 1) //mid 0.75 non 1.40
      {
        betVisa                 = "4074";
        betMC                   = "5074";
      }
      else if(this.betTypeCode == 2) //mid 1.10 non 1.40
      {
        betVisa                 = "4075";
        betMC                   = "5075";
      }
      else if(this.betTypeCode == 3) //mid 0.90 non 1.30
      {
        betVisa                 = "4076";
        betMC                   = "5076";
      }
      else if(this.betTypeCode == 4) //mid 0.80 non 1.25
      {
        betVisa                 = "4077";
        betMC                   = "5077";
      }
      else if(this.betTypeCode == 5) //mid 0.70 non 1.10
      {
        betVisa                 = "4078";
        betMC                   = "5078";
      }
      else if(this.betTypeCode == 6) //mid 0.65 non 1.10
      {
        betVisa                 = "4079";
        betMC                   = "5079";
      }
      else if(this.betTypeCode == 7) //mid 0.60 non 1.05
      {
        betVisa                 = "4080";
        betMC                   = "5080";
      }
      else if(this.betTypeCode == 8) //dial pay/ no interchange
      {
        betVisa                 = "4000";
        betMC                   = "5000";
      }
      else if(this.betTypeCode == 9) //0.75/1.20
      {
        betVisa                 = "4085";
        betMC                   = "5085";
      }
      else if(this.betTypeCode == 10) //0.55/0.90
      {
        betVisa                 = "4086";
        betMC                   = "5086";
      }
      else if(this.betTypeCode == 11) //0.50/0.95
      {
        betVisa                 = "4087";
        betMC                   = "5087";
      }
      else if(this.betTypeCode == 12) //0.40/0.85
      {
        betVisa                 = "4088";
        betMC                   = "5088";
      }
      else if(this.betTypeCode == 13) //0.10/0.66
      {
        betVisa                 = "4089";
        betMC                   = "5089";
      }
      else if(this.betTypeCode == 14) // MOTO .95 / Non-Qual
      {
        betVisa                 = "4084";
        betMC                   = "5084";
      }
      else if(this.betTypeCode == 15) // Interchange pass-thru
      {
        betVisa                 = "4001";
        betMC                   = "5001";
      }
      else if(this.betTypeCode == 20)
      {
        betVisa                 = "4141";
        betMC                   = "5141";
      }
      else if(this.betTypeCode == 21)
      {
        betVisa                 = "4142";
        betMC                   = "5142";
      }
      else if(this.betTypeCode == 22)
      {
        betVisa                 = "4143";
        betMC                   = "5143";
      }
      else if(this.betTypeCode == 23)
      {
        betVisa                 = "4144";
        betMC                   = "5144";
      }
      else if(this.betTypeCode == 24)
      {
        betVisa                 = "4148";
        betMC                   = "5148";
      }
      else if(this.betTypeCode == 25)
      {
        betVisa                 = "4145";
        betMC                   = "5145";
      }
      else if(this.betTypeCode == 26)
      {
        betVisa                 = "4146";
        betMC                   = "5146";
      }
      else if(this.betTypeCode == 27)
      {
        betVisa                 = "4147";
        betMC                   = "5147";
      }
      else if(this.betTypeCode == 28)
      {
        betVisa                 = "4149";
        betMC                   = "5149";
      }
      else if(this.betTypeCode == 29)
      {
        betVisa                 = "4150";
        betMC                   = "5150";
      }
      else if(this.betTypeCode == 30)
      {
        betVisa                 = "4151";
        betMC                   = "5151";
      }
      else if(this.betTypeCode == 31)
      {
        betVisa                 = "4152";
        betMC                   = "5152";
      }
      else if(this.betTypeCode == 32)
      {
        betVisa                 = "4153";
        betMC                   = "5153";
      }
      else if(this.betTypeCode == 33)
      {
        betVisa                 = "4127";
        betMC                   = "5127";
      }
      else if(this.betTypeCode == 34)
      {
        betVisa                 = "4128";
        betMC                   = "5128";
      }
      else if(this.betTypeCode == 35)
      {
        betVisa                 = "4083";
        betMC                   = "5083";
      }
      else if(this.betTypeCode == 36)
      {
        betVisa                 = "4100";
        betMC                   = "5100";
      }
    }
    else if(this.planType == mesConstants.APP_PS_INTERCHANGE)
    {
      betVisa                   = "4001";
      betMC                     = "5001";
    }
  }

  private void setImsInterchangeBets()
  {
    if(this.planType == mesConstants.IMS_RETAIL_RESTAURANT_PLAN)
    {
      betVisa                 = "4070";
      betMC                   = "5070";
    }
    else if(this.planType == mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN)
    {
      betVisa                 = "4071";
      betMC                   = "5071";
    }
  }

  private void setBbtMet(long primaryKey)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2369^7*/

//  ************************************************************
//  #sql [Ctx] it = { select nvl(mbs.met_table,babn.met_table)    as met_table
//          from   merchant                   mr,
//                 bbt_app_business_nature    babn,
//                 bbt_met_table_by_sic       mbs
//          where  mr.app_seq_num    = :primaryKey and
//                 babn.value = mr.business_nature and
//                 mbs.sic_code(+) = mr.sic_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select nvl(mbs.met_table,babn.met_table)    as met_table\n        from   merchant                   mr,\n               bbt_app_business_nature    babn,\n               bbt_met_table_by_sic       mbs\n        where  mr.app_seq_num    =  :1  and\n               babn.value = mr.business_nature and\n               mbs.sic_code(+) = mr.sic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2378^7*/
      rs = it.getResultSet();
     
      if(rs.next())
      {
        metTable = isBlank(rs.getString("met_table")) ? "" : rs.getString("met_table");
      }
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setBbtMet: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  private void setBbtNonBankCardWirelessBets(long primaryKey)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2408^7*/

//  ************************************************************
//  #sql [Ctx] it = { select cardtype_code,
//                 tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cardtype_code,\n               tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2414^7*/

      rs = it.getResultSet();
     
      while(rs.next())
      {
        double cardFee = rs.getDouble("tranchrg_per_tran");
        
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == 0.15)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3824"));
              }
              else if(cardFee == 0.19)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "4190"));
              }
              else if(cardFee == 0.20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3873"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == 0.15)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4178"));
              }
              else if(cardFee == 0.20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4301"));
              }
            }
          break;

          case mesConstants.APP_CT_DISCOVER:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {
 
              if(cardFee == .15)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3823"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "4189"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3874"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .15)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4177"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4199"));
              }
            }

          break;

          case mesConstants.APP_CT_JCB:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == .15)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3893"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "4191"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3872"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .15)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4179"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4302"));
              }
            }

          break;

          case mesConstants.APP_CT_DINERS_CLUB:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == .15)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3892"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "4188"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3875"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .15)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4164"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4198"));
              }
            }

          break;

        }

      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setBbtNonBankCardWirelessBets: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  
  }




  private void setBbtNonBankCardBets(long primaryKey)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2585^7*/

//  ************************************************************
//  #sql [Ctx] it = { select cardtype_code,
//                 tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cardtype_code,\n               tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2591^7*/

      rs = it.getResultSet();
     
      while(rs.next())
      {
        double cardFee = rs.getDouble("tranchrg_per_tran");
        
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {
              if(cardFee == 0.05)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3814"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3812"));
              }
              else if(cardFee == .075)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3910"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3911"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3708"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3709"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3887"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3881"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3970"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3964"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3875"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3869"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3946"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3940"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3996"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3990"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3958"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3952"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3934"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3928"));
              }
              else if(cardFee == .75)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3613"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3611"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
            {
              if(cardFee == 0.05)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4814"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4815"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4171"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4172"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4922"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4916"));
              }
              else if(cardFee == .12)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4875"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4869"));
              }
              else if(cardFee == .13)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4886"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4880"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4972"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4966"));
              }
              else if(cardFee == .17)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4934"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4928"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4910"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4904"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4948"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4942"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4996"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4990"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4960"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4954"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4851"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4845"));
              }
              else if(cardFee == .50)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4708"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4709"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
            {

              if(cardFee == .10)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4214"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4215"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4132"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4131"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4120"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4119"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4108"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4107"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4208"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4209"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4153"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4154"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4087"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4093"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4027"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4033"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4051"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4057"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4003"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4009"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4039"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4044"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4063"));
                bets.add(new Bet(AMEX_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4069"));
              }
            }
          break;

          case mesConstants.APP_CT_DISCOVER:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3816"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3815"));
              }
              else if(cardFee == .075)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3908"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3909"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3706"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3707"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3888"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3882"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3971"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3965"));
              }
              else if(cardFee == .17)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3923"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3917"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3876"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3870"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3947"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3941"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3997"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3991"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3959"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3953"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3935"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3929"));
              }
              else if(cardFee == .75)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3603"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3610"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
            {
              if(cardFee == .05)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4812"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4813"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4169"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4170"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4923"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4917"));
              }
              else if(cardFee == .12)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4876"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4870"));
              }
              else if(cardFee == .13)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4887"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4881"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4973"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4967"));
              }
              else if(cardFee == .17)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4935"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4929"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4911"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4905"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4949"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4943"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4997"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4991"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4961"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4955"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4852"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4846"));
              }
              else if(cardFee == .50)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4705"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4706"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
            {

              if(cardFee == .10)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4220"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4221"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4134"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4133"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4140"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4121"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4110"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4109"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4206"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4207"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4151"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4152"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4086"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4092"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4026"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4032"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4050"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4056"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4002"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4008"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4038"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4043"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4062"));
                bets.add(new Bet(DISCOVER_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4068"));
              }
            }

          break;

          case mesConstants.APP_CT_JCB:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3809"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3808"));
              }
              else if(cardFee == .075)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3896"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3897"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3886"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3880"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3969"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3963"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3874"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3868"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3945"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3939"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3995"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3989"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3957"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3951"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3933"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3927"));
              }
              else if(cardFee == .75)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3614"));
                bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3615"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4816"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4817"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4173"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4174"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4921"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4915"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4971"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4965"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4909"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4903"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4947"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4941"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4995"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4989"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4959"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4953"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4850"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4844"));
              }
              else if(cardFee == .50)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4710"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4711"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
            {

              if(cardFee == .10)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4222"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4223"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4130"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4129"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4118"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4117"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4106"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4105"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4210"));
                bets.add(new Bet(JCB_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4211"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .10)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4088"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4094"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4028"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4034"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4052"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4058"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4004"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4010"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4040"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4045"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4064"));
                bets.add(new Bet(JCB_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4070"));
              }
            }

          break;

          case mesConstants.APP_CT_DINERS_CLUB:

            if(this.processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3818"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3817"));
              }
              else if(cardFee == .075)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3906"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3907"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3703"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3704"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3889"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3883"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3972"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3966"));
              }
              else if(cardFee == .17)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3924"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3918"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3948"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3942"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3998"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3992"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3960"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3954"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3936"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3930"));
              }
              else if(cardFee == .75)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3612"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3609"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4810"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4811"));
              }
              else if(cardFee == .08)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4167"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "3168"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4924"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4918"));
              }
              else if(cardFee == .13)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4888"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4882"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4974"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4968"));
              }
              else if(cardFee == .17)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4936"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4930"));
              }
              else if(cardFee == .19)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4912"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4906"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4950"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4944"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4998"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4992"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4962"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4956"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4853"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4847"));
              }
              else if(cardFee == .50)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4703"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,      "4704"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
            {

              if(cardFee == .10)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4218"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4219"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4136"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4135"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4124"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4123"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4112"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4111"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4204"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,      "4205"));
              }
            }
            else if(this.processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {

              if(cardFee == .05)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4148"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4149"));
              }
              else if(cardFee == .10)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4085"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4091"));
              }
              else if(cardFee == .15)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4025"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4031"));
              }
              else if(cardFee == .20)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4049"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4055"));
              }
              else if(cardFee == .25)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4001"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4007"));
              }
              else if(cardFee == .30)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4037"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4047"));
              }
              else if(cardFee == .35)
              {
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4061"));
                bets.add(new Bet(DINERS_PLAN, VENDOR_PAYTECH, ECR_MEDIA,      "4067"));
              }
            }

          break;

        }

      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setBbtNonBankCardBets: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  
  }

  private boolean getDebitFee(long primaryKey)
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    boolean             hadDebitFee   = false;
    try
    {
      connect();

      //get debit fee
      /*@lineinfo:generated-code*//*@lineinfo:3404^7*/

//  ************************************************************
//  #sql [Ctx] it = { select tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey and 
//                 cardtype_code  = :mesConstants.APP_CT_DEBIT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1  and \n               cardtype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_DEBIT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3410^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        this.debitFee = rs.getDouble("tranchrg_per_tran");
        hadDebitFee   = true;
      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDebitFee: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return hadDebitFee;
  }


  private boolean getInternetFee(long primaryKey)
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    boolean             hadInternetFee  = false;

    try
    {
      connect();

      //get internet fee
      /*@lineinfo:generated-code*//*@lineinfo:3447^7*/

//  ************************************************************
//  #sql [Ctx] it = { select tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey and 
//                 cardtype_code  = :mesConstants.APP_CT_INTERNET
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1  and \n               cardtype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3453^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        this.internetFee = rs.getDouble("tranchrg_per_tran");
        hadInternetFee   = true;
      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getInternetFee: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return hadInternetFee;
  }


  private boolean getDialPayFee(long primaryKey)
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    boolean             hadDialPayFee  = false;

    try
    {
      connect();

      //get dialPay fee
      /*@lineinfo:generated-code*//*@lineinfo:3490^7*/

//  ************************************************************
//  #sql [Ctx] it = { select tranchrg_per_tran 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey and 
//                 cardtype_code  = :mesConstants.APP_CT_DIAL_PAY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tranchrg_per_tran \n        from   tranchrg\n        where  app_seq_num    =  :1  and \n               cardtype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_DIAL_PAY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3496^7*/

      rs = it.getResultSet();
     
      if(rs.next())
      {
        voiceAuthFee = rs.getDouble("tranchrg_per_tran");
        hadDialPayFee   = true;
      }
     
      it.close();
     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDialPayFee: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return hadDialPayFee;
  }

  private void setBbtDebitBet(long primaryKey)
  {
    if(getDebitFee(primaryKey))
    {

      switch(this.processor)
      {
        case mesConstants.APP_PROCESSOR_TYPE_VITAL:
      
          if(debitFee == .19)
          {
            betDebit = "7002";
          }
          else if(debitFee == .20)
          {
            betDebit = "4079";
          }
          else if(debitFee == .25)
          {
            betDebit = "4097";
          }
          else if(debitFee == .30)
          {
            betDebit = "7005";
          }
          else if(debitFee == .35)
          {
            betDebit = "7000";
          }
          else if(debitFee == .40)
          {
            betDebit = "7001";
          }
          else if(debitFee == .45)
          {
            betDebit = "7003";
          }
          else if(debitFee == .50)
          {
            betDebit = "7004";
          }
          else if(debitFee == .55)
          {
            betDebit = "7007";
          }
        
        break;  
      
        case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL:
          if(debitFee == .20)
          {
            betDebit = "4078";
          }
          else if(debitFee == .25)
          {
            betDebit = "4077";
          }
          else if(debitFee == .30)
          {
            betDebit = "4076";
          }
          else if(debitFee == .35)
          {
            betDebit = "4095";
          }
          else if(debitFee == .40)
          {
            betDebit = "4181";
          }
          else if(debitFee == .45)
          {
            betDebit = "3501";
          }
          else if(debitFee == .50)
          {
            betDebit = "3503";
          }
          else if(debitFee == .55)
          {
            betDebit = "7006";
          }
        
        break;

        case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST:
        
          if(debitFee == .40)
          {
            betDebit = "5502";
          }
          else if(debitFee == .45)
          {
            betDebit = "5501";
          }
          else if(debitFee == .50)
          {
            betDebit = "5503";
          }
        
        break;
      
        case mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH:

          if(debitFee == .20)
          {
            betDebit = "4227";
          }
          else if(debitFee == .25)
          {
            betDebit = "4228";
          }
          else if(debitFee == .45)
          {
            betDebit = "4226";
          }
        
        break;
    
      }


    }
  }

  private void setTranscomDebitBet(long primaryKey)
  {
     
    if(getDebitFee(primaryKey))
    {
      if(debitFee == 0.0)
      {
        betDebit = "0400";
      }
      else if(debitFee == .32)
      {
        betDebit = "0432";
      }
      else if(debitFee == .34)
      {
        betDebit = "0434";
      }
      else if(debitFee == .36)
      {
        betDebit = "0436";
      }
      else if(debitFee == .38)
      {
        betDebit = "0438";
      }
      else if(debitFee == .40)
      {
        betDebit = "0430";
      }
      else if(debitFee == .42)
      {
        betDebit = "0432";
      }
      else if(debitFee == .44)
      {
        betDebit = "0434";
      }
      else if(debitFee == .45)
      {
        betDebit = "0435";
      }
      else if(debitFee == .46)
      {
        betDebit = "0436";
      }
      else if(debitFee == .48)
      {
        betDebit = "0448";
      }
      else if(debitFee == .50)
      {
        betDebit = "0450";
      }
      else if(debitFee == .53)
      {
        betDebit = "0453";
      }
      else if(debitFee == .55)
      {
        betDebit = "0445";
      }
      else if(debitFee == .58)
      {
        betDebit = "0048";
      }
      else if(debitFee == .60)
      {
        betDebit = "0450";
      }
      else if(debitFee == .63)
      {
        betDebit = "0453";
      }
      else if(debitFee == .65)
      {
        betDebit = "0456";
      }
      else if(debitFee == .70)
      {
        betDebit = "0460";
      }
      else if(debitFee == .75)
      {
        betDebit = "0466";
      }
      else if(debitFee == .80)
      {
        betDebit = "0470";
      }
      else if(debitFee == .85)
      {
        betDebit = "0476";
      }
    }
  }
  
  private void setDebitBet(long primaryKey)
  {
    if(getDebitFee(primaryKey))
    {
      if(debitFee == 0.0)
      {
        betDebit = "0300";
      }
      if(debitFee == 0.40)
      {
        if(this.appType == mesConstants.APP_TYPE_MOUNTAIN_WEST)
        {
          // Mountain west gets a special at .40 debit for some reason
          betDebit = "3040";
        }
        else
        {
          betDebit = "0340";
        }
      }
      else if(debitFee == .45)
      {
        betDebit = "0345";
      }
      else if(debitFee == 0.50)
      {
        betDebit = "3050";
      }
      else if(debitFee == 0.52)
      {
        betDebit = "3052";
      }
      else if(debitFee == 0.55)
      {
        betDebit = "3055";
      }
      else if(debitFee == 0.59)
      {
        betDebit = "0459";
      }
      else if(debitFee == 0.60)
      {
        betDebit = "3060";
      }
      else if(debitFee == 0.61)
      {
        betDebit = "0461";
      }
      else if(debitFee == 0.62)
      {
        betDebit = "3062";
      }
      else if(debitFee == 0.63)
      {
        betDebit = "3063";
      }
      else if(debitFee == 0.64)
      {
        betDebit = "0464";
      }
      else if(debitFee == 0.65)
      {
        betDebit = "3065";
      }
      else if(debitFee == 0.66)
      {
        betDebit = "0366";
      }
      else if(debitFee == 0.67)
      {
        betDebit = "0467";
      }
      else if(debitFee == 0.68)
      {
        betDebit = "0468";
      }
      else if(debitFee == 0.69)
      {
        betDebit = "0469";
      }
      else if(debitFee == 0.70)
      {
        betDebit = "3070";
      }
      else if(debitFee == 0.75)
      {
        betDebit = "3075";
      }
      else if(debitFee == 0.76)
      {
        betDebit = "3076";
      }
      else if(debitFee == 0.80)
      {
        betDebit = "0480";
      }
      else if(debitFee == 0.85)
      {
        betDebit = "0484";
      }
      else if(debitFee == 0.90)
      {
        betDebit = "0490";
      }
      else if(debitFee == 0.95)
      {
        betDebit = "0495";
      }
      else if(debitFee == 1.00)
      {
        betDebit = "0499";
      }
      
      // set the debit auth bet to zero so they don't get billed twice
      bets.add(new Bet(DEBIT_PLAN, VENDOR_VITAL, BATCH_MEDIA,     "2800"));
      bets.add(new Bet(DEBIT_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3800"));
      bets.add(new Bet(DEBIT_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "7800"));
      bets.add(new Bet(DEBIT_PLAN, VENDOR_VITAL, FIXED_MEDIA,     "9800"));
      bets.add(new Bet(DEBIT_PLAN, VENDOR_INHOUSE, FIXED_MEDIA,   "1900"));
    }
  }
  
  private void setVoiceAuthFeeBets(long primaryKey)
  {
    String voiceBet = "";
    String aruBet   = "";
    
    if(voiceAuthFee > 0 || getDialPayFee(primaryKey))
    {
      if(voiceAuthFee == 0)
      {
        voiceBet  = "8000";
        aruBet    = "6000";
      }
      if(voiceAuthFee == 0.40)
      {
        voiceBet  = "8040";
        aruBet    = "6040";
      }
      if(voiceAuthFee == 0.45)
      {
        voiceBet  = "8045";
        aruBet    = "6045";
      }
      else if(voiceAuthFee == 0.50)
      {
        voiceBet  = "8050";
        aruBet    = "6050";
      }
      else if(voiceAuthFee == 0.55)
      {
        voiceBet  = "8055";
        aruBet    = "6055";
      }
      else if(voiceAuthFee == 0.60)
      {
        voiceBet  = "8060";
        aruBet    = "6060";
      }
      else if(voiceAuthFee == 0.65)
      {
        voiceBet  = "8065";
        aruBet    = "6065";
      }
      else if(voiceAuthFee == 0.69)
      {
        voiceBet  = "8069";
        aruBet    = "6069";
      }
      else if(voiceAuthFee == 0.70)
      {
        voiceBet  = "8070";
        aruBet    = "6070";
      }
      else if(voiceAuthFee == 0.75)
      {
        voiceBet  = "8075";
        aruBet    = "6075";
      }
      else if(voiceAuthFee == 0.80)
      {
        voiceBet  = "8080";
        aruBet    = "6080";
      }
      else if(voiceAuthFee == 0.85)
      {
        voiceBet  = "8085";
        aruBet    = "6085";
      }
      else if(voiceAuthFee == 1.0)
      {
        voiceBet  = "8010";
        aruBet    = "6010";
      }
      else if(voiceAuthFee == 2.5)
      {
        voiceBet  = "8091";
        aruBet    = "6091";
      }
    
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA, voiceBet));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,   aruBet));
    }
  }
  
  private void setWirelessBets()
  {
    String termBet    = "";
    String ecrBet     = "";
    String batchBet   = "";
    String fixedBet   = "";
    
    if(vmcAuthFee == .50)
    {
      termBet   = "7077";
      ecrBet    = "3077";
      batchBet  = "2077";
      fixedBet  = "9077";
    }
    else if(vmcAuthFee == .55)
    {
      termBet   = "7078";
      ecrBet    = "3078";
      batchBet  = "2078";
      fixedBet  = "9078";
    }
    else if(vmcAuthFee == .59)
    {
      termBet   = "7080";
      ecrBet    = "3080";
      batchBet  = "2080";
      fixedBet  = "9080";
    }
    else if(vmcAuthFee == .60)
    {
      termBet   = "7079";
      ecrBet    = "3079";
      batchBet  = "2079";
      fixedBet  = "9079";
    }
    else if(vmcAuthFee == .65)
    {
      termBet   = "7084";
      ecrBet    = "3084";
      batchBet  = "2084";
      fixedBet  = "9084";
    }
  }
  
  private void setAuthBetDB(long primaryKey, double fee, String planType)
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    int                 betFlavor = BET_FLAVOR_DEFAULT;
    try
    {
      connect();
      
      if( fee > 0.0 || planType.equals(OVERALL_PLAN) )
      {
        // determine if this app uses a special flavor
        int recCount;
        /*@lineinfo:generated-code*//*@lineinfo:4003^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abat.app_type)
//            
//            from    auth_bet_app_types abat,
//                    application app
//            where   app.app_seq_num = :primaryKey and
//                    app.app_type = abat.app_type and
//                    abat.plan_type = :planType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abat.app_type)\n           \n          from    auth_bet_app_types abat,\n                  application app\n          where   app.app_seq_num =  :1  and\n                  app.app_type = abat.app_type and\n                  abat.plan_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,planType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4012^9*/
      
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:4016^11*/

//  ************************************************************
//  #sql [Ctx] { select  abat.bet_flavor
//              
//              from    auth_bet_app_types abat,
//                      application app
//              where   app.app_seq_num = :primaryKey and
//                      app.app_type = abat.app_type and
//                      abat.plan_type = :planType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  abat.bet_flavor\n             \n            from    auth_bet_app_types abat,\n                    application app\n            where   app.app_seq_num =  :1  and\n                    app.app_type = abat.app_type and\n                    abat.plan_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,planType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   betFlavor = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4025^11*/
        }
      
        // establish auth bets from plan type and fee
        /*@lineinfo:generated-code*//*@lineinfo:4029^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ab.batch,
//                    ab.ecr,
//                    ab.term,
//                    ab.fixed,
//                    ab.trident
//            from    auth_bets ab
//            where   ab.bet_flavor = :betFlavor and
//                    ab.plan_type = :planType and
//                    ab.amount = :fee and
//                    ab.avs_fee = :avsFee
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ab.batch,\n                  ab.ecr,\n                  ab.term,\n                  ab.fixed,\n                  ab.trident\n          from    auth_bets ab\n          where   ab.bet_flavor =  :1  and\n                  ab.plan_type =  :2  and\n                  ab.amount =  :3  and\n                  ab.avs_fee =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betFlavor);
   __sJT_st.setString(2,planType);
   __sJT_st.setDouble(3,fee);
   __sJT_st.setDouble(4,avsFee);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4041^9*/
        
        rs = it.getResultSet();
        
        // reset plan type for special card type
        if(planType == VIRTERM_PLAN)
        {
          planType = OVERALL_PLAN;
        }
      
        if(rs.next())
        {
          bets.add(new Bet(planType, VENDOR_VITAL, BATCH_MEDIA,     rs.getString("batch")));
          bets.add(new Bet(planType, VENDOR_VITAL, ECR_MEDIA,       rs.getString("ecr")));
          bets.add(new Bet(planType, VENDOR_VITAL, TERMINAL_MEDIA,  rs.getString("term")));
          bets.add(new Bet(planType, VENDOR_VITAL, FIXED_MEDIA,     rs.getString("fixed")));
          bets.add(new Bet(planType, VENDOR_INHOUSE, FIXED_MEDIA,   rs.getString("trident")));
        }
      
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("setAuthBetDB(" + primaryKey + ", " + fee + ", " + planType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void setAuthBets(long primaryKey)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      connect();
      
      // if this is virtual terminal limited app then don't set standard overall v/mc auth bet
      int vtCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:4086^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merch_pos
//          where   app_seq_num = :primaryKey and
//                  pos_code = :mesConstants.APP_MPOS_VT_LIMITED
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merch_pos\n        where   app_seq_num =  :1  and\n                pos_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.AutoUploadBetInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_MPOS_VT_LIMITED);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vtCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4093^7*/
      
      if(vtCount > 0 && vmcAuthFee > -1)
      {
        setAuthBetDB(primaryKey, vmcAuthFee, VIRTERM_PLAN);
      }
      else
      {
        // visa/mastercard bets
        if(vmcAuthFee > -1)
        {
          setAuthBetDB(primaryKey, vmcAuthFee, OVERALL_PLAN);
        }
      }
      
      // non-bank card bets
      /*@lineinfo:generated-code*//*@lineinfo:4109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4115^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        double fee = rs.getDouble("tranchrg_per_tran");
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:
            setAuthBetDB(primaryKey, fee, AMEX_PLAN);
            break;
            
          case mesConstants.APP_CT_DISCOVER:
            setAuthBetDB(primaryKey, fee, DISCOVER_PLAN);
            break;
            
          case mesConstants.APP_CT_JCB:
            setAuthBetDB(primaryKey, fee, JCB_PLAN);
            break;
           
          case mesConstants.APP_CT_DINERS_CLUB:
            setAuthBetDB(primaryKey, fee, DINERS_PLAN);
            break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setAuthBets("+ primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void setCSBVisaMCAuthBets()
  {
    String  termBet   = "";
    String  ecrBet    = "";
    String  batchBet  = "";
    String  fixedBet  = "";
    
    if(vmcAuthFee == 0)
    {
      termBet = "7000";
      ecrBet = "3000";
      batchBet = "2000";
      fixedBet = "9000";
    }
    else if(vmcAuthFee == 0.05)
    {
      termBet = "7005";
      ecrBet  = "3005";
      batchBet= "2005";
      fixedBet= "9005";
    }
    else if(vmcAuthFee == 0.10)
    {
      termBet = "7010";
      ecrBet  = "3010";
      batchBet= "2010";
      fixedBet= "9010";
    }
    else if(vmcAuthFee == 0.15)
    {
      termBet = "7015";
      ecrBet = "3015";
      batchBet = "2015";
      fixedBet = "9015";
    }
    else if(vmcAuthFee == 0.20)
    {
      termBet = "7020";
      ecrBet = "3020";
      batchBet = "2020";
      fixedBet = "9020";
    }
    else if(vmcAuthFee == 0.25)
    {
      termBet = "7025";
      ecrBet = "3025";
      batchBet = "2025";
      fixedBet = "9025";
    }
    else if(vmcAuthFee == 0.30)
    {
      termBet = "7030";
      ecrBet = "3030";
      batchBet = "2030";
      fixedBet = "9030";
    }
    
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  termBet));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       ecrBet));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,     batchBet));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,     fixedBet));
  }
  
  private void setCSBNonBankCardBets(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:4226^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4232^7*/
      
      rs = it.getResultSet();
      
      String  termBet   = "";
      String  ecrBet    = "";
      String  batchBet  = "";
      String  fixedBet  = "";
      while(rs.next())
      {
        double  cardFee   = rs.getDouble("tranchrg_per_tran");
        
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:
            if(cardFee == 0)
            {
              termBet = "7100";
              ecrBet = "3100";
              batchBet = "2100";
              fixedBet = "9100";
            }
            else if(cardFee == 0.05)
            {
              termBet = "7105";
              ecrBet = "3105";
              batchBet = "2105";
              fixedBet = "9105";
            }
            else if(cardFee == 0.10)
            {
              termBet = "7110";
              ecrBet = "3110";
              batchBet = "2110";
              fixedBet = "9110";
            }
            else if(cardFee == 0.15)
            {
              termBet = "7115";
              ecrBet = "3115";
              batchBet = "2115";
              fixedBet = "9115";
            }
            else if(cardFee == 0.20)
            {
              termBet = "7120";
              ecrBet = "3120";
              batchBet = "2120";
              fixedBet = "9120";
            }
            else if(cardFee == 0.25)
            {
              termBet = "7125";
              ecrBet = "3125";
              batchBet = "2125";
              fixedBet = "9125";
            }
            else if(cardFee == 0.30)
            {
              termBet = "7130";
              ecrBet = "3130";
              batchBet = "2130";
              fixedBet = "9130";
            }
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, termBet));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA, ecrBet));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA, batchBet));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA, fixedBet));
            break;
            
          case mesConstants.APP_CT_DISCOVER:
            if(cardFee == 0)
            {
              termBet = "7200";
              ecrBet = "3200";
              batchBet = "2200";
              fixedBet = "9200";
            }
            else if(cardFee == 0.05)
            {
              termBet = "7205";
              ecrBet = "3205";
              batchBet = "2205";
              fixedBet = "9205";
            }
            else if(cardFee == 0.10)
            {
              termBet = "7210";
              ecrBet = "3210";
              batchBet = "2210";
              fixedBet = "9210";
            }
            else if(cardFee == 0.15)
            {
              termBet = "7215";
              ecrBet = "3215";
              batchBet = "2215";
              fixedBet = "9215";
            }
            else if(cardFee == 0.20)
            {
              termBet = "7220";
              ecrBet = "3220";
              batchBet = "2220";
              fixedBet = "9220";
            }
            else if(cardFee == 0.25)
            {
              termBet = "7225";
              ecrBet = "3225";
              batchBet = "2225";
              fixedBet = "9225";
            }
            else if(cardFee == 0.30)
            {
              termBet = "7230";
              ecrBet = "3230";
              batchBet = "2230";
              fixedBet = "9230";
            }
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, termBet));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA, ecrBet));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA, batchBet));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA, fixedBet));
            break;
            
          case mesConstants.APP_CT_JCB:
            if(cardFee == 0)
            {
              termBet = "7300";
              ecrBet = "3300";
              batchBet = "2300";
              fixedBet = "9300";
            }
            else if(cardFee == 0.05)
            {
              termBet = "7305";
              ecrBet = "3305";
              batchBet = "2305";
              fixedBet = "9305";
            }
            else if(cardFee == 0.10)
            {
              termBet = "7310";
              ecrBet = "3310";
              batchBet = "2310";
              fixedBet = "9310";
            }
            else if(cardFee == 0.15)
            {
              termBet = "7315";
              ecrBet = "3315";
              batchBet = "2315";
              fixedBet = "9315";
            }
            else if(cardFee == 0.20)
            {
              termBet = "7320";
              ecrBet = "3320";
              batchBet = "2320";
              fixedBet = "9320";
            }
            else if(cardFee == 0.25)
            {
              termBet = "7325";
              ecrBet = "3325";
              batchBet = "2325";
              fixedBet = "9325";
            }
            else if(cardFee == 0.30)
            {
              termBet = "7330";
              ecrBet = "3330";
              batchBet = "2330";
              fixedBet = "9330";
            }
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, termBet));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA, ecrBet));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA, batchBet));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA, fixedBet));
            break;
           
          case mesConstants.APP_CT_DINERS_CLUB:
            if(cardFee == 0)
            {
              termBet = "7400";
              ecrBet = "3400";
              batchBet = "2400";
              fixedBet = "9400";
            }
            else if(cardFee == 0.05)
            {
              termBet = "7405";
              ecrBet = "3405";
              batchBet = "2405";
              fixedBet = "9405";
            }
            else if(cardFee == 0.10)
            {
              termBet = "7410";
              ecrBet = "3410";
              batchBet = "2410";
              fixedBet = "9410";
            }
            else if(cardFee == 0.15)
            {
              termBet = "7415";
              ecrBet = "3415";
              batchBet = "2415";
              fixedBet = "9415";
            }
            else if(cardFee == 0.20)
            {
              termBet = "7420";
              ecrBet = "3420";
              batchBet = "2420";
              fixedBet = "9420";
            }
            else if(cardFee == 0.25)
            {
              termBet = "7425";
              ecrBet = "3425";
              batchBet = "2425";
              fixedBet = "9425";
            }
            else if(cardFee == 0.30)
            {
              termBet = "7430";
              ecrBet = "3430";
              batchBet = "2430";
              fixedBet = "9430";
            }
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, termBet));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA, ecrBet));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA, batchBet));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA, fixedBet));
            break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setCSBNonBankCardBets(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void setButteVoiceAuthFeeBets(long primaryKey)
  {
    if(getDialPayFee(primaryKey))
    {
      if(dialPayFee == .40)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,      "6040"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,    "8040"));
      } 
      else if(dialPayFee == .45)
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,      "6045"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,    "8045"));
      }
      else // default to .55
      {
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,      "6055"));
        bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,    "8055"));
      }
    }
    else
    {
      // set up voice auth fee for 0.75
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ARU_MEDIA,      "6075"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, VOICE_MEDIA,    "8075"));
    }
  }
  
  private void setButteVisaMcAuthBets()
  {
    // set to the $0.10 overall bet
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3073"));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2073"));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7073"));
    bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9073"));
  }
  
  private void setButteNonBankCardBets(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:4530^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4536^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        double cardFee = rs.getDouble("tranchrg_per_tran");
        
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:
            // only one amex per tran option - $0.15
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3175"));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2175"));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7175"));
            bets.add(new Bet(AMEX_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9175"));
            break;
            
          case mesConstants.APP_CT_DISCOVER:
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3275"));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2275"));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7275"));
            bets.add(new Bet(DISCOVER_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9275"));
            break;
            
          case mesConstants.APP_CT_JCB:
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3375"));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2375"));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7375"));
            bets.add(new Bet(JCB_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9375"));
            break;
            
          case mesConstants.APP_CT_DINERS_CLUB:
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, ECR_MEDIA,      "3475"));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "2475"));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "7475"));
            bets.add(new Bet(DINERS_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "9475"));
            break;
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setButteNonBankCardBets()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void setCbtVisaMcAuthBets()
  {
    if(vmcAuthFee == .10)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9410"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6410"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5410"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4410"));
    }
    else if(vmcAuthFee == .11)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9411"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6411"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5411"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4411"));
    }
    else if(vmcAuthFee == .12)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9412"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6412"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5412"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4412"));
    }
    else if(vmcAuthFee == .13)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9413"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6413"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5413"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4413"));
    }
    else if(vmcAuthFee == .14)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9414"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6414"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5414"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4414"));
    }
    else if(vmcAuthFee == .15)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9415"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6415"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5415"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4415"));
    }
    else if(vmcAuthFee == .16)
    {                        
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9416"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6416"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5416"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4416"));
    }
    else if(vmcAuthFee == .17)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9417"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6417"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5417"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4417"));
    }
    else if(vmcAuthFee == .18)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9418"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6418"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5418"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4418"));
    }
    else if(vmcAuthFee == .19)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9419"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6419"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5419"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4419"));
    }
    else if(vmcAuthFee == .20)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9420"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6420"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5420"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4420"));
    }
    else if(vmcAuthFee == .25)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9425"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6425"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5425"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4425"));
    }
    else if(vmcAuthFee == .27)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9427"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6427"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5427"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4427"));
    }
    else if(vmcAuthFee == .28)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9428"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6428"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5428"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4428"));
    }
    else if(vmcAuthFee == .30)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9430"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6430"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5430"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4430"));
    }
    else if(vmcAuthFee == .35)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9435"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6435"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5435"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4435"));
    }
    else if(vmcAuthFee == .40)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9440"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6440"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5440"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4440"));
    }
    else if(vmcAuthFee == .45)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9445"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6445"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5445"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4445"));
    }
    else if(vmcAuthFee == .50)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9450"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6450"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5450"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4450"));
    }
    else if(vmcAuthFee == .55)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9455"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6455"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5455"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4455"));
    }
    else if(vmcAuthFee == .60)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9460"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6460"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5460"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4460"));
    }
    else if(vmcAuthFee == .65)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9465"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6465"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5465"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4465"));
    }
    else if(vmcAuthFee == .70)
    {
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "9470"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, FIXED_MEDIA,    "6470"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,      "5470"));
      bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, BATCH_MEDIA,    "4470"));
    }
  }



  private void setBbtWirelessAuthBets()
  {
    switch(this.processor)
    {
      case mesConstants.APP_PROCESSOR_TYPE_VITAL:
      
        if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3890"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3973"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3949"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3999"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3961"));
        }
        

      break;  
      
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL:
        
        if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4925"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4975"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4951"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4999"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA, "4963"));
        }
        
      break;
    
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST:
        
        if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4216"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4182"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4138"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4126"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA, "4114"));
        }
        
      break;  
      
      case mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH:

        if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4084"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4024"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4048"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4000"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4036"));
        }
        
      break;
    
    }
  }

  private void setDataCaptureBet()
  {
    switch(this.processor)
    {
      case mesConstants.APP_PROCESSOR_TYPE_VITAL:
        vendorDataCapture1  = "VP";
        betDataCapture1     = "5999";
      break;
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL:
        vendorDataCapture1  = "MC";
        betDataCapture1     = "5998";
      break;
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST:
        vendorDataCapture1  = "ND";
        betDataCapture1     = "5996";
      break;
      case mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH:
        vendorDataCapture1  = "GS";
        betDataCapture1     = "5997";
      break;
    }
  }

  private void setBbtVisaMcWirelessAuthBets()
  {

    switch(this.processor)
    {
      case mesConstants.APP_PROCESSOR_TYPE_VITAL:
        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3900"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3891"));
        }
        else if(vmcAuthFee == .19)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "4183"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA, "3876"));
        }
      break;  
      
      
      case mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH:
        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4100"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4162"));
        }
        else if(vmcAuthFee == .19)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4931"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA, "4141"));
        }
      break;
    
    }



  }

  private void setBbtVisaMcAuthBets()
  {
    switch(this.processor)
    {
      case mesConstants.APP_PROCESSOR_TYPE_VITAL:
      

        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3900"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3901"));
        }
        else if(vmcAuthFee == .05)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3820"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3819"));
        }
        else if(vmcAuthFee == .075)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3913"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3912"));
        }
        else if(vmcAuthFee == .08)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3701"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3702"));
        }
        else if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3890"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3884"));
        }
        else if(vmcAuthFee == .13)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3866"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3860"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3973"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3967"));
        }
        else if(vmcAuthFee == .17)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3925"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3919"));
        }
        else if(vmcAuthFee == .18)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3742"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3724"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3949"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3943"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3999"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3993"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3961"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3955"));
        }
        else if(vmcAuthFee == .35)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3937"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3931"));
        }
        else if(vmcAuthFee == .65)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "4201"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3655"));
        }
        else if(vmcAuthFee == .75)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, TERMINAL_MEDIA,  "3977"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_VITAL, ECR_MEDIA,       "3608"));
        }
        

      break;  
      
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL:
        
        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4900"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4901"));
        }
        else if(vmcAuthFee == .08)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4165"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4166"));
        }
        else if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4925"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4919"));
        }
        else if(vmcAuthFee == .11)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4987"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4981"));
        }
        else if(vmcAuthFee == .13)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4890"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4883"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4975"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4969"));
        }
        else if(vmcAuthFee == .19)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4913"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4907"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4951"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4945"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4999"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4993"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4963"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4957"));
        }
        else if(vmcAuthFee == .35)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4854"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4848"));
        }
        else if(vmcAuthFee == .50)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, TERMINAL_MEDIA,  "4701"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_CENTRAL, ECR_MEDIA,       "4702"));
        }
        
      break;
    
      case mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST:
        
        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4200"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4400"));
        }
        else if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4216"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4217"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4138"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4137"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4126"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4125"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4114"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4113"));
        }
        else if(vmcAuthFee == .35)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, TERMINAL_MEDIA,  "4202"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_GLOBAL_EAST, ECR_MEDIA,       "4203"));
        }
        
      break;  
      
      case mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH:

        if(vmcAuthFee == 0.0)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4100"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4101"));
        }
        else if(vmcAuthFee == .05)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4146"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4147"));
        }
        else if(vmcAuthFee == .10)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4084"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4090"));
        }
        else if(vmcAuthFee == .15)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4024"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4030"));
        }
        else if(vmcAuthFee == .19)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4096"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4104"));
        }
        else if(vmcAuthFee == .20)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4048"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4054"));
        }
        else if(vmcAuthFee == .25)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4000"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4006"));
        }
        else if(vmcAuthFee == .30)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4036"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4042"));
        }
        else if(vmcAuthFee == .35)
        {
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, TERMINAL_MEDIA,  "4060"));
          bets.add(new Bet(OVERALL_PLAN, VENDOR_PAYTECH, ECR_MEDIA,       "4066"));
        }
        
      break;
    
    }
  }


  private void setTranscomVisaMcBets(long primaryKey)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:5189^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet_table
//          from    transcom_merchant
//          where   app_seq_num = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet_table\n        from    transcom_merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.ops.AutoUploadBetInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5194^7*/

      rs = it.getResultSet();
      
      if(rs.next() && !isBlank(rs.getString("bet_table")))
      {
        switch(rs.getInt("bet_table"))
        {
          case 4010:
            betVisa = rs.getString("bet_table");
            betMC   = "5010";
          break;
          case 4011:
            betVisa = rs.getString("bet_table");
            betMC   = "5011";
          break;
          case 4012:
            betVisa = rs.getString("bet_table");
            betMC   = "5012";
          break;
          case 4013:
            betVisa = rs.getString("bet_table");
            betMC   = "5013";
          break;
          case 4014:
            betVisa = rs.getString("bet_table");
            betMC   = "5014";
          break;
          case 4015:
            betVisa = rs.getString("bet_table");
            betMC   = "5015";
          break;
          case 4022:
            betVisa = rs.getString("bet_table");
            betMC   = "5022";
          break;
          case 4023:
            betVisa = rs.getString("bet_table");
            betMC   = "5023";
          break;
          case 4024:
            betVisa = rs.getString("bet_table");
            betMC   = "5024";
          break;
          case 4025:
            betVisa = rs.getString("bet_table");
            betMC   = "5025";
          break;
          case 4026:
            betVisa = rs.getString("bet_table");
            betMC   = "5026";
          break;
          case 4027:
            betVisa = rs.getString("bet_table");
            betMC   = "5027";
          break;
          case 4000:
            betVisa = rs.getString("bet_table");
            betMC   = "5000";
          break;
          case 4040:
            betVisa = rs.getString("bet_table");
            betMC   = "5001";
          break;
          case 4052:
            betVisa = rs.getString("bet_table");
            betMC   = "5052";
          break;
          case 4053:
            betVisa = rs.getString("bet_table");
            betMC   = "5053";
          break;
          case 4055:
            betVisa = rs.getString("bet_table");
            betMC   = "5055";
          break;
          case 4056:
            betVisa = rs.getString("bet_table");
            betMC   = "5056";
          break;
          case 4072:
            betVisa = rs.getString("bet_table");
            betMC   = "5072";
          break;
          case 4073:
            betVisa = rs.getString("bet_table");
            betMC   = "5073";
          break;
          case 4082:
            betVisa = rs.getString("bet_table");
            betMC   = "5082";
          break;
          case 4083:
            betVisa = rs.getString("bet_table");
            betMC   = "5083";
          break;
          case 4092:
            betVisa = rs.getString("bet_table");
            betMC   = "5092";
          break;
          case 4093:
            betVisa = rs.getString("bet_table");
            betMC   = "5093";
          break;
        }
      }

      it.close();

      indBetVisa  = "*";
      indBetMC    = "*";

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setTranscomVisaMcBets: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void submitData(long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:5328^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from   billcard_bet
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from   billcard_bet\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5333^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:5335^7*/

//  ************************************************************
//  #sql [Ctx] { insert into billcard_bet
//          (
//            interchange_bet_visa,
//            interchange_bet_mc,
//            debit_net_bet,
//            system_generated_bet,
//            ind_card_plan_visa,
//            ind_card_plan_mc,
//            data_capture_bet1,
//            data_capture_vendor1,
//            app_seq_num
//          )
//          values
//          (
//            :this.betVisa,
//            :this.betMC,
//            :this.betDebit,
//            :this.betSystem,
//            :this.indBetVisa,
//            :this.indBetMC,
//            :this.betDataCapture1,
//            :this.vendorDataCapture1,
//            :primaryKey
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into billcard_bet\n        (\n          interchange_bet_visa,\n          interchange_bet_mc,\n          debit_net_bet,\n          system_generated_bet,\n          ind_card_plan_visa,\n          ind_card_plan_mc,\n          data_capture_bet1,\n          data_capture_vendor1,\n          app_seq_num\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.betVisa);
   __sJT_st.setString(2,this.betMC);
   __sJT_st.setString(3,this.betDebit);
   __sJT_st.setString(4,this.betSystem);
   __sJT_st.setString(5,this.indBetVisa);
   __sJT_st.setString(6,this.indBetMC);
   __sJT_st.setString(7,this.betDataCapture1);
   __sJT_st.setString(8,this.vendorDataCapture1);
   __sJT_st.setLong(9,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5361^7*/

   
      /*@lineinfo:generated-code*//*@lineinfo:5364^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from   billcard_auth_bet
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from   billcard_auth_bet\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5369^7*/
    
      //get all bets from bets vector and submit to database
      for(int betNum=0; betNum < bets.size(); betNum++)
      {
        Bet bet =  (Bet)bets.elementAt(betNum);
        submitBet(primaryKey, bet.planType, bet.vendorId, bet.mediaType, bet.bet, (betNum+1));
      }

      //only update if we have a met table number for them.. currently this should only be BBT
      if(!isBlank(metTable))
      {
        /*@lineinfo:generated-code*//*@lineinfo:5381^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant 
//            set merch_met_table_number = :metTable
//            where  app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merchant \n          set merch_met_table_number =  :1 \n          where  app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,metTable);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5386^9*/
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  private void submitBet(long primaryKey, String planType, String vendorId, String mediaType, String bet, int betNum)
  {
    try
    {
        if(isBlank(bet))
        {
          return;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:5410^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billcard_auth_bet
//            (
//              app_seq_num,
//              plan_type,
//              vendor_id,
//              num_of_bet,
//              media_type,
//              bet
//            )
//            values
//            (
//              :primaryKey,
//              :planType,
//              :vendorId,
//              :betNum,
//              :mediaType,
//              :bet
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into billcard_auth_bet\n          (\n            app_seq_num,\n            plan_type,\n            vendor_id,\n            num_of_bet,\n            media_type,\n            bet\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.ops.AutoUploadBetInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,planType);
   __sJT_st.setString(3,vendorId);
   __sJT_st.setInt(4,betNum);
   __sJT_st.setString(5,mediaType);
   __sJT_st.setString(6,bet);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5430^9*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitBet: " + e.toString());
    }
  }

  
  private boolean isBlank(String test)
  {
    boolean result = false;

    if(test == null || test.length() == 0 || test.equals(""))
    {
      result = true;
    }

    return result;

  }

  public boolean isInterchangeDefault()
  {
    return interDefault;
  }

  public int getProductType()
  {
    return productType;
  }

  public String getProductTypeDesc()
  {
    return productTypeDesc;
  }

  public String getInternetType()
  {
    return internetType;
  }

  public class Bet
  {
    public String planType    = "";
    public String vendorId    = "";
    public String mediaType   = "";
    public String bet         = "";

    public Bet(String planType, String vendorId, String mediaType, String bet)
    {
      this.planType   = planType;
      this.vendorId   = vendorId;
      this.mediaType  = mediaType;
      this.bet        = bet;
    }
  }

}/*@lineinfo:generated-code*/