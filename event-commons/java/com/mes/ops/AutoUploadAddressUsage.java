/*@lineinfo:filename=AutoUploadAddressUsage*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadAddressUsage.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/14/04 1:40p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadAddressUsage extends SQLJConnectionBase
{

  private static final int BUSINESS_ADDRESS   = mesConstants.ADDR_TYPE_BUSINESS;
  private static final int MAILING_ADDRESS    = mesConstants.ADDR_TYPE_MAILING;
  private static final int SHIPPING_ADDRESS   = mesConstants.ADDR_TYPE_SHIPPING;

  private static final int ADDRESS_IND00    = 0;
  private static final int ADDRESS_IND01    = 1;
  private static final int ADDRESS_IND02    = 2;


  private   boolean   mes                   =   false;
  private   int       appType               =   -1;  
  private   String    address00Name         =   "";
  private   String    address00Line1        =   "";
  private   String    address00Line2        =   "";
  private   String    address00City         =   "";
  private   String    address00State        =   "";
  private   String    address00Zip          =   "";
  private   boolean   address00Used         =   false;

  private   String    address01Name         =   "";
  private   String    address01Line1        =   "";
  private   String    address01Line2        =   "";
  private   String    address01City         =   "";
  private   String    address01State        =   "";
  private   String    address01Zip          =   "";
  private   boolean   address01Used         =   false;

  private   String    address02Name         =   "";
  private   String    address02Line1        =   "";
  private   String    address02Line2        =   "";
  private   String    address02City         =   "";
  private   String    address02State        =   "";
  private   String    address02Zip          =   "";
  private   boolean   address02Used         =   false;

  private   boolean   add00Dis              = false;
  private   boolean   add00Rcl              = false;
  private   boolean   add00Crb              = false;
  private   boolean   add00Mlr              = false;
  private   boolean   add00Ccd              = false;
  private   boolean   add00Imp              = false;
  private   boolean   add00Mem              = false;
  private   boolean   add00Pos              = false;
  private   boolean   add00Mis              = false;
  private   boolean   add00Nsg              = false;
  private   boolean   add00Ajf              = false;
  private   boolean   add00Cbf              = false;
  private   boolean   add00Bt1              = false;
  private   boolean   add00Bt2              = false;
  private   boolean   add00Bt3              = false;

  private   boolean   add01Dis              = false;
  private   boolean   add01Rcl              = false;
  private   boolean   add01Crb              = false;
  private   boolean   add01Mlr              = false;
  private   boolean   add01Ccd              = false;
  private   boolean   add01Imp              = false;
  private   boolean   add01Mem              = false;
  private   boolean   add01Pos              = false;
  private   boolean   add01Mis              = false;
  private   boolean   add01Nsg              = false;
  private   boolean   add01Ajf              = false;
  private   boolean   add01Cbf              = false;
  private   boolean   add01Bt1              = false;
  private   boolean   add01Bt2              = false;
  private   boolean   add01Bt3              = false;

  private   boolean   add02Dis              = false;
  private   boolean   add02Rcl              = false;
  private   boolean   add02Crb              = false;
  private   boolean   add02Mlr              = false;
  private   boolean   add02Ccd              = false;
  private   boolean   add02Imp              = false;
  private   boolean   add02Mem              = false;
  private   boolean   add02Pos              = false;
  private   boolean   add02Mis              = false;
  private   boolean   add02Nsg              = false;
  private   boolean   add02Ajf              = false;
  private   boolean   add02Cbf              = false;
  private   boolean   add02Bt1              = false;
  private   boolean   add02Bt2              = false;
  private   boolean   add02Bt3              = false;

  public AutoUploadAddressUsage(long primaryKey)
  {
    setPageDefaults(primaryKey);
    getData(primaryKey);
    submitData(primaryKey);
  }
  
  public void setPageDefaults(long primaryKey)  
  {
    ResultSet           rs      = null;
    ResultSetIterator   it      = null;

    try 
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:145^7*/

//  ************************************************************
//  #sql [Ctx] it = { select app_type 
//          from   application 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select app_type \n        from   application \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadAddressUsage",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/


      rs = it.getResultSet();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");

        switch(this.appType)
        {
          case mesConstants.APP_TYPE_CBT:
          case mesConstants.APP_TYPE_CBT_NEW:
            this.mes = false;
          break;
          default:
            this.mes = true;
          break;
        }
      }
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void getData(long primaryKey)
  {
    getAppAddresses(primaryKey);
  }

  public String getAddressName(long primaryKey, int addInd)  
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              result      = "";
    boolean             useCleanUp  = false;

    try 
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:203^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merch_mailing_name,
//                 merch_business_name,
//                 merch_legal_name 
//          from   merchant 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select merch_mailing_name,\n               merch_business_name,\n               merch_legal_name \n        from   merchant \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AutoUploadAddressUsage",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/


      rs = it.getResultSet();

      if(rs.next())
      {
        
        switch(addInd)
        {
          case MAILING_ADDRESS:
            result = isBlank(rs.getString("merch_mailing_name"))   ? "" : rs.getString("merch_mailing_name");

            if(this.appType == mesConstants.APP_TYPE_BBT)
            {
              result = isBlank(rs.getString("merch_legal_name"))   ? "" : rs.getString("merch_legal_name");
            }
          break;
        
          case BUSINESS_ADDRESS:
            result = isBlank(rs.getString("merch_business_name"))   ? "" : rs.getString("merch_business_name");
          break;
        }
      }

      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAddressName: " + e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
    return result;
  }

  /*
    address indicator legend
    mes:
    00 business physical address
    01 business mailing address
    02 business shipping address

    cbt:
    00 business mailing address
    02 business physical address
  */
  
  private void getAppAddresses(long primaryKey)
  {

    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    boolean             mailingFound  = false;

    try 
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    * 
//          from      address 
//          where     app_seq_num = :primaryKey and 
//                    addresstype_code in (1,2,3) 
//          order by  addresstype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    * \n        from      address \n        where     app_seq_num =  :1  and \n                  addresstype_code in (1,2,3) \n        order by  addresstype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AutoUploadAddressUsage",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^7*/

      rs = it.getResultSet();


      while(rs.next())
      {
        switch(rs.getInt("addresstype_code"))
        {

          case BUSINESS_ADDRESS:
            if(isMes())
            {
              address00Name       =   getAddressName(primaryKey, BUSINESS_ADDRESS);
              address00Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address00Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address00City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address00State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address00Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
            else
            {
              address02Name       =   getAddressName(primaryKey, BUSINESS_ADDRESS);
              address02Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address02Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address02City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address02State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address02Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");

              //default business address in mailing address indicator.. 
              //if mailing address does exist this will get over written anyway
              address00Name       =   address02Name;
              address00Line1      =   address02Line1;
              address00Line2      =   address02Line2;
              address00City       =   address02City;
              address00State      =   address02State;
              address00Zip        =   address02Zip;
            }
          break;

          case MAILING_ADDRESS:
            if(isMes())
            {
              address01Name       =   getAddressName(primaryKey, MAILING_ADDRESS);
              
              if(isBlank(address01Name))
              {
                address01Name = address00Name;
              }
              
              address01Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address01Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address01City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address01State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address01Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
            else
            {
              address00Name       =   isBlank(getAddressName(primaryKey, MAILING_ADDRESS))  ? address00Name  : getAddressName(primaryKey, MAILING_ADDRESS);
              address00Line1      =   isBlank(rs.getString("address_line1"))                ? address00Line1 : rs.getString("address_line1");
              address00Line2      =   isBlank(rs.getString("address_line2"))                ? "" : rs.getString("address_line2");
              address00City       =   isBlank(rs.getString("address_city"))                 ? address00City  : rs.getString("address_city");
              address00State      =   isBlank(rs.getString("countrystate_code"))            ? address00State : rs.getString("countrystate_code");
              address00Zip        =   isBlank(rs.getString("address_zip"))                  ? address00Zip   : rs.getString("address_zip");
            }
            
            mailingFound  = true;
          break;

          case SHIPPING_ADDRESS:
            if(isMes() && this.appType != mesConstants.APP_TYPE_BBT)
            {
              address02Name       =   isBlank(rs.getString("address_name"))      ? "" : rs.getString("address_name");
              address02Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address02Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address02City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address02State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address02Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
          break;
        }
      }
      
      // use legal name for mailing address if mailing address wasn't found
      if(!isMes() && !mailingFound)
      {
        // change address00Name to be legal name in this case
        /*@lineinfo:generated-code*//*@lineinfo:368^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_legal_name
//            
//            from    merchant
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_legal_name\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   address00Name = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^9*/
      }
      
      setFlagDefaults();

      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppAddresses: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setFlagDefaults()
  {

    if(isMes())
    {
      if(this.appType == mesConstants.APP_TYPE_BBT)
      {
        add00Rcl              = true;
        add00Crb              = true;

        //if a mailing address exists set 01 indicators
        if(!isBlank(address01Name))
        {
          add01Dis              = true;
          add01Mlr              = true;
          add01Ccd              = true;
          add01Imp              = true;
          add01Mem              = true;
          add01Pos              = true;
          add01Mis              = true;
          add01Nsg              = true;
          add01Ajf              = true;
          add01Cbf              = true;
          add01Bt1              = true;
          add01Bt2              = true;
          add01Bt3              = true;
        }
        else //set all of 00 indicator to true
        {
          add00Dis              = true;
          add00Mlr              = true;
          add00Ccd              = true;
          add00Imp              = true;
          add00Mem              = true;
          add00Pos              = true;
          add00Mis              = true;
          add00Nsg              = true;
          add00Ajf              = true;
          add00Cbf              = true;
          add00Bt1              = true;
          add00Bt2              = true;
          add00Bt3              = true;
        }
      }
      else
      {
        //there is always a business address so this should always be set to true
        add00Rcl = true;
      
        //if a mailing address exists set 01 indicators
        if(!isBlank(address01Name))
        {
          add01Dis              = true;
          add01Crb              = true;
          add01Mlr              = true;
          add01Ccd              = true;
          add01Imp              = true;
          add01Mem              = true;
          add01Pos              = true;
          add01Mis              = true;
          add01Nsg              = true;
          add01Ajf              = true;
          add01Cbf              = true;
          add01Bt1              = true;
          add01Bt2              = true;
          add01Bt3              = true;
        }
        else //set all of 00 indicator to true
        {
          add00Dis              = true;
          add00Crb              = true;
          add00Mlr              = true;
          add00Ccd              = true;
          add00Imp              = true;
          add00Mem              = true;
          add00Pos              = true;
          add00Mis              = true;
          add00Nsg              = true;
          add00Ajf              = true;
          add00Cbf              = true;
          add00Bt1              = true;
          add00Bt2              = true;
          add00Bt3              = true;
        }

        if(!isBlank(address02Name))
        {
          add00Crb              = false;
          add01Crb              = false;
          add02Crb              = true;
        }

      }

    }
    else
    {
      add00Rcl                = true;
      add00Dis                = true;
      add00Crb                = true;
      add00Mlr                = true;
      add00Ccd                = true;
      add00Imp                = true;
      add00Mem                = true;
      add00Pos                = true;
      add00Mis                = true;
      add00Nsg                = true;
      add00Ajf                = true;
      add00Cbf                = true;
      add00Bt1                = true;
      add02Bt2                = true;
      add00Bt3                = true;
    }

  }

  
  private void submitData(long primaryKey)
  {
    submitUsageData(primaryKey);
    submitAddressData(primaryKey);
  }



  private void submitAddress(String addressName, String addressLine1, String addressLine2, String addressCity, String addressState, String addressZip, int addressInd, long primaryKey)
  {
    try
    {


      //connection already established here
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_setup_address
//          (
//            address_name,
//            address_line1,
//            address_line2,
//            address_city,
//            address_state,
//            address_zip,
//            address_ind,
//            app_seq_num
//          )
//          values
//          (
//            :addressName,
//            :addressLine1,
//            :addressLine2,
//            :addressCity,
//            :addressState,
//            :addressZip,
//            :addressInd,
//            :primaryKey
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_setup_address\n        (\n          address_name,\n          address_line1,\n          address_line2,\n          address_city,\n          address_state,\n          address_zip,\n          address_ind,\n          app_seq_num\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,addressName);
   __sJT_st.setString(2,addressLine1);
   __sJT_st.setString(3,addressLine2);
   __sJT_st.setString(4,addressCity);
   __sJT_st.setString(5,addressState);
   __sJT_st.setString(6,addressZip);
   __sJT_st.setInt(7,addressInd);
   __sJT_st.setLong(8,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:547^7*/


    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAddress: " + e.toString());
    }
  }

  public void submitAddressData(long primaryKey)
  {

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:564^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from      app_setup_address
//          where     app_seq_num = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from      app_setup_address\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:569^7*/

      if(!isBlank(address00Name))
      {
        submitAddress(address00Name, address00Line1, address00Line2, address00City, address00State, address00Zip, ADDRESS_IND00, primaryKey);
      }

      if(!isBlank(address01Name))
      {
        submitAddress(address01Name, address01Line1, address01Line2, address01City, address01State, address01Zip, ADDRESS_IND01, primaryKey);
      }

      if(!isBlank(address02Name))
      {
        submitAddress(address02Name, address02Line1, address02Line2, address02City, address02State, address02Zip, ADDRESS_IND02, primaryKey);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAddressData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private boolean isBlank(String test)
  {
    boolean result = false;

    if(test == null || test.length() == 0 || test.equals(""))
    {
      result = true;
    }

    return result;

  }
  
  public void submitUsageData(long primaryKey)
  {

    try 
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:617^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   address_indicator_usages 
//          where  app_seq_num = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   address_indicator_usages \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:622^7*/

      /*@lineinfo:generated-code*//*@lineinfo:624^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address_indicator_usages
//          (
//            ind_dis,
//            ind_rcl,
//            ind_crb,
//            ind_mlr,
//            ind_ccd,
//            ind_imp,
//            ind_mem,
//            ind_pos,
//            ind_mis,
//            ind_nsg,
//            ind_ajf,
//            ind_cbf,
//            ind_bt1,
//            ind_bt2,
//            ind_bt3,
//            app_seq_num
//          )
//          values
//          (
//            :getDIS(),
//            :getRCL(),
//            :getCRB(),
//            :getMLR(),
//            :getCCD(),
//            :getIMP(),
//            :getMEM(),
//            :getPOS(),
//            :getMIS(),
//            :getNSG(),
//            :getAJF(),
//            :getCBF(),
//            :getBT1(),
//            :getBT2(),
//            :getBT3(),
//            :primaryKey
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_677 = getDIS();
 String __sJT_678 = getRCL();
 String __sJT_679 = getCRB();
 String __sJT_680 = getMLR();
 String __sJT_681 = getCCD();
 String __sJT_682 = getIMP();
 String __sJT_683 = getMEM();
 String __sJT_684 = getPOS();
 String __sJT_685 = getMIS();
 String __sJT_686 = getNSG();
 String __sJT_687 = getAJF();
 String __sJT_688 = getCBF();
 String __sJT_689 = getBT1();
 String __sJT_690 = getBT2();
 String __sJT_691 = getBT3();
   String theSqlTS = "insert into address_indicator_usages\n        (\n          ind_dis,\n          ind_rcl,\n          ind_crb,\n          ind_mlr,\n          ind_ccd,\n          ind_imp,\n          ind_mem,\n          ind_pos,\n          ind_mis,\n          ind_nsg,\n          ind_ajf,\n          ind_cbf,\n          ind_bt1,\n          ind_bt2,\n          ind_bt3,\n          app_seq_num\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AutoUploadAddressUsage",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_677);
   __sJT_st.setString(2,__sJT_678);
   __sJT_st.setString(3,__sJT_679);
   __sJT_st.setString(4,__sJT_680);
   __sJT_st.setString(5,__sJT_681);
   __sJT_st.setString(6,__sJT_682);
   __sJT_st.setString(7,__sJT_683);
   __sJT_st.setString(8,__sJT_684);
   __sJT_st.setString(9,__sJT_685);
   __sJT_st.setString(10,__sJT_686);
   __sJT_st.setString(11,__sJT_687);
   __sJT_st.setString(12,__sJT_688);
   __sJT_st.setString(13,__sJT_689);
   __sJT_st.setString(14,__sJT_690);
   __sJT_st.setString(15,__sJT_691);
   __sJT_st.setLong(16,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:664^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitUsageData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private String getDIS()
  {
    String result = "";

    if(this.add00Dis)
    {
      result = "00";
    }
    else if(this.add01Dis)
    {
      result = "01";
    }
    else if(this.add02Dis)
    {
      result = "02";
    }
    return result;
  }

  private String getRCL()
  {
    String result = "";

    if(this.add00Rcl)
    {
      result = "00";
    }
    else if(this.add01Rcl)
    {
      result = "01";
    }
    else if(this.add02Rcl)
    {
      result = "02";
    }
    return result;
  }

  private String getCRB()
  {
    String result = "";

    if(this.add00Crb)
    {
      result = "00";
    }
    else if(this.add01Crb)
    {
      result = "01";
    }
    else if(this.add02Crb)
    {
      result = "02";
    }
    return result;
  }


  private String getMLR()
  {
    String result = "";

    if(this.add00Mlr)
    {
      result = "00";
    }
    else if(this.add01Mlr)
    {
      result = "01";
    }
    else if(this.add02Mlr)
    {
      result = "02";
    }
    return result;
  }

  private String getCCD()
  {
    String result = "";

    if(this.add00Ccd)
    {
      result = "00";
    }
    else if(this.add01Ccd)
    {
      result = "01";
    }
    else if(this.add02Ccd)
    {
      result = "02";
    }
    return result;
  }

  private String getIMP()
  {
    String result = "";

    if(this.add00Imp)
    {
      result = "00";
    }
    else if(this.add01Imp)
    {
      result = "01";
    }
    else if(this.add02Imp)
    {
      result = "02";
    }
    return result;
  }

  private String getMEM()
  {
    String result = "";

    if(this.add00Mem)
    {
      result = "00";
    }
    else if(this.add01Mem)
    {
      result = "01";
    }
    else if(this.add02Mem)
    {
      result = "02";
    }
    return result;
  }

  private String getPOS()
  {
    String result = "";

    if(this.add00Pos)
    {
      result = "00";
    }
    else if(this.add01Pos)
    {
      result = "01";
    }
    else if(this.add02Pos)
    {
      result = "02";
    }
    return result;
  }

  private String getMIS()
  {
    String result = "";

    if(this.add00Mis)
    {
      result = "00";
    }
    else if(this.add01Mis)
    {
      result = "01";
    }
    else if(this.add02Mis)
    {
      result = "02";
    }
    return result;
  }

  private String getNSG()
  {
    String result = "";

    if(this.add00Nsg)
    {
      result = "00";
    }
    else if(this.add01Nsg)
    {
      result = "01";
    }
    else if(this.add02Nsg)
    {
      result = "02";
    }
    return result;
  }

  private String getAJF()
  {
    String result = "";

    if(this.add00Ajf)
    {
      result = "00";
    }
    else if(this.add01Ajf)
    {
      result = "01";
    }
    else if(this.add02Ajf)
    {
      result = "02";
    }
    return result;
  }

  private String getCBF()
  {
    String result = "";

    if(this.add00Cbf)
    {
      result = "00";
    }
    else if(this.add01Cbf)
    {
      result = "01";
    }
    else if(this.add02Cbf)
    {
      result = "02";
    }
    return result;
  }

  private String getBT1()
  {
    String result = "";

    if(this.add00Bt1)
    {
      result = "00";
    }
    else if(this.add01Bt1)
    {
      result = "01";
    }
    else if(this.add02Bt1)
    {
      result = "02";
    }
    return result;
  }

  private String getBT2()
  {
    String result = "";

    if(this.add00Bt2)
    {
      result = "00";
    }
    else if(this.add01Bt2)
    {
      result = "01";
    }
    else if(this.add02Bt2)
    {
      result = "02";
    }
    return result;
  }

  private String getBT3()
  {
    String result = "";

    if(this.add00Bt3)
    {
      result = "00";
    }
    else if(this.add01Bt3)
    {
      result = "01";
    }
    else if(this.add02Bt3)
    {
      result = "02";
    }
    return result;
  }
  
  public boolean isMes()
  {
    return this.mes;
  }

}/*@lineinfo:generated-code*/