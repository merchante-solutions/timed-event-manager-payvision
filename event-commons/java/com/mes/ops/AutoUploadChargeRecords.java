/*@lineinfo:filename=AutoUploadChargeRecords*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadChargeRecords.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-02-25 10:59:49 -0800 (Mon, 25 Feb 2013) $
  Version            : $Revision: 20945 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadChargeRecords extends SQLJConnectionBase
{
  public static final int           CHARGE_TYPE_SINGLE    = 1;
  public static final int           CHARGE_TYPE_MONTHLY   = 2;

  private Vector  chargeRecs         = new Vector();

  private String  transitRoute1      = "";
  private String  checkActNum1       = "";
  private String  billingMethod1     = "";

  private String  transitRoute2      = "";
  private String  checkActNum2       = "";
  private String  billingMethod2     = "";
  private boolean buildAchChargeRec  = false;

  private String  minDisc            = "";
  private String  discountTaxInd     = "";
  private String  seasonalMonths     = "";
  private int     appType            = -1;
  
  public AutoUploadChargeRecords()
  {
  }

  public AutoUploadChargeRecords(long primaryKey)
  {
    getData(primaryKey);
    submitData(primaryKey);
  }

  private String getMiscDesc(long primaryKey, int miscCode)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    String              result  = "";

    try
    {
      if(miscCode == mesConstants.APP_MISC_CHARGE_UNIQUE_FEE)
      {
        result = "Unique Fee ";

        /*@lineinfo:generated-code*//*@lineinfo:92^9*/

//  ************************************************************
//  #sql [Ctx] it = { select siteinsp_unique_fee_desc 
//            from   siteinspection 
//            where  app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select siteinsp_unique_fee_desc \n          from   siteinspection \n          where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^9*/

        rs = it.getResultSet();
           
        if(rs.next())
        {
          result += isBlank(rs.getString("siteinsp_unique_fee_desc")) ? "" : " - " + rs.getString("siteinsp_unique_fee_desc");
        }

        rs.close();
        it.close();
      }
      else if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP  || 
              miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
      {
        
        /*@lineinfo:generated-code*//*@lineinfo:113^9*/

//  ************************************************************
//  #sql [Ctx] it = { select pos_desc 
//            from   pos_category pc,
//                   merch_pos    mp,
//                   application  app 
//            where  app.app_seq_num = :primaryKey    and 
//                   app.APP_TYPE not in (2,7)        and 
//                   app.app_seq_num = mp.app_seq_num and 
//                   mp.POS_CODE     = pc.POS_CODE    and 
//                   pc.POS_TYPE     = :mesConstants.POS_INTERNET
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select pos_desc \n          from   pos_category pc,\n                 merch_pos    mp,\n                 application  app \n          where  app.app_seq_num =  :1     and \n                 app.APP_TYPE not in (2,7)        and \n                 app.app_seq_num = mp.app_seq_num and \n                 mp.POS_CODE     = pc.POS_CODE    and \n                 pc.POS_TYPE     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.POS_INTERNET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^9*/

        rs = it.getResultSet();
        
        if(rs.next())
        {
          if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP)
          {
            result = "Setup Fee-" + rs.getString("pos_desc");
          }
          else if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
          {
            result = "Monthly Fee-" + rs.getString("pos_desc");
          }
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getMiscDesc(" + primaryKey + ", " + miscCode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }
  public void getData(long primaryKey)
  {
    getAppType(primaryKey);
    
    getMiscChrg(primaryKey);
    getEquip(primaryKey);
    setVisaFees(primaryKey);
    setMinimumCharge(primaryKey);

    getAchChargeRec(primaryKey);
  }

  private void getAppType(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] it = { select app_type 
//          from   application
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select app_type \n        from   application\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select MERCHBANK_TRANSIT_ROUTE_NUM,
//                 MERCHBANK_ACCT_NUM,
//                 BILLING_METHOD,
//                 MERCHBANK_ACCT_SRNUM
//          from   merchbank
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select MERCHBANK_TRANSIT_ROUTE_NUM,\n               MERCHBANK_ACCT_NUM,\n               BILLING_METHOD,\n               MERCHBANK_ACCT_SRNUM\n        from   merchbank\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("MERCHBANK_ACCT_SRNUM"))
        {
          case 1:
            transitRoute1   = rs.getString("MERCHBANK_TRANSIT_ROUTE_NUM"); 
            checkActNum1    = rs.getString("MERCHBANK_ACCT_NUM");
            billingMethod1  = (isBlank(rs.getString("BILLING_METHOD")) || rs.getString("BILLING_METHOD").equals("Debit") ? "D" : "R");
          break;

          case 2:
            transitRoute2     = rs.getString("MERCHBANK_TRANSIT_ROUTE_NUM");
            checkActNum2      = rs.getString("MERCHBANK_ACCT_NUM");
            billingMethod2    = (isBlank(rs.getString("BILLING_METHOD")) || rs.getString("BILLING_METHOD").equals("Debit") ? "D" : "R");
            buildAchChargeRec = true; 
          break;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void setMinimumCharge(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:250^7*/

//  ************************************************************
//  #sql [Ctx] it = { select tranchrg_mmin_chrg 
//          from   tranchrg
//          where  app_seq_num    = :primaryKey and
//                 cardtype_code  = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tranchrg_mmin_chrg \n        from   tranchrg\n        where  app_seq_num    =  :1  and\n               cardtype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        minDisc = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "0" : rs.getString("tranchrg_mmin_chrg");
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:268^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_min_disc_charge = :minDisc
//          where   app_seq_num           = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_min_disc_charge =  :1 \n        where   app_seq_num           =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,minDisc);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/
    }
    catch(Exception e)
    {
      logEntry("setMinimumCharge(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
  }
  
  private boolean isNBSCConversion(long appSeqNum)
  {
    boolean result    = false;
    int     recCount  = 0;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:296^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    application
//          where   app_seq_num = :appSeqNum and
//                  app_user_login = 'nbsc_conversion'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    application\n        where   app_seq_num =  :1  and\n                app_user_login = 'nbsc_conversion'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("isNBSCConversion(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }

  private void getMiscChrg(long primaryKey)  
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    ChargeRec           charge        = null;
    int                 paymentMonths = 0;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(mc.misc_code, -1)                     misc_code,
//                  round(mc.misc_chrg_amount/
//                    decode(nvl(mc.installment_plan_months,1),
//                      0, 1,
//                      nvl(mc.installment_plan_months,1)),2) misc_chrg_amount,
//                  mc.misc_bank_credit_amount                misc_bank_credit_amount,
//                  mc.misc_bank_chrg_amount                  misc_bank_chrg_amount,
//                  mc.misc_chrgbasis_code                    misc_chrgbasis_code,
//                  mc.misc_chrgbasis_descr                   misc_chrgbasis_descr, 
//                  nvl(mcd.chrgbasis_code, 'NA')             chrgbasis_code,
//                  mcd.misc_description                      misc_description,
//                  decode(nvl(mc.installment_plan_months,1),
//                    0, 1,
//                    nvl(mc.installment_plan_months,1))      installment_plan_months
//          from    miscchrg   mc, 
//                  miscdescrs mcd 
//          where   mc.app_seq_num = :primaryKey and 
//                  mc.misc_code = mcd.misc_code  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(mc.misc_code, -1)                     misc_code,\n                round(mc.misc_chrg_amount/\n                  decode(nvl(mc.installment_plan_months,1),\n                    0, 1,\n                    nvl(mc.installment_plan_months,1)),2) misc_chrg_amount,\n                mc.misc_bank_credit_amount                misc_bank_credit_amount,\n                mc.misc_bank_chrg_amount                  misc_bank_chrg_amount,\n                mc.misc_chrgbasis_code                    misc_chrgbasis_code,\n                mc.misc_chrgbasis_descr                   misc_chrgbasis_descr, \n                nvl(mcd.chrgbasis_code, 'NA')             chrgbasis_code,\n                mcd.misc_description                      misc_description,\n                decode(nvl(mc.installment_plan_months,1),\n                  0, 1,\n                  nvl(mc.installment_plan_months,1))      installment_plan_months\n        from    miscchrg   mc, \n                miscdescrs mcd \n        where   mc.app_seq_num =  :1  and \n                mc.misc_code = mcd.misc_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        boolean useSeasonalMonths = false;
        paymentMonths = rs.getInt("installment_plan_months");
         
//******************************************************        
//under these conditions we dont build charge records

        //we dont build charge records for these
        if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE   || 
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE    ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_CHARGEBACK) 
        {
          continue;
        }

        //if there is no fee we dont need it
        if(isBlank(rs.getString("misc_chrg_amount")) || rs.getDouble("misc_chrg_amount") == 0.0)
        {
          continue;
        }
//******************************************************        

        charge = new ChargeRec();
        
        charge.setChargeCode(rs.getInt("misc_code"));
          
//*************************************************************************************
//get the charge record description

        if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP  ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
        {
          charge.setChargeDesc(getMiscDesc(primaryKey, rs.getInt("misc_code")));
        }
        else if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_UNIQUE_FEE ||
                rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_MISC1_FEE  ||
                rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_MISC2_FEE)
        {
          charge.setChargeDesc( isBlank(rs.getString("MISC_CHRGBASIS_DESCR")) ? "" : rs.getString("MISC_CHRGBASIS_DESCR"));
        }
        else
        {
          charge.setChargeDesc( getChargeDesc(rs.getInt("misc_code"),rs.getString("misc_description")));
        }
//*************************************************************************************

        useSeasonalMonths = rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_NETWORK_ACCESS_FEE;

        if(delayConversionMonthlyFees(primaryKey))
        {
          charge.setChargeStart(getStartDatePlus(2));
        }
        else if(this.appType == mesConstants.APP_TYPE_STOREFRONT && rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE)
        {
          charge.setChargeStart(getStartDatePlus(1));
        }
        else if(this.appType == mesConstants.APP_TYPE_CBT_NEW && isNBSCConversion(primaryKey))
        {
          charge.setChargeStart("01/01/07");
        }
        else
        {
          charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th
        }
        
        //if this field is blank then use charge basis associated with fee.. 
        //but if its not blank.. then we use specified charge basis
        if(isBlank(rs.getString("MISC_CHRGBASIS_CODE")))
        {
          if(rs.getString("chrgbasis_code").equals("MC"))
          {
            charge.setChargeFreq((useSeasonalMonths && !isBlank(seasonalMonths)) ? seasonalMonths : "YYYYYYYYYYYY");
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else if(rs.getString("chrgbasis_code").equals("YR"))
          {
            if( rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING ) 
            {
              // this fee is always billed in December
              charge.setChargeFreq("NNNNNNNNNNNY");
            }
            else
            {
              charge.setChargeFreq(getNextMonthFlag());
            }
            charge.setChargeExpire("999999");
            charge.setChargeNum("0");
          }
          else if(rs.getString("chrgbasis_code").equals("QT"))
          {
            // quarterly charge - March, June, September, and December
            charge.setChargeStart(getStartDatePlus(6));
            charge.setChargeFreq("NNYNNYNNYNNY");
            charge.setChargeExpire("999999");
            charge.setChargeNum("1");
          }
          else
          {
            if(isBlank(charge.getChargeFreq()))
            {
              charge.setChargeFreq(getMonthFlag());
            }
            
            charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            charge.setChargeNum("0");
          }
        }
        else
        {
          if(rs.getString("misc_chrgbasis_code").equals("MC"))
          {
            // monthly charge - every month
            charge.setChargeFreq((useSeasonalMonths && !isBlank(seasonalMonths)) ? seasonalMonths : "YYYYYYYYYYYY");
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else if(rs.getString("misc_chrgbasis_code").equals("YR"))
          {
            // annual charge - once per year
            //set up above.. only used for bbt for now anyway
            //charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999");
            charge.setChargeNum("0");
          }
          else if(rs.getString("misc_chrgbasis_code").equals("QT"))
          {
            // quarterly charge - March, June, September, and December
            charge.setChargeFreq("NNYNNYNNYNNY");
            charge.setChargeExpire("999999");
            charge.setChargeNum("1");
          }
          else
          {
            // one-time charge
            if(isBlank(charge.getChargeFreq()))
            {
              charge.setChargeFreq(getMonthFlag());
            }
            charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            charge.setChargeNum("0");
          }
        }
        
        // set annual pci compliance fee to always bill in November
        if( rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE )
        {
          charge.setChargeFreq("NNNNNNNNNNYN");
          
          if( appType != mesConstants.APP_TYPE_PAYPAL_REFERRAL &&
              (DateTimeFormatter.getCurDate()).compareTo(DateTimeFormatter.parseDate("01/01/2009", "mm/dd/yyyy")) < 0)
          {
            // set charge start to be december 08 to make sure this event doesn't fire in november 08
            charge.setChargeStart("12/01/08");
          }
        }
        
        if( rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING )
        {
          charge.setChargeFreq("NNNNNNNNNNYN");
        }

        if(isBlank(transitRoute2))
        {
          charge.setChargeAcctNum(checkActNum1);
          charge.setChargeTransNum(transitRoute1);
          charge.setChargeMethod(billingMethod1);
        }
        else
        {
          charge.setChargeAcctNum(checkActNum2);
          charge.setChargeTransNum(transitRoute2);
          charge.setChargeMethod(billingMethod2);
        }
        
        charge.setChargeAmt( (isBlank(rs.getString("misc_chrg_amount"))           ? ""             : rs.getString("misc_chrg_amount")) );
        charge.setChargeTax("N");    //default to non applicable
        
        //getchargetype.. almost all are mem except term reprogramming for transcom
        charge.setChargeType(getMiscChargeType(rs.getInt("misc_code")));
        
        // if the charge type was set to POS then make sure the charge num is 1
        if(getMiscChargeType(rs.getInt("misc_code")).equals("POS"))
        {
          charge.setChargeNum("1");
        }
        
        // set up payment plan if necessary
        if( paymentMonths > 1 )
        {
          setPaymentPlanData(charge, paymentMonths);
        }

        chargeRecs.add(charge);
      }
      
      rs.close();
      it.close();
      
      // get entries in appo_charge_recs
      /*@lineinfo:generated-code*//*@lineinfo:556^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(acr.description, 1, 40)  description,
//                  acr.charge_rec_type             charge_rec_type,
//                  acr.amount                      amount
//          from    appo_charge_recs acr
//          where   acr.app_seq_num = :primaryKey
//          order by acr.charge_rec_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(acr.description, 1, 40)  description,\n                acr.charge_rec_type             charge_rec_type,\n                acr.amount                      amount\n        from    appo_charge_recs acr\n        where   acr.app_seq_num =  :1 \n        order by acr.charge_rec_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:564^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        charge = new ChargeRec();
        
        charge.setChargeDesc(rs.getString("description"));
        charge.setChargeStart(getStartDate());
        
        switch(rs.getInt("charge_rec_type"))
        {
          case CHARGE_TYPE_SINGLE:
            // set charge frequence to first available month
            charge.setChargeFreq(getMonthFlag());
            charge.setChargeExpire(getExpireDate());
            break;
            
          case CHARGE_TYPE_MONTHLY:
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999");
            break;
        }
        
        charge.setChargeNum("0");
        charge.setChargeAmt(rs.getString("amount"));
        charge.setChargeTax("N");
        charge.setChargeType("MIS");
        
        if(isBlank(transitRoute2))
        {
          charge.setChargeAcctNum(checkActNum1);
          charge.setChargeTransNum(transitRoute1);
          charge.setChargeMethod(billingMethod1);
        }
        else
        {
          charge.setChargeAcctNum(checkActNum2);
          charge.setChargeTransNum(transitRoute2);
          charge.setChargeMethod(billingMethod2);
        }
        
        chargeRecs.add(charge);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMiscChrg(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private boolean applyVisaFees(long primaryKey)
  {
    boolean result = true;
    
    try
    {
      connect();
      
      int recCnt = 1;
      
      /*@lineinfo:generated-code*//*@lineinfo:635^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    merchant mr
//          where   mr.app_seq_num = :primaryKey
//                  and mr.sic_code != 6010
//                  and mr.merch_mcc != 6010
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    merchant mr\n        where   mr.app_seq_num =  :1 \n                and mr.sic_code != 6010\n                and mr.merch_mcc != 6010";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^7*/
      
      result = ( recCnt > 0 );
    }
    catch(Exception e)
    {
      logEntry("applyVisaFees(" + primaryKey + ")", e.toString());
    }
    
    return( result );
  }
  
  private void setVisaFees(long primaryKey)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    ChargeRec charge = null;
    
    String  apfFlag = "Y";
    String  zflFlag = "Y";
    String  vmaFlag = "Y";
    
    try
    {
      // only apply these fees to valid merchant types (non-cash advance)
      if( applyVisaFees(primaryKey) )
      {
        // load fee matrix for this account
        /*@lineinfo:generated-code*//*@lineinfo:671^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(vfc.apf,'Y')  apf,
//                    nvl(vfc.zfl,'Y')  zfl,
//                    nvl(vfc.vma,'Y')  vma
//            from    merchant mr,
//                    t_hierarchy th,
//                    visa_fee_config vfc
//            where   mr.app_seq_num = :primaryKey
//                    and (mr.merch_bank_number||mr.asso_number) = th.descendent
//                    and th.ancestor = vfc.hierarchy_node
//            order by th.relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(vfc.apf,'Y')  apf,\n                  nvl(vfc.zfl,'Y')  zfl,\n                  nvl(vfc.vma,'Y')  vma\n          from    merchant mr,\n                  t_hierarchy th,\n                  visa_fee_config vfc\n          where   mr.app_seq_num =  :1 \n                  and (mr.merch_bank_number||mr.asso_number) = th.descendent\n                  and th.ancestor = vfc.hierarchy_node\n          order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:683^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          apfFlag = rs.getString("apf");
          zflFlag = rs.getString("zfl");
          vmaFlag = rs.getString("vma");
        }
        
        rs.close();
        it.close();
        
        
        if( ("Y").equals(apfFlag) )
        {
          // verify that apf charge is still valid for this merchant
          int apfCount = 0;
          
          /*@lineinfo:generated-code*//*@lineinfo:703^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    merchant
//              where   app_seq_num = :primaryKey
//                      and upper(nvl(charge_apf, 'Y')) = 'Y'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    merchant\n            where   app_seq_num =  :1 \n                    and upper(nvl(charge_apf, 'Y')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   apfCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:710^11*/
          
          if( apfCount > 0 )
          {
            // APF Fee of .0195 (TSYS maps an entry of $1.95 to $0.0195 for this charge record type)
            charge = new ChargeRec();
            charge.setChargeDesc("VISA APF (ACCESS FEE)");
            charge.setChargeStart(getSameMonthStartDate());
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999");
            charge.setChargeNum("0");
            charge.setChargeAmt("1.95");  // TSYS will modify this to be $0.0195 for this charge record type
            charge.setChargeTax("N");
            charge.setChargeType("APF");
      
            if(isBlank(transitRoute2))
            {
              charge.setChargeAcctNum(checkActNum1);
              charge.setChargeTransNum(transitRoute1);
              charge.setChargeMethod(billingMethod1);
            }
            else
            {
              charge.setChargeAcctNum(checkActNum2);
              charge.setChargeTransNum(transitRoute2);
              charge.setChargeMethod(billingMethod2);
            }
      
            chargeRecs.add(charge);
          }
        }
        
        if( ("Y").equals(zflFlag) )
        {
          // Zero Floor Limit fee
          charge = new ChargeRec();
          charge.setChargeDesc("VISA ZERO FLOOR LIMIT FEE");
          charge.setChargeStart(getSameMonthStartDate());
          charge.setChargeFreq("YYYYYYYYYYYY");
          charge.setChargeExpire("999999");
          charge.setChargeNum("0");
          charge.setChargeAmt("0.10");
          charge.setChargeTax("N");
          charge.setChargeType("ZFL");
      
          if(isBlank(transitRoute2))
          {
            charge.setChargeAcctNum(checkActNum1);
            charge.setChargeTransNum(transitRoute1);
            charge.setChargeMethod(billingMethod1);
          }
          else
          {
            charge.setChargeAcctNum(checkActNum2);
            charge.setChargeTransNum(transitRoute2);
            charge.setChargeMethod(billingMethod2);
          }
      
          chargeRecs.add(charge);
        }
        
        if( ("Y").equals(vmaFlag) )
        {
          // Zero Floor Limit fee
          charge = new ChargeRec();
          charge.setChargeDesc("VISA MISUSE OF AUTHORIZATION FEE");
          charge.setChargeStart(getSameMonthStartDate());
          charge.setChargeFreq("YYYYYYYYYYYY");
          charge.setChargeExpire("999999");
          charge.setChargeNum("0");
          charge.setChargeAmt("4.50");  // TSYS will modify this to be $.0450 for this charge record type
          charge.setChargeTax("N");
          charge.setChargeType("VMA");
      
          if(isBlank(transitRoute2))
          {
            charge.setChargeAcctNum(checkActNum1);
            charge.setChargeTransNum(transitRoute1);
            charge.setChargeMethod(billingMethod1);
          }
          else
          {
            charge.setChargeAcctNum(checkActNum2);
            charge.setChargeTransNum(transitRoute2);
            charge.setChargeMethod(billingMethod2);
          }
      
          chargeRecs.add(charge);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setVisaFees(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private String getMiscChargeType(int code)
  {
    String result = "MEM";

    switch(this.appType)
    {
      case mesConstants.APP_TYPE_TRANSCOM:
      case mesConstants.APP_TYPE_MES_NEW:
        if(code == mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE || code == mesConstants.APP_MISC_CHARGE_PINPAD_SWAP_FEE)
        {
          result = "POS";
        }
        else if(code == mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP)
        { 
          result = "MIS";
        }
        break;
      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_CBT_NEW:
        result = "MIS";
        break;
    }
    
    // always default APP_MISC_CHARGE_POS charges to POS
    if(code == mesConstants.APP_MISC_CHARGE_POS)
    {
      result = "POS";
    }

    return result;
  }

  private String getChargeDesc(int code, String descr)
  {
    String result = isBlank(descr) ? "" : descr;

    switch(appType)
    {
      case mesConstants.APP_TYPE_TRANSCOM:
        if(code == mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE)
        {
          result = "Monthly Service Fee";
        }
        break;
        
      case mesConstants.APP_TYPE_CBT_NEW:
        if(code == mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE)
        {
          result = "Merchant Statement Fee";
        }
        break;
    }

    return result;
  }
  
  private boolean addressExists(long primaryKey, int addrType)
  {
    int   addrCount = 0;
    try
    {
      // see if this address (zip and city) exists
      /*@lineinfo:generated-code*//*@lineinfo:874^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(address_zip)
//          
//          from    address
//          where   app_seq_num = :primaryKey and
//                  addresstype_code = :addrType and
//                  address_zip is not null and
//                  address_city is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(address_zip)\n         \n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2  and\n                address_zip is not null and\n                address_city is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,addrType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   addrCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:883^7*/
    }
    catch(Exception e)
    {
      logEntry("addressExists(" + primaryKey + ", " + addrType + ")", e.toString());
    }
    
    return (addrCount > 0);
  }
  
  private String getTaxAmount(long primaryKey, double itemCost)
  {
    String result = "0";
    
    try
    {
      connect();
      int   foundAddrType = 0;
      
      if(addressExists(primaryKey, mesConstants.ADDR_TYPE_SHIPPING))
      {
        foundAddrType = mesConstants.ADDR_TYPE_SHIPPING;
      }
      else if(addressExists(primaryKey, mesConstants.ADDR_TYPE_MAILING))
      {
        foundAddrType = mesConstants.ADDR_TYPE_MAILING;
      }
      else
      {
        foundAddrType = mesConstants.ADDR_TYPE_BUSINESS;
      }
      
      // get zip and city from found address type
      String  zip   = "";
      String  city  = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:919^7*/

//  ************************************************************
//  #sql [Ctx] { select  substr(address_zip, 1, 5),
//                  address_city
//          
//          from    address
//          where   app_seq_num = :primaryKey and
//                  addresstype_code = :foundAddrType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  substr(address_zip, 1, 5),\n                address_city\n         \n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,foundAddrType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   zip = (String)__sJT_rs.getString(1);
   city = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:928^7*/
      
      // now get tax amount
      double taxAmount = 0.0;
      
      /*@lineinfo:generated-code*//*@lineinfo:933^7*/

//  ************************************************************
//  #sql [Ctx] { select  round(sales_tax_rate(:zip, :city) * :itemCost, 2)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  round(sales_tax_rate( :1 ,  :2 ) *  :3 , 2)\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,zip);
   __sJT_st.setString(2,city);
   __sJT_st.setDouble(3,itemCost);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   taxAmount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:938^7*/
      
      result = Double.toString(taxAmount);
    }
    catch(Exception e)
    {
      logEntry("getTaxAmount(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }

  private void getEquip(long primaryKey)  
  {
    StringBuffer        qs        = new StringBuffer();
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    ChargeRec           charge    = null;

    boolean           ownedRec  = false;
    int               ownedQty  = 0;
    double            ownedAmt  = 0.0;
    int               paymentMonths  = 0;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:970^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equiplendtype_code,
//                  me.merchequip_equip_quantity,
//                  nvl(me.merchequip_amount, 0) merchequip_amount,
//                  me.equiptype_code,
//                  nvl(me.payment_plan_months, 0)  payment_plan_months,
//                  elt.equiplendtype_description,
//                  etd.equiptype_description,
//                  eq.equip_descriptor,
//                  eq.tsys_equip_desc
//          from    merchequipment me,
//                  equiplendtype  elt,
//                  equiptype      etd,
//                  equipment      eq
//          where   me.app_seq_num        = :primaryKey           and
//                  me.equiplendtype_code = elt.equiplendtype_code  and
//                  me.equiptype_code     = etd.equiptype_code      and
//                  me.equip_model        = eq.equip_model      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equiplendtype_code,\n                me.merchequip_equip_quantity,\n                nvl(me.merchequip_amount, 0) merchequip_amount,\n                me.equiptype_code,\n                nvl(me.payment_plan_months, 0)  payment_plan_months,\n                elt.equiplendtype_description,\n                etd.equiptype_description,\n                eq.equip_descriptor,\n                eq.tsys_equip_desc\n        from    merchequipment me,\n                equiplendtype  elt,\n                equiptype      etd,\n                equipment      eq\n        where   me.app_seq_num        =  :1            and\n                me.equiplendtype_code = elt.equiplendtype_code  and\n                me.equiptype_code     = etd.equiptype_code      and\n                me.equip_model        = eq.equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.AutoUploadChargeRecords",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:989^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        boolean makeSalesTaxChargeRecord = false;
        paymentMonths = rs.getInt("payment_plan_months");
        
        if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_OWNED)
        {
          ownedRec = true;
          ownedQty += rs.getInt("merchequip_equip_quantity");
          ownedAmt = isBlank(rs.getString("merchequip_amount")) ? 0.0 : rs.getDouble("merchequip_amount");
        }
        else
        {
          //if there is no fee we dont need it
          if(isBlank(rs.getString("merchequip_amount")) || rs.getDouble("merchequip_amount") == 0.0)
          {
            continue;
          }

          charge = new ChargeRec();
        
          qs.setLength(0);
          qs.append(rs.getString("equiplendtype_description") + " " + rs.getString("tsys_equip_desc"));
          qs.setLength(40);

          charge.setChargeDesc(qs.toString());

          charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th

          if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_RENT)
          {
            if(appType == mesConstants.APP_TYPE_CBT_NEW)
            {
              makeSalesTaxChargeRecord = true;
            }
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999"); //never expires
          }
          else
          {
            // set payment plan data if necessary
            if(paymentMonths > 1)
            {
              setPaymentPlanData(charge, paymentMonths);
            }
            else
            {
              if(isBlank(charge.getChargeFreq()))
              {
                charge.setChargeFreq(getMonthFlag());
              }
          
              charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            }
          }

          charge.setChargeNum( (isBlank(rs.getString("merchequip_equip_quantity"))         ? "1" : rs.getString("merchequip_equip_quantity")) );

          if(isBlank(transitRoute2))
          {
            charge.setChargeAcctNum(checkActNum1);
            charge.setChargeTransNum(transitRoute1);
            charge.setChargeMethod(billingMethod1);
          }
          else
          {
            charge.setChargeAcctNum(checkActNum2);
            charge.setChargeTransNum(transitRoute2);
            charge.setChargeMethod(billingMethod2);
          }

          if(paymentMonths > 1 && rs.getInt("equiplendtype_code") != mesConstants.APP_EQUIP_RENT)
          {
            // set charge amount equal to amount divided by total months (rounded up)
            double totalAmount = rs.getDouble("merchequip_amount");
            double paymentAmount = MesMath.round(totalAmount / paymentMonths, 2);
            
            charge.setChargeAmt(NumberFormatter.getDoubleString(paymentAmount, "########0.##"));
          }
          else
          {
            charge.setChargeAmt( (isBlank(rs.getString("merchequip_amount"))         ? "0.00" : rs.getString("merchequip_amount")) );
          }
          charge.setChargeTax("N");    //default to non applicable

          //dont know if this is right charge type for equipment.. not really used for now anyway.. 
          
          if(rs.getInt("equiptype_code") == 4)//imprinter
          {
            charge.setChargeType("IMP");
          }
          else
          {
            charge.setChargeType("POS");
          }
          
          // create sales tax record if necessary (and charge record is for a non-zero amount)
          if(makeSalesTaxChargeRecord && Double.parseDouble(charge.getChargeAmt()) > 0.00)
          {
            ChargeRec taxCharge = new ChargeRec();
            taxCharge.setChargeDesc("RENTAL SALES TAX");
            taxCharge.setChargeStart(charge.getChargeStart());
            taxCharge.setChargeFreq(charge.getChargeFreq());
            taxCharge.setChargeExpire(charge.getChargeExpire());
            taxCharge.setChargeNum("1");
            taxCharge.setChargeAcctNum(charge.getChargeAcctNum());
            taxCharge.setChargeTransNum(charge.getChargeTransNum());
            taxCharge.setChargeMethod(charge.getChargeMethod());
            taxCharge.setChargeAmt(getTaxAmount(primaryKey, Double.parseDouble(charge.getChargeAmt()) * Integer.parseInt(charge.getChargeNum())));
            taxCharge.setChargeTax(charge.getChargeTax());
            taxCharge.setChargeType(charge.getChargeType());
            chargeRecs.add(taxCharge);
          }

          chargeRecs.add(charge);
        }

      }
      
      rs.close();
      it.close();
        
      if(ownedRec && ownedAmt > 0.0)
      {
        charge = new ChargeRec();
      
        qs.setLength(0);
        qs.append("Owned equipment support fee");
        qs.setLength(40);
        charge.setChargeDesc(qs.toString());
        charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th
        charge.setChargeFreq("YYYYYYYYYYYY");
        charge.setChargeExpire("999999"); //never expires
        charge.setChargeNum(Integer.toString(ownedQty));

        if(isBlank(transitRoute2))
        {
          charge.setChargeAcctNum(checkActNum1);
          charge.setChargeTransNum(transitRoute1);
          charge.setChargeMethod(billingMethod1);
        }
        else
        {
          charge.setChargeAcctNum(checkActNum2);
          charge.setChargeTransNum(transitRoute2);
          charge.setChargeMethod(billingMethod2);
        }

        charge.setChargeAmt(Double.toString(ownedAmt));
        charge.setChargeTax("N");    //default to non applicable

        //dont know if this is right charge type not really used for now anyways
        charge.setChargeType("MIS");

        chargeRecs.add(charge);
      }
    }
    catch(Exception e)
    {
      logEntry("getEquip(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getAchChargeRec(long primaryKey)
  {
    if(!buildAchChargeRec)
    {
      return;
    }
    ChargeRec           charge    = null;

    charge = new ChargeRec();
    charge.setChargeDesc  ("ACH");
    charge.setChargeAmt   ("0");
    charge.setChargeStart (getSameMonthStartDate()); 
    charge.setChargeExpire("999999"); 
    charge.setChargeFreq  ("YYYYYYYYYYYY");
    charge.setChargeTax   ("N");
    charge.setChargeType  ("ACH");

    //always gets set with first dda/tr
    charge.setChargeAcctNum(checkActNum1);
    charge.setChargeTransNum(transitRoute1);
    charge.setChargeMethod(billingMethod1);

    charge.setChargeNum   ("1");
    chargeRecs.add(charge);
  }

  private boolean delayConversionMonthlyFees(long primaryKey)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1198^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app.app_seq_num)
//          
//          from    application app,
//                  app_type    apt,
//                  merchant    mr
//          where   app.app_seq_num = :primaryKey and
//                  app.app_seq_num = mr.app_seq_num and
//                  nvl(mr.account_type, 'X') = 'C' and
//                  app.app_type = apt.app_type_code and
//                  nvl(apt.delay_monthly_fees_conversion, 'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app.app_seq_num)\n         \n        from    application app,\n                app_type    apt,\n                merchant    mr\n        where   app.app_seq_num =  :1  and\n                app.app_seq_num = mr.app_seq_num and\n                nvl(mr.account_type, 'X') = 'C' and\n                app.app_type = apt.app_type_code and\n                nvl(apt.delay_monthly_fees_conversion, 'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1210^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("delayConversionMonthlyFees(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** getStartDatePlus(int addMonths)
  **
  ** Gets the start date in a situation where we need it to start some number
  ** of months in the future
  **
  ** NOTE: this method does not work for more than 11 months in the future
  */
  private String getStartDatePlus(int addMonths)
  {
    String result = "";
    
    Calendar  rightNow  = Calendar.getInstance();
    int       curMonth  = rightNow.get(Calendar.MONTH);
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);
    
    // add months
    if(thisMonth + addMonths > Calendar.DECEMBER)
    {
      thisMonth = addMonths - (Calendar.DECEMBER - thisMonth) - 1;
    }
    else
    {
      thisMonth += addMonths;
    }
    
    
    if(thisMonth < curMonth)
    {
      // this means we've skipped to the next year
      ++thisYear;
    }

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "01/01/" + fixYear((thisYear+1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/01/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "12/01/" + fixYear(thisYear);
      }
      else //else we just add one
      {
        result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
      }
    }

    return padDate(result);
  }
  
  private String getStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "01/01/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/01/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "12/01/" + fixYear(thisYear);
      }
      else //else we just add one
      {
        result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
      }
    }

    return padDate(result);
  }

  private String getSameMonthStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    if(thisMonth == 11)
    {
      result = "12/01/" + fixYear(thisYear);
    }
    else //else we just add one
    {
      result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
    }
    
    return padDate(result);
  }

  private String getNextMonthStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    //if its the 12th month we move it to the first month
    if(thisMonth == 11)
    {
      result = "01/01/" + fixYear((thisYear + 1));
    }
    else //else we just add one
    {
      result = (thisMonth + 2) + "/01/" + fixYear(thisYear);
    }

    return padDate(result);
  }

  private String getNextYearStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    //we want to make the start date in one year, so we add 1 to the year
    thisYear += 1;

    String    result    = "";

    if(thisMonth == 11)
    {
      result = "12/01/" + fixYear(thisYear);
    }
    else //else we just add one
    {
      result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
    }

    return padDate(result);
  }

  private String getExpireDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "02/15/" + fixYear((thisYear + 1));
      }
      else if(thisMonth == 10)
      {
        result = "01/15/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 3) + "/15/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "01/15/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/15/" + fixYear(thisYear);
      }
    }
    
    return padDate(result);
  }

  private String padDate(String dte)
  {
    String result = dte;

    if(dte.length() == 7)
    {
      result = "0" + dte;
    }

    return result;
  }

  private String fixYear(int intYear)
  {
    String result = "";
    String strDate = Integer.toString(intYear);

    if(strDate.length() == 4)
    {
      result = strDate.substring(2); // gets only the last two digits
    }
    else
    {
      result = strDate;
    }

    return result;
  }

  private int getChargeMonth()
  {
    int result = 0;
    try
    {
      int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
      int thisDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
      
      result = thisMonth;
      
      // if today is after the 15th then set the month to be next month
      if(thisDay > 15)
      {
        if(thisMonth == 11)
        {
          result = 0;
        }
        else
        {
          ++result;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getChargeMonth()", e.toString());
    }
    return result;
  }
  
  private String getMonthFlag()
  {
    StringBuffer result = new StringBuffer("NNNNNNNNNNNN");
    
    int nextMonth = getChargeMonth();

    try
    {
      result = result.replace(nextMonth,nextMonth+1,"Y");
    }
    catch(Exception e)
    {
      // System.out.println("Error in getMonthFlag() - " + e.toString());
    }

    return result.toString();
  }
  
  private String getMonthFlagPaymentPlan(int months)
  {
    String result = "";
    try
    {
      if(months == 0 || months == 1)
      {
        result = getMonthFlag();
      }
      else if(months >= 12)
      {
        // fires on all months
        result = "YYYYYYYYYYYY";
      }
      else
      {
        StringBuffer monthString = new StringBuffer("NNNNNNNNNNNN");
        
        int nextMonth = getChargeMonth();
        
        int monthsLeft = months;
        
        while(monthsLeft > 0)
        {
          monthString.replace(nextMonth, nextMonth+1, "Y");
          
          if(++nextMonth == 12)
          {
            nextMonth = 0;
          }
          
          --monthsLeft;
        };
        
        result = monthString.toString();
      }
    }
    catch(Exception e)
    {
      logEntry("getMonthFlagPaymentPlan(" + months + ")", e.toString());
    }
    return result;
  }
  
  private void setPaymentPlanData(ChargeRec charge, int paymentMonths)
  {
    try
    {
      // set months for charge record to fire
      charge.setChargeFreq(getMonthFlagPaymentPlan(paymentMonths));
      
      // set charge record expiration date
      Calendar expireMonth = Calendar.getInstance();
      expireMonth.set(Calendar.MONTH, getChargeMonth());
      expireMonth.add(Calendar.MONTH, paymentMonths);
      expireMonth.set(Calendar.DAY_OF_MONTH, 15);
      
     charge.setChargeExpire(DateTimeFormatter.getFormattedDate(expireMonth.getTime(), "MM/dd/yy"));
    }
    catch(Exception e)
    {
      logEntry("setPaymentPlanData()", e.toString());
    }
  }

  private String getSameMonthFlag()
  {
    StringBuffer result = new StringBuffer("NNNNNNNNNNNN");
    
    //months start at 0 to 11

    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH);

    try
    {
      result = result.replace(thisMonth,thisMonth+1,"Y");
    }
    catch(Exception e)
    {
      // System.out.println("Error in getSameMonthFlag() - " + e.toString());
    }

    return result.toString();
  }

  private String getNextMonthFlag()
  {
    StringBuffer result = new StringBuffer("NNNNNNNNNNNN");
    
    //months start at 0 to 11

    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH);
    int       nextMonth = -1;

    //if its the 12th month we move it to the first month
    if(thisMonth == 11)
    {
      nextMonth = 0;
    }
    else //else we just add one
    {
      nextMonth = thisMonth + 1;
    }
 
    try
    {
      result = result.replace(nextMonth,nextMonth+1,"Y");
    }
    catch(Exception e)
    {
      // System.out.println("Error in getNextMonthFlag() - " + e.toString());
    }

    return result.toString();
  }

 
  private String addSlash(String dateStr)
  {
    String result = dateStr;

    if(isBlank(dateStr) || dateStr.equals("999999"))
    {
      return result;
    }

    if(dateStr.length() == 6)
    {
      result = dateStr.substring(0,2) + "/" + dateStr.substring(2,4) + "/" + dateStr.substring(4);
    }

    return result;

  }

  public void submitData(long primaryKey)
  {
    submitChargeData(primaryKey);
  }

  private void submitChargeData(long primaryKey)
  {
    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1656^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from   billother
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from   billother\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1661^7*/
    
      for(int i=0; i<chargeRecs.size(); i++)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1665^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billother
//            (
//              billoth_charge_msg,
//              billoth_charge_type,
//              billoth_charge_startdate,
//              billoth_charge_expiredate,
//              billoth_charge_frq,
//              billoth_charge_times,
//              billoth_charge_method,
//              billoth_charge_amt,
//              billoth_charge_tax_ind,
//              billoth_charge_acct_num,
//              billoth_charge_trans_rte_num,   
//              app_seq_num,
//              billoth_sr_num,
//              misc_code
//            )
//            values
//            (
//              :getChargeDesc(i),
//              :getChargeType(i),
//              :getChargeStart(i),
//              :getChargeExpire(i),
//              :getChargeFreq(i),
//              :getChargeNum(i),
//              :getChargeMethod(i),
//              :getChargeAmt(i),
//              :getChargeTax(i),
//              :getChargeAcctNum(i),
//              :getChargeTransNum(i),
//              :primaryKey,
//              :i,
//              :getChargeCode(i)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_692 = getChargeDesc(i);
 String __sJT_693 = getChargeType(i);
 String __sJT_694 = getChargeStart(i);
 String __sJT_695 = getChargeExpire(i);
 String __sJT_696 = getChargeFreq(i);
 String __sJT_697 = getChargeNum(i);
 String __sJT_698 = getChargeMethod(i);
 String __sJT_699 = getChargeAmt(i);
 String __sJT_700 = getChargeTax(i);
 String __sJT_701 = getChargeAcctNum(i);
 String __sJT_702 = getChargeTransNum(i);
 int __sJT_703 = getChargeCode(i);
   String theSqlTS = "insert into billother\n          (\n            billoth_charge_msg,\n            billoth_charge_type,\n            billoth_charge_startdate,\n            billoth_charge_expiredate,\n            billoth_charge_frq,\n            billoth_charge_times,\n            billoth_charge_method,\n            billoth_charge_amt,\n            billoth_charge_tax_ind,\n            billoth_charge_acct_num,\n            billoth_charge_trans_rte_num,   \n            app_seq_num,\n            billoth_sr_num,\n            misc_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.AutoUploadChargeRecords",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_692);
   __sJT_st.setString(2,__sJT_693);
   __sJT_st.setString(3,__sJT_694);
   __sJT_st.setString(4,__sJT_695);
   __sJT_st.setString(5,__sJT_696);
   __sJT_st.setString(6,__sJT_697);
   __sJT_st.setString(7,__sJT_698);
   __sJT_st.setString(8,__sJT_699);
   __sJT_st.setString(9,__sJT_700);
   __sJT_st.setString(10,__sJT_701);
   __sJT_st.setString(11,__sJT_702);
   __sJT_st.setLong(12,primaryKey);
   __sJT_st.setInt(13,i);
   __sJT_st.setInt(14,__sJT_703);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1701^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitChargeData(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getChargeDesc(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeDesc());
  }
  public String getChargeType(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeType());
  }
  public String getChargeStart(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeStart());
  }
  public String getChargeExpire(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeExpire());
  }
  public String getChargeFreq(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeFreq());
  }
  public String getChargeNum(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeNum());
  }
  public String getChargeMethod(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeMethod());
  }
  public String getChargeAmt(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeAmt());
  }
  public String getChargeTax(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeTax());
  }
  public String getChargeAcctNum(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return (charge.getChargeAcctNum());
  }
  public String getChargeTransNum(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return (charge.getChargeTransNum());
  }
  public int getChargeCode(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return (charge.getChargeCode());
  }
  

  public class ChargeRec
  {
    private String chargeDesc     = "";
    private String chargeType     = "";
    private String chargeStart    = "";
    private String chargeExpire   = "";
    private String chargeFreq     = "";
    private String chargeNum      = "";
    private String chargeMethod   = "";
    private String chargeAmt      = "";
    private String chargeTax      = "";
    private String chargeAcctNum  = "";
    private String chargeTransNum = "";
    private int    chargeCode     = -1;

    ChargeRec()
    {}

    ChargeRec(String freq)
    {
      chargeFreq    = freq;
    }

    public void setChargeDesc(String chargeDesc)
    {
      this.chargeDesc = chargeDesc;  
    }
    public void setChargeType(String chargeType)
    {
      this.chargeType = chargeType;      
    }
    public void setChargeStart(String chargeStart)
    {
      this.chargeStart = chargeStart;  
    }
    public void setChargeExpire(String chargeExpire)
    {
      this.chargeExpire = chargeExpire;
    }
    public void setChargeFreq(String chargeFreq)
    {
      this.chargeFreq = chargeFreq;
    }
    public void setChargeNum(String chargeNum)
    {
      this.chargeNum = chargeNum;
    }
    public void setChargeAmt(String chargeAmt)
    {
      this.chargeAmt = chargeAmt;      
    }
    public void setChargeTax(String chargeTax)
    {
      this.chargeTax = chargeTax;
    }
    public void setChargeMethod(String chargeMethod)
    {
      this.chargeMethod = chargeMethod;      
    }
    public void setChargeAcctNum(String chargeAcctNum)
    {
      this.chargeAcctNum = chargeAcctNum;
    }
    public void setChargeTransNum(String chargeTransNum)
    {
      this.chargeTransNum = chargeTransNum;
    }
    public void setChargeCode(int chargeCode)
    {
      this.chargeCode = chargeCode;
    }

    public String getChargeAcctNum()
    {
      return this.chargeAcctNum;
    }
    public String getChargeTransNum()
    {
      return this.chargeTransNum;
    }
    public String getChargeDesc()
    {
      return this.chargeDesc;  
    }
    public String getChargeType()
    {
      return this.chargeType;      
    }
    public String getChargeStart()
    {
      return this.chargeStart;  
    }
    public String getChargeExpire()
    {
      return this.chargeExpire;
    }
    public String getChargeFreq()
    {
      return this.chargeFreq;
    }
    public String getChargeNum()
    {
      return this.chargeNum;
    }
    public String getChargeAmt()
    {
      return this.chargeAmt;      
    }
    public String getChargeTax()
    {
      return this.chargeTax;
    }
    public String getChargeMethod()
    {
      return this.chargeMethod;      
    }
    public int getChargeCode()
    {
      return this.chargeCode;
    }
  }

  private boolean isBlank(String test)
  {
    boolean result = false;

    if(test == null || test.length() == 0 || test.equals(""))
    {
      result = true;
    }

    return result;

  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    
    try
    {
      long appSeqNum = Long.parseLong(args[0]);
      AutoUploadChargeRecords grunt = new AutoUploadChargeRecords(appSeqNum);
    }
    catch(Exception e)
    {
      System.out.println("ERROR AutoUploadChargeRecords(): " + e.toString());
    }
  }

}/*@lineinfo:generated-code*/