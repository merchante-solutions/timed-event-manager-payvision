/*@lineinfo:filename=AutoUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ops/AutoUpload.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-06 11:17:09 -0800 (Tue, 06 Feb 2007) $
  Version            : $Revision: 13414 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;

public class AutoUpload extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(AutoUpload.class);
  
  public boolean __doAutoUpload(long appSeqNum)
  {
    boolean result = false;
    try
    {
      connect();
      
      if(__canAutoUpload(appSeqNum))
      {
        // do the deed
        AutoUploadIndividualMerchant      au1 = new AutoUploadIndividualMerchant(appSeqNum);
        AutoUploadMerchantDiscount        au2 = new AutoUploadMerchantDiscount(appSeqNum);
        AutoUploadBetInfo                 au3 = new AutoUploadBetInfo(appSeqNum);
        AutoUploadChargeRecords           au4 = new AutoUploadChargeRecords(appSeqNum);
        AutoUploadTransactionDestination  au5 = new AutoUploadTransactionDestination(appSeqNum);
        AutoUploadAddressUsage            au6 = new AutoUploadAddressUsage(appSeqNum);
        
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("__doAutoUpload(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void __submitToTape(long appSeqNum)
  {
    try
    {
      AccountSetupBean asb = new AccountSetupBean();
      asb.submitData(null, appSeqNum, true);
      asb.cleanUp();
    }
    catch(Exception e)
    {
      logEntry("__submitToTape(" + appSeqNum + ")", e.toString());
    }
  }
  
  public void __moveToMMS(long appSeqNum, UserBean userLogin)
  {
    try
    {
      AccountSetupBean asb = new AccountSetupBean();
      asb.postToMmsSetupQueues(userLogin, appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("__moveToMMS(" + appSeqNum + ")", e.toString());
    }
  }
  
  public boolean __appTypeAllowed(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      int appCount = 0;
      
      // check for chain or non-auto-uploadable app types
      /*@lineinfo:generated-code*//*@lineinfo:111^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(apt.app_type_code)
//          
//          from    app_type apt,
//                  application app,
//                  merchant mr
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_type = apt.app_type_code and
//                  nvl(apt.auto_data_expand, 'N') = 'Y' and
//                  app.app_seq_num = mr.app_seq_num and
//                  mr.merch_application_type not in (2, 3)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(apt.app_type_code)\n         \n        from    app_type apt,\n                application app,\n                merchant mr\n        where   app.app_seq_num =  :1  and\n                app.app_type = apt.app_type_code and\n                nvl(apt.auto_data_expand, 'N') = 'Y' and\n                app.app_seq_num = mr.app_seq_num and\n                mr.merch_application_type not in (2, 3)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^7*/
      
      result = (appCount > 0);
    }
    catch(Exception e)
    {
      logEntry("__appTypeAllowed(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public boolean __canAutoUpload(long appSeqNum)
  {
    boolean result = false;
      
    try
    {
      connect();
      
      int appCount = 0;
      
      result = __appTypeAllowed(appSeqNum);
      
      if(result)
      {
        // check for purchased equipment
        /*@lineinfo:generated-code*//*@lineinfo:154^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(me.app_seq_num)
//            
//            from    merchequipment me
//            where   me.app_seq_num = :appSeqNum and
//                    me.equiplendtype_code = 1 and
//                    me.equiptype_code != 9
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(me.app_seq_num)\n           \n          from    merchequipment me\n          where   me.app_seq_num =  :1  and\n                  me.equiplendtype_code = 1 and\n                  me.equiptype_code != 9";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^9*/
        
        result = (appCount == 0);
        
        if(result)
        {
          // banner rented equipment is also prevented from being auto-uploaded
          /*@lineinfo:generated-code*//*@lineinfo:169^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app.app_seq_num)
//              
//              from    application app,
//                      merchequipment me
//              where   app.app_seq_num = :appSeqNum and
//                      app.app_type = :mesConstants.APP_TYPE_BANNER and
//                      app.app_seq_num = me.app_seq_num and
//                      me.equiplendtype_code = 2
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app.app_seq_num)\n             \n            from    application app,\n                    merchequipment me\n            where   app.app_seq_num =  :1  and\n                    app.app_type =  :2  and\n                    app.app_seq_num = me.app_seq_num and\n                    me.equiplendtype_code = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_TYPE_BANNER);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^11*/
          
          result = (appCount == 0);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("__canAutoUpload(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void __moveToSetupComplete(long appSeqNum, UserBean userLogin)
  {
    try
    {
      connect();
      
      boolean found = false;
      
      // determine which queue this item is in currently
      int queueStage = 0;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:210^9*/

//  ************************************************************
//  #sql [Ctx] { select  aq.app_queue_stage
//            
//            from    app_queue aq
//            where   aq.app_queue_type = :QueueConstants.QUEUE_SETUP and
//                    aq.app_queue_stage != :QueueConstants.Q_SETUP_COMPLETE
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  aq.app_queue_stage\n           \n          from    app_queue aq\n          where   aq.app_queue_type =  :1  and\n                  aq.app_queue_stage !=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(2,QueueConstants.Q_SETUP_COMPLETE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueStage = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^9*/
        
        found = true;
      }
      catch(Exception e)
      {
      }
      
      if(found)
      {
        // move to setup complete
        /*@lineinfo:generated-code*//*@lineinfo:228^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//            set     app_queue_stage = :QueueConstants.Q_SETUP_COMPLETE,
//                    app_last_user = :userLogin.getLoginName(),
//                    app_last_date = sysdate
//            where   app_seq_num = :appSeqNum and
//                    app_queue_type = :QueueConstants.QUEUE_SETUP and
//                    app_queue_stage = :queueStage
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_676 = userLogin.getLoginName();
   String theSqlTS = "update  app_queue\n          set     app_queue_stage =  :1 ,\n                  app_last_user =  :2 ,\n                  app_last_date = sysdate\n          where   app_seq_num =  :3  and\n                  app_queue_type =  :4  and\n                  app_queue_stage =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.AutoUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_SETUP_COMPLETE);
   __sJT_st.setString(2,__sJT_676);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setInt(4,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(5,queueStage);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("__moveToSetupComplete(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean __checkManualSetupRequirement(long appSeqNum)
  {
    boolean result = false;
    try
    {
      connect();
      
      // check to see if comments exist
      int totalComments = 0;
      int commentCount  = 0;
      
      // merchant.merch_notes
      /*@lineinfo:generated-code*//*@lineinfo:262^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum and
//                  ltrim(rtrim(nvl(merch_notes, ' '))) is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1  and\n                ltrim(rtrim(nvl(merch_notes, ' '))) is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   commentCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:269^7*/
      totalComments += commentCount;
      
      // siteinspection.siteinsp_comment
      /*@lineinfo:generated-code*//*@lineinfo:273^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    siteinspection
//          where   app_seq_num = :appSeqNum and
//                  ltrim(rtrim(nvl(siteinsp_comment, ' '))) is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    siteinspection\n        where   app_seq_num =  :1  and\n                ltrim(rtrim(nvl(siteinsp_comment, ' '))) is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   commentCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^7*/
      totalComments += commentCount;
      
      // pos_features.equipment_comment
      // pos_features.shipping_comment
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    pos_features
//          where   app_seq_num = :appSeqNum and
//                  (
//                    ltrim(rtrim(nvl(equipment_comment, ' '))) is not null or
//                    ltrim(rtrim(nvl(shipping_comment, ' '))) is not null
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    pos_features\n        where   app_seq_num =  :1  and\n                (\n                  ltrim(rtrim(nvl(equipment_comment, ' '))) is not null or\n                  ltrim(rtrim(nvl(shipping_comment, ' '))) is not null\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   commentCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
      totalComments += commentCount;
      
      result = (totalComments > 0);
      
      if(! result)
      {
        // check to see if this is an additional location to a chain
        int newLocCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:304^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    merchant
//            where   app_seq_num = :appSeqNum and
//                    nvl(merch_application_type, 0) = 3
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    merchant\n          where   app_seq_num =  :1  and\n                  nvl(merch_application_type, 0) = 3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newLocCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:311^9*/
        
        result = (newLocCount > 0);
      }
      
      // always auto-upload apps that are virtual apps (verisign VA, authnet)
      int appType = 0;
      /*@lineinfo:generated-code*//*@lineinfo:318^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^7*/
      
      switch(appType)
      {
        case mesConstants.APP_TYPE_VERISIGN_V2:
        case mesConstants.APP_TYPE_VPS_ISO:
        case mesConstants.APP_TYPE_AUTHNET:
          result = false;
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("__checkManualSetupRequirement(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void __addToSetupQueue(long appSeqNum)
  {
    try
    {
      connect();
      
      // get app source and user data for this app
      long    appUserId     = 0L;
      String  appUserLogin  = "";
      /*@lineinfo:generated-code*//*@lineinfo:356^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.app_user_id,
//                  app.app_user_login
//          
//          from    application app
//          where   app.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.app_user_id,\n                app.app_user_login\n         \n        from    application app\n        where   app.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.AutoUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appUserId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appUserLogin = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^7*/
      
      AddQueueBean aqb = new AddQueueBean(Ctx);
      
      aqb.insertQueue(appSeqNum,
                      QueueConstants.QUEUE_SETUP,
                      QueueConstants.Q_SETUP_NEW,
                      QueueConstants.Q_STATUS_APPROVED,
                      appUserLogin,
                      appUserId,
                      appUserLogin);
    }
    catch(Exception e)
    {
      logEntry("__addToSetupQueue(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static boolean doAutoUpload(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    return worker.__doAutoUpload(appSeqNum);
  }
  
  public static void submitToTape(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    worker.__submitToTape(appSeqNum);
  }
  
  public static void moveToMMS(long appSeqNum, UserBean userLogin)
  {
    AutoUpload worker = new AutoUpload();
    worker.__moveToMMS(appSeqNum, userLogin);
  }
  
  public static boolean canAutoUpload(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    return worker.__canAutoUpload(appSeqNum);
  }
  
  public static boolean appTypeAllowed(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    return worker.__appTypeAllowed(appSeqNum);
  }
  
  public static void moveToSetupComplete(long appSeqNum, UserBean userLogin)
  {
    AutoUpload worker = new AutoUpload();
    worker.__moveToSetupComplete(appSeqNum, userLogin);
  }
  
  public static boolean checkManualSetupRequirement(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    return worker.__checkManualSetupRequirement(appSeqNum);
  }
  
  public static void addToSetupQueue(long appSeqNum)
  {
    AutoUpload worker = new AutoUpload();
    worker.__addToSetupQueue(appSeqNum);
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      
      long appSeqNum = Long.parseLong(args[0]);
      
      System.out.println("performing auto upload: " + appSeqNum);
      if(doAutoUpload(appSeqNum))
      {
        System.out.println("auto upload success");
      }
      else
      {
        System.out.println("auto upload failed");
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.ops.AutoUpload:main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/