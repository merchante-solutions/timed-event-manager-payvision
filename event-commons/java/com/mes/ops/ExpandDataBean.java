/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ExpandDataBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/01 4:13p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import com.mes.screens.SequenceDataBean;
import com.mes.screens.SequenceIdBean;

public class ExpandDataBean extends SequenceDataBean
{
  
  private boolean pended  = false;
  private boolean unpend  = false;

  public void setPend(String temp)
  {
    pended = true;
  }

  public void setUnpend(String temp)
  {
    unpend = true;
  }
  
  public void pend(long primaryKey)
  {
    if(!isPended())
    {
      return;
    }

    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    
    try
    {
      qs.append("update app_queue set ");
      qs.append("app_queue_stage = ? ");
      qs.append("where app_seq_num = ? and ");
      qs.append("app_queue_type = ? ");
      qs.append("and app_queue_stage = ? ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setInt(1, com.mes.ops.QueueConstants.Q_SETUP_PENDING);
      ps.setLong(2, primaryKey);
      ps.setInt(3, com.mes.ops.QueueConstants.QUEUE_SETUP);
      ps.setInt(4, com.mes.ops.QueueConstants.Q_SETUP_NEW);

      ps.executeUpdate();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "pend: " + e.toString());
    }
  }
  
  public void unpend(long primaryKey)
  {
    if(!isUnpend())
    {
      return;
    }

    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    
    try
    {
      qs.append("update app_queue set ");
      qs.append("app_queue_stage = ? ");
      qs.append("where app_seq_num = ? and ");
      qs.append("app_queue_type = ? ");
      qs.append("and app_queue_stage = ? ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setInt(1, com.mes.ops.QueueConstants.Q_SETUP_NEW);
      ps.setLong(2, primaryKey);
      ps.setInt(3, com.mes.ops.QueueConstants.QUEUE_SETUP);
      ps.setInt(4, com.mes.ops.QueueConstants.Q_SETUP_PENDING);

      ps.executeUpdate();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "unpend: " + e.toString());
    }
  }


  public boolean isPended()
  {
    return pended;
  }

  public boolean isUnpend()
  {
    return unpend;
  }


  // override loadTrackingData to load some merchant data
  public void loadTrackingData(long primaryKey, SequenceIdBean sib)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    SimpleDateFormat  simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
    
    try
    {
      qs.append("select ");
      qs.append("merch.merch_number, ");
      qs.append("merch.merc_cntrl_number, ");
      qs.append("merch.merch_business_name, ");
      qs.append("appQ.app_start_date, ");
      qs.append("appQ.app_source ");
      qs.append("from merchant merch, app_queue appQ ");
      qs.append("where merch.app_seq_num = appQ.app_seq_num and ");
      qs.append("merch.app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
      /*
        sib.setStringValue("merchantNumber", rs.getString("merch_number"));
        sib.setStringValue("merchantName", rs.getString("merch_business_name"));
        sib.setLongValue  ("appControlNum", rs.getLong("merc_cntrl_number"));
        sib.setStringValue("dateSubmitted", simpleDate.format(rs.getDate("app_start_date")));
        sib.setStringValue("appSource", rs.getString("app_source"));
      */
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadTrackingData: " + e.toString());
    }
  }
}
