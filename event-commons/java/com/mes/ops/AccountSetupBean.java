/*@lineinfo:filename=AccountSetupBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AccountSetupBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-15 10:17:10 -0700 (Tue, 15 Jul 2008) $
  Version            : $Revision: 15087 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class AccountSetupBean extends ExpandDataBean
{
  private String merchantNum = "";
  private String dba         = "";
  private int    appType     = -1;

  public AccountSetupBean()
  {
  }

  public void getData(long primaryKey)
  {
    getInfo(primaryKey);
  }
  
  private void getInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try 
    {
      connect();
      
      //put in file upload queue
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name,
//                  merch_number
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name,\n                merch_number\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AccountSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:76^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        dba         = rs.getString("merch_business_name");  
        merchantNum = rs.getString("merch_number");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getInfo: " + e.toString());
      addError("getInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
  }

  public void submitData(HttpServletRequest req, long primaryKey, boolean complete)
  {
    try 
    {
      connect();
      if(complete)
      {
        //put in file upload queue
        /*@lineinfo:generated-code*//*@lineinfo:111^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_setup_complete
//            (
//              app_seq_num,
//              date_completed
//            )
//            values
//            (
//              :primaryKey,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_setup_complete\n          (\n            app_seq_num,\n            date_completed\n          )\n          values\n          (\n             :1 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^9*/
      }
      
      //put in completed stage if done and being sent out via tape.. or put in manual queue for manual entry..
      int queueStage = 0;
      
      if(complete)
      {
        if(checkForOverflowData(primaryKey))
        {
          queueStage = QueueConstants.Q_SETUP_DATA_OVERFLOW;
        }
        else
        {
          queueStage = QueueConstants.Q_SETUP_QA;
        }
      }
      else
      {
        queueStage = QueueConstants.Q_SETUP_MANUAL;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:145^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//          set     app_queue_stage = :queueStage,
//                  app_start_date = sysdate
//          where   app_seq_num = :primaryKey and
//                  app_queue_type = :QueueConstants.QUEUE_SETUP
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n        set     app_queue_stage =  :1 ,\n                app_start_date = sysdate\n        where   app_seq_num =  :2  and\n                app_queue_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,queueStage);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.QUEUE_SETUP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  private boolean checkForOverflowData(long primaryKey)
  {
    boolean           result  = false;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    billcard_auth_bet
//          where   app_seq_num = :primaryKey and
//                  num_of_bet > 36
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    billcard_auth_bet\n        where   app_seq_num =  :1  and\n                num_of_bet > 36";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AccountSetupBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
      
      if(recCount > 0)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "checkForOverflowData: " + e.toString());
      addError("checkForOverflowData: " + e.toString());
    }  
    finally
    {
      cleanUp();
    }

    return result;
  }



  private void setAppType(long primaryKey)
  {
    try 
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AccountSetupBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setAppType: " + e.toString());
      addError("setAppType: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private boolean noProductSelected(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      connect();
     
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//                  and pos_code = :mesConstants.APP_MPOS_UNKNOWN
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merch_pos\n        where   app_seq_num =  :1 \n                and pos_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AccountSetupBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MPOS_UNKNOWN);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^7*/
      
      result = ( recCount > 0 );
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }

  public void postToMmsSetupQueues(UserBean userLogin, long primaryKey)
  {
    int qType = MesQueues.Q_NONE;
    
    setAppType(primaryKey);
    
    if( noProductSelected(primaryKey) )
    {
      qType = MesQueues.Q_PRODUCT_DETERMINE_NEW;
    }
    else
    {
      // determine the profile generator (i.e. mms, vericentre, termmaster ...)
      EquipProfileGeneratorDictator epgd = new EquipProfileGeneratorDictator();
      int appLevelProfGen = epgd.getAppLevelProfileGeneratorCode(primaryKey);

      // determine the appropriate mms setup queue as a function of the app level profile generator
      switch(appLevelProfGen) {
        case mesConstants.PROFGEN_VCTM:
          qType = MesQueues.Q_MMS_VCTM_NEW;
          break;
        default:
          if(this.appType == mesConstants.APP_TYPE_CBT || this.appType == mesConstants.APP_TYPE_CBT_NEW)
          {
            qType = MesQueues.Q_CBT_MMS_NEW;
          }
          else
          {
            qType = MesQueues.Q_MMS_NEW;
          }
          break;
      }
    }
    
    // put in new mms setup queue
    QueueTools.insertQueue(primaryKey, qType, userLogin);

    // fix time stamps so app tracking is correct
    fixTimeStamps(primaryKey, userLogin == null ? "" : userLogin.getLoginName());
  }

  private void putInMmsQueue(String loginName, long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    String            appSource   = "";
    long              appUser     = 0;
    java.sql.Date     startDate   = null;

    try 
    {
      connect();
      
      //put in file upload queue
      /*@lineinfo:generated-code*//*@lineinfo:315^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_start_date,
//                  app_user,
//                  app_source
//          from    app_queue
//          where   app_seq_num = :primaryKey
//          order by app_start_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_start_date,\n                app_user,\n                app_source\n        from    app_queue\n        where   app_seq_num =  :1 \n        order by app_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.AccountSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        appSource = rs.getString("app_source");  
        appUser   = rs.getLong  ("app_user");
        startDate = rs.getDate  ("app_start_date");
      }
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:336^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue
//          (
//            app_seq_num,
//            app_queue_type,
//            app_queue_stage,
//            app_status,
//            app_user,
//            app_start_date,
//            app_source,
//            app_last_date,
//            app_last_user
//          )
//          values
//          (
//            :primaryKey,
//            :QueueConstants.QUEUE_MMS,
//            :QueueConstants.Q_MMS_NEW,
//            :QueueConstants.Q_STATUS_APPROVED,
//            :appUser,
//            :startDate,
//            :appSource,
//            'SYSTEM'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue\n        (\n          app_seq_num,\n          app_queue_type,\n          app_queue_stage,\n          app_status,\n          app_user,\n          app_start_date,\n          app_source,\n          app_last_date,\n          app_last_user\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n          'SYSTEM'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.QUEUE_MMS);
   __sJT_st.setInt(3,QueueConstants.Q_MMS_NEW);
   __sJT_st.setInt(4,QueueConstants.Q_STATUS_APPROVED);
   __sJT_st.setLong(5,appUser);
   __sJT_st.setDate(6,startDate);
   __sJT_st.setString(7,appSource);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:361^7*/

      //hits the tsys completed time stamp.. and the mms received time stamp..
      fixTimeStamps(primaryKey, loginName);

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "putInMmsQueue: " + e.toString());
      addError("putInMmsQueue: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }


  public void fixTimeStamps(long primaryKey, String loginName)
  {
    try 
    {
      connect();
      
      //updates tsys department to completed status and time stamps completed
      /*@lineinfo:generated-code*//*@lineinfo:388^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_completed = sysdate,
//                  status_code = 202,
//                  contact_name = :loginName
//          where   app_seq_num = :primaryKey and
//                  dept_code = :QueueConstants.DEPARTMENT_TSYS
//                  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_completed = sysdate,\n                status_code = 202,\n                contact_name =  :1 \n        where   app_seq_num =  :2  and\n                dept_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_TSYS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:397^7*/

      //updates mms department to received status and time stamps received
      /*@lineinfo:generated-code*//*@lineinfo:400^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_received = sysdate,
//                  status_code = 301
//          where   app_seq_num = :primaryKey and
//                  dept_code = :QueueConstants.DEPARTMENT_MMS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_received = sysdate,\n                status_code = 301\n        where   app_seq_num =  :1  and\n                dept_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.AccountSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:407^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fixTimeStamps: " + e.toString());
      addError("fixTimeStamps: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getMerchantNum()
  {
    return this.merchantNum;
  }
  public String getDba()
  {
    return this.dba;
  }
}/*@lineinfo:generated-code*/