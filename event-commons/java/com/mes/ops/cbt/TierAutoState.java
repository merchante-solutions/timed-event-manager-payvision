package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class TierAutoState extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(TierAutoState.class);

  public boolean hasId;
  public boolean hasScore;
  public boolean hasData;
  public boolean hasCbtScore; 
  public boolean isTimedOut;
  public boolean isInProgress;
  public boolean isCanceled;
  public boolean isComplete;

  public TierAutoState(long appSeqNum)
  {
    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      // determine current state of the auto score process
      String qs = " select                                                "
                + "  nvl2(r.output_id,'Y','N')        id_received,        "
                + "  nvl2(r.request_data,'Y','N')     data_received,      "
                + "  nvl2(a.average_score,'Y','N')    score_received,     "
                + "  nvl2(m.cbt_credit_score,'Y','N') cbt_score_received, "
                + "  nvl2(p.date_completed,'done',                        "
                + "   decode(p.process_sequence,                          "
                + "           -1,'timeout',                               "
                + "           -2,'cancel',                                "
                + "           nvl2(p.app_seq_num,                         "
                + "                'in progress',                         "
                + "                'not started')))   process_state       "
                + " from                                                  "
                + "  credit_requests r,                                   "
                + "  credit_requests_process p,                           "
                + "  credit_score_average a,                              "
                + "  merchant m                                           "
                + " where                                                 "
                + "  m.app_seq_num = ?                                    "
                + "  and m.app_seq_num = p.app_seq_num(+)                 "
                + "  and m.app_seq_num = r.app_seq_num(+)                 "
                + "  and m.app_seq_num = a.app_seq_num(+)                 ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      if (!rs.next())
      {
        throw new NullPointerException("appSeqNum " + appSeqNum 
          + " not found in merchant table");
      }

      hasId         = rs.getString("id_received").equals("Y");
      hasScore      = rs.getString("score_received").equals("Y");
      hasData       = rs.getString("data_received").equals("Y");
      hasCbtScore   = rs.getString("cbt_score_received").equals("Y");
      isTimedOut    = rs.getString("process_state").equals("timeout");
      isInProgress  = rs.getString("process_state")
                          .equals("in progress");
      isCanceled    = rs.getString("process_state").equals("cancel");
      isComplete    = rs.getString("process_state").equals("done");
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      e.printStackTrace();
      logEntry("TierAutoState()","appSeqNum = " + appSeqNum);
      throw new RuntimeException("Error getting auto state: " + e);
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
}
