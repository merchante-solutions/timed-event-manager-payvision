/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * Handles ops for MCC assignment queues, new, completed and canceled.
 * Moves completed apps on to the tier number assignment queue.
 */
package com.mes.ops.cbt;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.user.UserBean;

public class TierMccQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierMccQueueOps.class);

  public static final int Q_MCC_NEW        
                                  = MesQueues.Q_CBT_TIER_MCC_ASSIGN_NEW;
  public static final int Q_MCC_COMPLETED  
                                  = MesQueues.Q_CBT_TIER_MCC_ASSIGN_COMPLETED;
  public static final int Q_MCC_CANCELED   
                                  = MesQueues.Q_CBT_TIER_MCC_ASSIGN_CANCELED;

  private void newMcc(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_MCC_NEW,CS_CLIENT_REVIEW,
      user,"newMcc");
  }
  public static void doNewMcc(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierMccQueueOps()).newMcc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelMcc(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_MCC_COMPLETED,"cancelMcc");
    moveQueueItem(appSeqNum,Q_MCC_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelMcc");
  }
  public static void doCancelMcc(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierMccQueueOps()).cancelMcc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void uncancelMcc(long appSeqNum, UserBean user)
    throws Exception
  {
    validateInQueue(appSeqNum,Q_MCC_CANCELED,"uncancelMcc");
    moveQueueItem(appSeqNum,Q_MCC_NEW,CS_CLIENT_REVIEW,user,
      "uncancelMcc");
  }
  public static void doUncancelMcc(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierMccQueueOps()).uncancelMcc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void completeMcc(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_MCC_COMPLETED,CS_CLIENT_REVIEW,user,
      "completeMcc");
    TierAssignQueueOps.doNewTierAssign(appSeqNum,user);
  }
  public static void doCompleteMcc(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierMccQueueOps()).completeMcc(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }
}
