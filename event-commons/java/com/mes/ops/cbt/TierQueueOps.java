package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class TierQueueOps extends BaseQueueOps
{
  static Logger log = Logger.getLogger(TierQueueOps.class);

  /**
   * Returns true if app tier num has been set to 3 or 4, which is stored
   * in merchant.client_data_2.
   */
  protected boolean appIsTier3or4(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String qs = " select  client_data_2                   "
                + " from    merchant                        "
                + " where   app_seq_num = ?                 "
                + "         and client_data_2               "
                + "           in ( '3G', '4G', '3F', '4F' ) ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsTier3or4()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Returns true if app tier num ends with 'G' (merchant.client_data_2)
   */
  protected boolean appIsTierG(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String qs = " select  client_data_2                   "
                + " from    merchant                        "
                + " where   app_seq_num = ?                 "
                + "         and client_data_2               "
                + "           in ( '1G', '2G', '3G', '4G' ) ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsTierG()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
  
  protected boolean appIs1G2G(long appSeqNum) throws Exception
  {
    PreparedStatement   ps = null;
    ResultSet           rs = null;
    
    try
    {
      connect();
      
      String qs = " select                                              "
                + "  m.app_seq_num                                      "
                + " from                                                "
                + "  merchant m                                         "
                + " where                                               "
                + "  m.app_seq_num = ?                                  "
                + "  and m.client_data_2 in ('1G', '2G')                ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIs3G4G()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
  
  /**
   * Used to test Tier 3/4 apps to determine if they are 3G or 4G
   */
  protected boolean appIs3G4G(long appSeqNum) throws Exception
  {
    PreparedStatement   ps = null;
    ResultSet           rs = null;
    
    try
    {
      connect();
      
      String qs = " select                                              "
                + "  m.app_seq_num                                      "
                + " from                                                "
                + "  merchant m                                         "
                + " where                                               "
                + "  m.app_seq_num = ?                                  "
                + "  and m.client_data_2 in ('3G', '4G')                ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIs3G4G()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Used to test Tier 3/4 apps to determine if they are 3F or 4F which means
   * they would be routed directly to the high risk financial queue without
   * first getting assigned a credit score
   */
  protected boolean appIs3F4F(long appSeqNum) throws Exception
  {
    PreparedStatement   ps = null;
    ResultSet           rs = null;
    
    try
    {
      connect();
      
      String qs = " select                                              "
                + "  m.app_seq_num                                      "
                + " from                                                "
                + "  merchant m                                         "
                + " where                                               "
                + "  m.app_seq_num = ?                                  "
                + "  and m.client_data_2 in ('3F', '4F')                ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIs3F4F()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Used to test Tier 3/4 apps to determine if they're considered high risk.
   * Return true for all 3F/4F, true for 3G/4G over $500K
   */
  protected boolean appIsHighRisk(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select                                              "
                + "  p.name                                             "
                + " from                                                "
                + "  cbt_credit_review_parms p,                         "
                + "  merchant m                                         "
                + " where                                               "
                + "  m.app_seq_num = ?                                  "
                + "  and p.name = 'high_risk_amount'                    "
                + "  and (m.client_data_2 in ('3F', '4F')               "
                + "       or (m.annual_vmc_sales >= to_number(p.value)  "
                + "           and m.client_data_2 in ('3G', '4G')))     ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsHighRisk()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Returns true if the average ticket of an app is over the large ticket
   * threshold defined in cbt_review_queue_parms.
   */
  protected boolean appIsLargeTicket(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                              "
                + " from    cbt_credit_review_parms p,          "
                + "         merchant m                          "
                + " where   m.app_seq_num = ?                   "
                + "         and m.merch_average_cc_tran         "
                + "           >= to_number(p.value)             "
                + "         and p.name = 'large_ticket_amount'  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsLargeTicket()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Used to test Tier 1/2 apps for high volume status.  Returns true if 
   * the transaction volume of an app is over the high volume threshold 
   * defined in cbt_credit_review_parms ($1M).
   */
  protected boolean appIsHighVolumeNew(long appSeqNum)
    throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                                        "
                + " from    cbt_credit_review_parms p,                    "
                + "         merchant m                                    "
                + " where   m.app_seq_num = ?                             "
                + "         and m.annual_vmc_sales >= to_number(p.value)  "
                + "         and p.name = 'high_volume_amount_new'         ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsHighVolumeNew()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  protected boolean appIsHighVolumeG(long appSeqNum)
    throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                                        "
                + " from    cbt_credit_review_parms p,                    "
                + "         merchant m                                    "
                + " where   m.app_seq_num = ?                             "
                + "         and m.annual_vmc_sales >= to_number(p.value)  "
                + "         and p.name = 'high_volume_amount_g'         ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsHighVolumeNew()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
  
  /**
   * Used to test Tier 1/2 apps for high volume status.  Returns true if 
   * the transaction volume of an app is over the high volume threshold 
   * defined in cbt_credit_review_parms ($1M).
   */
  protected boolean appIsHighVolume(long appSeqNum)
    throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                                        "
                + " from    cbt_credit_review_parms p,                    "
                + "         merchant m                                    "
                + " where   m.app_seq_num = ?                             "
                + "         and m.annual_vmc_sales >= to_number(p.value)  "
                + "         and p.name = 'high_volume_amount'             "
                + "         and m.client_data_2 not in ('3G', '4G')       ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsHighVolume()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Returns true if the credit score of an app is under the low credit score
   * threshold defined in cbt_review_queue_parms.
   */
  protected boolean appIsLowScoreNew(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                              "
                + " from    cbt_credit_review_parms p,          "
                + "         merchant m                          "
                + " where   m.app_seq_num = ?                   "
                + "         and cbt_credit_score                "
                + "           <= to_number(p.value)             "
                + "         and p.name = 'low_score_limit_new'  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsLowScoreNew()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
  
  /**
   * Returns true if the credit score of an app is under the low credit score
   * threshold defined in cbt_review_queue_parms.
   */
  protected boolean appIsLowScore(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                              "
                + " from    cbt_credit_review_parms p,          "
                + "         merchant m                          "
                + " where   m.app_seq_num = ?                   "
                + "         and cbt_credit_score                "
                + "           <= to_number(p.value)             "
                + "         and p.name = 'low_score_limit'  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("appIsLowScore()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
}