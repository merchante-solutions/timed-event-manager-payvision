/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * Handles ops for the various review queues: tier 3/4, high risk 3/4, 
 * large ticket, high volume, low score, financial.  Each of these have new, 
 * approved, declined, canceled and pended queues.  Tier 3/4 apps may move 
 * into high risk 3/4 queue or the score queues upon approval.  All others move 
 * into the doc queues upon approval.
 */
package com.mes.ops.cbt;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.user.UserBean;

public class TierReviewQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierReviewQueueOps.class);

  /********************************************************************
   *
   * Tier 3/4 review
   *
   *******************************************************************/

  public static final int Q_TIER_34_NEW        
                                  = MesQueues.Q_CBT_TIER_34_REVIEW_NEW;
  public static final int Q_TIER_34_APPROVED
                                  = MesQueues.Q_CBT_TIER_34_REVIEW_APPROVED;
  public static final int Q_TIER_34_DECLINED
                                  = MesQueues.Q_CBT_TIER_34_REVIEW_DECLINED;
  public static final int Q_TIER_34_CANCELED   
                                  = MesQueues.Q_CBT_TIER_34_REVIEW_CANCELED;
  public static final int Q_TIER_34_PENDED
                                  = MesQueues.Q_CBT_TIER_34_REVIEW_PENDED;

  private void newTier34(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_TIER_34_NEW,CS_CLIENT_REVIEW,
      user,"newTier34");
  }
  public static void doNewTier34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newTier34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelTier34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_TIER_34_APPROVED,"cancelTier34");
    moveQueueItem(appSeqNum,Q_TIER_34_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelTier34");
  }
  public static void doCancelTier34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelTier34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineTier34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_TIER_34_APPROVED,"declineTier34");
    moveQueueItem(appSeqNum,Q_TIER_34_DECLINED,CS_CLIENT_DECLINED,user,
      "declineTier34");
  }
  public static void doDeclineTier34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineTier34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendTier34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_TIER_34_APPROVED,"pendTier34");
    moveQueueItem(appSeqNum,Q_TIER_34_PENDED,CS_CLIENT_PENDED,user,
      "pendTier34");
  }
  public static void doPendTier34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendTier34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * 3G/4G < $500K -> scoring queues
   * 3G/4G > $500K -> high risk queues
   * 3F/4F -> high risk queues
   */
  private void approveTier34(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_TIER_34_APPROVED,CS_CLIENT_REVIEW,user,
      "approveTier34");

    // CB&T changed their minds -- they only want 3F/4F apps to go directly to
    // the High Risk Financial Queue.  3G/4G apps should go to the Credit Score
    // queue first.
    if (appIs3F4F(appSeqNum))
    {
      newFinancial(appSeqNum,user);
    }
    else
    {
      TierScoreQueueOps.doNewScore(appSeqNum,user);
    }
  }
  public static void doApproveTier34(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveTier34(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Tier 3/4 high volume review (high risk)
   *
   *******************************************************************/

  public static final int Q_HIGH_VOLUME_34_NEW        
                                  = MesQueues.Q_CBT_TIER_34_HIGH_VOLUME_NEW;
  public static final int Q_HIGH_VOLUME_34_APPROVED
                                  = MesQueues.Q_CBT_TIER_34_HIGH_VOLUME_APPROVED;
  public static final int Q_HIGH_VOLUME_34_DECLINED
                                  = MesQueues.Q_CBT_TIER_34_HIGH_VOLUME_DECLINED;
  public static final int Q_HIGH_VOLUME_34_CANCELED   
                                  = MesQueues.Q_CBT_TIER_34_HIGH_VOLUME_CANCELED;
  public static final int Q_HIGH_VOLUME_34_PENDED
                                  = MesQueues.Q_CBT_TIER_34_HIGH_VOLUME_PENDED;

  private void newHighVolume34(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_HIGH_VOLUME_34_NEW,CS_CLIENT_REVIEW,
      user,"newHighVolume34");
  }
  public static void doNewHighVolume34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newHighVolume34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelHighVolume34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_34_APPROVED,"cancelHighVolume34");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_34_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelHighVolume34");
  }
  public static void doCancelHighVolume34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelHighVolume34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineHighVolume34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_34_APPROVED,"declineHighVolume34");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_34_DECLINED,CS_CLIENT_DECLINED,user,
      "declineHighVolume34");
  }
  public static void doDeclineHighVolume34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineHighVolume34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendHighVolume34(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_34_APPROVED,"pendHighVolume34");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_34_PENDED,CS_CLIENT_PENDED,user,
      "pendHighVolume34");
  }
  public static void doPendHighVolume34(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendHighVolume34(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void approveHighVolume34(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_34_APPROVED,CS_CLIENT_REVIEW,user,
      "approveHighVolume34");
    TierDocQueueOps.doNewDoc(appSeqNum,user);
  }
  public static void doApproveHighVolume34(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveHighVolume34(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Large average ticket review
   *
   *******************************************************************/

  public static final int Q_LARGE_TKT_NEW        
                                  = MesQueues.Q_CBT_TIER_LARGE_TKT_NEW;
  public static final int Q_LARGE_TKT_APPROVED
                                  = MesQueues.Q_CBT_TIER_LARGE_TKT_APPROVED;
  public static final int Q_LARGE_TKT_DECLINED
                                  = MesQueues.Q_CBT_TIER_LARGE_TKT_DECLINED;
  public static final int Q_LARGE_TKT_PENDED
                                  = MesQueues.Q_CBT_TIER_LARGE_TKT_PENDED;
  public static final int Q_LARGE_TKT_CANCELED   
                                  = MesQueues.Q_CBT_TIER_LARGE_TKT_CANCELED;

  private void newLargeTicket(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_LARGE_TKT_NEW,CS_CLIENT_REVIEW,
      user,"newLargeTicket");
  }
  public static void doNewLargeTicket(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelLargeTicket(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LARGE_TKT_APPROVED,"cancelLargeTicket");
    moveQueueItem(appSeqNum,Q_LARGE_TKT_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelLargeTicket");
  }
  public static void doCancelLargeTicket(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineLargeTicket(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LARGE_TKT_APPROVED,"declineLargeTicket");
    moveQueueItem(appSeqNum,Q_LARGE_TKT_DECLINED,CS_CLIENT_DECLINED,user,
      "declineLargeTicket");
  }
  public static void doDeclineLargeTicket(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendLargeTicket(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LARGE_TKT_APPROVED,"pendLargeTicket");
    moveQueueItem(appSeqNum,Q_LARGE_TKT_PENDED,CS_CLIENT_PENDED,user,
      "pendLargeTicket");
  }
  public static void doPendLargeTicket(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void approveLargeTicket(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_LARGE_TKT_APPROVED,CS_CLIENT_REVIEW,user,
      "approveLargeTicket");
    TierDocQueueOps.doNewDoc(appSeqNum,user);
  }
  public static void doApproveLargeTicket(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * High annual volume review
   *
   *******************************************************************/

  public static final int Q_HIGH_VOLUME_NEW        
                                  = MesQueues.Q_CBT_TIER_HIGH_VOLUME_NEW;
  public static final int Q_HIGH_VOLUME_APPROVED
                                  = MesQueues.Q_CBT_TIER_HIGH_VOLUME_APPROVED;
  public static final int Q_HIGH_VOLUME_DECLINED
                                  = MesQueues.Q_CBT_TIER_HIGH_VOLUME_DECLINED;
  public static final int Q_HIGH_VOLUME_PENDED
                                  = MesQueues.Q_CBT_TIER_HIGH_VOLUME_PENDED;
  public static final int Q_HIGH_VOLUME_CANCELED   
                                  = MesQueues.Q_CBT_TIER_HIGH_VOLUME_CANCELED;

  private void newHighVolume(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_HIGH_VOLUME_NEW,CS_CLIENT_REVIEW,
      user,"newHighVolume");
  }
  public static void doNewHighVolume(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelHighVolume(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_APPROVED,"cancelHighVolume");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelHighVolume");
  }
  public static void doCancelHighVolume(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineHighVolume(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_APPROVED,"declineHighVolume");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_DECLINED,CS_CLIENT_DECLINED,user,
      "declineHighVolume");
  }
  public static void doDeclineHighVolume(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendHighVolume(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HIGH_VOLUME_APPROVED,"pendHighVolume");
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_PENDED,CS_CLIENT_PENDED,user,
      "pendHighVolume");
  }
  public static void doPendHighVolume(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void approveHighVolume(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_HIGH_VOLUME_APPROVED,CS_CLIENT_REVIEW,user,
      "approveHighVolume");
    TierDocQueueOps.doNewDoc(appSeqNum,user);
  }
  public static void doApproveHighVolume(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Low credit score review
   *
   *******************************************************************/

  public static final int Q_LOW_SCORE_NEW        
                                  = MesQueues.Q_CBT_TIER_LOW_SCORE_NEW;
  public static final int Q_LOW_SCORE_APPROVED
                                  = MesQueues.Q_CBT_TIER_LOW_SCORE_APPROVED;
  public static final int Q_LOW_SCORE_DECLINED
                                  = MesQueues.Q_CBT_TIER_LOW_SCORE_DECLINED;
  public static final int Q_LOW_SCORE_PENDED
                                  = MesQueues.Q_CBT_TIER_LOW_SCORE_PENDED;
  public static final int Q_LOW_SCORE_CANCELED   
                                  = MesQueues.Q_CBT_TIER_LOW_SCORE_CANCELED;

  private void newLowScore(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_LOW_SCORE_NEW,CS_CLIENT_REVIEW,
      user,"newLowScore");
  }
  public static void doNewLowScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelLowScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LOW_SCORE_APPROVED,"cancelLowScore");
    moveQueueItem(appSeqNum,Q_LOW_SCORE_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelLowScore");
  }
  public static void doCancelLowScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineLowScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LOW_SCORE_APPROVED,"declineLowScore");
    moveQueueItem(appSeqNum,Q_LOW_SCORE_DECLINED,CS_CLIENT_DECLINED,user,
      "declineLowScore");
  }
  public static void doDeclineLowScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendLowScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LOW_SCORE_APPROVED,"pendLowScore");
    moveQueueItem(appSeqNum,Q_LOW_SCORE_PENDED,CS_CLIENT_PENDED,user,
      "pendLowScore");
  }
  public static void doPendLowScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void approveLowScore(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_LOW_SCORE_APPROVED,CS_CLIENT_REVIEW,user,
      "approveLowScore");
    TierDocQueueOps.doNewDoc(appSeqNum,user);
  }
  public static void doApproveLowScore(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveLowScore(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Financial review
   *
   *******************************************************************/

  public static final int Q_FINANCIAL_NEW        
                                  = MesQueues.Q_CBT_TIER_FINANCIAL_NEW;
  public static final int Q_FINANCIAL_APPROVED
                                  = MesQueues.Q_CBT_TIER_FINANCIAL_APPROVED;
  public static final int Q_FINANCIAL_DECLINED
                                  = MesQueues.Q_CBT_TIER_FINANCIAL_DECLINED;
  public static final int Q_FINANCIAL_PENDED
                                  = MesQueues.Q_CBT_TIER_FINANCIAL_PENDED;
  public static final int Q_FINANCIAL_CANCELED   
                                  = MesQueues.Q_CBT_TIER_FINANCIAL_CANCELED;

  private void newFinancial(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_FINANCIAL_NEW,CS_CLIENT_REVIEW,
      user,"newFinancial");
  }
  public static void doNewFinancial(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).newFinancial(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelFinancial(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_FINANCIAL_APPROVED,"cancelFinancial");
    moveQueueItem(appSeqNum,Q_FINANCIAL_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelFinancial");
  }
  public static void doCancelFinancial(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).cancelFinancial(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void declineFinancial(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_FINANCIAL_APPROVED,"declineFinancial");
    moveQueueItem(appSeqNum,Q_FINANCIAL_DECLINED,CS_CLIENT_DECLINED,user,
      "declineFinancial");
  }
  public static void doDeclineFinancial(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).declineFinancial(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void pendFinancial(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_FINANCIAL_APPROVED,"pendFinancial");
    moveQueueItem(appSeqNum,Q_FINANCIAL_PENDED,CS_CLIENT_PENDED,user,
      "pendFinancial");
  }
  public static void doPendFinancial(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierReviewQueueOps()).pendFinancial(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void approveFinancial(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_FINANCIAL_APPROVED,CS_CLIENT_REVIEW,user,
      "approveFinancial");
    TierDocQueueOps.doNewDoc(appSeqNum,user);
  }
  public static void doApproveFinancial(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierReviewQueueOps()).approveFinancial(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }
}