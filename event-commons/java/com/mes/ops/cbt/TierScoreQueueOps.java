/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * This handles operations for the credit score queues.  This includes a monitor
 * method called by the timed event com.mes.startup.CbtAutoCreditMonitorEvent.
 * The scoring queues consist of auto, completed and canceled queues.  Scores
 * will usually be assigned via the MES automated credit scoring system.  There
 * are operations to allow manual submission of scores as well, and also to
 * restart/retry the automated scoring.
 */
package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.ops.CreditReportBean;
import com.mes.user.UserBean;

public class TierScoreQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierScoreQueueOps.class);

  public static final int     MAX_AUTO_MINUTES = 10;

  public static final String  CS_METHOD_AUTO   = "A";
  public static final String  CS_METHOD_MANUAL = "M";

  public static final int Q_CS_AUTO       
                                  = MesQueues.Q_CBT_TIER_SCORING_AUTO;
  public static final int Q_CS_COMPLETED  
                                  = MesQueues.Q_CBT_TIER_SCORING_COMPLETED;
  public static final int Q_CS_CANCELED   
                                  = MesQueues.Q_CBT_TIER_SCORING_CANCELED;

  /**
   * Sets an indicator in merchant.client_data_1 to indicate whether the
   * credit score is or has been pulled via the automated system or via
   * manual score entry.  A = auto (default), M = manual (auto failed)
   */
  private void setScoringMethod(long appSeqNum, String method)
    throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      String qs = " update  merchant          "
                + " set     client_data_1 = ? "
                + " where   app_seq_num = ?   ";

      ps = con.prepareStatement(qs);
      ps.setString(1,method);
      ps.setLong(2,appSeqNum);
      ps.execute();
    }
    catch (Exception e)
    {
      log.error("Error setting score method with app " 
        + appSeqNum + ": " + e);
      e.printStackTrace();
      logEntry("setScoringMethod()","Error with app " + appSeqNum
        + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * Looks for new average credit scores for apps that are in the cb&t
   * auto scoring review queue.  Any such scores found are copied to
   * merchant.cbt_credit_score and the app is moved into the completed
   * scoring queue.
   */
  private void autoComplete(UserBean user)
  {
    PreparedStatement ps  = null;
    PreparedStatement ps2 = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();

      // look for average score > 0 for merchants with null 
      // cbt_credit_score who are also in the cbt auto score queue

      String qs = " select                            "
                + "  q.id,                            "
                + "  s.average_score                  "
                + " from                              "
                + "  q_data q,                        "
                + "  merchant m,                      "
                + "  credit_requests_process p,       "
                + "  credit_score_average s           "
                + " where                             "
                + "  q.type = ?                       "
                + "  and q.id = p.app_seq_num         "
                + "  and p.date_completed is not null "
                + "  and q.id = s.app_seq_num         "
                + "  and nvl(s.average_score,0) > 0   "
                + "  and q.id = m.app_seq_num         "
                + "  and m.cbt_credit_score is null   ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,Q_CS_AUTO);
      rs = ps.executeQuery();

      qs = " update merchant          "
         + " set cbt_credit_score = ? "
         + " where app_seq_num = ?    ";

      while (rs.next())
      {
        long appSeqNum = rs.getLong("id");
        int score = rs.getInt("average_score");

        log.debug("completed appSeqNum " + appSeqNum + " found");

        // copy the average credit score into 
        // the cbt score column in merchant
        if (ps2 == null)
        {
          ps2 = con.prepareStatement(qs);
        }
        ps2.setInt(1,score);
        ps2.setLong(2,appSeqNum);
        ps2.execute();

        // move the associated auto score queue 
        // item to complete/mcc new
        completeScore(appSeqNum,user);
      }
    }
    catch(Exception e)
    {
      log.error("Error detecting completed scores: " + e);
      logEntry("autoComplete()", e.toString());
    }
    finally
    {
      try { ps2.close(); } catch (Exception e) { }
      cleanUp(ps,rs);
    }
  }

  /**
   * Looks for cb&t auto score queue apps that have taken too long to get
   * a credit score returned via the automated credit score system.  Any
   * such apps remain in the auto queue but the process_sequence in
   * credit_requests_process is set to a flag value to prevent further
   * automated attempts to fetch a score.
   */
  private void autoTimeout(UserBean user)
  {
    PreparedStatement ps =  null;
    PreparedStatement ps2 = null;
    ResultSet         rs =  null;
    
    try
    {
      connect();

      // look for apps that have taken to long to pull 
      // a credit score via the automated system
      String qs = " select                            "
                + "  q.id                             "
                + " from                              "
                + "  q_data q,                        "
                + "  credit_requests_process p        "
                + " where                             "
                + "  q.type = ?                       "
                + "  and q.id = p.app_seq_num         "
                + "  and p.date_completed is null     "
                + "  and p.process_sequence > -1      "
                + "  and p.date_created <             "
                + "   sysdate - interval             '" 
                +     String.valueOf(MAX_AUTO_MINUTES) 
                + "'  minute                          ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,Q_CS_AUTO);
      rs = ps.executeQuery();

      qs = " update credit_requests_process "
         + " set process_sequence = -1      "
         + " where app_seq_num = ?          ";

      while (rs.next())
      {
        long appSeqNum = rs.getLong("id");

        log.debug("failed appSeqNum " + appSeqNum + " found");

        // copy the average credit score into 
        // the cbt score column in merchant
        if (ps2 == null)
        {
          ps2 = con.prepareStatement(qs);
        }
        ps2.setLong(1,appSeqNum);
        ps2.execute();
      }
    }
    catch (Exception e)
    {
      log.error("Error in auto score timeout: " + e);
      logEntry("autoTimeout()", e.toString());
    }
    finally
    {
      try { ps2.close(); } catch (Exception e) { }
      cleanUp(ps,rs);
    }
  }

  /**
   * Entry point for com.mes.startup.CbtAutoCreditMonitorEvent.  Checks first
   * for completed scores and then handles any that have timed out.
   */
  public static void doAutoScoreMonitoring(UserBean user)
  {
    try
    {
      TierScoreQueueOps ops = new TierScoreQueueOps();
      ops.autoComplete(user);
      ops.autoTimeout(user);
    }
    catch (Exception e)
    {
      log.error("Error during auto credit scoring monitoring: " + e);
      e.printStackTrace();
      throw new RuntimeException(""+e);
    }
  }
  public static void doAutoScoreMonitoring()
  {
    doAutoScoreMonitoring(null);
  }


  /**
   * Tries to start the auto credit scoring process for the given app.
   * If process already in progress, the request is ignored.  If process
   * has already completed successfully, the request is ignored.  If process
   * has failed due to timeout or user cancellation a new entry is created
   * for the app in the process table.
   *
   * This is has the same effect as InsertCreditQueue.pullCredit().
   */
  private void startAutoScoring(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;

    //log.debug("starting auto scoring for app " + appSeqNum);

    try
    {
      if (TierTestQueueOps.isTestApp(appSeqNum))
      {
        //log.debug("test app " + appSeqNum 
        //  + " detected, auto scoring not initiated");
        return;
      }

      // determine auto score process state
      TierAutoState state = new TierAutoState(appSeqNum);

      // the auto score process is in progress or a
      // score has been received or manually submitted
      if (state.isInProgress || state.isComplete)
      {
        //log.debug("app " + appSeqNum + " is in progress or complete already");
        return;
      }

      // send an id request if it hasn't happened
      if (!state.hasId)
      {
        //log.debug("sending id request for app " + appSeqNum);
        CreditReportBean.sendIdRequest(appSeqNum);
        //log.debug("id request sent for app " + appSeqNum);
      }

      connect();

      // insert a new entry for the app into the process table
      String qs = " insert into credit_requests_process "
                + " ( app_seq_num ) values ( ? )        ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();

      //log.debug("credit_request_process record created for app " + appSeqNum);

      // make sure the scoring method indicates auto
      setScoringMethod(appSeqNum,CS_METHOD_AUTO);
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("startAutoScoring()","appSeqNum = " + appSeqNum);
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * Tries to halt the auto credit scoring process for the given app.
   * If process is not currently in progress the request is ignored.
   * The process_sequence for the app is set to -2 to indicate it was
   * halted by a user.
   */
  private void stopAutoScoring(long appSeqNum) 
    throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      // determine auto score process state
      TierAutoState state = new TierAutoState(appSeqNum);

      if (!state.isInProgress)
      {
        return;
      }

      connect();

      String qs = " update credit_requests_process  "
                + " set process_sequence = -2       "
                + " where app_seq_num = ? and process_sequence > 0 ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
    }
    catch (Exception e)
    {
      log.error("Error with app " + appSeqNum + ": " + e);
      logEntry("cancelAutoScoring()","appSeqNum = " + appSeqNum);
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * Called first time new app arrives in the review queues, starts
   * auto scoring and adds to the auto score queue.
   */
  private void newScore(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_CS_AUTO,CS_CLIENT_REVIEW,
      user,"newScoringAuto");
    startAutoScoring(appSeqNum);
  }
  public static void doNewScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierScoreQueueOps()).newScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }
  
  /**
   * Moves an app into the score completed queue.
   * From here apps need to route to low score, large ticket, high volume
   * or doc queues.
   */
  private void completeScore(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_CS_COMPLETED,CS_CLIENT_REVIEW,user,
      "completeScore");

    if( (appIs1G2G(appSeqNum) && appIsLowScoreNew(appSeqNum)) ||
        (! appIs1G2G(appSeqNum) && appIsLowScore(appSeqNum) ))
    {
      TierReviewQueueOps.doNewLowScore(appSeqNum,user);
    }
    else if( appIsTierG(appSeqNum) )
    {
      if( (appIs3G4G(appSeqNum) && appIsHighVolumeNew(appSeqNum)) ||
          (!appIs3G4G(appSeqNum) && appIsHighVolumeG(appSeqNum) ))
      {
        TierReviewQueueOps.doNewHighVolume(appSeqNum,user);
      }
      else
      {
        TierDocQueueOps.doNewDoc(appSeqNum,user);
      }
    }
    else if( appIsHighVolumeNew(appSeqNum) )
    {
      TierReviewQueueOps.doNewHighVolume(appSeqNum,user);
    }
    else
    {
      TierDocQueueOps.doNewDoc(appSeqNum,user);
    }
    
    /* this logic was removed as part of PRF-1399
    if( appIs3G4G(appSeqNum) )
    {
      if( appIsLowScore(appSeqNum) )
      {
        TierReviewQueueOps.doNewLowScore(appSeqNum,user);
      }
      else if( appIsHighVolumeNew(appSeqNum) )
      {
        TierReviewQueueOps.doNewHighVolume(appSeqNum,user);
      }
      else
      {
        TierDocQueueOps.doNewDoc(appSeqNum,user);
      }
    }
    else
    {
      if( appIsLowScoreNew(appSeqNum) )
      {
        TierReviewQueueOps.doNewLowScore(appSeqNum,user);
      }
      else if (appIsHighVolumeNew(appSeqNum))
      {
        TierReviewQueueOps.doNewHighVolume(appSeqNum,user);
      }
      else if (appIsHighRisk(appSeqNum))
      {
        TierReviewQueueOps.doNewHighVolume34(appSeqNum,user);
      }
      else if (appIsLargeTicket(appSeqNum))
      {
        TierReviewQueueOps.doNewLargeTicket(appSeqNum,user);
      }
      else
      {
        TierDocQueueOps.doNewDoc(appSeqNum,user);
      }
    }
    */
  }
  public static void doCompleteScore(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierScoreQueueOps()).completeScore(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Moves app to the canceled queue.  If auto score is in progress it
   * is stopped.
   */
  private void cancelScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_CS_COMPLETED,"cancelScore");
    stopAutoScoring(appSeqNum);
    moveQueueItem(appSeqNum,Q_CS_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelScore");
  }
  public static void doCancelScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierScoreQueueOps()).cancelScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Stops auto scoring if it is in progress and manually sets the credit 
   * score, sets the score method to manual, and runs completeScore() to
   * move app to completed score queue and into the new mcc assign queue.
   * The app must be in the auto queue to do this op.
   */
  private void manualScore(long appSeqNum, UserBean user, int score)
    throws Exception
  {
    validateInQueue(appSeqNum,Q_CS_AUTO,"manualScore");
    stopAutoScoring(appSeqNum);

    PreparedStatement ps = null;

    try
    {
      connect();

      String qs = " update  merchant              "
                + " set     cbt_credit_score = ?  "
                + " where   app_seq_num = ?       ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,score);
      ps.setLong(2,appSeqNum);
      ps.execute();
    }
    finally
    {
      cleanUp(ps);
    }

    setScoringMethod(appSeqNum,CS_METHOD_MANUAL);
    completeScore(appSeqNum,user);
  }
  public static void doManualScore(long appSeqNum, UserBean user, int score)
  {
    try
    {
      (new TierScoreQueueOps()).manualScore(appSeqNum,user,score);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Moves app from canceled back to auto score queue.  Resumes auto
   * credit score process if not timed out.
   */
  private void uncancelScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateInQueue(appSeqNum,Q_CS_CANCELED,"uncancelScore");
    moveQueueItem(appSeqNum,Q_CS_AUTO,CS_CLIENT_REVIEW,user,
      "uncancelScore");
    startAutoScoring(appSeqNum);
  }
  public static void doUncancelScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierScoreQueueOps()).uncancelScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Attempts to retry the auto credit score process.
   */
  private void retryScore(long appSeqNum, UserBean user)
    throws Exception
  {
    validateInQueue(appSeqNum,Q_CS_AUTO,"retryScore");
    startAutoScoring(appSeqNum);
  }
  public static void doRetryScore(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierScoreQueueOps()).retryScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }
}
