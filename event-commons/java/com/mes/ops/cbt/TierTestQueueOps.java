/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * Handles test operations.  Can be used to generate test apps in the queues.
 */
package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.user.UserBean;

public class TierTestQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierTestQueueOps.class);

  public static final long TEST_APP_FLAG = -31L;

  private void testApp(UserBean user, String name, int busType, double volume,
    double ticket, int score) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select application_sequence.nextval app_seq_num from dual ";
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();

      long appSeqNum = -1L;
      if (rs.next())
      {
        appSeqNum = rs.getLong("app_seq_num");
      }
      else
      {
        throw new RuntimeException("Error fetching app seq num");
      }

      rs.close();
      ps.close();

      qs  = " insert into application "
          + " ( app_seq_num,          "
          + "   appsrctype_code,      "
          + "   app_created_date,     "
          + "   app_user_id,          "
          + "   app_type,             "
          + "   screen_sequence_id,   "
          + "   app_user_login )      "
          + " values                  "
          + " ( ?,                    "
          + "   'CBT2',               "
          + "   sysdate,              "
          + "   ?,                    "
          + "   31,                   "
          + "   1008,                 "
          + "   ? )                   ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setLong(2,user.getUserId());
      ps.setString(3,user.getLoginName());
      ps.execute();
      ps.close();

      qs  = " insert into merchant          "
          + " ( app_seq_num,                "
          + "   merc_cntrl_number,          "
          + "   bustype_code,               "
          + "   merch_business_name,        "
          + "   annual_vmc_sales,           "
          + "   merch_month_visa_mc_sales,  "
          + "   merch_average_cc_tran,      "
          + "   cbt_credit_score )          "
          + " values                        "
          + " ( ?, ?, ?, ?, ?, ?, ?, ? )    ";
      
      double monthlyVolume = (int)(volume / 12 + 0.9999);

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setLong(2,TEST_APP_FLAG);
      ps.setInt(3,busType);
      ps.setString(4,name);
      ps.setDouble(5,volume);
      ps.setDouble(6,monthlyVolume);
      ps.setDouble(7,ticket);
      if (score != 0)
      {
        ps.setInt(8,score);
      }
      else
      {
        ps.setNull(8,java.sql.Types.INTEGER);
      }
      ps.execute();
      ps.close();

      qs  = " insert into address ( "
          + "  app_seq_num,         "
          + "  addresstype_code,    "
          + "  address_line1,       "
          + "  address_city,        "
          + "  countrystate_code,   "
          + "  address_zip )        "
          + " values (              "
          + "  ?,                   "
          + "  4,                   "
          + "  '1 South Van Ness',  "
          + "  'San Francisco',     "
          + "  'CA',                "
          + "  '97103' )            ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();

      qs  = " insert into businessowner (       "
          + "  app_seq_num,                     "
          + "  busowner_num,                    "
          + "  busowner_first_name,             "
          + "  busowner_last_name,              "
          + "  busowner_ssn )                   "
          + " values ( ?, 1, 'John', 'Doe', 0 ) ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();

      TierMccQueueOps.doNewMcc(appSeqNum,user);
    }
    catch (Exception e)
    {
      log.error("Error creating test app: " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }
  public static void doTestApp(UserBean user, String name, int busType,
    double volume, double ticket, int score)
  {
    try
    {
      (new TierTestQueueOps()).testApp(user,name,busType,volume,ticket,score);
    }
    catch (Exception e)
    {
      throw new RuntimeException(""+e);
    }
  }

  private boolean getIsTestApp(long appSeqNum) throws Exception
  {
    PreparedStatement ps  = null;
    ResultSet rs          = null;

    try
    {
      connect();
      String qs = " select  app_seq_num               "
                + " from    merchant                  "
                + " where   app_seq_num = ?           "
                + "         and merc_cntrl_number = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setLong(2,TEST_APP_FLAG);
      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error determining test app status: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  public static boolean isTestApp(long appSeqNum)
  {
    try
    {
      return (new TierTestQueueOps()).getIsTestApp(appSeqNum);
    }
    catch (Exception e)
    {
      throw new RuntimeException(""+e);
    }
  }

  private boolean transferFromOldQueues(long appSeqNum, UserBean user)
    throws Exception
  {
    PreparedStatement ps  = null;

    try
    {
      connect();
      String qs = " delete from q_data "
                + " where id = ? and type between 600 and 799 ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      TierScoreQueueOps.doNewScore(appSeqNum,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error determining test app status: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }
  public static boolean doTransferFromOldQueues(long appSeqNum, 
    UserBean user)
  {
    try
    {
      return (new TierTestQueueOps()).transferFromOldQueues(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException(""+e);
    }
  }

  private boolean resubmitToAutoScore(long appSeqNum, UserBean user)
    throws Exception
  {
    PreparedStatement ps  = null;

    try
    {
      connect();
      String qs = " delete from q_data "
                + " where id = ? and type between 2600 and 2799 ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps = con.prepareStatement(
        " delete from credit_requests_process where app_seq_num = ? ");
      ps.setLong(1,appSeqNum);
      ps.execute();
      TierScoreQueueOps.doNewScore(appSeqNum,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error determining test app status: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      cleanUp(ps);
    }
  }
  public static boolean doResubmitToAutoScore(long appSeqNum,
    UserBean user)
  {
    try
    {
      return (new TierTestQueueOps()).resubmitToAutoScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException(""+e);
    }
  }
}
