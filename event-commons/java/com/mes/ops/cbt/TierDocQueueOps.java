/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * Handles ops for documentation queues: new, completed, canceled and pended.
 * 
 * Moves completed apps on to the tier number assignment queue.
 */
package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;

public class TierDocQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierDocQueueOps.class);

  public static final int Q_DOC_ITEM      = MesQueues.Q_ITEM_TYPE_CBT_DOC;
  public static final int Q_DOC_NEW       = MesQueues.Q_CBT_TIER_DOC_NEW;
  public static final int Q_DOC_COMPLETED = MesQueues.Q_CBT_TIER_DOC_COMPLETED;
  public static final int Q_DOC_CANCELED  = MesQueues.Q_CBT_TIER_DOC_CANCELED;
  public static final int Q_DOC_PENDED    = MesQueues.Q_CBT_TIER_DOC_PENDED;

  /**
   * Returns true if the app is present in any of the documentation queues.
   */
  private boolean appInDocQueue(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String qs = " select  id                          "
                + " from    q_data                      "
                + " where   id = ?                      "
                + "         and type in ( ?, ?, ?, ? )  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,Q_DOC_NEW);
      ps.setInt(3,Q_DOC_COMPLETED);
      ps.setInt(4,Q_DOC_CANCELED);
      ps.setInt(5,Q_DOC_PENDED);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      logEntry("appIsLowScore()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  private void initDocData(long appSeqNum)
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // clear out current set of doc data
      String qs = " delete from q_doc_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();
      
      // setup a prepared statement to insert doc data
      qs  = " insert into q_doc_data  "
          + " ( id,                   "
          + "   doc_id,               "
          + "   doc_required,         "
          + "   doc_received )        "
          + " values                  "
          + " ( ?, ?, ?, ? )          ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,MesQueues.QDI_MERCHANT_AGREEMENT);
      ps.setString(3,"Y");
      ps.setString(4,"N");
      ps.execute();
    }
    catch (Exception e)
    {
      logEntry("submitDocData()","appSeqNum " + appSeqNum + ": " + e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  private void newDoc(long appSeqNum, UserBean user) throws Exception
  {
    if (!appInDocQueue(appSeqNum))
    {
      insertQueueItem(appSeqNum,Q_DOC_NEW,CS_CLIENT_REVIEW,user,"newDoc");
      initDocData(appSeqNum);
    }
  }
  public static void doNewDoc(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierDocQueueOps()).newDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }


  private void cancelDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "cancelDoc";
    validateInQueue(appSeqNum,Q_DOC_NEW,opName);
    moveQueueItem(appSeqNum,Q_DOC_CANCELED,CS_CLIENT_CANCELED,user,opName);
  }
  public static void doDocCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierDocQueueOps()).cancelDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void uncancelDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "uncancelDoc";
    validateInQueue(appSeqNum,Q_DOC_CANCELED,opName);
    moveQueueItem(appSeqNum,Q_DOC_NEW,CS_CLIENT_REVIEW,user,opName);
  }
  public static void doDocUncancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierDocQueueOps()).uncancelDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * "Pends" an app in the doc queues.  Apps in the pending queue are considered
   * complete for the purposes of MES (currently items will be pended if they
   * have received all doc items except onsite inspection), so when pending
   * the app is also placed into MES credit queues for MES setup.
   */
  private void pendDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "pendDoc";
    validateNotInQueue(appSeqNum,Q_DOC_PENDED,opName);
    validateNotInQueue(appSeqNum,Q_DOC_COMPLETED,opName);
    moveQueueItem(appSeqNum,Q_DOC_PENDED,CS_CLIENT_REVIEW,user,opName);

    // move into mes credit queues...
    if (!TierTestQueueOps.isTestApp(appSeqNum))
    {
      addMes(appSeqNum,user);
    }
  }
  public static void doDocPend(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierDocQueueOps()).pendDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Moves documentation app into doc completed queue.  If app was not previously
   * pended then the app must also be passed to MES for account setup and have it's
   * status updated.
   */
  private void completeDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "completeDoc";
    validateNotInQueue(appSeqNum,Q_DOC_COMPLETED,opName);
    if (QueueTools.getCurrentQueueType(appSeqNum,Q_DOC_ITEM) == Q_DOC_PENDED)
    {
      moveQueueItem(appSeqNum,Q_DOC_COMPLETED,CS_NO_CHANGE,user,opName);
    }
    else
    {
      moveQueueItem(appSeqNum,Q_DOC_COMPLETED,CS_CLIENT_REVIEW,user,opName);
      if (!TierTestQueueOps.isTestApp(appSeqNum))
      {
        addMes(appSeqNum,user);
      }
    }
  }
  public static void doDocComplete(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierDocQueueOps()).completeDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }
}
