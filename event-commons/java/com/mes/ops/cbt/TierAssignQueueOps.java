/**
 * Handles operations concerning the CB&T tiered review queues.  Extends the
 * base class TierQueueOps.
 *
 * Handles the tier number assignment queues, new, completed and canceled.
 * Upon completion it routes apps to tier 3/4, high ticket, large volume, low
 * score or doc queues based on the app data.
 */
package com.mes.ops.cbt;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.user.UserBean;

public class TierAssignQueueOps extends TierQueueOps
{
  static Logger log = Logger.getLogger(TierAssignQueueOps.class);

  public static final int Q_TIER_ASSIGN_NEW        
                                  = MesQueues.Q_CBT_TIER_NUM_ASSIGN_NEW;
  public static final int Q_TIER_ASSIGN_COMPLETED  
                                  = MesQueues.Q_CBT_TIER_NUM_ASSIGN_COMPLETED;
  public static final int Q_TIER_ASSIGN_CANCELED   
                                  = MesQueues.Q_CBT_TIER_NUM_ASSIGN_CANCELED;

  private void newTierAssign(long appSeqNum, UserBean user) 
    throws Exception
  {
    insertQueueItem(appSeqNum,Q_TIER_ASSIGN_NEW,CS_CLIENT_REVIEW,
      user,"newTierAssign");
  }
  public static void doNewTierAssign(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierAssignQueueOps()).newTierAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void cancelTierAssign(long appSeqNum, UserBean user)
    throws Exception
  {
    validateNotInQueue(appSeqNum,Q_TIER_ASSIGN_COMPLETED,"cancelTierAssign");
    moveQueueItem(appSeqNum,Q_TIER_ASSIGN_CANCELED,CS_CLIENT_CANCELED,user,
      "cancelTierAssign");
  }
  public static void doCancelTierAssign(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierAssignQueueOps()).cancelTierAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  private void uncancelTierAssign(long appSeqNum, UserBean user)
    throws Exception
  {
    validateInQueue(appSeqNum,Q_TIER_ASSIGN_CANCELED,"uncancelTierAssign");
    moveQueueItem(appSeqNum,Q_TIER_ASSIGN_NEW,CS_CLIENT_REVIEW,user,
      "uncancelTierAssign");
  }
  public static void doUncancelTierAssign(long appSeqNum, UserBean user)
  {
    try
    {
      (new TierAssignQueueOps()).uncancelTierAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /**
   * Upon completion of tier num assignment, app needs to be routed
   * to either Tier 3/4 review, a high risk review queue based on various
   * risk variables, or directly to the final doc queue before moving on
   * to MES setup.
   */
  private void completeTierAssign(long appSeqNum, UserBean user)
    throws Exception
  {
    moveQueueItem(appSeqNum,Q_TIER_ASSIGN_COMPLETED,CS_CLIENT_REVIEW,user,
      "completeTierAssign");

    // tier 3/4 apps go to tier 3/4 review
    if (appIsTier3or4(appSeqNum))
    {
      TierReviewQueueOps.doNewTier34(appSeqNum,user);
    }
    // 1G or 2G go to credit scoring
    else if (appIsTierG(appSeqNum))
    {
      TierScoreQueueOps.doNewScore(appSeqNum,user);
    }
    // all 1F or 2F go to financial review
    else
    {
      TierReviewQueueOps.doNewFinancial(appSeqNum,user);
    }
  }
  public static void doCompleteTierAssign(long appSeqNum, UserBean user)
  {
    try
    { 
      (new TierAssignQueueOps()).completeTierAssign(appSeqNum,user);
    }
    catch (Exception e)
    { 
      throw new RuntimeException("" + e);
    }
  }
}


