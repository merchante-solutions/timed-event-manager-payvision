/*@lineinfo:filename=AutoUploadMerchantDiscount*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AutoUploadMerchantDiscount.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-21 13:20:00 -0700 (Tue, 21 Aug 2007) $
  Version            : $Revision: 14046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AutoUploadMerchantDiscount extends SQLJConnectionBase
{
  private CardInfo visa           = null;
  private CardInfo mc             = null;
  private CardInfo diners         = null;
  private CardInfo discover       = null;
  private CardInfo amex           = null;
  private CardInfo jcb            = null;
  private CardInfo debit          = null;
  private CardInfo privateLabel1  = null;
  private CardInfo ebt            = null;
  private CardInfo visaCheck      = null;
  private CardInfo mcCheck        = null;
  private CardInfo visaBusiness   = null;
  private CardInfo mcBusiness     = null;

  private final int NUM_CARDS     = 13;
  public  final int[] cards       = new int[NUM_CARDS];
  
  private int     appType           = 0;

  private boolean cbt               = false;
  private boolean bbt               = false;
  private boolean transcom          = false;
  private boolean elmhurstNonDeploy = false;
  private boolean sabre             = false;

  public AutoUploadMerchantDiscount(long primaryKey)
  {

    visa          = new CardInfo(mesConstants.APP_CT_VISA,                "Visa");
    mc            = new CardInfo(mesConstants.APP_CT_MC,                  "MC");
    diners        = new CardInfo(mesConstants.APP_CT_DINERS_CLUB,         "Diners");
    discover      = new CardInfo(mesConstants.APP_CT_DISCOVER,            "Discover");
    amex          = new CardInfo(mesConstants.APP_CT_AMEX,                "Amex");
    jcb           = new CardInfo(mesConstants.APP_CT_JCB,                 "JCB");
    debit         = new CardInfo(mesConstants.APP_CT_DEBIT,               "Debit");
    privateLabel1 = new CardInfo(mesConstants.APP_CT_PRIVATE_LABEL_1,     "Private Label 1");
    ebt           = new CardInfo(mesConstants.APP_CT_EBT,                 "EBT");
    visaCheck     = new CardInfo(mesConstants.APP_CT_VISA_CHECK_CARD,     "Visa Check Card");
    mcCheck       = new CardInfo(mesConstants.APP_CT_MC_CHECK_CARD,       "MC Check Card");
    visaBusiness  = new CardInfo(mesConstants.APP_CT_VISA_BUSINESS_CARD,  "Visa Business Card");
    mcBusiness    = new CardInfo(mesConstants.APP_CT_MC_BUSINESS_CARD,    "MC Business Card");
    
    cards[0]  = mesConstants.APP_CT_VISA;
    cards[1]  = mesConstants.APP_CT_VISA_CHECK_CARD;
    cards[2]  = mesConstants.APP_CT_VISA_BUSINESS_CARD;
    cards[3]  = mesConstants.APP_CT_MC;
    cards[4]  = mesConstants.APP_CT_MC_CHECK_CARD;
    cards[5]  = mesConstants.APP_CT_MC_BUSINESS_CARD;
    cards[6]  = mesConstants.APP_CT_DINERS_CLUB;
    cards[7]  = mesConstants.APP_CT_DISCOVER;
    cards[8]  = mesConstants.APP_CT_AMEX;
    cards[9]  = mesConstants.APP_CT_JCB;
    cards[10] = mesConstants.APP_CT_DEBIT;
    cards[11] = mesConstants.APP_CT_PRIVATE_LABEL_1;
    cards[12] = mesConstants.APP_CT_EBT;

    getData(primaryKey);
    verifyNonBankNumsExist(primaryKey);
    submitData(primaryKey);
    submitAmexPcid(primaryKey);
  }

  public void getData(long primaryKey)
  {
    setAppType(primaryKey); //checks to see if is cbt application
    getCardInfo(primaryKey);
  }

  private void submitAmexPcid(long primaryKey)
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    String              amexPcid  = "";
     
    if(!amex.getCardSelected())
    {
      return;
    }

    try 
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sic_code
//          from   merchant 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sic_code\n        from   merchant \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AutoUploadMerchantDiscount",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        switch(rs.getInt("sic_code"))
        {
          case 5812:
          case 5814:
            amexPcid = "011100";
          break;
          
          case 7011:
            amexPcid = "011102";
          break;

          default:
            amexPcid = "010450";
          break;
        }
      }

      /*@lineinfo:generated-code*//*@lineinfo:155^7*/

//  ************************************************************
//  #sql [Ctx] { update merchant 
//          set    merch_amex_pcid_num = :amexPcid
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merchant \n        set    merch_amex_pcid_num =  :1 \n        where  app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,amexPcid);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAmexPcid: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }  


  //if no non bank card merchant number .. then we dont put anything in tape file
  //we changed it so that no plan types other than visa,mc or debit are put on tape file...
  private void verifyNonBankNumsExist(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:185^7*/

//  ************************************************************
//  #sql [Ctx] it = { select *
//          from   merchpayoption 
//          where  app_seq_num = :primaryKey and
//                 cardtype_code in (10,14,15,16)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select *\n        from   merchpayoption \n        where  app_seq_num =  :1  and\n               cardtype_code in (10,14,15,16)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AutoUploadMerchantDiscount",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        //if(rs.getString("merchpo_card_merch_number") == null || rs.getLong("merchpo_card_merch_number") == 0L)
        //{
           //since we dont setup plan types for non bank cards.. we just delete each one out.. but for sabre we want to leave the amex and discover plan in
          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_DISCOVER:
              if(!isSabre())
              {
                discover  = new CardInfo(mesConstants.APP_CT_DISCOVER,        "Discover");
              }
            break;
            case mesConstants.APP_CT_AMEX:
              if(!isSabre())
              {
                amex      = new CardInfo(mesConstants.APP_CT_AMEX,            "Amex");
              }
            break;
            case mesConstants.APP_CT_DINERS_CLUB:
              diners    = new CardInfo(mesConstants.APP_CT_DINERS_CLUB,     "Diners");
            break;
            case mesConstants.APP_CT_JCB:
              jcb       = new CardInfo(mesConstants.APP_CT_JCB,             "JCB");
            break;
          }
        //}
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "verifyNonBankNumsExist: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }  

  private void setAppType(long primaryKey)  
  {
    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { select app_type
//          
//          from   application 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_type\n         \n        from   application \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/

      this.cbt                = (appType == com.mes.constants.mesConstants.APP_TYPE_CBT || appType == com.mes.constants.mesConstants.APP_TYPE_CBT_NEW);
      this.transcom           = (appType == com.mes.constants.mesConstants.APP_TYPE_TRANSCOM);
      this.elmhurstNonDeploy  = (appType == com.mes.constants.mesConstants.APP_TYPE_ELM_NON_DEPLOY);
      this.bbt                = (appType == com.mes.constants.mesConstants.APP_TYPE_BBT);
      this.sabre              = (appType == com.mes.constants.mesConstants.APP_TYPE_SABRE);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setAppType: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void getCardInfo(long primaryKey)  
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    int                 i       = 0;
    CardInfo            card    = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    tranchrg
//          where   app_seq_num   = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    tranchrg\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AutoUploadMerchantDiscount",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        card = getCard(rs.getInt("cardtype_code"));
        if(card != null)
        {
          card.setCardSelected(true);

          card.setCardDiscType("G");

          if(isElmhurstNonDeploy())
          {
            switch(rs.getInt("cardtype_code"))
            {
              case mesConstants.APP_CT_VISA:
              case mesConstants.APP_CT_MC:
                card.setCardDiscType("N");
              break;
            }
          }

          card.setCardMinDisc("0000");
          card.setCardFloorLim("0000");
          card.setCardDiscRate( (isBlank(rs.getString("tranchrg_disc_rate")) ? "0.00" : rs.getString("tranchrg_disc_rate")) );
          card.setCardBRG(rs.getString("tranchrg_brg"));
                 
          if(isBlank(rs.getString("tranchrg_float_disc_flag")) || rs.getString("tranchrg_float_disc_flag").equals("N"))
          {
            card.setCardDiscMethod("F");
          }
          else
          {
            card.setCardDiscMethod("V");
          }
        
          if(isSabre())
          {
            card.setCardPerItem("0.00");
          }
          else
          {
            switch(rs.getInt("cardtype_code"))
            {
              case mesConstants.APP_CT_VISA:
              case mesConstants.APP_CT_VISA_CHECK_CARD:
              case mesConstants.APP_CT_VISA_BUSINESS_CARD:
              case mesConstants.APP_CT_MC:
              case mesConstants.APP_CT_MC_CHECK_CARD:
              case mesConstants.APP_CT_MC_BUSINESS_CARD:
                if(isTranscom())
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_per_tran")) ? "0.00" : rs.getString("tranchrg_per_tran")) );
                }
                else
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_pass_thru")) ? "" : rs.getString("tranchrg_pass_thru")) );
                }
               
              break;

              default:
                if(rs.getInt("cardtype_code") == mesConstants.APP_CT_EBT)
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_per_tran")) ? "0.00" : rs.getString("tranchrg_per_tran")) );
                }
                else
                {
                  card.setCardPerItem("0.00");
                }
              
              break;
            }
          }
          
          // default individual card plan bet to '*'
          card.setIndPlanBet("*");
          
          // handle special individual plan bet situations
          switch(appType)
          {
            case mesConstants.APP_TYPE_SABRE:
              switch(rs.getInt("cardtype_code"))
              {
                case mesConstants.APP_CT_VISA:
                  card.setIndPlanBet("0011");
                  break;
                case mesConstants.APP_CT_MC:
                  card.setIndPlanBet("0012");
                  break;
                case mesConstants.APP_CT_AMEX:
                  card.setIndPlanBet("0013");
                  break;
                case mesConstants.APP_CT_DINERS_CLUB:
                  card.setIndPlanBet("0015");
                  break;
                case mesConstants.APP_CT_DISCOVER:
                  card.setIndPlanBet("0016");
                  break;
              }
              break;
              
            case mesConstants.APP_TYPE_BBT:
              switch(rs.getInt("cardtype_code"))
              {
                case mesConstants.APP_CT_VISA:
                  card.setIndPlanBet("0001");
                  break;
                case mesConstants.APP_CT_MC:
                  card.setIndPlanBet("0002");
                  break;
              }
              break;

            case mesConstants.APP_TYPE_TRANSCOM:
            case mesConstants.APP_TYPE_MES_NEW:
              switch(rs.getInt("cardtype_code"))
              {
                case mesConstants.APP_CT_DEBIT:
                  card.setIndPlanBet("0010");
                  break;
              }
              break;
              
            case mesConstants.APP_TYPE_BCB:
              switch(rs.getInt("cardtype_code"))
              {
                case mesConstants.APP_CT_VISA:
                case mesConstants.APP_CT_MC:
                  card.setIndPlanBet("0014");
                  break;
              }
              break;
          }

          card.setCardDrtTable("9999");                              

          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_DISCOVER:
            case mesConstants.APP_CT_AMEX:
              //were settling for sabre for amex or something.. so we set this to 'D'
              if(isSabre() && false)  // JRF: disabled until Vital fixes their reject problem
              {
                card.setCardAchOption("D");
              }
              else
              {
                card.setCardAchOption("N");
              }
            break;
            default:
              card.setCardAchOption("D");
            break;
          }

          card.setCardAuthOption("N");
       
          if(isBbt())
          {
            card.setCardAchOption("C");
            card.setCardAuthOption("N");
          }
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void submitData(long primaryKey)
  {
    CardInfo          card    = null;

    try 
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:467^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from   billcard 
//          where  app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from   billcard \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:472^7*/

      for(int i=0; i<cards.length; i++)
      {
        card = getCard(cards[i]);
        
        if(card.getCardSelected())
        {
          /*@lineinfo:generated-code*//*@lineinfo:480^11*/

//  ************************************************************
//  #sql [Ctx] { insert into billcard
//              (
//                billcard_disc_type,
//                billcard_disc_method,
//                billcard_drt_number,
//                billcard_disc_rate,
//                billcard_peritem_fee,
//                billcard_disc_min,
//                billcard_flr_limit,
//                billcard_ach_option,
//                billcard_auth_option,
//                billcard_bet_number,
//                billcard_brg,
//                cardtype_code,
//                app_seq_num, 
//                billcard_sr_number
//              )
//              values
//              (
//                :card.getCardDiscType(),
//                :card.getCardDiscMethod(),
//                :card.getCardDrtTable(),
//                :card.getCardDiscRate(),
//                :card.getCardPerItem(),
//                :card.getCardMinDisc(),
//                :card.getCardFloorLim(),
//                :card.getCardAchOption(),
//                :card.getCardAuthOption(),
//                :card.getIndPlanBet(),
//                :card.getCardBRG(),
//                :card.getCardType(),
//                :primaryKey,
//                :i
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_704 = card.getCardDiscType();
 String __sJT_705 = card.getCardDiscMethod();
 String __sJT_706 = card.getCardDrtTable();
 String __sJT_707 = card.getCardDiscRate();
 String __sJT_708 = card.getCardPerItem();
 String __sJT_709 = card.getCardMinDisc();
 String __sJT_710 = card.getCardFloorLim();
 String __sJT_711 = card.getCardAchOption();
 String __sJT_712 = card.getCardAuthOption();
 String __sJT_713 = card.getIndPlanBet();
 String __sJT_714 = card.getCardBRG();
 int __sJT_715 = card.getCardType();
   String theSqlTS = "insert into billcard\n            (\n              billcard_disc_type,\n              billcard_disc_method,\n              billcard_drt_number,\n              billcard_disc_rate,\n              billcard_peritem_fee,\n              billcard_disc_min,\n              billcard_flr_limit,\n              billcard_ach_option,\n              billcard_auth_option,\n              billcard_bet_number,\n              billcard_brg,\n              cardtype_code,\n              app_seq_num, \n              billcard_sr_number\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 ,\n               :14 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.AutoUploadMerchantDiscount",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_704);
   __sJT_st.setString(2,__sJT_705);
   __sJT_st.setString(3,__sJT_706);
   __sJT_st.setString(4,__sJT_707);
   __sJT_st.setString(5,__sJT_708);
   __sJT_st.setString(6,__sJT_709);
   __sJT_st.setString(7,__sJT_710);
   __sJT_st.setString(8,__sJT_711);
   __sJT_st.setString(9,__sJT_712);
   __sJT_st.setString(10,__sJT_713);
   __sJT_st.setString(11,__sJT_714);
   __sJT_st.setInt(12,__sJT_715);
   __sJT_st.setLong(13,primaryKey);
   __sJT_st.setInt(14,i);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("submitData(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private boolean isBlank(String test)
  {
    boolean result = false;

    if(test == null || test.length() == 0 || test.equals(""))
    {
      result = true;
    }

    return result;

  }
  
  public boolean isCbt()
  {
    return this.cbt;
  }

  public boolean isBbt()
  {
    return this.bbt;
  }

  public boolean isSabre()
  {
    return this.sabre;
  }

  public boolean isTranscom()
  {
    return this.transcom;
  }

  public boolean isElmhurstNonDeploy()
  {
    return this.elmhurstNonDeploy;
  }

  public CardInfo getCard(int cardType)
  {
    CardInfo curCard = null;
    
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
        curCard = this.visa;
        break;
      case mesConstants.APP_CT_MC:
        curCard = this.mc;
        break;
      case mesConstants.APP_CT_DINERS_CLUB:
        curCard = this.diners;
        break;
      case mesConstants.APP_CT_DISCOVER:
        curCard = this.discover;
        break;
      case mesConstants.APP_CT_AMEX:
        curCard = this.amex;
        break;
      case mesConstants.APP_CT_JCB:
        curCard = this.jcb;
        break;
      case mesConstants.APP_CT_DEBIT:
        curCard = this.debit;
        break;
      case mesConstants.APP_CT_PRIVATE_LABEL_1:
        curCard = this.privateLabel1;
        break;
      case mesConstants.APP_CT_EBT:
        curCard = this.ebt;
        break;
      case mesConstants.APP_CT_VISA_CHECK_CARD:
        curCard = this.visaCheck;
        break;
      case mesConstants.APP_CT_MC_CHECK_CARD:
        curCard = this.mcCheck;
        break;
      case mesConstants.APP_CT_VISA_BUSINESS_CARD:
        curCard = this.visaBusiness;
        break;
      case mesConstants.APP_CT_MC_BUSINESS_CARD:
        curCard = this.mcBusiness;
        break;
    }
    return curCard;
  }
  
  public boolean getCardSelected(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardSelected());
  }
  public String getCardDiscType(int cardType)
  {
    String result = "";
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscType());
  }
  public String getCardDiscRate(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscRate());
  }
  public String getCardDrtTable(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDrtTable());
  }
  public String getCardDiscMethod(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscMethod());
  }
  public String getCardPerItem(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardPerItem());
  }
  public String getCardMinDisc(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardMinDisc());
  }
  public String getCardFloorLim(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardFloorLim());
  }

  public String getIndPlanBet(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getIndPlanBet());
  }

  
  public String getCardAchOption(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardAchOption());
  }
  public String getCardAuthOption(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardAuthOption());
  }

  public String getCardPlan(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardPlan());
  }

  public class CardInfo
  {
    private int cardType          = 0;
    private boolean cardSelected  = false;
    private String cardPlan       = "";
    private String discType       = "";
    private String discRate       = "";
    private String drtTable       = "";
    private String discMethod     = "";
    private String indPlanBet     = "";
    private String perItem        = "";
    private String minDisc        = "";
    private String floorLim       = "";
    private String achOption      = "";
    private String authOption     = "";
    private String brg            = "";

    CardInfo(int cardType, String cardPlan)
    {
      this.cardType = cardType;
      this.cardPlan = cardPlan;  
    }

    public void setCardBRG(String brg)
    {
      this.brg = brg;
    }
    public String getCardBRG()
    {
      return brg;
    }
    public void setCardDiscType(String discType)
    {
      this.discType = discType;  
    }
    public void setCardDiscRate(String discRate)
    {
      this.discRate = discRate;      
    }
    public void setCardDrtTable(String drtTable)
    {
      this.drtTable = drtTable;  
    }
    public void setCardDiscMethod(String discMethod)
    {
      this.discMethod = discMethod;
    }
    public void setCardPerItem(String perItem)
    {
      this.perItem = perItem;
    }
    public void setCardMinDisc(String minDisc)
    {
      this.minDisc = minDisc;
    }
    public void setCardFloorLim(String floorLim)
    {
      this.floorLim = floorLim;      
    }

    public void setCardAchOption(String achOption)
    {
      this.achOption = achOption;  
    }
    public void setCardAuthOption(String authOption)
    {
      this.authOption = authOption;      
    }
    
    public void setIndPlanBet(String indPlanBet)
    {
      this.indPlanBet = indPlanBet;      
    }

    public void setCardSelected(boolean cardSelected)
    {
      this.cardSelected = cardSelected;      
    }

    public boolean getCardSelected()
    {
      return this.cardSelected;  
    }

    public int getCardType()
    {
      return this.cardType;  
    }

    public String getCardAchOption()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.achOption;
      }
      return result;  
    }
    public String getCardAuthOption()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.authOption;
      }
      return result;  
    }


    public String getCardDiscType()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discType;
      }
      return result;  
    }
    public String getCardDiscRate()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discRate;
      }
      return result;  
    }
    public String getCardDrtTable()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.drtTable;
      }
      return result;  
    }
    public String getCardDiscMethod()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discMethod;
      }
      return result;  
    }
    public String getCardPerItem()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.perItem;
      }
      return result;  
    }
    public String getCardMinDisc()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.minDisc;
      }
      return result;  
    }

    public String getIndPlanBet()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.indPlanBet;
      }
      return result;  
    }


    public String getCardFloorLim()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.floorLim;
      }
      return result;  
    }
    public String getCardPlan()
    {
      return this.cardPlan;      
    }


  }



}/*@lineinfo:generated-code*/