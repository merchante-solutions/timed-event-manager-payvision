package com.mes.constants;

import java.util.HashMap;
import java.util.Map;

public enum FormatTypes {

    AIRLINES("AIRLINES"), CARRENTALS("CARRENTALS"), HOTELS("HOTELS"), OTHER("OTHER");

    private String formatType;
    private static Map<String, FormatTypes> formatTypeMap = new HashMap<>();
    static {
        for (FormatTypes formatCode : FormatTypes.values()) {
            formatTypeMap.put(formatCode.formatType, formatCode);
        }
    }

    FormatTypes(String formatType) {
        this.formatType = formatType;
    }

    public String getFormatType() {
        return formatType;
    }

    public static FormatTypes getFormatType(String key) {
        return formatTypeMap.get(key);
    }
}
