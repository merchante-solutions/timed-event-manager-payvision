/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesUsers.java $

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-09-11 14:07:20 -0700 (Fri, 11 Sep 2015) $
  Version            : $Revision: 23819 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesUsers
{
  /*
  ** User Types
  */

  public static final long    USER_NONE                           = 0;

  public static final long    USER_MERCHANT                       = 1;
  public static final long    USER_MES                            = 2;
  public static final long    USER_BANK                           = 3;
  public static final long    USER_SALES                          = 4;
  public static final long    USER_MERCHANT_ADMIN                 = 5;
  public static final long    USER_VERISIGN_REP                   = 6;
  public static final long    USER_VERISIGN_MANAGER               = 7;
  public static final long    USER_DEMO                           = 8;
  public static final long    USER_DEVELOPER                      = 9;
  public static final long    USER_DEFAULT                        = 10;
  public static final long    USER_CBT                            = 11;
  public static final long    USER_CEDAR                          = 12;
  public static final long    USER_FINANCE                        = 13;
  public static final long    USER_SVB                            = 14;
  public static final long    USER_ORANGE                         = 15;
  public static final long    USER_SALES_MANAGER                  = 16;
  public static final long    USER_NSI                            = 18;
  public static final long    USER_MES_OPS_MGT                    = 19;
  public static final long    USER_BRANNEN_INVERNESS              = 20;
  public static final long    USER_CEDARS_ADMIN                   = 21;
  public static final long    USER_MERCHANT_CHAIN                 = 22;


  public static final long    USER_CBT_AFFILIATE                  = 23;
  public static final long    USER_EAST_WEST                      = 24;
  public static final long    USER_FIRST_CITY                     = 25;
  public static final long    USER_SVB_SUMMARY                    = 26;
  public static final long    USER_MERCH_CB_RETRIEVAL             = 27;
  public static final long    USER_TRANSCOM                       = 28;
  public static final long    USER_STCU                           = 29;
  public static final long    USER_NBSC                           = 30;
  public static final long    USER_DISCOVER                       = 31;
  public static final long    USER_EMBRY                          = 32;
  public static final long    USER_ARROWHEAD                      = 33;
  public static final long    USER_BBT                            = 34;
  public static final long    USER_BBT_SALES_REP                  = 35;
  public static final long    USER_BBT_SALES_MANAGER              = 36;
  public static final long    USER_OHIO_SAVINGS_BANK              = 37;
  public static final long    USER_BANK_OF_INTERNET               = 38;
  public static final long    USER_UBOC_MERCHANT                  = 39;
  public static final long    USER_CBT_AGENT                      = 40;
  public static final long    USER_UBOC_BANK_USER                 = 41;
  public static final long    USER_UBOC_CHAIN_USER                = 42;

  public static final long    USER_RIVER_CITY_USER                = 65;

  public static final long    USER_BANNER_USER                    = 73;
  public static final long    USER_BANNER_VIEW_ONLY_USER          = 74;
  public static final long    USER_NETSUITE_USER                  = 75;
  public static final long    USER_SABRE_OLA_USER                 = 76;
  public static final long    USER_MERCHCOM_USER                  = 77;

  public static final long    USER_SABRE_MERCHANT                 = 85;
  public static final long    USER_SABRE_CHAIN                    = 86;

  public static final long    USER_ECX                            = 212;
  public static final long    USER_SVB_MERCHANT                   = 213;

  public static final long    USER_VISTA                          = 1020;
  public static final long    USER_GOLD                           = 1030;

  /*
  ** User Groups
  */

  public static final long    GROUP_ALL_USERS                     = 1;

  // employer groups
  public static final long    GROUP_MES                           = 101;
  public static final long    GROUP_VERISIGN                      = 102;
  public static final long    GROUP_CBT                           = 103;
  public static final long    GROUP_CEDAR                         = 104;
  public static final long    GROUP_SVB                           = 105;
  public static final long    GROUP_ORANGE                        = 106;
  public static final long    GROUP_NSI                           = 107;

  public static final long    GROUP_CALL_TRACKING                 = 108;
  public static final long    GROUP_CALL_TRACKING_RO              = 109;

  public static final long    GROUP_TRANSCOM                      = 110;
  public static final long    GROUP_BBT                           = 111;
  public static final long    GROUP_GOLD                          = 114;
  public static final long    GROUP_MMS_ACCESS                    = 120;
  // user types
  public static final long    GROUP_SUPERUSER                     = 200;
  public static final long    GROUP_MERCHANT                      = 201;
  public static final long    GROUP_MERCHANT_ADMIN                = 202;
  public static final long    GROUP_BANK                          = 203;
  public static final long    GROUP_SALES                         = 204;
  public static final long    GROUP_SALES_MANAGER                 = 205;
  public static final long    GROUP_USER_ADMIN                    = 206;
  public static final long    GROUP_SALES_ADMIN                   = 207;
  public static final long    GROUP_ACCOUNT_SERVICES              = 208;
  public static final long    GROUP_FINANCE                       = 209;
  public static final long    GROUP_PROFITABILITY                 = 210;
  public static final long    GROUP_ACCOUNTING_RPTS               = 212;
  public static final long    GROUP_SALES_SUMMARY_RPTS            = 213;
  public static final long    GROUP_SALES_COMMISSION_RPTS         = 214;
  public static final long    GROUP_MGT_TOOLS                     = 215;
  public static final long    GROUP_BANK_INCOME_EXPENSE           = 216;
  public static final long    GROUP_ACCOUNT_STATUS                = 217;
  public static final long    GROUP_BRANNEN_INVERNESS             = 218;
  public static final long    GROUP_EDIT_APPLICATION              = 219;
  public static final long    GROUP_RISK_REPORTS                  = 220;
  public static final long    GROUP_ADMIN_CB_REPORTS              = 221;
  public static final long    GROUP_INVENTORY_MGMT                = 222;
  public static final long    GROUP_RETRIEVAL_MGMT                = 223;
  public static final long    GROUP_MERCH_CB_REPORTS              = 224;
  public static final long    GROUP_MERCH_CB_PRENOT               = 225;
  public static final long    GROUP_REVENUE_REPORTS               = 226;
  public static final long    GROUP_INACTIVE_REPORTS              = 227;
  public static final long    GROUP_UNDERWATER_REPORTS            = 228;
  public static final long    GROUP_AVG_NET_DISC_REPORTS          = 229;
  public static final long    GROUP_CBT_BANK_REBATE_REPORTS       = 230;

  public static final long    GROUP_NBSC                          = 231;
  public static final long    GROUP_DISCOVER                      = 232;

  public static final long    GROUP_CLIENT_SERVICES               = 233;
  public static final long    GROUP_CBT_REPORTS                   = 234;
  public static final long    GROUP_APP_SUBMIT                    = 235;
  public static final long    GROUP_DISCOVER_REPORTS              = 236;
  public static final long    GROUP_SALES_REPORTS                 = 237;
  public static final long    GROUP_BBT_REPORTS                   = 238;
  public static final long    GROUP_UNMATCHED_CREDITS             = 239;
  public static final long    GROUP_PROSPECT_ADMINS               = 240;
  public static final long    GROUP_PROSPECT_USERS                = 241;
  public static final long    GROUP_PROSPECT_PEERS                = 246;
  public static final long    GROUP_COMMISSION_ADMIN              = 270;
  public static final long    GROUP_PAYPAL_CHECKOUT_SIGNUP        = 275;

  public static final long    GROUP_CBT_PRICING                   = 301;
  public static final long    GROUP_CBT_PRICING_ADMIN             = 302;
  public static final long    GROUP_CBT_DOCUMENTATION             = 303;
  public static final long    GROUP_CBT_ADMIN_USERS               = 304;
  public static final long    GROUP_CBT_SUPER_USERS               = 305;
  public static final long    GROUP_CBT_CREDIT_QUEUE_USERS        = 310;
  public static final long    GROUP_CBT_DOC_QUEUE_USERS           = 311;
  public static final long    GROUP_CBT_INCOMPLETE_QUEUE_USERS    = 312;
  public static final long    GROUP_CBT_DEMO_REPORTS              = 315;

  public static final long    GROUP_NEW_ACCOUNT_REPORT            = 316;
  public static final long    GROUP_BATCH_SUMMARY_REPORTS         = 317;
  public static final long    GROUP_VIEW_CONFIRMATION_CODES       = 318;
  public static final long    GROUP_CHAIN_STATEMENT               = 319;
  public static final long    GROUP_ACH_REJECTS                   = 320;
  public static final long    GROUP_MERCHANT_PRICING_DETAILS      = 321;
  public static final long    GROUP_MAINTENANCE_GROUP_NAMING      = 322;

  public static final long    GROUP_MRA_REJECTS                   = 480;


  public static final long    GROUP_SVB_CLIENT_DATA               = 700;
  public static final long    GROUP_TRANSMISSSION_REPORTS         = 701;

  public static final long    GROUP_ACTIVATION_QUEUE_ADMIN        = 1000;
  public static final long    GROUP_ACTIVATION_QUEUE_USER         = 1001;

  public static final long    GROUP_CBT_SALES_MATERIALS           = 1010;

  public static final long    GROUP_VISTA_USERS                   = 1020;

  public static final long    GROUP_UBOC_MERCHANTS                = 1030;
  public static final long    GROUP_UBOC_SUMMARY                  = 1031;

  public static final long    GROUP_BBT_MERCHANTS                 = 2000;
  public static final long    GROUP_BBT_SUMMARY                   = 2001;
  public static final long    GROUP_BBT_QA                        = 2002;
  public static final long    GROUP_BBT_MSR                       = 2003;

  public static final long    GROUP_REPORT_NEW_APP_REVENUE        = 3092;

  public static final long    GROUP_ACR_Q_HELP_CBT_MGMT           = 4150;
  public static final long    GROUP_ACR_Q_ASG_CBT_MGMT            = 4151;
  /*
  4200-4300 are reserved for ACR MGMT Queues
  */

  // miscellaneous
  public static final long    GROUP_USER_ADMIN_KING               = 666;
  public static final long    GROUP_DEMO                          = 901;
  public static final long    GROUP_DEFAULT                       = 902;
  public static final long    GROUP_MES_INTERNAL_VERISIGN_SALES   = 903;
  public static final long    GROUP_DISCOVER_IMS                  = 904;
  public static final long    GROUP_DISCOVER_BANK_REFERRAL        = 906;

  // tmg related groups
  public static final long    GROUP_TMG_USER                      = 8000;


  /*
  ** User Rights
  */

  // general permissions
  public static final long    RIGHT_PROFILE_CHANGE                = 1;
  public static final long    RIGHT_VALID_USER                    = 2;

  // mes internal permissions
  public static final long    RIGHT_ACCOUNT_SERVICING             = 101;
  public static final long    RIGHT_SALES_ADMIN                   = 102;
  public static final long    RIGHT_USER_ADMIN_PEON               = 103;
  public static final long    RIGHT_SYSTEM_ADMIN                  = 104;
  public static final long    RIGHT_USER_ADMIN_KING               = 105;
  public static final long    RIGHT_ACCOUNT_LOOKUP                = 106;
  public static final long    RIGHT_CALL_TRACKING                 = 107;
  public static final long    RIGHT_CALL_SUBMIT                   = 108;
  public static final long    RIGHT_INVENTORY_MANAGEMENT          = 109;
  public static final long    RIGHT_MES_INTERNAL_VERISIGN_SALES   = 110;
  public static final long    RIGHT_VIEW_ACH_REJECTS              = 111;
  public static final long    RIGHT_WORK_ACH_REJECTS_FROM_CIF     = 112;
  public static final long    RIGHT_VIEW_BANK_NOTES               = 113;
  public static final long    RIGHT_MODIFY_BANK_NOTES             = 114;
  public static final long    RIGHT_MMS_SCREENS                   = 115;
  public static final long    RIGHT_ACCOUNT_LOOKUP_BANNER         = 116;
  public static final long    RIGHT_COMMISSION_ADMIN              = 130;
  public static final long    RIGHT_VISA_MASTERCARD_REVERSAL      = 131;
  public static final long    RIGHT_VMC_FEE_COLLECTION_MESSAGES   = 132;


  // online app permissions
  public static final long    RIGHT_APPLICATION_PROXY             = 201;
  public static final long    RIGHT_APPLICATION_MGR               = 202;
  public static final long    RIGHT_APPLICATION_SUPER             = 203;
  public static final long    RIGHT_ACCOUNT_STATUS                = 204;
  public static final long    RIGHT_ACCOUNT_CHANGE                = 205;
  public static final long    RIGHT_APPLICATION_EDIT              = 206;
  public static final long    RIGHT_APP_CHANGE_SOURCE             = 207;
  public static final long    RIGHT_CANT_VIEW_APP                 = 208;
  public static final long    RIGHT_VIRTUAL_APP_SUBMIT            = 209;
  public static final long    RIGHT_VIEW_MATCH_RESULTS            = 210;
  public static final long    RIGHT_VIEW_APP_STATUS               = 211;

  // reporting rights
  public static final long    RIGHT_REPORT_MERCH                  = 301;
  public static final long    RIGHT_REPORT_ADMIN                  = 302;
  public static final long    RIGHT_REPORT_BANK                   = 303;
  public static final long    RIGHT_REPORT_COMMISSION             = 304;
  public static final long    RIGHT_REPORT_HR                     = 305;
  public static final long    RIGHT_REPORT_MANAGEMENT             = 306;
  public static final long    RIGHT_REPORT_MIS                    = 307;
  public static final long    RIGHT_REPORT_ACCOUNTING             = 308;
  public static final long    RIGHT_REPORT_RISK                   = 309;
  public static final long    RIGHT_REPORT_CHARGEBACK             = 310;
  public static final long    RIGHT_RETRIEVAL_MANAGEMENT          = 311;
  public static final long    RIGHT_REPORT_MERCH_CB_RETR          = 312;
  public static final long    RIGHT_REPORT_MERCH_CB_PRENOT        = 313;
  public static final long    RIGHT_REPORT_REVENUE                = 314;
  public static final long    RIGHT_REPORT_DEMO                   = 315;
  public static final long    RIGHT_REPORT_INACTIVE_MERCH         = 316;
  public static final long    RIGHT_REPORT_UNDER_WATER_MERCH      = 317;
  public static final long    RIGHT_REPORT_AVG_NET_DISC           = 318;
  public static final long    RIGHT_REPORT_BANK_INC_EXP           = 319;
  public static final long    RIGHT_REPORT_BANK_REBATE            = 320;
  public static final long    RIGHT_REPORT_CLIENT_SERVICES        = 321;
  public static final long    RIGHT_REPORT_NEW_ACCOUNTS           = 322;
  public static final long    RIGHT_REPORT_DISCOVER_SALES         = 323;
  public static final long    RIGHT_REPORT_SALES                  = 324;
  public static final long    RIGHT_REPORT_MERCH_RISK             = 325;
  public static final long    RIGHT_PROSPECT_USER                 = 326;
  public static final long    RIGHT_PROSPECT_ADMIN                = 327;
  public static final long    RIGHT_REPORT_AFFILIATE_MGMT         = 328;
  public static final long    RIGHT_REPORT_IC_SUMMARY             = 329;
  public static final long    RIGHT_REPORT_IC_MERCHANT            = 330;
  public static final long    RIGHT_REPORT_IC_ADMIN               = 331;
  public static final long    RIGHT_PROSPECT_PEER                 = 332;
  public static final long    RIGHT_REPORT_BANK_SALES_ACTIVITY    = 333;
  public static final long    RIGHT_REPORT_AUTH_LOG               = 334;
  public static final long    RIGHT_REPORT_UBOC_MERCHANT          = 335;
  public static final long    RIGHT_REPORT_UBOC_SUMMARY           = 336;
  public static final long    RIGHT_REPORT_AGENT_MGMT             = 337;
  public static final long    RIGHT_REPORT_AGENT_REBATE           = 338;
  public static final long    RIGHT_REPORT_AFFILIATE_REBATE       = 339;
  public static final long    RIGHT_REPORT_SVB_NET_PL             = 340;
  public static final long    RIGHT_REPORT_PORTFOLIO_DOWNLOAD     = 341;
  public static final long    RIGHT_REPORT_BATCH_SUMMARY          = 342;
  public static final long    RIGHT_REPORT_BANK_RECONCILE         = 343;
  public static final long    RIGHT_REPORT_CMS_ITINERARY          = 344;
  public static final long    RIGHT_REPORT_EXTRACT_SUMMARY        = 345;
  public static final long    RIGHT_REPORT_ACH_REJECTS            = 346;
  public static final long    RIGHT_REPORT_VIEW_PRICING           = 347;
  public static final long    RIGHT_SUMMIT_BANK_SALES             = 348;
  public static final long    RIGHT_BBT_ADD_MERCHANT              = 349;
  public static final long    RIGHT_REPORT_SALES_MANAGER          = 350;
  public static final long    RIGHT_REPORT_ALL_REJECTS            = 351;
  public static final long    RIGHT_REPORT_711                    = 352;
  public static final long    RIGHT_DISTRICT_MAINTENANCE          = 353;
  public static final long    RIGHT_REPORT_IC_LIABILITY           = 354;
  public static final long    RIGHT_REPORT_RETRIEVALS             = 355;
  public static final long    RIGHT_REPORT_RISK_ANALYST           = 356;
  public static final long    RIGHT_REPORT_CB_ADJ_SUMMARY         = 357;
  public static final long    RIGHT_REPORT_CB_ADJ_DETAILS         = 358;
  public static final long    RIGHT_REPORT_CB_PRE_SUMMARY         = 359;
  public static final long    RIGHT_REPORT_CB_PRE_DETAILS         = 360;
  public static final long    RIGHT_REPORT_RETRIEVAL_SUMMARY      = 361;
  public static final long    RIGHT_REPORT_RETRIEVAL_DETAILS      = 362;
  public static final long    RIGHT_REPORT_ASSOC_TRAN_DOWNLOAD    = 363;
  public static final long    RIGHT_REPORT_CUSTOM_QUERY           = 364;
  public static final long    RIGHT_REPORT_RISK_SCORE             = 365;
  public static final long    RIGHT_REPORT_DOWNGRADE_SUMMARY      = 366;
  public static final long    RIGHT_REPORT_DOWNGRADE_DETAILS      = 367;
  public static final long    RIGHT_REPORT_BORROW_BASE            = 368;
  public static final long    RIGHT_REPORT_MERCHANT_ACTIVATION    = 369;
  public static final long    RIGHT_REPORT_RISK_QUEUE_ADMIN       = 370;
  public static final long    RIGHT_REPORT_RISK_CB_DETAILS        = 371;
  public static final long    RIGHT_REPORT_RISK_RETR_DETAILS      = 372;
  public static final long    RIGHT_REPORT_IC_QUAL                = 373;
  public static final long    RIGHT_REPORT_MERCH_IC_QUAL          = 374;
  public static final long    RIGHT_REPORT_CB_BAL_SHEET           = 375;
  public static final long    RIGHT_REPORT_MERCH_BILLING          = 378;
  public static final long    RIGHT_REPORT_CHECK_SUMMARY          = 379;
  public static final long    RIGHT_REPORT_CHECK_DETAILS          = 380;
  public static final long    RIGHT_REPORT_ME_REVENUE             = 381;
  public static final long    RIGHT_REPORT_FILE_TRANSMISSION      = 382;
  public static final long    RIGHT_REPORT_IC_EDITOR              = 383;
  public static final long    RIGHT_REPORT_DISC_EVERGREEN_ADMIN   = 384;
  public static final long    RIGHT_REPORT_DISC_EVERGREEN_REPORT  = 385;
  public static final long    RIGHT_REPORT_BANKSERV_ACH           = 386;
  public static final long    RIGHT_REPORT_BANKSERV_PROCESS       = 387;
  public static final long    RIGHT_REPORT_REVENUE_RETENTION      = 388;
  public static final long    RIGHT_REPORT_PORTFOLIO_ACTIVITY     = 389;
  public static final long    RIGHT_REPORT_TPS_GRIN               = 390;
  public static final long    RIGHT_REPORT_BANKSERV_VIEW          = 391;
  public static final long    RIGHT_REPORT_SABRE_BACK_OFFICE      = 392;
  public static final long    RIGHT_REPORT_REJECT                 = 393;
  public static final long    RIGHT_REPORT_CHARGEBACK_AGING       = 394;
  public static final long    RIGHT_REPORT_RECONCILE_DETAILS      = 395;
  public static final long    RIGHT_REPORT_RECONCILE_SUMMARY      = 396;
  public static final long    RIGHT_REPORT_PENDING_BATCH_DETAILS  = 397;
  public static final long    RIGHT_REPORT_PENDING_BATCH_SUMMARY  = 398;
  public static final long    RIGHT_REPORT_SETTLEMENT_SUMMARY     = 399;
  public static final long    RIGHT_REPORT_SETTLEMENT_DETAILS     = 400;
  public static final long    RIGHT_REPORT_MGMT_VIEW_ONLY         = 410;
  public static final long    RIGHT_REPORT_EST_DEPOSIT            = 411;
  public static final long    RIGHT_REPORT_BANKSERV_UPLOAD        = 412;
  public static final long    RIGHT_REPORT_CERTEGY_MONTH_END      = 413;
  public static final long    RIGHT_REPORT_CERTEGY_CHECK_REBATE   = 414;
  public static final long    RIGHT_REPORT_NEW_APP_REVENUE        = 415;
  public static final long    RIGHT_REPORT_CERTEGY_REBATE_ADMIN   = 416;
  public static final long    RIGHT_REPORT_DDF_SETTLEMENT_SUMMARY = 417;
  public static final long    RIGHT_REPORT_COMDATA_TRAN_CHAIN     = 418;
  public static final long    RIGHT_REPORT_COMDATA_TRAN_MERCH     = 419;
  public static final long    RIGHT_REPORT_CBT_MILITARY           = 420;
  public static final long    RIGHT_REPORT_NEW_ACCOUNT_REVENUE    = 421;
  public static final long    RIGHT_REPORT_EPICOR_STAGING         = 422;
  public static final long    RIGHT_REPORT_TRIDENT_BATCHES        = 423;
  public static final long    RIGHT_REPORT_CHECK_SUMMARY_MERCH    = 424;
  public static final long    RIGHT_REPORT_CHECK_DETAILS_MERCH    = 425;
  public static final long    RIGHT_REPORT_AUTH_ITEM_DETAIL       = 426;
  public static final long    RIGHT_REPORT_TRIDENT_FILE_SUMMARY   = 427;
  public static final long    RIGHT_REPORT_HELD_BATCH_SUMMARY     = 428;
  public static final long    RIGHT_REPORT_AMEX_CB_UPLOAD         = 429;
  public static final long    RIGHT_REPORT_TRIDENT_PROFILE_UPLOAD = 430;
  public static final long    RIGHT_REPORT_VIEW_VNUMS             = 431;
  public static final long    RIGHT_REPORT_TRIDENT_BATCH_HOLD     = 432;
  public static final long    RIGHT_REPORT_SERVICECALL_UPLOAD     = 433;
  public static final long    RIGHT_REPORT_DOWNLOAD_STATEMENTS    = 434;
  public static final long    RIGHT_REPORT_RISK_FIND_TRANSACTION  = 435;
  public static final long    RIGHT_REPORT_TRIDENT_DEBIT          = 436;
  public static final long    RIGHT_REPORT_TPG_ACCESS_LOG         = 437;
  public static final long    RIGHT_REPORT_BUILD_TRIDENT_BATCH    = 440;
  public static final long    RIGHT_REPORT_TRIDENT_API_DEMO       = 441;
  public static final long    RIGHT_REPORT_FX_BATCH_SUMMARY       = 442;
  public static final long    RIGHT_REPORT_FX_BATCH_DETAILS       = 443;
  public static final long    RIGHT_REPORT_INTL_CB_SUMMARY        = 444;
  public static final long    RIGHT_REPORT_INTL_CB_DETAILS        = 445;
  public static final long    RIGHT_REPORT_INTL_RETR_SUMMARY      = 446;
  public static final long    RIGHT_REPORT_INTL_RETR_DETAILS      = 447;
  public static final long    RIGHT_REPORT_FX_IC_SUMMARY          = 448;
  public static final long    RIGHT_REPORT_FX_IC_DETAILS          = 449;
  public static final long    RIGHT_REPORT_DEFAULT_CURR_DATE      = 472;
  public static final long    RIGHT_REPORT_VIEW_BALANCE_SHEET     = 473;
  public static final long    RIGHT_REPORT_BALANCE_SHEET_ADMIN    = 474;
  public static final long    RIGHT_REPORT_CB_FIND                = 479;
  public static final long    RIGHT_REPORT_VIEW_ACH_SOURCE        = 481;
  public static final long    RIGHT_RISK_VIEW_LOSS_REPORT         = 482;
  public static final long    RIGHT_RISK_CREATE_LOSS_REPORT       = 483;
  public static final long    RIGHT_REPORT_PAYHERE_TRAN_DETAILS   = 484;
  public static final long    RIGHT_REPORT_ACH_PENDING            = 485;
  public static final long    RIGHT_REPORT_ACH_PENDING_ADMIN      = 486;
  public static final long    RIGHT_REPORT_API_STIP               = 487;
  public static final long    RIGHT_REPORT_SEE_ALL_CB_MERCHANTS   = 506;

  // ** moved RPT rights to 7000 block **
  public static final long    RIGHT_REPORT_INTL_SETTLEMENT_SUMMARY= 7000;
  public static final long    RIGHT_REPORT_INTL_SETTLEMENT_DETAILS= 7001;
  public static final long    RIGHT_REPORT_TRIDENT_ACH            = 7002;
  public static final long    RIGHT_REPORT_BML_SETTLEMENT         = 7003;
  public static final long    RIGHT_REPORT_MBS_FEE_COLL           = 7004;
  public static final long    RIGHT_REPORT_EXTERNAL_MATCH         = 7005;

  public static final long    RIGHT_VIEW_VPS_ISO_XML_APPS         = 456;

  public static final long    RIGHT_APP_VIEW_MES_DATE             = 610;

  public static final long    RIGHT_VIEW_POS_SURVEY               = 611;
  public static final long    RIGHT_EDIT_POS_SURVEY               = 612;

  public static final long    RIGHT_PAYPAL_REFERRAL_VETTING       = 614;

  public static final long    RIGHT_VIEW_USERS                    = 616;

  public static final long    RIGHT_REPORT_MERCHANT_REPORTS       = 4000;
  public static final long    RIGHT_REPORT_SUMMARY_REPORTS        = 4001;
  public static final long    RIGHT_MENU_ITEM_ACCOUNT_SERVICING   = 4002;
  public static final long    RIGHT_MENU_CBT_MILITARY_GUIDEs      = 4003;
  public static final long    RIGHT_MENU_VALUTEC_GIFT_SERVICES    = 4004;
  public static final long    RIGHT_MENU_CERTEGY_CHECK_SERVICES   = 4005;
  public static final long    RIGHT_MENU_MRA_REJECT_QUEUES        = 4006;

  // miscellaneous rights
  public static final long    RIGHT_GROUP_NAMING_CREATE           = 376;
  public static final long    RIGHT_CBT_PRICING                   = 401;
  public static final long    RIGHT_CBT_DOCUMENTATION             = 402;
  public static final long    RIGHT_CBT_PRICING_ADMIN             = 403;
  public static final long    RIGHT_ASSOC_FEE_ADMIN               = 404;
  public static final long    RIGHT_VIEW_CONFIRMATION_CODE        = 405;
  public static final long    RIGHT_MES_SALES_MENU                = 406;
  public static final long    RIGHT_CBT_CREDIT_QUEUES             = 407;
  public static final long    RIGHT_CBT_CREDIT_DOC_QUEUES         = 408;
  public static final long    RIGHT_CBT_INCOMPLETE_QUEUE          = 409;
  public static final long    RIGHT_CBT_REVIEW_MCC_QUEUES         = 459;
  public static final long    RIGHT_CBT_REVIEW_QUEUES             = 460;
  public static final long    RIGHT_CBT_REVIEW_DOC_QUEUES         = 461;
  public static final long    RIGHT_PCI_QUIZ_SUBMIT               = 462;
  public static final long    RIGHT_PCI_QUIZ_SEARCH               = 463;
  public static final long    RIGHT_CBT_VIEW_QUEUE_HISTORY        = 464;
  public static final long    RIGHT_PCI_QUIZ_VIEW                 = 465;
  public static final long    RIGHT_PCI_QUIZ_CIF_VIEW_SUMMARY     = 466;
  public static final long    RIGHT_PCI_QUIZ_CIF_SUBMIT_SUMMARY   = 467;
  public static final long    RIGHT_PCI_QUIZ_CIF_VIEW_DETAILS     = 468;
  public static final long    RIGHT_PCI_QUIZ_CQ_MERCHANT_STATUS   = 469;
  public static final long    RIGHT_PCI_QUIZ_RPT_STATUS_SUMMARY   = 470;
  public static final long    RIGHT_PCI_QUIZ_TESTING              = 471;
  public static final long    RIGHT_PCI_QUIZ_CIF_VIEW_DOC         = 480;

  // chargeback queue rights
  public static final long    RIGHT_QUEUE_CHARGEBACK              = 450;
  public static final long    RIGHT_QUEUE_RETRIEVAL               = 451;
  public static final long    RIGHT_QUEUE_CHARGEBACK_ADMIN        = 452;

  public static final long    RIGHT_ATTACH_DOCS                   = 454;

  public static final long    RIGHT_DSCVR_SALES_MATERIALS         = 501;

  public static final long    RIGHT_APP_STATUS_ONLY_DECLINES      = 601;
  public static final long    RIGHT_APP_CHANGE_QUEUES             = 602;
  public static final long    RIGHT_QUEUE_FORCE_UNLOCK            = 603;
  public static final long    RIGHT_DISCOVER_IMS                  = 604;
  public static final long    RIGHT_DISCOVER_BANK_REFERRAL        = 605;
  public static final long    RIGHT_APP_CONVERT_TO_NSI            = 606;
  public static final long    RIGHT_VIEW_ALL_NOTES                = 607;
  public static final long    RIGHT_APP_VIEW_NOTE                 = 608;
  public static final long    RIGHT_APP_EDIT_NOTE                 = 609;
  public static final long    RIGHT_EMAIL_DISTRIBUTION_ADMIN      = 613;

  public static final long    RIGHT_PAYPAL_CHECKOUT_ENROLLMENT    = 615;

  public static final long    RIGHT_SVB_CLIENT_DATA               = 700;
  public static final long    RIGHT_TRANSMISSION_REPORTS          = 701;

  public static final long    RIGHT_ACTIVATION_QUEUE_USERS        = 801;

  public static final long    RIGHT_REPORT_DEPOSIT_SUMMARY        = 900;
  public static final long    RIGHT_REPORT_STATEMENT_SUMMARY      = 902;

  public static final long    RIGHT_REPORT_CCR_UPLOAD             = 903;

  public static final long    RIGHT_REPORT_DEPOSIT_DETAILS        = 950;
  public static final long    RIGHT_REPORT_BATCH_DETAILS          = 951;
  public static final long    RIGHT_REPORT_STATEMENT_DETAILS      = 952;

  public static final long    RIGHT_HIDE_SERVICE_CALLS            = 953;

  public static final long    RIGHT_CIF_SCREEN_EDIT               = 954;

  public static final long    RIGHT_ACCOUNT_LOOKUP_EXTENSION      = 955;

  public static final long    RIGHT_MES_CREDIT_QUEUE              = 956;

  public static final long    RIGHT_REFRESH_HIERARCHY             = 957;
  public static final long    RIGHT_BANKSERV_EDIT                 = 958;

  public static final long    RIGHT_VIEW_EXTENDED_CONTACTS        = 959;
  public static final long    RIGHT_EDIT_EXTENDED_CONTACTS        = 960;

  public static final long    RIGHT_MERCHANT_ACH_EDIT             = 961;

  public static final long    RIGHT_ACTIVATION_QUEUE_ADMIN        = 1000;
  public static final long    RIGHT_ACTIVATION_QUEUE_USER         = 1001;

  public static final long    RIGHT_CBT_SALES_MATERIALS           = 1010;

  public static final long    RIGHT_CLIENT_USER_ADMIN             = 1020;
  public static final long    RIGHT_SEND_CLIENT_USER_EMAIL        = 1021;
  public static final long    RIGHT_CLIENT_USER_ADMIN_COMDATA     = 1022;
  public static final long    RIGHT_MANAGE_B2F                    = 1023;
  public static final long    RIGHT_MANAGE_AUTO_NOTIFY            = 1024;


  public static final long    RIGHT_AASW_DATA                     = 1050;
  public static final long    RIGHT_HIDE_APP                      = 1051;

  public static final long    RIGHT_RESTRICT_DEBIT_EXCEPTIONS     = 1052;

  public static final long    RIGHT_VIEW_CREDIT_WORKSHEET         = 1100;
  public static final long    RIGHT_VIEW_TERM_PROF_GEN_CIF        = 1101;

  // individual custom query rights - reserve 1500-2000 for these
  public static final long    RIGHT_CUSTOM_BONG_ADDRESS_DOWNLOAD  = 1500;
  public static final long    RIGHT_CUSTOM_TPS_ACTIVITY_BY_DAY    = 1501;
  public static final long    RIGHT_VIEW_IC_ENHANCEMENT           = 1711;
  //

  public static final long    RIGHT_REPORT_BBT_MERCHANT           = 2000;
  public static final long    RIGHT_REPORT_BBT_SUMMARY            = 2001;
  public static final long    RIGHT_BBT_QA                        = 2002; // i.e. application qa queues
  public static final long    RIGHT_BBT_MSR                       = 2003;

  public static final long    RIGHT_SHOW_CARD_NUM                 = 2050;

  public static final long    RIGHT_EVENT_MANAGER_CONTROL         = 2500;

  public static final long    RIGHT_MAINTENANCE_GROUP_NAMING      = 3000;

  public static final long    RIGHT_CLIENT_REVIEW_QUEUES          = 3001;
  public static final long    RIGHT_SUBMIT_QUEUE_NOTES            = 3002;

  // [3100 - 3600] ACR (Account Change Request) related
  public static final long    RIGHT_ACR_SYSTEM                    = 3100; // Access to the ACR system
  public static final long    RIGHT_ACR_CREATE                    = 3101; // Ability to submit a new ACR
  public static final long    RIGHT_ACR_STATUS_VIEW               = 3102; // Ability to view an existing ACR in read only mode
  public static final long    RIGHT_ACR_COMMAND                   = 3103; // Ability to issue commands against an existing ACR
  public static final long    RIGHT_ACR_COMMAND_RESTRICT          = 3104; // Ability to issue commands against an existing ACR (with restrictions)
  public static final long    RIGHT_ACR_DDR_TR_VIEW               = 3105; // Ability to view DDR/TR details

  // specific rights corresponding to each acr type
  public static final long    RIGHT_ACR_TYPE_UNDEFINED            = 3110;
  public static final long    RIGHT_ACR_TYPE_FEECHNG              = 3111; // fee changes
  public static final long    RIGHT_ACR_TYPE_TERMACCESS           = 3112; // terminal accessories
  public static final long    RIGHT_ACR_TYPE_DDACHNG              = 3113; // dda change
  public static final long    RIGHT_ACR_TYPE_CARDADDS             = 3114; // card adds
  public static final long    RIGHT_ACR_TYPE_ACCCLS               = 3115; // account closure
  public static final long    RIGHT_ACR_TYPE_SUPORDFRM            = 3116; // supply order form
  public static final long    RIGHT_ACR_TYPE_ADRCHNG              = 3117; // address change
  public static final long    RIGHT_ACR_TYPE_DBACHNG              = 3118; // dba change
  public static final long    RIGHT_ACR_TYPE_EQUIPSWAP            = 3119; // equipment swap
  public static final long    RIGHT_ACR_TYPE_FEERVSL              = 3120; // fee reversal
  public static final long    RIGHT_ACR_TYPE_REOPCL               = 3121; // seasonal re-open/close
  public static final long    RIGHT_ACR_TYPE_EQUIPUPG             = 3122; // equipment upgrade
  public static final long    RIGHT_ACR_TYPE_REQTRMPRG            = 3123; // request for terminal programming
  public static final long    RIGHT_ACR_TYPE_EQUIPPU              = 3124; // equip pickup
  public static final long    RIGHT_ACR_TYPE_RFNEWTYPE            = 3125; // request for change not listed
  public static final long    RIGHT_ACR_TYPE_PRICECHANGE          = 3126; // price change
  public static final long    RIGHT_ACR_TYPE_MERCCNTCTINFO        = 3127; // merchant contact info change
  public static final long    RIGHT_ACR_TYPE_SUPORDFRM_NEW        = 3128; // new supply order form
  public static final long    RIGHT_ACR_TYPE_SUPBILCONF           = 3129; // supply billing config
  public static final long    RIGHT_ACR_TYPE_CANSUPORD            = 3130; // supply order cancellation
  public static final long    RIGHT_ACR_TYPE_DEBITGIFT            = 3149; // debit and gift card
  public static final long    RIGHT_ACR_TYPE_EQUIPDISP            = 3150; // debit and gift card
  public static final long    RIGHT_ACR_TYPE_SEASONAL             = 3151; // debit and gift card

  public static final long    RIGHT_ACR_TYPE_SEASACTVT_V2         = 3131; // seasonal activity
  public static final long    RIGHT_ACR_TYPE_EQUIPDISP_V2         = 3132; // equip disposition
  public static final long    RIGHT_ACR_TYPE_OTHERCARDS_V2        = 3133; // non-credit card maintenance
  public static final long    RIGHT_ACR_TYPE_TERMACCESS_V2        = 3134; // all new beans (below)
  public static final long    RIGHT_ACR_TYPE_DDACHNG_V2           = 3135; //  ""
  public static final long    RIGHT_ACR_TYPE_CARDADDS_V2          = 3136; //  ""
  public static final long    RIGHT_ACR_TYPE_ACCCLS_V2            = 3137; //  ""
  public static final long    RIGHT_ACR_TYPE_ADRCHNG_V2           = 3138; //  ""
  public static final long    RIGHT_ACR_TYPE_DBACHNG_V2           = 3139; //  ""
  public static final long    RIGHT_ACR_TYPE_EQUIPSWAP_V2         = 3140; //  ""
  public static final long    RIGHT_ACR_TYPE_FEERVSL_V2           = 3141; //  ""
  public static final long    RIGHT_ACR_TYPE_EQUIPUPG_V2          = 3142; //  ""
  public static final long    RIGHT_ACR_TYPE_REQTRMPRG_V2         = 3143; //  ""
  public static final long    RIGHT_ACR_TYPE_EQUIPPU_V2           = 3144; //  ""
  public static final long    RIGHT_ACR_TYPE_MERCCNTCTINFO_V2     = 3145; //  ""
  public static final long    RIGHT_ACR_TYPE_PRICECHANGE_V2       = 3146; //  ""
  public static final long    RIGHT_ACR_TYPE_ACCREOP_V2           = 3147; //  ""
  public static final long    RIGHT_ACR_TYPE_SUPORDFRM_NEW_V2     = 3148; // all new beans (above)

  //BBT specific as they are operating a completely independent ACR system
  public static final long    RIGHT_ACR_BBT_CREATE                = 3201; // Ability to submit a new BBT ACR
  public static final long    RIGHT_ACR_BBT_STATUS_VIEW           = 3202; // Ability to view an existing BBT ACR in read only mode
  public static final long    RIGHT_ACR_BBT_COMMAND               = 3203; // Ability to issue commands against an existing BBT ACR

  public static final long    RIGHT_ACR_Q_BBT_RESEARCH            = 3211;
  public static final long    RIGHT_ACR_Q_BBT_RISK                = 3212;
  public static final long    RIGHT_ACR_Q_BBT_DEPLOYMENT          = 3213;
  public static final long    RIGHT_ACR_Q_BBT_RESEARCH_QA         = 3214;
  public static final long    RIGHT_ACR_Q_BBT_ACTIVATIONS         = 3215;
  public static final long    RIGHT_ACR_Q_BBT_PRICING             = 3216;
  public static final long    RIGHT_ACR_Q_BBT_TSYS_ACCOUNT_SETUP  = 3217;
  public static final long    RIGHT_ACR_Q_BBT_MMS_ACCOUNT_SETUP   = 3218;
  public static final long    RIGHT_ACR_Q_BBT_MGMT                = 3219;
  public static final long    RIGHT_ACR_Q_BBT_COMPLETED           = 3220;
  public static final long    RIGHT_ACR_Q_BBT_CANCELLED           = 3221;

  public static final long    RIGHT_ACR_Q_CBT_MGMT                = 3300;
  public static final long    RIGHT_ACR_Q_CBT_MMS                 = 3301;
  public static final long    RIGHT_ACR_Q_CBT_TSYS                = 3302;
  public static final long    RIGHT_ACR_Q_CBT_NEW                 = 3303;
  public static final long    RIGHT_ACR_Q_CBT_RISKMGMT            = 3304;

  public static final long    RIGHT_ACR_Q_HELP_CBT_MGMT           = 3402;
  public static final long    RIGHT_ACR_Q_ASG_CBT_MGMT            = 3403;

  public static final long    RIGHT_ACR_Q_BANNER_MGMT             = 3501;
  public static final long    RIGHT_ACR_Q_BUTTE_MGMT              = 3502;
  public static final long    RIGHT_ACR_Q_STERLING_MGMT           = 3503;
  public static final long    RIGHT_ACR_Q_FM_MGMT                 = 3504;
  public static final long    RIGHT_ACR_Q_TRANSCOM_MGMT           = 3505;
  public static final long    RIGHT_ACR_Q_CEDARS_MGMT             = 3506;
  public static final long    RIGHT_ACR_Q_CDMS_MGMT               = 3507;
  public static final long    RIGHT_ACR_Q_ELMHURST_MGMT           = 3508;
  public static final long    RIGHT_ACR_Q_FF_MGMT                 = 3509;
  public static final long    RIGHT_ACR_Q_FOOTHILL_MGMT           = 3510;
  public static final long    RIGHT_ACR_Q_MWB_MGMT                = 3511;
  public static final long    RIGHT_ACR_Q_RIVER_MGMT              = 3512;
  public static final long    RIGHT_ACR_Q_SILICON_MGMT            = 3513;
  public static final long    RIGHT_ACR_Q_GOLD_MGMT               = 3514;
  public static final long    RIGHT_ACR_Q_ORANGE_MGMT             = 3515;
  public static final long    RIGHT_ACR_Q_EXCHANGE_MGMT           = 3516;
  public static final long    RIGHT_ACR_Q_COUNTRY_MGMT            = 3517;
  public static final long    RIGHT_ACR_Q_BELL_MGMT               = 3518;


  public static final long    RIGHT_PG_PROFILE_ADMIN              = 5000; // modify pg profiles
  public static final long    RIGHT_PG_PROFILE_LOOKUP             = 5001; // view pg profiles
  public static final long    RIGHT_PG_EDIT_ALLOW_CREDITS         = 5002;
  public static final long    RIGHT_PG_BACK_OFFICE                = 5100; // basic back office reports
  public static final long    RIGHT_PG_BACK_OFFICE_VIEW_ONLY      = 5200;
  public static final long    RIGHT_PG_MINI_VIRTUAL_TERMINAL      = 5300;
  public static final long    RIGHT_PG_MINI_VIRTUAL_TERMINAL_VIEW_ONLY = 5400;
  public static final long    RIGHT_PG_TELE_PAY                   = 5500;
  public static final long    RIGHT_PG_SECURE_CHECKOUT            = 5600;
  public static final long    RIGHT_TMG_USER                      = 6000;

  public static final long    RIGHT_CHARGEBACK_API                = 6030;

  public static final long    RIGHT_MBS_MERCHANT_VIEW             = 8000;
  public static final long    RIGHT_MBS_MERCHANT_EDIT             = 8001;

  public static final long    RIGHT_MBS_STMT_MESSAGE_VIEW         = 8003;
  public static final long    RIGHT_MBS_STMT_MESSAGE_EDIT         = 8004;
  public static final long    RIGHT_MBS_STMT_MESSAGE_VIEW_ALL     = 8005;

};
