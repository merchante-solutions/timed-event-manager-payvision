package com.mes.constants;

public class FlatFileDefConstants {
    
    //Amex Optblue SPM Event
    public static final String SELLER_NAME =  "seller_name";
    public static final String SELLER_ID = "seller_id";
    public static final String SELLER_LEGAL_NAME = "seller_legal_name";
    public static final String SELLER_URL = "seller_url";
    public static final String SELLER_DBA_NAME = "seller_dba_name";
    public static final String SELLER_MCC = "seller_mcc";
    public static final String SELLER_BUS_PHONE = "seller_bus_phone";
    public static final String SIGNER_TITLE = "signer_title";
    public static final String OWNER_SSN = "owner_ssn";
    public static final String SELLER_ZIP = "seller_zip";
    public static final String OWNER_ZIP = "owner_zip";
    public static final String SELLER_FED_TAXID = "seller_fed_taxid";
    public static final String OWNER_NAME = "owner_name";
    public static final String SIGNER_FIRST_NAME = "signer_first_name";
    public static final String SIGNER_LAST_NAME = "signer_last_name";
    //Updated to correct value to match with database column value for flat file def 1017
    public static final String OWNER_FIRST_NAME = "owner_firstname";
    public static final String OWNER_LAST_NAME = "owner_lastname";
    public static final String SELLER_ADDRESS_1 = "seller_address_1";
    public static final String OWNER_ADDRESS_1 = "owner_address_1";
    public static final String OWNER_STATE = "owner_state";
    public static final String SELLER_STATE = "seller_state";
    public static final String OWNER_COUNTRY = "owner_country";
    public static final String SELLER_COUNTRY = "seller_country";
    public static final String OWNER_TYPE_INDICATOR = "owner_type_indicator";
    public static final String OWNER = "Owner";
    public static final String UNKNOWN= "UNKNOWN";
    public static final String IRS_RESULT = "irs_result";
    public static final String OWNER_CITY= "owner_city";
    public static final String OWNER_DOB= "owner_dob";
    public static final String BANK_NUM= "bank_number";
    public static final String SELLER_CITY= "seller_city";
    public static final String CITY1_LINE_4= "city1_line_4";
    public static final String STATE1_LINE_4= "state1_line_4";
    public static final String CARDTYPE_CODE= "cardtype_code";
    public static final String JCB_INDICATOR= "jcb_indicator";
    
  //Control Pong Data
    public static final String BUSOWNER_FIRST_NAME= "busowner_first_name";
    public static final String BUSOWNER_LAST_NAME= "busowner_last_name";
    public static final String BUSOWNER_SSN= "busowner_ssn";
    public static final String BUSOWNER_DL_DOB= "busowner_dl_dob";
    public static final String COUNTRYSTATE_CODE= "countrystate_code";
    public static final String BUSOWNER_TITLE= "busowner_title";
    public static final String ADDRESS_CITY= "address_city";
    public static final String ADDRESS_ZIP= "address_zip";
    public static final String ADDRESS_LINE1= "address_line1"; 

  //Discover Map Autoboard Event
    public static final String MERCHANT_NUMBER = "merch_number";
    public static final String RECORD_TYPE = "record_type";
    public static final String RECORD_SEQUENCE_NUMBER = "record_sequence_number";
    public static final String ACQUIRER_ID = "acquirer_id";
    public static final String DISCOVER_MERCHANT_NUMBER = "discover_merchant_number";
    public static final String DBA_NAME= "dba_name";
    public static final String DBA_ADDRESS_1 = "dba_address_1";
    public static final String DBA_ADDRESS_2 = "dba_address_2";
    public static final String DBA_CITY = "dba_city";
    public static final String DBA_STATE = "dba_state";
    public static final String DBA_COUNTRY = "dba_country";
    public static final String DBA_ZIP = "dba_zip";
    public static final String DBA_PHONE = "dba_phone";
    public static final String PRINCIPAL_FIRST_NAME = "principal_first_name";
    public static final String PRINCIPAL_LAST_NAME = "principal_last_name";
    public static final String PRINCIPAL_TITLE = "principal_title";
    public static final String PRINCIPAL_ADDRESS_1 = "principal_address_1";
    public static final String PRINCIPAL_ADDRESS_2 = "principal_address_2";
    public static final String PRINCIPAL_CITY = "principal_city";
    public static final String PRINCIPAL_STATE = "principal_state";
    public static final String PRINCIPAL_COUNTRY = "principal_country";
    public static final String PRINCIPAL_SSN = "principal_ssn";
    public static final String PRINCIPAL_ZIP = "principal_zip";
    public static final String TAX_ID_NUMBER = "tax_id_number";
    public static final String LEGAL_NAME = "legal_name";
    public static final String BUSINESS_ADDRESS_1 = "business_address_1";
    public static final String BUSINESS_ADDRESS_2 = "business_address_2";
    public static final String BUSINESS_CITY = "business_city";
    public static final String BUSINESS_STATE = "business_state";
    public static final String BUSINESS_ZIP = "business_zip";
    public static final String BUSINESS_COUNTRY = "business_country";
    public static final String MERCHANT_CATEGORY_CODE = "mcc";
    public static final String BUSINESS_TYPE_DESC = "business_type_desc";
    public static final String MERCHANT_STATUS_DATE = "merchant_status_date";
    public static final String MERCHANT_STATUS = "merchant_status";
    public static final String MERCHANT_WEBSITE_ADDRESS = "merchant_website_address";
    public static final String MERCHANT_DBA_EMAIL_ADDRESS = "merchant_dba_email_address";
    public static final String POS_DEVICE_INDICATOR = "pos_device_download_indicator";
    public static final String RECORD_NUMBER = "record_number";

    // DT2
    public static final String SELLER_START_DATE = "seller_start_date";
    public static final String SELLER_TERMINATION_DATE = "seller_termination_date";
    public static final String SELLER_STATUS = "seller_status";
    public static final String SALES_REP_ID = "sales_rep_id";

    
    private FlatFileDefConstants(){
        
    }

}
