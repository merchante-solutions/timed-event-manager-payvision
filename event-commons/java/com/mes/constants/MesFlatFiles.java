/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesFlatFiles.java $

  Last Modified By   : $Author: dsmith $
  Last Modified Date : $Date: 2018-03-29 17:11:33 -0700 (Thu, 29 Mar 2018) $
  Version            : $Revision: 23915 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001-2018 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesFlatFiles {
    public static final int DEF_TYPE_BANKSERV_HEADER = 1;
    public static final int DEF_TYPE_BANKSERV_DETAIL = 2;
    public static final int DEF_TYPE_BANKSERV_TRAILER = 3;
    public static final int DEF_TYPE_BM_HEADER = 4;
    public static final int DEF_TYPE_BM_CHARGE_REC_DT = 5;
    public static final int DEF_TYPE_BM_FREE_TEXT_DT = 6;
    public static final int DEF_TYPE_SABRE_PROFILE_HDR = 7;
    public static final int DEF_TYPE_SABRE_PROFILE_DT = 8;
    public static final int DEF_TYPE_SABRE_PROFILE_TRL = 9;
    public static final int DEF_TYPE_TSYS_TRANS_HDR = 10;
    public static final int DEF_TYPE_TSYS_BATCH_HDR = 11;
    public static final int DEF_TYPE_TSYS_TRANS_TRL = 12;
    public static final int DEF_TYPE_TSYS_BATCH_TRL = 13;
    public static final int DEF_TYPE_NTCR3_COMMON1 = 14;
    public static final int DEF_TYPE_NTCR3_COMMON2 = 15;
    public static final int DEF_TYPE_NTCR3_VISA = 16;
    public static final int DEF_TYPE_NTCR3_MCEUR = 17;
    public static final int DEF_TYPE_NTCR3_EXT3 = 18;
    public static final int DEF_TYPE_MRCH3 = 19;
    public static final int DEF_TYPE_SABRE_PCC = 20;
    public static final int DEF_TYPE_DISCOVER_HDR = 21;
    public static final int DEF_TYPE_DISCOVER_FTR = 22;
    public static final int DEF_TYPE_DISCOVER_ENTITLEMENT_DT = 23;
    public static final int DEF_TYPE_MAS_DETAIL = 24;
    public static final int DEF_TYPE_MAS_DETAIL_V117 = 124;
    public static final int DEF_TYPE_MAS_DETAIL_BRG = 1000;
    public static final int DEF_TYPE_MAS_TRAILER = 25;
    public static final int DEF_TYPE_VMS_REQ_HDR = 26;
    public static final int DEF_TYPE_VMS_REQ_DETAIL = 27;
    public static final int DEF_TYPE_VMS_REQ_CONFIG = 28;
    public static final int DEF_TYPE_GCF_SUPPLY_ORDER = 29;
    public static final int DEF_TYPE_BM_DEL_CHARGE_REC = 30;

    // Draft 256 records
    public static final int DEF_TYPE_D256_MERCH_HDR = 31;
    public static final int DEF_TYPE_D256_FINANCIAL_REC = 32;
    public static final int DEF_TYPE_D256_PT1_EXT_REC = 33;
    public static final int DEF_TYPE_D256_PT2_EXT_REC = 34;
    public static final int DEF_TYPE_D256_AUTO_RENT_EXT_REC = 35;
    public static final int DEF_TYPE_D256_AUTO_RENT_EXT_REC_OPT = 36;
    public static final int DEF_TYPE_D256_DIRECT_MARKET_EXT_REC = 37;
    public static final int DEF_TYPE_D256_FLEET1_EXT_REC = 38;
    public static final int DEF_TYPE_D256_FLEET2_EXT_REC = 39;
    public static final int DEF_TYPE_D256_GENERAL_EXT_REC = 40;
    public static final int DEF_TYPE_D256_HOTEL_EXT_REC = 41;
    public static final int DEF_TYPE_D256_HOTEL_EXT_REC_OPT = 42;
    public static final int DEF_TYPE_D256_MERCH_DESC_EXT_REC = 43;
    public static final int DEF_TYPE_D256_PURCH_CARD1_EXT_REC = 44;
    public static final int DEF_TYPE_D256_PURCH_CARD2_EXT_REC = 45;
    public static final int DEF_TYPE_D256_REST_EXT_REC = 46;
    public static final int DEF_TYPE_D256_RETAIL_EXT_REC = 47;
    public static final int DEF_TYPE_D256_SHIP_SERV1_EXT_REC = 48;
    public static final int DEF_TYPE_D256_SHIP_SERV2_EXT_REC = 49;
    public static final int DEF_TYPE_D256_TEMP_HELP1_EXT_REC = 50;
    public static final int DEF_TYPE_D256_TEMP_HELP2_EXT_REC = 51;

    // UATP (ATCAN) settlement records
    public static final int DEF_TYPE_ATCAN_FILE_HEADER = 52;
    public static final int DEF_TYPE_ATCAN_INVOICE_HEADER = 53;
    public static final int DEF_TYPE_ATCAN_BATCH_HEADER = 54;
    public static final int DEF_TYPE_ATCAN_DETAIL_RECORD = 55;
    public static final int DEF_TYPE_ATCAN_DETAIL_EXT_REC = 56;
    public static final int DEF_TYPE_ATCAN_BATCH_TRAILER = 57;
    public static final int DEF_TYPE_ATCAN_INVOICE_TRAILER = 58;
    public static final int DEF_TYPE_ATCAN_FILE_TRAILER = 59;

    // TSYS Auth Billing Summary Data
    public static final int DEF_TYPE_TSYS_AUTH_BILLING = 60;

    // ESA Setup format
    public static final int DEF_TYPE_AMEX_SETUP = 90;

    // MMS record formats
    public static final int DEF_TYPE_MMS_BASE = 100;
    public static final int DEF_TYPE_MMS_VT1TP08 = 101;
    public static final int DEF_TYPE_MMS_RR = 102;
    public static final int DEF_TYPE_MMS_52C8072 = 103;
    public static final int DEF_TYPE_MMS_VT46DA4 = 104;
    public static final int DEF_TYPE_MMS_DRR = 105;
    public static final int DEF_TYPE_MMS_VT1TPA7 = 106;
    public static final int DEF_TYPE_MMS_VXP0P16 = 107;
    public static final int DEF_TYPE_MMS_VSTDB01 = 108;
    public static final int DEF_TYPE_MMS_VT1H385 = 109;
    public static final int DEF_TYPE_MMS_VT1TPH6 = 110;
    public static final int DEF_TYPE_MMS_HTL = 111;
    public static final int DEF_TYPE_MMS_DHTL = 112;
    public static final int DEF_TYPE_MMS_FD = 113;
    public static final int DEF_TYPE_MMS_VT13DA8 = 114;
    public static final int DEF_TYPE_MMS_TLT004 = 115;
    public static final int DEF_TYPE_MMS_STAGE_V1_1_3 = 116;
    public static final int DEF_TYPE_MMS_STAGE_V1_1_4 = 117;

    // FraudNet
    public static final int DEF_TYPE_FRAUD_NET_REQUEST_FILE = 120;

    // NACHA file records
    public static final int DEF_TYPE_NACHA_FILE_HEADER = 150;
    public static final int DEF_TYPE_NACHA_BATCH_HEADER = 151;
    public static final int DEF_TYPE_NACHA_CCD_DETAIL = 152;
    public static final int DEF_TYPE_NACHA_CCD_ADDENDUM = 153;
    public static final int DEF_TYPE_NACHA_BATCH_TRAILER = 154;
    public static final int DEF_TYPE_NACHA_FILE_TRAILER = 155;

    public static final int DEF_TYPE_WELLS_ACH_SECURITY_RECORD = 156;

    // Discover Settlement Records (FILE PREP 4)
    public static final int DEF_TYPE_DISCOVER_FILE_HEADER_P4 = 160;
    public static final int DEF_TYPE_DISCOVER_GROUP_HEADER_P4 = 161;
    public static final int DEF_TYPE_DISCOVER_BATCH_HEADER_P4 = 162;
    public static final int DEF_TYPE_DISCOVER_DETAIL_P4 = 163;
    public static final int DEF_TYPE_DISCOVER_DETAIL_EXT_P4 = 164;
    public static final int DEF_TYPE_DISCOVER_BATCH_TRAILER_P4 = 165;
    public static final int DEF_TYPE_DISCOVER_GROUP_TRAILER_P4 = 166;
    public static final int DEF_TYPE_DISCOVER_FILE_TRAILER_P4 = 167;

    // Discover Settlement Records (FILE PREP 5)
    public static final int DEF_TYPE_DISCOVER_FILE_HEADER_P5 = 170;
    public static final int DEF_TYPE_DISCOVER_BATCH_HEADER_P5 = 171;
    public static final int DEF_TYPE_DISCOVER_DETAIL_P5 = 172;
    public static final int DEF_TYPE_DISCOVER_DETAIL_EXT_P5 = 173;
    public static final int DEF_TYPE_DISCOVER_BATCH_TRAILER_P5 = 174;
    public static final int DEF_TYPE_DISCOVER_FILE_TRAILER_P5 = 175;

    // Draft 256 V6.3 (Feb 2008) Modified Fields
    public static final int DEF_TYPE_D256_V63_GEN_EXT_REC = 180;
    public static final int DEF_TYPE_D256_V63_VISA_PURC1_REC = 181;
    public static final int DEF_TYPE_D256_V63_MC_PURC1_REC = 182;
    public static final int DEF_TYPE_D256_V63_VISA_PURC2_REC = 183;
    public static final int DEF_TYPE_D256_V63_MC_PURC2_REC = 184;
    public static final int DEF_TYPE_D256_V63_VISA_SHIP1_REC = 185;
    public static final int DEF_TYPE_D256_V63_VISA_SHIP2_REC = 186;
    public static final int DEF_TYPE_D256_V63_MC_SHIP1_REC = 187;
    public static final int DEF_TYPE_D256_V63_MC_SHIP2_REC = 188;

    // Discover Settlement Records Release 10.1 - April 2010
    public static final int DEF_TYPE_DISCOVER_FILE_HEADER_V101 = 212;
    public static final int DEF_TYPE_DISCOVER_BATCH_HEADER_V101 = 213;
    public static final int DEF_TYPE_DISCOVER_DETAIL_V101 = 214;
    public static final int DEF_TYPE_DISCOVER_DETAIL_EXT_V101 = 215;
    public static final int DEF_TYPE_DISCOVER_BATCH_TRAILER_V101 = 216;
    public static final int DEF_TYPE_DISCOVER_FILE_TRAILER_V101 = 217;
    public static final int DEF_TYPE_DISCOVER_DETAIL_SDR19_V191 = 207;

    public static final int DEF_TYPE_DISCOVER_CB_HDR = 218;
    public static final int DEF_TYPE_DISCOVER_CB_DTL_INCOMING = 219;
    public static final int DEF_TYPE_DISCOVER_CB_DTL_OUTGOING = 220;
    public static final int DEF_TYPE_DISCOVER_CB_TRL = 221;

    public static final int DEF_TYPE_DISCOVER_IMG_INDEX_HDR = 222;
    public static final int DEF_TYPE_DISCOVER_IMG_INDEX_DTL = 223;
    public static final int DEF_TYPE_DISCOVER_IMG_INDEX_TRL = 224;

    public static final int DEF_TYPE_DISCOVER_DETAIL_ICC1_V101 = 225; // ICC_EMV
    public static final int DEF_TYPE_DISCOVER_DETAIL_ICC2_V101 = 226; // ICC_EMV

    // Discover Merchant Profile records Release 10.1
    public static final int DEF_TYPE_DISCOVER_PROFILE_HEADER_03092 = 230;
    public static final int DEF_TYPE_DISCOVER_PROFILE_DETAIL_03092 = 231;
    public static final int DEF_TYPE_DISCOVER_PROFILE_TRAILER_03092 = 232;

    public static final int DEF_TYPE_DISCOVER_PROFILE_HEADER_03131 = 2000;
    public static final int DEF_TYPE_DISCOVER_PROFILE_DETAIL_03131 = 2001;
    public static final int DEF_TYPE_DISCOVER_PROFILE_TRAILER_03131 = 2002;

    public static final int DEF_TYPE_DISCOVER_PROFILE_HEADER_03152 = 2003;
    public static final int DEF_TYPE_DISCOVER_PROFILE_DETAIL_03152 = 2004;
    public static final int DEF_TYPE_DISCOVER_PROFILE_TRAILER_03152 = 2005;

    // Discover reconciliation files
    public static final int DEF_TYPE_DS_RECON_SETTLE_ACTIVITY = 235;
    public static final int DEF_TYPE_DS_RECON_BANK_ACTIVITY = 236;
    public static final int DEF_TYPE_DS_RECON_FEE_ACTIVITY = 237;
    public static final int DEF_TYPE_DS_RECON_TRAN_REJECT = 238;
    public static final int DEF_TYPE_DS_RECON_BATCH_REJECT = 239;
    public static final int DEF_TYPE_DS_RECON_FILE_REJECT = 240;

    // Extraction-related
    public static final int DEF_TYPE_VISA_ARDEF = 250;
    public static final int DEF_TYPE_MC_ARDEF = 251;
    public static final int DEF_TYPE_VISA_COUNTRY_CODE = 252;
    public static final int DEF_TYPE_DISCOVER_ARDEF = 253;
    public static final int DEF_TYPE_MC_IP0041T = 254;
    public static final int DEF_TYPE_MC_IP9154T = 255;
    public static final int DEF_TYPE_VISA_REGARDEF = 256;
    public static final int DEF_TYPE_MODBII_ARDEF = 257;

    // CAPN Amex Settlement Records V9.1
    public static final int DEF_TYPE_AMEX_CAPN_TFH_V91 = 280;
    public static final int DEF_TYPE_AMEX_CAPN_TAB_V91 = 281;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_LOC_DTL_V91 = 282;
    public static final int DEF_TYPE_AMEX_CAPN_TAD_V91 = 283;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_HOTEL_V91 = 284;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_CPS_L2_V91 = 285;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_AUTO_V91 = 286;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_AIR_V91 = 287;
    public static final int DEF_TYPE_AMEX_CAPN_TBT_V91 = 288;
    public static final int DEF_TYPE_AMEX_CAPN_TFS_V91 = 289;
    // CAPN Amex Settlement Records V9.1 continues below

    // Amex Chargebacks
    public static final int DEF_TYPE_AMEX_CBNOT_HDR = 290;
    public static final int DEF_TYPE_AMEX_CBNOT_DTL = 291;
    public static final int DEF_TYPE_AMEX_CBNOT_TRL = 292;

    // CAPN Amex Settlement Records V9.1 part 2, continued from above
    public static final int DEF_TYPE_AMEX_CAPN_TAA_ICC_EMV = 298;
    public static final int DEF_TYPE_AMEX_CAPN_TAA_TRAVEL_V91 = 299;

    // MATCH records
    public static final int DEF_TYPE_MATCH_HEADER = 301;
    public static final int DEF_TYPE_MATCH_GENERAL = 302;
    public static final int DEF_TYPE_MATCH_PRINCIPAL = 303;
    public static final int DEF_TYPE_MATCH_TRAILER = 304;
    public static final int DEF_TYPE_MATCH_RESP_HDR = 305;
    public static final int DEF_TYPE_MATCH_RESP_GEN_N = 306;
    public static final int DEF_TYPE_MATCH_RESP_PRIN_N = 307;
    public static final int DEF_TYPE_MATCH_RESP_GEN_E = 308;
    public static final int DEF_TYPE_MATCH_RESP_PRIN_E = 309;
    public static final int DEF_TYPE_MATCH_RESP_GEN_PR = 310;
    public static final int DEF_TYPE_MATCH_RESP_PRIN_PR = 311;
    public static final int DEF_TYPE_MATCH_RESP_GEN_MH = 312;
    public static final int DEF_TYPE_MATCH_RESP_PRIN_MH = 313;
    public static final int DEF_TYPE_MATCH_RESP_GEN_QY = 314;
    public static final int DEF_TYPE_MATCH_RESP_PRIN_QY = 315;

    public static final int DEF_TYPE_PARTNER_DDF_HDR = 350;
    public static final int DEF_TYPE_PARTNER_DDF_DTL = 351;
    public static final int DEF_TYPE_PARTNER_ATD_HDR = 352;
    public static final int DEF_TYPE_PARTNER_ATD_DTL = 353;
    public static final int DEF_TYPE_PARTNER_CB_HDR = 354;
    public static final int DEF_TYPE_PARTNER_CB_DTL = 355;

    // Visa Base II
    public static final int DEF_TYPE_VISA_TC05_TCR0 = 400;
    public static final int DEF_TYPE_VISA_TC05_TCR1 = 401;
    public static final int DEF_TYPE_VISA_TC05_TCR2 = 402;
    public static final int DEF_TYPE_VISA_TC05_TCR3_LODGING = 403;
    public static final int DEF_TYPE_VISA_TC05_TCR3_CAR_RENTAL = 404;
    public static final int DEF_TYPE_VISA_TC05_TCR5 = 405;
    public static final int DEF_TYPE_VISA_TC05_TCR6 = 406;
    public static final int DEF_TYPE_VISA_TC05_TCR7 = 407;
    public static final int DEF_TYPE_VISA_TC05_TCR4 = 408;
    public static final int DEF_TYPE_VISA_TC05_TCR4_SD = 409;
    public static final int DEF_TYPE_VISA_TC10_TCR0 = 410;
    public static final int DEF_TYPE_VISA_TC91_TCR0 = 411;
    public static final int DEF_TYPE_VISA_TC92_TCR0 = 412;
    public static final int DEF_TYPE_VISA_TC10_TCR2 = 413;
    public static final int DEF_TYPE_VISA_TC10_TCR4 = 414;

    public static final int DEF_TYPE_VISA_TC05_TCR7_DTL = 417;

    public static final int DEF_TYPE_VISA_TC50_TCR0_SUMMARY = 420;
    public static final int DEF_TYPE_VISA_TC50_TCR0_LINE_ITEM = 421;
    public static final int DEF_TYPE_VISA_TC50_TCR0_LODGING = 422;
    public static final int DEF_TYPE_VISA_TC50_TCR0_CAR_RENTAL = 423;
    public static final int DEF_TYPE_VISA_REJECT_TCR9 = 424;

    public static final int DEF_TYPE_VISA_TC52_TCR0 = 425;
    public static final int DEF_TYPE_VISA_TC52_TCR1 = 426;
    public static final int DEF_TYPE_VISA_TC52_TCR4 = 427;
    public static final int DEF_TYPE_VISA_TC01_TCR9 = 428;

    public static final int DEF_TYPE_VISA_TC50_TCR0_MPS_IN = 429;
    public static final int DEF_TYPE_VISA_TC50_TCR0_MPS_OUT = 430;
    public static final int DEF_TYPE_VISA_TC50_TCR1_MPS_OUT = 431;

    public static final int DEF_TYPE_VISA_TC56_TCR0 = 440;
    public static final int DEF_TYPE_VISA_TC56_TCR1 = 441;

    public static final int DEF_TYPE_VISA_TC46_TCR0_VSS115 = 450;

    // Visa monthly billing files
    public static final int DEF_TYPE_VISA_AUTH_MISUSE_SUMMARY = 501;
    public static final int DEF_TYPE_VISA_AUTH_MISUSE_DETAIL = 502;

    public static final int DEF_TYPE_WF_THIN_FILE = 503;

    public static final int DEF_TYPE_VISA_TIF_DETAIL = 504;

    public static final int DEF_TYPE_MC_T057_DETAIL = 505;

    public static final int DEF_TYPE_MC_TIF_DETAIL = 506;

    // Amex Retrievals
    public static final int DEF_TYPE_AMEX_INQ02_HDR = 550;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_AIRDS = 551;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_AIRLT = 552;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_AIRRT = 553;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_AIRTB = 554;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_AREXS = 555;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_CARRD = 556;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_GSDIS = 557;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_NAXMG = 558;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_NAXMR = 559;
    public static final int DEF_TYPE_AMEX_INQ02_DTL_SEDIS = 560;
    public static final int DEF_TYPE_AMEX_INQ02_TRL = 561;

    // Amex Recon Data
    public static final int DEF_TYPE_AMEX_EPRAW_HDR = 570;
    public static final int DEF_TYPE_AMEX_EPRAW_SUMMARY = 571;
    public static final int DEF_TYPE_AMEX_EPRAW_DTL_SOC = 572;
    public static final int DEF_TYPE_AMEX_EPRAW_DTL_CB = 573;
    public static final int DEF_TYPE_AMEX_EPRAW_DTL_ADJ = 574;
    public static final int DEF_TYPE_AMEX_EPRAW_DTL_OTHER = 575;
    public static final int DEF_TYPE_AMEX_EPRAW_TRL = 576;

    // Amex Recon Data - Optblue
    public static final int DEF_TYPE_AMEX_OPTBLUE_DTL_SOC = 577;
    public static final int DEF_TYPE_AMEX_OPTBLUE_PRICING_SOC = 578;
    public static final int DEF_TYPE_AMEX_OPTBLUE_DTL_ROC = 579;
    public static final int DEF_TYPE_AMEX_OPTBLUE_PRICING_ROC = 580;
    public static final int DEF_TYPE_AMEX_OPTBLUE_DTL_CB = 581;
    public static final int DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ = 582;
    public static final int DEF_TYPE_AMEX_OPTBLUE_DTL_OTHER = 583;

    // encore batch processing
    public static final int DEF_TYPE_EBP_HDR = 600;
    public static final int DEF_TYPE_EBP_DTL = 601;
    public static final int DEF_TYPE_EBP_DDBA = 602;
    public static final int DEF_TYPE_EBP_EXT_AM_1 = 603;
    public static final int DEF_TYPE_EBP_EXT_AM_2 = 604;
    public static final int DEF_TYPE_EBP_EXT_AM_4 = 605;
    public static final int DEF_TYPE_EBP_ADDR = 606;
    public static final int DEF_TYPE_EBP_RAW = 607;

    // Vital CDF
    public static final int DEF_TYPE_CDF_MERCH_HDR = 701;
    public static final int DEF_TYPE_CDF_FINANCIAL_REC = 702;
    public static final int DEF_TYPE_CDF_PT1_REC = 703;
    public static final int DEF_TYPE_CDF_PT2_REC = 704;
    public static final int DEF_TYPE_CDF_AUTO_RENT_REC = 705;
    public static final int DEF_TYPE_CDF_AUTO_RENT_REC_OPT = 706;
    public static final int DEF_TYPE_CDF_DIRECT_MARKET_REC = 707;
    public static final int DEF_TYPE_CDF_FLEET1_REC = 708;
    public static final int DEF_TYPE_CDF_FLEET2_REC = 709;
    public static final int DEF_TYPE_CDF_VISA_HOTEL_REC = 710;
    public static final int DEF_TYPE_CDF_HOTEL_REC_OPT = 711;
    public static final int DEF_TYPE_CDF_MERCH_DESC_REC = 712;
    public static final int DEF_TYPE_CDF_REST_REC = 713;
    public static final int DEF_TYPE_CDF_RETAIL_REC = 714;
    public static final int DEF_TYPE_CDF_TEMP_HELP1_REC = 715;
    public static final int DEF_TYPE_CDF_TEMP_HELP2_REC = 716;
    public static final int DEF_TYPE_CDF_GEN_REC = 717;
    public static final int DEF_TYPE_CDF_VISA_PURC1_REC = 718;
    public static final int DEF_TYPE_CDF_MC_PURC1_REC = 719;
    public static final int DEF_TYPE_CDF_VISA_PURC2_REC = 720;
    public static final int DEF_TYPE_CDF_MC_PURC2_REC = 721;
    public static final int DEF_TYPE_CDF_VISA_SHIP1_REC = 722;
    public static final int DEF_TYPE_CDF_VISA_SHIP2_REC = 723;
    public static final int DEF_TYPE_CDF_MC_SHIP1_REC = 724;
    public static final int DEF_TYPE_CDF_MC_SHIP2_REC = 725;
    public static final int DEF_TYPE_CDF_MC_HOTEL_REC = 726;
    public static final int DEF_TYPE_CDF_AMEX_HOTEL_REC = 751;
    public static final int DEF_TYPE_CDF_DIRECT_DELIVERY_REC = 752;

    // GRS (Global Retriever) file
    public static final int DEF_TYPE_GRS_HEADER = 727;
    public static final int DEF_TYPE_GRS_DETAIL = 728;
    public static final int DEF_TYPE_GRS_TRAILER = 729;

    // FIS Miscellaneous account file
    public static final int DEF_TYPE_FIS_DETAIL = 800;

    public static final int DEF_TYPE_MIF_CIELO_PRODUCTS = 900;
    public static final int DEF_TYPE_MIF_CIELO = 901;
    public static final int DEF_TYPE_MIF_CIELO_TEST = 902;
    public static final int DEF_TYPE_DDF_CIELO_VS = 903;
    public static final int DEF_TYPE_DDF_CIELO_VS_ELECTRON_PD = 904;
    public static final int DEF_TYPE_DDF_CIELO_VS_INSTALLMENT = 905;
    public static final int DEF_TYPE_DDF_CIELO_VS_REFUND = 906;
    public static final int DEF_TYPE_DDF_CIELO_VS_CASHBACK = 907;
    public static final int DEF_TYPE_DDF_CIELO_DT_ELECTRON_AGRO = 908;
    public static final int DEF_TYPE_DDF_CIELO_VS_COMPL_BASE = 909;
    public static final int DEF_TYPE_DDF_CIELO_MC = 910;
    public static final int DEF_TYPE_DDF_CIELO_MC_INSTALLMENT = 911;
    public static final int DEF_TYPE_DDF_CIELO_MC_REFUND = 912;
    public static final int DEF_TYPE_DDF_CIELO_MC_COMPL_BASE = 913;
    public static final int DEF_TYPE_DDF_CIELO_MC_COMPL_BASE_BIN = 914;

    // complementary data for base I transctions
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_001 = 920;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_002 = 921;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_003 = 922;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_004 = 923;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_005 = 924;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_006 = 925;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_007 = 926;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008A = 927;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_009 = 928;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_010 = 929;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_011 = 930;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_012 = 931;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_013 = 932;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_014 = 933;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_015 = 934;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_016 = 935;
    public static final int DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008B = 936;

    public static final int DEF_TYPE_MIF_CIELO_HIERARCHY = 950;
    public static final int DEF_TYPE_MIF_CIELO_BANK_INFO = 951;
    public static final int DEF_TYPE_CIELO_HOLIDAYS = 952;

    public static final int DEF_TYPE_AMEX_PSP_HEADER = 1010;
    public static final int DEF_TYPE_AMEX_PSP_DETAIL = 1011;
    public static final int DEF_TYPE_AMEX_PSP_TRAILER = 1012;

    public static final int DEF_TYPE_AMEX_SPONSOR_MERCHANT_HEADER = 1013;
    public static final int DEF_TYPE_AMEX_SPONSOR_MERCHANT_DETAIL = 1014;
    public static final int DEF_TYPE_AMEX_SPONSOR_MERCHANT_TRAILER = 1015;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL = 1017;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DT2 = 1023;


    public static final int DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_HEADER = 1016;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_DETAIL = 1018;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_TRAILER = 1020;
    public static final int DEF_TYPE_AMEX_OPTBLUE_ARDEF = 1019;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SELLER_CANCELLATIONS_DETAIL = 1021;
    public static final int DEF_TYPE_AMEX_OPTBLUE_SELLER_DISPUTES_DETAIL = 1022;

    // CAPN Amex settlement optblue TFH
    public static final int DEF_TYPE_AMEX_CAPN_TFH_OPTBLUE = 1055;

    // Flat file Defs for BOB
    public static final int DEF_TYPE_BOB_BRADESCO_FUTURE_PAYMENTS_HEADER = 960;
    public static final int DEF_TYPE_BOB_BRADESCO_FUTURE_PAYMENTS_DETAIL = 961;
    public static final int DEF_TYPE_BOB_BRADESCO_FUTURE_PAYMENTS_TRAILER = 962;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_FUTURE_PAYMENTS_HEADER = 963;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_FUTURE_PAYMENTS_DETAIL = 964;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_FUTURE_PAYMENTS_TRAILER = 965;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_FUTURE_PAYMENTS_HEADER = 966;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_FUTURE_PAYMENTS_DETAIL = 967;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_FUTURE_PAYMENTS_TRAILER = 968;
    public static final int DEF_TYPE_BOB_BRADESCO_AND_BB_INSTALLEMENTS_HEADER = 969;
    public static final int DEF_TYPE_BOB_BRADESCO_AND_BB_INSTALLEMENTS_DETAIL = 970;
    public static final int DEF_TYPE_BOB_BRADESCO_AND_BB_INSTALLEMENTS_TRAILER = 971;
    public static final int DEF_TYPE_BOB_ALFA_AND_PANAMERICANO_INSTALLEMENTS_HEADER = 972;
    public static final int DEF_TYPE_BOB_ALFA_AND_PANAMERICANO_INSTALLEMENTS_DETAIL = 973;
    public static final int DEF_TYPE_BOB_ALFA_AND_PANAMERICANO_INSTALLEMENTS_TRAILER = 974;
    public static final int DEF_TYPE_BOB_SANTANDER_INSTALLEMENTS_TRAILER = 977;
    public static final int DEF_TYPE_BOB_SANTANDER_INSTALLEMENTS_DETAIL = 976;
    public static final int DEF_TYPE_BOB_SANTANDER_INSTALLEMENTS_HEADER = 975;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_INSTALLEMENTS_TRAILER = 980;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_INSTALLEMENTS_DETAIL = 979;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_INSTALLEMENTS_HEADER = 978;
    public static final int DEF_TYPE_BOB_ALL_BANKS_REGISTRATION_TRAILER = 983;
    public static final int DEF_TYPE_BOB_ALL_BANKS_REGISTRATION_DETAIL = 982;
    public static final int DEF_TYPE_BOB_ALL_BANKS_REGISTRATION_HEADER = 981;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_CREDIT_PAYMENTS_TRAILER = 986;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_CREDIT_PAYMENTS_DETAIL = 985;
    public static final int DEF_TYPE_BOB_BANK_OF_BRASIL_CREDIT_PAYMENTS_HEADER = 984;
    public static final int DEF_TYPE_BOB_OTHER_BANK_CREDIT_PAYMENTS_HEADER = 987;
    public static final int DEF_TYPE_BOB_OTHER_BANK_CREDIT_PAYMENTS_SHIPMENT_DETAIL = 988;
    public static final int DEF_TYPE_BOB_OTHER_BANK_CREDIT_PAYMENTS_TRAILER = 989;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_SHIPMENTS_TRAILER = 992;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_SHIPMENTS_DETAIL = 991;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_SHIPMENTS_HEADER = 990;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_RETURNS_TRAILER = 995;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_RETURNS_DETAIL = 994;
    public static final int DEF_TYPE_BOB_BRADESCO_CREDIT_PAYMENTS_TO_RETURNS_HEADER = 993;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_HEADER = 996;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_DETAIL_SHIPMENT = 997;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_DETAIL_RETURNS = 998;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_TRAILER = 999;
    public static final int DEF_TYPE_BOB_BRADESCO_DEBIT_TO_MERCHANT_TRAILER = 1003;
    public static final int DEF_TYPE_BOB_BRADESCO_DEBIT_TO_MERCHANT_DETAIL = 1002;
    public static final int DEF_TYPE_BOB_BRADESCO_DEBIT_TO_MERCHANT_HEADER = 1001;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_PAYMENT_HEADER = 1004;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_PAYMENT_DETAIL = 1005;
    public static final int DEF_TYPE_BOB_OTHER_BANKS_DEBIT_PAYMENT_TRAILER = 1006;
    public static final int DEF_TYPE_BOB_OTHER_BANK_CREDIT_PAYMENTS_RETURN_DETAIL = 1007;

    // flat file definitions for EDI resolution files (Cielo)
    public static final int DEF_TYPE_CIELO_SALES_FILE_NO_FUTURE_INSTALLMENTS_HEADER = 1101;
    public static final int DEF_TYPE_CIELO_SALES_FILE_NO_FUTURE_INSTALLMENTS_RO_DETAIL = 1102;
    public static final int DEF_TYPE_CIELO_SALES_FILE_NO_FUTURE_INSTALLMENTS_CV_DETAIL = 1103;
    public static final int DEF_TYPE_CIELO_SALES_FILE_NO_FUTURE_INSTALLMENTS_TRAILER = 1104;

    public static final int DEF_TYPE_CIELO_SALES_FILE_WITH_FUTURE_INSTALLMENTS_HEADER = 1105;
    public static final int DEF_TYPE_CIELO_SALES_FILE_WITH_FUTURE_INSTALLMENTS_RO_DETAIL_FIRST_INSTALLMENT = 1106;
    public static final int DEF_TYPE_CIELO_SALES_FILE_WITH_FUTURE_INSTALLMENTS_CV_DETAIL = 1107;
    public static final int DEF_TYPE_CIELO_SALES_FILE_WITH_FUTURE_INSTALLMENTS_RO_DETAIL_SECOND_INSTALLMENT = 1108;
    public static final int DEF_TYPE_CIELO_SALES_FILE_WITH_FUTURE_INSTALLMENTS_TRAILER = 1109;

    public static final int DEF_TYPE_CIELO_PAYMENT_FILE_HEADER = 1110;
    public static final int DEF_TYPE_CIELO_PAYMENT_FILE_RO_DETAIL = 1111;
    public static final int DEF_TYPE_CIELO_PAYMENT_FILE_CV_DETAIL = 1112;
    public static final int DEF_TYPE_CIELO_PAYMENT_FILE_TRAILER = 1113;

    public static final int DEF_TYPE_CIELO_PREPAYMENT_OF_RECEIVABLES_FILE_HEADER = 1114;
    public static final int DEF_TYPE_CIELO_PREPAYMENT_OF_RECEIVABLES_FILE_REGTYPE_5_RO = 1115;
    public static final int DEF_TYPE_CIELO_PREPAYMENT_OF_RECEIVABLES_FILE_ANTICIPATED_DETAIL_REGISTER = 1116;
    public static final int DEF_TYPE_CIELO_PREPAYMENT_OF_RECEIVABLES_FILE_TRAILER = 1117;

    public static final int DEF_TYPE_CIELO_ASSIGNMENT_OF_RECEIVABLES_FILE_HEADER = 1118;
    public static final int DEF_TYPE_CIELO_ASSIGNMENT_OF_RECEIVABLES_FILE_REGTYPE_5_RO = 1119;
    public static final int DEF_TYPE_CIELO_ASSIGNMENT_OF_RECEIVABLES_FILE_REGTYPE_6_RO = 1120;
    public static final int DEF_TYPE_CIELO_ASSIGNMENT_OF_RECEIVABLES_FILE_TRAILER = 1121;

    public static final int DEF_TYPE_CIELO_PENDING_INSTALLMENT_FILE_HEADER = 1122;
    public static final int DEF_TYPE_CIELO_PENDING_INSTALLMENT_FILE_REGTYPE_1_RO = 1123;
    public static final int DEF_TYPE_CIELO_PENDING_INSTALLMENT_FILE_REGTYPE_2_CV = 1124;
    public static final int DEF_TYPE_CIELO_PENDING_INSTALLMENT_FILE_TRAILER = 1125;

    public static final int DEF_TYPE_CIELO_OPEN_BALANCE_FILE_HEADER = 1126;
    public static final int DEF_TYPE_CIELO_OPEN_BALANCE_FILE_REGTYPE_3_RO = 1127;
    public static final int DEF_TYPE_CIELO_OPEN_BALANCE_FILE_REGTYPE_4_BRAND_OF_OPEN_BALANCE = 1128;
    public static final int DEF_TYPE_CIELO_OPEN_BALANCE_FILE_TRAILER = 1129;

    // Modified Base II (Cielo)
    public static final int DEF_TYPE_MODBII_TC05_TCR0 = 1200;
    public static final int DEF_TYPE_MODBII_TC05_TCR1 = 1201;
    public static final int DEF_TYPE_MODBII_TC05_TCR2 = 1202;
    public static final int DEF_TYPE_MODBII_TC05_TCR5 = 1205;
    public static final int DEF_TYPE_MODBII_TC05_TCR6 = 1206;
    public static final int DEF_TYPE_MODBII_TC05_TCR7 = 1207;
    public static final int DEF_TYPE_MODBII_TC10_TCR0 = 1210;
    public static final int DEF_TYPE_MODBII_TC10_TCR2 = 1212;

    public static final int DEF_TYPE_MODBII_TC05_TCR7_DTL = 1217;

    public static final int DEF_TYPE_MODBII_TC20_TCR0 = 1220;
    public static final int DEF_TYPE_MODBII_TC20_TCR2 = 1222;

    public static final int DEF_TYPE_MODBII_REJECT_TCR9 = 1224;

    public static final int DEF_TYPE_MODBII_TC52_TCR0 = 1225;
    public static final int DEF_TYPE_MODBII_TC52_TCR1 = 1226;

    public static final int DEF_TYPE_MODBII_TC40_TCR0 = 1240;
    public static final int DEF_TYPE_MODBII_TC40_TCR2 = 1242;

    public static final int DEF_TYPE_MODBII_FILE_HEADER = 1290;
    public static final int DEF_TYPE_MODBII_FILE_TRAILER = 1291;
    public static final int DEF_TYPE_MODBII_FILE_ACK = 1292;

    // Amex OptBlue
    // CBNSP
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBNSP_HDR = 1030;
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBNSP_DTL = 1031;
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBNSP_TRL = 1032;

    // Amex Inquiry Header Record (INQ02 V1.3)
    // INQ02
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_HDR = 1035;

    // Amex Inquiry AIRDS Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRDS = 1036;

    // Amex Inquiry AIRLT Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRLT = 1037;

    // Amex Inquiry AIRRT Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRRT = 1038;

    // Amex Inquiry AIRTB Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRTB = 1039;

    // Amex Inquiry AREXS Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_AREXS = 1040;

    // Amex Inquiry CARRD Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_CARRD = 1041;

    // Amex Inquiry GSDIS Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_GSDIS = 1042;

    // Amex Inquiry NAXMG Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_NAXMG = 1043;

    // Amex Inquiry NAXMR Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_NAXMR = 1044;

    // Amex Inquiry SEDIS Detail Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_SEDIS = 1045;

    // Amex Inquiry Trailer Record (INQ02 V1.3)
    public static final int DEF_TYPE_AMEX_OPTBLUE_INQ02_TRL = 1046;

    // CBDIS
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBDIS_HDR = 1047;
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBDIS_DTL = 1048;
    public static final int DEF_TYPE_AMEX_OPTBLUE_CBDIS_TRL = 1049;

    // RESPN
    public static final int DEF_TYPE_AMEX_OPTBLUE_RESPN_HDR = 1050;
    public static final int DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_1 = 1051;
    public static final int DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_2 = 1052;
    public static final int DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_3 = 1053;
    public static final int DEF_TYPE_AMEX_OPTBLUE_RESPN_TRL = 1054;

    // DiscoverMap file deftypes for SDR 05
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_1 = 9301;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_2 = 9302;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_3 = 9303;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_4 = 9304;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_5 = 9305;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_6 = 9306;
    public static final int DEF_TYPE_DISCOVER_SDR05_SEQ_7 = 9307;

    // DiscoverMap file deftypes for SDR 06
    public static final int DEF_TYPE_DISCOVER_SDR06_SEQ_1 = 9311;
    public static final int DEF_TYPE_DISCOVER_SDR06_SEQ_2 = 9312;
    public static final int DEF_TYPE_DISCOVER_SDR06_SEQ_3 = 9313;
    public static final int DEF_TYPE_DISCOVER_SDR06_SEQ_4 = 9314;

    // DiscoverMap file deftypes for SDR 09
    public static final int DEF_TYPE_DISCOVER_SDR09_SEQ_1 = 9321;
    public static final int DEF_TYPE_DISCOVER_SDR09_SEQ_2 = 9322;
    public static final int DEF_TYPE_DISCOVER_SDR09_SEQ_3 = 9323;

}
