/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/payvision/PayvisionDb.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-10-01 14:49:42 -0700 (Tue, 01 Oct 2013) $
  Version            : $Revision: 21555 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.payvision;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class PayvisionDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(PayvisionDb.class);

  private void saveResponse(ApiResponse response) throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into payvision_api_responses ( " +
        "  request_id,                          " +
        "  request_ts,                          " +
        "  request_type,                        " +
        "  endpoint_addr,                       " +
        "  user_name,                           " +
        "  cr_member_id,                        " +
        "  cr_member_guid,                      " +
        "  member_id,                           " +
        "  start_ts,                            " +
        "  end_ts,                              " +
        "  result_code,                         " +
        "  result_message )                     " +
        " values (                              " +
        "  ?, ?, ?, ?, ?, ?,                    " +
        "  ?, ?, ?, ?, ?, ?  )                  ");

      ps.setLong      (1, response.getRequestId());
      ps.setTimestamp (2, response.getRequestTs());
      ps.setString    (3, response.getRequestType());
      ps.setString    (4, response.getHostAddr() + response.getEndpoint());
      ps.setString    (5, response.getUserName());
      ps.setInt       (6, response.getCrMemberId());
      ps.setString    (7, response.getCrMemberGuid());
      ps.setInt       (8, response.getMemberId());
      ps.setTimestamp (9, response.getStartTs());
      ps.setTimestamp (10,response.getEndTs());
      ps.setString    (11,""+response.getCode());
      ps.setString    (12,response.getMessage());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new Exception("Error saving response record: " + e,e);
    }
    finally
    {
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }

  public void saveChargebackRecords(ChargebackResponse response)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    ResultSet rs = null;

    try
    {
      connect(true);

      saveResponse(response);

      ps1 = con.prepareStatement(
        " insert into payvision_api_cb_records (  " +
        "  request_id,                            " +
        "  member_id,                             " +
        "  cb_id,                                 " +
        "  case_id,                               " +
        "  cb_type_id,                            " +
        "  amount,                                " +
        "  currency_code,                         " +
        "  proc_date,                             " +
        "  reason_code,                           " +
        "  card_id,                               " +
        "  card_guid,                             " +
        "  tran_date,                             " +
        "  tran_id,                               " +
        "  merch_acct_id )                        " +
        " values (                                " +
        "  ?, ?, ?, ?, ?, ?, ?,                   " +
        "  ?, ?, ?, ?, ?, ?, ?  )                 ");

      ps2 = con.prepareStatement(
        " insert into payvision_api_cb_entries (  " +
        "  request_id,                            " +
        "  member_id,                             " +
        "  cb_id,                                 " +
        "  key,                                   " +
        "  value )                                " +
        " values (                                " +
        "  ?, ?, ?, ?, ? )                        ");

      ps3 = con.prepareStatement(
        " select * from payvision_api_cb_records  " +
        " where cb_id = ?                         ");

      for (Iterator i = response.getRecords().iterator(); i.hasNext();)
      {
        ChargebackRecord record = (ChargebackRecord)i.next();

        ps3.setLong(1,record.getCbId());
        rs = ps3.executeQuery();
        if (rs.next())
        {
          log.debug("skipping duplicate cb id: " + record.getCbId());
          continue;
        }

        ps1.setLong       (1, record.getRequestId());
        ps1.setInt        (2, record.getMemberId());
        ps1.setLong       (3, record.getCbId());
        ps1.setString     (4, record.getCaseId());
        ps1.setInt        (5, record.getCbTypeId());
        ps1.setBigDecimal (6, record.getAmount());
        ps1.setInt        (7, record.getCurrencyCode());
        ps1.setTimestamp  (8, record.getProcTs());
        ps1.setString     (9, record.getReasonCode());
        ps1.setLong       (10,record.getCardId());
        ps1.setString     (11,record.getCardGuid());
        ps1.setTimestamp  (12,record.getTranTs());
        ps1.setLong       (13,record.getTranId());
        ps1.setLong       (14,record.getMerchAcctId());
        ps1.executeUpdate();
        for (Iterator j = record.getEntryItems().iterator(); j.hasNext();)
        {
          ChargebackEntryItem item = (ChargebackEntryItem)j.next();
          ps2.setLong   (1,item.getRequestId());
          ps2.setInt    (2,item.getMemberId());
          ps2.setLong   (3,item.getCbId());
          ps2.setString (4,item.getKey());
          ps2.setString (5,item.getValue());
          ps2.executeUpdate();
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error saving chargebacks: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      try { ps3.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }

  public List loadChargeBackRecords(Date fromDate, Date toDate)
  {
    try
    {
      // TODO: implement this function
      return new ArrayList();
    }
    catch (Exception e)
    {
    }
    finally
    {
    }

    return null;
  }

  public void saveDeclineRecords(DeclineResponse response)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    ResultSet rs = null;

    try
    {
      connect(true);

      saveResponse(response);

      ps1 = con.prepareStatement(
        " insert into payvision_api_decl_records (  " +
        "  request_id,                              " +
        "  member_id,                               " +
        "  proc_tran_id,                            " +
        "  proc_tran_guid,                          " +
        "  tran_type,                               " +
        "  track_mem_code,                          " +
        "  orig_amount,                             " +
        "  orig_currency_code,                      " +
        "  merch_amount,                            " +
        "  merch_currency_rate,                     " +
        "  request_date,                            " +
        "  process_date,                            " +
        "  card_id,                                 " +
        "  card_guid,                               " +
        "  card_type,                               " +
        "  merch_acct_id,                           " +
        "  country_code,                            " +
        "  bank_code,                               " +
        "  bank_msg )                               " +
        " values (                                  " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,            " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ? )              ");

      ps2 = con.prepareStatement(
        " select * from payvision_api_decl_records  " +
        " where proc_tran_id = ?                    ");

      for (Iterator i = response.getRecords().iterator(); i.hasNext();)
      {
        DeclineRecord record = (DeclineRecord)i.next();

        ps2.setLong(1,record.getProcTranId());
        rs = ps2.executeQuery();
        if (rs.next())
        {
          log.debug("skipping duplicate proc_tran_id: " 
            + record.getProcTranId());
          continue;
        }

        ps1.setLong       (1, record.getRequestId());
        ps1.setLong       (2, record.getMemberId());
        ps1.setLong       (3, record.getProcTranId());
        ps1.setString     (4, record.getProcTranGuid());
        ps1.setString     (5, record.getTranType());
        ps1.setString     (6, record.getTrackMemCode());
        ps1.setBigDecimal (7, record.getOrigAmount());
        ps1.setInt        (8, record.getOrigCurrencyCode());
        ps1.setBigDecimal (9, record.getMerchAmount());
        ps1.setBigDecimal (10,record.getMerchCurrencyRate());
        ps1.setTimestamp  (11,record.getRequestTs());
        ps1.setTimestamp  (12,record.getProcessTs());
        ps1.setLong       (13,record.getCardId());
        ps1.setString     (14,record.getCardGuid());
        ps1.setString     (15,record.getCardType());
        ps1.setLong       (16,record.getMerchAcctId());
        ps1.setInt        (17,record.getCountryCode());
        ps1.setString     (18,record.getBankCode());
        ps1.setString     (19,record.getBankMsg());
        ps1.executeUpdate();
      }
    }
    catch (Exception e)
    {
      log.error("Error saving chargebacks: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }

  public void saveReconciliationData(ReconciliationResponse response)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    ResultSet rs = null;

    try
    {
      connect(true);

      saveResponse(response);

      ps1 = con.prepareStatement(
        " insert into payvision_api_tran_summary (  " +
        "  request_id,                              " +
        "  member_id,                               " +
        "  merch_acct_id,                           " +
        "  currency_code,                           " +
        "  auth_count,                              " +
        "  auth_total,                              " +
        "  capture_count,                           " +
        "  capture_total,                           " +
        "  void_count,                              " +
        "  void_total,                              " +
        "  payment_count,                           " +
        "  payment_total,                           " +
        "  refund_count,                            " +
        "  refund_total,                            " +
        "  referral_count,                          " +
        "  referral_total )                         " +
        " values ( ?, ?, ?, ?, ?, ?, ?, ?,          " +
        "          ?, ?, ?, ?, ?, ?, ?, ? )         ");

      ps2 = con.prepareStatement(
        " insert into payvision_api_tran_detail (   " +
        "  request_id,                              " +
        "  member_id,                               " +
        "  merch_acct_id,                           " +
        "  tran_id,                                 " +
        "  tran_guid,                               " +
        "  track_mem_code,                          " +
        "  orig_amount,                             " +
        "  orig_currency_code,                      " +
        "  merch_amount,                            " +
        "  currency_rate,                           " +
        "  card_id,                                 " +
        "  card_guid,                               " +
        "  card_type_id,                            " +
        "  tran_date,                               " +
        "  country_code,                            " +
        "  batch_date )                             " +
        " values ( ?, ?, ?, ?, ?, ?, ?, ?,          " +
        "          ?, ?, ?, ?, ?, ?, ?, ? )         ");

      ps3 = con.prepareStatement(
        " select * from payvision_api_tran_detail   " +
        " where tran_id = ?                         ");

      for (Iterator i = response.getSummaries().iterator(); i.hasNext();)
      {
        ReconciliationSummary summary = (ReconciliationSummary)i.next();

        for (Iterator j = summary.getDetails().iterator(); j.hasNext();)
        {
          TransactionDetail detail = (TransactionDetail)j.next();
          ps3.setLong(1,detail.getTranId());
          rs = ps3.executeQuery();
          boolean isDup = rs.next();
          rs.close();
          if (isDup)
          {
            log.debug("skipping duplicate tran id: " + detail.getTranId());
            continue;
          }

          ps2.setLong       (1, detail.getRequestId());
          ps2.setInt        (2, detail.getMemberId());
          ps2.setLong       (3, detail.getMerchAcctId());
          ps2.setLong       (4, detail.getTranId());
          ps2.setString     (5, detail.getTranGuid());
          ps2.setString     (6, detail.getTrackMemCode());
          ps2.setBigDecimal (7, detail.getOrigAmount());
          ps2.setInt        (8, detail.getOrigCurrencyCode());
          ps2.setBigDecimal (9, detail.getMerchAmount());
          ps2.setBigDecimal (10,detail.getCurrencyRate());
          ps2.setLong       (11,detail.getCardId());
          ps2.setString     (12,detail.getCardGuid());
          ps2.setString     (13,detail.getCardTypeId());
          ps2.setTimestamp  (14,detail.getTranTs());
          ps2.setInt        (15,detail.getCountryCode());
          ps2.setString     (16,detail.getBatchDate());
          ps2.executeUpdate();
        }

        ps1.setLong       (1, response.getRequestId());
        ps1.setInt        (2, response.getMemberId());
        ps1.setLong       (3, summary.getMerchAcctId());
        ps1.setInt        (4, summary.getCurrencyCode());
        ps1.setInt        (5, summary.getAuthCount());
        ps1.setBigDecimal (6, summary.getAuthTotal());
        ps1.setInt        (7, summary.getCaptureCount());
        ps1.setBigDecimal (8, summary.getCaptureTotal());
        ps1.setInt        (9, summary.getVoidCount());
        ps1.setBigDecimal (10,summary.getVoidTotal());
        ps1.setInt        (11,summary.getPaymentCount());
        ps1.setBigDecimal (12,summary.getPaymentTotal());
        ps1.setInt        (13,summary.getRefundCount());
        ps1.setBigDecimal (14,summary.getRefundTotal());
        ps1.setInt        (15,summary.getReferralCount());
        ps1.setBigDecimal (16,summary.getReferralTotal());
        ps1.executeUpdate();
      }
    }
    catch (Exception e)
    {
      log.error("Error saving reconciliation data: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      try { ps3.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }

  public TransactionDetail loadTransactionDetailWithTrackMemCode(String trackMemCode)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from payvision_api_tran_detail where track_mem_code = ? ");
      ps.setString(1,trackMemCode);
      rs = ps.executeQuery();
      if (rs.next())
      {
        TransactionDetail detail = new TransactionDetail();
        detail.setRequestId(rs.getLong("request_id"));
        detail.setMemberId(rs.getInt("member_id"));
        detail.setMerchAcctId(rs.getInt("merch_acct_id"));
        detail.setTranId(rs.getInt("tran_id"));
        detail.setTranGuid(rs.getString("tran_guid"));
        detail.setTrackMemCode(rs.getString("track_mem_code"));
        detail.setOrigAmount(rs.getBigDecimal("orig_amount"));
        detail.setOrigCurrencyCode(rs.getInt("orig_currency_code"));
        detail.setMerchAmount(rs.getBigDecimal("merch_amount"));
        detail.setCurrencyRate(rs.getBigDecimal("currency_rate"));
        detail.setCardId(rs.getInt("card_id"));
        detail.setCardGuid(rs.getString("card_guid"));
        detail.setCardTypeId(rs.getString("card_type_id"));
        detail.setTranTs(rs.getTimestamp("tran_date"));
        detail.setCountryCode(rs.getInt("country_code"));
        detail.setBatchDate(rs.getString("batch_date"));
        detail.setFxAmountBase(rs.getBigDecimal("fx_amount_base"));
        return detail;
      }
    }
    catch (Exception e)
    {
      log.error("Error fetching tran detail: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
    return null;
  }

  public long getNewSettlementId()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " select payvision_setl_load_seq.nextval from dual ");
      rs = ps.executeQuery();
      rs.next();
      return rs.getLong(1);
    }
    catch (Exception e)
    {
      log.error("Error fetching next settlement load id: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
    return -1L;
  }

  public static Date truncateDate(Date date)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(cal.HOUR,0);
    cal.set(cal.MINUTE,0);
    cal.set(cal.SECOND,0);
    return cal.getTime();
  }

  /**
   * Fetch conversion rate in fx_rate_tables.
   */
  public BigDecimal fetchMerchantConversionRate(long merchNum, String fromCur, 
    String toCur, Date rateDate)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " select                                                    " +
        "  r.rate                                                   " +
        " from                                                      " +
        "  mes.fx_rate_tables r,                                    " +
        "  ( select                                                 " +
        "     min(expiration_date) expiration_date,                 " +
        "     merchant_currency_code,                               " +
        "     consumer_currency_code,                               " +
        "     merchant_number                                       " +
        "    from                                                   " +
        "     mes.fx_rate_tables                                    " +
        "    where                                                  " +
        "     expiration_date > ?                                   " +
        "    group by                                               " +
        "     merchant_number,                                      " +
        "     merchant_currency_code,                               " +
        "     consumer_currency_code ) sd                           " +
        " where                                                     " +
        "  r.merchant_number = ?                                    " +
        "  and r.merchant_currency_code = ?                         " +
        "  and r.consumer_currency_code = ?                         " +
        "  and r.expiration_date = sd.expiration_date               " +
        "  and r.merchant_number = sd.merchant_number               " +
        "  and r.merchant_currency_code = sd.merchant_currency_code " +
        "  and r.consumer_currency_code = sd.consumer_currency_code ");
      Timestamp rateTs = new Timestamp(rateDate.getTime());
      ps.setTimestamp(1,rateTs);
      ps.setLong(2,merchNum);
      ps.setString(3,toCur);
      ps.setString(4,fromCur);

      rs = ps.executeQuery();
      if (rs.next())
      {
        return rs.getBigDecimal(1);
      }
    }
    catch (Exception e)
    {
      log.error("Error fetching MERCHANT rate (merch " + merchNum + " from "
        + fromCur + " to " + toCur + " on " + rateDate + "): " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
    return null;
  }

  /**
   * Determine merchant conversion rate for the given merchant number.  Allows
   * conversion from any currency to any currency. If rate exists for
   * the given date, it is used, otherwise the most recent rate is returned.
   * If from and to currencies are identical, rate of 1 is automatically
   * generated.
   */
  public BigDecimal getMerchantConversionRate(long merchNum, String fromCur, 
    String toCur, Date rateDate)
  {
    BigDecimal rate = null;

    // identical to/from currencies case, just return 1
    if (fromCur.equals(toCur))
    {
      log.debug("Identical to and from currencies, setting rate to 1");
      rate = new BigDecimal(1);
    }
    // lookup rate from db
    else
    {
      rate = fetchMerchantConversionRate(merchNum,fromCur,toCur,rateDate);
    }

    // log determined rate
    if (rate != null)
    {
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      log.debug("Rate for " + merchNum + ", " + sdf.format(rateDate) + ", " 
        + fromCur + " -> " + toCur + ": " + rate);
    }
    else
    {
      log.debug("No rate found (merch " + merchNum + " from " + fromCur 
        + " to " + toCur + " on " + rateDate + ")");
        
      // default to use the MES default rate (no markup)
      rate = fetchMerchantConversionRate(394100000L,fromCur,toCur,rateDate);
      if (rate == null)
      {
        rate = new BigDecimal(1);
        log.debug("No default MES rate, fell back to 1");
      }
      else
      {
        log.debug("Loaded default MES rate: " + rate);
      }
    }

    return rate;
  }

  /**
   * Get conversion rate to convert from given currency to US Dollar.
   */
  public BigDecimal getMerchantConversionRateToUsd(long merchNum,
    String fromCur, Date rateDate)
  {
    return getMerchantConversionRate(merchNum,fromCur,"USD",rateDate);
  }

  /**
   * Fetches the merchant conversion rate corresponding to the given member id.
   */
  public BigDecimal getMemberConversionRate(int memberId, String fromCur,
    String toCur, Date rateDate)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String memStr = "'" + memberId + ",%'";
      
      s = con.createStatement();
      rs = s.executeQuery(
        " select                                      " +
        "  unique(tp.merchant_number) merchant_number " +
        " from                                        " +
        "  trident_profile_api tpa,                   " +
        "  trident_profile tp                         " +
        " where                                       " +
        "  tpa.intl_credentials like " + memStr + "   " +
        "  and tpa.terminal_id = tp.terminal_id       " +
        "  and rownum = 1                             ");
      long merchNum = 0L;
      if (rs.next())
      {
        merchNum = rs.getLong(1);
      }
      else
      {
        // HACK to prevent invalid member IDs from blowing up parsing
        merchNum = 941000057778L;
      }
      
      return getMerchantConversionRate(merchNum,fromCur,toCur,rateDate);
    }
    catch (Exception e)
    {
      log.error("Error fetching member rate (member " + memberId + " from " 
        + fromCur + " to " + toCur + " on " + rateDate + "): " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { s.close(); } catch (Exception ie) { }
      cleanUp();
    }
    return null;
  }
  public BigDecimal getMemberConversionRateToUsd(int memberId, String fromCur,
    Date rateDate)
  {
    return getMemberConversionRate(memberId,fromCur,"USD",rateDate);
  }
  
  /**
   * Fetches the special base conversion rates that are maintained for
   * conversions without markup.  These rates are inverted relative to
   * other rates in the db table, so the reciprocal of the db rate
   * is returned.  The rate date is truncated so that the 5pm expiration
   * date will work through the end of the daty.
   */
  public BigDecimal getBaseConversionRateToUsd(String fromCur, Date rateDate)
  {
    // get rate using base rate merchant id 394100000
    BigDecimal baseRate = getMerchantConversionRate(394100000L,
                            fromCur,"USD",truncateDate(rateDate));

    // invert, maintain original scale
    BigDecimal inverse = new BigDecimal(1);
    inverse = inverse.divide(baseRate, baseRate.scale(), RoundingMode.HALF_UP);
    log.debug("Base rate converted: " + inverse);
    return inverse;
  }



  /**
   * Clear out all settlement load data with the load file name given.
   * Also zeroes out fees in related payvision_api_tran_detail fields.
   */
  private void purgeSettlementLoad(SettlementLoad load)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    PreparedStatement ps4 = null;
    PreparedStatement ps5 = null;
    ResultSet rs = null;

    try
    {
      connect();

      // look for the file name
      String loadName = load.getLoadFile();
      ps1 = con.prepareStatement(
        " select load_id from payvision_setl_loads  " +
        " where load_file = ?                       ");
      ps1.setString(1,loadName);

      rs = ps1.executeQuery();
      if (!rs.next())
      {
        return;
      }
      long loadId = rs.getLong(1);

      // null out all related fee fields in api tran detail
      ps2 = con.prepareStatement(
        " update payvision_api_tran_detail          " +
        " set usd_discount_fee = null               " +
        " where                                     " +
        "  track_mem_code in                        " +
        "   ( select                                " +
        "      d.track_mem_code                     " +
        "     from                                  " +
        "      payvision_setl_detail d,             " +
        "      payvision_setl_loads l               " +
        "     where                                 " +
        "      l.load_file = ?                      " +
        "      and l.load_id = d.load_id )          ");
      ps2.setString(1,loadName);
      int cnt = ps2.executeUpdate();
      log.debug("Cleared " + cnt + " fees");

      // delete details
      ps3 = con.prepareStatement(
        " delete from payvision_setl_detail         " +
        " where load_id = ?                         ");
      ps3.setLong(1,loadId);
      cnt = ps3.executeUpdate();
      log.debug("Deleted " + cnt + " details");

      // delete summaries
      ps4 = con.prepareStatement(
        " delete from payvision_setl_summary        " +
        " where load_id = ?                         ");
      ps4.setLong(1,loadId);
      cnt = ps4.executeUpdate();
      log.debug("Deleted " + cnt + " summaries");

      // delete load record
      ps5 = con.prepareStatement(
        " delete from payvision_setl_loads          " +
        " where load_id = ?                         ");
      ps5.setLong(1,loadId);
      cnt = ps5.executeUpdate();
      log.debug("Deleted " + cnt + " loads");
    }
    catch (Exception e)
    {
      log.error("Error saving settlement load data: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      try { ps3.close(); } catch (Exception ie) { }
      try { ps4.close(); } catch (Exception ie) { }
      try { ps5.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }


  /**
   * Saves csv settlement load data.
   *
   * TODO: make this handle reloads gracefully
   */
  public void saveSettlementLoad(SettlementLoad load)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    PreparedStatement ps4 = null;
    ResultSet rs = null;

    try
    {
      connect(true);

      purgeSettlementLoad(load);

      ps1 = con.prepareStatement(
        " select * from payvision_setl_detail   " +
        " where track_mem_code = ?              ");

      ps2 = con.prepareStatement(
        " insert into payvision_setl_loads (    " +
        "  load_id,                             " +
        "  load_file,                           " +
        "  load_ts,                             " +
        "  result,                              " +
        "  details )                            " +
        " values ( ?, ?, ?, ?, ? )              ");

      ps3 = con.prepareStatement(
        " insert into payvision_setl_summary (  " +
        "  load_id,                             " +
        "  member_id,                           " +
        "  merch_acct_id,                       " +
        "  settle_date,                         " +
        "  currency_code_str,                   " +
        "  total_amount,                        " +
        "  discount_fees,                       " +
        "  other_fees,                          " +
        "  net_payment,                         " +
        "  payment_total,                       " +
        "  tran_count,                          " +
        "  rate_id,                             " +
        "  usd_discount_fees )                  " +
        " values ( ?, ?, ?, ?, ?, ?, ?,         " +
        "          ?, ?, ?, ?, ?, ? )           ");

      ps4 = con.prepareStatement(
        " insert into payvision_setl_detail (   " +
        "  load_id,                             " +
        "  member_id,                           " +
        "  merch_acct_id,                       " +
        "  settle_date,                         " +
        "  track_mem_code,                      " +
        "  tran_date,                           " +
        "  tran_amount,                         " +
        "  currency_code,                       " +
        "  card_type,                           " +
        "  tran_type,                           " +
        "  usd_discount_fee )                   " +
        " values ( ?, ?, ?, ?, ?, ?,            " +
        "          ?, ?, ?, ?, ? )              ");

      // load record
      ps2.setLong       (1,load.getLoadId());
      ps2.setString     (2,load.getLoadFile());
      ps2.setTimestamp  (3,load.getLoadTs());
      ps2.setString     (4,load.getResult());
      ps2.setString     (5,load.getDetails());
      ps2.executeUpdate();

      // summary records
      for (Iterator i = load.getSummaries().iterator(); i.hasNext();)
      {
        SettlementSummary summary = (SettlementSummary)i.next();
        ps3.setLong       (1,summary.getLoadId());
        ps3.setInt        (2,summary.getMemberId());
        ps3.setLong       (3,summary.getMerchAcctId());
        ps3.setTimestamp  (4,summary.getSettleTs());
        ps3.setString     (5,summary.getCurrencyCodeStr());
        ps3.setBigDecimal (6,summary.getTotalAmount());
        ps3.setBigDecimal (7,summary.getDiscountFees());
        ps3.setBigDecimal (8,summary.getOtherFees());
        ps3.setBigDecimal (9,summary.getNetPayment());
        ps3.setBigDecimal (10,summary.getPaymentTotal());
        ps3.setInt        (11,summary.getTranCount());
        ps3.setInt        (12,summary.getRateId());
        ps3.setBigDecimal (13,summary.getUsdDiscountFees());
        ps3.executeUpdate();

        for (Iterator j = summary.getDetails().iterator(); j.hasNext();)
        {
          SettlementDetail detail = (SettlementDetail)j.next();
          ps1.setString(1,detail.getTrackMemCode());
          rs = ps1.executeQuery();
          boolean isDup = rs.next();
          rs.close();
          if (isDup)
          {
            log.debug("skipping duplicate tracking member code: " 
              + detail.getTrackMemCode());
            continue;
          }
          ps4.setLong       (1,detail.getLoadId());
          ps4.setInt        (2,detail.getMemberId());
          ps4.setLong       (3,detail.getMerchAcctId());
          ps4.setTimestamp  (4,detail.getSettleTs());
          ps4.setString     (5,detail.getTrackMemCode());
          ps4.setTimestamp  (6,detail.getTranTs());
          ps4.setBigDecimal (7,detail.getTranAmount());
          ps4.setInt        (8,detail.getCurrencyCode());
          ps4.setInt        (9,detail.getCardType());
          ps4.setInt        (10,detail.getTranType());
          ps4.setBigDecimal (11,detail.getUsdDiscountFee());
          ps4.executeUpdate();
        }                           }
    }
    catch (Exception e)
    {
      log.error("Error saving settlement load data: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      try { ps3.close(); } catch (Exception ie) { }
      try { ps4.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }
}