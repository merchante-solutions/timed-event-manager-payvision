package com.mes.payvision;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SettlementSummary
{
  private long loadId;
  private int memberId;
  private long merchAcctId;
  private Date settleDate;
  private String currencyCodeStr;
  private BigDecimal totalAmount;
  private BigDecimal discountFees;
  private BigDecimal otherFees;
  private BigDecimal netPayment;
  private BigDecimal paymentTotal;
  private int tranCount;
  private int rateId;
  private BigDecimal usdDiscountFees;

  private List details = new ArrayList();

  public void setLoadId(long loadId)
  {
    this.loadId = loadId;
  }
  public long getLoadId()
  {
    return loadId;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setSettleDate(Date settleDate)
  {
    this.settleDate = settleDate;
  }
  public Date getSettleDate()
  {
    return settleDate;
  }
  public void setSettleTs(Timestamp settleTs)
  {
    settleDate = ApiRequest.tsToDate(settleTs);
  }
  public Timestamp getSettleTs()
  {
    return ApiRequest.dateToTs(settleDate);
  }

  public void setCurrencyCodeStr(String currencyCodeStr)
  {
    this.currencyCodeStr = currencyCodeStr;
  }
  public String getCurrencyCodeStr()
  {
    return currencyCodeStr;
  }

  public void setTotalAmount(BigDecimal totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  public BigDecimal getTotalAmount()
  {
    return totalAmount;
  }

  public void setDiscountFees(BigDecimal discountFees)
  {
    this.discountFees = discountFees;
  }
  public BigDecimal getDiscountFees()
  {
    return discountFees;
  }

  public void setOtherFees(BigDecimal otherFees)
  {
    this.otherFees = otherFees;
  }
  public BigDecimal getOtherFees()
  {
    return otherFees;
  }

  public void setNetPayment(BigDecimal netPayment)
  {
    this.netPayment = netPayment;
  }
  public BigDecimal getNetPayment()
  {
    return netPayment;
  }

  public void setPaymentTotal(BigDecimal paymentTotal)
  {
    this.paymentTotal = paymentTotal;
  }
  public BigDecimal getPaymentTotal()
  {
    return paymentTotal;
  }

  public void setTranCount(int tranCount)
  {
    this.tranCount = tranCount;
  }
  public int getTranCount()
  {
    return tranCount;
  }

  public void setRateId(int rateId)
  {
    this.rateId = rateId;
  }
  public int getRateId()
  {
    return rateId;
  }

  public void setUsdDiscountFees(BigDecimal usdDiscountFees)
  {
    this.usdDiscountFees = usdDiscountFees;
  }
  public BigDecimal getUsdDiscountFees()
  {
    return usdDiscountFees;
  }

  public void addDetail(SettlementDetail detail)
  {
    details.add(detail);
  }
  public List getDetails()
  {
    return details;
  }

  public String toString()
  {
    return "SettlementSummary [ "
      + "load id: " + loadId
      + ", member id: " + memberId
      + ", merch acct id: " + merchAcctId
      + ", settle date: " + settleDate
      + ", curr code: " + currencyCodeStr
      + ", total: " + totalAmount
      + ", disc fees: " + discountFees
      + ", other fees: " + otherFees
      + ", net: " + netPayment
      + ", pay total: " + paymentTotal
      + ", tran cnt: " + tranCount
      + ", rate id: " + rateId
      + ", usd disc fees: " + usdDiscountFees
      + " ]";
  }
}