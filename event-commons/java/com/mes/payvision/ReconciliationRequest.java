package com.mes.payvision;

import java.util.Calendar;
import java.util.Date;
import com.mes.config.MesDefaults;
import com.mes.payvision.reports.v1_1.axis.transactions.Reconciliation;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationMerchantAccount;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationResult;
import com.mes.payvision.reports.v1_1.axis.transactions.ReconciliationTransaction;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsLocator;
import com.mes.payvision.reports.v1_1.axis.transactions.TransactionsSoap_BindingStub;

public class ReconciliationRequest extends DateRangeRequest
{
  private Reconciliation reconciliation;
  
  public ReconciliationRequest(){  
  }

  public ReconciliationRequest(String userName, int memberId, Date startDate, 
    Date endDate) throws Exception
  {
    super(userName,memberId,startDate,endDate);
  }

  public void sendRequest()
  {
    try
    {
      Calendar startCal = Calendar.getInstance();
      startCal.setTime(startDate);

      Calendar endCal = Calendar.getInstance();
      endCal.setTime(endDate);

        TransactionsLocator service = new TransactionsLocator();
        service.setEndpointAddress(getPortName(), hostAddr + MesDefaults.PAYVISION_TRANSACTIONS_PATH);
        TransactionsSoap_BindingStub client = (TransactionsSoap_BindingStub)service.getTransactionsSoap();
        
      ReconciliationResult result = client.getReconciliation(
        crMemberId,crMemberGuid,memberId,startCal,endCal);
      reconciliation = result.getData();

      ReconciliationResponse response = 
        new ReconciliationResponse(this,result.getCode(),result.getMessage());

      if (result.getCode() == 0)
      {
        ReconciliationMerchantAccount[] merchAcct 
          = reconciliation.getMerchantAccounts()
              .getReconciliationMerchantAccount();

        for (int i = 0; i < merchAcct.length; ++i)
        {
          ReconciliationSummary summary = new ReconciliationSummary();
          summary.setMerchAcctId(merchAcct[i].getMerchantAccountId());
          summary.setCurrencyCode(merchAcct[i].getSettlementCurrency());
          summary.setAuthCount(merchAcct[i].getAuthorizes().getCount());
          summary.setAuthTotal(
            merchAcct[i].getAuthorizes().getTotalAmount());
          summary.setCaptureCount(merchAcct[i].getCaptures().getCount());
          summary.setCaptureTotal(
            merchAcct[i].getCaptures().getTotalAmount());
          summary.setVoidCount(merchAcct[i].getVoids().getCount());
          summary.setVoidTotal(merchAcct[i].getVoids().getTotalAmount());
          summary.setPaymentCount(merchAcct[i].getPayments().getCount());
          summary.setPaymentTotal(
            merchAcct[i].getPayments().getTotalAmount());
          summary.setRefundCount(merchAcct[i].getRefunds().getCount());
          summary.setRefundTotal(merchAcct[i].getRefunds().getTotalAmount());
          summary.setReferralCount(
            merchAcct[i].getReferralApprovals().getCount());
          summary.setReferralTotal(
            merchAcct[i].getReferralApprovals().getTotalAmount());
          summary.setTransferCount(
            merchAcct[i].getCardFundTransfers().getCount());
          summary.setTransferTotal(
            merchAcct[i].getCardFundTransfers().getTotalAmount());

          ReconciliationTransaction[] tran
            = merchAcct[i].getTransactions().getReconciliationTransaction();
          for (int j = 0; j < tran.length; ++j)
          {
            TransactionDetail detail = new TransactionDetail();
            detail.setRequestId(requestId);
            detail.setMemberId(memberId);
            detail.setMerchAcctId(summary.getMerchAcctId());
            detail.setTranId(tran[j].getTransactionId());
            detail.setTranGuid(tran[j].getTransactionGuid());
            detail.setTrackMemCode(tran[j].getTrackingMemberCode());
            detail.setOrigAmount(tran[j].getOriginalAmount());
            detail.setOrigCurrencyCode(tran[j].getOriginalCurrencyId());
            detail.setMerchAmount(tran[j].getMerchantAmount());
            detail.setCurrencyRate(tran[j].getCurrencyRate());
            detail.setCardId(tran[j].getCardId());
            detail.setCardGuid(tran[j].getCardGuid());
            detail.setCardTypeId(""+tran[j].getCardTypeId());
            detail.setTranDate(tran[j].getTransactionDate().getTime());
            detail.setCountryCode(tran[j].getCountryId());
            detail.setBatchDate(tran[j].getBatchDate());
            summary.addDetail(detail);
          }
          response.addSummary(summary);          
        }
      }
      apiResponse = response;
    }
    catch (Exception e)
    {
      apiResponse = new ReconciliationResponse(this,-1,""+e);
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public String getRequestType()
  {
    return "getReconciliation";
  }

  public String getPortName()
  {
    return "TransactionsSoap";
  }

  public Reconciliation getReconciliation()
  {
    return reconciliation;
  }

  public ReconciliationResponse getReconciliationResponse()
  {
    return (ReconciliationResponse)getResponse();
  }
}