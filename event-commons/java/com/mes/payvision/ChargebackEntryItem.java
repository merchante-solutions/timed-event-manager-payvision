package com.mes.payvision;

public class ChargebackEntryItem
{
  private long requestId;
  private int memberId;
  private long cbId;
  private String key;
  private String value;

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setMemberId(int memberId)
  {
    this.memberId = memberId;
  }
  public int getMemberId()
  {
    return memberId;
  }

  public void setCbId(long cbId)
  {
    this.cbId = cbId;
  }
  public long getCbId()
  {
    return cbId;
  }

  public void setKey(String key)
  {
    this.key = key;
  }
  public String getKey()
  {
    return key;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
  public String getValue()
  {
    return value;
  }

  public String toString()
  {
    return "ChargebackEntryItem [ "
      + "req id: " + requestId
      + ", mem id: " + memberId
      + ", cb id: " + cbId
      + ", key: " + key
      + ", value: " + value
      + " ]";
  }
}