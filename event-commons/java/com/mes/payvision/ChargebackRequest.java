package com.mes.payvision;

import java.util.Calendar;
import java.util.Date;
import com.mes.config.MesDefaults;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ArrayOfChargeback;
import com.mes.payvision.reports.v1_1.axis.chargebacks.CdcEntryItem;
import com.mes.payvision.reports.v1_1.axis.chargebacks.Chargeback;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebackResult;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksLocator;
import com.mes.payvision.reports.v1_1.axis.chargebacks.ChargebacksSoap_BindingStub;


public class ChargebackRequest extends DateRangeRequest
{
  private Chargeback[] chargebacks;
  
  public ChargebackRequest(){  
  }

  public ChargebackRequest(String userName, int memberId, Date startDate, 
    Date endDate) throws Exception
  {
    super(userName,memberId,startDate,endDate);
  }

  public void sendRequest()
  {
    try
    {
      Calendar startCal = Calendar.getInstance();
      startCal.setTime(startDate);

      Calendar endCal = Calendar.getInstance();
      endCal.setTime(endDate);

      ChargebacksLocator service = new ChargebacksLocator();
      service.setEndpointAddress(getPortName(), hostAddr + MesDefaults.PAYVISION_CHARGEBACKS_PATH);
      ChargebacksSoap_BindingStub client = (ChargebacksSoap_BindingStub)service.getChargebacksSoap();

      ChargebackResult result = client.getChargebacks(
        crMemberId,crMemberGuid,memberId,startCal,endCal);

      ChargebackResponse response = 
        new ChargebackResponse(this,result.getCode(),result.getMessage());
      
      ArrayOfChargeback data = result.getData();
      chargebacks = data.getChargeback();

      if (result.getCode() == 0)
      {
        for (int i = 0; i < chargebacks.length; ++i)
        {
          Chargeback cb = chargebacks[i];
          ChargebackRecord cbRec = new ChargebackRecord();
          cbRec.setRequestId(requestId);
          cbRec.setMemberId(memberId);
          cbRec.setCbId(cb.getChargebackId());
          cbRec.setCaseId(cb.getCaseId());
          cbRec.setCbTypeId(cb.getChargebackTypeId());
          cbRec.setAmount(cb.getAmount());
          cbRec.setCurrencyCode(cb.getCurrencyId());
          cbRec.setProcDate(cb.getProcessedDate().getTime());
          cbRec.setReasonCode(cb.getReasonCode());
          cbRec.setCardId(cb.getCardId());
          cbRec.setCardGuid(cb.getCardGuid());
          cbRec.setMerchAcctId(cb.getMerchantAccountId());
          cbRec.setTranDate(cb.getOriginalTransactionDate().getTime());
          cbRec.setTranId(cb.getTransactionId());

          CdcEntryItem[] item = cb.getAdditionalInfo().getCdcEntryItem();
          if (item != null)
          {
            for (int j = 0; j < item.length; ++j)
            {
              ChargebackEntryItem ei = new ChargebackEntryItem();
              ei.setRequestId(requestId);
              ei.setMemberId(memberId);
              ei.setCbId(cbRec.getCbId());
              ei.setKey(item[j].getKey());
              ei.setValue(item[j].getValue());
              cbRec.addEntryItem(ei);
            }
          }

          response.addRecord(cbRec);
        }
      }
      apiResponse = response;
    }
    catch (Exception e)
    {
      apiResponse = new ChargebackResponse(this,-1,""+e);
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public String getRequestType()
  {
    return "getChargebacks";
  }

  public String getPortName()
  {
    return "ChargebacksSoap";
  }

  public Chargeback[] getChargebacks()
  {
    return chargebacks;
  }

  public ChargebackResponse getChargebackResponse()
  {
    return (ChargebackResponse)getResponse();
  }
}