package com.mes.payvision;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class DeclineRecord
{
  private long        requestId;
  private long        memberId;
  private long        procTranId;
  private String      procTranGuid;
  private String      tranType;
  private String      trackMemCode;
  private BigDecimal  origAmount;
  private int         origCurrencyCode;
  private BigDecimal  merchAmount;
  private BigDecimal  merchCurrencyRate;
  private Date        requestDate;
  private Date        processDate;
  private long        cardId;
  private String      cardGuid;
  private String      cardType;
  private long        merchAcctId;
  private int         countryCode;
  private String      bankCode;
  private String      bankMsg;

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setMemberId(long memberId)
  {
    this.memberId = memberId;
  }
  public long getMemberId()
  {
    return memberId;
  }

  public void setProcTranId(long procTranId)
  {
    this.procTranId = procTranId;
  }
  public long getProcTranId()
  {
    return procTranId;
  }

  public void setProcTranGuid(String procTranGuid)
  {
    this.procTranGuid = procTranGuid;
  }
  public String getProcTranGuid()
  {
    return procTranGuid;
  }

  public void setTranType(String tranType)
  {
    this.tranType = tranType;
  }
  public String getTranType()
  {
    return tranType;
  }

  public void setTrackMemCode(String trackMemCode)
  {
    this.trackMemCode = trackMemCode;
  }
  public String getTrackMemCode()
  {
    return trackMemCode;
  }

  public void setOrigAmount(BigDecimal origAmount)
  {
    this.origAmount = origAmount;
  }
  public BigDecimal getOrigAmount()
  {
    return origAmount;
  }

  public void setOrigCurrencyCode(int origCurrencyCode)
  {
    this.origCurrencyCode = origCurrencyCode;
  }
  public int getOrigCurrencyCode()
  {
    return origCurrencyCode;
  }

  public void setMerchAmount(BigDecimal merchAmount)
  {
    this.merchAmount = merchAmount;
  }
  public BigDecimal getMerchAmount()
  {
    return merchAmount;
  }

  public void setMerchCurrencyRate(BigDecimal merchCurrencyRate)
  {
    this.merchCurrencyRate = merchCurrencyRate;
  }
  public BigDecimal getMerchCurrencyRate()
  {
    return merchCurrencyRate;
  }

  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  public Date getRequestDate()
  {
    return requestDate;
  }
  public void setRequestTs(Timestamp requestTs)
  {
    requestDate = ApiRequest.tsToDate(requestTs);
  }
  public Timestamp getRequestTs()
  {
    return ApiRequest.dateToTs(requestDate);
  }

  public void setProcessDate(Date processDate)
  {
    this.processDate = processDate;
  }
  public Date getProcessDate()
  {
    return processDate;
  }
  public void setProcessTs(Timestamp processTs)
  {
    processDate = ApiRequest.tsToDate(processTs);
  }
  public Timestamp getProcessTs()
  {
    return ApiRequest.dateToTs(processDate);
  }

  public void setCardId(long cardId)
  {
    this.cardId = cardId;
  }
  public long getCardId()
  {
    return cardId;
  }

  public void setCardGuid(String cardGuid)
  {
    this.cardGuid = cardGuid;
  }
  public String getCardGuid()
  {
    return cardGuid;
  }

  public void setCardType(String cardType)
  {
    this.cardType = cardType;
  }
  public String getCardType()
  {
    return cardType;
  }

  public void setMerchAcctId(long merchAcctId)
  {
    this.merchAcctId = merchAcctId;
  }
  public long getMerchAcctId()
  {
    return merchAcctId;
  }

  public void setCountryCode(int countryCode)
  {
    this.countryCode = countryCode;
  }
  public int getCountryCode()
  {
    return countryCode;
  }

  public void setBankCode(String bankCode)
  {
    this.bankCode = bankCode;
  }
  public String getBankCode()
  {
    return bankCode;
  }

  public void setBankMsg(String bankMsg)
  {
    this.bankMsg = bankMsg;
  }
  public String getBankMsg()
  {
    return bankMsg;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("DeclineRecord [ ");
    buf.append("req id: " + requestId);
    buf.append(", mem id: " + memberId);
    buf.append(", proc tran id:" + procTranId);
    buf.append(", proc tran guid:" + procTranGuid);
    buf.append(", tran type:" + tranType);
    buf.append(", track mem code:" + trackMemCode);
    buf.append(", orig amt:" + origAmount);
    buf.append(", orig cur code:" + origCurrencyCode);
    buf.append(", merch amt:" + merchAmount);
    buf.append(", merch cur rate:" + merchCurrencyRate);
    buf.append(", req date:" + requestDate);
    buf.append(", proc date:" + processDate);
    buf.append(", card id:" + cardId);
    buf.append(", card guid:" + cardGuid);
    buf.append(", card type:" + cardType);
    buf.append(", merch acct id:" + merchAcctId);
    buf.append(", cntry code:" + countryCode);
    buf.append(", bank code:" + bankCode);
    buf.append(", bank msg:" + bankMsg);
    buf.append(" ]");
    return ""+buf;
  }
}