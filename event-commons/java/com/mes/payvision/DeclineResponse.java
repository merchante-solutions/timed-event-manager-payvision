package com.mes.payvision;

import java.util.ArrayList;
import java.util.List;

public class DeclineResponse extends ApiResponse
{
  private List records = new ArrayList();
  
  public DeclineResponse() {	  
  }

  public DeclineResponse(DeclineRequest request, int code, 
    String message)
  {
    super(request,code,message);
  }

  public List getRecords()
  {
    return records;
  }
  public void addRecord(DeclineRecord record)
  {
    records.add(record);
  }
}