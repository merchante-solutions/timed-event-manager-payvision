package com.mes.payvision;

import java.util.ArrayList;
import java.util.List;

public class ChargebackResponse extends ApiResponse
{
  private List records = new ArrayList();
  
  public ChargebackResponse() {	  
  }

  public ChargebackResponse(ChargebackRequest request, int code, 
    String message)
  {
    super(request,code,message);
  }

  public List getRecords()
  {
    return records;
  }
  public void addRecord(ChargebackRecord record)
  {
    records.add(record);
  }
}