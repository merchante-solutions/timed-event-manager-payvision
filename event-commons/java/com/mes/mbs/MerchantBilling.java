/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/MerchantBilling.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2010-04-08 11:00:32 -0700 (Thu, 08 Apr 2010) $
  Version            : $Revision: 17164 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class MerchantBilling
{
  // merchant information
  protected   String    DbaName           = null;
  protected   long      MerchantId        = 0L;

  // billing storage
  protected   Vector    BillingElements   = new Vector();
  
  public MerchantBilling(long mid, String dbaName)
  {
    DbaName     = dbaName;
    MerchantId  = mid;
  }
  
  public void addElement( MerchantBillingElement el )
  {
    BillingElements.addElement(el);
  }
  
  public Vector getBillingElements()
  {
    return( BillingElements );
  }
  
  public MerchantBillingElement findItemByType( int itemType )
  {
    return( findItemByType(itemType,null,true) );
  }  
  
  public MerchantBillingElement findItemByType( int itemType, String itemSubclass )
  {
    return( findItemByType(itemType,itemSubclass,true) );
  }
  
  public MerchantBillingElement findItemByType( int itemType, String itemSubclass, boolean useDefault )
  {
    MerchantBillingElement  defaultItem   = null;
    MerchantBillingElement  item          = null;
    MerchantBillingElement  retVal        = null;
    
    for ( int i = 0; i < BillingElements.size(); ++i )
    {
      item = (MerchantBillingElement)BillingElements.elementAt(i);
      
      // store the default item for this item type
      if (itemType == item.ItemType)
      {
        if ( item.ItemSubclass == null )
        {
          defaultItem = item;
        
          // if we are looking for the default, then
          // store it and exit for loop
          if( useDefault && itemSubclass == null ) 
          {
            retVal = defaultItem;
            break;
          }
        }
        else if ( (itemSubclass != null) && itemSubclass.equals(item.ItemSubclass) )
        {
          retVal = item;
          break;
        }
      }
    }
    
    // if no match was found, then
    // return the default item
    if ( useDefault && retVal == null )
    {
      retVal = defaultItem;
    }
    return( retVal );
  }
  
  public MerchantBillingElement findItemByRecId( long recordId )
  {
    MerchantBillingElement          item = null;
    for ( int i = 0; i < BillingElements.size(); ++i )
    {
      if ( recordId == ((MerchantBillingElement)BillingElements.elementAt(i)).getRecId() )
      {
        item = (MerchantBillingElement)BillingElements.elementAt(i);
        break;
      }
    }
    return( item );
  }
  
  public MerchantBillingElement getBillingElement( int index )
  {
    MerchantBillingElement      item = null;
    try
    {
      item = (MerchantBillingElement)BillingElements.elementAt(index);
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( item );
  }
  
  public int getBillingElementCount( )
  {
    return( BillingElements.size() );
  }
  
  public String getBillingElementDesc( int itemType )
  {
    String                    retVal = "No item found";
    MerchantBillingElement    item   = findItemByType( itemType );
    
    try
    {
      retVal = item.ItemDescription;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public int getBillingElementExemptCount( int itemType )
  {
    return( getBillingElementExemptCount( itemType, null ) );
  }
  
  public int getBillingElementExemptCount( int itemType, String itemSubclass )
  {
    MerchantBillingElement  item    = findItemByType( itemType, itemSubclass );
    int                     retVal  = 0;
    
    try
    {
      retVal = item.ExemptItemCount;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public double getBillingElementRate( int itemType )
  {
    return( getBillingElementRate( itemType, null ) );
  }
  
  public double getBillingElementRate( int itemType, String itemSubclass )
  {
    MerchantBillingElement  item    = findItemByType( itemType, itemSubclass );
    double                  retVal  = 0.0;
    
    try
    {
      retVal = item.Rate;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public long getBillingElementRecordId( int index )
  {
    long               retVal = -1;
    
    try
    {
      retVal = ((MerchantBillingElement) BillingElements.elementAt(index)).getRecId();
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public int getBillingElementType( int index )
  {
    int               retVal = -1;
    
    try
    {
      retVal = ((MerchantBillingElement)BillingElements.elementAt(index)).getItemType();
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public String getBillingElementTypeList( )
  {
    // insure queries will always work even when there is no pricing
    StringBuffer      itemsList = new StringBuffer("( -1");
    
    for( int i = 0; i < BillingElements.size(); ++i )
    {
      // there will always be at least one entry (-1)
      // so it is necessary to insert a ',' before 
      // the next entry.  
      itemsList.append(",");
      itemsList.append( ((MerchantBillingElement)BillingElements.elementAt(i)).ItemType );
    }
    itemsList.append(")");
    
    return( itemsList.toString() );
  }
  
  public String getDbaName( )
  {
    return( DbaName );
  }
  
  public long getMerchantId( )
  {
    return( MerchantId );
  }
  
  public boolean isElementEnabled( int itemType, Date activityDate )
  {
    return( isElementEnabled(itemType,activityDate,null) );
  }
  
  public boolean isElementEnabled( int itemType, Date activityDate, String itemSubclass )
  {
    MerchantBillingElement    item    = findItemByType( itemType, itemSubclass );
    boolean                   retVal  = false;
    
    try
    {
      String fdata = item.BillingMonths;
      Calendar cal = Calendar.getInstance();
      cal.setTime(activityDate);
      int idx = cal.get(Calendar.MONTH);
      
      if ( fdata != null && idx < fdata.length() )
      {
        fdata = fdata.toUpperCase();
        retVal = (fdata.charAt(idx) == 'Y');
      }
    }
    catch( NullPointerException e )
    {
      // contract item not found
    }
    return( retVal );
  }
  
  public void reset( )
  {
    try
    {
        // empty the data
      BillingElements.removeAllElements();
    }
    finally
    {
    }      
  }
  
  public void setDbaName( String value )
  {
    DbaName = value;
  }
  
  public void showData()
  {
    System.out.println("DbaName                   : " + DbaName);
    System.out.println("MerchantId                : " + MerchantId);
    System.out.println("BillingElements.size()    : " + BillingElements.size());
  }
}

  