/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/mbs/BillingDb.java $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $LastChangedDate: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;

public class BillingDb extends SQLJConnectionBase
{
  
  protected static final String     crlf    = "\r\n";

  public static void clearBillingData( long loadFileId )
  {
    new BillingDb()._clearBillingData(loadFileId,0L);
  }
  
  public static void clearBillingData( long loadFileId, long nodeId )
  {
    new BillingDb()._clearBillingData(loadFileId,nodeId);
  }
  
  public void _clearBillingData( long loadFileId, long nodeId )
  {
    PreparedStatement   ps = null;
  
    try
    {
      connect();
      
      // remove any existing items for this type and file
      String sqlText = 
        " delete " + 
        " from    mbs_daily_summary bds  " +
        " where   bds.data_source_id = ? ";
        
      if ( nodeId > 0L )
      {
        sqlText +=  " and bds.merchant_number in                " +
                    "     (                                     " +
                    "       select  gm.merchant_number          " +
                    "       from    organization      o,        " +
                    "               group_merchant    gm        " +
                    "       where   o.org_group = ?             " +
                    "               and gm.org_num = o.org_num  " +
                    "     )                                     ";
      }        
        
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,loadFileId);
      if ( nodeId > 0L ) 
      { 
        ps.setLong(2,nodeId); 
      }
      ps.executeUpdate();
    }
    catch( Exception e )
    {
      logEntry("_clearBillingData(" + loadFileId + ")",e.toString());
    }      
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
  }
  
  public static long getNewLoadSec()
  {
    SQLJConnectionBase      ftpdb       = null;
    long                    loadSec     = 0L;
    PreparedStatement       ps          = null;
    ResultSet               rs          = null;
    
    try
    {
      // uses an mes schema sequence to generate these ids
      // not to be confused with mesload.charge_back_pk table ids
      ftpdb = new SQLJConnectionBase(MesDefaults.getString(MesDefaults.BILLING_DB_FTPDB));
      ftpdb.connect();                // connect to ftp db
      
      // get a new sequence number
      ps = ftpdb.getPreparedStatement("select seq_mon_ext.nextval as load_sec from dual");
      rs = ps.executeQuery();
      rs.next();
      loadSec = rs.getLong("load_sec");
      rs.close();
    }
    catch( Exception e )
    {
      loadSec = -1L;
      try{ftpdb.logEntry( "BillingDb::getNewLoadSec()", e.toString());}catch(Exception ee){}
    }      
    finally
    {
      try{ ps.close(); }catch(Exception ee){}
      try{ ftpdb.cleanUp(); }catch(Exception ee){}
    }
    return( loadSec );
  }
  
  public static Vector loadBillingGroups( int procType )
  {
    return( new BillingDb()._loadBillingGroups(procType) );
  }
  
  public Vector _loadBillingGroups( int procType )
  {
    BillingGroup        billingGroup    = null;
    Vector              billingGroups   = new Vector();
    PreparedStatement   ps              = null;
    ResultSet           resultSet       = null;
    
    try
    {
      connect();
      
      String sqlText = 
        " select  bme.item_type                             as item_type,      " +
        "         bme.item_category                         as item_category,  " +
        "         bme.query_id                              as query_id,       " +
        "         nvl(bme.count_column_name,'item_count')   as count_col_name, " +
        "         nvl(bme.amount_column_name,'item_amount') as amount_col_name," +
        "         bmq.sql_text                              as sql_text,       " +
        "         bmq.classname                             as classname       " +
        " from    mbs_elements   bme,                 " +
        "         mbs_queries    bmq                  " +
        " where   bme.process_type = ?                " +
        "         and bmq.query_id = bme.query_id     " +
        " order by bme.query_id, bme.item_type        ";
        
      ps = getPreparedStatement(sqlText);
      ps.setInt(1,procType);
      resultSet = ps.executeQuery();
      
      int lastQueryId = -1;
      while( resultSet.next() )
      {
        int queryId = resultSet.getInt("query_id");
        
        if ( queryId != lastQueryId )
        {
          billingGroup = new BillingGroup( queryId, resultSet );
          billingGroups.addElement( billingGroup );
        }
        billingGroup.addBillingElement( resultSet );
        lastQueryId = queryId;
      }
      resultSet.close();
      ps.close();
    }
    catch( Exception e )
    {
      logEntry("loadBillingGroups(" + procType + ")",e.toString());
    }      
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( billingGroups );
  }
  
  public static HashMap loadIcBillingData( int bankNumber, int betNumber, Date batchDate )
  {
    return( new BillingDb()._loadIcBillingData(bankNumber, betNumber, batchDate) );
  }  
  
  public HashMap _loadIcBillingData( int bankNumber, int betNumber, Date batchDate )
  {
    PreparedStatement ps          = null;
    ResultSet         resultSet   = null;
    HashMap           retVal      = new HashMap();
  
    try
    {
      connect();
      
      String sqlText = 
        "  with    icd   as (select * from daily_detail_file_ic_desc   where   ? between trunc(valid_date_begin,'month') and valid_date_end),  " + crlf + 
        "          icd2  as (select * from daily_detail_file_ic_desc   where   ? between trunc(valid_date_begin,'month') and valid_date_end),  " + crlf + 
        "          icd3  as (select * from daily_detail_file_ic_desc   where   ? between trunc(valid_date_begin,'month') and valid_date_end),  " + crlf + 
        "          icg   as (select * from mbs_ic_groups               where   ? between valid_date_begin and valid_date_end),  " + crlf + 
        "          icg2  as (select * from mbs_ic_groups               where   ? between valid_date_begin and valid_date_end),  " + crlf + 
        "          icm   as (select * from mbs_ic_markup               where   ? between valid_date_begin and valid_date_end),  " + crlf + 
        "          bgm   as (select * from mbs_bet_to_group_markup     where   ? between valid_date_begin and valid_date_end    " + crlf + 
        "                                                           and (bank_number = ? or (bank_number = 3941 and ? in (3003,3943)))  " + crlf + 
        "                                                           and bet_number = ?                                      )   " + crlf + 
        "  select  (icd.card_type || lpad(icd.ic_code,5,'0')) || '-' || icd.rec_id                                              " + crlf +
        "                                                        as ic_label,                                                   " + crlf + 
        "          icg.group_label                               as group_label,                                                " + crlf + 
        "          icd.ic_code                                   as ic_code,                                                    " + crlf + 
        "          icd.ic_desc                                   as ic_desc,                                                    " + crlf + 
        "          case                                                                                                         " + crlf +
        "            when icg.group_label like '- Pass%'    then  icd.ic_desc                                                   " + crlf +
        "            when bgm.use_alt_rates = 'Y'           then  nvl(icd2.ic_desc,icd.ic_desc)                                 " + crlf +
        "            when icd.issuer_ic_level like '%-reg'  then  nvl(icd3.ic_desc,icd.ic_desc)                                 " + crlf +
        "            else                                         icd.ic_desc                                                   " + crlf +
        "          end                                           as ic_desc_stmt,                                               " + crlf +
        "          case                                                                                                         " + crlf +
        "            when icg.group_label like '- Pass%'    then  icd.ic_rate                                                   " + crlf +
        "            when bgm.use_alt_rates = 'Y'           then  nvl(icd2.ic_rate,icd.ic_rate)                                 " + crlf +
        "            when icd.issuer_ic_level like '%-reg'  then  nvl(icd3.ic_rate,icd.ic_rate)                                 " + crlf +
        "            else                                         icd.ic_rate                                                   " + crlf +
        "          end                                           as ic_rate,                                                    " + crlf +
        "          case                                                                                                         " + crlf +
        "            when icg.group_label like '- Pass%'    then  icd.ic_per_item                                               " + crlf +
        "            when bgm.use_alt_rates = 'Y'           then  nvl(icd2.ic_per_item,icd.ic_per_item)                         " + crlf +
        "            when icd.issuer_ic_level like '%-reg'  then  nvl(icd3.ic_per_item,icd.ic_per_item)                         " + crlf +
        "            else                                         icd.ic_per_item                                               " + crlf +
        "          end                                           as ic_per_item,                                                " + crlf +
        "          icm.abs_rate                                  as abs_rate,                                                   " + crlf + 
        "          icm.abs_per_item                              as abs_per_item,                                               " + crlf + 
        "          case when icd.use_db_rates = 'Y' then nvl(icm.rel_rate_db,icm.rel_rate) else icm.rel_rate end                " + crlf +
        "                                                        as rel_rate,                                                   " + crlf + 
        "          case when icd.use_db_rates = 'Y' then nvl(icm.rel_per_item_db,icm.rel_per_item) else icm.rel_per_item end    " + crlf +
        "                                                        as rel_per_item,                                               " + crlf + 
        "          icm.addl_rate                                 as additional_rate,                                            " + crlf + 
        "          icm.addl_per_item                             as additional_per_item,                                        " + crlf + 
        "          bgm.show_rate_on_statement                    as show_rate_on_statement,                                     " + crlf + 
        "          icd.credits_only                              as returns_only,                                               " + crlf + 
        "          bgm.apply_to_returns                          as apply_to_returns,                                           " + crlf + 
        "          bgm.apply_per_item_when_rate_zero             as per_item_when_rate_zero,                                    " + crlf + 
        "          icd.use_db_rates                              as use_db_rates,                                               " + crlf + 
        "          bgm.use_alt_rates                             as use_alt_rates,                                              " + crlf + 
        "          bgm.percent_neg_db_m_up_to_return             as neg_markup_to_return                                        " + crlf + 
        "  from    bgm,icd,icg,icm,icd2,icd3                                                                                    " + crlf + 
        "  where       icg.group_id        = bgm.group_id                                                                       " + crlf + 
        "          and icm.markup_id       = bgm.markup_id                                                                      " + crlf + 
        "          and icm.group_label     = icg.group_label                                                                    " + crlf + 
        "          and icd.card_type       = icg.plan_type                                                                      " + crlf + 
        "          and icd.ic_code     like  icg.ic_code                                                                        " + crlf + 
        "          and ( icg.ic_code <> '%' or not exists                                                                       " + crlf + 
        "                (                                                                                                      " + crlf +
        "                  select  *                                                                                            " + crlf +
        "                  from    icg2                                                                                         " + crlf +
        "                  where   icg2.group_id = bgm.group_id                                                                 " + crlf +
        "                          and icg2.plan_type = icd.card_type                                                           " + crlf +
        "                          and icg2.ic_code = icd.ic_code                                                               " + crlf +
        "                )                                                                                                      " + crlf +
        "              )                                                                                                        " + crlf +
        "          and icd2.card_type(+) = icd.card_type                                                                        " + crlf +
        "          and icd2.ic_code(+) = icd.alt_ic_code                                                                        " + crlf +
        "          and icd2.valid_date_begin(+) = icd.valid_date_begin                                                          " + crlf +
        "          and icd2.valid_date_end(+) = icd.valid_date_end                                                              " + crlf +
        "          and icd3.card_type(+) = icd.card_type                                                                        " + crlf +
        "          and icd3.ic_code(+) = case when icd.issuer_ic_level like '%-reg' then icd.reg_ic_code else 'invalid' end     " + crlf +
        "          and icd3.valid_date_begin(+) = icd.valid_date_begin                                                          " + crlf +
        "          and icd3.valid_date_end(+) = icd.valid_date_end                                                              " + crlf +
        "  order by ic_label                                                                                                    ";

      ps = getPreparedStatement(sqlText);
      int idx = 0;
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setDate(++idx,batchDate);
      ps.setInt (++idx,bankNumber);
      ps.setInt (++idx,bankNumber);
      ps.setInt (++idx,betNumber);
      resultSet = ps.executeQuery();
      
      while( resultSet.next() )
      {
        retVal.put( resultSet.getString("ic_label"),
                    new IcBillingData(resultSet) );
      }  
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("_loadIcBillingData(" + bankNumber + "," + betNumber + "," + batchDate + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public static MerchantBilling loadMerchantBilling( long merchantId, Date activityDate )
  {
    return( new BillingDb()._loadMerchantBilling(merchantId,activityDate) );
  }
  
  public MerchantBilling _loadMerchantBilling( long merchantId, Date activityDate )
  {
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    MerchantBilling     mp      = new MerchantBilling(merchantId,null);
    
    try
    {
      connect();
      
      String sqlText      = 
        " select  mf.merchant_number        as merchant_number,   " +
        "         mf.dba_name               as dba_name,          " +
        "         mp.rec_id                 as rec_id,            " +
        "         mp.item_type              as item_type,         " +
        "         me.statement_msg_default  as item_desc,         " +
        "         mp.item_subclass          as item_subclass,     " +
        "         mp.rate                   as rate,              " +
        "         mp.per_item               as per_item,          " +
        "         0                         as exempt_count,      " +
        "         mp.volume_type            as vol_type,          " +
        "         mp.billing_months         as billing_months,    " +
        "         mp.valid_date_begin       as valid_date_begin,  " +
        "         mp.valid_date_end         as valid_date_end     " +
        " from    mif               mf,                           " +
        "         mbs_pricing       mp,                           " +
        "         mbs_elements      me                            " +
        " where   mf.merchant_number = ?                          " +
        "         and mp.merchant_number in                       " +
        "         (                                               " +
        "           mf.merchant_number,                           " +
        "           decode(mf.bank_number,3943,3941,3003,3941,mf.bank_number) || lpad(mf.ic_bet_visa,4,'0')" + 
        "         )                                               " +
        "         and ? between mp.valid_date_begin and           " +
        "                       mp.valid_date_end                 " +
        "         and me.item_type(+) = mp.item_type              ";
        
      ps = getPreparedStatement(sqlText);
      ps.setLong(1,merchantId);
      ps.setDate(2,activityDate);
      rs = ps.executeQuery();
      
      if ( rs.next() )
      {
        mp.setDbaName(rs.getString("dba_name"));
      
        // store the billing elements 
        do
        {
          mp.addElement(new MerchantBillingElement(rs));
        }
        while( rs.next() );
      }        
      rs.close();
      ps.close();
    }
    catch( Exception e )
    {
      logEntry("_loadMerchantBilling(" + merchantId + "," + activityDate + ")",e.toString());
    }      
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( mp );
  }
  
  public static HashMap loadMerchantIds( long nodeId )
  {
    return( new BillingDb()._loadMerchantIds(nodeId) );
  }
  
  public HashMap _loadMerchantIds( long nodeId )
  {
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    HashMap             ids     = new HashMap();
    
    try
    {
      connect();
      
      // prepared SQL for merchant pricing
      String sqlText = 
        " select  gm.merchant_number          " +
        " from    organization      o,        " +
        "         group_merchant    gm        " +
        " where   o.org_group = ?             " +
        "         and gm.org_num = o.org_num  ";

      ps = getPreparedStatement(sqlText);
      ps.setLong(1,nodeId);
      rs = ps.executeQuery();
      
      while ( rs.next() )
      {
        ids.put( rs.getString("merchant_number"),
                 rs.getString("merchant_number") );
      }        
      rs.close();
      ps.close();
    }
    catch( Exception e )
    {
      logEntry("_loadMerchantIds(" + nodeId + ")",e.toString());
    }      
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( ids );
  }
  
  public static void storeSimpleSummaryData( SimpleSummaryData summaryData )
  {
    new BillingDb()._storeSimpleSummaryData(summaryData);
  }
  
  public void _storeSimpleSummaryData( SimpleSummaryData summaryData )
  {
    PreparedStatement   ps      = null;
    
    try
    {
      connect();
      
      String sqlText = 
        " select  mbs_daily_summary_seq.nextval as rec_id " +
        " from    dual ";

      ps = getPreparedStatement(sqlText);
      ResultSet rs = ps.executeQuery();
      rs.next();
      long recId = rs.getLong("rec_id");
      rs.close();
      ps.close();
      
      sqlText = 
        " insert into mbs_daily_summary     " +
        " (                                 " +
        "   rec_id,                         " +
        "   merchant_number,                " +
        "   activity_date,                  " +
        "   item_type,                      " +
        "   item_subclass,                  " +
        "   ic_cat,                         " +
        "   item_count,                     " +
        "   item_amount,                    " +
        "   sales_count,                    " +
        "   sales_amount,                   " +
        "   credits_count,                  " +
        "   credits_amount,                 " +
        "   rate,                           " +
        "   per_item,                       " +
        "   fees_due,                       " +
        "   fees_paid,                      " +
        "   expense,                        " +
        "   expense_actual,                 " +
        "   data_source,                    " +
        "   data_source_id,                 " +
        "   me_load_filename,               " +
        "   me_load_file_id                 " +
        " )                                 " +
        " values                            " +
        " ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ) ";
        
      int i = 0;
      ps = getPreparedStatement(sqlText);
      ps.setLong        ( ++i , recId                               );
      ps.setLong        ( ++i , summaryData.getMerchantId()         );
      ps.setDate        ( ++i , summaryData.getActivityDate()       );
      ps.setInt         ( ++i , summaryData.getItemType()           );
      ps.setString      ( ++i , summaryData.getItemSubclass()       );
      ps.setString      ( ++i , summaryData.getIcCat()              );
      ps.setInt         ( ++i , summaryData.getItemCount()          );
      ps.setDouble      ( ++i , summaryData.getItemAmount()         );
      ps.setInt         ( ++i , summaryData.getSalesCount()         );
      ps.setDouble      ( ++i , summaryData.getSalesAmount()        );
      ps.setInt         ( ++i , summaryData.getCreditsCount()       );
      ps.setDouble      ( ++i , summaryData.getCreditsAmount()      );
      ps.setDouble      ( ++i , summaryData.getRate()               );
      ps.setDouble      ( ++i , summaryData.getPerItem()            );
      ps.setDouble      ( ++i , summaryData.getFeesDue()            );
      ps.setDouble      ( ++i , summaryData.getFeesPaid()           );
      ps.setDouble      ( ++i , summaryData.getExpense()            );
      ps.setDouble      ( ++i , summaryData.getExpenseActual()      );
      ps.setString      ( ++i , summaryData.getDataSource()         );
      ps.setLong        ( ++i , summaryData.getDataSourceId()       );
      ps.setString      ( ++i , summaryData.getMeLoadFilename()     );
      ps.setLong        ( ++i , summaryData.getMeLoadFileId()       );
      ps.executeUpdate();
      
      // if the update does not throw an exception, 
      // store the new rec id
      summaryData.setRecId(recId);
    }
    catch( Exception e )
    {
      logEntry("_storeSimpleSummaryData()",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
  }
}