/*************************************************************************

  FILE: $URL$

  Description:

  Last Modified By   : $Author$
  Last Modified Date : $LastChangedDate$
  Version            : $Revision$

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.ResultSet;

public class BillingElement
{
  protected   String    AmountColumnName    = null;
  protected   String    CountColumnName     = null;
  protected   String    ItemCategory        = null;
  protected   int       ItemType            = -1;
  
  public BillingElement( ResultSet resultSet )
    throws java.sql.SQLException
  {
    AmountColumnName  = resultSet.getString("amount_col_name");
    CountColumnName   = resultSet.getString("count_col_name");
    ItemCategory      = resultSet.getString("item_category");
    ItemType          = resultSet.getInt("item_type");
  }
  public String getAmountColumnName() { return(AmountColumnName); }
  public String getCountColumnName()  { return(CountColumnName); }
  public String getItemCategory()     { return(ItemCategory); }
  public int    getItemType()         { return(ItemType); }
}
