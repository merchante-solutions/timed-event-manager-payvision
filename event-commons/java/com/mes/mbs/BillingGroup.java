/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/BillingGroup.java $

  Description:

  Last Modified By   : $Author: mduyck $
  Last Modified Date : $LastChangedDate: 2009-10-23 14:44:23 -0700 (Fri, 23 Oct 2009) $
  Version            : $Revision: 16628 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.ResultSet;
import java.util.Vector;

public class BillingGroup
{
  protected   Vector    BillingElements   = new Vector();
  protected   String    BillingClassname  = null;
  protected   String    ItemCategory      = null;
  protected   int       QueryId           = -1;
  protected   String    SqlText           = null;
  
  public BillingGroup( int queryId, ResultSet resultSet )
    throws java.sql.SQLException
  {
    QueryId           = queryId;
    SqlText           = resultSet.getString("sql_text");
    BillingClassname  = resultSet.getString("classname");
    ItemCategory      = resultSet.getString("item_category");
  }
  
  public void addBillingElement( ResultSet resultSet )
    throws java.sql.SQLException
  {
    BillingElements.add( new BillingElement(resultSet) );
  }
  
  public String getBillingClassname()
  {
    return( BillingClassname );
  }
  
  public Vector getBillingElements()
  {
    return( BillingElements );
  }
  
  public String getItemCategory()
  {
    return( ItemCategory );
  }
  
  public int getQueryId()
  {
    return( QueryId );
  }
  
  public String getSqlText()
  {
    return( SqlText );
  }
}
