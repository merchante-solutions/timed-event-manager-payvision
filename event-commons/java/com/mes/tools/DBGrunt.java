/*@lineinfo:filename=DBGrunt*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DBGrunt.sqlj $

  Description:

    ManagerTable object.

    Loads sales rep manager list from sales_rep table.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 1/09/02 1:20p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DBGrunt extends SQLJConnectionBase
{
  public class MerchData
  {
    public long     merchantNumber;
    public String   phone;
    public String   dbaName;
    public String   email;
    public int      bankNumber;
    public String   association;
    public String   group1;
    public String   group2;
    public String   group3;
    public String   group4;
    public String   group5;
    public String   group6;

    public MerchData( long     merchantNumber,
                      String   phone,
                      String   dbaName,
                      String   email,
                      int      bankNumber,
                      String   association,
                      String   group1,
                      String   group2,
                      String   group3,
                      String   group4,
                      String   group5,
                      String   group6
                    )
    {
      this.merchantNumber = merchantNumber;
      this.phone = phone;
      this.dbaName = dbaName;
      this.email = email;
      this.bankNumber = bankNumber;
      this.association = association;
      this.group1 = group1;
      this.group2 = group2;
      this.group3 = group3;
      this.group4 = group4;
      this.group5 = group5;
      this.group6 = group6;
    }
  }
  
  public class GroupData
  {
    public long   association;
    public long   group1;
    public long   group2;
    public long   group3;
    public long   group4;
    public long   group5;
    public long   group6;
    public long   group7;
    public long   group8;
    public long   group9;
    public long   group10;
    
    public GroupData( long association,
                      long group1,
                      long group2,
                      long group3,
                      long group4,
                      long group5,
                      long group6,
                      long group7,
                      long group8,
                      long group9,
                      long group10)
    {
      this.association = association;
      this.group1 = group1;
      this.group2 = group2;
      this.group2 = group3;
      this.group2 = group4;
      this.group2 = group5;
      this.group2 = group6;
      this.group2 = group7;
      this.group2 = group8;
      this.group2 = group9;
      this.group2 = group10;
    }

  }

  public DBGrunt()
  {
  }
  
  public void rebuildGroupHierarchy()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    Vector              groups      = new Vector();
    long                startTime   = System.currentTimeMillis();
    
    try
    {
      System.out.println("Start Time: " + startTime);
      System.out.println("connecting...");
      connect();
      
      String dbName;
      
      /*@lineinfo:generated-code*//*@lineinfo:145^7*/

//  ************************************************************
//  #sql [Ctx] { select  db_name 
//          from    mes_db_info
//          where   db_name is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  db_name  \n        from    mes_db_info\n        where   db_name is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.DBGrunt",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dbName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
      
      System.out.println("Connected to: " + dbName);
      
      // just get group data for 3941 since that's the one that is screwed up
      int count;
      /*@lineinfo:generated-code*//*@lineinfo:156^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(assoc_number) 
//          from    groups
//          where   group_1 = 3941500001
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(assoc_number)  \n        from    groups\n        where   group_1 = 3941500001";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.DBGrunt",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^7*/
      
      System.out.println("Getting groups...");
      
      /*@lineinfo:generated-code*//*@lineinfo:165^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  assoc_number,
//                  group_1,
//                  group_2,
//                  group_3,
//                  group_4,
//                  group_5,
//                  group_6,
//                  group_7,
//                  group_8,
//                  group_9,
//                  group_10
//          from    groups
//          where   group_1 = 3941500001
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  assoc_number,\n                group_1,\n                group_2,\n                group_3,\n                group_4,\n                group_5,\n                group_6,\n                group_7,\n                group_8,\n                group_9,\n                group_10\n        from    groups\n        where   group_1 = 3941500001";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.DBGrunt",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tools.DBGrunt",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
      
      rs = it.getResultSet();

      int i = 0;
      System.out.print("Adding to Vector: 0 of " + Integer.toString(count));

      while(rs.next())
      {
        ++i;
        
        if(i % 10 == 0)
        {
          System.out.print("\rAdding to list: " + Integer.toString(i) + " of " + Integer.toString(count));
        }
        
        GroupData gd = new GroupData( rs.getLong("assoc_number"),
                                      rs.getLong("group_1"),
                                      rs.getLong("group_2"),
                                      rs.getLong("group_3"),
                                      rs.getLong("group_4"),
                                      rs.getLong("group_5"),
                                      rs.getLong("group_6"),
                                      rs.getLong("group_7"),
                                      rs.getLong("group_8"),
                                      rs.getLong("group_9"),
                                      rs.getLong("group_10"));
        groups.add(gd);
      }
      System.out.println("\nDone generating list.");

      it.close();

      System.out.print("\nGenerating hierarchy: 0 of " + groups.size());
      for(int j = 0; j < groups.size(); ++j)
      {
        GroupData gd = (GroupData)groups.elementAt(j);
        
        int percent = j * 100;
        percent = percent / groups.size();

        System.out.print("\rGenerating hierarchy: " + j + " of " + groups.size() + " (%" + percent + ")");
        
        /*@lineinfo:generated-code*//*@lineinfo:223^9*/

//  ************************************************************
//  #sql [Ctx] { call update_group_hierarchy(:gd.association,
//                                        :gd.group1,
//                                        :gd.group2,
//                                        :gd.group3,
//                                        :gd.group4,
//                                        :gd.group5,
//                                        :gd.group6,
//                                        :gd.group7,
//                                        :gd.group8,
//                                        :gd.group9,
//                                        :gd.group10)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN update_group_hierarchy( :1 ,\n                                       :2 ,\n                                       :3 ,\n                                       :4 ,\n                                       :5 ,\n                                       :6 ,\n                                       :7 ,\n                                       :8 ,\n                                       :9 ,\n                                       :10 ,\n                                       :11 )\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.DBGrunt",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,gd.association);
   __sJT_st.setLong(2,gd.group1);
   __sJT_st.setLong(3,gd.group2);
   __sJT_st.setLong(4,gd.group3);
   __sJT_st.setLong(5,gd.group4);
   __sJT_st.setLong(6,gd.group5);
   __sJT_st.setLong(7,gd.group6);
   __sJT_st.setLong(8,gd.group7);
   __sJT_st.setLong(9,gd.group8);
   __sJT_st.setLong(10,gd.group9);
   __sJT_st.setLong(11,gd.group10);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^9*/
        
        if(j % 10 == 0)
        {
          commit();
        }
      }
      
      commit();

      System.out.println("\nDone.");
      
      long endTime = System.currentTimeMillis();
      System.out.println("End Time : " + endTime);
      
      
      long totalTime = endTime - startTime;
      long hours = totalTime / 3600000L;
      long minutes = (totalTime - (hours * 3600000)) / 60000;
      System.out.println("\nOperation consumed " + hours + " hours, " + minutes + " minutes");
    }
    catch(Exception e)
    {
      System.out.println("DBGrunt.GoGruntGo(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void GoGruntGo()
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    Vector              merchants = new Vector();
    
    long                startTime = System.currentTimeMillis();

    // this version iterates through the groups table to populate the
    // TSYS hierarchy (minus leaf nodes) in Tyler's hierarchy table
    try
    {
      System.out.println("Start Time: " + startTime);
      System.out.println("connecting...");
      connect();
      
      String dbName;
      
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] { select  db_name 
//          from    mes_db_info
//          where   db_name is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  db_name  \n        from    mes_db_info\n        where   db_name is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.DBGrunt",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dbName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^7*/
      
      System.out.println("Connected to: " + dbName);

      int count;
      /*@lineinfo:generated-code*//*@lineinfo:295^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number) 
//          from    mif
//          where   bank_number = 3858 and
//                  mc_ica_mes is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)  \n        from    mif\n        where   bank_number = 3858 and\n                mc_ica_mes is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.DBGrunt",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:301^7*/

      System.out.println("Getting merchants...");
      
      /*@lineinfo:generated-code*//*@lineinfo:305^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchant_number,
//                  phone_1,
//                  dba_name,
//                  email_addr_00,
//                  bank_number,
//                  dmagent,
//                  group_1_association,
//                  group_2_association,
//                  group_3_association,
//                  group_4_association,
//                  group_5_association,
//                  group_6_association
//          from    mif
//          where   bank_number = 3858 and
//                  mc_ica_mes is null
//          order by merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchant_number,\n                phone_1,\n                dba_name,\n                email_addr_00,\n                bank_number,\n                dmagent,\n                group_1_association,\n                group_2_association,\n                group_3_association,\n                group_4_association,\n                group_5_association,\n                group_6_association\n        from    mif\n        where   bank_number = 3858 and\n                mc_ica_mes is null\n        order by merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tools.DBGrunt",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.tools.DBGrunt",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      
      rs = it.getResultSet();

      int i = 0;
      System.out.print("Adding to Vector: 0 of " + Integer.toString(count));

      while(rs.next())
      {
        ++i;
        
        if(i % 10 == 0)
        {
          System.out.print("\rAdding to list: " + Integer.toString(i) + " of " + Integer.toString(count));
        }
        
        MerchData md = new MerchData( rs.getLong  ("merchant_number"),
                                      rs.getString("phone_1"),
                                      rs.getString("dba_name"),
                                      rs.getString("email_addr_00"),
                                      rs.getInt   ("bank_number"),
                                      rs.getString("dmagent"),
                                      rs.getString("group_1_association"),
                                      rs.getString("group_2_association"),
                                      rs.getString("group_3_association"),
                                      rs.getString("group_4_association"),
                                      rs.getString("group_5_association"),
                                      rs.getString("group_6_association"));
        merchants.add(md);
      }
      System.out.println("\nDone generating list.");

      it.close();

      System.out.print("\nGenerating hierarchy: 0 of " + merchants.size());
      for(int j = 0; j < merchants.size(); ++j)
      {
        MerchData md = (MerchData)merchants.elementAt(j);
        
        int percent = j * 100;
        percent = percent / merchants.size();

        System.out.print("\rGenerating hierarchy: " + j + " of " + merchants.size() + " (%" + percent + ")");
        
        /*@lineinfo:generated-code*//*@lineinfo:367^9*/

//  ************************************************************
//  #sql [Ctx] { call update_hierarchy(:md.merchantNumber,
//                                  :md.phone,
//                                  :md.dbaName,
//                                  :md.email,
//                                  :md.bankNumber,
//                                  :md.association,
//                                  :md.group1,
//                                  :md.group2,
//                                  :md.group3,
//                                  :md.group4,
//                                  :md.group5,
//                                  :md.group6)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN update_hierarchy( :1 ,\n                                 :2 ,\n                                 :3 ,\n                                 :4 ,\n                                 :5 ,\n                                 :6 ,\n                                 :7 ,\n                                 :8 ,\n                                 :9 ,\n                                 :10 ,\n                                 :11 ,\n                                 :12 )\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.tools.DBGrunt",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,md.merchantNumber);
   __sJT_st.setString(2,md.phone);
   __sJT_st.setString(3,md.dbaName);
   __sJT_st.setString(4,md.email);
   __sJT_st.setInt(5,md.bankNumber);
   __sJT_st.setString(6,md.association);
   __sJT_st.setString(7,md.group1);
   __sJT_st.setString(8,md.group2);
   __sJT_st.setString(9,md.group3);
   __sJT_st.setString(10,md.group4);
   __sJT_st.setString(11,md.group5);
   __sJT_st.setString(12,md.group6);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:381^9*/
        
        // update this merchant to show that we are done with him
        /*@lineinfo:generated-code*//*@lineinfo:384^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     mc_ica_mes = 'done'
//            where   merchant_number = :md.merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n          set     mc_ica_mes = 'done'\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.tools.DBGrunt",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,md.merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^9*/
        
        if(j % 10 == 0)
        {
          commit();
        }
      }
      
      commit();

      System.out.println("\nDone.");
      
      long endTime = System.currentTimeMillis();
      System.out.println("End Time : " + endTime);
      
      
      long totalTime = endTime - startTime;
      long hours = totalTime / 3600000L;
      long minutes = (totalTime - (hours * 3600000)) / 60000;
      System.out.println("\nOperation consumed " + hours + " hours, " + minutes + " minutes");
    }
    catch(Exception e)
    {
      System.out.println("DBGrunt.GoGruntGo(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  //static utility to get current date from DB
  public static Date getCurrentDate()
  {
    DBGrunt dbg = new DBGrunt();
    return dbg._getCurrentDate();
  }
  
  private Date _getCurrentDate()
  {
    java.sql.Date temp;
    java.util.Date now;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:435^7*/

//  ************************************************************
//  #sql [Ctx] { select sysdate  from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sysdate   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.tools.DBGrunt",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   temp = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^7*/
      now = (java.util.Date)temp;
    }
    catch(Exception e)
    {
      now = Calendar.getInstance().getTime();
    }
    finally
    {
      cleanUp();
    }
    
    return now;
  }
}/*@lineinfo:generated-code*/