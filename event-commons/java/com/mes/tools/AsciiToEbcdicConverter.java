/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/AsciiToEbcdicConverter.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-06-27 11:02:38 -0700 (Thu, 27 Jun 2013) $
  Version            : $Revision: 21249 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.util.HashMap;

public class AsciiToEbcdicConverter
{
  private static final HashMap AsciiToEbcdicMap = 
    new HashMap()
    {
      {
        //  ascii   ebcdic        char    decimal
        put("00", "00");   // NULL      0     
        put("01", "01");   // SOH       1     
        put("02", "02");   // STX       2     
        put("03", "03");   // ETX       3     
        put("04", "37");   // EOT       4     
        put("05", "2D");   // ENQ       5     
        put("06", "2E");   // ACK       6     
        put("07", "2F");   // BEL       7     
        put("08", "16");   // BS        8     
        put("09", "05");   // HT        9     
        put("0A", "25");   // LF       10     
        put("0B", "0B");   // VT       11     
        put("0C", "0C");   // FF       12     
        put("0D", "0D");   // CR       13     
        put("0E", "0E");   // SO       14     
        put("0F", "0F");   // SI       15     
        put("10", "10");   // DLE      16     
        put("11", "11");   // DC1      17     
        put("12", "12");   // DC2      18     
        put("13", "13");   // DC3      19     
        put("14", "3C");   // DC4      20     
        put("15", "3D");   // NAK      21     
        put("16", "32");   // SYN      22     
        put("17", "26");   // ETB      23     
        put("18", "18");   // CAN      24     
        put("19", "19");   // EM       25     
        put("1A", "3F");   // SUB      26     
        put("1B", "27");   // ESC      27     
        put("1C", "1C");   // FS       28     
        put("1D", "1D");   // GS       29     
        put("1E", "1E");   // RS       30     
        put("1F", "1F");   // US       31     
        put("20", "40");   // SPACE    32     
        put("21", "4F");   // !        33     
        put("22", "7F");   // "        34     
        put("23", "7B");   // #        35     
        put("24", "5B");   // $        36     
        put("25", "6C");   // %        37     
        put("26", "50");   // &        38     
        put("27", "7D");   // '        39     
        put("28", "4D");   // (        40     
        put("29", "5D");   // )        41     
        put("2A", "5C");   // *        42     
        put("2B", "4E");   // +        43     
        put("2C", "6B");   // ,        44     
        put("2D", "60");   // -        45     
        put("2E", "4B");   // .        46     
        put("2F", "61");   // /        47     
        put("30", "F0");   // 0        48     
        put("31", "F1");   // 1        49     
        put("32", "F2");   // 2        50     
        put("33", "F3");   // 3        51     
        put("34", "F4");   // 4        52     
        put("35", "F5");   // 5        53     
        put("36", "F6");   // 6        54     
        put("37", "F7");   // 7        55     
        put("38", "F8");   // 8        56     
        put("39", "F9");   // 9        57     
        put("3A", "7A");   // :        58     
        put("3B", "5E");   // ;        59     
        put("3C", "4C");   // <        60     
        put("3D", "7E");   // =        61     
        put("3E", "6E");   // >        62     
        put("3F", "6F");   // ?        63     
        put("40", "7C");   // @        64     
        put("41", "C1");   // A        65     
        put("42", "C2");   // B        66     
        put("43", "C3");   // C        67     
        put("44", "C4");   // D        68     
        put("45", "C5");   // E        69     
        put("46", "C6");   // F        70     
        put("47", "C7");   // G        71     
        put("48", "C8");   // H        72     
        put("49", "C9");   // I        73     
        put("4A", "D1");   // J        74     
        put("4B", "D2");   // K        75     
        put("4C", "D3");   // L        76     
        put("4D", "D4");   // M        77     
        put("4E", "D5");   // N        78     
        put("4F", "D6");   // O        79     
        put("50", "D7");   // P        80     
        put("51", "D8");   // Q        81     
        put("52", "D9");   // R        82     
        put("53", "E2");   // S        83     
        put("54", "E3");   // T        84     
        put("55", "E4");   // U        85     
        put("56", "E5");   // V        86     
        put("57", "E6");   // W        87     
        put("58", "E7");   // X        88     
        put("59", "E8");   // Y        89     
        put("5A", "E9");   // Z        90     
        put("5B", "4A");   // [        91     
        put("5C", "E0");   // \        92     
        put("5D", "5A");   // ]        93     
        put("5E", "5F");   // ^        94     
        put("5F", "6D");   // _        95     
        put("60", "79");   // `        96     
        put("61", "81");   // a        97     
        put("62", "82");   // b        98     
        put("63", "83");   // c        99     
        put("64", "84");   // d        100    
        put("65", "85");   // e        101    
        put("66", "86");   // f        102    
        put("67", "87");   // g        103    
        put("68", "88");   // h        104    
        put("69", "89");   // i        105    
        put("6A", "91");   // j        106    
        put("6B", "92");   // k        107    
        put("6C", "93");   // l        108    
        put("6D", "94");   // m        109    
        put("6E", "95");   // n        110    
        put("6F", "96");   // o        111    
        put("70", "97");   // p        112    
        put("71", "98");   // q        113    
        put("72", "99");   // r        114    
        put("73", "A2");   // s        115    
        put("74", "A3");   // t        116    
        put("75", "A4");   // u        117    
        put("76", "A5");   // v        118    
        put("77", "A6");   // w        119    
        put("78", "A7");   // x        120    
        put("79", "A8");   // y        121    
        put("7A", "A9");   // z        122    
        put("7B", "C0");   // {        123    
        put("7C", "6A");   // |        124    
        put("7D", "D0");   // }        125    
        put("7E", "A1");   // ~        126    
        put("7F", "07");   //         127    
        put("80", "20");   // €        128    
        put("81", "21");   //          129    
        put("82", "22");   // ‚        130    
        put("83", "23");   // ƒ        131    
        put("84", "24");   // „        132    
        put("85", "15");   // …        133    
        put("86", "06");   // †        134    
        put("87", "17");   // ‡        135    
        put("88", "28");   // ˆ        136    
        put("89", "29");   // ‰        137    
        put("8A", "2A");   // Š        138    
        put("8B", "2B");   // ‹        139    
        put("8C", "2C");   // Œ        140    
        put("8D", "09");   //          141    
        put("8E", "0A");   // Ž        142    
        put("8F", "1B");   //          143    
        put("90", "30");   //          144    
        put("91", "31");   // ‘        145    
        put("92", "1A");   // ’        146    
        put("93", "33");   // “        147    
        put("94", "34");   // ”        148    
        put("95", "35");   // •        149    
        put("96", "36");   // –        150    
        put("97", "08");   // —        151    
        put("98", "38");   // ˜        152    
        put("99", "39");   // ™        153    
        put("9A", "3A");   // š        154    
        put("9B", "3B");   // ›        155    
        put("9C", "04");   // œ        156    
        put("9D", "14");   //          157    
        put("9E", "3E");   // ž        158    
        put("9F", "E1");   // Ÿ        159    
        put("A0", "41");   //          160    
        put("A1", "42");   // ¡        161    
        put("A2", "43");   // ¢        162    
        put("A3", "44");   // £        163    
        put("A4", "45");   // ¤        164    
        put("A5", "46");   // ¥        165    
        put("A6", "47");   // ¦        166    
        put("A7", "48");   // §        167    
        put("A8", "49");   // ¨        168    
        put("A9", "51");   // ©        169    
        put("AA", "52");   // ª        170    
        put("AB", "53");   // «        171    
        put("AC", "54");   // ¬        172    
        put("AD", "55");   // ­        173    
        put("AE", "56");   // ®        174    
        put("AF", "57");   // ¯        175    
        put("B0", "58");   // °        176    
        put("B1", "59");   // ±        177    
        put("B2", "62");   // ²        178    
        put("B3", "63");   // ³        179    
        put("B4", "64");   // ´        180    
        put("B5", "65");   // µ        181    
        put("B6", "66");   // ¶        182    
        put("B7", "67");   // ·        183    
        put("B8", "68");   // ¸        184    
        put("B9", "69");   // ¹        185    
        put("BA", "70");   // º        186    
        put("BB", "71");   // »        187    
        put("BC", "72");   // ¼        188    
        put("BD", "73");   // ½        189    
        put("BE", "74");   // ¾        190    
        put("BF", "75");   // ¿        191    
        put("C0", "76");   // À        192    
        put("C1", "77");   // Á        193    
        put("C2", "78");   // Â        194    
        put("C3", "80");   // Ã        195    
        put("C4", "8A");   // Ä        196    
        put("C5", "8B");   // Å        197    
        put("C6", "8C");   // Æ        198    
        put("C7", "8D");   // Ç        199    
        put("C8", "8E");   // È        200    
        put("C9", "8F");   // É        201    
        put("CA", "90");   // Ê        202    
        put("CB", "9A");   // Ë        203    
        put("CC", "9B");   // Ì        204    
        put("CD", "9C");   // Í        205    
        put("CE", "9D");   // Î        206    
        put("CF", "9E");   // Ï        207    
        put("D0", "9F");   // Ð        208    
        put("D1", "A0");   // Ñ        209    
        put("D2", "AA");   // Ò        210    
        put("D3", "AB");   // Ó        211    
        put("D4", "AC");   // Ô        212    
        put("D5", "AD");   // Õ        213    
        put("D6", "AE");   // Ö        214    
        put("D7", "AF");   // ×        215    
        put("D8", "B0");   // Ø        216    
        put("D9", "B1");   // Ù        217    
        put("DA", "B2");   // Ú        218    
        put("DB", "B3");   // Û        219    
        put("DC", "B4");   // Ü        220    
        put("DD", "B5");   // Ý        221    
        put("DE", "B6");   // Þ        222    
        put("DF", "B7");   // ß        223    
        put("E0", "B8");   // à        224    
        put("E1", "B9");   // á        225    
        put("E2", "BA");   // â        226    
        put("E3", "BB");   // ã        227    
        put("E4", "BC");   // ä        228    
        put("E5", "BD");   // å        229    
        put("E6", "BE");   // æ        230    
        put("E7", "BF");   // ç        231    
        put("E8", "CA");   // è        232    
        put("E9", "CB");   // é        233    
        put("EA", "CC");   // ê        234    
        put("EB", "CD");   // ë        235    
        put("EC", "CE");   // ì        236    
        put("ED", "CF");   // í        237    
        put("EE", "DA");   // î        238    
        put("EF", "DB");   // ï        239    
        put("F0", "DC");   // ð        240    
        put("F1", "DD");   // ñ        241    
        put("F2", "DE");   // ò        242    
        put("F3", "DF");   // ó        243    
        put("F4", "EA");   // ô        244    
        put("F5", "EB");   // õ        245    
        put("F6", "EC");   // ö        246    
        put("F7", "ED");   // ÷        247    
        put("F8", "EE");   // ø        248    
        put("F9", "EF");   // ù        249    
        put("FA", "FA");   // ú        250    
        put("FB", "FB");   // û        251    
        put("FC", "FC");   // ü        252    
        put("FD", "FD");   // ý        253    
        put("FE", "FE");   // þ        254    
        put("FF", "FF");   // ÿ        255    
      }
    };
    
  private static final HashMap EbcdicToAsciiMap = 
    new HashMap()
    {
      {
        //  ebcdic ascii          char    decimal
        put("00", "00");   // NULL      0     
        put("01", "01");   // SOH       1     
        put("02", "02");   // STX       2     
        put("03", "03");   // ETX       3     
        put("37", "04");   // EOT       4     
        put("2D", "05");   // ENQ       5     
        put("2E", "06");   // ACK       6     
        put("2F", "07");   // BEL       7     
        put("16", "08");   // BS        8     
        put("05", "09");   // HT        9     
        put("25", "0A");   // LF       10     
        put("0B", "0B");   // VT       11     
        put("0C", "0C");   // FF       12     
        put("0D", "0D");   // CR       13     
        put("0E", "0E");   // SO       14     
        put("0F", "0F");   // SI       15     
        put("10", "10");   // DLE      16     
        put("11", "11");   // DC1      17     
        put("12", "12");   // DC2      18     
        put("13", "13");   // DC3      19     
        put("3C", "14");   // DC4      20     
        put("3D", "15");   // NAK      21     
        put("32", "16");   // SYN      22     
        put("26", "17");   // ETB      23     
        put("18", "18");   // CAN      24     
        put("19", "19");   // EM       25     
        put("3F", "1A");   // SUB      26     
        put("27", "1B");   // ESC      27     
        put("1C", "1C");   // FS       28     
        put("1D", "1D");   // GS       29     
        put("1E", "1E");   // RS       30     
        put("1F", "1F");   // US       31     
        put("40", "20");   // SPACE    32     
        put("4F", "21");   // !        33     
        put("7F", "22");   // "        34     
        put("7B", "23");   // #        35     
        put("5B", "24");   // $        36     
        put("6C", "25");   // %        37     
        put("50", "26");   // &        38     
        put("7D", "27");   // '        39     
        put("4D", "28");   // (        40     
        put("5D", "29");   // )        41     
        put("5C", "2A");   // *        42     
        put("4E", "2B");   // +        43     
        put("6B", "2C");   // ,        44     
        put("60", "2D");   // -        45     
        put("4B", "2E");   // .        46     
        put("61", "2F");   // /        47     
        put("F0", "30");   // 0        48     
        put("F1", "31");   // 1        49     
        put("F2", "32");   // 2        50     
        put("F3", "33");   // 3        51     
        put("F4", "34");   // 4        52     
        put("F5", "35");   // 5        53     
        put("F6", "36");   // 6        54     
        put("F7", "37");   // 7        55     
        put("F8", "38");   // 8        56     
        put("F9", "39");   // 9        57     
        put("7A", "3A");   // :        58     
        put("5E", "3B");   // ;        59     
        put("4C", "3C");   // <        60     
        put("7E", "3D");   // =        61     
        put("6E", "3E");   // >        62     
        put("6F", "3F");   // ?        63     
        put("7C", "40");   // @        64     
        put("C1", "41");   // A        65     
        put("C2", "42");   // B        66     
        put("C3", "43");   // C        67     
        put("C4", "44");   // D        68     
        put("C5", "45");   // E        69     
        put("C6", "46");   // F        70     
        put("C7", "47");   // G        71     
        put("C8", "48");   // H        72     
        put("C9", "49");   // I        73     
        put("D1", "4A");   // J        74     
        put("D2", "4B");   // K        75     
        put("D3", "4C");   // L        76     
        put("D4", "4D");   // M        77     
        put("D5", "4E");   // N        78     
        put("D6", "4F");   // O        79     
        put("D7", "50");   // P        80     
        put("D8", "51");   // Q        81     
        put("D9", "52");   // R        82     
        put("E2", "53");   // S        83     
        put("E3", "54");   // T        84     
        put("E4", "55");   // U        85     
        put("E5", "56");   // V        86     
        put("E6", "57");   // W        87     
        put("E7", "58");   // X        88     
        put("E8", "59");   // Y        89     
        put("E9", "5A");   // Z        90     
        put("4A", "5B");   // [        91     
        put("E0", "5C");   // \        92     
        put("5A", "5D");   // ]        93     
        put("5F", "5E");   // ^        94     
        put("6D", "5F");   // _        95     
        put("79", "60");   // `        96     
        put("81", "61");   // a        97     
        put("82", "62");   // b        98     
        put("83", "63");   // c        99     
        put("84", "64");   // d        100    
        put("85", "65");   // e        101    
        put("86", "66");   // f        102    
        put("87", "67");   // g        103    
        put("88", "68");   // h        104    
        put("89", "69");   // i        105    
        put("91", "6A");   // j        106    
        put("92", "6B");   // k        107    
        put("93", "6C");   // l        108    
        put("94", "6D");   // m        109    
        put("95", "6E");   // n        110    
        put("96", "6F");   // o        111    
        put("97", "70");   // p        112    
        put("98", "71");   // q        113    
        put("99", "72");   // r        114    
        put("A2", "73");   // s        115    
        put("A3", "74");   // t        116    
        put("A4", "75");   // u        117    
        put("A5", "76");   // v        118    
        put("A6", "77");   // w        119    
        put("A7", "78");   // x        120    
        put("A8", "79");   // y        121    
        put("A9", "7A");   // z        122    
        put("C0", "7B");   // {        123    
        put("6A", "7C");   // |        124    
        put("D0", "7D");   // }        125    
        put("A1", "7E");   // ~        126    
        put("07", "7F");   //         127    
        put("20", "80");   // €        128    
        put("21", "81");   //          129    
        put("22", "82");   // ‚        130    
        put("23", "83");   // ƒ        131    
        put("24", "84");   // „        132    
        put("15", "85");   // …        133    
        put("06", "86");   // †        134    
        put("17", "87");   // ‡        135    
        put("28", "88");   // ˆ        136    
        put("29", "89");   // ‰        137    
        put("2A", "8A");   // Š        138    
        put("2B", "8B");   // ‹        139    
        put("2C", "8C");   // Œ        140    
        put("09", "8D");   //          141    
        put("0A", "8E");   // Ž        142    
        put("1B", "8F");   //          143    
        put("30", "90");   //          144    
        put("31", "91");   // ‘        145    
        put("1A", "92");   // ’        146    
        put("33", "93");   // “        147    
        put("34", "94");   // ”        148    
        put("35", "95");   // •        149    
        put("36", "96");   // –        150    
        put("08", "97");   // —        151    
        put("38", "98");   // ˜        152    
        put("39", "99");   // ™        153    
        put("3A", "9A");   // š        154    
        put("3B", "9B");   // ›        155    
        put("04", "9C");   // œ        156    
        put("14", "9D");   //          157    
        put("3E", "9E");   // ž        158    
        put("E1", "9F");   // Ÿ        159    
        put("41", "A0");   //          160    
        put("42", "A1");   // ¡        161    
        put("43", "A2");   // ¢        162    
        put("44", "A3");   // £        163    
        put("45", "A4");   // ¤        164    
        put("46", "A5");   // ¥        165    
        put("47", "A6");   // ¦        166    
        put("48", "A7");   // §        167    
        put("49", "A8");   // ¨        168    
        put("51", "A9");   // ©        169    
        put("52", "AA");   // ª        170    
        put("53", "AB");   // «        171    
        put("54", "AC");   // ¬        172    
        put("55", "AD");   // ­        173    
        put("56", "AE");   // ®        174    
        put("57", "AF");   // ¯        175    
        put("58", "B0");   // °        176    
        put("59", "B1");   // ±        177    
        put("62", "B2");   // ²        178    
        put("63", "B3");   // ³        179    
        put("64", "B4");   // ´        180    
        put("65", "B5");   // µ        181    
        put("66", "B6");   // ¶        182    
        put("67", "B7");   // ·        183    
        put("68", "B8");   // ¸        184    
        put("69", "B9");   // ¹        185    
        put("70", "BA");   // º        186    
        put("71", "BB");   // »        187    
        put("72", "BC");   // ¼        188    
        put("73", "BD");   // ½        189    
        put("74", "BE");   // ¾        190    
        put("75", "BF");   // ¿        191    
        put("76", "C0");   // À        192    
        put("77", "C1");   // Á        193    
        put("78", "C2");   // Â        194    
        put("80", "C3");   // Ã        195    
        put("8A", "C4");   // Ä        196    
        put("8B", "C5");   // Å        197    
        put("8C", "C6");   // Æ        198    
        put("8D", "C7");   // Ç        199    
        put("8E", "C8");   // È        200    
        put("8F", "C9");   // É        201    
        put("90", "CA");   // Ê        202    
        put("9A", "CB");   // Ë        203    
        put("9B", "CC");   // Ì        204    
        put("9C", "CD");   // Í        205    
        put("9D", "CE");   // Î        206    
        put("9E", "CF");   // Ï        207    
        put("9F", "D0");   // Ð        208    
        put("A0", "D1");   // Ñ        209    
        put("AA", "D2");   // Ò        210    
        put("AB", "D3");   // Ó        211    
        put("AC", "D4");   // Ô        212    
        put("AD", "D5");   // Õ        213    
        put("AE", "D6");   // Ö        214    
        put("AF", "D7");   // ×        215    
        put("B0", "D8");   // Ø        216    
        put("B1", "D9");   // Ù        217    
        put("B2", "DA");   // Ú        218    
        put("B3", "DB");   // Û        219    
        put("B4", "DC");   // Ü        220    
        put("B5", "DD");   // Ý        221    
        put("B6", "DE");   // Þ        222    
        put("B7", "DF");   // ß        223    
        put("B8", "E0");   // à        224    
        put("B9", "E1");   // á        225    
        put("BA", "E2");   // â        226    
        put("BB", "E3");   // ã        227    
        put("BC", "E4");   // ä        228    
        put("BD", "E5");   // å        229    
        put("BE", "E6");   // æ        230    
        put("BF", "E7");   // ç        231    
        put("CA", "E8");   // è        232    
        put("CB", "E9");   // é        233    
        put("CC", "EA");   // ê        234    
        put("CD", "EB");   // ë        235    
        put("CE", "EC");   // ì        236    
        put("CF", "ED");   // í        237    
        put("DA", "EE");   // î        238    
        put("DB", "EF");   // ï        239    
        put("DC", "F0");   // ð        240    
        put("DD", "F1");   // ñ        241    
        put("DE", "F2");   // ò        242    
        put("DF", "F3");   // ó        243    
        put("EA", "F4");   // ô        244    
        put("EB", "F5");   // õ        245    
        put("EC", "F6");   // ö        246    
        put("ED", "F7");   // ÷        247    
        put("EE", "F8");   // ø        248    
        put("EF", "F9");   // ù        249    
        put("FA", "FA");   // ú        250    
        put("FB", "FB");   // û        251    
        put("FC", "FC");   // ü        252    
        put("FD", "FD");   // ý        253    
        put("FE", "FE");   // þ        254    
        put("FF", "FF");   // ÿ        255    
      }
    };

  private AsciiToEbcdicConverter()
  {
  }
  
  public static String asciiToEbcdic( String asciiBytes )
    throws Exception
  {
    if ( (asciiBytes.length()%2) != 0 )
    {
      throw new Exception("byte buffer must have even length");
    }
  
    String ebcdicBytes = "";
    for( int i = 0; i < asciiBytes.length(); i += 2 )
    {
      ebcdicBytes += translateAsciiByte(asciiBytes.substring(i,i+2));
    }
    return( ebcdicBytes );
  }
  
  public static String ebcdicToAscii( String ebcdicBytes )
    throws Exception
  {
    if ( (ebcdicBytes.length()%2) != 0 )
    {
      throw new Exception("byte buffer must have even length");
    }
    
    String asciiBytes = "";
    for( int i = 0; i < ebcdicBytes.length(); i += 2 )
    {
      asciiBytes += translateEbcdicByte(ebcdicBytes.substring(i,i+2));
    }
    return( asciiBytes );
  }
  
  public static String translateAsciiByte( String asciiByte )
  {
    return( (String)AsciiToEbcdicMap.get(asciiByte) );
  }
  
  public static String translateEbcdicByte( String ebcdicByte )
  {
    return( (String)EbcdicToAsciiMap.get(ebcdicByte) );
  }
}  
