/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DropDownTable.java $

  Description:  
  
    DropDownTable object.
    
    Loads a list of value/description pairs from a table and allows
    iteration over the resulting lists.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-06 11:02:28 -0700 (Mon, 06 Aug 2007) $
  Version            : $Revision: 13944 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.SyncLog;

public class DropDownTable extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(DropDownTable.class);

  private List items = new Vector();
  private int curIdx;

  /**
   * addElement
   */

  public void addElement(DropDownItem item)
  {
    items.add(item);
  }
  public void addElement(String value, String description, String style)
  {
    addElement(new DropDownItem(value,description,style));
  }
  public void addElement(String value, String description)
  {
    addElement(value,description,null);
  }

  public void addElements(DropDownItem[] itemArray)
  {
    for (int i = 0; i < itemArray.length; ++i) addElement(itemArray[i]);
  }
  
  /**
   * Adds element (item) to the items list from a row of a result set.  
   * Result set needs to have at least  two fields, the value and description, 
   * and optionally may provide a style as a third field.  Fields are fetched 
   * as Strings and used to create a DropDownItem which is then added to the 
   * end of the items list.
   */
  public void addElement(ResultSet rs)
  {
    try
    {
      String value = rs.getString(1);
      String description = rs.getString(2);
      String style = null;
      try
      {
        style = rs.getString(3);
      }
      catch (Exception se) { }

      addElement(new DropDownItem(value,description,style));
    }
    catch (Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName(), 
        "addElement(rs): " + e.toString());
      log.error("Error loading item from result set: " + e);
      e.printStackTrace();
    }
  }
  
  public void removeAllElements()
  {
    try
    {
      // just clear the list of all entries
      items.clear();
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName(), "clearElements(): " + e.toString());
      log.error("Error clearing elements: " + e);
      e.printStackTrace();
    }
  }

  public DropDownItem getItem(int idx)
  {
    try
    {
      return (DropDownItem)items.get(idx);
    }
    catch (Exception e)
    {
      //log.warn("Attempted to fetch non-existent item with index " + idx 
      //  + " (of " + items.size() + " items)");
    }
    return null;
  }

  /**
   * curIdx
   */
  private boolean setCurIdxFirstEnabled(int newIdx)
  {
    DropDownItem item = getItem(newIdx);
    while (item != null && !item.isEnabled())
    {
      item = getItem(++newIdx);
    }
    if (item != null)
    {
      curIdx = newIdx;
      return true;
    }
    return false;
  }      

  public boolean setCurIdx(int newIdx)
  {
    return setCurIdxFirstEnabled(newIdx);
  }
  public int getCurIdx()
  {
    return curIdx;
  }

  /**
   * getCur*
   */
  public DropDownItem getCurItem()
  {
    return getItem(curIdx);
  }
  public String getCurrentValue()
  {
    DropDownItem item = getCurItem();
    return item != null ? item.getValue() : null;
  }
  public String getCurrentDescription()
  {
    DropDownItem item = getCurItem();
    return item != null ? item.getDescription() : null;
  }
  public String getCurrentStyle()
  {
    DropDownItem item = getCurItem();
    return item != null ? item.getStyle() : null;
  }
  
  /**
   * getFirst*
   */

  public DropDownItem getFirstItem()
  {
    setCurIdx(0);
    return getCurItem();
  }
  public String getFirstValue()
  {
    DropDownItem item = getFirstItem();
    return item != null ? item.getValue() : null;
  }
  public String getFirstDescription()
  {
    DropDownItem item = getFirstItem();
    return item != null ? item.getDescription() : null;
  }
  public String getFirstStyle()
  {
    DropDownItem item = getFirstItem();
    return item != null ? item.getStyle() : null;
  }
  
  /**
   * getNext*
   */
  public String getNextValue()
  {
    return setCurIdx(curIdx + 1) ? getCurrentValue() : null;
  }
  public String getNextDescription()
  {
    return setCurIdx(curIdx + 1) ? getCurrentDescription() : null;
  }
  public String getNextStyle()
  {
    return setCurIdx(curIdx + 1) ? getCurrentStyle() : null;
  }

  /**
   * Finds the first description matching the given value.  Returns null if
   * no match is found.
   */
  public String getDescription(String value)
  {
    try
    {
      for (Iterator i = items.iterator(); i.hasNext();)
      {
        DropDownItem item = (DropDownItem)i.next();
        if (item.isEnabled() && value.equals(item.getValue())) 
        {
          return item.getDescription();
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error fetching description for value '" + value + "': " + e);
      e.printStackTrace();
    }
    return null;
  }
  public String getDescription(int value)
  {
    return getDescription(Integer.toString(value));
  }
  public String getDescription(long value)
  {
    return getDescription(Long.toString(value));
  }
  
  public void initialize( Object[] params )
  {
  }

  public List getItems()
  {
    ArrayList enabledItems = new ArrayList();
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      DropDownItem item = (DropDownItem)i.next();
      if (item.isEnabled())
      {
        enabledItems.add(item);
      }
    }
    return enabledItems;
  }
}


