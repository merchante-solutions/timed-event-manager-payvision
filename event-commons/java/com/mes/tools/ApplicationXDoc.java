/*@lineinfo:filename=ApplicationXDoc*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/ApplicationXDoc.sqlj $

  Description:
  
  ApplicationXDoc
  
  Converts application database table data into an XML document and vice
  versa.  XML data is stored in xml_applications table.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/31/02 5:21p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tools;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.mes.net.XDoc;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class ApplicationXDoc extends XDoc
{
  private long appSeqNum;
  private long exportAppSeqNum;
  private boolean docLoaded = false;
  private ApplicationDescriptor appd = null;
  
  /*
  ** public ApplicationXDoc()
  **
  ** Constructor.
  **
  ** Default, sets up database connectivity with default settings.
  */
  public ApplicationXDoc()
  {
    super(false);
    initialize();
  }
  
  /*
  ** public ApplicationXDoc(String connectionString)
  **
  ** Constructor.
  **
  ** Allows specification of database connection string.
  */
  public ApplicationXDoc(String connectionString)
  {
    super(connectionString,false);
    initialize();
  }
  
  /*
  ** public class ColumnDescriptor
  **
  ** Contains the name and data type of a database column.
  */
  public class ColumnDescriptor
  {
    private String columnName;
    private int dataType;
    
    private static final int DT_NUMBER    = 0;
    private static final int DT_VARCHAR2  = 1;
    private static final int DT_CHAR      = 2;
    private static final int DT_DATE      = 3;

    public ColumnDescriptor(String columnName, String dataTypeStr)
    {
      this.columnName = columnName.toLowerCase();
      if (dataTypeStr.equals("DATE"))
      {
        dataType = DT_DATE;
      }
      else if (dataTypeStr.equals("VARCHAR2"))
      {
        dataType = DT_VARCHAR2;
      }
      else if (dataTypeStr.equals("CHAR"))
      {
        dataType = DT_CHAR;
      }
      else
      {
        dataType = DT_NUMBER;
      }
    }
    
    public String getColumnName()
    {
      return columnName;
    }
    public int getDataType()
    {
      return dataType;
    }
    
    public String getValue(String rawData)
    {
      String value = "";
      
      if (rawData.length() > 0)
      {
        switch (dataType)
        {
          case DT_VARCHAR2:
          case DT_CHAR:
            StringBuffer sbuf = new StringBuffer();
            for (int i = 0; i < rawData.length(); ++i)
            {
              if (rawData.charAt(i) == '\'')
              {
                sbuf.append("'");
              }
              sbuf.append(rawData.charAt(i));
            }
            value = "'" + sbuf.toString() + "'";
            break;

          case DT_DATE:
            value = "to_date('" + rawData + "','YYYY-MM-DD HH24:MI:SS')";
            break;
          
          case DT_NUMBER:
          default:
            value = rawData;
            break;
        }
      }
      
      return value;
    }
    
    public String getText(ResultSet rs)
    {
      String text = "";
      
      try
      {
        switch (dataType)
        {
          case DT_VARCHAR2:
          case DT_CHAR:
          case DT_NUMBER:
          default:
            text = rs.getString(columnName);
            break;

          case DT_DATE:
            java.util.Date date = rs.getTimestamp(columnName);
            if (date != null)
            {
              SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
              text = df.format(date);
            }
            break;
        }
      }
      catch (Exception e)
      {
        System.out.println("Exception in getText(): " + e.toString());
      }
      
      return text;
    }
  }
    
  /*
  ** public class TableDescriptor
  **
  ** Contains column descriptors for each column in a database table.
  */
  public class TableDescriptor extends Vector
  {
    private String tableName;
    private boolean isOneToMany;
    private ColumnDescriptor keyColumn;
    
    public TableDescriptor(String tableName, 
                           String keyName, 
                           boolean isOneToMany, 
                           DefaultContext Ctx)
      throws Exception
    {
      this.tableName = tableName;
      this.isOneToMany = isOneToMany;
      loadColumnDescriptors(keyName,Ctx);
    }
    
    private void loadColumnDescriptors(String keyName, DefaultContext Ctx)
      throws Exception
    {
      // load column names and types
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  column_name,
//                  data_type
//          from    user_tab_columns
//          where   table_name = :tableName.toUpperCase()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_25 = tableName.toUpperCase();
  try {
   String theSqlTS = "select  column_name,\n                data_type\n        from    user_tab_columns\n        where   table_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.ApplicationXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_25);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.ApplicationXDoc",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
      ResultSet rs = it.getResultSet();
      
      while (rs.next())
      {
        ColumnDescriptor cd = new ColumnDescriptor(rs.getString("column_name"),
                                                   rs.getString("data_type"));
        add(cd);
        if (cd.getColumnName().equals(keyName))
        {
          keyColumn = cd;
        }
      }
      
      rs.close();
      it.close();
    }
    
    public String getTableName()
    {
      return tableName;
    }
    public boolean getIsOneToMany()
    {
      return isOneToMany;
    }
    public ColumnDescriptor getKeyColumn()
    {
      return keyColumn;
    }
    
    public ColumnDescriptor getColumn(String columnName)
    {
      for (Iterator i = iterator(); i.hasNext();)
      {
        ColumnDescriptor cd = (ColumnDescriptor)i.next();
        if (cd.getColumnName().equals(columnName))
        {
          return cd;
        }
      }
      return null;
    }
  }
  
  /*
  ** public class ApplicationDescriptor
  **
  ** Contains a table descriptor for each database table that contains
  ** application data.
  */
  public class ApplicationDescriptor extends ArrayList
  {
    public ApplicationDescriptor(DefaultContext Ctx) throws Exception
    {
      add(new TableDescriptor("application",    "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("merchant",       "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("screen_progress","screen_pk",    true,   Ctx));
      add(new TableDescriptor("address",        "app_seq_num",  true,   Ctx));
      add(new TableDescriptor("businessowner",  "app_seq_num",  true,   Ctx));
      add(new TableDescriptor("merchbank",      "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("merchcontact",   "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("merchequipment", "app_seq_num",  true,   Ctx));
      add(new TableDescriptor("merchpayoption", "app_seq_num",  true,   Ctx));
      add(new TableDescriptor("merch_pos",      "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("miscchrg",       "app_seq_num",  true,   Ctx));
      add(new TableDescriptor("pos_features",   "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("siteinspection", "app_seq_num",  false,  Ctx));
      add(new TableDescriptor("tranchrg",       "app_seq_num",  true,   Ctx));
    }
    
    public TableDescriptor getTable(String tableName)
    {
      for (Iterator i = iterator(); i.hasNext();)
      {
        TableDescriptor td = (TableDescriptor)i.next();
        if (td.getTableName().equals(tableName))
        {
          return td;
        }
      }
      return null;
    }
  }
  
  /*
  ** private void initialize() throws Exception
  **
  ** Initialize application descriptor.
  */
  private void initialize()
  {
    try
    {
      connect();
      appd = new ApplicationDescriptor(Ctx);
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::initialize()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
  }
  
  /**************************************************************************
  **
  ** XML Import Routines
  **
  ** Deals with the loading of data from database tables and generating an
  ** XML document containing it.
  **
  ***************************************************************************
  
  /*
  ** private ResultSet getTableRows(TableDescriptor td)
  **
  ** Loads the rows of a database table that correspond with a key 
  ** into a ResultSet.  A query is generated dynamically from column
  ** names.
  **
  ** RETURNS: ResultSet containing the query results.
  */
  private ResultSet getTableRows(TableDescriptor td)
  {
    StringBuffer  qs = new StringBuffer();
    ResultSet     rs = null;
    
    try
    {
      // construct a query using the column names:
      // select each column in the table
      qs.append("select ");
      for (Iterator i = td.iterator(); i.hasNext();)
      {
        String columnName = ((ColumnDescriptor)i.next()).getColumnName();
        qs.append(columnName);
        qs.append(i.hasNext() ? ", " : " ");
      }
      
      // from the table
      qs.append("from ");
      qs.append(td.getTableName().toLowerCase() + " ");
      
      // where the key matches the key value
      qs.append("where ");
      qs.append(td.getKeyColumn().getColumnName() + " = " + appSeqNum);

      // execute query, get ResultSet
      PreparedStatement ps = con.prepareStatement(qs.toString());
      rs = ps.executeQuery();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::getTableRows()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      System.out.println("query: " + qs.toString());
      logEntry(func,desc);
    }
    
    return rs;
  }
  
  /*
  ** private Element getTableElement(TableDescriptor td)
  **
  ** Creates an element containing the data in the table indicated by the
  ** table descriptor and the key value.
  **
  ** XML will look like this for singe row tables:
  **
  **    <table_name>
  **      <table_column>column data</table_column>
  **      .
  **      .
  **    </table_name>
  **
  ** XML will look like this for multi-row tables:
  **
  **    <table_name>
  **      <row>
  **        <table_column>column data</table_column>
  **        .
  **        .
  **      </row>
  **      .
  **      .
  **    </table_name>
  **
  ** RETURNS: Element containing the table data elements.
  */
  private Element getTableElement(TableDescriptor td)
  {
    Element tableElement = new Element(td.getTableName().toLowerCase());
    
    try
    {
      // load the table data
      ResultSet rs = getTableRows(td);
      
      // add each row found to the parent
      while (rs.next())
      {
        Element rowElement = null;
        
        // one-to-many: create inner row element
        if (td.getIsOneToMany())
        {
          rowElement = new Element("row");
        }
        // one-to-one: use main table element as row element
        else
        {
          rowElement = tableElement;
        }
        
        // add the row's column data to the row element
        for (Iterator i = td.iterator(); i.hasNext();)
        {
          ColumnDescriptor cd = (ColumnDescriptor)i.next();
          
          // should encode some characters here to make it safe for xml
          String columnData = cd.getText(rs);
          
          Element columnElement = new Element(cd.getColumnName());
          if (columnData != null)
          {
            columnElement.setText(columnData);
          }
          rowElement.addContent(columnElement);
        }
        
        // add row element to table element
        // if row is inside table element
        if (tableElement != rowElement)
        {
          tableElement.addContent(rowElement);
        }
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::getTableElement()";
      String desc = "appSeqNum = " + appSeqNum + ", table = " 
        + td.getTableName() + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    
    return tableElement;
  }

  /*
  ** public void importXml(long appSeqNum)
  **
  ** Loads all data contained in the db tables described by the DBTables
  ** table descriptor array and places it into an XML document.
  */
  public void importXml(long appSeqNum)
  {
    try
    {
      // connect to database
      connect();
      
      // store appSeqNum
      this.appSeqNum = appSeqNum;
      
      // iterate through table descriptors, add each 
      // table's data to a xml root element
      Element root = new Element("app_data");
      for (Iterator i = appd.iterator(); i.hasNext(); )
      {
        root.addContent(getTableElement((TableDescriptor)i.next()));
      }

      // create a new xml document with the root element
      doc = new Document(root);
      
      // turn on the loaded flag
      docLoaded = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::importXml()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }
  
  /**************************************************************************
  **
  ** XML Export Routines
  **
  ** Routines to take xml application data and populate the originator 
  ** db tables with it.
  ** 
  **************************************************************************/
  
  /*
  ** private void exportRow(TableDescriptor td, Element rowElement)
  **
  ** Generates an sql statement to insert the contents of the rowElement
  ** into the table specified with the column names in the columns Vector
  ** and exectutes it to export the row to the database.
  */
  private void exportRow(TableDescriptor td, Element rowElement)
  {
    try
    {
      String tableName = td.getTableName();
      
      // iterate through columns to generate the name and value clauses
      StringBuffer nameClause = new StringBuffer("(");
      StringBuffer valueClause = new StringBuffer("(");
      int columnCount = 0;
      boolean priorColumn = false;
      for (Iterator i = td.iterator(); i.hasNext();)
      {
        // look for a child in the row element matching the column
        ColumnDescriptor cd = (ColumnDescriptor)i.next();
        String columnName = cd.getColumnName().toLowerCase();
        Element columnElement = rowElement.getChild(columnName);
        if (columnElement != null)
        {
          // derive a name and field value (substitue export
          // app seq num for app seq num)
          String name = columnName.toLowerCase();
          String value = (name.equals(td.getKeyColumn().getColumnName()) ? 
                          Long.toString(exportAppSeqNum) : 
                          cd.getValue(columnElement.getText()));
                          
          if (value.length() > 0)
          {
            if (priorColumn)
            {
              nameClause.append(",");
              valueClause.append(",");
            }
            nameClause.append("\n  " + name);
            valueClause.append("\n  " + value);
            ++columnCount;
            priorColumn = true;
          }
        }
      }

      // make sure at least one column of data was found to insert
      if (columnCount > 0)
      {
        // construct the insert statement
        nameClause.append("\n)\n");
        valueClause.append("\n)\n");
        String insertStatement = "insert into " + tableName.toLowerCase() 
          + "\n" + nameClause + "values\n" + valueClause;
          
        // insert the row data into the database
        PreparedStatement ps = con.prepareStatement(insertStatement);
        ps.executeQuery();
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::exportRow()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
  }

  /*
  ** private void exportTable(Element tableElement)
  **
  ** Exports the contents of a table element in the xml application data
  ** to it's corresponding table in the database.
  */
  private void exportTable(Element tableElement)
  {
    // get the table descriptor corresponding with this element
    TableDescriptor td = appd.getTable(tableElement.getName());
    
    try
    {
      // check for multi-row content
      List rowElements = tableElement.getChildren("row");
      if (rowElements.size() > 0)
      {
        for (Iterator i = rowElements.iterator(); i.hasNext();)
        {
          exportRow(td,(Element)i.next());
        }
      }
      // make sure there is content (single-row)
      else if (!tableElement.getChildren().isEmpty())
      {
        exportRow(td,tableElement);
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::exportTable()";
      String desc = "appSeqNum = " + appSeqNum + ", table = " 
        + td.getTableName() + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
  }

  /*
  ** public void exportXml(long exportAppSeqNum) throws Exception
  **
  ** Export xml data to originator tables.  The data records generated use
  ** the app seq num provided.
  **
  ** THROWS:  Exception if no xml document data loaded.
  */
  public void exportXml(long exportAppSeqNum) throws Exception
  {
    // make sure there's some xml data present to store
    if (!docLoaded)
    {
      throw new 
        Exception("Unable to export XML data: no application data loaded.");
    }
    
    try
    {
      connect();
      
      int existingCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:672^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :exportAppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.ApplicationXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,exportAppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   existingCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:678^7*/
      
      if(existingCount == 0)
      {
        // store the export app seq num
        this.exportAppSeqNum = exportAppSeqNum;
      
        // iterate through the table descriptors in order
        // and export each table to the databse
        for (Iterator i = appd.iterator(); i.hasNext();)
        {
          String tableName = ((TableDescriptor)i.next()).getTableName();
          Element tableElement = doc.getRootElement().getChild(tableName);
          if (tableElement != null)
          {
            exportTable(tableElement);
          }
        }

        /*@lineinfo:generated-code*//*@lineinfo:697^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:700^9*/
      }
      else
      {
        throw new Exception("UNABLE TO EXPORT XML APP - APP_SEQ_NUM EXISTS");
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::exportXml()";
      String desc = "appSeqNum = " + appSeqNum + ", exportAppSeqNum = " 
        + exportAppSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }
  
  /**************************************************************************
  **
  ** Clear Application Routines
  **
  ** Routine to clear out all table rows with the specified app seq num.
  **
  **************************************************************************/

  /*
  ** public void deleteApplication(long appSeqNum)
  **
  ** Delete all application table rows with the app seq num.
  */
  public void deleteApplication(long appSeqNum)
  {
    long tempAppSeqNum = this.appSeqNum;
    this.appSeqNum = appSeqNum;
    
    try
    {
      connect();

      // delete each table
      for (Iterator i = appd.iterator(); i.hasNext(); )
      {
        StringBuffer qs = new StringBuffer();
        TableDescriptor td = (TableDescriptor)i.next();
        qs.append("delete from " + td.getTableName() + "\n");
        qs.append("where " + td.getKeyColumn().getColumnName()
                  + " = " + appSeqNum);
        PreparedStatement ps = con.prepareStatement(qs.toString());
        ps.executeQuery();
      }

      /*@lineinfo:generated-code*//*@lineinfo:755^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:758^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::deleteTables()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
      this.appSeqNum = tempAppSeqNum;
    }
  }
  
  /**************************************************************************
  **
  ** Xml_applications Routines
  **
  ** Routines to access application data stored in the xml_applications
  ** table as compressed blob data.
  **
  **************************************************************************/
  
  /*
  ** public void storeXml() throws Exception
  **
  ** If an xml document has been loaded, this routine will take the xml data,
  ** render it as ascii data, compress it into a buffer and then store that
  ** data in the xml_applications table as a blob field.
  **
  ** THROWS:  Exception if no xml document data loaded.
  */
  public void storeXml() throws Exception
  {
    // make sure there's some xml data present to store
    if (!docLoaded)
    {
      throw new 
        Exception("Unable to store XML data: no application data loaded.");
    }
    
    try
    {
      connect();
      
      // load an xml_application sequence number
      long xaSeqNum;
      /*@lineinfo:generated-code*//*@lineinfo:807^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_app_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_app_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.ApplicationXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   xaSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:812^7*/

      // store the data as an xml_applications record
      // with an empty blob
      /*@lineinfo:generated-code*//*@lineinfo:816^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_applications
//          ( app_seq_num,
//            xa_seq_num,
//            xa_type,
//            xa_date,
//            xa_doc )
//          values
//          ( :appSeqNum,
//            :xaSeqNum,
//            0,
//            sysdate,
//            empty_blob() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_applications\n        ( app_seq_num,\n          xa_seq_num,\n          xa_type,\n          xa_date,\n          xa_doc )\n        values\n        (  :1 ,\n           :2 ,\n          0,\n          sysdate,\n          empty_blob() )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.ApplicationXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,xaSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:830^7*/

      // get a reference to the blob
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:834^7*/

//  ************************************************************
//  #sql [Ctx] { select  xa_doc 
//          from    xml_applications
//          where   app_seq_num = :appSeqNum
//                  and xa_seq_num = :xaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xa_doc  \n        from    xml_applications\n        where   app_seq_num =  :1 \n                and xa_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.ApplicationXDoc",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,xaSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:840^7*/

      // write the xml doc data to the blob
      DeflaterOutputStream dos = 
        new DeflaterOutputStream(b.getBinaryOutputStream());
      XMLOutputter fmt = new XMLOutputter();
      fmt.output(doc,dos);
      dos.flush();
      dos.close();
      
      // commit
      /*@lineinfo:generated-code*//*@lineinfo:851^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:854^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::storeXml()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** public boolean loadXml(long appSeqNum)
  **
  ** Loads the most recently created xml application record into the internal
  ** xml document.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean loadXml(long appSeqNum)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      connect();
      
      // turn off load flag until load completed
      docLoaded = false;
      doc = null;
      
      // store the app seq num
      this.appSeqNum = appSeqNum;
      
      // get a blob reference to the most recently stored xml document
      Blob b = null;
      long xaSeqNum = 0;
      /*@lineinfo:generated-code*//*@lineinfo:895^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  xa.xa_doc,
//                  xa.xa_seq_num
//          from    xml_applications xa,
//                  ( select  max(xa_seq_num) max_xsn
//                    from    xml_applications
//                    where   app_seq_num = :appSeqNum ) mx
//          where   xa.app_seq_num = :appSeqNum
//                  and xa.xa_seq_num = mx.max_xsn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xa.xa_doc,\n                xa.xa_seq_num\n        from    xml_applications xa,\n                ( select  max(xa_seq_num) max_xsn\n                  from    xml_applications\n                  where   app_seq_num =  :1  ) mx\n        where   xa.app_seq_num =  :2 \n                and xa.xa_seq_num = mx.max_xsn";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.ApplicationXDoc",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.tools.ApplicationXDoc",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:905^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        b = rs.getBlob(1);
        xaSeqNum = rs.getLong("xa_seq_num");
      }
      
      rs.close();
      it.close();
      
      // load the blob ref data as an xml document
      InflaterInputStream iis = new InflaterInputStream(b.getBinaryStream());
      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(iis);
      iis.close();
      
      docLoaded = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::loadXml()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    // return the load flag to indicate success or failure
    return docLoaded;
  }
  
  /**************************************************************************
  **
  ** XML Accessor Routines
  **
  ** Routines to allow access to the data in the XML document.
  **
  **************************************************************************/
  
  public class XMLFieldDescriptor
  {
    private String tableName = "";
    private String columnName = "";
    private int rowId = -1;
    
    public XMLFieldDescriptor(String tableName, String columnName, int rowId)
    {
      this.tableName = tableName;
      this.columnName = columnName;
      this.rowId = rowId;
    }
    public XMLFieldDescriptor(XMLFieldDescriptor fd)
    {
      this.tableName = fd.getTableName();
      this.columnName = fd.getColumnName();
      this.rowId = fd.getRowId();
    }
    
    public String getTableName()
    {
      return tableName;
    }
    public String getColumnName()
    {
      return columnName;
    }
    public int getRowId()
    {
      return rowId;
    }
  }
  
  /*
  ** public int getRowCount(String tableName) throws Exception
  **
  ** RETURNS: the number of rows in the table, 0 if no rows or not multi-row
  **          table.
  */
  public int getRowCount(String tableName) throws Exception
  {
    if (!docLoaded)
    {
      throw new 
        Exception("Unable to lookup field data: no application data loaded.");
    }
    
    // find the table
    Element table = doc.getRootElement().getChild(tableName);
    if (table == null)
    {
      throw new Exception("Invalid table name '" + tableName + "'.");
    }
      
    // return the number of row elements within the table
    return table.getChildren("row").size();
  }
  
  /*
  ** Field name parse states
  */
  private final static int PS_GET_TABLE_NAME        = 0;
  private final static int PS_GET_TABLE_NAME_DELIM  = 1;
  private final static int PS_GET_ROW_ID            = 2;
  private final static int PS_GET_ROW_ID_DELIM      = 3;
  private final static int PS_GET_COLUMN_NAME       = 4;
  private final static int PS_DONE                  = 5;
  private final static int PS_INVALID_FIELD_NAME    = 6;
  
  /*
  ** private XMLFieldDescriptor getXMLFieldDescriptor(String fieldName)
  **   throws Exception
  **
  ** Parses a field name and generates a field descriptor from it.
  **
  ** RETURNS: XMLFieldDescriptor corresponding with the field name.
  */
  private XMLFieldDescriptor getXMLFieldDescriptor(String fieldName)
    throws Exception
  {
    // field name consists of a table name, an optional row id and a column name
    String tableName = null;
    String columnName = null;
    int rowId = -1;
    
    // parse through the field name
    int state = 0;
    StringTokenizer toker = new StringTokenizer(fieldName,".[]",true);
    while (toker.hasMoreTokens())
    {
      String token = toker.nextToken();
      switch (state)
      {
        case PS_GET_TABLE_NAME:
          tableName = token.toLowerCase();
          state = PS_GET_TABLE_NAME_DELIM;
          break;
          
        case PS_GET_TABLE_NAME_DELIM:
          if (token.equals("."))
          {
            state = PS_GET_COLUMN_NAME;
          }
          else if (token.equals("["))
          {
            if (rowId > -1)
            {
              throw new Exception("Invalid field name '" + fieldName 
                + "', expecting '.' row id but got '"
                + token + "'.");
            }
            state = PS_GET_ROW_ID;
          }
          else
          {
            throw new Exception("Invalid field name '" + fieldName 
              + "', expecting '.' or '[' after column name but got '"
              + token + "'.");
          }
          break;
          
        case PS_GET_ROW_ID:
          try
          {
            rowId = Integer.parseInt(token);
          }
          catch (NumberFormatException e)
          {
            throw new Exception ("Invalid field name '" + fieldName 
              + "', row id must be an integer, was '" + token + "'.");
          }
          state = PS_GET_ROW_ID_DELIM;
          break;
            
        case PS_GET_ROW_ID_DELIM:
          if (!token.equals("]"))
          {
            throw new Exception("Invalid field name '" + fieldName 
              + "', expecting ']' after row id but got '" + token + "'.");
          }
          state = PS_GET_TABLE_NAME_DELIM;
          break;
            
        case PS_GET_COLUMN_NAME:
          columnName = token.toLowerCase();
          state = PS_DONE;
          break;
            
        case PS_DONE:
          throw new Exception("Invalid field name '" + fieldName 
            + "', expecting end of name but encountered '"
            + token + "'.");
        
        default:
          throw new Exception("Invalid field name '" + fieldName 
            + "', invalid parse state occured (" + state + ").");
      }
    }
    
    // if not in done state, error
    if (state != PS_DONE)
    {
      String errorString = "unknown error";
      switch (state)
      {
        case PS_GET_TABLE_NAME:
          errorString = "no table name given";
          break;

        case PS_GET_TABLE_NAME_DELIM:
          errorString = "expecting '.' or '[' after table name";
          break;
          
        case PS_GET_ROW_ID:
          errorString = "expecting row id";
          break;
            
        case PS_GET_ROW_ID_DELIM:
          errorString = "expecting ']' after row id";
          break;
            
        case PS_GET_COLUMN_NAME:
          errorString = "expecting column name";
          break;
      }
      throw new Exception("Invalid field name '" + fieldName
        + "', field name incomplete, " + errorString + ".");
    }
    
    return new XMLFieldDescriptor(tableName,columnName,rowId);
  }
  
  public class DocNotLoadedException extends Exception
  {
    public DocNotLoadedException(String m)
    {
      super(m);
    }
  }
  public class InvalidNameException extends Exception
  {
    public InvalidNameException(String m)
    {
      super(m);
    }
  }
  public class MissingRowException extends Exception
  {
    public MissingRowException(String m)
    {
      super(m);
    }
  }
  public class IllegalRowException extends Exception
  {
    public IllegalRowException(String m)
    {
      super(m);
    }
  }
  public class NullItemException extends Exception
  {
    public NullItemException(String m)
    {
      super(m);
    }
  }
  public class NullRowException extends NullItemException
  {
    public NullRowException(String m)
    {
      super(m);
    }
  }
  public class NullColumnException extends NullItemException
  {
    public NullColumnException(String m)
    {
      super(m);
    }
  }
  
  /*
  ** public Element getColumnContainer(XMLFieldDescriptor fd) throws Exception
  **
  ** Locates the containing element of a field described by a field
  ** descriptor.
  **
  ** RETURNS: the field's containing Element.
  */
  public Element getColumnContainer(XMLFieldDescriptor fd) throws Exception
  {
    if (!docLoaded)
    {
      throw new DocNotLoadedException("Unable to get column container '"
                                      + fd.toString() + "': "
                                      + "no application data loaded.");
    }
    
    // locate the table element
    Element container = doc.getRootElement().getChild(fd.getTableName());
    
    // if no table element found, error
    if (container == null)
    {
      throw new InvalidNameException("Invalid table name, table '"
        + fd.getTableName() + "' does not exist.");
    }
    
    // get rows if present
    List rows = container.getChildren("row");
    if (rows != null && rows.size() > 0)
    {
      // notice if no row id was given
      if (fd.getRowId() < 0)
      {
        throw new MissingRowException("Table '" + fd.getTableName()
          + "' is multi-row but no row id was given.");
      }
      
      try
      {
        container = (Element)rows.get(fd.getRowId());
      }
      catch (IndexOutOfBoundsException e)
      {
        throw new NullRowException("Row '" + fd.getRowId() 
          + "' in table '" + fd.getTableName() + "' is null.");
      }
    }
    // if no rows present but row id was given, error
    else if (fd.getRowId() >= 0)
    {
      if (appd.getTable(fd.getTableName()).getIsOneToMany())
      {
        throw new NullRowException("Row '" + fd.getRowId() 
          + "' in table '" + fd.getTableName() + "' is null.");
      }
      
      throw new IllegalRowException("Table '" + fd.getTableName() 
        + "' is not multi-row but row id (" + fd.getRowId() + ") was given.");
    }

    return container;    
  }  
  
  /*
  ** public Element getColumnElement(XMLFieldDescriptor fd) throws Exception
  **
  ** Given a field descriptor this routine retrieves the Element from the xml
  ** document indicated by the descriptor.
  **
  ** RETURNS: reference to the JDOM Element.
  */
  public Element getColumnElement(XMLFieldDescriptor fd) throws Exception
  {
    // locate the column's container
    Element container = getColumnContainer(fd);
    
    // get the column from the containing element
    Element column = container.getChild(fd.getColumnName());
    if (column == null)
    {
      ColumnDescriptor cd = 
        appd.getTable(fd.getTableName()).getColumn(fd.getColumnName());
      if (cd == null)
      {
        throw new InvalidNameException("Column '" + fd.getColumnName() 
        + "' does not exist in table '" + fd.getTableName() +"'.");
      }
      
      throw new NullColumnException("Column '" + fd.getColumnName() 
        + "' in table '" + fd.getTableName() + "' is null.");
    }
    
    // return the column data
    return column;
  }

  /*
  ** public String getColumnData(XMLFieldDescriptor fd) throws Exception
  **
  ** Given a field descriptor this routine retrieves the data from the xml
  ** document associated with the element indicated by the descriptor.
  **
  ** RETURNS: the element text as a String.
  */
  public String getColumnData(XMLFieldDescriptor fd) throws Exception
  {
    // return the column data
    return getColumnElement(fd).getText();
  }
  
  /*
  ** public String getString(String fieldName) throws Exception
  **
  ** Retrieves the data specified by the name.
  **
  ** Example names: application.app_user_id
  **                address[0].address_phone
  **
  ** RETURNS: String containing the data.
  **
  ** THROWS:  Exception - if invalid field name is given.
  */
  public String getString(String fieldName) throws Exception
  {
    return getColumnData(getXMLFieldDescriptor(fieldName));
  }
    
  /*
  ** public int getInt(String fieldName) throws Exception
  **
  ** RETURNS: field value as an int.
  */
  public int getInt(String fieldName) throws Exception
  {
    return Integer.parseInt(getString(fieldName));
  }

  /*
  ** public long getLong(String fieldName) throws Exception
  **
  ** RETURNS: field value as a long.
  */
  public long getLong(String fieldName) throws Exception
  {
    return Long.parseLong(getString(fieldName));
  }
  
  /*
  ** public long getFloat(String fieldName) throws Exception
  **
  ** RETURNS: field value as a float.
  */
  public float getFloat(String fieldName) throws Exception
  {
    return Float.parseFloat(getString(fieldName));
  }
  
  /*
  ** public long getDate(String fieldName) throws Exception
  **
  ** RETURNS: field value as a java.util.Date.
  */
  public java.util.Date getDate(String fieldName) throws Exception
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    ParsePosition pos = new ParsePosition(0);
    return sdf.parse(getString(fieldName),pos);
  }
  
  /*
  ** public String getString(String fieldName, 
  **                         String filterColumn, 
  **                         String filterValue)
  **   throws Exception
  **
  ** Example parameters: address.address_phone, addresstype_code, 2
  **
  **      This would request the address_phone field value from the address
  **      row with an addresstype_code of 2
  **
  ** Retrieves the field value requested from a multi-row table, the row
  ** retrieved from is determined by the filter column and value.
  **
  ** RETURNS: field value as a String, null if none found.
  */
  public String getString(String fieldName, 
                          String filterColumn,
                          String filterValue)
    throws Exception
  {
    // get the table and column names and the number of rows in the table
    XMLFieldDescriptor fd = getXMLFieldDescriptor(fieldName);
    String tableName = fd.getTableName();
    String columnName = fd.getColumnName();
    int rowCount = getRowCount(tableName);
    
    // scan the rows for a match
    int rowIdx = -1;
    for (int i = 0; i < rowCount; ++i)
    {
      XMLFieldDescriptor cd = new XMLFieldDescriptor(tableName,filterColumn,i);
      if (getColumnData(cd).equals(filterValue))
      {
        // return the value of the desired column 
        return getColumnData(new XMLFieldDescriptor(tableName,columnName,i));
      }
    }
    
    // no match found, return null
    return null;
  }
  
  /*
  ** public int getInt(String fieldName, 
  **                   String filterColumn,
  **                   String filterValue)
  **   throws Exception
  **
  ** RETURNS: field value as an int.
  */
  public int getInt(String fieldName, 
                    String filterColumn,
                    String filterValue)
    throws Exception
  {
    return Integer.parseInt(getString(fieldName,filterColumn,filterValue));
  }

  /*
  ** public long getLong(String fieldName, 
  **                     String filterColumn,
  **                     String filterValue)
  **   throws Exception
  **
  ** RETURNS: field value as a long.
  */
  public long getLong(String fieldName, 
                      String filterColumn,
                      String filterValue)
    throws Exception
  {
    return Long.parseLong(getString(fieldName,filterColumn,filterValue));
  }
  
  /*
  ** public Element addColumnElement(FieldDescriptor fd) throws Exception
  **
  ** Inserts an empty element described by the FieldDescriptor.
  **
  ** RETURNS: the new Element that was added.
  */
  public Element addColumnElement(XMLFieldDescriptor fd) throws Exception
  {
    Element column = null;
    
    // locate the column's container
    Element container;
    try
    {
      container = getColumnContainer(fd);
    }
    catch (NullRowException nre)
    {
      // row doesn't exist so create any nonexistent row elements
      // implied by the field descriptor
      int impliedSize = fd.getRowId() + 1;
      Element table = doc.getRootElement().getChild(fd.getTableName());
      Element row = null;
      while (table.getChildren().size() < impliedSize)
      {
        row = new Element("row");
        table.addContent(row);
      }
      
      // container will be the last row created
      container = row;
    }
    
    // see if column element already exists, if not then create it
    column = container.getChild(fd.getColumnName());
    if (column == null)
    {
      ColumnDescriptor cd = 
        appd.getTable(fd.getTableName()).getColumn(fd.getColumnName());
      if (cd == null)
      {
        throw new InvalidNameException("Column '" + fd.getColumnName() 
        + "' does not exist in table '" + fd.getTableName() +"'.");
      }
      
      // add the new column element
      column = new Element(fd.getColumnName());
      container.addContent(column);
    }
    
    // return the column data
    return column;
  }
  
  /*
  ** public void setColumnData(XMLFieldDescriptor fd, String data)
  **   throws Exception
  **
  ** Places the data in the field indicated by the field descriptor.
  */
  public void setColumnData(XMLFieldDescriptor fd, String data)
    throws Exception
  {
    try
    {
      getColumnElement(fd).setText(data);
    }
    catch (NullItemException nie)
    {
      addColumnElement(fd).setText(data);
    }
  }
  
  /*
  ** public void setData(String fieldName, String data) throws Exception
  ** public void setData(String fieldName, int    data) throws Exception
  ** public void setData(String fieldName, long   data) throws Exception
  ** public void setData(String fieldName, float  data) throws Exception
  ** public void setData(String fieldName, java.util.Date  data) throws Exception
  **
  ** Places the data into the field referred to by fieldName.
  */
  public void setData(String fieldName, String data) throws Exception
  {
    setColumnData(getXMLFieldDescriptor(fieldName),data);
  }
  public void setData(String fieldName, int data) throws Exception
  {
    setData(fieldName,Integer.toString(data));
  }
  public void setData(String fieldName, long data) throws Exception
  {
    setData(fieldName,Long.toString(data));
  }
  public void setData(String fieldName, float data) throws Exception
  {
    setData(fieldName,Float.toString(data));
  }
  public void setData(String fieldName, java.util.Date data) throws Exception
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    setData(fieldName,sdf.format(data));
  }

 public void generateEmptyApp(long appSeqNum)
 {
    // store appSeqNum
    this.appSeqNum = appSeqNum;
    
    // iterate through table descriptors, add an element
    // for each table
    Element root = new Element("app_data");
    for (Iterator i = appd.iterator(); i.hasNext(); )
    {
      TableDescriptor td = (TableDescriptor)i.next();
      Element table = new Element(td.getTableName().toLowerCase());
      root.addContent(table);
    }

    // create a new xml document with the root element
    doc = new Document(root);
    
    // turn on the loaded flag
    docLoaded = true;
  }
  
  /**************************************************************************
  **
  ** Static Convenience Routines
  **
  **************************************************************************/
  
  /*
  ** public static void exportXml(long appSeqNum, long exportAppSeqNum)
  **
  ** Static convenience function that will load xml from the xml_applications
  ** table and then export it to the specified export app seq num.
  */
  public static void exportXml(long appSeqNum, long exportAppSeqNum)
    throws Exception
  {
    ApplicationXDoc ax = new ApplicationXDoc();
    ax.loadXml(appSeqNum);
    ax.exportXml(exportAppSeqNum);
  }
  
  /*
  ** public static void storeXml(long appSeqNum)
  **
  ** Static convenience function that will import application data and then
  ** store it.
  */
  public static void storeXml(long appSeqNum)
    throws Exception
  {
    ApplicationXDoc ax = new ApplicationXDoc();
    ax.importXml(appSeqNum);
    ax.storeXml();
  }
  
  /*
  ** public static void deleteAppData(long appSeqNum)
  **
  ** Static convenience function that will clear all application table data
  ** associated with the given app seq num.
  */
  public static void deleteAppData(long appSeqNum)
  {
    ApplicationXDoc ax = new ApplicationXDoc();
    ax.deleteApplication(appSeqNum);
  }
  
  /*
  ** public ApplicationXDoc getEmptyInstance(long appSeqNum, 
  **                                         String connectionString)
  **
  ** Creates an empty instance (docLoaded == true) with appSeqNum.
  ** Takes a connection string is used.
  **
  ** RETURNS: empty ApplicationXDoc.
  */
  public static ApplicationXDoc getEmptyInstance(long appSeqNum,
                                          String connectionString)
  {
    ApplicationXDoc xDoc;
    if (connectionString != null)
    {
      xDoc = new ApplicationXDoc(connectionString);
    }
    else
    {
      xDoc = new ApplicationXDoc();
    }
    xDoc.generateEmptyApp(appSeqNum);
    return xDoc;
  }                                          
  public static ApplicationXDoc getEmptyInstance(long appSeqNum)
  {
    return getEmptyInstance(appSeqNum,null);
  }                                          
}/*@lineinfo:generated-code*/