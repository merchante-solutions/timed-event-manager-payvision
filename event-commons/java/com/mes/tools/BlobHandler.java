/*@lineinfo:filename=BlobHandler*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/BlobHandler.sqlj $

  Description:  BlobHandler.sqlj

  Designed to enable the saving and accessing of Blob-based documents in
  the database. Uses tables DOCUMENT and DOCUMENT_TYPE.


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-04-08 15:31:43 -0700 (Wed, 08 Apr 2015) $
  Version            : $Revision: 23559 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import oracle.sql.BLOB;

public class BlobHandler extends SQLJConnectionBase
{

  // create class log category
  static Logger log = Logger.getLogger(BlobHandler.class);

  /**
  Attributes
  **/
  private long generatedId = -1L;

  public long getGeneratedId(){
    return generatedId;
  }


  /**
   * insert - all methods are designed to take a file,
   * in any form, change it to a byte[] if needed,
   * and insert it, as a Blob, in the DB,
   * @param String fileName - needs to be fully qualified
   **/
  public boolean insert(String fileName)
  {
    try
    {
      File file = new File(fileName);
      return insert(file);
    }
    catch(Exception e)
    {
      return false;
    }
  }

  /**
   * insert - This allows you to rename the file when save to DB
   * @param String - fileName - to generate file
   * @param String - dbName - as saved in DB
   **/
  public boolean insert(String fileName, String dbName)
  {
    try
    {
      File file = new File(fileName);
      return insert(file, dbName);
    }
    catch(Exception e)
    {
      return false;
    }
  }

  /**
   * insert - all methods are designed to take a file,
   * in any form, change it to a byte[] if needed,
   * and insert it, as a Blob, in the DB,
   * @param File
   **/
  public boolean insert(File file)
  {
    return insert(file, file.getName());
  }

  /**
   * insert - This allows you to rename the file when save to DB
   * @param File - file to be saved
   * @param String - dbName - as saved in DB
   **/
  public boolean insert(File file, String dbName)
  {
    if(null==dbName || dbName.equals(""))
    {
      dbName = file.getName();
    }

    try
    {
      return insert(FileUtils.getBytesFromFile(file), dbName, 0);
    }
    catch (Exception ioe)
    {
      return false;
    }
  }

  public boolean insert(byte[] fileBody, String fileName)
  {
    return insert(fileBody, fileName, 0);
  }


  /**
   * insert - all methods are designed to take a file,
   * in any form, change it to a byte[] if needed,
   * and insert it, as a Blob, in the DB,
   * @param byte[] - no conversion needed
   * @param String - fileName for db
   **/
  public boolean insert(byte[] fileBody, String fileName, int fileType)
  {

    log.debug("start BlobHandler.insert...");

    //if there is no extension,
    //the file will be saved with none referenced
    if(fileType == 0)
    {
      fileType = FileUtils.getFileType(fileName);
    }
    boolean isOK = false;

    //always reset the ID
    generatedId = -1L;

    try
    {
      connect();
      setAutoCommit(false);

      /*@lineinfo:generated-code*//*@lineinfo:162^7*/

//  ************************************************************
//  #sql [Ctx] { select document_sequence.nextval  from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select document_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.BlobHandler",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   generatedId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^7*/


      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] { insert into document
//          (
//            doc_id,
//            doc_body,
//            doc_type_id,
//            doc_name
//          )
//          values
//          (
//            :generatedId,
//            empty_blob(),
//            :fileType,
//            :fileName
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into document\n        (\n          doc_id,\n          doc_body,\n          doc_type_id,\n          doc_name\n        )\n        values\n        (\n           :1 ,\n          empty_blob(),\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.BlobHandler",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generatedId);
   __sJT_st.setInt(2,fileType);
   __sJT_st.setString(3,fileName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^7*/

      // get a stream handler to the blob
      BLOB b;

      /*@lineinfo:generated-code*//*@lineinfo:189^7*/

//  ************************************************************
//  #sql [Ctx] { select  doc_body 
//          from    document
//          where   doc_id  = :generatedId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  doc_body  \n        from    document\n        where   doc_id  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.BlobHandler",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,generatedId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^7*/

      //deprecated
      //OutputStream bOut = b.getBinaryOutputStream();
      //Use, as of 9.0.2:
      OutputStream bOut = b.setBinaryStream(1L);

      // write the statement data to the blob
      bOut.write(fileBody,0,fileBody.length);
      bOut.flush();
      bOut.close();

      // commit
      /*@lineinfo:generated-code*//*@lineinfo:207^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/

      isOK = true;

    }
    catch (Exception e)
    {
      log.debug("insert() exception = "+e.getMessage());
      //reset id
      generatedId = -1;
    }
    finally
    {
      cleanUp();
    }

    log.debug("end BlobHandler.insert...");

    return isOK;

  }//insertBlob


  /**
   * getData - returns instance of BlobDocData, allowing
   * calling object to access raw byte[], extension to
   * handle/build data as needed
   * @param long - file id
   **/
  public BlobDocData getData(long id)
  {
    log.debug("start BlobHandler.getData...");
    byte[] fileBody = null;
    String ext = null;
    String fileName = null;
    String mimeType = null;
    int extInt = 0;

      // get applicable info (if present)
    try{

      connect();

      fileBody = getDataBody(id);

      /*@lineinfo:generated-code*//*@lineinfo:255^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            dt.extension,
//            dt.mimetype,
//            d.doc_name,
//            d.doc_type_id
//          
//          FROM
//            document_type dt
//            ,document d
//          WHERE
//            d.doc_id = :id
//          AND
//            d.doc_type_id = dt.doc_type_id(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          dt.extension,\n          dt.mimetype,\n          d.doc_name,\n          d.doc_type_id\n         \n        FROM\n          document_type dt\n          ,document d\n        WHERE\n          d.doc_id =  :1 \n        AND\n          d.doc_type_id = dt.doc_type_id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.BlobHandler",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ext = (String)__sJT_rs.getString(1);
   mimeType = (String)__sJT_rs.getString(2);
   fileName = (String)__sJT_rs.getString(3);
   extInt = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:274^7*/

    }
    catch (Exception e)
    {
      //do nothing - ext is null
    }
    finally
    {
      cleanUp();
    }

/*
    //check for encryption
    if(extInt == mesConstants.FILETYPE_ENC)
    {
      fileBody = decrypt(fileName, fileBody);
    }
*/
    log.debug("end BlobHandler.getData...");

    return new BlobDocData(id, ext, fileName, mimeType, fileBody);


  }//getBlob

  //Decryption
  //attempt to decrypt - if fails return original after logging
  //filename and error
/*
  private byte[] decrypt(String fileName, byte[] dirty)
  {
    try
    {
      EncryptionClient client = new masthead.encrypt.EncryptionClient("10.1.4.12", 9000);
      byte[] decryptedData = client.decrypt(dirty);
      return decryptedData;
    }
    catch (Exception e)
    {
      logEntry("decrypt(" + fileName + ")", e.toString());
      return dirty;
    }
  }
*/
  public byte[] getDataBody(long id)
  {
    byte[] fileBody = null;

    try
    {
      connect();

      BLOB b;

      /*@lineinfo:generated-code*//*@lineinfo:329^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            doc_body
//          
//          FROM
//            document
//          WHERE
//            doc_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          doc_body\n         \n        FROM\n          document\n        WHERE\n          doc_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.BlobHandler",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^7*/

      // use the blob ref to read the blob data into stream
      int dataLen = (int)b.length();
      int chunkSize = 0;
      int dataRead = 0;
      InputStream is = b.getBinaryStream();
      fileBody = new byte[dataLen];
      while (chunkSize != -1)
      {
        chunkSize = is.read(fileBody,dataRead,dataLen - dataRead);
        dataRead += chunkSize;
      }
      is.close();
    }
    catch (Exception e1)
    {
      log.debug("getDataBody() exception = "+e1.getMessage());
      //fileBody = null;
    }
    finally
    {
      cleanUp();
    }

    return fileBody;
  }

  public class BlobDocData extends DocData
  {
    private byte[] body;
    private BlobHandler bHandler;

    public BlobDocData(long id, String ext, String fileName, String mimeType, byte[] body)
    {
      super(id, ext, fileName, mimeType);
      this.body=body;
      bHandler = null;
    }

    public byte[] getBody()
    {
      if(null==body)
      {
        if(bHandler==null){
          bHandler = new BlobHandler();
        }
         body = bHandler.getDataBody(id);
      }
      return body;
    }

  }

}/*@lineinfo:generated-code*/