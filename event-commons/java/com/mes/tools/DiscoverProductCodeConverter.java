/*@lineinfo:filename=DiscoverProductCodeConverter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/DiscoverProductCodeConverter.sqlj $

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2012-03-09 12:49:22 -0800 (Fri, 09 Mar 2012) $
  Version            : $Revision: 19945 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011, 2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.HashMap;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DiscoverProductCodeConverter extends SQLJConnectionBase
{
  protected static final long       REFRESH_TIME_SECOND = 1000L;
  protected static final long       REFRESH_TIME_MINUTE = 60 * REFRESH_TIME_SECOND;
  protected static final long       REFRESH_TIME_HOUR   = 60 * REFRESH_TIME_MINUTE;
  
  protected static final long       REFRESH_TIME        = 24 * REFRESH_TIME_HOUR;
  
  protected static final String[][] ProductCodeDefaults = 
  {
    { "001" ,"B"  },
    { "002" ,"G"  },
    { "003" ,"H"  },
    { "004" ,"G2" },
    { "005" ,"J1" },
    { "006" ,"J1" },
    { "007" ,"C"  },
    { "008" ,"A"  },
    { "009" ,"Q"  },
    { "010" ,"G1" },
    { "011" ,"D"  },
    { "B"   ,"G"  },
    { "E"   ,"G1" },
    { "C"   ,"A"  },
    { "R"   ,"B"  },
    { "P"   ,"C"  },
    { "Q"   ,"D"  },
    { "Z"   ,"DI" },
  };
  
  // singleton
  private static DiscoverProductCodeConverter   TheDiscoverProductCodeConverter  = null;
  
  // instance variables
  private   HashMap     ProductCodeMap    = new HashMap();
  private   long        LoadTimeMillis    = 0L;
  
  private DiscoverProductCodeConverter()
  {
    loadDataDefault();
  }
  
  public String decodeVisaProductCode( String dsProdCode )
  {
    String        retVal  = null;
    
    try
    {
      if ( (retVal = (String)ProductCodeMap.get(dsProdCode)) == null )
      {
        retVal = "DI";
      }
    }
    catch( Exception e )
    {
      logEntry("decodeVisaProductCode(" + dsProdCode + ")",e.toString());
    }
    return( retVal );
  }
  
  public static DiscoverProductCodeConverter getInstance()
  {
    if ( TheDiscoverProductCodeConverter == null )
    {
      TheDiscoverProductCodeConverter = new DiscoverProductCodeConverter();
    }
    TheDiscoverProductCodeConverter.refresh();
    return( TheDiscoverProductCodeConverter );
  }
  
  protected synchronized void load( )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:108^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pd.product_code             as ds_product_code,
//                  pd.visa_product_code        as vs_product_code
//          from    tc33_auth_ds_prod_code_desc pd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pd.product_code             as ds_product_code,\n                pd.visa_product_code        as vs_product_code\n        from    tc33_auth_ds_prod_code_desc pd";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.DiscoverProductCodeConverter",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.DiscoverProductCodeConverter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        ProductCodeMap.clear();
        do
        {
          ProductCodeMap.put( resultSet.getString("ds_product_code"),
                              resultSet.getString("vs_product_code") );  
        }
        while( resultSet.next() );
      }
      resultSet.close();
      
      LoadTimeMillis = System.currentTimeMillis();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadDataDefault()
  {
    ProductCodeMap.clear();
    
    for( int i = 0; i < ProductCodeDefaults.length; ++i )
    {
      ProductCodeMap.put( ProductCodeDefaults[i][0],
                          ProductCodeDefaults[i][1] );  
    }
  }
  
  // synchronize the refresh method so two requests (threads)
  // do not trigger a back-to-back reload of the cached data
  protected synchronized void refresh()
  {
    // only load once at startup
    if ( LoadTimeMillis == 0L )
    {
      load();
    }
  }
}/*@lineinfo:generated-code*/