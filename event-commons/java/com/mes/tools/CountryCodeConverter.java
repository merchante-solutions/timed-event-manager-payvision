/*@lineinfo:filename=CountryCodeConverter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/tools/CountryCodeConverter.sqlj $

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-04-16 11:26:46 -0700 (Thu, 16 Apr 2015) $
  Version            : $Revision: 23574 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009, 2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class CountryCodeConverter extends SQLJConnectionBase
{
  protected static final long       REFRESH_TIME_SECOND = 1000L;
  protected static final long       REFRESH_TIME_MINUTE = 60 * REFRESH_TIME_SECOND;
  protected static final long       REFRESH_TIME_HOUR   = 60 * REFRESH_TIME_MINUTE;
  
  protected static final long       REFRESH_TIME        = 24 * REFRESH_TIME_HOUR;
  
  protected static final String[][] CountryCodeDefaults = 
  {
    { "AF", "004", "AFGHANISTAN",           "AFA" },
    { "AL", "008", "ALBANIA",               "ALL" },
    { "DZ", "012", "ALGERIA",               "DZD" },
    { "AS", "016", "AMERICAN SAMOA",        "USD" },
    { "AD", "020", "ANDORRA",               "EUR" },
    { "AO", "024", "ANGOLA",                "AOK" },
    { "AI", "660", "ANGUILLA",              "XCD" },
    { "AA", "",    "ANTARCTICA",            ""    },
    { "AG", "028", "ANTIGUA",               "XCD" },
    { "AR", "032", "ARGENTINA",             "ARA" },
    { "AM", "051", "ARMENIA",               "AMD" },
    { "AW", "533", "ARUBA",                 "AWG" },
    { "AU", "036", "AUSTRALIA",             "AUD" },
    { "AT", "040", "AUSTRIA",               "EUR" },
    { "AZ", "031", "AZERBAIJAN",            "AZM" },
    { "BS", "044", "BAHAMAS,THE",           "BSD" },
    { "BH", "048", "BAHRAIN",               "BHD" },
    { "BD", "050", "BANGLADESH",            "BDT" },
    { "BB", "052", "BARBADOS",              "BBD" },
    { "BY", "112", "BELARUS",               "BYR" },
    { "BE", "056", "BELGIUM",               "EUR" },
    { "BZ", "084", "BELIZE",                "BZD" },
    { "BJ", "204", "BENIN",                 "XAF" },
    { "BM", "060", "BERMUDA",               "BMD" },
    { "BT", "064", "BHUTAN",                "BTN" },
    { "BO", "068", "BOLIVIA",               "BOB" },
    { "BA", "070", "BOSNIA-HERZEGOVINA",    "BAM" },
    { "BW", "072", "BOTSWANA",              "BWP" },
    { "BV", "074", "BOUVET ISLAND",         "NOK" },
    { "BR", "076", "BRAZIL",                "BRC" },
    { "IO", "086", "BRITISH INDIAN OCEAN",  "GBP" },
    { "VG", "092", "BRITISH VIRGIN IS",     "GBP" },
    { "BN", "096", "BRUNEI",                "BND" },
    { "BG", "100", "BULGARIA",              "BGN" },
    { "BF", "854", "BURKINA FASO",          "XAF" },
    { "MM", "104", "BURMA",                 "MMK" },
    { "BI", "108", "BURUNDI",               "BIF" },
    { "KH", "116", "CAMBODIA",              "KHR" },
    { "CM", "120", "CAMEROON",              "XAF" },
    { "CA", "124", "CANADA",                "CAD" },
    { "CE", "",    "CANAL ZONE",            ""    },
    { "CV", "132", "CAPE VERDE,REPUBLIC",   "CVE" },
    { "KY", "136", "CAYMAN ISLANDS",        "KYD" },
    { "CF", "140", "CENTRAL AFRICAN EMP",   "XAF" },
    { "TD", "148", "CHAD",                  "XAF" },
    { "CD", "",    "CHANNEL ISLANDS",       "CDZ" },
    { "CL", "152", "CHILE",                 "CLP" },
    { "CN", "156", "CHINA (PRC)",           "CNY" },
    { "CX", "162", "CHRISTMAS ISLAND",      "AUD" },
    { "CC", "166", "COCOS (KEELING) IS",    "AUD" },
    { "CO", "170", "COLOMBIA",              "COP" },
    { "KM", "174", "COMOROS",               "KMF" },
    { "CG", "178", "CONGO",                 "XAF" },
    { "CK", "184", "COOK ISLANDS",          "NZD" },
    { "CR", "188", "COSTA RICA",            "CRC" },
    { "HR", "191", "CROATIA",               "HRD" },
    { "CU", "192", "CUBA",                  "CUP" },
    { "CY", "196", "CYPRUS",                "CYP" },
    { "CZ", "203", "CZECH REPUBLIC",        "CZK" },
    { "DK", "208", "DENMARK",               "DKK" },
    { "DJ", "262", "DJIBOUTI",              "DJF" },
    { "DM", "212", "DOMINICA",              "XCD" },
    { "DO", "214", "DOMINICAN REPUBLIC",    "DOP" },
    { "TP", "",    "EAST TIMOR",            "TPE" },
    { "EC", "218", "ECUADOR",               "ECS" },
    { "EG", "818", "EGYPT",                 "EGP" },
    { "SV", "222", "EL SALVADOR",           "USD" },
    { "GQ", "226", "EQUATORIAL GUINEA",     "GQE" },
    { "ER", "232", "ERITREA",               "ERN" },
    { "EE", "233", "ESTONIA",               "EEK" },
    { "ET", "210", "ETHIOPIA",              "ETB" },
    { "FK", "238", "FALKLAND ISLANDS",      "FKP" },
    { "FO", "234", "FAROE ISLANDS",         "DKK" },
    { "FJ", "242", "FIJI",                  "FJD" },
    { "FI", "246", "FINLAND",               "EUR" },
    { "FR", "250", "FRANCE",                "EUR" },
    { "GF", "254", "FRENCH GUIANA",         "EUR" },
    { "PF", "258", "FRENCH POLYNESIA",      "XPF" },
    { "TF", "260", "FRENCH SOUTHERN",       "EUR" },
    { "GA", "266", "GABON",                 "XAF" },
    { "GM", "270", "GAMBIA,THE",            "GMD" },
    { "GZ", "",    "GAZA STRIP",            ""    },
    { "GE", "268", "GEORGIA",               "GEL" },
    { "DE", "276", "GERMANY",               "EUR" },
    { "GH", "288", "GHANA",                 "GHC" },
    { "GI", "292", "GIBRALTAR",             "GIP" },
    { "GR", "300", "GREECE",                "EUR" },
    { "GL", "304", "GREENLAND",             "DKK" },
    { "GD", "308", "GRENADA",               "XCD" },
    { "GP", "312", "GUADELOUPE",            "EUR" },
    { "GU", "316", "GUAM",                  "USD" },
    { "GT", "320", "GUATEMALA",             "GTQ" },
    { "GN", "324", "GUINEA",                "GNS" },
    { "GW", "624", "GUINEA-BISSAU",         "GWP" },
    { "GY", "328", "GUYANA",                "GYD" },
    { "HT", "332", "HAITI",                 "HTG" },
    { "HM", "334", "HEARD & MCDONALD,IS",   "AUD" },
    { "HN", "340", "HONDURAS",              "HNL" },
    { "HK", "344", "HONG KONG",             "HKD" },
    { "HU", "348", "HUNGARY",               "HUF" },
    { "IS", "352", "ICELAND",               "ISK" },
    { "IN", "356", "INDIA",                 "INR" },
    { "ID", "360", "INDONESIA",             "IDR" },
    { "IR", "364", "IRAN",                  "IRR" },
    { "IQ", "368", "IRAQ",                  "IQD" },
    { "IE", "372", "IRELAND",               "EUR" },
    { "IL", "376", "ISRAEL",                "ILS" },
    { "IT", "380", "ITALY",                 "EUR" },
    { "CI", "384", "IVORY COAST",           "XAF" },
    { "JM", "388", "JAMAICA",               "JMD" },
    { "JP", "392", "JAPAN",                 "JPY" },
    { "JA", "",    "JOHNSTON ATOLL",        ""    },
    { "JO", "400", "JORDAN",                "JOD" },
    { "KE", "404", "KENYA",                 "KES" },
    { "KZ", "398", "KHAZAKSTAN",            "KZT" },
    { "KI", "296", "KIRIBATI",              "AUD" },
    { "KP", "408", "KOREA,P. DEM. REP.",    "KPW" },
    { "KR", "410", "KOREA,REPUBLIC OF",     "KRW" },
    { "KW", "414", "KUWAIT",                "KWD" },
    { "KG", "417", "KYRGYZSTAN",            "KGS" },
    { "LA", "418", "LAOS",                  "LAK" },
    { "LV", "428", "LATVIA",                "EUR" },
    { "LB", "422", "LEBANON",               "LBP" },
    { "LS", "426", "LESOTHO",               "LSL" },
    { "LR", "430", "LIBERIA",               "LRD" },
    { "LY", "434", "LIBYA",                 "LYD" },
    { "LI", "438", "LIECHTENSTEIN",         "CHF" },
    { "LT", "440", "LITHUANIA",             "EUR" },
    { "LU", "442", "LUXEMBOURG",            "EUR" },
    { "MO", "446", "MACAO",                 "MOP" },
    { "MK", "807", "MACEDONIA",             "MKD" },
    { "MG", "450", "MADAGASCAR",            "MGF" },
    { "MW", "454", "MALAWI",                "MWK" },
    { "MY", "458", "MALAYSIA",              "MYR" },
    { "MV", "462", "MALDIVES",              "MVR" },
    { "ML", "466", "MALI",                  "MLF" },
    { "MT", "470", "MALTA",                 "MTL" },
    { "MH", "584", "MARSHALL ISLANDS",      "USD" },
    { "MQ", "474", "MARTINIQUE",            "EUR" },
    { "MR", "478", "MAURITANIA",            "MRO" },
    { "MU", "480", "MAURITIUS",             "MUR" },
    { "MX", "484", "MEXICO",                "MXN" },
    { "FM", "583", "MICRONESIA,FED STATES", "USD" },
    { "MI", "",    "MIDWAY ISLANDS",        ""    },
    { "MD", "498", "MOLDOVA",               "MDL" },
    { "MC", "492", "MONACO",                "EUR" },
    { "MN", "496", "MONGOLIA",              "MNT" },
    { "ME", "",    "MONTENEGRO",            "EUR" },
    { "MS", "500", "MONTSERRAT",            "XCD" },
    { "MA", "504", "MOROCCO",               "MAD" },
    { "MZ", "508", "MOZAMBIQUE",            "MZM" },
    { "NA", "516", "NAMIBIA",               "NAD" },
    { "NR", "520", "NAURU",                 "AUD" },
    { "NV", "",    "NAVASSA ISLAND",        ""    },
    { "NP", "524", "NEPAL",                 "NPR" },
    { "NL", "528", "NETHERLANDS",           "EUR" },
    { "AN", "530", "NETHERLANDS ANTILLES",  "ANT" },
    { "CW", "531", "CURACAO",  "ANG" },
    { "SX", "534", "SINT MAARTEN",  "ANG" },
    { "BQ", "535", "BONAIRE, SINT EUSTATIUS, SABA",  "USD" },
    { "NC", "540", "NEW CALEDONIA",         "XPF" },
    { "NZ", "554", "NEW ZEALAND",           "NZD" },
    { "NI", "558", "NICARAGUA",             "NIC" },
    { "NE", "562", "NIGER",                 "XAF" },
    { "NG", "566", "NIGERIA",               "NGN" },
    { "NU", "570", "NIUE",                  "NZD" },
    { "NF", "574", "NORFOLK ISLAND",        "AUD" },
    { "NM", "",    "NORTHERN MARIANA IS",   ""    },
    { "NO", "578", "NORWAY",                "NOK" },
    { "OM", "512", "OMAN",                  "OMR" },
    { "PK", "586", "PAKISTAN",              "PKR" },
    { "PW", "585", "PALAU",                 "USD" },
    { "PA", "591", "PANAMA",                "PAB" },
    { "PG", "598", "PAPUA NEW GUINEA",      "PGK" },
    { "PI", "",    "PARACEL ISLANDS",       ""    },
    { "PY", "600", "PARAGUAY",              "PYG" },
    { "PE", "604", "PERU",                  "PEI" },
    { "PH", "608", "PHILIPPINES",           "PHP" },
    { "PN", "612", "PITCAIRN ISLANDS",      "NZD" },
    { "PL", "616", "POLAND",                "PLN" },
    { "PT", "620", "PORTUGAL",              "EUR" },
    { "PR", "630", "PUERTO RICO",           "USD" },
    { "QA", "634", "QATAR",                 "QAR" },
    { "RE", "638", "REUNION",               "EUR" },
    { "RO", "642", "ROMANIA",               "RON" },
    { "RU", "643", "RUSSIA",                "RUB" },
    { "RW", "646", "RWANDA",                "RWF" },
    { "SM", "674", "SAN MARINO",            "EUR" },
    { "ST", "678", "SAO TOME & PRINCIPE",   "STD" },
    { "SA", "682", "SAUDI ARABIA",          "SAR" },
    { "SN", "686", "SENEGAL",               "XAF" },
    { "SS", "",    "SERBIA",                ""    },
    { "SC", "690", "SEYCHELLES",            "SCR" },
    { "SL", "694", "SIERRA LEONE",          "SLL" },
    { "SG", "702", "SINGAPORE",             "SGD" },
    { "SK", "703", "SLOVAK REPUBLIC",       "SKK" },
    { "SI", "705", "SLOVENIA",              "EUR" },
    { "SB", "90 ", "SOLOMON ISLANDS",       "SBD" },
    { "SO", "706", "SOMALIA",               "SOS" },
    { "ZA", "710", "SOUTH AFRICA",          "ZAL" },
    { "ES", "724", "SPAIN",                 "EUR" },
    { "SP", "",    "SPRATLY ISLANDS",       ""    },
    { "LK", "144", "SRI LANKA",             "LKR" },
    { "KN", "659", "ST CHRISTOPER-NEVIS",   "XCD" },
    { "SH", "654", "ST HELENA",             "SHP" },
    { "PM", "666", "ST PIERRE & MIQUELON",  "EUR" },
    { "LC", "662", "ST. LUCIA",             "XCD" },
    { "VC", "670", "ST. VINCENT",           "XCD" },
    { "SD", "736", "SUDAN",                 "SDG" },
    { "SR", "740", "SURINAM",               "SRG" },
    { "SJ", "744", "SVALBARD & JAN MAYEN",  "NOK" },
    { "SZ", "748", "SWAZILAND",             "SZL" },
    { "SE", "752", "SWEDEN",                "SEK" },
    { "CH", "756", "SWITZERLAND",           "CHF" },
    { "SY", "760", "SYRIA",                 "SYP" },
    { "TW", "158", "TAIWAN",                "TWD" },
    { "TJ", "762", "TAJIKISTAN",            "TJR" },
    { "TZ", "834", "TANZANIA,UNITED REP",   "TZS" },
    { "TH", "764", "THAILAND",              "THB" },
    { "TG", "768", "TOGO",                  "XAF" },
    { "TK", "772", "TOKELAU",               "NZD" },
    { "TO", "776", "TONGA",                 "TOP" },
    { "TT", "780", "TRINIDAD AND TOBAGO",   "TTD" },
    { "TN", "788", "TUNISIA",               "TND" },
    { "TR", "792", "TURKEY",                "TRL" },
    { "TM", "795", "TURKMENISTAN",          "TMM" },
    { "TC", "796", "TURKS AND CAICOS IS",   "USD" },
    { "TV", "798", "TUVALU",                "AUD" },
    { "UG", "800", "UGANDA",                "UGS" },
    { "UA", "804", "UKRAINE",               "UAH" },
    { "AE", "784", "UNITED ARAB EMIRATES",  "AED" },
    { "GB", "826", "UNITED KINGDOM",        "GBP" },
    { "XX", "",    "-- unlisted --",        ""    },
    { "UY", "858", "URUGUAY",               "UYU" },
    { "UZ", "860", "UZBEKISTAN",            "UZS" },
    { "VU", "548", "VANUATU",               "VUV" },
    { "VA", "336", "VATICAN CITY",          "EUR" },
    { "VE", "862", "VENEZUELA",             "VEB" },
    { "VN", "704", "VIETNAM",               "VND" },
    { "VI", "850", "VIRGIN ISLANDS OF US",  "USD" },
    { "WA", "",    "WAKE ISLAND",           ""    },
    { "WF", "876", "WALLIS AND FUTUNA",     "XPF" },
    { "WE", "",    "WEST BANK",             ""    },
    { "EH", "732", "WESTERN SAHARA",        "MAD" },
    { "WS", "882", "WESTERN SAMOA",         "WST" },
    { "YE", "887", "YEMEN,P. DEM. REP.",    "YER" },
    { "YU", "891", "YUGOSLAVIA",            "YUD" },
    { "ZM", "894", "ZAMBIA",                "ZMK" },
    { "ZW", "716", "ZIMBABWE",              "ZWD" },
    { "US", "840", "UNITED STATES",         "USD" },
  };
  
  public static class CountryCode
  {
    private String      CountryCodeAlpha    = null;
    private String      CountryCodeNumeric  = null;
    private String      CountryName         = null;
    private String      CurrencyCodeAlpha   = null;
    
    public CountryCode( String ccAlpha, String ccNumeric, String name, String currencyCodeAlpha )
    {
      CountryCodeAlpha    = (ccAlpha == null ? "" : ccAlpha);
      CountryCodeNumeric  = (ccNumeric == null ? "" : ccNumeric);
      CountryName         = (name == null ? "" : name);
      CurrencyCodeAlpha   = (currencyCodeAlpha == null ? "" : currencyCodeAlpha);
    }
    
    public CountryCode( ResultSet resultSet ) 
      throws java.sql.SQLException
    {
      CountryCodeAlpha    = resultSet.getString("country_code_alpha");
      CountryCodeNumeric  = resultSet.getString("country_code_numeric");
      CountryName         = resultSet.getString("country_name");
      CurrencyCodeAlpha   = resultSet.getString("currency_code_alpha");
      
      // force to empty strings so string compare
      // does not throw a null pointer exception
      if ( CountryCodeAlpha == null )
      {
        CountryCodeAlpha = "";
      }
      
      if ( CountryCodeNumeric == null )
      {
        CountryCodeNumeric = "";
      }
    }
    
    public String getCountryCodeAlpha()
    {
      return( CountryCodeAlpha );
    }
    
    public String getCountryCodeNumeric()
    {
      return( CountryCodeNumeric );
    }
    
    public String getCountryName()
    {
      return( CountryName );
    }
    
    public String getCurrencyCode()
    {
      return( CurrencyCodeAlpha );
    }
    
    public boolean matchesAlpha( String ccAlpha )
    {
      return( CountryCodeAlpha.equals(ccAlpha) );
    }
    
    public boolean matchesNumeric( String ccNumeric )
    {
      return( CountryCodeNumeric.equals(ccNumeric) );
    }
  }
  
  // singleton
  private static CountryCodeConverter     TheCountryCodeConverter  = null;
  
  // instance variables
  private   Vector      CountryCodes      = new Vector();
  private   long        LoadTimeMillis    = 0L;
  
  private CountryCodeConverter()
  {
    loadDataDefault();
    load();
  }
  
  public String alphaCodeToNumericCode( String alphaCode )
  {
    CountryCode   cc      = null;
    String        retVal  = null;
    
    try
    {
      cc = findEntry(alphaCode);
      if ( cc != null )
      {
        retVal = cc.getCountryCodeNumeric();
      }
    }
    catch( Exception e )
    {
      logEntry("alphaCodeToNumericCode(" + alphaCode + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  protected CountryCode findEntry( String countryCode )
  {
    CountryCode   cc        = null;
    boolean       isNumber  = false;
    boolean       match     = false;
    CountryCode   retVal    = null;
    
    try
    {
      int tempInt = Integer.parseInt(countryCode);
      isNumber = true;
    }
    catch( Exception e )
    {
      // not a number
    }
    
    try
    {
      for( int i = 0; i < CountryCodes.size(); ++i )
      {
        cc    = (CountryCode)CountryCodes.elementAt(i);
        match = (isNumber ? cc.matchesNumeric(countryCode) : cc.matchesAlpha(countryCode));
        
        if ( match )
        {
          retVal = cc;
          break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry("findEntry(" + countryCode + ")",e.toString());
    }
    return( retVal );
  }
  
  public String getCountryName( String countryCode )
  {
    String    retVal    = null;
    try
    {
      CountryCode cc = findEntry( countryCode );
      if ( cc != null )
      {
        retVal = cc.getCountryName();
      }
    }
    catch( Exception e )
    {
      logEntry("getCountryName(" + countryCode + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  public String getCurrencyCode( String countryCode )
  {
    CountryCode   cc          = null;
    String        retVal      = null;
    
    try
    {
      cc = findEntry(countryCode);
      if ( cc != null )
      {
        retVal = cc.getCurrencyCode();
      }
    }
    catch( Exception e )
    {
      logEntry("getCurrencyCode(" + countryCode + ")",e.toString());
    }
    return( retVal );
  }
  
  public static CountryCodeConverter getInstance()
  {
    if ( TheCountryCodeConverter == null )
    {
      TheCountryCodeConverter = new CountryCodeConverter();
    }
    TheCountryCodeConverter.refresh();
    return( TheCountryCodeConverter );
  }
  
  protected synchronized void load( )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:492^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  c.country_desc              as country_name,
//                  c.country_code              as country_code_alpha,
//                  c.iso_country_code          as country_code_numeric,
//                  c.currency_code_alpha       as currency_code_alpha
//          from    country   c
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  c.country_desc              as country_name,\n                c.country_code              as country_code_alpha,\n                c.iso_country_code          as country_code_numeric,\n                c.currency_code_alpha       as currency_code_alpha\n        from    country   c";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.CountryCodeConverter",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.CountryCodeConverter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:499^7*/
      resultSet = it.getResultSet();
      
      if( resultSet.next() )
      {
        CountryCodes.removeAllElements();
        do
        {
          CountryCodes.add( new CountryCode(resultSet) );  
        }
        while( resultSet.next() );
      }        
      resultSet.close();
      
      LoadTimeMillis = System.currentTimeMillis();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadDataDefault()
  {
    CountryCodes.removeAllElements();
    
    for( int i = 0; i < CountryCodeDefaults.length; ++i )
    {
      CountryCodes.add( new CountryCode(CountryCodeDefaults[i][0],
                                        CountryCodeDefaults[i][1],
                                        CountryCodeDefaults[i][2],
                                        CountryCodeDefaults[i][3]) );  
    }
  }
  
  public String numericCodeToAlphaCode( int numericCode )
  {
    StringBuffer buf = new StringBuffer(""+numericCode);
    while (buf.length() < 3)
    {
      buf.insert(0,"0");
    }
    return numericCodeToAlphaCode(buf.toString());
  }

  public String numericCodeToAlphaCode( String numericCode )
  {
    CountryCode   cc      = null;
    String        retVal  = null;
    
    try
    {
      cc = findEntry( numericCode );
      if ( cc != null )
      {
        retVal = cc.getCountryCodeAlpha();
      }
    }
    catch( Exception e )
    {
      logEntry("numericCodeToAlphaCode(" + numericCode + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  // synchronize the refresh method so two requests (threads)
  // do not trigger a back-to-back reload of the cached data
  protected synchronized void refresh()
  {
    if ( LoadTimeMillis == 0L )   // only load once at startup
    {
      load();
    }
  }
}/*@lineinfo:generated-code*/