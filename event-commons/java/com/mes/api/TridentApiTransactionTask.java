/*@lineinfo:filename=TridentApiTransactionTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/TridentApiTransactionTask.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See VSS database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.axis.client.Stub;
// XML libraries
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.SyncLog;
import sqlj.runtime.ResultSetIterator;

public abstract class TridentApiTransactionTask extends SQLJConnectionBase
{
  
  protected   String                  ErrorCode       = TridentApiConstants.ER_NONE;
  protected   String                  ErrorDesc       = "";
  protected   boolean                 ProdFlagDefault = false;
  protected   Stub                    SoapService     = null;
  protected   TridentApiTransaction   Transaction     = null;
  
  public TridentApiTransactionTask( TridentApiTransaction tran )
  {
    Transaction = tran;
  }
  
  public abstract boolean doTask();
  
  public String getErrorCode( )
  {
    return( ErrorCode );
  }
  
  public String getErrorDesc( )
  {
    return( ErrorDesc );
  }
  
  public org.apache.axis.client.Stub getSoapService()
  {
    return( SoapService );
  }
  
  public boolean hasError( )
  {
    return( !(getErrorCode().equals(TridentApiConstants.ER_NONE)) );
  }
  
  public boolean isPinDebitTransaction()
  {
    return( isPinDebitTransaction( Transaction.getTridentTranId() ) );
  }
  
  public boolean isPinDebitTransaction( String tranId )
  {
    int       recCount  = 0;
    boolean   retVal    = false;
    
    try
    { 
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:94^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( trident_tran_id ) 
//          from    trident_capture_api   tapi
//          where   tapi.trident_tran_id = :tranId and
//                  tapi.card_type = 'DB'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( trident_tran_id )  \n        from    trident_capture_api   tapi\n        where   tapi.trident_tran_id =  :1  and\n                tapi.card_type = 'DB'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.TridentApiTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
      retVal = (recCount > 0);
    }
    catch( Exception e )
    {
      // database error, assume tran id is invalid
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected void loadAuthNetData()
  {
    loadAuthNetData( Transaction );
  }
  
  protected void loadAuthNetData( TridentApiTransaction data )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    String              tid           = data.getProfileId();
    long                tranIdNumber  = data.getTridentTranIdNumber();
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:130^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.trident_tran_id    as tran_id,
//                  tapi.auth_code          as auth_code,
//                  tapi.avs_zip            as avs_zip
//          from    trident_capture_api   tapi
//          where   tapi.trident_tran_id_number = :tranIdNumber and
//                  tapi.terminal_id = :tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.trident_tran_id    as tran_id,\n                tapi.auth_code          as auth_code,\n                tapi.avs_zip            as avs_zip\n        from    trident_capture_api   tapi\n        where   tapi.trident_tran_id_number =  :1  and\n                tapi.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.TridentApiTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tranIdNumber);
   __sJT_st.setString(2,tid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.api.TridentApiTransactionTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        data.setTridentTranId( resultSet.getString("tran_id") );
        data.setAuthCode( resultSet.getString("auth_code") );
        data.setAvsZip( resultSet.getString("avs_zip") );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName()+"::loadAuthNetData", e);
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
  }
  
  protected void loadAuthReversalData( )
  {
    loadAuthReversalDataFromXml( Transaction.getTridentTranId(),
                                 Transaction.getProfileId() );
  }
  
  protected void loadAuthReversalData( String tranId, String tid )
  {
    ResultSetIterator it          = null;
    ResultSet         resultSet   = null;
  
    try
    { 
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:175^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.authorization_code ||
//                  to_char( auth.transaction_time,
//                           'mmddyyhh24miss' ) ||
//                  auth.retrieval_refrence_number              as reversal_data,
//                  auth.authorized_amount                      as auth_amount,
//                  dukpt_decrypt_wrapper(auth.card_number_enc) as card_number_full,
//                  (substr(auth.expiration_date_incoming,3) ||
//                   substr(auth.expiration_date_incoming,1,2)) as exp_date,                                                  
//                  auth.auth_characteristics_ind               as returned_aci,
//                  auth.payment_service_transaction_id         as tran_id                
//          from    tc33_auth_trident_index   ai,
//                  tc33_authorization        auth
//          where   ai.trident_transaction_id = :tranId and
//                  auth.rec_id = ai.rec_id and
//                  auth.terminal_id = :tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.authorization_code ||\n                to_char( auth.transaction_time,\n                         'mmddyyhh24miss' ) ||\n                auth.retrieval_refrence_number              as reversal_data,\n                auth.authorized_amount                      as auth_amount,\n                dukpt_decrypt_wrapper(auth.card_number_enc) as card_number_full,\n                (substr(auth.expiration_date_incoming,3) ||\n                 substr(auth.expiration_date_incoming,1,2)) as exp_date,                                                  \n                auth.auth_characteristics_ind               as returned_aci,\n                auth.payment_service_transaction_id         as tran_id                \n        from    tc33_auth_trident_index   ai,\n                tc33_authorization        auth\n        where   ai.trident_transaction_id =  :1  and\n                auth.rec_id = ai.rec_id and\n                auth.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.TridentApiTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.api.TridentApiTransactionTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        Transaction.setAuthReversalData(resultSet.getString("reversal_data"));
        Transaction.setAuthAmount(resultSet.getDouble("auth_amount"));
        Transaction.setCardNumberFull(resultSet.getString("card_number_full"));
        Transaction.setExpDate( resultSet.getString("exp_date") );
        Transaction.setReturnedAci( resultSet.getString("returned_aci") );
        Transaction.setAuthTransactionId( resultSet.getString("tran_id") );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      // database error, assume tran id is invalid
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  protected void loadAuthReversalDataFromXml( String tranId, String tid )
  {
    double        authAmount      = 0.0;
    String        cardNumberFull  = null;
    String        expDate         = null;
    Blob          b               = null;
    String        value           = null;
  
    try
    { 
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:229^7*/

//  ************************************************************
//  #sql [Ctx] { select  dukpt_decrypt_wrapper(card_number_enc)  as card_number_full,
//                  nvl(tapi.auth_amount,0)                 as auth_amount,
//                  nvl(tapi.exp_date,
//                      to_char((sysdate+365),'mmyy'))      as exp_date,
//                  tapi.auth_response_xml                  as auth_resp_xml
//          
//          from    trident_capture_api     tapi
//          where   tapi.trident_tran_id = :tranId and
//                  tapi.terminal_id = :tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dukpt_decrypt_wrapper(card_number_enc)  as card_number_full,\n                nvl(tapi.auth_amount,0)                 as auth_amount,\n                nvl(tapi.exp_date,\n                    to_char((sysdate+365),'mmyy'))      as exp_date,\n                tapi.auth_response_xml                  as auth_resp_xml\n         \n        from    trident_capture_api     tapi\n        where   tapi.trident_tran_id =  :1  and\n                tapi.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.api.TridentApiTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardNumberFull = (String)__sJT_rs.getString(1);
   authAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   expDate = (String)__sJT_rs.getString(3);
   b = (java.sql.Blob)__sJT_rs.getBlob(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^7*/
      Document  authRespDoc     = null;
      Element   el              = null;
      String    id              = null;
      Element   root            = null;
      
      String    authCode        = null;
      String    retrievalRefNum = null;
      String    tranDate        = null;
      String    tranTime        = null;
      
      // create xdoc document to parse data
      SAXReader reader = new SAXReader();
      authRespDoc = reader.read( b.getBinaryStream() );
      root = authRespDoc.getRootElement();
      
      StringBuffer buffer = new StringBuffer();
      
      java.util.Iterator it = null; 
      it = root.element("groupI").elementIterator();
      while( it.hasNext() )
      {
        el    = (Element)it.next();
        id    = el.attributeValue("id","");
        value = el.attributeValue("value","");
        
        if ( id.equals("03") )
        {
          Transaction.setReturnedAci( value );
        }
        else if ( id.equals("09") )
        {
          authCode = value;
        }
        else if ( id.equals("10") )
        {
          tranDate = value;
        }
        else if ( id.equals("11") ) 
        {
          tranTime = value;
        }
        else if ( id.equals("14") )
        {
          retrievalRefNum = value;
        }
        else if ( id.equals("16") )
        {
          Transaction.setAuthTransactionId( value );
        }
      }
      Transaction.setAuthReversalData( authCode + tranDate + tranTime + retrievalRefNum );
      Transaction.setAuthAmount( authAmount );
      Transaction.setCardNumberFull( cardNumberFull );
      Transaction.setExpDate( expDate );
    }
    catch( Exception e )
    {
      // database error, assume tran id is invalid
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected double loadLinkedTranAmount()
  {
    double        retVal      = 0.0;
    String        tid         = null;
    String        tranId      = null;
    
    try
    {
      connect();
      
      if ( Transaction.isMultiCaptureAllowed() )
      {
        tranId  = Transaction.getTridentTranId();
        tid     = Transaction.getProfileId();
      
        /*@lineinfo:generated-code*//*@lineinfo:321^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(tapi.transaction_amount),0) as linked_tran_amount
//            
//            from    trident_capture_api   tapi
//            where   tapi.original_trident_tran_id = :tranId
//                    and tapi.terminal_id = :tid
//                    and tapi.debit_credit_indicator = 'D'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(tapi.transaction_amount),0) as linked_tran_amount\n           \n          from    trident_capture_api   tapi\n          where   tapi.original_trident_tran_id =  :1 \n                  and tapi.terminal_id =  :2 \n                  and tapi.debit_credit_indicator = 'D'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.api.TridentApiTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^9*/
      }
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::loadLinkedTranAmount", e);
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
  
  protected void loadTransactionData()
  {
    ResultSetIterator it          = null;
    ResultSet         resultSet   = null;
    PreparedStatement ps = null;
    
    try
    { 
      connect();
      
      String tranId = Transaction.getTridentTranId();
      String tid    = Transaction.getProfileId();
      
      String q = "select " + 
          "  tapi.trident_tran_id_number as tran_id_number, " + 
          "  tapi.external_tran_id as external_tran_id, " + 
          "  tapi.fx_amount_base as fx_amount, " + 
          "  tapi.fx_rate_id as fx_rate_id, " + 
          "  tapi.fx_reference_number as fx_ref_num, " + 
          "  tapi.currency_code as currency_code, " + 
          "  tapi.card_number as card_number, " + 
          "  tapi.auth_amount as auth_amount, " + 
          "  tapi.transaction_amount as tran_amount, " + 
          "  decode(" + 
          "    sign(tapi.batch_id - tpapi.current_batch_id), - 1, 'Y', 0, 'N', 1, 'N' " + 
          "  ) as settled " + 
          "from " + 
          "  trident_capture_api tapi, " + 
          "  trident_profile_api tpapi " + 
          "where " + 
          "  tapi.terminal_id = tpapi.terminal_id " + 
          "  and tapi.trident_tran_id = ? " + 
          "  and tapi.terminal_id = ?";
      ps = getPreparedStatement(q);
      ps.setString(1, tranId);
      ps.setString(2, tid);
      resultSet = ps.executeQuery();
      
      if ( resultSet.next() )
      {
        Transaction.setSettledFlag(resultSet.getString("settled"));
        Transaction.setPayVisionFields(resultSet);
        Transaction.setFxFields(resultSet);
        
        // card type needs to be determined for 
        // operations (e.g. voids) that do not require
        // a card number to be submitted.
        if ( Transaction.isBlank( Transaction.getCardNumber() ) )
        {
          Transaction.setCardNumber( resultSet.getString("card_number") );
        }
      }
      resultSet.close();
      it.close();
      
      Transaction.setLinkedTranAmount( loadLinkedTranAmount() );
      
      if ( Transaction.useAdyen() )
      {
        // for modification requests on pre-auth/capture 
        // transactions, Adyen requires the 
        // PSP reference (aka tran id) from the pre-auth
        /*@lineinfo:generated-code*//*@lineinfo:405^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  payvision_tran_guid   as tid
//            from    trident_api_payvision_lookup
//            where   trident_tran_id = :tranId
//                    and transaction_type = 'P'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  payvision_tran_guid   as tid\n          from    trident_api_payvision_lookup\n          where   trident_tran_id =  :1 \n                  and transaction_type = 'P'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.api.TridentApiTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.api.TridentApiTransactionTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^9*/
        resultSet = it.getResultSet();
        
        if ( resultSet.next() )
        {
          Transaction.setExternalTranId( resultSet.getString("tid") );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      // database error, assume tran id is invalid
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  public void setError( String ecode )
  {
    setErrorCode(ecode);
    setErrorDesc(TridentApiConstants.getErrorDesc(ecode));
  }
  
  public void setErrorCode( String ecode )
  {
    ErrorCode = ecode;
    if ( Transaction != null )
    {
      Transaction.setErrorCode(ecode);
    }
  }
  
  public void setErrorDesc( String desc )
  {
    ErrorDesc = desc;
    if ( Transaction != null )
    {
      Transaction.setErrorDesc(desc);
    }
  }
  
  public void setProdFlagDefault( boolean flag )
  {
    ProdFlagDefault = flag;
  }
  
  public void setSoapService( Stub service )
  {
    SoapService = service;
  }
  
  public boolean validTranId( )
  {
    return( validTranId( Transaction.getProfileId(), Transaction.getTridentTranId() ) );
  }
  
  public boolean validTranId( String tid, String tranId )
  {
    int       recCount  = 0;
    boolean   retVal    = false;
    
    try
    { 
      String testFlag = ApiDb.loadTestFlag(ProdFlagDefault);
    
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:483^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( trident_tran_id ) 
//          from    trident_capture_api   tapi
//          where   tapi.trident_tran_id = :tranId 
//                  and tapi.terminal_id = :tid
//                  and tapi.test_flag = :testFlag
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( trident_tran_id )  \n        from    trident_capture_api   tapi\n        where   tapi.trident_tran_id =  :1  \n                and tapi.terminal_id =  :2 \n                and tapi.test_flag =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.api.TridentApiTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   __sJT_st.setString(3,testFlag);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:490^7*/
      retVal = (recCount > 0);
    }
    catch( Exception e )
    {
      // database error, assume tran id is invalid
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean voidTransaction()
  {
    String        cardType    = null;
    boolean       retVal      = false;
    String        tranId      = null;
    String batchId = null;
    String batchNumber = null;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      String q = "select current_batch_id, current_batch_number from trident_profile_api where terminal_id = ?";
      PreparedStatement ps = getPreparedStatement(q);
      ps.setString(1, Transaction.getProfileId());
      ResultSet rs = ps.executeQuery();
      if(rs.next()) {
        batchId = rs.getString("current_batch_id");
        batchNumber = rs.getString("current_batch_number");
      }
      ps.close();
      rs.close();
      
      cardType  = Transaction.getCardType();
      tranId    = Transaction.getTridentTranId();
      
      String fxPostFlag = (Transaction.isFxTransaction() ? "P" : null);
      System.out.println("voiding - " + tranId);
      /*@lineinfo:generated-code*//*@lineinfo:533^7*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api   tapi
//          set     transaction_type = :TridentApiTranBase.TT_VOID,
//                  transaction_amount = 0,
//                  load_filename = 'void',
//                  fx_post_status = :fxPostFlag,
//                  fx_post_ts = null,
//                  batch_number = null,
//                  batch_id = null
//          where   tapi.trident_tran_id = :tranId
//                  and (
//                      tapi.batch_id = :batchId -- Most things should have a batch ID already
//                      or tapi.batch_id is null -- Pre-auths wont have a batch ID
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api   tapi\n        set     transaction_type =  :1 ,\n                transaction_amount = 0,\n                load_filename = 'void',\n                fx_post_status =  :2 ,\n                fx_post_ts = null,\n                batch_number = null,\n                batch_id = null\n        where   tapi.trident_tran_id =  :3 \n                and (\n                    tapi.batch_id =  :4  -- Most things should have a batch ID already\n                    or tapi.batch_id is null -- Pre-auths wont have a batch ID\n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.api.TridentApiTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TridentApiTranBase.TT_VOID);
   __sJT_st.setString(2,fxPostFlag);
   __sJT_st.setString(3,tranId);
   __sJT_st.setString(4,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^7*/
      retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
      
      /*@lineinfo:generated-code*//*@lineinfo:551^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^7*/
      
      if ( retVal )
      {
        try
        {                                
          // reverse the authorization
          loadAuthReversalData();   // load original data from database
          if ( Transaction.getAuthAmount() > 0.0 )
          {
            // issue a full reversal by setting the auth amount
            // to the remaining auth amount and setting the
            // transaction amount to 0
            Transaction.setAuthAmount(Transaction.getTranAmount());
            Transaction.setTranAmount(0.0);
            
            // reverse all USD auths during void process
            // foreign transactions should be reversed during the 
            // void process at the international vendor
            if ( !Transaction.useIntlVendor() )
            {
              Transaction.reverseAuthorization(); // issue reversal request
            }
          }          
        }
        catch( Exception re )
        {
          SyncLog.logThrottled(this.getClass().getName()+":voidTransaction(reversal-" + tranId + ")", re);
        }                  
      }
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:587^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:587^34*/ }catch( Exception ee){} // roll back tran
      SyncLog.logThrottled(this.getClass().getName()+":voidTransaction(reversal-" + tranId + ")", e);
      try{ /*@lineinfo:generated-code*//*@lineinfo:589^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:589^32*/ }catch( Exception ee){} // commit log entry
      setErrorCode(TridentApiConstants.ER_VOID_FAILED);
      setErrorDesc("Failed to void transaction - Retry");
    }
    finally
    {
      setAutoCommit(true);  // restore auto commit
      cleanUp();
    }
    return( retVal );      
  }        
}/*@lineinfo:generated-code*/