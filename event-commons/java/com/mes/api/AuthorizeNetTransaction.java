/*************************************************************************

  FILE: $Archive: /java/beans/com/mes/api/AuthorizeNetTransaction.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2007-01-31 11:49:45 -0800 (Wed, 31 Jan 2007) $
  Version            : $Revision: 13393 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.crypt.MD5;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import masthead.formats.visad.ResponseDMessage;

public class AuthorizeNetTransaction extends TridentApiTransaction
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  public static final int   RF_RESPONSE_CODE                      = 0;
  public static final int   RF_RESPONSE_SUBCODE                   = 1;
  public static final int   RF_REASON_CODE                        = 2;
  public static final int   RF_REASON_TEXT                        = 3;
  public static final int   RF_AUTH_CODE                          = 4;
  public static final int   RF_AVS_RESULT                         = 5;
  public static final int   RF_TRAN_ID                            = 6;
  public static final int   RF_INV_NUM                            = 7;
  public static final int   RF_DESC                               = 8;
  public static final int   RF_AMOUNT                             = 9;
  public static final int   RF_METHOD                             = 10;
  public static final int   RF_TRAN_TYPE                          = 11;
  public static final int   RF_CUSTOMER_ID                        = 12;
  public static final int   RF_CARDHOLDER_FIRST_NAME              = 13;
  public static final int   RF_CARDHOLDER_LAST_NAME               = 14;
  public static final int   RF_COMPANY                            = 15;
  public static final int   RF_BILLING_ADDRESS                    = 16;
  public static final int   RF_CITY                               = 17;
  public static final int   RF_STATE                              = 18;
  public static final int   RF_ZIP                                = 19;
  public static final int   RF_COUNTRY                            = 20;
  public static final int   RF_PHONE                              = 21;
  public static final int   RF_FAX                                = 22;
  public static final int   RF_EMAIL                              = 23;
  public static final int   RF_SHIP_TO_FIRST_NAME                 = 24;
  public static final int   RF_SHIP_TO_LAST_NAME                  = 25;
  public static final int   RF_SHIP_TO_COMPANY                    = 26;
  public static final int   RF_SHIP_TO_ADDRESS                    = 27;
  public static final int   RF_SHIP_TO_CITY                       = 28;
  public static final int   RF_SHIP_TO_STATE                      = 29;
  public static final int   RF_SHIP_TO_ZIP                        = 30;
  public static final int   RF_SHIP_TO_COUNTRY                    = 31;
  public static final int   RF_TAX                                = 32;
  public static final int   RF_DUTY                               = 33;
  public static final int   RF_FREIGHT                            = 34;
  public static final int   RF_TAX_EXEMPT                         = 35;
  public static final int   RF_PO_NUMBER                          = 36;
  public static final int   RF_MD5_HASH                           = 37;
  public static final int   RF_COUNT                              = 38;
  
  // Version 3.1 Fields
  public static final int   RF_CVV2_RESULT                        = 38;
  public static final int   RF31_COUNT                            = 68;
  
  // field value constants
  public static final String  AN_AVS_RESULT_NOT_APPLICABLE        = "P";
  

  private   String          EncapsulationChar     = "";
  private   String[]        ResponseFields        = null;
  private   String          ResponseDelimiter     = ",";
  private   boolean         TestMode              = false;
  private   String          Version               = "3.0";
  
  public AuthorizeNetTransaction( )
  {
    Emulation = TridentApiConstants.EMU_AUTHORIZE_NET;
  }
  
  public AuthorizeNetTransaction( String tranId )
  {
    super(tranId);
    Emulation = TridentApiConstants.EMU_AUTHORIZE_NET;
  }
  
  public AuthorizeNetTransaction( ResultSet resultSet )
    throws java.sql.SQLException
  {
    super(resultSet);
    Emulation = TridentApiConstants.EMU_AUTHORIZE_NET;
  }    
  
  public String encodeResponse()
  {
    String            authNetReasonCode = null;
    String            avsResult         = null;
    StringBuffer      response          = new StringBuffer();
    ResponseDMessage  resp              = getVisaDResponse();
    String            respCode          = resp.getResponseCode();
    
    // extract the AVS result (if any)
    try
    { 
      avsResult = resp.getAVSResultCode(); 
      if ( avsResult.equals("0") )  // no request made
      {
        avsResult = AN_AVS_RESULT_NOT_APPLICABLE;
      }
    } 
    catch(Exception e)
    {
      // ignore
    }
    
    if ( respCode.equals("00") )
    {
      authNetReasonCode                 = TridentApiConstants.ER_AN_NONE;
      ResponseFields[RF_RESPONSE_CODE]  = authNetReasonCode;
      ResponseFields[RF_REASON_CODE]    = authNetReasonCode;
      ResponseFields[RF_AUTH_CODE]      = resp.getApprovalCode();
      ResponseFields[RF_TRAN_ID]        = String.valueOf( getTridentTranIdNumber() );
    }
    else
    {
      avsResult                         = AN_AVS_RESULT_NOT_APPLICABLE;
      authNetReasonCode                 = TridentApiConstants.visakResponseCodeToAuthNetResponseCode(respCode);
      ResponseFields[RF_RESPONSE_CODE]  = TridentApiConstants.getAuthNetResponseCode(authNetReasonCode);
      ResponseFields[RF_REASON_CODE]    = authNetReasonCode;
      
      if( ResponseFields[RF_RESPONSE_CODE].equals(TridentApiConstants.RC_AN_DECLINED) )
      {
        ResponseFields[RF_TRAN_ID] = String.valueOf( getTridentTranIdNumber() );
      }
      else
      {
        ResponseFields[RF_TRAN_ID] = "0";
      }        
    }
    ResponseFields[RF_REASON_TEXT]  = TridentApiConstants.getAuthNetReasonText(authNetReasonCode);
    ResponseFields[RF_AVS_RESULT]   = avsResult;
    
    if ( Version.equals("3.1") )
    {
      if ( resp.hasCvv2ResultCode() )
      {
        ResponseFields[RF_CVV2_RESULT] = resp.getCvv2ResultCode();
      }
    }
    else    // default is 3.0
    {
    }
    
    return( encodeResponseFields(ResponseDelimiter) );
  }
  
  public String encodeResponse( String tranId, String msgId, String msgText )
  {
    String respCode   = "3";
    String rcode      = TridentApiConstants.apiErrorCodeToAuthNetResponseCode(msgId);
    String tranIdNum  = "0";
    
    if ( msgId.equals(TridentApiConstants.ER_NONE) )
    {
      respCode  = "1";
      tranIdNum = String.valueOf(getTridentTranIdNumber());
      ResponseFields[RF_ZIP] = getAvsZip();
    }
    ResponseFields[RF_RESPONSE_CODE]  = respCode;
    ResponseFields[RF_REASON_CODE]    = rcode;
    ResponseFields[RF_REASON_TEXT]    = TridentApiConstants.getAuthNetReasonText(rcode,(msgText + " (" + msgId + ")"));
    ResponseFields[RF_AUTH_CODE]      = getAuthCode();
    ResponseFields[RF_AVS_RESULT]     = AN_AVS_RESULT_NOT_APPLICABLE;
    ResponseFields[RF_TRAN_ID]        = tranIdNum;
    
    return( encodeResponseFields(ResponseDelimiter) );
  }
  
  private String encodeResponseFields( String delim )
  {
    StringBuffer      response    = new StringBuffer();
    StringBuffer      hashString  = new StringBuffer();
    
    // build the MD5 hash
    //    login + tran id + amount
    hashString.append( getProfileId() );
    hashString.append( ResponseFields[RF_TRAN_ID] );
    hashString.append( ResponseFields[RF_AMOUNT] );
    MD5 md5 = new MD5();
    md5.Init();
    md5.Update(hashString.toString());
    ResponseFields[RF_MD5_HASH] = md5.asHex().toUpperCase();
    
    if ( TestMode )
    {
      ResponseFields[RF_REASON_TEXT] = ("(TESTMODE) " + ResponseFields[RF_REASON_TEXT]);     
    }
  
    // build the delimited response text
    for( int i = 0; i < ResponseFields.length; ++i )
    {
      if( i > 0 )
      {
        response.append(delim);
      }
              
      response.append(EncapsulationChar);
      if ( ResponseFields[i] != null )
      {
        response.append(ResponseFields[i]);
      }
      response.append(EncapsulationChar);
    }
    return( response.toString() );
  }
  
  public String getPreAuthKey()
  {
    return( getProfileId() + "-" + String.valueOf(getTridentTranIdNumber()) );
  }
  
  public String getResponseDelimiter()
  {
    return( ResponseDelimiter );
  }
  
  public void setAuthNetFields( TridentApiTransaction tran )
  {
    setAvsZip( tran.getAvsZip() );
    setAuthCode( tran.getAuthCode() );
    setTridentTranId( tran.getTridentTranId() );
  }
  
  protected void setEncapsulationChar( String encapChar )
  {
    if( encapChar != null && encapChar.trim().length() > 0 )
    {
      EncapsulationChar = String.valueOf( encapChar.trim().charAt(0) );
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    long    userTranId  = 0L;
    
    // authorize.net emulation
    
    userTranId = HttpHelper.getLong(request,TridentApiConstants.FN_AN_TRAN_ID,0L);
    if ( userTranId > 0L )
    {
      TridentTranIdNumber = userTranId;
    }
    ProfileId         = HttpHelper.getString(request,TridentApiConstants.FN_AN_TID,null);
    Cvv2              = HttpHelper.getString(request,TridentApiConstants.FN_AN_CVV2,null);
    TranDate          = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    TranAmount        = HttpHelper.getDouble(request,TridentApiConstants.FN_AN_AMOUNT,0.0);
    TaxAmount         = HttpHelper.getDouble(request,TridentApiConstants.FN_AN_TAX,0.0);
    AvsStreet         = HttpHelper.getString(request,TridentApiConstants.FN_AN_AVS_STREET,null);
    AvsZip            = HttpHelper.getString(request,TridentApiConstants.FN_AN_AVS_ZIP,null);
    Version           = HttpHelper.getString(request,TridentApiConstants.FN_AN_VERSION,"3.0");
    MotoEcommInd      = TridentApiConstants.FV_MOTO_ECOMM_DEFAULT;
    
    setPurchaseId( HttpHelper.getString(request,TridentApiConstants.FN_AN_INV_NUM,null),
                   HttpHelper.getString(request,TridentApiConstants.FN_AN_PO_NUM,null) );
                   
    setExpDate( HttpHelper.getString(request,TridentApiConstants.FN_AN_EXP_DATE,null) );
    setTranType(HttpHelper.getString(request,TridentApiConstants.FN_AN_TRAN_TYPE,TT_INVALID));
    setCurrencyCode(HttpHelper.getString(request,TridentApiConstants.FN_AN_CURRENCY_CODE,TridentApiConstants.FV_CURRENCY_CODE_DEFAULT));
    
    setReponseDelimiter( HttpHelper.getString(request,TridentApiConstants.FN_AN_DELIM_CHAR,null) );
    setEncapsulationChar( HttpHelper.getString(request,TridentApiConstants.FN_AN_ENCAP_CHAR,null) );
    
    // store the original values for use by the validation
    setCardData(request);
    
    // store the full card number.  this method handles 
    // encrypting the full card value and truncating the clear value
    setCardNumberFull( HttpHelper.getString(request,TridentApiConstants.FN_AN_CARD_NUMBER,null) );
    
    if ( MotoEcommInd.equals( TridentApiConstants.FV_NOT_A_MOTO ) )
    {
      MotoEcommInd = " ";   // translate to blank space
      CardPresent  = "1";   // card present
    }
    
    if ( isCredit( TranType ) )
    {
      DebitCreditInd = "C";
    }
    else  // default is debit
    {
      DebitCreditInd  = "D";    
    }      
    
    // create the response field array
    if ( Version.equals("3.1") )
    {
      ResponseFields = new String[68];
    }
    else    // default to 3.0
    {
      ResponseFields = new String[38];
    }
    
    // set the test mode flag
    TestMode = HttpHelper.getString(request,TridentApiConstants.FN_AN_TEST_FLAG,"").toLowerCase().equals("true");
    
    // store request values to be echoed back in response
    ResponseFields[RF_RESPONSE_SUBCODE]   = "1";  // not used by MES, always 1
    ResponseFields[RF_INV_NUM]            = HttpHelper.getString(request,TridentApiConstants.FN_AN_INV_NUM,null);                        
    ResponseFields[RF_DESC]               = HttpHelper.getString(request,TridentApiConstants.FN_AN_DESC,null);                           
    ResponseFields[RF_AMOUNT]             = NumberFormatter.getDoubleString(HttpHelper.getDouble(request,TridentApiConstants.FN_AN_AMOUNT,0.0),"0.00");
    ResponseFields[RF_METHOD]             = HttpHelper.getString(request,TridentApiConstants.FN_AN_METHOD,"CC");                         
    ResponseFields[RF_TRAN_TYPE]          = HttpHelper.getString(request,TridentApiConstants.FN_AN_TRAN_TYPE,"").toLowerCase();                      
    ResponseFields[RF_CUSTOMER_ID]        = HttpHelper.getString(request,TridentApiConstants.FN_AN_CUST_ID,null);                    
    ResponseFields[RF_CARDHOLDER_FIRST_NAME] = HttpHelper.getString(request,TridentApiConstants.FN_AN_FIRST_NAME,null);          
    ResponseFields[RF_CARDHOLDER_LAST_NAME]  = HttpHelper.getString(request,TridentApiConstants.FN_AN_LAST_NAME,null);           
    ResponseFields[RF_COMPANY]            = HttpHelper.getString(request,TridentApiConstants.FN_AN_COMPANY,null);                        
    ResponseFields[RF_BILLING_ADDRESS]    = HttpHelper.getString(request,TridentApiConstants.FN_AN_AVS_STREET,null);                
    ResponseFields[RF_CITY]               = HttpHelper.getString(request,TridentApiConstants.FN_AN_CITY,null);                           
    ResponseFields[RF_STATE]              = HttpHelper.getString(request,TridentApiConstants.FN_AN_STATE,null);                          
    ResponseFields[RF_ZIP]                = HttpHelper.getString(request,TridentApiConstants.FN_AN_AVS_ZIP,null);                            
    ResponseFields[RF_COUNTRY]            = HttpHelper.getString(request,TridentApiConstants.FN_AN_COUNTRY,null);                        
    ResponseFields[RF_PHONE]              = HttpHelper.getString(request,TridentApiConstants.FN_AN_PHONE,null);                          
    ResponseFields[RF_FAX]                = HttpHelper.getString(request,TridentApiConstants.FN_AN_FAX,null);                            
    ResponseFields[RF_EMAIL]              = HttpHelper.getString(request,TridentApiConstants.FN_AN_EMAIL,null);                          
    ResponseFields[RF_SHIP_TO_FIRST_NAME] = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_FIRST_NAME,null);
    ResponseFields[RF_SHIP_TO_LAST_NAME]  = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_LAST_NAME,null);              
    ResponseFields[RF_SHIP_TO_COMPANY]    = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_COMPANY,null);                
    ResponseFields[RF_SHIP_TO_ADDRESS]    = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_ADDRESS,null);                
    ResponseFields[RF_SHIP_TO_CITY]       = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_CITY,null);                   
    ResponseFields[RF_SHIP_TO_STATE]      = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_STATE,null);                  
    ResponseFields[RF_SHIP_TO_ZIP]        = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_ZIP,null);                    
    ResponseFields[RF_SHIP_TO_COUNTRY]    = HttpHelper.getString(request,TridentApiConstants.FN_AN_SHIP_TO_COUNTRY,null);                
    
    if ( HttpHelper.getString(request,TridentApiConstants.FN_AN_TAX,null) != null && isTaxValid() )
    {
      ResponseFields[RF_TAX] = NumberFormatter.getDoubleString(TaxAmount,"0.0000");
    }
    if ( HttpHelper.getString(request,TridentApiConstants.FN_AN_DUTY,null) != null )
    {
      ResponseFields[RF_DUTY] = NumberFormatter.getDoubleString(HttpHelper.getDouble(request,TridentApiConstants.FN_AN_DUTY,0.0),"0.0000");
    }
    if ( HttpHelper.getString(request,TridentApiConstants.FN_AN_FREIGHT,null) != null )
    {
      ResponseFields[RF_FREIGHT] = NumberFormatter.getDoubleString(HttpHelper.getDouble(request,TridentApiConstants.FN_AN_FREIGHT,0.0),"0.0000");
    }
    if ( HttpHelper.getString(request,TridentApiConstants.FN_AN_TAX_EXEMPT,null) != null )
    {
      ResponseFields[RF_TAX_EXEMPT] = String.valueOf(HttpHelper.getBoolean(request,TridentApiConstants.FN_AN_TAX_EXEMPT,false)).toUpperCase();
    }
    ResponseFields[RF_PO_NUMBER] = HttpHelper.getString(request,TridentApiConstants.FN_AN_PO_NUM,null);                      
    
  }
  
  protected void setPurchaseId( String invoiceNum, String poNum )
  {
    if ( invoiceNum != null )
    {
      PurchaseId = invoiceNum;
    }
    else
    {
      PurchaseId = poNum;
    }
  }
  
  protected void setReponseDelimiter( String delim )
  {
    if ( delim == null || delim.trim().equals("") )
    {
      delim = ",";    // default is comma
    }
    else
    {
      ResponseDelimiter = delim;
    }
  }
  
  protected void setTranType( String anValue )
  {
    String    retVal    = TT_INVALID;
    String    value     = null;
    
    try
    {
      value = anValue.toLowerCase();
      
      if( value.equals("auth_capture") )
      {
        retVal = TT_DEBIT;
      }
      else if ( value.equals("auth_only") )
      {
        retVal = TT_PRE_AUTH;
      }
      else if ( value.equals("capture_only") )
      {
        retVal = TT_FORCE;
      }
      else if ( value.equals("credit") )
      {
        if( getTridentTranIdNumber() == 0L ) 
        {
          retVal = TT_CREDIT;
          setTridentTranId( getApiTranId() ); // establish auth.net tran id 
        }
        else
        {
          retVal = TT_REFUND;
        }
      }
      else if ( value.equals("void") )
      {
        retVal = TT_VOID;
      }
      else if ( value.equals("prior_auth_capture") )
      {
        retVal = TT_SETTLE;
      }
    }
    catch(Exception e)
    {
      // ignore, return default
    }
    TranType = retVal;
  }
}
