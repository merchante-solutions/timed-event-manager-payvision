/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/ApiDb.java $

  Description:  

  Last Modified By   : $Author: ttran $
  Last Modified Date : $LastChangedDate: 2015-02-17 09:18:30 -0800 (Tue, 17 Feb 2015) $
  Version            : $Revision: 23324 $

  Change History:
     See VSS database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.ach.AchDb;
import com.mes.ach.AchEntryData;
import com.mes.ach.AchStatementRecord;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
//import oracle.sql.BLOB;

public class ApiDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ApiDb.class);

  /**
   * NOTE: com.mes.maintenance.TridentProfile is still a parallel 
   *       profile accessor so it must be maintained along with
   *       with this class when making changes to the profile
   *       schema.
   */


  /**
   * Base profile query, modify this query 
   * when profile data is added, along with the
   * createProfile() method.
   */
  static String prfQs =
    " select                                                  " +
    "  tp.terminal_id                   tid,                  " +
    "  tp.merchant_number               merchant_number,      " +
    "  nvl(tpa.card_store_level,0)      card_store_level,     " + 
    "  tp.bin_number                    bin_number,           " +
    "  tp.merchant_name                 dba_name,             " +
    "  substr(tp.addr_city,1,13)        dba_city,             " +
    "  tp.addr_line_1                   dba_addr_1,           " +
    "  tp.addr_state                    dba_state,            " +
    "  tp.addr_zip                      dba_zip,              " +
    "  tp.phone_number                  phone_number,         " +
    "  tp.dm_contact_info               dm_contact_info,      " +
    "  nvl(tp.profile_status,'A')       profile_status,       " +
    "  tp.sic_code                      sic_code,             " +
    "  tp.time_zone                     time_zone,            " +
    "  tp.merchant_key                  profile_key,          " +
    "  decode(nvl(tp.secondary_key_enabled,'N'),              " +
    "        'Y',tp.secondary_key,null) secondary_key,        " +
    "  nvl(tp.vmc_accept,'N')           vmc_accept,           " +
    "  nvl(tp.amex_accept,'N')          amex_accept,          " +
    "  nvl(tp.disc_accept,'N')          disc_accept,          " +
    "  nvl(tp.diners_accept,'N')        dinr_accept,          " +
    "  nvl(tp.jcb_accept,'N')           jcb_accept,           " +
    "  nvl(tp.api_credits_allowed,'N')  api_credits_allowed,  " +
    "  nvl(tp.api_enabled,'N')          api_enabled,          " +
    "  nvl(tp.intl_processor,0)         intl_processor,       " +
    "  tpa.intl_credentials             intl_credentials,     " +
    "  upper(nvl(tpa.intl_usd,'N'))     intl_usd,             " + 
    "  nvl(tpa.amex_cid_response_enabled,'N')                 " +
    "                                   amex_cid_enabled,     " +
    "  tpa.avs_results_to_decline       bad_avs_results,      " +
    "  tpa.cvv2_results_to_decline      bad_cvv2_results,     " +
    "  decode( nvl(tpa.fx_client_id,0),                       " +
    "          0,'N','Y')               fx_enabled,           " +
    "  nvl(tpa.fx_client_id,0)          fx_client_id,         " +
    "  nvl(tpa.fx_product_type,0)       fx_product_type,      " +
    "  nvl(tpa.achp_ccd_accept,'N')     achp_ccd_accept,      " +
    "  nvl(tpa.achp_ppd_accept,'N')     achp_ppd_accept,      " +
    "  nvl(tpa.achp_web_accept,'N')     achp_web_accept,      " +
    "  nvl(tpa.achp_tel_accept,'N')     achp_tel_accept,      " +
    "  nvl(tpa.bml_accept,'N')          bml_accept,           " +
    "  tpa.bml_merch_id_1               bml_merch_id_1,       " +
    "  tpa.bml_promo_code_1             bml_promo_code_1,     " +
    "  tpa.bml_merch_id_2               bml_merch_id_2,       " +
    "  tpa.bml_promo_code_2             bml_promo_code_2,     " +
    "  tpa.bml_merch_id_3               bml_merch_id_3,       " +
    "  tpa.bml_promo_code_3             bml_promo_code_3,     " +
    "  tpa.bml_merch_id_4               bml_merch_id_4,       " +
    "  tpa.bml_promo_code_4             bml_promo_code_4,     " +
    "  tpa.bml_merch_id_5               bml_merch_id_5,       " +
    "  tpa.bml_promo_code_5             bml_promo_code_5,     " +
    "  nvl(tpa.amex_currency,'840')     amex_currency,        " +
    "  upper(nvl(tpa.use_vital_for_amex_auths,'N'))           " +
    "                                   use_vital_for_amex_auths," +
    "  upper(nvl(sic.tips_allowed,'N')) tips_allowed,         " +
    " upper(nvl(tpa.multi_capture_allowed,'N'))               " +
    "                                   multi_capture_allowed," + 
    "  upper(nvl(tpa.stand_in_processing_allowed,'N'))        " +
    "                                   stip_allowed,         " +
    "  decode(upper(nvl(tp.dynamic_dba_allowed,'N')),         " +
    "         'X','Y',                                        " +
    "         upper(nvl(tp.dynamic_dba_allowed,'N')))         " + 
    "                                   dynamic_dba_allowed,  " +
    "  tp.dynamic_dba_name_prefix       dynamic_dba_prefix,   " +
    "  decode(upper(nvl(tp.dynamic_dba_allowed,'N')),         " +
    "         'X','N',                                        " +
    "         'Y')                      ddba_prefix_edit_enabled," + 
    "  upper(nvl(tpa.force_declined_multi_cap,'N'))           " + 
    "                                   force_on_decline,     " +
    "  tpa.fraud_mode                   fraud_mode,           " +
    "  upper(nvl(tpa.http_get_enabled,'N'))                   " +
    "                                   get_allowed,          " +
    "  upper(nvl(tpa.validate_settle_amount,'Y'))             " +
    "                                   val_settle_amt        " +
    " from                                                    " +
    "  trident_profile                  tp,                   " +
    "  trident_profile_api              tpa,                  " +
    "  sic_codes                        sic                   " +
    " where                                                   " +
    "  tp.terminal_id = tpa.terminal_id(+)                    " +
    "  and sic.sic_code(+) = tp.sic_code                      ";

  // profile id clause
  static String prfPidQc =
    " and tp.terminal_id = ?                                  ";

  // node id clause
  static String prfNdQc =
    " and exists (                                            " +
    "   select                                                " +
    "    gm.merchant_number                                   " +
    "   from                                                  " +
    "    organization    o,                                   " +
    "    group_merchant  gm                                   " +
    "   where                                                 " +
    "    o.org_group = ?                                      " +
    "    and gm.org_num = o.org_num                           " +
    "    and gm.merchant_number = tp.merchant_number )        ";

  // last date clause
  static String prfLdQc =
    "  and tp.last_modified_date >= nvl( ? ,'01-JAN-2000')    ";
    
  // log entry insert
  static String logInsertQs = 
    " insert into trident_api_access_log  " +
    " (                       " +
    "   rec_id,               " +
    "   profile_id,           " +
    "   request_date,         " +
    "   request_method,       " +
    "   request_params,       " +
    "   response,             " +
    "   server_name,          " +
    "   retry_id,             " +
    "   transaction_type,     " +
    "   transaction_duration, " +
    "   switch_duration,      " +
    "   trident_tran_id,      " +
    "   retrieval_ref_num,    " +
    "   currency_code,        " +
    "   client_ip_address,    " +
    "   response_full         " +
    " )                       " +
    " values ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"; 
      
  static String logWithoutBlobQc =
    "null )           ";
    
  static String logWithBlobQc =
    "empty_blob() )   ";
  
  private int profileBatchNumber;
  private long profileBatchId;
  private int profileLastBatchNumber;
  
  /**
   * Constructs a query string based on the flags passed on.  The
   * base query string can have the following appended:
   *  profile id filter clause, node (hierarchy) id clause,
   *  last modified tade clause
   */
  private String buildQueryString(boolean withPid, boolean withNode, 
    boolean withLd)
  {
    StringBuffer buf = new StringBuffer(prfQs);
    buf.append(withPid  ? prfPidQc  : "");
    buf.append(withNode ? prfNdQc   : "");
    buf.append(withLd   ? prfLdQc   : "");
    return ""+buf;
  }
  
  /**
   * @deprecated
   * @see {@link ApiDb#_closeBatch(String)}
   */
  public static boolean closeBatch(String tid, java.sql.Date batchDate, int batchNumber, java.sql.Date autoDate) {
    return( (new ApiDb())._closeBatch(tid) );
  }
  
  /**
   * @deprecated
   * @see {@link ApiDb#_closeBatch(String)}
   */
  protected boolean _closeBatch(String profileId, Date batchDate, int batchNumber, Object object, int i) {
    return _closeBatch(profileId);
  }
  
  /**
   * Closes the current batch for this profile ID.  This uses today as the batch date, and the batch ID / batch number stored in the trident API profile.
   * @param tid Profile ID
   * @return true if the task was successful
   */
  public static boolean closeBatch(String tid) {
    return( (new ApiDb())._closeBatch(tid) );
  }
  
  protected boolean _closeBatch(String tid) {
    double                  delay         = 0;//(autoDate == null) ? 0 : 0.0035;    // IFF auto close, then 5 minute delay
    PreparedStatement       ps            = null;
    PreparedStatement       psBatch       = null;
    String                  qs            = null;
    int                     recCount      = 0;
    boolean                 retVal        = false;
    ResultSet               rs            = null;
    ResultSet               rsBatch       = null;
    profileLastBatchNumber = 0;
    profileBatchNumber = 0;
    profileBatchId = 0;
    long nextProfileBatchId = 0;
    int nextProfileBatchNum;
    
    int tranAge = 7;
    Date batchDate = new Date(System.currentTimeMillis());
    Date autoDate = new Date(System.currentTimeMillis());
    log.debug("Beginning batch close");
    
    // common where clause used to select open transactions
    String whereClause = 
      " where   tapi.terminal_id = ?                        " +   // This profile ID
      "         and tapi.transaction_date between           " + 
      "               trunc(sysdate-" + tranAge + ")        " +   // 
      "               and trunc(sysdate)                    " + 
      "         and tapi.mesdb_timestamp between            " +   // Authed within last 45 days
      "               (sysdate-45) and (sysdate-"+delay+")  " +
      "         and ( batch_id = ? OR batch_id is null )    " +   // We'll take any trans with the same batch_id as the profile, or an empty batch_id (the legacy way)
      "         and nvl(tapi.test_flag,'Y') = 'N'           " +   // Test flag is no
      "         and tapi.load_filename is null              " +   // No load filename
      "         and tapi.transaction_type in                " +   // Tran type can settle
      "         (                                           " + 
      "           select  tt.tran_type                      " + 
      "           from    trident_capture_api_tran_type tt  " + 
      "           where   nvl(tt.settle,'N') = 'Y'          " + 
      "         )                                           ";

    try {
      connect();
      setAutoCommit(false);
      
      // Check if Trident Profile has a batch ID / number
      {
        String q = "select nvl(current_batch_id, 0) as current_batch_id, nvl(current_batch_number, 0) as current_batch_number, last_batch_number from trident_profile_api where terminal_id = ?";
        ps = getPreparedStatement(q);
        ps.setString(1, tid);
        rs = ps.executeQuery();
        if(rs.next()) {
          profileBatchId = rs.getLong("current_batch_id");
          profileLastBatchNumber = rs.getInt("last_batch_number");
          profileBatchNumber = rs.getInt("current_batch_number");
          log.debug("Current USD Batch ID: "+profileBatchId);
          log.debug("Last Batch Num: "+profileLastBatchNumber+", Current Batch Num: "+profileBatchNumber);
          // The trident profile may not have a current batch number during migration. Increment from the previous batch number.
          if(profileBatchNumber == 0) {
            profileBatchNumber = profileLastBatchNumber+1;
            if(profileBatchNumber > 999)
              profileBatchNumber = 1;
            log.debug("No current batch number, incrementing from previous: "+profileBatchNumber);
          }
        }
        ps.close();
        rs.close();
      }
      
      // Update the Trident profile API batch number/id immediately for in-flight transactions and beyond.
      // Note: we have to revert this if we have to roll back
      {
        nextProfileBatchNum = profileBatchNumber+1;
        if(nextProfileBatchNum > 999)
          nextProfileBatchNum = 1;

        // Fetch a new Batch ID
        qs = "select trident_capture_test_sequence.nextval as batch_id from dual";
        ps = getPreparedStatement(qs);
        rs = ps.executeQuery();
        rs.next();
        nextProfileBatchId = rs.getLong("batch_id");
        rs.close();
        ps.close();

        qs = "update " + 
            "  trident_profile_api tpapi " + 
            "set " + 
            "  last_batch_date = ?, " + 
            "  last_batch_number = ?, " + 
            "  last_auto_close_date = nvl(?, last_auto_close_date), " + 
            "  current_batch_id = ?, " + 
            "  current_batch_number = ? " + 
            "where " + 
            "  tpapi.terminal_id = ?";

        ps = getPreparedStatement(qs);
        ps.setDate(1, batchDate);
        ps.setInt(2, profileBatchNumber);
        ps.setDate(3, autoDate);
        ps.setLong(4, nextProfileBatchId);
        ps.setInt(5, nextProfileBatchNum);
        ps.setString(6, tid);
        int updateCount = ps.executeUpdate();
        log.debug("Updating Trident Profile API with new batch ID/number: "+nextProfileBatchId+" / "+nextProfileBatchNum+".  "+updateCount+" updated (should be 1)");
        ps.close();

        // If we have set the last auto close date, don't set it again
        if (updateCount != 0)
          autoDate = null;
        commit();
      }

      // check for records in the batch
      qs =  " select  /*+ index(tapi idx_tapi_term_id) */     " +
            "         tapi.bank_number,                       " + 
            "         tapi.currency_code,                     " + 
            "         count(1)     as tran_count              " +
            " from    trident_capture_api    tapi             ";
      
      psBatch = getPreparedStatement( qs + whereClause + " group by tapi.bank_number,tapi.currency_code" );
      psBatch.setString(1, tid);
      psBatch.setLong(2, profileBatchId);
      rsBatch = psBatch.executeQuery();
      
      int batchesClosed = 0;
      // One loop per Profile / Currency
      while( rsBatch.next() ) {
        
        int bankNumber = rsBatch.getInt("bank_number");
        int tranCount = rsBatch.getInt("tran_count");
        String cc = rsBatch.getString("currency_code");
        long finalBatchId = profileBatchId;
        
        // If don't have a batch ID for this profile, or the currency is non-USD, manually close each transaction in the batch.
        if(profileBatchId == 0L || !"840".equals(cc)) {
          // Fetch a new Batch ID
          qs = "select trident_capture_test_sequence.nextval as batch_id from dual";
          ps = getPreparedStatement(qs);
          rs = ps.executeQuery();
          rs.next();
          long newBatchId = rs.getLong("batch_id");
          rs.close();
          ps.close();

          if(profileBatchId == 0L)
            log.debug("First batch - Manually closing via update ("+tranCount+" transactions, Currency Code "+cc+" batchId "+newBatchId+")");
          else
            log.debug("Non-USD - Manually closing via update ("+tranCount+" transactions, Currency Code "+cc+" batchId "+newBatchId+")");
          
          // update the pending records
          qs = " update  /*+ index(tapi idx_tapi_term_id) */ " +
            "         trident_capture_api   tapi          " + 
            " set     batch_date            = ?,          " + 
            "         batch_number          = ?,          " + 
            "         batch_id              = ?,          " +
            "         batch_close_timestamp = sysdate,    " +
            "         load_file_id          = -1,         " +
            "         ach_sequence          = 0           ";
          
          ps = getPreparedStatement( qs + whereClause + " and tapi.bank_number = ? and tapi.currency_code = ?" );
          ps.setDate(1, batchDate);
          ps.setInt(2, profileBatchNumber);
          ps.setLong(3, newBatchId); // Intentionally overwrite any existing batch ID, since we're non-USD
          ps.setString(4, tid);
          ps.setLong(5, profileBatchId);
          ps.setInt(6, bankNumber);
          ps.setString(7, cc);
          recCount = ps.executeUpdate();
          ps.close();
          log.debug("Updated "+recCount+" records (should be "+tranCount+")");
          finalBatchId = newBatchId; // Overwrite here too, so mbs_batches has the correct batchID
        }
        
        // Create the batch in mbs_batches
        if(tranCount > 0) {
          StringBuffer    tridentBatchId   = new StringBuffer();
          tridentBatchId.append(tid);
          tridentBatchId.append(".");
          tridentBatchId.append(DateTimeFormatter.getFormattedDate(batchDate,"MMdd"));
          tridentBatchId.append(".");
          tridentBatchId.append(StringUtilities.rightJustify(String.valueOf(profileBatchNumber),3,'0'));
          tridentBatchId.append("-");
          tridentBatchId.append(finalBatchId);
          
          qs = 
            " insert into mbs_batches " +
            " (                       " +
            "   bank_number,          " +
            "   merchant_number,      " +
            "   terminal_id,          " +
            "   merchant_batch_date,  " +
            "   batch_number,         " +
            "   batch_id,             " +
            "   trident_batch_id,     " +
            "   mbs_batch_type,       " +
            "   mesdb_timestamp,      " +
            "   detail_timestamp_min, " +
            "   detail_timestamp_max, " +
            "   test_flag,            " +
            "   response_code,        " +
            "   sales_count,          " +
            "   sales_amount,         " +
            "   credits_count,        " +
            "   credits_amount,       " +
            "   total_count,          " +
            "   net_amount,           " +
            "   currency_code,        " + 
            "   load_file_id          " +
            " )                       " +
            " select  tapi.bank_number,                           " +
            "         tapi.merchant_number,                       " +
            "         ?,?,?,?,?,                                  " +
            "         2,                                          " +
            "         sysdate,                                    " +
            "         min(mesdb_timestamp),                       " +
            "         max(mesdb_timestamp),                       " +
            "         'N',0,                                      " +
            "         sum(decode(debit_credit_indicator,'D',1,0))," +
            "         sum(decode(debit_credit_indicator,'D',1,0)  " +
            "             * transaction_amount),                  " +
            "         sum(decode(debit_credit_indicator,'C',1,0))," +
            "         sum(decode(debit_credit_indicator,'C',1,0)  " +
            "             * transaction_amount),                  " +
            "         count(1),                                   " +
            "         sum(decode(debit_credit_indicator,'C',-1,1) " +
            "             * transaction_amount),                  " +
            "         ?,                                          " +
            "         -1                                          " +
            " from    trident_capture_api   tapi                  " +
            " where   batch_id = ?                                " +
            "         and tapi.mesdb_timestamp between            " +
            "                (?-45) and (?+1)                     " +
            "         and nvl(tapi.test_flag,'Y') = 'N'           " + 
            "         and tapi.load_filename is null              " + 
            " group by tapi.bank_number,tapi.merchant_number      ";
          log.debug("Inserting into mbs_batches: "+profileBatchNumber+" / "+finalBatchId+" / "+cc);
          ps = getPreparedStatement(qs);
          int idx = 0;
          ps.setString(++idx,tid);
          ps.setDate  (++idx,batchDate);
          ps.setInt   (++idx,profileBatchNumber);
          ps.setLong  (++idx,finalBatchId);
          ps.setString(++idx,tridentBatchId.toString());
          
          ps.setString(++idx,cc);
          
          ps.setLong  (++idx,finalBatchId);
          ps.setDate  (++idx,batchDate);
          ps.setDate  (++idx,batchDate);
          ps.executeUpdate();
          ps.close();
          log.debug("Insert into mbs_batches completed: "+profileBatchNumber+" / "+finalBatchId+" / "+cc);
        }
        sendBatchCloseEmail(profileBatchId,batchDate,tid);
        batchesClosed++;
        log.debug("Email sent: "+profileBatchNumber+" / "+finalBatchId+" / "+cc);
      } // end loop through currency codes
      rsBatch.close();
      psBatch.close();
      
      if(batchesClosed == 0) {
        // roll back the batch id / number
        updateTAPIBatchDetails(tid, profileBatchId, profileBatchNumber);
      }
      
      log.debug("Batch close complete. Batch count: "+batchesClosed + " tid: " + tid);
      retVal = true;
      commit();
    } catch( Exception e ) {
      rollback();
      log.error("closeBatch("+tid+")", e);
      SyncLog.LogEntry("closeBatch("+tid+")", e);
      
      try {
        // Check for any in-flight transactions, and roll them down into the current batch. We don't want to deal with having 2 open batches.
        qs = " update  /*+ index(tapi idx_tapi_term_id) */ " +
          "         trident_capture_api   tapi          " + 
          " set     batch_number          = ?,          " + 
          "         batch_id              = ?           ";
        psBatch = getPreparedStatement(qs + whereClause);
        psBatch.setInt(1, profileBatchNumber);
        psBatch.setLong(2, profileBatchId);
        psBatch.setString(3, tid);
        psBatch.setLong(4, nextProfileBatchId);
        int updateCount = psBatch.executeUpdate();
        log.debug("Rolled in-flight transactions down into current batch. Updated: "+updateCount);
        
        // Revert tpapi current_batch_id/number
        updateTAPIBatchDetails(tid, profileBatchId, profileBatchNumber);
        commit();
      } catch(Exception ex) {
        ex.printStackTrace();
      }
    }
    finally {
      try {
        if (autoDate != null) {
        	log.debug("Updating auto close date for tid: " + tid);
          // always update the auto close date
          // so each TID will only try to auto
          // close one time per day
          qs = " update  trident_profile_api tpapi " +
            " set     last_auto_close_date = ?  " +
            " where   tpapi.terminal_id = ?     ";
          ps = getPreparedStatement(qs);
          ps.setDate  (1,autoDate);
          ps.setString(2,tid);
          ps.executeUpdate();
          ps.close();
          commit();
          log.debug("Committed all transactions for tid: " + tid);
        }
      } catch( Exception ee ) { ee.printStackTrace(); }
      setAutoCommit(true);
      try{ psBatch.close(); } catch( Exception ee ){}
      cleanUp(ps,rs);
    }
    return( retVal );
  }
  
  /**
   * Sets the current_batch_number and current_batch_id for a given profile in trident_profile_api.
   * @param tid
   * @param batchId
   * @param batchNumber
   */
  private void updateTAPIBatchDetails(String tid, long batchId, int batchNumber) {
    PreparedStatement ps = null;
    
    try {
      connect();
      log.debug("Reverting Trident Profile API batch ID/number to: "+profileBatchId+" / "+profileBatchNumber);
      String qs = "update " + 
          "  trident_profile_api tpapi " + 
          "set " + 
          "  last_batch_number = ?, " +  
          "  current_batch_id = ?, " + 
          "  current_batch_number = ? " + 
          "where " + 
          "  tpapi.terminal_id = ?";
      ps = getPreparedStatement(qs);
      ps.setInt(1, profileLastBatchNumber);
      ps.setLong(2, profileBatchId);
      ps.setInt(3, profileBatchNumber);
      ps.setString(4, tid);
      int updateCount = ps.executeUpdate();
      ps.close();
      log.debug("Updated TPAPI: "+updateCount+" rows (should be 1)");
    } catch(Exception ex) {
      ex.printStackTrace();
    } finally {
      cleanUp(ps);
    }
  }
  
  /**
   * When {@link ApiDb#closeBatch(String)} is called, it will attempt to close what the tpapi's profile has as the current batch number. <br />
   * This will return that batch number.  If we want to know the attempted batch number, we cannot rely on current/last batch number since these may have changed based on if the batch successfully closed or not.
   * @return The batch number {@link ApiDb#closeBatch(String)} attempted to close.
   */
  public int getAttemptedBatchCloseNumber() {
    return profileBatchNumber;
  }
  
  /**
   * When {@link ApiDb#closeBatch(String)} is called, it will attempt to close what the tpapi's profile has as the current batch id.
   * @return The batch id {@link ApiDb#closeBatch(String)} attempted to close.
   */
  public long getAttemptedBatchCloseId() {
    return profileBatchId;
  }
  
  public static boolean closeCAB( String profileId )
  {
    return( (new ApiDb())._closeCAB(profileId) );
  }
  
  protected boolean _closeCAB( String profileId )
  {
    Date                batchDate     = null;
    int                 batchNumber   = 1;
    PreparedStatement   ps            = null;
    boolean             retVal        = false;
    ResultSet           rs            = null;
    
    try
    {
      connect();
      
      // select a batch number 100 greater than the last 
      // batch for this profile in the last 5 days.
      // adding 100 to prevent conflict with any POS
      // devices using this profile id via the API.
      String sqlText = 
        " select  /*+                                              " +
        "             ordered                                      " +
        "             index(tp pk_trident_profile)                 " +
        "             index(tapi idx_tapi_merch_batch_date)        " +
        "         */                                               " +
        "         nvl(max(batch_number),0)+100    as batch_number, " +
        "         trunc(sysdate)                  as batch_date    " +
        " from    trident_profile       tp,                        " +
        "         trident_capture_api   tapi                       " +
        " where   tp.terminal_id = ?                               " +
        "         and tapi.merchant_number = tp.merchant_number    " +
        "         and tapi.batch_date >= (sysdate-5)               " +
        "         and tapi.test_flag = 'N'                         " +
        "         and tapi.terminal_id = tp.terminal_id            ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,profileId);
      rs = ps.executeQuery();
      rs.next();
      batchNumber = rs.getInt("batch_number"); 
      batchDate   = rs.getDate("batch_date");
      rs.close();
      ps.close();
        
      // batch close requires batch numbers between 1 and 999
      if ( batchNumber < 1 || batchNumber > 999 )
      {
        batchNumber = 1;
      }
      retVal = _closeBatch(profileId,batchDate,batchNumber,null,45);
    }
    catch( Exception e )
    {
      logEntry("closeCAB(" + profileId + "," + batchNumber + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }

  /**
   * Creates a new profile and loads data from the result set into it.
   */
  private TridentApiProfile createProfile(ResultSet rs)
    throws Exception
  {
    TridentApiProfile p = new TridentApiProfile();

    p.AchpCcdEnabled        = rs.getString ("achp_ccd_accept").toUpperCase().equals("Y");
    p.AchpPpdEnabled        = rs.getString ("achp_ppd_accept").toUpperCase().equals("Y");
    p.AchpWebEnabled        = rs.getString ("achp_web_accept").toUpperCase().equals("Y");
    p.AchpTelEnabled        = rs.getString ("achp_tel_accept").toUpperCase().equals("Y");
    p.ApiCreditsAllowed     = rs.getString ("api_credits_allowed").equals("Y");
    p.ApiEnabled            = rs.getString ("api_enabled").toUpperCase().equals("Y");
    p.BmlEnabled            = rs.getString ("bml_accept").toUpperCase().equals("Y");
    p.DbaAddrLine1          = rs.getString ("dba_addr_1");
    p.DbaCity               = rs.getString ("dba_city");
    p.DbaName               = rs.getString ("dba_name");
    p.DbaState              = rs.getString ("dba_state");
    p.DbaZip                = rs.getString ("dba_zip");
    p.FxClientId            = rs.getInt    ("fx_client_id");
    p.FxProductType         = rs.getInt    ("fx_product_type");
    p.FxEnabled             = rs.getString ("fx_enabled").equals("Y");
    p.MerchantId            = rs.getString ("merchant_number");
    p.PhoneNumber           = rs.getString ("phone_number");
    p.DMContactInfo         = rs.getString ("dm_contact_info");
    p.ProfileId             = rs.getString ("tid");
    p.ProfileKey            = rs.getString ("profile_key");
    p.SecondaryKey          = rs.getString ("secondary_key");
    p.ProfileStatus         = rs.getString ("profile_status");
    p.SicCode               = rs.getString ("sic_code");
    p.TipsAllowed           = rs.getString ("tips_allowed").equals("Y");
    p.TimeZone              = rs.getString ("time_zone");
    p.StipAllowed           = rs.getString ("stip_allowed").equals("Y");
    p.MultiCaptureAllowed   = rs.getString ("multi_capture_allowed").equals("Y");
    
    // bml promo codes
    for (int i = 1; i <= 5; ++i)
    {
      String promoCode = rs.getString("bml_promo_code_" + i);
      if (promoCode != null)
      {
        String merchId = rs.getString("bml_merch_id_" + i);
        p.BmlProducts.put(promoCode,merchId);
      }
    }

    p.setIntlProcessor( rs.getInt("intl_processor") );
    p.setIntlCredentials( rs.getString("intl_credentials") );
    p.setIntlUSDEnabled( "Y".equals(rs.getString("intl_usd")) );
    p.setUseVitalForAmexAuths( "Y".equals(rs.getString("use_vital_for_amex_auths")) );
    p.setCardStoreLevel(rs.getInt("card_store_level"));
    p.setDynamicDbaDataAllowed( "Y".equals(rs.getString("dynamic_dba_allowed")) );
    p.setDynamicDbaNamePrefix( rs.getString("dynamic_dba_prefix") );
    p.setDynamicDbaNamePrefixEditEnabled( "Y".equals(rs.getString("ddba_prefix_edit_enabled")) );
    p.setForceOnDecline( "Y".equals(rs.getString("force_on_decline")) );
    p.setFraudMode( rs.getInt("fraud_mode") );
    p.setSettleAmountValidationEnabled( "Y".equals(rs.getString("val_settle_amt")) );
    p.setHttpGetAllowed( "Y".equals(rs.getString ("get_allowed")) );
  
    for( int i = 0; i < p.CardTypeFieldNames.length; ++i )
    {
      p.CardTypesAccepted.put( p.CardTypeFieldNames[i],
                             rs.getString(p.CardTypeFieldNames[i]) );
    }
    
    p.setBinNumber( rs.getString("bin_number") );
    p.setAmexCIDEnabled(rs.getString("amex_cid_enabled"));
    p.setAmexCurrencyCode(rs.getString("amex_currency"));
    p.setAvsResultsToDecline(rs.getString("bad_avs_results"));
    p.setCvv2ResultsToDecline(rs.getString("bad_cvv2_results"));

    return p;
  }
  
  public static void extractAchTridentStatementRecord( String tid, Date batchDate, int batchNumber )
    throws Exception
  {
    (new ApiDb())._extractAchTridentStatementRecord(tid, batchDate, batchNumber, false);
  }
  
  public static void extractAchTridentStatementRecord( String tid, Date batchDate, int batchNumber, boolean testMode )
    throws Exception
  {
    (new ApiDb())._extractAchTridentStatementRecord(tid, batchDate, batchNumber, testMode);
  }
  
  protected void _extractAchTridentStatementRecord( String tid, Date batchDate, int batchNumber, boolean testMode )
    throws Exception
  {
    PreparedStatement   ps          = null;
    String              qs          = null;
    ResultSet           resultSet   = null;
    String              tableName   = (testMode ? "ach_trident_statement_test" : 
                                                  "ach_trident_statement");

    try
    {
      connect();
      
      qs =  
        " select  /*+                                                                         " + 
        "             ordered                                                                 " + 
        "             index(tapi idx_tapi_merch_batch_date)                                   " + 
        "         */                                                                          " + 
        "         tapi.batch_id                               as rec_id,                      " + 
        "         tapi.merchant_number                        as merchant_number,             " + 
        "         (? + nvl(mf.suspended_days,0))     as post_date,                            " + 
        "         ?                                           as entry_desc,                  " + 
        "         sum(decode(tapi.debit_credit_indicator,'D',1,0) *                           " + 
        "             decode(tapi.card_type,                                                  " + 
        "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),     " + 
        "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),    " + 
        "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0), " + 
        "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),     " + 
        "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),    " + 
        "                    'BL', 1,                                                         " + 
        "                     0 )                                                             " + 
        "            )                                        as sales_count,                 " + 
        "         sum(tapi.transaction_amount *                                               " + 
        "             decode(tapi.debit_credit_indicator,'D',1,0) *                           " + 
        "             decode(tapi.card_type,                                                  " + 
        "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),     " + 
        "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),    " + 
        "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0), " + 
        "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),     " + 
        "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),    " + 
        "                    'BL', 1,                                                         " + 
        "                     0 )                                                             " + 
        "            )                                        as sales_amount,                " + 
        "         sum(decode(tapi.debit_credit_indicator,'C',1,0) *                           " + 
        "             decode(tapi.card_type,                                                  " + 
        "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),     " + 
        "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),    " + 
        "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0), " + 
        "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),     " + 
        "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),    " + 
        "                    'BL', 1,                                                         " + 
        "                     0 )                                                             " + 
        "            )                                        as credits_count,               " + 
        "         sum(tapi.transaction_amount *                                               " + 
        "             decode(tapi.debit_credit_indicator,'C',1,0) *                           " + 
        "             decode(tapi.card_type,                                                  " + 
        "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),     " + 
        "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),    " + 
        "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0), " + 
        "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),     " + 
        "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),    " + 
        "                    'BL', 1,                                                         " + 
        "                     0 )                                                             " + 
        "            )                                        as credits_amount,              " + 
        "         sum(tapi.transaction_amount *                                               " + 
        "             decode(tapi.debit_credit_indicator,'C',-1,1) *                          " + 
        "             decode(tapi.card_type,                                                  " + 
        "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),     " + 
        "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),    " + 
        "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),   " + 
        "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0), " + 
        "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),     " + 
        "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),    " + 
        "                    'BL', 1,                                                         " + 
        "                     0 )                                                             " + 
        "            )                                        as net_amount                   " + 
        " from    trident_profile       tp,                                                   " + 
        "         trident_capture_api   tapi,                                                 " + 
        "         mif                   mf                                                    " + 
        " where   tp.terminal_id = ?                                                          " + 
        "         and tapi.merchant_number = tp.merchant_number                               " + 
        "         and tapi.batch_date = ?                                                     " + 
        "         and tapi.batch_number = ?                                                   " + 
        "         and tapi.terminal_id = tp.terminal_id                                       " + 
        "         and mf.merchant_number = tp.merchant_number                                 " + 
        " group by tapi.batch_id,tapi.merchant_number,nvl(mf.suspended_days,0)                ";
       
      ps = con.prepareStatement(qs);
      ps.setDate(1,batchDate);
      ps.setString(2,AchEntryData.ED_MERCH_DEP);
      ps.setString(3,tid);
      ps.setDate(4,batchDate);
      ps.setInt(5,batchNumber);
      resultSet = ps.executeQuery();
      
      while( resultSet.next() )
      {
        AchDb.storeAchStatementRecord(new AchStatementRecord(resultSet),tableName);
      }
      resultSet.close();
      ps.close();
    }
    finally
    {
      cleanUp(ps,resultSet);
    }      
  }
  
  public static long getCardStoreNodeId( long merchantId, int level )
  {
    return( (new ApiDb())._getCardStoreNodeId(merchantId, level) );
  }
  
  protected long _getCardStoreNodeId( long merchantId, int level )
  {
    PreparedStatement ps          = null;
    long              retVal      = merchantId;   // default to merchant
    ResultSet         rs          = null;
    
    try
    {
      connect();
      
      String sqlText = null;
      switch( level )
      {
        case 1:     // association
          sqlText = 
            " select  association_node  as node_id  " +
            " from    mif                           " + 
            " where   merchant_number = ?           ";
          break;
          
        case 2:     // association parent
          sqlText = 
            " select  op.org_group        as node_id            " +
            " from    mif               mf,                     " +
            "         organization      o,                      " +
            "         parent_org        po,                     " +
            "         organization      op                      " +
            " where   mf.merchant_number = ?                    " +
            "         and o.org_group = mf.association_node     " +
            "         and po.org_num = o.org_num                " +
            "         and op.org_num = po.parent_org_num        ";
          break;            
          
        default:    // default to merchant level
          retVal = merchantId;
          break;
      }
      
      if ( sqlText != null )
      {
        ps = getPreparedStatement(sqlText);
        ps.setLong(1,merchantId);
        rs = ps.executeQuery();
        rs.next();
        retVal = rs.getLong("node_id"); 
        rs.close();
        ps.close();
      }        
    }
    catch( Exception e )
    {
      logEntry("_getCardStoreNodeId(" + merchantId + "," + level + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  /**
   * Fetches a single profile with the given profile id and optionally
   * under the hierarchy node given.
   */
  public TridentApiProfile getProfile(String pid, String nodeId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      if (pid == null) throw new NullPointerException("Null profile ID");

      boolean withPid = pid != null;
      boolean withNode = nodeId != null;
      String qs = buildQueryString(withPid,withNode,false);

      connect();
      ps = con.prepareStatement(qs);
      if (withPid) ps.setString(1,pid);
      if (withNode) ps.setString(2,nodeId);
      rs = ps.executeQuery();

      if (rs.next())
      {
        return createProfile(rs);
      }
      log.warn("No profile found with profile id " + pid 
        + (withNode ? ", node " + nodeId : ""));
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }

    return null;
  }

  public TridentApiProfile getProfile(String pid)
  {
    return getProfile(pid,null);
  }
  
  /**
   * Fetches the profile ID associated with this hostName in the CORS hosts table.<br />
   * Wildcards can be associated with the sub-domain, and port number.<br />
   * Example database entries:
   * <ul>
   *    <li>www.example.com</li>
   *    <li>www.example.com:8080</li>
   *    <li>www.example.com:*</li>
   *    <li>*.example.com</li>
   *    <li>*.example.com:*</li>
   * </ul>
   * If multiple entries exist in the database, the priority returned is: Exact match, Match with wild port, Match with wild subdomain, then match with wild subdomain and port (The order of the examples above).
   * @param hostName No protocol or path elements in the hostName (Ex: www.example.com:8080)
   * @return Profile ID, or null if it doesn't exist.
   */
  public static String getCORSProfileId( String hostName) {
    return (new ApiDb())._getCORSProfileId(hostName);
  }
  
  /** @see ApiDb.getProfile(String) */
  public String _getCORSProfileId( String hostName) {
    PreparedStatement ps = null;
    ResultSet rs = null;
    String retProfile = null;

    String wildPort = hostName;         // www.example.com:*
    String wildSubdomain = hostName;    // *.example.com
    String wildSubAndPort = hostName;   // *.example.com:*

    // Add wild port
    int col = hostName.indexOf(':');
    if(col >= 0) {
      // Host has a port number. Trim it off and add wildcard.
      wildPort = hostName.substring(0, col) + ":*";
      wildSubAndPort = wildPort;
    }
    else {
      // Host has no port number. Just add wildcard.
      wildPort += ":*";
      wildSubAndPort = wildPort;
    }

    // Is the host an IP address or domain? Domains are difficult to determine, so we'll validate it as IP.
    if(Pattern.matches("^\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}(:\\d{1,5})?$", hostName)) {
      // ...
    }
    // Assume it's a domain name
    else {
      // Add wild port
      wildPort += ":*";
      
      // Add wild subdomain if it has a subdomain
      int dot = hostName.indexOf(".");
      if(dot >= 0) {
        wildSubdomain = "*"+hostName.substring(dot);
        wildSubAndPort = "*"+wildSubAndPort.substring(dot);
      }
    }

    try {
      // If separate entries, priority goes to the specific domain's profile ID, the wild port, then wild subdomain, then wild everything last.
      String q = "select " + 
          "  * " + 
          "from " + 
          "  trident_api_cors_hosts " + 
          "where " + 
          "  host_name = ? " + 
          "  or host_name = ? " + 
          "  or host_name = ? " + 
          "  or host_name = ? " + 
          "order by " + 
          "  host_name desc";

      connect();
      ps = con.prepareStatement(q);
      ps.setString(1, hostName);
      ps.setString(2, wildPort);
      ps.setString(3, wildSubdomain);
      ps.setString(4, wildSubAndPort);
      rs = ps.executeQuery();

      if(rs.next())
        retProfile = rs.getString("profile_id");

      log.debug("CORS hostName: "+hostName);
      log.debug("CORS wildPort: "+wildPort);
      log.debug("CORS wildSubDomain: "+wildSubdomain);
      log.debug("CORS wildSubDomain & Port: "+wildSubAndPort);
      log.debug("CORS Profile: "+retProfile);
      
      rs.close();
    }
    catch( Exception e ) {
      logEntry("_getCORSProfileId(" + hostName + ")",e.toString());
    }
    finally {
      cleanUp(ps,rs);
    }
    return( retProfile );
  }

  /**
   * Fetches a single profile.  Profile id is required, node id is optional.
   *    * Used by:
   *
   *   com.mes.reports.MerchUnsettledDataBean
   *   com.mes.tpg.TpgBaseDataBean
   * @param pid
   * @param nodeId
   * @return {@link TridentApiProfile} or null if profile does not exist.
   */
  public static TridentApiProfile loadProfile(String pid, String nodeId)
  {
    return (new ApiDb()).getProfile(pid,nodeId);
  }

  /**
   * Fetches a single profile.
   * @param pid Profile ID
   * @return {@link TridentApiProfile} or null if profile does not exist.
   */
  public static TridentApiProfile loadProfile(String pid)
  {
    return loadProfile(pid,null);
  }

  /**
   * Refreshes the test flag from the trident api properties file.
   * @param defaultValue
   * @return The test flag - Y or N
   */
  public static String loadTestFlag( boolean defaultValue )
  {
    return( (new ApiDb())._loadTestFlag(defaultValue) );
  }
  
  /**
   * Refreshes the test flag from the trident api properties file.
   * @param defaultValue
   * @return The test flag - Y or N
   */
  protected String _loadTestFlag( boolean defaultValue )
  {
    PropertiesFile  propsFile   = null;
    String          testFlag    = "Y";    // assume test
    
    try
    {
      propsFile = new PropertiesFile(TridentApiConstants.PropertiesFilename);
      
      // if true, then this is a production server and test flag is set to "N"
      testFlag  = (propsFile.getBoolean(TridentApiConstants.PROP_PRODUCTION_SERVER, defaultValue) ? "N" : "Y");
    }
    catch( Exception e )
    {
      logEntry("loadTestFlag()",e.toString());
    }      
    
    return( testFlag );
  }
  

  /**
   * Fetches all profiles modified after the given date and updates the map, adding/inserting
   * active profiles found and removing inactive/disabled profiles
   */
  public void doUpdateProfileMap(Timestamp lastDate, Map pMap)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    TridentApiProfile profile = null;

    try
    {
      String qs = buildQueryString(false,false,true);

      connect();
      ps = con.prepareStatement(qs);
      ps.setTimestamp(1,lastDate);
      rs = ps.executeQuery();

      ArrayList profiles = new ArrayList();
      while (rs.next())
      {
        String pid = rs.getString("tid");
        if (rs.getString("api_enabled").equals("Y") &&
            rs.getString("profile_status").equals("A") &&
            rs.getString("profile_key") != null)
        {
          profile = createProfile(rs);
          pMap.put(pid,profile);
        }
        else
        {
          pMap.remove(pid);
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      log.debug("profile id: " + profile.getProfileId());
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   *   Used by com.mes.api.ProfileSyncThread
   */
  public static void updateProfileMap(Timestamp lastDate, Map pMap)
  {
    (new ApiDb()).doUpdateProfileMap(lastDate,pMap);
  }
  
  public void sendBatchCloseEmail( long batchId, java.sql.Date batchDate, String tid )
  {
    String                  crlf      = "\r\n";
    String                  message   = null;
    PreparedStatement       ps        = null;
    ResultSet               rs        = null;
    
    try
    {
      String qs = 
        " select  /*+ ordered                                                                                         " + crlf +
        "             index( tp    pk_trident_profile       )                                                         " + crlf +
        "             index( tpa   pk_trident_profile_api   )                                                         " + crlf +
        "             index( tapi  idx_tapi_batch_id        )                                                         " + crlf +
        "             index( mb    pk_mbs_batches           )                                                         " + crlf +
        "         */                                                                                                  " + crlf +
        "         tp.terminal_id                                                                as profile_id,        " + crlf +
        "         tp.merchant_name                                                              as dba_name,          " + crlf +
        "         tpa.email_addresses                                                           as email_addresses,   " + crlf +
        "         to_char( mb.sales_count                                     ,  '999,999'    ) as sales_count,       " + crlf +
        "         to_char( mb.sales_amount                                    , '$999,999,999.99' ) as sales_amount,    " + crlf +
        "         to_char( mb.credits_count                                   ,  '999,999'    ) as credits_count,     " + crlf +
        "         to_char( mb.credits_amount                                  , '$999,999,999.99' ) as credits_amount,  " + crlf +
        "         to_char( mb.net_amount                                      , '$999,999,999.99' ) as net_amount,      " + crlf +
        "         to_char( sum( decode(tapi.card_type,'VS',tapi.transaction_amount,0) *                               " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as visa_amount,     " + crlf +
        "         to_char( sum( decode(tapi.card_type,'MC',tapi.transaction_amount,0) *                               " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as mc_amount,       " + crlf +
        "         to_char( sum( decode(tapi.card_type,                                                                " + crlf +
        "                              'VS',tapi.transaction_amount,                                                  " + crlf +
        "                              'MC',tapi.transaction_amount,                                                  " + crlf +
        "                              0) *                                                                           " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as vmc_amount,      " + crlf +
        "         to_char( sum( decode(tapi.card_type,'AM',tapi.transaction_amount,0) *                               " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as amex_amount,     " + crlf +
        "         to_char( sum( decode(tapi.card_type,'DS',tapi.transaction_amount,0) *                               " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as disc_amount,     " + crlf +
        "         to_char( sum( decode(tapi.card_type,'DB',tapi.transaction_amount,0) *                               " + crlf +
        "                       decode(tapi.debit_credit_indicator,'C',-1,1) ), '$999,999,999.99' ) as debit_amount     " + crlf +
        " from    trident_profile             tp,                                                                     " + crlf +
        "         trident_profile_api         tpa,                                                                    " + crlf +
        "         trident_capture_api         tapi,                                                                   " + crlf +
        "         mbs_batches                 mb                                                                      " + crlf +
        " where   tp.terminal_id                    = ?                                                               " + crlf +
        "     and tpa.terminal_id                   = ?                                                               " + crlf +
        "     and upper(tpa.email_batch_receipt)    = 'Y'                                                             " + crlf +
        "     and tpa.email_addresses               is not null                                                       " + crlf +
        "     and mb.batch_id                       = ?                                                               " + crlf +
        "     and mb.merchant_batch_date            = ?                                                               " + crlf +
        "     and tapi.batch_id                     = ?                                                               " + crlf +
        "     and tapi.mesdb_timestamp              between mb.detail_timestamp_min and mb.detail_timestamp_max       " + crlf +
        " group by tp.terminal_id, tp.merchant_name, tpa.email_addresses,                                             " + crlf +
        "          mb.sales_count, mb.sales_amount, mb.credits_count, mb.credits_amount, mb.net_amount                ";
          
      ps = con.prepareStatement(qs);
      int idx = 0;
      ps.setString(++idx,tid);
      ps.setString(++idx,tid);
      ps.setLong  (++idx,batchId);
      ps.setDate  (++idx,batchDate);
      ps.setLong  (++idx,batchId);
      rs = ps.executeQuery();
      
      if( rs.next() )
      {
        message =  
          "Batch Processed for Profile ID " + rs.getString("profile_id") + " - " + rs.getString("dba_name") + crlf + crlf +
          "Batch ID" +batchId + crlf +
          "Batch Totals" + crlf +
          "===================================================" + crlf +
          "Debits Count   :     " + rs.getString("sales_count") + crlf +
          "Debits Amount  : " + rs.getString("sales_amount") + crlf +
          "Credits Count  :     " + rs.getString("credits_count") + crlf +
          "Credits Amount : " + rs.getString("credits_amount") + crlf +
          "Net Amount     : " + rs.getString("net_amount") + 
          crlf + crlf +
          "Totals By Card Type" + crlf +
          "===================================================" + crlf +
          "Visa Net Amount             : " + rs.getString("visa_amount") + crlf +
          "MasterCard Net Amount       : " + rs.getString("mc_amount") + crlf +
          "Visa/MasterCard Net Amount  : " + rs.getString("vmc_amount") + crlf + crlf +
          "American Express Net Amount : " + rs.getString("amex_amount") + crlf +
          "Discover Net Amount         : " + rs.getString("disc_amount") + crlf +
          "Debit Net Amount            : " + rs.getString("debit_amount") + crlf + crlf;          
      
        MailMessage msg = new MailMessage();
        msg.setFrom("Merchant e-Solutions <noreply@merchante-solutions.com>");
        msg.addTo(rs.getString("email_addresses"));
        msg.setSubject("Credit Card Settlement Summary");
        msg.setText(message);
        //log.debug(message);
        msg.send();        
      }
      log.debug("Email sent: "+tid+" / "+batchId+" / "+batchDate);
      rs.close();
    }
    catch( Exception e )
    {
      logEntry("sendBatchCloseEmail(" + batchId + ")",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ){}
    }
  }
  
  public static boolean storeApiRequestLogEntry(ApiRequestLogEntry logEntry)
  {
    return( (new ApiDb())._storeApiRequestLogEntry(logEntry) );
  }
  
  public boolean _storeApiRequestLogEntry( ApiRequestLogEntry logEntry )
  {
    String            profileId   = null;
    PreparedStatement ps          = null;
    long              recId       = 0L;
    ResultSet         rs          = null;
    boolean           retVal      = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      if ( logEntry.isStoreReady() )
      {
        // check for auth.net emulation
        if ( logEntry.extractRequestParam(TridentApiConstants.FN_AN_VERSION) != null )
        {
          profileId = logEntry.extractRequestParam(TridentApiConstants.FN_AN_TID,20); 
        }
        else  // trident payment gateway request
        { 
          profileId = logEntry.extractRequestParam(TridentApiConstants.FN_TID,20);
        }
        
        // get the next sequence value
        ps = con.prepareStatement("select trident_api_log_sequence.nextval as rec_id from dual");
        rs = ps.executeQuery();
        rs.next();
        recId = rs.getLong("rec_id");
        rs.close();
        ps.close();
        
        String  requestParams= logEntry.getRequestParams();
        String  response     = logEntry.getResponse();
        String  retryId      = logEntry.getRetryId();
        String  tranType     = logEntry.getTranType();
        String  shortResp    = null;
        long    tranDuration = logEntry.getTranDuration(); 
        String  tridentTranId= logEntry.extractResponseParam(TridentApiConstants.FN_TRAN_ID,32);
        
        // prevent column overflows
        if ( response != null )
        {
          shortResp = response.substring(0,Math.min(response.length(),4000));
        }
        if ( requestParams != null && requestParams.length() > 4000 )
        {
          requestParams = requestParams.substring(0,4000);
        }
        if ( tranType != null && tranType.length() > 64 )
        {
          tranType = tranType.substring(0,64);
        }
        tranDuration = ((tranDuration < 0L) ? 0L : tranDuration);
        retryId = ((retryId != null && retryId.length() <= TridentApiConstants.FLM_RETRY_ID) ? retryId : null);
        
        // create the insert statement.  add empty blob if response
        // is larger than the max field length
        String  qs  = logInsertQs +
                      ( (response != null && response.length() > 4000) ? 
                          logWithBlobQc : logWithoutBlobQc );
          
        ps = con.prepareStatement(qs);
        ps.setLong(1,recId);
        ps.setString(2,profileId);
        ps.setTimestamp(3,logEntry.getRequestTimestamp());
        ps.setString(4,logEntry.getRequestMethod());
        ps.setString(5,requestParams);
        ps.setString(6,shortResp);
        ps.setString(7,logEntry.getServerName());
        ps.setString(8,retryId);
        ps.setString(9,tranType);
        if ( tranDuration == 0L )
        {
          ps.setNull(10,java.sql.Types.NUMERIC);
        }
        else
        {
          ps.setLong(10,tranDuration);
        }
        ps.setLong(11,logEntry.getSwitchDuration());
        ps.setString(12,tridentTranId);
        ps.setString(13,logEntry.getRetrievalRefNum());
        ps.setString(14,logEntry.getCurrencyCode());
        ps.setString(15,logEntry.getClientIpAddress());
        ps.executeUpdate();
        
        if ( response != null && (shortResp.length() != response.length()) )
        {
          Blob          blobData      = null;
          OutputStream  bOut          = null;
          
          qs =  " select  response_full     as blob_data  " +
                " from    trident_api_access_log  al      " +
                " where   al.rec_id = ?                   " +
                " for update                              ";
                
          ps = con.prepareStatement(qs);
          ps.setLong(1,recId);
          rs = ps.executeQuery();
          rs.next();
          blobData = rs.getBlob("blob_data");
          try
          {
            bOut = blobData.setBinaryStream(1L);
            bOut.write( response.getBytes(), 0, (response.getBytes()).length );
          }
          catch (Exception e)
          {
            logEntry("_storeApiRequestLogEntry(writing binary data)",
              e.toString());
          }
          finally
          {
            // flush and close the output buffer
            try { bOut.flush(); } catch (Exception e) { }
            try { bOut.close(); } catch (Exception e) { }
          }
          
          rs.close();
          ps.close();
        }
        con.commit();
        retVal = true;
      }        
    }
    catch( Exception e )
    {
      try{ con.rollback(); } catch( Exception ee ) {}
      logEntry("_storeApiRequestLogEntry()",e.toString());
      retVal = false;
    }
    finally
    {
      setAutoCommit(true);
      cleanUp(ps,rs); 
    }
    return( retVal );
  }
  
  
  public static boolean storeLogEntry( Object logEntry )
  {
    return( (new ApiDb())._storeLogEntry(logEntry) );
  }
  
  public boolean _storeLogEntry( Object logEntry )
  {
    boolean       retVal    = false;
    
    if ( logEntry instanceof ApiRequestLogEntry )
    {
      retVal = _storeApiRequestLogEntry( (ApiRequestLogEntry)logEntry );
    }
    return( retVal );
  }
  
  /**
   * Suspends the current thread until tranId is present in trident_capture_api, or until 10s have passed.
   * @param tranId
   * @param tranType
   * @return True when tranId is present in trident_capture_api.
   */
  public static boolean waitForUpdate( String tranId, String tranType )
  {
    return( (new ApiDb())._waitForUpdate(tranId,tranType,10000L) );
  }
  
  /**
   * Suspends the current thread until tranId is present in trident_capture_api, or timeout has passed.
   * @param tranId
   * @param tranType
   * @param timeout
   * @return True when tranId is present in trident_capture_api.
   */
  public static boolean waitForUpdate( String tranId, String tranType, long timeout )
  {
    return( (new ApiDb())._waitForUpdate(tranId,tranType,timeout) );
  }
  
  /**
   * Suspends the current thread until tranId is present in trident_capture_api, or timeout has passed.
   * @param tranId
   * @param tranType
   * @param timeout
   * @return True when tranId is present in trident_capture_api.
   */
  public boolean _waitForUpdate( String tranId, String tranType, long timeout )
  {
    PreparedStatement   ps        = null;
    ResultSet           rs        = null;
    boolean             retVal    = false;
    long                startTs   = System.currentTimeMillis();
    
    try
    {
      connect();
      
      if ( tranId != null )
      {
        String sqlText =
          " select  count(1)    as rec_count                  " +
          " from    trident_capture_api   tapi                " +
          " where   tapi.trident_tran_id = ?                  " +
          "         and tapi.transaction_type = ?             " +
          "         and tapi.mesdb_timestamp >= (sysdate-60)  ";
        
        ps = getPreparedStatement(sqlText);        
    
        while( true )
        {
          ps.setString(1,tranId);
          ps.setString(2,tranType);
          rs = ps.executeQuery();
          rs.next();
          retVal = (rs.getInt("rec_count") != 0);
          rs.close();
        
          if ( retVal || ((System.currentTimeMillis() - startTs) >= timeout) )
          {
            break;
          }
          Thread.sleep(250);  // delay before retrying
        }        
      }        
    }
    catch( Exception e )
    {
      logEntry("_waitForUpdate(" + tranId + "," + tranType + "," + timeout + ")",e.toString());
    }
    finally
    {
      cleanUp(ps,rs); 
    }
    return( retVal );
  }
}
