/*@lineinfo:filename=ForeignExchangeRate*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/ForeignExchangeRate.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import masthead.util.XOMSerializer;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Serializer;
import sqlj.runtime.ResultSetIterator;

public class ForeignExchangeRate extends SQLJConnectionBase
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  private String      ConsumerCurrencyCode    = null;
  private String      ConsumerCurrencyCountry = null;
  private String      ConsumerCurrencyDesc    = null;
  private Timestamp   ExpirationDate          = null;
  private int         FxClientId              = 0;
  private int         FxMerchantId            = 0;
  private String      MerchantCurrencyCode    = null;
  private long        MerchantId              = 0L;
  private double      Rate                    = 0.0;
  private int         RateId                  = 0;
  private boolean     Valid                   = false;
  
  public ForeignExchangeRate( ResultSet resultSet )
    throws java.sql.SQLException
  {
    MerchantId    = resultSet.getLong("merchant_number");
    RateId        = resultSet.getInt("rate_id");
    setData(resultSet);
  }
  
  public ForeignExchangeRate( String merchantId, int rateId )
  {
    this( Long.parseLong(merchantId), rateId );
  }
  
  public ForeignExchangeRate( long merchantId, int rateId )
  {
    MerchantId  = merchantId;
    RateId      = rateId;
    load();
  }
  
  public double convertAmount( double amount )
  {
    return( (amount * getRate()) );
  }
  
  public double convertAmountToBase( double amount )
  {
    return( (amount / getRate()) );
  }
  
  public boolean isExpired()
  {
    return( Calendar.getInstance().getTime().after( ExpirationDate ) );
  }
  
  public boolean isExpiredRecurring()
  {
    Calendar  cal   = Calendar.getInstance();
    int       days  = 90;
    
    try
    {
      days = MesDefaults.getInt(MesDefaults.DK_FX_RATE_RECURRING_EXTENSION);
    }
    catch( Exception e )
    {
      // ignore, just use the default number of days
    }
    cal.setTime(ExpirationDate);
    cal.add( Calendar.DAY_OF_MONTH, days );
    
    return( Calendar.getInstance().getTime().after( cal.getTime() ) );
  }
  
  public boolean isValid()
  {
    return( Valid );
  }
  
  public String getConsumerCurrencyCode()
  {
    return( ConsumerCurrencyCode );
  }
  
  public String getConsumerCurrencyCountry()
  {
    return( ConsumerCurrencyCountry );
  }
  
  public String getConsumerCurrencyDesc()
  {
    return( ConsumerCurrencyDesc );
  }
  
  public Timestamp getExpirationDate()
  {
    return( ExpirationDate );
  }
  
  public String getExpirationDateStr()
  {
    return( DateTimeFormatter.getFormattedDate(ExpirationDate,"MM/dd/yyyy HH:mm:ss zzz") );
  }
  
  public int getFxClientId()
  {
    return( FxClientId );
  }
  
  public int getFxMerchantId()
  {
    return( FxMerchantId );
  }
  
  public String getMerchantCurrencyCode()
  {
    return( MerchantCurrencyCode );
  }
  
  public long getMerchantId()
  {
    return( MerchantId );
  }
  
  public double getRate()
  {
    return( Rate );
  }
  
  public int getRateId()
  {
    return( RateId );
  }
  
  protected Element getXmlRootElement()
  {
    Element               field   = null;
    Element               root    = null;
    
    root = new Element("FXRate");
    
    field = new Element("RateId");
    field.appendChild( String.valueOf(getRateId()) );
    root.appendChild( field );
    
    field = new Element("MerchantCurrencyCode");
    field.appendChild( getMerchantCurrencyCode() );
    root.appendChild( field );
    
    field = new Element("ConsumerCurrencyCode");
    field.appendChild( getConsumerCurrencyCode() );
    root.appendChild( field );
    
    field = new Element("ConsumerCurrencyCountry");
    field.appendChild( getConsumerCurrencyCountry() );
    root.appendChild( field );
    
    field = new Element("ConsumerCurrencyDesc");
    field.appendChild( getConsumerCurrencyDesc() );
    root.appendChild( field );
    
    field = new Element("ExpirationDate");
    field.appendChild( getExpirationDateStr() );
    root.appendChild( field );
    
    field = new Element("Rate");
    field.appendChild( String.valueOf(getRate()) );
    root.appendChild( field );
    
    return( root );
  }
  
  public void load()
  {
    int                     hours       = 24;
    ResultSetIterator       it          = null;
    ResultSet               resultSet   = null;
    
    try
    {
      reset();
      connect();
      
      hours = MesDefaults.getInt(MesDefaults.DK_FX_RATE_EXPIRATION_EXTENSION);
      
      /*@lineinfo:generated-code*//*@lineinfo:225^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fxt.merchant_number                 as merchant_number,
//                  fxt.merchant_id                     as merchant_id,
//                  fxt.client_id                       as client_id,
//                  fxt.rate_id                         as rate_id,
//                  fxt.merchant_currency_code          as merchant_currency_code,
//                  fxt.consumer_currency_code          as consumer_currency_code,
//                  fxt.consumer_currency_country       as consumer_currency_country,
//                  fxt.consumer_currency_desc          as consumer_currency_desc,
//                  (fxt.expiration_date + (:hours/24)) as expiration_date,
//                  fxt.rate                            as rate
//          from    fx_rate_tables  fxt
//          where   fxt.merchant_number = :MerchantId
//                  and fxt.rate_id = :RateId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fxt.merchant_number                 as merchant_number,\n                fxt.merchant_id                     as merchant_id,\n                fxt.client_id                       as client_id,\n                fxt.rate_id                         as rate_id,\n                fxt.merchant_currency_code          as merchant_currency_code,\n                fxt.consumer_currency_code          as consumer_currency_code,\n                fxt.consumer_currency_country       as consumer_currency_country,\n                fxt.consumer_currency_desc          as consumer_currency_desc,\n                (fxt.expiration_date + ( :1 /24)) as expiration_date,\n                fxt.rate                            as rate\n        from    fx_rate_tables  fxt\n        where   fxt.merchant_number =  :2 \n                and fxt.rate_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.ForeignExchangeRate",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,hours);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setInt(3,RateId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.ForeignExchangeRate",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        setData(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadRate()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp(); 
    }
  }
  
  protected void reset()
  {
    ConsumerCurrencyCode    = null;  
    ConsumerCurrencyCountry = null;  
    ConsumerCurrencyDesc    = null;  
    ExpirationDate          = null;  
    MerchantCurrencyCode    = null;  
    Rate                    = 0.0;  
    Valid                   = false;  
  }
  
  protected void setData( ResultSet resultSet )
  {
    try
    {
      MerchantId              = resultSet.getLong("merchant_number");
      FxClientId              = resultSet.getInt("client_id");
      FxMerchantId            = resultSet.getInt("merchant_id");
      RateId                  = resultSet.getInt("rate_id");
      MerchantCurrencyCode    = resultSet.getString("merchant_currency_code");
      ConsumerCurrencyCode    = resultSet.getString("consumer_currency_code");
      ConsumerCurrencyCountry = resultSet.getString("consumer_currency_country");
      ConsumerCurrencyDesc    = resultSet.getString("consumer_currency_desc");
      ExpirationDate          = resultSet.getTimestamp("expiration_date");
      Rate                    = resultSet.getDouble("rate");
      Valid                   = true;
    }
    catch( Exception e )
    {
      logEntry( "setData()", e.toString() );
    }      
  }
  
  public void showData( )
  {
    System.out.println("MerchantId              : " + MerchantId);
    System.out.println("FxMerchantId            : " + FxMerchantId);
    System.out.println("RateId                  : " + RateId);
    System.out.println("MerchantCurrencyCode    : " + MerchantCurrencyCode);
    System.out.println("ConsumerCurrencyCode    : " + ConsumerCurrencyCode);
    System.out.println("ConsumerCurrencyCountry : " + ConsumerCurrencyCountry);
    System.out.println("ConsumerCurrencyDesc    : " + ConsumerCurrencyDesc);
    System.out.println("ExpirationDate          : " + ExpirationDate);
    System.out.println("Rate                    : " + Rate);
    System.out.println("Valid                   : " + Valid);
  }
  
  public String toXml( )
  {
    String                retVal  = null;
  
    try
    {
      Document              doc     = new Document( getXmlRootElement() );
      ByteArrayOutputStream xmlOut  = new ByteArrayOutputStream();
    
      Serializer serializer = new XOMSerializer(xmlOut, "ISO-8859-1");
    
      serializer.setIndent(1);    // enable std indentation
      serializer.write(doc);
      retVal = xmlOut.toString();
    }
    catch( Exception e )
    {
      logEntry("toXml()",e.toString());
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    ForeignExchangeRate   rate = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      long merchantId = 0L;
      rate = new ForeignExchangeRate(merchantId,31309);
      System.out.println(rate.toXml());
    }
    catch(Exception e )
    {
      System.out.println(e.toString());
    }
    
    
  }
}/*@lineinfo:generated-code*/