/*************************************************************************
 * 
 * FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/ApiMCLineItemDetailRecord.java $
 * 
 * Description:
 * 
 * 
 * Last Modified By : $Author: jduncan $
 * Last Modified Date : $Date: 2009-08-26 15:03:29 -0700 (Wed, 26 Aug 2009) $
 * Version : $Revision: 16428 $
 * 
 * Change History:
 * See SVN database
 * 
 * Copyright (C) 2009 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * 
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc. This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 * 
 **************************************************************************/
package com.mes.api;

import org.apache.commons.lang3.StringUtils;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import dto.McLineItem;
import masthead.formats.visak.MastercardLineItemDetailRecord;

public class ApiMCLineItemDetailRecord extends MastercardLineItemDetailRecord implements ApiLineItemDetailRecord {
	public ApiMCLineItemDetailRecord() {
	}

	public ApiMCLineItemDetailRecord(String rawData, Object bo) {
		super();
		setRawLineItemData(rawData, bo);

	}

	public void setRawLineItemData(String rawData, Object bo) {
		String[] items = null;
		McLineItem mcBO = (McLineItem) bo;
		
		if (StringUtils.isBlank(rawData)) {
			items = new String[13];
		}
		else
			items = (rawData + " ").split(TridentApiConstants.LINE_ITEM_DETAIL_FS_REGEX);

		setItemDescription(StringUtilities.leftJustify(StringUtilities.dataSwap(items[0], mcBO.getItemDescription()), 35, ' '));
		setProductCode(StringUtilities.leftJustify(StringUtilities.dataSwap(items[1], mcBO.getProductCode()), 12, ' '));
		setItemQuantity(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[2], mcBO.getItemQuantity())), "000000000000"));
		setItemUnitOfMeasure(StringUtilities.leftJustify(StringUtilities.dataSwap(items[3], mcBO.getItemUnitMeasure()), 12, ' '));
		setAlternateTaxIdentifier(StringUtilities.leftJustify(StringUtilities.dataSwap(items[4], mcBO.getAlternateTaxIdentifier()), 15, ' '));
		setTaxRateApplied(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[5], mcBO.getTaxRateApplied())) * 100, "0000"));
		setTaxTypeApplied(StringUtilities.leftJustify(StringUtilities.dataSwap(items[6], mcBO.getTaxTypeApplied()), 4, ' '));
		setTaxAmount(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[7], mcBO.getTaxAmount())) * 100, "000000000000"));
		setDiscountIndicator(StringUtilities.dataSwap(items[8], mcBO.getDiscountIndicator()).trim().substring(0, 1));
		setNetOrGrossIndicator(StringUtilities.dataSwap(items[9], mcBO.getNetOrGrossIndicator()).trim().substring(0, 1));
		setExtendedItemAmount(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[10], mcBO.getExtendedItemAmount())) * 100, "000000000"));
		setDebitOrCreditIndicator(StringUtilities.dataSwap(items[11], mcBO.getDebitCreditIndicator()).trim().substring(0, 1));
		setDiscountAmount(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[12], mcBO.getItemDiscountAmount())) * 100, "000000000000"));

	}

	@Override
	public void setRawLineItemData(String rawData) throws Exception {
		// TODO Auto-generated method stub

	}
}
