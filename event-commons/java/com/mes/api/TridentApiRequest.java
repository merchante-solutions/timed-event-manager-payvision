/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/TridentApiRequest.java $

  Description:
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.DateTimeFormatter;

public class TridentApiRequest
{
  static Logger log = Logger.getLogger(TridentApiRequest.class);

  private ParameterEncoder          parms         = null;
  private URL                       postUrl       = null;
  
  public TridentApiRequest()
  {
    try {
    setPostUrl(MesDefaults.getString(MesDefaults.TRIDENT_API_REQUEST_URL));
    } catch (Exception e) {
      log.error("TridentApiRequest() constructor - getting URL Request: "+ e.toString());
    }
    resetParms();
  }
  

  public TridentApiRequest(String urlStr)
  {
    setPostUrl(urlStr);
    resetParms();
  }

  public void setPostUrl(String urlStr)
  {
    try
    {
      postUrl = new URL(urlStr);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException("Error: " + e);
    }
  }

  public void resetParms()
  {
    parms = new ParameterEncoder();
  }

  public void addArg(String name, String value)
  {
    if (parms == null) resetParms();
    if (value == null || value.equals("")) return;
    parms.add(name,value);
  }

  public String getEncodedParms()
  {
    if (parms == null) resetParms();
    return parms.encode();
  }

  public TridentApiResponse post()
  {
    try
    {
      URLConnection con = postUrl.openConnection();
      con.setDoInput(true);
      con.setDoOutput(true);
      con.setUseCaches(false);
      con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
      OutputStream out = con.getOutputStream();
      String encodedParms = getEncodedParms();
      out.write(encodedParms.getBytes());
      out.flush();
      out.close();

      log.debug("Sending request to TPG: " + postUrl);

      BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String recvData = null;
      StringBuffer dataBuf = new StringBuffer();
      while (null != (recvData = in.readLine()))
      {
        dataBuf.append(recvData);
      }
      in.close();

      log.debug("Received TPG response: " + dataBuf);

      return( new TridentApiResponse(""+dataBuf) );
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException("Error: " + e);
    }
  }
  
  public void setArg(String name, double value)
  {
    setArg(name,String.valueOf(value));
  }
  
  public void setArg(String name, int value)
  {
    setArg(name,String.valueOf(value));
  }
  
  public void setArg(String name, long value)
  {
    setArg(name,String.valueOf(value));
  }
  
  public void setArg(String name, Date value)
  {
    setArg(name,value,"MM/dd/yyyy");
  }
  
  public void setArg(String name, Date value, String format)
  {
    setArg(name,DateTimeFormatter.getFormattedDate(value,format));
  }
  
  public void setArg(String name, String value)
  {
    if (parms == null) resetParms();
    if (value == null || value.equals("")) return;
    parms.put(name,value);
  }

  public String toString()
  {
    return( "TridentApiRequest [ " + getEncodedParms() + " ]" );
  }
}