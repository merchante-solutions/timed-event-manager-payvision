/*************************************************************************

  FILE: $Archive: /java/beans/com/mes/api/TridentApiProfile.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import com.mes.constants.mesConstants;

public class TridentApiProfile implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;
  
  private static int  CT_VMC    = 0;
  private static int  CT_AMEX   = 1;
  private static int  CT_DISC   = 2;
  private static int  CT_DINR   = 3;
  private static int  CT_JCB    = 4;
  
  public static final String[] CardTypeFieldNames = 
  {
    "vmc_accept",      // CT_VMC   
    "amex_accept",     // CT_AMEX  
    "disc_accept",     // CT_DISC  
    "dinr_accept",     // CT_DINR  
    "jcb_accept",      // CT_JCB   
  };
  
  public    boolean   AchpCcdEnabled                  = false;
  public    boolean   AchpPpdEnabled                  = false;
  public    boolean   AchpWebEnabled                  = false;
  public    boolean   AchpTelEnabled                  = false;
  public    boolean   ApiCreditsAllowed               = false;
  public    boolean   ApiEnabled                      = false;
  protected boolean   AmexCIDEnabled                  = false;
  private   String    AmexCurrencyCode                = "840";
  protected boolean   AssocCardStoreAllowed           = false;
  public    HashSet   AvsResultsToDecline             = new HashSet();
  protected String    BinNumber                       = null;
  public    boolean   BmlEnabled                      = false;
  public    HashMap   BmlProducts                     = new HashMap();
  protected int       CardStoreLevel                  = 0;
  public    HashMap   CardTypesAccepted               = new HashMap();
  public    HashSet   Cvv2ResultsToDecline            = new HashSet();
  public    String    DbaAddrLine1                    = null;
  public    String    DbaCity                         = null;
  public    String    DbaName                         = null;
  public    String    DbaState                        = null;
  public    String    DbaZip                          = null;
  public    String    DMContactInfo                   = null;
  protected boolean   DynamicDbaDataAllowed           = false;
  protected String    DynamicDbaNamePrefix            = null;
  protected boolean   DynamicDbaNamePrefixEditEnabled = true;
  protected boolean   ForceOnDecline                  = false;
  protected int       FxClientId                      = 0;
  protected int       FxProductType                   = 0;
  protected boolean   FxEnabled                       = false;
  protected boolean   HttpGetAllowed                  = false;
  protected int       IntlProcessor                   = 0;
  protected String[]  IntlCredentials                 = null;
  protected String    IntlCredentialsDelimiter        = ",";
  protected boolean   IntlUSDEnabled                  = false;
  protected boolean   IntlUSD                         = false;
  public    boolean   LoadBMLInfo                     = false;
  public    String    MerchantId                      = null;
  protected boolean   MultiCaptureAllowed             = false;
  public    String    PhoneNumber                     = null;
  public    String    ProfileId                       = null;
  public    String    ProfileKey                      = null;
  public    String    ProfileStatus                   = null;
  protected int       FraudMode                       = TridentApiConstants.FM_NONE;
  public    String    SecondaryKey                    = null;
  protected boolean   SettleAmountValidationEnabled   = false;
  public    String    SicCode                         = null;
  public    boolean   StipAllowed                     = false;
  public    String    TimeZone                        = null;
  public    boolean   TipsAllowed                     = false;
  public    boolean   UseVitalForAmexAuths            = false;

  // profile creation has been consolidated into com.mes.api.ApiDb
  public TridentApiProfile()
  {
  }

  public boolean acceptsCardType( String cardType )
  {
    boolean     retVal      = false;
    
    try
    {
      int ct = -1;
      
      if ( cardType.equals("VS") || cardType.equals("MC") )
      {
        ct = CT_VMC;
      }
      else if ( cardType.equals("AM") )
      {
        ct = CT_AMEX;
      }
      else if ( cardType.equals("DS") || cardType.equals("JC") )
      {
        ct = CT_DISC;
      }
      else if ( cardType.equals("DC") )
      {
        ct = CT_DINR;
      }
      retVal = ((String)CardTypesAccepted.get(CardTypeFieldNames[ct])).equals("Y");
    }
    catch( Exception e )
    {
      // ignore, return false
    }
    
    return( retVal );
  }
  
  public boolean acceptsCurrency( String currencyCode )
  {
    boolean     retVal      = false;
    
    try
    {
      if ( currencyCode.equals( TridentApiConstants.FV_CURRENCY_CODE_USD ) )
      {
        retVal = true;
      }
      else if ( getIntlProcessor() != mesConstants.INTL_PID_NONE )
      {
        retVal = true;
      }
    }
    catch( Exception e )
    {
      // assume false
    }
    
    return( retVal );
  }
  
  public boolean creditsAllowed()
  {
    return( ApiCreditsAllowed );
  }
  
  public boolean forceOnDecline()
  {
    return( ForceOnDecline );
  }
  
  public String getAmexCurrencyCode() {
    if(AmexCurrencyCode == null)
      return "840";
    return AmexCurrencyCode;
  }
  
  public String getBmlMerchId(String promoCode)
  {
    return ( (String)BmlProducts.get(promoCode) );
  }

  public Collection getBmlMerchIdSet()
  {
    return BmlProducts.values();
  }

  public Set getBmlPromoCodeSet()
  {
    return BmlProducts.keySet();
  }
  
  public String getBinNumber( )
  {
    return( BinNumber );
  }
  
  public int getCardStoreLevel()
  {
    return( CardStoreLevel );
  }
  
  public String getDbaAddrLine1( )
  {
    return( DbaAddrLine1 );
  }
  
  public String getDbaCity( )
  {
    return( DbaCity );
  }
  
  public String getDbaName( )
  {
    return( DbaName );
  }
  
  public String getDbaState( )
  {
    return( DbaState );
  }
  
  public String getDbaZip( )
  {
    return( DbaZip );
  }
  
  public String getDMContactInfo()
  {
    return( DMContactInfo );
  }
  
  public String getDynamicDbaNamePrefix()
  {
    return( DynamicDbaNamePrefix );
  }
  
  public int getFxClientId( )
  {
    return( FxClientId );
  }
  
  public int getFxProductType( )
  {
    return( FxProductType );
  }
  
  public int getIntlProcessor( )
  {
    return( IntlProcessor );
  }
  
  public String[] getIntlCredentials( )
  {
    return(IntlCredentials);
  }
  
  public String getIntlCredential( int index )
  {
    String        retVal      = null;
    
    if ( IntlCredentials != null && index < IntlCredentials.length )
    {
      retVal = IntlCredentials[index];
    }
    return( retVal );
  }
  
  public String getIntlCredentialsDelimiter( )
  {
    return(IntlCredentialsDelimiter); 
  }
  
  public String getMerchantId( )
  {
    return( MerchantId );
  }
  
  public String getPhoneNumber( )
  {
    return( PhoneNumber );
  }
  
  public String getProfileId( )
  {
    return( ProfileId );
  }
  
  public String getProfileKey( )
  {
    return( ProfileKey );
  }
  
  public String getProfileStatus( )
  {
    return( ProfileStatus );
  }
  
  public int getFraudMode()
  {
    return( FraudMode );
  }

  public String getSecondaryKey( )
  {
    return( SecondaryKey );
  }
  
  public String getSicCode( )
  {
    return( SicCode );
  }
  
  public String getTimeZone( )
  {
    return( TimeZone );
  }
  
  public boolean isAchEnabled()
  {
    return  isAchpCcdEnabled() || 
            isAchpPpdEnabled() || 
            isAchpWebEnabled() || 
            isAchpTelEnabled();
  }

  public boolean isAchpCcdEnabled()
  {
    return( AchpCcdEnabled );
  }

  public boolean isAchpPpdEnabled()
  {
    return( AchpPpdEnabled );
  }

  public boolean isAchpWebEnabled()
  {
    return( AchpWebEnabled );
  }

  public boolean isAchpTelEnabled()
  {
    return( AchpTelEnabled );
  }

  public boolean isAdyenEnabled()
  {
    return( getIntlProcessor() == mesConstants.INTL_PID_ADYEN );
  }
  
  public boolean isAmexCIDEnabled( )
  {
    return( AmexCIDEnabled );
  }
  
  public boolean isAssocCardStoreAllowed()
  {
    return( AssocCardStoreAllowed );
  }
  
  public boolean isAvsResultDeclined( String avsResult )
  {
    boolean   retVal    = false;
    
    if ( avsResult != null && !avsResult.equals("") )
    {
      retVal = AvsResultsToDecline.contains(avsResult);
    }
    return( retVal );
  }
  
  public boolean isBmlEnabled()
  {
    return( BmlEnabled );
  }
  
  public boolean isCvv2ResultDeclined( String cvv2Result )
  {
    boolean   retVal    = false;
    
    if ( cvv2Result != null && !cvv2Result.equals("") )
    {
      retVal = Cvv2ResultsToDecline.contains(cvv2Result);
    }
    return( retVal );
  }
  
  public boolean isDynamicDbaDataAllowed()
  {
    return( DynamicDbaDataAllowed );
  }
  
  public boolean isDynamicDbaNamePrefixEditEnabled()
  {
    return( DynamicDbaNamePrefixEditEnabled );
  }
  
  public boolean isFxEnabled()
  {
    return( FxEnabled );
  }
  
  public boolean isGtsEnabled()
  {
    return( getIntlProcessor() == mesConstants.INTL_PID_GTS );
  }
  
  public boolean isHttpGetAllowed()
  {
    return( HttpGetAllowed );
  }
  
  public boolean isIntlUSDEnabled()
  {
    return( IntlUSDEnabled );
  }
  
  public boolean isItpsEnabled() {
	  return( getIntlProcessor() == mesConstants.INTL_PID_ITPS );
  }
  
  public boolean isBraspagEnabled() {
	  return( getIntlProcessor() == mesConstants.INTL_PID_BRASPAG);
  }
  
  public boolean isMultiCaptureAllowed()
  {
    return( MultiCaptureAllowed );
  }
  
  public boolean isMultiCurrency()
  {
    boolean   retVal    = false;
    
    switch( getIntlProcessor() )
    {
      case mesConstants.INTL_PID_DCC:
      case mesConstants.INTL_PID_MES:
        retVal = true;
        break;
    }        
    return( retVal );
  }
  
  public boolean isPayVisionEnabled()
  {
    return( getIntlProcessor() == mesConstants.INTL_PID_PAYVISION );
  }
  
  public boolean isSettleAmountValidationEnabled()
  {
    return( SettleAmountValidationEnabled );
  }
  
  public boolean isStipAllowed()
  {
    return( StipAllowed );
  }
  
  public boolean isTipsAllowed()
  {
    return( TipsAllowed );
  }
  
  public boolean usingVitalForAmexAuths()
  {
    return( UseVitalForAmexAuths );
  }
  
  protected String processString( String input )
  {
    return( (input == null) ? "" : input );
  }
  
  public void setAmexCIDEnabled( String cidEnabled )
  {
    if ( cidEnabled != null && cidEnabled.toLowerCase().equals("y") )
    {
      AmexCIDEnabled = true;
    }
    else
    {
      AmexCIDEnabled = false;
    }
  }
  
  public void setAmexCurrencyCode(String code) {
    if(code != null && !code.equals(""))
      this.AmexCurrencyCode = code;
  }
  
  public void setAssocCardStoreAllowed( boolean value )
  {
    AssocCardStoreAllowed = value;
  }
  
  public void setBinNumber( String binNumber )
  {
    BinNumber = binNumber;
  }
  
  public void setDMContactInfo( String dmContactInfo )
  {
    DMContactInfo = dmContactInfo;
  }
  
  public void setDynamicDbaDataAllowed( boolean dynamicDbaDataAllowed )
  {
    DynamicDbaDataAllowed = dynamicDbaDataAllowed;
  }
  
  public void setDynamicDbaNamePrefix( String dynamicDbaNamePrefix )
  {
    DynamicDbaNamePrefix = dynamicDbaNamePrefix;
  }
  
  public void setDynamicDbaNamePrefixEditEnabled( boolean prefixEditEnabled )
  {
    DynamicDbaNamePrefixEditEnabled = prefixEditEnabled;
  }
  
  public void setForceOnDecline( boolean forceOnDecline )
  {
    ForceOnDecline = forceOnDecline;
  }
  
  public void setFraudMode( int value )
  {
    FraudMode = value;
  }
  
  public void setFxClientId( int clientId )
  {
    FxClientId = clientId;
    FxEnabled  = (FxClientId != 0);
  }
  
  public void setFxProductType( int productType )
  {
    FxProductType = productType;
  }
  
  public void setAvsResultsToDecline( String badResults )
  {
    AvsResultsToDecline.clear();
    
    if ( badResults != null && !badResults.equals("") )
    {
      StringTokenizer tokens = new StringTokenizer(badResults,",");
      while( tokens.hasMoreTokens() )
      {
        String token = (String)tokens.nextToken();
        AvsResultsToDecline.add(token);
      }
    }
  }
  
  public void setCardStoreLevel(int level)
  {
    CardStoreLevel = level;
    setAssocCardStoreAllowed(CardStoreLevel > 0);
  }
  
  public void setCvv2ResultsToDecline( String badResults )
  {
    Cvv2ResultsToDecline.clear();
  
    if ( badResults != null && !badResults.equals("") )
    {
      StringTokenizer tokens = new StringTokenizer(badResults,",");
      while( tokens.hasMoreTokens() )
      {
        String token = (String)tokens.nextToken();
        Cvv2ResultsToDecline.add(token);
      }
    }
  }
  
  public void setHttpGetAllowed( boolean value )
  {
    HttpGetAllowed = value;
  }
  
  public void setIntlProcessor( int value )
  {
    IntlProcessor = value;
  }
  
  public void setIntlCredentials( String credentials )
  {
    String delimiter = getIntlCredentialsDelimiter();
    IntlCredentials = (credentials == null ? null : credentials.split(delimiter));
  }
  
  public void setIntlCredentialsDelimiter( String delimiter )
  {
    IntlCredentialsDelimiter = delimiter;
  }
  
  public void setIntlUSDEnabled( boolean value )
  {
    IntlUSDEnabled = value;
  }
  
  public void setMultiCaptureAllowed( boolean value )
  {
    MultiCaptureAllowed = value;
  }
  
  public void setSettleAmountValidationEnabled( boolean enabled )
  {
    SettleAmountValidationEnabled = enabled;
  }
  
  public void setUseVitalForAmexAuths( boolean useVital )
  {
    UseVitalForAmexAuths = useVital;
  }
  
  public String toXml( )
  {
    StringBuffer      buffer    = new StringBuffer();
    
    buffer.append("<profile type=\"trident-api\">\n");
    buffer.append("  <fields>\n");
    buffer.append("    <field name=\"ProfileKey\" value=\"" + ProfileKey + "\"/>\n");
    buffer.append("    <field name=\"SecondaryKey\" value=\"" + SecondaryKey + "\"/>\n");
    buffer.append("    <field name=\"ApiCreditsAllowed\" value=\"" + ApiCreditsAllowed + "\"/>\n");
    buffer.append("    <field name=\"TipsAllowed\" value=\"" + TipsAllowed + "\"/>\n");
    buffer.append("    <field name=\"DbaAddrLine1\" value=\"" + DbaAddrLine1 + "\"/>\n");
    buffer.append("    <field name=\"DbaCity\" value=\"" + DbaCity + "\"/>\n");
    buffer.append("    <field name=\"DbaName\" value=\"" + DbaName + "\"/>\n");
    buffer.append("    <field name=\"DbaState\" value=\"" + DbaState + "\"/>\n");
    buffer.append("    <field name=\"DbaZip\" value=\"" + DbaZip + "\"/>\n");
    buffer.append("    <field name=\"MerchantId\" value=\"" + MerchantId + "\"/>\n");
    buffer.append("    <field name=\"PhoneNumber\" value=\"" + PhoneNumber + "\"/>\n");
    buffer.append("    <field name=\"ProfileStatus\" value=\"" + ProfileStatus + "\"/>\n");
    buffer.append("    <field name=\"SicCode\" value=\"" + SicCode + "\"/>\n");
    buffer.append("    <field name=\"ProfileId\" value=\"" + ProfileId + "\"/>\n");
    buffer.append("    <field name=\"TimeZone\" value=\"" + TimeZone + "\"/>\n");
    buffer.append("  </fields>\n");
    buffer.append("</profile>\n");
    
    return( buffer.toString() );
  }
}