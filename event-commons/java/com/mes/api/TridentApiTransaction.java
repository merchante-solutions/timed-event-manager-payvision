/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/TridentApiTransaction.java $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.axis.types.UnsignedByte;
import org.apache.log4j.Logger;
import org.datacontract.schemas._2004._07.RegularTransactionPacket;
import org.datacontract.schemas._2004._07.TransactionResponsePacket;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.BillingPurpose;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.CreditCardsTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.CurrencyTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.CustomerOriginatedTechnologyTypeIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.RecurringServiceExpiryMethod;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.ThreeDLevel;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxAccessManagementType;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxCodes;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxInstructionTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxOperationType;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxPaymentMethodsIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxPaymentTypeIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxProcessingTypeIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxRecurringInstallmentIntervalMethod;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxRequestTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxServicePurchasedCategorysIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxServicePurchasedSubCategorysIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxServicePurchasedTypesIDs;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.adyen.services.common.Address;
import com.adyen.services.common.Amount;
import com.adyen.services.payment.AnyType2AnyTypeMap;
import com.adyen.services.payment.AnyType2AnyTypeMapEntry;
import com.adyen.services.payment.Card;
import com.adyen.services.payment.ModificationRequest;
import com.adyen.services.payment.ModificationResult;
import com.adyen.services.payment.PaymentLocator;
import com.adyen.services.payment.PaymentPortType;
import com.adyen.services.payment.PaymentRequest;
import com.adyen.services.payment.PaymentResult;
import com.adyen.services.payment.ThreeDSecureData;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.net.Http;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesEncryption;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.Base64;
import com.mes.tools.CurrencyCodeConverter;
import com.mes.tools.DiscoverProductCodeConverter;
import com.mes.tools.MasterCardProductCodeConverter;
import com.payvision.gateway.BasicOperationsLocator;
import com.payvision.gateway.BasicOperationsSoap;
import com.payvision.gateway.BasicOperationsSoapStub;
import com.payvision.gateway.CdcEntry;
import com.payvision.gateway.CdcEntryItem;
import com.payvision.gateway.ThreeDSecureOperationsLocator;
import com.payvision.gateway.ThreeDSecureOperationsSoap;
import com.payvision.gateway.TransactionResult;
import br.com.pagador.www.webservice.pagador.AddressDataRequest;
import br.com.pagador.www.webservice.pagador.ArrayOfPaymentDataRequest;
import br.com.pagador.www.webservice.pagador.AuthorizeTransactionRequest;
import br.com.pagador.www.webservice.pagador.AuthorizeTransactionResponse;
import br.com.pagador.www.webservice.pagador.CreditCardDataRequest;
import br.com.pagador.www.webservice.pagador.CreditCardDataResponse;
import br.com.pagador.www.webservice.pagador.CustomerDataRequest;
import br.com.pagador.www.webservice.pagador.ErrorReportDataResponse;
import br.com.pagador.www.webservice.pagador.OrderDataRequest;
import br.com.pagador.www.webservice.pagador.PagadorTransactionLocator;
import br.com.pagador.www.webservice.pagador.PagadorTransactionSoap;
import br.com.pagador.www.webservice.pagador.PaymentDataRequest;
import masthead.client.NetConnectAuthClient;
import masthead.formats.visad.AmexItdData;
import masthead.formats.visad.DMessageFactory;
import masthead.formats.visad.RequestDMessage;
import masthead.formats.visad.ResponseDMessage;
import masthead.util.AdditionalAmounts;
import masthead.util.ByteUtil;
import masthead.util.StringUtil;
import net.atpay.transactions.webservices.ATPayTxWS.ATPayTxWSLocator;
import net.atpay.transactions.webservices.ATPayTxWS.IATPayTxWS;

public class TridentApiTransaction extends TridentApiTranBase
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;
  
  static Logger log = Logger.getLogger(TridentApiTransaction.class);

  public  static final int      ST_NEW              = 0;
  public  static final int      ST_AUTH_REQUESTED   = 1;
  public  static final int      ST_RESP_RECEIVED    = 2;
  public  static final int      ST_COMPLETE         = 3;
  public  static final int      ST_DELETE           = 99;
  
  public  static final int      CD_ID               = 0;
  public  static final int      CD_SWIPE            = 1;
  public  static final int      CD_NUMBER           = 2;
  public  static final int      CD_COUNT            = 3;

  public    String                AciReturned           = null;
  protected String                AdyenEndpoint         = TridentApiConstants.ADYEN_ENDPOINT_TEST;
  protected PaymentResult         AdyenResult           = null;
  public    String                ApiTranId             = null;
  private   String                AuthorizationHost     = null;
  private   int                   AuthorizationPort     = 0;
  protected String                AuthReversalData      = null;
  protected String                AuthTransactionId     = null;
  public    String                AvsStreet             = null;
  public    String                AvsZip                = null;
  protected boolean               BatchRequest          = false;
  private   CreditCardDataResponse braspagResult		= null;
  public    String                braspagEndpoint       = TridentApiConstants.BRASPAG_ENDPOINT_TEST;
  protected String                CardDataLoadError     = null;
  protected String                CardholderEmail       = null;
  protected String                CardholderFirstName   = null;
  protected String                CardholderLastName    = null;
  protected String                CardholderPhone       = null;
  protected String                CardholderRefNum      = null;
  protected String                CardholderState       = null;
  protected String                CardinalCentinelUrl   = TridentApiConstants.CENTINEL_URL_TEST;
  protected String                CardId                = null;
  public    String                CardPresent           = TridentApiConstants.FV_CARD_PRESENT_DEFAULT;
  protected boolean               CardStoreRequested    = false;
  protected String                CardSwipe             = null;
  protected byte[]                CardSwipeEnc          = null;
  protected boolean               Contactless           = false;
  public    String                Cvv2                  = null;
  protected String                DebitKeySerialNumber  = null;
  protected String                DebitPinBlock         = null;
  public    String                DeveloperId           = TridentApiConstants.AUTH_DEVELOPER_ID;
  protected HashMap               EchoFields            = null;
  protected int                   Emulation             = TridentApiConstants.EMU_TPG;
  protected String                ErrorCode             = TridentApiConstants.ER_NONE;
  protected String                ErrorDesc             = "";
  public    String                ExpDate               = null;
  protected String                ExternalTranId        = null;
  protected java.util.Date        FraudAcctCreationDate = null;
  protected String                FraudAcctEmail        = null;
  protected java.util.Date        FraudAcctLastChange   = null;
  protected String                FraudAcctName         = null;
  protected String                FraudBrowserLanguage  = null;
  protected String                FraudDeviceId         = null;
  protected boolean               FraudDigitalGoodsFlag = false;
  protected String                FraudEndpoint         = TridentApiConstants.EDNA_ENDPOINT_TEST;
  protected String                FraudResult           = null;
  protected String                FraudResultCodes      = null;
  protected boolean               FraudScan             = true;
  protected boolean               FraudSubscriptionFlag = false;
  protected ForeignExchangeRate   FxRate                = null;
  protected boolean               FxValidationsEnabled  = false;
  protected String                GtsEndpoint         	= TridentApiConstants.GTS_ENDPOINT_TEST;
  protected TransactionResponsePacket GtsResult 		= null;
  protected boolean               HoldForReview         = false;
  protected String[]              IncomingCardData      = new String[CD_COUNT];
  public    String                IndustryCode          = TridentApiConstants.FV_INDUSTRY_CODE_DEFAULT;
  protected String                InputErrorCode        = TridentApiConstants.ER_NONE;
  protected boolean               Intl3DEnabled         = false;
  protected boolean               IntlUSDRequest        = false;
  private String 				  ItpsAddressResult		= "";
  private String 				  ItpsZipResult			= "";
  private String 				  ItpsCvvResult			= "";
  private String 				  ItpsAuthCode			= "";
  protected String                ItpsEndpoint         	= TridentApiConstants.ITPS_ENDPOINT_TEST;
  protected String				  ItpsErrorCode			= "";
  private boolean 				  ItpsFlag;
  protected String                LevelIIICardType      = null;
  protected HashMap               LevelIIIFields        = new HashMap();
  protected List                  LineItemData          = null;
  protected double                LinkedTranAmount      = 0.0;
  protected double                OriginalFxAmount      = 0.0;
  protected double                OriginalTranAmount    = 0.0;
  protected String                OriginalTranId        = null;
  public    boolean               OrigTranIdRequired    = false;
  protected String                Payload3D             = null;
  protected String                PayVisionEndpoint     = TridentApiConstants.PAYVISION_ENDPOINT_TEST;
  protected TransactionResult     PayVisionResult       = null;
  public    TridentApiProfile     Profile               = null;
  public    Timestamp             RequestTimestamp      = null;
  public    boolean               ResponseHash           = false;
  protected HashMap               ResponseCtlFlags      = new HashMap();
  public    boolean               Settled               = false;
  protected long                  SwitchDuration        = 0L;
  protected long                  TransactionDuration   = 0L;
  protected String                TranId3D              = null;
  public    int                   TranState             = ST_NEW;
  protected String                TridentTranId         = null;
  protected long                  TridentTranIdNumber   = 0L;
  protected String                UcafAuthData          = null;
  protected String                UcafCollectionInd     = null;
  protected String                VisaCavv              = null;
  protected String                VisaXid               = null;
  public    ResponseDMessage      VisaDResponse         = null;
  public    String                XmlResponse           = null;
  
  public TridentApiTransaction( )
  {
  }
  
  public TridentApiTransaction( String tranId )
  {
    ApiTranId = tranId;
  }
  
  public TridentApiTransaction( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setProperties(resultSet);
  }  
  
  /**
   * Adds dashes back into a dashless UUID.
   * @param guid
   * @return
   */
  public String addGuidDashes(String dashlessGuid) {
    StringBuffer newGuid = new StringBuffer();
    newGuid.append(dashlessGuid.substring(0, 8));
    newGuid.append("-");
    newGuid.append(dashlessGuid.substring(8, 12));
    newGuid.append("-");
    newGuid.append(dashlessGuid.substring(12, 16));
    newGuid.append("-");
    newGuid.append(dashlessGuid.substring(16, 20));
    newGuid.append("-");
    newGuid.append(dashlessGuid.substring(20, 32));
    return newGuid.toString();
  }
  
  public boolean areFxValidationsEnabled()
  {
    return( FxValidationsEnabled );
  }
  
  public void clearFraudData()
  {
    setFraudScan( false );    
    setFraudResult( null );  
    setFraudResultCodes( null );
  }
  
  /**
   * Converts Numeric ITPS CVV result to the standard CVV result code.
   * @param itpsResult
   * @return null on invalid/unsupported itpsResult, or if "no information is available".
   */
  private String convertItpsCvvResult(String itpsResult) {
	  int itpsCodeNumeric = -1;
	  try {
		  itpsCodeNumeric = Integer.parseInt(itpsResult);
	  } catch(NumberFormatException e) {
		  return null;
	  }
	  
	  switch(itpsCodeNumeric) {
	  //case 0: return ""; 	// No Information Available
	  case 1: return "P"; 	// Data not checked
	  case 2: return "M"; 	// Data Matched
	  case 3: return "S";	// Should have Security Code
	  case 4: return "N";	// Data not matched
	  }
	  return null;
  }

  /**
   * Converts the numeric ITPS street and zip results to the standard AVS result code.
   * @param streetResult
   * @param zipResult
   * @return null on invalid/unsupported street/zip result.
   */
  private String convertItpsAvsResult(String streetResult, String zipResult) {
	  int aRes = -1;
	  int zRes = -1;
	  try {
		  aRes = Integer.parseInt(streetResult);
		  zRes = Integer.parseInt(zipResult);
	  } catch(NumberFormatException e) {
		  return null;
	  }
//	  ITPS Table
//	  0 No Information Available
//	  1 Data Not Checked
//	  2 Data Matched
//	  4 Data Not Matched
//	  8 Partial Match
	  if(aRes == 2 && zRes == 2) return "M";				// Full Match
	  if(aRes == 4 && zRes == 4) return "N";				// No Match
	  if(aRes == 2 && (zRes == 4 || zRes == 8)) return "A";	// Address Match (no zip match)
	  if((aRes == 4 || aRes == 8) && zRes == 2) return "Z";	// Zip match (no street match)
	  if(aRes == 2 && zRes == 1) return "B";				// Address Match (zip not checked)
	  if(aRes == 1 && zRes == 2) return "P";				// Zip match (address not checked)
	  return null;
  }
  
  protected String decodeResponseValue( HashMap map, String respValue )
  {
    String    retVal    = respValue;
    
    if ( respValue != null && map != null )
    {
      String mappedValue = (String)map.get(respValue);
      if ( mappedValue != null )
      {
        retVal = mappedValue;
      }
    }
    return( retVal );
  }
  
//@Override
  public void doEdits() {
	  super.doEdits();

	  // Edits on 3D secure fields (PMG-117)
	  if(is3dSecureRequest()) {
		  String cardType = getCardType();

		  // The only valid 3D ECI values are 5,6,7.  Force to 7 on invalid input.
		  if(!MotoEcommInd.equals("5") && !MotoEcommInd.equals("6"))
			  MotoEcommInd = "7";

		  // Card specific edits
		  if(cardType.equals("VS")) {
			  UcafAuthData = null;
			  UcafCollectionInd = null;
		  }
		  else if(cardType.equals("MC")) {
			  VisaCavv = null;
			  VisaXid = null;
			  // UCAF may only be 1 when ECI is 5/6
			  if(UcafCollectionInd != null && UcafCollectionInd.equals("1"))
				  if(MotoEcommInd.equals("7")) // Can only be 5,6, or 7 at this point.
					  UcafCollectionInd = null;
		  }
	  }
  }
  
  protected void encodeEchoFields( StringBuffer buffer )
  {
    String                fieldName     = null;
  
    // output the echo fields if present
    if ( EchoFields != null && buffer != null )
    {
      for ( Iterator it = EchoFields.keySet().iterator(); it.hasNext(); )
      {
        fieldName = (String)it.next();
        buffer.append( (buffer.length() > 0) ? "&" : "" );
        buffer.append( fieldName );
        buffer.append( "=" );
        buffer.append( EchoFields.get(fieldName) );
      }      
    }
  }
  
  public String encodeResponse()
  {
    String[]              addlAmounts       = new String[2];
    PaymentResult         adyenResp         = getAdyenResult();
    String                authCode          = null;
    String                avsResult         = null;
    String                cvv2Result        = null;
    String                fieldName         = null;
    String                fieldValue        = null;
    TransactionResponsePacket gtsResponse   = getGtsResult();
    String                productLevel      = null;
    String                commrclCard       = null;
    String                extendedAvs       = null;
    TransactionResult     pvResp            = getPayVisionResult();
    ResponseDMessage      resp              = getVisaDResponse();    
    String                respCode          = getErrorCode();
    String                respText          = getErrorDesc();
    StringBuffer          retVal            = new StringBuffer();
    
    if ( adyenResp != null )
    {
      // extract the response values from the Adyen result
      respText  = adyenResp.getResultCode();
      if ( "Refused".equals(respText) )
      {
        respText += " - " + adyenResp.getRefusalReason();
      }        
      authCode  = StringUtilities.rightJustify(processString(adyenResp.getAuthCode()),6,'0');
      
      // the raw response data is not guaranteed to have the 
      // auth response code in the first 2 bytes.  As a result
      // we only attempt to use this value when the auth was
      // declined and the response code does not equal zero
      if ( !"Authorised".equals(adyenResp.getResultCode()) )
      {
        fieldValue= getAdyenAdditionalValue("refusalReasonRaw",false);
        if ( fieldValue != null && fieldValue.length() >= 2)
        {
          try
          {
            int intVal = Integer.parseInt(fieldValue.substring(0,2));
            if ( intVal > 0 )
            {
              respCode = StringUtilities.rightJustify(String.valueOf(intVal),3,'0');
              respText = ((fieldValue.length() > 5) ? fieldValue.substring(5) : respText);
            }
          }
          catch( NumberFormatException nfe )
          {
          }
        }
      }
      cvv2Result= decodeResponseValue(TridentApiConstants.AdyenToTridentCvv2Map,getAdyenAdditionalValue("cvcResult",true));
      avsResult = decodeResponseValue(TridentApiConstants.AdyenToTridentAvsMap ,getAdyenAdditionalValue("avsResult",true));
    }
    else if ( pvResp != null )    // look for a payvision response
    {
      respCode = String.valueOf( pvResp.getResult() );
      while( respCode.length() < 3 )
      {
        respCode = ("0" + respCode);  // leading zeros
      }
      
      // extract the response values from the PayVision result
      respText  = getPayVisionCDCValue("BankInformation","BankMessage");
      if ( respText == null || respText.equals("") )
      {
        respText = getPayVisionCDCValue("ErrorInformation","ErrorMessage",pvResp.getMessage()) + " (" +
                   getPayVisionCDCValue("ErrorInformation","ErrorCode",respCode) + ")";
      }
      authCode  = getPayVisionCDCValue("BankInformation","BankApprovalCode");
      cvv2Result= getPayVisionCDCValue("BankInformation","Cvv");
      avsResult = getPayVisionCDCValue("BankInformation","Avs");
      
      // PayVision test system does not generate auth codes
      if ( (authCode == null || authCode.trim().equals("")) &&
           TridentApiConstants.PAYVISION_ENDPOINT_TEST.equals(getPayVisionEndpoint()) )
      {
        // create a dummy approval code for the test system
        authCode = TridentApiTools.generateDummyAuthCode();
      }
    }
    else if(gtsResponse != null) {
    	// TODO: ...
    }
    else if(getItpsFlag()) {
    	
    	// TODO: ITPS result codes are numeric
    	authCode = getItpsAuthCode();
    	cvv2Result = convertItpsCvvResult(getItpsCvvResult());
    	avsResult = convertItpsAvsResult(getItpsAddressResult(), getItpsZipResult());
    	respText = getErrorDesc();
    }
    else if(braspagResult != null) {
    	authCode = braspagResult.getAuthorizationCode();
    }
    else if ( resp != null )  // check for Trident response
    {
      if ( hasError() )
      {
        respCode  = getErrorCode();
        respText  = getErrorDesc();
        authCode  = "000000";
      }
      else
      {
        respCode      = "0" + resp.getResponseCode();
        respText      = resp.getAuthorizationResponseText().trim();
        authCode      = resp.getApprovalCode();
      }
     
      // get the cvv2 and avs results (if present)
      if ( resp.hasCvv2ResultCode() )
      {
        cvv2Result = resp.getCvv2ResultCode();
      }
      try{ avsResult = resp.getAVSResultCode(); } catch(Exception e){}
      
      // get optional fields
      try{ commrclCard   = resp.getCommercialCardType(); } catch(Exception e){}
      try
      { 
        productLevel  = resp.getCardLevelResults();   // default to issuer data
        
        if ( "MC".equals(getCardType()) )
        {
          String banknetRefNum = resp.getTransactionIdentifier();
          if ( banknetRefNum != null && banknetRefNum.length() >= 7 )
          {
            productLevel = MasterCardProductCodeConverter.getInstance().decodeVisaProductCode(banknetRefNum.substring(4,7));
          }
        }
        else if ( "DS".equals(getCardType()) || "JC".equals(getCardType()) )
        {
          if ( authCode != null && authCode.length() == 6 )
          {
            productLevel = DiscoverProductCodeConverter.getInstance().decodeVisaProductCode(authCode.substring(5));
          }
        }
      } 
      catch(Exception e)
      {
      }

      try
      { 
        // additional amount types are:
        //   02 - available balance
        //   10 - partially authorized amount
        //   57 - originally requested amount
        String amountType = null;
        for( int i = 0; i < addlAmounts.length; ++i )
        {
          switch(i)
          {
            case 0:   amountType = "02";  break;
            case 1:   amountType = "10";  break;
            default:                      continue;
          }
          if( resp.hasAdditionalAmounts(amountType) ) 
          {
            AdditionalAmounts addlAmount = resp.getAdditionalAmounts(amountType);
            addlAmounts[i] = MesMath.toFractionalAmount(Double.parseDouble(addlAmount.getAmount()) * ("D".equals(addlAmount.getSignCode()) ? -0.01 : 0.01),2,2,false);
          }
        }
      } 
      catch(Exception e)
      {
      }
      
      try
      { 
        // amex er codes: 
        //    BillingPostalCode     - pos 1
        //    BillingStreetAddress  - pos 2
        //    FirstAndLastName      - pos 3
        //    BillingPhoneNumber    - pos 4
        //    CustomerEmailAddress  - pos 5
        extendedAvs = 
          StringUtilities.leftJustify( processString(resp.getAmexBillingPostalCodeResponseCode())   ,1,'-' ) +
          StringUtilities.leftJustify( processString(resp.getAmexBillingStreetAddressResponseCode()),1,'-' ) +
          StringUtilities.leftJustify( processString(resp.getAmexFirstAndLastNameResponseCode())    ,1,'-' ) +
          StringUtilities.leftJustify( processString(resp.getAmexBillingPhoneNumberResponseCode())  ,1,'-' ) +
          StringUtilities.leftJustify( processString(resp.getAmexCustomerEmailAddressResponseCode()),1,'-' );
      } 
      catch(Exception e)
      {
      }
    }
   
    for( int i = 0; i < TridentApiTranBase.ResponseFields.length; ++i )
    { 
      fieldName   = TridentApiTranBase.ResponseFields[i];
      fieldValue  = null;
      
      if ( fieldName.equals(TridentApiConstants.FN_TRAN_ID) )
      {
        fieldValue = getTridentTranId();
      }
      else if ( fieldName.equals(TridentApiConstants.FN_ERROR_CODE) )
      {
        fieldValue = respCode;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_AUTH_RESP_TEXT) )
      {
        fieldValue = respText;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_AVS_RESULT) )
      {
        fieldValue = avsResult;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_CVV2_RESULT) )
      {
        fieldValue = cvv2Result;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_AUTH_CODE) )
      {
        fieldValue = authCode;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_PRODUCT_LEVEL) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_PRODUCT_LEVEL) != null )
        {
          fieldValue = productLevel;
        }
      }
      else if ( fieldName.equals(TridentApiConstants.FN_ACCOUNT_BALANCE) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_ACCOUNT_BALANCE) != null )
        {
          fieldValue = addlAmounts[0];
        }
      }
      else if ( fieldName.equals(TridentApiConstants.FN_PARTIAL_AUTH) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_PARTIAL_AUTH) != null )
        {
          fieldValue = addlAmounts[1];
        }
      }
      else if ( fieldName.equals(TridentApiConstants.FN_COMMERCIAL_CARD) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_COMMERCIAL_CARD) != null )
        {
          fieldValue = commrclCard;
        }
      }
      else if ( fieldName.equals(TridentApiConstants.FN_EXTENDED_AVS) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_EXTENDED_AVS) != null )
        {
          fieldValue = extendedAvs;
        }
      }
      // Truncated card number in response
      else if ( fieldName.equals(TridentApiConstants.FN_CARD_NUMBER_TRUNC) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_CARD_NUMBER_TRUNC) != null && getCardNumber() != null && !getCardNumber().equals(""))
        {
          fieldValue = this.getCardNumber();
        }
      }
      // Security hash in response
      else if ( fieldName.equals(TridentApiConstants.FN_RESP_HASH) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_RESP_HASH) != null )
        {
          fieldValue = getSecurityHash(getTridentTranId());
        }
      }
      else if ( fieldName.equals(TridentApiConstants.FN_FRAUD_RESULT) )
      {
        fieldValue = FraudResult;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_FRAUD_RESULT_CODES) )
      {
        fieldValue = FraudResultCodes;
      }
      
      if ( fieldValue != null )
      {
        retVal.append( (retVal.length() > 0) ? "&" : "" );
        retVal.append( fieldName );
        retVal.append( "=" );
        retVal.append( fieldValue.trim() );
      }
    }
    
    encodeEchoFields( retVal );

    return( retVal.toString() );
  }

  public String encodeResponse( String tranId, String msgId, String msgText )
  {
    StringBuffer          authRespText  = new StringBuffer();
    String                fieldName     = null;
    String                fieldValue    = null;
    ForeignExchangeRate   fxRate        = getFxRate();
    HashMap               respData3D    = get3DResponseData();
    String                tranType      = getTranType();
    
    for( int i = 0; i < TridentApiTranBase.ResponseFields.length; ++i )
    { 
      fieldName   = TridentApiTranBase.ResponseFields[i];
      fieldValue  = null;
      
      if ( fieldName.equals(TridentApiConstants.FN_TRAN_ID) )
      {
        fieldValue = tranId;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_ERROR_CODE) )
      {
        fieldValue = msgId;
      }
      else if ( fieldName.equals(TridentApiConstants.FN_AUTH_RESP_TEXT) )
      {
        if ( msgText == null || msgText.equals("") )
        {
          fieldValue = TridentApiConstants.getErrorDesc(msgId);
        }
        else
        {
          fieldValue = msgText;
        }
      }
      // URL encoded fields
      else if ( tranType.equals(TT_FX_GET_RATE) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_FX_RATE) ||
                fieldName.equals(TridentApiConstants.FN_FX_RATE_TABLE) )
      {
        if ( fieldName.equals(TridentApiConstants.FN_FX_RATE) )
        {
          fieldValue = getFxRateXml();
        }
        else if ( fieldName.equals(TridentApiConstants.FN_FX_RATE_TABLE) )
        {
          fieldValue = getFxRateTableXml();
        }
        
        if ( fieldValue != null )
        {
          try
          {
            fieldValue = URLEncoder.encode(fieldValue,"UTF-8");
          }
          catch( Exception ee )
          {
            // just use value non-encoded
          }
        }
      }
      else if ( tranType.equals(TT_FX_CONVERT_AMOUNT) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_FX_RATE_ID) && fxRate != null )
      {
        fieldValue = String.valueOf( getFxRate().getRateId() );
      }
      else if ( tranType.equals(TT_FX_CONVERT_AMOUNT) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_FX_EXP_DATE) && fxRate != null )
      {
        fieldValue = String.valueOf( getFxRate().getExpirationDateStr() );
      }
      else if ( tranType.equals(TT_FX_CONVERT_AMOUNT) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_AMOUNT) )
      {
        fieldValue = getTranAmountFormatted();
      }
      else if ( tranType.equals(TT_CURRENCY_LOOKUP) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_CURRENCY_CODE_ALPHA) )
      {
        fieldValue = getCurrencyCodeAlpha();
      }
      else if ( tranType.equals(TT_CURRENCY_LOOKUP) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_CURRENCY_CODE) )
      {
        fieldValue = getCurrencyCode();
      }
      else if ( tranType.equals(TT_CURRENCY_LOOKUP) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_COUNTRY_CODE) )
      {
        fieldValue = getCountryCode();
      }
      else if ( tranType.equals(TT_CURRENCY_LOOKUP) && !hasError() &&
                fieldName.equals(TridentApiConstants.FN_COUNTRY_NAME) )
      {
        fieldValue = getCountryName();
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_MOTO_IND) )
      {
        fieldValue = (String)respData3D.get(TridentApiConstants.FN_MOTO_IND);
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_3D_ENROLLED) )
      {
        fieldValue = (String)respData3D.get(TridentApiConstants.FN_3D_ENROLLED);
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_3D_TRAN_ID) )
      {
        fieldValue = (String)respData3D.get(TridentApiConstants.FN_3D_TRAN_ID);
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_3D_ACS_URL) )
      {
        fieldValue = urlEncode( (String)respData3D.get(TridentApiConstants.FN_3D_ACS_URL) );
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_3D_PAYLOAD) )
      {
        fieldValue = urlEncode( (String)respData3D.get(TridentApiConstants.FN_3D_PAYLOAD) );
      }
      else if ( tranType.equals(TT_3D_ENROLL_CHECK) && !hasError() &&
                respData3D != null &&
                fieldName.equals(TridentApiConstants.FN_3D_ORDER_ID) )
      {
        fieldValue = (String)respData3D.get(TridentApiConstants.FN_3D_ORDER_ID);
      }
      // Truncated card number in response
      else if ( fieldName.equals(TridentApiConstants.FN_CARD_NUMBER_TRUNC) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_CARD_NUMBER_TRUNC) != null && getCardNumber() != null && !getCardNumber().equals(""))
        {
          fieldValue = getCardNumber();
        }
      }
      // Security hash in response
      else if ( fieldName.equals(TridentApiConstants.FN_RESP_HASH) )
      {
        if ( ResponseCtlFlags.get(TridentApiConstants.FN_RESP_HASH) != null )
        {
          fieldValue = getSecurityHash(tranId);
        }
      }
      
      if ( fieldValue != null )
      {
        authRespText.append( (authRespText.length() > 0) ? "&" : "" );
        authRespText.append( fieldName );
        authRespText.append( "=" );
        authRespText.append( fieldValue );
      }
    }
    encodeEchoFields( authRespText );
    
    return( authRespText.toString() );
  }
  
  public boolean forceOnDecline()
  {
    return( Profile != null && Profile.forceOnDecline() );
  }
  
  public String getAccountDataSource()
  {
    String    retVal    = "@";      // assume keyed, no mag reader
    
    if ( isSwiped() )
    {
      if ( isContactlessSwipe() )
      {
        retVal = "Q";     // contactless swipe data
      }
      else if ( isTrack1Swipe() )
      {
        retVal = "H";    // track 1 swipe
      }
      else  // assume track 2
      {
        retVal = "D";    // track 2 swipe
      }        
    }
    return( retVal );      
  }
  
  public String getAdyenAdditionalValue( String fieldName, boolean trimField )
  {
    return( getAdyenAdditionalValue( fieldName, trimField, null ) );
  }
  
  public String getAdyenAdditionalValue( String fieldName, boolean trimField, String defaultValue )
  {
    PaymentResult   adyenResp     = getAdyenResult();
    String          retVal        = defaultValue;
    
    if ( adyenResp != null )
    {
      AnyType2AnyTypeMap additionalData = adyenResp.getAdditionalData();
      if ( additionalData != null )
      {
        AnyType2AnyTypeMapEntry[] entries = additionalData.getEntry();
        
        for( int entryIdx = 0; entries != null && entryIdx < entries.length; ++entryIdx )
        {
          if ( fieldName.equals((String)entries[entryIdx].getKey()) )
          {
            retVal = (String)entries[entryIdx].getValue();
            if ( trimField && retVal != null && retVal.indexOf(" ") >= 0 ) 
            {
              retVal = retVal.substring(0,retVal.indexOf(" "));
            }
          }
        }
      }
    }
    return( retVal );
  }
  
  public String getAdyen3dsAcquirerId()
  {
    return( Profile.getIntlCredential(3) );
  }
  
  public String getAdyen3dsMerchantId()
  {
    return( Profile.getIntlCredential(4) );
  }
  
  public String getAdyenCompanyId()
  {
    return( "ws@Company." + Profile.getIntlCredential(2) );
  }
  
  public String getAdyenEndpoint( )
  {
    return( AdyenEndpoint );
  }
  
  public String getAdyenMerchantId()
  {
    return( Profile.getIntlCredential(0) );
  }
  
  public String getAdyenPassword()
  {
    return( Profile.getIntlCredential(1) );
  }
  
  public PaymentResult getAdyenResult()
  {
    return( AdyenResult );
  }
  
  public String getAdyenTranId()
  {
    return( getExternalTranId() );
  }
  
  public String getApprovalCode()
  {
    ResponseDMessage  resp    = getVisaDResponse();
    String            retVal  = null;
    
    if ( resp != null )
    {
      retVal = resp.getApprovalCode();
    }
    return( retVal );
  }
  
  public String getApiTranId( )
  {
    return( ApiTranId );
  }
  
  public double getAuthRequestAmount( )
  {
    // always $0 for card verify requests
    return( isAuthVerifyOnly() ? 0.0 : getTranAmount() );
  }
  
  public String getAuthRetrievalRefNum( )
  {
    ResponseDMessage  resp      = getVisaDResponse();
    String            retVal    = null;
    
    if ( resp != null )
    {
      retVal = resp.getRetrievalReferenceNumber();
    }
    return( retVal );
  }
  
  public String getAuthReversalData( )
  {
    ResponseDMessage  resp      = getVisaDResponse();
    String            retVal    = AuthReversalData;
    
    if ( resp != null )
    {
      StringBuffer buffer = new StringBuffer();
      buffer.append( StringUtilities.leftJustify(resp.getApprovalCode(),6,' ') );
      buffer.append( StringUtilities.leftJustify(resp.getLocalTransactionDate(),6,' ') );
      buffer.append( StringUtilities.leftJustify(resp.getLocalTransactionTime(),6,' ') );
      buffer.append( resp.getRetrievalReferenceNumber() );
      retVal = buffer.toString();
    } 
    return( retVal );
  }
  
  public String getAuthTransactionId()
  {
    ResponseDMessage  resp    = getVisaDResponse();
    String            retVal  = AuthTransactionId;
    
    if ( resp != null )
    {
      retVal = resp.getTransactionIdentifier();
    }
    return( retVal );
  }
  
  public String getAvsData( )
  {
    String        addr    = processString(AvsStreet);
    StringBuffer  retVal  = new StringBuffer();
    String        zip     = processString(AvsZip);
    
    // truncate values if necessary
    if ( addr.length() > TridentApiConstants.FLM_AVS_ADDRESS )
    {
      addr = addr.substring(0,TridentApiConstants.FLM_AVS_ADDRESS);
    }
    if ( zip.length() > TridentApiConstants.FLM_AVS_ZIP )
    {
      zip = zip.substring(0,TridentApiConstants.FLM_AVS_ZIP);
    }
    
    // build the request string
    retVal.append(addr);
    retVal.append( (retVal.length() > 0) ? " " : "" );
    retVal.append(zip);
    
    return( retVal.toString() );
  }
  
  public String getAvsResultCode()
  {
    String            avsResult   = "";
    ResponseDMessage  resp        = getVisaDResponse();
    
    if ( resp != null )
    {
      try{ avsResult = resp.getAVSResultCode(); } catch(Exception e){}
    }
    return( avsResult );      
  }
  
  public String getBinNumber()
  {
    return( Profile.getBinNumber() );
  }
  
  /**
   * Gets the current endpoint for the Braspag gateway.
   * @return
   */
  public String getBraspagEndpoint() {
    return braspagEndpoint;
  }
  
  /**
   * Fetches the Braspag merchant ID as stored in the profiles first international credentials field.
   * @return
   */
  public String getBraspagMerchantId()
  {
    return( Profile.getIntlCredential(0) );
  }
  
  /**
   * Returns the Braspag payment method indicator. For now, we are basing the result on the card bin. <br />
   * This is stored in the third field of the profile's international credentials. Values:
   * <ul>
   *    <li>0 - Cielo (Default)</li>
   *    <li>1 - Banorte</li>
   *    <li>2 - Redecard</li>
   *    <li>3 - PagosOnLine</li>
   *    <li>4 - Banorte Cargos Automaticos</li>
   *    <li>5 - SITEF (NYI)</li>
   *    <li>6 - SUB1</li>
   * </ul>
   * @return Braspag payment method for this profile.
   */
  public short getBraspagPaymentMethod() {
    int paymentCategory = 0; // Cielo by default
    String sPaymentCategory = Profile.getIntlCredential(2);
    if(sPaymentCategory != null) {
      try {
        paymentCategory = Integer.parseInt(sPaymentCategory);
      } catch(Exception e) {
        throw new RuntimeException("Invalid Braspag payment method ("+sPaymentCategory+")");
      }
    }
    String cardType = TridentTools.decodeVisakCardType(getCardNumberFull());
    
    switch(paymentCategory) {
    case 0:
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 500; // Cielo Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 501; // Cielo MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_AMEX))    return 502; // Cielo AMEX
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 503; // Cielo Diners
      // TODO: Cielo Elo card?
      break;
    case 1:
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 505; // Banorte Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 506; // Banorte MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 507; // Banorte Diners
      if(cardType.equals(mesConstants.VITAL_ME_AP_AMEX))    return 508; // Banorte AMEX
      break;
    case 2: 
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 509; // Redecard Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 510; // Redecard MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 511; // Redecard Diners
      break;
    case 3:
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 512; // PagosOnLine Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 513; // PagosOnLine MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_AMEX))    return 514; // PagosOnLine AMEX
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 515; // PagosOnLine Diners
      break;
    case 4:
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 520; // BCA Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 521; // BCA MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 522; // BCA Diners
      break;
    case 5:
      // SITEF NYI
      break;
    case 6:
      if(cardType.equals(mesConstants.VITAL_ME_AP_VISA))    return 535; // SUB1 Visa
      if(cardType.equals(mesConstants.VITAL_ME_AP_MC))      return 536; // SUB1 MasterCard
      if(cardType.equals(mesConstants.VITAL_ME_AP_AMEX))    return 537; // SUB1 AMEX
      if(cardType.equals(mesConstants.VITAL_ME_AP_DINERS))  return 538; // SUB1 Diners
      break;
    default:
      throw new RuntimeException("Unsupported Braspag acquirer ("+paymentCategory+")");
    }
    // Valid payment category, but card type has fallen through.
    throw new RuntimeException("Card type is not supported by Braspag acquirer");
  }
  
  /**
   * Returns the country code associated with the acquirer for this Braspag payment method.
   * This is stored in the second field of the profile's international credentials. Return countries supported:
   * <ul>
   *    <li>BRA - Brasil</li>
   *    <li>ARG - Argentina</li>
   *    <li>MEX - Mexico</li>
   *    <li>COL - Colombia</li>
   *    <li>CHL - Chile</li>
   *    <li>PER - Peru</li>
   *    <li>VEN - Venezuela</li>
   *    <li>ECU - Ecuador</li>
   * </ul>
   * @return Braspag country code for this profile.
   */
  public String getBraspagCountryCode() {
    String paymentCountry = Profile.getIntlCredential(1);
    if(paymentCountry.equals("BRA")         // Brasil
        || paymentCountry.equals("ARG")     // Argentina
        || paymentCountry.equals("MEX")     // Mexico
        || paymentCountry.equals("COL")     // Colombia
        || paymentCountry.equals("CHL")     // Chile
        || paymentCountry.equals("PER")     // Peru
        || paymentCountry.equals("VEN")     // Venezuela
        || paymentCountry.equals("ECU"))    // Ecuador
      return paymentCountry;
    throw new RuntimeException("Unsupported Braspag country ("+paymentCountry+")");
  }
  
  public String getCardId()
  {
    return( CardId );
  }
  
  public String getCvv2ResultCode()
  {
    String            cvv2Result  = "";
    ResponseDMessage  resp        = getVisaDResponse();
    
    if ( resp != null )
    {
      // get the cvv2 and avs results (if present)
      if ( resp.hasCvv2ResultCode() )
      {
        cvv2Result = resp.getCvv2ResultCode();
      }
    }
    return( cvv2Result );      
  }
  
  public String getAvsStreet( )
  {
    return( processString(AvsStreet) );
  }
  
  public String getAvsZip( )
  {
    return( processString(AvsZip) );
  }
  
  public String getCardDataLoadError( )
  {
    return( CardDataLoadError );
  }
  
  public String getCardholderEmail()
  {
    return( CardholderEmail );
  }
  
  public String getCardholderFirstName()
  {
    return( processString(CardholderFirstName) );
  }
  
  public String getCardholderLastName()
  {
    return( processString(CardholderLastName) );
  }
  
  public String getCardholderName()
  {
    String  retVal  = getCardholderFirstName() + " " + getCardholderLastName();
    
    if ( retVal.trim().equals("") )
    {
      retVal = "NOT/AVAILABLE";
    }
    return( retVal );
  }
  
  public String getCardholderPhone()
  {
    return( CardholderPhone );
  }
  
  public String getCardholderRefNum()
  {
    return( CardholderRefNum );
  }
  
  public String getCardholderState()
  {
    return( processString(CardholderState) );
  }
  
  public String getCardIdCode()
  {
    String      retVal    = "N";  // card not present
    
    if ( CardPresent.equals("1") )
    {
      if ( getCardSwipe().equals("") )
      {
        retVal = "M";   // cardholder signature, bad mag reader
      }
      else
      {
        retVal = "@";   // cardholder signature
      }        
    }
    return( retVal );
  }
  
  public String getCardinalCentinelUrl( )
  {
    return( CardinalCentinelUrl );
  }
  
  public String getCardPresent()
  {
    return( processString(CardPresent) );
  }
  
  public String getCardPresentFlag()
  {
    return( (getCardPresent().equals("1") ? "Y" : "N") );
  }
  
  public int getCardStoreLevel()
  {
    return( (Profile == null) ? 0 : Profile.getCardStoreLevel() );
  }
  
  public long getCardStoreNodeId()
  {
    int       level     = getCardStoreLevel();
    long      nodeId    = getMerchantId();
    
    if ( level > 0 )
    {
      nodeId = ApiDb.getCardStoreNodeId(nodeId,level);
    }
    return( nodeId );
  }
  
  public String getCardSwipe()
  {
    return( processString(CardSwipe) );
  }
  
  public String getCardSwipeFull()
  {
    String      retVal    =   null;
    
    try
    {
      retVal = new String(MesEncryption.getClient().decrypt(CardSwipeEnc));
    }
    catch( Exception e )
    {
      log.error("getCardSwipeFull() - " + e.toString());
      SyncLog.LogEntry(this.getClass().getName() + "::" + "getCardSwipeFull()", e.toString());
    }
    return( retVal );
  }
  
  public String getCityPhone()
  {
    String  retVal  = DbaCity;
    String  phone   = getCustServicePhone();
    
    if ( phone != null && !phone.trim().equals("") )
    {
      retVal = getCustServicePhoneFormatted();
    }
    return(retVal);
  }
  
  public String getCustServicePhone( )
  {
    String retVal   = super.getCustServicePhone();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getPhoneNumber();
    }
    return( retVal );
  }
  
  public String getCvv2()
  {
    return( Cvv2 );
  }
  
  public String getCvv2Formatted()
  {
    String      prefix    = "11";   // value provide, return resp and result
    String      retVal    = null;
    
    if ( hasCvv2() )
    {
      if ( getCardType().equals("AM") )
      {
        // 11 = value provided, return resp & cid result
        // 10 = value provided, return resp only
        prefix = (isAmexCIDEnabled() ? "11" : "10");
      }
      retVal = (prefix + StringUtilities.rightJustify(Cvv2,4,' '));
    }
    return( retVal );
  }
  
  public String getDbaCity()
  {
    String    retVal  = super.getDbaCity();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getDbaCity();
    }
    return( retVal );
  }
  
  public String getDbaName()
  {
    String retVal = super.getDbaName();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getDbaName();
    }
    return(retVal);
  }
  
  public String getDbaState()
  {
    String      retVal    = super.getDbaState();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getDbaState();
    }
    return( retVal );
  }
  
  public String getDbaZip( )
  {
    String retVal = super.getDbaZip();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getDbaZip();
    }
    return( retVal );
  }
  
  public String getDebitPinBlock()
  {
    return( DebitPinBlock );
  }
  
  public String getDebitKeySerialNumber()
  {
    return( DebitKeySerialNumber );
  }
  
  public String getDeveloperId()
  {
    return( DeveloperId );
  }
  
  public String getDMContactInfo()
  {
    String retVal   = super.getDMContactInfo();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getDMContactInfo();
    }
    return( retVal );
  }
  
  public String getDynamicDbaNamePrefix()
  {
    return( Profile.getDynamicDbaNamePrefix() );
  }
  
  public String getEmulationString( )
  {
    String    retVal    = "TPG";
    
    switch( Emulation )
    {
      case TridentApiConstants.EMU_AUTHORIZE_NET:
        retVal = "AUTHNET";
        break;
        
      default:
        break;
    }
    return( retVal );
  }
  
  public String getErrorCode( )
  {
    return( ErrorCode );
  }
  
  public String getErrorDesc( )
  {
    return( ErrorDesc );
  }
  
  /**
   * Returns the card expiry date in the format MMyy. When the expiry date is null or empty string, "0000" is returned.
   * @return
   */
  public String getExpDate()
  {
	  return(getExpDate("MMyy"));
  }

  /**
   * Gets the expiry date in a specific format. When the date is null or empty string, "0000" is returned.
   * @param format
   * @return
   */
  public String getExpDate(String format) {
	  String      retVal    = "0000";

	  if ( ExpDate != null && !ExpDate.equals("") )
	  {
		  retVal = ExpDate;

		  // if this transaction is a recurring payment then
		  // set the expiration date to "0000" if the card has expired.
		  if ( isRecurringPayment() )
		  {
			  Calendar        cal       = Calendar.getInstance();
			  java.util.Date  testDate  = DateTimeFormatter.parseDate(ExpDate,format);

			  // force the date to the first of the month
			  cal.set(Calendar.DAY_OF_MONTH,1);

			  // clear the time elements
			  cal.clear(Calendar.HOUR);
			  cal.clear(Calendar.HOUR_OF_DAY);
			  cal.clear(Calendar.MINUTE);
			  cal.clear(Calendar.SECOND);
			  cal.clear(Calendar.MILLISECOND);

			  if ( testDate == null || testDate.before(cal.getTime()) )
			  {
				  retVal = "0000";
			  }
		  }
	  }
	  if(!format.equals("MMyy")) {
		  
		  try {
			SimpleDateFormat source = new SimpleDateFormat("MMyy");
			Date sourceDate = source.parse(retVal);
			
			SimpleDateFormat dest = new SimpleDateFormat(format);
			retVal = dest.format(sourceDate);
		} catch (ParseException e) { } 
	  }
	  return( retVal );
  }
  
  public org.apache.axis.types.UnsignedByte getExpDateMonth( )
  {
    int   retVal = 0;
    
    try{ retVal = Integer.parseInt(ExpDate.substring(0,2)); } catch (Exception e){}
    return( new org.apache.axis.types.UnsignedByte(retVal) );
  }
  
  public short getExpDateYear( )
  {
    short retVal = 0;
    
    try
    { 
      // start with the current year
      StringBuffer  temp  = new StringBuffer( String.valueOf(Calendar.getInstance().get(Calendar.YEAR)) );
      // replace last two digits with value from input string
      temp.replace(2,4,ExpDate.substring(2,4));
      // convert to number
      retVal = Short.parseShort(temp.toString()); 
    } 
    catch (Exception e)
    {
      // return -1
    }
    return( retVal );
  }
  
  public String getExternalTranId( )
  {
    return( ExternalTranId );
  }
  
  public String getFraudResult( )
  {
    return( processString(FraudResult) );
  }

  public String getFraudResultCodes( )
  {
    return( processString(FraudResultCodes) );
  }
  
  public int getFxClientId( )
  {
    return( Profile.getFxClientId() ); 
  }
  
  public int getFxProductType( )
  {
    return( Profile.getFxProductType() );
  }
  
  public ForeignExchangeRate getFxRate( )
  {
    //
    // if a rate has not been loaded and the rate id is non-zero OR
    // the current rate object no longer matches to requested rate id 
    // then reload the rate information from the database
    //
    if ( (FxRate == null && getFxRateId() != 0) ||
         (FxRate != null && FxRate.getRateId() != getFxRateId()) )
    {
      ForeignExchangeRate fxRate = new ForeignExchangeRate( getMerchantId(), getFxRateId() );
      
      if ( fxRate != null && fxRate.isValid() )
      {
        FxRate = fxRate;
      }
    }
    return( FxRate );
  }
  
  /**
   * Returns GTS gateway account ID credentials from the first profile International credentials field.
   * @return account ID if valid, -1 if not an integer
   */
  public int getGtsAccountId()
  {
	  try {
		  int temp = Integer.parseInt(Profile.getIntlCredential(0));
		  return temp > 0 ? temp : -1;
	  } catch( Exception e ) {
		  return -1;
	  }
  }

  /**
   * Returns the converted GTS currency code enum {@link CurrencyTypesIDs}.
   * @param isoCode
   * @return The converted {@link CurrencyTypesIDs} for currencyCode. Null if not supported.
   */
  public CurrencyTypesIDs getGtsCurrencyCode(String currencyCode) {
	  boolean isNumber = false;
	  int numericCode = -1;

	  try {
		  numericCode = Integer.parseInt(currencyCode);
		  isNumber = true;
	  }
	  catch( NumberFormatException e ) { }
	  
	  if(!isNumber) {
		  CurrencyCodeConverter converter = CurrencyCodeConverter.getInstance();
		  try {
			  numericCode = Integer.parseInt(converter.alphaCodeToNumericCode( currencyCode ));
		  } catch(NumberFormatException e) {
			  return null;
		  }
	  }
	  
	  // Map of supported GTS currencies
	  switch(numericCode) {
	  case 784: return CurrencyTypesIDs.AED;
	  case 032: return CurrencyTypesIDs.ARS;
	  case 036: return CurrencyTypesIDs.AUD;
	  case 986: return CurrencyTypesIDs.BRL;
	  case 124: return CurrencyTypesIDs.CAD;
	  case 756: return CurrencyTypesIDs.CHF;
	  case 208: return CurrencyTypesIDs.DKK;
	  case 978: return CurrencyTypesIDs.EUR;
	  case 826: return CurrencyTypesIDs.GBP;
	  case 344: return CurrencyTypesIDs.HKD;
	  case 376: return CurrencyTypesIDs.ILS;
	  case 356: return CurrencyTypesIDs.INR;
	  case 392: return CurrencyTypesIDs.JPY;
	  case 440: return CurrencyTypesIDs.LTL;
	  case 484: return CurrencyTypesIDs.MXN;
	  case 458: return CurrencyTypesIDs.MYR;
	  case 578: return CurrencyTypesIDs.NOK;
	  case 554: return CurrencyTypesIDs.NZD;
	  case 608: return CurrencyTypesIDs.PHP;
	  case 946: return CurrencyTypesIDs.RON;
	  case 643: return CurrencyTypesIDs.RUB;
	  case 752: return CurrencyTypesIDs.SEK;
	  case 702: return CurrencyTypesIDs.SGD;
	  case 949: return CurrencyTypesIDs.TRY;
	  case 840: return CurrencyTypesIDs.USD;
	  case 710: return CurrencyTypesIDs.ZAR;
	  default: return null;
	  }
  }

  /**
   * Returns GTS formatted Dynamic Descriptor, or regular DBA if DDBA is not applicable.<br />
   * <b>GTS Format:</b> 3 Characters + "*" (mandatory) + 1 - 21 characters (mandatory)
   * @return
   */
  public String getGtsDynamicDescriptor()
  {
	  if(!Profile.isDynamicDbaDataAllowed()) return getDbaName();

	  StringBuffer buff = new StringBuffer();
	  buff.append(Profile.getDynamicDbaNamePrefix()).append("*").append(Profile.getDbaName());
	  if(buff.length() > 25) buff.setLength(25);
	  return buff.toString();
  }

  public String getGtsEndpoint( )
  {
	  return( GtsEndpoint );
  }
  
  public TransactionResponsePacket getGtsResult() {
	  return GtsResult;
  }

  public int getGtsSubAccountId()
  {
	  return getGtsSubAccountId(getCurrencyCode());
  }

  /**
   * GTS sub-accounts are related to a currency code map unique to GTS.
   * @param currencyCode
   * @return Converted GTS Currency Code if valid, -1 if invalid/unsupported.
   */
  public int getGtsSubAccountId(String currencyCode) {
	  boolean isNumber = false;
	  int numericCode = -1;

	  try {
		  numericCode = Integer.parseInt(currencyCode);
		  isNumber = true;
	  }
	  catch( NumberFormatException e ) { }
	  
	  if(!isNumber) {
		  CurrencyCodeConverter converter = CurrencyCodeConverter.getInstance();
		  try {
			  numericCode = Integer.parseInt(converter.alphaCodeToNumericCode( currencyCode ));
		  } catch(NumberFormatException e) {
			  return -1;
		  }
	  }
	  
	  // Map of supported GTS currencies
	  switch(numericCode) {
	  case 784: return 1; 	//AED
	  case 032: return 2;	//ARS
	  case 036: return 3; 	//AUD
	  case 986: return 4;	//BRL 
	  case 124: return 5;	//CAD 
	  case 756: return 6;	//CHF 
	  case 208: return 7;	//DKK 
	  case 978: return 8; 	//EUR
	  case 826: return 9; 	//GBP
	  case 344: return 10; 	//HKD
	  case 376: return 11; 	//ILS
	  case 356: return 12; 	//INR
	  case 392: return 13; 	//JPY
	  case 440: return 14; 	//LTL
	  case 484: return 15; 	//MXN
	  case 458: return 16; 	//MYR
	  case 578: return 17; 	//NOK
	  case 554: return 18; 	//NZD
	  case 608: return 19; 	//PHP
	  case 946: return 20; 	//RON
	  case 643: return 21; 	//RUB
	  case 752: return 22; 	//SEK
	  case 702: return 23; 	//SGD
	  case 949: return 24; 	//TRY
	  case 840: return 25; 	//USD
	  case 710: return 26; 	//ZAR
	  default: return -1;
	  }
  }
  
  public String getIndustryCode( )
  {
    return( IndustryCode );
  }
  
  public String getInputErrorCode( )
  {
    return( InputErrorCode );
  }
  
  public int getIntlProcessor( )
  {
    return( Profile.getIntlProcessor() );
  }
  
  public String getIpAddress( )
  {
    return( IpAddress );
  }
  
  public String getIpAddressFormatted( )
  {
    String[]    octets  = processString( getIpAddress() ).split("\\.");
    String      retVal  = "";
    
    if ( octets.length == 4 )
    {
      retVal  = StringUtilities.rightJustify(octets[0],3,'0') + "." +
                StringUtilities.rightJustify(octets[1],3,'0') + "." +
                StringUtilities.rightJustify(octets[2],3,'0') + "." +
                StringUtilities.rightJustify(octets[3],3,'0');
    }
    return( StringUtilities.leftJustify(retVal,15,' ') );
  }
  
  public String getItpsAuthCode() {
	  return ItpsAuthCode;
  }
  
  public String getItpsAddressResult() {
	  return ItpsAddressResult;
  }
  
  public String getItpsZipResult() {
	  return ItpsZipResult;
  }

  public String getItpsCvvResult() {
	  return ItpsCvvResult;
  }
  
  public boolean getItpsFlag() {
	  return ItpsFlag;
  }

  public String getItpsEndpoint() {
	  return ItpsEndpoint;
  }

  public String getLevelIIICardType()
  {
    return( LevelIIICardType );
  }
  
  public String getLevelIIIField(String fname)
  {
    return( getLevelIIIField(fname,null) );
  }
  
  public String getLevelIIIField(String fname, String defaultValue)
  {
    String retVal = (String)LevelIIIFields.get(fname);
    return( ((retVal == null) ? defaultValue : retVal) );
  }
  
  public java.sql.Date getLevelIIIFieldAsDate(String fname)
  {
    return( getLevelIIIFieldAsDate(fname,"yyMMdd") );
  }
  
  public java.sql.Date getLevelIIIFieldAsDate(String fname, String format)
  {
    java.util.Date  javaDate  = null;
    java.sql.Date   retVal    = null;
    
    try 
    { 
      javaDate = DateTimeFormatter.parseDate( (String)LevelIIIFields.get(fname), format ); 
      if ( javaDate != null )
      {
        retVal = new java.sql.Date( javaDate.getTime() );
      }
    }
    catch( Exception e ) 
    {
    }
    return( retVal );
  }
  
  public double getLevelIIIFieldAsDouble(String fname)
  {
    return( getLevelIIIFieldAsDouble(fname,0.0) );
  }
  
  public double getLevelIIIFieldAsDouble(String fname, double defaultValue)
  {
    double value = defaultValue;
    try { value = Double.parseDouble( (String)LevelIIIFields.get(fname) ); }
    catch( Exception e ) { }
    return( value );
  }
  
  public int getLevelIIIFieldAsInt(String fname)
  {
    return( getLevelIIIFieldAsInt(fname,0) );
  }
  
  public int getLevelIIIFieldAsInt(String fname, int defaultValue)
  {
    int value = defaultValue;
    try { value = Integer.parseInt( (String)LevelIIIFields.get(fname) ); }
    catch( Exception e ) { }
    return( value );
  }
  
  public List getLineItemData( )
  {
    return( LineItemData );
  }
  
  public double getLinkedTranAmount()
  {
    return( LinkedTranAmount );
  }
  
  // overload base class and return profile value
  public long getMerchantId( )
  {
    return( Long.parseLong( Profile.getMerchantId() ) );
  }
  
  public double getOriginalFxAmount()
  {
    return( OriginalFxAmount );
  }
  
  public double getOriginalTranAmount()
  {
    return( OriginalTranAmount );
  }
  
  public String  getOriginalTranId()
  {
    return( OriginalTranId );
  }
  
  public String getPayload3D( )
  {
    return( Payload3D );
  }
  
  public int getPayVisionAccountType( )
  {
    String  ecomm   = ( TridentApiConstants.FV_MOTO_SECURE_ECOMMERCE          +
                        TridentApiConstants.FV_MOTO_NON_AUTH_SECURE_ECOMMERCE +
                        TridentApiConstants.FV_MOTO_ECOMMERCE                 +
                        TridentApiConstants.FV_MOTO_NON_SECURE_ECOMMERCE );
    int     retVal  = -1;                        
    
    if ( isRecurringPayment() )
    {
      retVal = TridentApiConstants.PAYVISION_ACCT_TYPE_RECURRING;
    }
    else if ( ecomm.indexOf( getMotoEcommIndicator() ) >= 0 )
    {
      retVal = TridentApiConstants.PAYVISION_ACCT_TYPE_ECOMMERCE;
    }
    else if ( isCardPresent() )
    {
      retVal = TridentApiConstants.PAYVISION_ACCT_TYPE_RETAIL;
    }
    else    // MOTO
    {
      retVal = TridentApiConstants.PAYVISION_ACCT_TYPE_MOTO;
    }
    return( retVal );
  }
  
  public String getPayVisionCDCValue( String cdcName, String fieldName )
  {
    return( getPayVisionCDCValue( cdcName, fieldName, null ) );
  }
  
  public String getPayVisionCDCValue( String cdcName, String fieldName, String defaultValue )
  {
    TransactionResult   pvResp    = getPayVisionResult();
    String              retVal    = defaultValue;
    
    if ( pvResp != null )
    {
      // look for a specific message
      CdcEntry[]      cdcs        = pvResp.getCdc().getCdcEntry();
      CdcEntryItem[]  cdcItems    = null;
      
      for( int cdcIdx = 0; (cdcs != null && cdcIdx < cdcs.length); ++cdcIdx )
      {
        if ( cdcs[cdcIdx].getName().equals(cdcName) )
        {
          cdcItems = cdcs[cdcIdx].getItems().getCdcEntryItem();
          for( int cdcItemIdx = 0; cdcItemIdx < cdcItems.length; ++cdcItemIdx )
          {
            if ( cdcItems[cdcItemIdx].getKey().equalsIgnoreCase(fieldName) )
            {
              retVal = cdcItems[cdcItemIdx].getValue();
              break;
            }
          }
          break;
        }
      }
    }
    return( retVal );
  }
  
  public int getPayVisionCountryCode()
  {
    return( Integer.parseInt(TridentApiConstants.FV_COUNTRY_CODE_USD) );
  }
  
  public int getPayVisionCurrencyCode()
  {
    return( Integer.parseInt(getCurrencyCode()) );
  }
  
  public String getPayVisionDynamicDescriptor()
  {
    StringBuffer  retVal  = new StringBuffer();
    
    retVal.append( getDbaName() );
    if ( retVal.length() > 25 )
    {
      retVal.setLength(25);
    }
    retVal.append("|");
    retVal.append(getCustServicePhoneFormatted());
    return( retVal.toString() );
  }
  
  public String getPayVisionEndpoint( )
  {
    return( PayVisionEndpoint );
  }
  
  public String getPayVisionErrorCode()
  {
    String              defaultValue  = getErrorCode();
    TransactionResult   pvResp        = getPayVisionResult();
    
    if ( pvResp != null )
    {
      defaultValue = String.valueOf( pvResp.getResult() );
    }
    return( getPayVisionCDCValue( "ErrorInformation", "ErrorCode", defaultValue ) );
  }
  
  public String getPayVisionErrorDesc()
  {
    String              defaultValue  = getErrorDesc();
    TransactionResult   pvResp        = getPayVisionResult();
    
    if ( pvResp != null )
    {
      defaultValue = pvResp.getMessage();
    }
    return( getPayVisionCDCValue( "ErrorInformation", "ErrorMessage", defaultValue ) );
  }
  
  public String getPayVisionMemberGuid()
  {
    String  retVal  = "00000000-0000-0000-0000-000000000000";
    String  temp    = Profile.getIntlCredential(1);
    
    if ( temp != null )
    {
      retVal = temp;
    }
    return( retVal );
  }
  
  public int getPayVisionMemberId()
  {
    int     retVal  = -1;
    
    try
    {
      int temp = Integer.parseInt(Profile.getIntlCredential(0));
    
      if ( temp > 0 )
      {
        retVal = temp;
      }
    }
    catch( Exception e )
    {
      // ignore, return -1
    }
    return( retVal );
  }
  
  public String getPayVisionResponseCode()
  {
    String              respCode      = null;
    TransactionResult   pvResp        = getPayVisionResult();

    if ( pvResp != null )
    {
      respCode = String.valueOf( pvResp.getResult() );
      while( respCode.length() < 3 )
      {
        respCode = ("0" + respCode);  // leading zeros
      }
    }
    
    return( respCode );
  }
  
  public String getPayVisionResponseText()
  {
    String              respText      = null;
    TransactionResult   pvResp        = getPayVisionResult();
    
    if ( pvResp != null )
    {
      respText  = getPayVisionCDCValue("BankInformation","BankMessage");
    }
    
    return( respText );
  }
  
  
  public TransactionResult getPayVisionResult()
  {
    return( PayVisionResult );
  }
  
  public String getPayVisionTranGuid()
  {
    StringBuffer  retVal  = new StringBuffer();
    String        tranId  = getExternalTranId();
    
    // convert to a formatted GUID
    // 01234567-8901-2345-6789-0123456789012
    retVal.append( tranId.substring(0,8) );
    retVal.append("-");
    retVal.append( tranId.substring(8,12) );
    retVal.append("-");
    retVal.append( tranId.substring(12,16) );
    retVal.append("-");
    retVal.append( tranId.substring(16,20) );
    retVal.append("-");
    retVal.append( tranId.substring(20) );
    
    return( retVal.toString() );
  }
  
  public int getPayVisionTranId()
  {
    return( (int)getTridentTranIdNumber() );
  }
  
  public TridentApiProfile getProfile( )
  {
    return( Profile );
  }
   
  public String getProfileId( )
  {
    String    retVal    = super.getProfileId();
    
    if( retVal == null || retVal.equals("") )
    {
      TridentApiProfile profile = getProfile();
      if ( profile != null )
      {
        retVal = profile.getProfileId();
      }
    }
    return( retVal );
  }
  
  public String getProfileKey( )
  {
    return( Profile.getProfileKey() );
  }
  
  public int getPurchaseIdLengthMax( )
  {
    int   retVal  = TridentApiConstants.FLM_PURCHASE_ID;
    
    if ( this instanceof AuthorizeNetTransaction )
    {
      retVal = TridentApiConstants.FLM_AN_PURCHASE_ID;
    }
    return( retVal );
  }
  
  public String getRequestData()
  {
    StringBuffer requestData   = new StringBuffer();
    
    requestData.setLength(0);
    requestData.append( TridentApiConstants.FN_TID + "=" + getProfileId() );
    requestData.append( "&" + TridentApiConstants.FN_TRAN_TYPE + "=" + getTranType() );
    requestData.append( "&" + TridentApiConstants.FN_AMOUNT + "=" );
    requestData.append( getTranAmount() == 0.0 ? (getAuthAmount()) : (getTranAmount()) );
    requestData.append( "&" + TridentApiConstants.FN_CARD_NUMBER + "=" + getCardNumber() );
    requestData.append( getExpDate().equals("0000") ? "" : ("&" + TridentApiConstants.FN_EXP_DATE + "=" + getExpDate()) );
    requestData.append( getCvv2() == null || getCvv2().equals("") ? "" : ("&" + TridentApiConstants.FN_CVV2 + "=" + "xxx") );
    requestData.append( getTaxAmount() == 0.0 ? "" : ("&" + TridentApiConstants.FN_TAX + "=" + getTaxAmount()) );
    requestData.append( getAuthCode() == null || getAuthCode().equals("") ? "" : ("&" + TridentApiConstants.FN_AUTH_CODE + "=" + getAuthCode()) );
    requestData.append( getAvsStreet() == null || getAvsStreet().equals("") ? "" : ("&" + TridentApiConstants.FN_AVS_STREET + "=" + getAvsStreet()) );
    requestData.append( getAvsZip() == null || getAvsZip().equals("") ? "" : ("&" + TridentApiConstants.FN_AVS_ZIP + "=" + getAvsZip()) );    
    requestData.append( getPurchaseId() == null || getPurchaseId().equals("") ? "" : ("&" + TridentApiConstants.FN_INV_NUM + "=" + getPurchaseId()) );
    requestData.append( getCurrencyCode() == null || getCurrencyCode().equals("") ? "&" : ("&" + TridentApiConstants.FN_CURRENCY_CODE + "=" + getCurrencyCode()) );    
    requestData.append( getFxAmount() == 0.0 ? "" : ("&" + TridentApiConstants.FN_FX_AMOUNT + "=" + getFxAmount()) );    
    requestData.append( getFxRateId() == 0.0 ? "" : ("&" + TridentApiConstants.FN_FX_RATE_ID + "=" + getFxRateId()) );    
    requestData.append( getCardId() == null || getCardId().equals("") ? "" : ("&" + TridentApiConstants.FN_CARD_ID + "=" + getCardId()) );    
    requestData.append( getOriginalTranId() == null || getOriginalTranId().equals("") ? "" : ("&" + TridentApiConstants.FN_TRAN_ID + "=" + getOriginalTranId()) );    
    requestData.append( getClientReferenceNumber() == null || getClientReferenceNumber().equals("") ? "" : ("&" + TridentApiConstants.FN_CLIENT_REF_NUM + "=" + getClientReferenceNumber()) );    
    requestData.append( getCardholderFirstName() == null || getCardholderFirstName().equals("") ? "" : ("&" + TridentApiConstants.FN_CH_FIRST_NAME + "=" + getCardholderFirstName()) );    
    requestData.append( getCardholderLastName() == null || getCardholderLastName().equals("") ? "" : ("&" + TridentApiConstants.FN_CH_LAST_NAME + "=" + getCardholderLastName()) );    
    requestData.append( getCardholderPhone() == null || getCardholderPhone().equals("") ? "" : ("&" + TridentApiConstants.FN_CH_PHONE + "=" + getCardholderPhone()) );    
    requestData.append( getCardholderEmail() == null || getCardholderEmail().equals("") ? "" : ("&" + TridentApiConstants.FN_CH_EMAIL + "=" + getCardholderEmail()) );    
    requestData.append( getShipToFirstName() == null || getShipToFirstName().equals("") ? "" : ("&" + TridentApiConstants.FN_SHIP_TO_FIRST_NAME + "=" + getShipToFirstName()) );    
    requestData.append( getShipToLastName() == null || getShipToLastName().equals("") ? "" : ("&" + TridentApiConstants.FN_SHIP_TO_LAST_NAME + "=" + getShipToLastName()) );    
    requestData.append( getShipToAddress() == null || getShipToAddress().equals("") ? "" : ("&" + TridentApiConstants.FN_SHIP_TO_ADDRESS + "=" + getShipToAddress()) );    
    return( requestData.toString() );
  }
  
  public Timestamp getRequestTimestamp()
  {
    return(RequestTimestamp);    
  }
  
  public String getResponseCode()
  {
    ResponseDMessage  resp    = getVisaDResponse();
    String            retVal  = null;
    
    if ( resp != null )
    {
      retVal = resp.getResponseCode();
    }
    return( retVal );
  }
  
  public HashMap getResponseFieldMap()
  {
    String[]  nvPairs = encodeResponse().split("&");
    HashMap   nvMap   = new HashMap(nvPairs.length);
    
    for ( int i = 0; i < nvPairs.length; ++i )
    {
      String[] nvPair = nvPairs[i].split("=");
      if ( nvPair.length >= 2 )
      {
        nvMap.put(nvPair[0],nvPair[1]);
      }
      else if ( nvPair.length == 1 )
      {
        nvMap.put(nvPair[0],"");
      }
    }
    return( nvMap );
  }
  
  public String getResponseText()
  {
    ResponseDMessage  resp    = getVisaDResponse();
    String            retVal  = null;
    
    if ( resp != null )
    {
      retVal = resp.getAuthorizationResponseText();
    }
    return( retVal );
  }
  
  public String getReturnedAci( )
  {
    String    retVal    = AciReturned;
    
    if ( VisaDResponse != null )
    {
      retVal = VisaDResponse.getReturnedACI();
    }
    return( retVal );
  }
  
  public java.util.Date getFraudAcctCreationDate()
  {
    return( FraudAcctCreationDate );
  }
  
  public String getFraudAcctEmail()
  {
    return( FraudAcctEmail );
  }
  
  public java.util.Date getFraudAcctLastChange()
  {
    return( FraudAcctLastChange );
  }
  
  public String getFraudAcctName()
  {
    return( FraudAcctName );
  }
  
  public String getFraudBrowserLanguage()
  {
    return( FraudBrowserLanguage );
  }
  
  public String getFraudDeviceId()
  {
    return( FraudDeviceId );
  }
  
  public boolean getFraudDigitalGoodsFlag()
  {
    return( FraudDigitalGoodsFlag );
  }
  
  public String getFraudEndpoint()
  {
    return( FraudEndpoint );
  }
  
  public int getFraudMode()
  {
    int     retVal    = TridentApiConstants.FM_NONE;
    
    if ( isFraudScanDisabled() )
    {
      retVal = TridentApiConstants.FM_NONE;
    }
    else
    {
      retVal = Profile.getFraudMode();
    }
    return( retVal );
  }
  
  public boolean getFraudSubscriptionFlag()
  {
    return( FraudSubscriptionFlag );
  }
  
  /**
   * @param tranId The transaction ID which will be returning in the response.
   * @return A SHA1 hash of the transaction ID, and profile Key. The truncated card number is additionally hashed if that control field was requested. Returns null if the hash cannot be calculated.
   */
  private String getSecurityHash(String tranId) {
    String retVal = null;
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("SHA1");
      md.reset();
      md.update(tranId.getBytes());
      md.update(getProfileKey().getBytes());
      
      // Add truncated card number to hash when truncated card number is requested
      if(ResponseCtlFlags.get(TridentApiConstants.FN_CARD_NUMBER_TRUNC) != null && getCardNumber() != null && !getCardNumber().equals(""))
        md.update(getCardNumber().getBytes());
      
      byte[] hashBytes = md.digest();
      retVal = ByteUtil.hexStringN(hashBytes).toLowerCase();
    } catch (NoSuchAlgorithmException e) {
      SyncLog.LogEntry("TridentApiTransaction::getSecurityHash", e);
    }
    return retVal;
  }
  
  public String getSicCode( )
  {
    String      retVal    = super.getSicCode();
    
    if ( retVal == null || retVal.equals("") )
    {
      retVal = Profile.getSicCode();
    }
    return( retVal );
  }
  
  public String getStoreNumber( )
  {
    return( getProfileId().substring(12,16) );
  }
  
  public long getSwitchDuration( )
  {
    return( SwitchDuration );
  }
  
  public String getTerminalNumber( )
  {
    return( getProfileId().substring(16) );
  }
  
  public String getTimeZone( )
  {
    return( Profile.getTimeZone() );
  }
  
  public String getTranCode( )
  {
    String    retVal    = "";
    String    tranType  = getTranType();
    
    if ( isAuthVerifyOnly() )
    {
      retVal = TridentApiConstants.VISAD_TC_CARD_VERIFY;  // card verify request
    }
    else if ( tranType.equals(TT_AUTH_REVERSAL) )
    {
      retVal = TridentApiConstants.VISAD_TC_REVERSAL;     // auth reversal
    }
    else if ( CardPresent.equals("1") )
    {
      retVal = TridentApiConstants.VISAD_TC_PURCHASE;     // card present
    }
    else
    {
      retVal = TridentApiConstants.VISAD_TC_CNP_PURCHASE; // card not present
    }
    return( retVal );
  }
  
  public String getTranId3D( )
  {
    return( TranId3D );
  }
  
  public long getTransactionDuration()
  {
    return( TransactionDuration );
  }
  
  public int getTranState( )
  {
    return(TranState);
  }
  
  public String getTridentTranId( )
  {
    String      retVal    = getApiTranId();

    if ( TridentTranId != null && !TridentTranId.equals("") )
    {
      retVal = TridentTranId;
    }
    return( retVal );
  }
  
  public long getTridentTranIdNumber()
  {
    return( TridentTranIdNumber );
  }
  
  public String getUcafAuthData()
  {
    return( UcafAuthData );
  }
  
  public String getUcafCollectionInd()
  {
    return( UcafCollectionInd );
  }
  
  public ResponseDMessage getVisaDResponse()
  {
    return( VisaDResponse );
  }
  
  public String getVisaCavv()
  {
    return( VisaCavv );
  }

  public String getVisaXid()
  {
    return( VisaXid );
  }

  public boolean has3DPayload()
  {
    boolean   retVal  = false;
    String    value   = getPayload3D();
    
    if ( value != null && !value.trim().equals("") )
    {
      retVal = true;
    }
    return( retVal );
  }  
  
  public boolean hasAvsData()
  {
    return( !isBlank(getAvsStreet()) && !isBlank(getAvsZip()) );
  }
  
  public boolean hasCardData()
  {
    boolean   retVal    = false;
    
    // has either the swipe or the card #, exp date combo
    if ( !getCardSwipe().equals("") ||
        (!getCardNumber().equals("") && !getExpDate().equals("")) )
    {
      retVal = true;
    }
    
    return( retVal );
  }
  
  public boolean hasCardDataLoadError( )
  {
    return( CardDataLoadError != null );
  }
  
  public boolean hasCardholderEmail()
  {
    return( !processString(CardholderEmail).equals("") );
  }
  
  public boolean hasCardholderFirstName()
  {
    return( !processString(CardholderFirstName).equals("") );
  }
  
  public boolean hasCardholderLastName()
  {
    return( !processString(CardholderLastName).equals("") );
  }
  
  public boolean hasCardholderPhone()
  {
    return( !processString(CardholderPhone).equals("") );
  }
  
  public boolean hasCardholderRefNum()
  {
    return( !processString(CardholderRefNum).equals("") );
  }
  
  public boolean hasCardholderState()
  {
    return( !processString(CardholderState).equals("") );
  }
  
  public boolean hasCvv2()
  {
    return( !processString(Cvv2).equals("") );
  }
  
  public boolean hasDebitData()
  {
    return( !processString(DebitPinBlock).equals("") &&
            !processString(DebitKeySerialNumber).equals("") );
  }
  
  public boolean hasAmexEnhancedAuthData()
  {
    return( !processString(getCardholderEmail()).equals("")     ||
            !processString(getCardholderFirstName()).equals("") ||
            !processString(getCardholderLastName()).equals("")  ||
            !processString(getCardholderPhone()).equals("") );
  }
  
  public boolean hasError()
  {
    boolean   retVal    = false;
    
    if ( ErrorCode != null && !ErrorCode.equals(TridentApiConstants.ER_NONE) )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public boolean hasLevelIIIData()
  {
    boolean retVal = false;
    if(hasLevelIIILineItemData())
      retVal = true;
    else if(LevelIIIFields != null) {
      if(LevelIIIFields.size() > 0)
        retVal = true;
      
      // Exclude shared field requester name from this check
      if(LevelIIIFields.containsKey(TridentApiConstants.FN_REQUESTER_NAME) && LevelIIIFields.size() == 1)
        retVal = false;
    }
    return retVal;
  }
  
  public boolean hasLevelIIILineItemData()
  {
    return( (LineItemData != null && LineItemData.size() > 0) );
  }
  
  public boolean hasUCAFData()
  {
    return( getUcafAuthData() != null );
  }
  
  public boolean hasVerifiedByVisaData()
  {
    return( getVisaCavv() != null );
  }
  
  public boolean holdForReview()
  {
    return( HoldForReview );
  }
  
  public boolean is3dSecureRequest()
  {
    String    cardType  = getCardType();
    boolean   retVal    = false;
    
    if ( cardType.equals("MC") && hasUCAFData() )
    {
      retVal = true;
    }
    else if ( cardType.equals("VS") && hasVerifiedByVisaData() )
    {
      retVal = true;
    }
    
    // only allow non-USD 3D transactions to be processed
    // when international 3D is enabled
    if ( (useAdyen() || usePayVision()) && !isIntl3DEnabled() )
    {
      retVal = false;
    }
    
    return( retVal );
  }
  
  public boolean isAmexCIDEnabled()
  {
    return( (Profile == null) ? false : Profile.isAmexCIDEnabled() );
  }
  
  public boolean isApproved()
  {
    boolean   retVal    = false;
    
    if ( useAdyen() )
    {
      retVal = ( AdyenResult != null && 
                 "Authorised".equals(AdyenResult.getResultCode()) );
    }
    else if ( usePayVision() )
    {
      retVal = ( PayVisionResult != null && 
                 PayVisionResult.getResult() == TridentApiConstants.PAYVISION_SUCCESS);
    }
    else if( useGts() ) {
    	retVal = GtsResult != null && GtsResult.getSuccessFlag().booleanValue();
    }
    else if( useItps() ) {
    	retVal = ItpsErrorCode.equals("0");
    }
    else
    {
      retVal = isApprovedByIssuer();
                 
      if ( retVal == true )
      {
        // check for user decline reasons
        if ( Profile.isAvsResultDeclined(VisaDResponse.getAVSResultCode()) )
        {
          retVal = false;
        }
        
        if ( VisaDResponse.hasCvv2ResultCode() &&
             Profile.isCvv2ResultDeclined(VisaDResponse.getCvv2ResultCode()) )
        {
          retVal = false;
        }
      }             
    }
    return( retVal );
  }
  
  public boolean isApprovedByIssuer()
  {
    boolean     retVal      = false;
    
    if ( VisaDResponse != null )
    {
      String rc = VisaDResponse.getResponseCode();
      if ( rc.equals(TridentApiConstants.RESP_APPROVED) )
      {
        retVal = true;  // rc = 00
      }
      else if ( isPartialAuthRequest() && rc.equals(TridentApiConstants.RESP_PARTIAL_APPROVAL) )
      {
        retVal = true;  // rc = 10
      }
      else if ( isAuthVerifyOnly() && rc.equals(TridentApiConstants.RESP_NO_REASON_TO_DECLINE) )
      {
        retVal = true;  // rc = 85
      }
    }
    return( retVal );
  }
  
  public boolean isAssocCardStoreAllowed()
  {
    return( (Profile == null) ? false : Profile.isAssocCardStoreAllowed() );
  }
  
  public boolean isAuthVerifyOnly()
  {
    return( isAuthVerifyOnly(getTranType()) );
  }
  
  public boolean isAuthVerifyOnly( String tranType )
  {
    return( tranType.equals(TT_CREDIT) || 
            tranType.equals(TT_CARD_VERIFY) || 
            tranType.equals(TT_STORE_CARD) );
  }
  
  public boolean isBatchRequest()
  {
  		return (BatchRequest);
  }

  public boolean isBmlRequest( )
  {
    return( TranType.equals(TT_BML_REQUEST) );
  }
  
  public boolean isCardDataValid()
  {
    int   dataSourceCount = 0;
    
    for( int i = 0; i < IncomingCardData.length; ++i )
    {
      if ( IncomingCardData[i] != null )
      {
        dataSourceCount++;
      }
    }
    // should only be one valid source 
    // of card data per request
    return( dataSourceCount == 1 );  
  }
  
  public boolean isCardPresent()
  {
    return( getCardPresentFlag().equals("Y") );
  }
  
  public boolean isCardStoreRequested()
  {
    return( CardStoreRequested );
  }
  
  public boolean isCashAdvance()
  {
    return( TT_CASH_ADVANCE.equals( getTranType() ) );
  }
  
  public boolean isCashAdvanceSic()
  {
    String sicCode = getSicCode();
    return( "6010".equals(sicCode) || "6011".equals(sicCode) );
  }
  
  public boolean isContactlessSwipe()
  { 
    return( Contactless );
  }
  
  public boolean isCredit( )
  {
    return( isCredit(TranType) );
  }
  
  public boolean isCredit( String status )
  {
    return( status.equals(TT_CREDIT) );
  }
  
  public boolean isDeveloperIdValid()
  {
    int       devIdLen    = processString(getDeveloperId()).length();
    boolean   retVal      = false;
    
    if ( devIdLen > 0 && devIdLen <= TridentApiConstants.FLM_DEVELOPER_ID )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public boolean isDynamicDbaDataAllowed()
  {
    return( Profile != null && Profile.isDynamicDbaDataAllowed() );
  }
  
  public boolean isDynamicDbaNamePrefixEditEnabled()
  {
    return( Profile != null && Profile.isDynamicDbaNamePrefixEditEnabled() );
  }
  
  
  public boolean isFullRefund()
  {
    return( TranType.equals(TT_REFUND) && (getTranAmount() == 0.0) );
  }
  
  public boolean isFxAmountValid()
  {
    ForeignExchangeRate   fxRate        = null;
    boolean               retVal        = false;
    double                thresholdRate = 0.01;
    
    try
    {
      thresholdRate = (double)(MesDefaults.getInt(MesDefaults.DK_FX_CONVERSION_TOLERANCE) * 0.0001);
    }
    catch( Exception e )
    {
      // ignore, use default of 1%
    }
    
    fxRate = getFxRate();
    
    if ( fxRate != null && fxRate.isValid() )
    {
      double tranAmount = getTranAmount();
      double threshold  = (tranAmount * thresholdRate);  
      double calcAmount = fxRate.convertAmount( getFxAmount() );
      
      if ( calcAmount >= (tranAmount - threshold) && calcAmount <= (tranAmount + threshold) )
      {
        retVal = true;
      }
    }
    return( retVal );
  }
  
  public boolean isFxCurrencyValid()
  {
    String                currencyCode  = getCurrencyCodeAlpha();
    ForeignExchangeRate   fxRate        = null;
    boolean               retVal        = false;
    
    fxRate = getFxRate();
    
    if ( fxRate != null )
    {
      String fxCurrency = fxRate.getConsumerCurrencyCode();
      
      if ( fxCurrency != null && currencyCode != null && 
           fxCurrency.equals(currencyCode) )
      {
        retVal = true;
      }
    }
    return( retVal );
  }
  
  public boolean isFxEnabled()
  {
    return( Profile.isFxEnabled() );
  }
  
  public boolean isFxRateIdExpired( )
  {
    boolean   retVal  = false;    // per jim, assume rate is valid 
  
    try
    {
      ForeignExchangeRate rate = getFxRate();
      
      if ( isRecurringPayment() )
      {
        retVal = rate.isExpiredRecurring();
      }
      else
      {
        retVal = rate.isExpired();
      }        
    }
    catch( Exception e )
    {
      log.error("isFxRateIdExpired(" + getFxRateId() + ") - " + e.toString());
      SyncLog.LogEntry(this.getClass().getName() + "::isFxRateIdExpired(" + getFxRateId() + ")", e.toString());
    }
    return( retVal );
  }
  
  public boolean isFxRateIdValid()
  {
    boolean   retVal  = false;
  
    try
    {
      ForeignExchangeRate rate = getFxRate();
      retVal = rate.isValid();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + "::isFxRateIdValid(" + getFxRateId() + ")", e.toString());
    }
    return( retVal );
  }
  
  public boolean isFxTransaction( )
  {
    return( isNonUSCurrency() && isFxEnabled() );
  }
  
  public boolean isFxTranType( )
  {
    return( TranType.equals(TT_FX_CONVERT_AMOUNT) ||
            TranType.equals(TT_FX_GET_RATE) );
  }
  
  public boolean isHttpGetAllowed()
  {
    return( (Profile == null) ? false : Profile.isHttpGetAllowed() );
  }
  
  /**
   * TODO
   * @return
   */
  public boolean isIndustryCodeValid() {
    return true;
  }
  
  public boolean isIntl3DEnabled()
  {
    return( Intl3DEnabled );
  }

  public boolean isIntlRequest( boolean intlUSDEnabled )
  {  
    // not US currency or ((request or profile routing) and v/mc)
    return( isNonUSCurrency() || ((isIntlUSDRequest() || intlUSDEnabled) && isCardTypeVMC()) );
  }
  
  public boolean isIntlUSDRequest()
  {
    return( IntlUSDRequest );
  }
  
  public boolean isLevelIIIDataValid()
  {
    int           itemCount  = getLevelIIIFieldAsInt(TridentApiConstants.FN_LINE_ITEM_COUNT);
    double        vatRate    = getLevelIIIFieldAsDouble(TridentApiConstants.FN_VAT_RATE);
    double		  altTax	 = getLevelIIIFieldAsDouble(TridentApiConstants.FN_ALT_TAX_AMOUNT);
    double 		  vatAmt	 = getLevelIIIFieldAsDouble(TridentApiConstants.FN_VAT_AMOUNT, -1.0);
    double 		  disAmt	 = getLevelIIIFieldAsDouble(TridentApiConstants.FN_DISCOUNT_AMOUNT, -1.0);
    double 		  shpAmt	 = getLevelIIIFieldAsDouble(TridentApiConstants.FN_SHIPPING_AMOUNT, -1.0);
    double 		  dtyAmt	 = getLevelIIIFieldAsDouble(TridentApiConstants.FN_DUTY_AMOUNT, -1.0);
    
    if( getLevelIIIField(TridentApiConstants.FN_MERCHANT_TAX_ID,"").length() > TridentApiConstants.FLM_MERCHANT_TAX_ID )
    {
      setError( TridentApiConstants.ER_INVALID_MERCHANT_TAX_ID );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_CUSTOMER_TAX_ID,"").length() > TridentApiConstants.FLM_CUSTOMER_TAX_ID )
    {
      setError( TridentApiConstants.ER_INVALID_CUSTOMER_TAX_ID );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_SUMMARY_COMMODITY_CODE,"").length() > TridentApiConstants.FLM_SUMMARY_COMMODITY_CODE )
    {
      setError( TridentApiConstants.ER_INVALID_SUMMARY_COMMODITY_CODE );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_SHIP_FROM_ZIP,"").length() > TridentApiConstants.FLM_SHIP_FROM_ZIP )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_FROM_ZIP );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_VAT_INVOICE_NUMBER,"").length() > TridentApiConstants.FLM_VAT_INVOICE_NUMBER )
    {
      setError( TridentApiConstants.ER_INVALID_VAT_INVOICE_NUMBER );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT_IND,"").length() > TridentApiConstants.FLM_ALTERNATE_TAX_IND )
    {
      setError( TridentApiConstants.ER_INVALID_ALTERNATE_TAX_IND );
    }
    else if( getLevelIIIField(TridentApiConstants.FN_REQUESTER_NAME,"").length() > TridentApiConstants.FLM_REQUESTER_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_REQUESTER_NAME );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_DISCOUNT_AMOUNT) && (disAmt > TridentApiConstants.MAX_LIII_AMOUNTS || disAmt < 0.0) )
    {
      setError( TridentApiConstants.ER_INVALID_DISCOUNT_AMOUNT );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_SHIPPING_AMOUNT) && (shpAmt > TridentApiConstants.MAX_LIII_AMOUNTS || shpAmt < 0.0) )
    {
      setError( TridentApiConstants.ER_INVALID_SHIPPING_AMOUNT );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_DUTY_AMOUNT) && (dtyAmt > TridentApiConstants.MAX_LIII_AMOUNTS || dtyAmt < 0.0) )
    {
      setError( TridentApiConstants.ER_INVALID_DUTY_AMOUNT );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_ORDER_DATE) && getLevelIIIFieldAsDate(TridentApiConstants.FN_ORDER_DATE) == null ) 
    {
      setError( TridentApiConstants.ER_INVALID_ORDER_DATE );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_VAT_AMOUNT) && getLevelIIIFieldAsDouble(TridentApiConstants.FN_VAT_AMOUNT,-1.0) < 0.0 )
    {
      setError( TridentApiConstants.ER_INVALID_VAT_AMOUNT );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_VAT_AMOUNT) && (vatAmt > TridentApiConstants.MAX_LIII_AMOUNTS || vatAmt < 0.0) )
    {
      setError( TridentApiConstants.ER_INVALID_VAT_AMOUNT );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_VAT_RATE) && (vatRate <= 0.0 || vatRate >= 50.0) )
    {
      setError( TridentApiConstants.ER_INVALID_VAT_RATE );
    }
    else if ( isLevelIIIFieldSet(TridentApiConstants.FN_ALT_TAX_AMOUNT) && (altTax > TridentApiConstants.MAX_LIII_AMOUNTS || altTax < 0.0) )
    {
      setError( TridentApiConstants.ER_INVALID_ALT_TAX_AMOUNT );
    }
    else if ( tranTypeRequiresCardData() && getLevelIIICardType() != null && !getCardType().equals(getLevelIIICardType()) )
    {
      setError( TridentApiConstants.ER_LEVEL_III_CARD_TYPE_MISMATCH );
    }
    else if ( (LineItemData != null && itemCount != LineItemData.size()) ||
              (LineItemData == null && itemCount != 0) )
    {
      setError( TridentApiConstants.ER_INVALID_LINE_ITEM_COUNT );
    }
    return( !hasError() );
  }
  
  public boolean isLevelIIIFieldSet(String fname)
  {
    return( LevelIIIFields.get(fname) != null );
  }
  
  public boolean isLineItemDataValid()
  {
    String                    cardType    = getLevelIIICardType();
    ApiLineItemDetailRecord   item        = null;
    boolean                   retVal      = true;   
    
    if ( LineItemData != null )
    {
      if ( "VS".equals(cardType) )
      {
        item = new ApiVisaLineItemDetailRecord();
      }
      else if ( "MC".equals(cardType) )
      {
        item = new ApiMCLineItemDetailRecord();
      }
      else if ( "AM".equals(cardType) )
      {
        item = new ApiAMLineItemDetailRecord();
      }
      
      if ( item != null )
      {
        for( int i = 0; i < LineItemData.size(); ++i )
        {
          try
          {
            item.setRawLineItemData( (String)LineItemData.get(i));
          }
          catch( Exception e )
          {
            retVal = false;
            break;
          }
        }
      }
    }
    return( retVal );
  }
  
  public boolean isMultiCaptureAllowed()
  {
    return( Profile != null && Profile.isMultiCaptureAllowed() );
  }
  
  public boolean isMultiCurrency()
  {
    return( Profile != null && Profile.isMultiCurrency() );
  }

  public boolean isNonUSCurrency()
  {
    return( !getCurrencyCode().equals(TridentApiConstants.FV_CURRENCY_CODE_USD) );
  }
  
  public boolean isOriginalTranIdRequired()
  {
    return( OrigTranIdRequired );
  }
  
  public boolean isPartialAuthRequest()
  {
    return( ResponseCtlFlags.get(TridentApiConstants.FN_PARTIAL_AUTH) != null );
  }
  
  public boolean isPartialRefund()
  {
    return( TranType.equals(TT_REFUND) && (getTranAmount() != 0.0) );
  }
  
  public boolean isFraudScanDisabled()
  {
    return( FraudScan == false );
  }
  
  public boolean isSettleAmountValidationEnabled()
  {
    return( Profile == null || Profile.isSettleAmountValidationEnabled() );
  }
  
  public boolean isSettled()
  {
    return( Settled );
  }
  
  public boolean isShipToZipValid() {
	  if(getCardType().equals("AM")) {
		  if(getShipToZip().length() > TridentApiConstants.FLM_SHIP_TO_ZIP_AMEX)
			  return false;
	  }
	  else if(getShipToZip().length() > TridentApiConstants.FLM_SHIP_TO_ZIP)
		  return false;
	  return true;
  }
  
  public boolean isSwiped()
  {
    return( CardSwipe != null );
  }
  
  public boolean isTaxValid()
  {
    return( TaxAmount >= 0.0 && TaxAmount <= 999999.99 );
  }
  
  public boolean isTipsAllowed()
  {
    return( Profile.isTipsAllowed() );
  }
  
  public boolean isTrack1Swipe()
  { 
    String      cardSwipe   = getCardSwipe();
    boolean     retVal      = false;
    
    if ( cardSwipe != null && cardSwipe.length() > 0 )
    {
      retVal = (cardSwipe.charAt(0) == 'B');
    }
    return( retVal );
  }
  
  public boolean isValid()
  {
    String    cardType          = TridentTools.decodeVisakCardType(getCardNumber());
    boolean   cardDataRequired  = tranTypeRequiresCardData();
    boolean   fxRateRequired    = tranTypeRequiresFxRateId();
    boolean   isValid           = !TranType.equals(TT_INVALID);
    
    // reset the current error code to ER_NONE
    setErrorCode(TridentApiConstants.ER_NONE);
    
    // requires a tid and valid profile
    if ( ProfileId == null || Profile == null )     
    {
      setErrorCode(TridentApiConstants.ER_INVALID_PROFILE);
    }
    else if ( !TridentApiConstants.ER_NONE.equals(getInputErrorCode()) )
    {
      setErrorCode( getInputErrorCode() );
    }
    else if ( TranType.equals(TT_STORE_CARD) && getCardId() != null )
    {
      setErrorCode(TridentApiConstants.ER_CARD_ID_NOT_ALLOWED);    
    }              
    else if ( hasCardDataLoadError() )
    {
      setErrorCode( getCardDataLoadError() ); 
    }
    else if ( cardDataRequired && !hasCardData() )
    {
      // sales, credits, pre-auths all require transaction data
      setErrorCode(TridentApiConstants.ER_MISSING_CARD_DATA);
    }
    else if ( cardDataRequired && !isCardDataValid() )
    {
      setErrorCode(TridentApiConstants.ER_INVALID_CARD_DATA);
    }
    else if ( cardDataRequired && !Profile.acceptsCardType(cardType) )
    {
      setErrorCode(TridentApiConstants.ER_CT_NOT_ACCEPTED);
    }
    else if ( cardDataRequired && !Profile.acceptsCurrency(getCurrencyCode()) )
    {
      setErrorCode(TridentApiConstants.ER_CURRENCY_NOT_ACCEPTED);
    }
    else if ( !validCurrencyFormat() )
    {
      setErrorCode(TridentApiConstants.ER_INVALID_CURRENCY_FORMAT);
    }
    else if ( tranTypeRequiresCardId() && isBlank(getCardId()) )
    {
      setErrorCode(TridentApiConstants.ER_CARD_ID_REQUIRED);
    }
    else if ( isCredit() && !Profile.creditsAllowed() )
    {
      // credits need to be enabled explicitly
      setErrorCode(TridentApiConstants.ER_CREDITS_NOT_ALLOWED);
    }
    else if ( tranTypeRequiresAmount() && TranAmount <= 0.0 )
    {
      // sales, credits, pre-auths all require a positive amount
      setErrorCode(TridentApiConstants.ER_INVALID_AMOUNT);
      setErrorDesc("Transaction amount must be greater than zero");
    }
    else if ( TranAmount < 0.0 )
    {
      // prevents settle requests from being submitted with a negative amount
      setErrorCode(TridentApiConstants.ER_INVALID_AMOUNT);
      setErrorDesc("Transaction amount cannot be negative");
    }
    else if ( tranTypeRequiresAmount() && TranAmount > TridentApiConstants.MAX_TRAN_AMOUNT )
    {
      setErrorCode(TridentApiConstants.ER_INVALID_AMOUNT);
      setErrorDesc("Transaction amount must be less than " + String.valueOf(TridentApiConstants.MAX_TRAN_AMOUNT));
    }
    else if ( TranType.equals(TT_CARD_VERIFY) && TranAmount != 0.0 )
    {
      // card validation requests can only submit zero
      setErrorCode(TridentApiConstants.ER_INVALID_AMOUNT);
      setErrorDesc("Card verify amount must be zero");
    }
    else if ( isTaxValid() == false )
    {
      setErrorCode(TridentApiConstants.ER_INVALID_TAX_AMOUNT);
    }
    else if ( tranTypeRequiresPurchaseId() && isBlank(getPurchaseId()) )
    {
      setError(TridentApiConstants.ER_PURCHASE_ID_REQUIRED);
    }
    else if ( getPurchaseId().length() > getPurchaseIdLengthMax() )
    {
      setError(TridentApiConstants.ER_PURCHASE_ID_LENGTH);
    }
    else if ( getReferenceNumber().length() > TridentApiConstants.FLM_REFERENCE_NUMBER )
    {
      setError(TridentApiConstants.ER_REFERENCE_NUMBER_LENGTH);
    }
    else if ( getClientReferenceNumber().length() > TridentApiConstants.FLM_CLIENT_REFERENCE_NUMBER )
    {
      setError(TridentApiConstants.ER_CLIENT_REFERENCE_NUMBER_LENGTH);
    }
    else if ( getAvsZip().length() > TridentApiConstants.FLM_AVS_ZIP )
    {
      setError(TridentApiConstants.ER_AVS_ZIP_LENGTH);
    }
    else if ( getDbaName().length() > TridentApiConstants.FLM_MERCHANT_NAME )
    {
      setError(TridentApiConstants.ER_MERCHANT_NAME_LENGTH);
    }
    else if ( getDbaCity().length() > TridentApiConstants.FLM_MERCHANT_CITY )
    {
      setError(TridentApiConstants.ER_MERCHANT_CITY_LENGTH);
    }
    else if ( getDbaState().length() > TridentApiConstants.FLM_MERCHANT_STATE )
    {
      setError(TridentApiConstants.ER_MERCHANT_STATE_LENGTH);
    }
    else if ( getDbaZip().length() > TridentApiConstants.FLM_MERCHANT_ZIP )
    {
      setError(TridentApiConstants.ER_MERCHANT_ZIP_LENGTH);
    }
    else if ( getSicCode().length() > TridentApiConstants.FLM_SIC_CODE )
    {
      setError(TridentApiConstants.ER_SIC_CODE_LENGTH);
    }
    else if ( getCustServicePhone().length() > TridentApiConstants.FLM_MERCHANT_PHONE )
    {
      setError(TridentApiConstants.ER_MERCHANT_PHONE_LENGTH);
    }
    else if ( !isBlank(getDMContactInfo()) && getDMContactInfo().length() > TridentApiConstants.FLM_DM_CONTACT_INFO )
    {
      setError(TridentApiConstants.ER_DM_CONTACT_INFO_LENGTH);
    }
    else if ( getMotoEcommIndicator().length() > TridentApiConstants.FLM_MOTO_ECOMM_IND )
    {
      setError(TridentApiConstants.ER_MOTO_ECOMM_IND_LENGTH);
    }
    else if ( !isValidNumber( getReferenceNumber() ) )
    {
      setError(TridentApiConstants.ER_REFERENCE_NUMBER_VALUE);
    }
    else if ( isOffline() && isBlank(getAuthCode()) )
    {
      setErrorCode(TridentApiConstants.ER_AUTH_CODE_REQUIRED);
    }
    else if ( TranType.equals(TT_RESPONSE_INQUIRY) && !hasRetryId() )
    {
      setError(TridentApiConstants.ER_INVALID_RETRY_ID);
    }
    else if ( hasRetryId() && getRetryId().length() > TridentApiConstants.FLM_RETRY_ID )
    {
      setError(TridentApiConstants.ER_RETRY_ID_LENGTH);
    }
    else if ( tranTypeRequiresFxAmount() && getFxAmount() == 0.0 )
    {
      setError( TridentApiConstants.ER_FX_AMOUNT_REQUIRED );
    }
    else if ( !fxRateRequired && getFxRateId() != 0 )
    {
      setError(TridentApiConstants.ER_FX_NOT_SUPPORTED);
    }
    else if ( isFxTranType() && !isFxEnabled() )
    {
      setError(TridentApiConstants.ER_FX_NOT_SUPPORTED);
    }
    else if ( fxRateRequired && getFxRateId() == 0 )
    {
      setError(TridentApiConstants.ER_RATE_ID_REQUIRED);
    }
    else if ( fxRateRequired && isFxRateIdExpired() )
    {
      setError(TridentApiConstants.ER_RATE_TABLE_EXPIRED);
    }
    else if ( fxRateRequired && !isFxCurrencyValid() )
    {
      setError(TridentApiConstants.ER_FX_CURRENCY_CODE_MISMATCH);
    }
    else if ( fxRateRequired && !isFxRateIdValid() )
    {
      setError(TridentApiConstants.ER_INVALID_RATE_ID);
    }
    else if ( fxRateRequired && !isFxAmountValid() )
    {
      setError( TridentApiConstants.ER_RATE_CALC_INVALID );
    }
    else if( hasShipToAddress() && getShipToAddress().length() > TridentApiConstants.FLM_SHIP_TO_ADDRESS )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_TO_ADDRESS );
    }
    else if( hasShipToCountryCode() && getShipToCountryCode().length() > TridentApiConstants.FLM_DEST_COUNTRY_CODE )
    {
      setError( TridentApiConstants.ER_INVALID_DEST_COUNTRY_CODE );
    }
    else if( hasShipToFirstName() && getShipToFirstName().length() > TridentApiConstants.FLM_SHIP_TO_FIRST_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_TO_FIRST_NAME );
    }
    else if( hasShipToLastName() && getShipToLastName().length() > TridentApiConstants.FLM_SHIP_TO_LAST_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_TO_LAST_NAME );
    }
    else if( hasShipToZip() && !isShipToZipValid() )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_TO_ZIP );
    } 
    else if( hasShipToPhone() && getShipToPhone().length() > TridentApiConstants.FLM_SHIP_TO_PHONE )
    {
      setError( TridentApiConstants.ER_INVALID_SHIP_TO_PHONE );
    }
    else if( hasCardholderFirstName() && getCardholderFirstName().length() > TridentApiConstants.FLM_CARDHOLDER_FIRST_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_CARDHOLDER_FIRST_NAME );
    }
    else if( hasCardholderLastName() && getCardholderLastName().length() > TridentApiConstants.FLM_CARDHOLDER_LAST_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_CARDHOLDER_LAST_NAME );
    }
    else if( hasCardholderEmail() && getCardholderEmail().length() > TridentApiConstants.FLM_CARDHOLDER_EMAIL )
    {
      setError( TridentApiConstants.ER_INVALID_CARDHOLDER_EMAIL );
    }
    else if( hasCardholderPhone() && getCardholderPhone().length() > TridentApiConstants.FLM_CARDHOLDER_PHONE )
    {
      setError( TridentApiConstants.ER_INVALID_CARDHOLDER_PHONE );
    }
    else if( hasCardholderRefNum() && getCardholderRefNum().length() > TridentApiConstants.FLM_CARDHOLDER_REF_NUM )
    {
      setError( TridentApiConstants.ER_INVALID_CARDHOLDER_REF_NUM );
    }
    else if( hasCustomerHostName() && getCustomerHostName().length() > TridentApiConstants.FLM_CUSTOMER_HOST_NAME )
    {
      setError( TridentApiConstants.ER_INVALID_CUSTOMER_HOST_NAME );
    }
    else if( hasCustomerBrowserType() && getCustomerBrowserType().length() > TridentApiConstants.FLM_CUSTOMER_BROWSER_TYPE )
    {
      setError( TridentApiConstants.ER_INVALID_CUSTOMER_BROWSER_TYPE );
    }
    else if( hasCustomerANI() && getCustomerANI().length() > TridentApiConstants.FLM_CUSTOMER_ANI )
    {
      setError( TridentApiConstants.ER_INVALID_CUSTOMER_ANI );
    }
    else if( hasCustomerANI_II() && getCustomerANI_II().length() > TridentApiConstants.FLM_CUSTOMER_ANI_II )
    {
      setError( TridentApiConstants.ER_INVALID_CUSTOMER_ANI_II );
    }
    else if( hasProductSku() && getProductSku().length() > TridentApiConstants.FLM_PRODUCT_SKU )
    {
      setError( TridentApiConstants.ER_INVALID_PRODUCT_SKU );
    }
    else if( hasShippingMethod() && getShippingMethod().length() > TridentApiConstants.FLM_SHIPPING_METHOD )
    {
      setError( TridentApiConstants.ER_INVALID_SHIPPING_METHOD );
    }
    else if ( hasDebitData() && !tranTypeSupportsDebit() )
    {
      setError( TridentApiConstants.ER_DEBIT_TRAN_TYPE_NOT_ALLOWED );
    }
    else if ( hasDebitData() && !isSwiped() )
    {
      setError( TridentApiConstants.ER_DEBIT_REQUIRES_SWIPE );
    }
    else if ( hasDebitData() && isNonUSCurrency() )
    {
      setError( TridentApiConstants.ER_DEBIT_NON_USD_NOT_SUPPORTED );
    }
    else if ( hasLevelIIIData() && !tranTypeSupportsLevelIII() )
    {
      setError( TridentApiConstants.ER_INVALID_LEVEL_III_TRAN_TYPE );
    }
    else if ( !isLineItemDataValid() )
    {
      setError( TridentApiConstants.ER_INVALID_LINE_ITEM_DATA );
    }
    else if ( hasLevelIIIData() && !isLevelIIIDataValid() )
    {
      // error code should be set by isLevelIIIDataValid()
      // set isValid in case the method failed to set the error code
      isValid = false;    
    }
    // Recurring
    else if(getRecurringPaymentCount() > TridentApiConstants.MAX_RECURRING_COUNT) {
    	setError(TridentApiConstants.ER_INVALID_RECURRING_COUNT);
    }
    else if(getRecurringPaymentNumber() > TridentApiConstants.MAX_RECURRING_NUMBER) {
    	setError(TridentApiConstants.ER_INVALID_RECURRING_NUM);
    }
    else if ( isCashAdvance() && !isCashAdvanceSic() )
    {
      setError( TridentApiConstants.ER_CASH_ADVANCE_NOT_SUPPORTED );
    }
    else if ( isCashAdvance() && isNonUSCurrency() )
    {
      setError( TridentApiConstants.ER_CASH_ADVANCE_NOT_SUPPORTED );
    }
    else if ( !isDeveloperIdValid() )
    {
      setError( TridentApiConstants.ER_INVALID_DEVELOPER_ID );
    }
    // Check for problems with travel data
    else if(hasTravelData() && TravelStayDuration < 0) {
      setError(TridentApiConstants.ER_TRAVEL_INVALID_CHECKOUT);
    }
    // Validate Industry Code
    else if(!TridentApiConstants.IndustryCodes.containsKey(getIndustryCode())) {
      setError(TridentApiConstants.ER_INVALID_INDUSTRY_CODE);
    }
    // For Amex - Validate that the amex currency set in profile is enforced when DCC is enabled (PMG-168)
    else if(getProfile().getIntlProcessor() == mesConstants.INTL_PID_DCC && this.isCardTypeAmex() && !getProfile().getAmexCurrencyCode().equals(getCurrencyCode())) {
      setError(TridentApiConstants.ER_CURRENCY_NOT_ACCEPTED);
    }
    // For Discover - Non USD is not supported (PMG-182)
    else if(this.isCardTypeDiscover() && !getCurrencyCode().equals("840")) {
      setError(TridentApiConstants.ER_CURRENCY_NOT_ACCEPTED);
    }

    // if the request is not valid but a specific error 
    // has not been set, then set the generic error code
    if ( isValid == false && hasError() == false )
    {
      setErrorCode(TridentApiConstants.ER_INVALID_REQUEST);
    }
    
    return( !hasError() );
  }
  
  public boolean isUSD()
  {
    return( getCurrencyCode().equals(TridentApiConstants.FV_CURRENCY_CODE_USD) );
  }
  
  private boolean isValidNumber( String value ) 
  {
    boolean     retVal    = true;
    
    // null and empty strings are considered 
    // valid numbers for the purpose of this
    // incoming transaction validity test
    if ( value != null && !value.equals("") )  
    {
      String    numbers = "0123456789";
      
      // test each character to see that it is numeric
      for( int i = 0; i < value.length(); ++i )
      {
        String ch = value.substring(i,i+1);
        if ( numbers.indexOf(ch) < 0 )
        {
          retVal = false;
          break;
        }
      }
    }
    return( retVal );
  }
  
  protected void loadCardData( )
  {
    // if the card id is non-null, attempt to load
    // the card data from Oracle.
    if ( CardId != null && !CardId.trim().equals("") )
    {
      CardDataLoadError = null; // reset the loader error code
      CardDataLoadTask task = new CardDataLoadTask(this);
      if ( task.doTask() == false )
      {
        // store to return to the client
        CardDataLoadError = task.getErrorCode();
      }
    }
  }
  
  public void loadData( )
  {
    loadCardData();  
  }
  
  protected void setCardId( String cardId )
  {
    CardId = cardId;  // set the class variable
  }
  
  public void setCvv2( String cvv2 )
  {
    Cvv2 = cvv2;
  }

  public boolean requestAuthorization()
    throws IOException
  {
    return( requestAuthorization( AuthorizationHost, AuthorizationPort ) );
  }
  
  public boolean requestAuthorization( String host, int port )
    throws IOException
  {
    long                  beginTS       = System.currentTimeMillis();
    boolean               retVal        = false;
    boolean               sendToIssuer  = true;
        
    if ( sendToIssuer )
    {
      if ( useAdyen() )
      {
        retVal = requestAuthorizationAdyen();
      }
      else if ( usePayVision() )
      {
        retVal = requestAuthorizationPayVision();
      }
      else if( useGts() )
      {
    	  retVal = requestAuthorizationGts();
      }
      else if( useItps() ) {
    	  retVal = requestAuthorizationItps();
      }
      else if( useBraspag() ) {
    	  retVal = requestAuthorizationBraspag();
      }
      else if ( isUSD() || isMultiCurrency() )
      {
        retVal = requestAuthorizationTrident(host, port);
      }
    }
        
    SwitchDuration = (System.currentTimeMillis() - beginTS);
    return( retVal );
  }
  
  protected boolean requestAuthorizationAdyen( )
  {
    PaymentResult               result              = null;
    boolean                     retVal              = false;
    PaymentPortType             service             = null;
    
    try
    {
      // get a reference to the basic Adyen web service
      PaymentLocator sl = new PaymentLocator(); 
      sl.setPaymentHttpPortEndpointAddress(getAdyenEndpoint());
      service = sl.getPaymentHttpPort(); 
      
      // setup the Adyen HTTP authentication values
      ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.USERNAME_PROPERTY, getAdyenCompanyId() );
      ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.PASSWORD_PROPERTY, getAdyenPassword()  );
      
      // build a card object for use in request
			Card card = new Card();
			card.setCvc(getCvv2());
			card.setExpiryMonth(String.valueOf(getExpDateMonth()));
			card.setExpiryYear(String.valueOf(getExpDateYear()));
			card.setNumber(getCardNumberFull());
      card.setHolderName("NOT/AVAILABLE");
      
      String countryCode = currencyCodeToCountryCode(getCurrencyCodeAlpha());
      if ( countryCode != null && hasAvsData() )
      {
        card.setBillingAddress(new Address( "NOT PROVIDED", // City
                                            countryCode,    // Country
                                            "",             // HouseNumberOrName
                                            getAvsZip(),    // PostalCode
                                            "XX",           // StateOrProvince
                                            getAvsStreet()  // Street
                                           ));
      }
      
      // create a payment request object
			PaymentRequest  request     = new PaymentRequest();
			request.setAmount( new Amount(getCurrencyCodeAlpha(),getTranAmountAsLong()) );
			request.setCard( card );
			request.setMerchantAccount( getAdyenMerchantId() );
			request.setReference( getTridentTranId() );
      
      if( is3dSecureRequest() )
      {
        String              ar          = null;
        String              cardType    = getCardType();
        String              cavv        = null;
        String              dr          = null;
        String              eci         = null;
        ThreeDSecureData    threeDData  = new ThreeDSecureData();
        String              xid         = null;
        
        if ( "MC".equals(cardType) )
        {
          cavv  = Base64.decodeAsHexString(getUcafAuthData());
          eci   = StringUtilities.rightJustify(getUcafCollectionInd(),2,'0');
          xid   = "";
        }
        else if ("VS".equals(cardType) )
        {
          cavv  = getVisaCavv();
          eci   = StringUtilities.rightJustify(getMotoEcommIndicator(),2,'0');
          xid   = getVisaXid();
        }
        
        // mc will be 02/01, visa will be 05/06
             if ( "02,05".indexOf(eci) >= 0 ) { ar = "Y"; dr = "Y"; } // full
        else if ( "01,06".indexOf(eci) >= 0 ) { ar = "A"; dr = "Y"; } // merchant
        else                                  { ar = "U"; dr = "Y"; } // none
        
        threeDData.setAuthenticationResponse(ar);
        threeDData.setDirectoryResponse(dr);
        threeDData.setCavv(ByteUtil.parseHexString(cavv));
        threeDData.setCavvAlgorithm("0");
        threeDData.setEci(eci);
        threeDData.setXid(ByteUtil.parseHexString(xid));
        
        AnyType2AnyTypeMapEntry[] anyTypeMapEntry = 
          new AnyType2AnyTypeMapEntry[]
          {
            new AnyType2AnyTypeMapEntry("acquirerCode"    ,getAdyen3dsAcquirerId()),
            new AnyType2AnyTypeMapEntry("authorisationMid",getAdyen3dsMerchantId()),
          };
        request.setMpiData(threeDData);
        request.setAdditionalData( new AnyType2AnyTypeMap(anyTypeMapEntry) );
      }
      
      try
      {
        result = service.authorise( request );
      
        setAdyenResult(result);
        setExternalTranId(result.getPspReference());
      
        if( "Authorised".equals(result.getResultCode()) )
        {
          if ( getTranType().equals(TridentApiTranBase.TT_DEBIT) )
          {
            ModificationRequest modRequest = new ModificationRequest();
            modRequest.setMerchantAccount( getAdyenMerchantId() );
            modRequest.setOriginalReference( getAdyenTranId() );
            modRequest.setModificationAmount( request.getAmount() );
            ModificationResult modResult = service.capture(modRequest);
            
            // if the capture failed, cancel the authorization
            if ( (retVal = "[capture-received]".equals(modResult.getResponse())) == false )
            {
              modRequest.setModificationAmount(null);
              service.cancel(modRequest);
              setErrorCode( TridentApiConstants.ER_ADYEN_CAPTURE_FAILED );
            }
          }
          else
          {
            retVal = true; 
          }
        }
        else
        {
          setErrorCode( "Error".equals(result.getResultCode()) ? TridentApiConstants.ER_ADYEN_ERROR : TridentApiConstants.ER_ADYEN_REFUSED );
          setErrorDesc( processString(result.getResultCode()) + " - " + 
                        processString(result.getRefusalReason()) );
        }
      }
      catch( org.apache.axis.AxisFault fault )   // adyen likes to return SOAP faults
      {
        setErrorCode( TridentApiConstants.ER_ADYEN_ERROR );
        setErrorDesc( fault.getFaultString() );
      }
      finally
      {
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + ".requestAuthorizationAdyen()", e.toString() );
      if ( !hasError() )
      {
        setError(TridentApiConstants.ER_ADYEN_REQUEST_FAILED);
      }
    }      
    return( retVal );
  }
  
  /**
   * Requests an authorization (Pre-auth or Sale) from the Braspag gateway.
   * @return true on successful authorization
   */
  protected boolean requestAuthorizationBraspag()
  {
    AuthorizeTransactionResponse result = null;
    boolean retVal = false;
    PagadorTransactionSoap service = null;
    StringBuffer b = new StringBuffer();

    try {
      PagadorTransactionLocator sl = new PagadorTransactionLocator();

      sl.setPagadorTransactionSoapEndpointAddress(getBraspagEndpoint());
      service = sl.getPagadorTransactionSoap();

      AuthorizeTransactionRequest request = new AuthorizeTransactionRequest();
      OrderDataRequest order = new OrderDataRequest(getBraspagMerchantId(), getApiTranId(), null);

      CustomerDataRequest customer = new CustomerDataRequest();
      customer.setCustomerEmail(getCardholderEmail());
      customer.setCustomerName(getCardholderName());

      AddressDataRequest billing = new AddressDataRequest();
      billing.setState(getCardholderState());
      billing.setStreet(getAvsStreet());
      billing.setZipCode(getAvsZip());
      customer.setCustomerAddressData(billing);

      AddressDataRequest shipping = new AddressDataRequest();
      shipping.setCountry(getShipToCountryCode());
      shipping.setStreet(getShipToAddress());
      shipping.setZipCode(getShipToZip());
      customer.setDeliveryAddressData(shipping);

      CreditCardDataRequest cardData = new CreditCardDataRequest();
      int decDigits = 2;
      try {
        decDigits = Currency.getInstance(getCurrencyCodeAlpha()).getDefaultFractionDigits();
      } catch(IllegalArgumentException e) {
        throw new RuntimeException("Unsupported currency: "+getCurrencyCodeAlpha());
      }
      cardData.setAmount(MesMath.doubleToLong(getTranAmount(), decDigits));
      cardData.setCardExpirationDate(getExpDate("MM/yyyy"));
      cardData.setCardNumber(getCardNumberFull());
      cardData.setCardSecurityCode(getCvv2());

      short paymentMethod = getBraspagPaymentMethod();
      String countryCode = getBraspagCountryCode();

      cardData.setCountry(countryCode);
      cardData.setCurrency(getCurrencyCodeAlpha()); // Alpha only
      cardData.setPaymentMethod(paymentMethod);
      short paymentCount = 1;
      cardData.setNumberOfPayments(paymentCount);

      UnsignedByte braspagType = null;
      if(getTranType().equals(TridentApiTranBase.TT_PRE_AUTH))
        braspagType = new UnsignedByte(1);
      else
        braspagType = new UnsignedByte(2);
      cardData.setTransactionType(braspagType); // 0 invalid(?), 1 pre-auth, 2 auto-capture, 3 preauth with authentication, 4 auto-capture with authentication

      PaymentDataRequest[] pdrArray = new PaymentDataRequest[1]; // We are only sending one transaction at a time
      pdrArray[0] = cardData;

      ArrayOfPaymentDataRequest paymentData = new ArrayOfPaymentDataRequest(pdrArray);
      request.setPaymentDataCollection(paymentData);
      request.setOrderData(order);
      request.setCustomerData(customer);
      request.setVersion("1.0"); // Required
      request.setRequestId(this.addGuidDashes(getTridentTranId()));

      result = service.authorizeTransaction(request);

      if(!result.isSuccess()) {
        // These are general configuration errors. Declines are separate.
        ErrorReportDataResponse[] resultArray = result.getErrorReportDataCollection().getErrorReportDataResponse();
        setErrorCode(TridentApiConstants.ER_BRASPAG_ERROR);
        setErrorDesc(resultArray[0].getErrorMessage() + " ("+resultArray[0].getErrorCode()+")"); // Only return the top error, one at a time
      }
      else {
        CreditCardDataResponse ccdr = (CreditCardDataResponse) result.getPaymentDataCollection().getPaymentDataResponse(0);
        if(ccdr != null) {
          log.debug("BP getBraspagTransactionId: "+ccdr.getBraspagTransactionId());
          log.debug("BP getReturnMessage: "+ccdr.getReturnMessage());
          log.debug("BP getReturnCode: "+ccdr.getReturnCode());
          log.debug("BP getAuthorizationCode: "+ccdr.getAuthorizationCode());
          log.debug("BP getStatus: "+ccdr.getStatus());
          
          setExternalTranId(ccdr.getBraspagTransactionId().replaceAll("-", "")); // Strip dashes, externalTranId is only 32 char in DB.
          setBraspagResult(ccdr);
          
          // Request was successful, though it could still be declined.
          switch(ccdr.getStatus().intValue()) {
          case 0:   // Capturado (issued for approved sales)
          case 1:   // Autorizada (issued for approved auths)
            retVal = true;
            setErrorCode(TridentApiConstants.ER_NONE);
            setErrorDesc("Transaction Authorized"); // Transa??o autorizada
            break;
          case 2:   // N�o Autorizada (declined by issuer)
          case 3:   // Erro Desqualificante (Disqualifying error?) 
          case 4:   // 
            setErrorCode(TridentApiConstants.ER_BRASPAG_ERROR);
            setErrorDesc("Transaction Declined ("+ccdr.getReturnCode()+")"); // Autoriza??o negada
            break;
          }
        }
        else
          throw new Exception("Braspag returned success but CreditCardDataResponse was null.");
      }
    } catch(RuntimeException e) {
      setError(TridentApiConstants.ER_BRASPAG_ERROR);
      if(e.getMessage() != null)
        setErrorDesc(e.getMessage());
    } catch(AxisFault f) {
      SyncLog.LogEntry(this.getClass().getName() + ".requestAuthorizationBraspag()", f.getMessage());
      setErrorCode(TridentApiConstants.ER_BRASPAG_ERROR);
      f.printStackTrace();
    } catch( Exception e ) {
      SyncLog.LogEntry(this.getClass().getName() + ".requestAuthorizationBraspag()", e.getMessage());
      setError(TridentApiConstants.ER_BRASPAG_ERROR);
      e.printStackTrace();
    }
    return(retVal);
  }

  protected boolean requestAuthorizationGts( )
  {
    TransactionResponsePacket   result              = null;
    boolean                     retVal              = false;
    IATPayTxWS             		service             = null;
    StringBuffer 				b 					= new StringBuffer();
    
    try {
    	// get a reference to the basic GTS web service
    	ATPayTxWSLocator sl = new ATPayTxWSLocator();
    	
    	sl.setBasicHttpBinding_IATPayTxWSEndpointAddress("https://transactions-test.rtpay.com/TxWS/ATPayTxWS.svc");
    	service = sl.getBasicHttpBinding_IATPayTxWS(); 
    	
    	((Stub) service)._setProperty(Call.USERNAME_PROPERTY, MesDefaults.getString(MesDefaults.TRIDENT_API_TRANSACTION_GTS_USERNAME));
    	((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, MesDefaults.getString(MesDefaults.TRIDENT_API_TRANSACTION_GTS_PASSWORD));

    	RegularTransactionPacket  request = new RegularTransactionPacket();
    	
    	request.setAccountID(getGtsAccountId());
    	request.setSubAccountID(getGtsSubAccountId(getCurrencyCode()));
    	
    	request.setCreditCardNumber( getCardNumberFull() );
    	request.setCreditCardType(CreditCardsTypesIDs.Visa);
    	request.setCreditCardCVV2( getCvv2() );
    	
    	b.append(String.valueOf(getExpDateMonth()));
    	String y = String.valueOf(getExpDateYear());
    	if(y.length() > 2) b.append(y.substring(2));
    	request.setCreditCardExpirationDate( b.toString() );
    	
    	request.setCustomerOriginatedTxID( getApiTranId() );
    	request.setAmount( new BigDecimal(getTranAmountFormatted()) );
    	request.setBaseAmount( new BigDecimal(getTranAmountFormatted()) );
    	
    	CurrencyTypesIDs gtsCurrency = getGtsCurrencyCode(getCurrencyCode());
    	if(gtsCurrency != null)
    		request.setCurrencyType( gtsCurrency );
    	
    	if(this.getTranType().equals(TridentApiTranBase.TT_PRE_AUTH))
    		request.setInstructionType( TxInstructionTypesIDs.Authorization );
    	else
    		request.setInstructionType( TxInstructionTypesIDs.Deposit );
    	
    	// 3D Secure
    	if(is3dSecureRequest()) {
    		request.setThreeDSecureFlags( ThreeDLevel.ThreeDPhase3 );
//    		request.setThreeDSecureCAVV(this.getVisaCavv());
//    		request.setThreeDSecureXID(this.getVisaXid());
//    		request.setThreeDSecureECI(this.getMotoEcommIndicator());
    		
    		// TODO: For testing
    		request.setThreeDSecureCAVV("ERERERERERERERERERERERERERE=");
    		request.setThreeDSecureXID("ERERERERERERERERERERERERERE=");
    		request.setThreeDSecureECI("5");
    	}
    	else
    		request.setThreeDSecureFlags( ThreeDLevel.NoThreeD );
    	// Dynamic DBA
    	if(Profile.isDynamicDbaDataAllowed())
    		request.setDynamicDescriptor(getGtsDynamicDescriptor());

    	// Static
    	request.setOperationType( TxOperationType.Debit );
    	request.setRequestType( TxRequestTypesIDs.Regular );
    	request.setProcessingType( TxProcessingTypeIDs.Processing );
    	request.setCustomerOriginatedTechnologyType( CustomerOriginatedTechnologyTypeIDs.Java );
    	request.setPaymentMethodType( TxPaymentMethodsIDs.CreditCard ); // do we need to use DebitCard?
    	request.setPaymentType( TxPaymentTypeIDs.Regular );
    	request.setTransactionCode( TxCodes.Internet );
    	request.setBillingPurpose( BillingPurpose.RegularCharge );
    	request.setScrubbingFlag(false);
    	request.setCreditCardNameOnCard( "NOT/AVAILABLE" );
    	request.setEndUserBillingAddressSameAsShippingAddress(true);
    	request.setCustomerDateTime( Calendar.getInstance() );  // now
    	request.setRecurringBillingFlag(false);
    	request.setAccessManagementFlag(false);
    	request.setSkinID(9);
    	request.setServicePurchasedType( TxServicePurchasedTypesIDs.InternetTransaction );
    	request.setUserLevelID(1);
    	request.setInitialPreAuthAmount( new BigDecimal(0.0) );
    	request.setServiceExpiryMethod(RecurringServiceExpiryMethod.None);
    	request.setRecurringAmount( new BigDecimal(0.0) );
    	request.setFirstInstallmentAmount( new BigDecimal(0.0) );
    	request.setServicePurchasedCategory( TxServicePurchasedCategorysIDs.None );
    	request.setServicePurchasedSubCategory( TxServicePurchasedSubCategorysIDs.None );
    	request.setTxRecurringBillingID(0L);
    	request.setRecurringInstallmentIntervalMethod( TxRecurringInstallmentIntervalMethod.None );
    	request.setFirstInstallmentInterval(0);
    	request.setCurrencyConversionFee( new BigDecimal(0.0) );
    	request.setBackOfficeFileId(0);
    	request.setAccessManagementType( TxAccessManagementType.None );
    	// Additional fields
    	//EndUserIPAddress
    	//ShippingMethodType
    	//NumberOfTries
    	//TerminalID
    	
    	System.out.println("*****************************************\n**************** REQUEST ****************\n*****************************************");
//    	String requestStr = BenUtil.Obj2String(request);
//    	System.out.println(requestStr);

    	result = service.regularTransaction( request );

    	System.out.println("****************************************\n**************** RESULT ****************\n****************************************");
//    	String respStr = BenUtil.Obj2String(result);
//    	System.out.println(respStr);
    	//BenUtil.SetWindowsClipboard(respStr);

    	// Check getSuccessFlag
    	if(!result.getSuccessFlag().booleanValue()) {
    		// ...
    		setErrorCode( TridentApiConstants.ER_GTS_ERROR );
    		setErrorDesc( result.getErrorInfo() +  " (" + result.getErrorID() + ")" );
    	}
    	else {
    		retVal = true;
    		setGtsResult(result);
            setExternalTranId(Long.toString(result.getTransactionID().longValue()));
    		setErrorCode( TridentApiConstants.ER_NONE );
    		setErrorDesc( result.getErrorInfo() );
    	}
    } catch( AxisFault f ) {
		setErrorCode( TridentApiConstants.ER_GTS_ERROR );
		setErrorDesc( f.getFaultString() );
	} catch( Exception e ) {
    	SyncLog.LogEntry( this.getClass().getName() + ".requestAuthorizationGts()", e.toString() );
    	setError(TridentApiConstants.ER_GTS_REQUEST_FAILED);
	}
    return( retVal );
  }

  protected boolean requestAuthorizationItps() {
	  String errorCode = TridentApiConstants.ER_INTERNAL_ERROR;
	  String errorDesc = null;
	  boolean success = false;
	  StringBuffer request = new StringBuffer();
	  
	  HashMap hm = new HashMap();
	  hm.put("merchantId", "merchantId");
	  hm.put("currencyISO", getCurrencyCodeAlpha());
	  hm.put("pending", "0");
	  hm.put("channel", "ECOM");
	  hm.put("merchantId", "ECOM");
	  hm.put("amount", getTranAmountFormatted());
	  hm.put("firstName", getCardholderFirstName());
	  hm.put("lastName", getCardholderLastName());
	  hm.put("phone", getCardholderPhone());
	  hm.put("merchantTxId", getApiTranId());
	  hm.put("action", "auth");	// Support for Sale?
	  hm.put("firstTimeTransaction", "0");
	  hm.put("acquirerId", "500");
	  hm.put("cardNumber", getCardNumberFull());
	  hm.put("cardType", "400");
	  hm.put("csc", getCvv2Formatted());
	  hm.put("issueNumber", "1");
	  hm.put("searchConsent", "N");
	  hm.put("addressHouseNumber", "123");
	  hm.put("addressStreet", "main");
	  hm.put("addressTown", "Seattle");
	  //hm.put("addressCounty", "merchantId");
	  hm.put("password", "s5Ac8xus");
	  hm.put("langISO", "en");
	  
	  Iterator it = hm.entrySet().iterator();
	  while(it.hasNext()) {
		  Entry pair = (Map.Entry)it.next();
		  request.append(pair.getKey()).append("=").append(pair.getValue());
		  if(it.hasNext()) request.append("&");
	  }
	  
	  Http http = Http.getInstance();
	  http
	  .setHostUrl("https://test.merchante-solutions.com/mes-api/tridentApi") //getItpsEndpoint()
	  .setVerbose(false)
	  .setMethod(Http.METHOD_POST)
	  .setRequest("");//request.toString());

	  try {
		  http.run();
	  } catch (Exception e) {
		  e.printStackTrace();
	  }

	  if(!http.connectOk()) {
		  errorCode = TridentApiConstants.ER_ITPS_REQUEST_FAILED;
		  errorDesc = TridentApiConstants.getErrorDesc(TridentApiConstants.ER_ITPS_REQUEST_FAILED) + " (Connect Failed)";
	  }
	  else if(http.getHttpCode() != 200) {
		  errorCode = TridentApiConstants.ER_ITPS_REQUEST_FAILED;
		  errorDesc = TridentApiConstants.getErrorDesc(TridentApiConstants.ER_ITPS_REQUEST_FAILED) + " (HTTP" + http.getHttpCode() + ")";
	  }
	  else {
		  String result = http.getResponse();
		  // Fake response for now.
		  result = 
"<myriadResponse>"+
	"<amount>10</amount>"+
	"<currencyISO>GBP</currencyISO>"+
	"<statusMsg>PENDING</statusMsg>"+
	"<customerId>123456</customerId>"+
	"<txId>39915966</txId>"+
	"<acquirerTxId>228972325</acquirerTxId>"+
	"<merchantTxId>1346164019664</merchantTxId>"+
	"<action>auth</action>"+
	"<errorCode>0</errorCode>"+
	"<errorMessage/>"+
	"<pan>3217778985991562</pan>"+
	"<promoCode/>"+
	"<productId>6600</productId>"+
	"<merchantFreeText/>"+
	"<langISO>en</langISO>"+
	"<acquirerAmount>20</acquirerAmount>"+
	"<acquirerCurrency>GBP</acquirerCurrency>"+
	"<subXml>"+
		"<authCode>417098</authCode>"+
	"</subXml>"+
	"<SecurityResponseecurityCode>2</SecurityResponseecurityCode>"+
	"<SecurityResponseAddress>2</SecurityResponseAddress>"+
	"<SecurityCodePostCode>2</SecurityCodePostCode>"+
"</myriadResponse>";

		  try {
			  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			  factory.setNamespaceAware(true);
			  DocumentBuilder b = factory.newDocumentBuilder();
			  Document d = b.parse(new ByteArrayInputStream(result.getBytes()));
			  
			  Node root = d.getFirstChild();
			  NodeList list = root.getChildNodes();

			  String itpsErrorCode = getDomNodeText(list, "errorCode");
			  setItpsErrorCode(itpsErrorCode);
			  
			  if(itpsErrorCode == null || !itpsErrorCode.equals("0")) {
				  String itpsResult = getDomNodeText(list, "errorMessage");
				  if(itpsResult != null) errorDesc = itpsResult + " (" + itpsErrorCode + ")"; // ITPS error msg
				  else errorDesc = TridentApiConstants.getErrorDesc(TridentApiConstants.ER_ITPS_REQUEST_FAILED) + " (" + itpsErrorCode + ")"; // Request failed fallback
			  }
			  else {
				  // Good transaction
				  success = true;
				  setItpsFlag(true);
				  String itpsTxId = getDomNodeText(list, "txId");
				  String authCode = getDomNodeText(list, "authCode");
				  String cvvResult = getDomNodeText(list, "SecurityResponseecurityCode");
				  String addressResult = getDomNodeText(list, "SecurityResponseAddress");
				  String zipResult = getDomNodeText(list, "SecurityCodePostCode");
				  
				  setItpsAuthCode(authCode);
				  setItpsCvvResult(cvvResult);
				  setItpsAddressResult(addressResult);
				  setItpsZipResult(zipResult);
				  
				  if(itpsTxId != null)
					  setExternalTranId(itpsTxId);
				  errorCode = TridentApiConstants.ER_NONE;
				  if(authCode != null) // ITPS has no text response on success
					  errorDesc = "Approval " + authCode;
				  else 
					  errorDesc = "Authorized";
			  }
		  } catch (ParserConfigurationException e) {
			  errorCode = TridentApiConstants.ER_ITPS_REQUEST_FAILED;
			  errorDesc = "Malformed ITPS Response";
			  e.printStackTrace();
		  } catch (SAXException e) {
			  errorCode = TridentApiConstants.ER_ITPS_REQUEST_FAILED;
			  errorDesc = "Malformed ITPS Response";
			  e.printStackTrace();
		  } catch (IOException e) {
			  errorCode = TridentApiConstants.ER_ITPS_REQUEST_FAILED;
			  errorDesc = "Malformed ITPS Response";
			  e.printStackTrace();
		  }
	  }
	  setError(errorCode);
	  if(errorDesc != null)
		  setErrorDesc(errorDesc);
	  return success;
  }

  /**
   * Recursively finds the first instance of a node named nodeName within nodeList.
   * @param nodeList
   * @param nodeName
   * @return null if node does not exist.
   */
  public Node getDomNode(NodeList nodeList, String nodeName) {
	  for(int i=0; i<nodeList.getLength(); i++) {
		  Node n = nodeList.item(i);
		  if(n.getNodeName().equals(nodeName))
			  return nodeList.item(i);
		  if(n.hasChildNodes()) {
			  Node r = getDomNode(n.getChildNodes(), nodeName);
			  if(r != null) return r;
		  }
	  }
	  return null;
  }
  
  /**
   * Returns the text value of the first node named nodeName.
   * @param nodeList
   * @param nodeName
   * @return null if node does not exist.
   */
  public String getDomNodeText(NodeList nodeList, String nodeName) {
	  Node n = getDomNode(nodeList, nodeName);
	  if(n != null) {
		  if(n.hasChildNodes())
			  return n.getFirstChild().getNodeValue();
		  else
			  return n.getNodeValue();
	  }
	  return null;
  }

  protected boolean requestAuthorizationPayVision( )
  {
    TransactionResult           result              = null;
    boolean                     retVal              = false;
    BasicOperationsSoap         service             = null;
    ThreeDSecureOperationsSoap  service3d           = null;
    BigDecimal                  tranAmount          = null;
    
    // 3D Secure params
    String                      authenticationValue = null;
    String                      authenticationInd   = null;
    String                      xid                 = null;
    
    try
    {
      tranAmount  = new BigDecimal(getTranAmountFormatted());
      
      // get a reference to the PayVision web service
      //@service = getPayVisionService();   // hook for unit testing
      if( is3dSecureRequest() )
      {
        // get a reference to the PayVision enhanced web service
        ThreeDSecureOperationsLocator sl = new ThreeDSecureOperationsLocator(); 
        sl.setThreeDSecureOperationsSoapEndpointAddress( getPayVisionEndpoint() + TridentApiConstants.PAYVISION_3D_SECURE_PATH );
        service3d = sl.getThreeDSecureOperationsSoap(); 
        
        if ( getCardType().equals("MC") )
        {
          authenticationValue = getUcafAuthData();
          authenticationInd   = (getUcafCollectionInd().equals(TridentApiConstants.FV_UCAF_COLL_IND_PRESENT) ? "11" : "13");
        }
        else    // default is Visa
        {
          if ( getMotoEcommIndicator().equals(TridentApiConstants.FV_MOTO_SECURE_ECOMMERCE) )
          {
            authenticationInd = "10";
            xid               = getVisaXid();
          }
          else    // attempted, do not pass an XID
          {
            authenticationInd = "12";
            xid               = null;
          }
          authenticationValue = getVisaCavv();
        }
      }
      else
      {
        // get a reference to the basic PayVision web service
        BasicOperationsLocator sl = new BasicOperationsLocator(); 
        sl.setBasicOperationsSoapEndpointAddress( getPayVisionEndpoint() + TridentApiConstants.PAYVISION_BASIC_PATH );
        service = sl.getBasicOperationsSoap(); 
      }
      
      if ( getTranType().equals(TridentApiTranBase.TT_DEBIT) )
      {
        if ( service3d == null ) 
        {
          result = service.payment( getPayVisionMemberId(), 
                                    getPayVisionMemberGuid(),
                                    getPayVisionCountryCode(),
                                    tranAmount, 
                                    getPayVisionCurrencyCode(),
                                    getApiTranId(),   // tracking member code
                                    getCardNumberFull(),
                                    getCardholderName(),
                                    getExpDateMonth(),
                                    getExpDateYear(),
                                    getCvv2(),
                                    null,                 // card type
                                    null,                 // issue number
                                    getPayVisionAccountType(),
                                    getPayVisionDynamicDescriptor(),
                                    getAvsStreet(),
                                    getAvsZip()
                                  );
        }
        else    // issue a 3d secure request
        {
          result = service3d.payment( getPayVisionMemberId(), 
                                      getPayVisionMemberGuid(),
                                      getPayVisionCountryCode(),
                                      tranAmount, 
                                      getPayVisionCurrencyCode(),
                                      getApiTranId(),   // tracking member code
                                      getCardNumberFull(),
                                      getCardholderName(),
                                      getExpDateMonth(),
                                      getExpDateYear(),
                                      getCvv2(),
                                      null,                 // card type
                                      null,                 // issue number
                                      getPayVisionAccountType(),
                                      getPayVisionDynamicDescriptor(),
                                      getAvsStreet(),
                                      getAvsZip(),
                                      xid,
                                      authenticationValue,
                                      authenticationInd
                                    );
        }
      }
      else 
      {
        if ( service3d == null ) 
        {
          result = service.authorize( getPayVisionMemberId(), 
                                      getPayVisionMemberGuid(), 
                                      getPayVisionCountryCode(), 
                                      tranAmount, 
                                      getPayVisionCurrencyCode(), 
                                      getApiTranId(), // tracking member code
                                      getCardNumberFull(),
                                      getCardholderName(),
                                      getExpDateMonth(),
                                      getExpDateYear(),
                                      getCvv2(),
                                      null,               // card type
                                      null,               // issue number
                                      getPayVisionAccountType(),
                                      getPayVisionDynamicDescriptor(), 
                                      getAvsStreet(),
                                      getAvsZip()
                                    );
        }
        else
        {
          result = service3d.authorize( getPayVisionMemberId(), 
                                        getPayVisionMemberGuid(), 
                                        getPayVisionCountryCode(), 
                                        tranAmount, 
                                        getPayVisionCurrencyCode(), 
                                        getApiTranId(), // tracking member code
                                        getCardNumberFull(),
                                        getCardholderName(),
                                        getExpDateMonth(),
                                        getExpDateYear(),
                                        getCvv2(),
                                        null,               // card type
                                        null,               // issue number
                                        getPayVisionAccountType(),
                                        getPayVisionDynamicDescriptor(), 
                                        getAvsStreet(),
                                        getAvsZip(),
                                        xid,
                                        authenticationValue,
                                        authenticationInd
                                      );
        }                                    
      }
      setPayVisionResult(result);
      // establish the tran id number and guid.  note
      // we use the payvision transaction guid as our 
      // trident transaction id.
      setTridentTranIdNumber((long)result.getTransactionId());
      setExternalTranId(result.getTransactionGuid().replaceAll("-", ""));
      
      if( result.getResult() == TridentApiConstants.PAYVISION_SUCCESS )
      {
        retVal = true; 
      }
      else
      {
        setErrorCode( "" + result.getResult());
        setErrorDesc( result.getMessage() );
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + ".requestAuthorizationPayVision()", e.toString() );
      if ( !hasError() )
      {
        setError(TridentApiConstants.ER_PV_REQUEST_FAILED);
      }
    }      
    return( retVal );
  }
  
  protected boolean requestAuthorizationTrident( String host, int port )
    throws IOException
  {
    RequestDMessage         authRequest     = null;
    ResponseDMessage        authResponse    = null;
    boolean                 retVal          = false;
    
    // create a request
    authRequest = DMessageFactory.createRequestMessage();
    setAuthRequestFields( authRequest );

    NetConnectAuthClient client = new NetConnectAuthClient();
    client.setUrl(host);    // default to Trident
    
    if ( "AM".equals(getCardType()) && hasAmexEnhancedAuthData() )
    {
      // clear standard avs data
      authRequest.setCardholderIdentificationData("");  
      
      // build the Amex ITD (Internet-Order Industries Data)
      AmexItdData     amexITD           = new AmexItdData();
      
      amexITD.setCustomerEmail    ( getCardholderEmail()     );   // customer email
      amexITD.setCustomerHostname ( getCustomerHostName()    );   // customer host
      amexITD.setHttpBrowserType  ( getCustomerBrowserType() );   // browser type
      amexITD.setShipToCountry    ( getShipToCountryCode()   );   // ship to country
      amexITD.setShippingMethod   ( getShippingMethod()      );   // shipping method
      amexITD.setProductSku       ( getProductSku()          );   // merchant product sku
      amexITD.setCustomerIp       ( getIpAddressFormatted()  );   // customer ip address
      amexITD.setCustomerAni      ( getCustomerANI()         );   // customer ANI
      amexITD.setCustomer2Digits  ( getCustomerANI_II()      );   // customer info id digits
      
      // extract the formatted AVS data
      String        addr    = getAvsStreet();
      String        zip     = getAvsZip();
    
      // truncate values if necessary
      if ( addr.length() > TridentApiConstants.FLM_AVS_ADDRESS )
      {
        addr = addr.substring(0,TridentApiConstants.FLM_AVS_ADDRESS);
      }
      if ( zip.length() > TridentApiConstants.FLM_AVS_ZIP )
      {
        zip = zip.substring(0,TridentApiConstants.FLM_AVS_ZIP);
      }
      
      // add enhanced auth fields
      authRequest.setAmexItdData(amexITD);
      authRequest.setGroupIIIField("029", 1, zip);                      // billing zip
      authRequest.setGroupIIIField("029", 2, addr);                     // billing address
      authRequest.setGroupIIIField("029", 6, getCardholderFirstName()); // cardholder first name
      authRequest.setGroupIIIField("029", 7, getCardholderLastName());  // cardholder last name
      authRequest.setGroupIIIField("029", 8, TridentTools.getFormattedPhone(processString(getCardholderPhone()),false));  // cardholder phone
      authRequest.setGroupIIIField("029", 9, getShipToZip());           // ship to zip
      authRequest.setGroupIIIField("029",10, getShipToAddress());       // ship to address
      authRequest.setGroupIIIField("029",13, getShipToCountryCode());   // ship to country
      authRequest.setGroupIIIField("029",14, getShipToFirstName());     // ship to first name
      authRequest.setGroupIIIField("029",15, getShipToLastName());      // ship to last name
      authRequest.setGroupIIIField("029",16, TridentTools.getFormattedPhone(processString(getShipToPhone()),false));      // ship to phone
    }
    
    // Add Market Specific Data Identifier
    if(hasTravelData()) {
      StringBuffer msdi = new StringBuffer();
      msdi.append(" "); // Prestigious Property Indicator (space - non-participating)
      // Market Specific Data Identifier (Auto Rental / Hotel supported, mapped from Industry Code)
      // May need to add a field for this later if we move beyond just A, and H.
      if(IndustryCode.equals("A") || IndustryCode.equals("H"))
        msdi.append(IndustryCode);
      else
        msdi.append(" ");
      msdi.append(StringUtil.zeroPadLeft(Long.toString(TravelStayDuration), 2));  // Duration of rental / stay, xx, zero padded
      authRequest.setMarketSpecificData(msdi.toString());
      authRequest.setRequestedACI("P"); // this is only ok as long as no PG incremental authorizations
    }
    
    // send the auth request to the the host
    authResponse = client.sendRequest(authRequest);
    
    // store the response
    setResponse(authResponse);  
    
    if ( isApprovedByIssuer() )
    {
      // if this was a partial auth request and received a partial
      // authorization then update the transaction amount to the partial amount
      if ( isPartialAuthRequest() )
      {
        if( authResponse.hasAdditionalAmounts("10") ) 
        {
          AdditionalAmounts addlAmount = authResponse.getAdditionalAmounts("10");
          setTranAmount( Double.parseDouble(addlAmount.getAmount()) * ("D".equals(addlAmount.getSignCode()) ? -0.01 : 0.01) );
        }
      }
      retVal = true;
    }
    else if ( getTranType().equals(TT_CREDIT) && 
              authResponse.getResponseCode().equals(TridentApiConstants.RESP_SERV_NOT_ALLOWED) &&
              !authResponse.getAuthorizationSourceCode().equals(TridentApiConstants.AUTH_SRC_TRIDENT) )
    {              
      // allow credits to process if:
      //  1) the card number is good
      //  2) merchant accepts the card type
      //  3) response came from a source other than trident
      //  4) response was the generic service not allowed
      retVal = true;
    }
    else
    {
      setErrorCode(authResponse.getResponseCode());
      setErrorDesc(authResponse.getAuthorizationResponseText().trim());
    }
    
    if ( retVal == true ) // look for user defined decline reasons
    {
      if ( Profile.isAvsResultDeclined( authResponse.getAVSResultCode() ) )
      {
        setError(TridentApiConstants.ER_AVS_RESULT_DECLINE);
        retVal = false;
      }
      else if ( authResponse.hasCvv2ResultCode() &&
                Profile.isCvv2ResultDeclined(authResponse.getCvv2ResultCode()) )
      {
        setError(TridentApiConstants.ER_CVV2_RESULT_DECLINE);
        retVal = false;
      }
      
      // found a user defined reason to decline
      // reverse the authorization, do not reverse verify only
      if ( retVal == false && isAuthVerifyOnly() == false )
      {
        try
        {
          setTranAmount(0.0);     // force full reversal
          reverseAuthorization(false);
        }
        catch( Exception e )
        {
          // failed to reverse auth, ignore
        }          
      }
    }
    return( retVal );
  }
  
  public boolean reverseAuthorization()
    throws IOException, javax.xml.rpc.ServiceException
  {
    return( reverseAuthorization(true) );
  }
  
  public boolean reverseAuthorization( boolean saveAuthResponse )
    throws IOException, javax.xml.rpc.ServiceException
  {
    return( reverseAuthorization( AuthorizationHost, AuthorizationPort, saveAuthResponse ) );
  }
  
  public boolean reverseAuthorization( String host, int port )
    throws IOException, javax.xml.rpc.ServiceException
  {
    return( reverseAuthorization( host, port, true ) );
  }   
  
  public boolean reverseAuthorization( String host, int port, boolean saveAuthResponse )
    throws IOException, javax.xml.rpc.ServiceException
  {
    int       intlPid   = getIntlProcessor();
    boolean   isIntlReq = isIntlRequest( getProfile().isIntlUSDEnabled() );
    boolean   retVal    = false;
    long      beginTS   = System.currentTimeMillis();

    if ( isIntlReq && intlPid == mesConstants.INTL_PID_ADYEN )
    {
      // get a reference to the basic Adyen web service
      PaymentLocator sl = new PaymentLocator(); 
      sl.setPaymentHttpPortEndpointAddress(getAdyenEndpoint());
      PaymentPortType service = sl.getPaymentHttpPort(); 
      
      // setup the Adyen HTTP authentication values
      ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.USERNAME_PROPERTY, getAdyenCompanyId() );
      ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.PASSWORD_PROPERTY, getAdyenPassword()  );
      
      ModificationRequest modRequest = new ModificationRequest();
      modRequest.setMerchantAccount( getAdyenMerchantId() );
      modRequest.setOriginalReference( getAdyenTranId() );
      service.cancel(modRequest);
    }
    else if (isIntlReq && intlPid == mesConstants.INTL_PID_PAYVISION )
    {
      // get a reference to the PayVision web service
      BasicOperationsLocator  sl      =  new BasicOperationsLocator(); 
      sl.setBasicOperationsSoapEndpointAddress( getPayVisionEndpoint() + TridentApiConstants.PAYVISION_BASIC_PATH );
      BasicOperationsSoapStub service = (BasicOperationsSoapStub)sl.getBasicOperationsSoap(); 
      TransactionResult       result  = service._void(getPayVisionMemberId(),
                                                      getPayVisionMemberGuid(),
                                                      getPayVisionTranId(),
                                                      getPayVisionTranGuid(),
                                                      getApiTranId());

      if ( result.getResult() == TridentApiConstants.PAYVISION_SUCCESS )
      {
        retVal = true;
      }
      else
      {
        setError(TridentApiConstants.ER_PV_REVERSAL_FAILED);
        setErrorDesc(getPayVisionErrorDesc() +  " (" + getPayVisionErrorCode() + ")");
      }
    }
    else    // standard Trident auth reversal
    {
      retVal = reverseAuthorizationTrident( host, port, saveAuthResponse );
    }
    
    SwitchDuration = (System.currentTimeMillis() - beginTS);

    return( retVal );
  }
  
  public boolean reverseAuthorizationTrident( String host, int port, boolean saveAuthResponse )
    throws IOException
  {
    RequestDMessage         authRequest     = null;
    ResponseDMessage        authResponse    = null;
    boolean                 retVal          = true;
  
    // only reverse if there is an amount to reverse
    if ( getAuthAmount() != 0.0 )
    {
      // create a request
      authRequest = DMessageFactory.createRequestMessage();
      setAuthReversalFields( authRequest );
    
      // send the request to Trident
      NetConnectAuthClient client = new NetConnectAuthClient();
      client.setUrl(host);
      authResponse = client.sendRequest(authRequest);

      if ( saveAuthResponse )
      {
        // store the response
        setResponse(authResponse);
      }        
    
      //System.out.println( authRequest.toXml() );
      //System.out.println( authResponse.toXml() );
      retVal = authResponse.getResponseCode().equals(TridentApiConstants.RESP_APPROVED);
    }
    return( retVal );
  }
  
  public void setAdyenEndpoint( String endpoint )
  {
    AdyenEndpoint = endpoint;
  }
  
  public void setAdyenFields( TridentApiTransaction tran )
  {
    setIntlFields(tran);
  }
  
  public void setAdyenResult( PaymentResult result )
  {
    AdyenResult = result;
  }
  
  public void setApiTranId( String tranId )
  {
    ApiTranId = tranId;
  }
  
  public void setAuthorizationHost( String host, int port )
  {
    AuthorizationHost = host;
    AuthorizationPort = port;
  }
  
  public void setAuthorizationHost( String host )
  {
    setAuthorizationHost( host, 0 );
  }
  
  protected void setAuthBaseFields( RequestDMessage authRequest )
  {
    // set base record related fields
    authRequest.setRecordFormat("D");
    authRequest.setApplicationType("2");
    authRequest.setMessageDelimiter(".");
    authRequest.setAcquirerBin(getBinNumber());
    authRequest.setCurrencyCode(getCurrencyCode());
    authRequest.setCountryCode("840");              // US merchant
    authRequest.setLanguageIndicator("00");         // Eng
    authRequest.setTimeZoneDifferential(getTimeZone());
    authRequest.setTransactionSequenceNumber("0001");
    authRequest.setTransactionCode(getTranCode());
    authRequest.createGroupIII("000");              // necessary?
    
    // set the merchant related fields
    authRequest.setProfileId(getProfileId());
    authRequest.setDeviceCode("O");    
    authRequest.setIndustryCode( getIndustryCode() );
    authRequest.setCityCode(getDbaZip());  // city code (zip)
    authRequest.setMerchantCategoryCode(getSicCode());
    authRequest.setCardAcceptorData( getDbaName(), 
                                     getCityPhone(), 
                                     getDbaState());
    
    // set the transaction related fields
    if ( isSwiped() )    // swiped
    {
      authRequest.setCustomerDataField(getCardSwipeFull());
    }
    else    // keyed
    {
      authRequest.setCustomerDataField(getCardNumberFull(), getExpDate());
      
      if ( !isCardPresent() && !TridentApiConstants.VISAD_TC_CARD_VERIFY.equals(getTranCode()))
      {
        // It is considered an error to send moto/ecomm with card present request
        // Or on card verify requests (PMG-189)
        authRequest.setMotoIndicator(getMotoEcommIndicator());
      }
       
      // only perform AVS and/or CVV2 check for keyed transactions
      authRequest.setCardholderIdentificationData(getAvsData());  // avs address
      
      if ( hasCvv2() )
      {
        authRequest.setCardVerificationValue2(getCvv2Formatted());  // grp III, ver 007, CVV2
      }
      
      String cardType = getCardType();
      if ( cardType.equals("MC") && hasUCAFData() )
      {
        authRequest.setUcafCollectionIndicator(getUcafCollectionInd());
        authRequest.setUcafAuthenticationData(getUcafAuthData());
      }
      else if ( cardType.equals("VS") && hasVerifiedByVisaData() )
      {
        authRequest.setCavv(getVisaCavv());
        authRequest.setXid(getVisaXid());
      }
    }
    authRequest.setAccountDataSource(getAccountDataSource()); 
    authRequest.setTransactionAmount(TridentTools.encodeAmount(getAuthRequestAmount(),1,getCurrencyCode()));
    authRequest.setCardholderIdentificationCode(getCardIdCode());  
    
    // set trident specified fields
    authRequest.setMerchantKey(getProfileKey()); // forward the key
    authRequest.setCardLevelResultsIndicator();  // request card level result code
    
    String versionId = String.valueOf(TridentApiConstants.ApiVersionId);
    if ( versionId.length() > 4 )
    {
      versionId = versionId.substring(0,4);
    }
    authRequest.setDeveloperInformation( getDeveloperId(),
                                         StringUtilities.rightJustify(versionId,4,'0') );
  }
  
  public void setAuthReversalData( String reversalData )
  {
    AuthReversalData = reversalData;
  }
  
  protected void setAuthRequestFields( RequestDMessage authRequest )
  {
    setAuthBaseFields(authRequest);
    authRequest.setRequestedACI("Y");   // CPS capable
    authRequest.setRequestUuid();       // request a unique id
    authRequest.createGroupIII("401");  // request enhanced amex auth response
    
    // set debit fields if present
    if ( hasDebitData() )
    {
      authRequest.setRecordFormat("T");
      authRequest.setApplicationType("0");
      authRequest.setRequestedACI("N");
      authRequest.setTransactionCode(TridentApiConstants.VISAD_TC_DEBIT_PURCHASE);
      authRequest.setCardholderIdentificationCode("K"); // dukpt
      authRequest.setAccountDataSource("D"); // track2
      
      // pinblock + KeySerialNumber - should come from the upstream request as a single field
      authRequest.setCardholderIdentificationData(getDebitPinBlock()+getDebitKeySerialNumber());

      // static debit info
      authRequest.createGroupII();
      authRequest.getGroupII().setField(1, "ELY8QGUHV");    // sharing group
      authRequest.getGroupII().setField(2, "990040808V301");// merchant aba
      authRequest.getGroupII().setField(3, "000000");       // agent bank
      authRequest.getGroupII().setField(4, "000000");       // agent chain
      authRequest.getGroupII().setField(5, "001");          // batch number
      authRequest.getGroupII().setField(6, "Z");            // reimburse attr
    }
    else if ( isCashAdvance() )
    {
      authRequest.setTransactionCode(TridentApiConstants.VISAD_TC_CASH_ADVANCE);
      authRequest.setIndustryCode("B");   // bank/financial institution
    }
    else
    {
      authRequest.setCommercialCardRequestIndicator();  // test for commercial card
      authRequest.setRequestAdditionalAmounts();        // request balance (pre-paid)
      
      if ( ResponseCtlFlags.get(TridentApiConstants.FN_PARTIAL_AUTH) != null )
      {
        authRequest.setPartialAuthorizationEnabled();
      }
    }

    //
    // expansion
    //
    //authRequest.setMarketSpecificData(String val)
    //authRequest.setAgentBankNumber(String val)
    //authRequest.setAgentChainNumber(String val)
    //authRequest.setBatchNumber(String val)
    //authRequest.setReimbursementAttribute(String val)
    //authRequest.setOriginalPurchaseData(String val)
    //authRequest.setMerchantVerificationValue(String mvv)
    //authRequest.setUcafCollectionIndicator(String val)
    //authRequest.setUcafAuthenticationData(String val)
    //
  }
  
  protected void setAuthReversalFields( RequestDMessage authRequest )
  {
    setAuthBaseFields(authRequest);
    authRequest.setTransactionCode( TridentApiConstants.VISAD_TC_REVERSAL );
    authRequest.setRequestedACI( getReturnedAci() );
    authRequest.setReversalAndCancelDataI( getAuthReversalData() );
    authRequest.setReversalandIncrementalTransactionID( getAuthTransactionId() );
    
    // during a reversal, the transaction amount is set 
    // to the original authorization amount and the 
    // secondary amount is set to the new auth amount
    authRequest.setTransactionAmount(getAuthAmount());
    
    if ( getTranAmount() == 0.0 )
    {
      // assume full reversal, secondary amount (aka settled amount) is $0
      authRequest.setSecondaryAmount(0.0);
    }
    else  // partial reversal
    {
      // setted amount will be the original transaction amount
      // less the amount of this reversal (i.e. current tran amount)
      // getDoubleString() will clean up any Java floating point 
      // inaccuracy (i.e. when value sb 28.00 and is 27.9999999998)
      double settledAmount = Double.parseDouble(NumberFormatter.getDoubleString((getAuthAmount() - getTranAmount()),"0.00"));
      authRequest.setSecondaryAmount( settledAmount );
    }      
  }
  
  protected void setAuthTransactionId( String tranId )
  {
    AuthTransactionId = tranId;
  }
  
  public void setAvsStreet( String avsStreet )
  {
    AvsStreet = avsStreet;
  }
  
  public void setAvsZip( String avsZip )
  {
    AvsZip = avsZip;
  }
  
  public void setCardData( HttpServletRequest request )
  {
    if ( Emulation == TridentApiConstants.EMU_AUTHORIZE_NET )
    {
      // authorize.net emulation only supports keyed card number
      IncomingCardData[CD_NUMBER] = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_AN_CARD_NUMBER,null);
    }
    else    // standard API param names
    {
      IncomingCardData[CD_ID]     = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_ID,null);
      IncomingCardData[CD_SWIPE]  = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_SWIPE,null);
      IncomingCardData[CD_NUMBER] = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_NUMBER,null);
      
      // if an encrypted card swipe was provided, decrypt and set to card swipe
      String cardSwipeEnc = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_SWIPE_ENC,null);
      if ( cardSwipeEnc != null )
      {
        try
        {
          String ksnHex = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_SWIPE_KSN,null);
          IncomingCardData[CD_SWIPE] = MesEncryption.decryptString(ksnHex,cardSwipeEnc);
        }
        catch( masthead.encrypt.EncryptionException ee )
        {
          setInputErrorCode( TridentApiConstants.ER_TRIDENT_ENCRYPTION_ERROR );
        }
        catch( Exception e )
        {
          setInputErrorCode( TridentApiConstants.ER_INTERNAL_ERROR );
        }
      }
      
      // if an encrypted keyed card was provided, decrypt and set to card number + exp date
      String cardNumEnc = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_NUMBER_ENC,null);
      if ( cardNumEnc != null )
      {
        try
        {
          String ksnHex   = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARD_SWIPE_KSN,null);
          String cardData = MesEncryption.decryptString(ksnHex,cardNumEnc);
          
          int offset = cardData.indexOf("=");
          IncomingCardData[CD_NUMBER] = cardData.substring(0,offset);
          setExpDate(   cardData.substring(offset+3,offset+5)    // month
                      + cardData.substring(offset+1,offset+3));  // year
        }
        catch( masthead.encrypt.EncryptionException ee )
        {
          setInputErrorCode( TridentApiConstants.ER_TRIDENT_ENCRYPTION_ERROR );
        }
        catch( Exception e )
        {
          setInputErrorCode( TridentApiConstants.ER_INTERNAL_ERROR );
        }
      }
    }      
  }
  
  public void setCardholderEmail( String value )
  {
    CardholderEmail = value;
  }
  
  public void setCardholderFirstName( String value )
  {
    CardholderFirstName = value;
  }
  
  public void setCardholderLastName( String value )
  {
    CardholderLastName = value;
  }
  
  public void setCardholderPhone( String value )
  {
    CardholderPhone = value;
  }
  
  public void setCardholderReferenceNumber( String value )
  {
    CardholderRefNum = value;
  }
  
  public void setCardholderState( String value )
  {
    CardholderState = value;
  }
  
  public void setCardStoreFlag( String flag )
  {
    if ( flag != null && flag.toUpperCase().equals("Y") )
    {
      CardStoreRequested = true;
    }
    else
    {
      CardStoreRequested = false;
    }
  }

  public void setCardinalCentinelUrl( String url )
  {
    CardinalCentinelUrl = url;
  }
  
  public void setCardPresent( String cardPresent )
  {
    CardPresent = cardPresent;
  }
  
  public void setCardSwipe( String input )
  {
    String          cardNumber    = null;
    String          cardSwipe     = null;
    int             offsetBegin   = 0;
    int             offsetEnd     = 0;
    
    try
    {
      if ( input != null && !input.equals("") )
      {
        // remove any sentinels etc
        cardSwipe = TridentTools.decodeCardSwipeTrackData(input);
        
        // extract the card number from the track data
        if( cardSwipe.charAt(0) == 'B' )
        {
          cardNumber = cardSwipe.substring(1,cardSwipe.indexOf("^"));
        }
        else
        {
          cardNumber = cardSwipe.substring(0,cardSwipe.indexOf("="));
        }
        
        // Some amex mag stripes have blank spaces in 
        // the card number field data, remove them.
        cardNumber = TridentTools.removeSpaces(cardNumber);
      
        // class member variable must be encrypted because this class
        // will get serialized to disk (using BDB).
        CardSwipeEnc = MesEncryption.getClient().encrypt(cardSwipe.getBytes());
        CardSwipe    = TridentTools.encodeCardSwipe(cardSwipe);
        
        // set the entry mode to swiped
        setPosEntryMode(TridentApiConstants.FV_ENTRY_MODE_SWIPED);
                
        // store the card number for insertion into Oracle
        setCardNumberFull(cardNumber);
      }
      else    // input was null, clear class values
      {
        CardSwipe     = null;
        CardSwipeEnc  = null;
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + ".setCardSwipe()", e.toString() );
      
      // default to null values
      CardSwipe     = null;
      CardSwipeEnc  = null;
    }
  }
  
  public void setContactless( boolean contactless )
  {
    Contactless = contactless;
  }
  
  public void setDebitKeySerialNumber( String keySerialNum )
  {
    DebitKeySerialNumber = keySerialNum;
  }
  
  public void setDebitPinBlock( String pinBlock )
  {
    DebitPinBlock = pinBlock;
  }
  
  public void setDeveloperId(String developerId )
  {
    DeveloperId = developerId;
  }
  
  public void setDynamicDbaData( HttpServletRequest request )
  {
    // set the dynamic dba information
    setDynamicDbaPhone(HttpHelper.getStringAttribute(request,TridentApiConstants.FN_PHONE,null));
    setDynamicDbaName (HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DBA_NAME,null));
    setDynamicSicCode (HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SIC,null));
  }    

  public void setDynamicDbaName( String value )
  {
    String    dbaName   = null;
    
    if ( isDynamicDbaDataAllowed() && !isBlank(value) )
    {
      if ( isDynamicDbaNamePrefixEditEnabled() )
      {
        String prefix = getDynamicDbaNamePrefix().toUpperCase() + "*";
      
        if ( value.toUpperCase().startsWith(prefix) )
        {
          dbaName = value;
        }
        else
        {
          dbaName = (prefix + value);
        }
      }
      else    // allow any value passed by merchant
      {
        dbaName = value;
      }
      
      // prevent length overflows on dynamic dba information
      if ( dbaName.length() > TridentApiConstants.FLM_MERCHANT_NAME )
      {
        dbaName = dbaName.substring(0,TridentApiConstants.FLM_MERCHANT_NAME);
      }
    }
    setDbaName(dbaName);
  }
  
  public void setDynamicDbaPhone( String value )
  {
    String    phoneNumber   = null;
    
    if ( value != null && value.trim().length() > 0 )  
    { 
      if ( TridentTools.isValidMerchantPhone(value) )
      {
        phoneNumber = TridentTools.getFormattedPhone(value,false);
      }
    }
    setCustServicePhone(phoneNumber);
  }
  
  public void setDynamicSicCode( String value )
  {
    if ( isDynamicDbaDataAllowed() && !isBlank(value) )  
    {
      Sic = value;
    }
  }
  
  public void setEchoFields( HttpServletRequest request )
  {
    StringBuffer    fieldName = null;
    String          name      = null;
    int             offset    = TridentApiConstants.FN_ECHO_PREFIX.length();
    Enumeration     params    = request.getAttributeNames();
    String[]        values    = null;
    
    while( params.hasMoreElements() )
    {
      name = (String)params.nextElement();
      
      if ( name.toLowerCase().startsWith( TridentApiConstants.FN_ECHO_PREFIX ) )
      {
        if ( EchoFields == null )
        {
          EchoFields = new HashMap();
        }
        
        EchoFields.put(TridentApiConstants.FN_ECHO_RESP_PREFIX+name.substring(offset), request.getAttribute(name));
        
        
//        values = request.getParameterValues(name);
//        for( int i = 0; i < values.length; ++i )
//        {
//          if ( fieldName == null )
//          {
//            fieldName = new StringBuffer();
//          }
//          else
//          {
//            fieldName.setLength(0);
//          }            
//          fieldName.append( TridentApiConstants.FN_ECHO_RESP_PREFIX );
//          fieldName.append( name.substring(offset) );
//          EchoFields.put( fieldName.toString(), values[i] );
//        }
      }
    }
  }
  
  public void setError( String ecode )
  {
    setErrorCode(ecode);
    setErrorDesc(TridentApiConstants.getErrorDesc(ecode));
  }
  
  public void setErrorCode( String ecode )
  {
    ErrorCode = ecode;
    
    // pad valid error codes to minimum of 3 digits
    if ( ErrorCode != null && !ErrorCode.equals("") )
    {
      while( ErrorCode.length() < 3 )
      {
        ErrorCode = ("0" + ErrorCode);  // leading zeros
      }
    }
  }
  
  public void setErrorDesc( String desc )
  {
    ErrorDesc = desc;
  }
  
  public void setExpDate( String expDate )
  {
    String[]  formats   = {"MMyy","MM/yy","MM-yy","MMyyyy","MM/yyyy","MM-yyyy","yyyy-MM-dd","yyyy/MM/dd"};
    int       formatIdx = 0;
    Date      temp      = null;
    
    try
    {
      while( temp == null && formatIdx < formats.length )
      {
        temp = DateTimeFormatter.parseDate(expDate,formats[formatIdx],false);
        formatIdx++;
      }
    
      if ( temp != null )
      {
        ExpDate = DateTimeFormatter.getFormattedDate(temp,"MMyy");
      }
    }
    catch( Exception e )
    {
      // ignore, request will be considered invalid
    }      
  }
  
  public void setExtendedResponseField( String fname, String value )
  {
    if ( EchoFields == null )
    {
      EchoFields = new HashMap();
    }
    EchoFields.put(fname,value);    // use the existing echo fields hash map
  }
  
  public void setExternalTranId( String tranId )
  {
    ExternalTranId = tranId;
  }
  
  public void setFxFields( TridentApiTransaction tran )
  {
    // clone the data fields directly from the passed object
    setOriginalFxAmount( tran.getFxAmount() );
    setFxRateId( tran.getFxRateId() );
    setFxTranId( tran.getFxTranId() );
  }
  
  public void setFxFields( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setOriginalFxAmount( resultSet.getDouble("fx_amount") );
    setFxRateId( resultSet.getInt("fx_rate_id") );
    setFxTranId( resultSet.getString("fx_ref_num") );
    
    //
    // FxAmount needs to be set via a ResultSet column
    // for the reporting only.  if the FxAmount has 
    // already been established (i.e. via a HttpServletRequest)
    // then this method should *not* override the existing value.
    //
    if ( getFxAmount() == 0.0 )
    {
      setFxAmount( resultSet.getDouble("fx_amount") );
    }
  }
  
  public void setFxRate( ForeignExchangeRate rate )
  {
    FxRate = rate;
  }
  
  public void setFxValidationsEnabled( boolean validationsEnabled )
  {
    FxValidationsEnabled = validationsEnabled;
  }
  
  public void setGtsEndpoint( String endpoint )
  {
    GtsEndpoint = endpoint;
  }
  
  public void setGtsResult(TransactionResponsePacket result)
  {
    GtsResult = result;
  }
  
  public void setHoldForReview( boolean holdForReview )
  {
    HoldForReview = holdForReview;
  }
  
  public void setIpAddress( String ipAddr )
  {
    try
    {
      if ( ipAddr != null && !ipAddr.equals("") )
      {
        // getByName will throw an exception if the ipAddr is 
        // not formatted correctly
        InetAddress inetAddr = InetAddress.getByName(ipAddr);
        IpAddress = ipAddr;
      }
    }
    catch( java.net.UnknownHostException uhe )
    {
      // ignore, bad data
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + ".setIpAddress(" + ipAddr + ")", e.toString());
    }
  }
  
  public void setIndustryCode( String industryCode )
  {
    IndustryCode = industryCode;
  }
  
  public void setInputErrorCode( String errorCode )
  {
    InputErrorCode = errorCode;
  }
  
  public void setIntl3DEnabled( boolean newValue )
  {
    Intl3DEnabled = newValue;
  }
  
  public void setIntlFields( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setCurrencyCode( resultSet.getString("currency_code") );
    setOriginalTranAmount( resultSet.getDouble("tran_amount") );
    setTridentTranIdNumber( resultSet.getLong("tran_id_number") );
    setExternalTranId( resultSet.getString("external_tran_id") );
    setAuthAmount( resultSet.getDouble("auth_amount") );
    
    if ( TranAmount == 0.0 )
    {
      TranAmount = resultSet.getDouble("tran_amount");
    }
  }
  
  public void setIntlFields( TridentApiTransaction tran )
  {
    // clone the data fields directly from the passed object
    setTridentTranIdNumber( tran.TridentTranIdNumber );
    setExternalTranId( tran.ExternalTranId );
    setCurrencyCode( tran.CurrencyCode );
  }

  private void setItpsFlag(boolean isIsps) {
	  ItpsFlag = isIsps;
  }

  private void setItpsAddressResult(String avsResult) {
	  ItpsAddressResult = avsResult;
  }
  
  private void setItpsZipResult(String avsResult) {
	  ItpsZipResult = avsResult;
  }

  private void setItpsCvvResult(String cvvResult) {
	  ItpsCvvResult = cvvResult;
  }

  private void setItpsAuthCode(String authCode) {
	  ItpsAuthCode = authCode;
  }
  
  public void setItpsEndpoint( String endpoint )
  {
    ItpsEndpoint = endpoint;
  }
  
  private void setItpsErrorCode(String errorCode) {
	  ItpsErrorCode = errorCode;
  }

  public void setLevelIIIData( HttpServletRequest request )
  {
    String[]  values      = null;
    
    // set the basic level III fields
    for( int i = 0; i < TridentApiConstants.LevelIIIFieldNames.length; ++i )
    {
      String fname = TridentApiConstants.LevelIIIFieldNames[i];
      String value = HttpHelper.getStringAttribute(request,fname,null);
      
      if ( value != null && !value.trim().equals("") )
      {
        LevelIIIFields.put(fname,value);
      }
    }
    
    Object viValues = request.getAttribute(TridentApiConstants.FN_VISA_LINE_ITEM);
    Object mcValues = request.getAttribute(TridentApiConstants.FN_MC_LINE_ITEM);
    Object amValues = request.getAttribute(TridentApiConstants.FN_AMEX_LINE_ITEM);
    
    if(viValues != null && viValues instanceof String[]) {
      values = (String[])viValues;
      LevelIIICardType = "VS";
    }
    else if(mcValues != null && mcValues instanceof String[]) {
      values = (String[])mcValues;
      LevelIIICardType = "MC";
    }
    else if(amValues != null && amValues instanceof String[]) {
      values = (String[])amValues;
      LevelIIICardType = "AM";
    }
    
    if ( values != null )
    {
      LineItemData = new ArrayList(values.length);
      for( int i = 0; i < values.length; ++i )
      {
        LineItemData.add(values[i]);
      }
    }
  }
  
  public void setLinkedTranAmount( double amount )
  {
    LinkedTranAmount = amount;
  }
  
  public void setOriginalFxAmount( double amount )
  {
    OriginalFxAmount = amount;
  }
  
  public void setOriginalTranAmount( double amount )
  {
    OriginalTranAmount = amount;
  }
  
  public void setOriginalTranId( String tranId )
  {
    OriginalTranId = tranId;
  }
  
  public void setOriginalTranIdRequired( boolean originalTranIdRequired )
  {
    OrigTranIdRequired = originalTranIdRequired;
  }
  
  public void setPayload3D( String payload )
  {
    Payload3D = payload;
  }
  
  public void setPayVisionEndpoint( String endpoint )
  {
    PayVisionEndpoint = endpoint;
  }
  
  public void setPayVisionFields( ResultSet resultSet )
    throws java.sql.SQLException
  {
    setIntlFields(resultSet);
  }
  
  public void setPayVisionFields( TridentApiTransaction tran )
  {
    setIntlFields(tran);
  }
  
  public void setPayVisionResult( TransactionResult result )
  {
    PayVisionResult = result;
  }
  
  public void setPreAuthFields( TridentApiTransaction tran )
  {
    // clone the data fields from the pre-auth that are 
    // needed to validate an incoming settle request
    setCardNumber   ( tran.getCardNumber() );     // required to determine card type
    setCardNumberEnc( tran.getCardNumberEnc() );  // required if reauth necessary
    setExpDate      ( tran.getExpDate() );        // ""
    setFraudResult  ( tran.getFraudResult() );
    setAvsZip       ( tran.getAvsZip() );
    setCurrencyCode ( tran.getCurrencyCode() );
  }
  
  public void setProfile( TridentApiProfile tp )
  {
    Profile = tp;
  }
  
  public void setProperties( ResultSet resultSet )
    throws java.sql.SQLException
  {
    TridentTranIdNumber = resultSet.getLong("trident_tran_id_num");
    TridentTranId     = processString(resultSet.getString("trident_tran_id"));
    ExternalTranId    = processString(resultSet.getString("external_tran_id"));
    AciReturned       = processString(resultSet.getString("returned_aci"));
    ProfileId         = processString(resultSet.getString("terminal_id"));
    DbaName           = processString(resultSet.getString("dba_name"));
    DbaCity           = processString(resultSet.getString("dba_city"));
    DbaState          = processString(resultSet.getString("dba_state"));
    DbaZip            = processString(resultSet.getString("dba_zip"));
    DebitCreditInd    = processString(resultSet.getString("debit_credit_ind"));
    Sic               = processString(resultSet.getString("sic_code"));
    CustServicePhone  = processString(resultSet.getString("phone_number"));
    TranDate          = resultSet.getDate("tran_date");
    TranAmount        = resultSet.getDouble("tran_amount");
    AuthAmount        = resultSet.getDouble("auth_amount");
    AuthCode          = resultSet.getString("auth_code");
    ReferenceNumber   = processString(resultSet.getString("reference_number"));
    PurchaseId        = processString(resultSet.getString("purchase_id"));
    TranType          = processString(resultSet.getString("tran_type"));
    MerchantId        = resultSet.getLong("merchant_number");
    PosEntryMode      = processString(resultSet.getString("pos_entry_mode"));
    AvsZip            = processString(resultSet.getString("avs_zip"));
    
    // set the debit network id
    setDebitNetworkId(resultSet.getString("debit_network_id"));
    
    // set the currency code
    setCurrencyCode( processString(resultSet.getString("currency_code"),
                                   TridentApiConstants.FV_CURRENCY_CODE_DEFAULT) );
    
    // result set is pre-auth data which contains truncated card numbers
    setCardNumber   ( processString(resultSet.getString("card_number")) );
    setCardNumberEnc( resultSet.getString("card_number_enc") );
    setExpDate      ( resultSet.getString("exp_date") );
    setFraudResult  ( resultSet.getString("fraud_result") );
    
    Emulation = resultSet.getInt("emulation");
    
    // set the foreign exchange fields 
    // with data from Oracle
    setFxFields(resultSet);
    
    try
    {
      // these fields are in the reporting only
      TestFlag = processString(resultSet.getString("test_flag"));
    }
    catch( Exception e )
    {
      // ignore errors, should be the pre-auth sync thread
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    String    userTranId  = null;
    
    ProfileId         = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_TID,null);
    Cvv2              = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CVV2,null);
    TranDate          = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    TranAmount        = Math.abs(MesMath.round(HttpHelper.getDoubleAttribute(request,TridentApiConstants.FN_AMOUNT,0.0),2));
    TranAmountRaw     = HttpHelper.getStringAttribute(request, TridentApiConstants.FN_AMOUNT, null); 
    ReferenceNumber   = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_REF_NUM,null);
    PurchaseId        = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_INV_NUM,null);
    TaxAmount         = HttpHelper.getDoubleAttribute(request,TridentApiConstants.FN_TAX,0.0);
    TranType          = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_TRAN_TYPE,TT_INVALID);
    AvsStreet         = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_AVS_STREET,null);
    AvsZip            = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_AVS_ZIP,null);
    IndustryCode      = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_INDUSTRY_CODE,TridentApiConstants.FV_INDUSTRY_CODE_DEFAULT);
    AuthCode          = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_AUTH_CODE,null);
    OriginalTranId    = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_TRAN_ID,null);
    IntlUSDRequest    = HttpHelper.getBooleanAttribute(request,TridentApiConstants.FN_INTL_USD_REQUEST,false);
    FraudScan         = HttpHelper.getBooleanAttribute(request,TridentApiConstants.FN_FRAUD_SCAN,true);
    CardStoreRequested= HttpHelper.getBooleanAttribute(request,TridentApiConstants.FN_STORE_CARD,false);
    ResponseHash      = HttpHelper.getBooleanAttribute(request, TridentApiConstants.FN_RESP_HASH, false);
    
    // set the direct marketing contact info if present
    setDMContactInfo( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DM_CONTACT_INFO,null) );
    
    // set the MOTO/e-Commerce indicator    
    setMotoEcommIndicator( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_MOTO_IND,TridentApiConstants.FV_MOTO_ECOMM_DEFAULT) );
    
    // set the country and currency codes
    setCurrencyCode( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CURRENCY_CODE,TridentApiConstants.FV_CURRENCY_CODE_DEFAULT) );
    setCountryCode( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_COUNTRY_CODE,TridentApiConstants.FV_COUNTRY_CODE_DEFAULT) );
    
    // set the IP address.  call *after* setCurrencyCode(..)
    setIpAddress( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_IP_ADDRESS,null) );
    
    // set the retry id
    setRetryId( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_RETRY_ID,TridentApiConstants.RETRY_ID_NONE) );
    
    // set the rate table id
    setFxRateId( HttpHelper.getIntAttribute(request,TridentApiConstants.FN_FX_RATE_ID,TridentApiConstants.RATE_ID_NONE) );
    
    // set the amount in USD
    setFxAmount( HttpHelper.getDoubleAttribute(request,TridentApiConstants.FN_FX_AMOUNT,0.0) );
    
    // set the card expiration date
    setExpDate( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_EXP_DATE,null) );
    
    // recurring payments data 
    setRecurringPaymentNumber( HttpHelper.getIntAttribute(request,TridentApiConstants.FN_RECURRING_NUM,0) );
    setRecurringPaymentCount( HttpHelper.getIntAttribute(request,TridentApiConstants.FN_RECURRING_COUNT,0) );
    
    // 3D security data
    setPayload3D( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_3D_PAYLOAD,null) );
    setTranId3D( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_3D_TRAN_ID,null) );
    
    // verified by visa
    setVisaCavv( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CAVV,null) );
    setVisaXid( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_XID,null) );
    
    // mastercard universal cardholder identification
    setUcafCollectionInd( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_UCAF_COLL_IND,null) );
    setUcafAuthData( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_UCAF_AUTH_DATA,null) );
    
    // set the debit pin data
    setDebitPinBlock( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DEBIT_PIN_BLOCK,null) );
    setDebitKeySerialNumber( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DEBIT_KSN,null) );
    
    // only store the transaction id provided by the 
    // incoming request if it is not empty and is required
    // by the transaction type requested.
    userTranId = HttpHelper.getStringAttribute(request,TridentApiConstants.FN_TRAN_ID,null);
    if ( userTranId != null && !userTranId.trim().equals("") && tranTypeRequiresTranId() )
    {
      TridentTranId = userTranId;
    }
    
    // extract the cardholder name (if provided)
    setCardholderFirstName( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CH_FIRST_NAME,null) );
    setCardholderLastName ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CH_LAST_NAME,null) );
    setCardholderState    ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CH_STATE,null) );
    setCardholderPhone    ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CH_PHONE,null) );
    setCardholderEmail    ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CH_EMAIL,null) );
    
    // set the enhanced AVS data 
    setCustomerHostName   ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CUSTOMER_HOST_NAME    ,null) );
    setCustomerBrowserType( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CUSTOMER_BROWSER_TYPE ,null) );
    setCustomerANI        ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CUSTOMER_ANI          ,null) );
    setCustomerANI_II     ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CUSTOMER_ANI_II       ,null) );
    setProductSku         ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_PRODUCT_SKU           ,null) );
    setShippingMethod     ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIPPING_METHOD       ,null) );
    
    // store the original values for use by the validation
    setCardData(request);
    
    // store the full card number.  this method handles 
    // encrypting the full card value and truncating the clear value
    setCardNumberFull( IncomingCardData[CD_NUMBER] );
    
    // store the full card swipe.  this method handles
    // scrubbing the incoming data to remove a single track of data and
    // encrypting the track data and truncating the clear value
    // ** call this after setCardNumberFull
    setCardSwipe( IncomingCardData[CD_SWIPE] );
    
    // store the card id
    setCardId( IncomingCardData[CD_ID] );
    
    setContactless( HttpHelper.getBooleanAttribute(request,TridentApiConstants.FN_CONTACTLESS,false) );
    
    // set any user requested echo fields
    setEchoFields( request );
    
    // set the response control flags
    setResponseControlFlags( request );
    
    // set the shipping data if present
    setShipToData( request );
    
    // Set the travel date related to Hotels, Cruise Ships, and Car Rental
    setTravelData(request);
    
    // set the client reference number
    setClientReferenceNumber( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CLIENT_REF_NUM,null) );
    
    // set the cardholder reference number
    setCardholderReferenceNumber( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_CARDHOLDER_REF_NUM,null) );

    // api uses 'Z' to indicate not a moto, set to blank space per visa spec
    if ( isSwiped() || MotoEcommInd.equals( TridentApiConstants.FV_NOT_A_MOTO ) )
    {
      MotoEcommInd = " ";   // translate to blank space
      CardPresent  = "1";   // card present
    }
    
    if ( isCredit( TranType ) )
    {
      DebitCreditInd = "C";
    }
    else  // default is debit
    {
      DebitCreditInd  = "D";    
    }
    
    // extract any level III data from the request
    setLevelIIIData( request );
    
    // misc fields
    setDeveloperId( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DEVELOPER_ID,TridentApiConstants.AUTH_DEVELOPER_ID) );
    
    // set all the fraud fields
    setFraudData(request);
  }
          
  public void setResponse(ResponseDMessage response)
  {
    VisaDResponse = response; // save Visa D response
    TridentTranId = null;     // reset the tran id
    
    try
    {
      // attempt to extract the Trident generated UUID 
      String temp = VisaDResponse.getUuid();
    
      // test for a valid value 
      // by testing the length
      if ( temp.length() == 32 )
      {
        setTridentTranId(temp);
      }
      
      if ( isApprovedByIssuer() )
      {
        AuthAmount = TranAmount;  // set the authorized amount
      }
      
      // save the network id if present
      if ( VisaDResponse.hasGroupII() ) 
      {
        setDebitNetworkId(VisaDResponse.getNetworkIDCode()); 
      }        
    }
    catch( Exception e )
    {
      // ignore when UUID is invalid, API id will be used
    }
  }
  
  public void setResponseControlFlags( HttpServletRequest request )
  {
    String          name      = null;
    int             offset    = TridentApiConstants.FN_RCTL_PREFIX.length();
    Enumeration     params    = request.getAttributeNames();
    String          value     = null;
    String[]        values    = null;
    
    ResponseCtlFlags.clear();
    while( params.hasMoreElements() )
    {
      name = ((String)params.nextElement());
      
      if ( name.toLowerCase().startsWith( TridentApiConstants.FN_RCTL_PREFIX ) )
      {
    	  value = (String) request.getAttribute(name);
    	  value = value.toLowerCase();
    	  
    	  if ( value.equals("yes") || value.equals("y") || value.equals("true") )
    		  ResponseCtlFlags.put( name.substring(offset).toLowerCase(), "true" );
      }
    }
  }
  
  /**
   * Set the result from a successful Braspag authorization attempt.
   * @param ccdr
   */
  private void setBraspagResult(CreditCardDataResponse ccdr) {
    this.braspagResult = ccdr;
  }
  
  /**
   * Sets the Braspag endpoint URL. Defaults to {@link TridentApiConstants.BRASPAG_ENDPOINT_TEST}
   * @param url
   */
  public void setBraspagEndpoint(String url) {
    if(url == null || url.equals(""))
      this.braspagEndpoint = TridentApiConstants.BRASPAG_ENDPOINT_TEST;
    else
      this.braspagEndpoint = url;
  }
  
  public void setRequestTimestamp()
  {
    RequestTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());    
  }
  
  public void setBatchRequestFlag(boolean val)
  {
    BatchRequest = val;
  }
  
  public void setReturnedAci( String returnedAci )
  {
    AciReturned = returnedAci;
  }
  
  public void setFraudAcctCreationDate( Date value )
  {
    FraudAcctCreationDate = value;   
  }
  
  public void setFraudAcctName( String value )
  {
    FraudAcctName = value;           
  }
  
  public void setFraudAcctEmail( String value )
  {
    FraudAcctEmail = value;          
  }
  
  public void setFraudAcctLastChange( Date value )
  {
    FraudAcctLastChange = value; 
  }
  
  public void setFraudBrowserLanguage( String value )
  {
    FraudBrowserLanguage = value; 
  }
  
  public void setFraudData( HttpServletRequest request )
  {
    setFraudAcctName         ( HttpHelper.getStringAttribute  (request,TridentApiConstants.FN_ACCT_NAME         ,null ) );         
    setFraudAcctEmail        ( HttpHelper.getStringAttribute  (request,TridentApiConstants.FN_ACCT_EMAIL        ,null ) );        
    setFraudDigitalGoodsFlag ( HttpHelper.getBooleanAttribute (request,TridentApiConstants.FN_DIGITAL_GOODS     ,false) );
    setFraudSubscriptionFlag ( HttpHelper.getBooleanAttribute (request,TridentApiConstants.FN_SUBSCRIPTION      ,false) );
    setFraudDeviceId         ( HttpHelper.getStringAttribute  (request,TridentApiConstants.FN_DEV_ID            ,null ) );
    setFraudBrowserLanguage  ( HttpHelper.getStringAttribute  (request,TridentApiConstants.FN_BROWSER_LANGUAGE  ,null ) );
    setFraudAcctLastChange   ( HttpHelper.getDateAttribute    (request,TridentApiConstants.FN_ACCT_LAST_CHANGE  ,"MM/dd/yyyy", null)  );
    setFraudAcctCreationDate ( HttpHelper.getDateAttribute    (request,TridentApiConstants.FN_ACCT_CREATE_DATE  ,"MM/dd/yyyy", null)  ); 
  }
  
  public void setFraudDeviceId( String value )
  {
    FraudDeviceId = value; 
  }
  
  public void setFraudDigitalGoodsFlag( boolean value )
  {
    FraudDigitalGoodsFlag = value;
  }
  
  public void setFraudEndpoint( String endpoint )
  {
    FraudEndpoint = endpoint;
  }
  
  public void setFraudResult( String result )
  {
    FraudResult = result;
  }
  
  public void setFraudResultCodes( String resultCodes )
  {
    FraudResultCodes = resultCodes;
  }
  
  public void setFraudScan( boolean value )
  {
    FraudScan = value;
  }
  
  public void setFraudSubscriptionFlag( boolean value )
  {
    FraudSubscriptionFlag = value;
  }
  
  public void setSettledFlag( String flag )
  {
    if ( flag != null && flag.toUpperCase().equals("Y") )
    {
      Settled = true;
    }
    else
    {
      Settled = false;
    }
  }
  
  public void setShipToData( HttpServletRequest request )
  {
    setShipToFirstName  ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIP_TO_FIRST_NAME,null) );
    setShipToLastName   ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIP_TO_LAST_NAME,null) );
    setShipToAddress    ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIP_TO_ADDRESS,null) );
    setShipToZip        ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIP_TO_ZIP,null) );
    setShipToPhone      ( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_SHIP_TO_PHONE,null) );
    setShipToCountryCode( HttpHelper.getStringAttribute(request,TridentApiConstants.FN_DEST_COUNTRY_CODE,null) );
  }
  
  public void setSwitchDuration( long switchDuration )
  {
    SwitchDuration = switchDuration;
  }
  
  public void setTranId3D( String tranId )
  {
    TranId3D = tranId;
  }
  
  public void setTransactionDuration( long tranDuration )
  {
    TransactionDuration = tranDuration;
  }
  
  public void setTranState(int state)
  {
    TranState = state;
  }
  
  public void setTridentTranId( String tranId )
  {
    TridentTranId = tranId;
    
    if ( this instanceof AuthorizeNetTransaction )
    {
      long unsignedVal = 0L;
      if ( TridentTranId != null )
      {
        int hashCode = TridentTranId.hashCode();

        // move the integer bytes to a long so we can
        // treat the value as an unsigned integer.
        unsignedVal = ((long) ((0x000000FF & hashCode)| 
                               (0x0000FF00 & hashCode)| 
                               (0x00FF0000 & hashCode)| 
                               (0xFF000000 & hashCode)))
                       & 0xFFFFFFFFL;
      }        
      setTridentTranIdNumber(unsignedVal);
    }
  }
  
  public void setTridentTranIdNumber( long value )
  {
    TridentTranIdNumber = value;
  }
  
  public void setUcafAuthData( String input )
  {
    UcafAuthData = input;
  }
  
  public void setUcafCollectionInd( String input )
  {
    try
    {
      // strip any leading zeroes from input data
      UcafCollectionInd = String.valueOf( Integer.parseInt(input) );
    }
    catch( Exception e )
    {
      // invalid value
      UcafCollectionInd = null;
    }
  }
  
  public void setVisaCavv( String input )
  {
    if ( input != null )
    {
      VisaCavv = Base64.decodeAsHexString(input);
    }
    else
    {
      VisaCavv = null;
    }
  }
  
  public void setVisaXid( String input )
  {
    if ( input != null )
    {
      VisaXid = Base64.decodeAsHexString(input);
    }
    else
    {
      VisaXid = null;
    }
  }
      
  public boolean tranTypeRequiresAmount( )
  {
    return( tranTypeRequiresAmount(TranType) );
  }
  
  public boolean tranTypeRequiresAmount( String tranType )
  {
    return( tranType.equals(TT_CREDIT) ||
            tranType.equals(TT_DEBIT) ||
            tranType.equals(TT_FORCE) ||
            tranType.equals(TT_SETTLE) ||
            tranType.equals(TT_CASH_ADVANCE) ||
            tranType.equals(TT_PRE_AUTH) );
  }
  
  public boolean tranTypeRequiresCardData( )
  {
    return( tranTypeRequiresCardData(TranType) );
  }
  
  public boolean tranTypeRequiresCardData( String tranType )
  {
    return( tranType.equals(TT_CREDIT) ||
            tranType.equals(TT_DEBIT) ||
            tranType.equals(TT_FORCE) ||
            tranType.equals(TT_CASH_ADVANCE) ||
            tranType.equals(TT_PRE_AUTH) ||
            tranType.equals(TT_STORE_CARD) ||
            tranType.equals(TT_3D_ENROLL_CHECK) ||
            tranType.equals(TT_CARD_VERIFY) );
  }
  
  public boolean tranTypeRequiresCardId( )
  {
    return( tranTypeRequiresCardId(TranType) );
  }
  
  public boolean tranTypeRequiresCardId( String tranType )
  {
    return( tranType.equals(TT_DELETE_CARD_ID) );
  }
  
  public boolean tranTypeRequiresFxAmount( )
  {
    return( tranTypeRequiresFxAmount(TranType) );
  }
  
  public boolean tranTypeRequiresFxAmount( String tranType )
  {
    boolean     fxAmountRequired  = false;
    
    if ( tranType.equals(TT_FX_CONVERT_AMOUNT) )
    {
      fxAmountRequired  = true;
    }
    else if ( isNonUSCurrency() && isFxEnabled() )
    {
      fxAmountRequired  = ( tranType.equals(TT_CREDIT) ||
                            tranType.equals(TT_DEBIT) ||
                            tranType.equals(TT_PRE_AUTH) );
       
      // only require the fx amount (aka USD amount) on refunds 
      // and capture requests if the fx validations are enabled. 
      if ( !fxAmountRequired && areFxValidationsEnabled() )
      {
        fxAmountRequired  = ( tranType.equals(TT_REFUND) ||
                              tranType.equals(TT_SETTLE) );
      }
    }
    return( fxAmountRequired );
  }
  
  public boolean tranTypeRequiresFxRateId( )
  {
    return( tranTypeRequiresFxRateId(TranType) );
  }
  
  public boolean tranTypeRequiresFxRateId( String tranType )
  {
    boolean     rateRequired  = false;
    
    if ( isNonUSCurrency() && isFxEnabled() )
    {
      rateRequired  = ( tranType.equals(TT_CREDIT) ||
                        tranType.equals(TT_DEBIT) ||
                        tranType.equals(TT_PRE_AUTH) );
    }
    return( rateRequired );
  }
  
  public boolean tranTypeRequiresPurchaseId( )
  {
    return( tranTypeRequiresPurchaseId(TranType) );
  }

  public boolean tranTypeRequiresPurchaseId( String tranType )
  {
    return( tranType.equals(TT_3D_ENROLL_CHECK) );
  }
  
  public boolean tranTypeRequiresTranId( )
  {
    return( tranTypeRequiresTranId(TranType) );
  }
  
  public boolean tranTypeRequiresTranId( String tranType )
  {
    return( tranType.equals(TT_VOID) ||
            tranType.equals(TT_REFUND) ||
            tranType.equals(TT_AUTH_REVERSAL) ||
            tranType.equals(TT_SETTLE) );
  }
  
  public boolean tranTypeSupportsDebit( )
  {
    return( tranTypeSupportsDebit(TranType) );
  }
  
  public boolean tranTypeSupportsDebit( String tranType )
  {
    return( tranType.equals(TT_DEBIT) );
  }
  
  public boolean tranTypeSupportsLevelIII( )
  {
    return( tranTypeSupportsLevelIII(TranType) );
  }
  
  public boolean tranTypeSupportsLevelIII( String tranType )
  {
    return( tranType.equals(TT_DEBIT) ||    // immediate sale
            tranType.equals(TT_SETTLE) );   // capture
  }
  
  public boolean useGts()
  {
	  // TODO: Temporarily disable for Itps testing
	  return false;
//    TridentApiProfile profile = getProfile();
//    return( isIntlRequest( profile.isIntlUSDEnabled() ) && profile.isGtsEnabled() );
  }
  
  public boolean useAdyen()
  {
    // if one of the following is true, use PayVision
    //  1. non-USD currency code requested
    //  2. tran OR profile flag set to send USD to Adyen AND card type is V/MC
    TridentApiProfile profile = getProfile();
    return( isIntlRequest( profile.isIntlUSDEnabled() ) && profile.isAdyenEnabled() );
  }
  
  public boolean useIntlVendor()
  {
    return( useAdyen() || usePayVision() || useGts() || useItps() || useBraspag() );
  }
  
  public boolean useItps()
  {
	  TridentApiProfile profile = getProfile();
	  return( isIntlRequest( profile.isIntlUSDEnabled() ) && profile.isItpsEnabled() );
  }
  
  public boolean useBraspag()
  {
	  TridentApiProfile profile = getProfile();
	  return( isIntlRequest( profile.isIntlUSDEnabled() ) && profile.isBraspagEnabled() );
  }
  
  public boolean usePayVision()
  {
    // if one of the following is true, use PayVision
    //  1. non-USD currency code requested
    //  2. tran OR profile flag set to send USD to PV AND card type is V/MC
    TridentApiProfile profile = getProfile();
    return( isIntlRequest( profile.isIntlUSDEnabled() ) && profile.isPayVisionEnabled() );
  }
  
  public boolean usingVitalForAmexAuths()
  {
    return( getProfile().usingVitalForAmexAuths() );
  }

  /**
   * Validates mantissa for certain currency types.
   * @return true on valid currency format.
   */
  public boolean validCurrencyFormat() {
	  int numericCode = -1;
	  try {
		  numericCode = Integer.parseInt(getCurrencyCode());
	  }
	  catch( NumberFormatException e ) { }
	  
	  switch(numericCode) {
	  // Zero decimal place currencies (Never allow any decimal)
	  case 174:		// KMF
	  case 262:		// DJF
	  case 360:		// IDR
	  case 392:		// JPY
	  case 310:		// KRW
	  case 548:		// VUV
	  case 600:		// PYG
	  case 646:		// RWF
	  case 950:		// XAF
	  case 952: 	// XOF
	  case 953:		// XPF
		  int decimal = getTranAmountRaw().indexOf(".");
		  if(decimal >= 0) return false;
		  break;
	  
	  // Three decimal place currencies (Only enforce when there are more than three digits after decimal)
	  case 368:		// IQD
	  case 414:		// KWD
	  case 512:		// OMR
	  case 788:		// TND
		  // All unsupported
		  break;
	  }
	  return true;
  }

  public void showData( )
  {
    System.out.println("TranId                   : " + String.valueOf(getTridentTranId()));
    System.out.println("ProfileId                : " + String.valueOf(ProfileId));
    System.out.println("MerchantId               : " + String.valueOf(MerchantId));
    System.out.println("DbaName                  : " + String.valueOf(DbaName));
    System.out.println("DbaCity                  : " + String.valueOf(DbaCity));
    System.out.println("DbaState                 : " + String.valueOf(DbaState));
    System.out.println("DbaZip                   : " + String.valueOf(DbaZip));
    System.out.println("Sic                      : " + String.valueOf(Sic));
    System.out.println("CountryCode              : " + String.valueOf(CountryCode));
    System.out.println("CustServicePhone         : " + String.valueOf(CustServicePhone));
    System.out.println("CardNumber               : " + String.valueOf(getCardNumber()));
    System.out.println("ExpDate                  : " + String.valueOf(ExpDate));
    System.out.println("TranDate                 : " + String.valueOf(TranDate));
    System.out.println("DebitCreditInd           : " + String.valueOf(DebitCreditInd));
    System.out.println("TranAmount               : " + String.valueOf(TranAmount));
    System.out.println("TranType                 : " + String.valueOf(TranType));
    System.out.println("ReferenceNumber          : " + String.valueOf(ReferenceNumber));
    System.out.println("PurchaseId               : " + String.valueOf(PurchaseId));
    System.out.println("TaxAmount                : " + String.valueOf(TaxAmount));
    System.out.println("PosEntryMode             : " + String.valueOf(PosEntryMode));
    System.out.println("IntlUSDRequest           : " + String.valueOf(IntlUSDRequest));
    
    if ( VisaDResponse != null )
    {
      System.out.println("ApprovalCode             : " + String.valueOf(VisaDResponse.getApprovalCode()));
      System.out.println("ResponseCode             : " + String.valueOf(VisaDResponse.getResponseCode()));
      System.out.println("RetrievalReferenceNumber : " + String.valueOf(VisaDResponse.getRetrievalReferenceNumber()));
      System.out.println("TransactionIdentifier    : " + String.valueOf(VisaDResponse.getTransactionIdentifier()));
      System.out.println("ValidationCode           : " + String.valueOf(VisaDResponse.getValidationCode()));
      System.out.println("AuthorizationSourceCode  : " + String.valueOf(VisaDResponse.getAuthorizationSourceCode()));
      System.out.println("AVSResultCode            : " + String.valueOf(VisaDResponse.getAVSResultCode()));
      System.out.println("ReturnedACI              : " + String.valueOf(VisaDResponse.getReturnedACI()));
    }      
  }
  
  protected void showPayVisionResult()
  {
    TransactionResult pvResp      = getPayVisionResult();
    
    if ( pvResp != null )
    {
      System.out.println();
      System.out.println("*************************************************");
      System.out.println("* Transaction Result                            *");
      System.out.println("*************************************************");
      System.out.println("Result      : " + pvResp.getResult());
      System.out.println("Message     : " + pvResp.getMessage());
      System.out.println("MES TranId  : " + pvResp.getTrackingMemberCode());
      System.out.println("PV TranId   : " + pvResp.getTransactionId());
      System.out.println("PV TranGuid : " + pvResp.getTransactionGuid());
      System.out.println("*************************************************");
      System.out.println("* CDCs                                          *");
      System.out.println("*************************************************");
      
      CdcEntry[]        cdcs        = pvResp.getCdc().getCdcEntry();
      CdcEntryItem[]    cdcItems    = null;
    
      for( int cdcIdx = 0; cdcIdx < cdcs.length; ++cdcIdx )
      {
        System.out.println("CDC Name: " + cdcs[cdcIdx].getName());
        cdcItems = cdcs[cdcIdx].getItems().getCdcEntryItem();
        for( int cdcItemIdx = 0; cdcItemIdx < cdcItems.length; ++cdcItemIdx )
        {
          System.out.println( "  Key: " + cdcItems[cdcItemIdx].getKey() + "  Value: " + cdcItems[cdcItemIdx].getValue() );
        }
      }
      System.out.println();
    }
  }
  
  public    void showSoapRequest( org.apache.axis.client.Stub service )
  {
    try
    {
      org.apache.axis.client.Call _call = ((org.apache.axis.client.Stub)service)._getCall();
      System.out.println("SOAP: " + _call.getMessageContext().getRequestMessage().getSOAPPartAsString());
    }
    catch( Exception e )
    {
      log.error("showSoapRequest(" + service + ") failed - " + e.toString());
    }      
  }
}
