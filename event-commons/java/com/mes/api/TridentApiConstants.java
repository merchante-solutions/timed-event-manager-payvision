/*************************************************************************
 * FILE: $Archive: /Java/beans/com/mes/api/TridentApiConstants.sqlj $
 * <p/>
 * Description:
 * <p/>
 * Change History:
 * See VSS database
 * <p/>
 * Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * <p/>
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc.  This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 **************************************************************************/
package com.mes.api;

import java.util.HashMap;

public class TridentApiConstants {
  //
  // The value below will be used by the serialized classes as their
  // serialVersionUID value.  Any time a field is removed or its name
  // is changed in one of the serialized classes, this value must be
  // incremented to prevent startup problems with the API local database
  //
  // Serialized Class List (10/07/2008)
  //    * com.mes.api.ApiRequestLogEntry
  //    * com.mes.api.TridentApiProfile
  //    * child:  com.mes.api.TridentApiTransaction
  //      parent: com.mes.api.TridentApiTranBase
  //
  public static final long ApiVersionId = 6L;

  /**
   * The host API transactions are sent to by default.
   */
  public static enum ApiHost {
    TRIDENT("TR"),
    SKYLINE("SK");

    private String code;

    ApiHost(String code) {
      this.code = code;
    }

    /** @return The assigned short code for this api host. */
    public String getCode() {
      return code;
    }

    /**
     * Gets the appropriate enum for the code provided.<br />
     * Throws {@link IllegalArgumentException} if the code provided is not a valid enum code.
     * @param code
     * @return
     */
    public static ApiHost getEnum(String code) {
      if (code == null)
        throw new IllegalArgumentException();
      for (ApiHost v : values())
        if (code.equalsIgnoreCase(v.getCode())) return v;
      throw new IllegalArgumentException();
    }
  }

  /**
   * The Account Data Source Code is used in the Visa D request to indicate the card's method of entry.
   * @author brice
   */
  public static enum AccountDataSource {
    A("A"), D("D"), G("G"), H("H"), P("P"), Q("Q"), R("R"), S("S"), T("T"), X("X"), AT("@"), W("W"), Z("Z");

    private String code;

    AccountDataSource(String code) {
      this.code = code;
    }

    /** @return The single-digit code for this account data source. */
    public String getCode() {
      return code;
    }

    /**
     * Fetches the Enum associated with the provided code.<br />
     * Returns null if the code does not match any known enum.
     * @param code
     * @return
     */
    public static AccountDataSource getEnum(String code) {
      if (code == null) return null;
      for (AccountDataSource v : values())
        if (code.equalsIgnoreCase(v.getCode())) return v;
      return null;
    }
  }

  // developer ID passed to the auth switch for identification
  public static final String AUTH_DEVELOPER_ID = "TRIAPI";
  public static final String VOICE_AUTH_DEVELOPER_ID = "VCAUTH";

  // path the the local BDB (SleepyCat)
  protected static final String API_DATA_DIR = "./webapps-data";
  protected static final String API_APP_NAME = "mes-api";

  // BDB (SleepyCat) API Table Name
  public static final String API_TABLE_CAPTURE = "trident_capture_api";
  public static final String API_TABLE_PREAUTH = "trident_preauth_api";
  public static final String API_TABLE_AN_PREAUTH = "trident_preauth_auth_net";
  public static final String API_TABLE_PROFILE = "trident_profile";
  public static final String API_TABLE_API_REQUEST = "trident_api_request";
  public static final String API_TABLE_PARAMS = "trident_api_params";
  public static final String API_TABLE_STIP = "trident_api_stip";
  public static final String API_TABLE_EDNA = "trident_edna_request";
  public static final String API_TABLE_BML_ENTRIES = "trident_bml_order_entries";
  public static final String API_TABLE_ACH_TRANSIT_NUMS
          = "trident_ach_transit_nums";

  // secondary index into bml order entries table
  public static final String API_SI_BML_DATE_ENTRIES
          = "trident_bml_entries_by_date";
  public static final String API_SI_ACH_TN_REC_IDS = "trident_ach_tn_by_rec_id";

  // Emulation Constants
  public static final int EMU_TPG = 0;
  public static final int EMU_AUTHORIZE_NET = 1;

  // response error codes
  public static final String ER_NONE = "000";
  public static final String ER_TRIDENT_ENCRYPTION_ERROR = "081";
  public static final String ER_INVALID_PROFILE = "101";
  public static final String ER_INVALID_REQUEST = "102";
  public static final String ER_PURCHASE_ID_LENGTH = "103";
  public static final String ER_REFERENCE_NUMBER_LENGTH = "104";
  public static final String ER_AVS_ADDRESS_LENGTH = "105";
  public static final String ER_AVS_ZIP_LENGTH = "106";
  public static final String ER_MERCHANT_NAME_LENGTH = "107";
  public static final String ER_MERCHANT_CITY_LENGTH = "108";
  public static final String ER_MERCHANT_STATE_LENGTH = "109";
  public static final String ER_MERCHANT_ZIP_LENGTH = "110";
  public static final String ER_SIC_CODE_LENGTH = "111";
  public static final String ER_MERCHANT_PHONE_LENGTH = "112";
  public static final String ER_REFERENCE_NUMBER_VALUE = "113";
  public static final String ER_MISSING_CARD_DATA = "114";
  public static final String ER_INVALID_CARD_NUMBER = "115";
  public static final String ER_CREDITS_NOT_ALLOWED = "116";
  public static final String ER_CT_NOT_ACCEPTED = "117";
  public static final String ER_CURRENCY_NOT_ACCEPTED = "118";
  public static final String ER_RETRY_ID_LENGTH = "119";
  public static final String ER_PURCHASE_ID_REQUIRED = "120";
  public static final String ER_MOTO_ECOMM_IND_LENGTH = "121";
  public static final String ER_NON_USD_OFFLINE_NOT_SUPPORTED = "122";
  public static final String ER_CLIENT_REFERENCE_NUMBER_LENGTH = "123";
  public static final String ER_DM_CONTACT_INFO_LENGTH = "126";
  public static final String ER_INVALID_LINE_ITEM_DATA = "127";
  public static final String ER_INVALID_MERCHANT_TAX_ID = "128";
  public static final String ER_INVALID_CUSTOMER_TAX_ID = "129";
  public static final String ER_INVALID_SUMMARY_COMMODITY_CODE = "130";
  public static final String ER_INVALID_SHIP_TO_ZIP = "131";
  public static final String ER_INVALID_SHIP_FROM_ZIP = "132";
  public static final String ER_INVALID_DEST_COUNTRY_CODE = "133";
  public static final String ER_INVALID_VAT_INVOICE_NUMBER = "134";
  public static final String ER_INVALID_ALTERNATE_TAX_IND = "135";
  public static final String ER_INVALID_LEVEL_III_TRAN_TYPE = "136";
  public static final String ER_INVALID_DISCOUNT_AMOUNT = "137";
  public static final String ER_INVALID_DUTY_AMOUNT = "138";
  public static final String ER_INVALID_ORDER_DATE = "139";
  public static final String ER_INVALID_VAT_AMOUNT = "140";
  public static final String ER_INVALID_VAT_RATE = "141";
  public static final String ER_INVALID_ALT_TAX_AMOUNT = "142";
  public static final String ER_INVALID_LINE_ITEM_COUNT = "143";
  public static final String ER_LEVEL_III_CARD_TYPE_MISMATCH = "144";
  public static final String ER_CASH_ADVANCE_NOT_SUPPORTED = "145";
  public static final String ER_INVALID_SHIP_TO_FIRST_NAME = "146";
  public static final String ER_INVALID_SHIP_TO_LAST_NAME = "147";
  public static final String ER_INVALID_SHIP_TO_ADDRESS = "148";
  public static final String ER_INVALID_SHIP_TO_PHONE = "149";
  public static final String ER_INVALID_CARDHOLDER_FIRST_NAME = "150";
  public static final String ER_INVALID_CARDHOLDER_LAST_NAME = "151";
  public static final String ER_INVALID_CARDHOLDER_EMAIL = "152";
  public static final String ER_INVALID_CARDHOLDER_PHONE = "153";
  public static final String ER_INVALID_CUSTOMER_HOST_NAME = "154";
  public static final String ER_INVALID_CUSTOMER_BROWSER_TYPE = "155";
  public static final String ER_INVALID_CUSTOMER_ANI = "156";
  public static final String ER_INVALID_CUSTOMER_ANI_II = "157";
  public static final String ER_INVALID_PRODUCT_SKU = "158";
  public static final String ER_INVALID_SHIPPING_METHOD = "159";
  public static final String ER_INVALID_DEVELOPER_ID = "160";
  public static final String ER_INVALID_REQUESTER_NAME = "161";
  public static final String ER_INVALID_CARDHOLDER_REF_NUM = "162";
  public static final String ER_HTTP_GET_NOT_ALLOWED = "163";
  public static final String ER_INVALID_CURRENCY_FORMAT = "164";
  public static final String ER_INVALID_SHIPPING_AMOUNT = "165";
  public static final String ER_INVALID_RECURRING_COUNT = "166";
  public static final String ER_INVALID_RECURRING_NUM = "167";
  public static final String ER_INVALID_INDUSTRY_CODE = "168";
  public static final String ER_INVALID_TRAN_ID = "201";
  public static final String ER_INVALID_AMOUNT = "202";
  public static final String ER_VOID_FAILED = "203";
  public static final String ER_VOID_TRAN_SETTLED = "204";
  public static final String ER_VOID_ALREADY = "205";
  public static final String ER_REFUNDED_ALREADY = "206";
  public static final String ER_REFUND_FAILED = "207";
  public static final String ER_MISSING_AUTH_RESPONSE = "208";
  public static final String ER_INVALID_TAX_AMOUNT = "209";
  public static final String ER_AVS_RESULT_DECLINE = "210";
  public static final String ER_CVV2_RESULT_DECLINE = "211";
  public static final String ER_INVALID_REFUND_AMOUNT = "212";
  public static final String ER_NOT_REFUND_ELIGIBLE = "213";
  public static final String ER_INVALID_CARD_DATA = "214";
  public static final String ER_INVALID_CARD_ID = "215";
  public static final String ER_CARD_DATA_LOAD_FAILURE = "216";
  public static final String ER_CARD_DATA_STORE_FAILURE = "217";
  public static final String ER_CARD_ID_NOT_ALLOWED = "218";
  public static final String ER_AUTH_CODE_REQUIRED = "219";
  public static final String ER_CARD_DATA_DELETE_FAILURE = "220";
  public static final String ER_CARD_DATA_DOES_NOT_EXIST = "221";
  public static final String ER_CARD_ID_REQUIRED = "222";
  public static final String ER_RETRY_ID_LOOKUP_FAILED = "223";
  public static final String ER_INVALID_RATE_ID = "224";
  public static final String ER_RATE_TABLE_EXPIRED = "225";
  public static final String ER_FX_RATE_LOOKUP_FAILED = "226";
  public static final String ER_RATE_ID_REQUIRED = "227";
  public static final String ER_RATE_CALC_INVALID = "228";
  public static final String ER_CURRENCY_LOOKUP_FAILED = "229";
  public static final String ER_FX_TRAN_POST_FAILED = "230";
  public static final String ER_FX_AMOUNT_REQUIRED = "231";
  public static final String ER_FX_NOT_SUPPORTED = "232";
  public static final String ER_FX_CURRENCY_CODE_MISMATCH = "233";
  public static final String ER_DEBIT_REQUIRES_SWIPE = "234";
  public static final String ER_DEBIT_TRAN_TYPE_NOT_ALLOWED = "235";
  public static final String ER_DEBIT_NON_USD_NOT_SUPPORTED = "236";
  public static final String ER_BATCH_CLOSE_FAILED = "237";
  public static final String ER_DUPLICATE_BATCH = "238";
  public static final String ER_INVALID_RETRY_ID = "239";
  public static final String ER_FX_MULTI_SETTLE_NOT_ALLOWED = "240";
  public static final String ER_PARTIAL_REFUND_NOT_SUPPORTED = "241";
  public static final String ER_PARTIAL_CAPTURE_NOT_SUPPORTED = "242";
  public static final String ER_FRAUD_REVIEW = "243";
  public static final String ER_FRAUD_DENY = "244";
  public static final String ER_FRAUD_ERROR = "245";
  public static final String ER_PV_CAPTURE_FAILED = "300";
  public static final String ER_PV_VOID_FAILED = "301";
  public static final String ER_PV_REFUND_FAILED = "302";
  public static final String ER_PV_CARD_VERIFY_NOT_SUPPORTED = "303";
  public static final String ER_PV_REVERSAL_FAILED = "304";
  public static final String ER_PV_CREDIT_FAILED = "305";
  public static final String ER_PV_FORCE_FAILED = "306";
  public static final String ER_PV_REQUEST_FAILED = "307";
  public static final String ER_ADYEN_REFUSED = "350";
  public static final String ER_ADYEN_ERROR = "351";
  public static final String ER_ADYEN_CAPTURE_FAILED = "352";
  public static final String ER_ADYEN_REFUND_FAILED = "353";
  public static final String ER_ADYEN_TRAN_TYPE_NOT_SUPPORTED = "354";
  public static final String ER_ADYEN_CREDIT_FAILED = "355";
  public static final String ER_ADYEN_REQUEST_FAILED = "356";
  public static final String ER_ADYEN_VOID_FAILED = "357";
  public static final String ER_GTS_ERROR = "381";
  public static final String ER_GTS_CAPTURE_FAILED = "382";
  public static final String ER_GTS_REFUND_FAILED = "383";
  public static final String ER_GTS_TRAN_TYPE_NOT_SUPPORTED = "384";
  public static final String ER_GTS_CREDIT_FAILED = "385";
  public static final String ER_GTS_REQUEST_FAILED = "386";
  public static final String ER_GTS_VOID_FAILED = "387";
  public static final String ER_ITPS_ERROR = "391";
  public static final String ER_ITPS_CAPTURE_FAILED = "392";
  public static final String ER_ITPS_REFUND_FAILED = "393";
  public static final String ER_ITPS_TRAN_TYPE_NOT_SUPPORTED = "394";
  public static final String ER_ITPS_CREDIT_FAILED = "395";
  public static final String ER_ITPS_REQUEST_FAILED = "396";
  public static final String ER_ITPS_VOID_FAILED = "397";
  public static final String ER_3D_ENROLL_CHECK_FAILED = "400";
  public static final String ER_3D_VERIFY_FAILED = "401";
  public static final String ER_REAUTH_NOT_ELIGIBLE = "411";
  public static final String ER_ORIGINAL_AUTH_LOAD_FAILED = "412";
  public static final String ER_ORIGINAL_TRAN_LOAD_FAILED = "413";
  public static final String ER_BRASPAG_ERROR = "420";
  // Errors related to travel fields
  public static final String ER_TRAVEL_INVALID_CHECKOUT = "430";

  public static final String ER_BML_REQUEST_FAILED = "500";
  public static final String ER_BML_INVALID_REQUEST = "501";
  public static final String ER_BML_PROMO_CODE_REQUIRED = "502";
  public static final String ER_BML_BILL_FIRST_NAME_REQUIRED = "503";
  public static final String ER_BML_BILL_LAST_NAME_REQUIRED = "504";
  public static final String ER_BML_BILL_ADDR1_REQUIRED = "505";
  public static final String ER_BML_BILL_CITY_REQUIRED = "506";
  public static final String ER_BML_BILL_STATE_REQUIRED = "507";
  public static final String ER_BML_BILL_ZIP_REQUIRED = "508";
  public static final String ER_BML_BILL_PHONE_REQUIRED = "509";
  public static final String ER_BML_SHIP_FIRST_NAME_REQUIRED = "510";
  public static final String ER_BML_SHIP_LAST_NAME_REQUIRED = "511";
  public static final String ER_BML_SHIP_ADDR1_REQUIRED = "512";
  public static final String ER_BML_SHIP_CITY_REQUIRED = "513";
  public static final String ER_BML_SHIP_STATE_REQUIRED = "514";
  public static final String ER_BML_SHIP_ZIP_REQUIRED = "515";
  public static final String ER_BML_SHIP_PHONE_REQUIRED = "516";
  public static final String ER_BML_SHIP_AMOUNT_REQUIRED = "517";
  public static final String ER_BML_AMOUNT_REQUIRED = "518";
  public static final String ER_BML_ORDER_NUM_REQUIRED = "519";
  public static final String ER_BML_LOOKUP_TRAN_ID_REQUIRED = "520";
  public static final String ER_BML_PA_RESP_REQUIRED = "521";
  public static final String ER_BML_NOT_ENABLED = "522";
  public static final String ER_BML_INVALID_PROMO_CODE = "523";
  public static final String ER_BML_ACCT_OR_TRAN_ID_REQUIRED = "524";
  public static final String ER_BML_CUST_REG_DATE_REQUIRED = "525";
  public static final String ER_BML_CUST_FLAG_REQUIRED = "526";
  public static final String ER_BML_CAT_CODE_REQUIRED = "527";
  public static final String ER_BML_TRAN_MODE_REQUIRED = "528";
  public static final String ER_BML_TERMS_REQUIRED = "529";
  public static final String ER_BML_ORDER_ID_REQUIRED = "530";
  public static final String ER_BML_ECOMM_EMAIL_REQUIRED = "531";
  public static final String ER_BML_ECOMM_IP_ADDR_REQUIRED = "532";
  public static final String ER_BML_AUTH_NOT_FOUND = "533";
  public static final String ER_BML_CAP_NOT_FOUND = "534";
  public static final String ER_BML_CAP_UNAVAILABLE = "535";
  public static final String ER_BML_REF_TRAN_ID_REQUIRED = "536";
  public static final String ER_BML_INVALID_REF_TRAN_ID = "537";
  public static final String ER_BML_SALE_CAP_INVALID = "538";
  public static final String ER_BML_SALE_REAUTH_INVALID = "539";

  public static final String ER_BML_CARDINAL_ERROR = "600";

  public static final String ER_BML_PROCESSOR_ERROR = "700";

  public static final String ER_ACH_REQUEST_FAILED = "800";
  public static final String ER_ACH_REQUEST_TYPE_INVALID = "801";
  public static final String ER_ACH_AUTH_TYPE_INVALID = "802";
  public static final String ER_ACH_TRANSIT_NUM_INVALID = "803";
  public static final String ER_ACH_ACCOUNT_NUM_INVALID = "804";
  public static final String ER_ACH_ACCOUNT_TYPE_INVALID = "805";
  public static final String ER_ACH_AMOUNT_INVALID = "806";
  public static final String ER_ACH_RECUR_FLAG_INVALID = "807";
  public static final String ER_ACH_TRAN_SOURCE_INVALID = "808";
  public static final String ER_ACH_AUTH_TYPE_DISABLED = "809";
  public static final String ER_ACH_IP_ADDRESS_MISSING = "810";
  public static final String ER_ACH_IP_ADDRESS_INVALID = "811";
  public static final String ER_ACH_CUST_NAME_INVALID = "812";
  public static final String ER_ACH_CUST_ID_INVALID = "813";
  public static final String ER_ACH_ACCOUNT_ID_INVALID = "814";
  public static final String ER_ACH_ACCOUNT_ID_DUPLICATE = "815";

  public static final String ER_SYNC_NOT_AVAILABLE = "901";
  public static final String ER_INTERNAL_ERROR = "999";

  // Visa D response codes
  public static final String RESP_APPROVED = "00";
  public static final String RESP_PARTIAL_APPROVAL = "10";
  public static final String RESP_SERV_NOT_ALLOWED = "57";
  public static final String RESP_NO_REASON_TO_DECLINE = "85";

  public static final String AUTH_SRC_TRIDENT = "7";
  public static final String AUTH_SRC_OFFLINE_MANUAL_ENTRY = "E";

  public static final String[][] ErrorDescriptionDefaults =
          {
                  //----------------------------------------------------------------------------------------------------------
                  //    Error Code                          Error Description
                  //----------------------------------------------------------------------------------------------------------
                  {ER_NONE, ""},
                  {ER_TRIDENT_ENCRYPTION_ERROR, "Encryption Error"},
                  {ER_INVALID_PROFILE, "Invalid Profile"},
                  {ER_INVALID_REQUEST, "Incomplete Request"},
                  {ER_PURCHASE_ID_LENGTH, "Invoice Number Length Error"},
                  {ER_REFERENCE_NUMBER_LENGTH, "Reference Number Length Error"},
                  {ER_REFERENCE_NUMBER_VALUE, "Reference Number Must Be Numeric"},
                  {ER_MISSING_CARD_DATA, "Missing Card Holder Account Data"},
                  {ER_AVS_ADDRESS_LENGTH, "AVS Address Length Error"},
                  {ER_AVS_ZIP_LENGTH, "AVS Zip Length Error"},
                  {ER_MERCHANT_NAME_LENGTH, "Merchant Name Length Error"},
                  {ER_MERCHANT_CITY_LENGTH, "Merchant City Length Error"},
                  {ER_MERCHANT_STATE_LENGTH, "Merchant State Length Error"},
                  {ER_MERCHANT_ZIP_LENGTH, "Merchant Zip Length Error"},
                  {ER_SIC_CODE_LENGTH, "Merchant Category Code Length Error"},
                  {ER_MERCHANT_PHONE_LENGTH, "Merchant Phone Length Error"},
                  {ER_INVALID_TRAN_ID, "Invalid Transaction ID"},
                  {ER_INVALID_AMOUNT, "Invalid Transaction Amount"},
                  {ER_VOID_FAILED, "Unable to void transaction"},
                  {ER_VOID_TRAN_SETTLED, "Transaction Settled, Issue Credit"},
                  {ER_VOID_ALREADY, "Transaction already voided"},
                  {ER_REFUNDED_ALREADY, "Transaction already refunded"},
                  {ER_REFUND_FAILED, "Unable to refund transaction"},
                  {ER_MISSING_AUTH_RESPONSE, "Failed to receive a response from auth host"},
                  {ER_INVALID_TAX_AMOUNT, "Invalid tax amount"},
                  {ER_AVS_RESULT_DECLINE, "AVS result is declined by user"},
                  {ER_CVV2_RESULT_DECLINE, "CVV2 result is declined by user"},
                  {ER_INVALID_REFUND_AMOUNT, "Refund amount must be between zero and the original amount"},
                  {ER_NOT_REFUND_ELIGIBLE, "Only sale transactions can be refunded."},
                  {ER_INVALID_CARD_DATA, "Only one type of card data allowed per request"},
                  {ER_INVALID_CARD_ID, "Invalid Card ID"},
                  {ER_CARD_DATA_LOAD_FAILURE, "Failed to load card data, retry request"},
                  {ER_CARD_DATA_STORE_FAILURE, "Failed to store card data, retry request"},
                  {ER_CARD_ID_NOT_ALLOWED, "Card ID parameter cannot be included in this type of transaction"},
                  {ER_AUTH_CODE_REQUIRED, "Offline transactions requires an authorization code"},
                  {ER_CARD_DATA_DELETE_FAILURE, "Failed to delete card data, retry request"},
                  {ER_CARD_DATA_DOES_NOT_EXIST, "Invalid Card ID"},
                  {ER_CARD_ID_REQUIRED, "Card ID required"},
                  {ER_RETRY_ID_LOOKUP_FAILED, "Retry Request ID Lookup Failed"},
                  {ER_INVALID_CARD_NUMBER, "Invalid card number"},
                  {ER_CREDITS_NOT_ALLOWED, "Credits Not Allowed"},
                  {ER_CT_NOT_ACCEPTED, "Card Type Not Accepted"},
                  {ER_CURRENCY_NOT_ACCEPTED, "Currency Type Not Accepted"},
                  {ER_RETRY_ID_LENGTH, "Retry ID length error.  Must be 16 characters or less."},
                  {ER_PURCHASE_ID_REQUIRED, "An invoice number is required for a 3D enrollment check."},
                  {ER_MOTO_ECOMM_IND_LENGTH, "MOTO/e-Commerce Indicator length error."},
                  {ER_NON_USD_OFFLINE_NOT_SUPPORTED, "Non-USD offline transactions are not supported."},
                  {ER_CLIENT_REFERENCE_NUMBER_LENGTH, "Client Reference Number Length Error."},
                  {ER_DM_CONTACT_INFO_LENGTH, "Direct Marketing Contact Info Length Error."},
                  {ER_INVALID_LINE_ITEM_DATA, "Invalid Level III Line Item Detail."},
                  {ER_INVALID_MERCHANT_TAX_ID, "Invalid Level III Merchant Tax ID."},
                  {ER_INVALID_CUSTOMER_TAX_ID, "Invalid Level III Customer Tax ID."},
                  {ER_INVALID_SUMMARY_COMMODITY_CODE, "Invalid Level III Summary Commodity Code."},
                  {ER_INVALID_SHIP_TO_ZIP, "Invalid Level III Ship To Zip."},
                  {ER_INVALID_SHIP_FROM_ZIP, "Invalid Level III Ship From Zip."},
                  {ER_INVALID_DEST_COUNTRY_CODE, "Invalid Ship To Country Code."},
                  {ER_INVALID_VAT_INVOICE_NUMBER, "Invalid Level III VAT Invoice Number."},
                  {ER_INVALID_ALTERNATE_TAX_IND, "Invalid Level III Alternate Tax Indicator."},
                  {ER_INVALID_LEVEL_III_TRAN_TYPE, "Transaction type does not support Level III data."},
                  {ER_INVALID_DISCOUNT_AMOUNT, "Invalid Level III Discount Amount."},
                  {ER_INVALID_DUTY_AMOUNT, "Invalid Level III Duty Amount."},
                  {ER_INVALID_ORDER_DATE, "Invalid Level III Order Date."},
                  {ER_INVALID_VAT_AMOUNT, "Invalid Level III VAT Amount."},
                  {ER_INVALID_VAT_RATE, "Invalid Level III VAT Rate."},
                  {ER_INVALID_ALT_TAX_AMOUNT, "Invalid Level III Alternate Tax Amount."},
                  {ER_INVALID_LINE_ITEM_COUNT, "Invalid Level III Line Item Count."},
                  {ER_LEVEL_III_CARD_TYPE_MISMATCH, "Invalid Level III Card Type."},
                  {ER_CASH_ADVANCE_NOT_SUPPORTED, "Cash advance transactions not supported."},
                  {ER_INVALID_SHIP_TO_FIRST_NAME, "Invalid Ship To First Name"},
                  {ER_INVALID_SHIP_TO_LAST_NAME, "Invalid Ship To Last Name"},
                  {ER_INVALID_SHIP_TO_ADDRESS, "Invalid Ship To Address"},
                  {ER_INVALID_SHIP_TO_PHONE, "Invalid Ship To Phone Number"},
                  {ER_INVALID_CARDHOLDER_FIRST_NAME, "Invalid Cardholder First Name"},
                  {ER_INVALID_CARDHOLDER_LAST_NAME, "Invalid Cardholder Last Name"},
                  {ER_INVALID_CARDHOLDER_EMAIL, "Invalid Cardholder Email Address"},
                  {ER_INVALID_CARDHOLDER_PHONE, "Invalid Cardholder Phone Number"},
                  {ER_INVALID_CUSTOMER_HOST_NAME, "Invalid customer host name"},
                  {ER_INVALID_CUSTOMER_BROWSER_TYPE, "Invalid customer browser type"},
                  {ER_INVALID_CUSTOMER_ANI, "Invalid customer ANI"},
                  {ER_INVALID_CUSTOMER_ANI_II, "Invalid customer II digits"},
                  {ER_INVALID_PRODUCT_SKU, "Invalid product SKU"},
                  {ER_INVALID_SHIPPING_METHOD, "Invalid shipping method"},
                  {ER_INVALID_DEVELOPER_ID, "Invalid developer id"},
                  {ER_INVALID_REQUESTER_NAME, "Invalid requester name"},
                  {ER_INVALID_CARDHOLDER_REF_NUM, "Invalid cardholder reference number"},
                  {ER_HTTP_GET_NOT_ALLOWED, "HTTP GET is not allowed, use HTTP POST"},
                  {ER_INVALID_CURRENCY_FORMAT, "Invalid currency format"},
                  {ER_INVALID_SHIPPING_AMOUNT, "Invalid shipping amount"},
                  {ER_INVALID_RECURRING_COUNT, "Invalid recurring payment count"},
                  {ER_INVALID_RECURRING_NUM, "Invalid recurring payment number"},
                  {ER_INVALID_INDUSTRY_CODE, "Invalid industry code"},
                  {ER_INVALID_RATE_ID, "FX rate ID invalid."},
                  {ER_RATE_TABLE_EXPIRED, "FX rate has expired."},
                  {ER_FX_RATE_LOOKUP_FAILED, "FX rate lookup failed, retry request."},
                  {ER_RATE_ID_REQUIRED, "FX rate ID required for foreign currency transactions."},
                  {ER_RATE_CALC_INVALID, "Base and consumer amounts are inconsistent with the FX rate."},
                  {ER_CURRENCY_LOOKUP_FAILED, "Failed to find currency code for the requested country code."},
                  {ER_FX_TRAN_POST_FAILED, "Failed to post transaction the FX service."},
                  {ER_FX_AMOUNT_REQUIRED, "FX amount in base currency is required."},
                  {ER_FX_NOT_SUPPORTED, "FX transactions not accepted for this account."},
                  {ER_FX_CURRENCY_CODE_MISMATCH, "Request currency code must match FX rate currency code."},
                  {ER_DEBIT_REQUIRES_SWIPE, "Pin debit transactions require track 2 swipe data."},
                  {ER_DEBIT_TRAN_TYPE_NOT_ALLOWED, "Invalid pin debit transaction type."},
                  {ER_BATCH_CLOSE_FAILED, "Batch close failed"},
                  {ER_DUPLICATE_BATCH, "Quit Duplicate Batch"},
                  {ER_INVALID_RETRY_ID, "Invalid retry id"},
                  {ER_FX_MULTI_SETTLE_NOT_ALLOWED, "Multiple captures are not supported on FX transactions"},
                  {ER_PARTIAL_REFUND_NOT_SUPPORTED, "Partial refund is not supported"},
                  {ER_PARTIAL_CAPTURE_NOT_SUPPORTED, "Partial capture is not supported"},
                  {ER_FRAUD_REVIEW, "Approved by issuer, held for fraud review"},
                  {ER_FRAUD_DENY, "Fraud system recommends denying, held for review"},
                  {ER_FRAUD_ERROR, "Fraud system unavialable.  Declined due to strict fraud setting"},
                  {ER_DEBIT_NON_USD_NOT_SUPPORTED, "Non-USD pin debit transactions are not supported."},
                  {ER_PV_CAPTURE_FAILED, "Failed to capture PayVision transaction"},
                  {ER_PV_VOID_FAILED, "Failed to void PayVision transaction"},
                  {ER_PV_REFUND_FAILED, "Failed to refund PayVision transaction"},
                  {ER_PV_CARD_VERIFY_NOT_SUPPORTED, "Card Verify Not Supported"},
                  {ER_PV_REVERSAL_FAILED, "Failed to reverse PayVision authorization"},
                  {ER_PV_CREDIT_FAILED, "PayVision credit failed"},
                  {ER_PV_FORCE_FAILED, "PayVision referral authorization failed."},
                  {ER_PV_REQUEST_FAILED, "PayVision request failed"},
                  {ER_ADYEN_REFUSED, "Adyen request refused."},
                  {ER_ADYEN_ERROR, "Adyen request failed."},
                  {ER_ADYEN_CAPTURE_FAILED, "Failed to capture Adyen transaction"},
                  {ER_ADYEN_REFUND_FAILED, "Failed to refund Adyen transaction"},
                  {ER_ADYEN_TRAN_TYPE_NOT_SUPPORTED, "Transaction type not supported by Adyen"},
                  {ER_ADYEN_CREDIT_FAILED, "Adyen credit failed"},
                  {ER_ADYEN_REQUEST_FAILED, "Adyen request failed"},
                  {ER_ADYEN_VOID_FAILED, "Adyen void failed"},
                  {ER_GTS_ERROR, "GTS request failed."},
                  {ER_GTS_CAPTURE_FAILED, "Failed to capture GTS transaction"},
                  {ER_GTS_REFUND_FAILED, "Failed to refund GTS transaction"},
                  {ER_GTS_TRAN_TYPE_NOT_SUPPORTED, "Transaction type not supported by GTS"},
                  {ER_GTS_CREDIT_FAILED, "GTS credit failed"},
                  {ER_GTS_REQUEST_FAILED, "GTS request failed"},
                  {ER_GTS_VOID_FAILED, "GTS void failed"},
                  {ER_ITPS_ERROR, "ITPS request failed."},
                  {ER_ITPS_CAPTURE_FAILED, "Failed to capture ITPS transaction"},
                  {ER_ITPS_REFUND_FAILED, "Failed to refund ITPS transaction"},
                  {ER_ITPS_TRAN_TYPE_NOT_SUPPORTED, "Transaction type not supported by ITPS"},
                  {ER_ITPS_CREDIT_FAILED, "ITPS credit failed"},
                  {ER_ITPS_REQUEST_FAILED, "ITPS request failed"},
                  {ER_ITPS_VOID_FAILED, "ITPS void failed"},
                  {ER_3D_ENROLL_CHECK_FAILED, "VBV/MSC Enrollment Check Failed."},
                  {ER_3D_VERIFY_FAILED, "VBV/MSC Verification Failed."},
                  {ER_REAUTH_NOT_ELIGIBLE, "Re-auth failed. Transaction already re-authorized in full."},
                  {ER_ORIGINAL_AUTH_LOAD_FAILED, "Failed to load declined auth data"},
                  {ER_ORIGINAL_TRAN_LOAD_FAILED, "Failed to load captured transaction data"},
                  {ER_BRASPAG_ERROR, "Braspag request failed"},
                  {ER_TRAVEL_INVALID_CHECKOUT, "Statement begin date must be the same or before statement end date"},

                  {ER_BML_REQUEST_FAILED, "BillMeLater Request Failed."},
                  {ER_BML_INVALID_REQUEST, "Invalid BillMeLater Request."},
                  {ER_BML_PROMO_CODE_REQUIRED, "Promo Code Required."},
                  {ER_BML_BILL_FIRST_NAME_REQUIRED, "Billing First Name Required."},
                  {ER_BML_BILL_LAST_NAME_REQUIRED, "Billing Last Name Required."},
                  {ER_BML_BILL_ADDR1_REQUIRED, "Billing Address Required."},
                  {ER_BML_BILL_CITY_REQUIRED, "Billing City Required."},
                  {ER_BML_BILL_STATE_REQUIRED, "Billing State Required."},
                  {ER_BML_BILL_ZIP_REQUIRED, "Billing Zip Required."},
                  {ER_BML_BILL_PHONE_REQUIRED, "Billing Phone Required."},
                  {ER_BML_SHIP_FIRST_NAME_REQUIRED, "Shipping First Name Required."},
                  {ER_BML_SHIP_LAST_NAME_REQUIRED, "Shipping Last Name Required."},
                  {ER_BML_SHIP_ADDR1_REQUIRED, "Shipping Address Required."},
                  {ER_BML_SHIP_CITY_REQUIRED, "Shipping City Required."},
                  {ER_BML_SHIP_STATE_REQUIRED, "Shipping State Required."},
                  {ER_BML_SHIP_ZIP_REQUIRED, "Shipping Zip Required."},
                  {ER_BML_SHIP_PHONE_REQUIRED, "Shipping Phone Required."},
                  {ER_BML_SHIP_AMOUNT_REQUIRED, "Shipping Amount Required."},
                  {ER_BML_AMOUNT_REQUIRED, "Amount Required."},
                  {ER_BML_ORDER_NUM_REQUIRED, "Order Number Required."},
                  {ER_BML_LOOKUP_TRAN_ID_REQUIRED, "Lookup Tran ID Required."},
                  {ER_BML_PA_RESP_REQUIRED, "PA Resp Required."},
                  {ER_BML_NOT_ENABLED, "Not Enabled."},
                  {ER_BML_INVALID_PROMO_CODE, "Invalid Promo Code."},
                  {ER_BML_ACCT_OR_TRAN_ID_REQUIRED, "Account or Ref. Transaction ID Required."},
                  {ER_BML_CUST_REG_DATE_REQUIRED, "Customer Registration Date Required."},
                  {ER_BML_CUST_FLAG_REQUIRED, "Customer Flag Required."},
                  {ER_BML_CAT_CODE_REQUIRED, "Category Code Required."},
                  {ER_BML_TRAN_MODE_REQUIRED, "Transaction Mode Required."},
                  {ER_BML_TERMS_REQUIRED, "Terms And Conditions Required."},
                  {ER_BML_ORDER_ID_REQUIRED, "Order ID Required."},
                  {ER_BML_ECOMM_EMAIL_REQUIRED, "Email Required for Ecommerce."},
                  {ER_BML_ECOMM_IP_ADDR_REQUIRED, "IP Address Required for Ecommerce."},
                  {ER_BML_AUTH_NOT_FOUND, "Authorization not found."},
                  {ER_BML_CAP_NOT_FOUND, "Capture not found."},
                  {ER_BML_CAP_UNAVAILABLE, "Capture unavailable."},
                  {ER_BML_REF_TRAN_ID_REQUIRED, "Reference Transaction ID Required."},
                  {ER_BML_INVALID_REF_TRAN_ID, "Invalid Reference Transaction ID."},
                  {ER_BML_SALE_CAP_INVALID, "Sale Capture Not Allowed."},
                  {ER_BML_SALE_REAUTH_INVALID, "Sale Reauth Not Allowed."},

                  {ER_BML_CARDINAL_ERROR, "Processor Error."},

                  {ER_BML_PROCESSOR_ERROR, "Processor Error."},

                  {ER_ACH_REQUEST_FAILED, "ACH Request Failed"},
                  {ER_ACH_REQUEST_TYPE_INVALID, "ACH Request Type Invalid"},
                  {ER_ACH_AUTH_TYPE_INVALID, "ACH Auth Type Invalid"},
                  {ER_ACH_TRANSIT_NUM_INVALID, "ACH Transit Routing Number Invalid"},
                  {ER_ACH_ACCOUNT_NUM_INVALID, "ACH Account Number Invalid"},
                  {ER_ACH_ACCOUNT_TYPE_INVALID, "ACH Account Type Invalid"},
                  {ER_ACH_AMOUNT_INVALID, "ACH Amount Invalid"},
                  {ER_ACH_RECUR_FLAG_INVALID, "ACH Recurring Flag Invalid"},
                  {ER_ACH_TRAN_SOURCE_INVALID, "ACH Transaction Source Invalid"},
                  {ER_ACH_AUTH_TYPE_DISABLED, "ACH Auth Type Disabled"},
                  {ER_ACH_IP_ADDRESS_MISSING, "ACH IP Address Required For WEB"},
                  {ER_ACH_IP_ADDRESS_INVALID, "ACH IP Address Invalid"},
                  {ER_ACH_CUST_NAME_INVALID, "ACH Customer Name Invalid"},
                  {ER_ACH_CUST_ID_INVALID, "ACH Customer ID Invalid"},
                  {ER_ACH_ACCOUNT_ID_INVALID, "ACH Account ID Invalid"},
                  {ER_ACH_ACCOUNT_ID_DUPLICATE, "ACH Account ID Duplicate"},

                  {ER_SYNC_NOT_AVAILABLE, ""},
                  {ER_INTERNAL_ERROR, "Internal Error"},
          };

  // required
  public static final String FN_TID = "profile_id";
  public static final String FN_TERM_PASS = "profile_key";
  public static final String FN_AVS_STREET = "cardholder_street_address";
  public static final String FN_AVS_ZIP = "cardholder_zip";
  public static final String FN_CVV2 = "cvv2";
  public static final String FN_AMOUNT = "transaction_amount";
  public static final String FN_CARD_NUMBER = "card_number";
  public static final String FN_CARD_NUMBER_ENC = "card_number_encrypted";
  public static final String FN_CARD_NUMBER_TRUNC = "card_number_truncated";
  public static final String FN_EXP_DATE = "card_exp_date";
  public static final String FN_TRAN_TYPE = "transaction_type";
  public static final String FN_TRAN_ID = "transaction_id";
  public static final String FN_PROD_AUTH_KEY = "production_key";
  public static final String FN_CARD_PRESENT = "card_present";
  public static final String FN_CARD_SWIPE = "card_swipe";
  public static final String FN_CARD_ID = "card_id";
  public static final String FN_TRIDENT_TRAN_ID = "trident_tran_id_num";
  public static final String FN_TRAN_DATE = "transaction_date";
  public static final String FN_ORG_TRIDENT_TRAN_ID = "org_trident_tran_id";
  public static final String FN_CARD_SWIPE_ENC = "card_swipe_encrypted";
  public static final String FN_CARD_SWIPE_KSN = "card_swipe_ksn";

  // optional
  public static final String FN_REF_NUM = "reference_number";
  public static final String FN_CLIENT_REF_NUM = "client_reference_number";
  public static final String FN_DEVELOPER_ID = "developer_id";
  public static final String FN_INTL_USD_REQUEST = "intl_usd";
  public static final String FN_CONTACTLESS = "contactless";

  // retry id
  public static final String FN_RETRY_ID = "retry_id";
  public static final String FN_RETRY_COUNT = "retry_count";

  // cardholder data
  public static final String FN_CH_FIRST_NAME = "cardholder_first_name";
  public static final String FN_CH_LAST_NAME = "cardholder_last_name";
  public static final String FN_CH_STATE = "cardholder_state";
  public static final String FN_CH_EMAIL = "cardholder_email";
  public static final String FN_CH_PHONE = "cardholder_phone";
  public static final String FN_SHIP_TO_FIRST_NAME = "ship_to_first_name";
  public static final String FN_SHIP_TO_LAST_NAME = "ship_to_last_name";
  public static final String FN_SHIP_TO_ADDRESS = "ship_to_address";
  public static final String FN_SHIP_TO_PHONE = "ship_to_phone";

  // enhanced AVS data
  public static final String FN_CUSTOMER_HOST_NAME = "customer_host_name";
  public static final String FN_CUSTOMER_BROWSER_TYPE = "customer_browser_type";
  public static final String FN_CUSTOMER_ANI = "customer_ani";
  public static final String FN_CUSTOMER_ANI_II = "customer_ani_ii";
  public static final String FN_PRODUCT_SKU = "product_sku";
  public static final String FN_SHIPPING_METHOD = "shipping_method";

  // Dynamic DBA
  public static final String FN_DBA_NAME = "merchant_name";
  public static final String FN_DBA_CITY = "merchant_city";
  public static final String FN_DBA_STATE = "merchant_state";
  public static final String FN_DBA_ZIP = "merchant_zip";
  public static final String FN_SIC = "merchant_category_code";
  public static final String FN_PHONE = "merchant_phone";
  public static final String FN_DM_CONTACT_INFO = "dm_contact_info";

  // level ii
  public static final String FN_INV_NUM = "invoice_number";
  public static final String FN_TAX = "tax_amount";
  public static final String FN_SHIP_TO_ZIP = "ship_to_zip";
  public static final String FN_DEST_COUNTRY_CODE = "dest_country_code";

  // level iii
  public static final String FN_LINE_ITEM_COUNT = "line_item_count";
  public static final String FN_MERCHANT_TAX_ID = "merchant_tax_id";
  public static final String FN_CUSTOMER_TAX_ID = "customer_tax_id";
  public static final String FN_SUMMARY_COMMODITY_CODE = "summary_commodity_code";
  public static final String FN_DISCOUNT_AMOUNT = "discount_amount";
  public static final String FN_SHIPPING_AMOUNT = "shipping_amount";
  public static final String FN_DUTY_AMOUNT = "duty_amount";
  public static final String FN_SHIP_FROM_ZIP = "ship_from_zip";
  public static final String FN_VAT_INVOICE_NUMBER = "vat_invoice_number";
  public static final String FN_ORDER_DATE = "order_date";
  public static final String FN_VAT_AMOUNT = "vat_amount";
  public static final String FN_VAT_RATE = "vat_rate";
  public static final String FN_ALT_TAX_AMOUNT = "alt_tax_amount";
  public static final String FN_ALT_TAX_AMOUNT_IND = "alt_tax_amount_indicator";
  public static final String FN_REQUESTER_NAME = "requester_name";
  public static final String FN_CARDHOLDER_REF_NUM = "cardholder_reference_number";

  // Hotel / Cruise / Car Rental
  public static final String FN_TRAVEL_CHECK_IN_DATE = "statement_date_begin";
  public static final String FN_TRAVEL_CHECK_OUT_DATE = "statement_date_end";
  public static final String FN_TRAVEL_CITY_BEGIN = "statement_city_begin";
  public static final String FN_TRAVEL_CITY_END = "statement_city_end";
  public static final String FN_TRAVEL_REGION_BEGIN = "statement_region_begin";
  public static final String FN_TRAVEL_REGION_END = "statement_region_end";
  public static final String FN_TRAVEL_COUNTRY_BEGIN = "statement_country_begin";
  public static final String FN_TRAVEL_COUNTRY_END = "statement_country_end";
  public static final String FN_TRAVEL_CHECK_IN_LOCATION = "name_of_place";
  public static final String FN_TRAVEL_CHECK_IN_RATE = "rate_daily";

  public static final String[] LevelIIIFieldNames =
          {
                  FN_LINE_ITEM_COUNT,
                  FN_MERCHANT_TAX_ID,
                  FN_CUSTOMER_TAX_ID,
                  FN_SUMMARY_COMMODITY_CODE,
                  FN_DISCOUNT_AMOUNT,
                  FN_SHIPPING_AMOUNT,
                  FN_DUTY_AMOUNT,
                  FN_SHIP_FROM_ZIP,
                  FN_VAT_INVOICE_NUMBER,
                  FN_ORDER_DATE,
                  FN_VAT_AMOUNT,
                  FN_VAT_RATE,
                  FN_ALT_TAX_AMOUNT,
                  FN_ALT_TAX_AMOUNT_IND,
                  FN_REQUESTER_NAME
          };

  // level iii line items
  public static final String FN_VISA_LINE_ITEM = "visa_line_item";
  public static final String FN_MC_LINE_ITEM = "mc_line_item";
  public static final String FN_AMEX_LINE_ITEM = "amex_line_item";
  public static final String LINE_ITEM_DETAIL_FS_REGEX = "\\<\\|\\>";

  // recurring billing
  public static final String FN_RECURRING_NUM = "recurring_pmt_num";
  public static final String FN_RECURRING_COUNT = "recurring_pmt_count";

  // pin debit
  public static final String FN_DEBIT_PIN_BLOCK = "debit_pin_block";
  public static final String FN_DEBIT_KSN = "debit_ksn";

  // optional
  public static final String FN_MOTO_IND = "moto_ecommerce_ind";
  public static final String FN_CURRENCY_CODE = "currency_code";
  public static final String FN_CURRENCY_CODE_ALPHA = "currency_code_alpha";
  public static final String FN_COUNTRY_CODE = "country_code";
  public static final String FN_COUNTRY_NAME = "country_name";

  // response control flags
  public static final String FN_RCTL_PREFIX = "rctl_";

  // inline card store request
  public static final String FN_STORE_CARD = "store_card";

  // echo field prefix fields
  public static final String FN_ECHO_PREFIX = "echo_";
  public static final String FN_ECHO_RESP_PREFIX = "eresp_";

  // force transaction
  public static final String FN_AUTH_CODE = "auth_code";

  // not implemented
  public static final String FN_INDUSTRY_CODE = "industry_code";

  // 3-d secure - verified by visa
  public static final String FN_CAVV = "cavv";
  public static final String FN_XID = "xid";

  // mastercard universal cardholder identification
  public static final String FN_UCAF_COLL_IND = "ucaf_collection_ind";
  public static final String FN_UCAF_AUTH_DATA = "ucaf_auth_data";

  // cardinal centinal fields (3-d secure)
  public static final String FN_3D_ENROLLED = "3d_enrolled";
  public static final String FN_3D_TRAN_ID = "3d_transaction_id";
  public static final String FN_3D_ORDER_ID = "3d_order_id";
  public static final String FN_3D_ACS_URL = "3d_acs_url";
  public static final String FN_3D_PAYLOAD = "3d_payload";

  // currency exchange fields
  public static final String FN_IP_ADDRESS = "ip_address";
  public static final String FN_FX_RATE_ID = "fx_rate_id";
  public static final String FN_FX_RATE = "fx_rate";
  public static final String FN_FX_RATE_TABLE = "fx_rate_table";
  public static final String FN_FX_EXP_DATE = "fx_expiration_date";
  public static final String FN_FX_AMOUNT = "fx_amount";

  // inline fraud field names
  public static final String FN_DEV_ID = "device_id";
  public static final String FN_BROWSER_LANGUAGE = "browser_language";
  public static final String FN_ACCT_NAME = "account_name";
  public static final String FN_ACCT_EMAIL = "account_email";
  public static final String FN_ACCT_CREATE_DATE = "account_creation_date";
  public static final String FN_ACCT_LAST_CHANGE = "account_last_change";
  public static final String FN_DIGITAL_GOODS = "digital_goods";
  public static final String FN_SUBSCRIPTION = "subscription";
  public static final String FN_FRAUD_SCAN = "fraud_scan";

  // response field names
  public static final String FN_ERROR_CODE = "error_code";
  public static final String FN_AUTH_RESP_TEXT = "auth_response_text";
  public static final String FN_AVS_RESULT = "avs_result";
  public static final String FN_CVV2_RESULT = "cvv2_result";
  public static final String FN_PRODUCT_LEVEL = "product_level";
  public static final String FN_COMMERCIAL_CARD = "commercial_card";
  public static final String FN_EXTENDED_AVS = "extended_avs";
  public static final String FN_ACCOUNT_BALANCE = "account_balance";
  public static final String FN_PARTIAL_AUTH = "partial_auth";
  public static final String FN_FRAUD_RESULT = "fraud_result";
  public static final String FN_FRAUD_RESULT_CODES = "fraud_result_codes";
  public static final String FN_RESP_HASH = "resp_hash";

  // request/response field values
  public static final String FV_NOT_A_MOTO = "Z";
  public static final String FV_INDUSTRY_CODE_DEFAULT = "D";
  public static final String FV_MOTO_ECOMM_DEFAULT = "7";
  public static final String FV_CARD_PRESENT_DEFAULT = "0";    // cnp
  public static final String FV_CURRENCY_CODE_USD = "840";  // USD
  public static final String FV_CURRENCY_CODE_USD_ALPHA = "USD";  // 840
  public static final String FV_CURRENCY_CODE_DEFAULT = FV_CURRENCY_CODE_USD;
  public static final String FV_CURRENCY_CODE_ALPHA_DEFAULT = FV_CURRENCY_CODE_USD_ALPHA;
  public static final String FV_COUNTRY_CODE_USD = "840";
  public static final String FV_COUNTRY_CODE_USD_ALPHA = "US";
  public static final String FV_COUNTRY_CODE_DEFAULT = FV_COUNTRY_CODE_USD_ALPHA;
  public static final String FV_ENTRY_MODE_KEYED = "01";
  public static final String FV_ENTRY_MODE_MC_ECOMMERCE = "81";
  public static final String FV_ENTRY_MODE_SWIPED = "90";
  public static final String FV_ENTRY_MODE_DEFAULT = FV_ENTRY_MODE_KEYED;

  // moto/ecommerce indicator values
  public static final String FV_MOTO_ONE_TIME_MOTO = "1";
  public static final String FV_MOTO_RECURRING_MOTO = "2";
  public static final String FV_MOTO_INSTALLMENT_PAYMENT = "3";
  public static final String FV_MOTO_SECURE_ECOMMERCE = "5";
  public static final String FV_MOTO_NON_AUTH_SECURE_ECOMMERCE = "6";
  public static final String FV_MOTO_ECOMMERCE = "7";
  public static final String FV_MOTO_NON_SECURE_ECOMMERCE = "8";

  public static final String FV_UCAF_COLL_IND_PRESENT = "2";

  // retry constants
  public static final String RETRY_ID_NONE = null;

  // rate table constants
  public static final int RATE_ID_NONE = 0;

  // maximums
  public static final double MAX_TRAN_AMOUNT = 9999999.99;
  public static final int MAX_RECURRING_COUNT = 999999;
  public static final int MAX_RECURRING_NUMBER = 999999;
  public static final double MAX_LIII_AMOUNTS = 999999.99;

  // fraud modes
  public static final int FM_NONE = 0;
  public static final int FM_OBSERVE = 1;
  public static final int FM_ACTIVE = 2;
  public static final int FM_STRICT = 3;

  // field length maximums
  public static final int FLM_REFERENCE_NUMBER = 11;
  public static final int FLM_PURCHASE_ID = 17;
  public static final int FLM_AN_PURCHASE_ID = 20;
  public static final int FLM_AVS_ADDRESS = 19;
  public static final int FLM_AVS_ZIP = 9;
  public static final int FLM_MERCHANT_NAME = 25;
  public static final int FLM_MERCHANT_CITY = 13;
  public static final int FLM_MERCHANT_STATE = 3;
  public static final int FLM_MERCHANT_ZIP = 9;
  public static final int FLM_SIC_CODE = 4;
  public static final int FLM_MERCHANT_PHONE = 10;
  public static final int FLM_DM_CONTACT_INFO = 13;
  public static final int FLM_RETRY_ID = 16;
  public static final int FLM_MOTO_ECOMM_IND = 1;
  public static final int FLM_CLIENT_REFERENCE_NUMBER = 96;
  public static final int FLM_MERCHANT_TAX_ID = 20;
  public static final int FLM_CUSTOMER_TAX_ID = 20;
  public static final int FLM_SUMMARY_COMMODITY_CODE = 4;
  public static final int FLM_SHIP_TO_ZIP_AMEX = 9;
  public static final int FLM_SHIP_TO_ZIP = 10;
  public static final int FLM_SHIP_FROM_ZIP = 10;
  public static final int FLM_DEST_COUNTRY_CODE = 3;
  public static final int FLM_VAT_INVOICE_NUMBER = 15;
  public static final int FLM_ALTERNATE_TAX_IND = 1;
  public static final int FLM_CARDHOLDER_FIRST_NAME = 15;
  public static final int FLM_CARDHOLDER_LAST_NAME = 30;
  public static final int FLM_CARDHOLDER_EMAIL = 60;
  public static final int FLM_CARDHOLDER_PHONE = 13;
  public static final int FLM_SHIP_TO_FIRST_NAME = 15;
  public static final int FLM_SHIP_TO_LAST_NAME = 30;
  public static final int FLM_SHIP_TO_ADDRESS = 50;
  public static final int FLM_SHIP_TO_PHONE = 13;
  public static final int FLM_CUSTOMER_HOST_NAME = 60;
  public static final int FLM_CUSTOMER_BROWSER_TYPE = 60;
  public static final int FLM_CUSTOMER_ANI = 15;
  public static final int FLM_CUSTOMER_ANI_II = 2;
  public static final int FLM_PRODUCT_SKU = 15;
  public static final int FLM_SHIPPING_METHOD = 2;
  public static final int FLM_DEVELOPER_ID = 6;
  public static final int FLM_REQUESTER_NAME = 38;
  public static final int FLM_CARDHOLDER_REF_NUM = 17;
  public static final int FLM_TRAVEL_CITY = 18;
  public static final int FLM_TRAVEL_REGION = 3;
  public static final int FLM_TRAVEL_COUNTRY = 3;

  // Authorize.net Emulation
  public static final String FN_AN_VERSION = "x_version";
  public static final String FN_AN_TID = "x_login";
  public static final String FN_AN_TERM_PASS = "x_tran_key";
  public static final String FN_AN_PASSWORD = "x_password";
  public static final String FN_AN_AMOUNT = "x_amount";
  public static final String FN_AN_CARD_NUMBER = "x_card_num";
  public static final String FN_AN_EXP_DATE = "x_exp_date";
  public static final String FN_AN_TRAN_TYPE = "x_type";
  public static final String FN_AN_TRAN_ID = "x_trans_id";
  public static final String FN_AN_AUTH_CODE = "x_auth_code";
  public static final String FN_AN_TAX = "x_tax";
  public static final String FN_AN_CVV2 = "x_card_code";
  public static final String FN_AN_TEST_FLAG = "x_test_request";
  public static final String FN_AN_CURRENCY_CODE = "x_currency_code";
  public static final String FN_AN_INV_NUM = "x_invoice_num";
  public static final String FN_AN_PO_NUM = "x_po_num";
  public static final String FN_AN_DELIM_CHAR = "x_delim_char";
  public static final String FN_AN_ENCAP_CHAR = "x_encap_char";
  public static final String FN_AN_AVS_STREET = "x_address";
  public static final String FN_AN_AVS_ZIP = "x_zip";

  public static final String FN_AN_TAX_EXEMPT = "x_tax_exempt";
  public static final String FN_AN_FREIGHT = "x_freight";
  public static final String FN_AN_DUTY = "x_duty";
  public static final String FN_AN_METHOD = "x_method";
  public static final String FN_AN_FIRST_NAME = "x_first_name";
  public static final String FN_AN_LAST_NAME = "x_last_name";
  public static final String FN_AN_COMPANY = "x_company";
  public static final String FN_AN_CITY = "x_city";
  public static final String FN_AN_STATE = "x_state";
  public static final String FN_AN_COUNTRY = "x_country";
  public static final String FN_AN_PHONE = "x_phone";
  public static final String FN_AN_FAX = "x_fax";
  public static final String FN_AN_CUST_ID = "x_cust_id";
  public static final String FN_AN_EMAIL = "x_email";
  public static final String FN_AN_DESC = "x_description";
  public static final String FN_AN_SHIP_TO_FIRST_NAME = "x_ship_to_first_name";
  public static final String FN_AN_SHIP_TO_LAST_NAME = "x_ship_to_last_name";
  public static final String FN_AN_SHIP_TO_COMPANY = "x_ship_to_company";
  public static final String FN_AN_SHIP_TO_ADDRESS = "x_ship_to_address";
  public static final String FN_AN_SHIP_TO_CITY = "x_ship_to_city";
  public static final String FN_AN_SHIP_TO_STATE = "x_ship_to_state";
  public static final String FN_AN_SHIP_TO_ZIP = "x_ship_to_zip";
  public static final String FN_AN_SHIP_TO_COUNTRY = "x_ship_to_country";

  // Authorize.net response fields
  public static final String FN_RESPONSE_CODE = "Response Code";
  public static final String FN_RESPONSE_SUBCODE = "Response Subcode";
  public static final String FN_RESPONSE_REASON_CODE = "Response Reason Code";
  public static final String FN_RESPONSE_REASON_TEXT = "Response Reason Text";
  public static final String FN_RSP_APPROVAL_CODE = "Approval Code";
  public static final String FN_RSP_AVS_RESULT_CODE = "AVS Result Code";
  public static final String FN_RSP_TRANSACTION_ID = "Transaction ID";
  public static final String FN_RSP_INVOICE_NUMBER = "Invoice Number";
  public static final String FN_RSP_DESCRIPTION = "Description";
  public static final String FN_RSP_AMOUNT = "Amount";
  public static final String FN_RSP_METHOD = "Method";
  public static final String FN_RSP_TRANSACTION_TYPE = "Transaction Type";
  public static final String FN_RSP_CUSTOMER_ID = "Customer ID";
  public static final String FN_RSP_CARDHOLDER_FIRST_NAME = "Cardholder First Name";
  public static final String FN_RSP_CARDHOLDER_LAST_NAME = "Cardholder Last Name";
  public static final String FN_RSP_COMPANY = "Company";
  public static final String FN_RSP_BILLING_ADDDRESS = "Billing Address";
  public static final String FN_RSP_CITY = "City";
  public static final String FN_RSP_STATE = "State";
  public static final String FN_RSP_ZIP = "Zip";
  public static final String FN_RSP_COUNTRY = "Country";
  public static final String FN_RSP_PHONE = "Phone";
  public static final String FN_RSP_FAX = "Fax";
  public static final String FN_RSP_EMAIL = "Email";
  public static final String FN_RSP_SHIP_TO_FIRST_NAME = "Ship To First Name";
  public static final String FN_RSP_SHIP_TO_LAST_NAME = "Ship To Last Name";
  public static final String FN_RSP_SHIP_TO_COMPANY = "Ship To Company";
  public static final String FN_RSP_SHIP_TO_ADDRESS = "Ship To Address";
  public static final String FN_RSP_SHIP_TO_CITY = "Ship To City";
  public static final String FN_RSP_SHIP_TO_STATE = "Ship To State";
  public static final String FN_RSP_SHIP_TO_ZIP = "Ship To Zip";
  public static final String FN_RSP_SHIP_TO_COUNTRY = "Ship To Country";
  public static final String FN_RSP_TAX_AMOUNT = "Tax Amount";
  public static final String FN_RSP_DUTY_AMOUNT = "Duty Amount";
  public static final String FN_RSP_FREIGHT_AMOUNT = "Freight Amount";
  public static final String FN_RSP_TAX_EXEMPT_FLAG = "Tax Exempt Flag";
  public static final String FN_RSP_PO_NUMBER = "PO Number";
  public static final String FN_RSP_MD5_HASH = "MD5 Hash";
  public static final String FN_RSP_CVV2_RESPONSE_CODE = "CVV2 Response Code";
  public static final String FN_RSP_CAVV_RESPONSE_CODE = "CAVV Response Code";

  // Authorize.net response codes
  public static final String RC_AN_APPROVED = "1";
  public static final String RC_AN_DECLINED = "2";
  public static final String RC_AN_ERROR = "3";

  // Authorize.net error code emulation
  public static final String ER_AN_NONE = "1";
  public static final String ER_AN_DECLINE = "2";
  public static final String ER_AN_INVALID_AMOUNT = "5";
  public static final String ER_AN_INVALID_CARD_NUMBER = "6";
  public static final String ER_AN_INVALID_EXP_DATE = "7";
  public static final String ER_AN_EXPIRED_CARD = "8";
  public static final String ER_AN_ABA_INVALID = "9";
  public static final String ER_AN_ACCOUNT_INVALID = "10";
  public static final String ER_AN_DUPLICATE = "11";
  public static final String ER_AN_AUTH_CODE_REQUIRED = "12";
  public static final String ER_AN_INACTIVE_ACCOUNT = "13";
  public static final String ER_AN_INVALID_REFERRER = "14";
  public static final String ER_AN_INVALID_TRAN_ID = "15";
  public static final String ER_AN_CT_NOT_ACCEPTED = "17";
  public static final String ER_AN_ACH_NOT_ACCEPTED = "18";
  public static final String ER_AN_INTERNAL_ERROR = "19";
  public static final String ER_AN_AVS_MISMATCH = "27";
  public static final String ER_AN_INVALID_PROFILE_KEY = "32";
  public static final String ER_AN_BLANK_FIELD = "33";
  public static final String ER_AN_INVALID_CVV2 = "44";
  public static final String ER_AN_INVALID_SETTLE_AMOUNT = "47";
  public static final String ER_AN_AMOUNT_TOO_LARGE = "49";
  public static final String ER_AN_INVALID_CREDIT = "54";
  public static final String ER_AN_BAD_VERSION = "68";
  public static final String ER_AN_INVALID_METHOD = "70";
  public static final String ER_AN_INVALID_DUTY = "74";
  public static final String ER_AN_INVALID_FREIGHT = "75";
  public static final String ER_AN_INVALID_TAX_AMOUNT = "76";
  public static final String ER_AN_INVALID_TAX_ID = "77";
  public static final String ER_AN_INVALID_CVV2_FORMAT = "78";
  public static final String ER_AN_INVALID_DL = "79";
  public static final String ER_AN_INVALID_DL_STATE = "80";
  public static final String ER_AN_INVALID_SHIP_STATE = "94";
  public static final String ER_AN_INVALID_STATE = "95";
  public static final String ER_AN_INVALID_BUYER_COUNTRY = "96";
  public static final String ER_AN_VOID_ALREADY = "310";
  public static final String ER_AN_INVALID_AVS = "901";

  public static final String[][] AuthNetResponseDescriptions =
          {
                  // error code                     response code     message text
                  {ER_AN_NONE, RC_AN_APPROVED, "This transaction has been approved."},
                  {ER_AN_DECLINE, RC_AN_DECLINED, "This transaction has been declined."},
                  {ER_AN_INVALID_AMOUNT, RC_AN_ERROR, "A valid amount is required."},
                  {ER_AN_INVALID_CARD_NUMBER, RC_AN_ERROR, "The credit card number is invalid."},
                  {ER_AN_INVALID_EXP_DATE, RC_AN_ERROR, "Credit card expiration date is invalid."},
                  {ER_AN_EXPIRED_CARD, RC_AN_ERROR, "The credit card has expired."},
                  {ER_AN_ABA_INVALID, RC_AN_ERROR, "The ABA code is invalid"},
                  {ER_AN_ACCOUNT_INVALID, RC_AN_ERROR, "The account number is invalid"},
                  {ER_AN_DUPLICATE, RC_AN_ERROR, "A duplicate transaction has been submitted."},
                  {ER_AN_AUTH_CODE_REQUIRED, RC_AN_ERROR, "An authorization code is required but not present."},
                  {ER_AN_INACTIVE_ACCOUNT, RC_AN_ERROR, "The merchant login ID or password is invalid or the account is inactive."},
                  {ER_AN_INVALID_REFERRER, RC_AN_ERROR, "The referrer or relay response URL is invalid."},
                  {ER_AN_INVALID_TRAN_ID, RC_AN_ERROR, "The transaction ID is invalid or not present."},
                  {ER_AN_CT_NOT_ACCEPTED, RC_AN_ERROR, "The merchant does not accept this type of credit card."},
                  {ER_AN_ACH_NOT_ACCEPTED, RC_AN_ERROR, "ACH transactions are not accepted by this merchant."},
                  {ER_AN_INTERNAL_ERROR, RC_AN_ERROR, "An error occurred during processing.  Please try again."},
                  {ER_AN_AVS_MISMATCH, RC_AN_ERROR, "The transaction resulted in an AVS mismatch. The address provided does not match billing address of cardholder."},
                  {ER_AN_INVALID_AVS, RC_AN_ERROR, "Invalid AVS address or zip data. Correct AVS data and reenter transaction."},
                  {ER_AN_INVALID_PROFILE_KEY, RC_AN_ERROR, "The merchant password is invalid or not present."},
                  {ER_AN_BLANK_FIELD, RC_AN_ERROR, "Field cannot be left blank."},
                  {ER_AN_INVALID_CVV2, RC_AN_ERROR, "This transaction has been declined."},
                  {ER_AN_INVALID_SETTLE_AMOUNT, RC_AN_ERROR, "The amount requested for settlement cannot be greater than the original amount authorized."},
                  {ER_AN_AMOUNT_TOO_LARGE, RC_AN_ERROR, "The transaction amount submitted was greater than the maximum amount allowed."},
                  {ER_AN_INVALID_CREDIT, RC_AN_ERROR, "The referenced transaction does not meet the criteria for issuing a credit."},
                  {ER_AN_BAD_VERSION, RC_AN_ERROR, "The version parameter is invalid"},
                  {ER_AN_INVALID_METHOD, RC_AN_ERROR, "The transaction method is invalid."},
                  {ER_AN_INVALID_DUTY, RC_AN_ERROR, "The duty amount is invalid."},
                  {ER_AN_INVALID_FREIGHT, RC_AN_ERROR, "The freight amount is invalid."},
                  {ER_AN_INVALID_TAX_AMOUNT, RC_AN_ERROR, "The tax amount is invalid."},
                  {ER_AN_INVALID_TAX_ID, RC_AN_ERROR, "The SSN or tax ID is invalid."},
                  {ER_AN_INVALID_CVV2_FORMAT, RC_AN_ERROR, "The card code is invalid."},
                  {ER_AN_INVALID_DL, RC_AN_ERROR, "The driver's license number is invalid."},
                  {ER_AN_INVALID_DL_STATE, RC_AN_ERROR, "The driver's license state is invalid."},
                  {ER_AN_INVALID_SHIP_STATE, RC_AN_ERROR, "The shipping state or country is invalid."},
                  {ER_AN_INVALID_STATE, RC_AN_ERROR, "A valid state is required."},
                  {ER_AN_INVALID_BUYER_COUNTRY, RC_AN_ERROR, "This country is not authorized for buyers."},
                  {ER_AN_VOID_ALREADY, RC_AN_ERROR, "This transaction has already been voided."},
          };

  public static final String[][] VisakRespCodeToAuthNetRespCodeMap =
          {
                  // visak    authorize.net
                  {"01", ER_AN_DECLINE},
                  {"02", ER_AN_DECLINE},
                  {"03", ER_AN_INVALID_PROFILE_KEY},
                  {"04", ER_AN_DECLINE},
                  {"05", ER_AN_DECLINE},
                  {"06", ER_AN_DECLINE},
                  {"07", ER_AN_DECLINE},
                  {"12", ER_AN_DECLINE},
                  {"13", ER_AN_INVALID_AMOUNT},
                  {"14", ER_AN_INVALID_CARD_NUMBER},
                  {"15", ER_AN_INVALID_CARD_NUMBER},
                  {"19", ER_AN_INTERNAL_ERROR},
                  {"21", ER_AN_INTERNAL_ERROR},
                  {"41", ER_AN_DECLINE},
                  {"43", ER_AN_DECLINE},
                  {"51", ER_AN_DECLINE},
                  {"54", ER_AN_EXPIRED_CARD},
                  {"57", ER_AN_DECLINE},
                  {"58", ER_AN_DECLINE},
                  {"62", ER_AN_DECLINE},
                  {"63", ER_AN_DECLINE},
                  {"65", ER_AN_DECLINE},
                  {"76", ER_AN_DECLINE},
                  {"77", ER_AN_INVALID_AMOUNT},
                  {"78", ER_AN_INVALID_CARD_NUMBER},
                  {"79", ER_AN_DECLINE},
                  {"80", ER_AN_DECLINE},
                  {"82", ER_AN_INVALID_CVV2},
                  {"85", ER_AN_DECLINE},
                  {"91", ER_AN_INTERNAL_ERROR},
                  {"92", ER_AN_INVALID_CARD_NUMBER},
                  {"93", ER_AN_DECLINE},
                  {"94", ER_AN_DUPLICATE},
                  {"96", ER_AN_INTERNAL_ERROR},
                  {"N7", ER_AN_INVALID_CVV2},
                  {"R1", ER_AN_DECLINE},
                  {"TA", ER_AN_CT_NOT_ACCEPTED},
                  {"TB", ER_AN_INVALID_EXP_DATE},
                  {"TC", ER_AN_INVALID_AVS},
                  {"TD", ER_AN_DECLINE},
                  {"TE", ER_AN_INVALID_PROFILE_KEY},
          };

  public static final String[][] ApiErrorCodeToAuthNetRespCodeMap =
          {
                  // tpg      authorize.net
                  {ER_NONE, ER_AN_NONE},
                  {ER_INVALID_PROFILE, ER_AN_INACTIVE_ACCOUNT},
                  {ER_INVALID_REQUEST, ER_AN_ACCOUNT_INVALID},
                  {ER_PURCHASE_ID_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_REFERENCE_NUMBER_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_AVS_ADDRESS_LENGTH, ER_AN_INVALID_AVS},
                  {ER_AVS_ZIP_LENGTH, ER_AN_INVALID_AVS},
                  {ER_MERCHANT_NAME_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_MERCHANT_CITY_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_MERCHANT_STATE_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_MERCHANT_ZIP_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_SIC_CODE_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_MERCHANT_PHONE_LENGTH, ER_AN_INTERNAL_ERROR},
                  {ER_REFERENCE_NUMBER_VALUE, ER_AN_INTERNAL_ERROR},
                  {ER_MISSING_CARD_DATA, ER_AN_INVALID_CARD_NUMBER},
                  {ER_INVALID_CARD_NUMBER, ER_AN_INVALID_CARD_NUMBER},
                  {ER_CREDITS_NOT_ALLOWED, ER_AN_INTERNAL_ERROR},
                  {ER_CT_NOT_ACCEPTED, ER_AN_CT_NOT_ACCEPTED},
                  {ER_INVALID_TRAN_ID, ER_AN_INVALID_TRAN_ID},
                  {ER_INVALID_AMOUNT, ER_AN_INVALID_AMOUNT},
                  {ER_VOID_FAILED, ER_AN_INTERNAL_ERROR},
                  {ER_VOID_TRAN_SETTLED, ER_AN_INTERNAL_ERROR},
                  {ER_VOID_ALREADY, ER_AN_VOID_ALREADY},
                  {ER_REFUNDED_ALREADY, ER_AN_INTERNAL_ERROR},
                  {ER_REFUND_FAILED, ER_AN_INTERNAL_ERROR},
                  {ER_INVALID_REFUND_AMOUNT, ER_AN_INVALID_CREDIT},
                  {ER_PV_CAPTURE_FAILED, ER_AN_INTERNAL_ERROR},
                  {ER_PV_VOID_FAILED, ER_AN_INTERNAL_ERROR},
                  {ER_PV_REFUND_FAILED, ER_AN_INTERNAL_ERROR},
                  {ER_PV_CARD_VERIFY_NOT_SUPPORTED, ER_AN_INTERNAL_ERROR},
                  {ER_SYNC_NOT_AVAILABLE, ER_AN_INTERNAL_ERROR},
                  {ER_INTERNAL_ERROR, ER_AN_INTERNAL_ERROR},
                  {ER_INVALID_TAX_AMOUNT, ER_AN_INVALID_TAX_AMOUNT},
          };

  // visad transaction codes
  public static String VISAD_TC_PURCHASE = "54";
  public static String VISAD_TC_CASH_ADVANCE = "55";
  public static String VISAD_TC_CNP_PURCHASE = "56";
  public static String VISAD_TC_CARD_VERIFY = "58";
  public static String VISAD_TC_REVERSAL = "59";
  public static String VISAD_TC_DEBIT_PURCHASE = "93";

  // Authorization Network Settings
  // internal host settings using the port forwarding from a rwc desktop
  public  static String       DESKTOP_NETWORK         = "10.1.1.";
  public  static String       DESKTOP_HOST_SIMULATOR  = "https://test.merchante-solutions.com/trident/txn";
  public  static String       DESKTOP_HOST_PRODUCTION = "https://api.merchante-solutions.com/trident/txn";
  public  static String       DESKTOP_PORT_SIMULATOR  = "";   // unused
  public  static String       DESKTOP_PORT_PRODUCTION = "";

  // PayVision Constants
  public static final int PAYVISION_SUCCESS = 0;

  public static final String PAYVISION_ENDPOINT_TEST = null;
  public static final String PAYVISION_BASIC_PATH = "/Gateway/BasicOperations.asmx";
  public static final String PAYVISION_3D_SECURE_PATH = "/Gateway/ThreeDSecureOperations.asmx";


  public static final int PAYVISION_ACCT_TYPE_ECOMMERCE = 1;
  public static final int PAYVISION_ACCT_TYPE_MOTO = 2;
  public static final int PAYVISION_ACCT_TYPE_RETAIL = 3;
  public static final int PAYVISION_ACCT_TYPE_RECURRING = 4;

  // Adyen Constants
  public static final String ADYEN_ENDPOINT_TEST = "https://pal-test.adyen.com/pal/servlet/soap/Payment";

  // Braspag
  public static final String BRASPAG_ENDPOINT_TEST = "https://homologacao.pagador.com.br/webservice/pagadorTransaction.asmx";

  // GTS Constants
  public static final int GTS_SUCCESS = 0;

  public static final String GTS_ENDPOINT_TEST = "https://transactions-test.rtpay.com/TxWS/ATPayTxWS.svc";

  // ITPS Constants
  public static final String ITPS_ENDPOINT_TEST = null;

  // EDNA Constants
  public static final String EDNA_ENDPOINT_TEST = null;
  public static final String EDNA_CREDENTIALS = null;

  public static final String EDNA_RESP_DENY = "DENY";
  public static final String EDNA_RESP_REVIEW = "MANUAL_REVIEW";
  public static final String EDNA_RESP_ERROR = "ERROR";

  // toccata transaction types
  public static final int TTT_AUTH = 101;
  public static final int TTT_CAPTURE = 103;
  public static final int TTT_PARTIAL_CAPTURE_CLOSE = 105;
  public static final int TTT_REFUND = 106;
  public static final int TTT_VOID = 108;
  public static final int TTT_RECURRING_CAPTURE = 109;
  public static final int TTT_CREDIT = 111;

  public static final String CENTINEL_URL_TEST = "https://centineltest.cardinalcommerce.com/maps/txns.asp";
  public static final String CENTINEL_MESSAGE_VERSION = "1.7";
  public static final int CENTINEL_TIMEOUT_CONNECT = 30000;// milliseconds
  public static final int CENTINEL_TIMEOUT_READ = 30000;// milliseconds
  public static final String CENTINEL_ERROR_NONE = "0";
  public static final String CENTINEL_PROCESSOR_ID = "240";

  // Properties
  // PMG-394
  public static final String PropertiesFilename = "trident-api.properties";

  public static final String PROP_PRODUCTION_SERVER = "production.server";

  public static final String PROP_MES_HOST_NAME = "com.mes.hostname";
  public static final String PROP_TRIDENT_HOST = "trident.host";
  public static final String PROP_TRIDENT_HOST_BACKUP = "trident.host.backup";
  public static final String PROP_SKYLINE_HOST_NAME = "skyline.host";
  public static final String PROP_TRIDENT_PORT = "trident.port";
  public static final String PROP_TRIDENT_PORT_BACKUP = "trident.port.backup";
  public static final String PROP_BACKUP_HOST_ENABLED = "trident.backup.enabled";
  public static final String PROP_PAYVISION_ENDPOINT = "payvision.endpoint";
  public static final String PROP_CENTINEL_URL = "cardinal.centinel.url";
  public static final String PROP_INTL_3D_SECURE_ENABLED = "intl.3d.secure.enabled";
  public static final String PROP_FX_VALIDATIONS_ENABLED = "fx.validations.enabled";
  public static final String PROP_ADYEN_ENDPOINT = "adyen.endpoint";
  public static final String PROP_BRASPAG_ENDPOINT = "braspag.endpoint";
  public static final String PROP_EDNA_ENDPOINT = "edna.endpoint";
  public static final String PROP_GTS_ENDPOINT = "gts.endpoint";
  public static final String PROP_ITPS_ENDPOINT = "itps.endpoint";
  public static final String PROP_GET_ALLOWED_GLOBAL = "com.mes.allowget";
  public static final String PROP_INTERNAL_NETWORK = "internal.network";
  public static final String PROP_TEST = "com.mes.test";
  public static final String PROP_JUNIT = "com.mes.junit";
  public static final String PROP_JUNIT_RECORDING = "com.mes.junitRecording";
  public static final String PROP_USE_SNI = "jsse.enableSNIExtension";


  // TSYS Cobol encoding of signed numbers
  public final static char[][] CobolPosValueEncoding =
          {
                  {'0', '{'},
                  {'1', 'A'},
                  {'2', 'B'},
                  {'3', 'C'},
                  {'4', 'D'},
                  {'5', 'E'},
                  {'6', 'F'},
                  {'7', 'G'},
                  {'8', 'H'},
                  {'9', 'I'},
          };

  public final static char[][] CobolNegValueEncoding =
          {
                  {'0', '}'},
                  {'1', 'J'},
                  {'2', 'K'},
                  {'3', 'L'},
                  {'4', 'M'},
                  {'5', 'N'},
                  {'6', 'O'},
                  {'7', 'P'},
                  {'8', 'Q'},
                  {'9', 'R'},
          };

  /** Supported API Industry Codes */
  public static final HashMap IndustryCodes =
          new HashMap() {
            {
              put("A", "Auto Rental");
//      put("B" ,"Bank/Financial Institution");
              put("D", "Direct Marketing");
//      put("F" ,"Food/Restaurant");
//      put("G" ,"Grocery Store/Supermarket");
              put("H", "Hotel");
//      put("L" ,"Limited Amount Terminal");
            }
          };

  // Address Verification (AVS) and CVV2 response mapping for Adyen
  public static final HashMap AdyenToTridentAvsMap =
          new HashMap() {
            {
              put("0", "U");
              put("1", "A");
              put("2", "N");
              put("3", "U");
              put("4", "U");
              put("5", "0");
              put("6", "Z");
              put("7", "Y");
              put("8", "N");
              put("9", "A");
              put("10", "N");
              put("11", "N");
              put("12", "A");
              put("13", "N");
              put("14", "Z");
              put("15", "Z");
              put("16", "N");
              put("17", "N");
              put("18", "0");
            }
          };

  public static final HashMap AdyenToTridentCvv2Map =
          new HashMap() {
            {
              put("0", "P");
              put("1", "M");
              put("2", "N");
              put("3", "P");
              put("4", "N");
              put("5", "U");
              put("6", "P");
            }
          };

  public static String apiErrorCodeToAuthNetResponseCode(String ecode) {
    String retVal = ER_AN_INTERNAL_ERROR;
    for (int i = 0; i < ApiErrorCodeToAuthNetRespCodeMap.length; ++i) {
      if (ApiErrorCodeToAuthNetRespCodeMap[i][0].equals(ecode)) {
        retVal = ApiErrorCodeToAuthNetRespCodeMap[i][1];
        break;
      }
    }
    return (retVal);
  }

  public static String getApiDatabasePath() {
    StringBuffer retVal = new StringBuffer(API_DATA_DIR);
    String serverId = null;

    // add the application path and version to path string
    retVal.append(java.io.File.separator);
    retVal.append(API_APP_NAME);
    retVal.append(java.io.File.separator);
    retVal.append("v");
    retVal.append(String.valueOf(ApiVersionId));

    return (retVal.toString());
  }

  public static String getAuthNetReasonText(String rcode) {
    return (getAuthNetReasonText(rcode, ""));
  }

  public static String getAuthNetReasonText(String rcode, String defaultValue) {
    String retVal = defaultValue;
    for (int i = 0; i < AuthNetResponseDescriptions.length; ++i) {
      if (AuthNetResponseDescriptions[i][0].equals(rcode)) {
        retVal = AuthNetResponseDescriptions[i][2];
        break;
      }
    }
    return (retVal);
  }

  public static String getAuthNetResponseCode(String rcode) {
    return (getAuthNetResponseCode(rcode, RC_AN_ERROR));
  }

  public static String getAuthNetResponseCode(String rcode, String defaultValue) {
    String retVal = defaultValue;
    for (int i = 0; i < AuthNetResponseDescriptions.length; ++i) {
      if (AuthNetResponseDescriptions[i][0].equals(rcode)) {
        retVal = AuthNetResponseDescriptions[i][1];
        break;
      }
    }
    return (retVal);
  }

  public static String getErrorDesc(String ecode) {
    String retVal = "";
    for (int i = 0; i < ErrorDescriptionDefaults.length; ++i) {
      if (ErrorDescriptionDefaults[i][0].equals(ecode)) {
        retVal = ErrorDescriptionDefaults[i][1];
        break;
      }
    }
    return (retVal);
  }

  public static String visakResponseCodeToAuthNetResponseCode(String visakValue) {
    String retVal = ER_AN_DECLINE;    // generic decline

    for (int i = 0; i < VisakRespCodeToAuthNetRespCodeMap.length; ++i) {
      if (VisakRespCodeToAuthNetRespCodeMap[i][0].equals(visakValue)) {
        retVal = VisakRespCodeToAuthNetRespCodeMap[i][1];
        break;
      }
    }
    return (retVal);
  }

//  public static void main( String[] args )
//  {
//    System.out.println( TridentApiConstants.getApiDatabasePath() );
//  }
}
