/*************************************************************************
 * 
 * FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/ApiAMLineItemDetailRecord.java $
 * 
 * Last Modified By : $Author: jfirman $
 * Last Modified Date : $LastChangedDate: 2011-02-13 13:48:37 -0800 (Sun, 13 Feb 2011) $
 * Version : $Revision: 18402 $
 * 
 * Change History:
 * See SVN database
 * 
 * Copyright (C) 2007-2010,2011 by Merchant e-Solutions Inc.
 * All rights reserved, Unauthorized distribution prohibited.
 * 
 * This document contains information which is the proprietary
 * property of Merchant e-Solutions, Inc. This document is received in
 * confidence and its contents may not be disclosed without the
 * prior written consent of Merchant e-Solutions, Inc.
 * 
 **************************************************************************/
package com.mes.api;

import org.apache.commons.lang3.StringUtils;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import dto.AmexLineItem;

public class ApiAMLineItemDetailRecord implements ApiLineItemDetailRecord {
	protected String ChargeDescription = null;
	protected String ChargeItemQuantity = null;
	protected String ChargeItemAmount = null;

	public ApiAMLineItemDetailRecord() {
	}

	public ApiAMLineItemDetailRecord(String rawData, Object bo) {
		super();
		setRawLineItemData(rawData, bo);
	}

	public String getChargeDescription() {
		return ChargeDescription;
	}

	public String getChargeItemQuantity() {
		return ChargeItemQuantity;
	}

	public String getChargeItemAmount() {
		return ChargeItemAmount;
	}

	public void setChargeDescription(String chargeDesc) {
		ChargeDescription = chargeDesc;
	}

	public void setChargeItemQuantity(String itemCount) {
		ChargeItemQuantity = itemCount;
	}

	public void setChargeItemAmount(String amount) {
		ChargeItemAmount = amount;
	}

	public void setRawLineItemData(String rawData, Object bo) {
		String[] items = null;

		AmexLineItem amexBO = (AmexLineItem) bo;
		
		if (StringUtils.isBlank(rawData)) {
			items = new String[3];
		}
		else
			items = (rawData+" ").split(TridentApiConstants.LINE_ITEM_DETAIL_FS_REGEX);

		setChargeDescription(StringUtilities.leftJustify(StringUtilities.dataSwap(items[0], amexBO.getItemDescription()), 40, ' '));
		setChargeItemQuantity(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[1], amexBO.getItemQuantity())), "000"));
		setChargeItemAmount(NumberFormatter.getDoubleString(Double.parseDouble(StringUtilities.dataSwap(items[2], amexBO.getExtendedItemAmount())), "000000000000"));
	}


	@Override
	public void setRawLineItemData(String rawData) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
