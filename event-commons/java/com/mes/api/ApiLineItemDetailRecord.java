/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/ApiLineItemDetailRecord.java $

  Description:
  
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-08-26 15:25:43 -0700 (Wed, 26 Aug 2009) $
  Version            : $Revision: 16429 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.api;

public interface ApiLineItemDetailRecord
{
  public void setRawLineItemData( String rawData ) throws Exception;
  public void setRawLineItemData( String rawData,  Object obj ) throws Exception;
}