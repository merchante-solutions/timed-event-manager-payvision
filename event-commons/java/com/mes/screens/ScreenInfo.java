/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/screens/ScreenInfo.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 12/08/00 9:58a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.screens;

public class ScreenInfo
{
  public static final long  MAX_SCREENS     = 0x8000000000000000L;
  
  private long              screenId;
  private Class             beanClass;
  private String            screenJsp;
  private String            screenDescription;
  private boolean           oneTime;
  private boolean           completed;
  private boolean           skipOver;
  private String            beanClassName;
  /*
  ** CONSTRUCTOR ScreenInfo
  */
  public ScreenInfo(String className, 
                    long id, 
                    String jsp, 
                    boolean onlyOnce,
                    String desc)
  {
    beanClass = null;
    try
    {
      if (className != null)
      {
        beanClass = Class.forName(className);
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "ScreenInfo(): " + e.toString());
    }
    
    screenId          = id;
    screenJsp         = jsp;
    oneTime           = onlyOnce;
    screenDescription = desc;
    completed         = false;
    skipOver          = false;
    beanClassName     = className;
  }
  
  /*
  ** METHOD newBeanInstance
  */
  public Object newBeanInstance()
  {
    try
    {
      if (beanClass != null)
      {
        return beanClass.newInstance();
      }
    }
    catch (Exception e)
    {
    }
  
    return null;
  }

  /*
  ** METHOD getBeanClass
  */
  public Class getBeanClass()
  {
    return beanClass;
  }

  /*
  ** METHOD getBeanClassName
  */
  public String getBeanClassName()
  {
    return beanClassName;
  }

  /*
  ** METHOD getJspName
  */
  public String getJspName()
  {
    return screenJsp;
  }

  /*
  ** METHOD getScreenId
  */
  public long getScreenId()
  {
    return screenId;
  }

  /*
  ** METHOD getOneTime
  */
  public boolean getOneTime()
  {
    return oneTime;
  }

  /*
  ** METHOD getSkipOver
  */
  public boolean getSkipOver()
  {
    return skipOver;
  }

  /*
  ** METHOD setSkipOver
  */
  public void setSkipOver(boolean skipOver)
  {
    this.skipOver = skipOver;
  }

  /*
  ** METHOD isVisitable
  */
  public boolean isVisitable()
  {
    return (!oneTime && !skipOver);
  }

  /*
  ** METHOD setCompleted
  */
  public void setCompleted(boolean compVal)
  {
    completed = compVal;
  }

  /*
  ** METHOD getCompleted
  */
  public boolean getCompleted()
  {
    return completed;
  }
  
  /*
  ** METHOD getScreenDescription
  */
  public String getScreenDescription()
  {
    return this.screenDescription;
  }
  
  /*
  ** METHOD isCompleted
  **
  ** Same as getCompleted
  */
  public boolean isCompleted()
  {
    return this.completed;
  }
}