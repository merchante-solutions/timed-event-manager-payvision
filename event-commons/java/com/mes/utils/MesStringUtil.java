package com.mes.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import masthead.util.StringUtil;

public class MesStringUtil {

	private static final String MASK = "******";

	public static String getPaddedString(String st, int len, String fill) {
		if (st.length() < len) {
			while (st.length() < len) {
				st += fill;
			}
		}
		return st;
	}

	public static String sanitize(String targetData, String apiTargetRegex) {
		String sanitizedFormData = null;
		if (!StringUtil.isEmpty(targetData)) {
			sanitizedFormData = targetData;
			Pattern p = Pattern.compile(apiTargetRegex);
			Matcher m = p.matcher(sanitizedFormData);
			while (m.find()) {
				sanitizedFormData = sanitizedFormData.replace(m.group().substring(13, m.group().length() - 1), MASK);
			}
		}
		return sanitizedFormData;
	}

}
