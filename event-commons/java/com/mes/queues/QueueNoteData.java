/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueNoteData.java $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import java.sql.Timestamp;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;

public class QueueNoteData
{
  private long    id          = 0;
  private int     type        = 0;
  private String  noteDate    = "";
  private String  source      = "";
  private String  note        = "";
  private String  description = "";
  
  public QueueNoteData()
  {
  }
  
  public void setData(ResultSet rs)
  {
    try
    {
      setId(rs.getLong("id"));
      setType(rs.getInt("type"));
      setNoteDate(rs.getTimestamp("note_date"));
      setSource(rs.getString("source"));
      setNote(rs.getString("note"));
      setDescription(rs.getString("description"));
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setData()", e.toString());
    }
  }
  
  public void setId(String id)
  {
    try
    {
      setId(Long.parseLong(id));
    }
    catch(Exception e)
    {
    }
  }
  public void setId(long id)
  {
    this.id = id;
  }
  public long getId()
  {
    return this.id;
  }
  
  public void setType(String type)
  {
    try
    {
      setType(Integer.parseInt(type));
    }
    catch(Exception e)
    {
    }
  }
  public void setType(int type)
  {
    this.type = type;
  }
  public int getType()
  {
    return this.type;
  }
  
  public void setNoteDate(Timestamp noteDate)
  {
    try
    {
      this.noteDate = DateTimeFormatter.getFormattedDate(noteDate, "MM/dd/yy hh:mm:ss a");
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setNoteDate()", e.toString());
    }
  }
  public String getNoteDate()
  {
    return this.noteDate;
  }
  
  public void setSource(String source)
  {
    this.source = source;
  }
  public String getSource()
  {
    return this.source;
  }
  
  public void setNote(String note)
  {
    this.note = note;
  }
  public String getNote()
  {
    return this.note;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return this.description;
  }
}
