/*@lineinfo:filename=QueueLogger*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueLogger.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/31/03 11:46a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import com.mes.user.UserQueueLockItem;

public class QueueLogger extends SQLJConnectionBase
{
  // possible actions to log
  public static final int   QLA_ADD     = 1;
  public static final int   QLA_DELETE  = 2;
  public static final int   QLA_COPY    = 3;
  public static final int   QLA_MOVE    = 4;
  
  private QueueLogger()
  {
  }
  
  /*
  ** private String getQueueDescription(int type) throws Exception
  **
  ** Gets a queues description from the q_types table using the type id.
  **
  ** RETURNS: String description of a queue.
  */
  private String getQueueDescription(int type) throws Exception
  {
    String description;
    /*@lineinfo:generated-code*//*@lineinfo:50^5*/

//  ************************************************************
//  #sql [Ctx] { select  description
//        
//        from    q_types
//        where   type = :type
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description\n       \n      from    q_types\n      where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.QueueLogger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   description = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:56^5*/
    return description;
  }
  
  /*
  ** private void logAction(long id, int action, int oldType, 
  **   int newType, UserBean user) throws Exception
  **
  ** Logs queue activity.
  */
  private void logAction(long id, int action, int oldType, 
    int newType, UserBean user) throws Exception
  {
    String progress = "top of method";
  
    String userName = "SYSTEM";
    try
    {
      userName = user.getLoginName();
    }
    catch (Exception e) {}
    
    try
    {
      connect();
      
      progress = "getting Queue descriptions";
      String description;
      switch (action)
      {
        case QLA_ADD:
          description = "Item " + id + " created in " 
            + getQueueDescription(newType) + " (" + newType + ")";
          break;
        
        case QLA_DELETE:
          description = "Item " + id + " deleted from " 
            + getQueueDescription(oldType) + " (" + oldType + ")";
          break;
        
        case QLA_COPY:
          description = "Item " + id + " copied from " 
            + getQueueDescription(oldType) + " (" + oldType + ") to "
            + getQueueDescription(newType) + " (" + newType + ")";
          break;
        
        case QLA_MOVE:
          description = "Item " + id + " moved from " 
            + getQueueDescription(oldType) + " (" + oldType + ") to "
            + getQueueDescription(newType) + " (" + newType + ")";
          break;
        
        default:
          throw new Exception("Invalid queue log action given");
      }
      
      progress = "getting log sequence";
      long logSequence = 0L;
      
      /*@lineinfo:generated-code*//*@lineinfo:115^7*/

//  ************************************************************
//  #sql [Ctx] { select  q_log_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  q_log_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.QueueLogger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   logSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^7*/
    
      progress = "inserting into q_log";
      /*@lineinfo:generated-code*//*@lineinfo:123^7*/

//  ************************************************************
//  #sql [Ctx] { insert into q_log
//          ( log_seq_num,
//            id,
//            log_date,
//            action,
//            old_type,
//            new_type,
//            description,
//            user_name )
//          values
//          ( :logSequence,
//            :id,
//            sysdate,
//            :action,
//            :oldType,
//            :newType,
//            :description,
//            :userName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_log\n        ( log_seq_num,\n          id,\n          log_date,\n          action,\n          old_type,\n          new_type,\n          description,\n          user_name )\n        values\n        (  :1 ,\n           :2 ,\n          sysdate,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.queues.QueueLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,logSequence);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,action);
   __sJT_st.setInt(4,oldType);
   __sJT_st.setInt(5,newType);
   __sJT_st.setString(6,description);
   __sJT_st.setString(7,userName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^7*/
      
      // try to record how long the user had this item locked
      progress = "recording q_log_lock_times";
      if(user != null)
      {
        UserQueueLockItem qli = user.getQueueLockData();
        
        if(qli != null)
        {
          // check to see if this queue item id and type match
          if(id == qli.id && oldType == qli.type)
          {
            // if there's a match, log the time it was locked
            /*@lineinfo:generated-code*//*@lineinfo:157^13*/

//  ************************************************************
//  #sql [Ctx] { insert into q_log_lock_times
//                (
//                  id,
//                  type,
//                  log_seq_num,
//                  login_name,
//                  lock_time,
//                  complete_time
//                )
//                values
//                (
//                  :qli.id,
//                  :qli.type,
//                  :logSequence,
//                  :user.getLoginName(),
//                  :qli.lockDate,
//                  sysdate
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_516 = user.getLoginName();
   String theSqlTS = "insert into q_log_lock_times\n              (\n                id,\n                type,\n                log_seq_num,\n                login_name,\n                lock_time,\n                complete_time\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                sysdate\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.queues.QueueLogger",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qli.id);
   __sJT_st.setInt(2,qli.type);
   __sJT_st.setLong(3,logSequence);
   __sJT_st.setString(4,__sJT_516);
   __sJT_st.setTimestamp(5,qli.lockDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^13*/
          }
        }
      }
    }
    catch (Exception e)
    {
      logEntry("logAction(id=" + id + ",action=" + action + ",old=" 
        + oldType + ",new=" + newType + "user=" + userName + ")",progress + ": " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** public static void log(long id, int action, int oldType, 
  **   int newType, UserBean user) throws Exception
  **
  ** Entry point for logging.
  */
  public static void log(long id, int action, int oldType, 
    int newType, UserBean user) throws Exception
  {
    (new QueueLogger()).logAction(id,action,oldType,newType,user);
  }
  public static void log(long id, int action, int oldType, 
    int newType) throws Exception
  {
    (new QueueLogger()).logAction(id,action,oldType,newType,null);
  }
}/*@lineinfo:generated-code*/