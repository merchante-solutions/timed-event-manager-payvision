/*@lineinfo:filename=QueueNotes*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueNotes.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-02-13 13:48:37 -0800 (Sun, 13 Feb 2011) $
  Version            : $Revision: 18402 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class QueueNotes extends SQLJConnectionBase
{
  private Vector    queueNotes      = new Vector();
  private long      appSeqNum       = -1L;
  private long      merchantNumber  = -1L;
  private long      noteId          = 0L;
  private int       noteType        = 0;
  private int       itemType        = 0;
  private String    noteSource      = "";
  private String    note            = "";
  private boolean   submitted       = false;
  
  public QueueNotes()
  {
  }
  
  public static void copyNotes( long oldId, int newType, long newId )
  {
    (new QueueNotes())._copyNotes(oldId,newType,newId);
  }
  
  public void _copyNotes( long oldId, int newType, long newId )
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] { insert into q_notes
//          (
//            id,
//            type,
//            note_date,
//            note_source,
//            note,
//            item_type
//          )
//          select  :newId,
//                  :newType,
//                  note_date,
//                  note_source,
//                  substr(('Copied from ' || :oldId || ': ' || note),1,500),
//                  item_type
//          from    q_notes
//          where   id = :oldId
//                  and item_type = :getItemType(newType)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_517 = getItemType(newType);
   String theSqlTS = "insert into q_notes\n        (\n          id,\n          type,\n          note_date,\n          note_source,\n          note,\n          item_type\n        )\n        select   :1 ,\n                 :2 ,\n                note_date,\n                note_source,\n                substr(('Copied from ' ||  :3  || ': ' || note),1,500),\n                item_type\n        from    q_notes\n        where   id =  :4 \n                and item_type =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newId);
   __sJT_st.setInt(2,newType);
   __sJT_st.setLong(3,oldId);
   __sJT_st.setLong(4,oldId);
   __sJT_st.setInt(5,__sJT_517);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^7*/
    }
    catch( Exception e )
    {
      logEntry("_copyNotes(" + oldId + "," + newType + "," + newId + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void loadData(long id)
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    boolean             showGroupNotes  = false;
    int                 countGroups     = 0;
    int                 groupType       = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(group_type)
//          
//          from    Q_GROUP_TO_TYPES
//          where   queue_type = :this.noteType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(group_type)\n         \n        from    Q_GROUP_TO_TYPES\n        where   queue_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,this.noteType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   countGroups = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^7*/
      
      showGroupNotes = (countGroups > 0);

      if(showGroupNotes)
      {
        // get group type -- use first one that comes up which should be the default
        /*@lineinfo:generated-code*//*@lineinfo:116^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  group_type
//            from    q_group_to_types
//            where   queue_type = :noteType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  group_type\n          from    q_group_to_types\n          where   queue_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,noteType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.queues.QueueNotes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          groupType = rs.getInt("group_type");
        }

        rs.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:133^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  qn.id             id,
//                    qn.type           type,
//                    qn.note_date      note_date,
//                    qn.note_source    source,
//                    qn.note           note,
//                    qt.description    description
//            from    q_notes           qn,
//                    q_types           qt,
//                    q_group_to_types  qtt
//            where   qn.id             = :id and
//                    qtt.group_type    = :groupType and
//                    qn.type           = qtt.queue_type and
//                    qn.type           = qt.type
//            order by qn.note_date asc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qn.id             id,\n                  qn.type           type,\n                  qn.note_date      note_date,\n                  qn.note_source    source,\n                  qn.note           note,\n                  qt.description    description\n          from    q_notes           qn,\n                  q_types           qt,\n                  q_group_to_types  qtt\n          where   qn.id             =  :1  and\n                  qtt.group_type    =  :2  and\n                  qn.type           = qtt.queue_type and\n                  qn.type           = qt.type\n          order by qn.note_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,groupType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.queues.QueueNotes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:153^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  qn.id           id,
//                    qn.type         type,
//                    qn.note_date    note_date,
//                    qn.note_source  source,
//                    qn.note         note,
//                    qt.description  description
//            from    q_notes         qn,
//                    q_types         qt
//            where   qn.id         = :id and
//                    qn.item_type  = :this.itemType and
//                    qn.type       = qt.type
//            order by qn.note_date asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qn.id           id,\n                  qn.type         type,\n                  qn.note_date    note_date,\n                  qn.note_source  source,\n                  qn.note         note,\n                  qt.description  description\n          from    q_notes         qn,\n                  q_types         qt\n          where   qn.id         =  :1  and\n                  qn.item_type  =  :2  and\n                  qn.type       = qt.type\n          order by qn.note_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,this.itemType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.QueueNotes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^9*/
      }
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        QueueNoteData qnd = new QueueNoteData();
        qnd.setData(rs);
        
        queueNotes.add(qnd);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + id + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public void submitData()
  {
    boolean useCleanUp = false;
    
    try
    {
      // insert the new note if not blank
      if(note != null && ! note.equals(""))
      {
        if(isConnectionStale())
        {
          connect();
          useCleanUp = true;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:210^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_notes
//            (
//              id,
//              type,
//              item_type,
//              note_source,
//              note
//            )
//            values
//            (
//              :this.noteId,
//              :this.noteType,
//              :this.itemType,
//              :this.noteSource,
//              :this.note
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_notes\n          (\n            id,\n            type,\n            item_type,\n            note_source,\n            note\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.noteId);
   __sJT_st.setInt(2,this.noteType);
   __sJT_st.setInt(3,this.itemType);
   __sJT_st.setString(4,this.noteSource);
   __sJT_st.setString(5,this.note);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }
  
  public void submitCallTrackingData()
  {
    boolean useCleanUp = false;
    
    try
    {
      connect();
      
      // insert the new note if not blank
      if(note != null && ! note.equals("") && this.merchantNumber > 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:255^9*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls
//            (
//              merchant_number,
//              call_date,
//              type,
//              other_description,
//              login_name,
//              status,
//              notes,
//              billable,
//              client_generated
//            )
//            values
//            (
//              :this.merchantNumber,
//              sysdate,
//              99,
//              :fixSize(MesQueues.getItemTypeString(this.itemType), 30),
//              :this.noteSource,
//              2,
//              :fixSize(this.note,260),
//              'N',
//              'N'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_518 = fixSize(MesQueues.getItemTypeString(this.itemType), 30);
 String __sJT_519 = fixSize(this.note,260);
   String theSqlTS = "insert into service_calls\n          (\n            merchant_number,\n            call_date,\n            type,\n            other_description,\n            login_name,\n            status,\n            notes,\n            billable,\n            client_generated\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n            99,\n             :2 ,\n             :3 ,\n            2,\n             :4 ,\n            'N',\n            'N'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.queues.QueueNotes",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.merchantNumber);
   __sJT_st.setString(2,__sJT_518);
   __sJT_st.setString(3,this.noteSource);
   __sJT_st.setString(4,__sJT_519);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitCallTrackingData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public int getItemType(int type)
  {
    int     result      = 0;
    boolean useCleanUp  = false;

    try
    {
      if(type > -1)
      {
        if(isConnectionStale())
        {
          connect();
          useCleanUp = true;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:309^9*/

//  ************************************************************
//  #sql [Ctx] { select  item_type
//            
//            from    q_types
//            where   type = :type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  item_type\n           \n          from    q_types\n          where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:315^9*/
      }
    }
    catch(Exception e)
    {
      // don't log all the freaking "no rows found" crap
      //logEntry("getItemType(" + type + ")", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
    
    return result;
  }

  public static void addNote(long id, int type, String source, String note)
  {
    addNote(id, type, source, note, false);
  }

  public static void addNote(long id, int type, String source, String note, boolean addToCallTracking)
  {
    try
    {
      QueueNotes qn = new QueueNotes();
      
      qn.setId(id);
      qn.setType(type);
      qn.setOtherIds(id, qn.getItemType(type)); //QueueTools.getQueueItemType(type));
      
      qn.setNoteSource(source);
      qn.setNote(note);
      
      qn.submitData();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("addNote()", e.toString());
    }
  }


  private String fixSize(String str, int size)
  {
    if(str == null || str.equals(""))
    {
      return "";
    }
    
    if(str.length() > size)
    {
      return (str.substring(0,size)).trim();
    }
    else
    {
      return str.trim();
    }
  }


  public void setOtherIds(long id, int itmType)
  {
    boolean useCleanUp = false;

    switch(itmType)
    {
      //skip these for now
      case MesQueues.Q_ITEM_TYPE_CBT_CREDIT:
      case MesQueues.Q_ITEM_TYPE_MES_CREDIT:
      case MesQueues.Q_ITEM_TYPE_MES_MATCHCREDIT:
        //do nothing
        break;

      case MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST:  //acr id
        try
        {
          if(isConnectionStale())
          {
            connect();
            useCleanUp = true;
          }

          /*@lineinfo:generated-code*//*@lineinfo:401^11*/

//  ************************************************************
//  #sql [Ctx] { select  decode(acr.app_seq_num, null, -1, acr.app_seq_num),
//                      decode(acr.merch_number, null, -1, acr.merch_number)
//              
//              from    ACR acr
//              where   acr_seq_num = :id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(acr.app_seq_num, null, -1, acr.app_seq_num),\n                    decode(acr.merch_number, null, -1, acr.merch_number)\n             \n            from    ACR acr\n            where   acr_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.merchantNumber = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:409^11*/
        }
        catch(Exception e)
        {
          //System.out.println("set other ids: acr " + e.toString());
        }
        finally
        {
          if(useCleanUp)
          {
            cleanUp();
          }
        }

        break;

      case MesQueues.Q_ITEM_TYPE_ACH_REJECTS:    //reject_id
      case MesQueues.Q_ITEM_TYPE_RISK:           //reject_id
      case MesQueues.Q_ITEM_TYPE_COLLECTIONS:    //reject_id
      case MesQueues.Q_ITEM_TYPE_LEGAL:          //reject_id
        try
        {
          if(isConnectionStale())
          {
            connect();
            useCleanUp = true;
          }

          /*@lineinfo:generated-code*//*@lineinfo:437^11*/

//  ************************************************************
//  #sql [Ctx] { select  decode(merch.app_seq_num, null, -1, merch.app_seq_num),
//                      ach.merchant_number 
//              
//              from    ACH_REJECTS ach,
//                      merchant merch
//              where   reject_seq_num = :id and ach.merchant_number = merch.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(merch.app_seq_num, null, -1, merch.app_seq_num),\n                    ach.merchant_number \n             \n            from    ACH_REJECTS ach,\n                    merchant merch\n            where   reject_seq_num =  :1  and ach.merchant_number = merch.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.merchantNumber = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^11*/
        }
        catch(Exception e)
        {
          //System.out.println("set other ids ach,risk,collections,legal" + e.toString());
        }
        finally
        {
          if(useCleanUp)
          {
            cleanUp();
          }
        }

        break;


      case MesQueues.Q_ITEM_TYPE_ACCT_CHANGE:     //change sequence id
        try
        {
          if(isConnectionStale())
          {
            connect();
            useCleanUp = true;
          }

          /*@lineinfo:generated-code*//*@lineinfo:472^11*/

//  ************************************************************
//  #sql [Ctx] { select  decode(merch.app_seq_num, null, -1, merch.app_seq_num),
//                      acr.merchant_number 
//              
//              from    ACCOUNT_CHANGE_REQUEST acr,
//                      merchant merch
//              where   CHANGE_SEQUENCE_ID = :id and acr.merchant_number = merch.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(merch.app_seq_num, null, -1, merch.app_seq_num),\n                    acr.merchant_number \n             \n            from    ACCOUNT_CHANGE_REQUEST acr,\n                    merchant merch\n            where   CHANGE_SEQUENCE_ID =  :1  and acr.merchant_number = merch.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.merchantNumber = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^11*/
        }
        catch(Exception e)
        {
          //System.out.println("set other ids acr " +e.toString());
        }
        finally
        {
          if(useCleanUp)
          {
            cleanUp();
          }
        }

        break;

      
      case MesQueues.Q_ITEM_TYPE_PROGRAMMING:     //request id
      case MesQueues.Q_ITEM_TYPE_MMS_ERROR:       //request_id
        try
        {
          if(isConnectionStale())
          {
            connect();
            useCleanUp = true;
          }

          /*@lineinfo:generated-code*//*@lineinfo:508^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num,
//                      merch_number 
//              
//              from    mms_stage_info
//              where   request_id = :id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num,\n                    merch_number \n             \n            from    mms_stage_info\n            where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.merchantNumber = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^11*/
        }
        catch(Exception e)
        {
          //System.out.println("set other ids programming error " + e.toString());
        }
        finally
        {
          if(useCleanUp)
          {
            cleanUp();
          }
        }

        break;
       
      
      case MesQueues.Q_ITEM_TYPE_ACTIVATION:  //appseqnum
      case MesQueues.Q_ITEM_TYPE_MMS:         //appseqnum
      case MesQueues.Q_ITEM_TYPE_ACTIVATE:    //appseqnum
      case MesQueues.Q_ITEM_TYPE_DEPLOYMENT:  //appseqnum
        this.appSeqNum = id;

        try
        {
          if(isConnectionStale())
          {
            connect();
            useCleanUp = true;
          }

          /*@lineinfo:generated-code*//*@lineinfo:547^11*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number 
//              from    merchant
//              where   app_seq_num = :this.appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number  \n            from    merchant\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.queues.QueueNotes",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:552^11*/
        }
        catch(Exception e)
        {
          //System.out.println("set other ids activation mms deployement " + e.toString());
        }
        finally
        {
          if(useCleanUp)
          {
            cleanUp();
          }
        }

        break;

    }

  
  }

  
  public Vector getQueueNotes()
  {
    return this.queueNotes;
  }
  
  public void setNote(String note)
  {
    if(note != null)
    {
      this.note = note.trim();
    }
  }
  public String getNote()
  {
    return this.note;
  }
  
  public void setId(String id)
  {
    setNoteId(id);
  }
  public void setId(long id)
  {
    setNoteId(Long.toString(id));
  }
  public void setNoteId(String noteId)
  {
    try
    {
      this.noteId = Long.parseLong(noteId);
    }
    catch(Exception e)
    {
      logEntry("setNoteId(" + noteId + ")", e.toString());
    }
  }
  public long getNoteId()
  {
    return this.noteId;
  }
  
  public void setType(String type)
  {
    try
    {
      setType(Integer.parseInt(type));
    }
    catch(Exception e)
    {
      logEntry("setType(" + type + ")", e.toString());
    }
  }
  public void setType(int type)
  {
    this.noteType = type;
    this.itemType = getItemType(type); //QueueTools.getQueueItemType(type);
  }

  public void setNoteType(String noteType)
  {
    try
    {
      this.noteType = Integer.parseInt(noteType);
    }
    catch(Exception e)
    {
      logEntry("setNoteType(" + noteType + ")", e.toString());
    }
  }
  public int getNoteType()
  {
    return this.noteType;
  }
  
  public void setNoteSource(String noteSource)
  {
    this.noteSource = noteSource;
  }
  public String getNoteSource()
  {
    return this.noteSource;
  }
  
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public void setItemType(String itemType)
  {
    try
    {
      this.itemType = Integer.parseInt(itemType);
    }
    catch(Exception e)
    {
    }
  }
  public int getItemType()
  {
    return this.itemType;
  }
  
}/*@lineinfo:generated-code*/