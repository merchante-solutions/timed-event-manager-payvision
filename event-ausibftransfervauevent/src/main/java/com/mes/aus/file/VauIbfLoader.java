package com.mes.aus.file;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.SysUtil;

public class VauIbfLoader extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauIbfLoader.class);
	private InputStream in;
	private String filename;
	private String fileType;
	private long obId;

	public VauIbfLoader(InputStream in, String filename, boolean directFlag) {
		super(directFlag);
		this.in = in;
		this.filename = filename;
	}

	/**
	 * Determine outbound file id from response data. This has been changed to
	 * inspect the first detail record, so the second line of the file should
	 * now be passed in rather than the header due to the obId in the header
	 * being truncated due to short field length.
	 */
	private void inspectResponseFileData(String line) throws Exception {
		fileType = SysUtil.FT_VAU_RSP;

		// extract ob id
		String field = null;
		try {
			field = line.substring(81, 91).trim();
			obId = Long.parseLong(field);
		}
		catch (Exception e) {
			throw new RuntimeException("Cannot determine outbound file ID," + " invalid ID '" + field + "'");
		}
	}

	private void inspectReport2FileData(LineNumberReader r) throws Exception {
		// set the file type
		fileType = SysUtil.FT_VAU_RPT2;

		// scan until reject details legend found
		String line = null;
		while ((line = r.readLine()) != null && !line.startsWith("Reject"));
		if (line != null) {
			// discard blank line if present
			line = r.readLine();
			line = (line.startsWith("  ") ? r.readLine() : line);

			// should have first reject line here, break out delimited fields
			// disable auto trimming to keep padding in disc data field
			String[] fields = VauFileUtil.parseReportLine(line, false);
			if (fields.length >= 6) {
				// fetch the obId from the discretionary data field
				String obIdStr = fields[5].substring(20).trim();
				try {
					obId = Long.parseLong(obIdStr);
				}
				catch (Exception e) {
					throw new RuntimeException(
							"Cannot determine outbound file ID," + " invalid ID '" + obIdStr + "' in proprietary info field '" + fields[5] + "'");
				}
			}
			else {
				throw new RuntimeException(
						"Cannot determine outbound file ID," + " unable to locate reject detail record discretionary data");
			}
		}
		else {
			throw new RuntimeException("Cannot determine outbound file ID," + " unable to locate reject detail section legend");
		}
	}

	/**
	 * Determine file type and extract obId.
	 */
	private void inspectFileData(InboundFile ibf) throws Exception {
		// create a reader to scan through lines of the file data
		InputStream in = ibf.getInputStream();
		LineNumberReader r = new LineNumberReader(new InputStreamReader(in));

		try {
			// a '0' indicates the line is the header record of a response file
			String line = r.readLine();
			if (line.startsWith("0")) {
				// grab first detail (second line) to determine obId
				line = r.readLine();
				inspectResponseFileData(line);
			}
			// inspect as a report file
			else {
				// scan for report type
				while ((line = r.readLine()) != null && !line.startsWith("Report:"));
				if (line == null) {
					throw new RuntimeException("Cannot determine report type," + " no report type line found");
				}
				String[] fields = VauFileUtil.parseReportLine(line);
				if (fields.length < 2) {
					throw new RuntimeException("Cannot determine report type," + " report type field not present");
				}
				if (fields[1].equals("01")) {
					fileType = SysUtil.FT_VAU_RPT1;
				}
				else if (fields[1].equals("02")) {
					// try to determine obId from reject data
					inspectReport2FileData(r);
				}
				else if (fields[1].equals("03")) {
					fileType = SysUtil.FT_VAU_RPT3;
				}
				else if (fields[1].equals("04")) {
					fileType = SysUtil.FT_VAU_RPT4;
				}
				else if (fields[1].equals("05")) {
					fileType = SysUtil.FT_VAU_RPT5;
				}
				else if (fields[1].equals("06")) {
					fileType = SysUtil.FT_VAU_RPT6;
				}
				else {
					throw new RuntimeException("Cannot determine report type," + " unknown report type '" + fields[1] + "'");
				}
			}
		}
		finally {
			try {
				r.close();
			}
			catch (Exception e) {}
			try {
				in.close();
			}
			catch (Exception e) {}
		}
	}

	private InboundFile upload() {
		try {
			InboundFile ibf = new InboundFile();
			ibf.setIbId(db.getNewId());
			ibf.setCreateDate(Calendar.getInstance().getTime());
			ibf.setSysCode(SysUtil.SYS_VAU);
			ibf.setFileName(filename);
			ibf.setData(in);

			// extract outbound file id from file
			inspectFileData(ibf);
			ibf.setFileType(fileType);
			ibf.setObId(obId);
			db.insertInboundFile(ibf, true);
			return ibf;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "upload(filename=" + filename + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
		}
		return null;
	}

	public static InboundFile uploadFile(InputStream in, String filename, boolean directFlag) {
		return (new VauIbfLoader(in, filename, directFlag)).upload();
	}
	public static InboundFile uploadFile(InputStream in, String filename) {
		return uploadFile(in, filename, true);
	}
}