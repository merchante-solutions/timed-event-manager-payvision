package com.mes.aus.file;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.sftp.SftpFile;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.aus.Notifier;

public class VauIbfXfer extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauIbfXfer.class);

	public VauIbfXfer(boolean directFlag) {
		super(directFlag);
	}

	public void transfer() {
		try {
			log.debug("Initiating vau inbound file transfers...");

			StringBuffer noticeText = new StringBuffer("VAU inbound file transfer activity:\n\n");
			int fileCount = 0;
			int errorCount = 0;

			// establish sftp connection with vau endpoint
			SshParameters sshParms = new SshParameters(db.getVauEndpointHost(), db.getVauEndpointUser(), db.getVauEndpointPassword());

			Sftp sftp = new Sftp(sshParms);
			sftp.connect();
			sftp.setDir(db.getVauIbXferLoc());
			sftp.setAscii();

			// date formatter to create arc file suffix
			SimpleDateFormat sdf = new SimpleDateFormat("M-d-yy.h-mm-ss");

			// get list of available inbound files
			// for each inbound file....
			List files = Collections.list(sftp.getDirListing("acq\\.in\\..+"));
			for (Iterator i = files.iterator(); i.hasNext();) {
				SftpFile sf = (SftpFile) i.next();
				log.debug("found " + sf.getFilename());
				try {
					// upload inbound file to db
					VauIbfLoader.uploadFile(sftp.getInputStream(sf.getFilename(), 0), sf.getFilename(), directFlag);

					// archive inbound file
					Calendar cal = Calendar.getInstance();
					String arcSuffix = "." + sdf.format(cal.getTime());
					if (!sftp.getDirListing("arc").hasMoreElements()) {
						sftp.makeDir("arc");
					}
					String newLoc = "arc/" + sf.getFilename() + arcSuffix;
					log.debug("Moving file to archive location: " + newLoc);
					sftp.renameFile(sf.getFilename(), newLoc);

					// add to list of files delivered
					noticeText.append("  " + sf.getFilename() + "\n");
					fileCount++;
				}
				catch (Exception ie) {
					log.error("Error: " + ie);
					notifyError(ie, "transfer(" + sf.getFilename() + ")");
					errorCount++;
				}
			}
			// disconnect
			sftp.disconnect();

			// notify developer inbound files transferred
			if (files.size() > 0) {
				noticeText.append("\n" + files.size() + " inbound files\n");
				noticeText.append("" + fileCount + " files successfully transferred\n");
				noticeText.append("" + errorCount + " errors");
				Notifier.notifyDeveloper("VAU Inbound File Transfer", "" + noticeText, directFlag);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "transfer()");
		}
	}
}