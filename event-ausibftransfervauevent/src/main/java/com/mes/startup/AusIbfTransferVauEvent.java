package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.VauIbfXfer;

public final class AusIbfTransferVauEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusIbfTransferVauEvent.class);

  public boolean execute()
  {
    try
    {
      (new VauIbfXfer(true)).transfer();
      return true;
    }
    catch (Exception e)
    {
      log.error("AUS request file process event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}