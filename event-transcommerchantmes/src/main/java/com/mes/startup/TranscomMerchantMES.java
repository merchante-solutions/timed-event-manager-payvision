/*@lineinfo:filename=TranscomMerchantMES*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomMerchantMES.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:27:32 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23997 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.QueueConstants;
import sqlj.runtime.ResultSetIterator;

public class TranscomMerchantMES extends TranscomFileProcess
{
  private Vector  apps          = new Vector();
  
  public TranscomMerchantMES()
  {
    super(FILE_TYPE_MERCHANT);
  }
  
  public TranscomMerchantMES(String connectionString)
  {
    super(FILE_TYPE_MERCHANT, connectionString);
  }
  
  private void markAppsToProcess()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_merchant_update_mes
//          set     process_job = :procSequence
//          where   process_job = -1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_merchant_update_mes\n        set     process_job =  :1 \n        where   process_job = -1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^7*/
      
      // get all app_seq_nums we need to process this go-around
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    transcom_merchant_update_mes
//          where   process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    transcom_merchant_update_mes\n        where   process_job =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        apps.add(rs.getLong("app_seq_num"));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      setError("markAppsToProcess()" + e.toString());
      logEntry("markAppsToProcess()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getOneToOneData(long appSeqNum, int sequence)
  {
    ResultSetIterator   it                = null;
    ResultSet           rs                = null;
    
    // data items
    String              recType           = "";
    String              userId            = "";
    String              merName           = "";
    String              merStatus         = "";  // from pricing_grid
    Date                dateSub           = null; // app.app_created_date
    Date                dateFirstDep      = null;
    String              trainBy           = "";
    long                merId             = 0L;
    long                merControlNum     = 0L;
    long                annVmcVol         = 0L;
    double              avgTicket         = 0L;
    long                annTranCount      = 0L;
    long                annVol            = 0L;
    String              rmi               = "";
    String              checkRecvdFlag    = "";
    double              checkRecvdAmt     = 0.0;
    String              merContact        = "";
    int                 sicCode           = 0;
    String              insuranceFlag     = "";
    String              merLegalName      = "";
    String              otherFee1Desc     = "";
    String              otherFee2Desc     = "";
    int                 prevStatementFlag = 0;
    Date                dateSubmitted     = null; // transcom_merchant.date_app_submitted
    Date                dateTrained       = null;
    String              comments          = "";
    Date                dateRecvd         = null; // transcom_merchant.date_app_received
    int                 numReprog         = 0;
    String              merDDA            = "";
    String              merTR             = "";
    String              merBankName       = "";
    double              percMail          = 0.0;
    double              percInternet      = 0.0;
    double              percPhone         = 0.0;
    int                 numLocs           = 0;
    String              contLast          = "";
    String              busEmail          = "";
    String              taxId             = "";   
    String              incStatus         = "";
    String              visaBet           = "";
    String              mcBet             = "";
    double              chPresent         = 0.0;
    double              chNotPresent      = 0.0;
    
    String progress = "start of method";
    
    try
    {
      // get record type for this app (U = updated, I = insert)
      progress = "getting record type";
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    transcom_process_merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    transcom_process_merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TranscomMerchantMES",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^7*/
      
      if(appCount > 0)
      {
        // this is an update
        recType = "U";
      }
      else
      {
        // this is a new record
        recType = "I";
      }
      
      // get the one-to-one data from assorted tables
      progress = "performing main query";
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merc_cntrl_number                 mer_control_num,
//                  m.merch_average_cc_tran             avg_ticket,
//                  replace(m.merch_business_name,',',' ')  mer_name,
//                  replace(m.merch_legal_name,',',' ') mer_legal_name,
//                  m.merch_month_tot_proj_sales * 12   ann_vol,
//                  m.merch_month_visa_mc_sales * 12    ann_vmc_vol,
//                  m.merch_number                      mer_id,
//                  m.merch_prior_cc_accp_flag          prev_statement_flag,
//                  m.pricing_grid                      pricing_grid,
//                  m.sic_code                          sic_code,
//                  m.merch_notes                       merch_notes,
//                  m.date_activated                    date_first_dep,
//                  m.merch_email_address               bus_email,
//                  m.merch_federal_tax_id              tax_id,
//                  m.merch_num_of_locations            num_locs,
//                  bt.bustype_desc                     inc_status,
//                  tmu.status                          status,
//                  app.app_created_date                date_sub,
//                  mb.merchbank_acct_num               mer_dda,
//                  replace(mb.merchbank_name,',',' ')  mer_bank_name,
//                  mb.merchbank_transit_route_num      mer_tr,
//                  mc.merchcont_prim_first_name        mer_contact_first,
//                  mc.merchcont_prim_last_name         mer_contact_last,
//                  tc.phone_training                   train_by,
//                  tc.annual_transaction_count         ann_tran_cnt,
//                  tc.check_amount_misc                check_recvd_amt,
//                  tc.date_app_received                date_recvd,
//                  app.app_created_date                date_submitted,
//                  tc.misc_fee1_desc                   other_fee_1_desc,
//                  tc.misc_fee2_desc                   other_fee_2_desc,
//                  tc.num_reprogrmming                 num_reprog,
//                  app.app_user_id                     user_id,
//                  tc.shipping_comment                 shipping_comments,
//                  tc.terminal_insurance               insurance_flag,
//                  tc.perc_mail                        perc_mail,
//                  tc.perc_internet                    perc_internet,
//                  tc.perc_telephone                   perc_phone,
//                  pf.equipment_comment                equipment_comments,
//                  tc.cardholder_present/100           ch_present,
//                  tc.cardholder_not_present/100       ch_not_present
//          from    merchant                            m,
//                  transcom_merchant_update_mes        tmu,
//                  application                         app,
//                  merchbank                           mb,
//                  merchcontact                        mc,
//                  transcom_merchant                   tc,
//                  pos_features                        pf,
//                  bustype                             bt,
//                  transcom_bet_tables                 tb
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = tmu.app_seq_num   and
//                  tmu.process_job = :procSequence   and
//                  m.app_seq_num = app.app_seq_num   and
//                  m.app_seq_num = mb.app_seq_num(+) and
//                  m.app_seq_num = mc.app_seq_num(+) and
//                  m.app_seq_num = tc.app_seq_num(+) and
//                  m.app_seq_num = pf.app_seq_num(+) and
//                  m.bustype_code = bt.bustype_code(+) and
//                  tc.bet_table   = tb.visa_bet(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merc_cntrl_number                 mer_control_num,\n                m.merch_average_cc_tran             avg_ticket,\n                replace(m.merch_business_name,',',' ')  mer_name,\n                replace(m.merch_legal_name,',',' ') mer_legal_name,\n                m.merch_month_tot_proj_sales * 12   ann_vol,\n                m.merch_month_visa_mc_sales * 12    ann_vmc_vol,\n                m.merch_number                      mer_id,\n                m.merch_prior_cc_accp_flag          prev_statement_flag,\n                m.pricing_grid                      pricing_grid,\n                m.sic_code                          sic_code,\n                m.merch_notes                       merch_notes,\n                m.date_activated                    date_first_dep,\n                m.merch_email_address               bus_email,\n                m.merch_federal_tax_id              tax_id,\n                m.merch_num_of_locations            num_locs,\n                bt.bustype_desc                     inc_status,\n                tmu.status                          status,\n                app.app_created_date                date_sub,\n                mb.merchbank_acct_num               mer_dda,\n                replace(mb.merchbank_name,',',' ')  mer_bank_name,\n                mb.merchbank_transit_route_num      mer_tr,\n                mc.merchcont_prim_first_name        mer_contact_first,\n                mc.merchcont_prim_last_name         mer_contact_last,\n                tc.phone_training                   train_by,\n                tc.annual_transaction_count         ann_tran_cnt,\n                tc.check_amount_misc                check_recvd_amt,\n                tc.date_app_received                date_recvd,\n                app.app_created_date                date_submitted,\n                tc.misc_fee1_desc                   other_fee_1_desc,\n                tc.misc_fee2_desc                   other_fee_2_desc,\n                tc.num_reprogrmming                 num_reprog,\n                app.app_user_id                     user_id,\n                tc.shipping_comment                 shipping_comments,\n                tc.terminal_insurance               insurance_flag,\n                tc.perc_mail                        perc_mail,\n                tc.perc_internet                    perc_internet,\n                tc.perc_telephone                   perc_phone,\n                pf.equipment_comment                equipment_comments,\n                tc.cardholder_present/100           ch_present,\n                tc.cardholder_not_present/100       ch_not_present\n        from    merchant                            m,\n                transcom_merchant_update_mes        tmu,\n                application                         app,\n                merchbank                           mb,\n                merchcontact                        mc,\n                transcom_merchant                   tc,\n                pos_features                        pf,\n                bustype                             bt,\n                transcom_bet_tables                 tb\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = tmu.app_seq_num   and\n                tmu.process_job =  :2    and\n                m.app_seq_num = app.app_seq_num   and\n                m.app_seq_num = mb.app_seq_num(+) and\n                m.app_seq_num = mc.app_seq_num(+) and\n                m.app_seq_num = tc.app_seq_num(+) and\n                m.app_seq_num = pf.app_seq_num(+) and\n                m.bustype_code = bt.bustype_code(+) and\n                tc.bet_table   = tb.visa_bet(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // get the easy items (no massaging necessary)
        progress = "getting merControlNum";
        merControlNum     = rs.getLong("mer_control_num");
        progress = "getting avgTicket";
        avgTicket         = rs.getDouble("avg_ticket");
        progress = "getting merName";
        merName           = blankIfNull(rs.getString("mer_name"));
        progress = "getting merLegalName";
        merLegalName      = blankIfNull(rs.getString("mer_legal_name"));
        progress = "getting annVol";
        annVol            = rs.getLong("ann_vol");
        progress = "getting annVmcVol";
        annVmcVol         = rs.getLong("ann_vmc_vol");
        progress = "getting merId";
        merId             = rs.getLong("mer_id");
        progress = "getting sicCode";
        sicCode           = rs.getInt("sic_code");
        progress = "getting dateFirstDep";
        dateFirstDep      = rs.getDate("date_first_dep");
        progress = "getting dateSub";
        dateSub           = rs.getDate("date_sub");
        progress = "getting merDDA";
        merDDA            = blankIfNull(rs.getString("mer_dda"));
        progress = "getting merBankName";
        merBankName       = blankIfNull(rs.getString("mer_bank_name"));
        progress = "getting merTR";
        merTR             = blankIfNull(rs.getString("mer_tr"));
        progress = "getting annTranCount";
        annTranCount      = rs.getLong("ann_tran_cnt");
        progress = "getting checkRecvdAmt";
        checkRecvdAmt     = rs.getDouble("check_recvd_amt");
        progress = "getting dateRecvd";
        dateRecvd         = rs.getDate("date_recvd");
        progress = "getting otherFee1Desc";
        otherFee1Desc     = blankIfNull(rs.getString("other_fee_1_desc"));
        progress = "getting otherFee2Desc";
        otherFee2Desc     = blankIfNull(rs.getString("other_fee_2_desc"));
        progress = "getting numReprog";
        numReprog         = rs.getInt("num_reprog");
        progress = "getting userId";
        userId            = blankIfNull(rs.getString("user_id"));
        progress = "getting insuranceFlag";
        insuranceFlag     = blankIfNull(rs.getString("insurance_flag"));
        progress = "getting trainBy";
        trainBy           = blankIfNull(rs.getString("train_by"));
        merContact        = blankIfNull(rs.getString("mer_contact_first"));
        contLast          = blankIfNull(rs.getString("mer_contact_last"));
        numLocs           = rs.getInt("num_locs");
        busEmail          = blankIfNull(rs.getString("bus_email"));
        taxId             = blankIfNull(rs.getString("tax_id"));        
        incStatus         = blankIfNull(rs.getString("inc_status"));

        // prevStatementFlag
        progress = "getting prevStatementFlag";
        String flag = blankIfNull(rs.getString("prev_statement_flag"));
        if(flag.equals("Y"))
        {
          prevStatementFlag = 1;
        }
        else
        {
          prevStatementFlag = 2;
        }
        
        // percMail
        String temp;
        progress = "getting percMail";
        temp = blankIfNull(rs.getString("perc_mail"));
        try
        {
          percMail = Double.parseDouble(temp);
        }
        catch(Exception pe)
        {
        }
        
        progress = "getting percPhone";
        temp = blankIfNull(rs.getString("perc_phone"));
        try
        {
          percPhone = Double.parseDouble(temp);
        }
        catch(Exception pe)
        {
        }
        
        progress = "getting percInternet";
        temp = blankIfNull(rs.getString("perc_internet"));
        try
        {
          percInternet = Double.parseDouble(temp);
        }
        catch(Exception pe)
        {
        }
        
        // chPresent
        progress = "getting ch_present";
        temp = blankIfNull(rs.getString("ch_present"));
        try
        {
          chPresent = Double.parseDouble(temp);
        }
        catch(Exception pe)
        {
          chPresent = 1;
        }
        
        // chNotPresent
        progress = "getting ch_not_present";
        temp = blankIfNull(rs.getString("ch_not_present"));
        try
        {
          chNotPresent = Double.parseDouble(temp);
        }
        catch(Exception pe)
        {
          chNotPresent = 0;
        }
        
        // merStatus
        progress = "getting merStatus";
        int     status = rs.getInt("status");
        switch(status)
        {
          case mesConstants.APP_STATUS_INCOMPLETE:
            merStatus = "INCOMPLETE";
            break;
            
          case mesConstants.APP_STATUS_COMPLETE:
            merStatus = "REVIEW";
            break;
            
          case mesConstants.APP_STATUS_APPROVED:
            if(rs.getString("date_first_dep") == null)
            {
              merStatus = "APPROVED";
            }
            else
            {
              merStatus = "ACTIVE";
            }
            break;
            
          case mesConstants.APP_STATUS_DECLINED:
            merStatus = "DECLINED";
            break;
            
          case mesConstants.APP_STATUS_PENDED:
            merStatus = "PENDED";
            break;
            
          case mesConstants.APP_STATUS_CANCELLED:
            merStatus = "CANCEL";
            break;
            
          case mesConstants.APP_STATUS_SETUP_COMPLETE:
          case mesConstants.APP_STATUS_ADDED_CARD:
          case mesConstants.APP_STATUS_REMOVED_CARD:
            if(rs.getString("date_first_dep") == null)
            {
              merStatus = "APPROVED";
            }
            else
            {
              merStatus = "ACTIVE";
            }
            break;
        }
        
        // check received
        progress = "getting checkRecvdFlag";
        checkRecvdFlag = "N";
        if(checkRecvdAmt > 0.0)
        {
          checkRecvdFlag = "Y";
        }
        
        // trained by
        progress = "getting trainBy";
        boolean mesTrain    = false;
        trainBy = "MES";
        mesTrain = true;
        
        // date trained
        progress = "getting dateTrained";
        if(mesTrain)
        {
          dateTrained = rs.getDate("date_first_dep");
        }
        
        // comments
        progress = "getting comments";
        StringBuffer  work = new StringBuffer("");
        work.append(blankIfNull(rs.getString("shipping_comments")));
        work.append(" ");
        work.append(blankIfNull(rs.getString("equipment_comments")));
        work.append(" ");
        work.append(blankIfNull(rs.getString("merch_notes")));
        
        // replace any carriage returns or newlines with spaces
        for(int i=0; i<work.length(); ++i)
        {
          if(work.charAt(i) == '\r' || work.charAt(i) == '\n')
          {
            work.setCharAt(i, ' ');
          }
        }
        
        if(work.length() > 200)
        {
          comments = work.substring(0, 200);
        }
        else
        {
          comments = work.toString();
        }
      }
        
      rs.close();
      it.close();
      
      // get visa/mastercard bets
      /*@lineinfo:generated-code*//*@lineinfo:463^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  aic.vs_bet  visa_bet,
//                  aic.mc_bet  mc_bet
//          from    appo_ic_bets  aic,
//                  tranchrg  tc
//          where   tc.app_seq_num = :appSeqNum and
//                  tc.cardtype_code = 1 and
//                  tc.tranchrg_interchangefee_fee = aic.combo_id and
//                  aic.app_type = 28        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  aic.vs_bet  visa_bet,\n                aic.mc_bet  mc_bet\n        from    appo_ic_bets  aic,\n                tranchrg  tc\n        where   tc.app_seq_num =  :1  and\n                tc.cardtype_code = 1 and\n                tc.tranchrg_interchangefee_fee = aic.combo_id and\n                aic.app_type = 28";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:473^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        visaBet = blankIfNull(rs.getString("visa_bet"));
        mcBet = blankIfNull(rs.getString("mc_bet"));
      }
      
      rs.close();
      it.close();
      
      // RMI
      int plan;
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:490^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(tranchrg_interchangefee_fee,1)
//            
//            from    tranchrg
//            where   app_seq_num = :appSeqNum and
//                    cardtype_code = :mesConstants.APP_CT_VISA
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(tranchrg_interchangefee_fee,1)\n           \n          from    tranchrg\n          where   app_seq_num =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TranscomMerchantMES",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   plan = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:497^9*/
      }
      catch( java.sql.SQLException sqe )
      {
        // necessary to allow old MES direct apps
        // to be sent over to Transcom for residual payments
        plan = 1;
      }        
      
      switch(plan)
      {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 12:
        case 16:
        case 17:
        case 18:
        default:
          rmi = "R";
          break;
          
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 13:
          rmi = "M";
          break;
          
        case 14:
          rmi = "L";
          break;
          
        case 15:
          rmi = "S";
          break;
      }
      
      progress = "inserting data into transcom_process_merchant";
      
      // insert data into the transcom_process_merchant table
      /*@lineinfo:generated-code*//*@lineinfo:543^7*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_process_merchant
//          (
//            process_job,
//            app_seq_num,
//            job_sequence,
//            rectype,
//            user_id,
//            mer_name,
//            mer_status,
//            date_sub,
//            date_first_dep,
//            train_by,
//            mer_id,
//            mer_control_num,
//            ann_vmc_vol,
//            avg_ticket,
//            ann_tran_cnt,
//            ann_vol,
//            rmi,
//            check_recvd_flag,
//            check_recvd_amt,
//            mer_contact,
//            sic_code,
//            network,
//            insurance_flag,
//            liability,
//            mer_legal_name,
//            other_fee_1_desc,
//            other_fee_2_desc,
//            prev_statement_flag,
//            date_submitted,
//            date_trained,
//            comments,
//            date_recvd,
//            num_reprog,
//            mer_dda,
//            mer_tr,
//            mer_bank_name,
//            perc_mail,
//            perc_internet,
//            perc_phone,
//            cont_last,
//            num_locs,
//            bus_email,
//            tax_id,
//            inc_status,
//            visa_bet,
//            mc_bet,
//            ch_present,
//            ch_not_present
//          )
//          values
//          (
//            :procSequence,
//            :appSeqNum,
//            :sequence,
//            :recType,
//            :userId,
//            :merName,
//            :merStatus,
//            :dateSub,
//            :dateFirstDep,
//            :trainBy,
//            :merId,
//            :merControlNum,
//            :annVmcVol,
//            :avgTicket,
//            :annTranCount,
//            :annVol,
//            :rmi,
//            :checkRecvdFlag,
//            :checkRecvdAmt,
//            :merContact,
//            :sicCode,
//            'Vital',
//            :insuranceFlag,
//            'local',
//            :merLegalName,
//            :otherFee1Desc,
//            :otherFee2Desc,
//            :prevStatementFlag,
//            :dateSubmitted,
//            :dateTrained,
//            :comments,
//            :dateRecvd,
//            :numReprog,
//            :merDDA,
//            :merTR,
//            :merBankName,
//            :percMail,
//            :percInternet,
//            :percPhone,
//            :contLast,
//            :numLocs,
//            :busEmail,
//            :taxId,
//            :incStatus,
//            :visaBet,
//            :mcBet,
//            :chPresent,
//            :chNotPresent
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_process_merchant\n        (\n          process_job,\n          app_seq_num,\n          job_sequence,\n          rectype,\n          user_id,\n          mer_name,\n          mer_status,\n          date_sub,\n          date_first_dep,\n          train_by,\n          mer_id,\n          mer_control_num,\n          ann_vmc_vol,\n          avg_ticket,\n          ann_tran_cnt,\n          ann_vol,\n          rmi,\n          check_recvd_flag,\n          check_recvd_amt,\n          mer_contact,\n          sic_code,\n          network,\n          insurance_flag,\n          liability,\n          mer_legal_name,\n          other_fee_1_desc,\n          other_fee_2_desc,\n          prev_statement_flag,\n          date_submitted,\n          date_trained,\n          comments,\n          date_recvd,\n          num_reprog,\n          mer_dda,\n          mer_tr,\n          mer_bank_name,\n          perc_mail,\n          perc_internet,\n          perc_phone,\n          cont_last,\n          num_locs,\n          bus_email,\n          tax_id,\n          inc_status,\n          visa_bet,\n          mc_bet,\n          ch_present,\n          ch_not_present\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n          'Vital',\n           :22 ,\n          'local',\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n           :36 ,\n           :37 ,\n           :38 ,\n           :39 ,\n           :40 ,\n           :41 ,\n           :42 ,\n           :43 ,\n           :44 ,\n           :45 ,\n           :46 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,sequence);
   __sJT_st.setString(4,recType);
   __sJT_st.setString(5,userId);
   __sJT_st.setString(6,merName);
   __sJT_st.setString(7,merStatus);
   __sJT_st.setDate(8,dateSub);
   __sJT_st.setDate(9,dateFirstDep);
   __sJT_st.setString(10,trainBy);
   __sJT_st.setLong(11,merId);
   __sJT_st.setLong(12,merControlNum);
   __sJT_st.setLong(13,annVmcVol);
   __sJT_st.setDouble(14,avgTicket);
   __sJT_st.setLong(15,annTranCount);
   __sJT_st.setLong(16,annVol);
   __sJT_st.setString(17,rmi);
   __sJT_st.setString(18,checkRecvdFlag);
   __sJT_st.setDouble(19,checkRecvdAmt);
   __sJT_st.setString(20,merContact);
   __sJT_st.setInt(21,sicCode);
   __sJT_st.setString(22,insuranceFlag);
   __sJT_st.setString(23,merLegalName);
   __sJT_st.setString(24,otherFee1Desc);
   __sJT_st.setString(25,otherFee2Desc);
   __sJT_st.setInt(26,prevStatementFlag);
   __sJT_st.setDate(27,dateSubmitted);
   __sJT_st.setDate(28,dateTrained);
   __sJT_st.setString(29,comments);
   __sJT_st.setDate(30,dateRecvd);
   __sJT_st.setInt(31,numReprog);
   __sJT_st.setString(32,merDDA);
   __sJT_st.setString(33,merTR);
   __sJT_st.setString(34,merBankName);
   __sJT_st.setDouble(35,percMail);
   __sJT_st.setDouble(36,percInternet);
   __sJT_st.setDouble(37,percPhone);
   __sJT_st.setString(38,contLast);
   __sJT_st.setInt(39,numLocs);
   __sJT_st.setString(40,busEmail);
   __sJT_st.setString(41,taxId);
   __sJT_st.setString(42,incStatus);
   __sJT_st.setString(43,visaBet);
   __sJT_st.setString(44,mcBet);
   __sJT_st.setDouble(45,chPresent);
   __sJT_st.setDouble(46,chNotPresent);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:647^7*/
    }
    catch(Exception e)
    {
      setError("getOneToOneData(" + appSeqNum + ")" + e.toString());
      logEntry("getOneToOneData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getAddressData(long appSeqNum)
  {
    ResultSetIterator     it              = null;
    ResultSet             rs              = null;
    String                merAddr         = "";
    String                merCity         = "";
    String                merState        = "";
    String                merZip          = "";
    long                  merPhone        = 0L;
    long                  busFax          = 0L;
    String                mailAddr        = "";
    String                mailCity        = "";
    String                mailState       = "";
    String                mailZip         = "";
    String                priAddr         = "";
    String                priCity         = "";
    String                priState        = "";
    String                priZip          = "";
    long                  priPhone        = 0L;
    String                secAddr         = "";
    String                secCity         = "";
    String                secState        = "";
    String                secZip          = "";
    long                  secPhone        = 0L;
    
    String                progress        = "start of method";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:690^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1,
//                  address_line2,
//                  address_city,
//                  countrystate_code,
//                  address_zip,
//                  address_phone,
//                  addresstype_code,
//                  address_fax
//          from    address
//          where   app_seq_num = :appSeqNum
//          order by  addresstype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1,\n                address_line2,\n                address_city,\n                countrystate_code,\n                address_zip,\n                address_phone,\n                addresstype_code,\n                address_fax\n        from    address\n        where   app_seq_num =  :1 \n        order by  addresstype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:703^7*/
      
      StringBuffer temp = new StringBuffer();
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        temp.setLength(0);
        temp.append(blankIfNull(rs.getString("address_line1")));
        temp.append(" ");
        temp.append(blankIfNull(rs.getString("address_line2")));
        
        int addrType = rs.getInt("addresstype_code");
        
        switch(addrType)
        {
          case mesConstants.ADDR_TYPE_BUSINESS:
            merAddr   = (temp.length() > 70) ? temp.substring(0, 70) : temp.toString();
            merCity   = blankIfNull(rs.getString("address_city"));
            merState  = blankIfNull(rs.getString("countrystate_code"));
            merZip    = blankIfNull(rs.getString("address_zip"));
            merPhone  = rs.getLong("address_phone");
            busFax    = rs.getLong("address_fax");
            break;
            
          case mesConstants.ADDR_TYPE_MAILING:
            mailAddr  = (temp.length() > 70) ? temp.substring(0, 70) : temp.toString();
            mailCity  = blankIfNull(rs.getString("address_city"));
            mailState = blankIfNull(rs.getString("countrystate_code"));
            mailZip   = blankIfNull(rs.getString("address_zip"));
            break;
            
          case mesConstants.ADDR_TYPE_OWNER1:
            priAddr   = (temp.length() > 70) ? temp.substring(0, 70) : temp.toString();
            priCity   = blankIfNull(rs.getString("address_city"));
            priState  = blankIfNull(rs.getString("countrystate_code"));
            priZip    = blankIfNull(rs.getString("address_zip"));
            priPhone  = rs.getLong("address_phone");
            break;
            
          case mesConstants.ADDR_TYPE_OWNER2:
            secAddr   = (temp.length() > 70) ? temp.substring(0, 70) : temp.toString();
            secCity   = blankIfNull(rs.getString("address_city"));
            secState  = blankIfNull(rs.getString("countrystate_code"));
            secZip    = blankIfNull(rs.getString("address_zip"));
            secPhone  = rs.getLong("address_phone");
            break;
        }
      }
      
      rs.close();
      it.close();
      
      // add the address information to the record
      /*@lineinfo:generated-code*//*@lineinfo:758^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     mer_addr    = :merAddr,
//                  mer_city    = :merCity,
//                  mer_state   = :merState,
//                  mer_zip     = :merZip,
//                  mer_phone   = :merPhone,
//                  mail_addr   = :mailAddr,
//                  mail_city   = :mailCity,
//                  mail_state  = :mailState,
//                  mail_zip    = :mailZip,
//                  bus_fax     = :busFax,
//                  pri_address = :priAddr,
//                  pri_city    = :priCity,
//                  pri_state   = :priState,
//                  pri_zip     = :priZip,
//                  pri_phone   = :priPhone,
//                  sec_address = :secAddr,
//                  sec_city    = :secCity,
//                  sec_state   = :secState,
//                  sec_zip     = :secZip,
//                  sec_phone   = :secPhone
//          where   app_seq_num = :appSeqNum and
//                  process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     mer_addr    =  :1 ,\n                mer_city    =  :2 ,\n                mer_state   =  :3 ,\n                mer_zip     =  :4 ,\n                mer_phone   =  :5 ,\n                mail_addr   =  :6 ,\n                mail_city   =  :7 ,\n                mail_state  =  :8 ,\n                mail_zip    =  :9 ,\n                bus_fax     =  :10 ,\n                pri_address =  :11 ,\n                pri_city    =  :12 ,\n                pri_state   =  :13 ,\n                pri_zip     =  :14 ,\n                pri_phone   =  :15 ,\n                sec_address =  :16 ,\n                sec_city    =  :17 ,\n                sec_state   =  :18 ,\n                sec_zip     =  :19 ,\n                sec_phone   =  :20 \n        where   app_seq_num =  :21  and\n                process_job =  :22";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merAddr);
   __sJT_st.setString(2,merCity);
   __sJT_st.setString(3,merState);
   __sJT_st.setString(4,merZip);
   __sJT_st.setLong(5,merPhone);
   __sJT_st.setString(6,mailAddr);
   __sJT_st.setString(7,mailCity);
   __sJT_st.setString(8,mailState);
   __sJT_st.setString(9,mailZip);
   __sJT_st.setLong(10,busFax);
   __sJT_st.setString(11,priAddr);
   __sJT_st.setString(12,priCity);
   __sJT_st.setString(13,priState);
   __sJT_st.setString(14,priZip);
   __sJT_st.setLong(15,priPhone);
   __sJT_st.setString(16,secAddr);
   __sJT_st.setString(17,secCity);
   __sJT_st.setString(18,secState);
   __sJT_st.setString(19,secZip);
   __sJT_st.setLong(20,secPhone);
   __sJT_st.setLong(21,appSeqNum);
   __sJT_st.setInt(22,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:783^7*/
    }
    catch(Exception e)
    {
      setError("getAddressData(" + appSeqNum + ")" + e.toString());
      logEntry("getAddressData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getMiscCharges(long appSeqNum)
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    double            merAppFee       = 0.0;
    double            rpFee           = 0.0;
    double            batchFee        = 0.0;
    double            annFee          = 0.0;
    double            debitAccessFee  = 0.0;
    double            otherFee1       = 0.0;
    double            otherFee2       = 0.0;
    double            chargebackFee   = 0.0;
    double            statementFee    = 0.0;
    double            trainingFee     = 0.0;
    double            intRepFee       = 0.0;
    double            ppSwapFee       = 0.0;
    double            intStartFee     = 0.0;
    double            intGateFee      = 0.0;
    double            pproSetup       = 0.0;
    double            plinkSetup      = 0.0;
    double            pproMon         = 0.0;
    double            plinkMon        = 0.0;
    double            wirelessSetup   = 0.0;
    double            wirelessMon     = 0.0;
    boolean           payflow         = false;
    String            payflowType     = "NONE";
    
    try
    {
      // determine if this merchant has payflow, and if so which type
      /*@lineinfo:generated-code*//*@lineinfo:827^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_code
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_code\n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:832^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        switch(rs.getInt("pos_code"))
        {
          case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK:
            payflow     = true;
            payflowType = "Link";
            break;
            
          case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO:
            payflow     = true;
            payflowType = "PRO";
            break;
        }
      }
      
      rs.close();
      it.close();
      
      // get miscellaneous charges
      /*@lineinfo:generated-code*//*@lineinfo:856^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount,
//                  misc_code
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount,\n                misc_code\n        from    miscchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:862^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int     code    = rs.getInt("misc_code");
        double  charge  = rs.getDouble("misc_chrg_amount");
        
        switch(code)
        {
          case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP:
            merAppFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE:
            rpFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE:
            batchFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_ANNUAL_FEE:
            annFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_DEBIT_ACCESS_FEE:
            debitAccessFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_MISC1_FEE:
            otherFee1 = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_MISC2_FEE:
            otherFee2 = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
            chargebackFee = charge;
            break;
          
          case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE:
            statementFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_TRAINING_FEE:
            trainingFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_WEB_REPORTING:
            intRepFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_PINPAD_SWAP_FEE:
            ppSwapFee = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP:
            intStartFee = charge;
            
            if(payflow)
            {
              if(payflowType.equals("Link"))
              {
                plinkSetup = charge;
              }
              else
              {
                pproSetup = charge;
              }
            }
            break;
            
          case mesConstants.APP_MISC_CHARGE_MONTHLY_GATEWAY:
            intGateFee = charge;
            
            if(payflow)
            {
              if(payflowType.equals("Link"))
              {
                plinkMon = charge;
              }
              else
              {
                pproMon = charge;
              }
            }
            break;
            
          case mesConstants.APP_MISC_CHARGE_WIRELESS_SETUP_FEE:
            wirelessSetup = charge;
            break;
            
          case mesConstants.APP_MISC_CHARGE_WIRELESS_MONTHLY_FEE:
            wirelessMon   = charge;
            break;
            
          default:
            break;
        }
      }
      
      rs.close();
      it.close();
      
      // update the transcom_process_merchant table
      /*@lineinfo:generated-code*//*@lineinfo:970^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     mer_app_fee       = :merAppFee,
//                  rp_fee            = :rpFee,
//                  batch_fee         = :batchFee,
//                  ann_fee           = :annFee,
//                  debit_access_fee  = :debitAccessFee,
//                  other_fee_1       = :otherFee1,
//                  other_fee_2       = :otherFee2,
//                  chargeback_fee    = :chargebackFee,
//                  statement_fee     = :statementFee,
//                  training_fee      = :trainingFee,
//                  int_rep_fee       = :intRepFee,
//                  pp_swap_fee       = :ppSwapFee,
//                  int_start_fee     = :intStartFee,
//                  int_gate_fee      = :intGateFee,
//                  ppro_setup        = :pproSetup,
//                  ppro_mon          = :pproMon,
//                  plink_setup       = :plinkSetup,
//                  plink_mon         = :plinkMon,
//                  wrless_setup      = :wirelessSetup,
//                  wrless_mon        = :wirelessMon,
//                  payflow           = :payflow ? "Y" : "N",
//                  pflow_type        = :payflowType
//          where   app_seq_num       = :appSeqNum and
//                  process_job       = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4866 = payflow ? "Y" : "N";
   String theSqlTS = "update  transcom_process_merchant\n        set     mer_app_fee       =  :1 ,\n                rp_fee            =  :2 ,\n                batch_fee         =  :3 ,\n                ann_fee           =  :4 ,\n                debit_access_fee  =  :5 ,\n                other_fee_1       =  :6 ,\n                other_fee_2       =  :7 ,\n                chargeback_fee    =  :8 ,\n                statement_fee     =  :9 ,\n                training_fee      =  :10 ,\n                int_rep_fee       =  :11 ,\n                pp_swap_fee       =  :12 ,\n                int_start_fee     =  :13 ,\n                int_gate_fee      =  :14 ,\n                ppro_setup        =  :15 ,\n                ppro_mon          =  :16 ,\n                plink_setup       =  :17 ,\n                plink_mon         =  :18 ,\n                wrless_setup      =  :19 ,\n                wrless_mon        =  :20 ,\n                payflow           =  :21 ,\n                pflow_type        =  :22 \n        where   app_seq_num       =  :23  and\n                process_job       =  :24";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,merAppFee);
   __sJT_st.setDouble(2,rpFee);
   __sJT_st.setDouble(3,batchFee);
   __sJT_st.setDouble(4,annFee);
   __sJT_st.setDouble(5,debitAccessFee);
   __sJT_st.setDouble(6,otherFee1);
   __sJT_st.setDouble(7,otherFee2);
   __sJT_st.setDouble(8,chargebackFee);
   __sJT_st.setDouble(9,statementFee);
   __sJT_st.setDouble(10,trainingFee);
   __sJT_st.setDouble(11,intRepFee);
   __sJT_st.setDouble(12,ppSwapFee);
   __sJT_st.setDouble(13,intStartFee);
   __sJT_st.setDouble(14,intGateFee);
   __sJT_st.setDouble(15,pproSetup);
   __sJT_st.setDouble(16,pproMon);
   __sJT_st.setDouble(17,plinkSetup);
   __sJT_st.setDouble(18,plinkMon);
   __sJT_st.setDouble(19,wirelessSetup);
   __sJT_st.setDouble(20,wirelessMon);
   __sJT_st.setString(21,__sJT_4866);
   __sJT_st.setString(22,payflowType);
   __sJT_st.setLong(23,appSeqNum);
   __sJT_st.setInt(24,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:997^7*/
    }
    catch(Exception e)
    {
      setError("getMiscCharges(" + appSeqNum + ")" + e.toString());
      logEntry("getMiscCharges(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getTranCharges(long appSeqNum)
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    double            mercDiscRate    = 0.0;
    double            merItemFee      = 0.0;
    double            merTeRate       = 0.0;
    double            merDebitRate    = 0.0;
    String            downgradeType   = "";
    double            tranSurcharge   = 0.0;
    String            monthlyMinFlag  = "";
    double            monthlyMinAmt   = 0.0;
    double            jcbRate         = 0.0;
    double            dinersRate      = 0.0;
    double            ebtRate         = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1029^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate,
//                  tranchrg_per_item,
//                  tranchrg_per_tran,
//                  tranchrg_discrate_type,
//                  tranchrg_interchangefee_fee,
//                  tranchrg_pass_thru,
//                  tranchrg_mmin_chrg,
//                  cardtype_code
//          from    tranchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate,\n                tranchrg_per_item,\n                tranchrg_per_tran,\n                tranchrg_discrate_type,\n                tranchrg_interchangefee_fee,\n                tranchrg_pass_thru,\n                tranchrg_mmin_chrg,\n                cardtype_code\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1041^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int code = rs.getInt("cardtype_code");
        int plan = rs.getInt("tranchrg_interchangefee_fee");
        
        switch(code)
        {
          case mesConstants.APP_CT_VISA:
            mercDiscRate   = rs.getDouble("tranchrg_disc_rate");
            merItemFee    = rs.getDouble("tranchrg_pass_thru");
            monthlyMinAmt = rs.getDouble("tranchrg_mmin_chrg");
            
            if(monthlyMinAmt > 0.0)
            {
              monthlyMinFlag = "Y";
            }
            else
            {
              monthlyMinFlag = "N";
            }
            
            switch(plan)
            {
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              case 7:
              case 8:
              case 9:
              case 10:
              case 11:
              default:
                downgradeType = "B";
                break;
                
              case 12:
              case 13:
              case 14:
              case 15:
              case 17:
              case 18:
                downgradeType = "D";
                break;
                
              case 16:
                downgradeType = "I";
                break;
                
            }
            break;
            
          case mesConstants.APP_CT_DISCOVER:
          case mesConstants.APP_CT_AMEX:
            if(rs.getDouble("tranchrg_per_tran") > merTeRate)
            {
              merTeRate = rs.getDouble("tranchrg_per_tran");
            }
            break;
            
          case mesConstants.APP_CT_JCB:
            jcbRate = rs.getDouble("tranchrg_per_tran");
            break;
            
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersRate = rs.getDouble("tranchrg_per_tran");
            break;
            
          case mesConstants.APP_CT_DEBIT:
            merDebitRate = rs.getDouble("tranchrg_per_tran");
            break;
            
          case mesConstants.APP_CT_EBT:
            ebtRate = rs.getDouble("tranchrg_per_tran");
            break;
        }
      }
      
      rs.close();
      it.close();
      
      // update the transcom_process_merchant table
      /*@lineinfo:generated-code*//*@lineinfo:1129^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     merc_disc_rate    = :mercDiscRate,
//                  mer_item_fee      = :merItemFee,
//                  mer_te_rate       = :merTeRate,
//                  mer_debit_rate    = :merDebitRate,
//                  downgrade_type    = :downgradeType,
//                  tran_surcharge    = :tranSurcharge,
//                  monthly_min_flag  = :monthlyMinFlag,
//                  monthly_min_amt   = :monthlyMinAmt,
//                  jcb_rate          = :jcbRate,
//                  diners_rate       = :dinersRate,
//                  ebt_rate          = :ebtRate
//          where   app_seq_num       = :appSeqNum and
//                  process_job       = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     merc_disc_rate    =  :1 ,\n                mer_item_fee      =  :2 ,\n                mer_te_rate       =  :3 ,\n                mer_debit_rate    =  :4 ,\n                downgrade_type    =  :5 ,\n                tran_surcharge    =  :6 ,\n                monthly_min_flag  =  :7 ,\n                monthly_min_amt   =  :8 ,\n                jcb_rate          =  :9 ,\n                diners_rate       =  :10 ,\n                ebt_rate          =  :11 \n        where   app_seq_num       =  :12  and\n                process_job       =  :13";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,mercDiscRate);
   __sJT_st.setDouble(2,merItemFee);
   __sJT_st.setDouble(3,merTeRate);
   __sJT_st.setDouble(4,merDebitRate);
   __sJT_st.setString(5,downgradeType);
   __sJT_st.setDouble(6,tranSurcharge);
   __sJT_st.setString(7,monthlyMinFlag);
   __sJT_st.setDouble(8,monthlyMinAmt);
   __sJT_st.setDouble(9,jcbRate);
   __sJT_st.setDouble(10,dinersRate);
   __sJT_st.setDouble(11,ebtRate);
   __sJT_st.setLong(12,appSeqNum);
   __sJT_st.setInt(13,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1145^7*/
    }
    catch(Exception e)
    {
      setError("getTranCharges(" + appSeqNum + ")" + e.toString());
      logEntry("getTranCharges(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getPaymentOptions(long appSeqNum)
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    String            amexDiscFlag  = "N";
    String            splitDialFlag = "N";
    String            debitFlag     = "N";
    String            rapFlag       = "N";
    String            esaFlag       = "N";
    String            ebtFlag       = "N";
    String            amexSE        = "";
    String            discoverSE    = "";
    String            teFlag        = "N";
    
    boolean           amexFlag      = false;
    boolean           discFlag      = false;
    
    try
    {
      // get the payment options
      /*@lineinfo:generated-code*//*@lineinfo:1179^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  merchpo_split_dial,
//                  merchpo_rate,
//                  merchpo_card_merch_number
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                merchpo_split_dial,\n                merchpo_rate,\n                merchpo_card_merch_number\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1187^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int     code        = rs.getInt("cardtype_code");
        String  splitFlag   = rs.getString("merchpo_split_dial");
        double  rate        = rs.getDouble("merchpo_rate");
        String  SE          = blankIfNull(rs.getString("merchpo_card_merch_number"));
        
        if(SE.equals("0"))
        {
          SE = "";
        }
        
        switch(code)
        {
          case mesConstants.APP_CT_DISCOVER:
            discFlag      = true;
            teFlag        = "Y";
            // commented out because there is no Split dial option for discover
            // splitDialFlag = splitFlag;
            if(rate > 0.0)
            {
              rapFlag = "Y";
            }
            else
            {
              rapFlag = "N";
            }
            discoverSE    = SE;
            break;
            
          case mesConstants.APP_CT_AMEX:
            amexFlag      = true;
            teFlag        = "Y";
            splitDialFlag = splitFlag;
            if(rate > 0.0)
            {
              esaFlag = "Y";
            }
            else
            {
              esaFlag = "N";
            }
            amexSE        = SE;
            break;
            
          case mesConstants.APP_CT_DEBIT:
            debitFlag = "Y";
            break;
            
          case mesConstants.APP_CT_EBT:
            ebtFlag = "Y";
            break;
            
          default:
            break;
        }
      }
      
      // set the amexDiscFlag ('B' = both, 'A' = amex only, 'D' = discover only
      if(amexFlag && discFlag)
      {
        amexDiscFlag = "B";
      }
      else if(amexFlag && !discFlag)
      {
        amexDiscFlag = "A";
      }
      else if(discFlag && !amexFlag)
      {
        amexDiscFlag = "D";
      }
      
      rs.close();
      it.close();
      
      // update the transcom_process_merchant table
      /*@lineinfo:generated-code*//*@lineinfo:1267^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     amex_disc_flag  = :amexDiscFlag,
//                  split_dial_flag = :splitDialFlag,
//                  debit_flag      = :debitFlag,
//                  rap_flag        = :rapFlag,
//                  esa_flag        = :esaFlag,
//                  ebt_flag        = :ebtFlag,
//                  amex_se         = :amexSE,
//                  discover_se     = :discoverSE,
//                  te_flag         = :teFlag
//          where   app_seq_num     = :appSeqNum and
//                  process_job     = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     amex_disc_flag  =  :1 ,\n                split_dial_flag =  :2 ,\n                debit_flag      =  :3 ,\n                rap_flag        =  :4 ,\n                esa_flag        =  :5 ,\n                ebt_flag        =  :6 ,\n                amex_se         =  :7 ,\n                discover_se     =  :8 ,\n                te_flag         =  :9 \n        where   app_seq_num     =  :10  and\n                process_job     =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,amexDiscFlag);
   __sJT_st.setString(2,splitDialFlag);
   __sJT_st.setString(3,debitFlag);
   __sJT_st.setString(4,rapFlag);
   __sJT_st.setString(5,esaFlag);
   __sJT_st.setString(6,ebtFlag);
   __sJT_st.setString(7,amexSE);
   __sJT_st.setString(8,discoverSE);
   __sJT_st.setString(9,teFlag);
   __sJT_st.setLong(10,appSeqNum);
   __sJT_st.setInt(11,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1281^7*/
    }
    catch(Exception e)
    {
      setError("getPaymentOptions(" + appSeqNum + ")" + e.toString());
      logEntry("getPaymentOptions(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getBusinessOwners(long appSeqNum)
  {
    ResultSetIterator it                = null;
    ResultSet         rs                = null;
    
    String            priFirst          = "";
    String            priLast           = "";
    String            priSSN            = "";
    String            priTitle          = "";
    int               priOwnSinceMonth  = 0;
    int               priOwnSinceYear   = 0;
    double            priOwnPercentage  = 0.0;
    String            secFirst          = "";
    String            secLast           = "";
    String            secSSN            = "";
    String            secTitle          = "";
    int               secOwnSinceMonth  = 0;
    int               secOwnSinceYear   = 0;
    double            secOwnPercentage  = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1317^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name,
//                  busowner_last_name,
//                  nvl(dukpt_decrypt_wrapper(busowner_ssn_enc), busowner_ssn) as busowner_ssn,
//                  busowner_title,
//                  busowner_period_month,
//                  busowner_period_year,
//                  busowner_owner_perc,
//                  busowner_num
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name,\n                busowner_last_name,\n                nvl(dukpt_decrypt_wrapper(busowner_ssn_enc), busowner_ssn) as busowner_ssn,\n                busowner_title,\n                busowner_period_month,\n                busowner_period_year,\n                busowner_owner_perc,\n                busowner_num\n        from    businessowner\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1329^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int ownerType = rs.getInt("busowner_num");
        
        switch(ownerType)
        {
          case mesConstants.BUS_OWNER_PRIMARY:
            priFirst          = blankIfNull(rs.getString("busowner_first_name"));
            priLast           = blankIfNull(rs.getString("busowner_last_name"));
            priSSN            = blankIfNull(rs.getString("busowner_ssn"));
            priTitle          = blankIfNull(rs.getString("busowner_title"));
            priOwnSinceMonth  = rs.getInt("busowner_period_month");
            priOwnSinceYear   = rs.getInt("busowner_period_year");
            priOwnPercentage  = rs.getDouble("busowner_owner_perc");
            break;
            
          case mesConstants.BUS_OWNER_SECONDARY:
            secFirst          = blankIfNull(rs.getString("busowner_first_name"));
            secLast           = blankIfNull(rs.getString("busowner_last_name"));
            secSSN            = blankIfNull(rs.getString("busowner_ssn"));
            secTitle          = blankIfNull(rs.getString("busowner_title"));
            secOwnSinceMonth  = rs.getInt("busowner_period_month");
            secOwnSinceYear   = rs.getInt("busowner_period_year");
            secOwnPercentage  = rs.getDouble("busowner_owner_perc");
            break;
        }
      }
      
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:1364^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     pri_first           = :priFirst,
//                  pri_last            = :priLast,
//                  pri_ssn             = :priSSN,
//                  pri_title           = :priTitle,
//                  pri_own_since_month = :priOwnSinceMonth,
//                  pri_own_since_year  = :priOwnSinceYear,
//                  pri_own_perc        = :priOwnPercentage,
//                  sec_first           = :secFirst,
//                  sec_last            = :secLast,
//                  sec_ssn             = :secSSN,
//                  sec_title           = :secTitle,
//                  sec_own_since_month = :secOwnSinceMonth,
//                  sec_own_since_year  = :secOwnSinceYear,
//                  sec_own_perc        = :secOwnPercentage
//          where   app_seq_num         = :appSeqNum and
//                  process_job         = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     pri_first           =  :1 ,\n                pri_last            =  :2 ,\n                pri_ssn             =  :3 ,\n                pri_title           =  :4 ,\n                pri_own_since_month =  :5 ,\n                pri_own_since_year  =  :6 ,\n                pri_own_perc        =  :7 ,\n                sec_first           =  :8 ,\n                sec_last            =  :9 ,\n                sec_ssn             =  :10 ,\n                sec_title           =  :11 ,\n                sec_own_since_month =  :12 ,\n                sec_own_since_year  =  :13 ,\n                sec_own_perc        =  :14 \n        where   app_seq_num         =  :15  and\n                process_job         =  :16";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,priFirst);
   __sJT_st.setString(2,priLast);
   __sJT_st.setString(3,priSSN);
   __sJT_st.setString(4,priTitle);
   __sJT_st.setInt(5,priOwnSinceMonth);
   __sJT_st.setInt(6,priOwnSinceYear);
   __sJT_st.setDouble(7,priOwnPercentage);
   __sJT_st.setString(8,secFirst);
   __sJT_st.setString(9,secLast);
   __sJT_st.setString(10,secSSN);
   __sJT_st.setString(11,secTitle);
   __sJT_st.setInt(12,secOwnSinceMonth);
   __sJT_st.setInt(13,secOwnSinceYear);
   __sJT_st.setDouble(14,secOwnPercentage);
   __sJT_st.setLong(15,appSeqNum);
   __sJT_st.setInt(16,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1383^7*/
    }
    catch(Exception e)
    {
      setError("getBusinessOwners(" + appSeqNum + ")" + e.toString());
      logEntry("getBusinessOwners(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  };
  
  private void getDateApproved(long appSeqNum)
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    Date              dateApproved  = null;
    
    try
    {
      // get the date approved
      /*@lineinfo:generated-code*//*@lineinfo:1406^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  date_completed
//          from    app_tracking
//          where   app_seq_num = :appSeqNum and
//                  dept_code   = :QueueConstants.DEPARTMENT_CREDIT and
//                  status_code = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  date_completed\n        from    app_tracking\n        where   app_seq_num =  :1  and\n                dept_code   =  :2  and\n                status_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(3,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1413^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        dateApproved = rs.getDate("date_completed");
      }
      
      rs.close();
      it.close();
      
      // update the transcom_process_merchant table
      /*@lineinfo:generated-code*//*@lineinfo:1426^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     date_appr   = :dateApproved
//          where   app_seq_num = :appSeqNum and
//                  process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     date_appr   =  :1 \n        where   app_seq_num =  :2  and\n                process_job =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,dateApproved);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1432^7*/
    }
    catch(Exception e)
    {
      setError("getDateApproved(" + appSeqNum + ")" + e.toString());
      logEntry("getDateApproved(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadCSVFile(Vector unused)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      csvFile.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rectype,
//                  user_id,
//                  mer_name,
//                  mer_status,
//                  date_sub,
//                  date_appr,
//                  date_first_dep,
//                  mer_app_fee,
//                  rp_fee,
//                  train_by,
//                  mer_id,
//                  mer_control_num,
//                  ann_vmc_vol,
//                  avg_ticket,
//                  ann_tran_cnt,
//                  ann_vol,
//                  merc_disc_rate,
//                  mer_item_fee,
//                  rmi,
//                  amex_disc_flag,
//                  te_flag,
//                  split_dial_flag,
//                  debit_flag,
//                  mer_te_rate,
//                  mer_debit_rate,
//                  downgrade_type,
//                  tran_surcharge,
//                  monthly_min_flag,
//                  monthly_min_amt,
//                  check_recvd_flag,
//                  check_recvd_amt,
//                  mer_addr,
//                  mer_city,
//                  mer_state,
//                  mer_zip,
//                  mer_phone,
//                  mer_contact,
//                  mail_addr,
//                  mail_city,
//                  mail_state,
//                  mail_zip,
//                  sic_code,
//                  network,
//                  insurance_flag,
//                  jcb_rate,
//                  diners_rate,
//                  rap_flag,
//                  esa_flag,
//                  batch_fee,
//                  ann_fee,
//                  debit_access_fee,
//                  liability,
//                  mer_legal_name,
//                  other_fee_1,
//                  other_fee_1_desc,
//                  other_fee_2,
//                  other_fee_2_desc,
//                  chargeback_fee,
//                  ebt_flag,
//                  ebt_rate,
//                  statement_fee,
//                  prev_statement_flag,
//                  amex_se,
//                  discover_se,
//                  date_submitted,
//                  date_trained,
//                  comments,
//                  date_recvd,
//                  training_fee,
//                  num_reprog,
//                  mer_dda,
//                  mer_tr,
//                  mer_bank_name,
//                  perc_mail,
//                  perc_internet,
//                  perc_phone,
//                  num_locs,
//                  cont_last,
//                  bus_fax,
//                  bus_email,
//                  tax_id,                
//                  inc_status,
//                  pri_first,
//                  pri_last,
//                  pri_ssn,
//                  pri_address,
//                  pri_city,
//                  pri_state,
//                  pri_zip,
//                  pri_phone,
//                  pri_title,
//                  pri_own_since_month,
//                  pri_own_since_year,
//                  pri_own_perc,
//                  sec_first,
//                  sec_last,
//                  sec_ssn,
//                  sec_address,
//                  sec_city,
//                  sec_state,
//                  sec_zip,
//                  sec_phone,
//                  sec_title,
//                  sec_own_since_month,
//                  sec_own_since_year,
//                  sec_own_perc,
//                  int_rep_fee,
//                  pp_swap_fee,
//                  int_start_fee,
//                  int_gate_fee,
//                  ppro_setup,
//                  plink_setup,
//                  ppro_mon,
//                  plink_mon,
//                  payflow,
//                  pflow_type,
//                  visa_bet,
//                  mc_bet,
//                  wrless_setup,
//                  wrless_mon,
//                  ch_present,
//                  ch_not_present
//          from    transcom_process_merchant
//          where   process_job = :procSequence
//          order by  job_sequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rectype,\n                user_id,\n                mer_name,\n                mer_status,\n                date_sub,\n                date_appr,\n                date_first_dep,\n                mer_app_fee,\n                rp_fee,\n                train_by,\n                mer_id,\n                mer_control_num,\n                ann_vmc_vol,\n                avg_ticket,\n                ann_tran_cnt,\n                ann_vol,\n                merc_disc_rate,\n                mer_item_fee,\n                rmi,\n                amex_disc_flag,\n                te_flag,\n                split_dial_flag,\n                debit_flag,\n                mer_te_rate,\n                mer_debit_rate,\n                downgrade_type,\n                tran_surcharge,\n                monthly_min_flag,\n                monthly_min_amt,\n                check_recvd_flag,\n                check_recvd_amt,\n                mer_addr,\n                mer_city,\n                mer_state,\n                mer_zip,\n                mer_phone,\n                mer_contact,\n                mail_addr,\n                mail_city,\n                mail_state,\n                mail_zip,\n                sic_code,\n                network,\n                insurance_flag,\n                jcb_rate,\n                diners_rate,\n                rap_flag,\n                esa_flag,\n                batch_fee,\n                ann_fee,\n                debit_access_fee,\n                liability,\n                mer_legal_name,\n                other_fee_1,\n                other_fee_1_desc,\n                other_fee_2,\n                other_fee_2_desc,\n                chargeback_fee,\n                ebt_flag,\n                ebt_rate,\n                statement_fee,\n                prev_statement_flag,\n                amex_se,\n                discover_se,\n                date_submitted,\n                date_trained,\n                comments,\n                date_recvd,\n                training_fee,\n                num_reprog,\n                mer_dda,\n                mer_tr,\n                mer_bank_name,\n                perc_mail,\n                perc_internet,\n                perc_phone,\n                num_locs,\n                cont_last,\n                bus_fax,\n                bus_email,\n                tax_id,                \n                inc_status,\n                pri_first,\n                pri_last,\n                pri_ssn,\n                pri_address,\n                pri_city,\n                pri_state,\n                pri_zip,\n                pri_phone,\n                pri_title,\n                pri_own_since_month,\n                pri_own_since_year,\n                pri_own_perc,\n                sec_first,\n                sec_last,\n                sec_ssn,\n                sec_address,\n                sec_city,\n                sec_state,\n                sec_zip,\n                sec_phone,\n                sec_title,\n                sec_own_since_month,\n                sec_own_since_year,\n                sec_own_perc,\n                int_rep_fee,\n                pp_swap_fee,\n                int_start_fee,\n                int_gate_fee,\n                ppro_setup,\n                plink_setup,\n                ppro_mon,\n                plink_mon,\n                payflow,\n                pflow_type,\n                visa_bet,\n                mc_bet,\n                wrless_setup,\n                wrless_mon,\n                ch_present,\n                ch_not_present\n        from    transcom_process_merchant\n        where   process_job =  :1 \n        order by  job_sequence";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.TranscomMerchantMES",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1581^7*/
      
      rs = it.getResultSet();
      
      // set the header
      csvFile.setHeader(rs);
      
      // add the rows
      csvFile.addRows(rs, "MM/dd/yyyy");
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      setError("loadCSVFile()" + e.toString());
      logEntry("loadCSVFile()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void markFilesProcessed()
  {
    try
    {
      // make sure the transcom_process table is updated
      super.markFilesProcessed();
      
      /*@lineinfo:generated-code*//*@lineinfo:1613^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_merchant
//          set     process_date = sysdate
//          where   process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_merchant\n        set     process_date = sysdate\n        where   process_job =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1618^7*/
    }
    catch(Exception e)
    {
      logEntry("markFilesProcessed()", e.toString());
    }
  }
  
  private void getSummaryData(long appSeqNum)
  {
    try
    {
      // get address data
      getAddressData(appSeqNum);
      
      // get miscellaneous charges
      getMiscCharges(appSeqNum);
      
      // get transaction charges
      getTranCharges(appSeqNum);
      
      // get payment options
      getPaymentOptions(appSeqNum);
      
      // get date approved
      getDateApproved(appSeqNum);
      
      // get business owner info
      getBusinessOwners(appSeqNum);
    }
    catch(Exception e)
    {
      setError("getSummaryData(" + appSeqNum + ")" + e.toString());
      logEntry("getSummaryData(" + appSeqNum + ")", e.toString());
    }
  }

  private void collectAppData()
  {
    for(int i=0; i<apps.size(); ++i)
    {
      long appSeqNum = ((Long)apps.elementAt(i)).longValue();
      // get easy ont-to-one data
      getOneToOneData(appSeqNum, i);
      
      // get multi-row (summary) data
      getSummaryData(appSeqNum);
    }
  }
  
  private void setTranscomProcess()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1672^7*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_process
//          (
//            file_type,
//            process_sequence,
//            load_filename
//          )
//          values
//          (
//            :FILE_TYPE_MERCHANT,
//            :procSequence,
//            'merchant'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_process\n        (\n          file_type,\n          process_sequence,\n          load_filename\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          'merchant'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.TranscomMerchantMES",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FILE_TYPE_MERCHANT);
   __sJT_st.setInt(2,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1686^7*/
    }
    catch(Exception e)
    {
      setError("setTranscomProcess()" + e.toString());
      logEntry("setTranscomProcess()", e.toString());
    }
  }
  
  protected void preProcess()
  {
    try
    {
      mesFile = true;
      
      // get all the necessary data in the transcom_merchant_update table
      markAppsToProcess();
    
      // collect the app info into the transcom_process_merchant table
      if(! getError())
      {
        collectAppData();
      }
    
      // put an entry in the transcom_process table so that the load will occur
      if(! getError())
      {
        setTranscomProcess();
      }
    }
    catch(Exception e)
    {
      setError("preProcess()" + e.toString());
      logEntry("preProcess()", e.toString());      
    }
  }
  
  public static void main( String[] args )
  {
    TranscomMerchantMES         tpsMerch    = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
    
      tpsMerch = new TranscomMerchantMES();
      
      tpsMerch.connect();
      tpsMerch.execute();
    }
    catch( Exception e )
    {
      log.error("ERROR: " + e.toString());
    }
    finally
    {
      try{ tpsMerch.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }      
  }
}/*@lineinfo:generated-code*/