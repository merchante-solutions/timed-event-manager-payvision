package com.mes.aus.file.job;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class AutoLoaderJob extends Job {
	static Logger log = Logger.getLogger(AutoLoaderJob.class);
	private boolean testFlag;

	public AutoLoaderJob(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	public AutoLoaderJob(boolean testFlag) {
		this(true, testFlag);
	}
	public void run() {
		try {
			System.out.println("Looking for profiles to auto load...");
			List loadList = db.getAutoLoadProfiles(testFlag);
			for (Iterator i = loadList.iterator(); i.hasNext();) {
				String profileId = "" + i.next();
				try {
					System.out.println("Auto loading profile: " + profileId);
					db.autoLoadProfile(profileId, testFlag);
				}
				catch (Exception ie) {
					log.error("Error: " + ie);
					notifyError(ie, "inner run exception, " + profileId);
				}
				finally {
					db.setLastAutoLoad(profileId);
				}
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}
}