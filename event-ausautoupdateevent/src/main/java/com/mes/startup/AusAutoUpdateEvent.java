package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.job.AutoLoaderJob;

public final class AusAutoUpdateEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusAutoUpdateEvent.class);

  public boolean execute()
  {
    try
    {
      boolean testFlag = "test".equals(getEventArg(0).toLowerCase());
      System.out.println("testFlag="+testFlag);
      log.debug("Testing log4j...");
      (new AutoLoaderJob(testFlag)).run();
      return true;
    }
    catch (Exception e)
    {
      log.error("AUS request file process event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}