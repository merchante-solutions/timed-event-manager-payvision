/*@lineinfo:filename=LoadApprovedApps*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/LoadApprovedApps.sqlj $

  Description:

    LoadApprovedApps

    Insert info for completed applications into MIF and other db tables.

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-09-14 09:55:55 -0700 (Mon, 14 Sep 2015) $
  Version            : $Revision: 23827 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;


import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Vector;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.db.mbs.MBSDefaultFees;
import com.mes.db.mbs.MBSDefaultFees.ChargeItem;
import com.mes.db.mbs.MBSDefaultFees.FeeItem;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;


public class LoadApprovedApps extends EventBase
{
  public static final int CT_VS                           =   0;
  public static final int CT_MC                           =   1;
  public static final int CT_DISC                         =   2;
  public static final int CT_DINR                         =   3;
  public static final int CT_JCB                          =   4;
  public static final int CT_AMEX                         =   5;
  public static final int CT_DEBIT                        =   6;
  public static final int CT_PVTL1                        =   7;
  public static final int CT_LEN                          =   8;  // number of card types in the arrays

  public static final int MBS_ELEMENTS_AUTH               =   1;
  public static final int MBS_ELEMENTS_AVS                =   2;
  public static final int MBS_ELEMENTS_CHARGEBACK         =   5;
  public static final int MBS_ELEMENTS_APF                =   7;
  public static final int MBS_ELEMENTS_BATCH              =   8;
  public static final int MBS_ELEMENTS_ARU                =   9;
  public static final int MBS_ELEMENTS_ZFL                =  10;
  public static final int MBS_ELEMENTS_VMA                =  11;
  public static final int MBS_ELEMENTS_PIN_DEBIT_NTWRK_PARTICIPATION = 17;
  public static final int MBS_ELEMENTS_DISCOUNT           = 101;
  public static final int MBS_ELEMENTS_NABU               = 102;
  public static final int MBS_ELEMENTS_DISCOUNT_MIN       = 103;
  public static final int MBS_ELEMENTS_XBORDER            = 113;
  public static final int MBS_ELEMENTS_ISA                = 115;
  public static final int MBS_ELEMENTS_IAF                = 116;
  public static final int MBS_ELEMENTS_DATA_TRANSMISSION  = 120;
  public static final int MBS_ELEMENTS_PIN_DEBIT          = 121;
  public static final int MBS_ELEMENTS_EBT_DEBIT          = 122;
  public static final int MBS_ELEMENTS_AM_OPTB_NETWORK_FEE   = 128;
  public static final int MBS_ELEMENTS_CREDIT_FEE            = 200;
  public static final int MBS_ELEMENTS_SETTLEMENT_FEE        = 201;
  public static final int MBS_ELEMENTS_CREDIT_VOUCHER_DEBIT  = 209;
  public static final int MBS_ELEMENTS_CREDIT_VOUCHER_CREDIT = 210;
  public static final int MBS_ELEMENTS_VS_FIXED_ACCESS       = 212;
  public static final int MBS_ELEMENTS_MC_FIXED_ACCESS       = 214;
  public static final int MBS_ELEMENTS_AM_OPTB_INBOUND_FEE   = 215;
  public static final int MBS_ELEMENTS_VS_CARD_VEFIFY_FEE    = 216;
  public static final int MBS_ELEMENTS_MC_CARD_VEFIFY_FEE    = 218;
  public static final int MBS_ELEMENTS_AM_OPTB_NON_SWIPE_TRAN_FEE   = 220;
  public static final int MBS_ELEMENTS_AM_OPTB_NON_COMPL_FEE        = 221;
  public static final int MBS_ELEMENTS_AM_OPTB_DATA_QUALITY_FEE     = 222;

  private boolean         errors                            ;
  private static final ArrayList<String> excludeNonSwipeFeeMCCList = new ArrayList<String>(
          Arrays.asList("5960","5968","6300","8211","8220","8351","9211","9222","9399","8398","8661"));
          
  private Vector          billInfoRows                    = new Vector();

  private Calendar        cal_01_thisMonth                  ; //  1st of this month
  private Calendar        cal_01_thisOrNext                 ; //  1st of this month if <= 15th,  1st of next month if       > 15th
  private Calendar        cal_15_nextOrFollowing            ; // 15th of next month if <= 15th, 15th of month after next if > 15th
  private Calendar        cal_31_Dec_9999                   ; // 31st of December, 9999 (used for never-expires billing)

  String  merchNameBusiness                               = null;
  public  String[]        mif_X_name                      = new String[3];      // varchar2(32 byte)
  public  String[]        mif_X_addr1                     = new String[3];      // varchar2(32 byte)
  public  String[]        mif_X_addr2                     = new String[3];      // varchar2(32 byte)
  public  String[]        mif_X_city                      = new String[3];      // varchar2(24 byte)
  public  String[]        mif_X_state                     = new String[3];      // varchar2(2 byte)
  public  String[]        mif_X_zip                       = new String[3];      // varchar2(9 byte)
  public  String[]        mif_X_transit_routing           = new String[2];      // number(9) or varchar2(9 byte)
  public  String[]        mif_X_dda                       = new String[2];      // varchar2(17 byte)
  public  String[]        mif_X_acct_type                 = new String[2];      // varchar2(2 byte)
//public  String[]        mif_dXXaccpt                    = new String[CT_LEN]; // varchar2(1 byte)
  public  String[]        mif_X_disc_table                = new String[CT_LEN]; // varchar2(4 byte)
  public  String[]        mif_X_disc_method               = new String[CT_LEN]; // varchar2(4 byte)
  public  String[]        mif_X_disc_type                 = new String[CT_LEN]; // char(1 byte)
  public  int[]           mif_X_disc_rate                 = new int   [CT_LEN]; // number(7) (3 implied decimal places)
  public  int[]           mif_X_per_item                  = new int   [CT_LEN]; // number(7) (3 implied decimal places)
  public  String[]        mif_X_plan                      = new String[CT_LEN]; // varchar2(2 byte)
  public  long[]          mif_X_mid                       = new long  [CT_LEN]; // disc=number(15) & amex/dinr=varchar2(10 byte)
  public  String          mif_load_filename                 ; // varchar2(32 byte)

  public  mifInfo         mif;
  public class mifInfo
  {
    public  long            merchant_number                   ; // number(16)
    public  int             bank_number                       ; // number(4)
    public  String          dmagent                           ; // varchar2(6 byte)
    public  String          sic_code                          ; // char(4 byte)
    public  String          class_code                        ; // char(4 byte)
    public  long            federal_tax_id                    ; // number(9)
    public  String          ssn                               ; // varchar2(9 byte)
    public  String          license_number                    ; // varchar2(9 byte)
    public  String          owner_name                        ; // varchar2(25 byte)
    public  String          manager_name                      ; // varchar2(25 byte)
    public  String          fdr_corp_name                     ; // varchar2(25 byte)
    public  long            phone_1                           ; // varchar2(10 byte)
    public  long            phone_2_fax                       ; // varchar2(10 byte)
    public  long            customer_service_phone            ; // varchar2(10 byte)
    public  String          cat_interc_value_flag           = "N";  // char(1 byte)             // default: N; sic 5542: Y
    public  String          print_statements                  ; // varchar2(1 byte)
    public  String          edc_flagst_other_3                ; // char(1 byte)
    public  String          merit_eligibility               = "N";  // char(1 byte)
    public  String          dmer3ind                        = "N";  // varchar2(1 byte)
    public  String          daily_discount_interchange      = "N";  // varchar2(1 byte)         // default: N; other options: D,I,B
    public  int             suspended_days                    ; // number(3)
    public  double          minimum_monthly_discount          ; // number(7,2)
    public  int             ic_bet_visa = 0                   ; // number(4)
    public  int             ic_bet_mc                         ; // number(4)
    public  String          rep_code                          ; // varchar2(4 byte)
    public  String          user_data_1                       ; // varchar2(4 byte)
    public  String          user_data_2                       ; // varchar2(4 byte)
    public  String          user_data_3                       ; // varchar2(4 byte)
    public  String          user_data_4                       ; // varchar2(16 byte)
    public  String          user_data_5                       ; // varchar2(16 byte)
    public  String          user_acount_1                     ; // varchar2(16 byte)
    public  String          user_account_2                    ; // varchar2(16 byte)
    public  String          dbfemid                           ; // varchar2(10 byte)
    public  String          dbfepflg                          ; // varchar2(1 byte)
    public  String          inv_code_investigate              ; // varchar2(4 byte)
    public  String          met_table                         ; // varchar2(4 byte)
    public  int             num_post_date_days_debit          ; // number(2)
    public  int             num_post_date_days_credt          ; // number(2)
    public  String          merchant_type                     ; // varchar2(4 byte)
    public  String          daily_ach                         ; // varchar2(1 byte)
    public  String          visa_fanf                         ; // varchar2(1 byte)
    public  String          mc_merch_loc_fee                  ; // varchar2(1 byte)

    public  int             ofac                              ; //number(1)
    public  int             irs_match_result                  ; //number(1)
    public  int             irs_tax_type                      ; //number(1)
    public  String          irs_exemptions                    ; //varchar2(1)
    public  String          debit_pass_through              = "N";
    public  String          exception_merchants_code        = "";
    public  String          pacwest_with_exception          = "N";
    public  String          remit_fees                      = "N";  // varchar2(1 byte)
    // constructor
    mifInfo() {}
  }

  public class BillingInfo
  {                                   //  mbs_pricing...............................  mbs_charge_records......................................
    public int      volumeType      ; //  .volume_type      = 1 (same for all)
    public int      itemTypeInt     ; //  .item_type        = mbs_elements.item_type  .charge_subtype   = null
    public String   itemTypeString  ; //  .item_subclass    = cardType/null/??        .charge_type      = mbs_charge_records_types.charge_type
    public String   statementMsg    ; //                                              .statement_message
    public double   multiplier      ; //  .rate             = percent of tran amt     .item_count       = # of items to charge
    public double   chargePerItem   ; //  .per_item         = per item fee            .charge_amount    = charge per item
    public String   billingMonths   ; //  .billing_months                             .billing_frequency
    public Date     dateStart       ; //  .valid_date_begin                           .start_date
    public Date     dateEnd         ; //  .valid_date_end                             .end_date

    // constructor with minimal info
    public BillingInfo( int thisItemTypeInt, double thisChargePerItem )
    {
      this( thisItemTypeInt, null, 0, thisChargePerItem, 1 );
    }
    
    public BillingInfo( int thisItemTypeInt, double thisMultiplier, double thisChargePerItem )
    {
      this( thisItemTypeInt, null, thisMultiplier, thisChargePerItem, 1 );
    }

    // copy constructor
    public BillingInfo( BillingInfo item )
    {
      this( item.itemTypeInt,
            item.itemTypeString,
            item.multiplier,
            item.chargePerItem,
            item.volumeType );

      billingMonths = item.billingMonths;
      dateStart     = item.dateStart;
      dateEnd       = item.dateEnd;
      statementMsg  = item.statementMsg;
    }

    // constructor with more info
    public BillingInfo( int thisItemTypeInt, String thisItemTypeString, double thisMultiplier, double thisChargePerItem, int thisVolumeType)
    {
      itemTypeInt     = thisItemTypeInt             ;
      itemTypeString  = thisItemTypeString          ;
      statementMsg    = null                        ;
      multiplier      = thisMultiplier              ;
      chargePerItem   = thisChargePerItem           ;
      billingMonths   = "YYYYYYYYYYYY"              ;
      dateStart       = new Date(cal_01_thisMonth.getTimeInMillis())  ;
      dateEnd         = new Date(cal_31_Dec_9999.getTimeInMillis())   ;
      volumeType      = thisVolumeType;  // default value
    }

    // constructor with most info plus variables to determine the remaining info
    public BillingInfo( int thisItemTypeInt, String thisItemTypeString, String thisStatementMsg,
                        double thisMultiplier, double thisChargePerItem,
                        char tempBillFreq, int tempPaymentMonths, int tempDelayMonths, int thisVolumeType,
                        int appType )
    {
      String    thisBillingMonths   = "YYYYYYYYYYYY";                     // default to "every month"
      Calendar  thisCalendarEnd     = (Calendar)cal_31_Dec_9999.clone();  // default to "never expires"
      Calendar  thisCalendarStart   = null;


      // allow for apps that don't want to delay billing until next month
      switch( appType )
      {
        case mesConstants.APP_TYPE_RIVERVIEW:
          thisCalendarStart = cal_01_thisMonth;
          break;

        default:
          thisCalendarStart = cal_01_thisOrNext;
          break;
      }

      thisCalendarStart.add( Calendar.MONTH, tempDelayMonths );

      switch( tempBillFreq )
      {
        case 'M':   // monthly
          break;

        case 'N':   // November (pci compliance fee) -- is a yearly charge, but always billed in November
          // if this is November and plan to start billing in November, advance start date one month, so won't bill until next year
          if(  cal_01_thisMonth.get(Calendar.MONTH) == Calendar.NOVEMBER  &&
              thisCalendarStart.get(Calendar.MONTH) == Calendar.NOVEMBER )
          {
            thisCalendarStart.add(Calendar.MONTH,1);
          }

          thisBillingMonths   = "NNNNNNNNNNYN";
          break;

        case 'D':   // December (pci compliance fee) -- is a yearly charge, but always billed in December
          // if today is a December date and plan to start billing in December, advance start date one month, so won't bill until next year
          if(  cal_01_thisMonth.get(Calendar.MONTH) == Calendar.DECEMBER  &&
              thisCalendarStart.get(Calendar.MONTH) == Calendar.DECEMBER )
          {
            thisCalendarStart.add(Calendar.MONTH,1);
          }

          thisBillingMonths   = "NNNNNNNNNNNY";
          break;
          
        case 'J': // January
          // if today is a January date and plan to start billing in January, advance start date one month, so won't bill until next year
          if(  cal_01_thisMonth.get(Calendar.MONTH) == Calendar.JANUARY  &&
              thisCalendarStart.get(Calendar.MONTH) == Calendar.JANUARY )
          {
            thisCalendarStart.add(Calendar.MONTH,1);
          }

          thisBillingMonths   = "YNNNNNNNNNNN";
          break;

        case 'P':   // payments (could be one-time or spread over several months)
          if( tempPaymentMonths > 1 )
          {
            // set thisChargePerItem equal to total charge divided by total months (rounded up)
            thisChargePerItem = MesMath.round( thisChargePerItem/tempPaymentMonths, 2 );
          }
          else
          {
            tempPaymentMonths   = 1;      // one-time (sometimes comes in as zero, so must be set here)
          }

          if( appType == mesConstants.APP_TYPE_RIVERVIEW )
          {
            thisCalendarEnd = (Calendar)thisCalendarStart.clone();
            thisCalendarEnd.add( Calendar.MONTH, 1 );
          }
          else
          {
            thisCalendarEnd   = (Calendar)cal_15_nextOrFollowing.clone();
            thisCalendarEnd.add( Calendar.MONTH, tempDelayMonths + tempPaymentMonths-1 );
          }

          if( tempPaymentMonths < 12 )
          {
            StringBuffer  allMonths   = new StringBuffer( "NNNNNNNNNNNN" );
            int           billMonth   = thisCalendarStart.get( Calendar.MONTH );  // returns 0-11
            int           lastMonth   = thisCalendarEnd.get( Calendar.MONTH );    // returns 0-11

            do
            {
              allMonths   = allMonths.replace( billMonth, billMonth+1, "Y" );
              billMonth   = (++billMonth) % 12;
            } while( billMonth != lastMonth );

            thisBillingMonths = allMonths.toString();
          }
          break;

        case 'Y':   // yearly
          {
            StringBuffer  allMonths   = new StringBuffer( "NNNNNNNNNNNN" );
            int           billMonth   = thisCalendarStart.get( Calendar.MONTH );  // returns 0-11
            allMonths  = allMonths.replace( billMonth, billMonth+1, "Y" );
            thisBillingMonths = allMonths.toString();
          }
          break;

        case 'Q':   // quarterly
          {
            thisBillingMonths = "NNYNNYNNYNNY";
            thisCalendarStart = (Calendar)cal_01_thisMonth.clone();
            thisCalendarStart.add( Calendar.MONTH, 6);
          }
          break;
      }

      itemTypeInt     = thisItemTypeInt             ;
      itemTypeString  = thisItemTypeString          ;
      statementMsg    = thisStatementMsg            ;
      multiplier      = thisMultiplier              ;
      chargePerItem   = thisChargePerItem           ;
      billingMonths   = thisBillingMonths           ;
      dateStart       = new Date(thisCalendarStart.getTimeInMillis()) ;
      dateEnd         = new Date(thisCalendarEnd.getTimeInMillis())   ;
      volumeType      = thisVolumeType              ;
    }

    public void display( )
    {
      if( statementMsg == null )
      {
        System.out.print("PRICING item: ");
        System.out.print(itemTypeInt + ", ");
        System.out.print(itemTypeString + ", ");
        System.out.print(multiplier +", ");
        System.out.println(chargePerItem);
        /*
        System.out.println("Item Type       : " + itemTypeInt);
        System.out.println("Item Subclass   : " + itemTypeString);
        System.out.println("Rate            : " + multiplier);
        System.out.println("Per Item        : " + chargePerItem);
        */
      }
      else
      {
        System.out.print("CHARGE item: ");
        System.out.print(itemTypeString + ", ");
        System.out.print(statementMsg + ", ");
        System.out.print(billingMonths + ", ");
        System.out.print(chargePerItem +", ");
        System.out.print(DateTimeFormatter.getFormattedDate(dateStart, "MM/dd/yy") + ", ");
        System.out.println(DateTimeFormatter.getFormattedDate(dateEnd, "MM/dd/yy"));
        /*
        System.out.println("Charge Type     : " + itemTypeString);
        System.out.println("Statement Msg   : " + statementMsg);
        System.out.println("Billing Months  : " + billingMonths);
        System.out.println("Item Count      : " + multiplier);
        System.out.println("Charge Amount   : " + chargePerItem);
        System.out.println("Start Date      : " + DateTimeFormatter.getFormattedDate(dateStart, "MM/dd/yy"));
        System.out.println("End Date        : " + DateTimeFormatter.getFormattedDate(dateEnd, "MM/dd/yy"));
        */
      }
    }
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Billing Info Details: ");
        sb.append("Charge Type     : " + itemTypeString);
        sb.append("; Item Type     : " + itemTypeInt);
        sb.append(" ;Statement Msg   : " + statementMsg);
        sb.append("; Billing Months  : " + billingMonths);
        sb.append("; Item Count      : " + multiplier);
        sb.append("; Charge Amount   : " + chargePerItem);
        sb.append("; Start Date      : " + DateTimeFormatter.getFormattedDate(dateStart, "MM/dd/yy"));
        sb.append("; End Date        : " + DateTimeFormatter.getFormattedDate(dateEnd, "MM/dd/yy"));
        return sb.toString();
    }
  }


  // constructor
  public LoadApprovedApps()
  {
    // initialize calendars
    {
            // Calendar.MONTH         --> 0 thru 11 (Jan thru Dec)
            // Calendar.DAY_OF_MONTH  --> 1 thru 31

      //  1st of this month (set to 1st day)
      cal_01_thisMonth        = Calendar.getInstance();
      cal_01_thisMonth.set( Calendar.DAY_OF_MONTH, 1 );

      //  1st of this month or next month (add month if after the 15th, set to 1st day)
      cal_01_thisOrNext       = Calendar.getInstance();
      if( cal_01_thisOrNext.get( Calendar.DAY_OF_MONTH ) > 15 )
          cal_01_thisOrNext.add( Calendar.MONTH, 1 );
      cal_01_thisOrNext.set( Calendar.DAY_OF_MONTH, 1 );

      // 15th of next month or the following month (start with 1st of this or next month, add one month, set to 15th day)
      cal_15_nextOrFollowing  = (Calendar)cal_01_thisOrNext.clone();
      cal_15_nextOrFollowing.add( Calendar.MONTH, 1 );
      cal_15_nextOrFollowing.set( Calendar.DAY_OF_MONTH, 15 );

      // 31st of December 9999
      cal_31_Dec_9999         = Calendar.getInstance();
      cal_31_Dec_9999.set( 9999, 11, 31 );
    }
  }

  protected void loadAppsLogEntry( String source, String msg )
  {
    logEntry(source, msg);
    errors = true;
  }

  public boolean execute()
  {
    return( loadApps( 0L ) );
  }
  public boolean loadApps()   // call from outside this file without parameters, use default parameters
  {
    return( loadApps( 0L ) );
  }
  public boolean loadApps( long testAppSeqNum )
  {
    boolean             retVal              = false;
    ResultSetIterator   it                  = null;
    ResultSet           rs                  = null;
    Vector              vectorAppSeqNums    = null;
    boolean             testMode            = false;

    try
    {
      connect();

      int thisProcSeq = 0;

      String procBanks = ( getEventArg(0) == null ? "3941" : getEventArg(0) );

      testMode = ( testAppSeqNum > 0L );

      // see if there's anything to process
      int procCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:480^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_setup_complete
//          where   process_sequence is null
//                  and app_seq_num in
//                  (
//                    select  ac.app_seq_num
//                    from    app_setup_complete ac,
//                            application app,
//                            app_type apt
//                    where   ac.process_sequence is null
//                            and ac.app_seq_num = app.app_seq_num
//                            and app.app_type = apt.app_type_code
//                            and instr(:procBanks, apt.app_bank_number) > 0
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_setup_complete\n        where   process_sequence is null\n                and app_seq_num in\n                (\n                  select  ac.app_seq_num\n                  from    app_setup_complete ac,\n                          application app,\n                          app_type apt\n                  where   ac.process_sequence is null\n                          and ac.app_seq_num = app.app_seq_num\n                          and app.app_type = apt.app_type_code\n                          and instr( :1  , apt.app_bank_number) > 0\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,procBanks);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:497^7*/

      if( procCount > 0 || testMode )
      {
        // get process sequence
        /*@lineinfo:generated-code*//*@lineinfo:502^9*/

//  ************************************************************
//  #sql [Ctx] { select  mas_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mas_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   thisProcSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^9*/

        log.debug("*** PROCESS SEQUENCE: " + thisProcSeq + "  ***");

        log.debug("TestAppSeqNum = " + testAppSeqNum);

        if( ! testMode )
        {
          // update waiting records in app_setup_complete
          /*@lineinfo:generated-code*//*@lineinfo:516^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//              set     process_sequence = :thisProcSeq
//              where   process_sequence is null
//                      and app_seq_num in
//                      (
//                        select  ac.app_seq_num
//                        from    app_setup_complete ac,
//                                application app,
//                                app_type apt
//                        where   ac.process_sequence is null
//                                and ac.app_seq_num = app.app_seq_num
//                                and app.app_type = apt.app_type_code
//                                and instr(:procBanks, apt.app_bank_number) > 0
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_setup_complete\n            set     process_sequence =  :1  \n            where   process_sequence is null\n                    and app_seq_num in\n                    (\n                      select  ac.app_seq_num\n                      from    app_setup_complete ac,\n                              application app,\n                              app_type apt\n                      where   ac.process_sequence is null\n                              and ac.app_seq_num = app.app_seq_num\n                              and app.app_type = apt.app_type_code\n                              and instr( :2  , apt.app_bank_number) > 0\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,thisProcSeq);
   __sJT_st.setString(2,procBanks);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:532^11*/
        }
        else
        {
          log.debug("updating app_setup_complete");
          // update row for the specified app_seq_num only
          /*@lineinfo:generated-code*//*@lineinfo:538^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//              set     process_sequence = :thisProcSeq
//              where   app_seq_num = :testAppSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_setup_complete\n            set     process_sequence =  :1  \n            where   app_seq_num =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,thisProcSeq);
   __sJT_st.setLong(2,testAppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:543^11*/

          commit();
        }

        // retrieve app_seq_num(s)
        /*@lineinfo:generated-code*//*@lineinfo:549^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct app_seq_num
//            from    app_setup_complete
//            where   process_sequence = :thisProcSeq
//            order by app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct app_seq_num\n          from    app_setup_complete\n          where   process_sequence =  :1  \n          order by app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,thisProcSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:555^9*/

        rs = it.getResultSet();

        // place app_seq_num(s) into vector
        vectorAppSeqNums = new Vector();
        while( rs.next() )
        {
          vectorAppSeqNums.add(rs.getInt("app_seq_num"));
        }

        rs.close();
        it.close();


        if( vectorAppSeqNums.size() > 0 )
        {
          // there is not really a .dat file, "filename" really just for date & run#; always use bank=3941
          mif_load_filename = generateFilename( "mif_mbs3941", ".dat" );

          for( int idx = 0; idx < vectorAppSeqNums.size(); ++idx )
          {
            int appSeqNum = ((Integer)(vectorAppSeqNums.elementAt(idx))).intValue();
            try {
              int     aptAppType  = -1;
              /*@lineinfo:generated-code*//*@lineinfo:580^15*/

//  ************************************************************
//  #sql [Ctx] { select  apt.app_type_code
//                  
//                  from    application     app,
//                          app_type        apt
//                  where   app.app_seq_num = :appSeqNum
//                          and app.app_type = apt.app_type_code
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  apt.app_type_code\n                 \n                from    application     app,\n                        app_type        apt\n                where   app.app_seq_num =  :1  \n                        and app.app_type = apt.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   aptAppType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:588^15*/
              /*  This is for PRF-3333
               * update merchant.asso_number for Pacwest OLA if "merchant exceptions" is selected
                set asso_number to a constant asociation 919221 for pacwest.
                In the future this could be changed to be a value depends on the selection of the "merchant exception" */
              if (aptAppType == mesConstants.APP_TYPE_PACWEST) {
                /*@lineinfo:generated-code*//*@lineinfo:594^17*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//                        set     asso_number = 919221
//                        where   app_seq_num = :appSeqNum and
//                                exception_merchants_code is not null
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n                      set     asso_number = 919221\n                      where   app_seq_num =  :1   and\n                              exception_merchants_code is not null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:600^17*/
              }  
            }
            catch (Exception e) {
              //nothing to be done since we should always have records in both merchant and application tables
            }
            log.debug("\nLOADING: " + appSeqNum);
            if( gatherMerchantInfo( appSeqNum ) &&
                insertMerchantInfo( appSeqNum, testMode ) )
            {
              // update record to show processed
              /*@lineinfo:generated-code*//*@lineinfo:611^15*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//                  set     date_processed = sysdate
//                  where   app_seq_num = :appSeqNum
//                          and process_sequence = :thisProcSeq
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_setup_complete\n                set     date_processed = sysdate\n                where   app_seq_num =  :1  \n                        and process_sequence =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,thisProcSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:617^15*/
            }
          }

          // generate and send email of status (only if production run)
          if( testAppSeqNum == 0L )
          {
            sendStatusEmail(thisProcSeq);
          }
        }
      }
      else
      {
        // no new setups to process, nothing to do.
        log.debug("No accounts to upload");
      }

      retVal = (errors == false);
    }
    catch(Exception e)
    {
      loadAppsLogEntry("loadApps()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( retVal );
  }

  private void sendStatusEmail(int procSeq)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    StringBuffer msgBody = new StringBuffer("New Accounts Uploaded:\n\n");
    msgBody.append("Control Num Type  Merchant #     DBA Name\n");
    msgBody.append("----------- ---- ------------ -------------------------\n");

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:660^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct mr.merc_cntrl_number control_number,
//                  lpad(app.appsrctype_code,4,' ')  app_type,
//                  mf.merchant_number            merchant_number,
//                  mf.dba_name                   dba_name
//          from    app_setup_complete ac,
//                  application app,
//                  merchant mr,
//                  mif mf
//          where   ac.process_sequence = :procSeq
//                  and ac.app_seq_num = app.app_seq_num
//                  and ac.app_seq_num = mr.app_seq_num
//                  and mr.merch_number = mf.merchant_number
//          order by mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct mr.merc_cntrl_number control_number,\n                lpad(app.appsrctype_code,4,' ')  app_type,\n                mf.merchant_number            merchant_number,\n                mf.dba_name                   dba_name\n        from    app_setup_complete ac,\n                application app,\n                merchant mr,\n                mif mf\n        where   ac.process_sequence =  :1  \n                and ac.app_seq_num = app.app_seq_num\n                and ac.app_seq_num = mr.app_seq_num\n                and mr.merch_number = mf.merchant_number\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:675^7*/

      rs = it.getResultSet();

      while( rs.next() )
      {
        msgBody.append(rs.getString("control_number"));
        msgBody.append(" ");
        msgBody.append(rs.getString("app_type"));
        msgBody.append(" ");
        msgBody.append(rs.getString("merchant_number"));
        msgBody.append(" ");
        msgBody.append(rs.getString("dba_name"));
        msgBody.append("\n");
      }

      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_NEW_ACCOUNTS); // 160
      msg.setSubject("MBS Accounts Loaded: " + com.mes.support.DateTimeFormatter.getCurDateTimeString());
      msg.setText(msgBody.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail(" + procSeq + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private void resetApp(int appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:712^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//          set     process_sequence = null,
//                  date_processed = null
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_setup_complete\n        set     process_sequence = null,\n                date_processed = null\n        where   app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:718^7*/
    }
    catch(Exception e)
    {
      logEntry("resetApp(" + appSeqNum + ")", e.toString());
    }
    finally
    {
    }
  }

  private boolean stringMatch(String s1, String s2)
  {
    boolean result = false;

    try
    {
      if( s1 == null && s2 == null )
      {
        result = true;
      }
      else if( s1 != null && s2 != null && s1.equals(s2) )
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      // one item is null, not a match
      result = false;
    }

    return( result );
  }

  private boolean containsItem( BillingInfo item )
  {
    boolean result = false;

    for( int i = 0; i < billInfoRows.size(); ++i )
    {
      BillingInfo testItem = (BillingInfo)billInfoRows.elementAt(i);

      if( testItem.statementMsg == null && item.statementMsg == null )
      {
        // both are pricing items
        if( testItem.itemTypeInt == item.itemTypeInt &&
            stringMatch(testItem.itemTypeString, item.itemTypeString) )
        {
          System.out.println("FOUND DUPLICATE PRICING ITEM");

          System.out.print("  Already Contains ");
          testItem.display();

          result = true;
          break;
        }
      }
      else if( testItem.statementMsg != null && item.statementMsg != null )
      {
        // both are charge records
        if( stringMatch(testItem.billingMonths, item.billingMonths) &&
            stringMatch(testItem.statementMsg, item.statementMsg) )
        {
          // match
          System.out.println("FOUND DUPLICATE PRICING ITEM");

          System.out.println("  Already Contains ");
          testItem.display();

          result = true;
          break;
        }
      }
    }

    return( result );
  }

  private boolean gatherMerchantInfo( int appSeqNum )
  {
    boolean                         retVal    = false;
    ResultSetIterator               it        = null;
    ResultSet                       rs        = null;

    int     aptAppType                        = -1;
    int     aptConversionDelayMonths          = 0;
    int     aptMinDiscountDelayMonths         = 0;
    String  merchNameLegal                    = null;
    String  merchNameMailing                  = null;
    String  merchChargeApf                    = "Y";
    String  merchAccountType                  = null;
    String  merchBankAssoc                    = null;
    int     merchBetTypeCode                  = 0;
    int     merchIndustryType                 = 0;
    int     merchPricingGrid                  = -1;
    double  miscchrgAuthFeeVoice              = 0.00;
    double  tcDPayPerItem                     = 0.00;
    int     tcVisaDiscRateType                = 0;
    int     tcVisaIcType                      = 0;
    double  tcVisaIcFee                       = 0.00;
    double  tcVisaAuthFeeAru                  = 0.00;
    double  tcVisaAuthFeeVoice                = 0.00;
    boolean tcSeparatePassThru                = false;
    String  progress                          = "start of method";

    try
    {
      log.debug("*** ENTERING gatherMerchantInfo(" + appSeqNum + ")");
      
      log.debug("removing billinforows");
      billInfoRows.removeAllElements();

      // initialize mif
      log.debug("instantiating mifInfo object");
      mif = new mifInfo();

      // initialize arrays
      log.debug("initializing arrays");
      for( int idx = 0; idx < 3; ++idx )
      {
        mif_X_name              [idx] = null;
        mif_X_addr1             [idx] = null;
        mif_X_addr2             [idx] = null;
        mif_X_city              [idx] = null;
        mif_X_state             [idx] = null;
        mif_X_zip               [idx] = null;
      }
      for( int idx = 0; idx < 2; ++idx )
      {
        mif_X_transit_routing   [idx] = null;
        mif_X_dda               [idx] = null;
        mif_X_acct_type         [idx] = null;
      }
      for( int idx = 0; idx < CT_LEN; ++idx )
      {
//      mif_dXXaccpt            [idx] =  "N";
        mif_X_disc_table        [idx] = null;
        mif_X_disc_method       [idx] = null;
        mif_X_disc_type         [idx] = null;
        mif_X_disc_rate         [idx] =    0;
        mif_X_per_item          [idx] =    0;
        mif_X_plan              [idx] = null;
        mif_X_mid               [idx] =    0;
      }

      // get data from table "merchant"
      progress = "getting data from table MERCHANT";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:867^7*/

//  ************************************************************
//  #sql [Ctx] { select  upper( trim( substr( m.merch_business_name, 1 , 32 )))  ,
//                  upper( trim( substr( m.merch_legal_name   , 1 , 32 )))  ,
//                  upper( trim( substr( m.merch_mailing_name , 1 , 32 )))  ,
//                  upper( nvl(m.charge_apf,'Y') )                          ,
//                  m.account_type                                          ,
//                  (apt.app_bank_number||lpad(m.asso_number,6,'0'))        ,
//                  nvl(m.bet_type_code           , 0)                      ,
//                  nvl(m.industype_code          , 0)                      ,
//                  nvl(m.pricing_grid            ,-1)                      ,
//                  m.merch_number                                          ,
//                  m.asso_number                                           ,
//                  nvl(m.merch_federal_tax_id, 0)                          ,                
//                  nvl(m.customer_service_phone  , 0)                      ,
//                  m.sic_code                                              ,
//                  m.merch_mcc                                             ,
//                  m.merch_rep_code                                        ,
//                  m.merch_invg_code                                       ,
//                  m.merch_met_table_number                                ,
//                  nvl(m.merch_edc_flag          ,'Y')                     ,
//                  nvl(m.merch_dly_ach_flag      ,'Y')                     ,
//                  nvl(m.exception_merchants_code, '')                     ,
//                  decode(m.bustype_code,
//                    '1','3',
//                    '2','1',
//                    '3','4',
//                    '4','1',
//                    '5','5',
//                    '6','6',
//                    '7','6',
//                    '8','8',
//                    '9','7',
//                    '3')
//                    || decode(m.merch_gender,'M','1','F','2','0')
//                    || '00'
//          
//          from    merchant        m,
//                  application     app,
//                  app_type        apt
//          where   m.app_seq_num = :appSeqNum
//                  and m.app_seq_num = app.app_seq_num
//                  and app.app_type = apt.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper( trim( substr( m.merch_business_name, 1 , 32 )))  ,\n                upper( trim( substr( m.merch_legal_name   , 1 , 32 )))  ,\n                upper( trim( substr( m.merch_mailing_name , 1 , 32 )))  ,\n                upper( nvl(m.charge_apf,'Y') )                          ,\n                m.account_type                                          ,\n                (apt.app_bank_number||lpad(m.asso_number,6,'0'))        ,\n                nvl(m.bet_type_code           , 0)                      ,\n                nvl(m.industype_code          , 0)                      ,\n                nvl(m.pricing_grid            ,-1)                      ,\n                m.merch_number                                          ,\n                m.asso_number                                           ,\n                nvl(m.merch_federal_tax_id, 0)                          ,                \n                nvl(m.customer_service_phone  , 0)                      ,\n                m.sic_code                                              ,\n                m.merch_mcc                                             ,\n                m.merch_rep_code                                        ,\n                m.merch_invg_code                                       ,\n                m.merch_met_table_number                                ,\n                nvl(m.merch_edc_flag          ,'Y')                     ,\n                nvl(m.merch_dly_ach_flag      ,'Y')                     ,\n                nvl(m.exception_merchants_code, '')                     ,\n                decode(m.bustype_code,\n                  '1','3',\n                  '2','1',\n                  '3','4',\n                  '4','1',\n                  '5','5',\n                  '6','6',\n                  '7','6',\n                  '8','8',\n                  '9','7',\n                  '3')\n                  || decode(m.merch_gender,'M','1','F','2','0')\n                  || '00'\n         \n        from    merchant        m,\n                application     app,\n                app_type        apt\n        where   m.app_seq_num =  :1  \n                and m.app_seq_num = app.app_seq_num\n                and app.app_type = apt.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 22) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(22,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchNameBusiness = (String)__sJT_rs.getString(1);
   merchNameLegal = (String)__sJT_rs.getString(2);
   merchNameMailing = (String)__sJT_rs.getString(3);
   merchChargeApf = (String)__sJT_rs.getString(4);
   merchAccountType = (String)__sJT_rs.getString(5);
   merchBankAssoc = (String)__sJT_rs.getString(6);
   merchBetTypeCode = __sJT_rs.getInt(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchIndustryType = __sJT_rs.getInt(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchPricingGrid = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.merchant_number = __sJT_rs.getLong(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.dmagent = (String)__sJT_rs.getString(11);
   mif.federal_tax_id = __sJT_rs.getLong(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.customer_service_phone = __sJT_rs.getLong(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.sic_code = (String)__sJT_rs.getString(14);
   mif.class_code = (String)__sJT_rs.getString(15);
   mif.rep_code = (String)__sJT_rs.getString(16);
   mif.inv_code_investigate = (String)__sJT_rs.getString(17);
   mif.met_table = (String)__sJT_rs.getString(18);
   mif.edc_flagst_other_3 = (String)__sJT_rs.getString(19);
   mif.daily_ach = (String)__sJT_rs.getString(20);
   mif.exception_merchants_code = (String)__sJT_rs.getString(21);
   mif.merchant_type = (String)__sJT_rs.getString(22);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:931^7*/

      // determine if association number is valid
      log.debug("validating association number");
      int validCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:936^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    groups
//          where   assoc_number = to_number(:merchBankAssoc)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    groups\n        where   assoc_number = to_number( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchBankAssoc);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   validCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:942^7*/

      if( validCount == 0 )
      {
        log.debug("*** INVALID ASSOCIATION ***");
        // throw exception because this is an invalid association
        throw new Exception("Invalid Association: " + merchBankAssoc);
      }

      // get data from tables "application" & "app_type"
      progress = "getting data from tables APPLICATION and APP_TYPE";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:954^7*/

//  ************************************************************
//  #sql [Ctx] { select  apt.app_type_code                         ,
//                  decode(apt.delay_monthly_fees_conversion,
//                          'Y' , decode(:merchAccountType,
//                                        'C',2,  0)  ,
//                          0 )                               ,
//                  nvl(apt.min_discount_delay_months, 0)     ,
//                  nvl(apt.print_statements, 'Y')            ,
//                  apt.app_bank_number
//          
//          from    application     app,
//                  app_type        apt
//          where   app.app_seq_num = :appSeqNum
//                  and app.app_type = apt.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  apt.app_type_code                         ,\n                decode(apt.delay_monthly_fees_conversion,\n                        'Y' , decode( :1  ,\n                                      'C',2,  0)  ,\n                        0 )                               ,\n                nvl(apt.min_discount_delay_months, 0)     ,\n                nvl(apt.print_statements, 'Y')            ,\n                apt.app_bank_number\n         \n        from    application     app,\n                app_type        apt\n        where   app.app_seq_num =  :2  \n                and app.app_type = apt.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchAccountType);
   __sJT_st.setInt(2,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   aptAppType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   aptConversionDelayMonths = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   aptMinDiscountDelayMonths = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.print_statements = (String)__sJT_rs.getString(4);
   mif.bank_number = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:973^7*/


      // get data from table "merchcredit"
      progress = "getting data from table MERCHCREDIT";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:979^7*/

//  ************************************************************
//  #sql [Ctx] { select  mcr.merch_debit_achpost_days      ,
//                  mcr.merch_credit_achpost_days     ,
//                  nvl(mcr.merch_days_suspend, 0 )
//          
//          from    merchcredit     mcr
//          where   mcr.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mcr.merch_debit_achpost_days      ,\n                mcr.merch_credit_achpost_days     ,\n                nvl(mcr.merch_days_suspend, 0 )\n         \n        from    merchcredit     mcr\n        where   mcr.app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.num_post_date_days_debit = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.num_post_date_days_credt = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.suspended_days = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:989^7*/


      // get data from table "merchbank"
      progress = "getting data from table MERCHBANK";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:995^7*/

//  ************************************************************
//  #sql [Ctx] it = { -- upper & lower oh's become zeroes; dee's & dashes become spaces; then spaces get deleted
//          select  lpad( replace(translate(translate(mb.merchbank_transit_route_num, 'Oo','0'), 'D-',' '),' '), 9,'0') as temp_mif_transit   ,
//                        replace(translate(translate(mb.merchbank_acct_num         , 'Oo','0'), 'D-',' '),' ')         as temp_mif_dda       ,
//                        -- for now, default account type to checking; could use mb.bankacc_type for more accurate accnt type
//                  'CK'                                                                                                as temp_mif_accnt_type
//          from    merchbank       mb
//          where   mb.app_seq_num = :appSeqNum
//          order by  mb.merchbank_acct_srnum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- upper & lower oh's become zeroes; dee's & dashes become spaces; then spaces get deleted\n        select  lpad( replace(translate(translate(mb.merchbank_transit_route_num, 'Oo','0'), 'D-',' '),' '), 9,'0') as temp_mif_transit   ,\n                      replace(translate(translate(mb.merchbank_acct_num         , 'Oo','0'), 'D-',' '),' ')         as temp_mif_dda       ,\n                      -- for now, default account type to checking; could use mb.bankacc_type for more accurate accnt type\n                'CK'                                                                                                as temp_mif_accnt_type\n        from    merchbank       mb\n        where   mb.app_seq_num =  :1  \n        order by  mb.merchbank_acct_srnum";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1005^7*/
      rs = it.getResultSet();

      // get the first set of account info; get second set if it exists, else use first set again
      if( rs.next() )
      {
        mif_X_transit_routing[0]  = rs.getString( "temp_mif_transit"    );
        mif_X_dda[0]              = rs.getString( "temp_mif_dda"        );
        mif_X_acct_type[0]        = rs.getString( "temp_mif_accnt_type" );

        if( rs.next() )
        {
          mif_X_transit_routing[1]  = rs.getString( "temp_mif_transit"    );
          mif_X_dda[1]              = rs.getString( "temp_mif_dda"        );
          mif_X_acct_type[1]        = rs.getString( "temp_mif_accnt_type" );
        }
        else
        {
          mif_X_transit_routing[1]  = mif_X_transit_routing[0];
          mif_X_dda[1]              = mif_X_dda[0]            ;
          mif_X_acct_type[1]        = mif_X_acct_type[0]      ;
        }
      }
      rs.close();
      it.close();


      // get data from table "businessowner"
      progress = "getting data from table BUSINESSOWNER";
      log.debug(progress);
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1037^9*/

//  ************************************************************
//  #sql [Ctx] { select  upper(trim(substr( bo1.busowner_first_name ||' '|| bo1.busowner_last_name,1,25 )))  , bo1.busowner_ssn  ,
//                    upper(trim(substr( bo2.busowner_first_name ||' '|| bo2.busowner_last_name,1,25 )))  , bo2.busowner_ssn
//            
//            from    (select * from businessowner  where busowner_num = 1)  bo1,
//                    (select * from businessowner  where busowner_num = 2)  bo2
//            where       bo1.app_seq_num(+) = :appSeqNum
//                    and bo2.app_seq_num(+) = bo1.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(trim(substr( bo1.busowner_first_name ||' '|| bo1.busowner_last_name,1,25 )))  , bo1.busowner_ssn  ,\n                  upper(trim(substr( bo2.busowner_first_name ||' '|| bo2.busowner_last_name,1,25 )))  , bo2.busowner_ssn\n           \n          from    (select * from businessowner  where busowner_num = 1)  bo1,\n                  (select * from businessowner  where busowner_num = 2)  bo2\n          where       bo1.app_seq_num(+) =  :1  \n                  and bo2.app_seq_num(+) = bo1.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.owner_name = (String)__sJT_rs.getString(1);
   mif.ssn = (String)__sJT_rs.getString(2);
   mif.manager_name = (String)__sJT_rs.getString(3);
   mif.license_number = (String)__sJT_rs.getString(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^9*/
      }
      catch(Exception boe)
      {
        log.debug("can't load business owner data -- maybe this is not a problem...");
      }


      // get data from table "address"
      // note: TSYS normalized addresses before they went into MIF (eg, P.O.B. & P.O. Box = PO BOX, Street & St. = ST, North & N. = N, etc)
      progress = "getting data from table ADDRESS";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:1059^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  addr.addresstype_code,
//                  upper(trim(addr.address_line1       ))               temp_mif_addr1        , -- char  (32)
//                  upper(trim(addr.address_line2       ))               temp_mif_addr2        , -- char  (32)
//                  upper(trim(addr.address_city        ))               temp_mif_city         , -- char  (25)
//                  upper(trim(addr.countrystate_code   ))               temp_mif_state        , -- char  ( 2)
//                  rpad(replace(translate(addr.address_zip,'/-.,?',' '),' '),9,'0') temp_mif_zip          , -- char  (10)
//                  addr.address_phone                        temp_mif_phone_1      , -- number(10)
//                  addr.address_fax                          temp_mif_phone_2_fax    -- number(10)
//          from    address       addr
//          where   addr.app_seq_num = :appSeqNum
//                  and addr.addresstype_code in ( :mesConstants.ADDR_TYPE_BUSINESS, :mesConstants.ADDR_TYPE_MAILING, :mesConstants.ADDR_TYPE_SHIPPING ) -- in (1,2,3)
//          order by  addr.addresstype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  addr.addresstype_code,\n                upper(trim(addr.address_line1       ))               temp_mif_addr1        , -- char  (32)\n                upper(trim(addr.address_line2       ))               temp_mif_addr2        , -- char  (32)\n                upper(trim(addr.address_city        ))               temp_mif_city         , -- char  (25)\n                upper(trim(addr.countrystate_code   ))               temp_mif_state        , -- char  ( 2)\n                rpad(replace(translate(addr.address_zip,'/-.,?',' '),' '),9,'0') temp_mif_zip          , -- char  (10)\n                addr.address_phone                        temp_mif_phone_1      , -- number(10)\n                addr.address_fax                          temp_mif_phone_2_fax    -- number(10)\n        from    address       addr\n        where   addr.app_seq_num =  :1  \n                and addr.addresstype_code in (  :2  ,  :3  ,  :4   ) -- in (1,2,3)\n        order by  addr.addresstype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   __sJT_st.setInt(3,mesConstants.ADDR_TYPE_MAILING);
   __sJT_st.setInt(4,mesConstants.ADDR_TYPE_SHIPPING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1073^7*/

      rs = it.getResultSet();

      // [0] = mif.dba_name     , mif.dmaddr        , mif.address_line_3  , mif.dmcity        , mif.dmstate         , mif.dmzip
      // [1] = mif.name1_line_1 , mif.addr1_line_1  , mif.addr1_line_2    , mif.city1_line_4  , mif.state1_line_4   , mif.zip1_line_4
      // [2] = mif.name2_line_1 , mif.addr2_line_1  , mif.addr2_line_2    , mif.city2_line_4  , mif.state2_line_4   , mif.zip2_line_4
      while( rs.next() )
      {
        switch( rs.getInt("addresstype_code") )
        {
          case mesConstants.ADDR_TYPE_BUSINESS:
            // business phone
            mif.phone_1                                              =   rs.getLong( "temp_mif_phone_1"     );
            mif.phone_2_fax                                          =   rs.getLong( "temp_mif_phone_2_fax" );

            // set all address fields to this value as an initialization
            mif_X_name[0]   =   mif_X_name[1]   =   mif_X_name[2]   =   merchNameBusiness;
            mif_X_addr1[0]  =   mif_X_addr1[1]  =   mif_X_addr1[2]  =   rs.getString( "temp_mif_addr1" );
            mif_X_addr2[0]  =   mif_X_addr2[1]  =   mif_X_addr2[2]  =   rs.getString( "temp_mif_addr2" );
            mif_X_city[0]   =   mif_X_city[1]   =   mif_X_city[2]   =   rs.getString( "temp_mif_city"  );
            mif_X_state[0]  =   mif_X_state[1]  =   mif_X_state[2]  =   rs.getString( "temp_mif_state" );
            mif_X_zip[0]    =   mif_X_zip[1]    =   mif_X_zip[2]    =   rs.getString( "temp_mif_zip"   );
            break;

          case mesConstants.ADDR_TYPE_MAILING:
            if( mif.bank_number == mesConstants.BANK_ID_CBT )
            {
              if( merchNameMailing != null )    // if null, keep merchNameBusiness
              {
                mif_X_name[0]           =   merchNameMailing;
                mif_X_name[2]           =   merchNameMailing;
              }

              // CB&T mailing address goes into mif[0] and mif[2]
              mif_X_addr1[0]          =   rs.getString( "temp_mif_addr1" );
              mif_X_addr2[0]          =   rs.getString( "temp_mif_addr2" );
              mif_X_city[0]           =   rs.getString( "temp_mif_city"  );
              mif_X_state[0]          =   rs.getString( "temp_mif_state" );
              mif_X_zip[0]            =   rs.getString( "temp_mif_zip"   );

              mif_X_addr1[2]          =   rs.getString( "temp_mif_addr1" );
              mif_X_addr2[2]          =   rs.getString( "temp_mif_addr2" );
              mif_X_city[2]           =   rs.getString( "temp_mif_city"  );
              mif_X_state[2]          =   rs.getString( "temp_mif_state" );
              mif_X_zip[2]            =   rs.getString( "temp_mif_zip"   );

            }
            else
            {
              if( merchNameMailing != null )    // if null, keep merchNameBusiness
                mif_X_name[1]           =   merchNameMailing;

              // non-CB&T merchants get the mailing address in mif[1]
              mif_X_addr1[1]          =   rs.getString( "temp_mif_addr1" );
              mif_X_addr2[1]          =   rs.getString( "temp_mif_addr2" );
              mif_X_city[1]           =   rs.getString( "temp_mif_city"  );
              mif_X_state[1]          =   rs.getString( "temp_mif_state" );
              mif_X_zip[1]            =   rs.getString( "temp_mif_zip"   );
            }
            break;

          case mesConstants.ADDR_TYPE_SHIPPING:
            // shipping address goes into mif[2] for all banks
            if( rs.getString("temp_mif_addr1") != null && ! ("").equals(rs.getString("temp_mif_addr1")) )
            {
              mif_X_addr1[2]          =   rs.getString( "temp_mif_addr1" );
              mif_X_addr2[2]          =   rs.getString( "temp_mif_addr2" );
              mif_X_city[2]           =   rs.getString( "temp_mif_city"  );
              mif_X_state[2]          =   rs.getString( "temp_mif_state" );
              mif_X_zip[2]            =   rs.getString( "temp_mif_zip"   );
            }
            break;
        }
      }

      if( mif.bank_number == mesConstants.BANK_ID_CBT )
      {
        if( merchNameLegal != null )      // if null, keep merchNameBusiness or merchNameMailing
          mif_X_name[2]           =   merchNameLegal;
      }

      rs.close();
      it.close();

      // fdr_corp_name should be legal name
      if( merchNameLegal != null )
      {
        if( merchNameLegal.length() > 50 )
        {
          mif.fdr_corp_name = merchNameLegal.substring( 0, 50 );
        }
        else
        {
          mif.fdr_corp_name = merchNameLegal;
        }
      }

      if( mif.customer_service_phone == 0 )
      {
        mif.customer_service_phone = mif.phone_1;
      }

      // get IRS data from table "credit_worksheet_info"
      progress = "getting data from table CREDIT_WORKSHEET_INFO";
      log.debug(progress);
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1181^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(to_number(ofac),0) ,
//                    nvl(to_number(irs_match_result),-1) ,
//                    nvl(to_number(irs_tax_type),0),
//                    nvl(irs_exemptions, 'N')
//            
//            from    credit_worksheet_info
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(to_number(ofac),0) ,\n                  nvl(to_number(irs_match_result),-1) ,\n                  nvl(to_number(irs_tax_type),0),\n                  nvl(irs_exemptions, 'N')\n           \n          from    credit_worksheet_info\n          where   app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.ofac = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.irs_match_result = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.irs_tax_type = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.irs_exemptions = (String)__sJT_rs.getString(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1193^9*/
      }
      catch(Exception irs)
      {
        //logEntry("error setting irs match data (" + appSeqNum + ")", irs.toString());
        //info not found so set defaults
        mif.ofac              = 0;
        mif.irs_match_result  = -1;
        mif.irs_tax_type      = 0;
        mif.irs_exemptions    = "N";
      }


      // get data from table "merchant_data"
      progress = "getting data from table MERCHANT_DATA";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:1209^7*/

//  ************************************************************
//  #sql [Ctx] { select  upper(md.user_data_1)                       ,
//                  upper(md.user_data_2)                       ,
//                  upper(md.user_data_3)                       ,
//                  upper(md.user_data_4)                       ,
//                  upper(md.user_data_5)                       ,
//                  rpad(nvl(md.user_account_1,'0'), 16, '0')   ,
//                  rpad(nvl(md.user_account_2,'0'), 16, '0')
//          
//          from    merchant_data   md
//          where   md.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(md.user_data_1)                       ,\n                upper(md.user_data_2)                       ,\n                upper(md.user_data_3)                       ,\n                upper(md.user_data_4)                       ,\n                upper(md.user_data_5)                       ,\n                rpad(nvl(md.user_account_1,'0'), 16, '0')   ,\n                rpad(nvl(md.user_account_2,'0'), 16, '0')\n         \n        from    merchant_data   md\n        where   md.app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 7) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(7,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.user_data_1 = (String)__sJT_rs.getString(1);
   mif.user_data_2 = (String)__sJT_rs.getString(2);
   mif.user_data_3 = (String)__sJT_rs.getString(3);
   mif.user_data_4 = (String)__sJT_rs.getString(4);
   mif.user_data_5 = (String)__sJT_rs.getString(5);
   mif.user_acount_1 = (String)__sJT_rs.getString(6);
   mif.user_account_2 = (String)__sJT_rs.getString(7);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1227^7*/

      if( mif.user_acount_1.length() >= 5 )
      {
        mif.dbfepflg  = mif.user_acount_1.substring( 5, 6 );
        if( !"0000000000000000".equals(mif.user_acount_1) )
        {
          mif.dbfemid   = mif.user_acount_1.substring( 6 );
        }
      }


      // get data from table "appo_charge_recs"
      progress = "getting data from table APPO_CHARGE_RECS";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:1242^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trim(substr(acr.description,1,40))  appo_desc   ,
//                  acr.charge_rec_type                 appo_type   ,
//                  acr.amount                          appo_amount
//          from    appo_charge_recs  acr
//          where   acr.app_seq_num = :appSeqNum
//          order by acr.charge_rec_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trim(substr(acr.description,1,40))  appo_desc   ,\n                acr.charge_rec_type                 appo_type   ,\n                acr.amount                          appo_amount\n        from    appo_charge_recs  acr\n        where   acr.app_seq_num =  :1  \n        order by acr.charge_rec_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1250^7*/
      rs = it.getResultSet();
      while( rs.next() )
      {
        // if appo_type == 1/CHARGE_TYPE_SINGLE   then  billFreq = 'P'/payment  && paymentMonths =  1 (one-time)    but we can say 0
        // if appo_type == 2/CHARGE_TYPE_MONTHLY  then  billFreq = 'M'/monthly  && paymentMonths = 12 (every month) but we can say 0

        BillingInfo   billInfo  = new BillingInfo(  -1,
                                                    "MIS",
                                                    rs.getString("appo_desc"),
                                                    0,
                                                    rs.getDouble("appo_amount"),
                                                    (( rs.getInt("appo_type") == 1 ) ? 'P':'M'),
                                                    0,
                                                    aptConversionDelayMonths,
                                                    1,
                                                    aptAppType );

        addBillingInfo( billInfo );
      }
      rs.close();
      it.close();

      // avs fee
      progress = "getting AVS fee";
      log.debug(progress);
      try
      {
        int avsCount = 0;

        /*@lineinfo:generated-code*//*@lineinfo:1280^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    billcard_auth_bet bab,
//                    auth_bets ab
//            where   bab.app_seq_num(+) = :appSeqNum
//                    and bab.plan_type(+) = 'OV'
//                    and bab.vendor_id(+) = 'IN'
//                    and bab.bet = ab.trident(+)
//                    and nvl(ab.avs_fee,0.0) > 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    billcard_auth_bet bab,\n                  auth_bets ab\n          where   bab.app_seq_num(+) =  :1  \n                  and bab.plan_type(+) = 'OV'\n                  and bab.vendor_id(+) = 'IN'\n                  and bab.bet = ab.trident(+)\n                  and nvl(ab.avs_fee,0.0) > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   avsCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1291^9*/

        if( avsCount > 0 )
        {
          double avsFee = 0.0;
          /*@lineinfo:generated-code*//*@lineinfo:1296^11*/

//  ************************************************************
//  #sql [Ctx] { select  distinct nvl(ab.avs_fee, 0.0)
//              
//              from    billcard_auth_bet bab,
//                      auth_bets ab
//              where   bab.app_seq_num(+) = :appSeqNum
//                      and bab.plan_type(+) = 'OV'
//                      and bab.vendor_id(+) = 'IN'
//                      and bab.bet = ab.trident(+)
//                      and nvl(ab.avs_fee,0.0) > 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct nvl(ab.avs_fee, 0.0)\n             \n            from    billcard_auth_bet bab,\n                    auth_bets ab\n            where   bab.app_seq_num(+) =  :1  \n                    and bab.plan_type(+) = 'OV'\n                    and bab.vendor_id(+) = 'IN'\n                    and bab.bet = ab.trident(+)\n                    and nvl(ab.avs_fee,0.0) > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   avsFee = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1307^11*/

          if(avsFee > 0.0)
          {
            BillingInfo   billInfoAvs  = new BillingInfo( MBS_ELEMENTS_AVS, null, 0.00, avsFee, 1 );
            addBillingInfo(billInfoAvs);
          }
        }
      }
      catch(Exception avse)
      {
        logEntry("error setting avs fee (" + appSeqNum + ")", avse.toString());
      }

      // declare variables for data in tables "tranchrg" and "miscchrg"
      final int                       FEE_CHARGEBACK = 0      , FEE_BATCH_ACH = 1   , FEE_ARU_AUTH = 2  , FEE_PIN_DEBIT = 3       , FEE_EBT_DEBIT = 4       , FEE_MIN_DISCOUNT  = 5     , NUM_FEES = 6;
      final int[]     feeTypeInt  = { MBS_ELEMENTS_CHARGEBACK , MBS_ELEMENTS_BATCH  , MBS_ELEMENTS_ARU  , MBS_ELEMENTS_PIN_DEBIT  , MBS_ELEMENTS_EBT_DEBIT  , MBS_ELEMENTS_DISCOUNT_MIN };
            double[]  feePerItem  = { 0.00                    , 0.00                , 0.00              , 0.00                    , 0.00                    , 0.00                      };

      
      double  debitPinPassThruRate = 0.0;

      progress = "loading amex_opt_blue flag";
      log.debug(progress);

      String amexOptBlue = "";
      /*@lineinfo:generated-code*//*@lineinfo:1333^7*/

//  ************************************************************
//  #sql [Ctx] it = { select amex_opt_blue
//          from   merchpayoption
//          where  cardtype_code = 16
//                 and app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select amex_opt_blue\n        from   merchpayoption\n        where  cardtype_code = 16\n               and app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1339^7*/

      rs = it.getResultSet();
      if( rs.next() )
      {
        amexOptBlue = rs.getString("amex_opt_blue");
      }

      rs.close();
      it.close();

      if( amexOptBlue != null && !amexOptBlue.equals("") && !amexOptBlue.equals("null") )
      {
        amexOptBlue = amexOptBlue.toUpperCase();
      }
      else
      {
        amexOptBlue = "";
      }

      // get data from tables "tranchrg" & "merchpayoption" & "cardtype"
      progress = "getting data from tables TRANCHRG and MERCHPAYOPTION and CARDTYPE";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:1362^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tc.cardtype_code                                    cardtype_code       ,
//                  decode(app.app_type,
//                    :mesConstants.APP_TYPE_SVB, decode( tc.cardtype_code, :mesConstants.APP_CT_INTERNET, 4, :mesConstants.APP_CT_VISA, 2, 3),
//                    decode(tc.cardtype_code, :mesConstants.APP_CT_INTERNET, 1, :mesConstants.APP_CT_VISA, 2, 3 ))        auth_fee_order,
//  --                decode(tc.tranchrg_float_disc_flag  ,'Y','V',  'F') chargeDiscMethod    ,
//                  'G'                                                 chargeDiscMethod    ,
//                  nvl(tc.tranchrg_interchangefee_fee  , 0.00  )       chargeIcFee         ,
//                  nvl(tc.tranchrg_interchangefee_type , 0     )       chargeIcType        ,
//                  nvl(tc.tranchrg_discrate_type       , 0     )       chargeDiscRateType  ,
//                  nvl(tc.tranchrg_disc_rate           , 0.00  )       chargeDiscRate      ,
//                  nvl(tc.tranchrg_aru_fee             , 0.00  )       chargeAruFee        ,
//                  nvl(tc.tranchrg_voice_fee           , 0.00  )       chargeVoiceFee      ,
//                  nvl(tc.tranchrg_avs_fee             , 0.00  )       chargeAvsFee        ,
//                  nvl(tc.tranchrg_per_auth            , 0.00  )       chargePerAuth       ,
//                  nvl(tc.tranchrg_per_tran            , 0.00  )       chargePerTran       ,
//                  nvl(tc.tranchrg_pass_thru           , 0.00  )       chargePassThru      ,
//                  nvl(tc.tranchrg_mmin_chrg           , 0.00  )       chargeMonthlyMin    ,
//                  nvl(mpo.merchpo_card_merch_number   , 0     )       mpoMerchNum         ,
//                  nvl(tc.pin_debit_pass_thru          , 'N'   )       pinDebitPassThru    ,
//                  decode(ct.cardtype, null, '--', 'P1', 'PL', ct.cardtype)  cardtype_name
//          from    (select * from  tranchrg        where app_seq_num = :appSeqNum) tc,
//                  (select * from  merchpayoption  where app_seq_num = :appSeqNum) mpo,
//                  cardtype                                        ct,
//                  application app
//          where     tc.cardtype_code = mpo.cardtype_code(+)
//                and tc.cardtype_code =  ct.cardtype_code
//                and tc.app_seq_num = app.app_seq_num
//                and decode(app.app_type, :mesConstants.APP_TYPE_SVB, decode(tc.cardtype_code, :mesConstants.APP_CT_INTERNET, 0, 1), 1) = 1
//          order by auth_fee_order    -- at present, the auth fees are the only ones where order matters
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tc.cardtype_code                                    cardtype_code       ,\n                decode(app.app_type,\n                   :1  , decode( tc.cardtype_code,  :2  , 4,  :3  , 2, 3),\n                  decode(tc.cardtype_code,  :4  , 1,  :5  , 2, 3 ))        auth_fee_order,\n--                decode(tc.tranchrg_float_disc_flag  ,'Y','V',  'F') chargeDiscMethod    ,\n                'G'                                                 chargeDiscMethod    ,\n                nvl(tc.tranchrg_interchangefee_fee  , 0.00  )       chargeIcFee         ,\n                nvl(tc.tranchrg_interchangefee_type , 0     )       chargeIcType        ,\n                nvl(tc.tranchrg_discrate_type       , 0     )       chargeDiscRateType  ,\n                nvl(tc.tranchrg_disc_rate           , 0.00  )       chargeDiscRate      ,\n                nvl(tc.tranchrg_aru_fee             , 0.00  )       chargeAruFee        ,\n                nvl(tc.tranchrg_voice_fee           , 0.00  )       chargeVoiceFee      ,\n                nvl(tc.tranchrg_avs_fee             , 0.00  )       chargeAvsFee        ,\n                nvl(tc.tranchrg_per_auth            , 0.00  )       chargePerAuth       ,\n                nvl(tc.tranchrg_per_tran            , 0.00  )       chargePerTran       ,\n                nvl(tc.tranchrg_pass_thru           , 0.00  )       chargePassThru      ,\n                nvl(tc.tranchrg_mmin_chrg           , 0.00  )       chargeMonthlyMin    ,\n                nvl(mpo.merchpo_card_merch_number   , 0     )       mpoMerchNum         ,\n                nvl(tc.pin_debit_pass_thru          , 'N'   )       pinDebitPassThru    ,\n                decode(ct.cardtype, null, '--', 'P1', 'PL', ct.cardtype)  cardtype_name\n        from    (select * from  tranchrg        where app_seq_num =  :6  ) tc,\n                (select * from  merchpayoption  where app_seq_num =  :7  ) mpo,\n                cardtype                                        ct,\n                application app\n        where     tc.cardtype_code = mpo.cardtype_code(+)\n              and tc.cardtype_code =  ct.cardtype_code\n              and tc.app_seq_num = app.app_seq_num\n              and decode(app.app_type,  :8  , decode(tc.cardtype_code,  :9  , 0, 1), 1) = 1\n        order by auth_fee_order    -- at present, the auth fees are the only ones where order matters";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_TYPE_SVB);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(3,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(4,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(5,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(6,appSeqNum);
   __sJT_st.setInt(7,appSeqNum);
   __sJT_st.setInt(8,mesConstants.APP_TYPE_SVB);
   __sJT_st.setInt(9,mesConstants.APP_CT_INTERNET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1393^7*/
      rs = it.getResultSet();

      final int[][] cardTypeValues = {
      // 0 = tc.cardtype_code
      // 1 = idx_arrays_mif_X         CT_xxx values index into mif_X arrays (local to this file); -1 if n/a
      // 2 = auth_fee_type            1=absolute (if find, use for all), 2=standard (if find, use for all except overridden), 3=override, 0=none
      // 3 = do_discount_and_avs      1=yes / 0=no
      // 4 = idx_array_bankCardInfo   0-5 index into array; -1 if n/a
      // 5 = idx_array_passThruInfo   0-1 index into array; -1 if n/a
      // ~~~~~~~~~~~~~~~~~~~0~~~~~~~~~~~~~~~~~~~~ , ~~~~~1~~~~~ , ~~2~~ , ~~3~~ , ~~4~~ , ~~5~~ }
        { mesConstants.APP_CT_VISA                ,   CT_VS     ,   2   ,   1   ,   0   ,   0   } ,
        { mesConstants.APP_CT_VISA_CHECK_CARD     ,  -1         ,   0   ,   1   ,   2   ,   1   } ,
        { mesConstants.APP_CT_VISA_BUSINESS_CARD  ,  -1         ,   0   ,   1   ,   3   ,  -1   } ,
        { mesConstants.APP_CT_MC                  ,   CT_MC     ,   0   ,   1   ,   1   ,  -1   } ,
        { mesConstants.APP_CT_MC_CHECK_CARD       ,  -1         ,   0   ,   1   ,   5   ,  -1   } ,
        { mesConstants.APP_CT_MC_BUSINESS_CARD    ,  -1         ,   0   ,   1   ,   4   ,  -1   } ,
        { mesConstants.APP_CT_AMEX                ,   CT_AMEX   ,   3   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_DISCOVER            ,   CT_DISC   ,   3   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_DINERS_CLUB         ,   CT_DINR   ,   3   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_JCB                 ,   CT_JCB    ,   3   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_DEBIT               ,   CT_DEBIT  ,   0   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_PRIVATE_LABEL_1     ,   CT_PVTL1  ,   0   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_EBT                 ,  -1         ,   0   ,   0   ,  -1   ,  -1   } ,
        { mesConstants.APP_CT_INTERNET            ,  -1         ,   1   ,   0   ,  -1   ,  -1   } ,
        { -1 /* when no special row for APP_CT */ ,  -1         ,   0   ,   0   ,  -1   ,  -1   } };

      boolean           lastAuthWasAbsolute   = false;
      int               bankCardInfoCount     = 0;
      BillingInfo[]     bankCardInfo          = { null, null, null, null, null, null };
      double[][]        passThruInfo          = { {-1.00, -1.00}, {-1.00, -1.00} };

      while( rs.next() )
      {
        double    temp_perItem          = 0.00;
        String    temp_discType         = "G";
        String    temp_plan             = "DN";           // ach_option || auth_option
        double    temp_chargeDiscRate   = rs.getDouble("chargeDiscRate");
        double    temp_chargePerTran    = rs.getDouble("chargePerTran");
        double    temp_chargePassThru   = rs.getDouble("chargePassThru");
        String    temp_cardtype_name    = rs.getString("cardtype_name");
        int       temp_cardtype_code    = rs.getInt("cardtype_code");
        int       temp_cardtype_idx;
        int       temp_int;

        // set temp_cardtype_idx to the correct row in cardTypeValues[]
        for( temp_cardtype_idx = 0; temp_cardtype_idx < cardTypeValues.length - 1; ++temp_cardtype_idx )
        {
          if( cardTypeValues[temp_cardtype_idx][0] == temp_cardtype_code )break;    // 0 = tc.cardtype_code
        }

        // normalize chargePassThru - if it is -1 in the database treat it as zero
        temp_chargePassThru = (temp_chargePassThru < 0 ? 0 : temp_chargePassThru);

        // set variables for use here and for use later
        switch( temp_cardtype_code )
        {
          case mesConstants.APP_CT_VISA:
            feePerItem[FEE_MIN_DISCOUNT]  = rs.getDouble("chargeMonthlyMin");
            mif.minimum_monthly_discount  = rs.getDouble("chargeMonthlyMin");

            tcVisaAuthFeeAru              = rs.getDouble("chargeAruFee");
            tcVisaAuthFeeVoice            = rs.getDouble("chargeVoiceFee");

            tcVisaDiscRateType            = rs.getInt("chargeDiscRateType");
            tcVisaIcType                  = rs.getInt("chargeIcType");
            tcVisaIcFee                   = rs.getDouble("chargeIcFee");

          // fall through
          case mesConstants.APP_CT_VISA_CHECK_CARD:
          case mesConstants.APP_CT_VISA_BUSINESS_CARD:
          case mesConstants.APP_CT_MC:
          case mesConstants.APP_CT_MC_CHECK_CARD:
          case mesConstants.APP_CT_MC_BUSINESS_CARD:
            switch( aptAppType )
            {
              // temp_discType only used for _VISA & _MC; for the other four, cardTypeValues[][idx_arrays_mif_X] = -1
              case mesConstants.APP_TYPE_ELM_NON_DEPLOY   :   temp_discType = "N";                  break;

              case mesConstants.APP_TYPE_SABRE            :   temp_perItem = temp_chargePassThru;   break;
              case mesConstants.APP_TYPE_TRANSCOM         :   temp_perItem = temp_chargePerTran;    break;
              case mesConstants.APP_TYPE_BANNER           :   temp_perItem = temp_chargePassThru;   break;

              default                                     :   temp_perItem = temp_chargePassThru;   break;
            }
            break;


          case mesConstants.APP_CT_AMEX:
            if( aptAppType == mesConstants.APP_TYPE_SABRE )
            {
              // set up amex discount rate and per item
              temp_perItem = temp_chargePassThru;
            }
            else if(amexOptBlue.equalsIgnoreCase("N")){
               temp_plan = "NN";
            }
            break;

          case mesConstants.APP_CT_DISCOVER:
            if( aptAppType == mesConstants.APP_TYPE_SABRE )
            {
              // set up amex discount rate and per item
              temp_perItem = temp_chargePassThru;
            }
            else
            {
              temp_plan = "NN";
            }
            break;

          case mesConstants.APP_CT_DIAL_PAY:
            tcDPayPerItem                 = temp_chargePerTran;
            break;

          case mesConstants.APP_CT_DEBIT:
            switch( aptAppType )
            {
              // add .10 to debit fee for Transcom and Mes New app types to mirror the effects of the individual plan bet that would have been assigned
              case mesConstants.APP_TYPE_TRANSCOM:
              case mesConstants.APP_TYPE_MES_NEW:
                feePerItem[FEE_PIN_DEBIT] = temp_chargePerTran + 0.10;
                break;

              case mesConstants.APP_TYPE_STERLING_3942:
              case mesConstants.APP_TYPE_RIVERVIEW:
              case mesConstants.APP_TYPE_CBT_NEW:
                break;
                
              default:
                feePerItem[FEE_PIN_DEBIT]     = temp_chargePerTran;
                break;
            }
            break;
            
          case mesConstants.APP_CT_DEBIT_PIN_PASS_THRU:
              mif.debit_pass_through = rs.getString("pinDebitPassThru");
              feePerItem[FEE_PIN_DEBIT] = temp_chargePassThru;
              debitPinPassThruRate      = temp_chargeDiscRate;
            break;

          case mesConstants.APP_CT_EBT:
            feePerItem[FEE_EBT_DEBIT]     = temp_chargePerTran;
            break;

          default:
            break;
        }

        temp_int = cardTypeValues[temp_cardtype_idx][1];    // 1 = idx_arrays_mif_X
        if( temp_int >= 0 && temp_int < CT_LEN )
        {
//        mif_dXXaccpt      [temp_int]  = "Y"                               ;
          mif_X_disc_table  [temp_int]  = "9999"                            ;
          mif_X_disc_method [temp_int]  = rs.getString("chargeDiscMethod")  ;
          mif_X_disc_type   [temp_int]  = temp_discType                     ;
          mif_X_disc_rate   [temp_int]  = (int)(temp_chargeDiscRate * 1000) ;
          mif_X_per_item    [temp_int]  = (int)(temp_perItem        * 1000) ;
          mif_X_plan        [temp_int]  = temp_plan                         ;
          mif_X_mid         [temp_int]  = rs.getLong("mpoMerchNum")         ;
        }

        if( lastAuthWasAbsolute == false )
        {
          temp_int = cardTypeValues[temp_cardtype_idx][2];    // 2 = auth_fee_type
          if( temp_int != 0 )
          {
                    lastAuthWasAbsolute   = ( temp_int == 1 );
            double  authAmt               = ( temp_int == 2 ) ? rs.getDouble("chargePerAuth") : temp_chargePerTran;

            // special v/mc auth case for some app types
            if( temp_int == 2 )
            {
              switch( aptAppType )
              {
                case mesConstants.APP_TYPE_MES:
                  authAmt = temp_chargePerTran;
                  break;
              }
            }
            String  authName              = ( temp_int == 3 ) ? temp_cardtype_name            : null;

            BillingInfo   billInfo  = new BillingInfo( MBS_ELEMENTS_AUTH, authName, 0.00, authAmt, 1 );

            addBillingInfo( billInfo );
          }
        }

        if( aptAppType == mesConstants.APP_TYPE_SABRE || cardTypeValues[temp_cardtype_idx][3] != 0 )     // 3 = do_discount_and_avs
        {
          BillingInfo billInfo  = new BillingInfo( MBS_ELEMENTS_DISCOUNT, temp_cardtype_name, temp_chargeDiscRate, temp_perItem, 1 );
          addBillingInfo( billInfo );

          temp_int = cardTypeValues[temp_cardtype_idx][4];    // 4 = idx_array_bankCardInfo
          if( temp_int >= 0 )
          {
            bankCardInfo[temp_int] = new BillingInfo(billInfo);
            ++bankCardInfoCount;
          }

          temp_int = cardTypeValues[temp_cardtype_idx][5];    // 5 = idx_array_passThruInfo
          if( temp_int >= 0 )
          {
            passThruInfo[temp_int][0] = temp_chargeDiscRate;
            passThruInfo[temp_int][1] = temp_chargePassThru;
          }
        }
      }
      rs.close();
      it.close();

      tcSeparatePassThru = passThruInfo[1][0] >= 0 && ( passThruInfo[0][0] != passThruInfo[1][0] ||
                                                        passThruInfo[0][1] != passThruInfo[1][1] );

      /*
      if( bankCardInfoCount > 2 && bankCardInfoCount < 6 )
      {
        // Most apps only have VS & MC pricing, so the other plans (VB/VD & MB/MD) get the same valus as VS & MC;
        // but if the app supports the split pricing, use the Vx and Mx values if they exist, but default to VS & MC.

        // So  if( VS/MC/VD != null && VB/MB/MD == null)  then change card type for VS/MC/VD to "VB/MB/MD" and add as a new row
        // but if( VS/MC/VD == null )                     then do nothing now because we have no default value to use
        //  or if( VB/MB/MD != null )                     then do nothing now because it had a row with its own pricing added earlier

        final String[]  newCardNames = {"VD","VB","MB","MD"};
        for( int idx = 0; idx < 4; ++idx )
        {
          if( bankCardInfo[idx]!= null   &&   bankCardInfo[idx+3] == null )
          {
            bankCardInfo[idx].itemTypeString = newCardNames[idx];
            addBillingInfo( bankCardInfo[idx] );
          }
        }
      }
      */

      // TB, 4/15/2014: fixing array index exception in above code
      //
      // Exception was being triggered when apps were being submitted
      // with empty check card pricing fields.
      //
      // Assumes card type positioning in bankCardInfo, based on above 
      // logic (see col 4 above in cardTypeValues):
      //
      //  bankCardInfo[0] - VS
      //  bankCardInfo[1] - MC
      //  bankCardInfo[2] - VD
      //  bankCardInfo[3] - VB
      //  bankCardInfo[4] - MB
      //  bankCardInfo[5] - MD
      //
      // Copy VS -> VB, MC -> MB, VD -> MD if available and needed.

      // if VS is available and VB is not provided, copy VS to VB
      if (bankCardInfo[0] != null && bankCardInfo[3] == null) {
        BillingInfo bi = new BillingInfo(bankCardInfo[0]);
        bi.itemTypeString = "VB";
        addBillingInfo(bi);
      }
      
      // if MC is available and MB is not provided, copy MC to MB
      if (bankCardInfo[1] != null && bankCardInfo[4] == null) {
        BillingInfo bi = new BillingInfo(bankCardInfo[1]);
        bi.itemTypeString = "MB";
        addBillingInfo(bi);
      }
      
      // if VD is available and MD is not provided, copy VD to MD
      if (bankCardInfo[2] != null && bankCardInfo[5] == null) {
        BillingInfo bi = new BillingInfo(bankCardInfo[2]);
        bi.itemTypeString = "MD";
        addBillingInfo(bi);
      }
      
      // End TB change.  NOTE: the code above seems redundant with
      // the next section which also creates BillingInfo elements
      // for the same card types.

      // look for missing discount info (non-VS or MC) and remedy
      double visaRate = 0.0;
      double visaPerItem = 0.0;
      double mcRate = 0.0;
      double mcPerItem = 0.0;

      boolean addVisaBusiness   = true;
      boolean addVisaCheckCard  = true;
      boolean addMCBusiness     = true;
      boolean addMCCheckCard    = true;

      log.debug("remedying missing discount info");
      for( int wtf = 0; wtf < billInfoRows.size(); ++wtf )
      {
        BillingInfo bi = (BillingInfo)billInfoRows.elementAt(wtf);

        if( bi.itemTypeInt == MBS_ELEMENTS_DISCOUNT )
        {
          if( ("VS").equals(bi.itemTypeString) )
          {
            visaRate    = bi.multiplier;
            visaPerItem = bi.chargePerItem;
          }

          if( ("MC").equals(bi.itemTypeString) )
          {
            mcRate      = bi.multiplier;
            mcPerItem   = bi.chargePerItem;
          }

          if( ("VB").equals(bi.itemTypeString) )
          {
            addVisaBusiness = false;
          }

          if( ("VD").equals(bi.itemTypeString) )
          {
            addVisaCheckCard = false;
          }

          if( ("MB").equals(bi.itemTypeString) )
          {
            addMCBusiness = false;
          }

          if( ("MD").equals(bi.itemTypeString) )
          {
            addMCCheckCard = false;
          }
        }
      }

      if( addVisaBusiness == true )
      {
        BillingInfo bi = new BillingInfo(MBS_ELEMENTS_DISCOUNT, "VB", visaRate, visaPerItem, 1);
        addBillingInfo(bi);
      }
      if( addVisaCheckCard == true )
      {
        BillingInfo bi = new BillingInfo(MBS_ELEMENTS_DISCOUNT, "VD", visaRate, visaPerItem, 1);
        addBillingInfo(bi);
      }
      if( addMCBusiness == true )
      {
        BillingInfo bi = new BillingInfo(MBS_ELEMENTS_DISCOUNT, "MB", mcRate, mcPerItem, 1);
        addBillingInfo(bi);
      }
      if( addMCCheckCard == true )
      {
        BillingInfo bi = new BillingInfo(MBS_ELEMENTS_DISCOUNT, "MD", mcRate, mcPerItem, 1);
        addBillingInfo(bi);
      }

      // get data from tables "miscchrg" & "miscdescrs"
      progress = "getting data from tables MISCCHRG and MISCDESCRS";
      log.debug(progress);
      /*@lineinfo:generated-code*//*@lineinfo:1747^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(mc.misc_code, -1)               misc_code       ,
//                  mc.misc_chrg_amount                 misc_amount     ,
//                  nvl(nvl(mc.misc_chrgbasis_code, mcd.misc_chrg_type),'MIS') misc_type,
//                  mc.misc_chrgbasis_descr             misc_desc       ,
//                  nvl(mcd.chrgbasis_code, 'NA')       miscdescrs_type ,
//                  upper(mcd.misc_description)         miscdescrs_desc ,
//                  nvl(mc.installment_plan_months, 0)  payment_months
//          from    miscchrg        mc,
//                  miscdescrs      mcd
//          where   mc.app_seq_num = :appSeqNum
//                  and mc.misc_code = mcd.misc_code
//                  and nvl(mc.misc_chrg_amount,0.00) <> 0.00
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(mc.misc_code, -1)               misc_code       ,\n                mc.misc_chrg_amount                 misc_amount     ,\n                nvl(nvl(mc.misc_chrgbasis_code, mcd.misc_chrg_type),'MIS') misc_type,\n                mc.misc_chrgbasis_descr             misc_desc       ,\n                nvl(mcd.chrgbasis_code, 'NA')       miscdescrs_type ,\n                upper(mcd.misc_description)         miscdescrs_desc ,\n                nvl(mc.installment_plan_months, 0)  payment_months\n        from    miscchrg        mc,\n                miscdescrs      mcd\n        where   mc.app_seq_num =  :1  \n                and mc.misc_code = mcd.misc_code\n                and nvl(mc.misc_chrg_amount,0.00) <> 0.00";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1761^7*/
      /*      
      #sql [Ctx] it =
      {
        select  nvl(mc.misc_code, -1)               misc_code       ,
                mc.misc_chrg_amount                 misc_amount     ,
                mc.misc_chrgbasis_code              misc_type       ,
                mc.misc_chrgbasis_descr             misc_desc       ,
                nvl(mcd.chrgbasis_code, 'NA')       miscdescrs_type ,
                upper(mcd.misc_description)         miscdescrs_desc ,
                nvl(mc.installment_plan_months, 0)  payment_months
        from    miscchrg        mc,
                miscdescrs      mcd
        where   mc.app_seq_num = :appSeqNum
                and mc.misc_code = mcd.misc_code
                and nvl(mc.misc_chrg_amount,0.00) <> 0.00
      };
      */
      rs = it.getResultSet();

      while( rs.next() )
      {
        int     miscCode  = rs.getInt("misc_code");
        double  miscAmt   = rs.getDouble("misc_amount");
        String  miscDesc  = rs.getString("miscdescrs_desc");    // default

        switch( miscCode )
        {
               // we don't build charge records for these
          case mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE       :                                             continue;

               // we don't build charge records for these here
          case mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE         :   feePerItem[FEE_BATCH_ACH]   = miscAmt;    continue;
          case mesConstants.APP_MISC_CHARGE_CHARGEBACK              :   feePerItem[FEE_CHARGEBACK]  = miscAmt;    continue;
          case mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE          :   miscchrgAuthFeeVoice        = miscAmt;    continue;


          case mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE        :
          case mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP  :
            miscDesc  = "";
            if( aptAppType != mesConstants.APP_TYPE_VERISIGN    &&
                aptAppType != mesConstants.APP_TYPE_NSI         )
            {
              // get data from tables "pos_category" & "merch_pos"
              progress = "getting data from tables POS_CATEGORY and MERCH_POS";
              try
              {
                /*@lineinfo:generated-code*//*@lineinfo:1808^17*/

//  ************************************************************
//  #sql [Ctx] { select  pos_desc
//                    
//                    from    pos_category pc,
//                            merch_pos    mp
//                    where   mp.app_seq_num  = :appSeqNum
//                            and mp.pos_code = pc.pos_code
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_desc\n                   \n                  from    pos_category pc,\n                          merch_pos    mp\n                  where   mp.app_seq_num  =  :1  \n                          and mp.pos_code = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   miscDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1816^17*/
              }
              catch(Exception e)
              {
                miscDesc = "Internet Service";
              }
            }
            if( miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP )
            {
              miscDesc = "Setup Fee-" + miscDesc;
            }
            else // if( miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE )
            {
              miscDesc = "Monthly Fee-" + miscDesc;
            }
            break;

          case mesConstants.APP_MISC_CHARGE_MISC1_FEE               :
          case mesConstants.APP_MISC_CHARGE_MISC2_FEE               :
          case mesConstants.APP_MISC_CHARGE_UNIQUE_FEE              :
            miscDesc = rs.getString("misc_desc");
            break;

          case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE   :
            if( aptAppType == mesConstants.APP_TYPE_CBT_NEW )
            {
              miscDesc = "Merchant Statement Fee";
            }
            break;

          default:
            break;
        }

        String miscType = rs.getString("misc_type");
        
        /* don't think we need this any more - charge type should be driven from table miscdescrs
        if( miscCode == mesConstants.APP_MISC_CHARGE_POS )
        {
          miscType = "POS";
        }
        else
        {
          switch( aptAppType )
          {
            case mesConstants.APP_TYPE_CBT:
            case mesConstants.APP_TYPE_CBT_NEW:
              miscType = "MIS";
              break;

            case mesConstants.APP_TYPE_MES_NEW:
            case mesConstants.APP_TYPE_TRANSCOM:
              switch( miscCode )
              {
                case mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE:
                case mesConstants.APP_MISC_CHARGE_PINPAD_SWAP_FEE:
                  miscType = "POS";
                  break;
                case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP:
                  miscType = "MIS";
                  break;
              }
              break;
          }
        }
        */

        char      billFreq;
        int       paymentMonths = rs.getInt("payment_months");
        int       delayMonths   = aptConversionDelayMonths;
        if( miscCode == mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE ||
            miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING ||
            miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_ADMINISTRATIVE_FEE
            )
        {
          if (aptAppType == mesConstants.APP_TYPE_ADVANCED_PAYMENT_SYSTEM && miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_ADMINISTRATIVE_FEE) {
            billFreq        = 'D';    // December 
          } else {
            System.out.println("***** Setting month to be November (" + miscCode + ") *****");
            billFreq        = 'N';    // November;  paymentMonths = 1 (once-per-year) but we can say 0 (or whatever)
          }
        }
        else if (miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_FEE_JAN )
        {
          billFreq = 'J'; // January
        }
        else if (miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_COMPLIANCE_DEC || miscCode == mesConstants.APP_MISC_CHARGE_ANNUAL_COMPLIANCE_REPORTING_SVB) // SVB wants the Annual Compliance Fee to be charged in Dec
        {
          billFreq        = 'D';    // December
        }
        else if( "YR".equals(rs.getString("miscdescrs_type")) )
        {
          billFreq        = 'Y';    // yearly;    paymentMonths = 1 (once-per-year) but we can say 0 (or whatever)
        }
        else  if( "MC".equals(rs.getString("misc_type")) ||
                  "MC".equals(rs.getString("miscdescrs_type")) )
        {
          if( aptAppType == mesConstants.APP_TYPE_STOREFRONT && miscCode == mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE )
          {
            ++delayMonths;
          }
          billFreq        = 'M';    // monthly;   paymentMonths = 12 (every month)  but we can say 0 (or whatever)
        }
        else if( "QT".equals(rs.getString("misc_type")) ||
                 "QT".equals(rs.getString("miscdescrs_type")) )
        {
          // Quarterly
          billFreq        = 'Q';    // Quarterly
        }
        else
        {
          billFreq        = 'P';    // payments
        }

        //PROD-1322
        //this is a temporary fix until all "Monthly PCI Compliance Fee" descriptions changed to "PCI Compliance Fee"
        if (aptAppType == mesConstants.APP_TYPE_BANK_OF_THE_PRAIRIE && miscCode == mesConstants.APP_MISC_CHARGE_PCI_MONTHLY && miscDesc.equalsIgnoreCase("Monthly PCI Compliance Fee")) {
          miscDesc = "PCI Compliance Fee";
        }
        
        BillingInfo   billInfo  = new BillingInfo( miscCode, miscType, miscDesc, 0, miscAmt, billFreq, paymentMonths, delayMonths, 1, aptAppType );
        addBillingInfo( billInfo );
      }
      rs.close();
      it.close();


      // finalize feePerItem[FEE_BATCH_ACH] for this app type
      log.debug("finalizing FEE_XXX");
      switch( aptAppType )
      {
        case mesConstants.APP_TYPE_TRANSCOM:
        case mesConstants.APP_TYPE_FNMS:
        case mesConstants.APP_TYPE_MES_NEW:           /* use miscchrg.achDeposit  */          break;

        case mesConstants.APP_TYPE_COUNTRY_CLUB:      feePerItem[FEE_BATCH_ACH]   = 0.26;     break;

        case mesConstants.APP_TYPE_CBT:
        case mesConstants.APP_TYPE_CBT_3941:
        case mesConstants.APP_TYPE_CBT_NEW:           feePerItem[FEE_BATCH_ACH]   = 0.20;     break;

        default:                                      feePerItem[FEE_BATCH_ACH]   = -1.00;     break;
      }

      // finalize feePerItem[FEE_CHARGEBACK] for this app type
      switch( aptAppType )
      {
        case mesConstants.APP_TYPE_NET_SUITE:
        case mesConstants.APP_TYPE_SABRE:
        case mesConstants.APP_TYPE_NSI:
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_VERISIGN_V2:       feePerItem[FEE_CHARGEBACK]  = 20.00;    break;

        case mesConstants.APP_TYPE_COUNTRY_CLUB:
          if( feePerItem[FEE_CHARGEBACK] != 0.0 &&
              feePerItem[FEE_CHARGEBACK] != 15.0 )    feePerItem[FEE_CHARGEBACK]  = 20.00;    break;  // if not $0 or $15, then $20

        default:                                      /* use miscchrg.chargeback  */          break;
      }

      // finalize feePerItem[FEE_ARU_AUTH] for this app type
      switch( aptAppType )
      {
        case mesConstants.APP_TYPE_AUTHNET:
        case mesConstants.APP_TYPE_NET_SUITE:
        case mesConstants.APP_TYPE_SABRE:             feePerItem[FEE_ARU_AUTH]    =  0.00;    break;

        case mesConstants.APP_TYPE_NSI:
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_VERISIGN_V2:       feePerItem[FEE_ARU_AUTH]    =  0.30;    break;

        case mesConstants.APP_TYPE_CBT:
        case mesConstants.APP_TYPE_CBT_NEW:           feePerItem[FEE_ARU_AUTH]    = tcVisaAuthFeeAru;   break;

        case mesConstants.APP_TYPE_DISCOVER_IMS:      feePerItem[FEE_ARU_AUTH]    = tcDPayPerItem;      break;

        default:
               if( miscchrgAuthFeeVoice > 0.00 )      feePerItem[FEE_ARU_AUTH]    = miscchrgAuthFeeVoice; // use miscchrg.voice
          else if( tcVisaAuthFeeVoice   > 0.00 )      feePerItem[FEE_ARU_AUTH]    = tcVisaAuthFeeVoice;   // use tranchrg.voice for Visa
          else                                        feePerItem[FEE_ARU_AUTH]    = tcDPayPerItem;        // use tranchrg.perTran for dial pay
          break;
      }

      // insert data from tables "tranchrg" & "miscchrg" and hard-coded data
      log.debug("inserting billinfo data from tranchrg and miscchrg");
      for( int idx = 0; idx < NUM_FEES; ++idx )
      {
        if( feePerItem[idx] >= 0.00 ) // we want to ensure zero dollar amount fees get added to the database
        {
          BillingInfo billInfo = null;
          if( aptAppType == mesConstants.APP_TYPE_CBT_3941 && idx == FEE_PIN_DEBIT )
          {
            // add 0.95% to debit pricing item
            billInfo = new BillingInfo( feeTypeInt[idx], null, 0.95, feePerItem[idx], 1 );
          }
          else if( (aptAppType == mesConstants.APP_TYPE_STERLING_3942 || aptAppType == mesConstants.APP_TYPE_RIVERVIEW || aptAppType == mesConstants.APP_TYPE_CBT_NEW) 
                   && idx == FEE_PIN_DEBIT)
          {
            billInfo  = new BillingInfo( feeTypeInt[idx], debitPinPassThruRate, feePerItem[idx] );
          }
          else
          {
            billInfo  = new BillingInfo( feeTypeInt[idx], feePerItem[idx] );
          }
          if( idx == FEE_MIN_DISCOUNT )
          {
            // start with the first of this-or-next month then add delay months (conversion delay if any, else min discount delay if any)
            Calendar calStart = (Calendar)cal_01_thisOrNext.clone();
            int delayMonths = (aptConversionDelayMonths > 0) ? aptConversionDelayMonths : aptMinDiscountDelayMonths;
            if( delayMonths > 0 ) calStart.add( Calendar.MONTH, delayMonths );
            billInfo.dateStart = new Date(calStart.getTimeInMillis()) ;
          }
          addBillingInfo( billInfo );
        }
      }

      // get data from tables "merchequipment" & "equiplendtype" & "equipment"
      progress = "getting data from tables MERCHEQUIPMENT and EQUIPLENDTYPE and EQUIPMENT";
      log.debug(progress);
      int       ownedQty  = 0;
      double    ownedAmt  = 0.0;
      double    taxRate   = -1.0;

      /*@lineinfo:generated-code*//*@lineinfo:2039^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equiplendtype_code                 equip_code      ,
//                  me.equiptype_code                     equip_type      ,
//                  nvl(me.merchequip_equip_quantity,1)   equip_qty       ,
//                  me.merchequip_amount                  equip_amt       ,
//                  nvl(me.payment_plan_months, 0)        payment_months  ,
//                  elt.equiplendtype_description         equiplend_desc  ,
//                  eq.tsys_equip_desc                    equiptsys_desc  ,
//                  upper(rtrim(ltrim(elt.equiplendtype_description||' '||
//                    substr(eq.tsys_equip_desc, 1, 40)))) stmt_message
//          from    merchequipment  me,
//                  equiplendtype   elt,
//                  equipment       eq
//          where   me.app_seq_num  = :appSeqNum
//                  and me.equiplendtype_code = elt.equiplendtype_code
//                  and me.equip_model        = eq.equip_model
//                  and nvl(me.merchequip_amount,0.00) <> 0.00
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equiplendtype_code                 equip_code      ,\n                me.equiptype_code                     equip_type      ,\n                nvl(me.merchequip_equip_quantity,1)   equip_qty       ,\n                me.merchequip_amount                  equip_amt       ,\n                nvl(me.payment_plan_months, 0)        payment_months  ,\n                elt.equiplendtype_description         equiplend_desc  ,\n                eq.tsys_equip_desc                    equiptsys_desc  ,\n                upper(rtrim(ltrim(elt.equiplendtype_description||' '||\n                  substr(eq.tsys_equip_desc, 1, 40)))) stmt_message\n        from    merchequipment  me,\n                equiplendtype   elt,\n                equipment       eq\n        where   me.app_seq_num  =  :1  \n                and me.equiplendtype_code = elt.equiplendtype_code\n                and me.equip_model        = eq.equip_model\n                and nvl(me.merchequip_amount,0.00) <> 0.00";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2057^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        double    equipAmt    = rs.getDouble("equip_amt");
        int       equipCode   = rs.getInt("equip_code");
        int       equipQty    = rs.getInt("equip_qty");

        if( equipCode == mesConstants.APP_EQUIP_OWNED )
        {
          ownedQty += equipQty;
          ownedAmt  = equipAmt;
        }
        else
        {
          boolean   makeSalesTaxChargeRecord  = false;
          char      billFreq;
          int       paymentMonths = rs.getInt("payment_months");
          if( equipCode == mesConstants.APP_EQUIP_RENT )
          {
            if( aptAppType == mesConstants.APP_TYPE_CBT_NEW )
            {
              makeSalesTaxChargeRecord = true;
            }
            billFreq        = 'M';    // monthly;   paymentMonths = 12 (every month) but we can say 0 (or whatever)
          }
          else
          {
            billFreq        = 'P';    // payments
          }
          BillingInfo   billInfo  = new BillingInfo( equipCode,
                                                    (rs.getInt("equip_type") == mesConstants.APP_EQUIP_TYPE_IMPRINTER) ? "IMP" : "POS",
                                                    rs.getString("stmt_message"),
                                                    equipQty, equipAmt,
                                                    billFreq, paymentMonths, aptConversionDelayMonths, 1, aptAppType );
          addBillingInfo( billInfo );

          // AFTER storing the charge record, use it to create a sales tax record if necessary
          if( makeSalesTaxChargeRecord )
          {
            if( taxRate < 0 ) // only do this once per appSeqNum
            {
              String  taxZip   = "";
              String  taxCity  = "";

              // get data from table "address"
              /*@lineinfo:generated-code*//*@lineinfo:2104^15*/

//  ************************************************************
//  #sql [Ctx] { select  substr(address_zip, 1, 5) , address_city
//                  
//                  from    address,
//                          (
//                            select  max(addresstype_code) as maxtype
//                            from    address
//                            where   app_seq_num   = :appSeqNum
//                                    and addresstype_code  in ( :mesConstants.ADDR_TYPE_SHIPPING, :mesConstants.ADDR_TYPE_MAILING, :mesConstants.ADDR_TYPE_BUSINESS )
//                                    and address_zip       is not null
//                                    and address_city      is not null
//                          )
//                  where   app_seq_num   = :appSeqNum
//                          and addresstype_code  = maxtype
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  substr(address_zip, 1, 5) , address_city\n                 \n                from    address,\n                        (\n                          select  max(addresstype_code) as maxtype\n                          from    address\n                          where   app_seq_num   =  :1  \n                                  and addresstype_code  in (  :2  ,  :3  ,  :4   )\n                                  and address_zip       is not null\n                                  and address_city      is not null\n                        )\n                where   app_seq_num   =  :5  \n                        and addresstype_code  = maxtype";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   __sJT_st.setInt(3,mesConstants.ADDR_TYPE_MAILING);
   __sJT_st.setInt(4,mesConstants.ADDR_TYPE_BUSINESS);
   __sJT_st.setInt(5,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   taxZip = (String)__sJT_rs.getString(1);
   taxCity = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2119^15*/

              // now get tax amount
              /*@lineinfo:generated-code*//*@lineinfo:2122^15*/

//  ************************************************************
//  #sql [Ctx] { select  sales_tax_rate(:taxZip, :taxCity)
//                  
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sales_tax_rate( :1  ,  :2  )\n                 \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,taxZip);
   __sJT_st.setString(2,taxCity);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   taxRate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2127^15*/
            }

            // create new BilingInfo from existing equipment item
            BillingInfo   billInfotax  = new BillingInfo( billInfo );
            billInfotax.statementMsg   = "RENTAL SALES TAX";
            billInfotax.multiplier     = 1;
            billInfotax.chargePerItem  = MesMath.round(taxRate * equipAmt * equipQty, 2);
            addBillingInfo( billInfotax );
          }
        }
      }
      rs.close();
      it.close();

      if( ownedQty > 0 )
      {
        // billFreq = 'M' (monthly) && paymentMonths = 12 (every month) but we can say 0
        BillingInfo   billInfo  = new BillingInfo( 0, "MIS", "OWNED EQUIPMENT SUPPORT FEE", ownedQty, ownedAmt,
                                                    'M', 0, aptConversionDelayMonths, 1, aptAppType );
        addBillingInfo( billInfo );
      }

      // make special changes for sic codes
      switch( Integer.valueOf(mif.sic_code).intValue() )
      {
        case 5542:    mif.cat_interc_value_flag = "Y";    break;
      }

      // make special changes for banks
      if( mif.bank_number == mesConstants.BANK_ID_CBT )
      {
        if( mif.manager_name == null )
        {
          // get data from table "merchcontact"
          progress = "getting data from table MERCHCONTACT";
          /*@lineinfo:generated-code*//*@lineinfo:2163^11*/

//  ************************************************************
//  #sql [Ctx] { select  upper( trim( substr(mcnt.merchcont_prim_first_name || ' ' || mcnt.merchcont_prim_last_name,1,25 )))
//              
//              from    merchcontact   mcnt
//              where   mcnt.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper( trim( substr(mcnt.merchcont_prim_first_name || ' ' || mcnt.merchcont_prim_last_name,1,25 )))\n             \n            from    merchcontact   mcnt\n            where   mcnt.app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.manager_name = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2169^11*/
        }
        if( mif.rep_code == null )
        {
          mif.rep_code = "0000";  // make sure rep code is either all zeroes or what was entered on app
        }
      }
      

      // make special changes for app types
      log.debug("making special changes for app types");
      switch( aptAppType )
      {
        case mesConstants.APP_TYPE_CBT:
        case mesConstants.APP_TYPE_CBT_NEW:
          {
            int pcPosType = 0;
            // get data from tables "merch_pos" & "pos_category"
            progress = "getting data from tables MERCH_POS and POS_CATEGORY";
            /*@lineinfo:generated-code*//*@lineinfo:2188^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(pc.pos_type,0)
//                
//                from    merch_pos     mp,
//                        pos_category  pc
//                where   mp.app_seq_num   = :appSeqNum and
//                        mp.pos_code      = pc.pos_code
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(pc.pos_type,0)\n               \n              from    merch_pos     mp,\n                      pos_category  pc\n              where   mp.app_seq_num   =  :1   and\n                      mp.pos_code      = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pcPosType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2196^13*/

            if( pcPosType == mesConstants.APP_PAYSOL_DIAL_TERMINAL      ||
                pcPosType == mesConstants.APP_PAYSOL_WIRELESS_TERMINALS ||
                merchIndustryType == 9 )
            {
              mif.merit_eligibility   = "Y";    // merit  = yes
              mif.dmer3ind            = "Y";    // merit3 = yes
            }
            else
            {
              mif.merit_eligibility   = "Y";    // merit  = yes
              mif.dmer3ind            = "N";    // merit3 = no (so merit 1)
            }
          }
          break;


        case mesConstants.APP_TYPE_BCB:
          mif.daily_discount_interchange    = "I";
          break;

        case mesConstants.APP_TYPE_NSI:
        case mesConstants.APP_TYPE_PAYPAL_REFERRAL:
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_VERISIGN_V2:
          mif.daily_discount_interchange    = "D";
          break;

        default:
          break;
      }


      // set v/mc ic bet
      try
      {
        progress = "getting IC bet value";
        log.debug(progress);
        /*@lineinfo:generated-code*//*@lineinfo:2235^9*/

//  ************************************************************
//  #sql [Ctx] { select  interchange_bet_visa
//            
//            from    billcard_bet
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  interchange_bet_visa\n           \n          from    billcard_bet\n          where   app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.ic_bet_visa = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2241^9*/
      }
      catch(Exception e)
      {
        mif.ic_bet_visa = 0;
      }

      if( mif.ic_bet_visa == 0 ) // was not set in billcard_bet for some reason
      {
        log.debug("ic bet wasn't set in billcard_bet, using app types");
        int[] betsThisAppType = null;

        switch( aptAppType )
        {
          case mesConstants.APP_TYPE_MES: // everything but MOTO gets 4032
            switch( merchPricingGrid )
            {
              case mesConstants.APP_PG_HOTEL:                   mif.ic_bet_visa = 4033;   break;
              default:                                          mif.ic_bet_visa = 4032;   break;
            }
            break;

          case mesConstants.APP_TYPE_SVB:
            switch( merchPricingGrid )
            {
              case mesConstants.APP_PG_MOTO:                mif.ic_bet_visa = 4053;   break;
              case mesConstants.APP_PG_SUPERMARKET:         mif.ic_bet_visa = 4132;   break;
              default:                                      mif.ic_bet_visa = 4052;   break;
            }
            break;

          case mesConstants.APP_TYPE_NSI:
          case mesConstants.APP_TYPE_VERISIGN:
          case mesConstants.APP_TYPE_VERISIGN_V2:           mif.ic_bet_visa = 4094;   break;

          case mesConstants.APP_TYPE_SABRE:                 mif.ic_bet_visa = 4000;   break;
          case mesConstants.APP_TYPE_NET_SUITE:             mif.ic_bet_visa = 4081;   break;

          case mesConstants.APP_TYPE_DISCOVER_IMS:
            switch( tcVisaDiscRateType )
            {
              case mesConstants.IMS_RETAIL_RESTAURANT_PLAN:     mif.ic_bet_visa = 4070;   break;
              case mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN: mif.ic_bet_visa = 4071;   break;
            }
            break;

          case mesConstants.APP_TYPE_GOLD:
            switch( tcVisaDiscRateType )
            {
              case mesConstants.APP_PS_INTERCHANGE:             mif.ic_bet_visa = 4001;   break;

              default:
                switch( merchPricingGrid )
                {
                  case mesConstants.APP_PG_HOTEL:                   mif.ic_bet_visa = 4009;   break;
                  case mesConstants.APP_PG_MOTO:                    mif.ic_bet_visa = 4031;   break;
                //case mesConstants.APP_PG_RETAIL:
                  default:                                          mif.ic_bet_visa = 4030;   break;
                }
                break;
            }
            break;

          case mesConstants.APP_TYPE_MOUNTAIN_WEST:
            switch( tcVisaDiscRateType )
            {
              case mesConstants.APP_PS_INTERCHANGE:             mif.ic_bet_visa = 4001;   break;
              case mesConstants.APP_PS_MTWEST_4090_5090:        mif.ic_bet_visa = 4090;   break;
              case mesConstants.APP_PS_MTWEST_4178_5178:        mif.ic_bet_visa = 4178;   break;
              case mesConstants.APP_PS_MTWEST_4179_5179:        mif.ic_bet_visa = 4179;   break;
              case mesConstants.APP_PS_MTWEST_4180_5180:        mif.ic_bet_visa = 4180;   break;

              case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
                // merchBetTypeCode  1- 2, array  0- 1
                betsThisAppType = new int[] { 4050, 4054 };
                break;
            }
            break;

          case mesConstants.APP_TYPE_ELM_NON_DEPLOY:
            switch( tcVisaDiscRateType )
            {
              case mesConstants.ELM_APP_PS_BUY_RATE_PLUS:       mif.ic_bet_visa = 4131;   break;  // special buy rate ic bets
              case mesConstants.ELM_APP_PS_FLAT_RATE:           mif.ic_bet_visa = 4064;   break;

              case mesConstants.ELM_APP_PS_RATE_PER_ITEM:
              case mesConstants.ELM_APP_PS_RATE_ONLY:
              case mesConstants.ELM_APP_PS_TIERED:
                // merchBetTypeCode  1- 4, array  0- 3
                betsThisAppType = new int[] { 4067, 4066, 4065, 4063 };
                break;
            }
            break;

          case mesConstants.APP_TYPE_BANNER:
            switch( tcVisaDiscRateType )
            {
              case mesConstants.APP_PS_INTERCHANGE:             mif.ic_bet_visa = 4001;   break;

              case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
                // merchBetTypeCode 1-36 (0 for 16-19), array 0-35
                betsThisAppType = new int[] { 4074, 4075, 4076, 4077, 4078, 4079, 4080, 4000, 4085, 4086, 4087, 4088,
                                              4089, 4084, 4001,    0,    0,    0,    0, 4141, 4142, 4143, 4144, 4148,
                                              4145, 4146, 4147, 4149, 4150, 4151, 4152, 4153, 4127, 4128, 4083, 4100, 4051 };
                break;
            }
            break;

          case mesConstants.APP_TYPE_TRANSCOM:
            // get data from table "transcom_merchant"
            /*@lineinfo:generated-code*//*@lineinfo:2351^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(bet_table, 0)
//                
//                from    transcom_merchant
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(bet_table, 0)\n               \n              from    transcom_merchant\n              where   app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.ic_bet_visa = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2357^13*/
            if( mif.ic_bet_visa == 4040 ) mif.ic_bet_mc = 5001;
            break;

          case mesConstants.APP_TYPE_CBT:
          case mesConstants.APP_TYPE_CBT_NEW:
            // deal with special situations where separate check card rate chosen for passthru interchange choice
            if( tcVisaIcType == 1 && tcSeparatePassThru )
            {
              // need separate bet
              if( tcVisaIcFee == 1 ) // retail
              {
                mif.ic_bet_visa = 1310;
                mif.ic_bet_mc   = 1470;
                break;  // this was a special situation; break the switch()
              }
              if( tcVisaIcFee == 2 ) // moto
              {
                mif.ic_bet_visa = 1311;
                mif.ic_bet_mc   = 1471;
                break;  // this was a special situation; break the switch()
              }
            }

          // fall through if this is NOT a special situation
          default:
            // get data from table "appo_ic_bets"
            /*@lineinfo:generated-code*//*@lineinfo:2384^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(vs_bet,0),
//                        nvl(mc_bet,0)
//                
//                from    appo_ic_bets
//                where   bet_type = :tcVisaIcType and
//                        combo_id = :tcVisaIcFee
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(vs_bet,0),\n                      nvl(mc_bet,0)\n               \n              from    appo_ic_bets\n              where   bet_type =  :1   and\n                      combo_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,tcVisaIcType);
   __sJT_st.setDouble(2,tcVisaIcFee);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mif.ic_bet_visa = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mif.ic_bet_mc = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2393^13*/
            break;
        }

        if( betsThisAppType != null && merchBetTypeCode > 0 && merchBetTypeCode <= betsThisAppType.length )
        {
          mif.ic_bet_visa = betsThisAppType[merchBetTypeCode - 1];
        }

        if( mif.ic_bet_mc == 0 && mif.ic_bet_visa != 0 )
            mif.ic_bet_mc      =  mif.ic_bet_visa + 1000;
      }
      
      
      if (aptAppType == mesConstants.APP_TYPE_CMDI) {
        mif.ic_bet_visa = 4000;
        mif.remit_fees  = "Y";
        if( mif.ic_bet_mc == 0) {
          mif.ic_bet_mc =  mif.ic_bet_visa + 1000;
        }
      }


      // settlement fee (individual plan bet in TSYS parlance)
      log.debug("creating settlement fee");
      BillingInfo billInfoSettlement = null;
      switch( aptAppType )
      {
        case mesConstants.APP_TYPE_SABRE:
          billInfoSettlement = new BillingInfo(MBS_ELEMENTS_SETTLEMENT_FEE, null, 0.0, 0.12, 3);
          addBillingInfo( billInfoSettlement );
          break;
      }
      
      //Settlement, Batch, and Credits Fees for Demosphere
      if (aptAppType == mesConstants.APP_TYPE_DEMOSPHERE) { 
        BillingInfo billInfoSettlementFee = new BillingInfo(MBS_ELEMENTS_SETTLEMENT_FEE, null, 0.0, 0.15, 3);
        addBillingInfo( billInfoSettlementFee );

        BillingInfo billInfoBatchFee = new BillingInfo(MBS_ELEMENTS_BATCH, null, 0.0, 0.25, 3);
        addBillingInfo( billInfoBatchFee );

        BillingInfo billInfoCreditsFee = new BillingInfo(MBS_ELEMENTS_CREDIT_FEE, null, 0.0, 0.08, 2);
        addBillingInfo( billInfoCreditsFee );
      }

      if( !"6010".equals(mif.sic_code) && !"8398".equals(mif.sic_code) && !"8661".equals(mif.sic_code) )
      {
        mif.mc_merch_loc_fee = "Y";
      }

      // if not cash advance, add card-brand-specific fees
      if(
          !"6010".equals(mif.sic_code) &&
          !"6011".equals(mif.class_code)
//          && !"6012".equals(mif.class_code)  // 6012 is apparently not a cash advance SIC
        )
      {
        progress = "getting rep-assigned fees";
        log.debug(progress);
        /*@lineinfo:generated-code*//*@lineinfo:2453^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  af.item_type                  item_type,
//                    nvl(mes.item_subclass,'NONE') item_subclass,
//                    af.assoc_rate                 per_item
//            from    assocfee af,
//                    mbs_element_subclass mes                  
//            where   af.app_seq_num = :appSeqNum
//                    and af.item_type = mes.item_type
//            order by af.item_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  af.item_type                  item_type,\n                  nvl(mes.item_subclass,'NONE') item_subclass,\n                  af.assoc_rate                 per_item\n          from    assocfee af,\n                  mbs_element_subclass mes                  \n          where   af.app_seq_num =  :1  \n                  and af.item_type = mes.item_type\n          order by af.item_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2463^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          BillingInfo billInfo = new BillingInfo(0, 0.000);
          billInfo.itemTypeInt = rs.getInt("item_type");
          if( "NONE".equals(rs.getString("item_subclass")) )
          {
            billInfo.itemTypeString = null;
          }
          else
          {
            billInfo.itemTypeString = rs.getString("item_subclass");
          }
          billInfo.multiplier = 0;
          billInfo.chargePerItem = rs.getDouble("per_item");
          
          if( ! containsItem(billInfo) )
          {
           addBillingInfo( billInfo );
          }
        }
        
        rs.close();
        it.close();
        
        progress = "getting default fees (" + merchBankAssoc + ")";
        log.debug("getting default fees (" + merchBankAssoc + ")");

        // get fee defaults from database based on merchant's hierarchy node
        Vector defaultFees = MBSDefaultFees._loadDefaultFees(Long.parseLong(merchBankAssoc));

        for( int idx = 0; idx < defaultFees.size(); ++idx )
        {
          FeeItem fi = (FeeItem)defaultFees.elementAt(idx);
          
          if( fi.ItemType == -100 ) // Visa FANF Fee
          {
            if( fi.CreateFee == true )
            {
              mif.visa_fanf = "Y";
            }
            else
            {
              mif.visa_fanf = "N";
            }
          }
          else
          {
            if( fi.CreateFee == true && ( fi.Rate != 0.0 || fi.PerItem != 0.0 ) )
            {
              if( (fi.ItemType == MBS_ELEMENTS_CREDIT_VOUCHER_CREDIT || fi.ItemType == MBS_ELEMENTS_CREDIT_VOUCHER_DEBIT || 
                  fi.ItemType == MBS_ELEMENTS_VS_FIXED_ACCESS || fi.ItemType == MBS_ELEMENTS_MC_FIXED_ACCESS || 
                  fi.ItemType == MBS_ELEMENTS_VS_CARD_VEFIFY_FEE || fi.ItemType == MBS_ELEMENTS_MC_CARD_VEFIFY_FEE || 
                  (fi.ItemType == MBS_ELEMENTS_DATA_TRANSMISSION && fi.ItemSubclass.equalsIgnoreCase("VS"))) &&
                  mif.sic_code != null && mif.sic_code.equals("6010") ) {
                //Do not charge Cash Advance merchants, SIC Code 6010, the following fees
                //Credit voucher, VMC Fixed Access, VMC Card Verify, Data Transmission VS fees
                continue;
              } else if( (fi.ItemType == MBS_ELEMENTS_AM_OPTB_NETWORK_FEE || fi.ItemType == MBS_ELEMENTS_AM_OPTB_INBOUND_FEE || 
                         fi.ItemType == MBS_ELEMENTS_AM_OPTB_NON_SWIPE_TRAN_FEE || fi.ItemType == MBS_ELEMENTS_AM_OPTB_NON_COMPL_FEE || 
                         fi.ItemType == MBS_ELEMENTS_AM_OPTB_DATA_QUALITY_FEE) && 
                         !amexOptBlue.equals("Y") ) {
                //Ignore fees for AmexOptblue if amexOptblue checkbox is not checked
                continue;
              } else {
              /*
              if (fi.ItemType == MBS_ELEMENTS_CREDIT_VOUCHER_DEBIT) {
                fi.ItemSubclass = "VD";
              }
              */

              BillingInfo billInfo = new BillingInfo( 0, 0.000 );
              billInfo.itemTypeInt = fi.ItemType;
              if( fi.ItemSubclass == null || fi.ItemSubclass.equals("null") )
              {
                billInfo.itemTypeString = null;
              }
              else
              {
                billInfo.itemTypeString = fi.ItemSubclass;
              }
              billInfo.multiplier = fi.Rate;
              billInfo.chargePerItem = fi.PerItem;

              if( ! containsItem( billInfo ) && applyNonSwipeFee(fi.ItemType) )
              {
                addBillingInfo( billInfo );
              }
              }
            }
          }
        }

        if (aptAppType == mesConstants.APP_TYPE_CMDI) {
          mif.visa_fanf = "N";
        }
        
        // get charge defaults from database based on merchant's hierarchy node
        log.debug("getting default charge Items (" + merchBankAssoc + ")");
        Vector defaultCharges = MBSDefaultFees._loadDefaultCharges(Long.parseLong(merchBankAssoc));

        for( int idx = 0; idx < defaultCharges.size(); ++idx )
        {
          ChargeItem ci = (ChargeItem)defaultCharges.elementAt(idx);

          if( ci.CreateCharge == true && ( ci.ChargeAmount != 0.0) )
          {
            Calendar startMonth = Calendar.getInstance();
            startMonth.set( Calendar.DAY_OF_MONTH, 1 );
            startMonth.add( Calendar.MONTH, ci.DelayMonths );

            BillingInfo billInfo = new BillingInfo( -1, 0.000 );

            billInfo.itemTypeString = ci.ChargeType;
            billInfo.statementMsg   = ci.StatementMessage;
            billInfo.multiplier     = 1;
            billInfo.chargePerItem  = ci.ChargeAmount;
            billInfo.billingMonths  = ci.BillingFrequency;
            billInfo.dateStart      = new Date( startMonth.getTimeInMillis() );

            if( ! containsItem( billInfo ) )
            {
              addBillingInfo( billInfo );
            }
          }
        }
      }

      retVal = true;
    }
    catch(Exception e)
    {
      try
      {
        if( e.toString().length() > 40 && e.toString().substring(0, 40).equals("java.lang.Exception: Invalid Association") )
        {
          // send message to some folks
          sendFailureEmail(mif.merchant_number, merchBankAssoc);
        }
      }
      catch(Exception fe)
      {
      }

      loadAppsLogEntry("gatherMerchantInfo(" + appSeqNum + "): "+ progress, e.toString());
      resetApp(appSeqNum);
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    return( retVal );
  }

  private void sendFailureEmail( long merchantNumber, String associationNode )
  {
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_NEW_ACCOUNT_LOAD_FAIL); // 163
      msg.setSubject("ACCOUNT LOAD FAILURE (" + merchantNumber + ")");

      StringBuffer  msgBody = new StringBuffer("");
      msgBody.append("Merchant " + merchantNumber + " has been assigned an invalid association number\r\n");
      msgBody.append("The invalid association number is:\r\n");
      msgBody.append("      ");
      msgBody.append(associationNode);

      msg.setText(msgBody.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendFailureEmail(" + merchantNumber + ")", e.toString());
    }
  }

  private boolean applyNonSwipeFee(int itemType) {
     return !(mif != null && ( itemType == 220 && excludeNonSwipeFeeMCCList.contains(mif.sic_code)));
  }
  
  private boolean insertMerchantInfo( int appSeqNum, boolean testMode )
  {
    boolean   retVal    = false;
    int       recCount  = 0;

    try
    {
      if( testMode == true )
      {
        // display card account info
        System.out.println("Discover #: " + mif_X_mid[CT_DISC]);
        System.out.println("Amex #:     " + mif_X_mid[CT_AMEX]);
        // display billing info
        System.out.println("Item Count: " + billInfoRows.size());

        // display billing items
        if( billInfoRows.size() > 0 )
        {
          for( int idx = 0; idx < billInfoRows.size(); ++idx )
          {
            BillingInfo   row = (BillingInfo)billInfoRows.elementAt(idx);

            row.display();
          }
        }
        
        // display irs exemptions
        System.out.println("IRS Exemptions: ->" + mif.irs_exemptions + "<-");
      }

      // only actually create the account if we're not in test mode
      if( testMode == false )
      {
        // make sure there are no records for this merchant_number
                            { /*@lineinfo:generated-code*//*@lineinfo:2682^31*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)    from mif                  where merchant_number = :mif.merchant_number   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)     from mif                  where merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2682^153*/  }
        if( recCount == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:2683^31*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)    from mbs_pricing          where merchant_number = :mif.merchant_number   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)     from mbs_pricing          where merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2683^153*/  }
        if( recCount == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:2684^31*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)    from mbs_charge_records   where merchant_number = :mif.merchant_number   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)     from mbs_charge_records   where merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2684^153*/  }

        // clean up some field lengths
        if( mif_X_name[2] != null && mif_X_name[2].length() > 32 )
        {
          mif_X_name[2] = mif_X_name[2].substring(0, 32);
        }

        if( recCount == 0 )
        {
          int     aptAppType  = -1;
          /*@lineinfo:generated-code*//*@lineinfo:2695^11*/

//  ************************************************************
//  #sql [Ctx] { select  apt.app_type_code
//              
//              from    application     app,
//                      app_type        apt
//              where   app.app_seq_num = :appSeqNum
//                      and app.app_type = apt.app_type_code
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  apt.app_type_code\n             \n            from    application     app,\n                    app_type        apt\n            where   app.app_seq_num =  :1  \n                    and app.app_type = apt.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   aptAppType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2703^11*/
          if (aptAppType == mesConstants.APP_TYPE_PACWEST &&  
              mif.exception_merchants_code != null && mif.exception_merchants_code.trim().length() > 0) {
            mif.pacwest_with_exception = "Y";
          }
          // store the merchant record in "mif"
          /*@lineinfo:generated-code*//*@lineinfo:2709^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mif
//              (
//                merchant_number                   , -- number(16)
//                bank_number                       , -- number(4)
//                dmagent                           , -- varchar2(6 byte)
//                sic_code                          , -- char(4 byte)
//                class_code                        , -- char(4 byte)
//                transit_routng_num                , -- number(9)
//                dda_num                           , -- varchar2(17 byte)
//                deposit_acct_type                 , -- varchar2(2 byte)
//                discount_routing                  , -- varchar2(9 byte)
//                discount_dda                      , -- varchar2(17 byte)
//                discount_acct_type                , -- varchar2(2 byte)
//                charge_back_routing               , -- varchar2(9 byte)
//                charge_back_dda                   , -- varchar2(17 byte)
//                charge_back_acct_type             , -- varchar2(2 byte)
//                federal_tax_id                    , -- number(9)
//                ssn                               , -- varchar2(9 byte)
//                license_number                    , -- varchar2(9 byte)
//                owner_name                        , -- varchar2(25 byte)
//                manager_name                      , -- varchar2(25 byte)
//                fdr_corp_name                     , -- varchar2(25 byte)
//                dba_name                          , -- varchar2(32 byte)
//                dmaddr                            , -- varchar2(32 byte)
//                address_line_3                    , -- varchar2(32 byte)
//                dmcity                            , -- varchar2(24 byte)
//                dmstate                           , -- varchar2(2 byte)
//                dmzip                             , -- varchar2(9 byte)
//                name1_line_1                      , -- varchar2(32 byte)
//                addr1_line_1                      , -- varchar2(32 byte)
//                addr1_line_2                      , -- varchar2(32 byte)
//                city1_line_4                      , -- varchar2(24 byte)
//                state1_line_4                     , -- varchar2(2 byte)
//                zip1_line_4                       , -- varchar2(9 byte)
//                name2_line_1                      , -- varchar2(32 byte)
//                addr2_line_1                      , -- varchar2(32 byte)
//                addr2_line_2                      , -- varchar2(32 byte)
//                city2_line_4                      , -- varchar2(24 byte)
//                state2_line_4                     , -- varchar2(2 byte)
//                zip2_line_4                       , -- varchar2(9 byte)
//                phone_1                           , -- varchar2(10 byte)
//                phone_2_fax                       , -- varchar2(10 byte)
//                customer_service_phone            , -- varchar2(10 byte)
//                cat_interc_value_flag             , -- char(1 byte)
//                print_statements                  , -- varchar2(1 byte)
//                edc_flagst_other_3                , -- char(1 byte)
//                smk1_eligibility                  , -- char(1 byte)
//                smk2_eligibility                  , -- char(1 byte)
//                eps_eligibility                   , -- char(1 byte)
//                club_eligibility                  , -- char(1 byte)
//                merit_eligibility                 , -- char(1 byte)
//                dmer3ind                          , -- varchar2(1 byte)
//                daily_discount_interchange        , -- varchar2(1 byte)
//                daily_ach                         , -- varchar2(1 byte)
//                visa_fanf                         , -- varchar2(1 byte)
//                mc_merch_loc_fee                  , -- varchar2(1 byte)
//                suspended_days                    , -- number(3)
//                minimum_monthly_discount          , -- number(7,2)
//                ic_bet_visa                       , -- number(4)
//                ic_bet_mc                         , -- number(4)
//                dmdsnum                           , -- number(15)
//                diners_club_number                , -- varchar2(10 byte)
//                damexse                           , -- varchar2(10 byte)
//                visa_disc_table                   , -- varchar2(4 byte)
//                visa_disc_method                  , -- char(1 byte)
//                visa_disc_type                    , -- char(1 byte)
//                visa_disc_rate                    , -- number(7) (3 implied decimal places)
//                visa_per_item                     , -- number(7) (3 implied decimal places)
//                mastcd_disc_table                 , -- varchar2(4 byte)
//                mastcd_disc_method                , -- char(1 byte)
//                mastcd_disc_type                  , -- char(1 byte)
//                mastcd_disc_rate                  , -- number(7) (3 implied decimal places)
//                mastcd_per_item                   , -- number(7) (3 implied decimal places)
//                discvr_disc_table                 , -- varchar2(4 byte)
//                discvr_disc_method                , -- char(1 byte)
//                discvr_disc_type                  , -- char(1 byte)
//                discvr_disc_rate                  , -- number(7) (3 implied decimal places)
//                discvr_per_item                   , -- number(7) (3 implied decimal places)
//                diners_disc_table                 , -- varchar2(4 byte)
//                diners_disc_method                , -- char(1 byte)
//                diners_disc_type                  , -- char(1 byte)
//                diners_disc_rate                  , -- number(7) (3 implied decimal places)
//                diners_per_item                   , -- number(7) (3 implied decimal places)
//                jcb_disc_table                    , -- varchar2(4 byte)
//                jcb_disc_method                   , -- char(1 byte)
//                jcb_disc_type                     , -- char(1 byte)
//                jcb_disc_rate                     , -- number(7) (3 implied decimal places)
//                jcb_per_item                      , -- number(7) (3 implied decimal places)
//                amex_rate_table                   , -- varchar2(4 byte)
//                amex_discount_rate                , -- number(7) (3 implied decimal places)
//                visa_plan                         , -- varchar2(2 byte)
//                mastcd_plan                       , -- varchar2(2 byte)
//                debit_plan                        , -- varchar2(2 byte)
//                discover_plan                     , -- varchar2(2 byte)
//                diners_plan                       , -- varchar2(2 byte)
//                jcb_plan                          , -- varchar2(2 byte)
//                amex_plan                         , -- varchar2(2 byte)
//                private_label_1_plan              , -- varchar2(2 byte)
//                rep_code                          , -- varchar2(4 byte)
//                user_data_1                       , -- varchar2(4 byte)
//                user_data_2                       , -- varchar2(4 byte)
//                user_data_3                       , -- varchar2(4 byte)
//                user_data_4                       , -- varchar2(16 byte)
//                user_data_5                       , -- varchar2(16 byte)
//                user_acount_1                     , -- varchar2(16 byte)
//                user_account_2                    , -- varchar2(16 byte)
//                dbfemid                           , -- varchar2(10 byte)
//                dbfepflg                          , -- varchar2(1 byte)
//                inv_code_investigate              , -- varchar2(4 byte)
//                met_table                         , -- varchar2(4 byte)
//                num_post_date_days_debit          , -- number(2)
//                num_post_date_days_credt          , -- number(2)
//                merchant_type                     , -- varchar2(4 byte)
//                load_filename                     , -- varchar2(32 byte)
//  
//                merchant_verification_val         , -- varchar2(10 byte)
//                visa_partner_program              , -- varchar2(1 byte)
//                mc_program_reg_id                 , -- varchar2(3 byte)
//                mc_assigned_id                    , -- varchar2(6 byte)
//                mc_business_type                  , -- varchar2(4 byte)
//                visa_eligibility_flags            , -- varchar2(64 byte)
//                mc_eligibility_flags              , -- varchar2(64 byte)
//                district                          , -- number(4)
//  
//                amex_pc_id                        , -- varchar2(6 byte)
//                dvsaccpt                          , -- varchar2(1 byte)
//                dmcaccpt                          , -- varchar2(1 byte)
//                dv$accpt                          , -- varchar2(1 byte)
//                dm$accpt                          , -- varchar2(1 byte)
//                ddsaccpt                          , -- varchar2(1 byte)
//                ddcaccpt                          , -- varchar2(1 byte)
//                djcaccpt                          , -- varchar2(1 byte)
//                damaccpt                          , -- varchar2(1 byte)
//                visa_cash_plan                    , -- varchar2(2 byte)
//                mastcd_cash_plan                  , -- varchar2(2 byte)
//                private_label_2_plan              , -- varchar2(2 byte)
//                private_label_3_plan              , -- varchar2(2 byte)
//                trans_dest_global                 , -- varchar2(1 byte)
//                trans_dest_deposit                , -- char(1 byte)
//                trans_dest_adj                    , -- char(1 byte)
//                trans_dest_chgback                , -- char(1 byte)
//                trans_dest_reversal               , -- char(1 byte)
//                trans_dest_cgbk_revrs             , -- char(1 byte)
//                trans_dest_dda_adj                , -- char(1 byte)
//                trans_dest_batch                  , -- char(1 byte)
//                trans_dest_other_1                , -- char(1 byte)
//                trans_dest_other_2                , -- char(1 byte)
//                trans_dest_other_3                , -- char(1 byte)
//                association_node                  , -- number(10)
//                group_1_association               , -- varchar2(6 byte)
//                group_2_association               , -- varchar2(6 byte)
//                group_3_association               , -- varchar2(6 byte)
//                group_4_association               , -- varchar2(6 byte)
//                group_5_association               , -- varchar2(6 byte)
//                group_6_association               , -- varchar2(6 byte)
//                date_opened                       , -- char(6 byte)
//                conversion_date                   , -- date
//                processor_id                      , -- number(4)
//                test_account                      , -- varchar2(1 byte)
//                business_license                  , -- varchar2(10 byte)
//                visa_rebate_percent               , -- number(7)
//                state_tax_id                      , -- number(9)
//                officer_1                         , -- varchar2(4 byte)
//                officer_2                         , -- varchar2(4 byte)
//                dachdest                          , -- varchar2(1 byte)
//                merchant_key                      , -- varchar2(32 byte)
//                activation_date                   , -- date
//                risk_activation_date              , -- date
//                amex_activation_date              , -- date
//                disc_activation_date              , -- date
//                country_code_syf                  , -- varchar2(2 byte)
//                bank_number_syf                   , -- number(4)
//                bank_name_syf                     , -- varchar2(10 byte)
//                other_name                        , -- varchar2(25 byte)
//                name2_line_1_blk6                 , -- varchar2(32 byte)
//                addr2_line_1_blk6                 , -- varchar2(32 byte)
//                addr2_line_2_blk6                 , -- varchar2(32 byte)
//                email_addr_ind_00                 , -- varchar2(2 byte)
//                email_desc_00                     , -- varchar2(25 byte)
//                email_addr_00                     , -- varchar2(75 byte)
//                email_addr_ind_01                 , -- varchar2(2 byte)
//                email_desc_01                     , -- varchar2(25 byte)
//                email_addr_01                     , -- varchar2(75 byte)
//                email_addr_ind_02                 , -- varchar2(2 byte)
//                email_desc_02                     , -- varchar2(25 byte)
//                email_addr_02                     , -- varchar2(75 byte)
//                email_addr_ind_03                 , -- varchar2(2 byte)
//                email_desc_03                     , -- varchar2(25 byte)
//                email_addr_03                     , -- varchar2(75 byte)
//                email_addr_ind_04                 , -- varchar2(2 byte)
//                email_desc_04                     , -- varchar2(25 byte)
//                email_addr_04                     , -- varchar2(75 byte)
//                email_addr_ind_05                 , -- varchar2(2 byte)
//                email_desc_05                     , -- varchar2(25 byte)
//                email_addr_05                     , -- varchar2(75 byte)
//                email_addr_ind_06                 , -- varchar2(2 byte)
//                email_desc_06                     , -- varchar2(25 byte)
//                email_addr_06                     , -- varchar2(75 byte)
//                email_addr_ind_07                 , -- varchar2(2 byte)
//                email_desc_07                     , -- varchar2(25 byte)
//                email_addr_07                     , -- varchar2(75 byte)
//                email_addr_ind_08                 , -- varchar2(2 byte)
//                email_desc_08                     , -- varchar2(25 byte)
//                email_addr_08                     , -- varchar2(75 byte)
//                email_addr_ind_09                 , -- varchar2(2 byte)
//                email_desc_09                     , -- varchar2(25 byte)
//                email_addr_09                     , -- varchar2(75 byte)
//                check_service_est_no              , -- varchar2(16 byte)
//                daily_settlement_plan             , -- varchar2(1 byte)
//                ndc_ve_file_flag                  , -- char(1 byte)
//                tiif_eligibility                  , -- char(1 byte)
//                psrf_eligibility                  , -- char(1 byte)
//                dmcpsind                          , -- varchar2(1 byte)
//                mtd_daily_maint_flag              , -- char(1 byte)
//                user_flag_1                       , -- char(1 byte)
//                user_flag_2                       , -- char(1 byte)
//                confirmatn_letter_flag            , -- char(1 byte)
//                confirmation_deposit              , -- char(3 byte)
//                confirmation_cardhldr             , -- char(1 byte)
//                date_of_1st_deposit               , -- date
//                date_last_deposit                 , -- date
//                last_deposit_date                 , -- date
//                last_deposit_amount               , -- number(12,2)
//                date_stat_chgd_to_dcb             , -- date
//                merchant_status                   , -- char(1 byte)
//                dmacctst                          , -- varchar2(1 byte)
//                date_billed_for_close             , -- date
//                term_contract_start_date          , -- date
//                date_last_activity                , -- date
//                last_changed                      , -- date
//                visa_bin_mes                      , -- varchar2(6 byte)
//                mc_ica_mes                        , -- varchar2(4 byte)
//                bt1_indicator                     , -- varchar2(2 byte)
//                bt2_indicator                     , -- varchar2(2 byte)
//                bt3_indicator                     , -- varchar2(2 byte)
//                debit_pass_through                , -- varchar2(1Byte)
//                pacwest_with_exception            , -- char(1 byte)
//                interchange_credit_value          , -- varchar2(1 byte)
//                remit_fees                        , -- varchar2(1 byte)
//                amex_conversion_date               -- date
//              )
//              values
//              (
//                :mif.merchant_number , -- number(16)
//                :mif.bank_number , -- number(4)
//                :mif.dmagent , -- varchar2(6 byte)
//                :mif.sic_code , -- char(4 byte)
//                :mif.class_code , -- char(4 byte)
//                to_number(:mif_X_transit_routing[1]), -- number(9)
//                :mif_X_dda[1] , -- varchar2(17 byte)
//                :mif_X_acct_type[1] , -- varchar2(2 byte)
//                :mif_X_transit_routing[0] , -- varchar2(9 byte)
//                :mif_X_dda[0] , -- varchar2(17 byte)
//                :mif_X_acct_type[0] , -- varchar2(2 byte)
//                :mif_X_transit_routing[0] , -- varchar2(9 byte)
//                :mif_X_dda[0] , -- varchar2(17 byte)
//                :mif_X_acct_type[0] , -- varchar2(2 byte)
//                :mif.federal_tax_id , -- number(9)
//                :mif.ssn , -- varchar2(9 byte)
//                :mif.license_number , -- varchar2(9 byte)
//                :mif.owner_name , -- varchar2(25 byte)
//                :mif.manager_name , -- varchar2(25 byte)
//                :mif.fdr_corp_name , -- varchar2(25 byte)
//                :merchNameBusiness                , -- varchar2(32 byte)
//                --:(mif_X_name[0]                 ) , -- varchar2(32 byte)
//                :mif_X_addr1[0] , -- varchar2(32 byte)
//                :mif_X_addr2[0] , -- varchar2(32 byte)
//                :mif_X_city[0] , -- varchar2(24 byte)
//                :mif_X_state[0] , -- varchar2(2 byte)
//                :mif_X_zip[0] , -- varchar2(9 byte)
//                :mif_X_name[1] , -- varchar2(32 byte)
//                :mif_X_addr1[1] , -- varchar2(32 byte)
//                :mif_X_addr2[1] , -- varchar2(32 byte)
//                :mif_X_city[1] , -- varchar2(24 byte)
//                :mif_X_state[1] , -- varchar2(2 byte)
//                :mif_X_zip[1] , -- varchar2(9 byte)
//                :mif_X_name[2] , -- varchar2(32 byte)
//                :mif_X_addr1[2] , -- varchar2(32 byte)
//                :mif_X_addr2[2] , -- varchar2(32 byte)
//                :mif_X_city[2] , -- varchar2(24 byte)
//                :mif_X_state[2] , -- varchar2(2 byte)
//                :mif_X_zip[2] , -- varchar2(9 byte)
//                :mif.phone_1 , -- varchar2(10 byte)
//                :mif.phone_2_fax , -- varchar2(10 byte)
//                :mif.customer_service_phone , -- varchar2(10 byte)
//                :mif.cat_interc_value_flag , -- char(1 byte)                                         -- default: N; sic 5542: Y
//                :mif.print_statements , -- varchar2(1 byte)
//                :mif.edc_flagst_other_3 , -- char(1 byte)
//                'N'                               , -- char(1 byte)         :mif.smk1_eligibility           -- default: N; sic 5411: Y
//                'N'                               , -- char(1 byte)         :mif.smk2_eligibility           -- default: N; sic 5411: Y
//                'N'                               , -- char(1 byte)         :mif.eps_eligibility            -- default: N; sic 5812, 5814, 7523, 7832: Y
//                'N'                               , -- char(1 byte)         :mif.club_eligibility           -- default: N; sic 5300: Y
//                :mif.merit_eligibility , -- char(1 byte)
//                :mif.dmer3ind , -- varchar2(1 byte)
//                :mif.daily_discount_interchange , -- varchar2(1 byte)
//                :mif.daily_ach , -- varchar2(1 byte)     :mif.daily_ach                  -- default: Y; other option: N
//                nvl(:mif.visa_fanf, 'N'       ) , -- varchar2(1 byte)
//                nvl(:mif.mc_merch_loc_fee, 'N') , -- varchar2(1 byte)
//                :mif.suspended_days , -- number(3)
//                :mif.minimum_monthly_discount , -- number(7,2)
//                :mif.ic_bet_visa , -- number(4)
//                :mif.ic_bet_mc , -- number(4)
//                decode(:mif_X_mid    [CT_DISC] ,
//                      0, null,
//                      :mif_X_mid     [CT_DISC]         ) , -- number(15)
//                decode(:mif_X_mid    [CT_DINR],
//                      0, null,
//                      lpad(:mif_X_mid[CT_DINR],10,'0') ), -- varchar2(10 byte)
//                decode(:mif_X_mid    [CT_AMEX],
//                      0, null,
//                      lpad(:mif_X_mid[CT_AMEX],10,'0') ), -- varchar2(10 byte)
//                :mif_X_disc_table  [CT_VS    ] , -- varchar2(4 byte)
//                :mif_X_disc_method [CT_VS    ] , -- char(1 byte)
//                :mif_X_disc_type   [CT_VS    ] , -- char(1 byte)
//                :mif_X_disc_rate   [CT_VS    ] , -- number(7) (3 implied decimal places)
//                :mif_X_per_item    [CT_VS    ] , -- number(7) (3 implied decimal places)
//                :mif_X_disc_table  [CT_MC    ] , -- varchar2(4 byte)
//                :mif_X_disc_method [CT_MC    ] , -- char(1 byte)
//                :mif_X_disc_type   [CT_MC    ] , -- char(1 byte)
//                :mif_X_disc_rate   [CT_MC    ] , -- number(7) (3 implied decimal places)
//                :mif_X_per_item    [CT_MC    ] , -- number(7) (3 implied decimal places)
//                :mif_X_disc_table  [CT_DISC  ] , -- varchar2(4 byte)
//                :mif_X_disc_method [CT_DISC  ] , -- char(1 byte)
//                :mif_X_disc_type   [CT_DISC  ] , -- char(1 byte)
//                :mif_X_disc_rate   [CT_DISC  ] , -- number(7) (3 implied decimal places)
//                :mif_X_per_item    [CT_DISC  ] , -- number(7) (3 implied decimal places)
//                :mif_X_disc_table  [CT_DINR  ] , -- varchar2(4 byte)
//                :mif_X_disc_method [CT_DINR  ] , -- char(1 byte)
//                :mif_X_disc_type   [CT_DINR  ] , -- char(1 byte)
//                :mif_X_disc_rate   [CT_DINR  ] , -- number(7) (3 implied decimal places)
//                :mif_X_per_item    [CT_DINR  ] , -- number(7) (3 implied decimal places)
//                :mif_X_disc_table  [CT_JCB   ] , -- varchar2(4 byte)
//                :mif_X_disc_method [CT_JCB   ] , -- char(1 byte)
//                :mif_X_disc_type   [CT_JCB   ] , -- char(1 byte)
//                :mif_X_disc_rate   [CT_JCB   ] , -- number(7) (3 implied decimal places)
//                :mif_X_per_item    [CT_JCB   ] , -- number(7) (3 implied decimal places)
//                :mif_X_disc_table  [CT_AMEX  ] , -- varchar2(4 byte)
//                :mif_X_disc_rate   [CT_AMEX  ] , -- number(7) (3 implied decimal places)
//                :mif_X_plan        [CT_VS    ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_MC    ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_DEBIT ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_DISC  ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_DINR  ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_JCB   ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_AMEX  ] , -- varchar2(2 byte)
//                :mif_X_plan        [CT_PVTL1 ] , -- varchar2(2 byte)
//                :mif.rep_code , -- varchar2(4 byte)
//                :mif.user_data_1 , -- varchar2(4 byte)
//                :mif.user_data_2 , -- varchar2(4 byte)
//                :mif.user_data_3 , -- varchar2(4 byte)
//                :mif.user_data_4 , -- varchar2(16 byte)
//                :mif.user_data_5 , -- varchar2(16 byte)
//                :mif.user_acount_1 , -- varchar2(16 byte)
//                :mif.user_account_2 , -- varchar2(16 byte)
//                :mif.dbfemid , -- varchar2(10 byte)
//                :mif.dbfepflg , -- varchar2(1 byte)
//                :mif.inv_code_investigate , -- varchar2(4 byte)
//                :mif.met_table , -- varchar2(4 byte)
//                :mif.num_post_date_days_debit , -- number(2)
//                :mif.num_post_date_days_credt , -- number(2)
//                :mif.merchant_type , -- varchar2(4 byte)
//                :mif_load_filename , -- varchar2(32 byte)
//  
//                null                              , -- varchar2(10 byte)    :mif.merchant_verification_val
//                null                              , -- varchar2(1 byte)     :mif.visa_partner_program
//                null                              , -- varchar2(3 byte)     :mif.mc_program_reg_id
//                null                              , -- varchar2(6 byte)     :mif.mc_assigned_id
//                :mif.merchant_type , -- varchar2(4 byte)     :mif.merchant_type
//                null                              , -- varchar2(64 byte)    :mif.visa_eligibility_flags
//                null                              , -- varchar2(64 byte)    :mif.mc_eligibility_flags
//                null                              , -- number(4)            :mif.district
//  
//                null                              , -- varchar2(6 byte)     :mif.amex_pc_id
//                null                              , -- varchar2(1 byte)     :mif.dvsaccpt               or  :(mif_dXXaccpt[CT_VS   ])
//                null                              , -- varchar2(1 byte)     :mif.dmcaccpt               or  :(mif_dXXaccpt[CT_MC   ])
//                null                              , -- varchar2(1 byte)     :mif.dv$accpt               or  :(mif_dXXaccpt[CT_VCSH ])
//                null                              , -- varchar2(1 byte)     :mif.dm$accpt               or  :(mif_dXXaccpt[CT_MCSH ])
//                null                              , -- varchar2(1 byte)     :mif.ddsaccpt               or  :(mif_dXXaccpt[CT_DISC ])
//                null                              , -- varchar2(1 byte)     :mif.ddcaccpt               or  :(mif_dXXaccpt[CT_DINR ])
//                null                              , -- varchar2(1 byte)     :mif.djcaccpt               or  :(mif_dXXaccpt[CT_JCB  ])
//                null                              , -- varchar2(1 byte)     :mif.damaccpt               or  :(mif_dXXaccpt[CT_AMEX ])
//                null                              , -- varchar2(2 byte)     :mif.visa_cash_plan         or  :(mif_X_plan  [CT_VCSH ])
//                null                              , -- varchar2(2 byte)     :mif.mastcd_cash_plan       or  :(mif_X_plan  [CT_MCSH ])
//                null                              , -- varchar2(2 byte)     :mif.private_label_2_plan   or  :(mif_X_plan  [CT_PVTL2])
//                null                              , -- varchar2(2 byte)     :mif.private_label_3_plan   or  :(mif_X_plan  [CT_PVTL3])
//                ''                                , -- varchar2(1 byte)     :mif.trans_dest_global
//                'M'                               , -- char(1 byte)         :mif.trans_dest_deposit
//                'M'                               , -- char(1 byte)         :mif.trans_dest_adj
//                'M'                               , -- char(1 byte)         :mif.trans_dest_chgback
//                'M'                               , -- char(1 byte)         :mif.trans_dest_reversal
//                'M'                               , -- char(1 byte)         :mif.trans_dest_cgbk_revrs
//                'M'                               , -- char(1 byte)         :mif.trans_dest_dda_adj
//                'M'                               , -- char(1 byte)         :mif.trans_dest_batch
//                'R'                               , -- char(1 byte)         :mif.trans_dest_other_1
//                'X'                               , -- char(1 byte)         :mif.trans_dest_other_2
//                'X'                               , -- char(1 byte)         :mif.trans_dest_other_3
//                0                                 , -- number(10)           :mif.association_node     // set on insert/update trigger to bank_number||dmagent
//                null                              , -- varchar2(6 byte)     :mif.group_1_association  //@ also needs to be set on insert/update using table "groups"
//                null                              , -- varchar2(6 byte)     :mif.group_2_association  //@ also needs to be set on insert/update using table "groups"
//                null                              , -- varchar2(6 byte)     :mif.group_3_association  //@ also needs to be set on insert/update using table "groups"
//                null                              , -- varchar2(6 byte)     :mif.group_4_association  //@ also needs to be set on insert/update using table "groups"
//                null                              , -- varchar2(6 byte)     :mif.group_5_association  //@ also needs to be set on insert/update using table "groups"
//                null                              , -- varchar2(6 byte)     :mif.group_6_association  //@ also needs to be set on insert/update using table "groups"
//                to_char(sysdate,'mmddrr')         , -- char(6 byte)         :mif.date_opened      = today
//                trunc(sysdate,'mon')              , -- date                 :mif.conversion_date  = this month
//                1                                 , -- number(4)            :mif.processor_id     = 1 / MeS back-end
//                'N'                               , -- varchar2(1 byte)     :mif.test_account
//                '0000000000'                      , -- varchar2(10 byte)    :mif.business_license
//                null                              , -- number(7)            :mif.visa_rebate_percent
//                0                                 , -- number(9)            :mif.state_tax_id
//                '0000'                            , -- varchar2(4 byte)     :mif.officer_1        //@ = "0000" or = null?? now always "0000"
//                '0000'                            , -- varchar2(4 byte)     :mif.officer_2        //@ = "0000" or = null?? now always "0000"
//                'Y'                               , -- varchar2(1 byte)     :mif.dachdest         //@ = "Y"    or = null?  now always Y; in mif, there are 1551 with 'N' & 159834 null
//                null                              , -- varchar2(32 byte)    :mif.merchant_key     // set on insert trigger
//                null                              , -- date                 :mif.activation_date
//                null                              , -- date                 :mif.risk_activation_date
//                null                              , -- date                 :mif.amex_activation_date
//                null                              , -- date                 :mif.disc_activation_date
//                null                              , -- varchar2(2 byte)     :mif.country_code_syf
//                null                              , -- number(4)            :mif.bank_number_syf
//                null                              , -- varchar2(10 byte)    :mif.bank_name_syf
//                null                              , -- varchar2(25 byte)    :mif.other_name
//                null                              , -- varchar2(32 byte)    :mif.name2_line_1_blk6
//                null                              , -- varchar2(32 byte)    :mif.addr2_line_1_blk6
//                null                              , -- varchar2(32 byte)    :mif.addr2_line_2_blk6
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_00
//                null                              , -- varchar2(25 byte)    :mif.email_desc_00
//                null                              , -- varchar2(75 byte)    :mif.email_addr_00
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_01
//                null                              , -- varchar2(25 byte)    :mif.email_desc_01
//                null                              , -- varchar2(75 byte)    :mif.email_addr_01
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_02
//                null                              , -- varchar2(25 byte)    :mif.email_desc_02
//                null                              , -- varchar2(75 byte)    :mif.email_addr_02
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_03
//                null                              , -- varchar2(25 byte)    :mif.email_desc_03
//                null                              , -- varchar2(75 byte)    :mif.email_addr_03
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_04
//                null                              , -- varchar2(25 byte)    :mif.email_desc_04
//                null                              , -- varchar2(75 byte)    :mif.email_addr_04
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_05
//                null                              , -- varchar2(25 byte)    :mif.email_desc_05
//                null                              , -- varchar2(75 byte)    :mif.email_addr_05
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_06
//                null                              , -- varchar2(25 byte)    :mif.email_desc_06
//                null                              , -- varchar2(75 byte)    :mif.email_addr_06
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_07
//                null                              , -- varchar2(25 byte)    :mif.email_desc_07
//                null                              , -- varchar2(75 byte)    :mif.email_addr_07
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_08
//                null                              , -- varchar2(25 byte)    :mif.email_desc_08
//                null                              , -- varchar2(75 byte)    :mif.email_addr_08
//                null                              , -- varchar2(2 byte)     :mif.email_addr_ind_09
//                null                              , -- varchar2(25 byte)    :mif.email_desc_09
//                null                              , -- varchar2(75 byte)    :mif.email_addr_09
//                null                              , -- varchar2(16 byte)    :mif.check_service_est_no
//                null                              , -- varchar2(1 byte)     :mif.daily_settlement_plan
//                null                              , -- char(1 byte)         :mif.ndc_ve_file_flag
//                null                              , -- char(1 byte)         :mif.tiif_eligibility
//                null                              , -- char(1 byte)         :mif.psrf_eligibility
//                null                              , -- varchar2(1 byte)     :mif.dmcpsind
//                null                              , -- char(1 byte)         :mif.mtd_daily_maint_flag
//                null                              , -- char(1 byte)         :mif.user_flag_1
//                null                              , -- char(1 byte)         :mif.user_flag_2
//                null                              , -- char(1 byte)         :mif.confirmatn_letter_flag
//                null                              , -- char(3 byte)         :mif.confirmation_deposit
//                null                              , -- char(1 byte)         :mif.confirmation_cardhldr
//                null                              , -- date                 :mif.date_of_1st_deposit
//                null                              , -- date                 :mif.date_last_deposit
//                null                              , -- date                 :mif.last_deposit_date
//                null                              , -- number(12,2)         :mif.last_deposit_amount
//                null                              , -- date                 :mif.date_stat_chgd_to_dcb
//                null                              , -- char(1 byte)         :mif.merchant_status
//                null                              , -- varchar2(1 byte)     :mif.dmacctst
//                null                              , -- date                 :mif.date_billed_for_close
//                null                              , -- date                 :mif.term_contract_start_date
//                null                              , -- date                 :mif.date_last_activity
//                null                              , -- date                 :mif.last_changed
//                null                              , -- varchar2(6 byte)     :mif.visa_bin_mes
//                null                              , -- varchar2(4 byte)     :mif.mc_ica_mes
//                null                              , -- varchar2(2 byte)     :mif.bt1_indicator
//                null                              , -- varchar2(2 byte)     :mif.bt2_indicator
//                null                              , -- varchar2(2 byte)     :mif.bt3_indicator
//                :mif.debit_pass_through , -- varchar2(1Byte)      :mif.debit_pass_through 
//                :mif.pacwest_with_exception , -- char(1 byte)         :mif.pacwest_with_exception
//                null                              , -- varchar2(1 byte)     :mif.interchange_credit_value
//                :mif.remit_fees  , -- varchar2(1 byte)     :mif.remit_fees
//                trunc(sysdate)               -- date                 :mif.amex_conversion_date
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_7 = mif_X_transit_routing[1];
 String __sJT_8 = mif_X_dda[1];
 String __sJT_9 = mif_X_acct_type[1];
 String __sJT_10 = mif_X_transit_routing[0];
 String __sJT_11 = mif_X_dda[0];
 String __sJT_12 = mif_X_acct_type[0];
 String __sJT_13 = mif_X_transit_routing[0];
 String __sJT_14 = mif_X_dda[0];
 String __sJT_15 = mif_X_acct_type[0];
 String __sJT_16 = mif_X_addr1[0];
 String __sJT_17 = mif_X_addr2[0];
 String __sJT_18 = mif_X_city[0];
 String __sJT_19 = mif_X_state[0];
 String __sJT_20 = mif_X_zip[0];
 String __sJT_21 = mif_X_name[1];
 String __sJT_22 = mif_X_addr1[1];
 String __sJT_23 = mif_X_addr2[1];
 String __sJT_24 = mif_X_city[1];
 String __sJT_25 = mif_X_state[1];
 String __sJT_26 = mif_X_zip[1];
 String __sJT_27 = mif_X_name[2];
 String __sJT_28 = mif_X_addr1[2];
 String __sJT_29 = mif_X_addr2[2];
 String __sJT_30 = mif_X_city[2];
 String __sJT_31 = mif_X_state[2];
 String __sJT_32 = mif_X_zip[2];
 long __sJT_33 = mif_X_mid    [CT_DISC];
 long __sJT_34 = mif_X_mid     [CT_DISC];
 long __sJT_35 = mif_X_mid    [CT_DINR];
 long __sJT_36 = mif_X_mid[CT_DINR];
 long __sJT_37 = mif_X_mid    [CT_AMEX];
 long __sJT_38 = mif_X_mid[CT_AMEX];
 String __sJT_39 = mif_X_disc_table  [CT_VS    ];
 String __sJT_40 = mif_X_disc_method [CT_VS    ];
 String __sJT_41 = mif_X_disc_type   [CT_VS    ];
 int __sJT_42 = mif_X_disc_rate   [CT_VS    ];
 int __sJT_43 = mif_X_per_item    [CT_VS    ];
 String __sJT_44 = mif_X_disc_table  [CT_MC    ];
 String __sJT_45 = mif_X_disc_method [CT_MC    ];
 String __sJT_46 = mif_X_disc_type   [CT_MC    ];
 int __sJT_47 = mif_X_disc_rate   [CT_MC    ];
 int __sJT_48 = mif_X_per_item    [CT_MC    ];
 String __sJT_49 = mif_X_disc_table  [CT_DISC  ];
 String __sJT_50 = mif_X_disc_method [CT_DISC  ];
 String __sJT_51 = mif_X_disc_type   [CT_DISC  ];
 int __sJT_52 = mif_X_disc_rate   [CT_DISC  ];
 int __sJT_53 = mif_X_per_item    [CT_DISC  ];
 String __sJT_54 = mif_X_disc_table  [CT_DINR  ];
 String __sJT_55 = mif_X_disc_method [CT_DINR  ];
 String __sJT_56 = mif_X_disc_type   [CT_DINR  ];
 int __sJT_57 = mif_X_disc_rate   [CT_DINR  ];
 int __sJT_58 = mif_X_per_item    [CT_DINR  ];
 String __sJT_59 = mif_X_disc_table  [CT_JCB   ];
 String __sJT_60 = mif_X_disc_method [CT_JCB   ];
 String __sJT_61 = mif_X_disc_type   [CT_JCB   ];
 int __sJT_62 = mif_X_disc_rate   [CT_JCB   ];
 int __sJT_63 = mif_X_per_item    [CT_JCB   ];
 String __sJT_64 = mif_X_disc_table  [CT_AMEX  ];
 int __sJT_65 = mif_X_disc_rate   [CT_AMEX  ];
 String __sJT_66 = mif_X_plan        [CT_VS    ];
 String __sJT_67 = mif_X_plan        [CT_MC    ];
 String __sJT_68 = mif_X_plan        [CT_DEBIT ];
 String __sJT_69 = mif_X_plan        [CT_DISC  ];
 String __sJT_70 = mif_X_plan        [CT_DINR  ];
 String __sJT_71 = mif_X_plan        [CT_JCB   ];
 String __sJT_72 = mif_X_plan        [CT_AMEX  ];
 String __sJT_73 = mif_X_plan        [CT_PVTL1 ];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into mif\n            (\n              merchant_number                   , -- number(16)\n              bank_number                       , -- number(4)\n              dmagent                           , -- varchar2(6 byte)\n              sic_code                          , -- char(4 byte)\n              class_code                        , -- char(4 byte)\n              transit_routng_num                , -- number(9)\n              dda_num                           , -- varchar2(17 byte)\n              deposit_acct_type                 , -- varchar2(2 byte)\n              discount_routing                  , -- varchar2(9 byte)\n              discount_dda                      , -- varchar2(17 byte)\n              discount_acct_type                , -- varchar2(2 byte)\n              charge_back_routing               , -- varchar2(9 byte)\n              charge_back_dda                   , -- varchar2(17 byte)\n              charge_back_acct_type             , -- varchar2(2 byte)\n              federal_tax_id                    , -- number(9)\n              ssn                               , -- varchar2(9 byte)\n              license_number                    , -- varchar2(9 byte)\n              owner_name                        , -- varchar2(25 byte)\n              manager_name                      , -- varchar2(25 byte)\n              fdr_corp_name                     , -- varchar2(25 byte)\n              dba_name                          , -- varchar2(32 byte)\n              dmaddr                            , -- varchar2(32 byte)\n              address_line_3                    , -- varchar2(32 byte)\n              dmcity                            , -- varchar2(24 byte)\n              dmstate                           , -- varchar2(2 byte)\n              dmzip                             , -- varchar2(9 byte)\n              name1_line_1                      , -- varchar2(32 byte)\n              addr1_line_1                      , -- varchar2(32 byte)\n              addr1_line_2                      , -- varchar2(32 byte)\n              city1_line_4                      , -- varchar2(24 byte)\n              state1_line_4                     , -- varchar2(2 byte)\n              zip1_line_4                       , -- varchar2(9 byte)\n              name2_line_1                      , -- varchar2(32 byte)\n              addr2_line_1                      , -- varchar2(32 byte)\n              addr2_line_2                      , -- varchar2(32 byte)\n              city2_line_4                      , -- varchar2(24 byte)\n              state2_line_4                     , -- varchar2(2 byte)\n              zip2_line_4                       , -- varchar2(9 byte)\n              phone_1                           , -- varchar2(10 byte)\n              phone_2_fax                       , -- varchar2(10 byte)\n              customer_service_phone            , -- varchar2(10 byte)\n              cat_interc_value_flag             , -- char(1 byte)\n              print_statements                  , -- varchar2(1 byte)\n              edc_flagst_other_3                , -- char(1 byte)\n              smk1_eligibility                  , -- char(1 byte)\n              smk2_eligibility                  , -- char(1 byte)\n              eps_eligibility                   , -- char(1 byte)\n              club_eligibility                  , -- char(1 byte)\n              merit_eligibility                 , -- char(1 byte)\n              dmer3ind                          , -- varchar2(1 byte)\n              daily_discount_interchange        , -- varchar2(1 byte)\n              daily_ach                         , -- varchar2(1 byte)\n              visa_fanf                         , -- varchar2(1 byte)\n              mc_merch_loc_fee                  , -- varchar2(1 byte)\n              suspended_days                    , -- number(3)\n              minimum_monthly_discount          , -- number(7,2)\n              ic_bet_visa                       , -- number(4)\n              ic_bet_mc                         , -- number(4)\n              dmdsnum                           , -- number(15)\n              diners_club_number                , -- varchar2(10 byte)\n              damexse                           , -- varchar2(10 byte)\n              visa_disc_table                   , -- varchar2(4 byte)\n              visa_disc_method                  , -- char(1 byte)\n              visa_disc_type                    , -- char(1 byte)\n              visa_disc_rate                    , -- number(7) (3 implied decimal places)\n              visa_per_item                     , -- number(7) (3 implied decimal places)\n              mastcd_disc_table                 , -- varchar2(4 byte)\n              mastcd_disc_method                , -- char(1 byte)\n              mastcd_disc_type                  , -- char(1 byte)\n              mastcd_disc_rate                  , -- number(7) (3 implied decimal places)\n              mastcd_per_item                   , -- number(7) (3 implied decimal places)\n              discvr_disc_table                 , -- varchar2(4 byte)\n              discvr_disc_method                , -- char(1 byte)\n              discvr_disc_type                  , -- char(1 byte)\n              discvr_disc_rate                  , -- number(7) (3 implied decimal places)\n              discvr_per_item                   , -- number(7) (3 implied decimal places)\n              diners_disc_table                 , -- varchar2(4 byte)\n              diners_disc_method                , -- char(1 byte)\n              diners_disc_type                  , -- char(1 byte)\n              diners_disc_rate                  , -- number(7) (3 implied decimal places)\n              diners_per_item                   , -- number(7) (3 implied decimal places)\n              jcb_disc_table                    , -- varchar2(4 byte)\n              jcb_disc_method                   , -- char(1 byte)\n              jcb_disc_type                     , -- char(1 byte)\n              jcb_disc_rate                     , -- number(7) (3 implied decimal places)\n              jcb_per_item                      , -- number(7) (3 implied decimal places)\n              amex_rate_table                   , -- varchar2(4 byte)\n              amex_discount_rate                , -- number(7) (3 implied decimal places)\n              visa_plan                         , -- varchar2(2 byte)\n              mastcd_plan                       , -- varchar2(2 byte)\n              debit_plan                        , -- varchar2(2 byte)\n              discover_plan                     , -- varchar2(2 byte)\n              diners_plan                       , -- varchar2(2 byte)\n              jcb_plan                          , -- varchar2(2 byte)\n              amex_plan                         , -- varchar2(2 byte)\n              private_label_1_plan              , -- varchar2(2 byte)\n              rep_code                          , -- varchar2(4 byte)\n              user_data_1                       , -- varchar2(4 byte)\n              user_data_2                       , -- varchar2(4 byte)\n              user_data_3                       , -- varchar2(4 byte)\n              user_data_4                       , -- varchar2(16 byte)\n              user_data_5                       , -- varchar2(16 byte)\n              user_acount_1                     , -- varchar2(16 byte)\n              user_account_2                    , -- varchar2(16 byte)\n              dbfemid                           , -- varchar2(10 byte)\n              dbfepflg                          , -- varchar2(1 byte)\n              inv_code_investigate              , -- varchar2(4 byte)\n              met_table                         , -- varchar2(4 byte)\n              num_post_date_days_debit          , -- number(2)\n              num_post_date_days_credt          , -- number(2)\n              merchant_type                     , -- varchar2(4 byte)\n              load_filename                     , -- varchar2(32 byte)\n\n              merchant_verification_val         , -- varchar2(10 byte)\n              visa_partner_program              , -- varchar2(1 byte)\n              mc_program_reg_id                 , -- varchar2(3 byte)\n              mc_assigned_id                    , -- varchar2(6 byte)\n              mc_business_type                  , -- varchar2(4 byte)\n              visa_eligibility_flags            , -- varchar2(64 byte)\n              mc_eligibility_flags              , -- varchar2(64 byte)\n              district                          , -- number(4)\n\n              amex_pc_id                        , -- varchar2(6 byte)\n              dvsaccpt                          , -- varchar2(1 byte)\n              dmcaccpt                          , -- varchar2(1 byte)\n              dv$accpt                          , -- varchar2(1 byte)\n              dm$accpt                          , -- varchar2(1 byte)\n              ddsaccpt                          , -- varchar2(1 byte)\n              ddcaccpt                          , -- varchar2(1 byte)\n              djcaccpt                          , -- varchar2(1 byte)\n              damaccpt                          , -- varchar2(1 byte)\n              visa_cash_plan                    , -- varchar2(2 byte)\n              mastcd_cash_plan                  , -- varchar2(2 byte)\n              private_label_2_plan              , -- varchar2(2 byte)\n              private_label_3_plan              , -- varchar2(2 byte)\n              trans_dest_global                 , -- varchar2(1 byte)\n              trans_dest_deposit                , -- char(1 byte)\n              trans_dest_adj                    , -- char(1 byte)\n              trans_dest_chgback                , -- char(1 byte)\n              trans_dest_reversal               , -- char(1 byte)\n              trans_dest_cgbk_revrs             , -- char(1 byte)\n              trans_dest_dda_adj                , -- char(1 byte)\n              trans_dest_batch                  , -- char(1 byte)\n              trans_dest_other_1                , -- char(1 byte)\n              trans_dest_other_2                , -- char(1 byte)\n              trans_dest_other_3                , -- char(1 byte)\n              association_node                  , -- number(10)\n              group_1_association               , -- varchar2(6 byte)\n              group_2_association               , -- varchar2(6 byte)\n              group_3_association               , -- varchar2(6 byte)\n              group_4_association               , -- varchar2(6 byte)\n              group_5_association               , -- varchar2(6 byte)\n              group_6_association               , -- varchar2(6 byte)\n              date_opened                       , -- char(6 byte)\n              conversion_date                   , -- date\n              processor_id                      , -- number(4)\n              test_account                      , -- varchar2(1 byte)\n              business_license                  , -- varchar2(10 byte)\n              visa_rebate_percent               , -- number(7)\n              state_tax_id                      , -- number(9)\n              officer_1                         , -- varchar2(4 byte)\n              officer_2                         , -- varchar2(4 byte)\n              dachdest                          , -- varchar2(1 byte)\n              merchant_key                      , -- varchar2(32 byte)\n              activation_date                   , -- date\n              risk_activation_date              , -- date\n              amex_activation_date              , -- date\n              disc_activation_date              , -- date\n              country_code_syf                  , -- varchar2(2 byte)\n              bank_number_syf                   , -- number(4)\n              bank_name_syf                     , -- varchar2(10 byte)\n              other_name                        , -- varchar2(25 byte)\n              name2_line_1_blk6                 , -- varchar2(32 byte)\n              addr2_line_1_blk6                 , -- varchar2(32 byte)\n              addr2_line_2_blk6                 , -- varchar2(32 byte)\n              email_addr_ind_00                 , -- varchar2(2 byte)\n              email_desc_00                     , -- varchar2(25 byte)\n              email_addr_00                     , -- varchar2(75 byte)\n              email_addr_ind_01                 , -- varchar2(2 byte)\n              email_desc_01                     , -- varchar2(25 byte)\n              email_addr_01                     , -- varchar2(75 byte)\n              email_addr_ind_02                 , -- varchar2(2 byte)\n              email_desc_02                     , -- varchar2(25 byte)\n              email_addr_02                     , -- varchar2(75 byte)\n              email_addr_ind_03                 , -- varchar2(2 byte)\n              email_desc_03                     , -- varchar2(25 byte)\n              email_addr_03                     , -- varchar2(75 byte)\n              email_addr_ind_04                 , -- varchar2(2 byte)\n              email_desc_04                     , -- varchar2(25 byte)\n              email_addr_04                     , -- varchar2(75 byte)\n              email_addr_ind_05                 , -- varchar2(2 byte)\n              email_desc_05                     , -- varchar2(25 byte)\n              email_addr_05                     , -- varchar2(75 byte)\n              email_addr_ind_06                 , -- varchar2(2 byte)\n              email_desc_06                     , -- varchar2(25 byte)\n              email_addr_06                     , -- varchar2(75 byte)\n              email_addr_ind_07                 , -- varchar2(2 byte)\n              email_desc_07                     , -- varchar2(25 byte)\n              email_addr_07                     , -- varchar2(75 byte)\n              email_addr_ind_08                 , -- varchar2(2 byte)\n              email_desc_08                     , -- varchar2(25 byte)\n              email_addr_08                     , -- varchar2(75 byte)\n              email_addr_ind_09                 , -- varchar2(2 byte)\n              email_desc_09                     , -- varchar2(25 byte)\n              email_addr_09                     , -- varchar2(75 byte)\n              check_service_est_no              , -- varchar2(16 byte)\n              daily_settlement_plan             , -- varchar2(1 byte)\n              ndc_ve_file_flag                  , -- char(1 byte)\n              tiif_eligibility                  , -- char(1 byte)\n              psrf_eligibility                  , -- char(1 byte)\n              dmcpsind                          , -- varchar2(1 byte)\n              mtd_daily_maint_flag              , -- char(1 byte)\n              user_flag_1                       , -- char(1 byte)\n              user_flag_2                       , -- char(1 byte)\n              confirmatn_letter_flag            , -- char(1 byte)\n              confirmation_deposit              , -- char(3 byte)\n              confirmation_cardhldr             , -- char(1 byte)\n              date_of_1st_deposit               , -- date\n              date_last_deposit                 , -- date\n              last_deposit_date                 , -- date\n              last_deposit_amount               , -- number(12,2)\n              date_stat_chgd_to_dcb             , -- date\n              merchant_status                   , -- char(1 byte)\n              dmacctst                          , -- varchar2(1 byte)\n              date_billed_for_close             , -- date\n              term_contract_start_date          , -- date\n              date_last_activity                , -- date\n              last_changed                      , -- date\n              visa_bin_mes                      , -- varchar2(6 byte)\n              mc_ica_mes                        , -- varchar2(4 byte)\n              bt1_indicator                     , -- varchar2(2 byte)\n              bt2_indicator                     , -- varchar2(2 byte)\n              bt3_indicator                     , -- varchar2(2 byte)\n              debit_pass_through                , -- varchar2(1Byte)\n              pacwest_with_exception            , -- char(1 byte)\n              interchange_credit_value          , -- varchar2(1 byte)\n              remit_fees                        , -- varchar2(1 byte)\n              amex_conversion_date               -- date\n            )\n            values\n            (\n               :1   , -- number(16)\n               :2   , -- number(4)\n               :3   , -- varchar2(6 byte)\n               :4   , -- char(4 byte)\n               :5   , -- char(4 byte)\n              to_number( :6  ), -- number(9)\n               :7   , -- varchar2(17 byte)\n               :8   , -- varchar2(2 byte)\n               :9   , -- varchar2(9 byte)\n               :10   , -- varchar2(17 byte)\n               :11   , -- varchar2(2 byte)\n               :12   , -- varchar2(9 byte)\n               :13   , -- varchar2(17 byte)\n               :14   , -- varchar2(2 byte)\n               :15   , -- number(9)\n               :16   , -- varchar2(9 byte)\n               :17   , -- varchar2(9 byte)\n               :18   , -- varchar2(25 byte)\n               :19   , -- varchar2(25 byte)\n               :20   , -- varchar2(25 byte)\n               :21                  , -- varchar2(32 byte)\n              --:(mif_X_name[0]                 ) , -- varchar2(32 byte)\n               :22   , -- varchar2(32 byte)\n               :23   , -- varchar2(32 byte)\n               :24   , -- varchar2(24 byte)\n               :25   , -- varchar2(2 byte)\n               :26   , -- varchar2(9 byte)\n               :27   , -- varchar2(32 byte)\n               :28   , -- varchar2(32 byte)\n               :29   , -- varchar2(32 byte)\n               :30   , -- varchar2(24 byte)\n               :31   , -- varchar2(2 byte)\n               :32   , -- varchar2(9 byte)\n               :33   , -- varchar2(32 byte)\n               :34   , -- varchar2(32 byte)\n               :35   , -- varchar2(32 byte)\n               :36   , -- varchar2(24 byte)\n               :37   , -- varchar2(2 byte)\n               :38   , -- varchar2(9 byte)\n               :39   , -- varchar2(10 byte)\n               :40   , -- varchar2(10 byte)\n               :41   , -- varchar2(10 byte)\n               :42   , -- char(1 byte)                                         -- default: N; sic 5542: Y\n               :43   , -- varchar2(1 byte)\n               :44   , -- char(1 byte)\n              'N'                               , -- char(1 byte)         :mif.smk1_eligibility           -- default: N; sic 5411: Y\n              'N'                               , -- char(1 byte)         :mif.smk2_eligibility           -- default: N; sic 5411: Y\n              'N'                               , -- char(1 byte)         :mif.eps_eligibility            -- default: N; sic 5812, 5814, 7523, 7832: Y\n              'N'                               , -- char(1 byte)         :mif.club_eligibility           -- default: N; sic 5300: Y\n               :45   , -- char(1 byte)\n               :46   , -- varchar2(1 byte)\n               :47   , -- varchar2(1 byte)\n               :48   , -- varchar2(1 byte)     :mif.daily_ach                  -- default: Y; other option: N\n              nvl( :49  , 'N'       ) , -- varchar2(1 byte)\n              nvl( :50  , 'N') , -- varchar2(1 byte)\n               :51   , -- number(3)\n               :52   , -- number(7,2)\n               :53   , -- number(4)\n               :54   , -- number(4)\n              decode( :55   ,\n                    0, null,\n                     :56           ) , -- number(15)\n              decode( :57  ,\n                    0, null,\n                    lpad( :58  ,10,'0') ), -- varchar2(10 byte)\n              decode( :59  ,\n                    0, null,\n                    lpad( :60  ,10,'0') ), -- varchar2(10 byte)\n               :61   , -- varchar2(4 byte)\n               :62   , -- char(1 byte)\n               :63   , -- char(1 byte)\n               :64   , -- number(7) (3 implied decimal places)\n               :65   , -- number(7) (3 implied decimal places)\n               :66   , -- varchar2(4 byte)\n               :67   , -- char(1 byte)\n               :68   , -- char(1 byte)\n               :69   , -- number(7) (3 implied decimal places)\n               :70   , -- number(7) (3 implied decimal places)\n               :71   , -- varchar2(4 byte)\n               :72   , -- char(1 byte)\n               :73   , -- char(1 byte)\n               :74   , -- number(7) (3 implied decimal places)\n               :75   , -- number(7) (3 implied decimal places)\n               :76   , -- varchar2(4 byte)\n               :77   , -- char(1 byte)\n               :78   , -- char(1 byte)\n               :79   , -- number(7) (3 implied decimal places)\n               :80   , -- number(7) (3 implied decimal places)\n               :81   , -- varchar2(4 byte)\n               :82   , -- char(1 byte)\n               :83   , -- char(1 byte)\n               :84   , -- number(7) (3 implied decimal places)\n               :85   , -- number(7) (3 implied decimal places)\n               :86   , -- varchar2(4 byte)\n               :87   , -- number(7) (3 implied decimal places)\n               :88   , -- varchar2(2 byte)\n               :89   , -- varchar2(2 byte)\n               :90   , -- varchar2(2 byte)\n               :91   , -- varchar2(2 byte)\n               :92   , -- varchar2(2 byte)\n               :93   , -- varchar2(2 byte)\n               :94   , -- varchar2(2 byte)\n               :95   , -- varchar2(2 byte)\n               :96   , -- varchar2(4 byte)\n               :97   , -- varchar2(4 byte)\n               :98   , -- varchar2(4 byte)\n               :99   , -- varchar2(4 byte)\n               :100   , -- varchar2(16 byte)\n               :101   , -- varchar2(16 byte)\n               :102   , -- varchar2(16 byte)\n               :103   , -- varchar2(16 byte)\n               :104   , -- varchar2(10 byte)\n               :105   , -- varchar2(1 byte)\n               :106   , -- varchar2(4 byte)\n               :107   , -- varchar2(4 byte)\n               :108   , -- number(2)\n               :109   , -- number(2)\n               :110   , -- varchar2(4 byte)\n               :111   , -- varchar2(32 byte)\n\n              null                              , -- varchar2(10 byte)    :mif.merchant_verification_val\n              null                              , -- varchar2(1 byte)     :mif.visa_partner_program\n              null                              , -- varchar2(3 byte)     :mif.mc_program_reg_id\n              null                              , -- varchar2(6 byte)     :mif.mc_assigned_id\n               :112   , -- varchar2(4 byte)     :mif.merchant_type\n              null                              , -- varchar2(64 byte)    :mif.visa_eligibility_flags\n              null                              , -- varchar2(64 byte)    :mif.mc_eligibility_flags\n              null                              , -- number(4)            :mif.district\n\n              null                              , -- varchar2(6 byte)     :mif.amex_pc_id\n              null                              , -- varchar2(1 byte)     :mif.dvsaccpt               or  :(mif_dXXaccpt[CT_VS   ])\n              null                              , -- varchar2(1 byte)     :mif.dmcaccpt               or  :(mif_dXXaccpt[CT_MC   ])\n              null                              , -- varchar2(1 byte)     :mif.dv$accpt               or  :(mif_dXXaccpt[CT_VCSH ])\n              null                              , -- varchar2(1 byte)     :mif.dm$accpt               or  :(mif_dXXaccpt[CT_MCSH ])\n              null                              , -- varchar2(1 byte)     :mif.ddsaccpt               or  :(mif_dXXaccpt[CT_DISC ])\n              null                              , -- varchar2(1 byte)     :mif.ddcaccpt               or  :(mif_dXXaccpt[CT_DINR ])\n              null                              , -- varchar2(1 byte)     :mif.djcaccpt               or  :(mif_dXXaccpt[CT_JCB  ])\n              null                              , -- varchar2(1 byte)     :mif.damaccpt               or  :(mif_dXXaccpt[CT_AMEX ])\n              null                              , -- varchar2(2 byte)     :mif.visa_cash_plan         or  :(mif_X_plan  [CT_VCSH ])\n              null                              , -- varchar2(2 byte)     :mif.mastcd_cash_plan       or  :(mif_X_plan  [CT_MCSH ])\n              null                              , -- varchar2(2 byte)     :mif.private_label_2_plan   or  :(mif_X_plan  [CT_PVTL2])\n              null                              , -- varchar2(2 byte)     :mif.private_label_3_plan   or  :(mif_X_plan  [CT_PVTL3])\n              ''                                , -- varchar2(1 byte)     :mif.trans_dest_global\n              'M'                               , -- char(1 byte)         :mif.trans_dest_deposit\n              'M'                               , -- char(1 byte)         :mif.trans_dest_adj\n              'M'                               , -- char(1 byte)         :mif.trans_dest_chgback\n              'M'                               , -- char(1 byte)         :mif.trans_dest_reversal\n              'M'                               , -- char(1 byte)         :mif.trans_dest_cgbk_revrs\n              'M'                               , -- char(1 byte)         :mif.trans_dest_dda_adj\n              'M'                               , -- char(1 byte)         :mif.trans_dest_batch\n              'R'                               , -- char(1 byte)         :mif.trans_dest_other_1\n              'X'                               , -- char(1 byte)         :mif.trans_dest_other_2\n              'X'                               , -- char(1 byte)         :mif.trans_dest_other_3\n              0                                 , -- number(10)           :mif.association_node     // set on insert/update trigger to bank_number||dmagent\n              null                              , -- varchar2(6 byte)     :mif.group_1_association  //@ also needs to be set on insert/update using table \"groups\"\n              null                              , -- varchar2(6 byte)     :mif.group_2_association  //@ also needs to be set on insert/update using table \"groups\"\n              null                              , -- varchar2(6 byte)     :mif.group_3_association  //@ also needs to be set on insert/update using table \"groups\"\n              null                              , -- varchar2(6 byte)     :mif.group_4_association  //@ also needs to be set on insert/update using table \"groups\"\n              null                              , -- varchar2(6 byte)     :mif.group_5_association  //@ also needs to be set on insert/update using table \"groups\"\n              null                              , -- varchar2(6 byte)     :mif.group_6_association  //@ also needs to be set on insert/update using table \"groups\"\n              to_char(sysdate,'mmddrr')         , -- char(6 byte)         :mif.date_opened      = today\n              trunc(sysdate,'mon')              , -- date                 :mif.conversion_date  = this month\n              1                                 , -- number(4)            :mif.processor_id     = 1 / MeS back-end\n              'N'                               , -- varchar2(1 byte)     :mif.test_account\n              '0000000000'                      , -- varchar2(10 byte)    :mif.business_license\n              null                              , -- number(7)            :mif.visa_rebate_percent\n              0                                 , -- number(9)            :mif.state_tax_id\n              '0000'                            , -- varchar2(4 byte)     :mif.officer_1        //@ = \"0000\" or = null?? now always \"0000\"\n              '0000'                            , -- varchar2(4 byte)     :mif.officer_2        //@ = \"0000\" or = null?? now always \"0000\"\n              'Y'                               , -- varchar2(1 byte)     :mif.dachdest         //@ = \"Y\"    or = null?  now always Y; in mif, there are 1551 with 'N' & 159834 null\n              null                              , -- varchar2(32 byte)    :mif.merchant_key     // set on insert trigger\n              null                              , -- date                 :mif.activation_date\n              null                              , -- date                 :mif.risk_activation_date\n              null                              , -- date                 :mif.amex_activation_date\n              null                              , -- date                 :mif.disc_activation_date\n              null                              , -- varchar2(2 byte)     :mif.country_code_syf\n              null                              , -- number(4)            :mif.bank_number_syf\n              null                              , -- varchar2(10 byte)    :mif.bank_name_syf\n              null                              , -- varchar2(25 byte)    :mif.other_name\n              null                              , -- varchar2(32 byte)    :mif.name2_line_1_blk6\n              null                              , -- varchar2(32 byte)    :mif.addr2_line_1_blk6\n              null                              , -- varchar2(32 byte)    :mif.addr2_line_2_blk6\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_00\n              null                              , -- varchar2(25 byte)    :mif.email_desc_00\n              null                              , -- varchar2(75 byte)    :mif.email_addr_00\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_01\n              null                              , -- varchar2(25 byte)    :mif.email_desc_01\n              null                              , -- varchar2(75 byte)    :mif.email_addr_01\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_02\n              null                              , -- varchar2(25 byte)    :mif.email_desc_02\n              null                              , -- varchar2(75 byte)    :mif.email_addr_02\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_03\n              null                              , -- varchar2(25 byte)    :mif.email_desc_03\n              null                              , -- varchar2(75 byte)    :mif.email_addr_03\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_04\n              null                              , -- varchar2(25 byte)    :mif.email_desc_04\n              null                              , -- varchar2(75 byte)    :mif.email_addr_04\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_05\n              null                              , -- varchar2(25 byte)    :mif.email_desc_05\n              null                              , -- varchar2(75 byte)    :mif.email_addr_05\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_06\n              null                              , -- varchar2(25 byte)    :mif.email_desc_06\n              null                              , -- varchar2(75 byte)    :mif.email_addr_06\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_07\n              null                              , -- varchar2(25 byte)    :mif.email_desc_07\n              null                              , -- varchar2(75 byte)    :mif.email_addr_07\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_08\n              null                              , -- varchar2(25 byte)    :mif.email_desc_08\n              null                              , -- varchar2(75 byte)    :mif.email_addr_08\n              null                              , -- varchar2(2 byte)     :mif.email_addr_ind_09\n              null                              , -- varchar2(25 byte)    :mif.email_desc_09\n              null                              ");
   __sjT_sb.append(", -- varchar2(75 byte)    :mif.email_addr_09\n              null                              , -- varchar2(16 byte)    :mif.check_service_est_no\n              null                              , -- varchar2(1 byte)     :mif.daily_settlement_plan\n              null                              , -- char(1 byte)         :mif.ndc_ve_file_flag\n              null                              , -- char(1 byte)         :mif.tiif_eligibility\n              null                              , -- char(1 byte)         :mif.psrf_eligibility\n              null                              , -- varchar2(1 byte)     :mif.dmcpsind\n              null                              , -- char(1 byte)         :mif.mtd_daily_maint_flag\n              null                              , -- char(1 byte)         :mif.user_flag_1\n              null                              , -- char(1 byte)         :mif.user_flag_2\n              null                              , -- char(1 byte)         :mif.confirmatn_letter_flag\n              null                              , -- char(3 byte)         :mif.confirmation_deposit\n              null                              , -- char(1 byte)         :mif.confirmation_cardhldr\n              null                              , -- date                 :mif.date_of_1st_deposit\n              null                              , -- date                 :mif.date_last_deposit\n              null                              , -- date                 :mif.last_deposit_date\n              null                              , -- number(12,2)         :mif.last_deposit_amount\n              null                              , -- date                 :mif.date_stat_chgd_to_dcb\n              null                              , -- char(1 byte)         :mif.merchant_status\n              null                              , -- varchar2(1 byte)     :mif.dmacctst\n              null                              , -- date                 :mif.date_billed_for_close\n              null                              , -- date                 :mif.term_contract_start_date\n              null                              , -- date                 :mif.date_last_activity\n              null                              , -- date                 :mif.last_changed\n              null                              , -- varchar2(6 byte)     :mif.visa_bin_mes\n              null                              , -- varchar2(4 byte)     :mif.mc_ica_mes\n              null                              , -- varchar2(2 byte)     :mif.bt1_indicator\n              null                              , -- varchar2(2 byte)     :mif.bt2_indicator\n              null                              , -- varchar2(2 byte)     :mif.bt3_indicator\n               :113   , -- varchar2(1Byte)      :mif.debit_pass_through \n               :114   , -- char(1 byte)         :mif.pacwest_with_exception\n              null                              , -- varchar2(1 byte)     :mif.interchange_credit_value\n               :115    , -- varchar2(1 byte)     :mif.remit_fees\n              trunc(sysdate)               -- date                 :mif.amex_conversion_date\n            )");
   String theSqlTS = __sjT_sb.toString();
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"39com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   __sJT_st.setInt(2,mif.bank_number);
   __sJT_st.setString(3,mif.dmagent);
   __sJT_st.setString(4,mif.sic_code);
   __sJT_st.setString(5,mif.class_code);
   __sJT_st.setString(6,__sJT_7);
   __sJT_st.setString(7,__sJT_8);
   __sJT_st.setString(8,__sJT_9);
   __sJT_st.setString(9,__sJT_10);
   __sJT_st.setString(10,__sJT_11);
   __sJT_st.setString(11,__sJT_12);
   __sJT_st.setString(12,__sJT_13);
   __sJT_st.setString(13,__sJT_14);
   __sJT_st.setString(14,__sJT_15);
   __sJT_st.setLong(15,mif.federal_tax_id);
   __sJT_st.setString(16,mif.ssn);
   __sJT_st.setString(17,mif.license_number);
   __sJT_st.setString(18,mif.owner_name);
   __sJT_st.setString(19,mif.manager_name);
   __sJT_st.setString(20,mif.fdr_corp_name);
   __sJT_st.setString(21,merchNameBusiness);
   __sJT_st.setString(22,__sJT_16);
   __sJT_st.setString(23,__sJT_17);
   __sJT_st.setString(24,__sJT_18);
   __sJT_st.setString(25,__sJT_19);
   __sJT_st.setString(26,__sJT_20);
   __sJT_st.setString(27,__sJT_21);
   __sJT_st.setString(28,__sJT_22);
   __sJT_st.setString(29,__sJT_23);
   __sJT_st.setString(30,__sJT_24);
   __sJT_st.setString(31,__sJT_25);
   __sJT_st.setString(32,__sJT_26);
   __sJT_st.setString(33,__sJT_27);
   __sJT_st.setString(34,__sJT_28);
   __sJT_st.setString(35,__sJT_29);
   __sJT_st.setString(36,__sJT_30);
   __sJT_st.setString(37,__sJT_31);
   __sJT_st.setString(38,__sJT_32);
   __sJT_st.setLong(39,mif.phone_1);
   __sJT_st.setLong(40,mif.phone_2_fax);
   __sJT_st.setLong(41,mif.customer_service_phone);
   __sJT_st.setString(42,mif.cat_interc_value_flag);
   __sJT_st.setString(43,mif.print_statements);
   __sJT_st.setString(44,mif.edc_flagst_other_3);
   __sJT_st.setString(45,mif.merit_eligibility);
   __sJT_st.setString(46,mif.dmer3ind);
   __sJT_st.setString(47,mif.daily_discount_interchange);
   __sJT_st.setString(48,mif.daily_ach);
   __sJT_st.setString(49,mif.visa_fanf);
   __sJT_st.setString(50,mif.mc_merch_loc_fee);
   __sJT_st.setInt(51,mif.suspended_days);
   __sJT_st.setDouble(52,mif.minimum_monthly_discount);
   __sJT_st.setInt(53,mif.ic_bet_visa);
   __sJT_st.setInt(54,mif.ic_bet_mc);
   __sJT_st.setLong(55,__sJT_33);
   __sJT_st.setLong(56,__sJT_34);
   __sJT_st.setLong(57,__sJT_35);
   __sJT_st.setLong(58,__sJT_36);
   __sJT_st.setLong(59,__sJT_37);
   __sJT_st.setLong(60,__sJT_38);
   __sJT_st.setString(61,__sJT_39);
   __sJT_st.setString(62,__sJT_40);
   __sJT_st.setString(63,__sJT_41);
   __sJT_st.setInt(64,__sJT_42);
   __sJT_st.setInt(65,__sJT_43);
   __sJT_st.setString(66,__sJT_44);
   __sJT_st.setString(67,__sJT_45);
   __sJT_st.setString(68,__sJT_46);
   __sJT_st.setInt(69,__sJT_47);
   __sJT_st.setInt(70,__sJT_48);
   __sJT_st.setString(71,__sJT_49);
   __sJT_st.setString(72,__sJT_50);
   __sJT_st.setString(73,__sJT_51);
   __sJT_st.setInt(74,__sJT_52);
   __sJT_st.setInt(75,__sJT_53);
   __sJT_st.setString(76,__sJT_54);
   __sJT_st.setString(77,__sJT_55);
   __sJT_st.setString(78,__sJT_56);
   __sJT_st.setInt(79,__sJT_57);
   __sJT_st.setInt(80,__sJT_58);
   __sJT_st.setString(81,__sJT_59);
   __sJT_st.setString(82,__sJT_60);
   __sJT_st.setString(83,__sJT_61);
   __sJT_st.setInt(84,__sJT_62);
   __sJT_st.setInt(85,__sJT_63);
   __sJT_st.setString(86,__sJT_64);
   __sJT_st.setInt(87,__sJT_65);
   __sJT_st.setString(88,__sJT_66);
   __sJT_st.setString(89,__sJT_67);
   __sJT_st.setString(90,__sJT_68);
   __sJT_st.setString(91,__sJT_69);
   __sJT_st.setString(92,__sJT_70);
   __sJT_st.setString(93,__sJT_71);
   __sJT_st.setString(94,__sJT_72);
   __sJT_st.setString(95,__sJT_73);
   __sJT_st.setString(96,mif.rep_code);
   __sJT_st.setString(97,mif.user_data_1);
   __sJT_st.setString(98,mif.user_data_2);
   __sJT_st.setString(99,mif.user_data_3);
   __sJT_st.setString(100,mif.user_data_4);
   __sJT_st.setString(101,mif.user_data_5);
   __sJT_st.setString(102,mif.user_acount_1);
   __sJT_st.setString(103,mif.user_account_2);
   __sJT_st.setString(104,mif.dbfemid);
   __sJT_st.setString(105,mif.dbfepflg);
   __sJT_st.setString(106,mif.inv_code_investigate);
   __sJT_st.setString(107,mif.met_table);
   __sJT_st.setInt(108,mif.num_post_date_days_debit);
   __sJT_st.setInt(109,mif.num_post_date_days_credt);
   __sJT_st.setString(110,mif.merchant_type);
   __sJT_st.setString(111,mif_load_filename);
   __sJT_st.setString(112,mif.merchant_type);
   __sJT_st.setString(113,mif.debit_pass_through);
   __sJT_st.setString(114,mif.pacwest_with_exception);
   __sJT_st.setString(115,mif.remit_fees);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3200^11*/

          //Add a row in the IRS table to indicate match status for 1099K etc
          /*@lineinfo:generated-code*//*@lineinfo:3203^11*/

//  ************************************************************
//  #sql [Ctx] { insert into irs_match_status
//              (
//                merchant_number,
//                legal_name,
//                legal_name_source,
//                tax_id,
//                tax_id_type,
//                tax_id_source,
//                match_result,
//                match_date_received,
//                match_date_source,
//                print_1099,
//                print_address,
//                print_source,
//                exempt_status,
//                exempt_source
//              )
//                values
//              (
//                :mif.merchant_number,
//                :mif.fdr_corp_name,
//                'OLA',
//                :mif.federal_tax_id,
//                :mif.irs_tax_type,
//                'OLA',
//                :mif.irs_match_result,
//                sysdate,
//                'OLA',
//                'N',
//                1,
//                'SYS',
//                ltrim(rtrim(:mif.irs_exemptions)),
//                'SYS'
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into irs_match_status\n            (\n              merchant_number,\n              legal_name,\n              legal_name_source,\n              tax_id,\n              tax_id_type,\n              tax_id_source,\n              match_result,\n              match_date_received,\n              match_date_source,\n              print_1099,\n              print_address,\n              print_source,\n              exempt_status,\n              exempt_source\n            )\n              values\n            (\n               :1  ,\n               :2  ,\n              'OLA',\n               :3  ,\n               :4  ,\n              'OLA',\n               :5  ,\n              sysdate,\n              'OLA',\n              'N',\n              1,\n              'SYS',\n              ltrim(rtrim( :6  )),\n              'SYS'\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   __sJT_st.setString(2,mif.fdr_corp_name);
   __sJT_st.setLong(3,mif.federal_tax_id);
   __sJT_st.setInt(4,mif.irs_tax_type);
   __sJT_st.setInt(5,mif.irs_match_result);
   __sJT_st.setString(6,mif.irs_exemptions);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3239^11*/

          // store the billing records in "mbs_pricing" and "mbs_charge_records"
          if( billInfoRows.size() > 0)
          {
            for( int idx = 0; idx < billInfoRows.size(); ++idx )
            {
              BillingInfo   row = (BillingInfo)billInfoRows.elementAt(idx);

              if( row.statementMsg == null )  // put rec in mbs_pricing and use default statementMsg
              { 
                if ((aptAppType == mesConstants.APP_TYPE_CMDI) 
                    ||  ((aptAppType == mesConstants.APP_TYPE_DEMOSPHERE) &&
                        (row.itemTypeInt == MBS_ELEMENTS_AVS ||
                         row.itemTypeInt == MBS_ELEMENTS_APF ||
                         row.itemTypeInt == MBS_ELEMENTS_PIN_DEBIT_NTWRK_PARTICIPATION)) 
                        ) {//rates should be zero for CMDI and some items for Demosphere applications
                  row.multiplier    = 0;
                  row.chargePerItem = 0;
                }
                
                // Set DISCOUNT chargePerItem for Demosphere application
                if (aptAppType == mesConstants.APP_TYPE_DEMOSPHERE && row.itemTypeInt == MBS_ELEMENTS_DISCOUNT) {
                  row.multiplier    = 0;
                  row.chargePerItem = 0.15;
                }
                
                log.debug("ADD ITEM PRICING: " + row.itemTypeInt + ", " + row.itemTypeString + ", " + row.multiplier + ", " + row.chargePerItem);
                
                /*@lineinfo:generated-code*//*@lineinfo:3268^17*/

//  ************************************************************
//  #sql [Ctx] { call mbs_add_pricing
//                    (
//                      :mif.merchant_number,
//                      :row.itemTypeInt,
//                      :row.itemTypeString,
//                      :row.multiplier,
//                      :row.chargePerItem,
//                      488441,               -- acccount uploader user id
//                      to_char(trunc(:row.dateStart,'month'),'mmrr'),
//                      to_char(trunc(:row.dateEnd,'month'),'mmyyyy'),
//                      :row.volumeType
//                    )
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN mbs_add_pricing\n                  (\n                     :1  ,\n                     :2  ,\n                     :3  ,\n                     :4  ,\n                     :5  ,\n                    488441,               -- acccount uploader user id\n                    to_char(trunc( :6  ,'month'),'mmrr'),\n                    to_char(trunc( :7  ,'month'),'mmyyyy'),\n                     :8  \n                  )\n                \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   __sJT_st.setInt(2,row.itemTypeInt);
   __sJT_st.setString(3,row.itemTypeString);
   __sJT_st.setDouble(4,row.multiplier);
   __sJT_st.setDouble(5,row.chargePerItem);
   __sJT_st.setDate(6,row.dateStart);
   __sJT_st.setDate(7,row.dateEnd);
   __sJT_st.setInt(8,row.volumeType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3282^17*/
              }
              else                            // put rec in mbs_charge_records and use the provided statementMsg
              //{ 
                if (aptAppType != mesConstants.APP_TYPE_CMDI && aptAppType != mesConstants.APP_TYPE_DEMOSPHERE) {// there should not be any charge records for CMDI and Demosphere
                log.debug("ADD mbs_add_charge_record: " + row.itemTypeInt + ", " + row.itemTypeString + ", " + row.multiplier + ", " + row.chargePerItem+ ", " + row.dateStart.toString() +  ", " + row.dateEnd.toString());

                /*@lineinfo:generated-code*//*@lineinfo:3289^17*/

//  ************************************************************
//  #sql [Ctx] { call mbs_add_charge_record
//                    (
//                      :mif.merchant_number,
//                      :row.itemTypeString,
//                      :row.billingMonths,
//                      :row.statementMsg,
//                      :row.chargePerItem,
//                      :row.multiplier,
//                      'accountuploader',
//                      :row.dateEnd,
//                      :row.dateStart
//                    )
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN mbs_add_charge_record\n                  (\n                     :1  ,\n                     :2  ,\n                     :3  ,\n                     :4  ,\n                     :5  ,\n                     :6  ,\n                    'accountuploader',\n                     :7  ,\n                     :8  \n                  )\n                \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   __sJT_st.setString(2,row.itemTypeString);
   __sJT_st.setString(3,row.billingMonths);
   __sJT_st.setString(4,row.statementMsg);
   __sJT_st.setDouble(5,row.chargePerItem);
   __sJT_st.setDouble(6,row.multiplier);
   __sJT_st.setDate(7,row.dateEnd);
   __sJT_st.setDate(8,row.dateStart);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3303^17*/
              }
              //}
              
            }
          }

          commit();

          log.debug("*** Convert Discover Map Process ***");
          // convert discover map if necessary
          String discNum = Long.toString(mif_X_mid[CT_DISC]);
          if( discNum != null && discNum.length() > 6 && ("601172").equals(discNum.substring(0, 6)) )
          {
            /*@lineinfo:generated-code*//*@lineinfo:3317^13*/

//  ************************************************************
//  #sql [Ctx] { call convert_discover_map(:mif.merchant_number, :mif_X_mid[CT_DISC] )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_74 = mif_X_mid[CT_DISC];
  try {
   String theSqlTS = "BEGIN convert_discover_map( :1  ,  :2   )\n            \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   __sJT_st.setLong(2,__sJT_74);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3320^13*/

            commit();
          }
          else if( discNum == null )
          {
            // this account does not have a discover number for some reason. Convert them anyway.
            /*@lineinfo:generated-code*//*@lineinfo:3327^13*/

//  ************************************************************
//  #sql [Ctx] { call convert_discover_map(:mif.merchant_number)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_discover_map( :1  )\n            \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3330^13*/
            
            commit();
          }
          
         log.debug("*** Starting Amex changes for Riverview OLA ***");        
         
          //riverview bank or MESP OLA amex mes funding related changes
          //get appType
          int appType = -1;
          /*@lineinfo:generated-code*//*@lineinfo:3340^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//              
//              from    application app
//              where   app.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n             \n            from    application app\n            where   app.app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3346^11*/

          int amexOptBlue = -1;
          /*@lineinfo:generated-code*//*@lineinfo:3349^11*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//              
//              from   mif
//              where  merchant_number = :mif.merchant_number
//                     and damexse in (select distinct amex_optblue_se_number from amex_se_mapping)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n             \n            from   mif\n            where  merchant_number =  :1  \n                   and damexse in (select distinct amex_optblue_se_number from amex_se_mapping)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,mif.merchant_number);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amexOptBlue = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3356^11*/

          if( amexOptBlue == 1 || (mesConstants.APP_TYPE_RIVERVIEW == appType) || ( mesConstants.APP_TYPE_MESP == appType) ){
            log.debug("*** Calling convertAmexMerchant() method with appSeqNum = " + appSeqNum + " sicCode = " + mif.sic_code + " merchantNum = " + mif.merchant_number + " ***");
            convertAmexMerchant(appSeqNum, mif.sic_code, mif.merchant_number);
          }
          log.debug("*********** Amex MeS funding changes done ****************");

          retVal = true;
        }
        else  // if( recCount != 0 )
        {
          loadAppsLogEntry("insertMerchantInfo(" + appSeqNum + ")", "Merchant record(s) already existed.");
        }
      }
      else // if( testMode == false )
      {
        // show what would have happened regarding discover conversion
        log.debug("convert_discover_map(" + mif.merchant_number + ", " + mif_X_mid[CT_DISC] + ")");
        retVal = true;
      }
    }
    catch(Exception e)
    {
      loadAppsLogEntry("insertMerchantInfo(" + appSeqNum + ")", e.toString());
      resetApp(appSeqNum);
    }
    finally
    {
    }
    return( retVal );
  }
  private void convertAmexMerchant(int appSeqNum, String sicCode, long merchantNumber){
    ResultSetIterator    it        = null;
    ResultSet            rs        = null;

    log.debug("*** Doing MeS amex processing changes for RCB OLA ***");
    log.debug("Received following values from parent call . MID = " + merchantNumber + ", appSeqNum = " + appSeqNum + ",  sicCode = " + sicCode);

    //check if mes is processing amex card for this application/merchant
    int     amexRecordCount = 0;
    double  amexDiscRate = 0.0;
    double  amexPerItem = 0.0;
    long    mesSENum = 1046000048; //default SE number for MeS
    int     mifRecCount  = 0;
    int     mccCode = Integer.parseInt(sicCode);

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3405^7*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//          
//          from    mif
//          where   merchant_number = :merchantNumber
//                  and nvl(mes_amex_processing,'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n         \n        from    mif\n        where   merchant_number =  :1  \n                and nvl(mes_amex_processing,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifRecCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3412^7*/

      log.debug("MIF record count for mes_amex_processing= Y : " + mifRecCount);

      if(mifRecCount == 0)
      {
        //do updates
        /*@lineinfo:generated-code*//*@lineinfo:3419^9*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//            
//            from   merchpayoption mpo
//            where  app_seq_num = :appSeqNum
//                   and cardtype_code = 16
//                   and lower (mes_processing) = 'y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n           \n          from   merchpayoption mpo\n          where  app_seq_num =  :1  \n                 and cardtype_code = 16\n                 and lower (mes_processing) = 'y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amexRecordCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3427^9*/
        log.debug("Amex Record count = " + amexRecordCount);

        if(amexRecordCount > 0){
          //mes is funding amex. get amex pricing details.
          log.debug("Retrieving Amex Disc Rate and Per Item for appSeqNum " + appSeqNum);
          try {
            /*@lineinfo:generated-code*//*@lineinfo:3434^13*/

//  ************************************************************
//  #sql [Ctx] { select tc.tranchrg_disc_rate , tc.tranchrg_pass_thru
//                
//                from   tranchrg tc
//                where  app_seq_num = :appSeqNum
//                       and cardtype_code = 16
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select tc.tranchrg_disc_rate , tc.tranchrg_pass_thru\n               \n              from   tranchrg tc\n              where  app_seq_num =  :1  \n                     and cardtype_code = 16";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amexDiscRate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amexPerItem = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3441^13*/
          } catch(Exception ea) {
            amexDiscRate = 0.00;
            amexPerItem = 0.00;
          }

          log.debug("amexDiscRate = " + amexDiscRate + " amexPerItem = " + amexPerItem);

          try {
            log.debug("Direct Connection String = " + SQLJConnectionBase.getDirectConnectString());
            log.debug("Pool Connection String = " + SQLJConnectionBase.getPoolConnectionString());

            //get SE# for sic code
            String amexOptBlue = "";
            /*@lineinfo:generated-code*//*@lineinfo:3455^13*/

//  ************************************************************
//  #sql [Ctx] it = { select amex_opt_blue
//                from   merchpayoption
//                where  cardtype_code = 16 and app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select amex_opt_blue\n              from   merchpayoption\n              where  cardtype_code = 16 and app_seq_num =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"50com.mes.startup.LoadApprovedApps",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3460^13*/

            rs = it.getResultSet();
            if( rs.next() )
            {
              amexOptBlue = rs.getString("amex_opt_blue");
            }

            rs.close();
            it.close();

            if( amexOptBlue != null && !amexOptBlue.equals("") && !amexOptBlue.equals("null") )
            {
              amexOptBlue = amexOptBlue.toUpperCase();
            }
            else
            {
              amexOptBlue = "";
            }

            if( amexOptBlue.equals("Y") )
            {
              /*@lineinfo:generated-code*//*@lineinfo:3482^15*/

//  ************************************************************
//  #sql [Ctx] { select amex_optblue_se_number
//                  
//                  from   amex_se_mapping asm,
//                         merchant m
//                  where  m.app_seq_num = :appSeqNum and
//                         asm.sic_code = m.sic_code and
//                         ( (asm.cap_number = '1101618742' and m.merch_bank_number = 3943) or
//                         (asm.cap_number = '1101618049' and m.merch_bank_number != 3943))
//                         and sysdate between valid_date_begin and valid_date_end
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select amex_optblue_se_number\n                 \n                from   amex_se_mapping asm,\n                       merchant m\n                where  m.app_seq_num =  :1   and\n                       asm.sic_code = m.sic_code and\n                       ( (asm.cap_number = '1101618742' and m.merch_bank_number = 3943) or\n                       (asm.cap_number = '1101618049' and m.merch_bank_number != 3943))\n                       and sysdate between valid_date_begin and valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesSENum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3493^15*/
              log.debug("amexOptBlue = Y --- mesSENum: " +mesSENum );
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:3498^15*/

//  ************************************************************
//  #sql [Ctx] { select amex_se_number
//                  
//                  from   sic_code_se_number
//                  where  sic_code = :mccCode
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select amex_se_number\n                 \n                from   sic_code_se_number\n                where  sic_code =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.startup.LoadApprovedApps",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mccCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesSENum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3504^15*/
              log.debug("amexOptBlue = N --- mesSENum: " +mesSENum );
            }
          } catch(Exception e) {
            log.debug("Exception in getting SE number from table - " + e.getMessage());
            e.printStackTrace();
          }

          log.debug("SE Num = " + mesSENum);

          /* mark mif to show MES as AMEX processor */
          /*@lineinfo:generated-code*//*@lineinfo:3515^11*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//              set     mes_amex_processing = 'Y',
//                      amex_conversion_date = trunc(sysdate),
//                      mes_amex_suspended_days=0
//              where   merchant_number = :merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n            set     mes_amex_processing = 'Y',\n                    amex_conversion_date = trunc(sysdate),\n                    mes_amex_suspended_days=0\n            where   merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"53com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3522^11*/
          log.debug("Updated MIF table for mes_amex_processing. MID = " + merchantNumber);

          /* update trident profile to have new SE number */
          /*@lineinfo:generated-code*//*@lineinfo:3526^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_profile
//              set     amex_caid = :mesSENum,
//                      amex_accept = 'Y'
//              where   merchant_number = :merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_profile\n            set     amex_caid =  :1  ,\n                    amex_accept = 'Y'\n            where   merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"54com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mesSENum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3532^11*/
          log.debug("Updated trident_profile table for amex_caid column. MID = " + merchantNumber);

          /* update mif to show amex as a card type we fund for */
          /*@lineinfo:generated-code*//*@lineinfo:3536^11*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//              set     amex_plan = 'DN',
//                      damexse = :mesSENum
//              where   merchant_number = :merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n            set     amex_plan = 'DN',\n                    damexse =  :1  \n            where   merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"55com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mesSENum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3542^11*/
          log.debug("Updated MIF table for amex_plan and damexse column. MID = " + merchantNumber);

          /* update merchpayoption to have new SE number */
          /*@lineinfo:generated-code*//*@lineinfo:3546^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption
//              set     merchpo_card_merch_number = :mesSENum
//              where   app_seq_num in (select app_seq_num from merchant where merch_number = :merchantNumber)
//                      and cardtype_code = 16
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption\n            set     merchpo_card_merch_number =  :1  \n            where   app_seq_num in (select app_seq_num from merchant where merch_number =  :2  )\n                    and cardtype_code = 16";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"56com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mesSENum);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3552^11*/
          log.debug("Updated merchpayoption table for merchpo_card_merch_number column. SE # =" + mesSENum);

          commit();
          log.debug("Checking if  amexDiscRate nad amexPerItem are > 0 and calling mbs_add_pricing stored procedure");

          if(amexDiscRate > 0 || amexPerItem > 0)
          {
            log.debug("Executing mbs_add_pricing stored procedure");
            System.out.println("Executing mbs_add_pricing stored procedure");
            /*@lineinfo:generated-code*//*@lineinfo:3562^13*/

//  ************************************************************
//  #sql [Ctx] { call mbs_add_pricing(:merchantNumber, 101, 'AM', :amexDiscRate, :amexPerItem)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN mbs_add_pricing( :1  , 101, 'AM',  :2  ,  :3  )\n            \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.startup.LoadApprovedApps",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDouble(2,amexDiscRate);
   __sJT_st.setDouble(3,amexPerItem);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3565^13*/
            commit();
          }   
          log.debug("Converting to Amex merchant done");
        }
      }
    } catch(Exception e) {
      logEntry("convertAmexMerchant() call for appSeqnum = " + appSeqNum, e.toString());      
      e.printStackTrace();
    }
  }
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();

      LoadApprovedApps  loadApprovedApps  = new LoadApprovedApps();
      //loadApprovedApps.convertAmexMerchant(372277, "7333", 941000113540L);

      if(args.length == 0)
      {
        loadApprovedApps.execute();
      }
      else
      {
        if(args[0].length() == 4)
        {
          loadApprovedApps.setEventArgs(args[0]);
          loadApprovedApps.execute();
        }
        else
        {
          // param is an app_seq_num
          long testAppSeqNum = Long.parseLong(args[0]);
          loadApprovedApps.loadApps( testAppSeqNum );
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("LoadApprovedApps::main(): " + e.toString());
    }
  }
    private void addBillingInfo(BillingInfo biToAdd) {
        billInfoRows.add(biToAdd);
    }
}/*@lineinfo:generated-code*/