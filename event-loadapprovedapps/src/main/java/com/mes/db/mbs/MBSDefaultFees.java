/*@lineinfo:filename=MBSDefaultFees*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.db.mbs;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MBSDefaultFees extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(MBSDefaultFees.class);
  
  public MBSDefaultFees()
  {
  }
  
  public class FeeItem
  {
    public int      ItemType        = -1;
    public String   ItemSubclass    = null;
    public boolean  CreateFee       = false;
    public double   Rate            = 0.0;
    public double   PerItem         = 0.0;
    
    public FeeItem(ResultSet rs)
    {
      try
      {
        ItemType      = rs.getInt("item_type");
        ItemSubclass  = rs.getString("item_subclass");
        CreateFee     = ("Y").equals(rs.getString("create_fee")) ? true : false;
        Rate          = rs.getDouble("rate");
        PerItem       = rs.getDouble("per_item");
      }
      catch(Exception e)
      {
        logEntry("FeeItem(ResultSet)", e.toString());
      }
    }
    
    public String getKey()
    {
      StringBuffer  key = new StringBuffer("");
      
      key.append(Integer.toString(ItemType));
      if( null == ItemSubclass )
      {
        key.append("NULL");
      }
      else
      {
        key.append(ItemSubclass);
      }
      
      return( key.toString() );
    }
  }
  
  public class ChargeItem
  {
    public String   ChargeType        = "";
    public String   StatementMessage  = "";
    public String   BillingFrequency  = "";
    public double   ChargeAmount      = 0.0;
    public int      DelayMonths       = 0;
    public boolean  CreateCharge      = false;
    
    public ChargeItem(ResultSet rs)
    {
      try
      {
        ChargeType        = rs.getString("charge_type");
        StatementMessage  = rs.getString("statement_message");
        BillingFrequency  = rs.getString("billing_frequency");
        ChargeAmount      = rs.getDouble("charge_amount");
        DelayMonths       = rs.getInt("delay_months");
        CreateCharge      = ("Y").equals(rs.getString("create_charge")) ? true : false;
      }
      catch(Exception e)
      {
        logEntry("ChargeItem(ResultSet)", e.toString());
      }
    }
    
    public String getKey()
    {
      StringBuffer key = new StringBuffer("");
      
      key.append(ChargeType);
      key.append(StatementMessage);
      
      return( key.toString() );
    }
  }
  
  public Vector loadDefaultCharges(long associationNode)
  {
    HashMap           charges = new HashMap();
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    Vector            results = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mcd.charge_type,
//                  mcd.statement_message,
//                  mcd.billing_frequency,
//                  mcd.charge_amount,
//                  mcd.delay_months,
//                  mcd.create_charge
//          from    t_hierarchy th,
//                  mbs_charge_defaults mcd
//          where   th.descendent = :associationNode
//                  and th.ancestor = mcd.hierarchy_node
//          order by th.relation desc, mcd.charge_type asc, mcd.statement_message asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mcd.charge_type,\n                mcd.statement_message,\n                mcd.billing_frequency,\n                mcd.charge_amount,\n                mcd.delay_months,\n                mcd.create_charge\n        from    t_hierarchy th,\n                mbs_charge_defaults mcd\n        where   th.descendent =  :1 \n                and th.ancestor = mcd.hierarchy_node\n        order by th.relation desc, mcd.charge_type asc, mcd.statement_message asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.db.mbs.MBSDefaultFees",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,associationNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.db.mbs.MBSDefaultFees",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^7*/
      
      rs = it.getResultSet();
      
      while( rs.next() )
      {
        ChargeItem ci = new ChargeItem(rs);
        
        // put this item in the HashMap.  If an item already exists for this key
        // it will be replaced with this value which is exactly what is desired
        charges.put(ci.getKey(), ci);
      }
      
      rs.close();
      it.close();
      
      results = new Vector( charges.values() );
    }
    catch(Exception e)
    {
      logEntry("loadDefaultCharges(" + associationNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( results );
  }
  
  public Vector loadDefaultFees(long associationNode)
  {
    HashMap           fees    = new HashMap();
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    Vector            results = null;
    
    try
    {
      connect();
      
      // load fees as they apply to this merchant in a top-down view
      // based on merchant's hierarchy
      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfd.item_type,
//                  mfd.item_subclass,
//                  mfd.create_fee,
//                  mfd.rate,
//                  mfd.per_item
//          from    t_hierarchy th,
//                  mbs_fee_defaults mfd
//          where   th.descendent = :associationNode
//                  and th.ancestor = mfd.hierarchy_node
//          order by th.relation desc, mfd.item_type asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfd.item_type,\n                mfd.item_subclass,\n                mfd.create_fee,\n                mfd.rate,\n                mfd.per_item\n        from    t_hierarchy th,\n                mbs_fee_defaults mfd\n        where   th.descendent =  :1 \n                and th.ancestor = mfd.hierarchy_node\n        order by th.relation desc, mfd.item_type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.db.mbs.MBSDefaultFees",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,associationNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.db.mbs.MBSDefaultFees",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        FeeItem fi = new FeeItem(rs);
        
        // put this item in the HashMap.  If an item already exists for this key
        // it will be replaced with this value which is exactly what is desired
        fees.put(fi.getKey(), fi);
      }
      
      rs.close();
      it.close();
      
      results = new Vector( fees.values() );
    }
    catch(Exception e)
    {
      logEntry("loadDefaultFees(" + associationNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( results );
  }
  
  public static Vector _loadDefaultFees(long associationNode)
  {
    MBSDefaultFees df = new MBSDefaultFees();
    
    return( df.loadDefaultFees(associationNode) );
  }
  
  public static Vector _loadDefaultCharges(long associationNode)
  {
    MBSDefaultFees df = new MBSDefaultFees();
    return( df.loadDefaultCharges(associationNode) );
  }
}/*@lineinfo:generated-code*/