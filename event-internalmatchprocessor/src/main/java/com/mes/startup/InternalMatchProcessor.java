/*@lineinfo:filename=InternalMatchProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class InternalMatchProcessor extends EventBase
{
  static Logger log = Logger.getLogger(InternalMatchProcessor.class);

  protected boolean           TestMode                = false;
  protected String            WorkFilename            = null;
  protected IMData            IMRecord                = null;
  
  private class IMData
  {
    public long   AppSeqNum            = 0L;
    public String MatchSeqNum          = null;
    public String MerchantNumber       = null;
    public String MerchantName         = null;
    public String DbaName              = null;
    public String MerchantUrl          = null;
    public String StateTaxId           = null;
    public String FedTaxId             = null;
    public String BusAddress1          = null;
    public String BusAddress2          = null;
    public String BusAddressCity       = null;
    public String BusAddressState      = null;
    public String BusAddressZip        = null;
    public String BusPhone             = null;
    public String BusCountry           = null;
    
    public Vector BusOwners            = new Vector();
    public Vector IMResponses          = new Vector();
    
    public IMData( ResultSet rs )
      throws java.sql.SQLException
    {
      AppSeqNum         = rs.getLong("app_seq_num");
      MatchSeqNum       = processString( rs.getString("match_seq_num") );
      MerchantNumber    = processString( rs.getString("merchant_number") );
      MerchantName      = processString( rs.getString("legal_name") );
      DbaName           = processString( rs.getString("dba_name") );
      MerchantUrl       = processString( rs.getString("web_url") );
      StateTaxId        = processString( rs.getString("state_tax_id") );
      FedTaxId          = processString( rs.getString("fed_tax_id") );
      BusAddress1       = processString( rs.getString("business_addr_line1") );
      BusAddress2       = processString( rs.getString("business_addr_line2") );
      BusAddressCity    = processString( rs.getString("business_city") );
      BusAddressState   = processString( rs.getString("business_state") );
      BusAddressZip     = processString( rs.getString("business_zip") );
      BusPhone          = processString( rs.getString("business_phone") );
      BusCountry        = processString( rs.getString("business_country") );
    }
    
    public long getAppSeqNum() { return AppSeqNum; }
    public String getMatchSeqNum() { return MatchSeqNum; }
    public String getMerchantNumber() { return MerchantNumber; }
    public String getMerchantName() { return MerchantName; }
    public String getDbaName() { return DbaName; }
    public String getMerchantUrl() { return MerchantUrl; }
    public String getStateTaxId() { return StateTaxId; }
    public String getFedTaxId() { return FedTaxId; }
    public String getBusAddress1() { return BusAddress1; }
    public String getBusAddress2() { return BusAddress2; }
    public String getBusAddressCity() { return BusAddressCity; }
    public String getBusAddressState() { return BusAddressState; }
    public String getBusAddressZip() { return BusAddressZip; }
    public String getBusPhone() { return BusPhone; }
    public String getBusCountry() { return BusCountry; }
    public Vector getBusOwners() { return BusOwners; }
    public Vector getIMResponses() { return IMResponses; }
  }
  
  private class BusOwner
  {
    public String FirstNmae       = null;
    public String LastName        = null;
    public String MiddleNmae      = null;
    public String SSN             = null;
    public String DL              = null;
    public String Address1        = null;
    public String Address2        = null;
    public String AddressCity     = null;
    public String AddressState    = null;
    public String AddressZip      = null;
    public String Country         = null;
    public String Phone           = null;
    public int    OwnerNumber     = 0;
 
    public BusOwner( ResultSet rs )
      throws java.sql.SQLException
    {
      FirstNmae    = processString( rs.getString("owner_first_name") );
      LastName     = processString( rs.getString("owner_last_name") );
      MiddleNmae   = processString( rs.getString("owner_initial") );
      SSN          = processString( rs.getString("owner_ssn") );
      DL           = processString( rs.getString("owner_dl") );
      Address1     = processString( rs.getString("owner_addr_line1") );
      Address2     = processString( rs.getString("owner_addr_line2") );
      AddressCity  = processString( rs.getString("owner_city") );
      AddressState = processString( rs.getString("owner_state") );
      AddressZip   = processString( rs.getString("owner_zip") );
      Country      = processString( rs.getString("owner_country") );
      Phone        = processString( rs.getString("owner_phone") );
      OwnerNumber  = rs.getInt("id_principal");
    }
    
    public String getFirstNmae() { return FirstNmae; }
    public String getLastName() { return LastName; }
    public String getMiddleNmae() { return MiddleNmae; }
    public String getSSN() { return SSN; }
    public String getDL() { return DL; }
    public String getAddress1() { return Address1; }
    public String getAddress2() { return Address2; }
    public String getAddressCity() { return AddressCity; }
    public String getAddressState() { return AddressState; }
    public String getAddressZip() { return AddressZip; }
    public String getCountry() { return Country; }
    public String getPhone() { return Phone; }
    public int    getOwnerNumber() { return OwnerNumber; }
  }
  
  public InternalMatchProcessor()
  {
  }
  
  private String[] buildRespString( ResultSet rs, IMData imData )
  {
    int      matchCount = 0;
    String   fData      = null;
    String   general    = null;
    String   principle  = null;
    String[] responses  = null;

    try
    {
      // build the general record
      FlatFileRecord ffd = new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_MH );
      ffd.setAllFieldData( rs );
      ffd.setFieldData("tran_code", "653");
      ffd.setFieldData("action_ind", "M");
      ffd.setFieldData("record_type", "1");
      ffd.setFieldData("processed_by", "IMP");
      
      fData = imData.getMerchantName().toUpperCase().equals(processString(rs.getString("legal_name")).toUpperCase()) ? "E" : "";
      ffd.setFieldData("legal_name_match", fData);
      fData = imData.getDbaName().toUpperCase().equals(processString(rs.getString("dba_name")).toUpperCase()) ? "E" : "";
      ffd.setFieldData("dba_name_match", fData);
      fData = imData.getBusAddress1().equals(processString(rs.getString("business_addr_line1"))) ? "E" : "";
      ffd.setFieldData("business_addr_match", fData);
      fData = imData.getFedTaxId().equals(processString(rs.getString("state_tax_id"))) ? "E" : "";
      ffd.setFieldData("state_tax_id_match", fData);
      fData = imData.getFedTaxId().equals(processString(rs.getString("fed_tax_id"))) ? "E" : "";
      ffd.setFieldData("fed_tax_id_match", fData);
      ffd.setFieldData("future_expansion", rs.getString("app_seq_num") );
      
      general = ffd.spew();
      
      // build the principal record
      ffd.setDefType( MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_MH );
      ffd.setAllFieldData( rs );
      ffd.setFieldData("tran_code", "653");
      ffd.setFieldData("action_ind", "M");
      ffd.setFieldData("record_type", "2");
      ffd.setFieldData("processed_by", "IMP");
      
      String lastname1  = processString(rs.getString("last_name_1")).toUpperCase(); 
      String lastname2  = processString(rs.getString("last_name_2")).toUpperCase(); 
      String ssn1       = processString(rs.getString("fed_tax_id_1")); 
      String ssn2       = processString(rs.getString("fed_tax_id_2"));
      String dl1        = processString(rs.getString("dl_1")); 
      String dl2        = processString(rs.getString("dl_2"));
      String addr1      = processString(rs.getString("addr_line1_1")).toUpperCase(); 
      String addr2      = processString(rs.getString("addr_line1_2")).toUpperCase();
      String phone1     = processString(rs.getString("phone_1")); 
      String phone2     = processString(rs.getString("phone_2"));
      
      for( int k = 0; k < imData.getBusOwners().size(); ++k )
      {
        BusOwner bo = (BusOwner)imData.getBusOwners().elementAt(k);
        String boId  = String.valueOf(bo.getOwnerNumber());
        if( lastname1.equals(bo.getLastName().toUpperCase()) ||
            lastname2.equals(bo.getLastName().toUpperCase()) )
        {
          ++matchCount;
          ffd.setFieldData("last_name_match_" + boId, "E");
        }
        
        if( bo.getPhone().equals(phone1) || bo.getPhone().equals(phone2) && (!bo.getPhone().equals("")) )
        {
          ++matchCount;
          ffd.setFieldData("phone_match_" + boId, "E");
        }
        
        if( !bo.getSSN().equals("123456789") && !bo.getSSN().equals("987654321") && 
            bo.getSSN().length() == 9 && (bo.getSSN().equals(ssn1) || bo.getSSN().equals(ssn2)) )
        {
          ++matchCount;
          ffd.setFieldData("tax_id_match_" + boId, "E");
        }
        
        if( !bo.getDL().equals("") && (bo.getDL().equals(dl1) || bo.getDL().equals(dl2)) )
        {
          ++matchCount;
          ffd.setFieldData("dl_match_" + boId, "E");
        }
        
        if( bo.getAddress1().toUpperCase().equals(addr1) || bo.getAddress1().toUpperCase().equals(addr2) &&
            (!bo.getAddress1().equals("")) )
        {
          ++matchCount;
          ffd.setFieldData("addr_match_" + boId, "E");
        }
      }
      
      principle = ffd.spew();
      
      if( matchCount != 0 )
      {
        responses = new String[2];
        responses[0] = general;
        responses[1] = principle;
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
    
    return responses;
  }
  
  private void imSearch( IMData imData )
  {
    ResultSet            rs         = null;
    ResultSetIterator    it         = null;
    
    if( imData == null )
    {
      return;
    }
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] it = { select mr.app_seq_num                          as app_seq_num,
//                 mr.merch_number                         as merchant_number,
//                 mr.merch_statetax_id                    as state_tax_id,
//                 mr.merch_federal_tax_id                 as fed_tax_id,
//                 mr.merch_web_url                        as web_url,
//                 mr.merch_legal_name                     as legal_name,
//                 mr.merch_business_name                  as dba_name,
//                 nvl(addr_m.address_line1, addr_b.address_line1)
//                                                         as business_addr_line1,
//                 nvl(addr_m.address_line2, addr_b.address_line2)
//                                                         as business_addr_line2,
//                 nvl(addr_m.address_city, addr_b.address_city)
//                                                         as business_city,
//                 nvl(addr_m.countrystate_code, addr_b.countrystate_code)
//                                                         as business_state,
//                 nvl(addr_m.address_zip, addr_b.address_zip)
//                                                         as business_zip,
//                 decode( nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')),
//                         'US','USA',
//                         nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')) )
//                                                         as business_country,
//                 nvl(addr_m.address_phone, addr_b.address_phone)
//                                                         as business_phone,
//                 bo_p.busowner_last_name                 as last_name_1,
//                 bo_p.busowner_first_name                as first_name_1,
//                 bo_p.busowner_name_initial              as initial_1,
//                 nvl(addr_o.address_line1, addr_m.address_line1)
//                                                         as addr_line1_1,
//                 nvl(addr_o.address_line2, addr_m.address_line2)
//                                                         as addr_line2_1,
//                 nvl(addr_o.address_city, addr_m.address_city)
//                                                         as city_1,
//                 nvl(addr_o.countrystate_code, addr_m.countrystate_code)
//                                                         as state_1,
//                 substr( nvl(addr_o.address_zip, addr_m.address_zip),0,5)
//                                                         as zip_1,
//                 decode( nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),
//                         'US','USA',
//                         nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')) )
//                                                         as country_1,
//                 decode( nvl(addr_o.address_phone,addr_m.address_phone),
//                         '0',null,
//                         nvl(addr_o.address_phone,addr_m.address_phone) )
//                                                         as phone_1,
//                 bo_p.busowner_ssn                       as fed_tax_id_1,
//                 bo_p.busowner_drvlicnum                 as dl_1,
//                 bo_s.busowner_last_name                 as last_name_2,
//                 bo_s.busowner_first_name                as first_name_2,
//                 bo_s.busowner_name_initial              as initial_2,
//                 nvl(addr_s.address_line1, addr_m.address_line1)
//                                                         as addr_line1_2,
//                 nvl(addr_s.address_line2, addr_m.address_line2)
//                                                         as addr_line2_2,
//                 nvl(addr_s.address_city, addr_m.address_city)
//                                                         as city_2,
//                 nvl(addr_s.countrystate_code, addr_m.countrystate_code)
//                                                         as state_2,
//                 substr( nvl(addr_s.address_zip, addr_m.address_zip),0,5)
//                                                         as zip_2,
//                 decode( nvl(addr_s.country_code,nvl(addr_m.country_code,'USA')),
//                         'US','USA',
//                         nvl(addr_s.country_code,nvl(addr_m.country_code,'USA')) )
//                                                         as country_2,
//                 decode( nvl(addr_s.address_phone,addr_m.address_phone),
//                         '0',null,
//                         nvl(addr_s.address_phone,addr_m.address_phone) )
//                                                         as phone_2,
//                 bo_s.busowner_ssn                       as fed_tax_id_2,
//                 bo_s.busowner_drvlicnum                 as dl_2
//          from   merchant       mr,
//                 address        addr_b,
//                 address        addr_m,
//                 address        addr_s,
//                 address        addr_o,
//                 businessowner  bo_s,
//                 businessowner  bo_p
//          where  ( upper(mr.merch_legal_name) like '%' || :imData.getMerchantName().toUpperCase() || '%' or
//                   upper(mr.merch_business_name) like '%' || :imData.getDbaName().toUpperCase() || '%' or
//                   mr.merch_federal_tax_id = :imData.getFedTaxId() 
//                 ) and
//                 mr.app_seq_num != :imData.getAppSeqNum() and
//                 addr_b.app_seq_num(+) = mr.app_seq_num and 
//                 addr_b.addresstype_code(+) = :mesConstants.ADDR_TYPE_BUSINESS and -- 1 and
//                 addr_m.app_seq_num(+) = mr.app_seq_num and 
//                 addr_m.addresstype_code(+) = :mesConstants.ADDR_TYPE_MAILING and -- 2 and
//                 addr_o.app_seq_num(+) = mr.app_seq_num and 
//                 addr_o.addresstype_code(+) = :mesConstants.ADDR_TYPE_OWNER1 and -- 4 and
//                 addr_s.app_seq_num(+) = mr.app_seq_num and 
//                 addr_s.addresstype_code(+) = :mesConstants.ADDR_TYPE_OWNER2 and -- 5 and
//                 bo_p.app_seq_num(+) = mr.app_seq_num and
//                 bo_p.busowner_num(+) = 1 and
//                 bo_p.busowner_last_name(+) is not null
//                 and bo_s.app_seq_num(+) = mr.app_seq_num and
//                 bo_s.busowner_num(+) = 2 
//                 and bo_s.busowner_last_name(+) is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4238 = imData.getMerchantName().toUpperCase();
 String __sJT_4239 = imData.getDbaName().toUpperCase();
 String __sJT_4240 = imData.getFedTaxId();
 long __sJT_4241 = imData.getAppSeqNum();
  try {
   String theSqlTS = "select mr.app_seq_num                          as app_seq_num,\n               mr.merch_number                         as merchant_number,\n               mr.merch_statetax_id                    as state_tax_id,\n               mr.merch_federal_tax_id                 as fed_tax_id,\n               mr.merch_web_url                        as web_url,\n               mr.merch_legal_name                     as legal_name,\n               mr.merch_business_name                  as dba_name,\n               nvl(addr_m.address_line1, addr_b.address_line1)\n                                                       as business_addr_line1,\n               nvl(addr_m.address_line2, addr_b.address_line2)\n                                                       as business_addr_line2,\n               nvl(addr_m.address_city, addr_b.address_city)\n                                                       as business_city,\n               nvl(addr_m.countrystate_code, addr_b.countrystate_code)\n                                                       as business_state,\n               nvl(addr_m.address_zip, addr_b.address_zip)\n                                                       as business_zip,\n               decode( nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')),\n                       'US','USA',\n                       nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')) )\n                                                       as business_country,\n               nvl(addr_m.address_phone, addr_b.address_phone)\n                                                       as business_phone,\n               bo_p.busowner_last_name                 as last_name_1,\n               bo_p.busowner_first_name                as first_name_1,\n               bo_p.busowner_name_initial              as initial_1,\n               nvl(addr_o.address_line1, addr_m.address_line1)\n                                                       as addr_line1_1,\n               nvl(addr_o.address_line2, addr_m.address_line2)\n                                                       as addr_line2_1,\n               nvl(addr_o.address_city, addr_m.address_city)\n                                                       as city_1,\n               nvl(addr_o.countrystate_code, addr_m.countrystate_code)\n                                                       as state_1,\n               substr( nvl(addr_o.address_zip, addr_m.address_zip),0,5)\n                                                       as zip_1,\n               decode( nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),\n                       'US','USA',\n                       nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')) )\n                                                       as country_1,\n               decode( nvl(addr_o.address_phone,addr_m.address_phone),\n                       '0',null,\n                       nvl(addr_o.address_phone,addr_m.address_phone) )\n                                                       as phone_1,\n               bo_p.busowner_ssn                       as fed_tax_id_1,\n               bo_p.busowner_drvlicnum                 as dl_1,\n               bo_s.busowner_last_name                 as last_name_2,\n               bo_s.busowner_first_name                as first_name_2,\n               bo_s.busowner_name_initial              as initial_2,\n               nvl(addr_s.address_line1, addr_m.address_line1)\n                                                       as addr_line1_2,\n               nvl(addr_s.address_line2, addr_m.address_line2)\n                                                       as addr_line2_2,\n               nvl(addr_s.address_city, addr_m.address_city)\n                                                       as city_2,\n               nvl(addr_s.countrystate_code, addr_m.countrystate_code)\n                                                       as state_2,\n               substr( nvl(addr_s.address_zip, addr_m.address_zip),0,5)\n                                                       as zip_2,\n               decode( nvl(addr_s.country_code,nvl(addr_m.country_code,'USA')),\n                       'US','USA',\n                       nvl(addr_s.country_code,nvl(addr_m.country_code,'USA')) )\n                                                       as country_2,\n               decode( nvl(addr_s.address_phone,addr_m.address_phone),\n                       '0',null,\n                       nvl(addr_s.address_phone,addr_m.address_phone) )\n                                                       as phone_2,\n               bo_s.busowner_ssn                       as fed_tax_id_2,\n               bo_s.busowner_drvlicnum                 as dl_2\n        from   merchant       mr,\n               address        addr_b,\n               address        addr_m,\n               address        addr_s,\n               address        addr_o,\n               businessowner  bo_s,\n               businessowner  bo_p\n        where  ( upper(mr.merch_legal_name) like '%' ||  :1  || '%' or\n                 upper(mr.merch_business_name) like '%' ||  :2  || '%' or\n                 mr.merch_federal_tax_id =  :3  \n               ) and\n               mr.app_seq_num !=  :4  and\n               addr_b.app_seq_num(+) = mr.app_seq_num and \n               addr_b.addresstype_code(+) =  :5  and -- 1 and\n               addr_m.app_seq_num(+) = mr.app_seq_num and \n               addr_m.addresstype_code(+) =  :6  and -- 2 and\n               addr_o.app_seq_num(+) = mr.app_seq_num and \n               addr_o.addresstype_code(+) =  :7  and -- 4 and\n               addr_s.app_seq_num(+) = mr.app_seq_num and \n               addr_s.addresstype_code(+) =  :8  and -- 5 and\n               bo_p.app_seq_num(+) = mr.app_seq_num and\n               bo_p.busowner_num(+) = 1 and\n               bo_p.busowner_last_name(+) is not null\n               and bo_s.app_seq_num(+) = mr.app_seq_num and\n               bo_s.busowner_num(+) = 2 \n               and bo_s.busowner_last_name(+) is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4238);
   __sJT_st.setString(2,__sJT_4239);
   __sJT_st.setString(3,__sJT_4240);
   __sJT_st.setLong(4,__sJT_4241);
   __sJT_st.setInt(5,mesConstants.ADDR_TYPE_BUSINESS);
   __sJT_st.setInt(6,mesConstants.ADDR_TYPE_MAILING);
   __sJT_st.setInt(7,mesConstants.ADDR_TYPE_OWNER1);
   __sJT_st.setInt(8,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.InternalMatchProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
      rs = it.getResultSet();
      while( rs.next() )
      {
        String[] responses = buildRespString(rs, imData);
        if( responses != null )
        {
          imData.getIMResponses().addElement( responses );
        }
      }
      
      rs.close();
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
  }
  
  protected IMData loadRecord( long mSeqNum )
  {
    ResultSet            rs         = null;
    ResultSetIterator    it         = null;
    IMData               imData     = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:385^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mrq.match_seq_num                       as match_seq_num,
//                  mrq.app_seq_num                         as app_seq_num,
//                  mr.merch_number                         as merchant_number,
//                  mr.merch_statetax_id                    as state_tax_id,
//                  mr.merch_federal_tax_id                 as fed_tax_id,
//                  mr.merch_web_url                        as web_url,
//                  mr.merch_legal_name                     as legal_name,
//                  mr.merch_business_name                  as dba_name,
//                  nvl(addr_b.address_line1,addr_m.address_line1)
//                                                          as business_addr_line1,
//                  nvl(addr_b.address_line2,addr_m.address_line2)
//                                                          as business_addr_line2,
//                  nvl(addr_b.address_city,addr_m.address_city)
//                                                          as business_city,
//                  nvl(addr_b.countrystate_code,addr_m.countrystate_code)
//                                                          as business_state,
//                  nvl(addr_b.address_zip,addr_m.address_zip)
//                                                          as business_zip,
//                  decode( nvl(addr_b.country_code,nvl(addr_m.country_code,'USA')),
//                          'US','USA',
//                          nvl(addr_b.country_code,nvl(addr_m.country_code,'USA'))
//                        )                                 as business_country,
//                  nvl(addr_b.address_phone,addr_m.address_phone)
//                                                          as business_phone,
//                  bo.busowner_num                         as id_principal,
//                  bo.busowner_last_name                   as owner_last_name,
//                  bo.busowner_first_name                  as owner_first_name,
//                  bo.busowner_name_initial                as owner_initial,
//                  nvl(addr_o.address_line1, addr_m.address_line1)
//                                                          as owner_addr_line1,
//                  nvl(addr_o.address_line2, addr_m.address_line2)
//                                                          as owner_addr_line2,
//                  nvl(addr_o.address_city, addr_m.address_city)
//                                                          as owner_city,
//                  nvl(addr_o.countrystate_code, addr_m.countrystate_code)
//                                                          as owner_state,
//                  nvl(addr_o.address_zip, addr_m.address_zip)
//                                                          as owner_zip,
//                  decode( nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),
//                          'US','USA',
//                          nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')) )
//                                                          as owner_country,
//                  decode( nvl(addr_o.address_phone,addr_m.address_phone),
//                          '0',null,
//                          nvl(addr_o.address_phone,addr_m.address_phone) )
//                                                          as owner_phone,
//                  bo.busowner_ssn                         as owner_ssn,
//                  bo.busowner_drvlicnum                   as owner_dl
//          from    match_requests          mrq,
//                  merchant                mr,
//                  address                 addr_b,
//                  address                 addr_m,
//                  address                 addr_o,
//                  businessowner           bo
//          where   mrq.match_seq_num = :mSeqNum and
//                  mr.app_seq_num = mrq.app_seq_num and
//                  addr_b.app_seq_num(+) = mr.app_seq_num and 
//                  addr_b.addresstype_code(+) = :mesConstants.ADDR_TYPE_BUSINESS and -- 1 and
//                  addr_m.app_seq_num(+) = mr.app_seq_num and 
//                  addr_m.addresstype_code(+) = :mesConstants.ADDR_TYPE_MAILING and -- 2 and
//                  addr_o.app_seq_num(+) = mr.app_seq_num and 
//                  addr_o.addresstype_code(+) = :mesConstants.ADDR_TYPE_OWNER1 and -- 4 and
//                  bo.app_seq_num(+) = mr.app_seq_num and
//                  bo.busowner_last_name(+) is not null 
//          order by bo.busowner_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mrq.match_seq_num                       as match_seq_num,\n                mrq.app_seq_num                         as app_seq_num,\n                mr.merch_number                         as merchant_number,\n                mr.merch_statetax_id                    as state_tax_id,\n                mr.merch_federal_tax_id                 as fed_tax_id,\n                mr.merch_web_url                        as web_url,\n                mr.merch_legal_name                     as legal_name,\n                mr.merch_business_name                  as dba_name,\n                nvl(addr_b.address_line1,addr_m.address_line1)\n                                                        as business_addr_line1,\n                nvl(addr_b.address_line2,addr_m.address_line2)\n                                                        as business_addr_line2,\n                nvl(addr_b.address_city,addr_m.address_city)\n                                                        as business_city,\n                nvl(addr_b.countrystate_code,addr_m.countrystate_code)\n                                                        as business_state,\n                nvl(addr_b.address_zip,addr_m.address_zip)\n                                                        as business_zip,\n                decode( nvl(addr_b.country_code,nvl(addr_m.country_code,'USA')),\n                        'US','USA',\n                        nvl(addr_b.country_code,nvl(addr_m.country_code,'USA'))\n                      )                                 as business_country,\n                nvl(addr_b.address_phone,addr_m.address_phone)\n                                                        as business_phone,\n                bo.busowner_num                         as id_principal,\n                bo.busowner_last_name                   as owner_last_name,\n                bo.busowner_first_name                  as owner_first_name,\n                bo.busowner_name_initial                as owner_initial,\n                nvl(addr_o.address_line1, addr_m.address_line1)\n                                                        as owner_addr_line1,\n                nvl(addr_o.address_line2, addr_m.address_line2)\n                                                        as owner_addr_line2,\n                nvl(addr_o.address_city, addr_m.address_city)\n                                                        as owner_city,\n                nvl(addr_o.countrystate_code, addr_m.countrystate_code)\n                                                        as owner_state,\n                nvl(addr_o.address_zip, addr_m.address_zip)\n                                                        as owner_zip,\n                decode( nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),\n                        'US','USA',\n                        nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')) )\n                                                        as owner_country,\n                decode( nvl(addr_o.address_phone,addr_m.address_phone),\n                        '0',null,\n                        nvl(addr_o.address_phone,addr_m.address_phone) )\n                                                        as owner_phone,\n                bo.busowner_ssn                         as owner_ssn,\n                bo.busowner_drvlicnum                   as owner_dl\n        from    match_requests          mrq,\n                merchant                mr,\n                address                 addr_b,\n                address                 addr_m,\n                address                 addr_o,\n                businessowner           bo\n        where   mrq.match_seq_num =  :1  and\n                mr.app_seq_num = mrq.app_seq_num and\n                addr_b.app_seq_num(+) = mr.app_seq_num and \n                addr_b.addresstype_code(+) =  :2  and -- 1 and\n                addr_m.app_seq_num(+) = mr.app_seq_num and \n                addr_m.addresstype_code(+) =  :3  and -- 2 and\n                addr_o.app_seq_num(+) = mr.app_seq_num and \n                addr_o.addresstype_code(+) =  :4  and -- 4 and\n                bo.app_seq_num(+) = mr.app_seq_num and\n                bo.busowner_last_name(+) is not null \n        order by bo.busowner_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   __sJT_st.setInt(3,mesConstants.ADDR_TYPE_MAILING);
   __sJT_st.setInt(4,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.InternalMatchProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:452^7*/
      rs = it.getResultSet();
      while( rs.next() )
      {
        // only get the first record
        if( imData == null )
        {
          imData = new IMData(rs);
        }
        imData.getBusOwners().addElement(new BusOwner(rs));
      }
      
      rs.close();
    }
    catch( Exception e )
    {
      imData = null;
      e.printStackTrace();
    }
    
    return imData;
  }
  
  /*
   ** METHOD execute
   **
   ** Entry point from timed event manager.
   */
  public boolean execute()
  {
    int   recCount         = 0;
    long  recId            = 0L;
    long  sequenceId       = 0L;
    ResultSet         rs   = null;
    ResultSetIterator it   = null;

    
    try
    {
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:493^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.app_seq_num      as app_seq_num,
//                  mr.match_seq_num    as match_seq_num
//          from    match_requests mr
//          where   request_status = 'C'
//                  and mr.process_end_date > sysdate - 7 
//                  and mr.request_comments like '%APP_QUEUE_MATCH_INSERT trigger%'  
//                  and not exists
//                  (
//                    select match_seq_num
//                    from   internal_match_process
//                    where  match_seq_num = mr.match_seq_num
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.app_seq_num      as app_seq_num,\n                mr.match_seq_num    as match_seq_num\n        from    match_requests mr\n        where   request_status = 'C'\n                and mr.process_end_date > sysdate - 7 \n                and mr.request_comments like '%APP_QUEUE_MATCH_INSERT trigger%'  \n                and not exists\n                (\n                  select match_seq_num\n                  from   internal_match_process\n                  where  match_seq_num = mr.match_seq_num\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.InternalMatchProcessor",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.InternalMatchProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^7*/
      rs = it.getResultSet();
      while( rs.next() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:511^9*/

//  ************************************************************
//  #sql [Ctx] { insert into internal_match_process (process_type, match_seq_num )
//            values (1, :rs.getLong("match_seq_num") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4242 = rs.getLong("match_seq_num");
   String theSqlTS = "insert into internal_match_process (process_type, match_seq_num )\n          values (1,  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4242);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:514^9*/
      }
      /*@lineinfo:generated-code*//*@lineinfo:516^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^26*/
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:519^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rec_id)  
//          from    internal_match_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rec_id)   \n        from    internal_match_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.InternalMatchProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:524^7*/
      
      if ( recCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:528^9*/

//  ************************************************************
//  #sql [Ctx] { select  imp_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  imp_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.InternalMatchProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:532^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:534^9*/

//  ************************************************************
//  #sql [Ctx] { update  internal_match_process
//            set     process_sequence = :sequenceId
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  internal_match_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:539^9*/ 
      }
      
      if( sequenceId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:544^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id         as rec_id,
//                    match_seq_num  as match_seq_num
//            from    internal_match_process 
//            where   process_sequence = :sequenceId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id         as rec_id,\n                  match_seq_num  as match_seq_num\n          from    internal_match_process \n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.InternalMatchProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:550^9*/
        rs = it.getResultSet();
      
        while( rs.next() )
        {
          recId = rs.getLong("rec_id");
          recordTsBegin( recId );
          IMData imData = loadRecord( rs.getLong("match_seq_num") );
          imSearch(imData);
          storeIMResponses(imData);
          recordTsEnd( recId );
        }
        rs.close();
      }
    }
    catch( Exception e )
    {
      logEntry("execute()recId=" + recId + " ", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( true );
  }
  
  protected void processTest( long matchSeqNum )
  {
    try
    {
      IMData imData = loadRecord( matchSeqNum );
      imSearch(imData);
      storeIMResponses(imData);
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
  }
  
  protected void recordTsBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:599^7*/

//  ************************************************************
//  #sql [Ctx] { update  internal_match_process
//          set     process_begin_date = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  internal_match_process\n        set     process_begin_date =  :1 \n        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:604^7*/
    }
    catch(Exception e)
    {
      logEntry("recordTsBegin()", e.toString());
    }      
  }
  
  protected void recordTsEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L;
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:620^7*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//          from    internal_match_process
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n        from    internal_match_process\n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.InternalMatchProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:629^7*/

//  ************************************************************
//  #sql [Ctx] { update  internal_match_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4243 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  internal_match_process\n        set     process_end_date =  :1 ,\n                process_elapsed =  :2 \n        where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4243);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:635^7*/
    }
    catch(Exception e)
    {
      logEntry("recordTsEnd()", e.toString());
    }      
  }
  
  private void storeIMResponses(IMData imData )
  {
    int imrSeqNum = 0;

    try
    {
      connect();
      
      for( int j = 0; j < imData.getIMResponses().size(); ++j )
      {
        String[] responses = (String[])imData.getIMResponses().elementAt(j);
        /*@lineinfo:generated-code*//*@lineinfo:654^9*/

//  ************************************************************
//  #sql [Ctx] { select  match_response_data_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  match_response_data_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.InternalMatchProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   imrSeqNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:659^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:661^9*/

//  ************************************************************
//  #sql [Ctx] { insert into match_requests_responsedata 
//            (
//              match_seq_num, 
//              general_record, 
//              principal_record, 
//              mrd_seq_num, 
//              isacknowledged,
//              is_imr,
//              is_from_impblacklist
//            )
//            values 
//            (
//              :imData.getMatchSeqNum().substring(imData.getMatchSeqNum().length()-5, imData.getMatchSeqNum().length()),
//              :responses[0],
//              :responses[1],
//              :imrSeqNum,
//              0,
//              1,
//              0
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4244 = imData.getMatchSeqNum().substring(imData.getMatchSeqNum().length()-5, imData.getMatchSeqNum().length());
 String __sJT_4245 = responses[0];
 String __sJT_4246 = responses[1];
   String theSqlTS = "insert into match_requests_responsedata \n          (\n            match_seq_num, \n            general_record, \n            principal_record, \n            mrd_seq_num, \n            isacknowledged,\n            is_imr,\n            is_from_impblacklist\n          )\n          values \n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            0,\n            1,\n            0\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.InternalMatchProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4244);
   __sJT_st.setString(2,__sJT_4245);
   __sJT_st.setString(3,__sJT_4246);
   __sJT_st.setInt(4,imrSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:683^9*/
      }
      
      if( imrSeqNum != 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:688^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:688^28*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeIMResponses() matchSeqNum=" + imData.getMatchSeqNum() + " ", e.toString());
    }
  }
  
  public static void main( String[] args )
  {
    InternalMatchProcessor   imp  = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      imp = new InternalMatchProcessor();
      
      if ( args.length == 2 && args[0].equals("test") )
      {
        imp.connect();
        imp.processTest( Long.parseLong(args[1]) );
      }
      else
      {
        imp.execute();
      }
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/