package com.mes.utils;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.mes.constants.FlatFileDefConstants;

public class DiscoverRegistrationFileUtil implements Serializable {

    private static final long serialVersionUID = 1L;
    
    static Logger log = Logger.getLogger(DiscoverRegistrationFileUtil.class);
    private static StringBuilder missingMandatoryField = null;
    
    private static String eiphenDelimiter = "-";
    private static String semiColonDelimiter = ";";
    private static String INVALID_NUMBER = "000000000";
    private static String BUS_TYPE_S = "S";
    
    private static final List<String> merchantMandatoryFields = new ArrayList(Arrays.asList(FlatFileDefConstants.DISCOVER_MERCHANT_NUMBER,
            FlatFileDefConstants.DBA_ADDRESS_1, FlatFileDefConstants.DBA_CITY, FlatFileDefConstants.DBA_COUNTRY,
            FlatFileDefConstants.DBA_NAME, FlatFileDefConstants.DBA_PHONE, FlatFileDefConstants.DBA_STATE,
            FlatFileDefConstants.DBA_ZIP, FlatFileDefConstants.MERCHANT_CATEGORY_CODE, FlatFileDefConstants.MERCHANT_STATUS_DATE,
            FlatFileDefConstants.BUSINESS_TYPE_DESC,FlatFileDefConstants.MERCHANT_STATUS,FlatFileDefConstants.PRINCIPAL_FIRST_NAME));
    
    private static final List<String> principalMandatoryFields = new ArrayList(Arrays.asList(
            FlatFileDefConstants.PRINCIPAL_ADDRESS_1,FlatFileDefConstants.PRINCIPAL_CITY,FlatFileDefConstants.PRINCIPAL_COUNTRY,
            FlatFileDefConstants.PRINCIPAL_STATE,FlatFileDefConstants.PRINCIPAL_ZIP));
    
    private static final List<String> businessMandatoryFields = new ArrayList(Arrays.asList(FlatFileDefConstants.BUSINESS_ADDRESS_1,
            FlatFileDefConstants.BUSINESS_CITY,FlatFileDefConstants.BUSINESS_COUNTRY,FlatFileDefConstants.BUSINESS_STATE,
            FlatFileDefConstants.BUSINESS_ZIP,FlatFileDefConstants.LEGAL_NAME));
    
    private static final List<String> businessTypeDesc = new ArrayList(Arrays.asList("G", "C", "N"));
    
    private static final List<String> taxIdSSNFields = new ArrayList(Arrays.asList(FlatFileDefConstants.PRINCIPAL_SSN, FlatFileDefConstants.TAX_ID_NUMBER));
    
    public static List<String> getmerchantMandatoryFields() {
        return merchantMandatoryFields;
    }
    
    public static List<String> getprincipalMandatoryFields() {
        return principalMandatoryFields;
    }
    
    public static List<String> getbusinessMandatoryFields() {
        return businessMandatoryFields;
    }
    
    public static List<String> getTaxIdSsnFields() {
        return taxIdSSNFields;
    }
    
    public static void validatePrincipalFields(ResultSet rs, String fieldName){
        
        try {
               if(fieldName.equals(FlatFileDefConstants.PRINCIPAL_ADDRESS_1) && isPrincipalAddressLineMandatory(rs)){
                  missingMandatoryField.append(fieldName + semiColonDelimiter);
               }
               if(fieldName.equals(FlatFileDefConstants.PRINCIPAL_CITY) && isPrincipalCityMandatory(rs)){
                  missingMandatoryField.append(fieldName + semiColonDelimiter);
               }
               if(fieldName.equals(FlatFileDefConstants.PRINCIPAL_STATE) && isPrincipalStateMandatory(rs)){
                  missingMandatoryField.append(fieldName + semiColonDelimiter);
               }
               if(fieldName.equals(FlatFileDefConstants.PRINCIPAL_ZIP) && isPrincipalZipMandatory(rs)){
                  missingMandatoryField.append(fieldName + semiColonDelimiter);
               }
        } catch(Exception e){
            log.error("Exception in DiscoverRegistrationFileUtil::validatePrincipalFields():" +e.getMessage());
        }
    }
    
    public static String validateRegistrationFileDetail(ResultSet rs,String busType){
        
        missingMandatoryField = new StringBuilder();
         try {
            for(String fieldName : getmerchantMandatoryFields()){
                    if(StringUtils.isEmpty(rs.getString(fieldName))){
                        missingMandatoryField.append(fieldName + semiColonDelimiter);
                        continue;
                    }
             }
            
            for(String fieldName : getbusinessMandatoryFields()){
                  if(StringUtils.isEmpty(rs.getString(fieldName)) && businessTypeDesc.contains(rs.getString(fieldName))){
                         missingMandatoryField.append(fieldName + semiColonDelimiter);
                         continue;
                  }
             }
            
            for(String fieldName : getprincipalMandatoryFields()){
                   if(StringUtils.isEmpty(rs.getString(fieldName))){
                      validatePrincipalFields(rs,fieldName);
                  }
            }
            
            for(String fieldName : getTaxIdSsnFields()){
	            	String tempNumber = rs.getString(fieldName);
	            	if(INVALID_NUMBER.equals(tempNumber)){
	            		if(BUS_TYPE_S.equals(busType) && FlatFileDefConstants.PRINCIPAL_SSN.equals(fieldName)) {
	            			missingMandatoryField.append(fieldName + semiColonDelimiter);
	            			break;
	            		} else if(!BUS_TYPE_S.equals(busType) && FlatFileDefConstants.TAX_ID_NUMBER.equals(fieldName)){
	            			missingMandatoryField.append(fieldName + semiColonDelimiter);
	            			break;
	            		}
	            	}
            }
            
            if(!StringUtils.isEmpty(missingMandatoryField.toString())){
                missingMandatoryField.append(eiphenDelimiter + rs.getString(FlatFileDefConstants.MERCHANT_NUMBER));
            }
            
            }
            
            catch (SQLException e) {
                log.error("Exception in DiscoverRegistrationFileUtil::validateRegistrationFileDetail():" +e.getMessage());
            }
            
        return missingMandatoryField.toString();
    }
    
    
    private static boolean isPrincipalAddressLineMandatory(ResultSet rs) throws SQLException{
        
        boolean mandatory = false;
        if(StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_CITY)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_STATE)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ZIP)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_COUNTRY))){
            mandatory = false;
        }else {
            mandatory = true;
        }
        
        return mandatory;
    }
    
     private static boolean isPrincipalCityMandatory(ResultSet rs) throws SQLException{
        
        boolean mandatory = false;
        if(StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ADDRESS_1)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_STATE)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ZIP)) ||
           StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_COUNTRY))){
            mandatory = false;
        }else {
            mandatory = true;
        }
        
        return mandatory;
    }
     
     private static boolean isPrincipalStateMandatory(ResultSet rs) throws SQLException{
         
         boolean mandatory = false;
         if(StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ADDRESS_1)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_CITY)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ZIP)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_COUNTRY))){
             mandatory = false;
         }else {
             mandatory = true;
         }
         
         return mandatory;
     }
     
     private static boolean isPrincipalZipMandatory(ResultSet rs) throws SQLException{
         
         boolean mandatory = false;
         if(StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_ADDRESS_1)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_STATE)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_CITY)) ||
            StringUtils.isEmpty(rs.getString(FlatFileDefConstants.PRINCIPAL_COUNTRY))){
             mandatory = false;
         }else {
             mandatory = true;
         }
         
         return mandatory;
     }

}
