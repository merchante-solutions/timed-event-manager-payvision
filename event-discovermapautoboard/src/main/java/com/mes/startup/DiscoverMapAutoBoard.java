/*************************************************************************

  FILE: $URL: /src/main/com/mes/startup/DiscoverMapAutoBoard.java $

  Description:

  Created by         : $Author: sivaturi $
  Created Date       : $Date:   2017-11-21  $
  Last Modified By   : $Author:             $
  Last Modified Date : $Date:               $
  Version            : $Revision:0.1        $

  Change History:
     See Bitbucket

  Copyright (C) 2000-2014,2015,2016,2017 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.FlatFileDefConstants;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.utils.DiscoverRegistrationFileUtil;

/**
 * <b>purpose</b>
 * <ul>
 *  <li>The purpose of this class is to generate Discover Profile Registration File and transmit to Discover</li>
 *  <li>This file includes the merchant information that has to be sent out to Discover.</li>
 *  <li>Discover will also transmit a response file that provides information about the merchant profiles that were processed
 *      or rejected</li>
 *  </ul>
 *  <p>{@link execute} base method of the event</p>
 *  <p>{@link sendBoardingFile} sends generated registration file to discover </p>
 *  <p>{@link buildHeaderRecord} builds the header record for profile registration</p>
 *  <p>{@link buildDetailRecord} builds the detail record for profile registration</p>
 *  <p>{@link buildTrailerRecord} builds the trailer record for profile registration</p>
 *  <p>{@link generateFileName} generates the filename for the .dat file</p>
 *  <p>{@link updateProcessSequence} gets the merchant numbers that are eligible for profile registration</p>
 *  <p>{@link sendSuccessEmail} sends the success email to the group for the file transmission</p>
 *  <p>{@link sendFailureEmail} sends the failure email to the group if there are any missing mandatory fields</p>
 */
public class DiscoverMapAutoBoard extends TsysFileBase {
    
    private static final long serialVersionUID = 1L;
    static Logger logger = Logger.getLogger(DiscoverMapAutoBoard.class);
    
    private int processSequence = 0;
    private static final String SEND = "SEND";
    private static final String RECEIVE = "RECEIVE";
    private StringBuilder missingFields = new StringBuilder();
    private String sendFileName = null;
    private FlatFileRecord ffd = null;
    private int recordCount = 0;
    private boolean testMode = false;
    private Set<String> invalidMerchantList = new HashSet();
    private Set<String> updateActionForMerchantList = new HashSet();
    private static final String EMPTY_STR = "";
    private static ArrayList<String> validRecordTypes = new ArrayList<String>(
            Arrays.asList("01","04","10","11","12","20","21","22"));
    private static ArrayList<String> recordTypesWithCMNF = new ArrayList<String>(
            Arrays.asList("20","21","22"));
    
    public DiscoverMapAutoBoard(){
        PropertiesFilename = "discover-registration.properties";
        EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_NOTIFY;
        EmailGroupFailure = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_FAILURE;
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li> this is the base method for this event which will trigger from the GlobalEventManager
     *      based on the time and type of the event in the timed_events table</li>
     * </ul>
     * <p> {@link getEventArg} gets the eventArg mentioned in timed_events table for this event </p>
     * <p> {@link sendBoardingFile} If SEND is action, this method will be called </p>  
     * <p> {@link retrieveResponseFile} If RECEIVE is action, this method will be called </p>  
     */
    @Override
    public boolean execute(){
        boolean result = false;
        String action = getEventArg(0);
        
        if(!StringUtils.isEmpty(action)){
            
            if(SEND.equalsIgnoreCase(action)){
                result = sendBoardingFile();
            }
            
            if(RECEIVE.equalsIgnoreCase(action)){
                 result = retrieveResponseFile();
            }
        }
        
        return result;
    }
    
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li> this is to build a header record and write to the file</li>
     * </ul>
     * <p> {@link DEF_TYPE_DISCOVER_PROFILE_HEADER_03152} uses this def_type to generate header record </p> 
     */
    private void buildHeaderRecord(BufferedWriter out){
        
        logger.info("Building Header Record");
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_HEADER_03152);
        Date date = new Date();
        try {
            String dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSSSSS").format(date);
            ffd.setFieldData("time_stamp", dateFormat);
            out.write(ffd.spew("~"));
            out.newLine();
        } catch(Exception e){
            logger.error("Exception in DiscoverMapAutoBoard::buildHeaderRecord():" +e.getMessage());
        }
    }
    
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li> generates the filename for the discover registration file</li>
     * </ul>
     * <p> {@link buildFilename} builds the fileName by appending sysdate and sequence </p>
     */
    private void generateFileName(){
        
        logger.info("generating FileName:");
        sendFileName = buildFilename( "disc-reg3941" );
        log.debug("fileName generated:" +sendFileName);
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li> this method validates the if the detail records obtained from the query has all the mandatory fields</li>
     * <li> Constructs a string if there are any missing mandatory fields with the merchant number</li>
     * <li> builds a detail record and write to the file</li>
     * </ul>
     * <p> {@link DEF_TYPE_DISCOVER_PROFILE_DETAIL_03152} uses this def_type to generate detail record </p>
     * <p> {@link SQL_GET_DISCOVER_REG_DETAILS} Sql query to get the details of the merchant for detail record  </p>
     * <p> {@link DiscoverRegistrationFileUtil#validateRegistrationFileDetail} validates the detail record 
     *      if it is missing any mandatory field</p>
     */
    private void buildDetailRecord(BufferedWriter out){
        
        logger.info("Building Detail Record");
        ResultSet rs = null;
        PreparedStatement ps = null;
        String missingMandatoryField = null;
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_DETAIL_03152);
        try{
            ps = con.prepareStatement(SQL_GET_DISCOVER_REG_DETAILS);
            ps.setInt(1, processSequence);
            ps.setInt(2, processSequence);
            ps.setInt(3, processSequence);
            rs = ps.executeQuery();
            while(rs.next()){
                String busTypeDesc = rs.getString(FlatFileDefConstants.BUSINESS_TYPE_DESC);
                String merchantId = rs.getString(FlatFileDefConstants.MERCHANT_NUMBER);
                missingMandatoryField = DiscoverRegistrationFileUtil.validateRegistrationFileDetail(rs,busTypeDesc);
                if(StringUtils.isEmpty(missingMandatoryField)){
                    if(!rs.getString("record_type").equals(rs.getString("old_record_type"))) {
                    	  updateActionForMerchantList.add(merchantId);
                    }
                    recordCount++;
                    ffd.resetAllFields();
                    ffd.setAllFieldData(rs);
                    ffd.setFieldData(FlatFileDefConstants.RECORD_SEQUENCE_NUMBER, recordCount);
                    //1	Sole Proprietorship - 'S'
		    		    	if("S".equals(busTypeDesc)) {
		    		    		ffd.setFieldData(FlatFileDefConstants.TAX_ID_NUMBER,EMPTY_STR);
		    		    	} else {
		    		    		ffd.setFieldData(FlatFileDefConstants.PRINCIPAL_SSN,EMPTY_STR);
		    		    	}
                    out.write(ffd.spew( "~" ));
                    out.newLine();
                } else {
            		    invalidMerchantList.add(merchantId);
                    if(StringUtils.isEmpty(missingFields.toString())) 
                        missingFields.append(missingMandatoryField);
                    else {
                        missingFields.append("\n\n");
                        missingFields.append(missingMandatoryField);
                    }
                }
            }
            
            rs.close();
            ps.close();
            
        }catch(Exception e){
            logger.error("Exception in DiscoverMapAutoBoard::buildDetailRecord():" +e.getMessage());
        }finally {
            try{ 
                if(ps != null) ps.close(); 
                if(rs != null) rs.close(); 
            } catch( Exception e ) {
                logger.error("Inside finally clause for DiscoverMapAutoBoard::buildDetailRecord():"+e.getMessage());
            }
        }
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li> this is to build a trailer record and write to the file</li>
     * </ul>
     * <p> {@link DEF_TYPE_DISCOVER_PROFILE_TRAILER_03152} uses this def_type to generate trailer record </p> 
     */
    private void buildTrailerRecord(BufferedWriter out){
        
        logger.info("Building Trailer Record");
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_TRAILER_03152);
        try{
          ffd.setFieldData("record_count", recordCount);
          out.write(ffd.spew("~"));
          out.newLine();
        }catch(Exception e){
            logger.error("Exception in DiscoverMapAutoBoard::buildTrailerRecord():" +e.getMessage());
        }
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li>updates the process sequence and generates the filename </li>
     * <li>builds registration file with header,detail & trailer records </li>
     * <li>sends status email to the group</li>
     * <li>returns a boolean value if the data file generated is transmitted successfully or not</li>
     * </ul>
     * <p> {@link generateFileName()} generates file name for the discover registration file </p> 
     * <p> {@link updateProcessSequence()} generates a sequence and updates to the db table </p> 
     * <p> {@link buildHeaderRecord()} builds a header record for the discover registration file</p>
     * <p> {@link buildDetailRecord()} builds a detail record for the discover registration file</p> 
     * <p> {@link buildTrailerRecord()} builds a trailer record for the discover registration file</p>
     * <p> {@link sendDataFile()} sends data file to discover host</p>
     * <p> {@link archiveDailyFile()} Archives the data file if file transmission is successful</p>
     * @return boolean status of file generation
     */
    private boolean sendBoardingFile(){
        
        FileWriter writer = null;
        
        BufferedWriter out = null;
        // db connection
        connect();
        
        //generates file name
        generateFileName();
        
        try {
           
           writer = new FileWriter(sendFileName);
            
           out = new BufferedWriter(writer);
           // updates process sequence of discover_map_autoboard 
           updateProcessSequence();
           
           // builds header record
           buildHeaderRecord(out);
           
           //builds detail record
           buildDetailRecord(out);
           
           //builds trailer record
           buildTrailerRecord(out);
           
           // update load file name for merchant in discover_map_autoboard, which fail mandatory field check 
           if(!invalidMerchantList.isEmpty()) {
               updateLoadFileName();
           }
           
           if(!updateActionForMerchantList.isEmpty()) {
        	       updateActionCode();
           }
        
           out.flush();
           out.close();
           if(recordCount > 0 && !testMode){
               
               logger.info("Send data file" +sendFileName);
               boolean success = 
                       sendDataFile( sendFileName, 
                                     MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST),
                                     MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_USER),
                                     MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_PW),
                                     MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_PATH),
                                     false,
                                     true );
               
               if(success)
               {
                 logger.debug("archiving file");
                 archiveDailyFile(sendFileName);
               }
               else
               {
                 logger.error("*** transmission failed ***  -- File not sent due to SFTP Issue");
                 success = false;
               }
               
               // send success email to Email Group
               sendSuccessEmail(success, sendFileName);
           }
           
        // Failure email in sending the merchant with missing mandatory fields
         if(!StringUtils.isEmpty(missingFields.toString())){
               logger.info("Missing Mandatory Field data: " + missingFields);
               sendFailureEmail(sendFileName);
          }
         logger.info("DiscoverMapAutoBorad::sendBoardingFile() completed");
           
        }catch(Exception e){
            logger.error("Exception in DiscoverMapAutoBoard::sendBoardingFile():" +e.getMessage());
        }finally {
            try {
                if(writer != null) writer.close();
                if(out != null) out.close();
            } catch(Exception e){
                logger.error("Inside finally clause of DiscoverMapAutoBoard::sendBoardingFile():" +e.getMessage());
            }
        }
        
        return true;
    }
    
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li>Sends Success Email to the group notifying the status of Discover Registration
     *     File Generation and Transmission </li>
     * </ul>   
     * @param success status of the file transmission
     * @param workFilename name of the generated file
     */
    private void sendSuccessEmail(boolean success, String workFilename)
    {
      StringBuilder subject  = new StringBuilder(EMPTY_STR);
      StringBuilder body     = new StringBuilder(EMPTY_STR);
      
      try
      {
        subject.append( "Outgoing Discover MAP Profile Registration - " );
        
        body.append("Filename: ");
        body.append(workFilename);
        body.append("\n\n");
        
        if( success )
        {
          subject.append("SUCCESS");
        }
        else
        {
          subject.append("FAILURE");
        }
        
        MailMessage msg = new MailMessage();
        
        msg.setAddresses( EmailGroupSuccess );
        msg.setSubject(subject.toString());
        msg.setText(body.toString());
        msg.send();
      }
      catch( Exception e )
      {
        logEntry("Exception in DiscoverMapAutoBoard::sendSuccessEmail()", e.getMessage());
      }
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li>Sends Failure Email to the group notifying the status of Discover Registration
     *     File Missing Fields </li>
     * </ul>   
     * @param failure status of the file transmission
     * @param workFilename name of the generated file
     */
    private void sendFailureEmail(String workFilename)
    {
      StringBuilder subject  = new StringBuilder(EMPTY_STR);
      StringBuilder body     = new StringBuilder(EMPTY_STR);
      
      try
      {
        subject.append( "Outgoing Discover MAP Profile Registration - " );
        
        body.append("Filename: ");
        body.append(workFilename);
        body.append("\n\n");
        body.append("========List of Failed Merchants===========");
        body.append("\n\n");
        body.append("Mandatory Field Missing - Merchant Number");
        body.append("\n\n");
        body.append("-----------------------------------------");
        body.append("\n\n");
        body.append(missingFields.toString());
        
        subject.append("FAILURE");
        
        MailMessage msg = new MailMessage();
   
        msg.setAddresses( EmailGroupSuccess );
        msg.setSubject(subject.toString());
        msg.setText(body.toString());
        msg.send();
      }
      catch( Exception e )
      {
        logEntry("Exception in DiscoverMapAutoBoard::sendFailureEmail()", e.getMessage());
      }
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li>updates the process sequence in discover_map_autoboard table</li>
     * </ul>
     * <p> {@link SQL_GET_DISCOVER_AUTOBOARD} Sql Query to get the count of records with 0 as process sequence  </p> 
     * <p> {@link SQL_GET_DAB_PROCESS_SEQUENCE} Sql Query to generates a sequence</p> 
     * <p> {@link SQL_UPDATE_DAB_PROCESS_SEQUENCE} Sql Query to update the sequence </p>
     */
    private void updateProcessSequence(){
        logger.info("Inside updateProcessSequence method");
        PreparedStatement ps          = null;
        ResultSet         rs          = null;
        int merchCount = 0;
        
        try {
            ps = con.prepareStatement(SQL_GET_DISCOVER_AUTOBOARD);
            rs = ps.executeQuery();
            
            if(rs.next()){
                merchCount = rs.getInt(1);
            }
            ps.close();
            rs.close();
            
            logger.info("Merch count with process sequence 0 in Discover_map_auto_board :"+merchCount);
            if(merchCount > 0){
                ps = con.prepareStatement(SQL_GET_DAB_PROCESS_SEQUENCE);
                rs = ps.executeQuery();
                
                if(rs.next()){
                    processSequence = rs.getInt(1);
                }
                
                ps.close();
                rs.close();
                
                ps = con.prepareStatement(SQL_UPDATE_DAB_PROCESS_SEQUENCE);
                ps.setInt(1, processSequence);
                ps.setString(2, sendFileName);
                ps.executeUpdate();
            }
            logger.info("Process sequence updated in Discover_map_auto_board :"+processSequence);
            
        } catch (SQLException e) {
            log.error("Exception in DiscoverMapAutoBoard:updateProcessSequence"+e.getMessage());
        }
        finally
        {
          try{ 
              if(ps != null) ps.close(); 
              if(rs != null) rs.close(); 
          } catch( Exception e ) {
              log.error("Inside finally clause"+e.getMessage());
           }
        }
    }
    
    /**
     * <b>purpose</b>
     * <ul>
     * <li>updates the load file name to null for the merchants which are not sent in file to discover in discover_map_autoboard table</li>
     * </ul>
     * <p> Sql Query to update load file name </p>
     */
    private void updateLoadFileName(){
        logger.info("Inside updateLoadFileName method");
        PreparedStatement ps          = null;
        try {
        	    logger.info("Updating loadfilename to null for the merchant which are failing mandatory field check. List of merchant: "+invalidMerchantList.toString());
        		String query = "update discover_map_auto_board set load_filename = null, errors_received = null where process_sequence = ? and merchant_number in (MERCHANT_LIST)";
        		String merchantList = org.apache.commons.lang3.StringUtils.join(invalidMerchantList,",");
        		query = query.replace("MERCHANT_LIST",merchantList);
	        	ps = con.prepareStatement(query);
	        	ps.setInt(1, processSequence);
	        	ps.executeUpdate();
	        	ps.close();
        } catch (SQLException e) {
            log.error("Exception in DiscoverMapAutoBoard:updateLoadFileName"+e.getMessage());
        }
        finally
        {
          try{ 
              if(ps != null) ps.close(); 
          } catch( Exception e ) {
              log.error("Inside finally clause"+e.getMessage());
           }
        }
    }
    
    private void updateActionCode(){
        logger.info("Inside updateActionCode method");
        PreparedStatement ps          = null;
        try {
        	    logger.info("Updating action code to correct value based on the previous requests response from discover List of merchant: "+updateActionForMerchantList.toString());
        		String query = "update discover_map_auto_board set action = '01' where process_sequence = ? and merchant_number in (MERCHANT_LIST)";
        		String merchantList = org.apache.commons.lang3.StringUtils.join(updateActionForMerchantList,",");
        		query = query.replace("MERCHANT_LIST",merchantList);
	        	ps = con.prepareStatement(query);
	        	ps.setInt(1, processSequence);
	        	ps.executeUpdate();
        } catch (SQLException e) {
            log.error("Exception in DiscoverMapAutoBoard:updateActionCode"+e.getMessage());
        }
        finally
        {
          try{ 
              if(ps != null) ps.close(); 
          } catch( Exception e ) {
              log.error("Inside finally clause"+e.getMessage());
           }
        }
    }
    
    private boolean retrieveResponseFile(){
        log.info("retrieving profile response file(s)");
        
        try
        {
          String host = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST);
          String user = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER);
          String pw   = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PW);
          String path = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PATH);
          
          Sftp sftp = null;
          
          sftp = getSftp( host, user, pw, path, false);
          
          SimpleDateFormat sfm = new SimpleDateFormat("yyyyDDD");
          String fileMaskDate = sfm.format(new java.util.Date()); 
          String fileMask = null;
         
          fileMask = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_RESPONSE_FILE_MASK) + ".J" +fileMaskDate + ".*";
          
          EnumerationIterator enumi = new EnumerationIterator();
          Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
          
          String fileName = null;
          
          while( it.hasNext() )
          {
            fileName = (String)it.next();
            
            log.debug("found file: " + fileName);
            
            log.debug("retrieving file");
            // retrieve this file
            byte[] respFile = ftpGet(fileName, null, host, user, pw, path, false);
            
            // generate filename for this file
            String outFilename = buildFilename( "disc_resp3941" );
            
            log.debug("writing response file: " + outFilename);
            
            FileWriter out = null;
            try {
              out = new FileWriter(outFilename);
            
              String data = new String(respFile);
              
              out.write( data );
              
              out.flush();
              out.close();
              
              processResponseFile(outFilename, data);
            } catch(Exception fe) {
              logEntry("retrieveResponseFile() - writing file", fe.toString());
            } finally {
                if (out != null) {
                    out.close();
                }
            }
          }
          logger.info("DiscoverMapAutoBorad::retrieveResponseFile() completed");
        }
        catch(Exception e)
        {
          logEntry("retrieveResponseFile()", e.toString());
        }
        
        return true;
    }

	private void processResponseFile(String outFilename, String data) throws Exception {
		// send file in email
		  MailMessage msg = new MailMessage();
		  
		  msg.setAddresses( EmailGroupSuccess );
		  msg.setSubject("Discover Profile Response File - " + outFilename);
		  msg.setText("Response file retrieved from Discover\n\n" + outFilename);
		  
		  msg.addStringAsTextFile(outFilename, data);
		  
		  String reviewedData = getReviewedData(outFilename);
		  SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy");
		  String dateStr = dateFormat.format(new java.util.Date());
		  try {
		     msg.addStringAsTextFile("disc_for_review_"+dateStr+".dat", reviewedData);
		  } catch (Exception e) {
		    logEntry("Error in processResponseFile() - creation of disc_for_review_"+dateStr+".dat file", e.toString());
		  }
		  
		  msg.send();
		  
		  // archive file
		  archiveDailyFile(outFilename);
	}
 
    /**
     * <b>purpose</b>
     * <ul>
     * <li>parses supplied outFilename for reported errors </li>
     * <li>builds/returns a formatted string of data, for email, based on reported errors </li>
     * <li>builds a list of merchants with reported errors </li>
     * <li>updates DiscoverAutoBoard table with error status as appropriate </li>
     * </ul>
     * <p> {@link addToListMerchantsInError()} generates a list of merchants with reported errors</p>
     * <p> {@link updateDABErrorStatus()} updated discover_map_autoboard table with error status for pertinent merchants</p>  
     * <p> {@link setAnyRemainingFlaggedOutboundRecordsToN()} flag any remaining merchants, from last outbound/SEND, as no-error status</p> 
     * @param outFilename
     * @return String reviewedData from parsing
     */
    public String getReviewedData(String outFilename) {
        StringBuilder reviewedData = new StringBuilder("");
        String line = null;
        String recordType;
        String dbaName = "";
        StringBuilder errors = new StringBuilder("");
        StringBuilder errorCodes = new StringBuilder("");
        long discoverMerchantNumber = 0L;
        long mesMerchantNumber = 0L;
        final String newLine = System.getProperty("line.separator");
        final String delimiter = "~";
        Map<Long, String> errorStatusMap = new HashMap<Long, String>();
        
        Reader reader = null;
        BufferedReader bufferReader = null;
        try {
            reader = new FileReader(outFilename);
            bufferReader = new BufferedReader(reader);
            connect(true);
            // append the header
            reviewedData.append("Discover Merchant Number").append(delimiter).append("MES Merchant Number")
                    .append(delimiter).append("DBA Name").append(delimiter).append("Error List").append(newLine);

            while ((line = bufferReader.readLine()) != null) {
                line = line.trim();
                if (line.length() > 2) {
                    recordType = line.substring(0, 2);
                    if (validRecordTypes.contains(recordType)) {
                        errors = new StringBuilder("");
                        errorCodes = new StringBuilder("");
                        discoverMerchantNumber = Long.parseLong(line.substring(27, 42));
                        mesMerchantNumber = getMerchantNumberFromDmdsnum(discoverMerchantNumber);
                        line = line.substring(43);
                        if (!recordTypesWithCMNF.contains(recordType)) {
                            int idx = line.indexOf(delimiter);
                            String errString = null;
                            if (idx != -1) {
                                dbaName = line.substring(0, idx);
                                int numberOfErrors = Integer.parseInt(line.substring(idx + 1, idx + 4));
                                line = line.substring(idx + 5);
                                idx = 0;
                                for (int j = 0; j < numberOfErrors; j++, idx += 5) {
                                    errString = line.substring(idx, idx + 4);
                                    errorCodes.append(errString);
                                    errors.append(getErrorDescription(errString));
                                    if (j < numberOfErrors - 1) {
                                        errors.append("; ");
                                        errorCodes.append(",");
                                    }
                                }
                            } else {
                                dbaName = line;
                            }
                            if(StringUtils.isNotEmpty(errorCodes.toString())) {
                            	addToListMerchantsInError(dbaName, errorCodes, discoverMerchantNumber, mesMerchantNumber,
                                        errorStatusMap);
                            }
                        } else {
                        	    dbaName = "<CMNF>" + line;
                        }
                        // create a detail record here
                        reviewedData.append(discoverMerchantNumber).append(delimiter).append(mesMerchantNumber)
                                .append(delimiter).append(dbaName).append(delimiter).append(errors.toString())
                                .append(newLine);
                    }
                }
            }
            
            int countOfOutboundRecordsWithNoReportedErrors = 0;
            int countOfOutboundRecordsWithReportedErrors = 0;
            if(!errorStatusMap.isEmpty()) {
                countOfOutboundRecordsWithReportedErrors = updateDABErrorStatus(errorStatusMap);
            }
            countOfOutboundRecordsWithNoReportedErrors = setAnyRemainingFlaggedOutboundRecordsToN();
            
            logger.debug(countOfOutboundRecordsWithNoReportedErrors + " merchants from outbound file NOT reporting errors. ");
            logger.debug(countOfOutboundRecordsWithReportedErrors + " merchants from outbound file are reporting errors. ");
            bufferReader.close();
        } catch (Exception e) {
            logEntry("Error in getReviewedData() ", e.toString());
        } finally {
            try {
                if(reader != null) reader.close();
                if(bufferReader != null) bufferReader.close();
            } catch(Exception e){
                logger.error("Inside finally clause of DiscoverMapAutoBoard::sendBoardingFile(): " +e.getMessage());
            }
            cleanUp();
        }
        return reviewedData.toString();
    }

    private void addToListMerchantsInError(String dbaName, StringBuilder errorCodes, long discoverMerchantNumber,
            long mesMerchantNumber, Map<Long, String> errorStatusMap) {
        final String maskingChars = "xxxxxx";

        try {
            if (mesMerchantNumber > 0) {
                String errorString = "";
                if (errorStatusMap.containsKey(mesMerchantNumber)) {
                    errorString = errorStatusMap.get(mesMerchantNumber) + "," + errorCodes.toString();
                } else {
                    errorString = errorCodes.toString();
                }
                errorStatusMap.put(mesMerchantNumber, errorString);
            } else {
                String dmn = Long.toString(discoverMerchantNumber).substring(0, 6) + maskingChars + Long
                        .toString(discoverMerchantNumber).substring(Long.toString(discoverMerchantNumber).length() - 4);
                logger.debug("Unable to locate MeS Merchant Number for Discover Merchant : " + dmn + " -- " + dbaName);
            }
        } catch (Exception e) {
            logEntry("Error in addToListMerchantsInError() ", e.toString());
        }
    }

    private int setAnyRemainingFlaggedOutboundRecordsToN() {
        PreparedStatement ps = null;
        int rowCount         = 0;
        
        try {
            logger.debug("Inside setAnyRemainingFlaggedOutboundRecordsToN... ");
            ps = con.prepareStatement(SQL_SET_DAB_RECORD_ERRORS_RECEIVED_N);
            rowCount = ps.executeUpdate();
        } catch (SQLException e) {
            log.error("Exception in DiscoverMapAutoBoard:setAnyRemainingFlaggedOutboundRecordsToN"+e.getMessage());
        }
        finally
        {
          try{ 
              if(ps != null) ps.close(); 
          } catch( Exception e ) {
              log.error("Inside finally clause  "+e.getMessage());
           }
        }
        return rowCount;
    }

    private int updateDABErrorStatus(Map<Long, String> errorStatusMap) {
        logger.info("Inside updateDABErrorStatus method... ");
        PreparedStatement ps = null;
        int rowsUpdated      = 0;
        
        try {
            if (errorStatusMap.size() < 1) {
                return rowsUpdated;
            }

            ps = con.prepareStatement(SQL_UPDATE_DAB_RECORD_ERROR_STATUS);
            Set<Entry<Long, String>> set = errorStatusMap.entrySet();
            Iterator<Entry<Long, String>> iterator = set.iterator();

            while (iterator.hasNext()) {
                Map.Entry<Long, String> mentry = iterator.next();
                String errors = mentry.getValue().substring(0, Math.min(mentry.getValue().length(), 50));
                ps.setString(1, errors);
                ps.setLong(2, mentry.getKey());
                ps.addBatch();
            }

            int[] count = ps.executeBatch();
            rowsUpdated = count.length;
        } catch (SQLException e) {
            log.error("Exception in DiscoverMapAutoBoard:updateDABErrorStatus  " + e.getMessage());
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (Exception e) {
                log.error("Inside finally clause  " + e.getMessage());
            }
        }
        return rowsUpdated;
    }
    
    private long getMerchantNumberFromDmdsnum(long discoverMerchantNumber) {
        PreparedStatement ps          = null;
        ResultSet         rs          = null;
        long merchNumber = 0L;

        try {
            ps = con.prepareStatement(SQL_GET_MERCHANT_NUM_FROM_DMSDSNUM);
            ps.setLong(1, discoverMerchantNumber);
            rs = ps.executeQuery();

            if (rs.next()) {
                merchNumber = rs.getLong("merchantNumber");
            }
        } catch (Exception e) {
            logEntry("Error in getMerchantNumberFromDmdsnum() - discoverMerchantNumber: " + discoverMerchantNumber,
                    e.toString());
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                log.error("Inside finally clause " + e.getMessage());
            }
        }

        return merchNumber;
    }
    
    private String getErrorDescription(String errorCode) {
        PreparedStatement ps    = null;
        ResultSet rs            = null;
        String errorDescription = "";

        try {
            ps = con.prepareStatement(SQL_GET_ERROR_DESCRIPTION);
            ps.setString(1, errorCode);
            rs = ps.executeQuery();

            if (rs.next()) {
                errorDescription = rs.getString("description");
            }

        } catch (Exception e) {
            logEntry("Error in getErrorDescription() - errorCode: " + errorCode, e.toString());
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                log.error("Inside finally clause " + e.getMessage());
            }
        }
        
        return ("<" + errorCode + ">" + (errorDescription == null ? "" : errorDescription));
    }
    
    /**
     * @param args
     */
    public static void main(String[] args){
        DiscoverMapAutoBoard autoBoard = null;
        try {
            autoBoard = new DiscoverMapAutoBoard();
            
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST,
                        MesDefaults.DK_DISCOVER_MAP_PROFILE_USER,
                        MesDefaults.DK_DISCOVER_MAP_PROFILE_PW,
                        MesDefaults.DK_DISCOVER_MAP_PROFILE_PATH
                });
            }
            
            autoBoard.testMode = true;
            
            if(args.length == 0){
                logger.info("Command line execution with no arguments - arg set to SEND and sendBoardingFile() will be called from execute()");
                //to send discover reg file
                autoBoard.setEventArgs(SEND);
                autoBoard.execute();
            } else if(args.length > 0){
                if (args[0].equalsIgnoreCase(RECEIVE)) {
                    autoBoard.setEventArgs(RECEIVE);
                    
                    if (args.length == 1) {
                        logger.info("Command line execution with RECEIVE arguments - retrieveResponseFile() will be called from execute()");
                        autoBoard.execute();
                    } else {
                        logger.info("Command line execution with RECEIVE and file name as arguments - getReviewedData(fileName) is called");
                        // calls this method when at least 2 arguments are passed...
                        autoBoard.getReviewedData(args[1]);
                    }
                    
                } else {
                    logger.info("Command line execution with SEND and processSequence arguments - arg set to SEND and sendBoardingFile() will be called from execute()");
                    logger.info("processSequence #: "+ args[1]);
                    // to rebuild previous discover reg file
                    autoBoard.setEventArgs(SEND);
                    autoBoard.processSequence = Integer.parseInt(args[1]);

                    if (args.length > 1 && args[2].equalsIgnoreCase("1")) {
                        autoBoard.testMode = false;
                    }

                    autoBoard.execute();
                }
            }
            
            Runtime.getRuntime().exit(0);
            
        } catch(Exception e){
            logger.error("Exception in DiscoverMapAutoBoard::main(): " +e.getMessage());
        }
        
    }
    
    private static final String SQL_GET_MERCHANT_NUM_FROM_DMSDSNUM = "select merchant_number as merchantNumber "
            + " from  mif where dmdsnum = ? and nvl(dmacctst,'A') not in ( 'D','C','B' ) ";
    
    private static final String SQL_GET_ERROR_DESCRIPTION = "select description from reject_type "
            + " where id = ? and type = 'DS' ";

    private static final String SQL_UPDATE_DAB_RECORD_ERROR_STATUS = "update discover_map_auto_board set errors_received = 'Y', error_codes = ? where merchant_number = ? and errors_received = 'O'";
    
    private static final String SQL_SET_DAB_RECORD_ERRORS_RECEIVED_N = "update discover_map_auto_board set errors_received = 'N' where errors_received = 'O'";
    
    private static final String SQL_GET_DISCOVER_AUTOBOARD = "select count(1) from discover_map_auto_board where process_sequence = 0";
    
    private static final String SQL_GET_DAB_PROCESS_SEQUENCE = "select wf_auto_board_seq.nextval from dual";
    
    private static final String SQL_UPDATE_DAB_PROCESS_SEQUENCE = "update discover_map_auto_board set process_sequence = ?, load_filename= ?, errors_received = 'O' where process_sequence = 0";
    
    private static final String SQL_GET_DISCOVER_REG_DETAILS =
            "select distinct mf.merchant_number                                                                 as merch_number,                  "
          + "       CASE                                                                                                                          "
          + "       WHEN (dab.action = '02' AND exists (SELECT db.MERCHANT_NUMBER FROM discover_map_auto_board db                                 "
          + "       WHERE dab.merchant_number = db.merchant_number AND db.ACTION = '01'                                                           "
          + "       AND (db.LOAD_FILENAME IS NULL OR db.ERRORS_RECEIVED = 'Y') AND db.PROCESS_SEQUENCE <> ?)                                                                  "
          + "       AND not exists (SELECT db.MERCHANT_NUMBER FROM discover_map_auto_board db                                                     "
          + "       WHERE dab.merchant_number = db.merchant_number AND db.ACTION = '01'                                                           "
          + "       AND db.ERRORS_RECEIVED   = 'N' AND db.PROCESS_SEQUENCE <> ?)) THEN '01'                                                       "
          + "       ELSE dab.action  END                                                                        as record_type,                   "
          + "       dab.action                                                                                  as old_record_type,               "
          + "       to_char(mb.ds_acquirer_id)                                                                  as acquirer_id,                   "
          + "       '01'                                                                                        as merchant_type,                 "
          + "       mf.dmdsnum                                                                                  as discover_merchant_number,      "
          + "       mf.dba_name                                                                                 as dba_name,                      "
          + "       decode(mf.bank_number,3858, mf.addr1_line_1,mf.dmaddr)                                      as dba_address_1,                 "
          + "       decode(mf.bank_number,3858, mf.addr1_line_2,mf.address_line_3)                              as dba_address_2,                 "
          + "       decode(mf.bank_number,3858, mf.city1_line_4,mf.dmcity)                                      as dba_city,                      "
          + "       decode(mf.bank_number,3858, mf.state1_line_4,mf.dmstate)                                    as dba_state,                     "
          + "       decode(mf.bank_number,3858, "
          + "           rtrim(ltrim(substr(rtrim(ltrim(mf.zip1_line_4)),1,5))),"
          + "           rtrim(ltrim(substr(rtrim(ltrim(mf.dmzip)),1,5))))                                       as dba_zip,                       "
          + "       decode(mf.dmstate,'PR','PRI','USA')                                                         as dba_country,                   "
          + "       mf.phone_1                                                                                  as dba_phone,                     "
          + "       ltrim(rtrim(substr(mf.owner_name, 1, instr(mf.owner_name,' '))))                            as principal_first_name,          "
          + "       ltrim(rtrim(substr(mf.owner_name, instr(mf.owner_name,' '),length(mf.owner_name))))         as principal_last_name,           "
          + "       nvl(bo.busowner_title,'OWNER')                                                              as principal_title,               "
          + "       ba.address_line1                                                                            as principal_address_1,           "
          + "       ba.address_line2                                                                            as principal_address_2,           "
          + "       decode(ba.address_line1,null,null,ba.address_city)                                          as principal_city,                "
          + "       decode(ba.address_line1,null, null,ba.countrystate_code)                                    as principal_state,               "
          + "       rtrim(ltrim(substr(decode(ba.address_line1,null, null,ltrim(rtrim(ba.address_zip))),1,5)))  as principal_zip,                 "
          + "       decode(ba.address_line1,null, null,decode(ba.countrystate_code,'PR', 'PRI','USA'))          as principal_country,             "
          + "       nvl2(bo.busowner_ssn,lpad(bo.busowner_ssn,9,'0'),'000000000')                               as principal_ssn,                 "
		  + "       nvl2(mf.federal_tax_id,lpad(mf.federal_tax_id,9,'0'),'000000000')                           as tax_id_number,                 "
          + "       mf.fdr_corp_name                                                                            as legal_name,                    "
          + "       mf.addr1_line_1                                                                             as business_address_1,            "
          + "       mf.addr1_line_2                                                                             as business_address_2,            "
          + "       mf.city1_line_4                                                                             as business_city,                 "
          + "       mf.state1_line_4                                                                            as business_state,                "
          + "       rtrim(ltrim(substr(ltrim(rtrim(mf.zip1_line_4)),1,5)))                                      as business_zip,                  "
          + "       decode(mf.state1_line_4,'PR', 'PRI','USA')                                                  as business_country,              "
          + "       mf.sic_code                                                                                 as mcc,                           "
          + "       decode(mr.bustype_code, "
          + "              2, 'C', "
          + "              3, 'P', "
          + "              4, 'C', "
          + "              5, 'N', "
          + "              6, 'N', "
          + "              7, 'G', "
          + "              8, 'L', "
          + "              'S')                                                                                 as business_type_desc,            "
          + "       to_char(dab.date_created,'yyyy-mm-dd')                                                      as merchant_status_date,          "
          + "       decode(mf.dmacctst, "
          + "              null, '0001', "
          + "              'Z',  '0001', "
          + "               '1009')                                                                             as merchant_status,               "
          + "       null                                                                                        as merchant_website_address,      "
          + "       mr.merch_email_address                                                                      as merchant_email_address,        "
          + "       '05'                                                                                        as pos_device_download_indicator, "
          + "       2                                                                                           as network_indicator              "
          + "       from       discover_map_auto_board   dab,"
          + "                  mif                       mf, "
          + "                  mbs_banks                 mb, "
          + "                  merchant                  mr, "
          + "                  merchcontact              mc, "
          + "                  businessowner             bo, "
          + "                  address                   ba  "
          + "       where      dab.process_sequence = ?      "
          + "                  and dab.merchant_number = mf.merchant_number "
          + "                  and mf.bank_number = mb.bank_number "
          + "                  and mf.merchant_number = mr.merch_number "
          + "                  and mr.app_seq_num = mc.app_seq_num(+) "
          + "                  and mr.app_seq_num = bo.app_seq_num(+) "
          + "                  and bo.busowner_num(+) = 1 "
          + "                  and mr.app_seq_num = ba.app_seq_num(+) "
          + "                  and ba.addresstype_code(+) = 4";


}