/*@lineinfo:filename=PMMonitoringEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/PMMonitoringEvent.sqlj $

  Description:

  Last Modified By   : $Author: swaxman $
  Last Modified Date : $Date: 2014-08-08 $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;
import javax.mail.Message.RecipientType;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.PropertiesFile;
import com.mes.support.SMTPMail;
import sqlj.runtime.ResultSetIterator;

public class PMMonitoringEvent extends EventBase
{
	private static final String[] fileNames = {"256s3941_001.dat", "mddf3943_002.dat", "mddf3941_002.dat", "cdf3942_001.dat", 
    		  "cdf3003_001.dat", "mddf3003_001.dat", "mddf9999_001.dat", "cdf3941_002.dat", "mddf3943_001.dat",
    		  "mddf3941_001.dat", "mddf3942_001.dat", "mddf3858_001.dat"};
	
	private static String DEFAULT_EMAIL_HOST;
	private static Logger log = Logger.getLogger(PMMonitoringEvent.class);

	public boolean TestMode = false;

	private Vector queryResults = new Vector();
	private String runType = "0";
	private HashMap constraints = new HashMap();
	private HashMap acknowledgements = new HashMap();
	private HashMap timings = new HashMap();
	private double var = .25;
	private double secs_per_rec = .05;
	private String senderEmailAddress = "DevOpsTeam@merchante-solutions.com";
	private String[] devOpsNotices = new String[]{};
	private String[] devOpsAlerts = new String[]{};
	private int pgAuthCount = 150;
	private int tridentCaptureCount = 700;
	private int riskScanningCount = 600;
	private int visaAuthCount = 6000;

	public boolean allPass;
	public boolean nullConnectionResult;
	public boolean slowVisaAuthResult;
	public boolean extractionsResult;
	public boolean settlementErrorsResult;
	public boolean slowPaymentAuthResult;
	public boolean tridentCaptureResult;
	public boolean riskScanningResult;
	
	public PMMonitoringEvent() {
	  try {
	    String devOpsNoticesList = MesDefaults.getString(MesDefaults.PM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST);
	    devOpsNotices = devOpsNoticesList.split(",");

	    String devOpsAlertsList = MesDefaults.getString(MesDefaults.PM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST);
	    devOpsAlerts = devOpsAlertsList.split(",");
	  } catch (Exception e) {
	    logEntry("PMMonitoringEvent() constructor - Set the devops notices' emails and alerts' emails", e.toString());
	  }
	}
	
	public void getNullConnectionResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:90^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '20'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '20'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^4*/
			nullConnectionResult = false;
			String[] cur = new String[2];
			cur[0] = ""; 
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:98^4*/

//  ************************************************************
//  #sql [Ctx] it = { select log_time
//  					from    java_log 
//  					where   log_time >= sysdate- 5/60/24
//  					and log_message = 'java.sql.SQLException: found null connection context' 
//  					and server_name in ('mbsevents', 'mainevents')
//  					order by log_time desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select log_time\n\t\t\t\t\tfrom    java_log \n\t\t\t\t\twhere   log_time >= sysdate- 5/60/24\n\t\t\t\t\tand log_message = 'java.sql.SQLException: found null connection context' \n\t\t\t\t\tand server_name in ('mbsevents', 'mainevents')\n\t\t\t\t\torder by log_time desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^5*/

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] = "<span style='background-color:green'>No results returned.</span>";
				nullConnectionResult = true;
			} else {
				nullConnectionResult = false;
				cur[1] += rs.getString("log_time");
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:122^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  					creation_date,
//  					text,
//  					achnowledgement,
//  					pass,
//  					error_type
//  				)
//  				values
//  				(
//  					sysdate,
//  					:cur[1],
//  					'N',
//  					:nullConnectionResult ? "Y" : "N",
//  					20
//  				)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4505 = cur[1];
 String __sJT_4506 = nullConnectionResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\tcreation_date,\n\t\t\t\t\ttext,\n\t\t\t\t\tachnowledgement,\n\t\t\t\t\tpass,\n\t\t\t\t\terror_type\n\t\t\t\t)\n\t\t\t\tvalues\n\t\t\t\t(\n\t\t\t\t\tsysdate,\n\t\t\t\t\t :1 ,\n\t\t\t\t\t'N',\n\t\t\t\t\t :2 ,\n\t\t\t\t\t20\n\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4505);
   __sJT_st.setString(2,__sJT_4506);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^4*/
			if(!nullConnectionResult) {
				sendAlertEmail(!nullConnectionResult, "One or More Null Connection Context Errors.");
			}
		} catch (Exception e) {
			logEntry("getNullConnectionResult()", e.toString());
		}
	}

	public void getSlowVisaAuthResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:150^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '21'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '21'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^4*/
			slowVisaAuthResult = false;
			String[] cur = new String[2];
			cur[0] = "";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:158^4*/

//  ************************************************************
//  #sql [Ctx] it = { select count(*) count
//  					from trident_detail_lookup tdl
//  					where mesdb_timestamp > sysdate-0.125
//  					and auth_rec_id = 0
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select count(*) count\n\t\t\t\t\tfrom trident_detail_lookup tdl\n\t\t\t\t\twhere mesdb_timestamp > sysdate-0.125\n\t\t\t\t\tand auth_rec_id = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^5*/

			rs = it.getResultSet();

			Calendar now = Calendar.getInstance();
			if(!rs.next()) {
				cur[1] = "<span style='background-color:red'>No results returned.</span>";
			} else {
				slowVisaAuthResult = rs.getInt("count") < visaAuthCount;
				cur[1] = rs.getInt("count") + "";
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:180^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:slowVisaAuthResult ? "Y" : "N",
//  								21
//  								)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4507 = cur[1];
 String __sJT_4508 = slowVisaAuthResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t21\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4507);
   __sJT_st.setString(2,__sJT_4508);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^4*/
			if(!slowVisaAuthResult) {
				sendAlertEmail(!slowVisaAuthResult, "Visa Slow Auth Link Count Out of Bounds.");
			}
		} catch (Exception e) {
			logEntry("getSlowVisaAuthResult()", e.toString());
		}
	}

	public void getExtractionsResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:208^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '22'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '22'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^4*/
			String errorText = "";
			HashMap files = new HashMap();
			ArrayList passes = new ArrayList();
			extractionsResult = true;
			Calendar now = Calendar.getInstance();
			Calendar nineteenFifty = Calendar.getInstance();
			nineteenFifty.set(Calendar.HOUR_OF_DAY, 19);
			nineteenFifty.set(Calendar.MINUTE, 50);
			
			int offset = now.after(nineteenFifty) ? 1 : 0;
			Calendar today = Calendar.getInstance();
			today.add(Calendar.DAY_OF_MONTH, offset == 1 ? 0 : -1);
			String[] cur = new String[2];
			cur[0] = " MC - Generally 5 in a day from Monday - Friday at approx 4:30 AM, 6:30 AM, 4:30 PM. 7:30 PM, 10:30 PM " +
					" -- 2 on Saturday  at approx 4:30 AM, 6:30 AM  --  3 on Sunday.at approx 3:00 / 3:30 PM. 7:30 PM, 10:30 PM";
			cur[1] = " ";
			/*@lineinfo:generated-code*//*@lineinfo:228^4*/

//  ************************************************************
//  #sql [Ctx] it = { with details as ((select  'VS' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,
//  					min(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt
//  					from    visa_settlement       where   batch_date >= trunc(sysdate  + :offset  )  --   -1)
//  					group by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)
//  					) union
//  					(select  'MC' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,
//  					min(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt
//  					from    mc_settlement         where   batch_date >= trunc(sysdate + :offset  )  --   -1)
//  					group by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)
//  					) union
//  					(select  'AM' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,
//  					min(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt
//  					from    amex_settlement       where   batch_date >= trunc(sysdate + :offset  )  --   -1)
//  					group by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)
//  					) union
//  					(select  'DS' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,
//  					min(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt
//  					from    discover_settlement   where   batch_date >= trunc(sysdate  + :offset )  --   -1)
//  					group by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)
//  					) )
//  					select batch_date, load_filename, load_file_id, --output_filename,
//  					min(begin_time) as begintime, max(end_time) as endtime, round((max(end_time)-min(begin_time))*60*60*24,0) as t_secs,
//  					sum(rec_cnt) as count, round(((max(end_time)-min(begin_time))*60*60*24)/sum(rec_cnt),3) as secs_per_rec,test_flag
//  					from details
//  					group by batch_date, load_filename, load_file_id, test_flag--, output_filename
//  					order by batch_date desc, begintime desc,load_file_id,endtime				
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "with details as ((select  'VS' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,\n\t\t\t\t\tmin(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt\n\t\t\t\t\tfrom    visa_settlement       where   batch_date >= trunc(sysdate  +  :1   )  --   -1)\n\t\t\t\t\tgroup by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)\n\t\t\t\t\t) union\n\t\t\t\t\t(select  'MC' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,\n\t\t\t\t\tmin(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt\n\t\t\t\t\tfrom    mc_settlement         where   batch_date >= trunc(sysdate +  :2   )  --   -1)\n\t\t\t\t\tgroup by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)\n\t\t\t\t\t) union\n\t\t\t\t\t(select  'AM' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,\n\t\t\t\t\tmin(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt\n\t\t\t\t\tfrom    amex_settlement       where   batch_date >= trunc(sysdate +  :3   )  --   -1)\n\t\t\t\t\tgroup by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)\n\t\t\t\t\t) union\n\t\t\t\t\t(select  'DS' as card_type, batch_date, load_filename,  load_file_id, test_flag, --substr(output_filename,3,23) as output_filename,\n\t\t\t\t\tmin(mesdb_timestamp) as begin_time, max(mesdb_timestamp) as end_time , count(1) as rec_cnt\n\t\t\t\t\tfrom    discover_settlement   where   batch_date >= trunc(sysdate  +  :4  )  --   -1)\n\t\t\t\t\tgroup by batch_date, load_filename , load_file_id, test_flag--, substr(output_filename,3,23)\n\t\t\t\t\t) )\n\t\t\t\t\tselect batch_date, load_filename, load_file_id, --output_filename,\n\t\t\t\t\tmin(begin_time) as begintime, max(end_time) as endtime, round((max(end_time)-min(begin_time))*60*60*24,0) as t_secs,\n\t\t\t\t\tsum(rec_cnt) as count, round(((max(end_time)-min(begin_time))*60*60*24)/sum(rec_cnt),3) as secs_per_rec,test_flag\n\t\t\t\t\tfrom details\n\t\t\t\t\tgroup by batch_date, load_filename, load_file_id, test_flag--, output_filename\n\t\t\t\t\torder by batch_date desc, begintime desc,load_file_id,endtime";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,offset);
   __sJT_st.setInt(2,offset);
   __sJT_st.setInt(3,offset);
   __sJT_st.setInt(4,offset);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^5*/

			rs = it.getResultSet();
			Calendar due;
			String[] time;

			if(!rs.next()) {
				cur[1] = "No records found.";
				time = ((String)timings.get("256s3941_001.dat")).split(":");
				due = Calendar.getInstance();
				due.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
				due.set(Calendar.MINUTE, Integer.parseInt(time[1]));
				extractionsResult = (due.get(Calendar.HOUR_OF_DAY) >= 20 ? (offset == 1 && now.before(due)) : (offset == 1 || now.before(due)));
			} else {
				String name;
				do
				{
					name = rs.getString("load_filename");
					name = name.substring(0, name.indexOf("_")) + name.substring(name.lastIndexOf("_"));
					files.put(name, new String[] {name, rs.getString("begintime"), rs.getString("endtime"), 
						rs.getString("count"), rs.getString("secs_per_rec")});
					cur[1] += name + "," + 
						rs.getString("count") + "," + rs.getString("secs_per_rec") + ";";
				} while(rs.next());
				String[] file;
				int[] cons;
				int count;
				int len = fileNames.length;
				int dow = today.get(Calendar.DAY_OF_WEEK) - 1;
				for(int i = 0; i < len; i++) {
					name = fileNames[i];
					time = ((String)timings.get(name)).split(":");
					due = Calendar.getInstance();
					due.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
					due.set(Calendar.MINUTE, Integer.parseInt(time[1]));
					if(files.containsKey(name)) {
						file = (String[]) files.get(name);
						cons = (int[]) constraints.get(name);
						count = Integer.parseInt(file[3]);
						if(Double.parseDouble(file[4]) > secs_per_rec && !((boolean)acknowledgements.get(name))) {
							extractionsResult = false;
							errorText += name + ": secs_per_rec out of bounds\n";
						}
						else if((count < (cons[dow] * (1 - var)) && (due.get(Calendar.HOUR_OF_DAY) >= 20 ? 
								(offset == 0 || now.after(due)) : (offset == 0 && now.after(due)))) && !((boolean)acknowledgements.get(name))) {
							extractionsResult = false;
							errorText += name + ": count out of bounds\n";
						}
					}
					else if((due.get(Calendar.HOUR_OF_DAY) >= 20 ? (offset == 0 || now.after(due)) : (offset == 0 && now.after(due))) 
							&& !((boolean)acknowledgements.get(name))) {
						extractionsResult = false;
						errorText += name + ": not found";
					}
				}
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:317^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:extractionsResult ? "Y" : "N",
//  								22
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4509 = cur[1];
 String __sJT_4510 = extractionsResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t22\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4509);
   __sJT_st.setString(2,__sJT_4510);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^6*/
			if(!extractionsResult) {
				sendAlertEmail(!extractionsResult, "One or More Extraction Issues.\n" + errorText);
			}
		} catch (Exception e) {
			logEntry("getExtractionsResult()", e.toString());
		}
	}

	public void getSettlementErrorsResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:346^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '23'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '23'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:349^4*/
			settlementErrorsResult = false;
			String[] cur = new String[2];
			cur[0] = "";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:354^4*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//  					from    java_log 
//  					where   log_time >= sysdate - 30/60/24
//  					        and (log_source like 'com.mes.startup.RiskBatchScanner%'
//  					            or (log_source like 'com.mes.settlement%' and log_message not like 'cannot create tran card type bitfield%'))
//  					order by log_time desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n\t\t\t\t\tfrom    java_log \n\t\t\t\t\twhere   log_time >= sysdate - 30/60/24\n\t\t\t\t\t        and (log_source like 'com.mes.startup.RiskBatchScanner%'\n\t\t\t\t\t            or (log_source like 'com.mes.settlement%' and log_message not like 'cannot create tran card type bitfield%'))\n\t\t\t\t\torder by log_time desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:362^5*/

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] = "<span style='background-color:green'>No results returned.</span>";
				settlementErrorsResult = true;
			} else {
				settlementErrorsResult = false;
				cur[1] += rs.getString("log_time");
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:378^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:settlementErrorsResult ? "Y" : "N",
//  								23
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4511 = cur[1];
 String __sJT_4512 = settlementErrorsResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t23\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4511);
   __sJT_st.setString(2,__sJT_4512);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^6*/
			if(!settlementErrorsResult) {
				sendAlertEmail(!settlementErrorsResult, "One or More Settlement or Risk Scanning Errors.");
			}
		} catch (Exception e) {
			logEntry("getSettlementErrorsResult()", e.toString());
		}
	}

	public void getSlowPaymentAuthResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:407^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '24'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '24'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^4*/
			String[] cur = new String[2];
			cur[0] = "AMEX - 1 everyday from Monday - Saturday at approx 6:30 AM - There is NONE on Sunday";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:414^4*/

//  ************************************************************
//  #sql [Ctx] it = { select count(*) count
//  					from trident_capture_api
//  					where mesdb_timestamp >sysdate-.5 
//  					      and mesdb_timestamp < sysdate-.005 -- not the new stuff!
//  					      and merchant_number not in ( 941000099999, 941000106713, 941000073518, 941000086152,941000060005, 941000064134, 941000116856, 941000107405 )
//  					      and nvl(auth_rec_id,0) in (-1,0)
//  					      and auth_code is not null
//  					      and debit_credit_indicator = 'D'
//  					      and currency_code in ( '840', 'USD' )
//  					      and nvl(test_flag,'Y') = 'N'
//  					      and server_name <> 'Acculynk'
//  					      and dba_city <> 'MINNETONKA'
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select count(*) count\n\t\t\t\t\tfrom trident_capture_api\n\t\t\t\t\twhere mesdb_timestamp >sysdate-.5 \n\t\t\t\t\t      and mesdb_timestamp < sysdate-.005 -- not the new stuff!\n\t\t\t\t\t      and merchant_number not in ( 941000099999, 941000106713, 941000073518, 941000086152,941000060005, 941000064134, 941000116856, 941000107405 )\n\t\t\t\t\t      and nvl(auth_rec_id,0) in (-1,0)\n\t\t\t\t\t      and auth_code is not null\n\t\t\t\t\t      and debit_credit_indicator = 'D'\n\t\t\t\t\t      and currency_code in ( '840', 'USD' )\n\t\t\t\t\t      and nvl(test_flag,'Y') = 'N'\n\t\t\t\t\t      and server_name <> 'Acculynk'\n\t\t\t\t\t      and dba_city <> 'MINNETONKA'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^5*/

			rs = it.getResultSet();

			slowPaymentAuthResult = false;
			Calendar sixThirty = Calendar.getInstance();
			sixThirty.set(Calendar.HOUR_OF_DAY, 6);
			sixThirty.set(Calendar.MINUTE, 30);
			if(!rs.next()) {
				cur[1] = "<span style='background-color:red'>No results returned.</span>";
			} else {
					slowPaymentAuthResult = rs.getInt("count") <= pgAuthCount;
					cur[1] = rs.getInt("count") + "";
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:447^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:slowPaymentAuthResult ? "Y" : "N",
//  								24
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4513 = cur[1];
 String __sJT_4514 = slowPaymentAuthResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t24\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4513);
   __sJT_st.setString(2,__sJT_4514);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^6*/
			if(!slowPaymentAuthResult) {
				sendAlertEmail(!slowPaymentAuthResult, "Slow Payment Gateway Auth Link Count Out of Bounds.");
			}
		} catch (Exception e) {
			logEntry("getSlowPaymentAuthResult()", e.toString());
		}
	}

	public void getTridentCaptureResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:476^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '25'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '25'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:479^4*/
			tridentCaptureResult = false;
			String[] cur = new String[2];
			cur[0] = ""; 
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:484^4*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(*) count
//  					from    trident_capture 
//  					where   mesdb_timestamp > trunc(sysdate) 
//  					    and ach_sequence = 0 
//  					    and bank_number is not null
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(*) count\n\t\t\t\t\tfrom    trident_capture \n\t\t\t\t\twhere   mesdb_timestamp > trunc(sysdate) \n\t\t\t\t\t    and ach_sequence = 0 \n\t\t\t\t\t    and bank_number is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:491^5*/
			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] += "<span style='background-color:red'>No results returned.</span>";
				tridentCaptureResult = false;
			} else {
				tridentCaptureResult = rs.getInt("count") <= tridentCaptureCount;
				cur[1] = rs.getInt("count") + "";
			}

			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:507^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:tridentCaptureResult ? "Y" : "N",
//  								25
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4515 = cur[1];
 String __sJT_4516 = tridentCaptureResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t25\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4515);
   __sJT_st.setString(2,__sJT_4516);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^6*/
			if(!tridentCaptureResult) {
				sendAlertEmail(!tridentCaptureResult, "Failure to sweep from trident_capture to mbs_batches.");
			}
		} catch (Exception e) {
			logEntry("getTridentCaptureResult()", e.toString());
		}
	}

	public void getRiskScanningResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:536^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '26'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '26'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.PMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:539^4*/
			riskScanningResult = false;
			String[] cur = new String[2];
			cur[0] = "";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:544^4*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(*) count
//  					from mbs_batches
//  					where merchant_batch_date >= sysdate -1 
//  					  and load_file_id = -1
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(*) count\n\t\t\t\t\tfrom mbs_batches\n\t\t\t\t\twhere merchant_batch_date >= sysdate -1 \n\t\t\t\t\t  and load_file_id = -1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.PMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.PMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:550^5*/
			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] += "<span style='background-color:red'>No results returned.</span>";
				riskScanningResult = false;
			} else {
				riskScanningResult = rs.getInt("count") <= riskScanningCount;
				cur[1] = rs.getInt("count") + "";
			}

			queryResults.add(cur);

			rs.close();
			it.close();
			
			/*@lineinfo:generated-code*//*@lineinfo:566^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:riskScanningResult ? "Y" : "N",
//  								26
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4517 = cur[1];
 String __sJT_4518 = riskScanningResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t26\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.PMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4517);
   __sJT_st.setString(2,__sJT_4518);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:584^6*/
			if(!riskScanningResult) {
				sendAlertEmail(!riskScanningResult, "Failure to complete risk scanning.");
			}
		} catch (Exception e) {
			logEntry("getRiskScanningResult()", e.toString());
		}
	}

	/*
	 ** METHOD public void getData()
	 **
	 */
	public synchronized void getData()
	{
		ResultSetIterator it        = null;
		ResultSet         rs        = null;
		boolean           restrict  = false;

		try
		{
			loadProps();
			queryResults.clear();
			allPass = false;
			connect();

			if(runType.equals("1")) {
				getNullConnectionResult(it, rs);
			}
			else if(runType.equals("2")) {
				getSlowVisaAuthResult(it, rs);
			}
			else if(runType.equals("3")) {
				getExtractionsResult(it, rs);
			}
			else if(runType.equals("4")) {
				getSettlementErrorsResult(it, rs);
				getSlowPaymentAuthResult(it, rs);
				getTridentCaptureResult(it, rs);
				getRiskScanningResult(it, rs);
			}
			else {
				getNullConnectionResult(it, rs);
				getSlowVisaAuthResult(it, rs);
				getExtractionsResult(it, rs);
				getSettlementErrorsResult(it, rs);
				getSlowPaymentAuthResult(it, rs);
				getTridentCaptureResult(it, rs);
				getRiskScanningResult(it, rs);
				allPass();
				if(allPass) {
					sendAlertEmail(!allPass, "");
				}
			}

		}
		catch(Exception e)
		{
			logEntry("getData()", e.toString());
		}
		finally
		{
			try { rs.close(); } catch(Exception e) {}
			try { it.close(); } catch(Exception e) {}
			cleanUp();
		}
	}

	/**
	 * METHOD allPass
	 * 
	 * Sets the allPass variable
	 */
	public void allPass() {
		allPass = slowPaymentAuthResult && riskScanningResult && settlementErrorsResult && extractionsResult 
				&& slowVisaAuthResult && tridentCaptureResult && nullConnectionResult;
	}

	public void sendAlertEmail(boolean alert, String emailContext) {
		String  body             = "";
		try {
			// create a mailer object with the host parameters in MesDefaults
			String emailHost = MesDefaults.getString(MesDefaults.DK_SMTP_HOST);
			SMTPMail mailer = new SMTPMail(emailHost);
			mailer.setFrom(senderEmailAddress);

			if (alert){ 
				int count = devOpsAlerts.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsAlerts[i]);
				}
					mailer.setSubject("PM Monitor Alert");
			}
			else {
				int count = devOpsNotices.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsNotices[i]);
				}
					mailer.setSubject("PM Monitor Notification");                           
			}

			if (!alert) {
				body += "\n";
				body += "------------------------------------------------------------" + "\n";
				Calendar now = Calendar.getInstance();
				Calendar nineteenFifty = Calendar.getInstance();
				nineteenFifty.set(Calendar.HOUR_OF_DAY, 19);
				nineteenFifty.set(Calendar.MINUTE, 50);
				Calendar fifty = Calendar.getInstance();
				fifty.set(Calendar.HOUR_OF_DAY, 0);
				fifty.set(Calendar.MINUTE, 50);
				if(now.before(nineteenFifty) && now.after(fifty)) {
					body += "All 12 files processed without error.\n";
				}
				else {
					body += "No Current Issues with PM Monitoring.\n";
				}
				body += "------------------------------------------------------------" + "\n";
				mailer.setText(body);
				mailer.send();
			}
			else {
				body += "\n";
				body += "One or more errors were found by the PM monitoring process.\n";
				body += emailContext;
				body += "\n";
				mailer.setText(body);
				mailer.send();
			}
		} catch(Exception e) {
			logEntry("sendEmail()", e.toString());
		}
	}
	
  private void loadProps()
  {
    log.debug("in loadProps... ");
    try
    {
      PropertiesFile props = new PropertiesFile("monitoring.properties");
      
      DEFAULT_EMAIL_HOST = props.getString("DEFAULT_EMAIL_HOST");
      senderEmailAddress = props.getString("SENDER_EMAIL_ADDRESS", senderEmailAddress);
      devOpsNotices = props.getString("DEV_OPS_NOTICES_PM").split(";");
      devOpsAlerts = props.getString("DEV_OPS_ALERTS_PM").split(";");
      var = props.getDouble("VARIANCE", var);
      secs_per_rec = props.getDouble("SECS_PER_REC", secs_per_rec);
      pgAuthCount = props.getInt("PG_AUTH_COUNT", pgAuthCount);
      tridentCaptureCount = props.getInt("TRIDENT_CAPTURE_COUNT", tridentCaptureCount);
      riskScanningCount = props.getInt("RISK_SCANNING_COUNT", riskScanningCount);
      visaAuthCount = props.getInt("VISA_AUTH_COUNT", visaAuthCount); 
      int[] cons = new int[7];
      String[] temp = new String[7];
      int len = fileNames.length;
      for(int i = 0; i < len; i++) {
    	  temp = new String[7];
    	  temp = props.getString(fileNames[i]).split(",");
    	  cons = new int[7];
    	  for(int j = 0; j < 7; j++) {
    		  cons[j] = Integer.parseInt(temp[j]);
    	  }
    	  constraints.put(fileNames[i], cons);
    	  acknowledgements.put(fileNames[i], props.getString(fileNames[i] + "_ack").equals("Y"));
    	  timings.put(fileNames[i], props.getString(fileNames[i] + "_time"));
      }
    }
    catch(Exception e)
    {
      logEntry("loadProps()", e.toString());
      log.error(e.getMessage());
    }
  }
  
	public boolean execute( )
	{
		boolean result = true;
		if(getEventArgCount() > 0) {
			runType = getEventArg(0);
		}
		getData();
		return( result );
	}

	public static void main(String[] args)
	{
			try {
				if (args.length > 0 && args[0].equals("testproperties")) {
					EventBase.printKeyListStatus(new String[]{
							MesDefaults.PM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST,
							MesDefaults.PM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST,
							MesDefaults.DK_SMTP_HOST
					});
				}
			} catch (Exception e) {
				log.error(e.toString());
			}

		try
		{
			PMMonitoringEvent worker = new PMMonitoringEvent();
			if(args.length > 0) {
				worker.setRunType(args[0]);;
			} else {
				worker.setRunType("0");
			}

			worker.execute();
		}
		catch(Exception e)
		{
			System.out.println("main(): " + e.toString());
			log.error(e.getMessage());
		}
	}

	public String getRunType() {
		return runType;
	}

	public void setRunType(String runType) {
		this.runType = runType.trim();
	}
}/*@lineinfo:generated-code*/