package com.mes.startup;

import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;

public class MBSEventBase extends EventBase
{
  // overload logEntry to use a different email group
  public void logEntry(String method, String message)
  {
    try
    {
      MailMessage.sendErrorEmail("Timed Event " + this.getClass().getName() + " FAIL", method + "\n" + message, MesEmails.MSG_ADDRS_MBS_ERRORS);
    }
    catch(Exception e)
    {
    }

    super.logEntry(method, message);
  }
  
}