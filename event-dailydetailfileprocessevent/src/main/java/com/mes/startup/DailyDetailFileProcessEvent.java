/*@lineinfo:filename=DailyDetailFileProcessEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DailyDetailFileProcessEvent.sqlj $

  Description:

  Last Modified By   : $Author: hmeng $
  Last Modified Date : $Date: 2015-06-12 21:33:02 -0700 (Fri, 12 Jun 2015) $
  Version            : $Revision: 23686 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.ach.AchDb;
import com.mes.ach.AchEntryData;
import com.mes.ach.AchStatementRecord;
import com.mes.clearing.utils.ClearingConstants;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.fasterfunding.FasterFundingDao;
import com.mes.startup.ProcessTable.ProcessTableEntry;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DccRateUtil;
import com.mes.tools.DccRateUtil.DccRate;
import sqlj.runtime.ResultSetIterator;

public class DailyDetailFileProcessEvent extends MBSEventBase
{
	static Logger log = Logger.getLogger(DailyDetailFileProcessEvent.class);

	public static final int PT_DISCOUNT_IC_CALC = 1;
	public static final int PT_ACH_EXTRACT = 2;

	private int ProcessType = -1;

	enum DccRecColumn {

		BASE_CC("base_currency_code"),
		BATCH_DATE("batch_date"),
		BATCH_NUMBER("batch_number"),
		CARD_TYPE("card_type"),
		COUNTER_CC("counter_currency_code"),
		DDF_DT_ID("ddf_dt_id"),
		FX_MARK_UP("fx_markup"),
		FX_CONVERSION_DATE("fx_conv_date");
		private String columnName;

		private DccRecColumn(String name) {
			this.columnName = name;
		}
	}
  
  private DccRateUtil fxRateUtil = null;
  
  private String SQL_GET_DDF_DATA = ""
          + "SELECT          DECODE(tf.merchant,NULL,dt.bank_number,mf.bank_number)                                              AS bank_number, "
          + "                dt.batch_date                                                                                       AS batch_date, "
          + "                dt.batch_number                                                                                     AS rec_id, "
          + "                dt.merchant_batch_number                                                                            AS batch_number, "
          + "                DECODE(tf.merchant,NULL,dt.merchant_account_number,tf.merchant)                                     AS merchant_number, "
          + "                (TRUNC(sysdate) + ( "
          + "                  CASE "
          + "                    WHEN mf.bank_number IN (fasterFundingBanks) "
          + "                        AND NVL(mf.suspended_days, 0) > 0 "
          + "                    THEN NVL(mf.suspended_days, 0) - 1 "
          + "                    ELSE NVL(mf.suspended_days, 0) "
          + "                END ) )                                                                                             AS post_date, "
          + "                :1                                                                                                  AS entry_desc, "
          + "                SUM(DECODE(dt.debit_credit_indicator, 'D',1,0))                                                     AS sales_count, "
          + "                SUM(DECODE(tf.amount,NULL, "
          + "                           dt.transaction_amount * DECODE(dt.debit_credit_indicator, 'D', 1, 0), "
          + "                           tf.amount * DECODE(dt.debit_credit_indicator, 'D', 1, 0)))                               AS sales_amount, "
          + "                SUM(DECODE(dt.debit_credit_indicator, 'C', 1, 0))                                                   AS credits_count, "
          + "                SUM(DECODE(tf.amount,NULL, "
          + "                           dt.transaction_amount * DECODE(dt.debit_credit_indicator, 'C', 1, 0), "
          + "                           tf.amount * DECODE(dt.debit_credit_indicator, 'C', 1, 0)))                               AS credits_amount, "
          + "                SUM( NVL(dt.discount_paid, 0) )                                                                     AS daily_discount_amount, "
          + "                SUM( NVL(dt.ic_paid, 0) )                                                                           AS daily_ic_amount, "
          + "                SUM(DECODE(dt.debit_credit_indicator, 'D', 1, -1) * "
          + "                    DECODE(tf.amount,NULL,dt.transaction_amount,tf.amount)) "
          + "                    - sum( nvl(dt.discount_paid, 0) ) "
          + "                    - sum( nvl(dt.ic_paid, 0) )                                                                     as net_amount, "
          + "                sum(decode(atsc.sponsored,'Y', "
          + "                     (decode(dt.debit_credit_indicator,'D',1,-1) * "
          + "                       decode(tf.amount,null,dt.transaction_amount,tf.amount)) "
          + "                       - (nvl(dt.discount_paid,0) + nvl(dt.ic_paid,0)), "
          + "                    0))                                                                                             as sponsored_amount, "
          + "                sum(decode(nvl(atsc.sponsored,'N'),'N', "
          + "                     decode(dt.debit_credit_indicator,'D',1,-1) * "
          + "                     decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     (nvl(dt.discount_paid,0) + nvl(dt.ic_paid,0)), "
          + "                    0))                                                                                             as non_sponsored_amount, "
          + "                dt.load_filename                                                                                    as load_filename,"
          + "                nvl(ff.faster_funding_enabled,'N')                                                                           as same_day_ind, "
          + "                nvl(ff.pre_funding_enabled , 'N')                                                                             as pre_fund_ind, "
          + "                dt.load_filename                                                                                    as load_filename, "
          + "                dt.load_file_id                                                                                     as load_file_id, "
          + "                sum(decode(substr(dt.card_type,1,1),'V',1,0) * "
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as vs_amount, "
          + "                sum(decode(substr(dt.card_type,1,1),'M',1,0) * "	
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as mc_amount, "
          + "                sum(decode(dt.card_type,'AM',1,0) * "
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as am_amount, "
          + "                sum(decode(dt.card_type,'DS',1,'JC',1,0) * "
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as ds_amount, "	
          + "                sum(decode(dt.card_type,'PL',1,0) * "	
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as pl_amount, "
          + "                sum(decode(dt.card_type, 'DB',1,'EB',1,0) * "	
          + "                     decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                     (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                     nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                                  as db_amount "
          + "from            daily_detail_file_dt dt, "
          + "                ach_trident_sponsored_cards atsc, "
          + "                mif mf, "
          + "                transaction_funding_amount tf,"
          + "                ff_merchant ff "
          + "where           dt.batch_date =  :2 "
          + "                and dt.load_filename =  :3 "
          + "                and dt.merchant_account_number = mf.merchant_number "
          + "                and (nvl(mf.mes_amex_processing, 'N') = 'N' or dt.card_type != 'AM') "
          + "                and nvl(dt.ach_flag,'Y') = 'Y' "
          + "                and dt.card_type = atsc.card_type(+) "
          + "                and tf.transaction(+)=dt.trident_tran_id "
          + "                and ff.merchant_number(+)=dt.merchant_account_number "
          + "group by        dt.bank_number,dt.batch_date,dt.batch_number,dt.merchant_batch_number,dt.merchant_account_number, "
          + "                (trunc(sysdate) + "
          + "                  (case when mf.bank_number in (fasterFundingBanks) and nvl(mf.suspended_days, 0) > 0 "
          + "                        then nvl(mf.suspended_days, 0) - 1 "
          + "                        else nvl(mf.suspended_days, 0) "
          + "                   end)), "
          + "                dt.load_filename, "
          + "                decode(tf.merchant,null,dt.merchant_account_number,tf.merchant), "
          + "                decode(tf.merchant,null,dt.bank_number,mf.bank_number), "
          + "               ff.faster_funding_enabled, ff.pre_funding_enabled,"
          + "                dt.load_file_id ";
  
  private String SQL_GET_DDF_DATA_AMEX = ""
          + "select  decode(tf.merchant,null,dt.bank_number,mf.bank_number)                                          as bank_number, "
          + "                    dt.batch_date                                                                       as batch_date, "
          + "                    dt.batch_number                                                                     as rec_id, "
          + "                    dt.merchant_batch_number                                                            as batch_number, "
          + "                    decode(tf.merchant,null,dt.merchant_account_number,tf.merchant)                     as merchant_number, "
          + "                    (trunc(sysdate) + "
          + "                     ( "
          + "                      case "
          + "                          when nvl(mf.suspended_days, 0) > nvl(mf.mes_amex_suspended_days,2) "
          + "                          then "
          + "                              case "
          + "                                  when mf.bank_number in (fasterFundingBanks) "
          + "                                  then nvl(mf.suspended_days, 0) - 1 "
          + "                                  else nvl(mf.suspended_days,0) "
          + "                              end "
          + "                          else "
          + "                              case "
          + "                                  when mf.bank_number in (fasterFundingBanks) "
          + "                                  then "
          + "                                      case "
          + "                                          when nvl(mf.mes_amex_suspended_days, 0) = 0 "
          + "                                          then 0 "
          + "                                          else nvl(mf.mes_amex_suspended_days, 0) - 1 "
          + "                                      end "
          + "                                  else nvl(mf.mes_amex_suspended_days,2) "
          + "                              end "
          + "                      end "
          + "                     ) "
          + "                    )                                                                                   as post_date, "
          + "                    :1                                                                                  as entry_desc, "
          + "                    sum(decode(dt.debit_credit_indicator, 'D',1,0))                                     as sales_count, "
          + "                    sum(decode(tf.amount,null, "
          + "                        dt.transaction_amount * decode(dt.debit_credit_indicator, 'D', 1, 0), "
          + "                        tf.amount * decode(dt.debit_credit_indicator, 'D', 1, 0)))                      as sales_amount, "
          + "                    sum(decode(dt.debit_credit_indicator, 'C', 1, 0))                                   as credits_count, "
          + "                    sum(decode(tf.amount,null, "
          + "                        dt.transaction_amount * decode(dt.debit_credit_indicator, 'C', 1, 0), "
          + "                        tf.amount * decode(dt.debit_credit_indicator, 'C', 1, 0)))                      as credits_amount, "
          + "                    sum( nvl(dt.discount_paid, 0) )                                                     as daily_discount_amount, "
          + "                    sum( nvl(dt.ic_paid, 0) )                                                           as daily_ic_amount, "
          + "                    sum(decode(dt.debit_credit_indicator, 'D', 1, -1) * "
          + "                        decode(tf.amount,null,dt.transaction_amount,tf.amount)) "
          + "                        - sum( nvl(dt.discount_paid, 0) ) "
          + "                        - sum( nvl(dt.ic_paid, 0) )                                                     as net_amount, "
          + "                    sum(decode(atsc.sponsored,'Y', "
          + "                         (decode(dt.debit_credit_indicator,'D',1,-1) * "
          + "                           decode(tf.amount,null,dt.transaction_amount,tf.amount)) - "
          + "                            (nvl(dt.discount_paid,0) + nvl(dt.ic_paid,0)), "
          + "                        0))                                                                             as sponsored_amount, "
          + "                    sum(decode(nvl(atsc.sponsored,'N'),'N', "
          + "                          decode(dt.debit_credit_indicator,'D',1,-1) * "
          + "                           decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                           (nvl(dt.discount_paid,0) + nvl(dt.ic_paid,0)), "
          + "                        0))                                                                             as non_sponsored_amount, "
          + "                    dt.load_filename                                                                    as load_filename, "
          + "                nvl(ff.faster_funding_enabled,'N')                                                               as same_day_ind,"
          + "                nvl(ff.pre_funding_enabled,'N')                                                                  as pre_fund_ind, "
          + "                    dt.load_file_id                                                                     as load_file_id, "
          + "                    sum(decode(substr(dt.card_type,1,1),'V',1,0) * "
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "
          + "                          (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                           nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                as vs_amount, "
          + "                    sum(decode(substr(dt.card_type,1,1),'M',1,0) * "	
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                         (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                         nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                  as mc_amount, "
          + "                    sum(decode(dt.card_type,'AM',1,0) * "
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                         (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                         nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                  as am_amount, "
          + "                    sum(decode(dt.card_type,'DS',1,'JC',1,0) * "
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                         (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                         nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                  as ds_amount, "	
          + "                    sum(decode(dt.card_type,'PL',1,0) * "	
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "
          + "                         (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                         nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                  as pl_amount, "
          + "                    sum(decode(dt.card_type, 'DB',1,'EB',1,0) * "	
          + "                         decode(dt.debit_credit_indicator,'C',-1,1) * "	
          + "                         (decode(tf.amount,null,dt.transaction_amount,tf.amount) - "
          + "                         nvl(dt.discount_paid,0) - nvl(dt.ic_paid,0)))                                  as db_amount " 
          + "from                daily_detail_file_dt dt, "
          + "                    ach_trident_sponsored_cards atsc, "
          + "                    transaction_funding_amount tf, "
          + "                    mif mf,"
          + "                    ff_merchant ff "
          + " where              dt.batch_date = :2 "
          + "                    and dt.load_filename = :3 "
          + "                    and dt.merchant_account_number = mf.merchant_number "
          + "                    and (nvl(mf.mes_amex_processing, 'N') != 'N' and dt.card_type = 'AM') "
          + "                    and nvl(dt.ach_flag,'Y') = 'Y' "
          + "                    and dt.card_type = atsc.card_type(+) "
          + "                    and tf.transaction(+) = dt.trident_tran_id "
          + "                    and ff.merchant_number(+) = dt.merchant_account_number "
          + "group by            dt.bank_number,dt.batch_date, dt.batch_number,dt.merchant_batch_number, "
          + "                    dt.merchant_account_number, "
          + "                      (trunc(sysdate) + "
          + "                       ( "
          + "                          case "
          + "                              when nvl(mf.suspended_days, 0) > nvl(mf.mes_amex_suspended_days,2) "
          + "                              then "
          + "                                  case "
          + "                                      when mf.bank_number in (fasterFundingBanks) "
          + "                                      then nvl(mf.suspended_days, 0) - 1 "
          + "                                      else nvl(mf.suspended_days,0) "
          + "                                  end "
          + "                              else "
          + "                                  case "
          + "                                      when mf.bank_number in (fasterFundingBanks) "
          + "                                      then "
          + "                                          case "
          + "                                              when nvl(mf.mes_amex_suspended_days, 0) = 0 "
          + "                                              then 0 "
          + "                                              else nvl(mf.mes_amex_suspended_days, 0) - 1 "
          + "                                          end "
          + "                                      else nvl(mf.mes_amex_suspended_days,2) "
          + "                                  end "
          + "                          end "
          + "                       ) "
          + "                      ), "
          + "                      decode(tf.merchant,null,dt.merchant_account_number,tf.merchant), "
          + "                      decode(tf.merchant,null,dt.bank_number,mf.bank_number), "
          + "                      dt.load_filename,"
          + "                      ff.faster_funding_enabled, ff.pre_funding_enabled,"
          + "                      dt.load_filename, "
          + "                      dt.load_file_id ";
  
  private boolean extractACHRecords(FileRec fr)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           retVal  = false;
    Vector            stmts   = new Vector();
    String            tableName = "ach_trident_statement_test";
    Map<Long,String> fasterFundingConfig = new HashMap<>();
    String fasterFundingBanks = "0000";
    
    try
    {
      long startTime = System.currentTimeMillis();
      
      FasterFundingDao fasterFundingDao = new FasterFundingDao(con);
      fasterFundingConfig = fasterFundingDao.loadFasterFundingConfig();
      
      if(!fasterFundingConfig.isEmpty()) {
          fasterFundingBanks = StringUtils.join(fasterFundingConfig.keySet(), ",");
      }
      
      if( "PROD".equals(getEventArg(1)) )
      {
        tableName = "ach_trident_statement";
      }
      
      log.debug("checking to see if this file was successfully processed");
      
      int   recCount      = 0;
      int   icCount       = 0;
      int   discountCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:81^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(dt.merchant_account_number)           record_count,
//                  sum(decode(dt.ic_expense, null, 0, 1))      ic_count,
//                  sum(decode(dt.discount_amount, null, 0, 1)) discount_count
//          
//          from    daily_detail_file_dt dt
//          where   dt.batch_date = :fr.getBatchDate()
//                  and dt.load_filename = :fr.getLoadFilename()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(dt.merchant_account_number)           record_count,\n                sum(decode(dt.ic_expense, null, 0, 1))      ic_count,\n                sum(decode(dt.discount_amount, null, 0, 1)) discount_count\n         \n        from    daily_detail_file_dt dt\n        where   dt.batch_date =  :1  \n                and dt.load_filename =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,fr.getBatchDate());
   __sJT_st.setString(2,fr.getLoadFilename());
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   icCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   discountCount = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
      
      if( icCount < recCount || discountCount < recCount )
      {
        // some of the records in fhe file have not been processed successfully
        logEntry("extractACHRecords(" + fr.getLoadFilename() + ")", 
                  "Total Records: " + recCount + "\n" +
                  "IC:            " + icCount + "\n" + 
                  "Discount:      " + discountCount);
      }
      else
      {
        log.debug("retrieving regular funding ach details");

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
      SQL_GET_DDF_DATA = SQL_GET_DDF_DATA.replace("fasterFundingBanks", fasterFundingBanks);
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DailyDetailFileProcessEvent",SQL_GET_DDF_DATA);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setDate(2,fr.getBatchDate());
   __sJT_st.setString(3,fr.getLoadFilename());
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DailyDetailFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^9*/

        rs = it.getResultSet();

        log.debug("  query complete: " + (System.currentTimeMillis() - startTime) + " ms");
        while(rs.next())
        {
          stmts.add(new AchStatementRecord(rs));

          // set net deposit amount for this batch
          /*@lineinfo:generated-code*//*@lineinfo:196^11*/

//  ************************************************************
//  #sql [Ctx] { merge into daily_detail_file_dt dt
//              using mif mf
//              on
//              (
//                dt.batch_date = :rs.getDate("batch_date")
//                and dt.batch_number = :rs.getLong("rec_id")
//                and dt.merchant_account_number = mf.merchant_number
//                and ( nvl(mf.mes_amex_processing, 'N') = 'N' or dt.card_type != 'AM' )
//              )
//              when matched then
//              update
//                set dt.net_deposit = :rs.getDouble("net_amount")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1 = rs.getDate("batch_date");
 long __sJT_2 = rs.getLong("rec_id");
 double __sJT_3 = rs.getDouble("net_amount");
   String theSqlTS = "merge into daily_detail_file_dt dt\n            using mif mf\n            on\n            (\n              dt.batch_date =  :1  \n              and dt.batch_number =  :2  \n              and dt.merchant_account_number = mf.merchant_number\n              and ( nvl(mf.mes_amex_processing, 'N') = 'N' or dt.card_type != 'AM' )\n            )\n            when matched then\n            update\n              set dt.net_deposit =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1);
   __sJT_st.setLong(2,__sJT_2);
   __sJT_st.setDouble(3,__sJT_3);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^11*/
        }

        rs.close();
        it.close();

        log.debug("found " + stmts.size() + " ach records to add to ach_trident_statement");

        log.debug("retrieving amex funding ach details");
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
      SQL_GET_DDF_DATA_AMEX = SQL_GET_DDF_DATA_AMEX.replace("fasterFundingBanks", fasterFundingBanks);
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.DailyDetailFileProcessEvent",SQL_GET_DDF_DATA_AMEX);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_AMEX_DEP);
   __sJT_st.setDate(2,fr.getBatchDate());
   __sJT_st.setString(3,fr.getLoadFilename());
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.DailyDetailFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^9*/
      
        rs = it.getResultSet();
      
        log.debug("  query complete: " + (System.currentTimeMillis() - startTime) + " ms");
        while(rs.next())
        {
          stmts.add(new AchStatementRecord(rs));
          
          /*@lineinfo:generated-code*//*@lineinfo:340^11*/

//  ************************************************************
//  #sql [Ctx] { merge into daily_detail_file_dt dt
//              using mif mf
//              on
//              (
//                dt.batch_date = :rs.getDate("batch_date")
//                and dt.batch_number = :rs.getLong("rec_id")
//                and dt.merchant_account_number = mf.merchant_number
//                and ( nvl(mf.mes_amex_processing, 'N') != 'N' and dt.card_type = 'AM' )
//              )
//              when matched then
//              update
//                set dt.net_deposit = :rs.getDouble("net_amount")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4 = rs.getDate("batch_date");
 long __sJT_5 = rs.getLong("rec_id");
 double __sJT_6 = rs.getDouble("net_amount");
   String theSqlTS = "merge into daily_detail_file_dt dt\n            using mif mf\n            on\n            (\n              dt.batch_date =  :1  \n              and dt.batch_number =  :2  \n              and dt.merchant_account_number = mf.merchant_number\n              and ( nvl(mf.mes_amex_processing, 'N') != 'N' and dt.card_type = 'AM' )\n            )\n            when matched then\n            update\n              set dt.net_deposit =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4);
   __sJT_st.setLong(2,__sJT_5);
   __sJT_st.setDouble(3,__sJT_6);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:354^11*/
        }
      
        rs.close();
        it.close();
      
        log.debug("found " + stmts.size() + " ach records to add to ach_trident_statement");
      
        // add statements to table in batch mode
        log.debug("PROCESSING...");
        if( AchDb.storeAchStatementVector(stmts, tableName) )
        {
          retVal = true;
        }
        commit();
        
        log.debug("  DONE: " + (System.currentTimeMillis() - startTime) + " ms");
        if (retVal) {
        	AchDb.storeAchExtStatementVector(stmts, tableName + "_dtl ");
        }
        log.debug(" _dtl completed " );
        
        log.debug("Creating ProcessEntries for Payouts Merchants");
        AchDb.createPayoutsFundingProcessEntries(stmts);
      }
    }
    catch(Exception e)
    {
      logEntry("extractACHRecords(" + fr.getLoadFilename() + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return( retVal );
  }
  
	public boolean calcDccFundingAmounts(FileRec fr) {
		log.info(String.format("[calcDccFundingAmounts()] - calculating funding amounts for FileRec=%s", fr));
		long startTime = System.currentTimeMillis();
		Boolean retVal = Boolean.TRUE;
		MesQueryHandlerResultSet queryHandler = null;
		try {
			// get list of records to process
			MesResultSet resultSet = getTransactions(fr);
			int counter = 0;
			for (Map<String, Object> clearingMap : resultSet) {
				if (queryHandler == null) {
					queryHandler = getQueryHandler();
					queryHandler.setAutoCommit(false);
				}
				// get fx conversion rate
				DccRateUtil.DccRate fxRate = getExchangeRate(fr, clearingMap);
				if (fxRate == null) {
					retVal = Boolean.FALSE;
					if (log.isDebugEnabled()) {
						log.error("[calcDccFundingAmounts()] - FX rate missing for fileRec=" + fr + " clearingMap=" + clearingMap);
					}
					continue;
				}
				// update daily_detail_file_dt
				updateMerchantClearing(queryHandler, clearingMap, fxRate);
				// update settlement
				updatePresentment(queryHandler, clearingMap, fxRate);

				if (counter > 0 && counter % 50 == 0) {
					queryHandler.commit();
					log.info(String.format("[calcDccFundingAmounts()] - updated 50 transactions of FileRec=%s", fr));
				}
				counter++;
			}
		}
		catch (Exception e) {
			log.error(String.format("[calcDccFundingAmounts()] - Error %s processing %s", e.getMessage(), fr.getLoadFilename()), e);
		}
		finally {
			if (queryHandler != null) {
				try {
					queryHandler.commit();
					queryHandler.closeConnection();
				}
				catch (SQLException e) {
					log.error(String.format("[calcDccFundingAmounts()] - Error %s processing %s", e.getMessage(), fr.getLoadFilename()), e);
				}
			}
			long elapsedTime = System.currentTimeMillis() - startTime;
			log.info(String.format("[calcDccFundingAmounts()] - Processing %s competed,success=%s elapsed time=%s", fr.getLoadFilename(), retVal, elapsedTime));
		}
		return (retVal);
	}

	protected DccRateUtil.DccRate getExchangeRate(FileRec fileRec, Map<String, Object> clearingMap) {
		DccRateUtil util = getDccRateUtil(fileRec);
		String cardType = (String) clearingMap.get(DccRecColumn.CARD_TYPE.columnName);
		String baseCurrency = (String) clearingMap.get(DccRecColumn.BASE_CC.columnName);
		String counterCurrency = (String) clearingMap.get(DccRecColumn.COUNTER_CC.columnName);
		java.util.Date aFxConversionDate = (java.util.Date) clearingMap.get(DccRecColumn.FX_CONVERSION_DATE.columnName);
		Date fxConvDate = new java.sql.Date(aFxConversionDate.getTime());

		if (baseCurrency.equals(counterCurrency)) {
			return DccRateUtil.getDefaultRate(cardType, baseCurrency);
		}

		return util.getDccRate(cardType, baseCurrency, counterCurrency, fxConvDate);
	}

	protected DccRateUtil getDccRateUtil(FileRec fileRec) {
		if (fxRateUtil == null) {
			fxRateUtil = DccRateUtil.getInstance(fileRec.getBatchDate());
		}
		if (fxRateUtil.getBatchDate().getTime() != fileRec.getBatchDate().getTime()) {
			fxRateUtil = DccRateUtil.getInstance(fileRec.getBatchDate());
		}
		return fxRateUtil;
	}

	protected MesQueryHandlerResultSet getQueryHandler() {
		return new MesQueryHandlerResultSet();
	}

	protected MesResultSet getTransactions(FileRec fr) throws SQLException {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getTransactions()] - Loading transactions for batchDate=%s and fileName=%s", fr.getBatchDate(), fr.getLoadFilename()));
		}
		//@formatter:off
		String transactionSql = "select  "
		   		   			  + "   dt.batch_date as batch_date, dt.batch_number as batch_number,  "
		   		   			  + "   dt.ddf_dt_id as ddf_dt_id,(nvl(mp.rate,0)*0.01) as fx_markup, "
		   		   			  + "   dt.currency_code as base_currency_code , dt.original_currency_code as counter_currency_code ,"
		   		   			  + "   case "
		   		   			  + " 	  when dt.card_type in ('VS','VB','VD') then 'VS' "
		   		   			  + " 	  when dt.card_type in ('MC','MB','MD') then 'MC' " 
		   		   			  + " 	  when dt.card_type in ('DC','DS','JC') then 'DS' "
		   		   			  + "     else 'MC' "
		   		   			  + "  	end as card_type , "
		   		   			  + "   nvl(dt.fx_conv_date,dt.batch_date) as fx_conv_date "
		   		   			  + "from"
		   		   			  + "  daily_detail_file_dt dt,"
		   		   			  + "  mbs_pricing  mp " 
		   		   			  + " where   "
		   		   			  + "	dt.batch_date = ? " 
		   		   			  + " 	and dt.load_filename = ? "
		   		   			  + " 	and dt.transaction_amount is null "
		   		   			  + " 	and mp.merchant_number(+) = dt.merchant_account_number "
		   		   			  + " 	and mp.item_type(+) = 124 "  
		   		   			  + " 	and dt.batch_date between mp.valid_date_begin(+) and mp.valid_date_end(+) ";
		 //@formatter:on
		String sqlName = "13com.mes.startup.DailyDetailFileProcessEvent";
		return getQueryHandler().executePreparedStatement(sqlName, transactionSql, fr.getBatchDate(), fr.getLoadFilename());
	}

	protected void updatePresentment(MesQueryHandlerResultSet queryHandler, Map<String, Object> clearingMap, DccRate fxRate) throws Exception {
		String tableName = null;
		String sqlExtra = null;
		String cardType = (String) clearingMap.get("card_type");
		if ("VS".equals(cardType)) {
			tableName = "visa_settlement";
			sqlExtra = "or s.cash_disbursement = 'Y' ";
		}
		else if ("MC".equals(cardType)) {
			tableName = "mc_settlement ";
			sqlExtra = "or s.cash_disbursement = 'Y' ";
		}
		else if ("AM".equals(cardType)) {
			tableName = "amex_settlement ";
			sqlExtra = "";
		}
		else if ("DS".equals(cardType)) {
			tableName = "discover_settlement ";
			sqlExtra = "";
		}
		else {
			log.error("[updatePresentment()] - Invalid card type : " + cardType);
			throw new Exception("Invalid card type : " + cardType);
		}

		try {
			double buyRate = fxRate.getBuyRate();
			double sellRate = fxRate.getSellRate();

			StringBuilder preparedSQLQuery = new StringBuilder();
			preparedSQLQuery.append("update ");
			preparedSQLQuery.append(tableName);
			preparedSQLQuery.append(" s  set funding_currency_code =  ?  , ");
			preparedSQLQuery.append("funding_amount = round( ( case  when s.debit_credit_indicator = 'C' ");
			preparedSQLQuery.append(sqlExtra);
			preparedSQLQuery.append(" then  ?  else  ? end * s.transaction_amount), 2 ) ");
			preparedSQLQuery.append(" where batch_id =  ? and batch_record_id =  ? and batch_date =  ?");
			String finalSQLQuery = preparedSQLQuery.toString();

			Object[] params = new Object[6];
			params[0] = clearingMap.get(DccRecColumn.BASE_CC.columnName);
			params[1] = buyRate;
			params[2] = sellRate;
			params[3] = clearingMap.get(DccRecColumn.BATCH_NUMBER.columnName);
			params[4] = clearingMap.get(DccRecColumn.DDF_DT_ID.columnName);
			params[5] = clearingMap.get(DccRecColumn.BATCH_DATE.columnName);

			queryHandler.executePreparedUpdate("15com.mes.startup.DailyDetailFileProcessEvent", finalSQLQuery, params);
		}
		catch (SQLException e) {
			log.error("[updatePresentment()] - SQLException updating presentment record", e);
		}
	}

	protected void updateMerchantClearing(MesQueryHandlerResultSet queryHandler, Map<String, Object> clearingMap, DccRate fxRate) {
		//@formatter:off
		String updateClearingSql = "update daily_detail_file_dt    dt"
					 			 + " 	set fx_rate = "
					 			 + " 			case "
					 			 + " 				when dt.debit_credit_indicator = 'C' "
					 			 + "					or dt.cash_disbursement = 'Y' "
					 			 + "				then ? "
					 			 + " 				else ? "
					 			 + " 			end, "
					 			 + " 		fx_markup = ?, "
					 			 + " 		fx_markup_amount = round(( case when dt.debit_credit_indicator = 'C'  "
					 			 + "											 or dt.cash_disbursement = 'Y' "
					 			 + " 										then - ( ? * ? ) "
					 			 + " 										else   ( ? * ? ) "
					 			 + " 									end "
					 			 + " 							* dt.original_transaction_amount), 2 ),"
					 			 + " 		transaction_amount = round(( case when dt.debit_credit_indicator = 'C' "
					 			 + "		or dt.cash_disbursement = 'Y' "
					 			 + " 			then ? "
					 			 + " 			else ? "
					 			 + "   end "
					 			 + " 		* dt.original_transaction_amount), 2 )- round(( case when dt.debit_credit_indicator = 'C' "
					 			 + "		or dt.cash_disbursement = 'Y' "
					 			 + " 			then - ( ? * ? ) "
					 			 + " 			else   ( ? * ? ) "
					 			 + "   end "
					 			 + "   * dt.original_transaction_amount), 2 ) "
					 			 + " where "
					 			 + "   batch_date = ? "
					 			 + "   and batch_number = ? "
					 			 + "   and ddf_dt_id = ? ";
		//@formatter:on

		try {

			double buyRate = fxRate.getBuyRate();
			double sellRate = fxRate.getSellRate();
			if (buyRate == 0.0 || sellRate == 0.0) {
				log.debug(String.format("[updateMerchantClearing()] - BuyRate or SellRate is 0.0, buyRate=%s, sellRate=%s", buyRate, sellRate));
				return;
			}
			Object fxMarkup = clearingMap.get(DccRecColumn.FX_MARK_UP.columnName);

			Object[] params = new Object[16];
			params[0] = buyRate;
			params[1] = sellRate;
			params[2] = fxMarkup;
			params[3] = buyRate;
			params[4] = fxMarkup;
			params[5] = sellRate;
			params[6] = fxMarkup;
			params[7] = buyRate;
			params[8] = sellRate;
			params[9] = buyRate;
			params[10] = fxMarkup;
			params[11] = sellRate;
			params[12] = fxMarkup;
			params[13] = clearingMap.get(DccRecColumn.BATCH_DATE.columnName);
			params[14] = clearingMap.get(DccRecColumn.BATCH_NUMBER.columnName);
			params[15] = clearingMap.get(DccRecColumn.DDF_DT_ID.columnName);

			queryHandler.executePreparedUpdate("14com.mes.startup.DailyDetailFileProcessEvent", updateClearingSql, params);
		}
		catch (SQLException e) {
			log.error("[updateMerchantClearing()] - SQLException updating merchange clearing", e);
		}
	}
	
  private boolean calcDiscountIC(FileRec fr)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           retVal  = false;
    
    try
    {
      // retrieve dt records along with their associated discount, ic billing, and ic expense
      long startTime = System.currentTimeMillis();
      
      log.debug("[calcDiscountIC()] - retrieving items to process");
      /*
        This query retrieves all the records in the file that need to be processed and saves info
        to calculate the discount amount, interchange billing amount, and interchange cost for each transaction.
        
        Discount rate --> is calculated based on the valid value in MBS_PRICING for
        discount rate and per-item (identfied by item_sublcass = card type).
        
        If the merchant is configured to be billed NET (MIF.VISA_DISC_METHOD='N')
        then the discount rate is calcualated for credit transactions and netted
        out of the total.  If they are configured to be billed GROSS (MF.VISA_DISC_METHOD != 'N')
        then credits are ignored for the purpose of calculating discount rate.
        
        Interchange expense --> is calculated as actual interchange cost based on
        the current values in DAILY_DETAIL_FILE_IC_DESC.  Interchange cost
        for credits is always negative but it is important to note that unless
        the merchant is confgured for interchange pass-through pricing, the actual
        cost of interchange to the merchant will be different than this value.
        The interchange cost is what Visa or MasterCard will bill Merchant e-Solutions
        for the transaction, not necessarily what MES will bill the merchant.
      */
      /*@lineinfo:generated-code*//*@lineinfo:564^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_date                             as batch_date,
//                  dt.batch_number                           as batch_number,
//                  dt.ddf_dt_id                              as ddf_dt_id,
//                  nvl(mp.rate,0)                            as discount_rate,
//                  nvl(mp.per_item,0)                        as per_item,
//                  -- discount is negative for credits if the merchant is billed NET and zero if billed GROSS
//                  decode(dt.debit_credit_indicator,
//                    'C', decode(mf.visa_disc_method, 'N', -1, 0),
//                    1) * round((dt.transaction_amount*nvl(mp.rate,0)*.01) + nvl(mp.per_item,0),2) 
//                                                            as discount_amount,
//                  decode(mf.daily_discount_interchange,'D',1,'B',1,0)*decode(dt.debit_credit_indicator,
//                    'C', decode(mf.visa_disc_method, 'N', -1, 0),
//                    1) * round((dt.transaction_amount*nvl(mp.rate,0)*.01) + nvl(mp.per_item,0),2) 
//                                                            as discount_paid,
//                  -- interchange is always negative for credits
//                  (
//                    decode(dt.debit_credit_indicator,'C',-1,1) 
//                    * round((dt.transaction_amount*(nvl(icd.ic_rate, 0)*.01) + nvl(icd.ic_per_item,0)), 2) 
//                  )                                         as ic_expense,
//                  (
//                    decode(dt.debit_credit_indicator,'C',-1,1) 
//                    * round((dt.transaction_amount*(nvl(icd_enh.ic_rate, 0)*.01) + nvl(icd_enh.ic_per_item,0)), 2) 
//                  )                                         as ic_expense_enh,
//                  (                                                       
//                      decode(mf.daily_discount_interchange,'I',1,'B',1,0)
//                    * decode(dt.debit_credit_indicator,'C',-1,1) 
//                    * round((dt.transaction_amount*(nvl(icd.ic_rate, 0)*.01) + nvl(icd.ic_per_item,0)), 2) 
//                  )                                         as ic_paid,
//                  mf.daily_discount_interchange             as daily_discount_interchange
//          from    daily_detail_file_dt dt,
//                  (
//                    select  mpd.merchant_number,
//                            mpd.item_subclass,
//                            mpd.rate,
//                            mpd.per_item
//                    from    mbs_pricing mpd,
//                            mbs_elements mbe
//                    where   :fr.getBatchDate() between mpd.valid_date_begin and mpd.valid_date_end
//                            and mpd.item_type = mbe.item_type
//                            and mbe.item_category = 'DISC' 
//                  ) mp,
//                  (
//                    select  substr(icd.card_type,1,1)||icd.ic_code ic_code,
//                            icd.ic_rate                          ic_rate,
//                            icd.ic_per_item                      ic_per_item
//                    from    daily_detail_file_ic_desc icd
//                    where   :fr.getBatchDate() between icd.valid_date_begin and icd.valid_date_end
//                  ) icd,
//                  (
//                    select  substr(icd.card_type,1,1)||icd.ic_code ic_code,
//                            icd.ic_rate                          ic_rate,
//                            icd.ic_per_item                      ic_per_item
//                    from    daily_detail_file_ic_desc icd
//                    where   :fr.getBatchDate() between icd.valid_date_begin and icd.valid_date_end
//                  ) icd_enh,
//                  mif mf
//          where   dt.batch_date = :fr.getBatchDate()
//                  and dt.load_filename = :fr.getLoadFilename()
//                  and mp.merchant_number(+) = dt.merchant_account_number
//                  and mp.item_subclass(+) = dt.card_type
//                  and icd.ic_code(+) = substr(dt.card_type,1,1)||dt.submitted_interchange2
//                  and icd_enh.ic_code(+) = substr(dt.card_type,1,1)||nvl(dt.enhanced_interchange,dt.submitted_interchange2)
//                  and mf.merchant_number = dt.merchant_account_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.batch_date                             as batch_date,\n                dt.batch_number                           as batch_number,\n                dt.ddf_dt_id                              as ddf_dt_id,\n                nvl(mp.rate,0)                            as discount_rate,\n                nvl(mp.per_item,0)                        as per_item,\n                -- discount is negative for credits if the merchant is billed NET and zero if billed GROSS\n                decode(dt.debit_credit_indicator,\n                  'C', decode(mf.visa_disc_method, 'N', -1, 0),\n                  1) * round((dt.transaction_amount*nvl(mp.rate,0)*.01) + nvl(mp.per_item,0),2) \n                                                          as discount_amount,\n                decode(mf.daily_discount_interchange,'D',1,'B',1,0)*decode(dt.debit_credit_indicator,\n                  'C', decode(mf.visa_disc_method, 'N', -1, 0),\n                  1) * round((dt.transaction_amount*nvl(mp.rate,0)*.01) + nvl(mp.per_item,0),2) \n                                                          as discount_paid,\n                -- interchange is always negative for credits\n                (\n                  decode(dt.debit_credit_indicator,'C',-1,1) \n                  * round((dt.transaction_amount*(nvl(icd.ic_rate, 0)*.01) + nvl(icd.ic_per_item,0)), 2) \n                )                                         as ic_expense,\n                (\n                  decode(dt.debit_credit_indicator,'C',-1,1) \n                  * round((dt.transaction_amount*(nvl(icd_enh.ic_rate, 0)*.01) + nvl(icd_enh.ic_per_item,0)), 2) \n                )                                         as ic_expense_enh,\n                (                                                       \n                    decode(mf.daily_discount_interchange,'I',1,'B',1,0)\n                  * decode(dt.debit_credit_indicator,'C',-1,1) \n                  * round((dt.transaction_amount*(nvl(icd.ic_rate, 0)*.01) + nvl(icd.ic_per_item,0)), 2) \n                )                                         as ic_paid,\n                mf.daily_discount_interchange             as daily_discount_interchange\n        from    daily_detail_file_dt dt,\n                (\n                  select  mpd.merchant_number,\n                          mpd.item_subclass,\n                          mpd.rate,\n                          mpd.per_item\n                  from    mbs_pricing mpd,\n                          mbs_elements mbe\n                  where    :1   between mpd.valid_date_begin and mpd.valid_date_end\n                          and mpd.item_type = mbe.item_type\n                          and mbe.item_category = 'DISC' \n                ) mp,\n                (\n                  select  substr(icd.card_type,1,1)||icd.ic_code ic_code,\n                          icd.ic_rate                          ic_rate,\n                          icd.ic_per_item                      ic_per_item\n                  from    daily_detail_file_ic_desc icd\n                  where    :2   between icd.valid_date_begin and icd.valid_date_end\n                ) icd,\n                (\n                  select  substr(icd.card_type,1,1)||icd.ic_code ic_code,\n                          icd.ic_rate                          ic_rate,\n                          icd.ic_per_item                      ic_per_item\n                  from    daily_detail_file_ic_desc icd\n                  where    :3   between icd.valid_date_begin and icd.valid_date_end\n                ) icd_enh,\n                mif mf\n        where   dt.batch_date =  :4  \n                and dt.load_filename =  :5  \n                and mp.merchant_number(+) = dt.merchant_account_number\n                and mp.item_subclass(+) = dt.card_type\n                and icd.ic_code(+) = substr(dt.card_type,1,1)||dt.submitted_interchange2\n                and icd_enh.ic_code(+) = substr(dt.card_type,1,1)||nvl(dt.enhanced_interchange,dt.submitted_interchange2)\n                and mf.merchant_number = dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fr.getBatchDate());
   __sJT_st.setDate(2,fr.getBatchDate());
   __sJT_st.setDate(3,fr.getBatchDate());
   __sJT_st.setDate(4,fr.getBatchDate());
   __sJT_st.setString(5,fr.getLoadFilename());
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.DailyDetailFileProcessEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:629^7*/
      rs = it.getResultSet();
      log.debug("[calcDiscountIC()] - query complete: " + (System.currentTimeMillis() - startTime) + " ms");
      
      Vector dtRecs = getDTRecs(rs);
      
      rs.close();
      it.close();
      
      log.debug("[calcDiscountIC()] - found " + dtRecs.size() + " records to update in DDF table");
      
      // update dt records with expense values
      log.debug("[calcDiscountIC()] - PROCESSING...");
      startTime = System.currentTimeMillis();
      for(int i=0; i<dtRecs.size(); ++i)
      {
        DTRec rec = (DTRec)dtRecs.elementAt(i);
        
        /*@lineinfo:generated-code*//*@lineinfo:647^9*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_dt
//            set     discount_rate               = :rec.DiscountRate,
//                    per_item                    = :rec.PerItem,
//                    discount_amount             = :rec.DiscountAmount,
//                    discount_paid               = :rec.DiscountPaid,
//                    ic_expense                  = :rec.ICExpense,
//                    ic_expense_enhanced         = :rec.ICExpenseEnhanced,
//                    ic_paid                     = :rec.ICPaid,
//                    daily_discount_interchange  = :rec.DailyDiscountIC
//            where   batch_date        = :rec.BatchDate
//                    and batch_number  = :rec.BatchNumber
//                    and ddf_dt_id     = :rec.DdfDtId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_dt\n          set     discount_rate               =  :1  ,\n                  per_item                    =  :2  ,\n                  discount_amount             =  :3  ,\n                  discount_paid               =  :4  ,\n                  ic_expense                  =  :5  ,\n                  ic_expense_enhanced         =  :6  ,\n                  ic_paid                     =  :7  ,\n                  daily_discount_interchange  =  :8  \n          where   batch_date        =  :9  \n                  and batch_number  =  :10  \n                  and ddf_dt_id     =  :11 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,rec.DiscountRate);
   __sJT_st.setDouble(2,rec.PerItem);
   __sJT_st.setDouble(3,rec.DiscountAmount);
   __sJT_st.setDouble(4,rec.DiscountPaid);
   __sJT_st.setDouble(5,rec.ICExpense);
   __sJT_st.setDouble(6,rec.ICExpenseEnhanced);
   __sJT_st.setDouble(7,rec.ICPaid);
   __sJT_st.setString(8,rec.DailyDiscountIC);
   __sJT_st.setDate(9,rec.BatchDate);
   __sJT_st.setString(10,rec.BatchNumber);
   __sJT_st.setLong(11,rec.DdfDtId);
  // execute statement
   if ( log.isDebugEnabled()) {
	   log.debug("[calcDiscountIC()] - execute update : \r\n"+theSqlTS);
   }
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^9*/
        
        if(i > 0 && i%1000 == 0)
        {
          commit();
        }
      }
      commit();
      log.debug("[calcDiscountIC()] -   DONE: " + (System.currentTimeMillis() - startTime) + " ms");
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("calcDiscountIC(" + fr.getLoadFilename() + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return( retVal );
  }
  
  private void addACHRec(FileRec fr)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:690^7*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_dt_process
//          (
//            process_sequence,
//            process_type,
//            load_filename,
//            batch_date
//          )
//          values
//          (
//            0,
//            :PT_ACH_EXTRACT,
//            :fr.getLoadFilename(),
//            :fr.getBatchDate()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_dt_process\n        (\n          process_sequence,\n          process_type,\n          load_filename,\n          batch_date\n        )\n        values\n        (\n          0,\n           :1  ,\n           :2  ,\n           :3  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_ACH_EXTRACT);
   __sJT_st.setString(2,fr.getLoadFilename());
   __sJT_st.setDate(3,fr.getBatchDate());
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:706^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:708^7*/

//  ************************************************************
//  #sql [Ctx] { insert into summarizer_process
//          (
//            file_type,
//            load_filename
//          )
//          values
//          (
//            :Summarizer.SUM_TYPE_DDF,  -- 1
//            :fr.getLoadFilename()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into summarizer_process\n        (\n          file_type,\n          load_filename\n        )\n        values\n        (\n           :1  ,  -- 1\n           :2  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ClearingConstants.SUM_TYPE_DDF.getValue());
   __sJT_st.setString(2,fr.getLoadFilename());
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:720^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("addACHRec(" + fr.getLoadFilename() + ")", e.toString());
    }
  }
  
  private boolean processFile(ProcessTableEntry entry)
  {
    boolean retVal = false;
    
    try
    {
      FileRec fr = new FileRec(entry.getLoadFilename(),entry.getBatchDate());
    
      switch( ProcessType )
      {
        case PT_DISCOUNT_IC_CALC:
          log.debug("  process type means calculate discount and interchange cost");
          if( !calcDccFundingAmounts(fr) )
          {
            entry.reset();
          }
          else if( calcDiscountIC(fr) )
          {          
            // calculate discount/ic and update DAILY_DETAIL_FILE_DT with the results
            // add new process table entry to extract the ACH entries
            addACHRec(fr);
            retVal = true;
          }
          break;
          
        case PT_ACH_EXTRACT:
          log.debug("  process type means extract ACH entries for this file");
          retVal = extractACHRecords(fr);
          break;
          
        default:
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("processFileRec(" + entry.getLoadFilename() + ")", e.toString());
    }
    
    return( retVal );
  }
  
  private boolean processFiles()
  {
    ProcessTableEntry               entry     = null;
    HashMap                         fileList  = new HashMap();
    boolean                         retVal    = false;
    ResultSetIterator               it        = null;
    ProcessTable                    pt        = null;
    ResultSet                       rs        = null;
    
    try
    {
      pt = new ProcessTable("daily_detail_file_dt_process","ddf_proc_sequence",ProcessType);
      
      if ( pt.hasPendingEntries() )
      {
        log.debug("marking files to process with sequence");
        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
          
          log.debug("processing file: " + entry.getLoadFilename());
          
          entry.recordTimestampBegin();
          retVal = processFile(entry);
          
          if( retVal )
          {
            entry.recordTimestampEnd();
            // process table may have multiple entries
            // for the same filename if there are multiple
            // batch dates.  to prevent two entries in the
            // billing table it is necessary to build a 
            // unique list.  implemented using HashMap
            fileList.put(entry.getLoadFilename(),entry.getLoadFilename());
          }
          else
          {
            log.debug("processFiles() - Failed to process file (" + entry.getLoadFilename() + ", " + entry.getRecId() + ")");
            // don't reprocess, probably need to fix something first
            // entry.reset();
          }
          commit();
        }
        
        // insert process table entries if necessary
        for( Iterator keys = fileList.keySet().iterator(); keys.hasNext(); )
        {
          String  loadFilename      = (String)keys.next();
          int     billingProcType   = -1;
          
          switch( ProcessType )
          {
            case PT_DISCOUNT_IC_CALC:   billingProcType = 4;    break;
          }
          
          if ( billingProcType > 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:832^13*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//                (
//                  process_sequence,process_type,load_filename
//                )
//                values
//                (
//                  0,:billingProcType,:loadFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n              (\n                process_sequence,process_type,load_filename\n              )\n              values\n              (\n                0, :1  , :2  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.DailyDetailFileProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,billingProcType);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:842^13*/
          }
        }          
      }
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("processFiles()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return( retVal );
  }
  
  public boolean execute()
  {
    boolean retVal = false;
    
    try
    {
      connect(true);
      
      ProcessType = Integer.parseInt( getEventArg(0) );
      log.debug("Process starting for type: " + ProcessType);
      
      retVal = processFiles();
      
      log.debug("Process complete");
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( retVal );
  }
  
  public Vector getFileRecs(ResultSet rs)
  {
    Vector retVal = new Vector();
    
    try
    {
      while(rs.next())
      {
        retVal.add( new FileRec(rs) );
      }
    }
    catch(Exception e)
    {
      logEntry("getFileRecs()", e.toString());
    }
    
    return( retVal );
  }
  
  public Vector getDTRecs(ResultSet rs)
  {
    Vector retVal = new Vector();
    
    try
    {
      while(rs.next())
      {
        retVal.add( new DTRec(rs) );
      }
    }
    catch(Exception e)
    {
      logEntry("getDTRecs()", e.toString());
    }
    
    return( retVal );
  }
  
  public class DTRec
  {
    Date    BatchDate           = null;
    String  BatchNumber         = null;
    String  DailyDiscountIC     = null;
    long    DdfDtId             = 0L;
    
    double  DiscountRate        = 0.0;
    double  PerItem             = 0.0;
    double  DiscountAmount      = 0.0;
    double  DiscountPaid        = 0.0;
    double  ICBilling           = 0.0;
    double  ICExpense           = 0.0;
    double  ICExpenseEnhanced   = 0.0;
    double  ICPaid              = 0.0;
    
    public DTRec(ResultSet rs)
    {
      try
      {
        BatchDate         = rs.getDate("batch_date");
        BatchNumber       = rs.getString("batch_number");
        DailyDiscountIC   = rs.getString("daily_discount_interchange");
        DdfDtId           = rs.getLong("ddf_dt_id");

        DiscountRate      = rs.getDouble("discount_rate");
        PerItem           = rs.getDouble("per_item");

        DiscountAmount    = rs.getDouble("discount_amount");
        DiscountPaid      = rs.getDouble("discount_paid");
        ICExpense         = rs.getDouble("ic_expense");
        ICExpenseEnhanced = rs.getDouble("ic_expense_enh");
        ICPaid            = rs.getDouble("ic_paid");
      }
      catch(Exception e)
      {
        logEntry("DTRec() constructor", e.toString());
      }
    }
  }
  
  public static void main(String[] args)
  {
    DailyDetailFileProcessEvent   evt   = null;
    
    try
    {
      SQLJConnectionBase.initStandalonePool();
      
      evt = new DailyDetailFileProcessEvent();
      evt.connect(true);
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      if ( "extractACHRecords".equals(args[0]) )
      {
        FileRec fr = new FileRec(args[1],new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() ));
        evt.setEventArgs("2," + ((args.length > 3) ? args[3] : "TEST") );
        evt.extractACHRecords(fr);
      }
      else if ( "calcDiscountIC".equals(args[0]) )
      {
        FileRec fr = new FileRec(args[1],new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() ));
        evt.calcDiscountIC(fr);
      }
      else if ( "calcDccFundingAmounts".equals(args[0]) )
      {
        FileRec fr = new FileRec(args[1],new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() ));
        evt.calcDccFundingAmounts(fr);
      }
      else if ( "execute".equals(args[0]) )
      {
        evt.setEventArgs(args[1] + "," + ((args.length > 2) ? args[2] : "TEST"));
        evt.execute();
      }        
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch(Exception e)
    {
      System.out.println("FAIL: " + e.toString());
    }
    finally
    {
      try{ evt.cleanUp(); } catch( Exception ee ) {}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/