/*@lineinfo:filename=FixDisquals*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class FixDisquals extends EventBase
{
  static Logger log = Logger.getLogger(FixDisquals.class);
  
  public boolean execute()
  {
    boolean           retVal    = true;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    Vector            disquals  = new Vector();
    
    try
    {
      connect();
      setAutoCommit(false);
      
      log.debug("finding broken disquals");
      // load discrepancies to fix
      /*@lineinfo:generated-code*//*@lineinfo:30^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ad.trace_number,
//                  ad.disqualified_date
//          from    mif mf,
//                  ach_trident_detail ad,
//                  ach_trident_statement ats
//          where   mf.bank_number in (3941, 3942, 3943, 3858)
//                  and mf.merchant_number = ad.merchant_number
//                  and ad.post_date >= sysdate-1
//                  and ad.disqualified = 'Y'
//                  and ad.trace_number = ats.trace_number
//                  and nvl(ats.disqualified,'N') != 'Y'                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ad.trace_number,\n                ad.disqualified_date\n        from    mif mf,\n                ach_trident_detail ad,\n                ach_trident_statement ats\n        where   mf.bank_number in (3941, 3942, 3943, 3858)\n                and mf.merchant_number = ad.merchant_number\n                and ad.post_date >= sysdate-1\n                and ad.disqualified = 'Y'\n                and ad.trace_number = ats.trace_number\n                and nvl(ats.disqualified,'N') != 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.FixDisquals",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.FixDisquals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:43^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        disquals.add(new DisqualItem(rs));
      }
      
      rs.close();
      it.close();
      
      log.debug("Found " + disquals.size() + " broken disquals");
      
      for( int i = 0; i < disquals.size(); ++i )
      {
        DisqualItem di = (DisqualItem)disquals.elementAt(i);
        
        /*@lineinfo:generated-code*//*@lineinfo:61^9*/

//  ************************************************************
//  #sql [Ctx] { update  ach_trident_statement
//            set     disqualified = 'Y',
//                    disqualified_date = :di.DisqualDate
//            where   trace_number = :di.TraceNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  ach_trident_statement\n          set     disqualified = 'Y',\n                  disqualified_date =  :1 \n          where   trace_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.FixDisquals",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,di.DisqualDate);
   __sJT_st.setLong(2,di.TraceNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:67^9*/
      }
      
      log.debug("all fixed");
      commit();
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( retVal );
  }
  
  public class DisqualItem
  {
    public java.sql.Date  DisqualDate = null;
    public long           TraceNumber = 0L;
    
    public DisqualItem( ResultSet rs )
    {
      try
      {
        DisqualDate = rs.getDate("disqualified_date");
        TraceNumber = rs.getLong("trace_number");
      }
      catch(Exception e)
      {
        logEntry("DisqualItem()", e.toString());
      }
    }
  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    
    FixDisquals worker = new FixDisquals();
    
    worker.execute();
  }
}/*@lineinfo:generated-code*/