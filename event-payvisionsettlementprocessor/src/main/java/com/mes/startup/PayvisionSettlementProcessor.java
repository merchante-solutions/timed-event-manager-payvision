/*@lineinfo:filename=PayvisionSettlementProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.File;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.sftp.SftpFile;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.payvision.SettlementLoad;
import com.mes.payvision.SettlementParser;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;

public class PayvisionSettlementProcessor extends EventBase
{
  static Logger log = Logger.getLogger(PayvisionSettlementProcessor.class);
  
  private String          filename            = "";
  private String          loadFilename        = "";
  private String          arcFilename         = "";
  private boolean         TestMode            = false;
  private SettlementLoad  load                = null;
  private String          loadResult          = "";

  
  ArrayList clientList = new ArrayList();
  
  public PayvisionSettlementProcessor( )
  {
    PropertiesFilename = "payvision-settlement.properties";
  }
  
  public void setFilename( String fname )
  {
    filename = fname;
  }
  
  public void setLoadFilename( String fname )
  {
    loadFilename = fname;
  }
  
  public void processFile()
  {
    boolean result      = false;
    boolean ftpSuccess  = false;
    String  success     = "N";
    String  progress    = "start of method";
    
    try
    {
      connect();
      
      if( !loadFilename.equals("") )
      {
        // get list of files from server
        String host = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_HOST);
        String user = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_USER);
        String pwd  = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_PASSWORD);
        
        String serchFilename = loadFilename.substring(5); 
        StringBuffer dirPath = new StringBuffer();
        dirPath.append("Out/");
        dirPath.append( loadFilename.substring(0,4) );
        
        log.info("dirPath: " + dirPath);
        log.info("loadFilename: " + loadFilename);
        log.info("serchfilename: " + serchFilename);
        
        //get merchant directory list
        log.debug("Preparing merchant directory list");
        log.debug("creating new sftp ");      
        
        //path - Out/<yyyy>
        Sftp secureFTP = getSftp(host, user, pwd, dirPath.toString(), false);
        EnumerationIterator enumi = new EnumerationIterator();      
        Iterator it = enumi.iterator(secureFTP.getNameListing());

        ArrayList merchantDirList = new ArrayList();
        while( it.hasNext() )
        {
          filename = (String)it.next();
          merchantDirList.add(filename);
        }
        log.info("Merchant list created");
        
        secureFTP.disconnect();
        
        //get client list
        clientList = getClientList(merchantDirList , clientList, host, user, pwd, dirPath.toString());
        log.info("Client list created");
        
        //iterate over client list to get files
        boolean found = false;
        for( int idx = 0; idx < clientList.size(); ++idx )
        {
          if( found )
          {
            break;
          }
          
          String path = (String)clientList.get(idx);
          
          log.debug("host: " +  host);
          log.debug("user: " +  user);
          //log.debug("pwd : " +  pwd);

          Iterator i = getSftpDirList(host, user, pwd, path, false);
          while( i.hasNext() )
          {
            filename = (String)i.next();          
            log.debug("SftpDirList filename: " + filename);
            
            if( !filename.equals(serchFilename) )
            {
              continue;
            }
            
            try
            {
              log.debug("instantiating SshParameters for downloading " + serchFilename);
              
              // establish connection to payvision server
              Security.addProvider((Provider)new BouncyCastleProvider());
              SshParameters params = 
                new SshParameters(MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_HOST),
                                  MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_USER),
                                  MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_PASSWORD));
              
              log.debug("instantiating new Sftp object");
              Sftp sftp = new Sftp(params);
      
              log.debug("connecting...");
              progress = "Connecting to sftp server";
              sftp.connect();
              
              // change to proper subdirectory
              log.debug("setting directory");
              progress = "setting subdirectory";
              sftp.setDir(path);
              
              // set ascii mode
              log.debug("setting ASCII mode");
              progress = "Setting ASCII mode";
              sftp.setAscii();
              
              // retrieving file
              log.debug("retrieving " + filename );
              progress = "retrieving file for today (" + filename + ")";
              arcFilename = buildFilename("pvsetl3941");
              sftp.download(arcFilename, filename);
              
              log.debug("disconnecting");      
              progress = "disconnecting from site";
              sftp.disconnect();
        
              // process file
              log.debug("processing file");
              progress = "processing file";
              load = SettlementParser.parse(new File(arcFilename));
        
              loadResult = load.toString();   
              
              found = true;
              ftpSuccess = true;
            }
            catch(com.jscape.inet.sftp.SftpAuthenticationException authExcept)
            {
              log.debug(authExcept.toString());
              loadResult = authExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
            catch(com.jscape.inet.sftp.SftpFileNotFoundException notFoundExcept)
            {
              log.debug(notFoundExcept.toString());
              loadResult = notFoundExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
            catch(com.jscape.inet.sftp.SftpPermissionDeniedException permissionExcept)
            {
              log.debug(permissionExcept.toString());
              loadResult = permissionExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
            
            if( ftpSuccess )
            {
              // send success email
              log.debug("sending success email");
              sendSuccessEmail(arcFilename, loadResult);
          
              // archive file on kirin
              log.debug("archiving file");
              archiveDailyFile(arcFilename);
              success = "Y";
            }
            else
            {
              // remove file from filesystem if present
              log.debug("deleting partial file");
              (new File(arcFilename)).delete();
              success = "N";
            }
            
            log.debug("updating database");
            log.debug("insert payvision_setl_process loadFilename: " + loadFilename);
            /*@lineinfo:generated-code*//*@lineinfo:219^13*/

//  ************************************************************
//  #sql [Ctx] { insert into payvision_setl_process
//                (
//                  load_filename,
//                  date_processed,
//                  result,
//                  success,
//                  arc_filename
//                )
//                values
//                (
//                  :loadFilename,
//                  sysdate,
//                  :loadResult,
//                  :success,
//                  :arcFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into payvision_setl_process\n              (\n                load_filename,\n                date_processed,\n                result,\n                success,\n                arc_filename\n              )\n              values\n              (\n                 :1 ,\n                sysdate,\n                 :2 ,\n                 :3 ,\n                 :4 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.PayvisionSettlementProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadResult);
   __sJT_st.setString(3,success);
   __sJT_st.setString(4,arcFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^13*/
            break;
          } // while
        }   // for
      }
    }
    catch(Exception e)
    {
      log.debug("processFile() filename:" + loadFilename + " progress:" + progress + " " + e.toString());
      logEntry("processFile() filename:" + loadFilename + " progress:" + progress, e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean execute()
  {
    boolean result      = false;
    boolean ftpSuccess  = false;
    
    String  success = "N";
    
    String  progress  = "start of method";
    
    try
    {
      connect();
      
      // get list of filenames from server
      Calendar cal = Calendar.getInstance();
      String loadFilenameBase = DateTimeFormatter.getFormattedDate(cal.getTime(), "yyyy") + "_";
      
      // get list of files from server
      String host = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_HOST);
      String user = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_USER);
      String pwd  = MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_PASSWORD);

      
      StringBuffer dirPath = new StringBuffer();
      dirPath.append("Out/");
      dirPath.append(DateTimeFormatter.getFormattedDate(cal.getTime(), "yyyy"));
     
      
      //get merchant directory list
      log.debug("Preparing merchant directory list");
      log.debug("creating new sftp ");      
      
      //path - Out/<yyyy>
      Sftp secureFTP = getSftp(host, user, pwd, dirPath.toString(), false);
      EnumerationIterator enumi = new EnumerationIterator();      
      Iterator it = enumi.iterator(secureFTP.getNameListing());

      ArrayList merchantDirList = new ArrayList();
      while( it.hasNext() )
      {
        filename = (String)it.next();
        merchantDirList.add(filename);
      }
      log.info("Merchant list created");
      
      secureFTP.disconnect();
      
      //get client list
      clientList = getClientList(merchantDirList , clientList, host, user, pwd, dirPath.toString());
      log.info("Client list created");
      //iterate over client list to get files
      
      for( int idx = 0; idx < clientList.size(); ++idx )
      {
        String path = (String)clientList.get(idx);        
        log.debug("host: " +  host);
        log.debug("user: " +  user);
        //log.debug("pwd : " +  pwd);

        Iterator i = getSftpDirList(host, user, pwd, path, false);
      
        String loadFilename = "";
        while( i.hasNext() )
        { 
          loadFilename = (String)i.next();
          if (loadFilename.indexOf(".csv") < 0) {
        	  continue;
          }
        
          log.info("checking file: " + filename + " (" + loadFilename + ")");
        
          int fileCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:324^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    payvision_setl_process
//              where   load_filename = :loadFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    payvision_setl_process\n            where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.PayvisionSettlementProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fileCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^11*/
        
          if( fileCount == 0 )
          {
            try
            {
              log.debug("instantiating SshParameters");
              // establish connection to payvision server
              SshParameters params = 
                new SshParameters(MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_HOST),
                                  MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_USER),
                                  MesDefaults.getString(MesDefaults.DK_PAYVISION_FTP_PASSWORD));
                          
              log.debug("instantiating new Sftp object");
              Security.addProvider((Provider)new BouncyCastleProvider());
              Sftp sftp = new Sftp(params);
      
              log.debug("connecting...");
              progress = "Connecting to sftp server";
              sftp.connect();
      
              // change to proper subdirectory
              log.debug("setting directory");
              progress = "setting subdirectory";
              sftp.setDir(path);
      
              // set ascii mode
              log.debug("setting ASCII mode");
              progress = "Setting ASCII mode";
              sftp.setAscii();
      
              // retrieving file
              log.debug("retrieving " + loadFilename );
              progress = "retrieving file for today (" + loadFilename + ")";
              arcFilename = buildFilename("pvsetl3941");
              sftp.download(arcFilename, loadFilename);

              log.debug("disconnecting");      
              progress = "disconnecting from site";
              sftp.disconnect();
        
              // process file
              log.debug("processing file");
              progress = "processing file";
              File file = changeEncoding(arcFilename,loadFilename);
              load = SettlementParser.parse(file);
        
              loadResult = load.toString();   
            
              ftpSuccess = true;     
            }
            catch(com.jscape.inet.sftp.SftpAuthenticationException authExcept)
            {
              loadResult = authExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
            catch(com.jscape.inet.sftp.SftpFileNotFoundException notFoundExcept)
            {
              loadResult = notFoundExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
            catch(com.jscape.inet.sftp.SftpPermissionDeniedException permissionExcept)
            {
              loadResult = permissionExcept.toString();
              setError(loadResult);
              ftpSuccess = false;
            }
          
            if( ftpSuccess )
            {
              // send success email
              log.debug("sending success email");
              sendSuccessEmail(arcFilename, loadResult);
          
              // archive file on kirin
              log.debug("archiving file");
              archiveDailyFile(arcFilename);
              success = "Y";
            }
            else
            {
              // remove file from filesystem if present
              log.debug("deleting partial file");
              (new File(arcFilename)).delete();
              success = "N";
            }
          
            log.debug("updating database");
            log.debug("insert payvision_setl_process loadFilename: " + loadFilename);
            /*@lineinfo:generated-code*//*@lineinfo:419^13*/

//  ************************************************************
//  #sql [Ctx] { insert into payvision_setl_process
//                (
//                  load_filename,
//                  date_processed,
//                  result,
//                  success,
//                  arc_filename
//                )
//                values
//                (
//                  :loadFilename,
//                  sysdate,
//                  :loadResult,
//                  :success,
//                  :arcFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into payvision_setl_process\n              (\n                load_filename,\n                date_processed,\n                result,\n                success,\n                arc_filename\n              )\n              values\n              (\n                 :1 ,\n                sysdate,\n                 :2 ,\n                 :3 ,\n                 :4 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.PayvisionSettlementProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadResult);
   __sJT_st.setString(3,success);
   __sJT_st.setString(4,arcFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:437^13*/
          }
        }
      }
    
      result = true;  
    }
    catch(Exception e)
    {
      logEntry("excecute(" + progress + ")", e.toString());
      log.error(e);
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
  
  /***
   * 
   * Returns client file path list. (Folders where files are stored)
   */
  private ArrayList getClientList(ArrayList merchantDirList, ArrayList clientList, String host, String user, String pwd, String dirPath) throws Exception {
    if(merchantDirList.size() > 0){
     
      StringBuffer folderPath = null;
      
      for(int i=0; i< merchantDirList.size(); i++){
        folderPath = new StringBuffer();
        folderPath.append(dirPath).append("/").append((String)merchantDirList.get(i));
      
        //get sftp connection to path
        Sftp secureFTP = getSftp(host, user, pwd, folderPath.toString(), false);        
        EnumerationIterator enumi = new EnumerationIterator();
        Iterator itr = enumi.iterator(secureFTP.getNameListing("^Merge_.+"));
        String subfoldername = null;
        while (itr.hasNext()) {
            subfoldername = (String) itr.next();
            folderPath.append("/").append(subfoldername);
            secureFTP = getSftp(host, user, pwd, folderPath.toString(), false);
            Iterator itrsubfolder = enumi.iterator(secureFTP.getDirListing());
            while (itrsubfolder.hasNext()) {
                SftpFile sftpFile = (SftpFile) itrsubfolder.next();
                String subfolder = (String) sftpFile.getFilename();
                if (!sftpFile.isDirectory()) {
                    clientList.add(folderPath.toString());
                    continue;
                }
                folderPath.append("/").append(subfolder);
                clientList.add(folderPath.toString());
                folderPath.delete(folderPath.indexOf("/" + subfolder), folderPath.length());
            }
            folderPath.delete(folderPath.indexOf("/" + subfoldername), folderPath.length());
        }
        secureFTP.disconnect();
      }
    }
    return clientList;
  }

  

  private String generatePath(Calendar cal, String client)
  {
    StringBuffer path = new StringBuffer ("Out/");
    try
    {
      // set date to one day in the past
      cal.add(Calendar.DATE, -1);
      
      path.append(DateTimeFormatter.getFormattedDate(cal.getTime(), "yyyy"));
      path.append(client);
      //path.append("/Merchant ESolutions TEST/Merge_1285");
      //path.append("/MeS 2CO/Merge_111"); // old path, changed 9/26/12
    }
    catch(Exception e)
    {
      logEntry("generatePath()", e.toString());
    }
    
    return( path.toString() );
  }
  
  private String generateFilename(Calendar cal)
  {
    StringBuffer result = new StringBuffer("MeS 2CO_epa.");
    
    try
    {
      // set date to one day in the past
      cal.add(Calendar.DATE, -1);
      
      int curJulianDay = Integer.parseInt(DateTimeFormatter.getFormattedDate(cal.getTime(), "DDD"));
      
      result.append(NumberFormatter.getPaddedInt(curJulianDay, 3));
      result.append(".csv");
    }
    catch(Exception e)
    {
      logEntry("generateFielname()", e.toString());
    }
    
    return( result.toString() );
  }
  
  private void sendSuccessEmail(String filename, String result)
  {
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_PAYVISION_SETTLEMENT);
      msg.setSubject("PayVision Settlement File " + filename + " Success");
      msg.setText(result);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendSuccessEmail("+filename+")", e.toString());
    }
    
  }
  
  private File changeEncoding(String archFile , String loadFile) throws Exception {
    File file = new File(archFile);
    if (!loadFile.startsWith("Anixter")) {
      List<String> content = FileUtils.readLines(file, "UTF-16LE");
      for (int i = 0; i < content.size(); i++) {
          content.set(i, content.get(i).replace("\"", ""));
      }
      FileUtils.writeLines(file, content);
    }
    return file;
  }
  
  public static void main(String[] args)
  {
    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_PAYVISION_FTP_HOST,
                MesDefaults.DK_PAYVISION_FTP_USER,
                MesDefaults.DK_PAYVISION_FTP_PASSWORD
        });
      }
    } catch (Exception e) {
      log.error(e.toString());
    }

    SQLJConnectionBase.initStandalone("DEBUG");
    try
    {
      PayvisionSettlementProcessor proc = new PayvisionSettlementProcessor();
      
      if( (args.length >= 2) && args[0].equals("processfile") )
      {
        //args[1]: "2014_2CO Global Ltd (2checkout) B+S_epa.028.csv"
        proc.setLoadFilename(args[1]);
        proc.processFile();
      }
      else
      {
        //proc.filename = args[0];
        //proc.TestMode = true;      
        proc.execute();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    
    Runtime.getRuntime().exit(0);
  }
  
}/*@lineinfo:generated-code*/