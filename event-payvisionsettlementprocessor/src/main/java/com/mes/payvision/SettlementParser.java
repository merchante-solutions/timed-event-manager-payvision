/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/payvision/SettlementParser.java $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.payvision;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.tools.CurrencyCodeConverter;

public class SettlementParser
{
  static Logger log = Logger.getLogger(SettlementParser.class);

  private PayvisionDb db        = new PayvisionDb();
  private SimpleDateFormat sdf  = new SimpleDateFormat("MM/dd/yyyy");
  private SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy");
  private Map rates             = new HashMap();

  private LineNumberReader in;
  private String loadName;
  private int lineCount;
  private String pushedLine;
  private boolean pushFlag;
  private boolean testFlag;

  private SettlementParser(Reader reader, String loadName, boolean testFlag)
  {
    in = new LineNumberReader(reader);
    this.loadName = loadName;
    this.testFlag = testFlag;
  }

  /**
   * A crude one line buffer mechanism to allow one line look ahead.
   */
  private void pushLine()
  {
    --lineCount;
    pushFlag = true;
  }

  private String getNextLine() throws Exception
  {
    String line = null;
    if (pushFlag)
    {
      line = pushedLine;
      pushFlag = false;
    }
    else
    {
      line = in.readLine();
      pushedLine = line;
    }
    if (line != null) ++lineCount;
    return line;
  }

  private void error(String msg)
  {
    throw new RuntimeException("Error at line " + lineCount 
      + " in csv file '" + loadName + "': " + msg);
  }

  /** 
   * Payvision settlement files have started using a new date format:
   *
   * mm-dd-yyyy vs. original mm/ddy/yyyy, so need to support both
   * formats.
   */
  private Date parseDate(String dateStr) {
  
    Date date = sdf.parse(dateStr,new ParsePosition(0));
    if (date == null) {
      date = sdf2.parse(dateStr,new ParsePosition(0));
    }
    if (date == null) {
      throw new RuntimeException("Unable to parse date: '" + dateStr + "'");
    }
    return date;
  }

  /**
   * recordtype = 100 csv header, 200 member batch header, 600 batch detail
   *
   * Header record
   *
   * 100 <name/number csv file>
   */

  private SettlementLoad parseHeader() throws Exception
  {
    String line = getNextLine();
    if (line == null || !line.startsWith("100"))
    {
      error("Missing '100' header record");
    }
    SettlementLoad load = new SettlementLoad();
    String[] fields = line.split(",");
    load.setLoadId(db.getNewSettlementId());
    load.setLoadDate(Calendar.getInstance().getTime());
    load.setLoadFile(loadName);
    load.setResult("SUCCESS");
    load.setDetails("Success.");
    return load;
  }

  /**
   * Detail record
   *
   * 600  <1 member id>
   *      <2 merch acct code>
   *      <3 track mem code>
   *      <4 tran date> 
   *      <5 tran amt> 
   *      <6 numeric currency type> 
   *      <7 card type code> 
   *      <8 op type code>
   *
   * card type codes:              op type codes:
   *
   *  0 unknown                     1 payment
   *  1 mastercard                  2 refund
   *  2 visa                        3 authorize
   *  3 amex                        4 capture
   *  4 diners                      5 void
   *  5 jcb                         6 chargeback
   *  6 carte bleue                 7 referral approval
   *  7 galeria
   *  8 delta
   *  9 laser
   *  10 solo
   *  11 switch
   *  12 enroute
   */

  private SettlementDetail parseDetail(long loadId, String line, Date settleDate)
  {
    SettlementDetail detail = new SettlementDetail();
    String[] fields = line.split(",");
    detail.setLoadId(loadId);
    detail.setMemberId(Integer.parseInt(fields[1]));
    detail.setMerchAcctId(Long.parseLong(fields[2]));
    detail.setTrackMemCode(fields[3] == null ? "0" : fields[3]);
    detail.setSettleDate(settleDate);
    detail.setTranDate(parseDate(fields[4]));
    detail.setTranAmount(fields[5].equals("") ? new BigDecimal(0) : new BigDecimal(fields[5]));
    detail.setCurrencyCode(Integer.parseInt(fields[6]));
    detail.setCardType(Integer.parseInt(fields[7]));
    detail.setTranType(Integer.parseInt(fields[8]));
    return detail;
  }


  /**
   * Summary record
   *
   * 200  <1 member id> 
   *      <2 merch acct code> 
   *      <3 csv creation date>
   *      <4 total amount for member id>
   *      <5 text currency type>
   *      <6 discount fee total (neg)> 
   *      <7 refund/chargeback fee total (neg)> 
   *      <8 net payment total> 
   *      <9 payment total> 
   *      <10 tran count>
   */

  private SettlementSummary parseSummary(long loadId) throws Exception
  {
    String line = getNextLine();
    if (line == null)
    {
      return null;
    }

    if (!line.startsWith("200"))
    {
      error("Missing '200' summary record");
    }

    SettlementSummary summary = new SettlementSummary();
    String[] fields = line.split(",");
    summary.setLoadId(loadId);
    summary.setMemberId(Integer.parseInt(fields[1]));
    summary.setMerchAcctId(Long.parseLong(fields[2]));
    summary.setSettleDate(parseDate(fields[3]));
    summary.setTotalAmount(new BigDecimal(fields[4]));
    summary.setCurrencyCodeStr(fields[5]);
    summary.setDiscountFees(new BigDecimal(fields[6]));
    summary.setOtherFees(new BigDecimal(fields[7]));
    summary.setNetPayment(new BigDecimal(fields[8]));
    summary.setPaymentTotal(new BigDecimal(fields[9]));
    summary.setTranCount(Integer.parseInt(fields[10]));

    // load details
    line = getNextLine();
    while ( line != null && !line.startsWith("200") )
    {
      if ( line.startsWith("600") ) // detail record, process
      {
        summary.addDetail(parseDetail(loadId,line,summary.getSettleDate()));
      }
      line = getNextLine();
    }

    // not end of file, must be start of next summary batch, push summary back
    if (line != null)
    {
      pushLine();
    }
    return summary;
  }

  /**
   * Fetches base conversion rate.  Looks to rate cache first.
   */
  private BigDecimal getBaseRate(String fromCur, Date rateDate)
  {
    // generate a key for this rate, look for it in the cache
    String key = "394100000" + fromCur + rateDate;
    BigDecimal rate = (BigDecimal)rates.get(key);
    if (rate == null)
    {
      rate = db.getBaseConversionRateToUsd(fromCur,rateDate);
      rates.put(key,rate);
    }
    return rate;
  }

  private String getRateStr(int intCur)
  {
    CurrencyCodeConverter ccc = CurrencyCodeConverter.getInstance();
    return ccc.numericCodeToAlphaCode(intCur);
  }

  /**
   * Fetches rate.  Looks to the cache first, then falls back to a db lookup.
   */
  private BigDecimal getTranRate(int memberId, String fromCur, Date rateDate)
  {
    // generate a key for this rate, look for it in the cache
    String key = ""+memberId + fromCur + rateDate;
    BigDecimal rate = (BigDecimal)rates.get(key);
    if (rate == null)
    {
      rate = db.getMemberConversionRateToUsd(memberId,fromCur,rateDate);
      rates.put(key,rate);
    }
    return rate;
  }
  private BigDecimal getTranRate(int memberId, int fromCur, Date rateDate)
  {
    return getTranRate(memberId,getRateStr(fromCur),rateDate);
  }

  /**
   * 1. Convert settlement record discount fee to us dollars using base 
   * conversion rate.
   */
  private BigDecimal getUsdFeeTotal(SettlementSummary summary)
  {
    // get base conversion rate for summary fee currency type
    // and convert the fee amount to usd
    BigDecimal rate = 
      getBaseRate(summary.getCurrencyCodeStr(),summary.getSettleDate());
    if (rate == null)
    {
      throw new NullPointerException("Unable to convert discount fees in " 
        + summary.getCurrencyCodeStr() + " to USD");
    }
    BigDecimal usdFeeTotal = summary.getDiscountFees().divide(rate, 2, RoundingMode.HALF_UP);
    usdFeeTotal = usdFeeTotal.setScale(2, RoundingMode.HALF_UP);

    // store the dollar amt in the summary
    summary.setUsdDiscountFees(usdFeeTotal);

    return usdFeeTotal;
  }

  public class TranAnalysis
  {
    HashMap usdTranAmounts  = new HashMap();

    List captureTranList    = new ArrayList();
    List refundTranList     = new ArrayList();
    List cbTranList         = new ArrayList();
    List otherTranList      = new ArrayList();

    BigDecimal captureTotal = new BigDecimal(0);
    BigDecimal refundTotal  = new BigDecimal(0);
    BigDecimal cbTotal      = new BigDecimal(0);
    BigDecimal otherTotal   = new BigDecimal(0);

    public int tranCount() 
    {
      return captureCount() + refundCount() + cbCount() + otherCount();
    }
    public int captureCount()
    {
      return captureTranList.size();
    }
    public int refundCount()
    {
      return refundTranList.size();
    }
    public int cbCount()
    {
      return cbTranList.size();
    }
    public int otherCount()
    {
      return otherTranList.size();
    }
  };
  
  /**
   * 2. Scan through transactions, total by tran type, convert tran amounts 
   * to usd.
   */
  private TranAnalysis getTranAnalysis(SettlementSummary summary)
  {
    // create a tran analysis object to store the various data
    // accumulated in this function
    TranAnalysis ta = new TranAnalysis();

    // scan the summary record's detail list
    for (Iterator j = summary.getDetails().iterator(); j.hasNext();)
    {
      SettlementDetail detail = (SettlementDetail)j.next();

      // convert tran amount using merchant conversion rate
      BigDecimal tranRate 
        = getTranRate(detail.getMemberId(),detail.getCurrencyCode(),
                        detail.getSettleDate());
      BigDecimal usdTranAmount = detail.getTranAmount().divide(tranRate,2,RoundingMode.HALF_UP);

      ta.usdTranAmounts.put(detail,usdTranAmount);

      int tranType = detail.getTranType();
      if (tranType == detail.TT_CAPTURE)
      {
        ta.captureTotal = ta.captureTotal.add(usdTranAmount);
        ta.captureTranList.add(detail);
      }
      else if (tranType == detail.TT_REFUND)
      {
        ta.refundTotal = ta.refundTotal.add(usdTranAmount);
        ta.refundTranList.add(detail);
      }
      else if (tranType == detail.TT_CHARGEBACK)
      {
        ta.cbTotal = ta.cbTotal.add(usdTranAmount);
        ta.cbTranList.add(detail);
      }
      else
      {
        ta.otherTotal = ta.otherTotal.add(usdTranAmount);
        ta.otherTranList.add(detail);
      }
    }

    // truncate the totals
    ta.captureTotal = ta.captureTotal.setScale(2, RoundingMode.HALF_UP);
    ta.refundTotal  = ta.refundTotal.setScale(2, RoundingMode.HALF_UP);
    ta.cbTotal      = ta.cbTotal.setScale(2, RoundingMode.HALF_UP);
    ta.otherTotal   = ta.otherTotal.setScale(2, RoundingMode.HALF_UP);

    return ta;
  }

  /**
   * 5. Takes a list of tran details, a map detail->usd tran amounts,
   * the tran total amount and the tran total fee amount.  The
   * fee amount is distributed over the details proportionate to the
   * tran amounts.  
   */
  private void distributeFees(List tranList, Map usdTranAmounts,
    BigDecimal tranTotal, BigDecimal feeTotal)
  {
    BigDecimal distFeeTotal = new BigDecimal(0);
    for (Iterator j = tranList.iterator(); j.hasNext();)
    {
      // proportionate distribution of fees:
      // fee = (tran amt / tran total) * tran fee total
      SettlementDetail detail = (SettlementDetail)j.next();
      BigDecimal usdFee = (BigDecimal)usdTranAmounts.get(detail);
      usdFee = usdFee.divide(tranTotal, 6, RoundingMode.HALF_UP);
      usdFee = usdFee.multiply(feeTotal).setScale(2, RoundingMode.HALF_UP);
      distFeeTotal = distFeeTotal.add(usdFee);
      detail.setUsdDiscountFee(usdFee); 
    }
  }

  private BigDecimal sumTranFees(List tranList)
  {
    // determine initial sum of fees
    BigDecimal feeTotal = new BigDecimal(0);
    for (Iterator i = tranList.iterator(); i.hasNext();)
    {
      SettlementDetail detail = (SettlementDetail)i.next();
      feeTotal = feeTotal.add(detail.getUsdDiscountFee());
    }
    return feeTotal;
  }

  /**
   * 6. Adjust fees to equal desired fee total.  Returns the adjusted 
   * total amount.
   */
  private BigDecimal adjustFees(List origTranList, BigDecimal usdFeeTotal)
  {
    // make sure list isn't empty
    if (origTranList.isEmpty())
    {
      log.warn("Empty transaction list in adjustFees(), no adjustment possible.");
      return usdFeeTotal;
    }

    // get a copy of the tran list to reorder
    List tranList = new ArrayList();
    tranList.addAll(origTranList);

    // determine initial sum of fees
    BigDecimal actualTotal = sumTranFees(tranList);

    // a flag to detect scenario when no fees are adjustable
    boolean isStuck = false;

    // adjust by penny increments
    BigDecimal adjustAmount = new BigDecimal("0.01");

    // nudge fee amounts up or down according to the discrepency
    while (!actualTotal.equals(usdFeeTotal) && !isStuck)
    {
      isStuck = true;

      // randomize list order
      // NOTE: may want to change this to sort highest absolute
      // value to lowest to make adjustments on largest fees
      Collections.shuffle(tranList);

      // make adjustments
      for (Iterator i = tranList.iterator(); i.hasNext();)
      {
        SettlementDetail detail = (SettlementDetail)i.next();
        BigDecimal usdFee = detail.getUsdDiscountFee();

        // make sure fee amount is larger than adjustment amount
        // this simplistic check means one penny fees can't adjust
        // upward, but that is probably for the best
        if (usdFee.abs().compareTo(adjustAmount) > 0)
        {
          // actualTotal < usd
          int compare = actualTotal.compareTo(usdFeeTotal);
          if (compare < 0)
          {
            detail.setUsdDiscountFee(usdFee.add(adjustAmount));
            actualTotal = actualTotal.add(adjustAmount);
            isStuck = false;
          }
          else if (compare > 0)
          {
            detail.setUsdDiscountFee(usdFee.subtract(adjustAmount));
            actualTotal = actualTotal.subtract(adjustAmount);
            isStuck = false;
          }
          else
          {
            // currently, don't allow to get stuck
            isStuck = false;
            break;
          }
        }
      }
    }

    // worst case scenario, adjust a single random transaction
    // to a value that makes the actual total match the desired total
    if (isStuck)
    {
      SettlementDetail detail = (SettlementDetail)tranList.get(0);
      BigDecimal usdFee = detail.getUsdDiscountFee();
      log.warn("Unable to adjust fees subtley, adjusting original fee:"
        + " USD " + usdFee);
      adjustAmount = usdFeeTotal.subtract(actualTotal);
      log.warn("Adjustment amount: " + adjustAmount);
      usdFee = usdFee.add(adjustAmount);
      detail.setUsdDiscountFee(usdFee);
      log.warn("Adjusted detail: " + detail);
    }

    return actualTotal;
  }

  /**
   * Outline of fee generation:
   *
   * Positive amounts reflect money paid to the merchant by Payvision.  
   * Negative amounts are paid by the merchant to Payvision.  Captures and
   * refund "fees" are positive, refunds and capture fees are negative.
   *
   *  Total amount 
   *    - net amount of capture, refund, cb and other activity (pos/neg)
   *  Discount fees - discount fees on captures (+ cb as well?) (neg)
   *  Refund/other fees - discount fees on refunds, cbs, others (pos)
   *  Net pay total - total amount + disc fees + ref/oth fees (pos/neg)
   *  Payment total - same as net pay usually (pos/neg)
   *
   * 1. Convert net discount fee amount in summary record to usd using base 
   * conversion rate.  This is the sum total of negative capture fees and
   * positive refund fees.
   *
   * 2. Scan through captures and refunds, convert each of these to usd using
   * merchant's conversion rates.  Sum refunds and captures separately.
   *
   * 3. The net sum of these two subtotals is divided into the net discount
   * fee.  This ratio determines an estimated percentage rate to apply to each
   * transaction dollar amount to estimate the fees associated with it (5), 
   * and to determine refund and capture fee subtotals (4).
   *
   * NOTE: if the net tran amount is zero in step 3 the actual ration cannot 
   * be determined since it becomes division by zero.  In this case an
   * arbitrary percentage rate must be substituted, currently this is 3%.
   *
   * 4. The rate is applied to the refund and capture subtotals to determine
   * an estimated total negative capture and positive refund fee total for 
   * each of these subcategories of transactions.  The sum of these should add 
   * up to the net discount fee in the summary record (1).
   *
   * 5. The transactions are iterated through and the rate is applied and the
   * resulting fee amount is stored with each transaction.  
   *
   * 6. Adjust fees by penny increments until the net sum is equal to the
   * summary record net discount amount.
   */
  private void generateTransactionFees(SettlementLoad load)
  {
    for (Iterator i = load.getSummaries().iterator(); i.hasNext();)
    {
      SettlementSummary summary = (SettlementSummary)i.next();

      // 1. convert net discount fee in summary to usd
      BigDecimal usdFeeTotal = getUsdFeeTotal(summary);

      // 2. analyze transaction data (convert details to usd, get
      //    subtotals and totals in tran analysis object)
      TranAnalysis ta = getTranAnalysis(summary);

      // 3. determine effective discount rate to appy to transactions
      //    fee rate = fee amount / (cap + ref + cb)
      BigDecimal feeRate = ta.captureTotal
                            .add(ta.refundTotal).add(ta.cbTotal);
      if (feeRate.compareTo(new BigDecimal(0)) != 0 &&
          usdFeeTotal.compareTo(new BigDecimal(0)) != 0)
      {
        feeRate = usdFeeTotal.divide(feeRate, 6, RoundingMode.HALF_UP);
      }
      // if by some chance the net detail totals to zero or fees are zero
      // can't calculate the rate, use an arbitrary rate
      else
      {
        log.debug("Using dummy rate due to 0 net capture + refund + cb "
          + " or zero net fees");
        feeRate = new BigDecimal("0.015");
      }

      // 4. calculate capture and refund fee subtotals: subTotal * feeRate
      BigDecimal refundFeeTotal = ta.refundTotal.multiply(feeRate).setScale(2, RoundingMode.HALF_UP);
      BigDecimal captureFeeTotal = ta.captureTotal.multiply(feeRate).setScale(2, RoundingMode.HALF_UP);
      BigDecimal cbFeeTotal = ta.cbTotal.multiply(feeRate).setScale(2, RoundingMode.HALF_UP);

      // 5. distribute fees
      distributeFees(
        ta.captureTranList,ta.usdTranAmounts,ta.captureTotal,captureFeeTotal);
      distributeFees(
        ta.refundTranList,ta.usdTranAmounts,ta.refundTotal,refundFeeTotal);
      distributeFees(
        ta.cbTranList,ta.usdTranAmounts,ta.cbTotal,cbFeeTotal);

      // 6. adjust fees
      List tranList = new ArrayList(ta.captureTranList);
      tranList.addAll(ta.refundTranList);
      tranList.addAll(ta.cbTranList);
      BigDecimal unadjustedTotalFees = sumTranFees(tranList);
      BigDecimal adjustedTotalFees = adjustFees(tranList,usdFeeTotal);

      // list tran fees for debug
      log.debug("Adjusted transactions:");
      tranList.addAll(ta.otherTranList);
      for (Iterator j = tranList.iterator(); j.hasNext();)
      {
        SettlementDetail detail = (SettlementDetail)j.next();
        BigDecimal tranAmount = (BigDecimal)ta.usdTranAmounts.get(detail);
        BigDecimal usdFee = detail.getUsdDiscountFee();
        log.debug("  " + detail.getTranTypeStr() + " USD " + tranAmount
          + " fee " + (usdFee != null ? ""+usdFee : "--") + " ( "
          + getRateStr(detail.getCurrencyCode()) + " " 
          + detail.getTranAmount() + " )");
      }

      log.debug("***********************************************************");
      log.debug("Total summary discount fees: " 
        + summary.getCurrencyCodeStr() + " " + summary.getDiscountFees());
      log.debug("Total summary discount fees: USD " + usdFeeTotal);
      log.debug("Chargeback count:            " + ta.cbTranList.size());
      log.debug("Total chargeback amount:     " + ta.cbTotal);
      log.debug("Refund count:                " + ta.refundTranList.size());
      log.debug("Total refund amount:         " + ta.refundTotal);
      log.debug("Capture count:               " + ta.captureTranList.size());
      log.debug("Total capture amount:        " + ta.captureTotal);
      log.debug("Effective fee % rate:        " + feeRate);
      log.debug("Target cb fees:              " + cbFeeTotal);
      log.debug("Target refund fees:          " + refundFeeTotal);
      log.debug("Target capture fees:         " + captureFeeTotal);
      log.debug("Unadjusted fees:             " + unadjustedTotalFees);
      log.debug("Adjusted fees:               " + adjustedTotalFees);
      log.debug("***********************************************************");

    }
  }

  private SettlementLoad run() throws Exception
  {
    // file header record
    SettlementLoad load = parseHeader();

    log.debug("Parsing csv file " + load.getLoadFile());

    // summary records
    SettlementSummary summary = null;
    do
    {
      summary = parseSummary(load.getLoadId());
      if (summary != null)
      {
        load.addSummary(summary);
      }
    } while (summary != null);

    generateTransactionFees(load);

    if (!testFlag)
    {
      db.saveSettlementLoad(load);
    }
    else
    {
      log.debug("(DB save skipped due to test flag)");
    }

    log.debug("*** PARSE COMPLETE ***");

    return load;
  }

  public static SettlementLoad parse(Reader reader, String loadName, boolean testFlag) throws Exception
  {
    try
    {
      SettlementParser parser = new SettlementParser(reader,loadName,testFlag);
      return parser.run();
    }
    catch (Exception e)
    {
      log.error("Parse error: " + e);
      e.printStackTrace();
    }
    return null;
  }
  public static SettlementLoad parse(Reader reader, String loadName) throws Exception
  {
    return parse(reader,loadName,false);
  }
  public static SettlementLoad parse(InputStream in, String loadName) throws Exception
  {
    return parse(new InputStreamReader(in),loadName);
  }
  public static SettlementLoad parse(File file) throws Exception
  {
    return parse(new FileReader(file),""+file);
  }
  public static SettlementLoad parse(File file, boolean testFlag) throws Exception
  {
    return parse(new FileReader(file),""+file,testFlag);
  }
  
  public static void main(String[] args)
  {
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone();
      
      SettlementLoad load = parse( new File(args[0]) );
      System.out.println(load.toString());
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
    finally
    {
    }
  }
}