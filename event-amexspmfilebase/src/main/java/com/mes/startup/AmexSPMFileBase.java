/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac/src/main/com/mes/startup/AmexSPMFileBase.java $

  Description:

  Last Modified By   : $Author: sceemarla $
  Last Modified Date : $Date: 2015-06-10 15:10:05 -0700 (Wed, 10 Jun 2015) $
  Version            : $Revision:  $

  Change History:
     See SVN database

  Copyright (C) 2000-2014,2015 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;

public class AmexSPMFileBase extends AmexFileBase {

    FlatFileRecord ffd = null;
    BufferedWriter out = null;
    long rec_num = 0;
    String fname = null;
    String fields[] = null;

    public AmexSPMFileBase(){

    	CardTypeFileDesc            = "Amex SPM file";
    	DkOutgoingHost              = MesDefaults.DK_AMEX_OUTGOING_HOST;
        DkOutgoingUser              = MesDefaults.DK_AMEX_OUTGOING_USER;
        DkOutgoingPassword          = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
        DkOutgoingPath              = MesDefaults.DK_AMEX_SPM_OUTGOING_PATH;
        SettlementAddrsNotify       = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
        SettlementAddrsFailure      = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
        DkOutgoingUseBinary         = false;
        DkOutgoingSendFlagFile      = true;
    }

    protected boolean processTransactions() {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String seNum = null;
        log.debug("Starting AmexSPMFileBase:ProcessTransactions().....");
        
        String query = "select distinct mif.merchant_number  as seller_id,"
                + "        mif.fdr_corp_name   as seller_legal_name,"
                + "        mif.dba_name        as seller_dba_name,"
                + "        mif.federal_tax_id  as seller_fed_taxid,"
                + "        mif.sic_code        as seller_mcc,"
                + "        mif.dmaddr          as seller_address," 
                + " nvl(mif.addr1_line_1,'')   as address1,"
                + "        mif.dmcity          as seller_city,"
                + "        mif.dmstate         as seller_state,"
                + "        mif.dmzip           as seller_zip,"
                + "        mif.phone_1         as seller_bus_phone,"
                + "   me.merch_email_address   as seller_email,"
                + "   me.merch_web_url         as seller_url,"
                + "        mif.owner_name      as owner_name,"
                + "   nvl(dukpt_decrypt_wrapper(mif.ssn_enc), mif.ssn) as owner_ssn,"
                + "        null                as owner_dob,"
                + "        mif.dmaddr          as owner_address,"
                + "        mif.dmcity          as owner_city,"
                + "        mif.dmstate         as owner_state,"
                + "        mif.dmzip           as owner_zip,"
                + "        irs.match_result    as irs_result,"
                + " decode( me.bustype_code,"
                + "          1, 'S',"
                + "          2, 'D',"
                + "          3, 'Q',"
                + "          4, 'D',"
                + "          5, 'F',"
                + "          6, 'F',"
                + "          7, 'G',"
                + "          8, 'M',"
                + "         10, 'D',"
                + "         11, 'Q',"
                + "         12, 'E',"
                + "         13, 'N',"
                + "         14, 'R',"
                + "         15, 'X',"
                + "        'S')                as owner_type_indicator, "
                + "  decode(mif.date_stat_chgd_to_dcb, null,null,'N') as se_dtl_status_code, "
                + "  mif.date_stat_chgd_to_dcb as se_dtl_status_changedate  "
                + "  from  mif, merchant me, mif_changes mc, irs_match_status irs"
                + "  where mif.merchant_number = me.merch_number"
                + "  and mif.merchant_number = irs.merchant_number"
                + "  and mif.test_account = 'N'"
                + "  and  mif.dmstate <> 'PR' "
 //             + "  and mif.activation_date is not null "
                + "  and mif.merchant_number = mc.merchant_number "
                + "  and mif.damexse = ?" 
                + "  and ( trunc(mif.amex_conversion_date) = trunc(sysdate-1) or mif.date_stat_chgd_to_dcb =trunc(sysdate-1)" 
                + "  or (TRUNC(mc.date_changed) = trunc(sysdate-1) and (mc.change_details like '%corporatename%' "
                + "  or  mc.change_details like '%dbaname%' or  mc.change_details like '%federaltaxid%' "
                + "  or  mc.change_details like '%siccode%' or  mc.change_details like '%addr100%'"
                + "  or  mc.change_details like '%addr200%' or  mc.change_details like '%city00%'"
                + "  or  mc.change_details like '%state00%' or  mc.change_details like '%zip00%' "
                + "  or  mc.change_details like '%phone1%'  or  mc.change_details like '%ownername%'"
                + "  or  mc.change_details like '%ssn%'     or  mc.change_details like '%amexOptBlueMktIndicator%')))" 
                + "  order by seller_id , se_dtl_status_changedate desc ";
        
        
        try {
            connect(true);
            generateFile();
            ps = con.prepareStatement(query);
            for (int i = 0; i < EventArgsVector.size(); i++) {
                seNum = (String) EventArgsVector.elementAt(i);
                log.debug("processing transactions for SE:" + seNum);
                ps.setString(1, seNum);
                rs = ps.executeQuery();
                buildHeader(out, seNum);
                buildDetail(out, rs);
                buildTrailer(out);
            }
            out.flush();
            out.close();
            sendFile(fname);
        } catch (Exception e) {
            logEntry("processTransactions()", e.toString());
            return false;
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception e) {

            }

        }
        return true;
    }

    protected void generateFile() {
        log.debug("generating file name...");
        try {
            fname = generateFilename("am_spm", ".dat");
            loadFilenameToLoadFileId(fname);
            out = new BufferedWriter(new FileWriter(fname, false));
        } catch (Exception e) {
             logEntry("generateFile()", e.toString());
        }
    }

    protected void buildHeader(BufferedWriter out, String seNum) {
        log.debug("building header for SE#" + seNum);
        rec_num = 1;
        try {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_HEADER);
            ffd.setFieldData("se_number", seNum);
            ffd.setFieldData("file_creation_date", Calendar.getInstance().getTime());
            ffd.setFieldData("file_creation_time", Calendar.getInstance().getTime());
            ffd.setFieldData("file_transmission_date", Calendar.getInstance().getTime());
            ffd.setFieldData("file_transmission_time", Calendar.getInstance().getTime());
            out.write(ffd.spew());
            out.newLine();
        } catch (Exception e) {
            logEntry("buildHeader()", e.toString());
        }
    }

    protected void buildDetail(BufferedWriter out, ResultSet rs) {
        log.debug("building detail record");
        try {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_DETAIL);
            while(rs.next()){

                ffd.setAllFieldData(rs);
                ffd.setFieldData("record_number", ++rec_num);
                if(rs.getString("owner_ssn") != null)
                    ffd.setFieldData("owner_ssn",  getPaddedString( rs.getString("owner_ssn").trim(), 9, "0"));
                if(rs.getString("seller_zip") != null)
                {
                    ffd.setFieldData("seller_zip", getPaddedString( rs.getString("seller_zip").trim(), 9, "0"));
                    ffd.setFieldData("owner_zip",  getPaddedString( rs.getString("seller_zip").trim(), 9, "0"));
                }
                if(rs.getString("seller_fed_taxid") != null)
                    ffd.setFieldData("seller_fed_taxid",  getPaddedString( rs.getString("seller_fed_taxid").trim(), 9, "0"));

                //Split owner name to first name and last name
                String owner = rs.getString("owner_name");
                if (owner != null) {
                    String st[] = owner.split(" ", 2);
                    ffd.setFieldData("owner_firstname", st[0].trim());
                    if (st.length == 2)
                        ffd.setFieldData("owner_lastname", st[1].trim());
                    else
                        ffd.setFieldData("owner_lastname", "UNKNOWN");
                }
                String addr = rs.getString("seller_address");
                if(  addr != null &&   (addr.equals("") || addr.contains("P.O.B.") 
                        || addr.contains("P.O. BOX") || addr.contains("P.O.BOX") 
                        || addr.contains("PO BOX") || addr.contains("P O BOX")
                        || addr.contains("P. O. BOX") || addr.contains("P. O, BOX"))){
                    String addr1 = rs.getString("address1");
                    if( addr1 != null && (addr1.equals("") || addr1.contains("P.O.B.") 
                            || addr1.contains("P.O. BOX") || addr1.contains("P.O.BOX") 
                            || addr1.contains("PO BOX") || addr1.contains("P O BOX")
                            || addr1.contains("P. O. BOX")|| addr1.contains("P. O, BOX"))){
                        ffd.setFieldData("seller_address", rs.getString("seller_dba_name"));
                        ffd.setFieldData("owner_address", rs.getString("seller_dba_name"));
                    }else{
                        ffd.setFieldData("seller_address", addr1);
                        ffd.setFieldData("owner_address", addr1);
                    }
                    
                }
                if(rs.getString("owner_type_indicator").equals("D") && rs.getInt("irs_result") == 7)
                	ffd.setFieldData("owner_type_indicator", "E");
                else if(rs.getString("owner_type_indicator").equals("M") && rs.getInt("irs_result") == 7)
                	ffd.setFieldData("owner_type_indicator", "N");
                else if(rs.getString("owner_type_indicator").equals("Q") && rs.getInt("irs_result") == 7)
                	ffd.setFieldData("owner_type_indicator", "R");
                
                out.write(ffd.spew(2));
                out.newLine();
            }

        } catch (Exception e) {
            logEntry("buildDetail()", e.toString());
        }
    }

    protected void buildTrailer(BufferedWriter out) {
        log.debug("building trailer");
        try {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_TRAILER);
            ffd.setFieldData("record_number", ++rec_num);
            ffd.setFieldData("total_count", rec_num);
            out.write(ffd.spew());
            out.newLine();
        } catch (Exception e) {
            logEntry("buildTrailer()", e.toString());
        }
    }

    protected String getPaddedString(String st, int len, String fill){
        String res = "";
        if(st.length() < len){
            while(st.length() < len) {
                st += fill;
            }
        }
        return st;
    }

    public static void main(String args[]) {
        AmexSPMFileBase test = null;
        try {

            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_AMEX_OUTGOING_HOST, MesDefaults.DK_AMEX_OUTGOING_USER,
                        MesDefaults.DK_AMEX_OUTGOING_PASSWORD, MesDefaults.DK_AMEX_SPM_OUTGOING_PATH
                });
            }

            test = new AmexSPMFileBase();
            test.setEventArgs(args);
            test.execute();
        } catch (Exception e) {
            log.debug(e.toString());
        } finally {
            try {
                test.cleanUp();
            } catch (Exception e) {
            }
            Runtime.getRuntime().exit(0);
        }
    }

}
