package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;


public class MCMerchLocationFeeLoader extends EventBase
{
  static Logger    log                = Logger.getLogger(MCMerchLocationFeeLoader.class);

  private Date     ActiveDate         = null;
  private double   MCMerchLocFee      = 0.00;
  private int      BankNumber         = -1;
  private String   crlf               = "\r\n";

  public MCMerchLocationFeeLoader()
  {
  }

  protected void readProps() throws Exception
  {
    PropertiesFile props = new PropertiesFile("pricing.properties");

    MCMerchLocFee = props.getDouble("mc_merch_location_fee", MCMerchLocFee);
  }

  public boolean execute()
  {
    int               count             = 0;
    int               paramIndex        = 0;
    boolean           lastDayOfMonth    = false;
    String            qs                = null;
    ResultSet         rs                = null;
    PreparedStatement ps                = null;

    try
    {
      connect();

      readProps();

      qs = " select case when trunc(sysdate) = trunc(last_day(sysdate)) then 1 else 0 end  from dual ";
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();
      if( rs.next() )
      {
        lastDayOfMonth = (rs.getInt(1) == 1);
      }
      ps.close();
      rs.close();

      if( lastDayOfMonth || (ActiveDate != null && BankNumber != -1) )
      {
        if( ActiveDate == null )
        {
          qs = " select trunc(sysdate,'month')  from dual ";
          ps = con.prepareStatement(qs);
          rs = ps.executeQuery();
          if( rs.next() )
          {
            ActiveDate = (rs.getDate(1));
          }
        }
        ps.close();
        rs.close();

        // remove existing entries in MC_MERCH_LOCATION_FEE
        log.info("====== Remove existing entries in MC_MERCH_LOCATION_FEE ======");
        qs = " delete                           " + crlf +
             " from   mc_merch_location_fee     " + crlf +
             " where  active_date = ?           " + crlf +
             "        and ( (? = -1 and bank_number in (3941,3942,3943,3858,3003)) " + crlf +
             "              or bank_number = ? ) ";
        ps = con.prepareStatement(qs);
        ps.setDate(1, ActiveDate);
        ps.setInt(2, BankNumber);
        ps.setInt(3, BankNumber);
        ps.executeUpdate();
        commit();
        ps.close();

        // create entries in MC_MERCH_LOCATION_FEE
        log.info("====== Create entries in MC_MERCH_LOCATION_FEE ======");
        qs = " insert into mc_merch_location_fee                                             " + crlf +
             " (                                                                             " + crlf +
             "   active_date,       bank_number,       merchant_number,                      " + crlf +
             "   dba_name,          address_line1,     address_line2,                        " + crlf +
             "   address_city,      address_state,     address_zip,                          " + crlf +
             "   sic_code,          sales_count,       sales_amount,                         " + crlf +
             "   fee_amount                                                                  " + crlf +
             " )                                                                             " + crlf +
             " select  ?                            as active_date,                          " + crlf +
             "         mf.bank_number               as bank_number,                          " + crlf +
             "         mf.merchant_number           as merchant_number,                      " + crlf +
             "         mf.dba_name                  as dba_name,                             " + crlf +
             "         mf.addr1_line_1              as address_line1,                        " + crlf +
             "         mf.addr1_line_2              as address_line2,                        " + crlf +
             "         mf.city1_line_4              as address_city,                         " + crlf +
             "         mf.state1_line_4             as address_state,                        " + crlf +
             "         mf.zip1_line_4               as address_zip,                          " + crlf +
             "         mf.sic_code                  as sic_code,                             " + crlf +
             "         sum( nvl(sm.sales_count,0) )                    as sales_count,       " + crlf +
             "         sum( nvl(sm.sales_amount,0) )                   as sales_amount,      " + crlf +
             "         decode( sum(nvl(sm.sales_count,0)), 0, 0, ? )   as fee_amount         " + crlf +
             " from    mif mf,                                                               " + crlf +
             "         mbs_daily_summary sm                                                  " + crlf +
             " where   ( (? = -1 and mf.bank_number in (3941,3942,3943,3858,3003)) or mf.bank_number = ? ) " + crlf +
             "         and mf.processor_id = 1     --no simulator                            " + crlf +
             "         and mf.test_account = 'N'   --no test account                         " + crlf +
             "         and nvl(mf.mc_merch_loc_fee,'N') = 'Y'                                " + crlf +
             "         and sm.merchant_number(+) = mf.merchant_number                        " + crlf +
             "         and sm.activity_date(+) between ? and last_day(?)                     " + crlf +
             "         and sm.item_subclass(+) in ('MB','MC','MD','MC$')                     " + crlf +
             " group by mf.bank_number, mf.merchant_number, mf.dba_name, mf.addr1_line_1,    " + crlf +
             "         mf.addr1_line_2, mf.city1_line_4, mf.state1_line_4, mf.zip1_line_4, mf.sic_code ";

        paramIndex = 1;
        ps = con.prepareStatement(qs);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDouble(paramIndex++, MCMerchLocFee);
        ps.setInt(paramIndex++,    BankNumber);
        ps.setInt(paramIndex++,    BankNumber);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        count = ps.executeUpdate();
        commit();
        ps.close();
        log.info("====== MASTERCARD MONTHLY LOCATION FEE - MC_MERCH_LOCATION_FEE "+ count + " records");

        // create charge records
        log.info("====== Create entries in CHARGE_RECORDS ======");
        qs = " insert into mbs_charge_records                                                 " + crlf +
             " (                                                                              " + crlf +
             "   charge_type,          merchant_number,          billing_frequency,           " + crlf +
             "   item_count,           charge_amount,            transit_routing,             " + crlf +
             "   dda,                  start_date,               end_date,                    " + crlf +
             "   statement_message,    charge_acct_type,         enabled,                     " + crlf +
             "   created_by                                                                   " + crlf +
             " )                                                                              " + crlf +
             " select 'MIS'                                 as charge_type,                   " + crlf +
             "        mcf.merchant_number                   as merchant_number,               " + crlf +
             "        case                                                                    " + crlf +
             "          when to_char(?,'mm') =  1 then 'YNNNNNNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  2 then 'NYNNNNNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  3 then 'NNYNNNNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  4 then 'NNNYNNNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  5 then 'NNNNYNNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  6 then 'NNNNNYNNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  7 then 'NNNNNNYNNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  8 then 'NNNNNNNYNNNN'                         " + crlf +
             "          when to_char(?,'mm') =  9 then 'NNNNNNNNYNNN'                         " + crlf +
             "          when to_char(?,'mm') = 10 then 'NNNNNNNNNYNN'                         " + crlf +
             "          when to_char(?,'mm') = 11 then 'NNNNNNNNNNYN'                         " + crlf +
             "          when to_char(?,'mm') = 12 then 'NNNNNNNNNNNY'                         " + crlf +
             "          else                           'NNNNNNNNNNNN'                         " + crlf +
             "        end                                   as billing_months,                " + crlf +
             "        null                                  as item_count,                    " + crlf +
             "        mcf.fee_amount                        as charge_amount,                 " + crlf +
             "        decode(mi.discount_routing, null, mi.transit_routng_num, to_char(mi.discount_routing)) " + crlf +
             "                                              as transit_routing,               " + crlf +
             "        decode(mi.discount_dda, null, mi.dda_num, mi.discount_dda)              " + crlf +
             "                                              as dda,                           " + crlf +
             "        ?                                     as start_date,                    " + crlf +
             "        trunc(last_day(?))                    as end_date,                      " + crlf +
             "        'MASTERCARD MONTHLY LOCATION FEE'     as statement_message,             " + crlf +
             "        'CK'                                  as charge_acct_type,              " + crlf +
             "        'Y'                                   as enabled,                       " + crlf +
             "        'SYSTEM'                              as created_by                     " + crlf +
             " from   mif mi,                                                                 " + crlf +
             "        mc_merch_location_fee mcf                                               " + crlf +
             " where  mcf.active_date = ?                                                     " + crlf +
             "        and ( (? = -1 and mcf.bank_number in (3941,3942,3943,3858,3003)) or mcf.bank_number = ? ) " + crlf +
             "        and mi.merchant_number = mcf.merchant_number                            " + crlf +
             "        and mcf.fee_amount > 0                                                  " + crlf +
             "        and mcf.merchant_number not in                                          " + crlf +
             "        ( select mf.merchant_number                                             " + crlf +
             "          from   mif mf                                                         " + crlf +
             "          where  mf.bank_number in (3941,3943)                                  " + crlf +
             "                 and mf.processor_id = 1    --no simulator                      " + crlf +
             "                 and mf.test_account = 'N'  --no test account                   " + crlf +
             "                 and ( (mf.group_2_association = 400066 and mf.group_3_association != 300280) --BankServ, except sub-hierarchy 3941300280 " + crlf +
             "                       or mf.group_2_association = 490005   --Primoris, they bill their own merchants          " + crlf +
             "                       or mf.group_2_association = 490012   --PayNSeconds       " + crlf +
             "                       or mf.group_3_association = 300429   --'NO FEE' node which belongs to Billing Tree      " + crlf +
             "                       or mf.group_4_association = 201334   --CMDI, they bill their own merchants              " + crlf +
             "                       or mf.group_4_association = 290009   --Demosphere, they bill their own merchants        " + crlf +
             "                     )                                                          " + crlf +
             "        )                                                                       ";

        count = 0;
        paramIndex = 1;
        ps = con.prepareStatement(qs);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setDate(paramIndex++,   ActiveDate);
        ps.setInt(paramIndex++,    BankNumber);
        ps.setInt(paramIndex++,    BankNumber);
        count = ps.executeUpdate();
        commit();
        ps.close();
        log.info("====== MASTERCARD MONTHLY LOCATION FEE - MBS_CHARGE_RECORDS " + count + " records");
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
    return( true );
  }

  public void setActiveDate( Date activeDate )
  {
    ActiveDate = activeDate;
  }

  public void setBankNumber( int bankNumber )
  {
    BankNumber = bankNumber;
  }

  public static void main(String[] args)
  {
    MCMerchLocationFeeLoader event = null;

    try
    {
      event = new MCMerchLocationFeeLoader();
      event.connect();

      if( args.length == 3 && args[0].equals("loadfee") )
      {
        event.setActiveDate( DateTimeFormatter.parseSQLDate( args[1].toString(), "MM/dd/yyyy") );
        event.setBankNumber( Integer.parseInt(args[2]) );
        log.info("execute() ActiveDate " + args[1].toString() + "   BankNumber " +  Integer.parseInt(args[2]));
        event.execute();
      }
      else
      {
        log.info("===== Invalid Arguments =====");
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      try{ event.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}
