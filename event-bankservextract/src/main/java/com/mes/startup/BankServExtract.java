/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BankServExtract.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-10-21 16:03:45 -0700 (Fri, 21 Oct 2011) $
  Version            : $Revision: 19446 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import com.mes.support.WildCardFilter;
import bankserv.hermes.api.FileProcess;

public class BankServExtract extends EventBase
{
  public static final int     ZIP_BUFFER_LEN  = 4096;
  public static final String  OUTFILE_PATH    = "./download/";
  public static final String  OUTFILE_NAME    = "bserv_ext3941.dat";
  public static final String  OUTFILE_BASE    = "bserv_ext3941";
  
  public BankServExtract()
  {
  }
  
  /*
  ** METHOD execute()
  **
  ** Entry point from timed event manager
  */
  public boolean execute()
  {
    boolean         result      = false;
    File[]          fileList    = null;
    String          fileName    = "";
    byte[]          zipData     = new byte[ZIP_BUFFER_LEN];
    int             bytesRead   = 0;
    
    try
    {
      // retrieve extract file
      FileProcess fp = new FileProcess();
      fp.downloadFileList("zip");
      
      // get list of .zip files in download directory
      WildCardFilter filter = new WildCardFilter("*.zip");
      
      File dir = new File(OUTFILE_PATH);
      fileList = dir.listFiles(filter);
      
      if(fileList != null)
      {
        // walk through each matching file and process it
        for(int i=0; i<fileList.length; ++i)
        {
          fileName = fileList[i].getName();
          
          FileInputStream   fin     = new FileInputStream(OUTFILE_PATH + fileName);
          ZipInputStream    zin     = new ZipInputStream(fin);
          
          FileOutputStream  outFile = new FileOutputStream(OUTFILE_PATH + OUTFILE_NAME);
          ZipEntry          zentry  = null;
          
          while((zentry = zin.getNextEntry()) != null)
          {
            if(!zentry.isDirectory())
            {
              break;
            }
          }
          
          bytesRead = 0;
          
          while(bytesRead != -1)
          {
            bytesRead = zin.read(zipData);
            if(bytesRead != -1)
            {
              outFile.write(zipData, 0, bytesRead);
            }
          }
          
          zin.close();
          outFile.close();
          
          // now file has been extracted, process it using the NDMFile class
          //FileTransfer ft = new FileTransfer(OUTFILE_BASE);
          //ft.ndm.transfer();
          
          // remove original file
          new File(OUTFILE_PATH + fileName).delete();
        }
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    
    return result;
  }
}
