/*@lineinfo:filename=SabreMerchantProfile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/SabreMerchantProfile.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import sqlj.runtime.ResultSetIterator;

public class SabreMerchantProfile extends EventBase
{
  static Logger log = Logger.getLogger(SabreMerchantProfile.class);
  
  private int               FileRecordCount   = 0;
  
  public final static int     PT_MERCHANT_PROFILE           = 0;
  public final static int     PT_EMAIL_WELCOME_KIT          = 1;
  public final static int     PT_EMAIL_NON_BANK_ACTIVATION  = 2;
  public final static int     PT_COUNT                      = 3;
  
  public final static String  SABRE_EMAIL_TEMPLATE            = "sabre/sabre_welcome_email.txt";
  public final static String  SABRE_EMAIL_NON_BANK_TEMPLATE   = "sabre/sabre_non_bank_activation_email.txt";
  
  protected boolean TestMode      = false;
  
  public SabreMerchantProfile( )
  {
  }
  
  private void buildProfileHeader( long sequenceId, BufferedWriter out )
  {
    FlatFileRecord        ffd     = null;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_SABRE_PROFILE_HDR);
      ffd.setFieldData("file_number",Long.toString(sequenceId));
      ffd.setFieldData("file_date",Calendar.getInstance().getTime());
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildProfileHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildProfileDetails( long sequenceId, BufferedWriter out )
  {
    FlatFileRecord        ffd       = null;
    ResultSetIterator     it        = null;
    int                   recType   = MesFlatFiles.DEF_TYPE_SABRE_PROFILE_DT;
    ResultSet             rs        = null;
    
    try
    {
      if ( TestMode == true )
      {
//@        recType = 212;      // use test fields
      }
      ffd = new FlatFileRecord(recType);
    
      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(mrs.pcc)                    as pcc, 
//                  upper(sp.profile_type)            as action,                       
//                  mf.merchant_number                as merchant_number, 
//                  upper(mrs.statement_dba_name)     as dba_name,
//                  upper(mf.addr1_line_1)            as addr1, 
//                  upper(mf.addr1_line_2)            as addr2, 
//                  substr(mf.DMCITY,1,13)            as city, --@
//                  mf.DMSTATE                        as state, 
//                  rpad(nvl(mf.dmzip,0),9,'0')       as zip, 
//                  mr.merch_email_address            as email, 
//                  nvl(mf.phone_1,0)                 as phone, 
//                  decode( mf.phone_2_fax,
//                          '0000000000',null,
//                          mf.phone_2_fax )          as fax, 
//                  upper( mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || 
//                    mc.MERCHCONT_PRIM_LAST_NAME )   as contact_name,
//                  mf.federal_tax_id                 as fed_tax_id, 
//                  mf.federal_tax_id                 as state_tax_id,  
//                  mf.sic_code                       as sic_code, 
//                  --mf.visa_disc_rate                 as visa_disc_rate, 
//                  --mf.mastcd_disc_rate               as mc_disc_rate, 
//                  decode(nvl(mrs.include_rates,'N'),
//                         'Y',decode(mrs.net_percentage,
//                                    null, nvl(mf.visa_disc_rate,0),
//                                    mrs.net_percentage*1000
//                                   ),
//                         0)                         as visa_disc_rate, 
//                  decode(nvl(mrs.include_rates,'N'),
//                         'Y',decode(mrs.net_percentage,
//                                     null, nvl(mf.mastcd_disc_rate,0),
//                                    mrs.net_percentage*1000
//                                   ),
//                         0)                         as mc_disc_rate, 
//                  decode(mf.damexse,
//                    null, null,
//                    0, null,
//                    'AX')                           as amex_accept,
//                  decode(mf.damexse,
//                    null, null,
//                    0, null,
//                    mf.damexse)                     as amex_se, 
//                  decode( nvl(mf.damexse,0),
//                          0, 0,
//                          decode(nvl(mrs.include_rates,'N'),
//                                     'Y',decode(mrs.net_percentage,
//                                                 null, nvl(mf.amex_discount_rate,0),
//                                                mrs.net_percentage*1000
//                                               ),
//                                     0) )           as amex_disc_rate, 
//                  decode( mf.dmdsnum,
//                          null, null,
//                          0, null,
//                          'DS' )                    as discover_accept,
//                  decode( mf.dmdsnum,
//                          null, null,
//                          0,null,
//                          601101436679714 )         as discover_id, 
//                  decode( nvl(mf.damexse,0),
//                          0, 0,
//                          decode(nvl(mrs.include_rates,'N'),
//                                     'Y',decode(mrs.net_percentage,
//                                                 null, nvl(mf.discvr_disc_rate,0),
//                                                mrs.net_percentage*1000
//                                               ),
//                                     0) ) 
//                                                    as discover_disc_rate,
//                  'DC'                              as diners_accept,
//                  mf.merchant_number                as diners_id, 
//                  decode( nvl(mf.damexse,0),
//                          0, 0,
//                          decode(nvl(mrs.include_rates,'N'),
//                                     'Y',decode(mrs.net_percentage,
//                                                 null, nvl(mf.diners_disc_rate,0),
//                                                mrs.net_percentage*1000
//                                               ),
//                                     0) ) 
//                                                    as diners_disc_rate,
//                  'TP'                              as uatp_accept,
//                  ( '9' || 
//                    substr(mf.merchant_number,-7) ) as uatp_id, 
//                  decode(nvl(mrs.include_rates,'N'),
//                         'Y',decode(mrs.net_percentage,
//                                     null, nvl(mf.visa_disc_rate,0),
//                                    mrs.net_percentage*1000),
//                          0)
//                                                    as uatp_disc_rate,
//                  upper(nvl(mrs.sales_charge_code,'999'))       as sales_charge_code,
//                  upper(nvl(mrs.sales_vendor_code,'999999'))    as sales_vendor_code,
//                  upper(nvl(mrs.refunds_charge_code,'999'))     as refund_charge_code,
//                  upper(nvl(mrs.refunds_vendor_code,'999999'))  as refund_vendor_code,
//                  nvl(mrs.country_code,'US')                    as country,
//                  nvl(mrs.country_code,'US')                    as country_code,
//                  nvl(mrs.currency_code,'USD')                  as currency_code,
//                  'Sabre Welcome Kit'                           as subject,
//                  :SABRE_EMAIL_TEMPLATE                         as template
//          from    sabre_process     sp, 
//                  mif               mf, 
//                  merchant          mr, 
//                  merchant_sabre    mrs, 
//                  merchcontact      mc 
//          where   sp.process_sequence = :sequenceId and 
//                  sp.process_type = :PT_MERCHANT_PROFILE and  -- 0 and
//                  mf.merchant_number = sp.merchant_number and 
//                  mr.merch_number = mf.merchant_number and 
//                  mrs.app_seq_num = mr.app_seq_num and 
//                  length(nvl(mrs.pcc,'BAD')) = 4 and
//                  mc.app_seq_num(+) = mr.app_seq_num      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(mrs.pcc)                    as pcc, \n                upper(sp.profile_type)            as action,                       \n                mf.merchant_number                as merchant_number, \n                upper(mrs.statement_dba_name)     as dba_name,\n                upper(mf.addr1_line_1)            as addr1, \n                upper(mf.addr1_line_2)            as addr2, \n                substr(mf.DMCITY,1,13)            as city, --@\n                mf.DMSTATE                        as state, \n                rpad(nvl(mf.dmzip,0),9,'0')       as zip, \n                mr.merch_email_address            as email, \n                nvl(mf.phone_1,0)                 as phone, \n                decode( mf.phone_2_fax,\n                        '0000000000',null,\n                        mf.phone_2_fax )          as fax, \n                upper( mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || \n                  mc.MERCHCONT_PRIM_LAST_NAME )   as contact_name,\n                mf.federal_tax_id                 as fed_tax_id, \n                mf.federal_tax_id                 as state_tax_id,  \n                mf.sic_code                       as sic_code, \n                --mf.visa_disc_rate                 as visa_disc_rate, \n                --mf.mastcd_disc_rate               as mc_disc_rate, \n                decode(nvl(mrs.include_rates,'N'),\n                       'Y',decode(mrs.net_percentage,\n                                  null, nvl(mf.visa_disc_rate,0),\n                                  mrs.net_percentage*1000\n                                 ),\n                       0)                         as visa_disc_rate, \n                decode(nvl(mrs.include_rates,'N'),\n                       'Y',decode(mrs.net_percentage,\n                                   null, nvl(mf.mastcd_disc_rate,0),\n                                  mrs.net_percentage*1000\n                                 ),\n                       0)                         as mc_disc_rate, \n                decode(mf.damexse,\n                  null, null,\n                  0, null,\n                  'AX')                           as amex_accept,\n                decode(mf.damexse,\n                  null, null,\n                  0, null,\n                  mf.damexse)                     as amex_se, \n                decode( nvl(mf.damexse,0),\n                        0, 0,\n                        decode(nvl(mrs.include_rates,'N'),\n                                   'Y',decode(mrs.net_percentage,\n                                               null, nvl(mf.amex_discount_rate,0),\n                                              mrs.net_percentage*1000\n                                             ),\n                                   0) )           as amex_disc_rate, \n                decode( mf.dmdsnum,\n                        null, null,\n                        0, null,\n                        'DS' )                    as discover_accept,\n                decode( mf.dmdsnum,\n                        null, null,\n                        0,null,\n                        601101436679714 )         as discover_id, \n                decode( nvl(mf.damexse,0),\n                        0, 0,\n                        decode(nvl(mrs.include_rates,'N'),\n                                   'Y',decode(mrs.net_percentage,\n                                               null, nvl(mf.discvr_disc_rate,0),\n                                              mrs.net_percentage*1000\n                                             ),\n                                   0) ) \n                                                  as discover_disc_rate,\n                'DC'                              as diners_accept,\n                mf.merchant_number                as diners_id, \n                decode( nvl(mf.damexse,0),\n                        0, 0,\n                        decode(nvl(mrs.include_rates,'N'),\n                                   'Y',decode(mrs.net_percentage,\n                                               null, nvl(mf.diners_disc_rate,0),\n                                              mrs.net_percentage*1000\n                                             ),\n                                   0) ) \n                                                  as diners_disc_rate,\n                'TP'                              as uatp_accept,\n                ( '9' || \n                  substr(mf.merchant_number,-7) ) as uatp_id, \n                decode(nvl(mrs.include_rates,'N'),\n                       'Y',decode(mrs.net_percentage,\n                                   null, nvl(mf.visa_disc_rate,0),\n                                  mrs.net_percentage*1000),\n                        0)\n                                                  as uatp_disc_rate,\n                upper(nvl(mrs.sales_charge_code,'999'))       as sales_charge_code,\n                upper(nvl(mrs.sales_vendor_code,'999999'))    as sales_vendor_code,\n                upper(nvl(mrs.refunds_charge_code,'999'))     as refund_charge_code,\n                upper(nvl(mrs.refunds_vendor_code,'999999'))  as refund_vendor_code,\n                nvl(mrs.country_code,'US')                    as country,\n                nvl(mrs.country_code,'US')                    as country_code,\n                nvl(mrs.currency_code,'USD')                  as currency_code,\n                'Sabre Welcome Kit'                           as subject,\n                 :1                          as template\n        from    sabre_process     sp, \n                mif               mf, \n                merchant          mr, \n                merchant_sabre    mrs, \n                merchcontact      mc \n        where   sp.process_sequence =  :2  and \n                sp.process_type =  :3  and  -- 0 and\n                mf.merchant_number = sp.merchant_number and \n                mr.merch_number = mf.merchant_number and \n                mrs.app_seq_num = mr.app_seq_num and \n                length(nvl(mrs.pcc,'BAD')) = 4 and\n                mc.app_seq_num(+) = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,SABRE_EMAIL_TEMPLATE);
   __sJT_st.setLong(2,sequenceId);
   __sJT_st.setInt(3,PT_MERCHANT_PROFILE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.SabreMerchantProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        ++FileRecordCount;
        
        ffd.setAllFieldData(rs);
        
        out.write( ffd.spew() );
        out.newLine();
        
        // new record, send welcome kit email if necessary
        if ( rs.getString("action").equals("C") )
        {
          notifySabreAccount(rs);
        }
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildProfileDetails()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  public void buildProfileTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_SABRE_PROFILE_TRL);
      
      // set record count and total
      ffd.setFieldData("merchant_count", FileRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildProfileTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    int                 itemCount   = 0;
    int                 sequenceId  = -1;
  
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      for( int i = 0; i < PT_COUNT; ++ i)
      {
        /*@lineinfo:generated-code*//*@lineinfo:291^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(rec_id) 
//            from    sabre_process
//            where   process_type = :i and
//                    process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rec_id)  \n          from    sabre_process\n          where   process_type =  :1  and\n                  process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.SabreMerchantProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,i);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^9*/
      
        if ( itemCount > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:301^11*/

//  ************************************************************
//  #sql [Ctx] { select  sabre_profile_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sabre_profile_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.SabreMerchantProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:307^11*/

//  ************************************************************
//  #sql [Ctx] { update sabre_process
//              set     process_sequence = :sequenceId
//              where   process_type = :i and
//                      process_sequence is null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update sabre_process\n            set     process_sequence =  :1 \n            where   process_type =  :2  and\n                    process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   __sJT_st.setInt(2,i);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:313^11*/                    
          
          switch( i )
          {
            case PT_MERCHANT_PROFILE:
              processMerchantProfile( sequenceId );
              break;
              
            case PT_EMAIL_WELCOME_KIT:
            case PT_EMAIL_NON_BANK_ACTIVATION:
              processEmailNotify( sequenceId, i );
              break;
              
            default:    // not supported
              break;
          }
        }
      }        
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }      
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  public synchronized int getFileId( String dateString )
  {
    int               fileId    = 0;
    PropertiesFile    propsFile = null;
    
    try
    {
      new PropertiesFile("transfer.properties");
    } catch(Exception e) {}
    
    if ( dateString.equals( propsFile.getString("file.lastdate") ) )
    {
      fileId = Integer.parseInt( propsFile.getString("file.lastcount"));
    }
    fileId++;
    
    propsFile.setProperty("file.lastdate",dateString);
    propsFile.setProperty("file.lastcount",Integer.toString(fileId));
    propsFile.store("transfer.properties");
    
    return( fileId );
  }
  
  private void notifySabreAccount(ResultSet resultSet)
  {
    ResultSetIterator   it            = null;
    long                merchantId    = 0L;
    int                 recCount      = 0;
    String              subject       = null;
    String              template      = null;
    
    try
    {
      merchantId  = resultSet.getLong("merchant_number");
      subject     = resultSet.getString("subject");
      template    = resultSet.getString("template");
    
      StringBuffer        body              = new StringBuffer();
      StringBuffer        cardList          = new StringBuffer();
      ResultSet           cardSet           = null;
      String              ct                = null;
      String              line              = null;
      HashMap             keywordList       = new HashMap();
      MailMessage         msg               = new MailMessage();
      
      /*@lineinfo:generated-code*//*@lineinfo:389^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mpo.cardtype_code,
//                          1,'Visa',               -- APP_CT_VISA               
//                          4,'MasterCard',         -- APP_CT_MC                 
//                          10,'Diners',            -- APP_CT_DINERS_CLUB        
//                          14,'Discover',          -- APP_CT_DISCOVER           
//                          15,'JCB',               -- APP_CT_JCB                
//                          16,'American Express',  -- APP_CT_AMEX               
//                          17,'Debit',             -- APP_CT_DEBIT
//                          '' )        as card_type
//          from    merchant        mr,
//                  merchpayoption  mpo
//          where   mr.merch_number = :merchantId and
//                  mpo.app_seq_num = mr.app_seq_num
//          order by mpo.cardtype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mpo.cardtype_code,\n                        1,'Visa',               -- APP_CT_VISA               \n                        4,'MasterCard',         -- APP_CT_MC                 \n                        10,'Diners',            -- APP_CT_DINERS_CLUB        \n                        14,'Discover',          -- APP_CT_DISCOVER           \n                        15,'JCB',               -- APP_CT_JCB                \n                        16,'American Express',  -- APP_CT_AMEX               \n                        17,'Debit',             -- APP_CT_DEBIT\n                        '' )        as card_type\n        from    merchant        mr,\n                merchpayoption  mpo\n        where   mr.merch_number =  :1  and\n                mpo.app_seq_num = mr.app_seq_num\n        order by mpo.cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.SabreMerchantProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^7*/
      cardSet = it.getResultSet();
      
      while(cardSet.next())
      {
        ct = cardSet.getString("card_type");
        
        if ( ct == null ) 
        {
          continue;
        }
        
        if( cardList.length() > 0 )
        {
          cardList.append(", ");
        }              
        cardList.append(ct);
      }
      cardSet.close();
      it.close();
      
      // setup the replacement param values
      keywordList.put("<merchantId>", Long.toString(merchantId) );
      keywordList.put("<dbaName>", resultSet.getString("dba_name"));
      keywordList.put("<cardList>",cardList.toString());
      
      BufferedReader in = new BufferedReader( new FileReader(template) );
      while( ( line = in.readLine() ) != null )
      {
        body.append(replaceKeywords(line,keywordList));
        body.append("\n");
      }
      in.close();
      
      msg.setFrom("noreply@merchante-solutions.com");
      msg.addTo( resultSet.getString("email") );
      //@msg.addTo( "jduncan@merchante-solutions.com" );
      msg.setSubject(subject);
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("notifySabreAccount(" + merchantId + ")", e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
  }
  
  public void processMerchantProfile( int sequenceId )
  {
    String              dateString  = null;
    BufferedWriter      out         = null;
    StringBuffer        tfilename   = null;
    
    try
    {
      recordTimestampBegin( sequenceId, PT_MERCHANT_PROFILE );
      
      // setup a temp filename
      // MDF-MES-yyyymmdd.TXT
      dateString = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyyyy");
      tfilename = new StringBuffer();
      tfilename.append("mdf-mes-");
      tfilename.append(dateString);
//@        tfilename.append("_");
//@        tfilename.append( NumberFormatter.getPaddedInt( getFileId(dateString), 3 ) );
      
      // open the data file
      out = new BufferedWriter( new FileWriter( tfilename.toString() + ".txt", true ) );
      
      // build the file using the FlatFileDef
      FileRecordCount = 0;
      buildProfileHeader(sequenceId,out);
      buildProfileDetails(sequenceId,out);
      buildProfileTrailer(out);
      out.close();
      
      // only transmit the file in production mode
      if ( TestMode == false )
      {
        // ftp the file to the outgoing directory
        sendFile(tfilename.toString());
      }        

      // update the process table timestamp
      recordTimestampEnd( sequenceId, PT_MERCHANT_PROFILE );
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processMerchantProfile()", e.toString());
      logEntry("processMerchantProfile()", e.toString());
    }
    finally
    {
    }
  }
  
  public void processEmailNotify( int sequenceId, int processType )
  {
    ResultSetIterator       it = null;
    ResultSet               rs = null;
    
    try
    {
      recordTimestampBegin( sequenceId, processType );
      
      /*@lineinfo:generated-code*//*@lineinfo:514^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number            as merchant_number,
//                  mf.dba_name                   as dba_name,
//                  nvl(sp.email_address,
//                      mr.merch_email_address)   as email,
//                  decode(sp.process_type,
//                         :PT_EMAIL_NON_BANK_ACTIVATION,'Sabre T&E Card Activation',
//                         'Sabre Welcome Kit')   as subject,
//                  decode(sp.process_type,
//                         :PT_EMAIL_NON_BANK_ACTIVATION,:SABRE_EMAIL_NON_BANK_TEMPLATE,
//                         :SABRE_EMAIL_TEMPLATE) as template
//          from    sabre_process     sp, 
//                  merchant          mr,
//                  mif               mf
//          where   sp.process_sequence = :sequenceId and 
//                  sp.process_type = :processType and
//                  mr.merch_number = sp.merchant_number and
//                  mf.merchant_number = mr.merch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number            as merchant_number,\n                mf.dba_name                   as dba_name,\n                nvl(sp.email_address,\n                    mr.merch_email_address)   as email,\n                decode(sp.process_type,\n                        :1 ,'Sabre T&E Card Activation',\n                       'Sabre Welcome Kit')   as subject,\n                decode(sp.process_type,\n                        :2 , :3 ,\n                        :4 ) as template\n        from    sabre_process     sp, \n                merchant          mr,\n                mif               mf\n        where   sp.process_sequence =  :5  and \n                sp.process_type =  :6  and\n                mr.merch_number = sp.merchant_number and\n                mf.merchant_number = mr.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_EMAIL_NON_BANK_ACTIVATION);
   __sJT_st.setInt(2,PT_EMAIL_NON_BANK_ACTIVATION);
   __sJT_st.setString(3,SABRE_EMAIL_NON_BANK_TEMPLATE);
   __sJT_st.setString(4,SABRE_EMAIL_TEMPLATE);
   __sJT_st.setInt(5,sequenceId);
   __sJT_st.setInt(6,processType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.SabreMerchantProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:533^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        notifySabreAccount(rs);
      }
      rs.close();
      it.close();
      
      // update the process table timestamp
      recordTimestampEnd( sequenceId, processType );
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processEmailNotify()", e.toString());
      logEntry("processEmailNotify()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
 
  protected void recordTimestampBegin( int sequenceId, int procType )
  {
    Timestamp           beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:565^7*/

//  ************************************************************
//  #sql [Ctx] { update  sabre_process
//          set     process_begin_date = :beginTime
//          where   process_type = :procType and
//                  process_sequence = :sequenceId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  sabre_process\n        set     process_begin_date =  :1 \n        where   process_type =  :2  and\n                process_sequence =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setInt(2,procType);
   __sJT_st.setInt(3,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:571^7*/
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  } 
  
  protected void recordTimestampEnd( int sequenceId, int procType )
  {
    Timestamp           beginTime   = null;
    long                elapsed     = 0;
    Timestamp           endTime     = null;
    
    try
    {
      // retrieve the process begin time
      /*@lineinfo:generated-code*//*@lineinfo:589^7*/

//  ************************************************************
//  #sql [Ctx] { select  min(sp.process_begin_date) 
//          from    sabre_process sp
//          where   sp.process_type = :procType and
//                  sp.process_sequence = :sequenceId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(sp.process_begin_date)  \n        from    sabre_process sp\n        where   sp.process_type =  :1  and\n                sp.process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.SabreMerchantProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,procType);
   __sJT_st.setInt(2,sequenceId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:595^7*/
      
      // calculate the elapsed
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
      
      /*@lineinfo:generated-code*//*@lineinfo:601^7*/

//  ************************************************************
//  #sql [Ctx] { update  sabre_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//          where   process_type = :procType and
//                  process_sequence = :sequenceId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4672 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  sabre_process\n        set     process_end_date =  :1 ,\n                process_elapsed =  :2 \n        where   process_type =  :3  and\n                process_sequence =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.SabreMerchantProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4672);
   __sJT_st.setInt(3,procType);
   __sJT_st.setInt(4,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:608^7*/
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  } 
  
  protected String replaceKeywords( String line, HashMap keywordList )
  {
    StringBuffer  buffer            = new StringBuffer(line);
    int           offsetBegin       = 0;
    int           offsetEnd         = 0;
    String        keyword           = null;
    
    for ( Iterator it = keywordList.keySet().iterator(); it.hasNext(); )
    {
      keyword = (String)it.next();
      
      while ( ( offsetBegin = buffer.toString().indexOf(keyword) ) != -1 )
      {
        offsetEnd = (offsetBegin + keyword.length());
        buffer.replace(offsetBegin,offsetEnd,(String)keywordList.get(keyword));
      }
    }      
    return( buffer.toString() );
  }
  
  protected void sendFile( String baseFilename )
  {
    String        dataFilename    = baseFilename + ".txt";
    String        flagFilename    = baseFilename + ".flg";
    String        progress        = "";
    boolean       success         = false;
    StringBuffer  status          = new StringBuffer();
    
    try
    {    
      connect();
      
      try
      {
        // log in to the proper host
        progress = "instantiating FTP";
        FTPClient ftp = new FTPClient(MesDefaults.getString(MesDefaults.DK_SABRE_HOST), 21);
        //@FTPClient ftp = new FTPClient(SABRE_HOST, 21);
      
        // set timeout
        ftp.setTimeout(5000);   // 5 seconds
  
        progress = "logging in to ftp server";
        ftp.login( MesDefaults.getString(MesDefaults.DK_SABRE_USER), 
                   MesDefaults.getString(MesDefaults.DK_SABRE_PASSWORD) );
        //@ftp.login( SABRE_USER, SABRE_PASSWORD );
        
        progress = "setting command mode to PASSIVE";
        ftp.setConnectMode(FTPConnectMode.PASV);
        
        progress = "setting transfer type to BINARY";
        ftp.setType(FTPTransferType.ASCII);
      
        progress = "changing directory";
//@        ftp.chdir(MesDefaults.getString(MesDefaults.DK_SABRE_OUTGOING_PATH));
      
        // sending file
        progress = "transmitting file";
        ftp.put( dataFilename , dataFilename );
        
        // create a flag file
        BufferedWriter out = new BufferedWriter( new FileWriter( flagFilename, false) );
        out.write( dataFilename );
        out.close();
        
        // send the flag file
//@        ftp.put( flagFilename , flagFilename );
        
        // close
        progress = "closing connection";
        ftp.quit();
      
        progress = "archiving files";
        ftp = new FTPClient(MesDefaults.getString(MesDefaults.SABRE_ARCHIVE_HOST), 21);
        
        ftp.login(MesDefaults.getString(MesDefaults.SABRE_ARCHIVE_USER), MesDefaults.getString(MesDefaults.SABRE_ARCHIVE_PASSWORD));
        ftp.setType(FTPTransferType.ASCII);
        ftp.chdir(MesDefaults.getString(MesDefaults.SABRE_ARCHIVE_PATH));
        
        ftp.put( dataFilename, dataFilename);
        ftp.quit();
        
        new File(dataFilename).delete();
        new File(flagFilename).delete();
        
        status.append("[");
        status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        status.append("] Successfully processed Sabre merchant profile updates as ");
        status.append(dataFilename);
        
        success = true;
      }
      catch(Exception ftpe)
      {
        logEntry("sendFile(" + baseFilename + ")", progress + ": " + ftpe.toString());
        status.append(ftpe.toString());
      }
      
      // send a status email
      sendStatusEmail(success, dataFilename, status.toString() );
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + baseFilename + ")", e.toString());
    }
    finally
    {
    }
  }    
  
  private void sendStatusEmail(boolean success, String fileName, String message)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      if(success)
      {
        subject.append("Sabre Success");
      }
      else
      {
        subject.append("Sabre Error");
        
        // make sure the filename is in 
        // the message text for error research
        body.append("Filename: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_SABRE_NOTIFY);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_SABRE_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  public void setTestMode( boolean testModeActive )
  {
    TestMode = testModeActive;
  }
  
  public static void main( String[] args )
  {
    SabreMerchantProfile        test          = null;
    boolean                     tm            = true;
    

        try {
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_SABRE_HOST,
                        MesDefaults.DK_SABRE_USER,
                        MesDefaults.DK_SABRE_PASSWORD,
                        MesDefaults.SABRE_ARCHIVE_HOST,
                        MesDefaults.SABRE_ARCHIVE_USER,
                        MesDefaults.SABRE_ARCHIVE_PASSWORD,
                        MesDefaults.SABRE_ARCHIVE_PATH
                });
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      log.debug("creating obj");//@
      test = new SabreMerchantProfile();
      
      if ( args.length == 2 && args[0].equals("sendfile") )
      {
        int           extOffset = args[1].indexOf(".");
        StringBuffer  filename  = new StringBuffer(args[1]);
        
        if ( extOffset >= 0 )
        {
          filename.setLength(extOffset);
        }
        log.debug("sending '" + filename.toString() + "'...");
        test.sendFile(filename.toString());
      }
      else 
      {
        if ( args.length > 0 )
        {
          if( args[0].equals("prod") )
          {
            tm = false;
          }
        }
        test.setTestMode(tm);
        log.debug("calling execute");//@
        test.execute();
      }
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/