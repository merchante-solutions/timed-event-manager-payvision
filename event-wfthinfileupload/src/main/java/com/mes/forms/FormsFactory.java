/*@lineinfo:filename=FormsFactory*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/forms/FormsFactory.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-07-18 14:22:41 -0700 (Mon, 18 Jul 2011) $
  Version            : $Revision: 19035 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.forms;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class FormsFactory
  extends SQLJConnectionBase
{
  public static FieldGroup buildFieldGroup( int defType, String groupName )
  {
    return( new FormsFactory()._buildFieldGroup(defType,groupName) );
  }

  public FieldGroup _buildFieldGroup( int defType, String groupName )
  {
    StringBuffer          buffer        = new StringBuffer();
    Field                 field         = null;
    FieldGroup            fgroup        = new FieldGroup(groupName);
    ResultSetIterator     it            = null;
    ResultSet             rs            = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:51^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  field_order,
//                  name,
//                  type,
//                  length,
//                  decimal_places,
//                  mask,
//                  default_value
//          from    flat_file_def
//          where   def_type = :defType
//          order by field_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  field_order,\n                name,\n                type,\n                length,\n                decimal_places,\n                mask,\n                default_value\n        from    flat_file_def\n        where   def_type =  :1 \n        order by field_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.FormsFactory",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,defType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.FormsFactory",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        String  defaultValue    = rs.getString("default_value");
        String  fieldName       = rs.getString("name");
        int     fieldLength     = rs.getInt("length");
        int     decimalPlaces   = rs.getInt("decimal_places");
        
        switch( rs.getInt("type") )
        {
          case 1:   // number
            buffer.setLength(0);
            for( int i = 0; i < decimalPlaces; ++i )
            {
              buffer.append("0");
            }
            if ( buffer.length() > 0 )
            {
              buffer.insert(0,".");
            }
            for( int i = 0; i < fieldLength-decimalPlaces; ++i )
            {
              buffer.insert(0,(i == 0) ? "0" : "#");
            }
            field = new NumberField(fieldName,fieldName,fieldLength,fieldLength+2,true,decimalPlaces);
            ((NumberField) field).setNumberFormat(buffer.toString());
            break;
            
          case 2:   // date
            field = new DateField(fieldName,fieldName,true);
            ((DateField) field).setDateFormatInput(rs.getString("mask"));
            ((DateField) field).setDateFormat(rs.getString("mask"));
            break;
            
          //case 0:   // alphanumeric
          //case 3:   // alphanumeric (number and letters only)
          //case 4:   // alphanumeric (letters only)
          //case 5:   // alphanumeric (no padding)
          //case 6:   // alphanumeric (left pad with spaces)
          //case 7:   // alphanumeric (minimum one space)
          //case 8:   // cobol encoded
          default:
            field = new Field(fieldName,fieldName,fieldLength,fieldLength+2,true);
            break;
        }
        if ( defaultValue != null )
        {
          field.setDefaultValue(defaultValue);
        }
        fgroup.add(field);
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildFieldGroup(" + defType + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( fgroup );
  }
}/*@lineinfo:generated-code*/