/*@lineinfo:filename=WFThinFileUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/WFThinFileUpload.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-09-14 10:30:38 -0700 (Mon, 14 Sep 2015) $
  Version            : $Revision: 23829 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import java.util.stream.IntStream;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.FormsFactory;
import com.mes.forms.NumberField;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class WFThinFileUpload extends TsysFileBase
{
  private boolean TestMode = false;
  
  private static final String[] CardPresentFieldNames = new String[]
  {
    "vs_sales_count",
    "vs_sales_amount",
    "mc_sales_count",
    "mc_sales_amount",
    "am_sales_count",
    "am_sales_amount",
    "ds_sales_count",
    "ds_sales_amount",
  };
  
  public static final class ThinFileAccount
  {
    public    Date              ActiveDate        = null;
    public    long              MerchantId        = 0L;
    public    Date              MonthBeginDate    = null;
    public    Date              MonthEndDate      = null;
    
    public ThinFileAccount( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      ActiveDate        = resultSet.getDate("active_date");
      MerchantId        = resultSet.getLong("merchant_number");
      MonthBeginDate    = resultSet.getDate("month_begin_date");
      MonthEndDate      = resultSet.getDate("month_end_date");
    }
  }
  
  protected class ThinFileWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    private   ThinFileAccount   AccountData   = null;
    
    public ThinFileWorker( ThinFileAccount accountData )
    {
      AccountData = accountData;
    }
    
    public void run()
    {
      int                 descriptorCount = 0;
      FieldGroup          fgroup          = null;
      ResultSetIterator   it              = null;
      ResultSet           resultSet       = null;
    
      try
      {
        connect(true);
        fgroup = FormsFactory.buildFieldGroup(MesFlatFiles.DEF_TYPE_WF_THIN_FILE,"wfThinFile");
        
        // PROD -1973: The affiliate_id and affiliate_name additional check put in 'or (mf.group_1_association = 500001 and mf.group_2_association = 400245)'.
        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  :AccountData.ActiveDate         as report_month,
//                    mf.merchant_number                as merchant_number,
//                    nvl(mf.fdr_corp_name,mf.dba_name) as legal_name,
//                    mf.dba_name                       as dba_name,
//                    mf.dmaddr                         as dba_address_street,
//                    mf.dmcity                         as dba_address_city,
//                    mf.dmstate                        as dba_address_state,
//                    mf.dmzip                          as dba_address_zip,
//                    nvl(addr.country_code,'US')       as dba_address_country,
//                    nvl(mf.phone_1, '')				as dba_phone,
//                    nvl(mf.fdr_corp_name,'')          as primary_descriptor,
//                    null                              as registration_id,
//                    null                              as household_id,
//                    null                              as household_name,
//                    case when (mf.group_1_association = 500002 or (mf.group_1_association = 500001 and mf.group_2_association = 400245)) then o.org_group else null end
//                                                      as affiliate_id,
//                    case when (mf.group_1_association = 500002 or (mf.group_1_association = 500001 and mf.group_2_association = 400245)) then o.org_name  else null end
//                                                      as affiliate_name,
//                    case when mf.dmacctst in ('D','C','B') then 'C' else 'O' end
//                                                      as account_status,                
//                    mmddyy_to_date(mf.date_opened)    as date_opened,
//                    mf.date_stat_chgd_to_dcb          as date_closed,
//                    mf.sic_code                       as mcc,
//                    nvl(mr.business_web_address,mr.merch_web_url)
//                                                      as website_address
//            from    mif                   mf,
//                    organization          o,
//                    merchant              mr,
//                    address               addr
//            where   mf.merchant_number = :AccountData.MerchantId
//                    and o.org_group = (mf.bank_number || mf.group_2_association)
//                    and mr.merch_number(+) = mf.merchant_number
//                    and addr.app_seq_num(+) = mr.app_seq_num
//                    and addr.addresstype_code(+) = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1          as report_month,\n                  mf.merchant_number                as merchant_number,\n                  nvl(mf.fdr_corp_name,mf.dba_name) as legal_name,\n                  mf.dba_name                       as dba_name,\n                  mf.dmaddr                         as dba_address_street,\n                  mf.dmcity                         as dba_address_city,\n                  mf.dmstate                        as dba_address_state,\n                  mf.dmzip                          as dba_address_zip,\n                  nvl(addr.country_code,'US')       as dba_address_country,\n                  nvl(mf.phone_1, '')\t\t\t\tas dba_phone,\n                  nvl(mf.fdr_corp_name,'')          as primary_descriptor,\n                  null                              as registration_id,\n                  null                              as household_id,\n                  null                              as household_name,\n                  case when (mf.group_1_association = 500002 or (mf.group_1_association = 500001 and mf.group_2_association = 400245)) then o.org_group else null end\n                                                    as affiliate_id,\n                  case when (mf.group_1_association = 500002 or (mf.group_1_association = 500001 and mf.group_2_association = 400245)) then o.org_name  else null end\n                                                    as affiliate_name,\n                  case when mf.dmacctst in ('D','C','B') then 'C' else 'O' end\n                                                    as account_status,                \n                  mmddyy_to_date(mf.date_opened)    as date_opened,\n                  mf.date_stat_chgd_to_dcb          as date_closed,\n                  mf.sic_code                       as mcc,\n                  nvl(mr.business_web_address,mr.merch_web_url)\n                                                    as website_address\n          from    mif                   mf,\n                  organization          o,\n                  merchant              mr,\n                  address               addr\n          where   mf.merchant_number =  :2 \n                  and o.org_group = (mf.bank_number || mf.group_2_association)\n                  and mr.merch_number(+) = mf.merchant_number\n                  and addr.app_seq_num(+) = mr.app_seq_num\n                  and addr.addresstype_code(+) = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.WFThinFileUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,AccountData.ActiveDate);
   __sJT_st.setLong(2,AccountData.MerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.WFThinFileUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^9*/
        resultSet = it.getResultSet();
        resultSet.next();
        
        // reset data and set base fields
        fgroup.resetDefaults();
        FieldBean.setFields(fgroup,resultSet,false,false);
        
        resultSet.close();
        it.close();
        
        // load the volume data
        /*@lineinfo:generated-code*//*@lineinfo:155^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(distinct dt.merchant_name)  as descriptor_count,
//                    sum(case 
//                      when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0 
//                        end)                          as vs_sales_count,
//                    sum(case 
//                      when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0 
//                        end)                          as mc_sales_count,
//                    sum(case 
//                      when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0 
//                        end)                          as am_sales_count,
//                    sum(case 
//                      when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0 
//                        end)                          as ds_sales_count,
//                    sum(case 
//                      when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as vs_sales_amount,
//                    sum(case 
//                      when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as mc_sales_amount,
//                    sum(case 
//                      when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as am_sales_amount,
//                    sum(case 
//                      when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as ds_sales_amount,
//                    sum(case 
//                      when dt.card_type like 'V%' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end)                       as vs_credits_count,
//                    sum(case 
//                      when dt.card_type like 'M%' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end)                       as mc_credits_count,
//                    sum(case 
//                      when dt.card_type = 'AM' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end)                       as am_credits_count,
//                    sum(case 
//                      when dt.card_type = 'DS' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end)                       as ds_credits_count,
//                    sum(case 
//                      when dt.card_type like 'V%' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as vs_credits_amount,
//                    sum(case 
//                      when dt.card_type like 'M%' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as mc_credits_amount,
//                    sum(case 
//                      when dt.card_type = 'AM' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as am_credits_amount,
//                    sum(case 
//                      when dt.card_type = 'DS' and dt.debit_credit_indicator = 'C' 
//                        then 1 
//                        else 0
//                        end*nvl(dt.transaction_amount,0)) as ds_credits_amount,
//                    sum(case when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) )   as cnp_vs_sales_count,
//                    sum(case when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1)
//                        * nvl(dt.transaction_amount,0))                   as cnp_vs_sales_amount,
//                    sum(case when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))    as cnp_mc_sales_count,
//                    sum(case when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1)
//                        * nvl(dt.transaction_amount,0))                   as cnp_mc_sales_amount,
//                    sum(case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))    as cnp_am_sales_count,
//                    sum(case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1)
//                        * nvl(dt.transaction_amount,0))                   as cnp_am_sales_amount,
//                    sum(case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))    as cnp_ds_sales_count,                      
//                    sum(case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end
//                        * decode(nvl(dt.mo_to_indicator,' '),' ',0,1)       
//                        * nvl(dt.transaction_amount,0))                   as cnp_ds_sales_amount                      
//            from    daily_detail_file_dt  dt
//            where   dt.merchant_account_number = :AccountData.MerchantId
//                    and dt.batch_date between :AccountData.ActiveDate and last_day(:AccountData.ActiveDate)
//                    and dt.reject_reason is null  -- ignore rejects
//                    and dt.bank_number = :BankNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "  SELECT count(distinct dt.merchant_name) as descriptor_count, " +
		   "    sum( case when dt.card_type like 'V%' and dt.card_type != 'VP' and dt.debit_credit_indicator = 'D' then 1 else 0 end)      as vs_sales_count, " +
		   "    sum( case when dt.card_type like 'M%' and dt.card_type != 'MP' and dt.debit_credit_indicator = 'D' then 1 else 0 end)      as mc_sales_count, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end)                                  as am_sales_count, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end)                                  as ds_sales_count, " +
		   "    sum( case when dt.card_type like 'V%' and dt.card_type != 'VP' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as vs_sales_amount, " +
		   "    sum( case when dt.card_type like 'M%' and dt.card_type != 'MP' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as mc_sales_amount, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as am_sales_amount, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as ds_sales_amount, " +
		   "    sum( case when dt.card_type like 'V%' and dt.card_type != 'VP' and dt.debit_credit_indicator = 'C' then 1 else 0 end)      as vs_credits_count, " +
		   "    sum( case when dt.card_type like 'M%' and dt.card_type != 'MP' and dt.debit_credit_indicator = 'C' then 1 else 0 end)      as mc_credits_count, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'C' then 1 else 0 end)                                  as am_credits_count, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'C' then 1 else 0 end)                                  as ds_credits_count, " +
		   "    sum( case when dt.card_type like 'V%' and dt.card_type != 'VP' and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as vs_credits_amount, " +
		   "    sum( case when dt.card_type like 'M%' and dt.card_type != 'MP' and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as mc_credits_amount, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as am_credits_amount, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as ds_credits_amount, " +
		   "    sum( case when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) )                                                                          as cnp_vs_sales_count, " +
		   "    sum( case when dt.card_type like 'V%' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) * nvl(dt.transaction_amount,0))                                            as cnp_vs_sales_amount, " +
		   "    sum( case when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))                                                                           as cnp_mc_sales_count, " +
		   "    sum( case when dt.card_type like 'M%' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) * nvl(dt.transaction_amount,0))                                            as cnp_mc_sales_amount, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))                                                                           as cnp_am_sales_count, " +
		   "    sum( case when dt.card_type = 'AM' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) * nvl(dt.transaction_amount,0))                                            as cnp_am_sales_amount, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1))                                                                           as cnp_ds_sales_count, " +
		   "    sum( case when dt.card_type = 'DS' and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      * decode(nvl(dt.mo_to_indicator,' '),' ',0,1) * nvl(dt.transaction_amount,0))                                            as cnp_ds_sales_amount, " +
		   "    sum( case when dt.card_type in ('DB','EB') and dt.debit_credit_indicator = 'D' then 1 else 0 end)                          as pin_debit_sales_count, " +
		   "    sum( case when dt.card_type in ('DB','EB') and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as pin_debit_sales_amount, " +
		   "    sum( case when dt.card_type in ('DB','EB') and dt.debit_credit_indicator = 'C' then 1 else 0 end)                          as pin_debit_credits_count, " +
		   "    sum( case when dt.card_type in ('DB','EB') and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as pin_debit_credits_amount, " +
		   "    sum( case when dt.card_type in ('MP','VP') and dt.debit_credit_indicator = 'D' then 1 else 0 end)                          as pinless_debit_sales_count, " +
		   "    sum( case when dt.card_type in ('MP','VP') and dt.debit_credit_indicator = 'D' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as pinless_debit_sales_amount, " +
		   "    sum( case when dt.card_type in ('MP','VP') and dt.debit_credit_indicator = 'C' then 1 else 0 end)                          as pinless_debit_credits_count, " +
		   "    sum( case when dt.card_type in ('MP','VP') and dt.debit_credit_indicator = 'C' then 1 else 0 end " +
		   "      *nvl(dt.transaction_amount,0))                                                                                           as pinless_debit_credits_amount, " +
		   "    dt.original_currency_code                                                                                                  as currency_code_to_fbo, " +
		   "    dt.currency_code                                                                                                           as currency_code_to_merchant " +
		   "  FROM daily_detail_file_dt dt " +
		   "  WHERE dt.merchant_account_number = :1 " +
		   "  and dt.batch_date between :2 and last_day( :3 ) " +
		   "  and dt.reject_reason is null " +
		   "  and dt.bank_number in (:4,:5) " +
		   "  GROUP BY dt.original_currency_code, dt.currency_code ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.WFThinFileUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AccountData.MerchantId);
   __sJT_st.setDate(2,AccountData.ActiveDate);
   __sJT_st.setDate(3,AccountData.ActiveDate);
   __sJT_st.setInt(4,bankNumberWellsFargo1);
   __sJT_st.setInt(5,bankNumberWellsFargo2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.WFThinFileUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^9*/
        resultSet = it.getResultSet();
        boolean hasData = resultSet.next();        
        Vector <FieldGroup> fgroupList = new Vector<>();
        boolean volumeDataList = true ;
        
        while (volumeDataList) {
			FieldGroup fgroupThin = FormsFactory.buildFieldGroup(MesFlatFiles.DEF_TYPE_WF_THIN_FILE, "wfThinFile");
			copyFieldGroup(fgroup, fgroupThin);
        	
            if (hasData) {
                FieldBean.setFields(fgroupThin,resultSet,false,false);
            }
        
            // set the CNP percentages
            for ( int fldIdx = 0; fldIdx < CardPresentFieldNames.length; ++fldIdx )
            {
              String  cnpFieldName  = "cnp_" + CardPresentFieldNames[fldIdx];
              double  cnpFieldValue = hasData ? resultSet.getDouble(cnpFieldName) : 0.0;
              double  cpFieldValue  = fgroupThin.getField(CardPresentFieldNames[fldIdx]).asDouble();
          
              if ( cnpFieldValue != 0.0 && cpFieldValue != 0.0 )
              {
                  fgroupThin.setData(cnpFieldName,(cnpFieldValue/cpFieldValue));
              }
            }
        
            // set the descriptor count if necessary
            descriptorCount = hasData ? resultSet.getInt("descriptor_count") : 0;
            if ( descriptorCount > 1 )
            {
                fgroupThin.setData("multiple_descriptor_flag" ,"Y");
                fgroupThin.setData("multiple_descriptor_count",descriptorCount);
            }
            else
            {
                fgroupThin.setData("multiple_descriptor_flag" ,"N");
            }
            fgroupList.add(fgroupThin);
            hasData = resultSet.next();
            volumeDataList = hasData;
        }
        
        resultSet.close();
        it.close();
        
        // load the chargebacks
        /*@lineinfo:generated-code*//*@lineinfo:298^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(decode(cb.card_type,'VS',1,0))                  as vs_cb_count,
//                    sum(decode(cb.card_type,'VS',1,0) * cb.tran_amount) as vs_cb_amount,
//                    sum(decode(cb.card_type,'MC',1,0))                  as mc_cb_count,
//                    sum(decode(cb.card_type,'MC',1,0) * cb.tran_amount) as mc_cb_amount,
//                    sum(decode(cb.card_type,'AM',1,0))                  as am_cb_count,
//                    sum(decode(cb.card_type,'AM',1,0) * cb.tran_amount) as am_cb_amount,
//                    sum(decode(cb.card_type,'DS',1,0))                  as ds_cb_count,
//                    sum(decode(cb.card_type,'DS',1,0) * cb.tran_amount) as ds_cb_amount
//            from    mif mf,
//                    network_chargebacks     cb
//            where   mf.merchant_number = :AccountData.MerchantId
//                    and cb.merchant_number = mf.merchant_number
//                    and cb.incoming_date between :AccountData.MonthBeginDate and :AccountData.MonthEndDate
//                    and cb.first_time_chargeback = 'Y'
//                    and cb.bank_number = :BankNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "  SELECT " +
   		   "    sum(decode(cb.card_type,'VS', 1, 0))                                as vs_cb_count, " +
		   "    sum(decode(cb.card_type,'VS', 1, 0) * cb.tran_amount)               as vs_cb_amount, " +
		   "    sum(decode(cb.card_type,'MC', 1, 0))                                as mc_cb_count, " +
		   "    sum(decode(cb.card_type,'MC', 1, 0) * cb.tran_amount)               as mc_cb_amount, " +
		   "    sum(decode(cb.card_type,'AM', 1, 0))                                as am_cb_count, " +
		   "    sum(decode(cb.card_type,'AM', 1, 0) * cb.tran_amount)               as am_cb_amount, " +
		   "    sum(decode(cb.card_type,'DS', 1, 0))                                as ds_cb_count, " +
		   "    sum(decode(cb.card_type,'DS', 1, 0) * cb.tran_amount)               as ds_cb_amount, " +
		   "    sum(decode(cb.card_type,'DB', 1,'EB', 1, 0))                        as pin_debit_cb_count, " +
		   "    sum(decode(cb.card_type,'DB', 1,'EB', 1, 0) * cb.tran_amount)       as pin_debit_cb_amount, " +
		   "    sum(decode(cb.card_type,'MP', 1,'VP', 1, 0))                        as pinless_debit_cb_count, " +
		   "    sum(decode(cb.card_type,'MP', 1,'VP', 1, 0) * cb.tran_amount)       as pinless_debit_cb_amount " +
		   "  FROM mif mf, " +
		   "    network_chargebacks cb " +
		   "  WHERE mf.merchant_number = :1 " +
		   "  and cb.merchant_number   = mf.merchant_number " +
		   "  and cb.incoming_date between :2 and :3 " +
		   "  and cb.first_time_chargeback = 'Y' " +
		   "  and cb.bank_number           in (:4,:5) ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.WFThinFileUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AccountData.MerchantId);
   __sJT_st.setDate(2,AccountData.MonthBeginDate);
   __sJT_st.setDate(3,AccountData.MonthEndDate);
   __sJT_st.setInt(4,bankNumberWellsFargo1);
   __sJT_st.setInt(5,bankNumberWellsFargo2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.WFThinFileUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:315^9*/
        resultSet = it.getResultSet();
        resultSet.next();
        int cbIndex = IntStream.range(0,fgroupList.size()).filter(i -> "840".equals(fgroupList.get(i).getField("currency_code_to_fbo").getData())).findFirst().orElse(0);
        FieldGroup cbFgroup = fgroupList.get(cbIndex);
        FieldBean.setFields(cbFgroup,resultSet,false,false);
        resultSet.close();
        it.close();
        fgroupList.set(cbIndex, cbFgroup);
        for (FieldGroup fieldGroup : fgroupList) {
        	outputRecord(fieldGroup);
        }
      }
      catch( Exception e )
      {
        logEntry("run()", e.toString() + " ("+AccountData.MerchantId + ")");
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  private int               bankNumberWellsFargo1 = 3003;
  private int               bankNumberWellsFargo2 = 3943;
  private BufferedWriter    OutputFile    = null;
  private int               RecCount      = 0;
  private int               TotalRecCount = 0;
  
  public WFThinFileUpload()
  {
    this( false );
  }
  
  public WFThinFileUpload( boolean testMode )
  {
    TestMode = testMode;
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_WFAB_NOTIFY;   
    EmailGroupFailure = MesEmails.MSG_ADDRS_OUTGOING_WFAB_FAILURE;  
  }
  
  public boolean execute()
  {
    Vector              accounts            = new Vector();
    ResultSetIterator   it                  = null;
    ThreadPool          pool                = null;
    ResultSet           resultSet           = null;
    boolean             retVal              = false;
    String              outputFilename      = "";
    
    try
    {
      connect(true);
      
      Date            activeDate  = null;
      Date            fileDate    = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:370^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(trunc(sysdate,'month')-1,'month'),
//                  trunc(sysdate,'month')-1
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(trunc(sysdate,'month')-1,'month'),\n                trunc(sysdate,'month')-1\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.WFThinFileUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   fileDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^7*/
      
      // open the output file
      outputFilename  = "1186_AltE_" + DateTimeFormatter.getFormattedDate(fileDate,"yyyyMMdd") + "_MES_ThinFile.txt";
      OutputFile      = new BufferedWriter( new FileWriter(outputFilename, false ) );
      
      // PROD-1973: Query changed to pull merchants as described in JIRA
      /*@lineinfo:generated-code*//*@lineinfo:383^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merch_num merchant_number,
//      		:activeDate active_date,
//      		:activeDate month_begin_date,
//      		last_day(:activeDate) month_end_date
//      	from (        
//      		-- All closed accounts for a period of 6 months        
//      		select mf.merchant_number merch_num
//      		from mif mf
//      		where nvl(dmacctst,'A') in ( 'D','C','B' ) 
//      			and mf.date_stat_chgd_to_dcb >= add_months(:activeDate,-5) 
//      			and mf.bank_number = :BankNumber
//      		union
//      		-- All closed accounts with any activity in the last month
//      		select mf.merchant_number merch_num
//      		from mif mf, network_chargebacks nc 
//      		where nvl(dmacctst,'A') in ( 'D','C','B' ) 
//      			and mf.bank_number = :BankNumber
//      			and nc.merchant_number = mf.merchant_number 
//      			and nc.incoming_date between :activeDate and last_day(:activeDate)
//      		union
//      		--  All Open accounts
//      		select mf.merchant_number merch_num
//      		from mif mf
//      		where nvl(dmacctst,'A') not in ( 'D','C','B' ) 
//      			and mf.bank_number = :BankNumber)
//      		group by merch_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "  select merch_num merchant_number, " +
		   "    :1 active_date, " +
		   "    :2 month_begin_date, " +
		   "    last_day( :3 ) month_end_date " +
		   "  from " +
		   "    (select mf.merchant_number merch_num " +
		   "    from mif mf " +
		   "    where nvl(dmacctst,'A')                        in ( 'D','C','B' ) " +
		   "    and mf.date_stat_chgd_to_dcb >= add_months( :4 ,-5) " +
		   "    and mf.bank_number            in (:5,:6) " +
		   "    union " +
		   "    select mf.merchant_number merch_num " +
		   "    from mif mf, " +
		   "      network_chargebacks nc " +
		   "    where nvl(dmacctst,'A') in ( 'D','C','B' ) " +
		   "    and mf.bank_number       in (:7,:8) " +
		   "    and nc.merchant_number   = mf.merchant_number " +
		   "    and nc.incoming_date between :9 and last_day( :10 ) " +
		   "    union " +
		   "    select mf.merchant_number merch_num " +
		   "    from mif mf " +
		   "    where nvl(dmacctst,'A') not in ( 'D','C','B' ) " +
		   "    and mf.bank_number           in (:11,:12)) " +
		   "  group by merch_num ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.WFThinFileUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setInt(5,bankNumberWellsFargo1);
   __sJT_st.setInt(6,bankNumberWellsFargo2);
   __sJT_st.setInt(7,bankNumberWellsFargo1);
   __sJT_st.setInt(8,bankNumberWellsFargo2);
   __sJT_st.setDate(9,activeDate);
   __sJT_st.setDate(10,activeDate);
   __sJT_st.setInt(11,bankNumberWellsFargo1);
   __sJT_st.setInt(12,bankNumberWellsFargo2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.WFThinFileUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        accounts.addElement( new ThinFileAccount(resultSet) );
      }
      resultSet.close();
      it.close();
      
      TotalRecCount = accounts.size();
      
      pool = new ThreadPool(10);
      for( int i = 0; i < accounts.size(); ++i )
      {
        Thread thread = pool.getThread( new ThinFileWorker((ThinFileAccount)accounts.elementAt(i)) );
        thread.start();
      }
      pool.waitForAllThreads();
      
      OutputFile.close();
      
      // transmit file when in production mode only
      if( TestMode == false )
      {
        if ( sendDataFile( outputFilename,
                           MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST),     
                           MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER),     
                           MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD), 
                           MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_AB_PATH),  
                           false,                                                      // !binary
                           true ) )                                                    // send flag file
        {
          // send success email
          sendEmail(true, outputFilename);
          
          // send email along with file to operations
          sendEmailFile( outputFilename );
          
          // archive file
          archiveMonthlyFile( outputFilename );
        }
        else
        {
          sendEmail(false, outputFilename);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ OutputFile.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( retVal );
  }
  
  private void sendEmailFile( String filename )
  {
    StringBuffer  msgBody = new StringBuffer("Wells Fargo Thin File Success");
    String        subject = "WF Thin File Success (attached)";
    
    try
    {
      MailMessage msg = new MailMessage();
      
      msgBody.append("\n\n" + filename);
      msg.setAddresses(MesEmails.MSG_ADDRS_WF_THIN_FILE);
      
      msg.setSubject(subject);
      msg.setText(msgBody.toString());
      msg.addFile(filename);
      
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendEmailFile(" + filename + ")", e.toString());
    }
  }
  
  private void sendEmail(boolean success, String filename)
  {
    StringBuffer  msgBody = new StringBuffer("Wells Fargo Thin File ");
    StringBuffer  subject = new StringBuffer("WF Thin File");
    
    try
    {
      MailMessage msg = new MailMessage();
      
      // create subject and messsage body header
      if( success )
      {
        msgBody.append("SUCCESS ");
        subject.append("SUCCESS ");
        msg.setAddresses(EmailGroupSuccess);
      }
      else
      {
        msgBody.append("FAILURE ");
        subject.append("FAILURE ");
        msg.setAddresses(EmailGroupFailure);
      }
    
      String curTime = com.mes.support.DateTimeFormatter.getCurDateTimeString();
      msgBody.append( curTime );
      subject.append( curTime );
      
      msgBody.append(": ");
      msgBody.append(filename);
    
      msgBody.append("\n\n");
      
      msg.setSubject(subject.toString());
      msg.setText(msgBody.toString());
      msg.send();
    }      
    catch(Exception e)
    {
      logEntry("sendEmail()", e.toString());
    }
  }
  
  protected synchronized void outputRecord( FieldGroup fgroup )
  {
    String        delimiter       = "\t";
    Field         field           = null;
    Vector        fieldsVector    = null;
    
    try
    {
      fieldsVector = fgroup.getFieldsVector();
      
      // output the file header on the first row
      if ( RecCount == 0 )
      {
        for( int i = 0; i < fieldsVector.size(); ++i )
        {
          field = (Field)fieldsVector.elementAt(i);
          OutputFile.write((i == 0) ? "" : delimiter);
          OutputFile.write( (field.getName().startsWith("cnp_") ? "% " : "") 
                            + field.getName().toUpperCase().replaceAll("_"," "));
        }
        OutputFile.newLine();
      }
      
      // output the field data
      for( int i = 0; i < fieldsVector.size(); ++i )
      {
        field = (Field)fieldsVector.elementAt(i);
        
        if ( field instanceof NumberField && field.isBlank() )
        {
          field.setData("0");
        }
        OutputFile.write((i == 0) ? "" : delimiter);
        OutputFile.write(field.getRenderData());
      }
      OutputFile.newLine();
      
      ++RecCount;
      
      if( TestMode == true )
      {
        System.out.print("Processed " + RecCount + " of " + TotalRecCount + " (" + NumberFormatter.getPercentString(((double)RecCount/(double)TotalRecCount),3) + ")            \r");
      }
    }
    catch( Exception e )
    {
      logEntry("outputRecord()",e.toString());
    }
  }
  
	private void copyFieldGroup(FieldGroup fields1, FieldGroup fields2) {
		Vector allFields = fields1.getFieldsVector();
		for (int i = 0; i < allFields.size(); ++i) {
			Field f = (Field) (allFields.elementAt(i));
			String fname = f.getName();
			fields2.getField(fname).setData(f.getData());
		}
	}
  
  public static void main( String[] args )
  {
      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_WF_OUTGOING_HOST,
                      MesDefaults.DK_WF_OUTGOING_USER,
                      MesDefaults.DK_WF_OUTGOING_PASSWORD,
                      MesDefaults.DK_WF_OUTGOING_AB_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    WFThinFileUpload   testBean    = null;
    
    SQLJConnectionBase.initStandalonePool();
    
    try
    {
      // instantiate in test mode
      testBean = new WFThinFileUpload(true);
      testBean.connect(true);
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
           if (             "xxx".equals(args[0]) ) { int dummy = 1; }
      else if (         "execute".equals(args[0]) ) { testBean.execute();     }
      
      System.out.println("\n\n\n\n");
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    finally
    {
      try{ testBean.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }  
}/*@lineinfo:generated-code*/