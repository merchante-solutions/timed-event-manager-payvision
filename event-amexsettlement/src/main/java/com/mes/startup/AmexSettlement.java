/*@lineinfo:filename=AmexSettlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/AmexSettlement.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class AmexSettlement extends AmexFileBase
{
  public AmexSettlement( )
  {
    PropertiesFilename          = "amex-settlement.properties";

    CardTypeTable               = "amex_settlement";
    CardTypeTableActivity       = "amex_settlement_activity";
    CardTypePostTotals          = "am_settle";
    CardTypeFileDesc            = "Amex Settlement";
    DkOutgoingHost              = MesDefaults.DK_AMEX_OUTGOING_HOST;
    DkOutgoingUser              = MesDefaults.DK_AMEX_OUTGOING_USER;
    DkOutgoingPassword          = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_AMEX_OUTGOING_PATH;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_AMEX_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_AMEX_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_FIRST_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    int                       bankNumber        = Integer.parseInt( getEventArg(0) );
    String                    bankNumberClause  = (bankNumber == 9999) ? "" : " and am.bank_number = " + bankNumber + " ";
    Date                      cpd               = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    long                      workFileId        = 0L;
    String                    workFilename      = null;

    try
    {
      String optClause = " and am.amex_se_number not in (" +  getSeList() +")";
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());

        cpd             = getSettlementDate();

        workFilename    = generateFilename(( "R".equals(ActionCode) ? "am_reversals" : "am_settle" )    // base name
                                          + String.valueOf(bankNumber),                                 // bank number
                                          ".dat");                                                      // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);
        
        
        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:84^11*/

//  ************************************************************
//  #sql [Ctx] { update  amex_settlement   am
//              set     am.output_filename  = :workFilename,
//                      am.output_file_id   = :workFileId,
//                      am.settlement_date  = :cpd
//              where   am.output_file_id = 0
//                      and am.output_filename is null
//                      and am.test_flag = 'N'
//                      :bankNumberClause
//                      :optClause
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  amex_settlement   am\n            set     am.output_filename  =  ? ,\n                    am.output_file_id   =  ? ,\n                    am.settlement_date  =  ? \n            where   am.output_file_id = 0\n                    and am.output_filename is null\n                    and am.test_flag = 'N'\n                     ");
   __sjT_sb.append(bankNumberClause);
   __sjT_sb.append(" \n                     ");
   __sjT_sb.append(optClause);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.AmexSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:99^11*/

//  ************************************************************
//  #sql [Ctx] { update  amex_settlement_activity   ama
//              set     ama.load_filename  = :workFilename,
//                      ama.load_file_id   = :workFileId,
//                      ama.settlement_date  = :cpd
//              where   ama.load_file_id = 0
//                      and ama.load_filename is null
//                      and ama.action_code = :ActionCode
//                      and exists
//                      (
//                        select  am.rec_id
//                        from    amex_settlement am
//                        where   am.rec_id = ama.rec_id
//                                :bankNumberClause
//                                :optClause
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  amex_settlement_activity   ama\n            set     ama.load_filename  =  ? ,\n                    ama.load_file_id   =  ? ,\n                    ama.settlement_date  =  ? \n            where   ama.load_file_id = 0\n                    and ama.load_filename is null\n                    and ama.action_code =  ? \n                    and exists\n                    (\n                      select  am.rec_id\n                      from    amex_settlement am\n                      where   am.rec_id = ama.rec_id\n                               ");
   __sjT_sb.append(bankNumberClause);
   __sjT_sb.append(" \n                               ");
   __sjT_sb.append(optClause);
   __sjT_sb.append(" \n                    )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.startup.AmexSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setString(4,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^11*/
        }
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
        recCount        = 1;   // always re-build
      }

      if ( recCount > 0 )
      {
        // select the transactions to process
        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:134^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  am.*
//              from    amex_settlement           am
//              where   am.output_file_id = :workFileId
//                      and am.test_flag = 'N'
//              order by am.batch_id, am.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  am.*\n            from    amex_settlement           am\n            where   am.output_file_id =  :1 \n                    and am.test_flag = 'N'\n            order by am.batch_id, am.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AmexSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.AmexSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:145^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  am.*
//              from    amex_settlement_activity  ama,
//                      amex_settlement           am
//              where   ama.load_file_id = :workFileId
//                      and ama.action_code = :ActionCode
//                      and am.rec_id = ama.rec_id
//              order by am.batch_id, am.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  am.*, "
   		+ " ama.reproc as external_reject "
   		+ " from amex_settlement_activity  ama,"
   		+ " amex_settlement am where ama.load_file_id =  :1"
   		+ " and ama.action_code =  :2 "
   		+ " and am.rec_id = ama.rec_id "
   		+ " order by am.batch_id, am.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AmexSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   __sJT_st.setString(2,ActionCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.AmexSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^11*/
        }

        FileReferenceId = String.valueOf(workFileId);
        handleSelectedRecords( it, workFilename,workFileId );
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }

  
  protected String getSeList(){
	  String seL = "";
	  StringBuffer sb = new StringBuffer();
	  ResultSet rs = null;
	  ResultSetIterator it = null;
	  try{
		  /*@lineinfo:generated-code*//*@lineinfo:180^5*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct amex_optblue_se_number as senum 
//  		  		from amex_se_mapping 
//  	          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct amex_optblue_se_number as senum \n\t\t  \t\tfrom amex_se_mapping";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.AmexSettlement",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.AmexSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^11*/
	  rs = it.getResultSet();  
	  while(rs.next()){
		  sb.append(",");
		  sb.append(rs.getString("senum"));
	  }
	  sb.deleteCharAt(0);
	  }
	  catch(Exception e){
		e.printStackTrace();
	    logEvent(this.getClass().getName(), "getSeList()", e.toString());
	    logEntry("getSeList()", e.toString());
	  } finally
	    {
	      try{ it.close(); rs.close(); } catch(Exception e) {}
	    }
	  
	  return sb.toString();
	  
  }
  
  
  public static void main( String[] args )
  {

    AmexSettlement                    test          = null;
    
    try
    { 
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_AMEX_OUTGOING_HOST, MesDefaults.DK_AMEX_OUTGOING_USER,
                    MesDefaults.DK_AMEX_OUTGOING_PASSWORD, MesDefaults.DK_AMEX_OUTGOING_PATH
            });
        }

      test = new AmexSettlement();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/