/*@lineinfo:filename=TridentDebitAdjustmentUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentDebitAdjustmentUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.ach.AchEntryData;
import com.mes.ach.AchOriginList;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.SettlementTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class TridentDebitAdjustmentUpload extends TsysFileBase
{
  static Logger log = Logger.getLogger(TridentDebitAdjustmentUpload.class);

  private final static int[] ActiveBanks = 
  {
    mesConstants.BANK_ID_MES,
    mesConstants.BANK_ID_CBT,
  };
  
  // these are the banks that use the MES ACH system
  // for issuing debit adjustments.
  private final static int[] ActiveBanksMES = 
  {
    mesConstants.BANK_ID_STERLING,
  };
  
  public TridentDebitAdjustmentUpload( )
  {
    PropertiesFilename = "trident-debit-upload.properties";
    EmailGroupSuccess  = MesEmails.MSG_ADDRS_DEBIT_ADJ_NOTIFY;
    EmailGroupFailure  = MesEmails.MSG_ADDRS_DEBIT_ADJ_FAILURE;
  }
  
  private void buildAdjustmentDetails( String loadFilename, BufferedWriter out )
  {
    FlatFileRecord        ffd             = null;
    ResultSetIterator     it              = null;
    ResultSet             resultSet       = null;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MRCH3);
      ffd.connect(true);
    
      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(dt.bank_number,
//                         3858,'0','') ||
//                  to_char(dt.merchant_number)         as merchant_number,
//                  tda.action_code                     as action_code,
//                  ac.adj_tran_code                    as tran_code,
//                  tda.action_date                     as tran_date,
//                  substr(dt.retrieval_reference_number,-11) as ref_num,
//                  -- extract as an unsigned integer value
//                  abs(dt.transaction_amount)          as tran_amount,
//                  dt.transaction_amount               as tran_amount_signed,
//                  dt.card_number                      as card_number,                       
//                  'U'                                 as volume_indicator,
//                  'D'                                 as debit_credit_ind                       
//          from    trident_debit_financial       dt,
//                  trident_debit_activity        tda,
//                  trident_debit_action_codes    ac,
//                  mif                           mf
//          where   dt.bank_number = :FileBankNumber and
//                  dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                  tda.rec_id = dt.rec_id and
//                  tda.load_filename = :loadFilename and
//                  ac.short_action_code = tda.action_code and
//                  not ac.adj_tran_code is null and
//                  mf.merchant_number = dt.merchant_number
//          order by  dt.merchant_number, dt.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(dt.bank_number,\n                       3858,'0','') ||\n                to_char(dt.merchant_number)         as merchant_number,\n                tda.action_code                     as action_code,\n                ac.adj_tran_code                    as tran_code,\n                tda.action_date                     as tran_date,\n                substr(dt.retrieval_reference_number,-11) as ref_num,\n                -- extract as an unsigned integer value\n                abs(dt.transaction_amount)          as tran_amount,\n                dt.transaction_amount               as tran_amount_signed,\n                dt.card_number                      as card_number,                       \n                'U'                                 as volume_indicator,\n                'D'                                 as debit_credit_ind                       \n        from    trident_debit_financial       dt,\n                trident_debit_activity        tda,\n                trident_debit_action_codes    ac,\n                mif                           mf\n        where   dt.bank_number =  :1  and\n                dt.settlement_date between ( :2 -180) and  :3  and\n                tda.rec_id = dt.rec_id and\n                tda.load_filename =  :4  and\n                ac.short_action_code = tda.action_code and\n                not ac.adj_tran_code is null and\n                mf.merchant_number = dt.merchant_number\n        order by  dt.merchant_number, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TridentDebitAdjustmentUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:111^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        ffd.resetAllFields();
        
        // ** all debit adjustments are "debits"
        BatchDebitsAmount  += resultSet.getDouble("tran_amount");
        BatchDebitsCount++;
        FileDebitsAmount  += resultSet.getDouble("tran_amount");
        FileDebitsCount++;
        
        // build the net deposit string
        ffd.setFieldData("net_deposit", TridentTools.encodeCobolAmount(resultSet.getDouble("tran_amount_signed"),(ffd.findFieldByName("net_deposit")).getLength()));
        
        // setup the sales/credits field for the merchant statements
        // ** all debit adjustments are "debits"
        ffd.setFieldData("sales_count",1);
        ffd.setFieldData("sales_amount",resultSet.getDouble("tran_amount"));
        
        // build the common 1 record
        ffd.setAllFieldData(resultSet);
        
        // Visa/MC are decoded in the query, all other 
        // card types use the card bin to determine card type
        ffd.setFieldData("plan_rep","DB");
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildAdjDetails()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  private void buildAdjustmentHeader( String loadFilename, BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;
    ResultSetIterator it                = null;
    ResultSet         resultSet         = null;
    
    try
    {
      // build the batch header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_HDR);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("bank_number",FileBankNumber);
      ffd.setFieldData("batch_format_ind","MRCH3");
      
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("batch_number",1);
      /*@lineinfo:generated-code*//*@lineinfo:172^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate)-5                          as default_tran_date,
//                  substr(dt.retrieval_reference_number,-11) as adj_ref_num
//          from    trident_debit_financial       dt,
//                  trident_debit_activity        tda
//          where   dt.bank_number = :FileBankNumber and
//                  dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                  tda.rec_id = dt.rec_id and
//                  tda.load_filename = :loadFilename
//          order by dt.card_number,  dt.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate)-5                          as default_tran_date,\n                substr(dt.retrieval_reference_number,-11) as adj_ref_num\n        from    trident_debit_financial       dt,\n                trident_debit_activity        tda\n        where   dt.bank_number =  :1  and\n                dt.settlement_date between ( :2 -180) and  :3  and\n                tda.rec_id = dt.rec_id and\n                tda.load_filename =  :4 \n        order by dt.card_number,  dt.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TridentDebitAdjustmentUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      ffd.setFieldData("tran_date_override",resultSet.getDate("default_tran_date"));
      ffd.setFieldData("ref_num_begin",resultSet.getLong("adj_ref_num"));
      resultSet.close();
      it.close();
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildAdjustmentHeader()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildAdjustmentTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // create the batch trailer record
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_TRL);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("batch_format_ind","MRCH3");
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("bank_number", FileBankNumber);
      ffd.setFieldData("batch_number",1);
      ffd.setFieldData("batch_amount_net", (BatchDebitsAmount - BatchCreditsAmount));
      ffd.setFieldData("debits_amount", BatchDebitsAmount);
      ffd.setFieldData("credits_amount", BatchCreditsAmount);
      ffd.setFieldData("payments_amount", BatchPaymentsAmount);
      ffd.setFieldData("debits_count", BatchDebitsCount);
      ffd.setFieldData("credits_count", BatchCreditsCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildAdjustmentTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      processTsysAccounts();
      processMbsAccounts();
      processMESBanks();
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  protected boolean processMbsAccounts()
  {
    AchEntryData        achData         = null;
    Vector              achEntries      = null;
    AchOriginList       achOriginList   = null;
    long                achSequence     = 0L;
    long                actionRecId     = 0L;
    Date                batchDate       = null;
    ResultSetIterator   it              = null;
    long                originNode      = 0L;
    BufferedWriter      out             = null;
    long                recId           = 0L;
    ResultSet           resultSet       = null;
    boolean             retVal          = false;
    int                 rowCount        = 0;
    long                traceNumber     = 0L;
    String              workFilename    = null;
  
    try
    {
      log.debug("Processing MBS Accounts...");
      
      for( int i = 0; i < ActiveBanks.length; ++i )
      {
        FileBankNumber = ActiveBanks[i];
    
        if ( TestFilename == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:291^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^11*/
          workFilename = buildFilename( ("mdbt_adj" + String.valueOf(FileBankNumber)) );
  
          /*@lineinfo:generated-code*//*@lineinfo:298^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_activity    tda
//              set     tda.load_filename = :workFilename
//              where   tda.rec_id in
//                      (
//                        select  dt.rec_id
//                        from    trident_debit_financial   dt,
//                                mif                       mf
//                        where   dt.bank_number = :FileBankNumber and
//                                dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate
//                                and mf.merchant_number = dt.merchant_number
//                                and nvl(mf.processor_id,0) = 1  -- mbs only
//                      ) and
//                      tda.load_filename is null
//                      and exists
//                      ( 
//                        select  ac.short_action_code
//                        from    trident_debit_action_codes    ac
//                        where   ac.short_action_code = tda.action_code and
//                                not ac.adj_tran_code is null
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_debit_activity    tda\n            set     tda.load_filename =  :1 \n            where   tda.rec_id in\n                    (\n                      select  dt.rec_id\n                      from    trident_debit_financial   dt,\n                              mif                       mf\n                      where   dt.bank_number =  :2  and\n                              dt.settlement_date between ( :3 -180) and  :4 \n                              and mf.merchant_number = dt.merchant_number\n                              and nvl(mf.processor_id,0) = 1  -- mbs only\n                    ) and\n                    tda.load_filename is null\n                    and exists\n                    ( \n                      select  ac.short_action_code\n                      from    trident_debit_action_codes    ac\n                      where   ac.short_action_code = tda.action_code and\n                              not ac.adj_tran_code is null\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setDate(4,FileActivityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:320^11*/
        }          
        else  // allow for test files to be specified
        {
          workFilename    = TestFilename;
          FileBankNumber  = getFileBankNumber(TestFilename);
    
          /*@lineinfo:generated-code*//*@lineinfo:327^11*/

//  ************************************************************
//  #sql [Ctx] { select  max(tda.action_date) 
//              from    trident_debit_activity      tda
//              where   tda.load_filename = :workFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(tda.action_date)  \n            from    trident_debit_activity      tda\n            where   tda.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^11*/
        }
    
        /*@lineinfo:generated-code*//*@lineinfo:335^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(dt.merchant_number) 
//            from    trident_debit_financial       dt,
//                    trident_debit_activity        tda,
//                    trident_debit_action_codes    ac
//            where   dt.bank_number = :FileBankNumber and
//                    dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                    tda.rec_id = dt.rec_id and
//                    tda.load_filename = :workFilename and
//                    ac.short_action_code = tda.action_code and
//                    not ac.adj_tran_code is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(dt.merchant_number)  \n          from    trident_debit_financial       dt,\n                  trident_debit_activity        tda,\n                  trident_debit_action_codes    ac\n          where   dt.bank_number =  :1  and\n                  dt.settlement_date between ( :2 -180) and  :3  and\n                  tda.rec_id = dt.rec_id and\n                  tda.load_filename =  :4  and\n                  ac.short_action_code = tda.action_code and\n                  not ac.adj_tran_code is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:347^9*/
        
        log.debug("Bank Number   : " + FileBankNumber);
        log.debug("Activity Date : " + DateTimeFormatter.getFormattedDate(FileActivityDate,"MM/dd/yyyy"));
        log.debug("Work Filename : " + workFilename);
        
        if ( rowCount > 0 )
        {
          if ( achOriginList == null )
          {
            achOriginList = new AchOriginList();
          }
          if ( achEntries == null )
          {
            achEntries = new Vector();
          }
          else
          {
            achEntries.removeAllElements();
          }
          
          // get the batch date, cutoff is 0030 hours
          batchDate = SettlementTools.getSettlementBatchDate();
          
          /*@lineinfo:generated-code*//*@lineinfo:371^11*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_file_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_file_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:377^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  td.rec_id                             as rec_id,
//                      tda.action_rec_id                     as action_rec_id,
//                      mf.merchant_number                    as merchant_number,
//                      mf.dba_name                           as dba_name,
//                      o.org_group                           as deposit_merchant_number,
//                      o.org_name                            as deposit_dba_name,
//                      0                                     as roll_up_deposit,
//                      lpad(nvl(atn.deposit_transit_routing,
//                               mf.transit_routng_num), 9, '0')   
//                                                            as transit_routing,
//                      nvl(atn.deposit_dda,mf.dda_num)       as dda_num,
//                      (trunc(sysdate) + nvl(mf.suspended_days,0))
//                                                            as post_date,
//                      'CCD'                                 as class_code,
//                      :AchEntryData.ED_ADJUSTMENTS        as entry_desc,
//                      :achSequence                          as ach_sequence,
//                      1                                     as batch_number,
//                      ( decode(td.debit_credit_indicator,'D',1,0) )
//                                                            as sales_count,
//                      ( decode(td.debit_credit_indicator,'D',1,0) *
//                        td.transaction_amount )             as sales_amount,
//                      ( decode(td.debit_credit_indicator,'C',1,0) )
//                                                            as credits_count,
//                      ( decode(td.debit_credit_indicator,'C',1,0) *
//                        td.transaction_amount )             as credits_amount,
//                      ( td.transaction_amount * 
//                        decode(td.debit_credit_indicator,'C',-1,1) ) 
//                                                            as net_amount
//              from    trident_debit_financial       td,
//                      trident_debit_activity        tda,
//                      trident_debit_action_codes    ac,
//                      mif                           mf,
//                      ach_trident_nodes             atn,
//                      organization                  o
//              where   td.bank_number = :FileBankNumber and
//                      td.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                      tda.rec_id = td.rec_id and
//                      tda.load_filename = :workFilename and
//                      ac.short_action_code = tda.action_code and
//                      not ac.adj_tran_code is null and
//                      mf.merchant_number = td.merchant_number and
//                      nvl(mf.processor_id,0) = 1 and  -- mbs only
//                      nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X' and
//                      atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,
//                                                          mf.trans_dest_global),
//                                                      'M', mf.association_node,
//                                                      0 ) and 
//                      o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)
//              order by  td.merchant_number, td.card_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  td.rec_id                             as rec_id,\n                    tda.action_rec_id                     as action_rec_id,\n                    mf.merchant_number                    as merchant_number,\n                    mf.dba_name                           as dba_name,\n                    o.org_group                           as deposit_merchant_number,\n                    o.org_name                            as deposit_dba_name,\n                    0                                     as roll_up_deposit,\n                    lpad(nvl(atn.deposit_transit_routing,\n                             mf.transit_routng_num), 9, '0')   \n                                                          as transit_routing,\n                    nvl(atn.deposit_dda,mf.dda_num)       as dda_num,\n                    (trunc(sysdate) + nvl(mf.suspended_days,0))\n                                                          as post_date,\n                    'CCD'                                 as class_code,\n                     :1         as entry_desc,\n                     :2                           as ach_sequence,\n                    1                                     as batch_number,\n                    ( decode(td.debit_credit_indicator,'D',1,0) )\n                                                          as sales_count,\n                    ( decode(td.debit_credit_indicator,'D',1,0) *\n                      td.transaction_amount )             as sales_amount,\n                    ( decode(td.debit_credit_indicator,'C',1,0) )\n                                                          as credits_count,\n                    ( decode(td.debit_credit_indicator,'C',1,0) *\n                      td.transaction_amount )             as credits_amount,\n                    ( td.transaction_amount * \n                      decode(td.debit_credit_indicator,'C',-1,1) ) \n                                                          as net_amount\n            from    trident_debit_financial       td,\n                    trident_debit_activity        tda,\n                    trident_debit_action_codes    ac,\n                    mif                           mf,\n                    ach_trident_nodes             atn,\n                    organization                  o\n            where   td.bank_number =  :3  and\n                    td.settlement_date between ( :4 -180) and  :5  and\n                    tda.rec_id = td.rec_id and\n                    tda.load_filename =  :6  and\n                    ac.short_action_code = tda.action_code and\n                    not ac.adj_tran_code is null and\n                    mf.merchant_number = td.merchant_number and\n                    nvl(mf.processor_id,0) = 1 and  -- mbs only\n                    nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X' and\n                    atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,\n                                                        mf.trans_dest_global),\n                                                    'M', mf.association_node,\n                                                    0 ) and \n                    o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)\n            order by  td.merchant_number, td.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_ADJUSTMENTS);
   __sJT_st.setLong(2,achSequence);
   __sJT_st.setInt(3,FileBankNumber);
   __sJT_st.setDate(4,FileActivityDate);
   __sJT_st.setDate(5,FileActivityDate);
   __sJT_st.setString(6,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.TridentDebitAdjustmentUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^11*/
          resultSet = it.getResultSet();
      
          while(resultSet.next())
          {
            originNode  = achOriginList.getMerchantOriginNode(resultSet.getLong("merchant_number"),AchEntryData.ED_ADJUSTMENTS);
            recId       = resultSet.getLong("rec_id");
            actionRecId = resultSet.getLong("action_rec_id");
            
            if ( originNode != 0L )
            {
              /*@lineinfo:generated-code*//*@lineinfo:439^15*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_trace_sequence.nextval 
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_trace_sequence.nextval  \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   traceNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:443^15*/
            
              achData = new AchEntryData();
              achData.setData(resultSet);
              achData.setOriginNode(originNode);  
              achData.setTraceNumber(traceNumber);
              achData.addStatementData(resultSet);
              achData.setSourceFilename(workFilename);
              achEntries.add(achData);    // add to the list to be stored
              
              // Add rows to daily_detail_file_adjustment for reporting
              /*@lineinfo:generated-code*//*@lineinfo:454^15*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_adjustment  adj
//                  (
//                    ach_flag,
//                    adjustment_amount,
//                    batch_date,
//                    batch_julian_date_dddd,
//                    batch_number,
//                    carryover_indicator,
//                    ddf_ad_id,
//                    debit_credit_ind,
//                    load_filename,
//                    merchant_account_number,
//                    merchant_reference_number,
//                    net_deposit,
//                    online_entry,
//                    record_identifier,
//                    reference_number,
//                    reversal_flag,
//                    transacction_code,
//                    transaction_date_mmddccyy
//                  )
//                  select  'Y'                             as ach_flag,
//                          abs(td.transaction_amount)      as adjustment_amount,
//                          :batchDate                      as batch_date,
//                          null                            as batch_julian_date_dddd,
//                          tda.action_rec_id               as batch_number,
//                          'Y'                             as carryover_indicator,
//                          td.rec_id                       as ddf_ad_id,
//                          decode(ac.adj_tran_code,
//                                 9072,'C',    -- dda credit
//                                 9073,'D',    -- dda debit
//                                 'D')                     as debit_credit_ind,
//                          tda.load_filename               as load_filename,
//                          td.merchant_number              as merchant_account_number,
//                          td.rec_id                       as merchant_reference_number,
//                          null                            as net_deposit,
//                          'N'                             as online_entry,
//                          'AD'                            as record_identifier,
//                          td.retrieval_reference_number   as reference_number,
//                          null                            as reversal_flag,
//                          ac.adj_tran_code                as tran_code,
//                          tda.action_date                 as transaction_date_mmddccyy
//                  from    trident_debit_financial     td,
//                          trident_debit_activity      tda,
//                          trident_debit_action_codes  ac
//                  where   td.rec_id = :recId
//                          and tda.rec_id = td.rec_id
//                          and tda.action_rec_id = :actionRecId
//                          and ac.short_action_code = tda.action_code
//                          and not ac.adj_tran_code is null
//                  order by td.rec_id, nvl(tda.action_rec_id,0)
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_adjustment  adj\n                (\n                  ach_flag,\n                  adjustment_amount,\n                  batch_date,\n                  batch_julian_date_dddd,\n                  batch_number,\n                  carryover_indicator,\n                  ddf_ad_id,\n                  debit_credit_ind,\n                  load_filename,\n                  merchant_account_number,\n                  merchant_reference_number,\n                  net_deposit,\n                  online_entry,\n                  record_identifier,\n                  reference_number,\n                  reversal_flag,\n                  transacction_code,\n                  transaction_date_mmddccyy\n                )\n                select  'Y'                             as ach_flag,\n                        abs(td.transaction_amount)      as adjustment_amount,\n                         :1                       as batch_date,\n                        null                            as batch_julian_date_dddd,\n                        tda.action_rec_id               as batch_number,\n                        'Y'                             as carryover_indicator,\n                        td.rec_id                       as ddf_ad_id,\n                        decode(ac.adj_tran_code,\n                               9072,'C',    -- dda credit\n                               9073,'D',    -- dda debit\n                               'D')                     as debit_credit_ind,\n                        tda.load_filename               as load_filename,\n                        td.merchant_number              as merchant_account_number,\n                        td.rec_id                       as merchant_reference_number,\n                        null                            as net_deposit,\n                        'N'                             as online_entry,\n                        'AD'                            as record_identifier,\n                        td.retrieval_reference_number   as reference_number,\n                        null                            as reversal_flag,\n                        ac.adj_tran_code                as tran_code,\n                        tda.action_date                 as transaction_date_mmddccyy\n                from    trident_debit_financial     td,\n                        trident_debit_activity      tda,\n                        trident_debit_action_codes  ac\n                where   td.rec_id =  :2 \n                        and tda.rec_id = td.rec_id\n                        and tda.action_rec_id =  :3 \n                        and ac.short_action_code = tda.action_code\n                        and not ac.adj_tran_code is null\n                order by td.rec_id, nvl(tda.action_rec_id,0)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,recId);
   __sJT_st.setLong(3,actionRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^15*/
            }
            else
            {
              // this will cause a system alert to be emailed
              logEntry("processMbsAccounts(" + workFilename + ")",
                       "WARNING:  Missing ACH origin node for merchant " + resultSet.getLong("merchant_number"));
            }              
          }
          resultSet.close();
          it.close();
          
          for( int achEntryIdx = 0; achEntryIdx < achEntries.size(); ++achEntryIdx )
          {
            achData = (AchEntryData)achEntries.elementAt(achEntryIdx);
            //@log.debug( String.valueOf(achData) );//@
            achData.store();
          }
          
          if ( TestFilename != null )
          {
            // exit the loop through active banks.
            break;
          }
        }
        else
        {
          log.debug("No records to process");
        }
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("processMbsAccounts()",e.toString());
    }
    return( retVal );
  }
  
  protected boolean processMESBanks()
  {
    AchEntryData        achData         = null;
    Vector              achEntries      = null;
    AchOriginList       achOriginList   = null;
    long                achSequence     = 0L;
    ResultSetIterator   it              = null;
    long                originNode      = 0L;
    BufferedWriter      out             = null;
    ResultSet           resultSet       = null;
    boolean             retVal          = false;
    int                 rowCount        = 0;
    long                traceNumber     = 0L;
    String              workFilename    = null;
  
    try
    {
      log.debug("Processing MES Banks...");
      
      for( int i = 0; i < ActiveBanksMES.length; ++i )
      {
        FileBankNumber = ActiveBanksMES[i];
    
        if ( TestFilename == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:571^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:575^11*/
          workFilename = buildFilename( ("tdbt_adj" + String.valueOf(FileBankNumber)) );
  
          /*@lineinfo:generated-code*//*@lineinfo:578^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_activity    tda
//              set     tda.load_filename = :workFilename
//              where   tda.rec_id in
//                      (
//                        select  dt.rec_id
//                        from    trident_debit_financial   dt
//                        where   dt.bank_number = :FileBankNumber and
//                                dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate
//                      ) and
//                      tda.load_filename is null
//                      and exists
//                      ( 
//                        select  ac.short_action_code
//                        from    trident_debit_action_codes    ac
//                        where   ac.short_action_code = tda.action_code and
//                                not ac.adj_tran_code is null
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_debit_activity    tda\n            set     tda.load_filename =  :1 \n            where   tda.rec_id in\n                    (\n                      select  dt.rec_id\n                      from    trident_debit_financial   dt\n                      where   dt.bank_number =  :2  and\n                              dt.settlement_date between ( :3 -180) and  :4 \n                    ) and\n                    tda.load_filename is null\n                    and exists\n                    ( \n                      select  ac.short_action_code\n                      from    trident_debit_action_codes    ac\n                      where   ac.short_action_code = tda.action_code and\n                              not ac.adj_tran_code is null\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setDate(4,FileActivityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:597^11*/
        }          
        else  // allow for test files to be specified
        {
          workFilename    = TestFilename;
          FileBankNumber  = getFileBankNumber(TestFilename);
    
          /*@lineinfo:generated-code*//*@lineinfo:604^11*/

//  ************************************************************
//  #sql [Ctx] { select  max(tda.action_date) 
//              from    trident_debit_activity      tda
//              where   tda.load_filename = :workFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(tda.action_date)  \n            from    trident_debit_activity      tda\n            where   tda.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:609^11*/
        }
    
        /*@lineinfo:generated-code*//*@lineinfo:612^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(dt.merchant_number) 
//            from    trident_debit_financial       dt,
//                    trident_debit_activity        tda,
//                    trident_debit_action_codes    ac
//            where   dt.bank_number = :FileBankNumber and
//                    dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                    tda.rec_id = dt.rec_id and
//                    tda.load_filename = :workFilename and
//                    ac.short_action_code = tda.action_code and
//                    not ac.adj_tran_code is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(dt.merchant_number)  \n          from    trident_debit_financial       dt,\n                  trident_debit_activity        tda,\n                  trident_debit_action_codes    ac\n          where   dt.bank_number =  :1  and\n                  dt.settlement_date between ( :2 -180) and  :3  and\n                  tda.rec_id = dt.rec_id and\n                  tda.load_filename =  :4  and\n                  ac.short_action_code = tda.action_code and\n                  not ac.adj_tran_code is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:624^9*/
        
        log.debug("Bank Number   : " + FileBankNumber);
        log.debug("Activity Date : " + DateTimeFormatter.getFormattedDate(FileActivityDate,"MM/dd/yyyy"));
        log.debug("Work Filename : " + workFilename);
        
        if ( rowCount > 0 )
        {
          if ( achOriginList == null )
          {
            achOriginList = new AchOriginList();
          }
          if ( achEntries == null )
          {
            achEntries = new Vector();
          }
          else
          {
            achEntries.removeAllElements();
          }            
          
          /*@lineinfo:generated-code*//*@lineinfo:645^11*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_file_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_file_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:649^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:651^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                    as merchant_number,
//                      mf.dba_name                           as dba_name,
//                      o.org_group                           as deposit_merchant_number,
//                      o.org_name                            as deposit_dba_name,
//                      0                                     as roll_up_deposit,
//                      lpad(nvl(atn.deposit_transit_routing,
//                               mf.transit_routng_num), 9, '0')   
//                                                            as transit_routing,
//                      nvl(atn.deposit_dda,mf.dda_num)       as dda_num,
//                      (trunc(sysdate) + nvl(mf.suspended_days,0))
//                                                            as post_date,
//                      'CCD'                                 as class_code,
//                      :AchEntryData.ED_ADJUSTMENTS        as entry_desc,
//                      :achSequence                          as ach_sequence,
//                      1                                     as batch_number,
//                      ( decode(dt.debit_credit_indicator,'D',1,0) )
//                                                            as sales_count,
//                      ( decode(dt.debit_credit_indicator,'D',1,0) *
//                        dt.transaction_amount )             as sales_amount,
//                      ( decode(dt.debit_credit_indicator,'C',1,0) )
//                                                            as credits_count,
//                      ( decode(dt.debit_credit_indicator,'C',1,0) *
//                        dt.transaction_amount )             as credits_amount,
//                      ( dt.transaction_amount * 
//                        decode(dt.debit_credit_indicator,'C',-1,1) ) 
//                                                            as net_amount
//              from    trident_debit_financial       dt,
//                      trident_debit_activity        tda,
//                      trident_debit_action_codes    ac,
//                      mif                           mf,
//                      ach_trident_nodes             atn,
//                      organization                  o
//              where   dt.bank_number = :FileBankNumber and
//                      dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                      tda.rec_id = dt.rec_id and
//                      tda.load_filename = :workFilename and
//                      ac.short_action_code = tda.action_code and
//                      not ac.adj_tran_code is null and
//                      mf.merchant_number = dt.merchant_number and
//                      nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X' and
//                      atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,
//                                                          mf.trans_dest_global),
//                                                      'M', mf.association_node,
//                                                      0 ) and 
//                      o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)
//              order by  dt.merchant_number, dt.card_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                    as merchant_number,\n                    mf.dba_name                           as dba_name,\n                    o.org_group                           as deposit_merchant_number,\n                    o.org_name                            as deposit_dba_name,\n                    0                                     as roll_up_deposit,\n                    lpad(nvl(atn.deposit_transit_routing,\n                             mf.transit_routng_num), 9, '0')   \n                                                          as transit_routing,\n                    nvl(atn.deposit_dda,mf.dda_num)       as dda_num,\n                    (trunc(sysdate) + nvl(mf.suspended_days,0))\n                                                          as post_date,\n                    'CCD'                                 as class_code,\n                     :1         as entry_desc,\n                     :2                           as ach_sequence,\n                    1                                     as batch_number,\n                    ( decode(dt.debit_credit_indicator,'D',1,0) )\n                                                          as sales_count,\n                    ( decode(dt.debit_credit_indicator,'D',1,0) *\n                      dt.transaction_amount )             as sales_amount,\n                    ( decode(dt.debit_credit_indicator,'C',1,0) )\n                                                          as credits_count,\n                    ( decode(dt.debit_credit_indicator,'C',1,0) *\n                      dt.transaction_amount )             as credits_amount,\n                    ( dt.transaction_amount * \n                      decode(dt.debit_credit_indicator,'C',-1,1) ) \n                                                          as net_amount\n            from    trident_debit_financial       dt,\n                    trident_debit_activity        tda,\n                    trident_debit_action_codes    ac,\n                    mif                           mf,\n                    ach_trident_nodes             atn,\n                    organization                  o\n            where   dt.bank_number =  :3  and\n                    dt.settlement_date between ( :4 -180) and  :5  and\n                    tda.rec_id = dt.rec_id and\n                    tda.load_filename =  :6  and\n                    ac.short_action_code = tda.action_code and\n                    not ac.adj_tran_code is null and\n                    mf.merchant_number = dt.merchant_number and\n                    nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X' and\n                    atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,\n                                                        mf.trans_dest_global),\n                                                    'M', mf.association_node,\n                                                    0 ) and \n                    o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)\n            order by  dt.merchant_number, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_ADJUSTMENTS);
   __sJT_st.setLong(2,achSequence);
   __sJT_st.setInt(3,FileBankNumber);
   __sJT_st.setDate(4,FileActivityDate);
   __sJT_st.setDate(5,FileActivityDate);
   __sJT_st.setString(6,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.startup.TridentDebitAdjustmentUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:699^11*/
          resultSet = it.getResultSet();
      
          while(resultSet.next())
          {
            originNode = achOriginList.getMerchantOriginNode(resultSet.getLong("merchant_number"),AchEntryData.ED_ADJUSTMENTS);
          
            if ( originNode != 0L )
            {
              /*@lineinfo:generated-code*//*@lineinfo:708^15*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_trace_sequence.nextval 
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_trace_sequence.nextval  \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   traceNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:712^15*/
            
              achData = new AchEntryData();
              achData.setData(resultSet);
              achData.setOriginNode(originNode);  
              achData.setTraceNumber(traceNumber);
              achData.addStatementData(resultSet);
              achEntries.add(achData);    // add to the list to be stored
            }              
          }
          resultSet.close();
          it.close();
          
          for( int achEntryIdx = 0; achEntryIdx < achEntries.size(); ++achEntryIdx )
          {
            achData = (AchEntryData)achEntries.elementAt(achEntryIdx);
            //@log.debug( String.valueOf(achData) );//@
            achData.store();
          }
          
          if ( TestFilename != null )
          {
            // exit the loop through active banks.
            break;
          }
        }
        else
        {
          log.debug("No records to process");
        }
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("processMESBanks()",e.toString());
    }
    return( retVal );
  }

  protected boolean processTsysAccounts()
  {
    BufferedWriter      out             = null;
    boolean             retVal          = false;
    int                 rowCount        = 0;
    String              workFilename    = null;
  
    try
    {  
      for( int i = 0; i < ActiveBanks.length; ++i )
      {
        FileBankNumber = ActiveBanks[i];
    
        // reset the file counters
        FileCreditsAmount     = 0.0;
        FileDebitsAmount      = 0.0;
        FilePaymentsAmount    = 0.0;
        FileRecordCount       = 0;
        FileCreditsCount      = 0;
        FileDebitsCount       = 0;
        
        // reset the batch totals
        BatchCreditsAmount     = 0.0;
        BatchDebitsAmount      = 0.0;
        BatchPaymentsAmount    = 0.0;
        BatchRecordCount       = 0;
        BatchCreditsCount      = 0;
        BatchDebitsCount       = 0;
    
        if ( TestFilename == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:783^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:787^11*/
          workFilename = buildFilename( ("tdbt_adj" + String.valueOf(FileBankNumber)) );
  
          /*@lineinfo:generated-code*//*@lineinfo:790^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_activity    tda
//              set     tda.load_filename = :workFilename
//              where   tda.rec_id in
//                      (
//                        select  dt.rec_id
//                        from    trident_debit_financial   dt,
//                                mif                       mf
//                        where   dt.bank_number = :FileBankNumber 
//                                and dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate
//                                and mf.merchant_number = dt.merchant_number
//                                and nvl(mf.processor_id,0) = 0  -- tsys back end only
//                      ) 
//                      and tda.load_filename is null 
//                      and exists
//                      ( 
//                        select  ac.short_action_code
//                        from    trident_debit_action_codes    ac
//                        where   ac.short_action_code = tda.action_code and
//                                not ac.adj_tran_code is null
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_debit_activity    tda\n            set     tda.load_filename =  :1 \n            where   tda.rec_id in\n                    (\n                      select  dt.rec_id\n                      from    trident_debit_financial   dt,\n                              mif                       mf\n                      where   dt.bank_number =  :2  \n                              and dt.settlement_date between ( :3 -180) and  :4 \n                              and mf.merchant_number = dt.merchant_number\n                              and nvl(mf.processor_id,0) = 0  -- tsys back end only\n                    ) \n                    and tda.load_filename is null \n                    and exists\n                    ( \n                      select  ac.short_action_code\n                      from    trident_debit_action_codes    ac\n                      where   ac.short_action_code = tda.action_code and\n                              not ac.adj_tran_code is null\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setDate(4,FileActivityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:812^11*/
        }          
        else  // allow for test files to be specified
        {
          workFilename    = TestFilename;
          FileBankNumber  = getFileBankNumber(TestFilename);
    
          /*@lineinfo:generated-code*//*@lineinfo:819^11*/

//  ************************************************************
//  #sql [Ctx] { select  max(tda.action_date) 
//              from    trident_debit_activity      tda
//              where   tda.load_filename = :workFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(tda.action_date)  \n            from    trident_debit_activity      tda\n            where   tda.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^11*/
        }
    
        /*@lineinfo:generated-code*//*@lineinfo:827^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(dt.merchant_number) 
//            from    trident_debit_financial       dt,
//                    trident_debit_activity        tda,
//                    trident_debit_action_codes    ac
//            where   dt.bank_number = :FileBankNumber and
//                    dt.settlement_date between (:FileActivityDate-180) and :FileActivityDate and
//                    tda.rec_id = dt.rec_id and
//                    tda.load_filename = :workFilename and
//                    ac.short_action_code = tda.action_code and
//                    not ac.adj_tran_code is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(dt.merchant_number)  \n          from    trident_debit_financial       dt,\n                  trident_debit_activity        tda,\n                  trident_debit_action_codes    ac\n          where   dt.bank_number =  :1  and\n                  dt.settlement_date between ( :2 -180) and  :3  and\n                  tda.rec_id = dt.rec_id and\n                  tda.load_filename =  :4  and\n                  ac.short_action_code = tda.action_code and\n                  not ac.adj_tran_code is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.TridentDebitAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:839^9*/
        
        log.debug("Bank Number   : " + FileBankNumber);
        log.debug("Activity Date : " + DateTimeFormatter.getFormattedDate(FileActivityDate,"MM/dd/yyyy"));
        log.debug("Work Filename : " + workFilename);
        
        if ( rowCount > 0 )
        {
          // open the data file
          out = new BufferedWriter( new FileWriter( workFilename, false ) );
        
          // build the transmission header for a worked debit adjustment file
          buildTransmissionHeader(out, ("TDBT" + FileBankNumber), "TRIDB" );
          buildAdjustmentHeader(workFilename,out);
          buildAdjustmentDetails(workFilename,out);
          buildAdjustmentTrailer(out);
          buildTransmissionTrailer(out);
    
          out.close();          // close the file buffer
    
          if ( TestFilename == null ) // only send non-test files
          {
            PropertiesFile propsFile = new PropertiesFile( PropertiesFilename );
          
            // ftp the file to the outgoing directory
            sendFile(workFilename,
                     propsFile.getString("outgoing.host",MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_HOST)),
                     MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_USER),
                     MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_PASSWORD));
          }
          else
          {
            log.debug("\n\n");
            log.debug("Test Filename '" + TestFilename + "' complete (no transmit)");
          }
    
          if ( TestFilename != null )
          {
            // exit the loop through active banks.
            break;
          }
        }
        else
        {
          log.debug("No records to process");
        }
      }        
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("processTsysAccounts()",e.toString());
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_OUTGOING_CB_HOST,
                      MesDefaults.DK_OUTGOING_CB_USER,
                      MesDefaults.DK_OUTGOING_CB_PASSWORD,
                      MesDefaults.DK_RISK_SCAN_NO_ACTIVITY_DAYS
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    TridentDebitAdjustmentUpload        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new TridentDebitAdjustmentUpload();
      
      if ( args.length > 0 && args[0].equals("processMESBanks") )
      {
        test.connect();
        test.setTestFilename( (args.length > 1) ? args[1] : null );
        test.processMESBanks();
        test.cleanUp();
      }
      else if ( args.length > 0 && args[0].equals("processMbsAccounts") )
      {
        test.connect();
        test.setTestFilename( (args.length > 1) ? args[1] : null );
        test.processMbsAccounts();
        test.cleanUp();
      }
      else
      {
        test.setTestFilename( (args.length == 0) ? null : args[0]);
        test.execute();
      }        
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/