/*@lineinfo:filename=TPGNonUSDBatchCutoff*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TPGNonUSDBatchCutoff.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.ThreadPool;
import com.tfxusa.secure.FXCloseBatch;
import com.tfxusa.secure.FXCloseBatchResponse;
import com.tfxusa.secure.TFXUSAServiceLocator;
import com.tfxusa.secure.TFXUSAServiceSoapStub;
import sqlj.runtime.ResultSetIterator;

public class TPGNonUSDBatchCutoff extends EventBase
{
  private static class IntlBatchCloseWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    Date      BatchDate           = null;
    String    BatchNumber         = null;
    long      LoadFileId          = 0L;
    String    LoadFilename        = null;
    String    TerminalId          = null;
    
    public IntlBatchCloseWorker( String tid, java.sql.Date batchDate, String batchNumber, String loadFilename, long loadFileId )
    {
      TerminalId    = tid;
      BatchDate     = batchDate;
      BatchNumber   = batchNumber;
      LoadFilename  = loadFilename;
      LoadFileId    = loadFileId;
    }
    
    public void run()
    {
      long      batchId     = 0L;
      
      try
      {
        connect(true);
        setAutoCommit(false);
        
        /*@lineinfo:generated-code*//*@lineinfo:71^9*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_test_sequence.nextval as batch_id
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_test_sequence.nextval as batch_id\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:76^9*/
        
        // mark the non-USD batches as closed by assigning a batch date and id
        /*@lineinfo:generated-code*//*@lineinfo:79^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api   tapi
//            set     tapi.batch_date     = :BatchDate,
//                    tapi.batch_id       = :batchId,
//                    tapi.load_filename  = :LoadFilename,
//                    tapi.load_file_id   = :LoadFileId
//            where   tapi.terminal_id = :TerminalId
//                    and tapi.transaction_date between trunc(sysdate-7) and trunc(sysdate)
//                    and tapi.mesdb_timestamp >= (sysdate-30)
//                    and tapi.batch_number = :BatchNumber
//                    and tapi.batch_date is null
//                    and tapi.transaction_type in 
//                        (
//                          select  tt.tran_type
//                          from    trident_capture_api_tran_type tt
//                          where   nvl(tt.settle,'N') = 'Y'
//                        ) 
//                    and nvl(tapi.test_flag,'Y') = 'N' 
//                    and tapi.load_filename like 'processed%'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api   tapi\n          set     tapi.batch_date     =  :1 ,\n                  tapi.batch_id       =  :2 ,\n                  tapi.load_filename  =  :3 ,\n                  tapi.load_file_id   =  :4 \n          where   tapi.terminal_id =  :5 \n                  and tapi.transaction_date between trunc(sysdate-7) and trunc(sysdate)\n                  and tapi.mesdb_timestamp >= (sysdate-30)\n                  and tapi.batch_number =  :6 \n                  and tapi.batch_date is null\n                  and tapi.transaction_type in \n                      (\n                        select  tt.tran_type\n                        from    trident_capture_api_tran_type tt\n                        where   nvl(tt.settle,'N') = 'Y'\n                      ) \n                  and nvl(tapi.test_flag,'Y') = 'N' \n                  and tapi.load_filename like 'processed%'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,BatchDate);
   __sJT_st.setLong(2,batchId);
   __sJT_st.setString(3,LoadFilename);
   __sJT_st.setLong(4,LoadFileId);
   __sJT_st.setString(5,TerminalId);
   __sJT_st.setString(6,BatchNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:101^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^9*/
      }
      catch( Exception e )
      {
        try{ /*@lineinfo:generated-code*//*@lineinfo:108^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^36*/ }catch( Exception ee ) {}
        logEntry("run(" + TerminalId + "," + BatchNumber + ")",e.toString());
        try{ /*@lineinfo:generated-code*//*@lineinfo:110^14*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^34*/ }catch( Exception ee ) {}
      }
      finally
      {
        setAutoCommit(true);
        cleanUp();
      }
    }
  }

  static Logger log = Logger.getLogger(TPGNonUSDBatchCutoff.class);

  private Date          TestBatchDate     = null;
  private String        TestBatchNumber   = null;
  private String        TestTerminalId    = null;
  
  public TPGNonUSDBatchCutoff( )
  {
    PropertiesFilename = "non-usd-batch-cutoff.properties";
  }
  
  protected void closeNonUSDBatches( )
  {
    Vector              batchesToClose    = new Vector();
    Date                batchDate         = null;
    long                batchId           = 0L;
    String              batchNumber       = null;
    ResultSetIterator   it                = null;
    long                loadFileId        = 0L;
    String              loadFilename      = null;
    ThreadPool          pool              = null;
    ResultSet           resultSet         = null;
    
    try
    {
      if ( inTestMode() )
      {
        batchDate     = TestBatchDate;
        batchNumber   = TestBatchNumber;
      }
      else  // query the current batch number and batch date
      {
        /*@lineinfo:generated-code*//*@lineinfo:152^9*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate)      as batch_date,
//                    value               as batch_number
//            
//            from    mes_defaults  
//            where   key = 152
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)      as batch_date,\n                  value               as batch_number\n           \n          from    mes_defaults  \n          where   key = 152";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchDate = (java.sql.Date)__sJT_rs.getDate(1);
   batchNumber = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^9*/
        
        // update to the new value
        /*@lineinfo:generated-code*//*@lineinfo:162^9*/

//  ************************************************************
//  #sql [Ctx] { update  mes_defaults
//            set     value = to_char((sysdate+1),'yyyymmdd')
//            where   key = 152
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mes_defaults\n          set     value = to_char((sysdate+1),'yyyymmdd')\n          where   key = 152";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^9*/
      
        // commit changes
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^9*/
      }
      
      // generate a load filename to indicate the transactions are complete
      loadFilename  = generateFilename( "intl3941", batchDate );
      loadFileId    = loadFilenameToLoadFileId( loadFilename );
      
      System.out.println("batchDate     : " + batchDate);
      System.out.println("loadFilename  : " + loadFilename);
      System.out.println("batchNumber   : " + batchNumber);
      
      /*@lineinfo:generated-code*//*@lineinfo:184^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_tapi_batch_number) */
//                  tapi.terminal_id        as tid,
//                  count(1)                as rc
//          from    trident_capture_api   tapi                
//          where   tapi.batch_number = :batchNumber
//                  and tapi.batch_date is null
//                  and tapi.transaction_date >= (sysdate-28) 
//                  and ( :TestTerminalId is null or tapi.terminal_id = :TestTerminalId )
//                  and tapi.transaction_type in 
//                      (
//                        select  tt.tran_type
//                        from    trident_capture_api_tran_type tt
//                        where   nvl(tt.settle,'N') = 'Y'
//                      ) 
//                  and nvl(tapi.test_flag,'Y') = 'N' 
//                  and 
//                  (
//                    not tapi.currency_code in ( 'USD','840' )
//                    or (tapi.currency_code in ( 'USD','840' )
//                        and tapi.load_filename like 'processed%')
//                  )
//          group by tapi.terminal_id
//          order by rc desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (tapi idx_tapi_batch_number) */\n                tapi.terminal_id        as tid,\n                count(1)                as rc\n        from    trident_capture_api   tapi                \n        where   tapi.batch_number =  :1 \n                and tapi.batch_date is null\n                and tapi.transaction_date >= (sysdate-28) \n                and (  :2  is null or tapi.terminal_id =  :3  )\n                and tapi.transaction_type in \n                    (\n                      select  tt.tran_type\n                      from    trident_capture_api_tran_type tt\n                      where   nvl(tt.settle,'N') = 'Y'\n                    ) \n                and nvl(tapi.test_flag,'Y') = 'N' \n                and \n                (\n                  not tapi.currency_code in ( 'USD','840' )\n                  or (tapi.currency_code in ( 'USD','840' )\n                      and tapi.load_filename like 'processed%')\n                )\n        group by tapi.terminal_id\n        order by rc desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,batchNumber);
   __sJT_st.setString(2,TestTerminalId);
   __sJT_st.setString(3,TestTerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.TPGNonUSDBatchCutoff",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:209^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        batchesToClose.addElement(resultSet.getString("tid"));
      }
      resultSet.close();
      it.close();
      
      pool = new ThreadPool(5);
      
      for( int i = 0; i < batchesToClose.size(); ++i )
      {
        String tid = (String)batchesToClose.elementAt(i);

        Thread worker = pool.getThread( new IntlBatchCloseWorker(tid,batchDate,batchNumber,loadFilename,loadFileId) );
        worker.start();
      }
      pool.waitForAllThreads();
        
      
      // select the FX batches to close
      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tpa.fx_client_id          as fx_client_id,
//                  sum( tapi.fx_amount_base *
//                       decode(tapi.debit_credit_indicator,'C',-1,1) )
//                                            as fx_batch_amount
//          from    trident_capture_api tapi,
//                  trident_profile_api tpa
//          where   tapi.batch_number = :batchNumber
//                  and ( :TestTerminalId is null or tapi.terminal_id = :TestTerminalId )
//                  and tapi.transaction_date >= (sysdate-28) 
//                  and tapi.transaction_type in 
//                      (
//                        select  tt.tran_type
//                        from    trident_capture_api_tran_type tt
//                        where   nvl(tt.settle,'N') = 'Y'
//                      ) 
//                  and nvl(tapi.test_flag,'Y') = 'N' 
//                  and not tapi.currency_code in ( 'USD','840' )
//                  and tpa.terminal_id = tapi.terminal_id
//                  and nvl(tpa.fx_client_id,0) != 0
//          group by tpa.fx_client_id      
//          order by tpa.fx_client_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tpa.fx_client_id          as fx_client_id,\n                sum( tapi.fx_amount_base *\n                     decode(tapi.debit_credit_indicator,'C',-1,1) )\n                                          as fx_batch_amount\n        from    trident_capture_api tapi,\n                trident_profile_api tpa\n        where   tapi.batch_number =  :1 \n                and (  :2  is null or tapi.terminal_id =  :3  )\n                and tapi.transaction_date >= (sysdate-28) \n                and tapi.transaction_type in \n                    (\n                      select  tt.tran_type\n                      from    trident_capture_api_tran_type tt\n                      where   nvl(tt.settle,'N') = 'Y'\n                    ) \n                and nvl(tapi.test_flag,'Y') = 'N' \n                and not tapi.currency_code in ( 'USD','840' )\n                and tpa.terminal_id = tapi.terminal_id\n                and nvl(tpa.fx_client_id,0) != 0\n        group by tpa.fx_client_id      \n        order by tpa.fx_client_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TPGNonUSDBatchCutoff",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,batchNumber);
   __sJT_st.setString(2,TestTerminalId);
   __sJT_st.setString(3,TestTerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.TPGNonUSDBatchCutoff",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/
      resultSet = it.getResultSet();
      
      batchesToClose.removeAllElements(); // clear the tid data from the vector
      
      while( resultSet.next() )
      {
        batchesToClose.add( new FXCloseBatch( MesDefaults.getString(MesDefaults.DK_TOCCATA_USER_ID),
                                              MesDefaults.getString(MesDefaults.DK_TOCCATA_ACCESS_KEY),
                                              resultSet.getInt("fx_client_id"),
                                              batchNumber,
                                              new BigDecimal(resultSet.getDouble("fx_batch_amount")) )
                          );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < batchesToClose.size(); ++i )
      {
        FXCloseBatch batchCloseMsg = (FXCloseBatch)batchesToClose.elementAt(i);
        
        TFXUSAServiceLocator   sl      =  new TFXUSAServiceLocator(); 
        TFXUSAServiceSoapStub  service = (TFXUSAServiceSoapStub)sl.getTFXUSAServiceSoap(); 
      
        FXCloseBatchResponse response= service.TFX_TransBatchClose_v1_10( batchCloseMsg );
        
        if ( response.getErrorCode() != 0 )
        {
          StringBuffer errorMessage = new StringBuffer("FX batch close failed.  ");
          errorMessage.append("FX Client ID: ");
          errorMessage.append(batchCloseMsg.getClientID());
          errorMessage.append("  Batch #: ");
          errorMessage.append(batchCloseMsg.getBatchID());
          errorMessage.append("  Error Msg: ");
          errorMessage.append(response.getMessage());
          logEntry( "closeNonUSDBatches()", errorMessage.toString() );
        }
      }
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:296^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^34*/ }catch( Exception ee ) {}
      logEntry("closeNonUSDBatches()",e.toString());
      log.error(e);
    }
  }
  
  public boolean execute()
  {
    boolean     retVal      = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      closeNonUSDBatches();
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      setAutoCommit(true);
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean inTestMode()
  {
    return( TestBatchDate != null ||
            TestBatchNumber != null || 
            TestTerminalId != null );
  }
  
  protected void setTestBatchDate( String batchDateString )
  {
    TestBatchDate = new java.sql.Date( DateTimeFormatter.parseDate(batchDateString,"MM/dd/yyyy").getTime() );
  }
  
  protected void setTestBatchNumber( String batchNumber )
  {
    TestBatchNumber = batchNumber;
  }
  
  protected void setTestTerminalId( String tid )
  {
    TestTerminalId = tid;
  }
  
  public static void main( String[] args )
  {
      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_TOCCATA_USER_ID,
                      MesDefaults.DK_TOCCATA_ACCESS_KEY
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    TPGNonUSDBatchCutoff      bc  = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      bc = new TPGNonUSDBatchCutoff();
      bc.connect();
    
      if ( args.length > 0 && args[0].equals("closeBatch") )
      {
        bc.setTestBatchDate( args[1] );
        bc.setTestBatchNumber( args[2] );
        bc.closeNonUSDBatches();
      }
      else
      {
        bc.setTestTerminalId((args.length > 0 ? args[0] : null));
        bc.execute();
      }        
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bc.cleanUp(); } catch( Exception ee ){}
    }
  }
}/*@lineinfo:generated-code*/