/*@lineinfo:filename=ArdefMonitoringEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ArdefMonitoringEvent.sqlj $

  Description:

  Last Modified By   : $Author: swaxman $
  Last Modified Date : $Date: 2014-08-08 $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.mail.Message.RecipientType;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.PropertiesFile;
import com.mes.support.SMTPMail;
import sqlj.runtime.ResultSetIterator;

public class ArdefMonitoringEvent extends EventBase
{
	private static String DEFAULT_EMAIL_HOST;
	static Logger log = Logger.getLogger(ArdefMonitoringEvent.class);

	public boolean TestMode = false;

	private Vector queryResults = new Vector();
	private SftpFilesCheck sftpobject = null;
	private String senderEmailAddress = "DevOpsTeam@merchante-solutions.com";
	private String[] devOpsNotices =  new String[]{};
	private String[] devOpsAlerts =  new String[]{};
	private int[] mcArdefRange = { 50000, 70000 };
	private int[] visaArdefRange = { 80000, 90000 };
	private int[] mcEpRange = { 2600000, 2800000 };

	public boolean allPass;
	public boolean mcArdefResult;
	public boolean mcEpResult;
	public boolean visaArdefResult;

	public ArdefMonitoringEvent() {
	  try {
	    String devOpsNoticesList = MesDefaults.getString(MesDefaults.ARDEF_MONITORING_DEVOPS_NOTICES_EMAIL_LIST);
	    devOpsNotices = devOpsNoticesList.split(",");

	    String devOpsAlertsList = MesDefaults.getString(MesDefaults.ARDEF_MONITORING_DEVOPS_ALERTS_EMAIL_LIST);
	    devOpsAlerts = devOpsAlertsList.split(",");
	  } catch (Exception e) {
	    logEntry("ArdefMonitoringEvent() constructor - set DevOps notices' emails and alerts' emails", e.toString());
	  }
	}
	
	public void getMCArdefResult(ResultSetIterator it, ResultSet rs) throws Exception{
		try {
			mcArdefResult = false;
			String[] cur = new String[2];
			cur[0] = "The MC ardef loads are scheduled to run at 9:00 AM on the EPS server. " +
					"-- This one generally throws out a pop up alert about an IO error. You need to " + 
					"click OK and it resumes okay. This alert can happen twice in one run. -- " +
					"After the load ends at about 10:00 / 10:15 AM run the following SQL to ensure that " +
					"load was okay. -- Check the load_filename , it should be for today and the " +
					"count has traditionally been 52531 so far -- mc_ardef3941_051614_0916.dat    52531 " +
					"-- mc_ardef3941_052714_0919.dat    52535";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:85^4*/

//  ************************************************************
//  #sql [Ctx] it = { select load_filename, count(*) as count
//  					from mc_ardef_table
//  					group by load_filename
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select load_filename, count(*) as count\n\t\t\t\t\tfrom mc_ardef_table\n\t\t\t\t\tgroup by load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.ArdefMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^5*/

			rs = it.getResultSet();
			boolean pass = false;
			if(!rs.next()) {
				cur[1] = "No results returned.";
			} else {
				do
				{
					pass = rs.getString("load_filename").substring(13, 19).equals(new SimpleDateFormat("MMddyy").format(Calendar.getInstance().getTime())) && rs.getInt("count") > mcArdefRange[0] && rs.getInt("count") < mcArdefRange[1];
					mcArdefResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("load_filename") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:109^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:mcArdefResult ? "Y" : "N",
//  								8
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3775 = cur[1];
 String __sJT_3776 = mcArdefResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t8\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3775);
   __sJT_st.setString(2,__sJT_3776);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^6*/
		} catch (Exception e) {
			logEntry("getMCArdefResult()", e.toString());
		}
	}

	public void getMCEPResult(ResultSetIterator it, ResultSet rs) throws Exception{
		try {
			mcEpResult = false;
			String[] cur = new String[2];
			cur[0] = "This has been in the range of about ~2635662";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:139^4*/

//  ************************************************************
//  #sql [Ctx] it = { select count(*) as count from mc_ep_table_data
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select count(*) as count from mc_ep_table_data";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.ArdefMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^5*/

			rs = it.getResultSet();
			boolean pass = false;
			if(!rs.next()) {
				cur[1] = "No results returned.";
			} else {
				do
				{
					pass = rs.getInt("count") > mcEpRange[0] && rs.getInt("count") < mcEpRange[1];
					mcEpResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:161^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:mcEpResult ? "Y" : "N",
//  								9
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3777 = cur[1];
 String __sJT_3778 = mcEpResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t9\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3777);
   __sJT_st.setString(2,__sJT_3778);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^6*/
		} catch (Exception e) {
			logEntry("getMCEPResult()", e.toString());
		}
	}

	public void getVisaArdefResult(ResultSetIterator it, ResultSet rs) throws Exception{
		try {
			visaArdefResult = false;
			String[] cur = new String[2];
			cur[0] = "The VISA ardef loads are scheduled to run at 9:45 AM on the EPS server. " + 
					"-- After the load ends at about 10:00 AM run the following SQL to ensure that load was okay. " +
					"-- Check the load_filename , it should be for today and the count has been in the range of " +
					"~80700 so far. -- If receive visa ardef/settlement failure email run 'Visa EP Updates' " + 
					"scheduled task on EPS";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:195^4*/

//  ************************************************************
//  #sql [Ctx] it = { select load_filename, count(*) as count
//  					from visa_ardef_table
//  					group by load_filename
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select load_filename, count(*) as count\n\t\t\t\t\tfrom visa_ardef_table\n\t\t\t\t\tgroup by load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.ArdefMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^5*/

			rs = it.getResultSet();
			boolean pass = false;
			if(!rs.next()) {
				cur[1] = "No results returned.";
			} else {
				do
				{
					pass = rs.getString("load_filename").substring(13, 19).equals(new SimpleDateFormat("MMddyy").format(Calendar.getInstance().getTime())) && rs.getInt("count") > visaArdefRange[0] && rs.getInt("count") < visaArdefRange[1];
					visaArdefResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("load_filename") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:219^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:visaArdefResult ? "Y" : "N",
//  								10
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3779 = cur[1];
 String __sJT_3780 = visaArdefResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t10\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.ArdefMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3779);
   __sJT_st.setString(2,__sJT_3780);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^6*/
		} catch (Exception e) {
			logEntry("getVisaArdefResult()", e.toString());
		}
	}

	/*
	 ** METHOD public void getData()
	 **
	 */
	public synchronized void getData()
	{
		ResultSetIterator it        = null;
		ResultSet         rs        = null;
		boolean           restrict  = false;
		sftpobject = SftpFilesCheck.sftpObject();

		try
		{
			loadProps();
			queryResults.clear();
			allPass = false;
			connect();
			/*@lineinfo:generated-code*//*@lineinfo:260^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type in ('8','9','10')
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type in ('8','9','10')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.ArdefMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^4*/

			getMCArdefResult(it, rs);
			getMCEPResult(it, rs);
			getVisaArdefResult(it, rs);
			allPass();
			sendAlertEmail(!allPass, "");

		}
		catch(Exception e)
		{
			logEntry("getData()", e.toString());
		}
		finally
		{
			try { rs.close(); } catch(Exception e) {}
			try { it.close(); } catch(Exception e) {}
			sftpobject.cleanUp();
			cleanUp();
		}
	}

	/**
	 * METHOD allPass
	 * 
	 * Sets the allPass variable
	 */
	public void allPass() {
		allPass = mcArdefResult && mcEpResult && visaArdefResult;
	}

	public void sendAlertEmail(boolean alert, String emailContext) {
		String  body             = "";

		try {
			// create a mailer object with the host parameters in MesDefaults
			String emailHost = MesDefaults.getString(MesDefaults.DK_SMTP_HOST);
			SMTPMail mailer = new SMTPMail(emailHost);
			mailer.setFrom(senderEmailAddress);

			if (alert){ 
				int count = devOpsAlerts.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsAlerts[i]);
					mailer.setSubject("AM Monitor Alert");
				}
			}
			else {
				int count = devOpsNotices.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsNotices[i]);
					mailer.setSubject("AM Monitor Notification");                           
				}
			}

			if (!alert) {
				body += "\n";
				body += "------------------------------------------------------------" + "\n";
				body += "Visa and MC Ardef loads successful.\n";
				body += "------------------------------------------------------------" + "\n";
				mailer.setText(body);
				mailer.send();
			}
			else {
				body += "\n";
				body += "One or more errors were found with ardef loads.\n";
				body += emailContext;
				body += "\n";
				mailer.setText(body);
				mailer.send();
			}
		} catch(Exception e) {
			logEntry("sendEmail()", e.toString());
		}
	}
	
  private void loadProps()
  {
    log.debug("in loadProps... ");
    try
    {
      PropertiesFile props = new PropertiesFile("monitoring.properties");
      
      DEFAULT_EMAIL_HOST = props.getString("DEFAULT_EMAIL_HOST");
      senderEmailAddress = props.getString("SENDER_EMAIL_ADDRESS", senderEmailAddress);
      devOpsNotices = props.getString("DEV_OPS_NOTICES").split(";");
      devOpsAlerts = props.getString("DEV_OPS_ALERTS").split(";");
      mcArdefRange[0] = Integer.parseInt((props.getString("MC_ARDEF_RANGE").split(","))[0]);
      mcArdefRange[1] = Integer.parseInt((props.getString("MC_ARDEF_RANGE").split(","))[1]);
      mcEpRange[0] = Integer.parseInt((props.getString("MC_EP_RANGE").split(","))[0]);
      mcEpRange[1] = Integer.parseInt((props.getString("MC_EP_RANGE").split(","))[1]);
      visaArdefRange[0] = Integer.parseInt((props.getString("VISA_ARDEF_RANGE").split(","))[0]);
      visaArdefRange[1] = Integer.parseInt((props.getString("VISA_ARDEF_RANGE").split(","))[1]);
    }
    catch(Exception e)
    {
      logEntry("loadProps()", e.toString());
      log.error(e.getMessage());
    }
  }
  
	public boolean execute( )
	{
		boolean result = true;
		getData();
		return( result );
	}

	public static void main(String[] args)
	{
		try
		{
    		if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(new String[]{
						MesDefaults.ARDEF_MONITORING_DEVOPS_NOTICES_EMAIL_LIST, MesDefaults.ARDEF_MONITORING_DEVOPS_ALERTS_EMAIL_LIST,
						MesDefaults.DK_SMTP_HOST
				});
			}

			//				com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
			ArdefMonitoringEvent worker = new ArdefMonitoringEvent();

			worker.execute();
		}
		catch(Exception e)
		{
			System.out.println("main(): " + e.toString());
			log.error(e.getMessage());
		}
	}
}/*@lineinfo:generated-code*/