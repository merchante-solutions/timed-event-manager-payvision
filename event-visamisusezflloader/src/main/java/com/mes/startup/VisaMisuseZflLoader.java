/*@lineinfo:filename=VisaMisuseZflLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaMisuseZflLoader.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

public class VisaMisuseZflLoader extends EventBase
{
  private boolean       DeleteFirst       = false;
  
  public VisaMisuseZflLoader()
  {
  }
  
  @Deprecated
  public void addVmaPricing()
  {
    ResultSetIterator         it        = null;
    ResultSet                 resultSet = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number
//          from    mif       mf
//          where   mf.bank_number = 3941
//                  and not mf.merchant_number in
//                  (
//                    select  gm.merchant_number
//                    from    visa_fee_config   vfc,
//                            organization      o,
//                            group_merchant    gm
//                    where   vfc.vma = 'N'
//                            and o.org_group = vfc.hierarchy_node
//                            and gm.org_num = o.org_num                  
//                  )
//                  and not exists
//                  (
//                    select  mp.merchant_number
//                    from    mbs_pricing   mp
//                    where   mp.merchant_number = mf.merchant_number
//                            and mp.item_type = 11
//                            and mp.valid_date_end > sysdate
//                  )
//                  and exists
//                  (
//                    select  mp.merchant_number
//                    from    mbs_pricing   mp
//                    where   mp.merchant_number = mf.merchant_number                  
//                  )      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number\n        from    mif       mf\n        where   mf.bank_number = 3941\n                and not mf.merchant_number in\n                (\n                  select  gm.merchant_number\n                  from    visa_fee_config   vfc,\n                          organization      o,\n                          group_merchant    gm\n                  where   vfc.vma = 'N'\n                          and o.org_group = vfc.hierarchy_node\n                          and gm.org_num = o.org_num                  \n                )\n                and not exists\n                (\n                  select  mp.merchant_number\n                  from    mbs_pricing   mp\n                  where   mp.merchant_number = mf.merchant_number\n                          and mp.item_type = 11\n                          and mp.valid_date_end > sysdate\n                )\n                and exists\n                (\n                  select  mp.merchant_number\n                  from    mbs_pricing   mp\n                  where   mp.merchant_number = mf.merchant_number                  \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.VisaMisuseZflLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        long merchantId = resultSet.getLong("merchant_number");
        System.out.println("Adding pricing for " + merchantId);//@
        /*@lineinfo:generated-code*//*@lineinfo:95^9*/

//  ************************************************************
//  #sql [Ctx] { call mbs_add_pricing( :merchantId, 11, null, 0, 0.045, 117445, '0710', '129999', null )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN mbs_add_pricing(  :1  , 11, null, 0, 0.045, 117445, '0710', '129999', null )\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^9*/
      }        
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      System.out.println("addVmaPricing() - " + e.toString());
    }
    finally
    {
      cleanUp();
    }      
  }
  
  public boolean execute()
  {
    Vector    dataFilenames = new Vector();
    String    dataFilename  = null;
    String    flagFilename  = null;
    boolean   retVal        = false;
    Sftp      sftp          = null;
    
    try
    {
      DeleteFirst = true;
      
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_HOST),
                      MesDefaults.getString(MesDefaults.DK_VISA_USER),
                      MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_VISA_INCOMING_PATH),
                      false     // !binary
                    );
      
      String filePrefix = getEventArg(0);
      log.debug("Attempting to retrieve " + filePrefix + " files for loading");
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(filePrefix + ".*\\.flg"));
      
      while( it.hasNext() )
      {
        flagFilename = (String)it.next();
        dataFilename = flagFilename.substring(0,flagFilename.lastIndexOf(".")) + ".dat";
        
        log.debug("retrieving " + dataFilename);
        sftp.download(dataFilename, dataFilename);
        sftp.deleteFile(flagFilename);
        sftp.deleteFile(dataFilename);
        
        dataFilenames.addElement(dataFilename);
      }
      
      for( int i = 0; i < dataFilenames.size(); ++i )
      {
        dataFilename = (String)dataFilenames.elementAt(i);
        
        if ( "ti_fees".equals(filePrefix) )
        {
          loadTifDetailData(dataFilename);
        }
        else    // default is misuse and zero floor
        {
          if ( dataFilename.indexOf("detail") >= 0 )
          {
            log.debug("loading detail data");
            loadDetailData(dataFilename);
          }
          else
          {
            log.debug("loading summary data");
            loadSummaryData(dataFilename);
          }
        }
        (new File(dataFilename)).delete();
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
      
    }
    return( retVal );
  }
  
  protected String generateLoadFilename( String inputFilename ) 
  {
    String[][]    filePrefixes  =
    {
      {"misuse_report"      , "vs_mua_sm"},
      {"misuse_detail"      , "vs_mua_dt"},
      {"zero_floor_report"  , "vs_zfl_sm"},
      {"zero_floor_detail"  , "vs_zfl_dt"},
      {"ti_fees"            , "vs_tif_dt"},
    };
    
    // strip the path from the input filename
    String loadFilename = inputFilename.replace('\\','/');
    if ( loadFilename.indexOf("/") >= 0 )
    {
      loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
    }
    
    for( int i = 0; i < filePrefixes.length; ++i )
    {
      if ( loadFilename.startsWith( filePrefixes[i][0] ) )
      {
        loadFilename = (filePrefixes[i][1] + 9999 + loadFilename.substring( filePrefixes[i][0].length() ));
        break;
      }
    }
    return( loadFilename );      
  }
  
  public void loadDetailData( String inputFilename )
  {
    FlatFileRecord      ffd             = null;
    BufferedReader      in              = null;
    int                 insertCount     = 0;
    String              line            = null;
    long                loadFileId      = 0L;
    String              loadFilename    = null;
    long                merchantId      = 0L;
    
    try
    {
      connect(true); // timeout exempt
      setAutoCommit(false);
      
      in  = new BufferedReader( new FileReader( inputFilename ) );
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_AUTH_MISUSE_DETAIL);
      
      loadFilename = generateLoadFilename(inputFilename);
      System.out.println("Processing Visa Misuse of Auth File - " + loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:238^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:243^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^7*/
      
      if ( DeleteFirst )
      {
        /*@lineinfo:generated-code*//*@lineinfo:252^9*/

//  ************************************************************
//  #sql [Ctx] { delete  /*+ index (vma idx_visa_auth_misuse_dt_lfid) */
//            from    visa_auth_misuse_detail   vma
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  /*+ index (vma idx_visa_auth_misuse_dt_lfid) */\n          from    visa_auth_misuse_detail   vma\n          where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:257^9*/
      }
      
      while( (line = in.readLine()) != null )
      {
        ffd.resetAllFields();
        ffd.suck(StringUtilities.leftJustify(line,215,' '));
        merchantId = resolveMid(ffd.getFieldData("card_acceptor_id"));
        
        System.out.print("Processing " + ++insertCount + "            \r");//@
        /*@lineinfo:generated-code*//*@lineinfo:267^9*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_auth_misuse_detail
//            (
//              merchant_number,
//              merchant_name,
//              active_date,
//              ofd_group_id,
//              report_identifier,
//              recipient_identifier,
//              business_id,
//              acquirer_bin,
//              acquirer_name,
//              processor_id,
//              processor_name,
//              card_acceptor_id,
//              mvv,
//              transaction_identifier,
//              transaction_date,
//              transaction_amount,
//              pos_entry_mode,
//              pos_terminal_id,
//              card_number,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :merchantId,
//              ltrim(rtrim(:ffd.getFieldData("merchant_name"))),
//              :ffd.getFieldAsSqlDate("active_date"),
//              ltrim(rtrim(:ffd.getFieldData("ofd_group_id"))),
//              ltrim(rtrim(:ffd.getFieldData("report_identifier"))),
//              ltrim(rtrim(:ffd.getFieldData("recipient_identifier"))),
//              ltrim(rtrim(:ffd.getFieldData("business_id"))),
//              ltrim(rtrim(:ffd.getFieldData("acquirer_bin"))),
//              ltrim(rtrim(:ffd.getFieldData("acquirer_name"))),
//              ltrim(rtrim(:ffd.getFieldData("processor_id"))),
//              ltrim(rtrim(:ffd.getFieldData("processor_name"))),
//              ltrim(rtrim(:ffd.getFieldData("card_acceptor_id"))),
//              ltrim(rtrim(:ffd.getFieldData("mvv"))),
//              ltrim(rtrim(:ffd.getFieldData("transaction_identifier"))),
//              :ffd.getFieldAsSqlDate("transaction_date"),
//              :ffd.getFieldAsDouble("transaction_amount"),
//              ltrim(rtrim(:ffd.getFieldData("pos_entry_mode"))),
//              ltrim(rtrim(:ffd.getFieldData("pos_terminal_id"))),
//              ltrim(rtrim(:ffd.getFieldData("card_number"))),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_208 = ffd.getFieldData("merchant_name");
 java.sql.Date __sJT_209 = ffd.getFieldAsSqlDate("active_date");
 String __sJT_210 = ffd.getFieldData("ofd_group_id");
 String __sJT_211 = ffd.getFieldData("report_identifier");
 String __sJT_212 = ffd.getFieldData("recipient_identifier");
 String __sJT_213 = ffd.getFieldData("business_id");
 String __sJT_214 = ffd.getFieldData("acquirer_bin");
 String __sJT_215 = ffd.getFieldData("acquirer_name");
 String __sJT_216 = ffd.getFieldData("processor_id");
 String __sJT_217 = ffd.getFieldData("processor_name");
 String __sJT_218 = ffd.getFieldData("card_acceptor_id");
 String __sJT_219 = ffd.getFieldData("mvv");
 String __sJT_220 = ffd.getFieldData("transaction_identifier");
 java.sql.Date __sJT_221 = ffd.getFieldAsSqlDate("transaction_date");
 double __sJT_222 = ffd.getFieldAsDouble("transaction_amount");
 String __sJT_223 = ffd.getFieldData("pos_entry_mode");
 String __sJT_224 = ffd.getFieldData("pos_terminal_id");
 String __sJT_225 = ffd.getFieldData("card_number");
   String theSqlTS = "insert into visa_auth_misuse_detail\n          (\n            merchant_number,\n            merchant_name,\n            active_date,\n            ofd_group_id,\n            report_identifier,\n            recipient_identifier,\n            business_id,\n            acquirer_bin,\n            acquirer_name,\n            processor_id,\n            processor_name,\n            card_acceptor_id,\n            mvv,\n            transaction_identifier,\n            transaction_date,\n            transaction_amount,\n            pos_entry_mode,\n            pos_terminal_id,\n            card_number,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n            ltrim(rtrim( :2  )),\n             :3  ,\n            ltrim(rtrim( :4  )),\n            ltrim(rtrim( :5  )),\n            ltrim(rtrim( :6  )),\n            ltrim(rtrim( :7  )),\n            ltrim(rtrim( :8  )),\n            ltrim(rtrim( :9  )),\n            ltrim(rtrim( :10  )),\n            ltrim(rtrim( :11  )),\n            ltrim(rtrim( :12  )),\n            ltrim(rtrim( :13  )),\n            ltrim(rtrim( :14  )),\n             :15  ,\n             :16  ,\n            ltrim(rtrim( :17  )),\n            ltrim(rtrim( :18  )),\n            ltrim(rtrim( :19  )),\n             :20  ,\n             :21  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_208);
   __sJT_st.setDate(3,__sJT_209);
   __sJT_st.setString(4,__sJT_210);
   __sJT_st.setString(5,__sJT_211);
   __sJT_st.setString(6,__sJT_212);
   __sJT_st.setString(7,__sJT_213);
   __sJT_st.setString(8,__sJT_214);
   __sJT_st.setString(9,__sJT_215);
   __sJT_st.setString(10,__sJT_216);
   __sJT_st.setString(11,__sJT_217);
   __sJT_st.setString(12,__sJT_218);
   __sJT_st.setString(13,__sJT_219);
   __sJT_st.setString(14,__sJT_220);
   __sJT_st.setDate(15,__sJT_221);
   __sJT_st.setDouble(16,__sJT_222);
   __sJT_st.setString(17,__sJT_223);
   __sJT_st.setString(18,__sJT_224);
   __sJT_st.setString(19,__sJT_225);
   __sJT_st.setString(20,loadFilename);
   __sJT_st.setLong(21,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:317^9*/
        
        if ( insertCount%200 == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:319^39*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:319^59*/  }
      }
    }
    catch( Exception e )
    {
      logEntry("loadDetailData(" + loadFilename + ")", e.toString());
    }
    finally
    {
      setAutoCommit(true);
      try{ in.close(); }catch( Exception ee ){}
      cleanUp();
    }
  }
  
  public void loadSummaryData( String inputFilename )
  {
    FlatFileRecord      ffd             = null;
    BufferedReader      in              = null;
    int                 insertCount     = 0;
    String              line            = null;
    long                loadFileId      = 0L;
    String              loadFilename    = null;
    long                merchantId      = 0L;
    
    try
    {
      connect(true);
      setAutoCommit(false);
      
      in  = new BufferedReader( new FileReader( inputFilename ) );
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_AUTH_MISUSE_SUMMARY);
      
      loadFilename = generateLoadFilename(inputFilename);
      System.out.println("Processing Visa Misuse of Auth File - " + loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:355^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:360^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:365^7*/
      
      if ( DeleteFirst )
      {
        /*@lineinfo:generated-code*//*@lineinfo:369^9*/

//  ************************************************************
//  #sql [Ctx] { delete  /*+ index (sm idx_visa_auth_misuse_sm_lfid) */
//            from    visa_auth_misuse_summary  sm
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  /*+ index (sm idx_visa_auth_misuse_sm_lfid) */\n          from    visa_auth_misuse_summary  sm\n          where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^9*/
      }
      
      while( (line = in.readLine()) != null )
      {
        ffd.resetAllFields();
        ffd.suck(StringUtilities.leftJustify(line,215,' '));
        merchantId = resolveMid(ffd.getFieldData("card_acceptor_id"));
        
        System.out.print("Processing " + ++insertCount + "            \r");//@
        /*@lineinfo:generated-code*//*@lineinfo:384^9*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_auth_misuse_summary
//            (
//              merchant_number,
//              merchant_name,
//              active_date,
//              ofd_group_id,
//              report_identifier,
//              recipient_identifier,
//              business_id,
//              acquirer_bin,
//              acquirer_name,
//              processor_id,
//              processor_name,
//              card_acceptor_id,
//              mvv,
//              mcc,
//              item_count,
//              item_amount,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :merchantId,
//              ltrim(rtrim(:ffd.getFieldData("merchant_name"))),
//              :ffd.getFieldAsSqlDate("active_date"),
//              ltrim(rtrim(:ffd.getFieldData("ofd_group_id"))),
//              ltrim(rtrim(:ffd.getFieldData("report_identifier"))),
//              ltrim(rtrim(:ffd.getFieldData("recipient_identifier"))),
//              ltrim(rtrim(:ffd.getFieldData("business_id"))),
//              ltrim(rtrim(:ffd.getFieldData("acquirer_bin"))),
//              ltrim(rtrim(:ffd.getFieldData("acquirer_name"))),
//              ltrim(rtrim(:ffd.getFieldData("processor_id"))),
//              ltrim(rtrim(:ffd.getFieldData("processor_name"))),
//              ltrim(rtrim(:ffd.getFieldData("card_acceptor_id"))),
//              ltrim(rtrim(:ffd.getFieldData("mvv"))),
//              ltrim(rtrim(:ffd.getFieldData("mcc"))),
//              ltrim(rtrim(:ffd.getFieldData("item_count"))),
//              :ffd.getFieldAsDouble("item_amount"),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_226 = ffd.getFieldData("merchant_name");
 java.sql.Date __sJT_227 = ffd.getFieldAsSqlDate("active_date");
 String __sJT_228 = ffd.getFieldData("ofd_group_id");
 String __sJT_229 = ffd.getFieldData("report_identifier");
 String __sJT_230 = ffd.getFieldData("recipient_identifier");
 String __sJT_231 = ffd.getFieldData("business_id");
 String __sJT_232 = ffd.getFieldData("acquirer_bin");
 String __sJT_233 = ffd.getFieldData("acquirer_name");
 String __sJT_234 = ffd.getFieldData("processor_id");
 String __sJT_235 = ffd.getFieldData("processor_name");
 String __sJT_236 = ffd.getFieldData("card_acceptor_id");
 String __sJT_237 = ffd.getFieldData("mvv");
 String __sJT_238 = ffd.getFieldData("mcc");
 String __sJT_239 = ffd.getFieldData("item_count");
 double __sJT_240 = ffd.getFieldAsDouble("item_amount");
   String theSqlTS = "insert into visa_auth_misuse_summary\n          (\n            merchant_number,\n            merchant_name,\n            active_date,\n            ofd_group_id,\n            report_identifier,\n            recipient_identifier,\n            business_id,\n            acquirer_bin,\n            acquirer_name,\n            processor_id,\n            processor_name,\n            card_acceptor_id,\n            mvv,\n            mcc,\n            item_count,\n            item_amount,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n            ltrim(rtrim( :2  )),\n             :3  ,\n            ltrim(rtrim( :4  )),\n            ltrim(rtrim( :5  )),\n            ltrim(rtrim( :6  )),\n            ltrim(rtrim( :7  )),\n            ltrim(rtrim( :8  )),\n            ltrim(rtrim( :9  )),\n            ltrim(rtrim( :10  )),\n            ltrim(rtrim( :11  )),\n            ltrim(rtrim( :12  )),\n            ltrim(rtrim( :13  )),\n            ltrim(rtrim( :14  )),\n            ltrim(rtrim( :15  )),\n             :16  ,\n             :17  ,\n             :18  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_226);
   __sJT_st.setDate(3,__sJT_227);
   __sJT_st.setString(4,__sJT_228);
   __sJT_st.setString(5,__sJT_229);
   __sJT_st.setString(6,__sJT_230);
   __sJT_st.setString(7,__sJT_231);
   __sJT_st.setString(8,__sJT_232);
   __sJT_st.setString(9,__sJT_233);
   __sJT_st.setString(10,__sJT_234);
   __sJT_st.setString(11,__sJT_235);
   __sJT_st.setString(12,__sJT_236);
   __sJT_st.setString(13,__sJT_237);
   __sJT_st.setString(14,__sJT_238);
   __sJT_st.setString(15,__sJT_239);
   __sJT_st.setDouble(16,__sJT_240);
   __sJT_st.setString(17,loadFilename);
   __sJT_st.setLong(18,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^9*/
        
        if ( insertCount%200 == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:430^39*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:430^59*/  }
      }
    }
    catch( Exception e )
    {
      logEntry("loadSummaryData(" + loadFilename + ")", e.toString());
    }
    finally
    {
      setAutoCommit(true);
      try{ in.close(); }catch( Exception ee ){}
      cleanUp();
    }
  }
  
  public void loadTifDetailData( String inputFilename )
  {
    FlatFileRecord      ffd             = null;
    BufferedReader      in              = null;
    int                 insertCount     = 0;
    String              line            = null;
    long                loadFileId      = 0L;
    String              loadFilename    = null;
    long                merchantId      = 0L;
    
    try
    {
      connect(true); // timeout exempt
      setAutoCommit(false);
      
      in  = new BufferedReader( new FileReader( inputFilename ) );
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TIF_DETAIL);
      
      loadFilename = generateLoadFilename(inputFilename);
      System.out.println("Processing Visa TIF File - " + loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:466^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:471^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:476^7*/
      
      if ( DeleteFirst )
      {
        /*@lineinfo:generated-code*//*@lineinfo:480^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    visa_tif_detail           tif
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    visa_tif_detail           tif\n          where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^9*/
      }
              
      
      while( (line = in.readLine()) != null )
      {
        if ( "99".equals(line.substring(23,25)) )
        {
          continue;   // skip trailer record
        }
        
        ffd.resetAllFields();
        ffd.suck(StringUtilities.leftJustify(line,200,' '));
        merchantId = resolveMid(ffd.getFieldData("card_acceptor_id"));
        
        System.out.print("Processing " + ++insertCount + "            \r");//@
        /*@lineinfo:generated-code*//*@lineinfo:501^9*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_tif_detail
//            (
//              merchant_number,
//              merchant_name,
//              settlement_date,
//              transaction_date,
//              ofd_group_id,
//              report_identifier,
//              recipient_identifier,
//              tif_amount,
//              tif_amount_sign,
//              tif_reclass_reason,
//              mvv,
//              transaction_identifier,
//              transaction_amount,
//              submitted_fpi,
//              assessed_fpi,
//              card_number,
//              reference_number,
//              load_filename,
//              load_file_id,
//              acct_funding_source
//            )
//            values
//            (
//              :merchantId,
//              ltrim(rtrim(:ffd.getFieldData("merchant_name"))),
//              :ffd.getFieldAsSqlDate("settlement_date"),
//              :ffd.getFieldAsSqlDate("transaction_date"),
//              ltrim(rtrim(:ffd.getFieldData("ofd_group_id"))),
//              ltrim(rtrim(:ffd.getFieldData("report_identifier"))),
//              ltrim(rtrim(:ffd.getFieldData("recipient_identifier"))),
//              :ffd.getFieldAsDouble("tif_amount"),
//              ltrim(rtrim(:ffd.getFieldData("tif_amount_sign"))),
//              ltrim(rtrim(:ffd.getFieldData("tif_reclass_reason"))),
//              ltrim(rtrim(:ffd.getFieldData("mvv"))),
//              ltrim(rtrim(:ffd.getFieldData("transaction_identifier"))),
//              :ffd.getFieldAsDouble("transaction_amount"),
//              ltrim(rtrim(:ffd.getFieldData("submitted_fpi"))),
//              ltrim(rtrim(:ffd.getFieldData("assessed_fpi"))),
//              ltrim(rtrim(:ffd.getFieldData("card_number"))),
//              ltrim(rtrim(:ffd.getFieldData("reference_number"))),
//              :loadFilename,
//              :loadFileId,
//              ltrim(rtrim(:ffd.getFieldData("account_funding_source")))
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_241 = ffd.getFieldData("merchant_name");
 java.sql.Date __sJT_242 = ffd.getFieldAsSqlDate("settlement_date");
 java.sql.Date __sJT_243 = ffd.getFieldAsSqlDate("transaction_date");
 String __sJT_244 = ffd.getFieldData("ofd_group_id");
 String __sJT_245 = ffd.getFieldData("report_identifier");
 String __sJT_246 = ffd.getFieldData("recipient_identifier");
 double __sJT_247 = ffd.getFieldAsDouble("tif_amount");
 String __sJT_248 = ffd.getFieldData("tif_amount_sign");
 String __sJT_249 = ffd.getFieldData("tif_reclass_reason");
 String __sJT_250 = ffd.getFieldData("mvv");
 String __sJT_251 = ffd.getFieldData("transaction_identifier");
 double __sJT_252 = ffd.getFieldAsDouble("transaction_amount");
 String __sJT_253 = ffd.getFieldData("submitted_fpi");
 String __sJT_254 = ffd.getFieldData("assessed_fpi");
 String __sJT_255 = ffd.getFieldData("card_number");
 String __sJT_256 = ffd.getFieldData("reference_number");
 String __sJT_257 = ffd.getFieldData("account_funding_source");
   String theSqlTS = "insert into visa_tif_detail\n          (\n            merchant_number,\n            merchant_name,\n            settlement_date,\n            transaction_date,\n            ofd_group_id,\n            report_identifier,\n            recipient_identifier,\n            tif_amount,\n            tif_amount_sign,\n            tif_reclass_reason,\n            mvv,\n            transaction_identifier,\n            transaction_amount,\n            submitted_fpi,\n            assessed_fpi,\n            card_number,\n            reference_number,\n            load_filename,\n            load_file_id,\n            acct_funding_source\n          )\n          values\n          (\n             :1  ,\n            ltrim(rtrim( :2  )),\n             :3  ,\n             :4  ,\n            ltrim(rtrim( :5  )),\n            ltrim(rtrim( :6  )),\n            ltrim(rtrim( :7  )),\n             :8  ,\n            ltrim(rtrim( :9  )),\n            ltrim(rtrim( :10  )),\n            ltrim(rtrim( :11  )),\n            ltrim(rtrim( :12  )),\n             :13  ,\n            ltrim(rtrim( :14  )),\n            ltrim(rtrim( :15  )),\n            ltrim(rtrim( :16  )),\n            ltrim(rtrim( :17  )),\n             :18  ,\n             :19  ,\n            ltrim(rtrim( :20  ))\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_241);
   __sJT_st.setDate(3,__sJT_242);
   __sJT_st.setDate(4,__sJT_243);
   __sJT_st.setString(5,__sJT_244);
   __sJT_st.setString(6,__sJT_245);
   __sJT_st.setString(7,__sJT_246);
   __sJT_st.setDouble(8,__sJT_247);
   __sJT_st.setString(9,__sJT_248);
   __sJT_st.setString(10,__sJT_249);
   __sJT_st.setString(11,__sJT_250);
   __sJT_st.setString(12,__sJT_251);
   __sJT_st.setDouble(13,__sJT_252);
   __sJT_st.setString(14,__sJT_253);
   __sJT_st.setString(15,__sJT_254);
   __sJT_st.setString(16,__sJT_255);
   __sJT_st.setString(17,__sJT_256);
   __sJT_st.setString(18,loadFilename);
   __sJT_st.setLong(19,loadFileId);
   __sJT_st.setString(20,__sJT_257);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:549^9*/
        
        if ( insertCount%200 == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:551^39*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:551^59*/  }
      }
      
      // queue the billing summarization
      /*@lineinfo:generated-code*//*@lineinfo:555^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//          (
//            process_type,process_sequence,load_filename
//          )
//          values
//          (
//            6,0,:loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n        (\n          process_type,process_sequence,load_filename\n        )\n        values\n        (\n          6,0, :1  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:565^7*/
    }
    catch( Exception e )
    {
      logEntry("loadTifDetailData(" + loadFilename + ")", e.toString());
    }
    finally
    {
      setAutoCommit(true);
      try{ in.close(); }catch( Exception ee ){}
      cleanUp();
    }
  }
  
  public long resolveMid( String rawData )
  {
    long      merchantId    = 0L;
    String    temp          = (rawData == null ? null : rawData.trim());
  
    try
    {
      if ( isNumber(temp) )
      {
        while( merchantId == 0L && temp.length() > 0 )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:592^13*/

//  ************************************************************
//  #sql [Ctx] { select  mf.merchant_number
//                
//                from    mif     mf
//                where   mf.merchant_number = to_number(:temp)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.merchant_number\n               \n              from    mif     mf\n              where   mf.merchant_number = to_number( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.VisaMisuseZflLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,temp);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^13*/
          }
          catch( Exception ee )
          {
            temp = temp.substring(0,temp.length()-1); 
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("resolveMid(" + rawData + ")",e.toString());
    }
    finally
    {
    }
    return( merchantId );
  }

  public boolean isNumber( String lval )
  {
    boolean       retVal      = false;
    
    try
    {
      long temp = Long.parseLong(lval);
      retVal = true;
    }
    catch( Exception e )
    {
      // ignore
    }
    return( retVal );
  }

  public static void main( String[] args )
  {
      try
      {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_VISA_HOST,
                      MesDefaults.DK_VISA_USER,
                      MesDefaults.DK_VISA_PASSWORD,
                      MesDefaults.DK_VISA_INCOMING_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }


    VisaMisuseZflLoader   testBean    = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      testBean = new VisaMisuseZflLoader();
      testBean.connect(true);
      
           if (  "loadSummaryData".equals(args[0]) ) { testBean.loadSummaryData(args[1]);                         }
      else if (    "addVmaPricing".equals(args[0]) ) { testBean.addVmaPricing();                                  }
      else if (   "loadDetailData".equals(args[0]) ) { testBean.loadDetailData(args[1]);                          }
      else if ("loadTifDetailData".equals(args[0]) ) { testBean.loadTifDetailData(args[1]);                       }
      else if (          "execute".equals(args[0]) ) { testBean.setEventArgs(args[1]);   testBean.execute();      }
      else                                           { System.out.println("Invalid command - " + args[0]);        }
    }
    finally
    {
      try{ testBean.cleanUp(); } catch(Exception e){}
    }
  }  
}/*@lineinfo:generated-code*/