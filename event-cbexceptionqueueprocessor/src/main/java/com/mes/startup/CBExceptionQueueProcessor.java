/*@lineinfo:filename=CBExceptionQueueProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesChargebacks;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import sqlj.runtime.ResultSetIterator;


public class CBExceptionQueueProcessor extends EventBase
{

  // create class log category
  static Logger log = Logger.getLogger(CBExceptionQueueProcessor.class);

  //list of properties needed for nca entry
  private int FILE_LOAD_SEC       = 1;
  private String ACTION_CODE      = "D";
  private String MESSAGE          = "Auto-Process: moved from Exception queue";
  private String USER_AUTO        = "system";


  /**
   * METHOD execute()
   *
   * generates/processes exception queue items
   * that have been in queue for longer than allotted time
   */
  public boolean execute()
  {
    ResultSetIterator rsItr = null;
    ResultSet rs = null;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:44^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select distinct(cb.cb_load_sec)
//          from  q_data qd,
//                network_chargebacks cb,
//                network_chargeback_auto_act ncaa,
//                mif m,
//                t_hierarchy th
//          where qd.type = :MesQueues.Q_CHARGEBACKS_EXCEPTION
//          and   qd.item_type = :MesQueues.Q_ITEM_TYPE_CHARGEBACK
//          and   qd.id = cb.cb_load_sec
//          --and   trunc(sysdate) - cb.incoming_date < ncaa.day_interval --test
//          and   trunc(sysdate) - cb.incoming_date > ncaa.day_interval
//          and   cb.merchant_number = m.merchant_number
//          and   ( ncaa.hierarchy_node = m.merchant_number
//                  or
//                  (ncaa.hierarchy_node = th.ancestor and th.descendent = m.association_node)
//                )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct(cb.cb_load_sec)\n        from  q_data qd,\n              network_chargebacks cb,\n              network_chargeback_auto_act ncaa,\n              mif m,\n              t_hierarchy th\n        where qd.type =  :1 \n        and   qd.item_type =  :2 \n        and   qd.id = cb.cb_load_sec\n        --and   trunc(sysdate) - cb.incoming_date < ncaa.day_interval --test\n        and   trunc(sysdate) - cb.incoming_date > ncaa.day_interval\n        and   cb.merchant_number = m.merchant_number\n        and   ( ncaa.hierarchy_node = m.merchant_number\n                or\n                (ncaa.hierarchy_node = th.ancestor and th.descendent = m.association_node)\n              )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CBExceptionQueueProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_CHARGEBACKS_EXCEPTION);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_CHARGEBACK);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.CBExceptionQueueProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^7*/

      rs = rsItr.getResultSet();

      //loop through list, creating network_chargeback_activity entry
      //and moving item from exception queue to MCHB queue
      long id;
      while(rs.next())
      {
        log.debug("this item will be moved to MCHB: "+ rs.getLong("cb_load_sec"));

        //get control number
        id =  rs.getLong("cb_load_sec");

        try
        {
          //insert the new activity
          /*@lineinfo:generated-code*//*@lineinfo:79^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//              (
//                cb_load_sec,
//                file_load_sec,
//                action_code,
//                action_date,
//                user_message,
//                user_login,
//                action_source
//              )
//              values
//              (
//                :id,
//                :FILE_LOAD_SEC,
//                :ACTION_CODE,
//                trunc(sysdate),
//                :MESSAGE,
//                :USER_AUTO,
//                :MesChargebacks.AS_AUTO_PROCESS
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_activity\n            (\n              cb_load_sec,\n              file_load_sec,\n              action_code,\n              action_date,\n              user_message,\n              user_login,\n              action_source\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n              trunc(sysdate),\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.CBExceptionQueueProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,FILE_LOAD_SEC);
   __sJT_st.setString(3,ACTION_CODE);
   __sJT_st.setString(4,MESSAGE);
   __sJT_st.setString(5,USER_AUTO);
   __sJT_st.setInt(6,MesChargebacks.AS_AUTO_PROCESS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:101^11*/

          //update the queue
          QueueTools tools = new QueueTools();
          tools.moveQueue(  id,
                            MesQueues.Q_CHARGEBACKS_EXCEPTION,
                            MesQueues.Q_CHARGEBACKS_MCHB,
                            null,
                            MESSAGE,
                            false);

        }
        catch (Exception e)
        {
          log.debug("EXCEPTION in rs: "+e.getMessage());
        }

      }//end while

    }
    catch (Exception e)
    {
      log.debug("EXCEPTION in item selection: "+e.getMessage());
    }

    return true;

  }
}/*@lineinfo:generated-code*/