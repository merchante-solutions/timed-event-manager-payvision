/*@lineinfo:filename=TPGPreAuthCleanup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TPGPreAuthCleanup.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class TPGPreAuthCleanup extends EventBase
{
  public static class ProfileData
  {
    public    Date      ExpDate       = null;
    public    String    TerminalId    = null;
    public    String    TestFlag      = null;
  
    public ProfileData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ExpDate       = resultSet.getDate("exp_date");
      TerminalId    = resultSet.getString("tid");
      TestFlag      = resultSet.getString("test_flag");
    }
  }
    
  public static class Worker 
    extends SQLJConnectionBase
    implements Runnable
  {
    private   ProfileData       Profile;
    
    public Worker( ProfileData profile )
    {
      Profile = profile;
    }    
    
    public void run()
    {
      ResultSetIterator   it              = null;
      Vector              entriesToVoid   = new Vector();
      ResultSet           resultSet       = null;
      
      try
      {
        connect(true);
        
       /*@lineinfo:generated-code*//*@lineinfo:72^8*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ FIRST_ROWS */     tapi.rowid      as row_id
//            from    trident_capture_api   tapi
//            where   tapi.terminal_id = :Profile.TerminalId
//                    and tapi.test_flag = :Profile.TestFlag
//                    and tapi.transaction_type = 'P'
//                    and 
//                    ((tapi.transaction_date between :Profile.ExpDate-7 and :Profile.ExpDate
//                    and tapi.card_type in ( 'VS','AM','DS','JC' )
//                    and tapi.mesdb_timestamp between :Profile.ExpDate-7 and :Profile.ExpDate+1
//                 	  ) or
//                 	  (tapi.transaction_date between trunc(sysdate-38) and trunc(sysdate-31)
//                    and tapi.card_type in ('MC')
//                    and tapi.mesdb_timestamp between trunc(sysdate-38) and trunc(sysdate-30)
//                 	  ))
//            order by tapi.card_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ FIRST_ROWS */     tapi.rowid      as row_id\n          from    trident_capture_api   tapi\n          where   tapi.terminal_id =  :1 \n                  and tapi.test_flag =  :2 \n                  and tapi.transaction_type = 'P'\n                  and \n                  ((tapi.transaction_date between  :3 -7 and  :4 \n                  and tapi.card_type in ( 'VS','AM','DS','JC' )\n                  and tapi.mesdb_timestamp between  :5 -7 and  :6 +1\n               \t  ) or\n               \t  (tapi.transaction_date between trunc(sysdate-38) and trunc(sysdate-31)\n                  and tapi.card_type in ('MC')\n                  and tapi.mesdb_timestamp between trunc(sysdate-38) and trunc(sysdate-30)\n               \t  ))\n          order by tapi.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TPGPreAuthCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,Profile.TerminalId);
   __sJT_st.setString(2,Profile.TestFlag);
   __sJT_st.setDate(3,Profile.ExpDate);
   __sJT_st.setDate(4,Profile.ExpDate);
   __sJT_st.setDate(5,Profile.ExpDate);
   __sJT_st.setDate(6,Profile.ExpDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TPGPreAuthCleanup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^9*/
        
        resultSet = it.getResultSet();
        
        // store the entries to void in a vector
        while( resultSet.next() )
        {
          entriesToVoid.add( resultSet.getString("row_id") );
        }
        resultSet.close();
        it.close();
        
        // void the entries from the vector
        for( int i = 0; i < entriesToVoid.size(); ++i )
        {
          String rowId = (String)entriesToVoid.elementAt(i);

          /*@lineinfo:generated-code*//*@lineinfo:106^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api
//              set     transaction_type      = 'V',
//                      transaction_amount    = 0,
//                      fx_post_status        = decode(nvl(fx_rate_id,0),0,null,'P'),
//                      fx_post_ts            = null,
//                      load_filename         = nvl(load_filename,'auto-void')
//              where   rowid = :rowId
//                      and transaction_type = 'P'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api\n            set     transaction_type      = 'V',\n                    transaction_amount    = 0,\n                    fx_post_status        = decode(nvl(fx_rate_id,0),0,null,'P'),\n                    fx_post_ts            = null,\n                    load_filename         = nvl(load_filename,'auto-void')\n            where   rowid =  :1 \n                    and transaction_type = 'P'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TPGPreAuthCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rowId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^11*/
        }
      }
      catch( Exception e )
      {
        logEntry("run(" + Profile.TerminalId + "," + Profile.ExpDate + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public boolean execute()
  {
    int                 daysBack        = 30;
    ResultSetIterator   it              = null;
    ThreadPool          pool            = null;
    Vector              profileList     = new Vector();
    boolean             result          = false;
    ResultSet           resultSet       = null;
    String              testFlag        = null;
    
    try
    {
      connect(true);
      
      pool = new ThreadPool(7);
      
      // first get how old is too old from MesDefaults
      int expireDays = MesDefaults.getInt(MesDefaults.DK_TPG_PREAUTH_EXPIRE_DAYS);
      int expireDaysTest = MesDefaults.getInt(MesDefaults.DK_TPG_PREAUTH_EXPIRE_DAYS_TEST);
      
      for( int pass = 0; pass < 2; ++pass )
      {
        switch( pass )
        {
          case 0:   // production
            testFlag = "N";
            daysBack = Math.max(expireDays,28);
            break;
            
          case 1:   // test
            testFlag = "Y";
            daysBack = Math.max(expireDaysTest,7);
            break;
            
          default:
            continue;
        }
        
        //
        // Days before expiration can be specified at the terminal level for
        // production void operations.  For test, the expiration is
        // always the system default.  The least clause prevents
        // voiding transactions before they reach an age of 7 days.
        // The additional -1 on all date calculations causes the transactions
        // to void when they reach an age of N+1 days.
        //
        // Defaults: 8th day for test, 29th day production
        //
        /*@lineinfo:generated-code*//*@lineinfo:178^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id      as tid,
//                    case
//                      when :testFlag = 'Y' then trunc(sysdate-:daysBack)-1
//                      else least( trunc(sysdate - nvl(tpa.auth_expiration_days,:daysBack))-1,
//                                  trunc(sysdate-7)-1 )
//                    end                 as exp_date,
//                    :testFlag           as test_flag
//            from    trident_profile       tp,
//                    trident_profile_api   tpa
//            where   nvl(tp.api_enabled,'N') = 'Y'
//                    and tpa.terminal_id(+) = tp.terminal_id
//            order by exp_date desc,tp.terminal_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id      as tid,\n                  case\n                    when  :1  = 'Y' then trunc(sysdate- :2 )-1\n                    else least( trunc(sysdate - nvl(tpa.auth_expiration_days, :3 ))-1,\n                                trunc(sysdate-7)-1 )\n                  end                 as exp_date,\n                   :4            as test_flag\n          from    trident_profile       tp,\n                  trident_profile_api   tpa\n          where   nvl(tp.api_enabled,'N') = 'Y'\n                  and tpa.terminal_id(+) = tp.terminal_id\n          order by exp_date desc,tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TPGPreAuthCleanup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,testFlag);
   __sJT_st.setInt(2,daysBack);
   __sJT_st.setInt(3,daysBack);
   __sJT_st.setString(4,testFlag);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.TPGPreAuthCleanup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^9*/
        resultSet = it.getResultSet();
        
        profileList.removeAllElements();
        while( resultSet.next() )
        {
          profileList.addElement( new ProfileData(resultSet) );
        }
        resultSet.close();
        it.close();
        
        for( int i = 0; i < profileList.size(); ++i )
        {
          ProfileData pd = (ProfileData)profileList.elementAt(i);
          Thread thread = pool.getThread( new Worker((ProfileData)profileList.elementAt(i)) );
          thread.start();
        }
        pool.waitForAllThreads();
      }   // end for loop
        
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( result );
  }
  
  public static void main(String[] args)
  {
      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_TPG_PREAUTH_EXPIRE_DAYS,
                      MesDefaults.DK_TPG_PREAUTH_EXPIRE_DAYS_TEST
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    TPGPreAuthCleanup         t     = null;

    try
    {
      t = new TPGPreAuthCleanup();
      t.connect();

      if ( "execute".equals(args[0]) )
      {
        t.execute();
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ t.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/