package com.mes.pach;

import java.util.HashMap;

public class TranStatus
{
  // ach transaction
  // sale statuses
  public static final TranStatus S_JECT_S = new TranStatus("S_JECT_S",  "Rejected");
  public static final TranStatus S_JECT_U = new TranStatus("S_JECT_U",  "Rejected");
  public static final TranStatus S_NEW    = new TranStatus("S_NEW",     "New");  
  public static final TranStatus S_PEND   = new TranStatus("S_PEND",    "Pended");
  public static final TranStatus S_POST   = new TranStatus("S_POST",    "Posted");
  public static final TranStatus S_RET_S  = new TranStatus("S_RET_S",   "Returned");
  public static final TranStatus S_RET_U  = new TranStatus("S_RET_U",   "Returned");
  public static final TranStatus S_SET    = new TranStatus("S_SET",     "Settled");
  public static final TranStatus S_VOID   = new TranStatus("S_VOID",    "Voided");

  // credit statuses
  public static final TranStatus C_JECT   = new TranStatus("C_JECT",    "Rejected");
  public static final TranStatus C_NEW    = new TranStatus("C_NEW",     "New");
  public static final TranStatus C_PEND   = new TranStatus("C_PEND",    "Pended");
  public static final TranStatus C_POST   = new TranStatus("C_POST",    "Posted");
  public static final TranStatus C_RET    = new TranStatus("C_RET",     "Returned");
  public static final TranStatus C_SET    = new TranStatus("C_SET",     "Settled");
  public static final TranStatus C_VOID   = new TranStatus("C_VOID",    "Voided");

  private static HashMap statusHash = new HashMap();

  static
  {
    statusHash.put(""+S_JECT_S, S_JECT_S);
    statusHash.put(""+S_JECT_U, S_JECT_U);
    statusHash.put(""+S_NEW,    S_NEW);
    statusHash.put(""+S_PEND,   S_PEND);
    statusHash.put(""+S_POST,   S_POST);
    statusHash.put(""+S_RET_S,  S_RET_S);
    statusHash.put(""+S_RET_U,  S_RET_U);
    statusHash.put(""+S_SET,    S_SET);
    statusHash.put(""+S_VOID,   S_VOID);

    statusHash.put(""+C_JECT,   C_JECT);
    statusHash.put(""+C_NEW,    C_NEW);
    statusHash.put(""+C_PEND,   C_PEND);
    statusHash.put(""+C_POST,   C_POST);
    statusHash.put(""+C_RET,    C_RET);
    statusHash.put(""+C_SET,    C_SET);
    statusHash.put(""+C_VOID,   C_VOID);
  };

  private String statusStr;
  private String description;

  private TranStatus(String statusStr, String description)
  {
    this.statusStr = statusStr;
    this.description = description;
  }

  public static TranStatus statusFor(String forStr, 
    TranStatus dflt)
  {
    TranStatus status = (TranStatus)statusHash.get(forStr);
    return status != null ? status : dflt;
  }
  public static TranStatus statusFor(String forStr)
  {
    TranStatus status = statusFor(forStr,null);
    if (forStr == null)
    {
      throw new NullPointerException("Invalid status string: '" 
        + forStr + "'");
    }
    return status;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof TranStatus))
    {
      return false;
    }
    TranStatus that = (TranStatus)o;
    return that.statusStr.equals(statusStr);
  }

  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return statusStr;
  }
}