package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchFileDetail extends AchBase
{
  static Logger log = Logger.getLogger(AchFileDetail.class);

  private long            detailId;
  private Date            createDate;
  private String          merchNum;
  private String          profileId;
  private long            ftId;
  private AchAccount      account = new AchAccount();
  private String          accountId;
  private String          accountName;
  private CreditDebitCode cdCode;
  private BigDecimal      amount;
  private Destination destination;
  private long            sourceId;
  private SecCode         secCode;
  private String          entryDescription;
  private String          testFlag;
  private String          holdFlag = FLAG_NO;
  private long            batchId;
  private long            fileId;
  private String          traceNum;
  private String          recurCode;

  public void setDetailId(long detailId)
  {
    this.detailId = detailId;
  }
  public long getDetailId()
  {
    return detailId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setAccount(AchAccount account)
  {
    this.account = account;
  }
  public AchAccount getAccount()
  {
    return account;
  }

  public void setAccountNum(String accountNum)
  {
    account.setAccountNum(accountNum);
  }
  public String getAccountNum()
  {
    return account.truncated();
  }

  public void setAccountType(String accountType)
  {
    account.setAccountType(accountType);
  }
  public String getAccountType()
  {
    return account.getAccountType();
  }

  public void setTransitNum(String transitNum)
  {
    account.setTransitNum(transitNum);
  }
  public String getTransitNum()
  {
    return account.getTransitNum();
  }

  public boolean isCheckingAccount()
  {
    return account.isCheckingAccount();
  }
  public boolean isSavingsAccount()
  {
    return account.isSavingsAccount();
  }

  public void setAccountId(String accountId)
  {
    this.accountId = accountId;
  }
  public String getAccountId()
  {
    return accountId;
  }

  public void setAccountName(String accountName)
  {
    this.accountName = accountName;
  }
  public String getAccountName()
  {
    return accountName;
  }

  // credit/debit indicator, indicates if this will 
  // credit(deposit in) or debit a destination account
  public void setCdCode(CreditDebitCode cdCode)
  {
    this.cdCode = cdCode;
  }
  public CreditDebitCode getCdCode()
  {
    return cdCode;
  }
  public boolean isCredit()
  {
    return CreditDebitCode.CREDIT.equals(cdCode);
  }
  public boolean isDeposit()
  {
    return isCredit();
  }
  public boolean isDebit()
  {
    return CreditDebitCode.DEBIT.equals(cdCode);
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setDestination(Destination destination)
  {
    this.destination = destination;
  }
  public Destination getDestination()
  {
    return destination;
  }

  public void setSourceId(long sourceId)
  {
    this.sourceId = sourceId;
  }
  public long getSourceId()
  {
    return sourceId;
  }

  public void setSecCode(SecCode secCode)
  {
    this.secCode = secCode;
  }
  public SecCode getSecCode()
  {
    return secCode;
  }

  public void setEntryDescription(String entryDescription)
  {
    this.entryDescription = entryDescription;
  }
  public String getEntryDescription()
  {
    return entryDescription;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setHoldFlag(String holdFlag)
  {
    validateFlag(holdFlag,"hold");
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return holdFlag;
  }
  public boolean isHeld()
  {
    return flagBoolean(holdFlag);
  }

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }

  public void setFileId(long fileId)
  {
    this.fileId = fileId;
  }
  public long getFileId()
  {
    return fileId;
  }

  public void setTraceNum(String traceNum)
  {
    this.traceNum = traceNum;
  }
  public String getTraceNum()
  {
    return traceNum;
  }

  public void setRecurCode(String recurCode)
  {
    this.recurCode = recurCode;
  }
  public String getRecurCode()
  {
    return recurCode;
  }

  public String toString()
  {
    return "AchFileDetail"
      + "\n  detailId:    " + detailId
      + "\n  createDate:  " + createDate
      + "\n  merchNum:    " + merchNum
      + "\n  profileId:   " + profileId
      + "\n  ftId:        " + ftId
      + "\n  transitNum:  " + account.getTransitNum()
      + "\n  accountNum:  " + account.truncated()
      + "\n  accountType: " + account.getAccountType()
      + "\n  accountId:   " + accountId
      + "\n  accountName: " + accountName
      + "\n  cdCode:      " + cdCode
      + "\n  amount:      " + amount
      + "\n  destination: " + destination
      + "\n  sourceId:    " + sourceId
      + "\n  secCode:     " + secCode
      + "\n  entryDescription: " + entryDescription
      + "\n  holdFlag:    " + isHeld()
      + "\n  testFlag:    " + isTest()
      + "\n  batchId:     " + (batchId > 0 ? ""+batchId : "--")
      + "\n  fileId:      " + (fileId > 0 ? ""+fileId : "--")
      + "\n  recurCode:   " + recurCode
      + "\n  traceNum:    " + (traceNum != null ? traceNum : "--");
  }
}