package com.mes.pach;

import java.util.HashMap;

public class FundingEntryType
{
  // settlement funding entry types
  public static final FundingEntryType SALE          
                                  = new FundingEntryType("SALE");
  public static final FundingEntryType SALE_RET_ADJ  
                                  = new FundingEntryType("SALE_RET_ADJ");
  public static final FundingEntryType CREDIT          
                                  = new FundingEntryType("CREDIT");
  public static final FundingEntryType CREDIT_RET_ADJ  
                                  = new FundingEntryType("CREDIT_RET_ADJ");

  private static HashMap typeHash = new HashMap();

  static
  {
    typeHash.put(""+SALE,           SALE);
    typeHash.put(""+SALE_RET_ADJ,   SALE_RET_ADJ);
    typeHash.put(""+CREDIT,         CREDIT);
    typeHash.put(""+CREDIT_RET_ADJ, CREDIT_RET_ADJ);
  };

  private String typeStr;

  private FundingEntryType(String typeStr)
  {
    this.typeStr = typeStr;
  }

  public static FundingEntryType typeFor(String forStr, FundingEntryType dflt)
  {
    FundingEntryType entryType = (FundingEntryType)typeHash.get(forStr);
    return entryType != null ? entryType : dflt;
  }
  public static FundingEntryType typeFor(String forStr)
  {
    FundingEntryType entryType = typeFor(forStr,null);
    if (entryType == null)
    {
      throw new NullPointerException("Invalid type string: '" + forStr + "'");
    }
    return entryType;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof FundingEntryType))
    {
      return false;
    }
    FundingEntryType that = (FundingEntryType)o;
    return that.typeStr.equals(typeStr);
  }

  public String toString()
  {
    return typeStr;
  }
}