package com.mes.pach;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.DropDownItem;
import sqlj.runtime.ResultSetIterator;

public class PachDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(PachDb.class);

  // names of config parameters stored in ACHP_CONFIG
  public static final String CFG_ERROR_EMAILS   = "error_emails";
  public static final String CFG_RISK_EMAILS    = "risk_emails";
  public static final String CFG_ORIGIN_ID      = "origin_id";
  public static final String CFG_CUTOFF_TIME    = "cutoff_time";
  public static final String CFG_ACH_FILE_DETAIL_LIMIT
                                                = "ach_file_detail_limit";
  public static final String CFG_DFLT_FT_ID     = "default_file_type";
  public static final String CFG_SSH_PARMS_PROD = "ssh_parms_prod";
  public static final String CFG_SSH_PARMS_TEST = "ssh_parms_test";
  public static final String CFG_EDT_DEPOSIT    = "entry_desc_deposit";
  public static final String CFG_EDT_DEBIT      = "entry_desc_debit";
  public static final String CFG_EDT_SALE       = "entry_desc_sale";
  public static final String CFG_EDT_CREDIT     = "entry_desc_credit";
  public static final String CFG_LAST_POST_UPDATE
                                                = "last_post_update";
  public static final String CFG_WEB_HOST_NAME  = "web_host_name";
  public static final String CFG_ORG_RPT_PATH   = "org_report_path";

  // entry description types
  public static final String EDT_DEPOSIT        = "EDT_DEPOSIT";
  public static final String EDT_DEBIT          = "EDT_DEBIT";
  public static final String EDT_SALE           = "EDT_SALE";
  public static final String EDT_CREDIT         = "EDT_CREDIT";

  // date/time formatting strings
  public static final String FMT_HMMSS_A        = "h:mm:ss a";
  public static final String FMT_HMMSSA         = "h:mm:ssa";
  public static final String FMT_HMM_A          = "h:mm a";
  public static final String FMT_HMMA           = "h:mma";
  public static final String FMT_H_A            = "h a";
  public static final String FMT_HA             = "ha";

  public static final String FMT_DATE1          = "MM/dd/yyyy";
  public static final String FMT_DATE2          = "M/d/yyyy";

  public static final String FMT_TS1            = "MM/dd/yyyy h:mm:ss a";
  public static final String FMT_TS2            = "M/d/yyyy h:mm:ss a";

  public static final String FMT_HTML           = "'<nobr>'EEE M/d/yy'</nobr> "
                                                + "<nobr>'h:mma'</nobr>'";

  public static final String FMT_ORA_DATE       = "''d-MMM-yyyy''";
  public static final String FMT_FILE_DATE      = "MMddyy";

  // possible time formats
  public static final String[] FMT_TIME_PATS    =
      { FMT_HMMSS_A, FMT_HMMSSA, FMT_HMM_A, FMT_HMMA, FMT_H_A, FMT_HA };

  // possible date formats 
  public static final String[] FMT_DATE_PATS    = { FMT_DATE1, FMT_DATE2 };

  // possible time stamp formats (date + time components)
  public static final String[] FMT_TS_PATS      = { FMT_TS1, FMT_TS2 };

  public static final int BATCH_SIZE_ALL = 0;

  public static final int DISALLOW_HOLDS = 0;
  public static final int ALLOW_HOLDS = 1;

  public static final int SEC_DISABLED_OK = 0;
  public static final int SEC_ENABLED_ONLY = 1;

  public static final int SYS_TEST = 0;
  public static final int SYS_PROD = 1;

  private boolean directFlag;

  private String nachFileName = null;
  public PachDb(boolean directFlag)
  {
    super(directFlag ? DbProperties.dbDirect() : DbProperties.dbPool());
    this.directFlag = directFlag;
  }
  public PachDb()
  {
    // default direct flag off
    this(false);
  }

  public boolean isDirect()
  {
    return directFlag;
  }

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(FMT_HTML);
  private static String formatDate(Date date, SimpleDateFormat sdf)
  {
    if (date != null)
    {
      return sdf.format(date);
    }
    return null;
  }
  public static String formatDate(Date date, String formatStr)
  {
    return formatDate(date,new SimpleDateFormat(formatStr));
  }
  public static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    String dateStr = formatDate(date,sdf);
    return dateStr == null ? "--" : dateStr;
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }

  /**
   * Error logging
   */

  public void logEntry(Object src, String method, Exception e)
  {
    logEntry(src.getClass().getName() + "." + method,""+e);
  }
  public void logEntry(String method, Exception e)
  {
    logEntry(this,method,e);
  }
    
  /**
   * Option checking
   */

  private boolean checkAllowHoldsOption(int opt)
  {
    if (opt == ALLOW_HOLDS)
    {
      return true;
    }
    if (opt == DISALLOW_HOLDS)
    {
      return false;
    }
    throw new IllegalArgumentException(
      "Invalid hold allowed field value: " + opt);
  }

  private boolean checkEnabledOnlyOption(int opt)
  {
    if (opt == SEC_DISABLED_OK)
    {
      return false;
    }
    if (opt == SEC_ENABLED_ONLY)
    {
      return true;
    }
    throw new IllegalArgumentException(
      "Invalid SEC enabled field value: " + opt);
  }

  /**
   * Returns true if test system selected.
   */
  private boolean checkSysTypeOption(int opt)
  {
    if (opt == SYS_TEST)
    {
      return true;
    }
    if (opt == SYS_PROD)
    {
      return false;
    }
    throw new IllegalArgumentException(
      "Invalid system type field value: " + opt);
  }

  /**
   * Fetches next value from aus id sequence.
   */
  public long getNewId() throws SQLException
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select achp_id_sequence.nextval from dual ");
      rs.next();
      long newId = rs.getLong(1);
      return newId;
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Convert string to BigDecimal amount if possible.
   */

  private BigDecimal stringToBigDecimal(String str)
  {
    BigDecimal bigDec = null;

    try
    {
      bigDec = new BigDecimal(str).setScale(2);
    }
    catch (Exception e)
    {
      log.warn("Invalid amount string: '" + str + "'");
    }

    return bigDec;
  }

  /**
   * Transaction support.
   *
   * These allow multi step transactions to occur.
   */

  private boolean inTranFlag;
  private boolean storedAcFlag;

  /**
   * Throws exception if the indicated transaction status is not the
   * current status.
   */

  private void checkTranStatus(boolean tranStatus)
  {
    if (inTranFlag != tranStatus)
    {
      String errMsg = inTranFlag ? 
        "Invalid operation, cannot perform while in transaction" :
        "Invalid operation, cannot perform outside of transaction";
      throw new RuntimeException(errMsg);
    }
  }

  /**
   * Override connect() and cleanUp variants, only connect/cleanUp outside
   * of transactions.
   */
  public boolean connect()
  {
    if (!inTranFlag || con == null)
    {
      return super.connect();
    }
    return true;
  }

  public void cleanUp()
  {
    if (!inTranFlag)
    {
      super.cleanUp();
    }
  }
  public void cleanUp(PreparedStatement ps)
  {
    try { ps.close(); } catch (Exception e) { }
    cleanUp();
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanUp(PreparedStatement ps, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    cleanUp(ps);
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanUp(ResultSetIterator it, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    try { it.close(); } catch (Exception e) { }
    cleanUp();
  }
  public void cleanUp(Statement s)
  {
    try { s.close(); } catch (Exception e) { }
    cleanUp();
  }
  // (closing rs is redundant, provided for legacy)
  public void cleanUp(Statement s, ResultSet rs)
  {
    try { rs.close(); } catch (Exception e) { }
    cleanUp(s);
  }


  private void storeAcState()
  {
    storedAcFlag = getAutoCommit();
  }

  private void restoreAcState()
  {
    setAutoCommit(storedAcFlag);
  }

  public void startTransaction()
  {
    checkTranStatus(false);
    storeAcState();
    setAutoCommit(false);
    connect();
    inTranFlag = true;
  }

  public void commitTransaction()
  {
    checkTranStatus(true);
    commit();
    cleanUp();
    restoreAcState();
    inTranFlag = false;
  }

  public void rollbackTransaction()
  {
    checkTranStatus(true);
    rollback();
    cleanUp();
    restoreAcState();
    inTranFlag = false;
  }

  /**
   * ACHP_CONFIG, configuration parameter handling.
   *
   * (transaction safe)
   */

  private String getConfigValue(String name)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select value from achp_config where name = '" + name + "'");
      if (rs.next())
      {
        return rs.getString("value");
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      logEntry("getConfigValue(name='" + name + "')",e);
      throw new RuntimeException(e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  private void setConfigValue(String name, String value)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      int rowCount = s.executeUpdate(
        " update achp_config set value = '" + value + "' where name = '" 
          + name + "'");
      if (rowCount < 1)
      {
        s.executeUpdate(
          " insert into achp_config ( name, value ) values ( '" 
            + name + "', '" + value + "' ) ");
      } 
    }
    catch (Exception e)
    {
      logEntry("setConfigValueInt('" + name + "'='" + value + "')",e);
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(s);
    }
  }

  private void deleteConfigValue(String name)
  {
    Statement s = null;

    try
    {
      if (!inTranFlag) connect();
      s = con.createStatement();
      int rowCount = s.executeUpdate(
        " delete from achp_config where name = '" + name + "'");
    }
    catch (Exception e)
    {
      throw new RuntimeException("deleteConfigValue('" + name + "')",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      if (!inTranFlag) cleanUp();
    }
  }

  /**
   * Config parm caching scheme
   */

  // default number of milliseconds to expire parms: 5 secs
  private static long DEFAULT_TIMEOUT = 5000;

  private static Map parms = new Hashtable();

  private long timeout = DEFAULT_TIMEOUT;

  public class Parm
  {
    public String name;
    public String value;
    public long valueTs;

    public Parm(String name, String value)
    {
      this.name = name;
      this.value = value;
      valueTs = now();
    }

    private long now()
    {
      return Calendar.getInstance().getTime().getTime();
    }

    public boolean expired()
    {
      return (now() - valueTs) > timeout;
    }
  }

  private String getValue(String name)
  {
    String value = null;
    Parm parm = (Parm)parms.get(name);
    if (parm == null || parm.expired())
    {
      value = getConfigValue(name);
      if (value != null)
      {
        parms.put(name,new Parm(name,value));
      }
    }
    else
    {
      value = parm.value;
    }
    return value;
  }

  private void setValue(String name, String value)
  {
    if (value != null)
    {
      setConfigValue(name,value);
      parms.put(name,new Parm(name,value));
    }
    else
    {
      deleteConfigValue(name);
      parms.remove(name);
    }
  }
  private void setValue(String name, int value)
  {
    setValue(name,String.valueOf(value));
  }
  private void setValue(String name, Date date, String pattern)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat(pattern);
      setValue(name,sdf.format(date));
    }
    catch (Exception e)
    {
      throw new RuntimeException("setValue('" + name + "', date=" + date 
        + ", pat='" + pattern + "')");
    }
  }
      
  private long getValueAsLong(String name)
  {
    String value = getValue(name);
    try
    {
      return Long.parseLong(value);
    }
    catch (Exception e)
    {
      throw new RuntimeException("getValueAsLong(name='" + name + "')",e);
    }
  }

  private int getValueAsInt(String name)
  {
    String value = getValue(name);
    try
    {
      return Integer.parseInt(value);
    }
    catch (Exception e)
    {
      throw new RuntimeException("getValueAsInt(name='" + name + "')",e);
    }
  }

  private Date getValueAsDate(String name, String pattern)
    throws ParseException
  {
    String value = getValue(name);
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    return sdf.parse(value);
  }

  /**
   * Parses value into a Date object.
   */
  private Date getValueAsTs(String name, String[] patterns)
  {
    String value = getValue(name);
    SimpleDateFormat sdf = new SimpleDateFormat();
    Calendar cal = Calendar.getInstance();
    for (int i = 0; i < patterns.length; ++i)
    {
      sdf.applyPattern(patterns[i]);
      try
      {
        cal.setTime(sdf.parse(value));
        return cal.getTime();
      }
      catch (ParseException pe) { }
    }
    throw new RuntimeException("Unparseable date found for name '" 
      + name + "': '" + value + "'");
  }
  private Date getValueAsTs(String name)
  {
    return getValueAsTs(name,FMT_TS_PATS);
  }

  /**
   * Returns parsed time from a config value, the date fields
   * will be set to the current date.
   */
  private Date getValueAsTime(String name, String[] patterns)
  {
    String value = getValue(name);
    SimpleDateFormat sdf = new SimpleDateFormat();
    Calendar cal = Calendar.getInstance();
    for (int i = 0; i < patterns.length; ++i)
    {
      sdf.applyPattern(patterns[i]);
      Calendar timeCal = Calendar.getInstance();
      try
      {
        timeCal.setTime(sdf.parse(value));
        timeCal.set(Calendar.YEAR,  cal.get(Calendar.YEAR));
        timeCal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        timeCal.set(Calendar.DATE,  cal.get(Calendar.DATE));
        return timeCal.getTime();
      }
      catch (ParseException pe) { }
    }
    throw new RuntimeException("Unparseable time found for name '" 
      + name + "': '" + value + "'");
  }
  private Date getValueAsTime(String name)
  {
    return getValueAsTime(name,FMT_TIME_PATS);    
  }

  /**
   * Specific config value accessors, these allow public access to config 
   * information and can provide defaults, etc.
   */

  public String getErrorNotificationEmails()
  {
    String emails = getValue(CFG_ERROR_EMAILS);
    return (emails != null ? emails : "tbaker@merchante-solutions.com");
  }

  public String getRiskEmails()
  {
    return getValue(CFG_RISK_EMAILS);
  }

  /**
   * Returns the cutoff time that would fall on the current date.
   */
  public Date getCutoffTime()
  {
    return getValueAsTime(CFG_CUTOFF_TIME);
  }

  /**
   * Returns the last post update.  This can be used to determine
   * how many days to increment post day counts in transactions.
   */
  public Date getLastPostUpdateAsTs()
  {
    return getValueAsTs(CFG_LAST_POST_UPDATE);
  }
  public Date getLastPostUpdateAsDate()
  {
    try
    {
      return getValueAsDate(CFG_LAST_POST_UPDATE,FMT_DATE1);
    }
    catch (ParseException pe)
    {
      throw new RuntimeException("Error retrieving date value for name '" 
        + CFG_LAST_POST_UPDATE + "'",pe);
    }
  }

  /**
   * Sets the last post update.
   */
  public void setLastPostUpdate(Date lastUpdate)
  {
    setValue(CFG_LAST_POST_UPDATE,lastUpdate,FMT_TS1);
  }
  public void setLastPostUpdate()
  {
    setLastPostUpdate(Calendar.getInstance().getTime());
  }

  public int getAchFileDetailLimit()
  {
    return getValueAsInt(CFG_ACH_FILE_DETAIL_LIMIT);
  }

  public long getDefaultFtId()
  {
    return getValueAsLong(CFG_DFLT_FT_ID);
  }

  /**
   * Web host name for links to the web site.
   */
  public String getWebHostName()
  {
    return getValue(CFG_WEB_HOST_NAME);
  }

  /**
   * Path to org sum data bean type reports.
   */
  public String getOrgReportPath()
  {
    return getValue(CFG_ORG_RPT_PATH);
  }

  /**
   * Return xfer parm object for the indicated system type
   */
  public XferParameters getXferParameters(int sysTypeOpt)
  {
    String parmStr = null;
    if (sysTypeOpt == SYS_TEST)
    {
      parmStr = getValue(CFG_SSH_PARMS_TEST);
    }
    else if (sysTypeOpt == SYS_PROD)
    {
      parmStr = getValue(CFG_SSH_PARMS_PROD);
    }
    else
    {
      throw new RuntimeException("Invalid system type: " + sysTypeOpt);
    }

    Pattern p = Pattern.compile("[:]?([^:]*)");
    Matcher m = p.matcher(parmStr);

    String host = null;
    String user = null;
    String pass = null;
    String path = null;
    
    m.find(); host = m.group(1);
    m.find(); user = m.group(1);
    m.find(); pass = m.group(1);
    m.find(); path = m.group(1);

    return new XferParameters(host,user,pass,path,"3941");
  }

  /**
   * Entry descriptions are assigned to ACH detail records and will show up
   * on deposit reports and customer bank statements.
   */
  public String getEntryDescription(String edtType)
  {
    if (EDT_DEPOSIT.equals(edtType))
    {
      return getValue(CFG_EDT_DEPOSIT);
    }
    else if (EDT_DEBIT.equals(edtType))
    {
      return getValue(CFG_EDT_DEBIT);
    }
    else if (EDT_SALE.equals(edtType))
    {
      return getValue(CFG_EDT_SALE);
    }
    else if (EDT_CREDIT.equals(edtType))
    {
      return getValue(CFG_EDT_CREDIT);
    }
    throw new RuntimeException("Invalid entry description type: '" 
      + edtType + "'");
  }

  /**
   * MerchProfileRef
   */

  private MerchProfileRef createMerchProfileRef(ResultSet rs) throws Exception
  {
    MerchProfileRef ref = new MerchProfileRef();
    ref.setProfileId(rs.getString("profile_id"));
    ref.setMerchNum(rs.getString("merch_num"));
    ref.setMerchName(rs.getString("merch_name"));
    ref.setMifTransitNum(rs.getString("mif_transit_num"));
    ref.setMifDdaNum(rs.getString("mif_dda_num"));
    ref.setLinkFlag(rs.getString("link_flag"));
    ref.setCcdFlag(rs.getString("ccd_flag"));
    ref.setPpdFlag(rs.getString("ppd_flag"));
    ref.setWebFlag(rs.getString("web_flag"));
    ref.setTelFlag(rs.getString("tel_flag"));
    ref.setHoldFlag(rs.getString("hold_flag"));
    return ref;
  }

  /**
   * Finds any profile ref objects matching a search term.  If more than
   * 200 matches are found null is returned.
   */
  public List lookupMerchProfileRefs(String searchTerm)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select count(tp.terminal_id) res_count " +
        " from trident_profile tp, achp_profiles p, mif " +
        " where " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and tp.terminal_id = p.profile_id(+) " +
        "  and ( tp.terminal_id like '%" + searchTerm + "%' " +
        "        or tp.merchant_number like '%" + searchTerm + "%' " +
        "        or upper(tp.merchant_name) " +
        "         like '%" + searchTerm.toUpperCase() + "%' ) ");

      if (rs.next() && rs.getInt("res_count") > 200)
      {
        return null;
      }

      rs.close();
      s.close();

      s = con.createStatement();
      rs = s.executeQuery(
        " select tp.terminal_id profile_id, tp.merchant_name merch_name, " +
        "  mif.dda_num mif_dda_num, tp.merchant_number merch_num, " +
        "  lpad(to_char(mif.transit_routng_num), 9, '0') mif_transit_num, " +
        "  nvl2(p.profile_id,'Y','N') link_flag, " +
        "  upper(nvl(p.ccd_enable_flag,'N')) ccd_flag, " +
        "  upper(nvl(p.ppd_enable_flag,'N')) ppd_flag, " +
        "  upper(nvl(p.web_enable_flag,'N')) web_flag, " +
        "  upper(nvl(p.tel_enable_flag,'N')) tel_flag, " +
        "  upper(nvl(p.hold_flag,'N')) hold_flag " +
        " from trident_profile tp, mif, " +
        "  trident_profile_api tpa, achp_profiles p " +
        " where " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and tp.terminal_id = p.profile_id(+) " +
        "  and tp.terminal_id = tpa.terminal_id " +
        "  and ( tp.terminal_id like '%" + searchTerm + "%' " +
        "        or tp.merchant_number like '%" + searchTerm + "%' " +
        "        or upper(tp.merchant_name) " +
        "         like '%" + searchTerm.toUpperCase() + "%' ) " +
        " order by tp.terminal_id ");

      List refs = new ArrayList();
      while (rs.next())
      {
        refs.add(createMerchProfileRef(rs));
      }
      return refs;
    }
    catch (Exception e)
    {
      throw new RuntimeException("lookupProfileRefs(searchTerm='" 
        + searchTerm + "')",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Returns a MerchProfileRef with the given profile ID, null if
   * none found.
   */
  public MerchProfileRef getMerchProfileRef(String profileId)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();

      s = con.createStatement();
      rs = s.executeQuery(
        " select " +
        "  tp.terminal_id                     profile_id,       " +
        "  tp.merchant_name                   merch_name,       " +
        "  tp.merchant_number                 merch_num,        " +
        "  lpad(to_char(                                        " +
        "    mif.transit_routng_num), 9, '0') mif_transit_num,  " +
        "  mif.dda_num                        mif_dda_num,      " +
        "  nvl2(p.profile_id,'Y','N')         link_flag,        " +
        "  upper(nvl(p.ccd_enable_flag,'N'))  ccd_flag,         " +
        "  upper(nvl(p.ppd_enable_flag,'N'))  ppd_flag,         " +
        "  upper(nvl(p.web_enable_flag,'N'))  web_flag,         " +
        "  upper(nvl(p.tel_enable_flag,'N'))  tel_flag,         " +
        "  upper(nvl(p.hold_flag,'N'))        hold_flag         " +
        " from " +
        "  trident_profile      tp,   " +
        "  trident_profile_api  tpa,  " +
        "  achp_profiles        p,    " +
        "  mif " +
        " where " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and tp.terminal_id = '" + profileId + "' " +
        "  and tp.terminal_id = tpa.terminal_id " +
        "  and tp.terminal_id = p.profile_id(+) ");

      if (rs.next())
      {
        return createMerchProfileRef(rs);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getProfileRef(profileId='" 
        + profileId + "')",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * ACHP_FILE_TYPES / AchFileType
   */

  public void insertAchFileType(AchFileType ft)
  {
    PreparedStatement ps = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " insert into achp_file_types ( " +
        "   ft_id, description, destination_id, destination_name, " +
        "   origin_id, origin_name, company_id, company_name, endpoint ) " +
        " values ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
      int col = 1;
      ps.setLong  (col++,ft.getFtId());
      ps.setString(col++,ft.getDescription());
      ps.setString(col++,ft.getDestinationId());
      ps.setString(col++,ft.getDestinationName());
      ps.setString(col++,ft.getOriginId());
      ps.setString(col++,ft.getOriginName());
      ps.setString(col++,ft.getCompanyId());
      ps.setString(col++,ft.getCompanyName());
      ps.setString(col++,ft.getEndpoint());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertAchFileType()",e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public void updateAchFileType(AchFileType ft)
  {
    PreparedStatement ps = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " update achp_file_types set  " +
        "   description = ?,          " +
        "   destination_id = ?,       " +
        "   destination_name = ?,     " +
        "   origin_id = ?,            " +
        "   origin_name = ?,          " +
        "   company_id = ?,           " +
        "   company_name = ?          " +
        "   endpoint = ?              " +
        " where ft_id = ?             ");

      int col = 1;
      ps.setString(col++,ft.getDescription());
      ps.setString(col++,ft.getDestinationId());
      ps.setString(col++,ft.getDestinationName());
      ps.setString(col++,ft.getOriginId());
      ps.setString(col++,ft.getOriginName());
      ps.setString(col++,ft.getCompanyId());
      ps.setString(col++,ft.getCompanyName());
      ps.setString(col++,ft.getEndpoint());
      ps.setLong(col++,ft.getFtId());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("updateAchFileType(ftId=" 
        + ft.getFtId() + ")",e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  private AchFileType createAchFileType(ResultSet rs) throws Exception
  {
    AchFileType ft = new AchFileType();
    ft.setFtId            (rs.getLong   ("ft_id"));
    ft.setDescription     (rs.getString ("description"));
    ft.setDestinationId   (rs.getString ("destination_id"));
    ft.setDestinationName (rs.getString ("destination_name"));
    ft.setOriginId        (rs.getString ("origin_id"));
    ft.setOriginName      (rs.getString ("origin_name"));
    ft.setCompanyId       (rs.getString ("company_id"));
    ft.setCompanyName     (rs.getString ("company_name"));
    ft.setEndpoint        (rs.getString ("endpoint"));
    return ft;
  }

  public AchFileType getAchFileType(long ftId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_file_types where ft_id = " + ftId);
      if (rs.next())
      {
        return createAchFileType(rs);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchFileType(ftId=" + ftId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public List getAchFileTypes()
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select * from achp_file_types ");

      List fileTypes = new ArrayList();
      while (rs.next())
      {
        fileTypes.add(createAchFileType(rs));
      }
      return fileTypes;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchFileTypes()",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public DropDownItem[] getAchFileTypeDdItems()
  {
    List fts = getAchFileTypes();
    List ddList = new ArrayList();
    for (Iterator i = fts.iterator(); i.hasNext();)
    {
      AchFileType ft = (AchFileType)i.next();
      ddList.add(new DropDownItem(""+ft.getFtId(),ft.getDescription()));
    }
    return (DropDownItem[])ddList.toArray(new DropDownItem[] { });
  }

  /**
   * ACHP_PROFILES
   */

  private MerchProfile createMerchProfile(ResultSet rs) throws Exception
  {
    MerchProfile pro = new MerchProfile();
    pro.setProfileId    (rs.getString("profile_id"));
    pro.setMerchNum     (rs.getString("merch_num"));
    pro.setMerchName    (rs.getString("merch_name"));
    pro.setCompanyName  (rs.getString("company_name"));
    pro.setCcdEnableFlag(rs.getString("ccd_enable_flag"));
    pro.setPpdEnableFlag(rs.getString("ppd_enable_flag"));
    pro.setWebEnableFlag(rs.getString("web_enable_flag"));
    pro.setTelEnableFlag(rs.getString("tel_enable_flag"));
    pro.setCcdPendDays  (rs.getInt   ("ccd_pend_days"));
    pro.setPpdPendDays  (rs.getInt   ("ppd_pend_days"));
    pro.setWebPendDays  (rs.getInt   ("web_pend_days"));
    pro.setTelPendDays  (rs.getInt   ("tel_pend_days"));
    pro.setSfSecCode(SecCode.codeFor
                        (rs.getString("sf_sec_code")));
    pro.setFtId         (rs.getLong  ("ft_id"));
    pro.setBatchNum     (rs.getInt   ("batch_num"));
    pro.setTestBatchNum (rs.getInt   ("test_batch_num"));
    pro.setHoldFlag     (rs.getString("hold_flag"));
    pro.setEmailAddress (rs.getString("email_addr"));

    String accountNum  = rs.getString("account_num");
    String accountType = rs.getString("account_type");
    String transitNum  = rs.getString("transit_num");
    if (accountNum != null)
    {
      pro.setAccountNum(accountNum);
    }
    if (accountType != null)
    {
      pro.setAccountType(accountType);
    }
    if (transitNum != null)
    {
      pro.setTransitNum(transitNum);
    }

    return pro;
  }

  /**
   * Fetch profile matching profile id.
   */
  public MerchProfile getMerchProfile(String profileId)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select " +
        "  profile_id,        " +
        "  merch_num,         " +
        "  merch_name,        " +
        "  company_name,      " +
        "  transit_num,       " +
        "  account_type,      " +
        "  dukpt_decrypt_wrapper(account_num_enc) account_num, " +
        "  sf_sec_code,       " +
        "  ccd_enable_flag,   " +
        "  ppd_enable_flag,   " +
        "  web_enable_flag,   " +
        "  tel_enable_flag,   " +
        "  ccd_pend_days,     " +
        "  ppd_pend_days,     " +
        "  web_pend_days,     " +
        "  tel_pend_days,     " +
        "  hold_flag,         " +
        "  batch_num,         " +
        "  test_batch_num,    " +
        "  email_addr,        " +
        "  ft_id              " +
        " from achp_profiles  " +
        " where " +
        "  profile_id = '" + profileId + "' ");

      if (rs.next())
      {
        return createMerchProfile(rs);
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      logEntry("getMerchProfile(profileId=" + profileId + ")",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
    return null;
  }

  /**
   * Find any profiles matching the search term.  Attempts to match search
   * term to dba name, merch num, profile id.
   */
  public List lookupMerchProfiles(String searchTerm)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " select  " +
        "  profile_id,      " +
        "  merch_num,       " +
        "  merch_name,      " +
        "  company_name,    " +
        "  transit_num,     " +
        "  account_type,    " +
        "  dukpt_decrypt_wrapper(account_num_enc) account_num, " +
        "  sf_sec_code,     " +
        "  ccd_enable_flag, " +
        "  ppd_enable_flag, " +
        "  web_enable_flag, " +
        "  tel_enable_flag, " +
        "  ccd_pend_days,   " +
        "  ppd_pend_days,   " +
        "  web_pend_days,   " +
        "  tel_pend_days,   " +
        "  hold_flag,       " +
        "  batch_num,       " +
        "  test_batch_num,  " +
        "  email_addr,      " +
        "  ft_id            " +
        " from " +
        "  achp_profiles    " +
        " where " +
        "  ( profile_id like '%' || ? || '%' " +
        "    or merch_num like '%' || ? || '%' " +
        "    or upper(merch_name) like '%' || upper(?) || '%' ) ");

      ps.setString(1,searchTerm);
      ps.setString(2,searchTerm);
      ps.setString(3,searchTerm);
      rs = ps.executeQuery();

      List profiles = new ArrayList();
      while (rs.next())
      {
        profiles.add(createMerchProfile(rs));
      }
      return profiles;
    }
    catch (Exception e)
    {
      throw new RuntimeException("lookupMerchProfiles(searchTerm='" 
        + searchTerm + "')",e);
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  public void insertMerchProfile(MerchProfile profile)
  {
    PreparedStatement ps = null;

    try
    {
      byte[] accountNumEnc = profile.getAccount().getEncodedData();

      connect();

      ps = con.prepareStatement(
        " insert into achp_profiles ( " +
        "  profile_id,        " +
        "  merch_num,         " +
        "  merch_name,        " +
        "  company_name,      " +
        "  ccd_enable_flag,   " +
        "  ppd_enable_flag,   " +
        "  web_enable_flag,   " +
        "  tel_enable_flag,   " +
        "  ccd_pend_days,     " +
        "  ppd_pend_days,     " +
        "  web_pend_days,     " +
        "  tel_pend_days,     " +
        "  transit_num,       " +
        "  account_num,       " +
        "  account_num_enc,   " +
        "  account_type,      " +
        "  sf_sec_code,       " +
        "  hold_flag,         " +
        "  batch_num,         " +
        "  test_batch_num,    " +
        "  email_addr,        " +
        "  ft_id )            " +
        " values (            " +
        "  ?, ?, ?, ?, ?, ?, ?,   " +
        "  ?, ?, ?, ?, ?, ?, ?,   " +
        "  ?, ?, ?, ?, ?, ?, ?, ? )  ");

      int col = 1;
      ps.setString    (col++,profile.getProfileId());
      ps.setString    (col++,profile.getMerchNum());
      ps.setString    (col++,profile.getMerchName());
      ps.setString    (col++,profile.getCompanyName());
      ps.setString    (col++,profile.getCcdEnableFlag());
      ps.setString    (col++,profile.getPpdEnableFlag());
      ps.setString    (col++,profile.getWebEnableFlag());
      ps.setString    (col++,profile.getTelEnableFlag());
      ps.setInt       (col++,profile.getCcdPendDays());
      ps.setInt       (col++,profile.getPpdPendDays());
      ps.setInt       (col++,profile.getWebPendDays());
      ps.setInt       (col++,profile.getTelPendDays());
      ps.setString    (col++,profile.getTransitNum());
      ps.setString    (col++,profile.getAccountNum());
      ps.setBytes     (col++,accountNumEnc);
      ps.setString    (col++,profile.getAccountType());
      ps.setString    (col++,""+profile.getSfSecCode());
      ps.setString    (col++,profile.getHoldFlag());
      ps.setInt       (col++,profile.getBatchNum());
      ps.setInt       (col++,profile.getTestBatchNum());
      ps.setString    (col++,profile.getEmailAddress());
      ps.setLong      (col++,profile.getFtId());

      ps.executeUpdate();
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      throw new RuntimeException("insertMerchProfile(profileId=" 
        + profile.getProfileId() + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  public void updateMerchProfile(MerchProfile profile)
  {
    PreparedStatement ps = null;

    try
    {
      byte[] accountNumEnc = profile.getAccount().getEncodedData();

      connect();

      ps = con.prepareStatement(
        " update achp_profiles set " +
        "  company_name = ?,    " +
        "  ccd_enable_flag = ?, " +
        "  ppd_enable_flag = ?, " +
        "  web_enable_flag = ?, " +
        "  tel_enable_flag = ?, " +
        "  ccd_pend_days = ?,   " +
        "  ppd_pend_days = ?,   " +
        "  web_pend_days = ?,   " +
        "  tel_pend_days = ?,   " +
        "  transit_num = ?,     " +
        "  account_num = ?,     " +
        "  account_num_enc = ?, " +
        "  account_type = ?,    " +
        "  sf_sec_code = ?,     " + 
        "  hold_flag = ?,       " +
        "  batch_num = ?,       " +
        "  test_batch_num = ?,  " +
        "  email_addr = ?,      " +
        "  ft_id = ?            " +
        " where profile_id = ?  ");

      int col = 1;
      ps.setString    (col++,profile.getCompanyName());
      ps.setString    (col++,profile.getCcdEnableFlag());
      ps.setString    (col++,profile.getPpdEnableFlag());
      ps.setString    (col++,profile.getWebEnableFlag());
      ps.setString    (col++,profile.getTelEnableFlag());
      ps.setInt       (col++,profile.getCcdPendDays());
      ps.setInt       (col++,profile.getPpdPendDays());
      ps.setInt       (col++,profile.getWebPendDays());
      ps.setInt       (col++,profile.getTelPendDays());
      ps.setString    (col++,profile.getTransitNum());
      ps.setString    (col++,profile.getAccountNum());
      ps.setBytes     (col++,accountNumEnc);
      ps.setString    (col++,profile.getAccountType());
      ps.setString    (col++,""+profile.getSfSecCode());
      ps.setString    (col++,profile.getHoldFlag());
      ps.setInt       (col++,profile.getBatchNum());
      ps.setInt       (col++,profile.getTestBatchNum());
      ps.setString    (col++,profile.getEmailAddress());
      ps.setLong      (col++,profile.getFtId());
      ps.setString    (col++,profile.getProfileId());

      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("updateMerchProfile(profileId=" 
        + profile.getProfileId() + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * ACHP_TRANSACTIONS
   */

  private void loadAchTransaction(AchTransaction tran, ResultSet rs) 
    throws Exception
  {
    tran.setTranId        (rs.getLong     ("tran_id"));
    tran.setTridentTranId (rs.getString   ("trident_tran_id"));
    tran.setMerchNum      (rs.getString   ("merch_num"));
    tran.setProfileId     (rs.getString   ("profile_id"));
    tran.setFtId          (rs.getLong     ("ft_id"));
    tran.setTranTs        (rs.getTimestamp("tran_ts"));
    tran.setPendTs        (rs.getTimestamp("pend_ts"));
    tran.setTranSource    (TranSource.sourceFor(rs.getString("tran_source")));
    tran.setRequestId     (rs.getLong     ("request_id"));
    tran.setTranType      (TranType.typeFor(rs.getString("tran_type")));
    tran.setStatus        (TranStatus.statusFor(rs.getString("tran_status")));
    tran.setAccountNum    (rs.getString   ("account_num"));
    tran.setAccountType   (rs.getString   ("account_type"));
    tran.setTransitNum    (rs.getString   ("transit_num"));
    tran.setAmount        (stringToBigDecimal(rs.getString("amount")));
    tran.setRefNum        (rs.getString   ("ref_num"));
    tran.setSecCode       (SecCode.codeFor(rs.getString("sec_code")));
    tran.setCustName      (rs.getString   ("cust_name"));
    tran.setCustId        (rs.getString   ("cust_id"));
    tran.setIpAddress     (rs.getString   ("ip_address"));
    tran.setBatchId       (rs.getInt      ("batch_id"));
    tran.setRecurFlag     (rs.getString   ("recur_flag"));
    tran.setTestFlag      (rs.getString   ("test_flag"));
    tran.setHoldFlag      (rs.getString   ("hold_flag"));
  }

  private AchTransaction createAchTransaction(ResultSet rs) throws Exception
  {
    AchTransaction tran = new AchTransaction();
    loadAchTransaction(tran,rs);
    return tran;
  }

  public AchTransaction getAchTransaction(long tranId)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select              " +
        "  tran_id,           " +
        "  trident_tran_id,   " + 
        "  merch_num,         " +
        "  profile_id,        " +
        "  ft_id,             " +
        "  tran_ts,           " +
        "  pend_ts,           " +
        "  tran_source,       " +
        "  tran_status,       " +
        "  request_id,        " +
        "  tran_type,         " + 
        "  tran_source,       " +
        "  request_id,        " +
        "  dukpt_decrypt_wrapper(" +
        "   account_num_enc)  " +
        "    account_num,     " +
        "  account_type,      " +
        "  transit_num,       " +
        "  amount,            " +
        "  ref_num,           " +
        "  sec_code,          " +
        "  cust_name,         " +
        "  cust_id,           " +
        "  ip_address,        " +
        "  batch_id,          " +
        "  recur_flag,        " +
        "  test_flag,         " +
        "  hold_flag          " +
        " from                " +
        "  achp_transactions  " +
        " where               " +
        "  tran_id = " + tranId);

      if (rs.next())
      {
        return createAchTransaction(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchTransaction(tranId=" + tranId + ")",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
    return null;
  }

  public void updateAchTransactionStatus(AchTransaction tran)
  {
    Statement s = null;

    try
    {
      s = con.createStatement();
      s.executeUpdate(
        " update achp_transactions " +
        " set tran_status = '" + tran.getStatus() + "' " +
        " where tran_id = " + tran.getTranId());
    }
    catch (Exception e)
    {
      throw new RuntimeException("updateAchTransaction(tranId=" 
        + tran.getTranId() + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  public List getTransWithStatus(List statusList, int batchSize, 
    int enabledOnlyOpt, int allowHoldsOpt, int sysTypeOpt, long batchId)
  {
    Statement s = null;
    ResultSet rs = null;

    boolean enabledOnly = checkEnabledOnlyOption(enabledOnlyOpt);
    boolean allowHolds = checkAllowHoldsOption(allowHoldsOpt);
    boolean batchFilter = batchId != -1L;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    StringBuffer statuses = new StringBuffer();

    try
    {
      for (Iterator i = statusList.iterator(); i.hasNext();)
      {
        String status = ""+i.next();
        statuses.append("'" + status + "'");
        statuses.append(i.hasNext() ? ", " : "");
      }

      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select                " +
        "  t.tran_id,           " +
        "  t.trident_tran_id,   " + 
        "  t.merch_num,         " +
        "  t.profile_id,        " +
        "  t.ft_id,             " +
        "  t.tran_ts,           " +
        "  t.pend_ts,           " +
        "  t.tran_source,       " +
        "  t.request_id,        " +
        "  t.tran_type,         " + 
        "  t.tran_source,       " +
        "  t.tran_status,       " +
        "  t.request_id,        " +
        "  dukpt_decrypt_wrapper( " +
        "   t.account_num_enc)  " +
        "    account_num,       " +
        "  t.account_type,      " +
        "  t.transit_num,       " +
        "  t.amount,            " +
        "  t.ref_num,           " +
        "  t.sec_code,          " +
        "  t.cust_name,         " +
        "  t.cust_id,           " +
        "  t.ip_address,        " +
        "  t.batch_id,          " +
        "  t.recur_flag,        " +
        "  t.test_flag,         " +
        "  t.hold_flag          " +
        " from                  " +
        "  achp_transactions t, " +
        "  achp_profiles p      " +
        " where                 " +
        "  t.profile_id = p.profile_id " +
        (batchSize > 0 ? " and rownum <= " + batchSize : "") +
        (!allowHolds ? " and p.hold_flag <> 'Y' and t.hold_flag <> 'Y' " : "") +
        (enabledOnly ? " and decode( t.sec_code,    " + 
                       "  'CCD', p.ccd_enable_flag, " +
                       "  'PPD', p.ppd_enable_flag, " +
                       "  'WEB', p.web_enable_flag, " +
                       "  'TEL', p.tel_enable_flag, 'N' ) = 'Y' " : "") +
        (batchFilter ? " and t.batch_id = " + batchId + " " : "") +
        "  and test_flag = '" + testFlagVal + "' " +
        "  and t.tran_status in ( " + statuses + " )");

      List trans = new ArrayList();
      while (rs.next())
      {
        trans.add(createAchTransaction(rs));
      }
      return trans;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getTransWithStatus(" + statuses + ")",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }
  public List getTransWithStatus(TranStatus status, int batchSize, 
    int enabledOnlyOpt, int allowHoldsOpt, int sysTypeOpt, long batchId)
  {
    List statuses = new ArrayList();
    statuses.add(status);
    return getTransWithStatus(
      statuses,batchSize,enabledOnlyOpt,allowHoldsOpt,sysTypeOpt,batchId);
  }

  /**
   * ACHP_FILE_DETAILS
   */

  public void insertAchFileDetail(AchFileDetail detail)
  {
    PreparedStatement ps = null;

    try
    {
      byte[] accountNumEnc = detail.getAccount().getEncodedData();

      connect();

      ps = con.prepareStatement(
        " insert into achp_file_details ( " +
        "  merch_num,         " +
        "  profile_id,        " +
        "  ft_id,             " +
        "  transit_num,       " +
        "  account_num,       " +
        "  account_num_enc,   " +
        "  account_type,      " +
        "  account_id,        " +
        "  account_name,      " +
        "  cd_code,           " +
        "  amount,            " +
        "  destination,       " +
        "  source_id,         " +
        "  sec_code,          " +
        "  entry_description, " +
        "  trace_num,         " +
        "  test_flag,         " +
        "  recur_code,        " +
        "  hold_flag )        " +
        " values ( " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ? )    ");

      int col = 1;
      ps.setString    (col++,detail.getMerchNum());
      ps.setString    (col++,detail.getProfileId());
      ps.setLong      (col++,detail.getFtId());
      ps.setString    (col++,detail.getTransitNum());
      ps.setString    (col++,detail.getAccountNum());
      ps.setBytes     (col++,accountNumEnc);
      ps.setString    (col++,detail.getAccountType());
      ps.setString    (col++,detail.getAccountId());
      // dba name is being used for settlement 
      // need to truncate at 23 if too long
      String name = detail.getAccountName();
      name = name.length() > 23 ? name.substring(0,23) : name;
      ps.setString    (col++,name);
      ps.setString    (col++,""+detail.getCdCode());
      ps.setBigDecimal(col++,detail.getAmount());
      ps.setString    (col++,""+detail.getDestination());
      ps.setLong      (col++,detail.getSourceId());
      ps.setString    (col++,""+detail.getSecCode());
      ps.setString    (col++,detail.getEntryDescription());
      ps.setString    (col++,detail.getTraceNum());
      ps.setString    (col++,detail.getTestFlag());
      ps.setString    (col++,detail.getRecurCode());
      ps.setString    (col++,detail.getHoldFlag());

      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertAchFileDetail(sourceId=" 
        + detail.getSourceId() + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  private AchFileDetail createAchFileDetail(ResultSet rs) throws Exception
  {
    AchFileDetail detail = new AchFileDetail();
    detail.setDetailId    (rs.getLong       ("detail_id"));
    detail.setCreateTs    (rs.getTimestamp  ("create_ts"));
    detail.setMerchNum    (rs.getString     ("merch_num"));
    detail.setProfileId   (rs.getString     ("profile_id"));
    detail.setBatchId     (rs.getLong       ("batch_id"));
    detail.setFileId      (rs.getLong       ("file_id"));
    detail.setFtId        (rs.getLong       ("ft_id"));
    detail.setTransitNum  (rs.getString     ("transit_num"));
    detail.setAccountNum  (rs.getString     ("account_num"));
    detail.setAccountType (rs.getString     ("account_type"));
    detail.setAccountId   (rs.getString     ("account_id"));
    detail.setAccountName (rs.getString     ("account_name"));
    detail.setCdCode      (
      CreditDebitCode.codeFor(rs.getString("cd_code")));
    detail.setAmount      (rs.getBigDecimal ("amount").setScale(2));
    detail.setDestination (
      Destination.destinationFor(rs.getString("destination")));
    detail.setSourceId    (rs.getLong       ("source_id"));
    detail.setSecCode    (SecCode.codeFor(rs.getString("sec_code")));
    detail.setEntryDescription
                          (rs.getString     ("entry_description"));
    detail.setTestFlag    (rs.getString     ("test_flag"));
    detail.setHoldFlag    (rs.getString     ("hold_flag"));
    detail.setRecurCode   (rs.getString     ("recur_code"));
    detail.setTraceNum    (rs.getString     ("trace_num"));
    return detail;
  }

  /**
   * Fetches file record using recId.
   */
  public AchFileDetail getAchFileDetail(long detailId)
  {
    Statement s = null;
    ResultSet rs = null;

    try
    {
      connect();
      s = con.createStatement();
      rs = s.executeQuery(
        " select              " +
        "  detail_id,            " +
        "  create_ts,            " +
        "  merch_num,         " +
        "  profile_id,        " +
        "  ft_id,             " +
        "  transit_num,       " +
        "  dukpt_decrypt_wrapper(" +
        "   account_num_enc)  " +
        "    account_num,     " +
        "  account_type,      " +
        "  account_id,        " +
        "  account_name,      " +
        "  cd_code,           " +
        "  amount,            " +
        "  destination,       " +
        "  source_id,         " +
        "  sec_code,          " +
        "  entry_description, " +
        "  test_flag,         " +
        "  hold_flag,         " +
        "  file_id,           " +
        "  trace_num,         " +
        "  recur_code,        " +
        "  batch_id           " +
        " from                " +
        "  achp_file_details  " +
        " where               " +
        "  detail_id = " + detailId);

      if (rs.next())
      {
        return createAchFileDetail(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchFileDetail(detailId=" + detailId + ")",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
    return null;
  }

  public List getBatchAchDetails(long batchId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                  " +
        "  d.detail_id,           " +
        "  d.create_ts,           " +
        "  d.merch_num,           " +
        "  d.profile_id,          " +
        "  d.ft_id,               " +
        "  d.transit_num,         " +
        "  dukpt_decrypt_wrapper( " +
        "   d.account_num_enc)    " +
        "    account_num,         " +
        "  d.account_type,        " +
        "  d.account_id,          " +
        "  d.account_name,        " +
        "  d.cd_code,             " +
        "  d.amount,              " +
        "  d.destination,         " +
        "  d.source_id,           " +
        "  d.sec_code,            " +
        "  d.entry_description,   " +
        "  d.test_flag,           " +
        "  d.hold_flag,           " +
        "  d.file_id,             " +
        "  d.trace_num,           " +
        "  d.recur_code,          " +
        "  d.batch_id             " +
        " from                    " +
        "  achp_file_details d,   " +
        "  achp_transactions t    " +
        " where                   " +
        "  d.source_id = t.tran_id " +
        "  and t.batch_id = " + batchId);

      List dtls = new ArrayList();
      while (rs.next())
      {
        dtls.add(createAchFileDetail(rs));
      }

      s.close();
      s = con.createStatement();
      rs = s.executeQuery(
        " select                  " +
        "  d.detail_id,           " +
        "  d.create_ts,           " +
        "  d.merch_num,           " +
        "  d.profile_id,          " +
        "  d.ft_id,               " +
        "  d.transit_num,         " +
        "  dukpt_decrypt_wrapper( " +
        "   d.account_num_enc)    " +
        "    account_num,         " +
        "  d.account_type,        " +
        "  d.account_id,          " +
        "  d.account_name,        " +
        "  d.cd_code,             " +
        "  d.amount,              " +
        "  d.destination,         " +
        "  d.source_id,           " +
        "  d.sec_code,            " +
        "  d.entry_description,   " +
        "  d.test_flag,           " +
        "  d.hold_flag,           " +
        "  d.file_id,             " +
        "  d.trace_num,           " +
        "  d.recur_code,          " +
        "  d.batch_id             " +
        " from                    " +
        "  achp_file_details d    " +
        " where                   " +
        "  d.source_id in (       " +
        "   select                " +
        "    unique(e.summary_id) " +
        "   from                  " +
        "    achp_sf_entries e,   " +
        "    achp_transactions t  " +
        "   where                 " +
        "    e.tran_id = t.tran_id " +
        "    and t.batch_id = " + batchId + " )");

      while (rs.next())
      {
        dtls.add(createAchFileDetail(rs));
      }

      return dtls;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getBatchAchDetails()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * ACHP_SF_ENTRIES
   */

  private FundingEntry createFundingEntry(ResultSet rs) throws Exception
  {
    FundingEntry entry = new FundingEntry();
    entry.setEntryId  (rs.getLong       ("entry_id"));
    entry.setEntryTs  (rs.getTimestamp  ("entry_ts"));
    entry.setMerchNum (rs.getString     ("merch_num"));
    entry.setProfileId(rs.getString     ("profile_id"));
    entry.setTranId   (rs.getLong       ("tran_id"));
    entry.setFtId     (rs.getLong       ("ft_id"));
    entry.setEntryType(FundingEntryType.typeFor(rs.getString("entry_type")));
    entry.setSummaryId(rs.getLong       ("summary_id"));
    entry.setAmount   (rs.getBigDecimal ("amount").setScale(2));
    entry.setTestFlag (rs.getString     ("test_flag"));
    return entry;
  }

  public FundingEntry getFundingEntry(long entryId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_sf_entries " +
        " where entry_id = " + entryId);

      if (rs.next())
      {
        return createFundingEntry(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("getFundingEntry(entryId=" + entryId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  public List getBatchFundingEntries(long batchId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select e.* from achp_sf_entries e, achp_transactions t " +
        " where e.tran_id = t.tran_id and t.batch_id = " + batchId);
      List entries = new ArrayList();
      while (rs.next())
      {
        entries.add(createFundingEntry(rs));
      }
      return entries;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getBatchFundingEntries(batchId=" + batchId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public void insertFundingEntry(FundingEntry entry)
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into achp_sf_entries ( " +
        "  merch_num,   " +
        "  profile_id,  " +
        "  tran_id,     " +
        "  ft_id,       " +
        "  entry_type,  " +
        "  amount,      " +
        "  test_flag )  " +
        " values (      " +
        "  ?, ?, ?, ?, ?, ?, ? )");

      int col = 1;
      ps.setString    (col++,entry.getMerchNum());
      ps.setString    (col++,entry.getProfileId());
      ps.setLong      (col++,entry.getTranId());
      ps.setLong      (col++,entry.getFtId());
      ps.setString    (col++,""+entry.getEntryType());
      ps.setBigDecimal(col++,entry.getAmount());
      ps.setString    (col++,entry.getTestFlag());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertFundingEntry(tranId=" 
        + entry.getEntryId() + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  /**
   * Processing
   */

  /**
   * Fetch batch of new sales to pend batch fetching.
   *
   * NOTES:
   *
   *   This requires SEC type to be enabled in order to pend.  Settlement
   *   does not currently check this to allow transactions to complete
   *   life cycle once started even if SEC type is disabled before
   *   settlement...
   */
  public List getNewSalesToPend(int batchSize, int sysTypeOpt, long batchId)
  {
    return getTransWithStatus(
      TranStatus.S_NEW,batchSize,SEC_ENABLED_ONLY,DISALLOW_HOLDS,sysTypeOpt,batchId);
  }      
  public List getNewSalesToPend(int batchSize, int sysTypeOpt)
  {
    return getNewSalesToPend(batchSize,sysTypeOpt,-1L);
  }

  /**
   * Generate ach file record, update sales transaction to pending.
   */
  public void pendNewSale(AchTransaction sale, AchFileDetail detail)
  {
    boolean acSetting = getAutoCommit();
    boolean opSuccess = false;

    try
    {
      setAutoCommit(false);
      connect();
      insertAchFileDetail(detail);
      updateAchTransactionStatus(sale);
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("pendNewSale(tranId=" + sale.getTranId() + ")",e);
    }
    finally
    {
      if (opSuccess) { commit(); } else { rollback(); }
      cleanUp();
      setAutoCommit(acSetting);
    }
  }

  /**
   * Fetch batch of pending sales eligible for settlement.
   *
   * Settlement eligibility: if post days >= pend wait period, then settle.
   * If batch != -1L then override pend delay.
   */
  public List getPendingSalesToSettle(int batchSize, int sysTypeOpt, 
    long batchId)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    boolean fastModeFlag = batchId != -1L;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                    " +
        "  t.tran_id,               " +
        "  t.trident_tran_id,       " + 
        "  t.merch_num,             " +
        "  t.profile_id,            " +
        "  t.ft_id,                 " +
        "  t.tran_ts,               " +
        "  t.pend_ts,               " +
        "  t.tran_source,           " +
        "  t.tran_status,           " +
        "  t.request_id,            " +
        "  t.tran_type,             " + 
        "  t.tran_source,           " +
        "  t.request_id,            " +
        "  dukpt_decrypt_wrapper(   " +
        "   t.account_num_enc)      " +
        "    account_num,           " +
        "  t.account_type,          " +
        "  t.transit_num,           " +
        "  t.amount,                " +
        "  t.ref_num,               " +
        "  t.sec_code,              " +
        "  t.cust_name,             " +
        "  t.cust_id,               " +
        "  t.ip_address,            " +
        "  t.batch_id,              " +
        "  t.recur_flag,            " +
        "  t.test_flag,             " +
        "  t.hold_flag              " +
        " from achp_transactions t, achp_profiles p     " +
        " where                                         " +
        "  t.tran_status = '" + TranStatus.S_POST + "'  " +
        "  and t.test_flag = '" + testFlagVal + "'      " +
        "  and t.profile_id = p.profile_id              " +
        "  and t.hold_flag <> 'Y'                       " + 
        "  and p.hold_flag <> 'Y'                       " +
        "  and decode( t.sec_code,                      " + 
        "   'CCD', p.ccd_enable_flag,                   " +
        "   'PPD', p.ppd_enable_flag,                   " +
        "   'WEB', p.web_enable_flag,                   " +
        "   'TEL', p.tel_enable_flag, 'N' ) = 'Y'       " +
        "  and ( " + (fastModeFlag ? "1" : "0") + " = 1 " +
        "        or t.post_days >=                      " +
        "         decode( t.sec_code,                   " +
        "          'CCD', p.ccd_pend_days,              " +
        "          'PPD', p.ppd_pend_days,              " +
        "          'WEB', p.web_pend_days,              " +
        "          'TEL', p.tel_pend_days, 3 ) )        " +
        (batchId != -1L ? " and t.batch_id = " + batchId + " " : "") +
        (batchSize > 0 ? " and rownum <= " + batchSize : ""));

      List trans = new ArrayList();
      while (rs.next())
      {
        trans.add(createAchTransaction(rs));
      }
      return trans;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getPendingSalesToSettle()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Generate merchant settlement funding entry, update sales transaction 
   * to settled.
   */
  public void settlePendingSale(AchTransaction sale, FundingEntry entry)
  {
    boolean acSetting = getAutoCommit();
    boolean opSuccess = false;

    try
    {
      setAutoCommit(false);
      connect();
      insertFundingEntry(entry);
      updateAchTransactionStatus(sale);
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("settlePendingSale(tranId=" 
        + sale.getTranId() + ")",e);
    }
    finally
    {
      if (opSuccess) { commit(); } else { rollback(); }
      cleanUp();
      setAutoCommit(acSetting);
    }
  }

  /**
   * Fetch new batch of credits to pend.
   */
  public List getNewCreditsToPend(int batchSize, int sysTypeOpt, long batchId)
  {
    return getTransWithStatus(
      TranStatus.C_NEW,batchSize,SEC_ENABLED_ONLY,
      DISALLOW_HOLDS,sysTypeOpt,batchId);
  }
  public List getNewCreditsToPend(int batchSize, int sysTypeOpt)
  {
    return getNewCreditsToPend(batchSize,sysTypeOpt,-1L);
  }

  /**
   * Generate merch settlement funding credit entry, update credit transaction
   * to pending.
   */
  public void pendNewCredit(AchTransaction credit, FundingEntry entry)
  {
    boolean opSuccess = false;

    try
    {
      startTransaction();
      insertFundingEntry(entry);
      updateAchTransactionStatus(credit);
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("pendNewCredit(tranId=" 
        + credit.getTranId() + ")",e);
    }
    finally
    {
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * Fetch batch of pending credits eligible for settlement.
   *
   * Settlement eligibility: if post days is >= than the profile
   * pend wait period, then settle.  If batch != -1L then override
   * pend delay.
   */
  public List getPendingCreditsToSettle(int batchSize, int sysTypeOpt, 
    long batchId)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    boolean fastModeFlag = batchId != -1L;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                    " +
        "  t.tran_id,               " +
        "  t.trident_tran_id,       " + 
        "  t.merch_num,             " +
        "  t.profile_id,            " +
        "  t.ft_id,                 " +
        "  t.tran_ts,               " +
        "  t.pend_ts,               " +
        "  t.tran_source,           " +
        "  t.tran_status,           " +
        "  t.request_id,            " +
        "  t.tran_type,             " + 
        "  t.tran_source,           " +
        "  t.request_id,            " +
        "  dukpt_decrypt_wrapper(   " +
        "   t.account_num_enc)      " +
        "    account_num,           " +
        "  t.account_type,          " +
        "  t.transit_num,           " +
        "  t.amount,                " +
        "  t.ref_num,               " +
        "  t.sec_code,              " +
        "  t.cust_name,             " +
        "  t.cust_id,               " +
        "  t.ip_address,            " +
        "  t.batch_id,              " +
        "  t.recur_flag,            " +
        "  t.test_flag,             " +
        "  t.hold_flag              " +
        " from                      " +
        "  achp_transactions  t,                        " +
        "  achp_profiles      p,                        " +
        "  achp_sf_entries    e,                        " + 
        "  achp_sf_summaries  s,                        " +
        "  achp_file_details  fd                        " +
        " where                                         " +
        "  t.tran_status = '" + TranStatus.C_POST + "'  " +
        "  and t.test_flag = '" + testFlagVal + "'      " +
        "  and t.profile_id = p.profile_id              " +
        "  and t.hold_flag <> 'Y'                       " +
        "  and p.hold_flag <> 'Y'                       " +
        "  and decode( t.sec_code,                      " + 
        "   'CCD', p.ccd_enable_flag,                   " +
        "   'PPD', p.ppd_enable_flag,                   " +
        "   'WEB', p.web_enable_flag,                   " +
        "   'TEL', p.tel_enable_flag, 'N' ) = 'Y'       " +
        "  and ( " + (fastModeFlag ? "1" : "0") + " = 1 " +
        "        or t.post_days >=                      " +
        "         decode( t.sec_code,                   " +
        "          'CCD', p.ccd_pend_days,              " +
        "          'PPD', p.ppd_pend_days,              " +
        "          'WEB', p.web_pend_days,              " +
        "          'TEL', p.tel_pend_days, 3 ) )        " +
        "  and t.tran_id = e.tran_id                    " +
        "  and e.summary_id = s.summary_id              " + 
        "  and s.summary_id = fd.source_id              " +
        //"  and trunc(t.pend_ts) + 1 < sysdate " +
        (batchId != -1L ? " and t.batch_id = " + batchId + " " : "") +
        (batchSize > 0 ? "  and rownum <= " + batchSize : ""));

      List trans = new ArrayList();
      while (rs.next())
      {
        trans.add(createAchTransaction(rs));
      }
      return trans;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getPendingCreditsToSettle()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Generate ach file record, update credit transaction to settled.
   */
  public void settlePendingCredit(AchTransaction credit, AchFileDetail detail)
  {
    boolean opSuccess = false;

    try
    {
      startTransaction();
      insertAchFileDetail(detail);
      updateAchTransactionStatus(credit);
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("settlePendingCredit(tranId=" 
        + credit.getTranId() + ")",e);
    }
    finally
    {
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * Find profiles with settlement funding entries that have not been 
   * summarized, that are not on hold and that match the test flag.
   */
  public List getUnsummarizedProfiles(int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                    " +
        "  unique(e.profile_id)                     " +
        " from                                      " +
        "  achp_sf_entries e,                       " +
        "  achp_transactions t,                     " +
        "  achp_profiles p                          " +
        " where                                     " +
        "  e.summary_id is null                     " +
        "  and e.test_flag = '" + testFlagVal + "'  " +
        "  and e.tran_id = t.tran_id                " +
        "  and t.hold_flag <> 'Y'                   " +
        "  and e.profile_id = p.profile_id          " +
        "  and p.hold_flag <> 'Y'                   ");

      List profiles = new ArrayList();
      while (rs.next())
      {
        profiles.add(rs.getString("profile_id"));
      }
      return profiles;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getUnsummarizedProfiles(testFlagVal=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Fetch new summary id and assign to each entry in ACHP_SF_ENTRIES
   * that does not have a summary id, has the summary profile id and 
   * falls before the cutoff timestamp.  Returns the assigned id, which
   * should be used to construct a summary record from the entries
   * it was assigned to.  Only assigns to entries with correct test flag.
   * If batchId is given, filters by batchId.
   *
   * TODO: get rid of cutoff date filtering, just look for all entries
   * associated with closed batches and a given profile (see above query)...
   */
  private long assignSummaryIdToEntries(Date cutoff, String profileId,
    int sysTypeOpt, long batchId)
  {
    PreparedStatement ps = null;
    long summaryId = -1L;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();

      summaryId = getNewId();

      ps = con.prepareStatement(
        " update achp_sf_entries      " +
        " set summary_id = ?          " +
        " where entry_id in  (        " +
        "  select e.entry_id          " +
        "  from                       " +
        "   achp_sf_entries e,        " +
        "   achp_transactions t,      " +
        "   achp_profiles p           " +
        "  where                      " +
        "   e.profile_id = ?          " +
        "   and e.summary_id is null  " +
        "   and e.entry_ts <= ?       " +
        "   and e.test_flag = ?       " +
        "   and e.tran_id = t.tran_id " +
        "   and t.hold_flag <> 'Y'    " +
        (batchId != -1L ? " and t.batch_id = " + batchId + " " : "") + 
        "   and e.profile_id = p.profile_id " +
        "   and p.hold_flag <> 'Y' )  ");
      int fld = 1;
      ps.setLong      (fld++,summaryId);
      ps.setString    (fld++,profileId);
      ps.setTimestamp (fld++,toTimestamp(cutoff));
      ps.setString    (fld++,testFlagVal);
      int rows = ps.executeUpdate();

      return rows > 0 ? summaryId : -1L;
    }
    catch (Exception e)
    {
      throw new RuntimeException("assignSummaryIdToEntries(profileId=" 
        + profileId + ", cutoff=" + cutoff + ", summaryId=" + summaryId 
        + ", testFlagVal=" + testFlagVal + ", batchId=" + batchId + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  private FundingSummary createFundingSummary(ResultSet rs) throws Exception
  {
    FundingSummary sum = new FundingSummary();
    sum.setSummaryId        (rs.getLong       ("summary_id"));
    sum.setSummaryTs        (rs.getTimestamp  ("summary_ts"));
    sum.setCutoffTs         (rs.getTimestamp  ("cutoff_ts"));
    sum.setMerchNum         (rs.getString     ("merch_num"));
    sum.setProfileId        (rs.getString     ("profile_id"));
    sum.setFtId             (rs.getLong       ("ft_id"));
    sum.setSaleCount        (rs.getInt        ("sale_count"));
    sum.setSaleTotal        (rs.getBigDecimal ("sale_total").setScale(2));
    sum.setCreditCount      (rs.getInt        ("credit_count"));
    sum.setCreditTotal      (rs.getBigDecimal ("credit_total").setScale(2));
    sum.setSaleRetAdjCount  (rs.getInt        ("sale_ret_adj_count"));
    sum.setSaleRetAdjTotal  (rs.getBigDecimal ("sale_ret_adj_total").setScale(2));
    sum.setCreditRetAdjCount(rs.getInt        ("credit_ret_adj_count"));
    sum.setCreditRetAdjTotal(rs.getBigDecimal ("credit_ret_adj_total").setScale(2));
    sum.setEntryCount       (rs.getInt        ("entry_count"));
    sum.setNetTotal         (rs.getBigDecimal ("net_total").setScale(2));
    sum.setTestFlag         (rs.getString     ("test_flag"));
    return sum;
  }

  /**
   * Takes a cutoff date time, a merch profile id and a summary id.  These 
   * are used to summarize all entries that have had the summary id assigned 
   * to it. A summary object is generated and returned, which should then be
   * inserted as a new record into ACHP_SF_SUMMARIES.
   */
  private FundingSummary generateFundingSummary(Date cutoff, String profileId, 
    long summaryId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " select " +
        "  summary_id,  " +
        "  sysdate summary_ts, " +
        "  ? cutoff_ts, " +
        "  merch_num,   " +
        "  profile_id,  " +
        "  ft_id,       " +
        "  s_cnt sale_count, " +
        "  s_amt sale_total, " +
        "  c_cnt credit_count, " +
        "  c_amt credit_total, " +
        "  sra_cnt sale_ret_adj_count, " +
        "  sra_amt sale_ret_adj_total, " +
        "  cra_cnt credit_ret_adj_count, " +
        "  cra_amt credit_ret_adj_total, " +
        "  (s_cnt + c_cnt + sra_cnt + cra_cnt) entry_count, " +
        "  (s_amt - c_amt - sra_amt + cra_amt) net_total, " +
        "  test_flag " +
        " from " +
        "  ( select " +
        "     summary_id, " +
        "     merch_num,  " +
        "     profile_id, " +
        "     ft_id,    " +
        "     sum(decode(entry_type,'SALE',1,0))          s_cnt, " +
        "     sum(decode(entry_type,'SALE',amount,0))     s_amt, " +
        "     sum(decode(entry_type,'CREDIT',1,0))        c_cnt, " +
        "     sum(decode(entry_type,'CREDIT',amount,0))   c_amt, " +
        "     sum(decode(entry_type,'SALE_RET_ADJ',1,0))        sra_cnt, " +
        "     sum(decode(entry_type,'SALE_RET_ADJ',amount,0))   sra_amt, " +
        "     sum(decode(entry_type,'CREDIT_RET_ADJ',1,0))      cra_cnt, " +
        "     sum(decode(entry_type,'CREDIT_RET_ADJ',amount,0)) cra_amt, " +
        "     test_flag " +
        "    from " +
        "     achp_sf_entries " +
        "    where " +
        "     summary_id = ?      " +
        "     and profile_id = ?  " +
        "     and entry_ts <= ?   " +
        "    group by summary_id, profile_id, merch_num, ft_id, test_flag ) ");

      ps.setTimestamp (1,toTimestamp(cutoff));
      ps.setLong      (2,summaryId);
      ps.setString    (3,profileId);
      ps.setTimestamp (4,toTimestamp(cutoff));

      rs = ps.executeQuery();
      if (rs.next())
      {
        return createFundingSummary(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("createFundingSummary(cutoff=" 
        + cutoff + ", profileId='" + profileId +"')",e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
    return null;
  }

  /**
   * Insert summary record into ACHP_SF_SUMMARIES.
   */
  private void insertFundingSummary(FundingSummary summary)
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into achp_sf_summaries ( " +
        "  summary_id,    " +
        "  summary_ts,    " +
        "  cutoff_ts,     " +
        "  merch_num,     " +
        "  profile_id,    " +
        "  ft_id,         " +
        "  sale_count,    " +
        "  sale_total,    " +
        "  credit_count,  " +
        "  credit_total,  " +
        "  sale_ret_adj_count,    " +
        "  sale_ret_adj_total,    " +
        "  credit_ret_adj_count,  " +
        "  credit_ret_adj_total,  " +
        "  entry_count,   " +
        "  net_total,     " +
        "  test_flag )    " +
        " values (  ?,    " +
        "  ?, ?, ?, ?,    " +
        "  ?, ?, ?, ?,    " +
        "  ?, ?, ?, ?,    " +
        "  ?, ?, ?, ? )   ");

      int col = 1;
      ps.setLong      (col++,summary.getSummaryId());
      ps.setTimestamp (col++,summary.getSummaryTs());
      ps.setTimestamp (col++,summary.getCutoffTs());
      ps.setString    (col++,summary.getMerchNum());
      ps.setString    (col++,summary.getProfileId());
      ps.setLong      (col++,summary.getFtId());
      ps.setInt       (col++,summary.getSaleCount());
      ps.setBigDecimal(col++,summary.getSaleTotal());
      ps.setInt       (col++,summary.getCreditCount());
      ps.setBigDecimal(col++,summary.getCreditTotal());
      ps.setInt       (col++,summary.getSaleRetAdjCount());
      ps.setBigDecimal(col++,summary.getSaleRetAdjTotal());
      ps.setInt       (col++,summary.getCreditRetAdjCount());
      ps.setBigDecimal(col++,summary.getCreditRetAdjTotal());
      ps.setInt       (col++,summary.getEntryCount());
      ps.setBigDecimal(col++,summary.getNetTotal());
      ps.setString    (col++,summary.getTestFlag());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertFundingSummary(profileId=" 
        + summary.getProfileId() + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  private boolean isNegative(BigDecimal dec)
  {
    return dec.compareTo(new BigDecimal(0)) < 0;
  }

  private boolean isZero(BigDecimal dec)
  {
    return dec.compareTo(new BigDecimal(0)) == 0;
  }

  private boolean isPositive(BigDecimal dec)
  {
    return dec.compareTo(new BigDecimal(0)) > 0;
  }

  /**
   * Summarize a profile batch.  Used by test batch processing.
   */
  public void summarizeProfileBatch(Date cutoff, String profileId, 
    int sysTypeOpt, long batchId)
  {
    boolean opSuccess = false;

    try
    {
      startTransaction();

      // assign summary id to entries
      long summaryId = 
        assignSummaryIdToEntries(cutoff,profileId,sysTypeOpt,batchId);

      // if no summary id was generated there was no activity to summarize
      if (summaryId == -1L)
      {
        log.info("No summary activity found.");
        return;
      }

      // generate summary data
      FundingSummary sum = 
        generateFundingSummary(cutoff,profileId,summaryId);

      // insert the summary record
      insertFundingSummary(sum);

      // generate ach file record for non-zero summaries
      if (!isZero(sum.getNetTotal()))
      {
        // determine if this is a deposit or debit
        boolean isDeposit = isPositive(sum.getNetTotal());

        // generate a file record from the summary record + profile
        MerchProfile pro = getMerchProfile(profileId);
        AchFileDetail detail = new AchFileDetail();
        CreditDebitCode cdCode = 
          isDeposit ? CreditDebitCode.CREDIT : CreditDebitCode.DEBIT;
        String entryDesc = 
          getEntryDescription(isDeposit ? EDT_DEPOSIT : EDT_DEBIT);
        detail.setMerchNum(sum.getMerchNum());
        detail.setProfileId(sum.getProfileId());
        detail.setFtId(sum.getFtId());
        detail.setCdCode(cdCode);
        detail.setAmount(sum.getNetTotal().abs());
        detail.setAccount(pro.getAccount());
        detail.setAccountId(pro.getMerchNum());
        detail.setAccountName(pro.getMerchName());
        detail.setDestination(Destination.MERCHANT);
        detail.setSourceId(sum.getSummaryId());
        detail.setSecCode(pro.getSfSecCode());
        detail.setEntryDescription(entryDesc);
        detail.setTestFlag(sum.getTestFlag());
        insertAchFileDetail(detail);
      }

      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("summarizeProfile(cutoff=" + cutoff
        + ", profileId='" + profileId + "')",e);
    }
    finally
    {
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * Returns a summary ID if a summary record exists for a given profile
   * id and system type (test/production) that has not already been
   * associated with an ach detail record (and thus settled to the merchant
   * account).  Returns -1 if none found.
   */
  private long getSummaryIdForProfile(String profileId, int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                    " +
        "  s.summary_id             " +
        " from                      " +
        "  achp_sf_summaries s,     " +
        "  achp_file_details d      " +
        " where                     " +
        "  s.profile_id = '" + profileId + "'       " +
        "  and s.test_flag = '" + testFlagVal + "'  " +
        "  and s.summary_id = d.source_id(+)        " +
        "  and d.detail_id is null  ");
      return rs.next() ? rs.getLong(1) : -1L;
    }
    catch (Exception e)
    {
      throw new RuntimeException("profileSummaryExists(profileId=" 
        + profileId + ",testFlag=" + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Generates a new summary record and assigns entries to it.
   */
  private void insertProfileSummary(String profileId, int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    boolean opSuccess = false;

    try
    {
      startTransaction();

      // assign new summary id to entries
      long summaryId = getNewId();
      s = con.createStatement();
      s.executeUpdate(
        " update achp_sf_entries                  " +
        " set summary_id = " + summaryId + "      " +
        " where                                   " +
        "  profile_id = '" + profileId + "'       " +
        "  and test_flag = '" + testFlagVal + "'  " +
        "  and summary_id is null                 ");

      System.out.println("testFlagVal = " + testFlagVal + ", summaryId = " + summaryId);
      log.debug("testFlagVal = " + testFlagVal + ", summaryId = " + summaryId);

      // create new summary for entries
      s.executeUpdate(
        " insert into achp_sf_summaries ( " +
        "   summary_id,           " +
        "   summary_ts,           " +
        "   merch_num,            " +
        "   profile_id,           " +
        "   ft_id,                " +
        "   sale_count,           " +
        "   sale_total,           " +
        "   credit_count,         " +
        "   credit_total,         " +
        "   sale_ret_adj_count,   " +
        "   sale_ret_adj_total,   " +
        "   credit_ret_adj_count, " +
        "   credit_ret_adj_total, " +
        "   entry_count,          " +
        "   net_total,            " +
        "   test_flag )           " +
        "  select                 " +
        "   summary_id,           " +
        "   sysdate summary_ts,   " +
        "   merch_num,            " +
        "   profile_id,           " +
        "   ft_id,                " +
        "   s_cnt sale_count,     " +
        "   s_amt sale_total,     " +
        "   c_cnt credit_count,   " +
        "   c_amt credit_total,   " +
        "   sra_cnt sale_ret_adj_count,    " +
        "   sra_amt sale_ret_adj_total,    " +
        "   cra_cnt credit_ret_adj_count,  " +
        "   cra_amt credit_ret_adj_total,  " +
        "   (s_cnt + c_cnt + sra_cnt + cra_cnt) entry_count, " +
        "   (s_amt - c_amt - sra_amt + cra_amt) net_total,   " +
        "   test_flag      " +
        "  from            " +
        "   ( select       " +
        "      summary_id, " +
        "      merch_num,  " +
        "      profile_id, " +
        "      ft_id,      " +
        "      sum(decode(entry_type,'SALE',1,0))          s_cnt, " +
        "      sum(decode(entry_type,'SALE',amount,0))     s_amt, " +
        "      sum(decode(entry_type,'CREDIT',1,0))        c_cnt, " +
        "      sum(decode(entry_type,'CREDIT',amount,0))   c_amt, " +
        "      sum(decode(entry_type,'SALE_RET_ADJ',1,0))        sra_cnt, " +
        "      sum(decode(entry_type,'SALE_RET_ADJ',amount,0))   sra_amt, " +
        "      sum(decode(entry_type,'CREDIT_RET_ADJ',1,0))      cra_cnt, " +
        "      sum(decode(entry_type,'CREDIT_RET_ADJ',amount,0)) cra_amt, " +
        "      test_flag       " +
        "     from             " +
        "      achp_sf_entries " +
        "     where            " +
        "      summary_id = " + summaryId + "        " +
        "      and profile_id = '" + profileId + "'  " +
        "      and test_flag = '" + testFlagVal + "' " +
        "     group by summary_id, profile_id, merch_num, ft_id, test_flag ) ");

      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertProfileSummary(profileId=" + profileId
        + ", testFlag=" + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * Update existing summary record with new summary entries.
   */
  private void updateProfileSummary(String profileId, long summaryId, 
    int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    boolean opSuccess = false;

    try
    {
      startTransaction();

      // assign temporary id to entries
      long tempId = getNewId();
      s = con.createStatement();
      s.executeUpdate(
        " update achp_sf_entries " +
        " set summary_id = " + tempId + " " +
        " where " +
        "  profile_id = '" + profileId + "' " +
        "  and test_flag = '" + testFlagVal + "' " +
        "  and summary_id is null ");

      // add new entry summary data to existing summary record
      s.executeUpdate(
        " update achp_sf_summaries s set  " +
        "  summary_ts = sysdate,        " +
        "  ( sale_count, sale_total, credit_count, credit_total,  " +
        "    sale_ret_adj_count, sale_ret_adj_total,              " +
        "    credit_ret_adj_count, credit_ret_adj_total,          " +
        "    entry_count, net_total ) =                           " +
        "  ( select                                               " +
        "     sale_count + s_cnt,                                 " +
        "     sale_total + s_amt,                                 " +
        "     credit_count + c_cnt,                               " +
        "     credit_total + c_amt,                               " +
        "     sale_ret_adj_count + sra_cnt,                       " +
        "     sale_ret_adj_total + sra_amt,                       " +
        "     credit_ret_adj_count + cra_cnt,                     " +
        "     credit_ret_adj_total + cra_amt,                     " +
        "     entry_count + s_cnt + c_cnt + sra_cnt + cra_cnt,    " +
        "     net_total + ( s_amt - c_amt - sra_amt + cra_amt )   " +
        "    from                                                 " +
        "     ( select                                            " +
        "        sum(decode(entry_type,'SALE',1,0))                 s_cnt,    " +
        "        sum(decode(entry_type,'SALE',amount,0))            s_amt,    " +
        "        sum(decode(entry_type,'CREDIT',1,0))               c_cnt,    " +
        "        sum(decode(entry_type,'CREDIT',amount,0))          c_amt,    " +
        "        sum(decode(entry_type,'SALE_RET_ADJ',1,0))         sra_cnt,  " +
        "        sum(decode(entry_type,'SALE_RET_ADJ',amount,0))    sra_amt,  " +
        "        sum(decode(entry_type,'CREDIT_RET_ADJ',1,0))       cra_cnt,  " +
        "        sum(decode(entry_type,'CREDIT_RET_ADJ',amount,0))  cra_amt   " +
        "       from                                        " +
        "        achp_sf_entries                            " +
        "       where                                       " +
        "        summary_id = " + tempId + "                " +
        "        and profile_id = '" + profileId + "'       " +
        "        and test_flag = '" + testFlagVal + "'  ) ) " +
        " where summary_id = " + summaryId);

      // assign existing summary id to entries
      s.executeUpdate(
        " update achp_sf_entries " +
        " set summary_id = " + summaryId +
        " where summary_id = " + tempId);

      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("updateProfileSummary(profileId=" + profileId
        + ", summaryId=" + summaryId + ", testFlag=" + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * Updates existing summary record with new unsummarized entries, or 
   * creates a new summary record for unsummarized entries.
   */
  public void summarizeProfile(String profileId, int sysTypeOpt)
  {
    long summaryId = getSummaryIdForProfile(profileId,sysTypeOpt);
    if (summaryId > 0)
    {
      updateProfileSummary(profileId,summaryId,sysTypeOpt);
    }
    else
    {
      insertProfileSummary(profileId,sysTypeOpt);
    }
  }

  /**
   * Daily generation of ACH file details for merchant settlement funding
   * deposits determined by settlement funding summary records.
   */
  public void generateSummaryDetails(int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    boolean opSuccess = false;

    try
    {
      // fetch possible entry descriptions
      String edtDeposit = "'" + getEntryDescription(EDT_DEPOSIT) + "'";
      String edtDebit   = "'" + getEntryDescription(EDT_DEBIT) + "'";

      startTransaction();
      s = con.createStatement();

      // set the cutoff timestamp in the summary records
      s.executeUpdate(
        " update achp_sf_summaries              " +
        " set cutoff_ts = sysdate               " +
        " where                                 " +
        "  cutoff_ts is null                    " +
        "  and test_flag = '" + testFlagVal + "'");

      // create the detail records for the summaries
      // only for non-zero summaries
      s.executeUpdate(
        " insert into achp_file_details (           " +
        "  ft_id,                                   " +
        "  merch_num,                               " +
        "  profile_id,                              " +
        "  destination,                             " +
        "  source_id,                               " +
        "  sec_code,                                " +
        "  entry_description,                       " +
        "  transit_num,                             " +
        "  account_num,                             " +
        "  account_num_enc,                         " +
        "  account_type,                            " +
        "  account_id,                              " +
        "  account_name,                            " +
        "  cd_code,                                 " +
        "  amount,                                  " +
        "  recur_code,                              " +
        "  test_flag )                              " +
        " select                                    " +
        "  p.ft_id,                                 " +
        "  p.merch_num,                             " +
        "  p.profile_id,                            " +
        "  '" + Destination.MERCHANT + "',          " +
        "  s.summary_id,                            " +
        "  p.sf_sec_code,                           " +
        "  case                                     " +
        "    when s.net_total < 0 then              " +
        "    " + edtDebit + "                       " +
        "    else                                   " +
        "    " + edtDeposit + " end,                " +
        "  p.transit_num,                           " +
        "  p.account_num,                           " +
        "  p.account_num_enc,                       " +
        "  p.account_type,                          " +
        "  p.merch_num,                             " +
        "  substr(p.merch_name,0,22),               " +
        "  case                                     " +
        "    when s.net_total < 0 then              " + 
        "    '" + CreditDebitCode.DEBIT + "'        " + 
        "    else                                   " +
        "    '" + CreditDebitCode.CREDIT + "' end,  " +
        "  abs(s.net_total),                        " +
        "  'S',                                     " +
        "  s.test_flag                              " +
        " from                                      " +
        "  achp_profiles p,                         " +
        "  achp_sf_summaries s,                     " +
        "  achp_file_details d                      " +
        " where                                     " +
        "  s.summary_id = d.source_id (+)           " +
        "  and d.detail_id is null                  " +
        "  and s.profile_id = p.profile_id          " +
        "  and s.test_flag = '" + testFlagVal + "'  " +
        "  and s.net_total <> 0                     ");

        opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("generateSummaryDetails(testFlag=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * ACH File Initialization
   *
   * These methods are part of a file initialization transaction that
   * identifies unprocessed detail records in ACHP_FILE_DETAILS and
   * collects them into batches and files.  These associations are stored 
   * in ACHP_FILE_BATCHES and ACHP_FILES along with field values that will
   * be used to generate the nacha file format (see ACH File Generation 
   * below).  Entry point is intiateAchFiles().
   */

  /**
   * Flag all eligible details for processing.  Details must match the
   * system type (test/production) and be timestamped before the cutoff
   * timestamp.  A temporary flag id is assigned to each details batch
   * id and returned.
   */
  private long flagAchDetails(Timestamp cutoffTs, Destination destination,
    int sysTypeOpt)
  {
    PreparedStatement ps = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();

      // HACK: use the cutoff date but adjust forward 30 minutes to account for
      // time differences between database and timed event server, as well as
      // for ach details generated after the start of end of day processing
      Timestamp adjustedCutoffTs = new Timestamp(cutoffTs.getTime() + 1800000L);
      long flagId = getNewId();
      ps = con.prepareStatement(
        " update achp_file_details set              " +
        "  batch_id = " + flagId + "                " +
        " where                                     " +
        "  batch_id is null                         " +
        "  and test_flag = '" + testFlagVal + "'    " +
        "  and destination = '" + destination + "'  " +
        "  and create_ts <= ?                       ");
      ps.setTimestamp (1,adjustedCutoffTs);
      ps.executeUpdate();
      return flagId;
    }
    catch (Exception e)
    {
      throw new RuntimeException("flagAchDetails(cutoffTs=" + cutoffTs 
        + ",testFlagVal=" + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }  
  }

  /**
   * customer file batch selector: selects batch row data for customer 
   * destination files grouping details by profile_id and using the profile 
   * company name of the merchant for the batch company name.  This should
   * cause the merchant name to appear on customer statements.
   */
  private String custSel =
    " select " +
    "  d.profile_id, " +
    "  d.ft_id, " +
    "  d.destination, " +
    "  d.test_flag, " +
    "  case " +
    "   when d.debit_count = 0 then 220 " +
    "   when d.credit_count = 0 then 225 " +
    "   else 200 " +
    "  end class_code, " +
    "  p.company_name, " +
    "  t.company_id, " +
    "  d.sec_code, " +
    "  d.entry_description, " +
    "  to_char(cur_date,'MMDDYY') descriptive_date, " +
    "  to_char(cur_date,'YYMMDD') effective_entry_date, " +
    "  substr(t.destination_id,1,8) origin_dfi_id, " +
    "  d.detail_count, " +
    "  d.flag_id " +
    " from " +
    "  achp_file_types t, " +
    "  achp_profiles p, " +
    "  ( select " +
    "     batch_id flag_id, " +
    "     sysdate cur_date, " +
    "     profile_id, " +
    "     ft_id, " +
    "     destination, " +
    "     test_flag, " +
    "     sec_code, " +
    "     entry_description, " +
    "     sum(decode(cd_code,'D',1,0)) debit_count, " +
    "     sum(decode(cd_code,'C',1,0)) credit_count, " +
    "     sum(1) detail_count " +
    "    from " +
    "     achp_file_details " +
    "    where " +
    "     destination = 'CUST' " +
    "     and batch_id = ? " +
    "     and test_flag = ? " +
    "    group by " +
    "     batch_id, " +
    "     profile_id, " +
    "     ft_id, " +
    "     destination, " +
    "     sec_code, " +
    "     entry_description, " +
    "     test_flag ) d " +
    " where " +
    "  d.ft_id = t.ft_id " +
    "  and d.profile_id = p.profile_id ";

  /**
   * merchant file batch selector: selects batch row data for merchant 
   * destination files ignoring profile id and grouping primarily
   * by sec code and using the file type company name ('MERCHE-SOLUTIONS') as
   * the batch company name. This should cause MES to appear on merchant
   * statements.
   */
  private String merchSel =
    " select " +
    "  null profile_id, " +
    "  d.ft_id, " +
    "  d.destination, " +
    "  d.test_flag, " +
    "  case " +
    "   when d.debit_count = 0 then 220 " +
    "   when d.credit_count = 0 then 225 " +
    "   else 200 " +
    "  end class_code, " +
    "  t.company_name, " +
    "  t.company_id, " +
    "  d.sec_code, " +
    "  d.entry_description, " +
    "  to_char(cur_date,'MMDDYY') descriptive_date, " +
    "  to_char(cur_date,'YYMMDD') effective_entry_date, " +
    "  substr(t.destination_id,1,8) origin_dfi_id, " +
    "  d.detail_count, " +
    "  d.flag_id " +
    " from " +
    "  achp_file_types t, " +
    "  ( select " +
    "     batch_id flag_id, " +
    "     sysdate cur_date, " +
    "     ft_id, " +
    "     destination, " +
    "     test_flag, " +
    "     sec_code, " +
    "     entry_description, " +
    "     sum(decode(cd_code,'D',1,0)) debit_count, " +
    "     sum(decode(cd_code,'C',1,0)) credit_count, " +
    "     sum(1) detail_count " +
    "    from " +
    "     achp_file_details " +
    "    where " +
    "     destination = 'MERCH' " +
    "     and batch_id = ? " +
    "     and test_flag = ? " +
    "    group by " +
    "     batch_id, " +
    "     ft_id, " +
    "     destination, " +
    "     sec_code, " +
    "     entry_description, " +
    "     test_flag ) d " +
    " where " +
    "  d.ft_id = t.ft_id ";

  /**
   * Inserts new batch records for groups of unassigned details in 
   * ACHP_FILE_DETAILS.  The details must match the given destination,
   * the test/production indicator and have the flagId assigned.
   */

  private void createAchBatches(Destination destination, long flagId, 
    int sysTypeOpt)
  {
    PreparedStatement ps = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      // select query type by destination
      String detailSel 
        = Destination.CUSTOMER.equals(destination) ? custSel : merchSel;

      connect();

      ps = con.prepareStatement(
        " insert into achp_file_batches ( " +
        "  profile_id,                    " +
        "  ft_id,                         " +
        "  destination,                   " +
        "  test_flag,                     " +
        "  class_code,                    " +
        "  company_name,                  " +
        "  company_id,                    " +
        "  sec_code,                      " +
        "  entry_description,             " +
        "  descriptive_date,              " +
        "  effective_entry_date,          " +
        "  origin_dfi_id,                 " +
        "  detail_count,                  " +
        "  file_id )                      " +
        " ( " + detailSel + " )           ");
      ps.setLong      (1,flagId);
      ps.setString    (2,testFlagVal);
      log.debug("generated batches: " + ps.executeUpdate());
    }
    catch (Exception e)
    {
      throw new RuntimeException("createAchBatches(destination=" 
        + destination + ",flagId=" + flagId + ",testFlagVal=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }  
  }

  /**
   * Assigns details to newly created batches.
   */
  private void assignAchDetailsToBatches(Destination destination, 
    long flagId, int sysTypeOpt)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();

      s = con.createStatement();
      int count = s.executeUpdate(
        " update achp_file_details d                  " +
        " set d.batch_id = nvl( (                     " +
        "  select                                     " +
        "   b.batch_id                                " +
        "  from                                       " +
        "   achp_file_batches b                       " +
        "  where                                      " +
        "   d.destination = '" + destination + "'     " +
        "   and d.test_flag = '" + testFlagVal + "'   " +
        "   and d.batch_id = " + flagId + "           " +
        "   and ( ( b.destination = 'MERCH'           " +
        "           and b.profile_id is null )        " +
        "         or d.profile_id = b.profile_id )    " +
        "   and d.ft_id = b.ft_id                     " +
        "   and d.destination = b.destination         " +
        "   and d.sec_code = b.sec_code               " +
        "   and d.entry_description                   " +
        "     = b.entry_description                   " +
        "   and b.file_id = " + flagId + "            " +
        "   and d.test_flag = b.test_flag ),          " + 
        "   " + flagId + " )                          " +
        " where                                       " +
        "  d.batch_id = " + flagId + "                ");
      log.debug("assigned " + count + " details to batches for '" + destination + "', flag id " + flagId + ", test=" + testFlagVal);
    }
    catch (Exception e)
    {
      throw new RuntimeException("assignAchDetailsToBatches(destination=" 
        + destination + ",flagId=" + flagId + ",testFlagVal=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }  
  }

  /**
   * Determine distinct file types of currently unassigned batches
   * with the given destination and test status.
   */
  private List getUnassignedBatchFileTypes(Destination destination,
    long flagId, int sysTypeOpt)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select unique(ft_id) ft_id                " +
        " from achp_file_batches                    " +
        " where                                     " +
        "  file_id = " + flagId + "                 " +
        "  and destination = '" + destination + "'  " +
        "  and test_flag = '" + testFlagVal + "'    ");
      List ftIds = new ArrayList();
      while (rs.next())
      {
        ftIds.add(rs.getLong("ft_id"));
      }
      return ftIds;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getUnassignedBatchFileTypes(destination="
        + destination + ",testFlagVal=" + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Assigns a new file ID to unassigned batches in ACH_FILE_BATCHES.
   * Returns the number of batches assigned to a file, or zero if none
   * found.  Batches will be assigned until the detail count limit is hit, 
   * but at least one will be assigned if available, even if exceeding
   * the limit.  This is intended to limit the overall file size.
   *
   * This also assigns the batch sequence number.
   */
  private int assignAchBatchToFile(long fileId, long ftId, long flagId,
    Destination destination, int sysTypeOpt)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      int limit = getAchFileDetailLimit();
      connect();
      s = con.createStatement();
      return s.executeUpdate(
        " update achp_file_batches set    " +
        "  file_id = " + fileId + ",      " +
        "  batch_num = rownum             " +
        " where batch_id in (             " +
        "  select                         " +
        "   batch_id                      " +
        "  from                           " +
        "   ( select                      " +
        "      batch_id,                  " +
        "      sum(1) over                " +
        "        (order by batch_id)      " + 
        "          batch_count,           " +
        "      sum(detail_count) over     " +
        "        (order by batch_id)      " +
        "          detail_total           " +
        "     from                        " +
        "      achp_file_batches          " +
        "     where                       " +
        "      destination =              " +
        "        '" + destination + "'    " +
        "      and ft_id = " + ftId + "   " +
        "      and test_flag =            " +
        "        '" + testFlagVal + "'    " +
        "      and file_id = " + flagId + " ) " +
        "  where                          " +
        "   detail_total <= " + limit + " " +
        "   or batch_count = 1 )          ");
    }
    catch (Exception e)
    {
      throw new RuntimeException("assignAchBatchToFile(fileId=" + fileId 
        + ", ftId=" + ftId + ", destination=" + destination + ", test=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Assigns details in ACHP_FILE_DETAILS to a file ID.  The batches that
   * were assigned to the file (see above) are used to fetch the details
   * to assign.
   */
  private void assignAchDetailsToFile(long fileId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      s.executeUpdate(
        " update achp_file_details      " +
        " set file_id = " + fileId + "  " +
        " where                         " +
        "  batch_id in (                " +
        "   select batch_id             " +
        "   from achp_file_batches      " +
        "   where                       " +
        "    file_id = " + fileId + " ) ");
    }
    catch (Exception e)
    {
      throw new RuntimeException("assignAchDetailsToFile(fileId=" 
        + fileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public String getNextAchFileSequence(Date date, int sysTypeOpt, 
    Destination destination, long ftId)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";
    String expStr = 
      " decode( '" + testFlagVal + "', " +
      "        'Y', 'T' || to_char( daily_count + 1, 'FM000' ), " +
      "        to_char( daily_count + 1, 'FM000' ) ) ";
    String dateStr = formatDate(date,FMT_ORA_DATE);

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select " + expStr + "                               " +
        " from                                                " +
        "  ( select nvl(count(*),0) daily_count               " +
        "    from achp_files                                  " +
        "    where                                            " +
        "     trunc(create_ts) = " + dateStr + "              " +
        "     and test_flag = '" + testFlagVal + "'           " +
        "     and ft_id = " + ftId + "                        " +
        "     and destination = '" + destination + "' )       ");
      if (rs.next())
      {
        return rs.getString(1);
      }
      return "xxx";
    }
    catch (Exception e)
    {
      throw new RuntimeException("getNextAchFileSequence(date=" 
        + date + "): ",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Merchant file names: pach394x_MMDDYY_[T]SEQ.dat
   * Customer file names: nach394x_MMDDYY_[T]SEQ.dat
   */
  public String createAchFileName(Date date, int sysTypeOpt, long ftId,
    Destination destination)
  {
    StringBuffer buf = new StringBuffer();
    buf.append(Destination.CUSTOMER.equals(destination) ? "n" : "p");
    buf.append("ach");
    AchFileType ft = getAchFileType(ftId);
    buf.append(ft.getXferParameters().prefix);
    buf.append("00000_");
    String dateStr = formatDate(date,FMT_FILE_DATE);
    buf.append(dateStr + "_");
    buf.append(getNextAchFileSequence(date,sysTypeOpt,destination,ftId));
    buf.append(".dat");
    return ""+buf;

    //return "MCHNTES.NACHA.Nacha."
    //  + (checkSysTypeOption(sysTypeOpt) ? "T" : "") + fileId + ".txt.asc";
  }

  /**
   * Creates a new ACHP_FILE record with the given file ID.
   */
  private void createAchFile(long fileId, long ftId, Destination destination, 
    int sysTypeOpt)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      Date curDate = Calendar.getInstance().getTime();
      String fileName 
        = createAchFileName(curDate,sysTypeOpt,ftId,destination);

      connect();
      s = con.createStatement();
      s.executeUpdate(
        " insert into achp_files (  " +
        "  file_id,                 " +
        "  ft_id,                   " +
        "  destination,             " +
        "  file_name,               " +
        "  test_flag,               " +
        "  destination_id,          " +
        "  destination_name,        " +
        "  origin_id,               " +
        "  origin_name )            " +
        " select                    " +
        "  " + fileId + ",          " +
        "  ft_id,                   " +
        "  '" + destination + "',   " +
        "  '" + fileName + "',      " +
        "  '" + testFlagVal + "',   " +
        "  destination_id,          " +
        "  destination_name,        " +
        "  origin_id,               " +
        "  origin_name              " +
        " from                      " +
        "  achp_file_types          " +
        " where                     " +
        "  ft_id = " + ftId + "     ");
    }
    catch (Exception e)
    {
      throw new RuntimeException("createAchFile(fileId=" + fileId
        + ",ftId=" + ftId + ",destination=" + destination + ",testFlagVal="
        + testFlagVal + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Creates records in ACHP_FILES for unassigned batches and details
   * and assigns them to files.
   */
  private void createAchFiles(Destination destination, long flagId, 
    int sysTypeOpt)
  {
    try
    {

      boolean batchesFound;
      do
      {
        batchesFound = false;
        List ftIds 
          = getUnassignedBatchFileTypes(destination,flagId,sysTypeOpt);
        for (Iterator i = ftIds.iterator(); i.hasNext();)
        {
          long ftId = ((Long)i.next()).longValue();
          long fileId = getNewId();
          int batchCount = 
            assignAchBatchToFile(fileId,ftId,flagId,destination,sysTypeOpt);
          if (batchCount > 0)
          {
            assignAchDetailsToFile(fileId);
            createAchFile(fileId,ftId,destination,sysTypeOpt);
            batchesFound = true;
          }
        }
      }
      while (batchesFound);
    }
    catch (Exception e)
    {
      throw new RuntimeException("createAchFiles(sysTypeOpt=" + sysTypeOpt
        + ")",e);
    }
  }

  /**
   * This is the main entry point to run a transaction that generates batch and 
   * file records for untransmitted ach detail records in ACHP_FILE_DETAILS. 
   * After it has executed all eligible details should have batches and file(s) 
   * associated with them (created during the process).  This is the first step
   * to be taken before generating the nacha formatted ach files and uploading 
   * it to the database as data contained in ACHP_FILES records.
   */
  public void initiateAchFiles(Destination destination, Date cutoffDate,
    int sysTypeOpt)
  {
    boolean opSuccess = false;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {   
      startTransaction();
      Timestamp cutoffTs = toTimestamp(cutoffDate);
      long flagId = flagAchDetails(cutoffTs,destination,sysTypeOpt);
      createAchBatches(destination,flagId,sysTypeOpt);
      assignAchDetailsToBatches(destination,flagId,sysTypeOpt);
      createAchFiles(destination,flagId,sysTypeOpt);
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("intitiateAchFiles(destination=" 
        + destination + ",cutoffDate=" + cutoffDate + ",sysTypeOpt=" 
        + sysTypeOpt + ")",e);
    }
    finally
    {
      //commitTransaction();
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }

  /**
   * ACH File Generation
   *
   * After initialization (see above), these methods support the AchFileGenerator
   * process which uses the file, batch and detail records in ACHP_FILE* tables 
   * to generate a nacha formatted ACH file.
   */

  private Statement fetchStmt;
  private ResultSet fetchRs;

  private void endFetch()
  {
    if (fetchRs != null)
    {
      try { fetchRs.close(); } catch (Exception e) { }
    }
    if (fetchStmt != null)
    {
      try { fetchStmt.close(); } catch (Exception e) { }
      fetchStmt = null;
    }
  }

  public void endFileDetailFetch()
  {
    endFetch();
  }

  /**
   * (transaction)
   */
  public void startFileDetailFetch(long batchId)
  {
    try
    {
      fetchStmt = con.createStatement();
      fetchRs = fetchStmt.executeQuery(
        " select " +
        "  detail_id,         " +
        "  create_ts,         " +
        "  merch_num,         " +
        "  profile_id,        " +
        "  ft_id,             " +
        "  transit_num,       " +
        "  dukpt_decrypt_wrapper( " +
        "   account_num_enc)  " +
        "    account_num,     " +
        "  account_type,      " +
        "  account_id,        " +
        "  account_name,      " +
        "  cd_code,           " +
        "  amount,            " +
        "  destination,       " +
        "  source_id,         " +
        "  sec_code,          " +
        "  entry_description, " +
        "  test_flag,         " +
        "  hold_flag,         " +
        "  file_id,           " +
        "  trace_num,         " +
        "  recur_code,        " +
        "  batch_id           " +
        " from achp_file_details " +
        " where batch_id = " + batchId + " " +
        " order by profile_id, sec_code ");
    }
    catch (Exception e)
    {
      throw new RuntimeException("startFileDetailFetch(batchId=" + batchId + ")",e);
    }
  }

  public AchFileDetail fetchNextFileDetail()
  {
    try
    {
      if (fetchRs.next())
      {
        return createAchFileDetail(fetchRs);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("fetchNextFileDetail()",e);
    }
  }

  private AchFile createAchFile(ResultSet rs) throws Exception
  {
    AchFile file = new AchFile();
    file.setFileId          (rs.getLong     ("file_id"));
    file.setCreateTs        (rs.getTimestamp("create_ts"));
    file.setFtId            (rs.getLong     ("ft_id"));
    file.setDestination (
      Destination.destinationFor(rs.getString("destination")));
    file.setDestinationId   (rs.getString   ("destination_id"));
    file.setDestinationName (rs.getString   ("destination_name"));
    file.setOriginId        (rs.getString   ("origin_id"));
    file.setOriginName      (rs.getString   ("origin_name"));
    file.setTestFlag        (rs.getString   ("test_flag"));
    file.setUploadTs        (rs.getTimestamp("upload_ts"));
    file.setFileName        (rs.getString   ("file_name"));
    file.setFileSize        (rs.getInt      ("file_size"));
    file.setTransmitTs      (rs.getTimestamp("transmit_ts"));
    return file;
  }

  /** 
   * Fetches records from ACHP_FILES that have been initialized but
   * not generated.
   *
   * (transaction)
   */
  public List getInitializedAchFiles(int sysTypeOpt)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_files " +
        " where " +
        "  upload_ts is null " +
        "  and test_flag = '" + testFlagVal + "' ");
      List files = new ArrayList();
      while (rs.next())
      {
        files.add(createAchFile(rs));
      }
      return files;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getInitializedAchFiles(testFlagVal=" 
        + testFlagVal + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  private AchFileBatch createAchFileBatch(ResultSet rs) throws Exception
  {
    AchFileBatch batch = new AchFileBatch();
    batch.setBatchId    (rs.getLong     ("batch_id"));
    batch.setCreateTs   (rs.getTimestamp("create_ts"));
    batch.setFileId     (rs.getLong     ("file_id"));
    batch.setTestFlag   (rs.getString   ("test_flag"));
    batch.setFtId       (rs.getLong     ("ft_id"));
    batch.setDestination
      (Destination.destinationFor(rs.getString("destination")));
    batch.setSecCode    (SecCode.codeFor(rs.getString("sec_code")));
    batch.setClassCode  (rs.getInt      ("class_code"));
    batch.setCompanyName(rs.getString   ("company_name"));
    batch.setCompanyId  (rs.getString   ("company_id"));
    batch.setDiscData   (rs.getString   ("disc_data"));
    batch.setEntryDescription
                        (rs.getString   ("entry_description"));
    batch.setDescriptiveDate
                        (rs.getString   ("descriptive_date"));
    batch.setEffectiveEntryDate
                        (rs.getString   ("effective_entry_date"));
    batch.setOriginDfiId(rs.getString   ("origin_dfi_id"));
    batch.setBatchNum   (rs.getInt      ("batch_num"));
    batch.setDetailCount(rs.getInt      ("detail_count"));
    return batch;
  }
  
  /**
   * Returns a list of all AchFileBatch records in ACHP_FILE_BATCHES with
   * the given fileId.  Orders by batch_num.
   *
   * (transaction)
   */
  public List getAchBatches(long fileId)
  {
    Statement s = null;

    try
    {
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_file_batches " +
        " where file_id = " + fileId + " " +
        " order by batch_num ");
      List batches = new ArrayList();
      while (rs.next())
      {
        batches.add(createAchFileBatch(rs));
      }
      return batches;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchBatches(fileId=" + fileId + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }
  
  /**
   * Uploads file data to an ACHP_FILES record.  The file size is set.
   *
   * (transaction)
   */
  public void uploadAchFileData(AchFile file, InputStream in) 
  {
    Statement s = null;
    boolean opSuccess = false;

    try
    {
      if (in == null || in.available() == 0)
      {
        throw new RuntimeException("InputStream invalid");
      }

      // clear file data from file record
      s = con.createStatement();
      s.executeUpdate(
        " update achp_files " +
        " set file_data = empty_blob() " +
        " where file_id = " + file.getFileId());
      
      // fetch output stream to write file data blob,
      // upload data from input stream to it
      ResultSet rs = s.executeQuery(
        " select file_data from achp_files " +
        " where file_id = " + file.getFileId() +
        " for update ");
      rs.next();
      Blob b = rs.getBlob(1);
      OutputStream out = b.setBinaryStream(1L);
      int size = 0;
      try
      {
        // using 10K buffer for now
        byte[] data = new byte[10000];
        int len = 0;
        while ((len = in.read(data)) != -1)
        {
          out.write(data,0,len);
          size += len;
        }
      }
      finally
      {
        try { out.flush(); out.close(); } catch (Exception e) { }
      }

      // update the file size, set the upload timestamp
      s.executeUpdate(
        " update achp_files set " +
        "  upload_ts = sysdate, " +
        "  file_size = " + size + 
        " where file_id = " + file.getFileId());

    }
    catch (Exception e)
    {
      throw new RuntimeException("uploadAchFileData(fileId="  
        + file.getFileId() + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  public AchFile getAchFile(long fileId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_files where file_id = " + fileId);
      if (rs.next())
      {
        return createAchFile(rs);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchFileData(fileId="  + fileId + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public void prepareFileForResend(AchFile file)
  {
    Statement s = null;

    try
    {
      connect();
      
      // store some ids
      long origFileId = file.getFileId();
      long copyFileId = getNewId();
      long tempFileId = getNewId();
      
      // generate new file
      int sysTypeOpt = file.isTest() ? SYS_TEST : SYS_PROD;
      createAchFile(tempFileId,file.getFtId(),file.getDestination(),
        sysTypeOpt);
        
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select file_name from achp_files where file_id = " + tempFileId);
      if (!rs.next()) throw new RuntimeException(
        "Failed to create new file with temp id " + tempFileId);
      String copyFileName = rs.getString(1);
      log.debug("Created new file named " + copyFileName);
      
      // move orig file to copy file id
      s.executeUpdate(
        " update achp_files set file_id = " + copyFileId +
        " where file_id = " + origFileId);
      log.debug("Original file " + origFileId + " moved to " + copyFileId);
      
      // move new file to orig file id
      s.executeUpdate(
        " update achp_files set file_id = " + origFileId +
        " where file_id = " + tempFileId);
      log.debug("Original file replaced with copy");
    }
    catch (Exception e)
    {
      throw new RuntimeException("clearGeneratedFile(file="  + file + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }    

  public void downloadAchFileData(AchFile file, OutputStream out) 
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select file_data from achp_files where file_id = " 
        + file.getFileId());
      if (!rs.next())
      {
        return;
      }
      Blob b = rs.getBlob(1);
      InputStream in = b.getBinaryStream();

      try
      {
        // using 10K buffer for now
        byte[] data = new byte[10000];
        int len = 0;
        while ((len = in.read(data)) != -1)
        {
          out.write(data,0,len);
        }
      }
      finally
      {
        try { out.flush(); out.close(); } catch (Exception e) { }
        try { in.close(); } catch (Exception e) { }
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("downloadAchFileData(fileId="  
        + file.getFileId() + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Find ach files that are transmitted today
   * Usually there is only 1 file each day that goes out. Destination - CUSTOMER
   * But this code can handle multiple if there are more than one
   * 
   * @param sysTypeOpt
   * @return
   */
  public List getTransferredAchFiles(int sysTypeOpt){
	  
	  Statement s = null;
	    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

	    try
	    {
	      connect();
	      s = con.createStatement();
	      ResultSet rs = s.executeQuery(
	        " select * from achp_files  " +
	        " where transmit_ts > sysdate -1 " +
	        " and file_name like 'nach%' " +
	        "  and test_flag = '" + testFlagVal + "'");
	      List files = new ArrayList();
	      while (rs.next())
	      {
	        files.add(createAchFile(rs));
	      }
	      return files;
	        
	    }
	    catch (Exception e)
	    {
	    	logEntry("getTransferredAchFiles()" , e.toString());
	      throw new RuntimeException("getTransferredAchFiles(sysTypeOpt=" 
	        + sysTypeOpt + ")",e);
	      
	    }
	    finally
	    {
	      cleanUp(s);
	    }
	  
  }

  /**
   * Finds ACH files that have not been transferred.
   */
  public List getUntransferredAchFiles(int sysTypeOpt)
  {
    Statement s = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_files  " +
        " where transmit_ts is null " +
        "  and test_flag = '" + testFlagVal + "'" +
        " order by ft_id ");
      List files = new ArrayList();
      while (rs.next())
      {
        files.add(createAchFile(rs));
      }
      return files;
        
    }
    catch (Exception e)
    {
      throw new RuntimeException("getUntransferredAchFiles(sysTypeOpt=" 
        + sysTypeOpt + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Timestamp an ACH file as transferred (set transmit_ts)
   */
  public void setFileTransmitDate(long fileId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      s.executeUpdate(
        " update achp_files " +
        " set transmit_ts = sysdate " +
        " where file_id = " + fileId);
    }
    catch (Exception e)
    {
      throw new RuntimeException("setFileTransmitDate(fileId=" 
        + fileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private AchReturn createAchReturn(ResultSet rs) throws Exception
  {
    AchReturn ret = new AchReturn();
    ret.setReturnId     (rs.getLong       ("return_id"));
    ret.setCreateTs     (rs.getTimestamp  ("create_ts"));
    ret.setProfileId    (rs.getString     ("profile_id"));
    ret.setDestination(Destination.destinationFor
                        (rs.getString     ("destination")));
    ret.setTranId       (rs.getLong       ("tran_id"));
    ret.setSummaryId    (rs.getLong       ("summary_id"));
    ret.setReturnType   (rs.getString     ("return_type"));
    ret.setReasonCode   (rs.getInt        ("reason_code"));
    ret.setBankControlNum
                        (rs.getString     ("bank_control_num"));
    ret.setReturnDescription
                        (rs.getString     ("return_description"));
    ret.setDescriptor   (rs.getString     ("descriptor"));
    ret.setDeathTs      (rs.getTimestamp  ("date_of_death"));
    ret.setMesTraceNum  (rs.getString     ("mes_trace_num"));
    ret.setProcTraceNum (rs.getString     ("proc_trace_num"));
    ret.setProcFlag     (rs.getString     ("proc_flag"));
    ret.setTestFlag     (rs.getString     ("test_flag"));
    ret.setAmount       (rs.getBigDecimal ("amount").setScale(2));
    return ret;
  }

  public AchReturn getAchReturn(long returnId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_returns where return_id = " + returnId);
      if (rs.next())
      {
        return createAchReturn(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchReturn(returnId=" + returnId + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }

    return null;
  }

  public AchReturn getAchReturnByTranId(long tranId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_returns where tran_id = " + tranId);
      if (rs.next())
      {
        return createAchReturn(rs);
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchReturnByTranId(trannId=" 
        + tranId + ")",e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
      cleanUp();
    }

    return null;
  }

  /**
   * Fetch unprocessed returns with matching test flag.  Filters out
   * non-return types ('C' as in notice of change), only gets 'R' types
   * which are returns and/or rejects, currently being handled as the
   * same.
   */
  public List getUnprocessedReturns(int batchSize, int sysTypeOpt, 
    long batchId)
  {
    Statement s = null;

    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select r.* from achp_returns r, achp_transactions t " +
        " where r.proc_flag <> 'Y' " +
        "  and r.return_type = 'R' " +
        "  and r.test_flag = '" + testFlagVal + "' " +
        "  and r.tran_id = t.tran_id " +
        (batchId != -1L ? " and t.batch_id = " + batchId + " " : "") +
        (batchSize > 0 ? "  and rownum <= " + batchSize : ""));

      List rets = new ArrayList();
      while (rs.next())
      {
        rets.add(createAchReturn(rs));
      }
      return rets;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getUnprocessedReturns()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public void markReturnProcessed(AchReturn ret)
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " update achp_returns set proc_flag = 'Y' " +
        " where return_id = " + ret.getReturnId());
      ret.setProcFlag(AchReturn.FLAG_YES);
    }
    catch (Exception e)
    {
      throw new RuntimeException("markReturnProcessed(returnId=" 
        + ret.getReturnId() + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public List getBatchReturns(long batchId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select r.* " +
        " from achp_returns r, achp_transactions t " +
        " where " +
        "  r.return_type = 'R' " +
        "  and r.tran_id = t.tran_id " +
        "  and t.batch_id = " + batchId);

      List rets = new ArrayList();
      while (rs.next())
      {
        rets.add(createAchReturn(rs));
      }
      return rets;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getBatchReturns()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public void generateTestReturns(long batchId)
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " insert into achp_returns " +
        "  select " +
        "   achp_id_sequence.nextval ret_id,  " +
        "   sysdate create_ts,    " +
        "   t.profile_id,         " +
        "   d.destination,        " +
        "   t.tran_id,            " +
        "   null summary_id,      " +
        "   tr.return_type,       " +
        "   tr.reason_code,       " +
        "   tr.bank_control_num,  " +
        "   nvl(rc.description,   " +
        "       'Uknown reason code: ' || tr.reason_code) " +
        "            return_description,        " +
        "   tr.date_of_death,     " +
        "   d.trace_num,          " +
        "   tr.proc_trace_num,    " +
        "   'N' proc_flag,        " +
        "   t.test_flag,          " +
        "   d.amount              " +
        "  from " +
        "   achp_test_returns tr,       " +
        "   achp_transactions t,        " +
        "   achp_file_details d,        " +
        "   ach_reject_reason_codes rc  " +
        "  where  " +
        "   t.batch_id = " + batchId +
        "   and dukpt_decrypt_wrapper(t.account_num_enc) = tr.account_num " +
        "   and t.tran_status = tr.tran_status  " +
        "   and t.test_flag = 'Y'               " +
        "   and t.tran_id = d.source_id         " +
        "   and (tr.return_type || to_char(tr.reason_code,'FM00')) " +
        "    = rc.reason_code(+) ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("generateTestReturns(batchId="   
        + batchId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public void insertTestRequest(TestRequest request)
  {
    PreparedStatement ps = null;

    try
    {
      byte[] accountNumEnc = request.getAccount().getEncodedData();

      connect();

      ps = con.prepareStatement(
        " insert into achp_test_requests ( " +
        "  request_id,        " +
        "  request_ts,        " +
        "  profile_id,        " +
        "  trident_tran_id,   " +
        "  tran_source,       " +
        "  tran_type,         " +
        "  sec_code,          " +
        "  transit_num,       " +
        "  account_type,      " +
        "  account_num,       " +
        "  account_num_enc,   " +
        "  amount,            " +
        "  ref_num,           " +
        "  cust_name,         " +
        "  cust_id,           " +
        "  recur_flag,        " +
        "  test_flag )        " +
        " values (            " +
        "  ?, ?, ?, ?, ?, ?,  " +
        "  ?, ?, ?, ?, ?, ?,  " +
        "  ?, ?, ?, ?, ? )    ");

      int col = 1;
      if (request.getRequestId() <= 0)
      {
        ps.setNull(col++,java.sql.Types.NUMERIC);
      }
      else
      {
        ps.setLong(col++,request.getRequestId());
      }
      if (request.getRequestTs() == null)
      {
        ps.setNull(col++,java.sql.Types.TIMESTAMP);
      }
      else
      {
        ps.setTimestamp(col++,request.getRequestTs());
      }
      ps.setString    (col++,request.getProfileId());
      ps.setString    (col++,request.getTridentTranId());
      ps.setString    (col++,""+request.getTranSource());
      ps.setString    (col++,""+request.getTranType());
      ps.setString    (col++,""+request.getSecCode());
      ps.setString    (col++,request.getTransitNum());
      ps.setString    (col++,request.getAccountType());
      ps.setString    (col++,request.getAccountNum());
      ps.setBytes     (col++,accountNumEnc);
      ps.setBigDecimal(col++,request.getAmount());
      ps.setString    (col++,request.getRefNum());
      ps.setString    (col++,request.getCustName());
      ps.setString    (col++,request.getCustId());
      ps.setString    (col++,request.getRecurFlag());
      ps.setString    (col++,request.getTestFlag());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertMerchProfile(request=" 
        + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  public DropDownItem[] getReportStatusDdItems()
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select unique(display_text) item_str from achp_tran_statuses ");
      List ddList = new ArrayList();
      while (rs.next())
      {
        String itemStr = rs.getString("item_str");
        ddList.add(new DropDownItem(itemStr,itemStr));
      }
      return (DropDownItem[])ddList.toArray(new DropDownItem[] { });
    }
    catch (Exception e)
    {
      throw new RuntimeException("getReportStatusDdItems()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private StatusReportRow createStatusReportRow(ResultSet rs) 
    throws Exception
  {
    StatusReportRow row = new StatusReportRow();
    loadAchTransaction(row,rs);
    row.setReportStatus (rs.getString("report_status"));
    row.setBatchNum     (rs.getInt("batch_num"));
    return row;
  }

  public List getStatusReportRows(StatusReportParms parms)
  {
    Statement s = null;

    try
    {
      connect();

      StringBuffer qs = new StringBuffer();
      if (parms.hasFromDate())
      {
        String fromDateStr = formatDate(parms.getFromDate(),FMT_ORA_DATE);
        String toDateStr = formatDate(parms.getToDate(),FMT_ORA_DATE);
        toDateStr = toDateStr == null ? fromDateStr : toDateStr;
        qs.append("  and trunc(t.tran_ts) between " + fromDateStr + " and " 
          + toDateStr + "\n");
      }
      if (parms.hasTranType())
      {
        qs.append("  and t.tran_type = '" + parms.getTranType() + "'\n");
      }
      if (parms.hasMerchNum())
      {
        qs.append("  and t.merch_num = '" + parms.getMerchNum() + "'\n");
      }
      if (parms.hasRefNum())
      {
        qs.append("  and lower(t.ref_num) like '%" 
          + parms.getRefNum().toLowerCase() + "%'\n");
      }
      if (parms.hasAccountNum())
      {
        qs.append("  and lower(account_num) like '%" 
          + parms.getAccountNum().toLowerCase() + "%'\n");
      }
      if (parms.hasSecCode())
      {
        qs.append("  and t.sec_code = '" + parms.getSecCode() + "'\n");
      }
      if (parms.hasCustName())
      {
        qs.append("  and lower(t.cust_name) like '%" 
          + parms.getCustName().toLowerCase() + "%'\n");
      }
      if (parms.hasCustId())
      {
        qs.append("  and lower(t.cust_id) like '%"
          + parms.getCustId().toLowerCase() + "%'\n");
      }
      if (parms.hasTranId())
      {
        qs.append("  and t.tran_id = " + parms.getTranId() + "\n");
      }
      // TODO: handle status filtering
      if (parms.hasInStats())
      {
        qs.append("  and s.display_text in ( " 
          + parms.getInStatsString() + " )\n");
      }
      if (parms.hasBatchNum())
      {
        qs.append("  and b.batch_num = " + parms.getBatchNum() + "\n");
        if (parms.curBatchOnly())
        {
          qs.append("  and b.end_ts is null\n");
        }
        // if no date range given then use batch 
        // date range when filtering by batch num
        if (!parms.hasFromDate())
        {
          qs.append("  and trunc(t.tran_ts) between");
          qs.append(" trunc(b.start_ts) and trunc(nvl(b.end_ts,sysdate))\n");
        }
      }
      if (parms.hasProfileId())
      {
        qs.append("  and t.profile_id = '" + parms.getProfileId() + "'\n");
      }
      if (parms.hasBatchId())
      {
        qs.append("  and t.batch_id = " + parms.getBatchId() + "\n");
      }

      qs.insert(0,
        " select                  \n" +
        "  t.tran_id,             \n" +
        "  t.trident_tran_id,     \n" + 
        "  t.merch_num,           \n" +
        "  t.profile_id,          \n" +
        "  t.ft_id,               \n" +
        "  t.tran_ts,             \n" +
        "  t.pend_ts,             \n" +
        "  t.tran_source,         \n" +
        "  t.tran_status,         \n" +
        "  t.request_id,          \n" +
        "  t.tran_type,           \n" + 
        "  dukpt_decrypt_wrapper( \n" +
        "   t.account_num_enc)    \n" +
        "    account_num,         \n" +
        "  t.account_type,        \n" +
        "  t.transit_num,         \n" +
        "  t.amount,              \n" +
        "  t.ref_num,             \n" +
        "  t.sec_code,            \n" +
        "  t.cust_name,           \n" +
        "  t.cust_id,             \n" +
        "  t.ip_address,          \n" +
        "  t.batch_id,            \n" +
        "  b.batch_num,           \n" +
        "  t.recur_flag,          \n" +
        "  t.test_flag,           \n" +
        "  t.hold_flag,           \n" +
        "  s.display_text report_status \n" +
        " from                    \n" +
        "  achp_transactions  t,  \n" +
        "  achp_tran_statuses s,  \n" +
        "  organization       o,  \n" +
        "  group_merchant     g,  \n" +
        "  achp_tran_batches  b   \n" +
        " where                   \n" +
        "  o.org_group = " + parms.getUserNodeId() + " \n" +
        "  and o.org_num = g.org_num \n" +
        "  and g.merchant_number = t.merch_num \n" +
        "  and t.batch_id = b.batch_id(+) \n" +
        "  and t.tran_status = s.tran_status(+) \n");
      //log.debug("status report query:\n" + qs);
      s = con.createStatement();
      ResultSet rs = s.executeQuery(""+qs);

      List rows = new ArrayList();
      while (rs.next())
      {
        rows.add(createStatusReportRow(rs));
      }
      return rows;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getStatusReportRows()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private AchTranLogEntry createAchTranLogEntry(ResultSet rs) throws Exception
  {
    AchTranLogEntry log = new AchTranLogEntry();
    log.setLogId(rs.getLong("log_id"));
    log.setLogTs(rs.getTimestamp("log_ts"));
    log.setStatus(TranStatus.statusFor(rs.getString("tran_status")));
    log.setStatusDesc(rs.getString("status_desc"));
    log.setHoldFlag(rs.getString("hold_flag"));
    return log;
  }

  public List getAchTranLogEntries(long tranId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select " +
        "  l.*,  " +
        "  s.display_text status_desc " +
        " from " +
        "  achp_tran_log l, " +
        "  achp_tran_statuses s  " +
        " where " +
        "  l.tran_id = " + tranId + " " +
        "  and l.tran_status = s.tran_status " +
        " order by log_ts ");

      List history = new ArrayList();
      while (rs.next())
      {
        history.add(createAchTranLogEntry(rs));
      }
      return history;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getAchTranLogEntries(tranId=" 
        + tranId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private TranBatch createTranBatch(ResultSet rs) throws Exception
  {
    TranBatch batch = new TranBatch();
    batch.setBatchId  (rs.getLong     ("batch_id"));
    batch.setProfileId(rs.getString   ("profile_id"));
    batch.setBatchNum (rs.getInt      ("batch_num"));
    batch.setStartTs  (rs.getTimestamp("start_ts"));
    batch.setEndTs    (rs.getTimestamp("end_ts"));
    batch.setTestFlag (rs.getString   ("test_flag"));
    return batch;
  }

  public TranBatch getTranBatch(long batchId)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_tran_batches where batch_id = " + batchId);
      if (rs.next())
      {
        return createTranBatch(rs);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getTranBatch(batchId=" + batchId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public TranBatch getTranBatch(String profileId, String testFlag)
  {
    CallableStatement cs = null;

    try
    {
      connect();
      cs = con.prepareCall("{ ? = call achp_get_batch_id( ?, ? ) }");
      cs.registerOutParameter(1,Types.NUMERIC);
      cs.setString(2,profileId);
      cs.setString(3,testFlag);
      cs.execute();
      long batchId = cs.getLong(1);
      if (batchId > 0)
      {
        return getTranBatch(batchId);
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getTranBatch(profileId=" + profileId 
        + ", testFlag=" + testFlag + ")",e);
    }
    finally
    {
      try { cs.close(); } catch (Exception e) { }
      cleanUp();
    }
  }


  public List getTranBatches(String profileId, Date fromDate, 
    Date toDate, boolean testFlag)
  {
    PreparedStatement ps = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " select * " +
        " from achp_tran_batches " +
        " where " +
        "  profile_id = ? " +
        "  and test_flag = '" + 
        (testFlag ? "Y" : "N") + "' " +
        "  and trunc(end_ts) between ? and ? " +
        " order by batch_id desc ");
      ps.setString(1,profileId);
      ps.setTimestamp(2,toTimestamp(fromDate));
      ps.setTimestamp(3,toTimestamp(toDate));
      ResultSet rs = ps.executeQuery();
      List batches = new ArrayList(); 
      while (rs.next())
      {
        batches.add(createTranBatch(rs));
      }
      return batches;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getTestBatches(fromDate=" + fromDate
        + ",toDate=" + toDate + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  public void closeTranBatch(long batchId)
  {
    Statement s = null;
    boolean opSuccess = false;

    try
    {
      startTransaction();

      TranBatch b = getTranBatch(batchId);

      s = con.createStatement();
      s.executeUpdate(
        " update achp_tran_batches " +
        " set end_ts = sysdate " +
        " where batch_id = " + batchId);

      int batchNum = b.getBatchNum() + 1;
      batchNum = (batchNum > 9999 ? 1 : batchNum);
      s.executeUpdate(
        " update achp_profiles set " +
        (b.isTest() ? " test_batch_num " : " batch_num ") + 
        " = " + batchNum + " " +
        " where profile_id = '" + b.getProfileId() + "' ");

      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("closeTranBatch(batchId=" + batchId + ")",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess)
      {
        commitTransaction();
      }
      else
      {
        rollbackTransaction();
      }
    }
  }

  /**
   * This closes all open test or production batches and updates
   * the batch number in the profile. 
   */
  public void closeAllBatches(int sysTypeOpt, long runId, Date cutoffDate)
  {
    boolean opSuccess = false;
    PreparedStatement ps = null;
    String testFlagVal = checkSysTypeOption(sysTypeOpt) ? "Y" : "N";

    try
    {
      startTransaction();

      ps = con.prepareStatement(
        " update achp_tran_batches " +
        " set end_ts = sysdate, run_id =  " + runId + " " +
        " where end_ts is null and test_flag = '" + testFlagVal + "' " +
        "  and start_ts <= ? ");
      ps.setTimestamp(1,toTimestamp(cutoffDate));
      ps.executeUpdate();
      ps.close();
         
      if (sysTypeOpt == SYS_PROD)
      {
        ps = con.prepareStatement(
          " update achp_profiles " + 
          " set batch_num =  " +
          "  case when " +
          "   batch_num is null " +
          "   or batch_num < 1 " +
          "   or batch_num > 998 " +
          "  then 1 else batch_num + 1 end " +
          " where profile_id in ( " +
          "  select profile_id " +
          "  from achp_tran_batches " +
          "  where run_id = " + runId + " ) ");
      }
      else
      {
        ps = con.prepareStatement(
          " update achp_profiles " + 
          " set test_batch_num =  " +
          "  case when " +
          "   test_batch_num is null " +
          "   or test_batch_num < 1 " +
          "   or test_batch_num > 998 " +
          "  then 1 else test_batch_num + 1 end " +
          " where profile_id in ( " +
          "  select profile_id " +
          "  from achp_tran_batches " +
          "  where run_id = " + runId + " ) ");
      }
      ps.executeUpdate();
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("closeAllBatches(sysTypeOpt=" + sysTypeOpt 
        + ", runId=" + runId + ", cutoffDate=" + cutoffDate + ")",e);
    }
    finally
    {
      cleanUp(ps);
      if (opSuccess)
      {
        commitTransaction();
      }
      else
      {
        rollbackTransaction();
      }
    }
  }

  private String getSecDescriptor(SecCode secCode)
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select descriptor from achp_sec_codes " +
        " where sec_code = '" + secCode + "' ");
      if (rs.next())
      {
        return rs.getString("descriptor");
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getSecDescriptor(secCode=" 
        + secCode + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public TranDetailData getTranDetailData(long tranId)
  {
    try
    {
      AchTransaction tran = getAchTransaction(tranId);
      MerchProfile profile = getMerchProfile(tran.getProfileId());
      MerchProfileRef ref = getMerchProfileRef(tran.getProfileId());
      TranBatch batch = getTranBatch(tran.getProfileId(),tran.getTestFlag());
      AchReturn ret = getAchReturnByTranId(tranId);
      String secDesc = getSecDescriptor(tran.getSecCode());
      List logs = getAchTranLogEntries(tranId);
      return new TranDetailData(tran,profile,ref,batch,ret,secDesc,logs);
    }
    catch (Exception e)
    {
      throw new RuntimeException("getTranDetailData(tranId=" + tranId + ")",e);
    }
  }

  public void insertRun(Run run)
  {
    PreparedStatement ps = null;

    log.debug("inserting run " + run.getClass().getName() + " with batch " + run.getBatchId());

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into achp_eod_runs ( " +
        "  start_ts,    " +
        "  end_ts,      " +
        "  run_rc,      " +
        "  message,     " +
        "  user_name,   " +
        "  batch_id )   " +
        " values ( ?, ?, ?, ?, ?, ? ) ");

      int col = 1;
      ps.setTimestamp (col++,run.getStartTs());
      ps.setTimestamp (col++,run.getEndTs());
      ps.setString    (col++,run.getRunRc());
      if (run.getMessage() != null)
      {
        ps.setString(col++,run.getMessage());
      }
      else
      {
        ps.setNull(col++,java.sql.Types.VARCHAR);
      }
      ps.setString    (col++,run.getUserName());
      if (run.getBatchId() > 0)
      {
        ps.setLong(col++,run.getBatchId());
      }
      else
      {
        ps.setNull(col++,java.sql.Types.NUMERIC);
      }
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertRun(run=" + run + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  public void insertJob(Job job)
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into achp_jobs ( " +
        "  run_id,    " +
        "  start_ts,  " +
        "  end_ts,    " +
        "  job_name,  " +
        "  job_rc,    " +
        "  batch_id,  " +
        "  message )  " +
        " values (    " +
        "  ?, ?, ?, ?, ?, ?, ? )  ");

      int col = 1;
      if (job.getRunId() > 0)
      {
        ps.setLong(col++,job.getRunId());
      }
      else
      {
        ps.setNull(col++,java.sql.Types.NUMERIC);
      }
      ps.setTimestamp (col++,job.getStartTs());
      ps.setTimestamp (col++,job.getEndTs());
      ps.setString    (col++,job.getJobName());
      ps.setString    (col++,job.getJobRc());
      if (job.getBatchId() > 0)
      {
        ps.setLong(col++,job.getBatchId());
      }
      else
      {
        ps.setNull(col++,java.sql.Types.NUMERIC);
      }
      if (job.getMessage() != null)
      {
        ps.setString(col++,job.getMessage());
      }
      else
      {
        ps.setNull(col++,java.sql.Types.VARCHAR);
      }
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException("insertJob(job=" + job + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  public class JobSummary extends AchBase implements Job
  {
    public long jobId;
    public long runId;
    public long batchId;
    public Date startDate;
    public Date endDate;
    public String jobName;
    public String jobRc;
    public String message;
    public boolean testFlag;

    public boolean isDirect()               { throw new RuntimeException("Illegal operation"); }
    public boolean isTest()                 { return testFlag; }

    public void setJobId(long jobId)        { this.jobId = jobId; }
    public long getJobId()                  { return jobId; }
    public void setRunId(long runId)        { this.runId = runId; }
    public long getRunId()                  { return runId; }
    public void setBatchId(long batchId)    { this.batchId = batchId; }
    public long getBatchId()                { return batchId; }
    public void setStartDate(Date startDate){ this.startDate = startDate; }
    public Date getStartDate()              { return startDate; }
    public void setStartTs(Timestamp startTs)
                                            { this.startDate = toDate(startTs); }
    public Timestamp getStartTs()           { return toTimestamp(startDate); }
    public void setEndDate(Date endDate)    { this.endDate = endDate; }
    public Date getEndDate()                { return endDate; }
    public void setEndTs(Timestamp endTs)   { this.endDate = toDate(endTs); }
    public Timestamp getEndTs()             { return toTimestamp(endDate); }
    public void setJobName(String jobName)  { this.jobName = jobName; }
    public String getJobName()              { return jobName; }
    public void setJobRc(String jobRc)      { this.jobRc = jobRc; }
    public String getJobRc()                { return jobRc; }
    public void setMessage(String message)  { this.message = message; }
    public String getMessage()              { return message; }
    public void run() { }
  }
    
  private Job createJob(ResultSet rs) throws Exception
  {
    JobSummary job = new JobSummary();
    job.jobId     = rs.getLong("job_id");
    job.runId     = rs.getLong("run_id");
    job.batchId   = rs.getLong("batch_id");
    job.setStartTs(rs.getTimestamp("start_ts"));
    job.setEndTs(rs.getTimestamp("end_ts"));
    job.jobName   = rs.getString("job_name");
    job.jobRc     = rs.getString("job_rc");
    job.message   = rs.getString("message");
    //job.testFlag  = rs.getString("test_flag").toUpperCase().equals("Y");
    return job;
  }

  public List getBatchJobs(long batchId)
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select * from achp_jobs " +
        " where batch_id = " + batchId +
        " order by job_id ");
      List jobs = new ArrayList();
      while (rs.next())
      {
        jobs.add(createJob(rs));
      }
      return jobs;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getBatchJobs(batchId=" + batchId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Returns current sysdate from database.  Useful in some cases when local 
   * time is out of sync with database time.
   */
  public Date getDbDate()
  {
    Statement s = null;
    
    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(" select sysdate from dual ");
      rs.next();
      return toDate(rs.getTimestamp(1));
    }
    catch (Exception e)
    {
      throw new RuntimeException("getDbDate()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Returns true if the given date is a banking holiday.
   */
  public boolean isHoliday(Calendar cal)
  {
    Statement s = null;
    
    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select holiday from achp_holidays " +
        " where holiday = " + formatDate(cal.getTime(),FMT_ORA_DATE));
      return rs.next();
    }
    catch (Exception e)
    {
      throw new RuntimeException("isHoliday(cal=" + cal.getTime() + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }
  public boolean isHoliday()
  {
    return isHoliday(Calendar.getInstance());
  }

  /**
   * Returns true if the indicated date is Monday - Friday and is not a
   * banking holiday.
   */
  public boolean isBusinessDay(Calendar cal)
  {
    int dayOfWeek = cal.get(cal.DAY_OF_WEEK);
    if (dayOfWeek == cal.SATURDAY || dayOfWeek == cal.SUNDAY)
    {
      return false;
    }
    return !isHoliday(cal);
  }
  public boolean isBusinessDay()
  {
    return isBusinessDay(Calendar.getInstance());
  }

  /**
   * Returns true if the indicated date is Sunday - Thursday and is
   * not a banking holiday.
   */
  public boolean isProcessingDay(Calendar cal)
  {
    int dayOfWeek = cal.get(cal.DAY_OF_WEEK);
    if (dayOfWeek == cal.SATURDAY || dayOfWeek == cal.FRIDAY)
    {
      return false;
    }
    return !isHoliday(cal);
  }
  public boolean isProcessingDay()
  {
    return isProcessingDay(Calendar.getInstance());
  }

  /**
   * Summary data for ACH files to be emailed to accounting upon file 
   * transmission.
   */

  public class AchFileEmailSummary
  {
    public String     fileName;
    public long       fileId;
    public int        recordCount;
    public int        debitCount;
    public BigDecimal debitAmount;
    public int        creditCount;
    public BigDecimal creditAmount;

    public String toString()
    {
      return "AchFileEmailSummary [ " +
        "fileName: " + fileName +
        ", fileId: " + fileId +
        ", recordCount: " + recordCount +
        ", debitCount: " + debitCount +
        ", debitAmount: " + debitAmount +
        ", creditCount: " + creditCount +
        ", creditAmount: " + creditAmount +
        " ]";
    }
  }
  
  public AchFileEmailSummary getEmailSummary(long fileId)
  {
    Statement s = null;
    
    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                                " +
        "  f.file_id,                                           " +
        "  f.file_name,                                         " +
        "  count(d.detail_id) record_count,                     " +
        "  sum(decode(d.cd_code,'D',1,0)) debit_count,          " +
        "  sum(decode(d.cd_code,'D',d.amount,0)) debit_amount,  " +
        "  sum(decode(d.cd_code,'C',1,0)) credit_count,         " +
        "  sum(decode(d.cd_code,'C',d.amount,0)) credit_amount  " +
        " from                                                  " +
        "  achp_files f,                                        " +
        "  achp_file_details d                                  " +
        " where                                                 " +
        "  f.file_id = d.file_id                                " +
        "  and f.file_id = " + fileId + "                       " +
        " group by f.file_name, f.file_id                       ");
      if (rs.next())
      {
        AchFileEmailSummary sum = new AchFileEmailSummary();
        sum.fileId        = fileId;
        sum.fileName      = rs.getString("file_name");
        sum.recordCount   = rs.getInt("record_count");
        sum.debitCount    = rs.getInt("debit_count");
        sum.debitAmount   = rs.getBigDecimal("debit_amount").setScale(2);
        sum.creditCount   = rs.getInt("credit_count");
        sum.creditAmount  = rs.getBigDecimal("credit_amount").setScale(2);
        return sum;
      }
      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException("getEmailSummary(fileId=" + fileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Load returns in ACH_RETURN_DETAIL and ACH_RETURN_DETAIL_ADDENDA 
   * into ACHP_RETURNS.
   *
   * This may be temporary as it might be simpler to handle this with
   * a trigger, but for now that trigger cannot be created.
   */
  public void loadNewReturns()
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " " +
        " insert into achp_returns                                  " +
        " ( select                                                  " +
        "    achp_id_sequence.nextval return_id,                    " +
        "    sysdate create_ts,                                     " +
        "    d.profile_id,                                          " +
        "    d.destination,                                         " +
        "    decode(d.destination,'CUST',d.source_id,null)          " +
        "             tran_id,                                      " +
        "    decode(d.destination,'MERCH',source_id,null)           " +
        "             summary_id,                                   " +
        "    rda.return_type,                                       " +
        "    rda.reason_code,                                       " +
        "    rda.bank_control_number bank_control_num,              " +
        "    rda.return_description,                                " +
        "    rda.date_of_death,                                     " +
        "    rda.original_trace_number mes_trace_num,               " +
        "    rda.jpmc_trace_number proc_trace_num,                  " +
        "    'N' proc_flag,                                         " +
        "    d.test_flag,                                           " +
        "    d.amount,                                              " +
        "    rrc.description                                        " +
        "   from                                                    " +
        "    achp_file_details d,                                   " +
        "    ach_return_detail_addenda rda,                         " +
        "    ach_reject_reason_codes rrc                            " +
        "   where                                                   " +
        "    d.trace_num = rda.original_trace_number                " +
        "    and rda.return_type || to_char(rda.reason_code,'FM00') " +
        "     = rrc.reason_code                                     " +
        "    and not exists                                         " +
        "     ( select * from achp_returns                          " +
        "       where mes_trace_num = rda.original_trace_number ) ) ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("loadNewReturns()",e); 
    }
    finally
    {
      cleanUp(s);
    }
  }

  /** 
   * Set the post status and timestamp on all transactions linked to a given
   * file. This is intended to be called immediately after file transmission
   * on processing days.
   */
  public void postFileTransactions(AchFile file)
  {
    // file to transaction linkage depends on destination
    if (file.getDestination().equals(Destination.CUSTOMER))
    {
      postCustomerFileTransactions(file);
      nachFileName = file.getFileName();
    }
    else
    {
      postMerchantFileTransactions(file);
    }
  }

  /**
   * Set status to post on all transactions related to given ACH customer 
   * file.  Transactions are linked directly to detail records.  These
   * should be sales.
   */
  private void postCustomerFileTransactions(AchFile file)
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " update achp_transactions set  " +
        "  tran_status =                " +
        "   decode(tran_status,         " +
        "    'C_PEND','C_POST',         " +
        "    'S_PEND','S_POST',         " +
        "    tran_status),              " +
        "  post_ts = sysdate,           " +
        "  post_days = 0                " +
        " where                         " +
        "  tran_status in               " +
        "   ( 'C_PEND', 'S_PEND' )      " +
        "  and tran_id in (             " +
        "   select source_id            " +
        "   from achp_file_details      " +
        "   where file_id =             " + 
        "    " + file.getFileId() + " ) ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("postCustomerFileTransactions()",e); 
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Set status to post on all transactions related to given ACH customer 
   * file.  Transactions are linked to sf entries via detail records. These
   * should be credits.
   */
  private void postMerchantFileTransactions(AchFile file)
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " update achp_transactions set          " +
        "  tran_status =                        " +
        "   decode(tran_status,                 " +
        "    'C_PEND','C_POST',                 " +
        "    'S_PEND','S_POST',                 " +
        "    tran_status),                      " +
        "  post_ts = sysdate,                   " +
        "  post_days = 0                        " +
        " where                                 " +
        "  tran_status in                       " +
        "   ( 'C_PEND', 'S_PEND' )              " +
        "  and tran_id in (                     " +
        "   select                              " +
        "    e.tran_id                          " +
        "   from                                " +
        "    achp_file_details d,               " +
        "    achp_sf_entries e                  " +
        "   where                               " +
        "    d.file_id =                        " +
        "     " + file.getFileId() + "          " +
        "    and d.source_id = e.summary_id )   ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("postMerchantFileTransactions()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Set status to post on all transactions related to closed (cutoff <> null)
   * settlement funding summary records.  This triggers the post logic on
   * transactions that are not posted due to association with a transferred
   * ach file.
   */
  public void postNetZeroTransactions()
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " update achp_transactions set          " +
        "  tran_status =                        " +
        "   decode(tran_status,                 " +
        "    'C_PEND','C_POST',                 " +
        "    'S_PEND','S_POST',                 " +
        "    tran_status),                      " +
        "  post_ts = sysdate,                   " +
        "  post_days = 0                        " +
        " where                                 " +
        "  tran_status in                       " +
        "   ( 'C_PEND', 'S_PEND' )              " +
        "  and tran_id in (                     " +
        "   select                              " +
        "    e.tran_id                          " +
        "   from                                " +
        "    achp_sf_summaries s,               " +
        "    achp_sf_entries e                  " +
        "   where                               " +
        "    s.summary_id = e.summary_id        " +
        "    and s.cutoff_ts is not null        " +
        "    and s.net_total = 0 )              ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("postMerchantFileTransactions()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Increment post days by one on all posted transactions.
   */
  public void incrementPostDays()
  {
    Statement s = null;

    try
    {
      connect();

      s = con.createStatement();
      s.executeUpdate(
        " update achp_transactions set            " +
        "  post_days = post_days + 1              " +
        " where                                   " +
        "  tran_status in ( 'S_POST', 'C_POST' )  ");
   }
    catch (Exception e)
    {
      throw new RuntimeException("incrementPostDays()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public static class CustReturnSummary
  {
    public String       merchNum;
    public String       merchName;
    public String       profileId;
    public String       companyName;
    public Date         recvDate;
    public Destination  destination;
    public int          returnCount;
    public int          nocCount;
    public BigDecimal   netReturn;

    public CustReturnSummary(
      String merchNum, 
      String merchName,
      String profileId, 
      String companyName,
      Date recvDate, 
      Destination destination,
      int returnCount, 
      int nocCount, 
      BigDecimal netReturn)
    {
      this.merchNum     = merchNum;
      this.merchName    = merchName;
      this.profileId    = profileId;
      this.companyName  = companyName;
      this.recvDate     = recvDate;
      this.destination  = destination;
      this.returnCount  = returnCount;
      this.nocCount     = nocCount;
      this.netReturn    = netReturn;
    }
  }

  /**
   * Returns list of customer ACH return/NOC summaries by profile 
   * ID for the given date.
   */
  public List getCustReturnSummaries(Date recvDate)
  {
    Statement s = null;

    try
    {
      connect();

      String dateStr = formatDate(recvDate,FMT_ORA_DATE);
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                                " +
        "  p.merch_num,                                         " +
        "  r.profile_id,                                        " +
        "  p.merch_name,                                        " +
        "  p.company_name,                                      " +
        "  trunc(r.create_ts) recv_date,                        " +
        "  d.destination,                                       " +
        "  sum(decode(r.return_type,'R',1,0)) return_count,     " +
        "  sum(decode(r.return_type,'C',1,0)) noc_count,        " +
        "  sum(decode(r.return_type,'R',r.amount,0)) net_return " +
        " from                                                  " +
        "  achp_returns       r,                                " +
        "  achp_profiles      p,                                " +
        "  achp_file_details  d                                 " +
        " where                                                 " +
        "  trunc(r.create_ts) = " + dateStr + "                 " +
        "  and r.profile_id = p.profile_id                      " +
        "  and r.mes_trace_num = d.trace_num                    " +
        "  and d.destination = 'CUST'                           " +
        " group by p.merch_num, r.profile_id, p.merch_name,     " +
        "  p.company_name, d.destination, trunc(r.create_ts)    ");

      List sums = new ArrayList();
      while (rs.next())
      {
        sums.add(new CustReturnSummary(
          rs.getString("merch_num"),
          rs.getString("merch_name"),
          rs.getString("profile_id"),
          rs.getString("company_name"),
          toDate(rs.getTimestamp("recv_date")),
          Destination.destinationFor(rs.getString("destination")),
          rs.getInt("return_count"),
          rs.getInt("noc_count"),
          rs.getBigDecimal("net_return").setScale(2)));
      }
      return sums;
   }
    catch (Exception e)
    {
      throw new RuntimeException("()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  public static class MerchReturnItem
  {
    public AchReturn ret;
    public String merchNum;
    public String merchName;
    public String profileId;
    public String companyName;

    public MerchReturnItem(AchReturn ret, String merchNum, String merchName, 
      String profileId, String companyName)
    {
      this.ret          = ret;
      this.merchNum     = merchNum;
      this.merchName    = merchName;
      this.profileId    = profileId;
      this.companyName  = companyName;
    }
  }

  /**
   * Returns list of merchant settlement return items.  These could
   * be used to notify Risk department.
   */
  public List getMerchReturnItems(Date recvDate)
  {
    Statement s = null;

    try
    {
      connect();

      String dateStr = formatDate(recvDate,FMT_ORA_DATE);
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                " +
        "  r.*,                                 " +
        "  p.company_name,                      " +
        "  p.merch_name,                        " +
        "  p.merch_num                          " +
        " from                                  " +
        "  achp_returns       r,                " +
        "  achp_file_details  d,                " +
        "  achp_profiles      p                 " +
        " where                                 " +
        "  trunc(r.create_ts) = " + dateStr + " " +
        "  and r.mes_trace_num = d.trace_num    " +
        "  and d.destination = 'MERCH'          " +
        "  and r.profile_id = p.profile_id      ");

      List items = new ArrayList();
      while (rs.next())
      {
        items.add(new MerchReturnItem(
          createAchReturn(rs),
          rs.getString("merch_num"),
          rs.getString("merch_name"),
          rs.getString("profile_id"),
          rs.getString("company_name")));
      }
      return items;
   }
    catch (Exception e)
    {
      throw new RuntimeException("()",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private ArrayList getAchpRiskProcessTypes(){
	  log.debug("Getting achp risk proc types from database");
	  //ArrayList<Integer> achpProcList = new ArrayList<>();
	  ArrayList achpProcList = new ArrayList();
	  Statement stmt = null;
	  
	  try{
		  connect();
		  String sqlStr = "select process_type from risk_process_types where process_desc like 'ACHP%'";
		  stmt = con.createStatement();
		  ResultSet rs = stmt.executeQuery(sqlStr);
		
		  while(rs.next()){
			//achpProcList.add(rs.getInt("process_type"));
			achpProcList.add(rs.getInt("process_type"));
		  }    
		  
	  }catch(Exception e){
		  logEntry("getAchpRiskProcessTypes() ", e.toString());
	     
	  }finally{
		  cleanUp();
	  }
	
	  return achpProcList;
  }
  
  
public void generateRiskProcessEntries(String nachFileName) {
	 log.debug("Generating risk process row in db table");
	PreparedStatement stmt = null;
	
	//ArrayList<Integer> riskRules = getAchpRiskProcessTypes();
	ArrayList riskRules = getAchpRiskProcessTypes();
	
    try
    {
      connect();      
      
      String insertSQL = "insert into mes.risk_process (process_type, load_filename) values (?, ?)";
      stmt = con.prepareStatement(insertSQL);
      
	    //for(Integer procType : riskRules){
      for (Iterator i = riskRules.iterator(); i.hasNext();) {
	    	
	    	Integer procType = (Integer)i.next();
        
        stmt.setInt(1, procType.intValue());
	    	stmt.setString(2, nachFileName);
	    	int rows = stmt.executeUpdate();	        
	        log.debug("Risk Proc row created for " + procType + " and filename " + nachFileName);
	    }        
   }
    catch (Exception e)
    {
      logEntry("generateRiskProcessEntries() ", e.toString());
      throw new RuntimeException("generateRiskProcessEntries()",e);
    }
    finally
    {
      cleanUp(stmt);
    }
}
}