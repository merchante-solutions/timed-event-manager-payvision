package com.mes.pach;

import java.util.HashMap;

public class Destination
{
  // ach "destination" account types
  public static final Destination CUSTOMER  = new Destination("CUST");
  public static final Destination MERCHANT  = new Destination("MERCH");

  private static HashMap destHash = new HashMap();

  static
  {
    destHash.put(""+CUSTOMER,CUSTOMER);
    destHash.put(""+MERCHANT,MERCHANT);
  };

  private String destStr;

  private Destination(String destStr)
  {
    this.destStr = destStr;
  }

  public static Destination destinationFor(String forStr, Destination dflt)
  {
    Destination dest = (Destination)destHash.get(forStr);
    return dest != null ? dest : dflt;
  }
    
  public static Destination destinationFor(String forStr)
  {
    Destination dest = destinationFor(forStr,null);
    if (dest == null)
    {
      throw new NullPointerException("Invalid indicator string: '" 
        + forStr + "'");
    }
    return dest;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof Destination))
    {
      return false;
    }
    Destination that = (Destination)o;
    return that.destStr.equals(destStr);
  }

  public String toString()
  {
    return destStr;
  }
}