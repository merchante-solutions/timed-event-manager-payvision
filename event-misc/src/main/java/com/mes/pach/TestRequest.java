package com.mes.pach;

import org.apache.log4j.Logger;

public class TestRequest extends AchRequest
{
  static Logger log = Logger.getLogger(TestRequest.class);

  private TranSource tranSource;

  public void setTranSource(TranSource tranSource)
  {
    throw new RuntimeException("Invalid operation: setTranSource()");
  }
  public TranSource getTranSource()
  {
    return TranSource.TEST;
  }
}