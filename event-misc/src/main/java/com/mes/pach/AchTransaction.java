package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchTransaction extends AchBase
{
  static Logger log = Logger.getLogger(AchTransaction.class);

  private long            tranId;
  private String          tridentTranId;
  private String          merchNum;
  private String          profileId;
  private long            ftId;
  private Date            tranDate;
  private Date            pendDate;
  private TranSource      tranSource;
  private long            requestId;
  private TranType        tranType        = TranType.SALE;
  private TranStatus      status;
  private AchAccount      account         = new AchAccount();
  private BigDecimal      amount          = new BigDecimal(0).setScale(2);
  private String          refNum;
  private SecCode         secCode;
  private String          custName;
  private String          custId;
  private String          ipAddress;
  private int             batchId;
  private String          recurFlag;
  private String          testFlag;
  private String          holdFlag;

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public void setTridentTranId(String tridentTranId)
  {
    this.tridentTranId = tridentTranId;
  }
  public String getTridentTranId()
  {
    return tridentTranId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setTranDate(Date tranDate)
  {
    this.tranDate = tranDate;
  }
  public Date getTranDate()
  {
    return tranDate;
  }
  public void setTranTs(Timestamp tranTs)
  {
    tranDate = toDate(tranTs);
  }
  public Timestamp getTranTs()
  {
    return toTimestamp(tranDate);
  }

  public void setPendDate(Date pendDate)
  {
    this.pendDate = pendDate;
  }
  public Date getPendDate()
  {
    return pendDate;
  }
  public void setPendTs(Timestamp pendTs)
  {
    pendDate = toDate(pendTs);
  }
  public Timestamp getPendTs()
  {
    return toTimestamp(pendDate);
  }

  public void setTranSource(TranSource tranSource)
  {
    this.tranSource = tranSource;
  }
  public TranSource getTranSource()
  {
    return tranSource;
  }

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setTranType(TranType tranType)
  {
    this.tranType = tranType;
  }
  public TranType getTranType()
  {
    return tranType;
  }

  public boolean isSale()
  {
    return TranType.SALE.equals(tranType);
  }
  public boolean isCredit()
  {
    return TranType.CREDIT.equals(tranType);
  }

  public void setStatus(TranStatus status)
  {
    this.status = status;
  }
  public TranStatus getStatus()
  {
    return status;
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setRefNum(String refNum)
  {
    this.refNum = refNum;
  }
  public String getRefNum()
  {
    return refNum;
  }

  public void setSecCode(SecCode secCode)
  {
    this.secCode = secCode;
  }
  public SecCode getSecCode()
  {
    return secCode;
  }

  public AchAccount getAccount()
  {
    return account;
  }

  public void setAccountNum(String accountNum)
  {
    account.setAccountNum(accountNum);
  }
  public String getAccountNum()
  {
    return account.truncated();
  }

  public void setAccountType(String accountType)
  {
    account.setAccountType(accountType);
  }
  public String getAccountType()
  {
    return account.getAccountType();
  }
  public String getAccountDescription()
  {
    return account.getAccountDescription();
  }

  public void setTransitNum(String transitNum)
  {
    account.setTransitNum(transitNum);
  }
  public String getTransitNum()
  {
    return account.getTransitNum();
  }

  public boolean isCheckingAccount()
  {
    return account.isCheckingAccount();
  }
  public boolean isSavingsAccount()
  {
    return account.isSavingsAccount();
  }

  public void setCustName(String custName)
  {
    this.custName = custName;
  }
  public String getCustName()
  {
    return custName;
  }

  public void setCustId(String custId)
  {
    this.custId = custId;
  }
  public String getCustId()
  {
    return custId;
  }

  public void setIpAddress(String ipAddress)
  {
    this.ipAddress = ipAddress;
  }
  public String getIpAddress()
  {
    return ipAddress;
  }

  public void setBatchId(int batchId)
  {
    this.batchId = batchId;
  }
  public int getBatchId()
  {
    return batchId;
  }

  public void setRecurFlag(String recurFlag)
  {
    validateFlag(recurFlag,"recur");
    this.recurFlag = recurFlag;
  }
  public String getRecurFlag()
  {
    return recurFlag;
  }
  public boolean isRecurring()
  {
    return flagBoolean(recurFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setHoldFlag(String holdFlag)
  {
    validateFlag(holdFlag,"hold");
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return holdFlag;
  }
  public boolean isHeld()
  {
    return flagBoolean(holdFlag);
  }

  public String toString()
  {
    return "AchTransaction"
      + "\n  tranId:        " + tranId
      + "\n  tridentTranId: " + tridentTranId
      + "\n  merchNum:      " + merchNum
      + "\n  profileId:     " + profileId
      + "\n  ftId:          " + ftId
      + "\n  tranDate:      " + tranDate
      + "\n  tranSource:    " + tranSource
      + "\n  requestId:     " + requestId
      + "\n  tranType:      " + tranType
      + "\n  tranStatus:    " + status
      + "\n  transitNum:    " + getTransitNum()
      + "\n  accountNum:    " + getAccountNum()
      + "\n  accountType:   " + getAccountType()
      + "\n  amount:        " + amount
      + "\n  refNum:        " + refNum
      + "\n  secCode:       " + secCode
      + "\n  custName:      " + custName
      + "\n  custId:        " + custId
      + "\n  ipAddress:     " + ipAddress
      + "\n  batchId:       " + batchId
      + "\n  recurFlag:     " + isRecurring()
      + "\n  holdFlag:      " + isHeld()
      + "\n  testFlag:      " + isTest();
  }
}