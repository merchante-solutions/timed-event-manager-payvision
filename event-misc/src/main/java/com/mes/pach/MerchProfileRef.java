package com.mes.pach;

import org.apache.log4j.Logger;

public class MerchProfileRef extends AchBase
{
  static Logger log = Logger.getLogger(MerchProfileRef.class);

  private String profileId;
  private String merchNum;
  private String merchName;
  private String mifTransitNum;
  private String mifDdaNum;
  private String linkFlag;
  private String ccdFlag;
  private String ppdFlag;
  private String webFlag;
  private String telFlag;
  private String holdFlag;

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setMifTransitNum(String mifTransitNum)
  {
    this.mifTransitNum = mifTransitNum;
  }
  public String getMifTransitNum()
  {
    return mifTransitNum;
  }

  public void setMifDdaNum(String mifDdaNum)
  {
    this.mifDdaNum = mifDdaNum;
  }
  public String getMifDdaNum()
  {
    return mifDdaNum;
  }

  public void setLinkFlag(String linkFlag)
  {
    validateFlag(linkFlag,"link");
    this.linkFlag = linkFlag;
  }
  public String getLinkFlag()
  {
    return linkFlag;
  }
  public boolean isLinked()
  {
    return flagBoolean(linkFlag);
  }

  public void setCcdFlag(String ccdFlag)
  {
    validateFlag(linkFlag,"ccd");
    this.ccdFlag = ccdFlag;
  }
  public String getCcdFlag()
  {
    return ccdFlag;
  }
  public boolean ccdEnabled()
  {
    return flagBoolean(ccdFlag);
  }

  public void setPpdFlag(String ppdFlag)
  {
    validateFlag(linkFlag,"ppd");
    this.ppdFlag = ppdFlag;
  }
  public String getPpdFlag()
  {
    return ppdFlag;
  }
  public boolean ppdEnabled()
  {
    return flagBoolean(ppdFlag);
  }

  public void setWebFlag(String webFlag)
  {
    validateFlag(linkFlag,"web");
    this.webFlag = webFlag;
  }
  public String getWebFlag()
  {
    return webFlag;
  }
  public boolean webEnabled()
  {
    return flagBoolean(webFlag);
  }

  public void setTelFlag(String telFlag)
  {
    validateFlag(linkFlag,"tel");
    this.telFlag = telFlag;
  }
  public String getTelFlag()
  {
    return telFlag;
  }
  public boolean telEnabled()
  {
    return flagBoolean(telFlag);
  }

  public void setHoldFlag(String holdFlag)
  {
    validateFlag(holdFlag,"hold");
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return holdFlag;
  }
  public boolean isHeld()
  {
    return flagBoolean(holdFlag);
  }

  public String toString()
  {
    return "MerchProfileRef"
      + "\n  profileId:     " + profileId
      + "\n  merchNum:      " + merchNum
      + "\n  merchName:     " + merchName
      + "\n  mifTransitNum: " + mifTransitNum
      + "\n  mifDdaNum:     " + mifDdaNum
      + "\n  linked:        " + isLinked()
      + "\n  ccd enabled:   " + ccdEnabled()
      + "\n  ppd enabled:   " + ppdEnabled()
      + "\n  web enabled:   " + webEnabled()
      + "\n  tel enabled:   " + telEnabled()
      + "\n  on hold:       " + isHeld();
  }
}
