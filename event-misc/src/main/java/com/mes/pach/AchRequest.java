package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public abstract class AchRequest extends AchBase
{
  static Logger log = Logger.getLogger(AchRequest.class);

  private long        requestId;
  private Date        requestDate;
  private String      tridentTranId;
  private String      merchNum;
  private String      profileId;
  private TranType    tranType;
  private AchAccount  account = new AchAccount();
  private BigDecimal  amount = new BigDecimal(0).setScale(2);
  private String      refNum;
  private SecCode     secCode;
  private String      custName;
  private String      custId;
  private String      ipAddress;
  private String      recurFlag;
  private String      testFlag;

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }
  public long getRequestId()
  {
    return requestId;
  }

  public void setTridentTranId(String tridentTranId)
  {
    this.tridentTranId = tridentTranId;
  }
  public String getTridentTranId()
  {
    return tridentTranId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  public Date getRequestDate()
  {
    return requestDate;
  }
  public void setRequestTs(Timestamp requestTs)
  {
    requestDate = toDate(requestTs);
  }
  public Timestamp getRequestTs()
  {
    return toTimestamp(requestDate);
  }

  public void setTranType(TranType tranType)
  {
    this.tranType = tranType;
  }
  public TranType getTranType()
  {
    return tranType;
  }

  public void setRefNum(String refNum)
  {
    this.refNum = refNum;
  }
  public String getRefNum()
  {
    return refNum;
  }

  public void setSecCode(SecCode secCode)
  {
    this.secCode = secCode;
  }
  public SecCode getSecCode()
  {
    return secCode;
  }

  public void setCustName(String custName)
  {
    this.custName = custName;
  }
  public String getCustName()
  {
    return custName;
  }

  public void setCustId(String custId)
  {
    this.custId = custId;
  }
  public String getCustId()
  {
    return custId;
  }

  public void setRecurFlag(String recurFlag)
  {
    validateFlag(recurFlag,"recur");
    this.recurFlag = recurFlag;
  }
  public String getRecurFlag()
  {
    return recurFlag;
  }
  public boolean isRecurring()
  {
    return flagBoolean(recurFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  /**
   * ACH account accessors
   */
  public AchAccount getAccount()
  {
    return account;
  }

  public void setAccountNum(String accountNum)
  {
    account.setAccountNum(accountNum);
  }
  public String getAccountNum()
  {
    return account.truncated();
  }

  public void setAccountType(String accountType)
  {
    account.setAccountType(accountType);
  }
  public String getAccountType()
  {
    return account.getAccountType();
  }

  public void setTransitNum(String transitNum)
  {
    account.setTransitNum(transitNum);
  }
  public String getTransitNum()
  {
    return account.getTransitNum();
  }

  public boolean isCheckingAccount()
  {
    return account.isCheckingAccount();
  }
  public boolean isSavingsAccount()
  {
    return account.isSavingsAccount();
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setIpAddress(String ipAddress)
  {
    this.ipAddress = ipAddress;
  }
  public String getIpAddress()
  {
    return ipAddress;
  }

  public abstract void setTranSource(TranSource tranSource);
  public abstract TranSource getTranSource();

  public String toString()
  {
    return "AchRequest "
      + "\n  requestId:     " + requestId
      + "\n  requestDate:   " + requestDate
      + "\n  tridentTranId: " + tridentTranId
      + "\n  merchNum:      " + merchNum
      + "\n  profileId:     " + profileId
      + "\n  tranSource:    " + getTranSource()
      + "\n  tranType:      " + tranType
      + "\n  transitNum:    " + getTransitNum()
      + "\n  accountNum:    " + getAccountNum()
      + "\n  accountType:   " + getAccountType()
      + "\n  amount:        " + amount
      + "\n  refNum:        " + refNum
      + "\n  secCode:       " + secCode
      + "\n  custName:      " + custName
      + "\n  custId:        " + custId
      + "\n  ipAddress:     " + ipAddress
      + "\n  recurFlag:     " + isRecurring()
      + "\n  testFlag:      " + isTest()
      + toStringExt();
  }

  protected String toStringExt()
  {
    return "";
  }
}