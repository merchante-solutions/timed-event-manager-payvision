package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class TranBatch extends AchBase
{
  static Logger log = Logger.getLogger(TranBatch.class);

  private long batchId;
  private String profileId;
  private int batchNum;
  private Date startDate;
  private Date endDate;
  private String testFlag;
  private long runId;

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setBatchNum(int batchNum)
  {
    this.batchNum = batchNum;
  }
  public int getBatchNum()
  {
    return batchNum;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = toDate(startTs);
  }
  public Timestamp getStartTs()
  {
    return toTimestamp(startDate);
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = toDate(endTs);
  }
  public Timestamp getEndTs()
  {
    return toTimestamp(endDate);
  }
  public boolean isClosed()
  {
    return endDate != null;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setRunId(long runId)
  {
    this.runId = runId;
  }
  public long getRunId()
  {
    return runId;
  }

  public String toString()
  {
    return "TranBatch"
      + "\n  batchId:     " + batchId
      + "\n  runId:       " + (runId > 0 ? ""+runId : "--")
      + "\n  profileId:   " + profileId
      + "\n  batchNum:    " + batchNum
      + "\n  startDate:   " + startDate
      + "\n  endDate:     " + endDate
      + "\n  testFlag:    " + isTest();
  }
}