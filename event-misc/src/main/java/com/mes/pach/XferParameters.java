package com.mes.pach;

public class XferParameters
{
  public String host;
  public String user;
  public String pass;
  public String path;
  public String prefix;

  public XferParameters(String host, String user, String pass, String path, String prefix)
  {
    this.host = host;
    this.user = user;
    this.pass = pass;
    this.path = path;
    this.prefix = prefix;
  }

  public String toString()
  {
    return "XferParameters [ "
      + "host: " + host
      + ", user: " + user
      + ", pass: " + pass
      + ", path: " + path
      + ", prefix: " + prefix
      + " ]";
  }
}
