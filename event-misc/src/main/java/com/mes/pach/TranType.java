package com.mes.pach;

import java.util.HashMap;

public class TranType
{
  // ach transaction types
  public static final TranType SALE   = new TranType("SALE",    "Sale");
  public static final TranType CREDIT = new TranType("CREDIT",  "Credit");

  private static HashMap typeHash = new HashMap();

  static
  {
    typeHash.put(""+SALE,SALE);
    typeHash.put(""+CREDIT,CREDIT);
  };

  private String typeStr;
  private String description;

  private TranType(String typeStr, String description)
  {
    this.typeStr = typeStr;
    this.description = description;
  }

  public static TranType typeFor(String forStr, TranType dflt)
  {
    TranType type = (TranType)typeHash.get(forStr);
    return type != null ? type : dflt;
  }
    
  public static TranType typeFor(String forStr)
  {
    TranType type = typeFor(forStr,null);
    if (type == null)
    {
      throw new NullPointerException("Invalid type string: '" 
        + forStr + "'");
    }
    return type;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof TranType))
    {
      return false;
    }
    TranType that = (TranType)o;
    return that.typeStr.equals(typeStr);
  }

  public String getDescription()
  { 
    return description;
  }

  public String toString()
  {
    return typeStr;
  }
}