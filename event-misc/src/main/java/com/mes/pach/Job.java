package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;

public interface Job extends Runnable
{
  public static final String JRC_OK     = "OK";
  public static final String JRC_ERROR  = "ERROR";

  public boolean isDirect();
  public boolean isTest();

  public void setJobId(long jobId);
  public long getJobId();

  public void setRunId(long runId);
  public long getRunId();

  public void setBatchId(long batchId);
  public long getBatchId();

  public void setStartDate(Date startDate);
  public Date getStartDate();
  public void setStartTs(Timestamp startTs);
  public Timestamp getStartTs();

  public void setEndDate(Date endDate);
  public Date getEndDate();
  public void setEndTs(Timestamp endTs);
  public Timestamp getEndTs();

  public void setJobName(String jobName);
  public String getJobName();

  public void setJobRc(String jobRc);
  public String getJobRc();

  public void setMessage(String message);
  public String getMessage();
}
