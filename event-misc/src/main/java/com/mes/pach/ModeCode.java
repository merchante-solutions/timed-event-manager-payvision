package com.mes.pach;

import java.util.HashMap;

public class ModeCode
{
  // ach transaction types
  public static final ModeCode TEST_ACCEL     = new ModeCode("TEST_ACCEL");
  public static final ModeCode TEST_STANDARD  = new ModeCode("TEST_STANDARD");
  public static final ModeCode PRODUCTION     = new ModeCode("PRODUCTION");

  private static HashMap codeHash = new HashMap();

  static
  {
    codeHash.put(""+TEST_ACCEL,TEST_ACCEL);
    codeHash.put(""+TEST_STANDARD,TEST_STANDARD);
    codeHash.put(""+PRODUCTION,PRODUCTION);
  };

  private String codeStr;

  private ModeCode(String codeStr)
  {
    this.codeStr = codeStr;
  }

  public static ModeCode codeFor(String forStr, ModeCode dflt)
  {
    ModeCode code = (ModeCode)codeHash.get(forStr);
    return code != null ? code : dflt;
  }
    
  public static ModeCode codeFor(String forStr)
  {
    ModeCode code = codeFor(forStr,null);
    if (code == null)
    {
      throw new NullPointerException("Invalid code string: '" 
        + forStr + "'");
    }
    return code;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof ModeCode))
    {
      return false;
    }
    ModeCode that = (ModeCode)o;
    return that.codeStr.equals(codeStr);
  }

  public boolean isTest()
  {
    return TEST_ACCEL.equals(this) || TEST_STANDARD.equals(this);
  }
  public boolean isAccelerated()
  {
    return TEST_ACCEL.equals(this);
  }

  public String toString()
  {
    return codeStr;
  }
}