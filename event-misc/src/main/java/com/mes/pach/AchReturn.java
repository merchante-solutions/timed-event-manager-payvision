package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchReturn extends AchBase
{
  static Logger log = Logger.getLogger(AchReturn.class);

  private long returnId;
  private Date createDate;
  private String profileId;
  private Destination destination;
  private long tranId;
  private long summaryId;
  private String returnType;
  private int reasonCode;
  private String bankControlNum;
  private String returnDescription;
  private String descriptor;
  private Date deathDate;
  private String mesTraceNum;
  private String procTraceNum;
  private String procFlag;
  private String testFlag;
  private BigDecimal amount;

  public void setReturnId(long returnId)
  {
    this.returnId = returnId;
  }
  public long getReturnId()
  {
    return returnId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setDestination(Destination destination)
  {
    this.destination = destination;
  }
  public Destination getDestination()
  {
    return destination;
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public void setSummaryId(long summaryId)
  {
    this.summaryId = summaryId;
  }
  public long getSummaryId()
  {
    return summaryId;
  }

  public void setReturnType(String returnType)
  {
    this.returnType = returnType;
  }
  public String getReturnType()
  {
    return returnType;
  }

  public void setReasonCode(int reasonCode)
  {
    this.reasonCode = reasonCode;
  }
  public int getReasonCode()
  {
    return reasonCode;
  }

  public void setBankControlNum(String bankControlNum)
  {
    this.bankControlNum = bankControlNum;
  }
  public String getBankControlNum()
  {
    return bankControlNum;
  }

  /**
   * Data loaded from return file, may be descriptive text or additional data.
   */
  public void setReturnDescription(String returnDescription)
  {
    this.returnDescription = returnDescription;
  }
  public String getReturnDescription()
  {
    return returnDescription;
  }

  /**
   * MES generated descriptor
   */
  public void setDescriptor(String descriptor)
  {
    this.descriptor = descriptor;
  }
  public String getDescriptor()
  {
    return descriptor;
  }

  public void setDeathDate(Date deathDate)
  {
    this.deathDate = deathDate;
  }
  public Date getDeathDate()
  {
    return deathDate;
  }
  public void setDeathTs(Timestamp deathTs)
  {
    deathDate = toDate(deathTs);
  }
  public Timestamp getDeathTs()
  {
    return toTimestamp(deathDate);
  }

  public void setMesTraceNum(String mesTraceNum)
  {
    this.mesTraceNum = mesTraceNum;
  }
  public String getMesTraceNum()
  {
    return mesTraceNum;
  }

  public void setProcTraceNum(String procTraceNum)
  {
    this.procTraceNum = procTraceNum;
  }
  public String getProcTraceNum()
  {
    return procTraceNum;
  }

  public void setProcFlag(String procFlag)
  {
    validateFlag(procFlag,"proc");
    this.procFlag = procFlag;
  }
  public String getProcFlag()
  {
    return procFlag;
  }
  public boolean isProcessed()
  {
    return flagBoolean(procFlag);
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public String toString()
  {
    return "AchReturn"
      + "\n  returnId:          " + returnId
      + "\n  createDate:        " + createDate
      + "\n  profileId:         " + profileId
      + "\n  destination:       " + destination
      + "\n  tranId:            " + tranId
      + "\n  summaryId:         " + summaryId
      + "\n  returnType:        " + returnType
      + "\n  reasonCode:        " + reasonCode
      + "\n  bankControlNum:    " + bankControlNum
      + "\n  returnDescription: " + returnDescription
      + "\n  deathDate:         " + deathDate
      + "\n  mesTraceNum:       " + mesTraceNum
      + "\n  procTraceNum:      " + procTraceNum
      + "\n  procFlag:          " + procFlag
      + "\n  testFlag:          " + testFlag
      + "\n  amount:            " + amount;
  }
}