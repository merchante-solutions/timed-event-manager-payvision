package com.mes.pach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class TranDetailData extends AchBase
{
  static Logger log = Logger.getLogger(TranDetailData.class);

  private AchTransaction  _tran;
  private AchReturn       _ret;
  private MerchProfile    _pro;
  private MerchProfileRef _merch;
  private TranBatch       _batch;
  private String          _secDesc;
  private List            _logs = new ArrayList();

  public TranDetailData(AchTransaction _tran, MerchProfile _pro, 
    MerchProfileRef _merch, TranBatch _batch, AchReturn _ret, 
    String _secDesc, List _logs)
  {
    this._tran    = _tran;
    this._pro     = _pro;
    this._merch   = _merch;
    this._batch   = _batch;
    this._ret     = _ret;
    this._secDesc = _secDesc;
    this._logs    = _logs;
  }

  public AchTransaction tran()
  {
    return _tran;
  }

  public MerchProfile pro()
  {
    return _pro;
  }

  public MerchProfileRef merch()
  {
    return _merch;
  }

  public TranBatch batch()
  {
    return _batch;
  }

  public AchReturn ret()
  {
    return _ret;
  }
  public boolean hasReturn()
  {
    return _ret != null;
  }
  
  public String secDesc()
  {
    return _secDesc;
  }

  public List logs()
  {
    return _logs;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("TranDetailData:");
    buf.append("\n" + _pro);
    buf.append("\n" + _tran);
    buf.append("\n" + _batch);
    buf.append("\n" + _ret);
    buf.append("\n" + _secDesc);
    for (Iterator i = _logs.iterator(); i.hasNext();)
    {
      buf.append("\n" + i.next());
    }
    return ""+buf;
  }
}