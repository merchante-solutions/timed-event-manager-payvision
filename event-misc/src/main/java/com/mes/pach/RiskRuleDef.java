package com.mes.pach;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.log4j.Logger;

public class RiskRuleDef extends AchBase
{
  static Logger log = Logger.getLogger(RiskRuleDef.class);
  
  public static String DC_SALE_AMOUNT     = "SALE_AMOUNT";
  public static String DC_CREDIT_AMOUNT   = "CREDIT_AMOUNT";
  public static String DC_AVERAGE_TICKET  = "AVERAGE_TICKET";
  public static String DC_SALE_COUNT      = "SALE_COUNT";
  public static String DC_SALE_VOLUME     = "SALE_VOLUME";
  public static String DC_CREDIT_COUNT    = "CREDIT_COUNT";
  public static String DC_CREDIT_VOLUME   = "CREDIT_VOLUME";
  // new risk rules
  public static String DC_ACH_RETURN_COUNT   = "ACH_RETURN_COUNT";
  public static String DC_MONTHLY_PROCESSING_AMT   = "MONTHLY_PROCESSING_AMT";
  public static String DC_DUPLICATE_DDA_COUNT   = "DUPLICATE_DDA_COUNT";
  public static String DC_DUPLICATE_CUSTOMER_NAME_COUNT   = "DUPLICATE_CUSTOMER_NAME_COUNT";
  
  public static String DA_HOLD_ACCT       = "HOLD_ACCT";
  public static String DA_HOLD_TRAN       = "HOLD_TRAN";
  
  private long defId;
  private String defCode;
  private String defName;
  private String description;
  private String defType;
  private String frequency;

  private Set varNames = new HashSet();
  public Set actions = new HashSet();
  
  
  public long getDefId()
  {
    return defId;
  }
  public void setDefId(long defId)
  {
    this.defId = defId;
  }
  
  public String getDefCode()
  {
    return defCode;
  }
  public void setDefCode(String defCode)
  {
    this.defCode = defCode;
  }
  
  public String getDefName()
  {
    return defName;
  }
  public void setDefName(String defName)
  {
    this.defName = defName;
  }
  
  public String getDescription()
  {
    return description;
  }
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public Set getVarNames()
  {
    return varNames;
  }

  public String getDefData()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = varNames.iterator(); i.hasNext();)
    {
      buf.append(""+i.next() + (i.hasNext() ? ";" : ""));
    }
    return ""+buf;
  }
  public void setDefData(String defData)
  {
    varNames = new HashSet();
    String[] vars = defData.split(";");
    for (int i = 0; i < vars.length; ++i)
    {
      varNames.add(vars[i]);
    }
  }
  
  public Set getActions()
  {
    return actions;
  }

  public String getDefActions()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = actions.iterator(); i.hasNext();)
    {
      buf.append(""+i.next() + (i.hasNext() ? ";" : ""));
    }
    return ""+buf;
  }
  public void setDefActions(String defActions)
  {
    actions = new HashSet();
    String[] acts = defActions.split(";");
    for (int i = 0; i < acts.length; ++i)
    {
      actions.add(acts[i]);
    }
  }

  public String getDefType()
  {
    return defType;
  }
  public void setDefType(String defType)
  {
    this.defType = defType;
  }
  
  public String getFrequency()
  {
    return frequency;
  }
  public void setFrequency(String frequency)
  {
    this.frequency = frequency;
  }
  

  public String toString()
  {
    return "RiskRuleDef [ "
      + "defId: " + defId
      + ", defCode: " + defCode
      + ", defName: " + defName
      + ", description: " + description
      + ", defData: " + getDefData()
      + ", defActions: " + getDefActions()
      + ", defType: " + defType
      + ", frequency: " + frequency
      + " ]";
  }
}