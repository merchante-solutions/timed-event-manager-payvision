package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchTranLogEntry extends AchBase
{
  static Logger log = Logger.getLogger(AchTranLogEntry.class);

  private long            logId;
  private Date            logDate;
  private TranStatus      status;
  private String          statusDesc;
  private String          holdFlag;

  public void setLogId(long logId)
  {
    this.logId = logId;
  }
  public long getLogId()
  {
    return logId;
  }

  public void setLogDate(Date logDate)
  {
    this.logDate = logDate;
  }
  public Date getLogDate()
  {
    return logDate;
  }
  public void setLogTs(Timestamp logTs)
  {
    logDate = toDate(logTs);
  }
  public Timestamp getLogTs()
  {
    return toTimestamp(logDate);
  }

  public void setStatus(TranStatus status)
  {
    this.status = status;
  }
  public TranStatus getStatus()
  {
    return status;
  }

  public void setStatusDesc(String statusDesc)
  {
    this.statusDesc = statusDesc;
  }
  public String getStatusDesc()
  {
    return statusDesc;
  }

  public void setHoldFlag(String holdFlag)
  {
    validateFlag(holdFlag,"hold");
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return holdFlag;
  }
  public boolean isHeld()
  {
    return flagBoolean(holdFlag);
  }

  public String toString()
  {
    return "AchTranLogEntry"
      + "\n  logId:         " + logId
      + "\n  logDate:       " + logDate
      + "\n  status:        " + status
      + "\n  statusDesc:    " + statusDesc
      + "\n  holdFlag:      " + isHeld();
  }
}