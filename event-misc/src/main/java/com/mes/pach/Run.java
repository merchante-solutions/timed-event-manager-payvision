package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Defines interface for automated runs such as EodRun and TestSimulator.
 */
public interface Run extends Runnable
{
  public static final String RRC_OK = "OK";
  public static final String RRC_ERROR = "ERROR";

  public boolean isDirect();
  public boolean isTest();

  public void setRunId(long runId);
  public long getRunId();

  public void setBatchId(long batchId);
  public long getBatchId();

  public void setStartDate(Date startDate);
  public Date getStartDate();
  public void setStartTs(Timestamp startTs);
  public Timestamp getStartTs();

  public void setEndDate(Date endDate);
  public Date getEndDate();
  public void setEndTs(Timestamp endTs);
  public Timestamp getEndTs();

  public void setUserName(String userName);
  public String getUserName();

  public void setRunRc(String runRc);
  public String getRunRc();

  public void setMessage(String message);
  public String getMessage();
}
