package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class FundingEntry extends AchBase
{
  static Logger log = Logger.getLogger(FundingEntry.class);

  private long              entryId;
  private Date              entryDate;
  private String            merchNum;
  private String            profileId;
  private long              tranId;
  private long              ftId;
  private FundingEntryType  entryType;
  private BigDecimal        amount;
  private long              summaryId;
  private String            testFlag;

  public void setEntryId(long entryId)
  {
    this.entryId = entryId;
  }
  public long getEntryId()
  {
    return entryId;
  }

  public void setEntryDate(Date entryDate)
  {
    this.entryDate = entryDate;
  }
  public Date getEntryDate()
  {
    return entryDate;
  }
  public void setEntryTs(Timestamp recTs)
  {
    entryDate = toDate(recTs);
  }
  public Timestamp getEntryTs()
  {
    return toTimestamp(entryDate);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setEntryType(FundingEntryType entryType)
  {
    this.entryType = entryType;
  }
  public FundingEntryType getEntryType()
  {
    return entryType;
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setSummaryId(long summaryId)
  {
    this.summaryId = summaryId;
  }
  public long getSummaryId()
  {
    return summaryId;
  }

  public String toString()
  {
    return "FundingEntry"
      + "\n  entryId:     " + entryId
      + "\n  entryDate:   " + entryDate
      + "\n  merchNum:    " + merchNum
      + "\n  profileId:   " + profileId
      + "\n  tranId:      " + tranId
      + "\n  ftId:        " + ftId
      + "\n  entryType:   " + entryType
      + "\n  amount:      " + amount
      + "\n  testFlag:    " + isTest()
      + "\n  summaryId:   " + summaryId;
  }
}