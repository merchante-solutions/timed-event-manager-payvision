package com.mes.pach;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * Contains NACHA file parameters including origin and destination IDs.
 */
public class AchFileType extends AchBase
{
  static Logger log = Logger.getLogger(AchFileType.class);

  private long ftId;
  private String description;
  private String destinationId;
  private String destinationName;
  private String originId;
  private String originName;
  private String companyName;
  private String companyId;
  private String endpoint;

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setDestinationId(String destinationId)
  {
    this.destinationId = destinationId;
  }
  public String getDestinationId()
  {
    return destinationId;
  }

  public void setDestinationName(String destinationName)
  {
    this.destinationName = destinationName;
  }
  public String getDestinationName()
  {
    return destinationName;
  }

  public void setOriginId(String originId)
  {
    this.originId = originId;
  }
  public String getOriginId()
  {
    return originId;
  }

  public void setOriginName(String originName)
  {
    this.originName = originName;
  }
  public String getOriginName()
  {
    return originName;
  }

  public void setCompanyId(String companyId)
  {
    this.companyId = companyId;
  }
  public String getCompanyId()
  {
    return companyId;
  }

  public void setCompanyName(String companyName)
  {
    this.companyName = companyName;
  }
  public String getCompanyName()
  {
    return companyName;
  }
  
  public void setEndpoint(String endpoint)
  {
    this.endpoint = endpoint;
  }
  public String getEndpoint()
  {
    return endpoint;
  }
  
  /**
   * Wells Fargo requires a "security header" (see FLAT_FILE_DEF def_type=156
   * used by AchEvent to generate security header):
   *
   *  $$ADD ID=KNYCDAF6 BID='NWFACHWFMSACHM'
   *
   *  ID is hard coded, also called 'user' by FLAT_FILE_DEF
   *
   *  BID starts with NWFACH then uses first 8 digits of ORIGIN_ID in 
   *  ACH_TRIDENT_DESTINATIONS ('[WFMSACHM]ES'), also called application_id
   *  by FLAT_FILE_DEF
   *
   * TODO: these are hard coded right now, need to add these three as 
   *       new columns in ACHP_FILE_TYPES.
   */
   
  public boolean requiresSecurityHeader() {
    return description != null && description.startsWith("WF");
  }
  
  public String getSecHdrUserId() {
    return "KNYCDAF6";
  }
  
  public String getSecHdrAppId() {
    return "NWFACHWFMSACHM";
  }

  public XferParameters getXferParameters()
  {
    Pattern p = Pattern.compile("[:]?([^:]*)");
    Matcher m = p.matcher(endpoint);

    String host = null;
    String user = null;
    String pass = null;
    String path = null;
    String prefix = null;
    
    try {
      m.find(); host = m.group(1);
      m.find(); user = m.group(1);
      m.find(); pass = m.group(1);
      m.find(); path = m.group(1);
      m.find(); prefix = m.group(1);
    }
    catch (Exception e) {
      log.warn("Unable to extract all transfer parameters",e);
    }

    return new XferParameters(host,user,pass,path,prefix);
  }

  public String toString()
  {
    return "AchFileType"
      + "\n  ftId:            " + ftId
      + "\n  description:     " + description
      + "\n  destinationId:   " + destinationId
      + "\n  destinationName: " + destinationName
      + "\n  originId:        " + originId
      + "\n  originName:      " + originName
      + "\n  companyId:       " + companyId
      + "\n  companyName:     " + companyName
      + "\n  endpoint:        " + endpoint;
  }
}
