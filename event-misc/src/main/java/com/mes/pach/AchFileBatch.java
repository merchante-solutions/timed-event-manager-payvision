package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchFileBatch extends AchBase
{
  static Logger log = Logger.getLogger(AchFileBatch.class);

  private long    batchId;
  private Date    createDate;
  private String  testFlag;
  private int     detailCount;
  private int     batchNum;
  private long    ftId;
  private Destination destination;
  private int     classCode;
  private String  companyName;
  private String  companyId;
  private SecCode secCode;
  private String  entryDescription;
  private String  descriptiveDate;
  private String  effectiveEntryDate;
  private String  originDfiId;
  private String  discData;
  private long    fileId;

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setFileId(long fileId)
  {
    this.fileId = fileId;
  }
  public long getFileId()
  {
    return fileId;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setDestination(Destination destination)
  {
    this.destination = destination;
  }
  public Destination getDestination()
  {
    return destination;
  }

  public void setSecCode(SecCode secCode)
  {
    this.secCode = secCode;
  }
  public SecCode getSecCode()
  {
    return secCode;
  }

  public void setClassCode(int classCode)
  {
    this.classCode = classCode;
  }
  public int getClassCode()
  {
    return classCode;
  }

  public void setCompanyName(String companyName)
  {
    this.companyName = companyName;
  }
  public String getCompanyName()
  {
    return companyName;
  }

  public void setCompanyId(String companyId)
  {
    this.companyId = companyId;
  }
  public String getCompanyId()
  {
    return companyId;
  }

  public void setDiscData(String discData)
  {
    this.discData = discData;
  }
  public String getDiscData()
  {
    return discData;
  }

  public void setEntryDescription(String entryDescription)
  {
    this.entryDescription = entryDescription;
  }
  public String getEntryDescription()
  {
    return entryDescription;
  }

  public void setDescriptiveDate(String descriptiveDate)
  {
    this.descriptiveDate = descriptiveDate;
  }
  public String getDescriptiveDate()
  {
    return descriptiveDate;
  }

  public void setEffectiveEntryDate(String effectiveEntryDate)
  {
    this.effectiveEntryDate = effectiveEntryDate;
  }
  public String getEffectiveEntryDate()
  {
    return effectiveEntryDate;
  }

  public void setOriginDfiId(String originDfiId)
  {
    this.originDfiId = originDfiId;
  }
  public String getOriginDfiId()
  {
    return originDfiId;
  }

  public void setBatchNum(int batchNum)
  {
    this.batchNum = batchNum;
  }
  public int getBatchNum()
  {
    return batchNum;
  }

  public void setDetailCount(int detailCount)
  {
    this.detailCount = detailCount;
  }
  public int getDetailCount()
  {
    return detailCount;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public String toString()
  {
    return "AchFileBatch"
      + "\n  batchId:             " + batchId
      + "\n  createDate:          " + createDate
      + "\n  fileId:              " + (fileId > 0 ? ""+fileId : "--")
      + "\n  ftId:                " + ftId
      + "\n  destination:         " + destination
      + "\n  secCode:             " + secCode
      + "\n  classCode:           " + classCode
      + "\n  companyName:         " + companyName
      + "\n  companyId:           " + companyId
      + "\n  discData:            " + discData
      + "\n  entryDescription:    " + entryDescription
      + "\n  descriptiveDate:     " + descriptiveDate
      + "\n  effectiveEntryDate:  " + effectiveEntryDate
      + "\n  originDfiId:         " + originDfiId
      + "\n  batchNum:            " + batchNum
      + "\n  detailCount:         " + detailCount
      + "\n  testFlag:            " + testFlag;
  }
}
