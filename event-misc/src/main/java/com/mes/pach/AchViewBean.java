package com.mes.pach;

import org.apache.log4j.Logger;
import com.mes.mvc.mes.MesAction;
import com.mes.mvc.mes.MesViewBean;

public class AchViewBean extends MesViewBean
{
  static Logger log = Logger.getLogger(AchViewBean.class);

  private PachDb db = new PachDb();
  
  public AchViewBean(MesAction action)
  {
    super(action);
  }

  protected PachDb db()
  {
    if (db == null)
    {
      db = new PachDb();
    }
    return db;
  }
}
