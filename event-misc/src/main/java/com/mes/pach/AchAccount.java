package com.mes.pach;

import org.apache.log4j.Logger;
import com.mes.support.dc.MesEncryptedDataContainer;

public class AchAccount extends MesEncryptedDataContainer
{
  static Logger log = Logger.getLogger(AchAccount.class);

  public static final String  AT_CHECKING = "C";
  public static final String  AT_SAVINGS  = "S";

  public static final String  AT_UNKNOWN  = "U";
  public static final String  AT_INVALID  = "I";

  private String _truncated;
  private String accountType  = AT_UNKNOWN;
  private String transitNum;

  // min left clear 0, max left clear 2, min right clear 1, max right clear 4,
  // min truncation 1, min vis trunc str 1, max trunc str 100
  private Truncator truncr    = new Truncator("x",0,2,1,4,1,1,100);

  public AchAccount()
  {
  }
  public AchAccount(String accountNum, String transitNum, String accountType)
  {
    setData(accountNum);
    setTransitNum(transitNum);
    setAccountType(accountType);
  }

  public String clearText()
  {
    return empty() ? null : new String(getData());
  }

  public String truncated()
  {
    if (!empty() && _truncated == null)
    {
      try
      {
        _truncated = truncr.truncate(clearText());
        //  TridentTools.encodeCardNumber(clearText());
      }
      catch (Exception e)
      {
        log.error("Truncation error: " + e);
        e.printStackTrace();
      }
    }
    return _truncated;
  }

  public void setEncodedData(byte[] data)
  {
    super.setEncodedData(data);
    _truncated = null;
  }

  public void setAccountNum(String accountNum)
  {
    setData(accountNum.getBytes());
  }
  public String getAccountNum()
  {
    return clearText();
  }

  public static boolean isValidAccountType(String accountType)
  {
    return AT_CHECKING.equals(accountType) || 
           AT_SAVINGS.equals(accountType) || 
           AT_UNKNOWN.equals(accountType) ||
           AT_INVALID.equals(accountType);
  }

  public void setAccountType(String accountType)
  {
    if (!isValidAccountType(accountType))
    {
      throw new RuntimeException("Invalid DDA account type: '" + accountType + "'");
    }
    this.accountType = accountType;
  }
  public String getAccountType()
  {
    return accountType;
  }

  public String getAccountDescription()
  {
    return isCheckingAccount() ? "Checking" : "Savings";
  }

  public boolean isCheckingAccount()
  {
    return AT_CHECKING.equals(accountType);
  }
  public boolean isSavingsAccount()
  {
    return AT_SAVINGS.equals(accountType);
  }

  public void setTransitNum(String transitNum)
  {
    this.transitNum = transitNum;
  }
  public String getTransitNum()
  {
    return transitNum;
  }

  public String toString()
  {
    return "AchAccount [ "
      + "accountNum: " + truncated()
      + ", accountType: " + accountType
      + ", transitNum: " + transitNum
      + " ]";
  }
}

