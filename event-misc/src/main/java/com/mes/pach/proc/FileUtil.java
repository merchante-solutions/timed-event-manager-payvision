package com.mes.pach.proc;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * File generator utility functions.  Useful for formatting text file fields.
 */
public class FileUtil
{
  static Logger log = Logger.getLogger(FileUtil.class);
  
  public static final int PAD_RIGHT   = 0;
  public static final int PAD_LEFT    = 1;

  public static final int TRUNC_NONE  = 0;
  public static final int TRUNC_RIGHT = 1;
  public static final int TRUNC_LEFT  = 2;

  public static String format(String data, int len, String padding,
    int padDir, int truncType)
  {
    // replace null data with empty string
    data = (data == null ? "" : data);
    StringBuffer buf = new StringBuffer(data);

    // padding
    int insPos = (padDir == PAD_RIGHT ? data.length() : 0);
    while (buf.length() < len)
    {
      buf.insert(insPos,padding);
    }

    // truncation
    if (truncType == TRUNC_RIGHT)
    {
      buf.setLength(len);
    }
    else if (truncType == TRUNC_LEFT)
    {
      buf.delete(0,buf.length() - len);
    }

    return buf.toString();
  }

  public static String padRight(String data, int len, String padding)
  {
    return format(data,len,padding,PAD_RIGHT,TRUNC_RIGHT);
  }
  public static String padLeft(String data, int len, String padding)
  {
    return format(data,len,padding,PAD_LEFT,TRUNC_LEFT);
  }

  public static String padRight(String data, int len)
  {
    return padRight(data,len," ");
  }
  public static String padLeft(String data, int len)
  {
    return padLeft(data,len," ");
  }

  public static String spaces(int len)
  {
    return padLeft(null,len);
  }

  public static String zeroPad(String data, int len)
  {
    return padLeft(data,len,"0");
  }
  public static String zeroPad(int len)
  {
    return zeroPad(null,len);
  }

  public static String trim(String data, String padding, int padDir)
  {
    // replace null data with empty string
    data = (data == null ? "" : data);
    StringBuffer buf = new StringBuffer(data);

    // trim off the left
    if (padDir == PAD_LEFT)
    {
      while (buf.indexOf(padding) == 0)
      {
        buf.delete(0,padding.length());
      }
    }
    // trim off the right
    else if (padDir == PAD_RIGHT)
    {
      int startIdx = 0;
      while ((startIdx = buf.indexOf(padding)) == buf.length() - padding.length())
      {
        buf.delete(startIdx,padding.length());
      }
    }
    return buf.toString();
  }

  public static String trimZeroes(String data)
  {
    return trim(data,"0",PAD_LEFT);
  }

  public static String formatDate(Date date, String fmt)
  {
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat(fmt);
      return sdf.format(date);
    }
    catch (Exception e)
    {
      throw new RuntimeException("formatDate(date=" + date + ",fmt=" 
        + fmt + ")",e);
    }
  }

  public static String strip(String data, String neg)
  {
    StringBuffer buf = new StringBuffer(data);

    for (int i = buf.length() - 1; i >= 0; --i)
    {
      if (neg.indexOf(buf.charAt(i)) >= 0)
      {
        buf.deleteCharAt(i);
      }
    }
    return ""+buf;
  }
}