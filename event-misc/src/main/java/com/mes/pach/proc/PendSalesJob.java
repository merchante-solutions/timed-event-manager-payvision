package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchFileDetail;
import com.mes.pach.AchTransaction;
import com.mes.pach.CreditDebitCode;
import com.mes.pach.Destination;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;
import com.mes.pach.TranStatus;

/**
 * Pend new ACH sales transactions.
 *
 * Generates ACH file records for new ACH sales to debit customer accounts 
 * and move money into MES holding account.
 */

public class PendSalesJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(PendSalesJob.class);

  public static final int     BATCH_SIZE  = 100;

  public PendSalesJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public PendSalesJob(Run run)
  {
    super(run);
  }
  public PendSalesJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  private void pendNewSale(AchTransaction sale)
  {
    // generate customer debit file record
    AchFileDetail detail = new AchFileDetail();
    detail.setMerchNum(sale.getMerchNum());
    detail.setProfileId(sale.getProfileId());
    detail.setFtId(sale.getFtId());
    detail.setAccount(sale.getAccount());
    detail.setAccountId(sale.getCustId());
    detail.setAccountName(sale.getCustName());
    detail.setCdCode(CreditDebitCode.DEBIT);
    detail.setAmount(sale.getAmount());
    detail.setDestination(Destination.CUSTOMER);
    detail.setSourceId(sale.getTranId());
    detail.setSecCode(sale.getSecCode());
    detail.setEntryDescription(db.getEntryDescription(db.EDT_SALE));
    detail.setTestFlag(sale.getTestFlag());
    detail.setRecurCode(sale.isRecurring() ? "R" : "S");

    // set sales status to pending
    sale.setStatus(TranStatus.S_PEND);

    // insert record into ACHP_FILE_DETAILS
    // and update sale status in ACHP_TRANSACTIONS
    db.pendNewSale(sale,detail);
  }

  public void doJob()
  {
    List batch = db.getNewSalesToPend(BATCH_SIZE,sysTypeOpt,batchId);
    while (!batch.isEmpty())
    {
      for (Iterator i = batch.iterator(); i.hasNext();)
      {
        pendNewSale((AchTransaction)i.next());
      }
      batch = db.getNewSalesToPend(BATCH_SIZE,sysTypeOpt);
    }
  }
}