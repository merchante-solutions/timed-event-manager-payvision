package com.mes.pach.proc;

import java.util.Calendar;
import org.apache.log4j.Logger;

/**
 * Generate achp_returns records from newly loaded records in ach_*.
 */

public class LoadReturnsJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(LoadReturnsJob.class);

  public static final int     BATCH_SIZE  = 100;

  public LoadReturnsJob(boolean directFlag)
  {
    // only load production returns, test returns not applicable
    super(directFlag,false);
  }

  public void doJob()
  {
    db.loadNewReturns();
    (new ReturnNotificationSender(db))
      .sendNotifications(Calendar.getInstance().getTime());
  }
}