package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchFileDetail;
import com.mes.pach.AchTransaction;
import com.mes.pach.CreditDebitCode;
import com.mes.pach.Destination;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;
import com.mes.pach.TranStatus;

/**
 * Settles pending ACH credit transactions.
 *
 * Generates ACH file records for pending ACH credits to deposit funds in 
 * customer accounts and move money from MES holding account.
 */

public class SettleCreditsJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(SettleCreditsJob.class);

  public static final int     BATCH_SIZE  = 100;

  public SettleCreditsJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public SettleCreditsJob(Run run)
  {
    super(run);
  }
  public SettleCreditsJob(Run run, TranBatch batch) 
  {
    super(run,batch);
  }

  private void settlePendingCredit(AchTransaction credit)
  {
    // generate customer debit file record
    AchFileDetail detail = new AchFileDetail();
    detail.setMerchNum(credit.getMerchNum());
    detail.setProfileId(credit.getProfileId());
    detail.setFtId(credit.getFtId());
    detail.setAccount(credit.getAccount());
    detail.setAccountId(credit.getCustId());
    detail.setAccountName(credit.getCustName());
    detail.setCdCode(CreditDebitCode.CREDIT);
    detail.setAmount(credit.getAmount());
    detail.setDestination(Destination.CUSTOMER);
    detail.setSourceId(credit.getTranId());
    detail.setSecCode(credit.getSecCode());
    detail.setEntryDescription(db.getEntryDescription(db.EDT_CREDIT));
    detail.setTestFlag(credit.getTestFlag());
    detail.setRecurCode(credit.isRecurring() ? "R" : "S");

    // set credits status to pending
    credit.setStatus(TranStatus.C_SET);

    // insert record into ACHP_FILE_DETAILS
    // and update credit status in ACHP_TRANSACTIONS
    db.settlePendingCredit(credit,detail);
  }

  public void doJob()
  {
    List batch = db.getPendingCreditsToSettle(BATCH_SIZE,sysTypeOpt,batchId);
    while (!batch.isEmpty())
    {
      for (Iterator i = batch.iterator(); i.hasNext();)
      {
        settlePendingCredit((AchTransaction)i.next());
      }
      batch = db.getPendingCreditsToSettle(BATCH_SIZE,sysTypeOpt,batchId);
    }
  }
}
