package com.mes.pach.proc;

import java.util.Calendar;
import org.apache.log4j.Logger;

/**
 * Update post day count on all posted transactions.
 */

public class UpdatePostDaysJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(UpdatePostDaysJob.class);

  public static final int     BATCH_SIZE  = 100;

  public UpdatePostDaysJob(boolean directFlag)
  {
    super(directFlag,false);
  }

  private Calendar getTruncatedDate()
  {
    Calendar curCal = Calendar.getInstance();
    curCal.set(Calendar.HOUR,0);
    curCal.set(Calendar.MINUTE,0);
    curCal.set(Calendar.SECOND,0);
    curCal.set(Calendar.MILLISECOND,0);
    return curCal;
  }
    
  /**
   * NOTE: this will update post days on both test and production
   * transactions.
   */
  public void doJob()
  {
    // don't increment on non-business days
    // sat, sun and holidays will not count towards
    // post_days count
    if (!db.isBusinessDay())
    {
      return;
    }
    
    // retrieve last update date
    Calendar lastCal = Calendar.getInstance();
    lastCal.setTime(db.getLastPostUpdateAsDate());
    Calendar curCal = getTruncatedDate();

    // if post day update hasn't already happened today, do it
    if (curCal.after(lastCal))
    {
      db.incrementPostDays();
      db.setLastPostUpdate();
    }
  }
}