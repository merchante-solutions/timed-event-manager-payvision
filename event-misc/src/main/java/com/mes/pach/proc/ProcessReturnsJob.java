package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchReturn;
import com.mes.pach.AchTransaction;
import com.mes.pach.Destination;
import com.mes.pach.FundingEntry;
import com.mes.pach.FundingEntryType;
import com.mes.pach.MerchProfile;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;
import com.mes.pach.TranStatus;

/**
 * Process incoming returns.
 */

public class ProcessReturnsJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(ProcessReturnsJob.class);

  public static final int     BATCH_SIZE  = 100;

  public ProcessReturnsJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public ProcessReturnsJob(Run run)
  {
    super(run);
  }
  public ProcessReturnsJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  /**
   * Posted Sale: cust > MES ACH generated return before settlement to 
   * merchant occurred.  
   *
   *  1) SF Entries Sale + Ret. Sale Adj. (net 0 to merch)
   *  2) tran status := S_RET_U
   *  3) log pending sale return event
   */
  private void processPostedSaleReturn(AchReturn ret, AchTransaction tran)
    throws Exception
  {
    // create sale entry
    FundingEntry entry = new FundingEntry();
    entry.setProfileId(tran.getProfileId());
    entry.setMerchNum(tran.getMerchNum());
    entry.setFtId(tran.getFtId());
    entry.setTranId(tran.getTranId());
    entry.setEntryType(FundingEntryType.SALE);
    entry.setAmount(tran.getAmount());
    entry.setTestFlag(tran.getTestFlag());
    db.insertFundingEntry(entry);

    // create returned sale adjustment sf entry
    entry = new FundingEntry();
    entry.setProfileId(tran.getProfileId());
    entry.setMerchNum(tran.getMerchNum());
    entry.setFtId(tran.getFtId());
    entry.setTranId(tran.getTranId());
    entry.setEntryType(FundingEntryType.SALE_RET_ADJ);
    entry.setAmount(tran.getAmount());
    entry.setTestFlag(tran.getTestFlag());
    db.insertFundingEntry(entry);

    // set trans status to unsettled sale return
    tran.setStatus(TranStatus.S_RET_U);
    db.updateAchTransactionStatus(tran);

    // TODO: log unsettled sale return event
  }

  /**
   * Settled Sale: cust > MES ACH generated return AFTER settlement to
   * merchant occurred.
   *
   *  1) SF Entry Ret. Sale Adj. (pull sale amt back from merch)
   *  2) tran status := S_RET_S
   *  3) log settled sale return event
   */
  private void processSettledSaleReturn(AchReturn ret, AchTransaction tran)
    throws Exception
  {
    // create returned sale adjustment sf entry
    FundingEntry entry = new FundingEntry();
    entry.setProfileId(tran.getProfileId());
    entry.setMerchNum(tran.getMerchNum());
    entry.setFtId(tran.getFtId());
    entry.setTranId(tran.getTranId());
    entry.setEntryType(FundingEntryType.SALE_RET_ADJ);
    entry.setAmount(tran.getAmount());
    entry.setTestFlag(tran.getTestFlag());
    db.insertFundingEntry(entry);

    // set trans status to settled sale return
    tran.setStatus(TranStatus.S_RET_S);
    db.updateAchTransactionStatus(tran);

    // TODO: log settled sale return event
  }

  /**
   * Settled Credit: MES > cust ACH generated return.
   *
   *  1) SF Entry Ret. Credit Adj. (push credit amt back to merch)
   *  2) tran status := C_RET
   *  3) log settled credit return event
   */
  private void processSettledCreditReturn(AchReturn ret, AchTransaction tran)
    throws Exception
  {
    // create returned credit adjustment sf entry
    FundingEntry entry = new FundingEntry();
    entry.setProfileId(tran.getProfileId());
    entry.setMerchNum(tran.getMerchNum());
    entry.setFtId(tran.getFtId());
    entry.setTranId(tran.getTranId());
    entry.setEntryType(FundingEntryType.CREDIT_RET_ADJ);
    entry.setAmount(tran.getAmount());
    entry.setTestFlag(tran.getTestFlag());
    db.insertFundingEntry(entry);

    // set trans status to returned
    tran.setStatus(TranStatus.C_RET);
    db.updateAchTransactionStatus(tran);

    // TODO: log credit return event
  }

  /**
   * Customer return processing.
   *
   * Handle three anticipated return scenarior: pending sales, settled
   * sales and settled credits.
   */
  private void processCustomerReturn(AchReturn ret)
  {
    // get transaction
    AchTransaction tran = db.getAchTransaction(ret.getTranId());
    if (tran == null)
    {
      throw new RuntimeException("Transaction not found for return" 
        + " with tranId: " + ret.getTranId());
    }

    boolean opOk = false;
    try
    {
      db.startTransaction();

      TranStatus status = tran.getStatus();
      if (TranStatus.S_POST.equals(status))
      {
        processPostedSaleReturn(ret,tran);
      }
      else if (TranStatus.S_SET.equals(status))
      {
        processSettledSaleReturn(ret,tran);
      }
      else if (TranStatus.C_SET.equals(status))
      {
        processSettledCreditReturn(ret,tran);
      }
      else
      {
        // TODO: log unhandled return event
      }

      // flag return as processed
      db.markReturnProcessed(ret);

      db.commitTransaction();
      opOk = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("processCustomerReturn(ret=" + ret + ")",e);
    }
    finally
    {
      if (!opOk)
      {
        try 
        {
          db.rollbackTransaction();
        }
        catch (Exception e) 
        {
          log.error("Error attempting rollback in processCustomerReturn"
            + " for " + ret);
        }
      }
    }
  }

  /**
   * Merchant return processing.
   *
   * Currently the only action taken on a merchant ACH return is to put
   * the merchant account on hold.
   */
  private void processMerchantReturn(AchReturn ret)
  {
    boolean opOk = false;
    try
    {
      db.startTransaction();
      
      // place merchant activity on hold
      MerchProfile pro = db.getMerchProfile(ret.getProfileId());
      pro.setHoldFlag(MerchProfile.FLAG_YES);
      db.updateMerchProfile(pro);

      // flag return as processed
      db.markReturnProcessed(ret);

      // TODO: log this event

      db.commitTransaction();
      opOk = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("processMerchantReturn(ret=" + ret + ")",e);
    }
    finally
    {
      if (!opOk)
      {
        try 
        {
          db.rollbackTransaction();
        }
        catch (Exception e) 
        {
          log.error("Error attempting rollback in processMerchantReturn"
            + " for " + ret);
        }
      }
    }
  }

  public void doJob()
  {
    List batch = db.getUnprocessedReturns(BATCH_SIZE,sysTypeOpt,batchId);
    while (!batch.isEmpty())
    {
      for (Iterator i = batch.iterator(); i.hasNext();)
      {
        AchReturn ret = (AchReturn)i.next();
        if (Destination.CUSTOMER.equals(ret.getDestination()))
        {
          processCustomerReturn(ret);
        }
        else
        {
          processMerchantReturn(ret);
        }
      }
      batch = db.getUnprocessedReturns(BATCH_SIZE,sysTypeOpt,batchId);
    }
  }
}