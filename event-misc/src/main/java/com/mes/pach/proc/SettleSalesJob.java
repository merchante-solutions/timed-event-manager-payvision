package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchTransaction;
import com.mes.pach.FundingEntry;
import com.mes.pach.FundingEntryType;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;
import com.mes.pach.TranStatus;

/**
 * Settles pending ACH sales transactions.
 *
 * Generates merchant settlement funding entries for eligible pending sales 
 * transaction to fund merchant accounts for sales.
 */

public class SettleSalesJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(SettleSalesJob.class);

  public static final int     BATCH_SIZE  = 100;

  public SettleSalesJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public SettleSalesJob(Run run)
  {
    super(run);
  }
  public SettleSalesJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  private void settlePendingSale(AchTransaction sale)
  {
    // generate merchant settlement funding entry
    FundingEntry entry = new FundingEntry();
    entry.setProfileId(sale.getProfileId());
    entry.setFtId(sale.getFtId());
    entry.setMerchNum(sale.getMerchNum());
    entry.setTranId(sale.getTranId());
    entry.setEntryType(FundingEntryType.SALE);
    entry.setAmount(sale.getAmount());
    entry.setTestFlag(sale.getTestFlag());

    // set sales status to settled
    sale.setStatus(TranStatus.S_SET);

    // insert entry into achp_sf_entries
    // and update sale status in achp_transactions
    db.settlePendingSale(sale,entry);
  }

  public void doJob()
  {
    List batch = db.getPendingSalesToSettle(BATCH_SIZE,sysTypeOpt,batchId);
    while (!batch.isEmpty())
    {
      for (Iterator i = batch.iterator(); i.hasNext();)
      {
        settlePendingSale((AchTransaction)i.next());
      }
      batch = db.getPendingSalesToSettle(BATCH_SIZE,sysTypeOpt,batchId);
    }
  }
}