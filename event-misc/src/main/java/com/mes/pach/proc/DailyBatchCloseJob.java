package com.mes.pach.proc;

import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.pach.Run;

/**
 * Close all open batches in preparation for daily processing.
 *
 * All open batches (test or production depending on test flag) are closed
 * and the profile batch number is incremented.  The run ID is assigned
 * to all closed batches so further processing can use the run ID as a 
 * reference.
 */

public class DailyBatchCloseJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(DailyBatchCloseJob.class);

  private Date cutoffDate;

  public DailyBatchCloseJob(Run run, Date cutoffDate)
  {
    super(run);
  }

  public void doJob()
  {
    db.closeAllBatches(sysTypeOpt,runId,cutoffDate);
  }
}
