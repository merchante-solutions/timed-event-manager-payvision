package com.mes.pach.proc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.pach.AchFile;
import com.mes.pach.AchFileBatch;
import com.mes.pach.AchFileDetail;
import com.mes.pach.AchFileType;
import com.mes.pach.Destination;
import com.mes.pach.PachDb;
import com.mes.pach.SecCode;
import com.mes.support.LoggingConfigurator;

public class AchFileGenerator {

  static { LoggingConfigurator.requireConfiguration(); } // initialize log4j
  static Logger log = Logger.getLogger(AchFileGenerator.class);
  
  static { DbProperties.requireConfiguration(); } // initialize database properties
  
  private PachDb db = null;
  private AchFile achFile = null;
  
  public class FileState 
  {
    // file accumulators
    public int            fileBatchCount;
    public int            fileRecordCount;
    public int            fileDetailCount;
    public int            fileAddendumCount;
    public int            fileCreditCount;
    public BigDecimal     fileCreditAmount;
    public int            fileDebitCount;
    public BigDecimal     fileDebitAmount;
    public long           fileDetailHash;

    // batch accumulators
    public int            batchRecordCount;
    public int            batchDetailCount;
    public int            batchAddendumCount;
    public int            batchCreditCount;
    public BigDecimal     batchCreditAmount;
    public int            batchDebitCount;
    public BigDecimal     batchDebitAmount;
    public long           batchDetailHash;

    public String         traceNumPrefix;

    public void startFile(AchFile file)
    {
      fileBatchCount = 0;
      fileRecordCount = 0;
      fileDetailCount = 0;
      fileAddendumCount = 0;
      fileCreditCount = 0;
      fileCreditAmount = new BigDecimal(0).setScale(2);
      fileDebitCount = 0;
      fileDebitAmount = new BigDecimal(0).setScale(2);
      fileDetailHash = 0;

      ++fileRecordCount;

      traceNumPrefix = file.getOriginId().substring(1);
      if (traceNumPrefix.length() > 8)
      {
        traceNumPrefix = traceNumPrefix.substring(0,8);
      }
    }

    /**
     * Resets batch accumulators, stores the file records auth type
     * and profile ID to allow detecting end of batch.
     */
    public void startBatch()
    {
      batchRecordCount    = 0;
      batchDetailCount    = 0;
      batchAddendumCount  = 0;
      batchCreditCount    = 0;
      batchCreditAmount   = new BigDecimal(0).setScale(2);
      batchDebitCount     = 0;
      batchDebitAmount    = new BigDecimal(0).setScale(2);
      batchDetailHash     = 0;

      ++fileRecordCount;
      ++batchRecordCount;
    }

    /**
     * Accumulate some data from the file record for the batch and
     * file control records.
     */
    public void processDetail(AchFileDetail detail)
    {
      ++fileRecordCount;
      ++fileDetailCount;
      ++batchRecordCount;
      ++batchDetailCount;

      if (detail.isDebit())
      {
        ++batchDebitCount;
        batchDebitAmount = batchDebitAmount.add(detail.getAmount());
        ++fileDebitCount;
        fileDebitAmount = fileDebitAmount.add(detail.getAmount());
      }
      else if (detail.isCredit())
      {
        ++batchCreditCount;
        batchCreditAmount = batchCreditAmount.add(detail.getAmount());
        ++fileCreditCount;
        fileCreditAmount = fileCreditAmount.add(detail.getAmount());
      }

      // batch hash is the sum of first 8 digits of each detail's 
      // transit routing number (drop transit num's check digit 9)
      batchDetailHash += 
        Long.valueOf(detail.getTransitNum().substring(0,8)).longValue();
    }

    public void endBatch()
    {
      ++fileRecordCount;
      ++batchRecordCount;
      ++fileBatchCount;

      // file hash is sum of batch hashes
      fileDetailHash += batchDetailHash;
    }

    public void endFile()
    {
      ++fileRecordCount;
    }

    public int getBlockCount()
    {
      // file record count divided by 10, 
      // rounded up to nearest integer
      return (fileRecordCount + 9) / 10;
    }
  }

  public AchFileGenerator(PachDb db, AchFile achFile) {
    this.db = db;
    this.achFile = achFile;
  }

  private String getFileName(Destination destination)
  {
    return ""+destination + "_test.txt";
  }
  
  /**
   * Wells Fargo requires a "security header" (see FLAT_FILE_DEF def_type=156
   * used by AchEvent to generate security header):
   *
   *  $$ADD ID=KNYCDAF6 BID='NWFACHWFMSACHM'
   *
   *  ID is hard coded, also called 'user' by FLAT_FILE_DEF
   *
   *  BID starts with NWFACH then uses first 8 digits of ORIGIN_ID in 
   *  ACH_TRIDENT_DESTINATIONS ('[WFMSACHM]ES'), also called application_id
   *  by FLAT_FILE_DEF
   */
  private void generateSecurityHeader(BufferedWriter out, AchFileType fileType) 
    throws Exception
  {
    StringBuffer buf = new StringBuffer();
    
    buf.append("$$ADD ID=");
    
    buf.append(fileType.getSecHdrUserId());
    
    buf.append(" BID=");
    
    buf.append("'" + fileType.getSecHdrAppId() + "'");
    
    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }

  private void generateFileHeader(BufferedWriter out, FileState state, 
    AchFile file) throws Exception
  {
    state.startFile(file);

    StringBuffer buf = new StringBuffer();

    // file record type
    buf.append("1");

    // priority code
    buf.append("01");

    // immediate destination (destination id)
    buf.append(FileUtil.padLeft(file.getDestinationId(),10));

    // immediate origin (origin id)
    buf.append(FileUtil.padLeft(file.getOriginId(),10));

    // file creation date, file creation time YYMMDDHHMI
    buf.append(FileUtil.formatDate(file.getCreateDate(),"yyMMddhhmm"));

    // file id modifier
    buf.append("A");

    // record size
    buf.append("094");

    // blocking factor
    buf.append("10");

    // format code
    buf.append("1");

    // immediate destination name
    buf.append(FileUtil.padRight(file.getDestinationName(),23));

    // immediate origin name
    buf.append(FileUtil.padRight(file.getOriginName(),23));

    // reference code
    buf.append(FileUtil.padRight("",8));

    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }

  private void generateBatchHeader(BufferedWriter out, FileState state, 
    AchFileBatch batch) throws Exception
  {
    state.startBatch();

    StringBuffer buf = new StringBuffer();

    // file record type
    buf.append("5");

    // service class code
    buf.append(FileUtil.zeroPad(""+batch.getClassCode(),3));

    // company name
    buf.append(FileUtil.padRight(batch.getCompanyName(),16));

    // company discretionary data (blank)
    buf.append(FileUtil.padRight("",20));

    // company id
    buf.append(FileUtil.padLeft(batch.getCompanyId(),10));

    // sec code
    buf.append(FileUtil.padRight(""+batch.getSecCode(),3));

    // entry description
    buf.append(FileUtil.padRight(batch.getEntryDescription(),10));

    // descriptive date
    // HACK: deposit report loader needs this to be in the same format
    // as effective entry date, so use it instead here...
    //buf.append(FileUtil.padRight(batch.getDescriptiveDate(),6));
    buf.append(FileUtil.padRight(batch.getEffectiveEntryDate(),6));

    // effective entry date
    buf.append(FileUtil.padRight(batch.getEffectiveEntryDate(),6));

    // julian date (leave blank)
    buf.append(FileUtil.padRight("",3));

    // originator status code, '1'
    buf.append("1");

    // originating dfi id
    buf.append(FileUtil.zeroPad(batch.getOriginDfiId(),8));

    // batch number
    buf.append(FileUtil.zeroPad(""+batch.getBatchNum(),7));

    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }

  /**
   * Determines tran code from credit/debit indicator and account
   * type of detail.  Currently supported:
   * 
   *  22 - checking deposit
   *  27 - checking debit
   *  32 - savings deposit
   *  37 - savings debit
   *
   * Some other unsupported codes:
   *
   *  23 - checking deposit prenote
   *  28 - checking debit prenote
   *  33 - savings deposit prenote
   *  38 - savings debit prenote
   */
  private String getTranCode(AchFileDetail detail)
  {
    // checking account tran codes
    if (detail.isCheckingAccount())
    {
      return detail.isDebit() ? "27" : "22";
    }

    // savings account tran codes
    return detail.isDebit() ? "37" : "32";
  }

  private String getFormattedAmount(BigDecimal amount, int len)
  {
    return FileUtil.zeroPad(FileUtil.strip(""+amount,"."),len);
  }

  private void generateDetail(BufferedWriter out, FileState state,
    AchFileDetail detail) throws Exception
  {
    state.processDetail(detail);

    StringBuffer buf = new StringBuffer();

    // file record type
    buf.append("6");

    // transaction code
    buf.append(FileUtil.zeroPad(getTranCode(detail),2));

    // transit routing number (including check digit 9)
    buf.append(FileUtil.zeroPad(detail.getTransitNum(),9));

    // account number
    buf.append(FileUtil.padRight(detail.getAccount().clearText(),17));

    // dollar amount 
    buf.append(getFormattedAmount(detail.getAmount(),10));

    // receiver id
    // 10-15-2010 THIS NEEDS TO BE ZERO FILLED FOR THE ACH RETURN FILE LOADER
    //            buf.append(FileUtil.padRight(detail.getAccountId(),15));
    //
    // 1-11-2011 Zero fill this for customer ACHs, put merchant number in for
    //           merchant deposit ACHs
    //
    // TODO: nach files (customer) do not actually get loaded any longer, so 
    //       ideally we should be able to put the customer id in this field
    //       but would need to be able to handle the returns for customer achs
    //       (perhaps this can be addressed by fixing the triggers on 
    //        ach_rejects table to ignore nach returns with trace number 
    //        matches to the achp system)
    if (Destination.CUSTOMER.equals(detail.getDestination()))
    {
      buf.append(FileUtil.zeroPad("0",15));
    }
    else
    {
      buf.append(FileUtil.zeroPad(detail.getMerchNum(),15));
    }

    // receiver name
    buf.append(FileUtil.padRight(detail.getAccountName(),22));

    // WEB transactions 'R' = recurring, 'S' = single entry
    // all other, blank
    if (detail.getSecCode().equals(SecCode.WEB))
    {
      buf.append(FileUtil.padRight(detail.getRecurCode(),2));
    }
    else
    {
      buf.append("  ");
    }

    // addenda record indicator 1=yes, 0=no
    buf.append("0");

    // trace number, currently we are using middle 8 digits of origin id
    // followed by first 7 digits of ach_trident_trace_sequence
    buf.append(FileUtil.zeroPad(detail.getTraceNum(),15));

    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }

  private void generateBatchControl(BufferedWriter out, FileState state, 
    AchFileBatch batch) throws Exception
  {
    state.endBatch();

    StringBuffer buf = new StringBuffer();

    // file record type
    buf.append("8");

    // service class code
    buf.append(FileUtil.zeroPad(""+batch.getClassCode(),3));

    // batch detail/addendum count 
    buf.append(FileUtil.zeroPad(
      ""+(state.batchDetailCount + state.batchAddendumCount),6));

    // batch detail hash
    buf.append(FileUtil.zeroPad(""+state.batchDetailHash,10));

    // batch debit total
    buf.append(getFormattedAmount(state.batchDebitAmount,12));

    // batch credit total
    buf.append(getFormattedAmount(state.batchCreditAmount,12));
    
    // company id
    buf.append(FileUtil.padLeft(batch.getCompanyId(),10));

    // message authentication code (blank)
    buf.append(FileUtil.padRight("",19));

    // reserved (blank)
    buf.append(FileUtil.padRight("",6));

    // originating dfi id
    buf.append(FileUtil.zeroPad(batch.getOriginDfiId(),8));

    // batch number
    buf.append(FileUtil.zeroPad(""+batch.getBatchNum(),7));

    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }

  private void generateFileControl(BufferedWriter out, FileState state,
    AchFile file) throws Exception
  {
    state.endFile();

    StringBuffer buf = new StringBuffer();

    // file record type
    buf.append("9");

    // batch count
    buf.append(FileUtil.zeroPad(""+state.fileBatchCount,6));

    // block count
    buf.append(FileUtil.zeroPad(""+state.getBlockCount(),6));

    // file detail/addendum count
    buf.append(FileUtil.zeroPad(
      ""+(state.fileDetailCount + state.fileAddendumCount),8));

    // file detail hash
    buf.append(FileUtil.zeroPad(""+state.fileDetailHash,10));

    // file debit total
    buf.append(getFormattedAmount(state.fileDebitAmount,12));

    // file credit total
    buf.append(getFormattedAmount(state.fileCreditAmount,12));

    // reserved (blank)
    buf.append(FileUtil.padRight("",39));

    log.debug(""+buf);
    out.write(""+buf);
    out.newLine();
  }
  
  public File _generateFile() throws Exception {

    File outFile = null;
    BufferedWriter out = null;
    
    try {
    
      db.connect();
    
      // start a new file
      FileState state = new FileState();
      String fileName = "nacha-" + achFile.getFileId() + ".txt";
      outFile = new File(fileName);
      out = new BufferedWriter(new FileWriter(outFile));
      
      // fetch file type to handle security header
      AchFileType fileType = db.getAchFileType(achFile.getFtId());
      if (fileType.requiresSecurityHeader()) {
        generateSecurityHeader(out,fileType);
      }
      
      // file header
      generateFileHeader(out,state,achFile);

      // get file's batches
      List batches = db.getAchBatches(achFile.getFileId());
      for (Iterator b = batches.iterator(); b.hasNext();) {
      
        // start batch
        AchFileBatch batch = (AchFileBatch)b.next();
        generateBatchHeader(out,state,batch);

        // iterate through batch details
        AchFileDetail detail = null;
        try {
        
          db.startFileDetailFetch(batch.getBatchId());
          while ((detail = db.fetchNextFileDetail()) != null) {
          
            generateDetail(out,state,detail);
          }
        }
        finally {
        
          db.endFileDetailFetch();
        }

        // end batch
        generateBatchControl(out,state,batch);
      }

      // end file
      generateFileControl(out,state,achFile);
    }
    finally {

      // close output writer
      try { out.flush(); } catch (Exception e) { }
      try { out.close(); } catch (Exception e) { }
      db.cleanUp();
    }
    
    return outFile;
  }
  
  public static File generateFile(PachDb db, AchFile achFile) 
    throws Exception
  {
    return (new AchFileGenerator(db,achFile))._generateFile();
  }
  
  public static void main(String[] args) {
  
    try {
    
      if (args.length < 1) {
      
        throw new RuntimeException("No ACH file id provided");
      }
      
      long fileId = Long.parseLong(args[0]);
      PachDb db = new PachDb();
      AchFile achFile = db.getAchFile(fileId);
      File theFile = generateFile(db,achFile);
      log.debug("ACH file " + theFile + " generated");
    }
    catch (Exception e) {
    
      log.error("Error generating ACH file",e);
      System.exit(-1);
    }
    
    System.exit(0);
  }

}