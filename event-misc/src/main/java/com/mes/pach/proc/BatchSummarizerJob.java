package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;

/**
 * Summarize a single batch (test only).  Used to simulate processing 
 * during testing.
 */

public class BatchSummarizerJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(BatchSummarizerJob.class);

  public BatchSummarizerJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  public void doJob()
  {
    // use batch id to determine profile id
    TranBatch batch = db.getTranBatch(batchId);
    if (batch == null)
    {
      throw new NullPointerException("Invalid batch ID " + batchId);
    }
    if (!batch.isTest())
    {
      throw new RuntimeException("Cannot summarize production batch " + batchId);
    }
    db.summarizeProfileBatch(
      db.getDbDate(),batch.getProfileId(),sysTypeOpt,batchId);
  }
}

