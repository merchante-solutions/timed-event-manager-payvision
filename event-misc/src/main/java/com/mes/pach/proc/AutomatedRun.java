package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Run;

public abstract class AutomatedRun extends AutomatedProcess implements Run
{
  static Logger log = Logger.getLogger(AutomatedRun.class);

  public static final int MAX_MSG_LEN = 300;

  protected String userName;
  protected String runRc = RRC_OK;

  public AutomatedRun(boolean directFlag, boolean testFlag, long batchId,
    String userName)
  {
    super(directFlag,testFlag,-1L,batchId);
    this.userName = userName;
  }
  public AutomatedRun(boolean directFlag, boolean testFlag, String userName)
  {
    super(directFlag,testFlag,-1L,-1L);
    this.userName = userName;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setRunRc(String runRc)
  {
    this.runRc = runRc;
  }
  public String getRunRc()
  {
    return runRc;
  }

  public void handleError(Exception e)
  {
    runRc = RRC_ERROR;
    message = e.getMessage();
    if (message != null && message.length() > MAX_MSG_LEN)
    {
      message = message.substring(0,MAX_MSG_LEN);
    }
  }

  public abstract void doRun();

  public final void doProcess()
  {
    doRun();
  }

  public void endProcess()
  {
    db.insertRun(this);
  }
}