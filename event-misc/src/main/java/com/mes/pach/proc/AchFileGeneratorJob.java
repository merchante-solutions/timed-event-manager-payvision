package com.mes.pach.proc;

import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchFile;
import com.mes.pach.Destination;
import com.mes.pach.Run;

/**
 * Generates ACH Files
 *
 * Generates files from detail records in ACHP_FILE_DETAILS that do not
 * have a file_id assigned to them.  Two types of files are generated,
 * a merchant file containing ACH details to handle the daily settlement
 * activity (moves money between MES and merchant accounts) and a customer 
 * file which contains ACH details to move money between MES and merchants'
 * customers.
 */

public class AchFileGeneratorJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(AchFileGeneratorJob.class);

  public AchFileGeneratorJob(Run run)
  {
    super(run);
  }

  private void generateFiles() 
  {
    boolean opSuccess = false;

    try
    {
      db.startTransaction();
      
      // get list of files needing to be generated
      List files = db.getInitializedAchFiles(sysTypeOpt);
      
      // build and upload each file
      for (Iterator i = files.iterator(); i.hasNext();) {
      
        File outFile = null;
        FileInputStream in = null;
        
        // process an ach file 
        AchFile achFile = (AchFile)i.next();
        
        try {
        
          // generate the file locally on disk
          outFile = AchFileGenerator.generateFile(db,achFile);
          
          // upload the file data to the database file entry
          in = new FileInputStream(outFile);
          db.uploadAchFileData(achFile,in);
        }
        catch (Exception e) {
        
          log.error("Error generating ach file",e);
        }
        finally {
        
          try { in.close(); } catch (Exception e) { }
        }
        
        // delete the local file
        outFile.delete();
      }
        
      db.commitTransaction();
      opSuccess = true;
    }
    catch (Exception e)
    {
      throw new RuntimeException("generateFiles()",e);
    }
    finally
    {
      if (!opSuccess)
      {
        try
        {
          db.rollbackTransaction();
          log.error("ACH file generation rolled back.");
        }
        catch (Exception e)
        {
          log.error("Unable to perform rollback: " + e);
        }
      }
    }
  }

  public void doJob()
  {
    // using the current time, prep merchant 
    // and customer files for generation
    Date cutoff = Calendar.getInstance().getTime();
    db.initiateAchFiles(Destination.MERCHANT,cutoff,sysTypeOpt);
    db.initiateAchFiles(Destination.CUSTOMER,cutoff,sysTypeOpt);

    // generate the initialized files
    generateFiles();
  }
  
  
}