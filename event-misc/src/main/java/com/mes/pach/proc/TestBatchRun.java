package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Job;
import com.mes.pach.TranBatch;

/**
 * Runs full cycle of processing on a single profile test batch of 
 * transactions.  Simulates multiple days worth of processing in one
 * quick run.
 */
public class TestBatchRun extends AutomatedRun
{
  static Logger log = Logger.getLogger(TestBatchRun.class);

  private TranBatch batch;

  public TestBatchRun(boolean directFlag, long batchId, String userName)
  {
    super(directFlag,true,batchId,userName);
  }

  private void loadBatch()
  {
    // use batch id to determine profile id
    batch = db.getTranBatch(batchId);
    if (batch == null)
    {
      throw new NullPointerException("Batch not found for " + batchId);
    }
    if (!batch.isTest())
    {
      throw new RuntimeException("Cannot summarize production batch " + batchId);
    }
    //if (batch.isClosed())
    //{
    //  throw new RuntimeException("Cannot run test batch processing on "
    //    + "closed batch with ID " + batchId);
    //}
  }

  private void closeBatch()
  {
    if (!batch.isClosed())
    {
      log.debug("closing batch");
      db.closeTranBatch(batchId);
    }
    else
    {
      log.debug("batch already closed");
    }
  }

  private void runJob(Job job, String message)
  {
    job.setMessage(message);
    job.run();
    if (!Job.JRC_OK.equals(job.getJobRc()))
    {
      throw new RuntimeException("Error running job " + job);
    }
  }

  private void pendSales(String message)
  {
    log.debug("pending sales");
    runJob(new PendSalesJob(this,batch),message);
  }

  private void pendCredits(String message)
  {
    log.debug("pending credits");
    runJob(new PendCreditsJob(this,batch),message);
  }

  private void summarizeBatch(String message)
  {
    log.debug("summarizing batch");
    runJob(new BatchSummarizerJob(this,batch),message);
  }    

  private void createReturns(String desc, String message)
  {
    log.debug("generating " + desc + " returns");
    runJob(new TestReturnsGeneratorJob(this,batch),message);
    log.debug("processing " + desc + " returns");
    runJob(new ProcessReturnsJob(this,batch),message);
  }

  private void settleSales(String message)
  {
    log.debug("settling sales");
    runJob(new SettleSalesJob(this,batch),message);
  }

  private void settleCredits(String message)
  {
    log.debug("settling credits");
    runJob(new SettleCreditsJob(this,batch),message);
  }

  /**
   * Simulates full processing cycle on a batch of transactions.
   *
   * Normally this would take place over multiple days, need to
   * accelerate the process here, but still do things in the correct
   * order to create opportunities for various types of returns.
   *
   */
  public void doRun()
  {
    // make sure batch exists
    loadBatch();

    // close batch, new transactions will
    // go in next batch after this
    closeBatch();

    // pend sales to generate
    // customer bound ACH file records
    pendSales("Day 1, generate customer sale ACHs");

    // pend credits to generate sf entries
    pendCredits("Day 1, generate settlement credit entries");

    // summarize sf entries to generate 
    // merchant bound ACH file records
    // for pended credits
    summarizeBatch("End of Day 1, summarize settlement entries (pended credits)");

    // pre-settlement returns for pended sales
    createReturns("pending","Day 2-4, process pre-settlement returns");

    // settle pending sales to generate
    // sf entries
    settleSales("Day 4, generate settlement sale entries");

    // settle pending credits to generate
    // customer bound ach file records
    settleCredits("Day 4, generate customer credit ACHs");

    // summarize sf entries to generate
    // merchant bound ACH file records
    summarizeBatch("End of Day 4, summarize settlement entries (settled sales, return adjustments)");

    // post-settlement returns for settled credits
    createReturns("settled","Day 5+, process post-settlement returns");

    // summarize sf entries from any
    // post-settlement returns
    summarizeBatch("End of Day 5+, process post settlement return adjustments");

    log.debug("done");
  }
}