package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Job;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;

public abstract class AutomatedJob extends AutomatedProcess implements Job
{
  static Logger log = Logger.getLogger(AutomatedJob.class);

  public static final int MAX_MSG_LEN = 300;

  protected long jobId;
  protected String jobName;
  protected String jobRc = JRC_OK;

  public AutomatedJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag,-1L,-1L);
  }
  public AutomatedJob(Run run)
  {
    super(run.isDirect(),run.isTest(),run.getRunId(),-1L);
  }
  public AutomatedJob(Run run, TranBatch batch)
  {
    super(run.isDirect(),run.isTest(),run.getRunId(),batch.getBatchId());
  }

  public void setJobId(long jobId)
  {
    this.jobId = jobId;
  }
  public long getJobId()
  {
    return jobId;
  }

  public void setJobName(String jobName)
  {
    this.jobName = jobName;
  }
  public String getJobName()
  {
    return (jobName != null ? jobName : getProcessName());
  }

  public void setJobRc(String jobRc)
  {
    this.jobRc = jobRc;
  }
  public String getJobRc()
  {
    return jobRc;
  }

  public void handleError(Exception e)
  {
    jobRc = JRC_ERROR;
    message = e.getMessage();
    if (message != null && message.length() > MAX_MSG_LEN)
    {
      message = message.substring(0,MAX_MSG_LEN);
    }
  }

  public abstract void doJob();

  public final void doProcess()
  {
    doJob();
  }

  public void endProcess()
  {
    db.insertJob(this);
  }
}