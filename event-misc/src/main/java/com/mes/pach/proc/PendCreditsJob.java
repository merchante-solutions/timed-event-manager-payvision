package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchTransaction;
import com.mes.pach.FundingEntry;
import com.mes.pach.FundingEntryType;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;
import com.mes.pach.TranStatus;

/**
 * Pend new ACH credit transactions.
 *
 * Generates merchant settlement funding for new ACH credits to debit 
 * merchant accounts and move money into MES holding account.
 */

public class PendCreditsJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(PendCreditsJob.class);

  public static final int     BATCH_SIZE  = 10;

  public PendCreditsJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public PendCreditsJob(Run run)
  {
    super(run);
  }
  public PendCreditsJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  private void pendNewCredit(AchTransaction credit)
  {
    // generate merchant settlement funding entry
    FundingEntry entry = new FundingEntry();
    entry.setProfileId(credit.getProfileId());
    entry.setMerchNum(credit.getMerchNum());
    entry.setFtId(credit.getFtId());
    entry.setTranId(credit.getTranId());
    entry.setEntryType(FundingEntryType.CREDIT);
    entry.setAmount(credit.getAmount());
    entry.setTestFlag(credit.getTestFlag());

    // set credits status to settled
    credit.setStatus(TranStatus.C_PEND);

    // insert entry into ACHP_SF_ENTRIES
    // and update credit status in ACHP_TRANSACTIONS
    db.pendNewCredit(credit,entry);
  }

  public void doJob()
  {
    List batch = db.getNewCreditsToPend(BATCH_SIZE,sysTypeOpt,batchId);
    while (!batch.isEmpty())
    {
      for (Iterator i = batch.iterator(); i.hasNext();)
      {
        pendNewCredit((AchTransaction)i.next());
      }
      batch = db.getNewCreditsToPend(BATCH_SIZE,sysTypeOpt);
    }
  }
}