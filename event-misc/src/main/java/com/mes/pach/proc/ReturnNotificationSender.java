package com.mes.pach.proc;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchBase;
import com.mes.pach.Notifier;
import com.mes.pach.PachDb;

public class ReturnNotificationSender extends AchBase
{
  static Logger log = Logger.getLogger(AutomatedJob.class);

  private PachDb db;

  public ReturnNotificationSender(PachDb db)
  {
    this.db = db;
  }

  /**
   * Notify merchants of customer returns and nocs.
   */
  public void sendMerchNotifications(Date recvDate)
  {
    List retSums = db.getCustReturnSummaries(recvDate);
    Notifier notifier = new Notifier(db.isDirect());
    for (Iterator i = retSums.iterator(); i.hasNext();)
    {
      PachDb.CustReturnSummary summary
        = (PachDb.CustReturnSummary)i.next();
      notifier.notifyMerchOfCustReturn(summary);
    }
  }
  
  /**
   * Notify risk of merchant settlement returns and nocs.
   */
  public void sendRiskNotifications(Date recvDate)
  {
    List items = db.getMerchReturnItems(recvDate);
    Notifier notifier = new Notifier(db.isDirect());
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      PachDb.MerchReturnItem item
        = (PachDb.MerchReturnItem)i.next();
      notifier.notifyRiskOfMerchReturn(item);
    }
  }    

  public void sendNotifications(Date recvDate)
  {
    sendMerchNotifications(recvDate);
    sendRiskNotifications(recvDate);
  }
}