package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.Run;

/**
 * Summarize new settlement funding entries.
 */

public class SummarizerJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(SummarizerJob.class);

  public SummarizerJob(boolean directFlag, boolean testFlag)
  {
    super(directFlag,testFlag);
  }
  public SummarizerJob(Run run)
  {
    super(run);
  }

  public void doJob()
  {
    List profiles = db.getUnsummarizedProfiles(sysTypeOpt);
    for (Iterator i = profiles.iterator(); i.hasNext();)
    {
      String profile = ""+i.next();
      db.summarizeProfile(profile,sysTypeOpt);
    }
  }
}
