package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Run;

/**
 * Generate ach file deposit details for unsettled settlement summaries.
 */

public class SummaryDetailsJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(SummaryDetailsJob.class);

  public SummaryDetailsJob(Run run)
  {
    super(run);
  }

  public void doJob()
  {
    db.generateSummaryDetails(sysTypeOpt);
  }
}
