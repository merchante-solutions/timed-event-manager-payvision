package com.mes.pach.proc;

import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.mes.pach.AchBase;
import com.mes.pach.Notifier;
import com.mes.pach.PachDb;
import com.mes.pach.Run;

public abstract class AutomatedProcess extends AchBase implements Runnable
{
  static Logger log = Logger.getLogger(AutomatedProcess.class);

  protected PachDb db;
  protected boolean directFlag;
  protected boolean testFlag;
  protected boolean errorFlag;
  protected int sysTypeOpt;
  protected Date startDate;
  protected Date endDate;
  protected long runId = -1L;
  protected long batchId = -1L; // for test batch runs
  protected String message;

  protected AutomatedProcess(boolean directFlag, boolean testFlag, long runId, long batchId)
  {
    db = new PachDb(directFlag);

    // HACK: runs must have a run id, assign one if it's not given
    if (runId == -1L && this instanceof Run)
    {
      try
      {
        this.runId = db.getNewId();
      }
      catch (Exception e)
      {
        throw new RuntimeException("Error getting new ID from db",e);
      }
    }
    else
    {
      this.runId = runId;
    }
    this.directFlag = directFlag;
    this.testFlag = testFlag;
    this.batchId = batchId;
    initSysType();
  }
    
  private void initSysType()
  {
    sysTypeOpt = testFlag ? db.SYS_TEST : db.SYS_PROD;
  }

  public boolean isDirect()
  {
    return directFlag;
  }

  public boolean isTest()
  {
    return testFlag;
  }

  public static void initLog4j(String fileName)
  {
    File log4jConfig = new File(fileName);
    if (log4jConfig.exists()) {
      PropertyConfigurator.configure(fileName);
    }
    else {
      System.out.println("Warning, " + fileName 
        + " not found, log4j not configured...");
    }
  }
  public static void initLog4j()
  {
    initLog4j("log4j.cfg");
  }

  public String getProcessName()
  {
    return this.getClass().getName();
  }

  /**
   * Common process accessors
   */

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = toDate(startTs);
  }
  public Timestamp getStartTs()
  {
    return toTimestamp(startDate);
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = toDate(endTs);
  }                                                                                                      
  public Timestamp getEndTs()
  {
    return toTimestamp(endDate);
  }

  public void setRunId(long runId)
  {
    this.runId = runId;
  }
  public long getRunId()
  {
    return runId;
  }

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }

  public void setMessage(String message)
  {
    this.message = message;
  }
  public String getMessage()
  {
    return message;
  }

  /**
   * Error handling
   */

  private void notifyError(Exception e)
  {
    Notifier.notifyError(this,e,e.getMessage(),directFlag);
  }

  /**
   * To be overridden for specific handling of errors.
   */
  public void handleError(Exception e)
  {
  }

  protected final void doError(Exception e)
  {
    errorFlag = true;
    handleError(e);
    notifyError(e);
  }

  /**
   * Process life cycle
   */

  public void startProcess()
  {
  }

  public abstract void doProcess();

  public void endProcess()
  {
  }

  public final void run()
  {
    try
    {
      startDate = Calendar.getInstance().getTime();
      startProcess();
      doProcess();
    }
    catch (Exception e)
    {
      doError(e);
    }
    finally
    {
      endDate = Calendar.getInstance().getTime();
      endProcess();
    }
  }
}