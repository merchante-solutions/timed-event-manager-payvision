package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.AchFile;
import com.mes.pach.Job;

/**
 * Causes a file to be resent.
 */
public class ResendFileRun extends AutomatedRun
{
  static Logger log = Logger.getLogger(ResendFileRun.class);
  
  private long fileId;

  /**
   * Cutoff date is set to the most recent cutoff time based on the 
   * cutoff time set in the db achp config parameters.
   */
  public ResendFileRun(boolean directFlag, boolean testFlag, 
    String userName, long fileId)
  {
    super(directFlag,testFlag,userName);
    this.fileId = fileId;
  }

  private void runJob(Job job, String message)
  {
    job.setMessage(message);
    job.run();
    if (!Job.JRC_OK.equals(job.getJobRc()))
    {
      throw new RuntimeException("Error running job " + job);
    }
  }
  
  /**
   * Clear file data and upload and transmit timestamps.
   * Regenerate file
   * Retransfer file
   */
  public void doRun()
  {
    AchFile file = db.getAchFile(fileId);
    if (file != null)
    {
      log.debug("preparing file for resend: " + file);
      db.prepareFileForResend(file);
    
      // generate ach file
      log.debug("regenerating file data");
      runJob(new AchFileGeneratorJob(this),message);

      // tranfer files
      log.debug("retransferring file data");
      runJob(new AchFileXferJob(this),message);
      
      log.debug("done");
    }
    else
    {
      log.debug("File " + fileId + " not found");
    }
  }
}