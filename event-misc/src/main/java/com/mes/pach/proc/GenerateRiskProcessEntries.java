package com.mes.pach.proc;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.pach.AchFile;
import com.mes.pach.Run;

public class GenerateRiskProcessEntries extends AutomatedJob {

	  static Logger log = Logger.getLogger(GenerateRiskProcessEntries.class);

	  public static final int     BATCH_SIZE  = 100;

	  public GenerateRiskProcessEntries(boolean directFlag)
	  {
	    // only load production returns, test returns not applicable
	    super(directFlag,false);
	  }
	  public GenerateRiskProcessEntries(Run run)
	  {
	    super(run);
	  }

	  public void doJob()
	  {
		  if (!db.isProcessingDay())
		    {
		      log.debug("No files transferred, not a processing day");
		      return;
		    }
		  
		  List files = db.getTransferredAchFiles(sysTypeOpt);
		    if (files.size() == 0)
		    {
		      log.debug("No untransferred files found");
		      return;
		    }
		    //for(Object file : files){
		    for (Iterator i = files.iterator(); i.hasNext();) {
		    
        	AchFile nachFile = (AchFile)i.next();
		    	db.generateRiskProcessEntries(nachFile.getFileName());    	
		    }
	    
	  }
	  
	  
	  public static void main(String args[]){
		  
		  GenerateRiskProcessEntries proc = new GenerateRiskProcessEntries(true);
		  proc.doJob();
	  }
	
	
}
