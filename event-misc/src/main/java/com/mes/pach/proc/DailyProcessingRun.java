package com.mes.pach.proc;

import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.pach.Job;
import com.mes.pach.TranBatch;

/**
 * Runs standard end of day processing on test or production transactions.
 */
public class DailyProcessingRun extends AutomatedRun
{
  static Logger log = Logger.getLogger(DailyProcessingRun.class);

  private TranBatch batch;
  private Date cutoffDate;
  private boolean forceRunFlag;

  /**
   * Cutoff date is set to the most recent cutoff time based on the 
   * cutoff time set in the db achp config parameters.
   */
  public DailyProcessingRun(boolean directFlag, boolean testFlag, 
    String userName)
  {
    super(directFlag,testFlag,userName);
    cutoffDate = getLastCutoffDate();
  }

  /**
   * Allows a manual cutoff date to be assigned.  This is useful for running
   * an immediate processing run on the current date and time.
   */
  public DailyProcessingRun(boolean directFlag, boolean testFlag, 
    String userName, Date cutoffDate)
  {
    super(directFlag,testFlag,userName);
    this.cutoffDate = cutoffDate;
  }

  /**
   * Determine cutoff time for a given date and time.  This will be the last 
   * cutoff time that falls prior to the given date and time.  The time of day
   * is loaded from the achp_config table.  If forDate is null then the 
   * current date time is used.
   *
   * TODO: take into account business days and banking holidays.
   */
  public Date getLastCutoffDateBefore(Date forDate)
  {
    //log.debug("cutoff time requested for " + forTs);

    // setup a calendar with for the desired date 
    // and time, or the current date and time if
    // no specific time requested (forTs)
    Calendar forCal = Calendar.getInstance();
    if (forDate != null)
    {
      forCal.setTime(forDate);
    }

    //log.debug("calculating cutoff time for " + forCal.getTime());

    // retrieve today's cutoff time and date
    // from db, may be later or earlier than
    // present time, if after current time then
    // adjust to yesterday
    Calendar cutoffCal = Calendar.getInstance();
    cutoffCal.setTime(db.getCutoffTime());
    if (cutoffCal.after(Calendar.getInstance()))
    {
      cutoffCal.add(Calendar.DATE,-1);
    }

    //log.debug("most recent cutoff time is " + cutoffCal.getTime());

    // |----c----|----c----|----c----|
    //      <-x       <-x
    //      <------x

    // adjust the cutoff to the nearest date and time
    // occurring before the requested date and time
    if (forCal.before(cutoffCal))
    {
      // first adjust cutoff date fields to requested date
      cutoffCal.set(Calendar.YEAR,  forCal.get(Calendar.YEAR));
      cutoffCal.set(Calendar.MONTH, forCal.get(Calendar.MONTH));
      cutoffCal.set(Calendar.DATE,  forCal.get(Calendar.DATE));

      // if still after, adjust cutoff date back a day
      if (forCal.before(cutoffCal))
      {
        cutoffCal.add(Calendar.DATE,-1);
      }
    }

    //log.debug("cutoff time adjusted to " + cutoffCal.getTime());

    /*
    // 
    while (!db.isBusinessDay(cutoffCal))
    {
      cutoffCal.add(Calendar.DATE,-1);
    }
    */

    // convert to java date obj
    return cutoffCal.getTime();
  }

  /**
   * Returns the most recent cutoff date prior to the current date and time.
   */
  public Date getLastCutoffDate()
  {
    return getLastCutoffDateBefore(null);
  }

  private void runJob(Job job, String message)
  {
    job.setMessage(message);
    job.run();
    if (!Job.JRC_OK.equals(job.getJobRc()))
    {
      throw new RuntimeException("Error running job " + job);
    }
  }

  /**
   * Use this to force a run on a non-processing day.
   */
  public void forceRun()
  {
    forceRunFlag = true;
  }

  /**
   * End of day processing:
   *
   * Close batches
   * Pend last minute stragglers
   * Settle transactions
   * Process outstanding returns
   * Summarize settled transactions
   * Generate file
   * Transfer file 
   */
  public void doRun()
  {
    // don't perform end of day processing unless
    // if it's Friday, Saturday or a banking holiday
    if (db.isProcessingDay() || forceRunFlag)
    {
      String message = "EOD, " + (testFlag ? "Test" : "Production");
      log.debug("cutoffDate set to " + cutoffDate);

      // close all batches containing activity
      log.debug("closing batches");
      runJob(new DailyBatchCloseJob(this,cutoffDate),message);

      // pend any recent activity
      log.debug("pending sales");
      runJob(new PendSalesJob(this),message);
      log.debug("pending credits");
      runJob(new PendCreditsJob(this),message);

      // settle eligible transactions
      log.debug("settling sales");
      runJob(new SettleSalesJob(this),message);
      log.debug("settling credits");
      runJob(new SettleCreditsJob(this),message);

      // process any outstanding returns
      log.debug("processing returns");
      runJob(new ProcessReturnsJob(this),message);

      // summarize any new settlement funding entries
      log.debug("summarizing entries");
      runJob(new SummarizerJob(this),message);

      // generate merchant deposit ach details
      log.debug("generating deposit details");
      runJob(new SummaryDetailsJob(this),message);

      // generate ach file
      log.debug("generating files");
      runJob(new AchFileGeneratorJob(this),message);

      // tranfer files
      log.debug("transferring files");
      runJob(new AchFileXferJob(this),message);

      // post net zero summary linked transactions
      log.debug("posting net zero summary linked transactions");
      runJob(new PostNetZeroJob(this),message);

      //generate risk score entries for ACHP Risk Score event
      log.debug("generating risk process entrie for achp risk scoring loader");
      runJob(new GenerateRiskProcessEntries(this), message);
    }
    else
    {
      log.debug("End of day not performed, not a processing day");
    }

    log.debug("done");
  }
}