package com.mes.pach.proc;

import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.net.MailMessage;
import com.mes.pach.AchFile;
import com.mes.pach.PachDb;
import com.mes.pach.Run;
import com.mes.pach.XferParameters;
import com.mes.support.MesMath;

/**
 * Transfer ach files to outbound file server.
 */

public class AchFileXferJob extends AutomatedJob {

  static Logger log = Logger.getLogger(AchFileXferJob.class);

  public AchFileXferJob(boolean directFlag, boolean testFlag) {
    super(directFlag,testFlag);
  }
  public AchFileXferJob(Run run) {
    super(run);
  }

  private void sendFileTransmitEmail(AchFile file) throws Exception {
  
    PachDb.AchFileEmailSummary sum = db.getEmailSummary(file.getFileId());
    Date curDate = Calendar.getInstance().getTime();
    String subject = "Outgoing ACH Success - " + sum.fileName;
    String text =
      "[" +
      db.formatDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss") +
      "] Successfully processed outgoing ACH file: " + sum.fileName + "\n\n" +
      "File Statistics\n" +
      "================================================\n" +
      "Record Count  : " + sum.recordCount + "\n" +
      "Debits Count  : " + sum.debitCount + "\n" +
      "Debits Amount : " + MesMath.toCurrency(""+sum.debitAmount) + "\n" +
      "Credits Count : " + sum.creditCount + "\n" +
      "Credits Amount: " + MesMath.toCurrency(""+sum.creditAmount) + "\n";

      // 127 = test email id, 108 = 3941 ach accounting notification group
      MailMessage msg = new MailMessage();
      msg.setAddresses(file.isTest() ? 127 : 108);
      msg.setSubject(subject);
      msg.setText(text);
      msg.send();
  }
  
  public void transferFiles(List files) {

    Sftp sftp = null;
      
    try {
    
      long ftId = -1;
      
      // transfer each file
      for (Iterator i = files.iterator(); i.hasNext();) {
      
        AchFile file = (AchFile)i.next();

        // notice if first or switching to new file type        
        if (file.getFtId() != ftId) {
        
          ftId = file.getFtId();
          
          // if connected disconnect to get ready to 
          // connect for the new file type
          if (sftp != null && sftp.isConnected()) {
          
            try {
              sftp.disconnect();
            }
            catch (Exception e) {
              log.warn("Error disconnecting from sftp host",e);
            }
          }
        }
        
        // connect/re-connect if needed
        if (sftp == null || !sftp.isConnected()) {
        
          XferParameters xferParms = 
            db.getAchFileType(file.getFtId()).getXferParameters();
          SshParameters sshParms = 
            new SshParameters(xferParms.host,xferParms.user,xferParms.pass);
          sftp = new Sftp(sshParms);
          sftp.connect();
          sftp.setDir(xferParms.path);
        }
        
        // transfer file to endpoint
        OutputStream out = null;

        try {
        
          // send ach file data and flag file to server
          out = sftp.getOutputStream(file.getFileName(),0,false);
          db.downloadAchFileData(file,out);
          db.setFileTransmitDate(file.getFileId());

          // create flag file
          String flagFileName = file.getFileName();
          int offset = flagFileName.lastIndexOf('.');
          if ( offset < 0 ) {
            flagFileName  = flagFileName + ".flg";
          }
          else {
            flagFileName  = flagFileName.substring(0,offset) + ".flg";
          }
          byte[] flagData = new byte[0];
          sftp.upload(flagData,flagFileName);

          // send file transmission notice email to accounting
          sendFileTransmitEmail(file);

          // set all transactions to post
          db.postFileTransactions(file);
        }
        catch (Exception e) {
        
          throw new RuntimeException("Error uploading file " 
            + file.getFileName(),e);
        }
        finally {
          try { out.close(); } catch (Exception e) { }
        }
      }
    }
    catch (Exception e) {
      throw new RuntimeException("Error transferring files",e);
    }
    finally {
      try { sftp.disconnect(); } catch (Exception e) { }
    }
  }

  public void doJob() {
  
    if (!db.isProcessingDay()) {
    
      log.debug("No files transferred, not a processing day");
      return;
    }

    List files = db.getUntransferredAchFiles(sysTypeOpt);
    if (files.size() == 0) {
    
      log.debug("No untransferred files found");
      return;
    }
    
    transferFiles(files);
    
  }
}