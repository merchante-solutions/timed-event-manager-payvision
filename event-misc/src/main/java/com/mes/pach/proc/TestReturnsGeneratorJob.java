package com.mes.pach.proc;

import org.apache.log4j.Logger;
import com.mes.pach.Run;
import com.mes.pach.TranBatch;

/**
 * Generate returns for a test batch.
 */

public class TestReturnsGeneratorJob extends AutomatedJob
{
  static Logger log = Logger.getLogger(TestReturnsGeneratorJob.class);

  public static final int     BATCH_SIZE  = 100;

  public TestReturnsGeneratorJob(Run run, TranBatch batch)
  {
    super(run,batch);
  }

  public void doJob()
  {
    db.generateTestReturns(batchId);
  }
}