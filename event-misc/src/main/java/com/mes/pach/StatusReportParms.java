package com.mes.pach;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.user.UserBean;

public class StatusReportParms
{
  static Logger log = Logger.getLogger(StatusReportParms.class);

  private UserBean user;
  private Date fromDate;
  private Date toDate;
  private long tranId;
  private String profileId;
  private String merchNum;
  private String accountNum;
  private String refNum;
  private String custName;
  private String custId;
  private String tranType;
  private String secCode;
  private int batchNum;
  private boolean curBatchFlag;
  private List inStats = new ArrayList();
  private List notInStats = new ArrayList();
  private long batchId;

  public StatusReportParms(UserBean user)
  {
    this.user = user;
  }

  public void setFromDate(Date fromDate)
  {
    this.fromDate = fromDate;
  }
  public Date getFromDate()
  {
    return fromDate;
  }
  public boolean hasFromDate()
  {
    return fromDate != null;
  }

  public void setToDate(Date toDate)
  {
    this.toDate = toDate;
  }
  public Date getToDate()
  {
    return toDate;
  }
  public boolean hasToDate()
  {
    return toDate != null;
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }
  public boolean hasTranId()
  {
    return tranId > 0;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }
  public boolean hasProfileId()
  {
    return profileId != null;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public boolean hasMerchNum()
  {
    return merchNum != null;
  }

  public void setAccountNum(String accountNum)
  {
    this.accountNum = accountNum;
  }
  public String getAccountNum()
  {
    return accountNum;
  }
  public boolean hasAccountNum()
  {
    return accountNum != null;
  }

  public void setRefNum(String refNum)
  {
    this.refNum = refNum;
  }
  public String getRefNum()
  {
    return refNum;
  }
  public boolean hasRefNum()
  {
    return refNum != null;
  }

  public void setCustName(String custName)
  {
    this.custName = custName;
  }
  public String getCustName()
  {
    return custName;
  }
  public boolean hasCustName()
  {
    return custName != null;
  }

  public void setCustId(String custId)
  {
    this.custId = custId;
  }
  public String getCustId()
  {
    return custId;
  }
  public boolean hasCustId()
  {
    return custId != null;
  }

  public void setTranType(String tranType)
  {
    this.tranType = tranType;
  }
  public String getTranType()
  {
    return tranType;
  }
  public boolean hasTranType()
  {
    return tranType != null;
  }

  public void setSecCode(String secCode)
  {
    this.secCode = secCode;
  }
  public String getSecCode()
  {
    return secCode;
  }
  public boolean hasSecCode()
  {
    return secCode != null;
  }

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }
  public boolean hasBatchId()
  {
    return batchId > 0;
  }

  /**
   * Convert list to string.  Each list item is single-tick quoted and 
   * separated by commas.
   */
  private String listToString(List list)
  {
    if (list == null) return "(null)";
    if (list.isEmpty()) return "(empty)";
    StringBuffer buf = new StringBuffer();
    for (Iterator i = list.iterator(); i.hasNext();)
    {
      buf.append("'" + i.next() + "'" + (i.hasNext() ? ", " : ""));
    }
    return ""+buf;
  }

  public void setBatchNum(int batchNum)
  {
    this.batchNum = batchNum;
  }
  public int getBatchNum()
  {
    return batchNum;
  }
  public boolean hasBatchNum()
  {
    return batchNum > 0;
  }

  public void setCurBatchFlag(boolean curBatchFlag)
  {
    this.curBatchFlag = curBatchFlag;
  }
  public boolean curBatchOnly()
  {
    return curBatchFlag;
  }

  public void setInStats(List inStats)
  {
    this.inStats = inStats;
  }
  public List getInStats()
  {
    return inStats;
  }
  public boolean hasInStats()
  {
    return inStats != null && !inStats.isEmpty();
  }
  public void addInStat(String status)
  {
    inStats.add(status);
  }
  public String getInStatsString()
  {
    return listToString(inStats);
  }

  public void setNotInStats(List notInStats)
  {
    this.notInStats = notInStats;
  }
  public List getNotInStats()
  {
    return notInStats;
  }
  public void addNotInStat(String status)
  {
    notInStats.add(status);
  }
  public boolean hasNotInStats()
  {
    return notInStats != null && !notInStats.isEmpty();
  }
  public String getNotInStatsString()
  {
    return listToString(notInStats);
  }

  public long getUserNodeId()
  {
    return user.getHierarchyNode();
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("StatusReportParms:");
    buf.append("\n  user:         " + user.getLoginName());
    buf.append("\n  userNode:     " + getUserNodeId());
    buf.append("\n  fromDate:     " + fromDate);
    buf.append("\n  toDate:       " + toDate);
    buf.append("\n  tranId:       " + tranId);
    buf.append("\n  profileId:    " + profileId);
    buf.append("\n  merchNum:     " + merchNum);
    buf.append("\n  accountNum:   " + accountNum);
    buf.append("\n  refNum:       " + refNum);
    buf.append("\n  custName:     " + custName);
    buf.append("\n  custId:       " + custId);
    buf.append("\n  tranType:     " + tranType);
    buf.append("\n  secCode:      " + secCode);
    buf.append("\n  batchNum:     " + batchNum);
    buf.append("\n  curBatchOnly: " + curBatchOnly());
    buf.append("\n  batchId:      " + batchId);
    buf.append("\n  inStats:      " + listToString(inStats));
    buf.append("\n  notInStats:   " + listToString(notInStats));
    return ""+buf;
  }
}