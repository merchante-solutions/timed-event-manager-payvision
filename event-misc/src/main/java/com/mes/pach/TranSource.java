package com.mes.pach;

import java.util.HashMap;

public class TranSource
{
  // ach request sources
  public static final TranSource PMG      = new TranSource("PMG",     "Payment Gateway");
  public static final TranSource TEST     = new TranSource("TEST",    "Test Request");
  public static final TranSource BATCH    = new TranSource("BATCH",   "Batch File");
  public static final TranSource BACKOFF  = new TranSource("BACKOFF", "Back Office");

  private static HashMap sourceHash = new HashMap();

  static
  {
    sourceHash.put(""+PMG,      PMG);
    sourceHash.put(""+TEST,     TEST);
    sourceHash.put(""+BATCH,    BATCH);
    sourceHash.put(""+BACKOFF,  BACKOFF);
  };

  private String sourceStr;
  private String description;

  private TranSource(String sourceStr, String description)
  {
    this.sourceStr = sourceStr;
    this.description = description;
  }

  public static TranSource sourceFor(String forStr, TranSource dflt)
  {
    TranSource source = (TranSource)sourceHash.get(forStr);
    return source != null ? source : dflt;
  }
    
  public static TranSource sourceFor(String forStr)
  {
    TranSource source = sourceFor(forStr,null);
    if (source == null)
    {
      throw new NullPointerException("Invalid source string: '" 
        + forStr + "'");
    }
    return source;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof TranSource))
    {
      return false;
    }
    TranSource that = (TranSource)o;
    return that.sourceStr.equals(sourceStr);
  }

  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return sourceStr;
  }
}