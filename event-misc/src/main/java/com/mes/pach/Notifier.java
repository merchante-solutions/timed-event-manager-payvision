package com.mes.pach;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.net.MailMessage;
import com.mes.pach.proc.FileUtil;
import com.mes.reports.OrgSummaryDataBean;
import com.mes.reports.ReportSQLJBean;

public class Notifier
{
  static Logger log = Logger.getLogger(Notifier.class);

  private PachDb db;
  private boolean directFlag;
  public static final String DEV_ADDR   = "tbaker@merchante-solutions.com";
  public static final String FROM_ADDR  = "achpaymentsystem@merchante-solutions.com";

  public Notifier(boolean directFlag)
  {
    this.directFlag = directFlag;
    db = new PachDb(directFlag);
  }

  private List getAddressList(String addrStr)
  {
    Pattern p = Pattern.compile("([^,; ]+)[,; ]?");
    Matcher m = p.matcher(addrStr);
    List addrList = new ArrayList();
    while (m.find())
    {
      addrList.add(m.group(1));
    }
    return addrList;
  }

  private MailMessage createMailMessage(String addrStr)
  {
    try
    {
      MailMessage msg = new MailMessage(directFlag);
      msg.setFrom(FROM_ADDR);
      List addrList = getAddressList(addrStr);
      for (Iterator i = addrList.iterator(); i.hasNext();)
      {
        msg.addTo(""+i.next());
      }
      return msg;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      db.logEntry(this,"createMailMessage(addrStr=" + addrStr + ")",e);
    }
    return null;
  }

  /**
   * Returns an exception stack trace as a string.
   */
  public String getStackTrace(Exception e)
  {
    PrintWriter pw = null;
    try
    {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      pw = new PrintWriter(baos);
      e.printStackTrace(pw);
      pw.flush();
      return ""+baos;
    }
    finally
    {
      pw.close();
    }
  }

  /**
   * Sends error message to developer email as defined in db config parameter.
   * Places a stack trace in the message text along with other clues as to
   * the cause. Useful for automated process error reporting.
   */
  public void notifyDeveloper(Object src, String clue, Exception e)
  {
    try
    {
      MailMessage msg = createMailMessage(db.getErrorNotificationEmails());
      msg.setSubject("ACHP Error");
      StringBuffer buf = new StringBuffer();
      buf.append("Source: " + src.getClass().getName() + "\n");
      buf.append("Clue:   " + clue + "\n");
      buf.append("Error:  " + e + "\n\n");
      buf.append(""+getStackTrace(e));
      msg.setText(""+buf);
      msg.send();
    }
    catch (Exception ee)
    {
      db.logEntry(this,"notifyDeveloper(src=" + src.getClass().getName() 
        + ",clue='" + clue + "')",ee);
      log.error("Error: " + ee);
      ee.printStackTrace();
    }
  }
  public static void notifyDeveloper(Object src, String clue, Exception e, 
    boolean directFlag)
  {
    (new Notifier(directFlag)).notifyDeveloper(src,clue,e);
  }

  /**
   * Sends a message to the developer email recipients.
   */
  public void notifyDeveloper(String subject, String text)
  {
    try
    {
      MailMessage msg = createMailMessage(db.getErrorNotificationEmails());
      msg.setSubject(subject);
      msg.setText(text);
      msg.send();
    }
    catch (Exception ee)
    {
      db.logEntry(this,"notifyDeveloper(subj=" + subject
        + ",text='" + text + "')",ee);
      log.error("Error: " + ee);
      ee.printStackTrace();
    }
  }
  public void notifyDeveloper(String text)
  {
    notifyDeveloper("ACHP Notification",text);
  }
  public static void notifyDeveloper(String text, boolean directFlag)
  {
    (new Notifier(directFlag)).notifyDeveloper(text);
  }
  public static void notifyDeveloper(String subject, String text, boolean directFlag)
  {
    (new Notifier(directFlag)).notifyDeveloper(subject,text);
  }

  /**
   * Convenience method, this spams error notifications to various channels:
   *
   *   log4j, error out, java_log and email.
   *
   * Intended for use by automated processes that are running unattended.
   */
  public void notifyError(Object obj, Exception e, String clue)
  {
    log.error("Error: " + e + "\n" + getStackTrace(e));
    db.logEntry(obj,clue,e);
    notifyDeveloper(obj,clue,e);
  }
  public static void notifyError(Object obj, Exception e, String clue, boolean directFlag)
  {
    (new Notifier(directFlag)).notifyError(obj,e,clue);
  }

  /**
   * Send an email to a merchant email address retrieved from achp_profiles.
   */
  public void notifyMerchant(String profileId, String subject, String body)
    throws Exception
  {
    // get the profile email address
    MerchProfile profile = db.getMerchProfile(profileId);
    String emailAddr = profile.getEmailAddress();
    if (emailAddr == null || emailAddr.length() == 0)
    {
      return;
    }

    // send email to the address
    MailMessage msg = createMailMessage(emailAddr);
    msg.setSubject(subject);
    msg.setText(body);
    msg.send();
  }

  /**
   * Send email to risk recipients as specified in db risk parm.
   */
  public void notifyRisk(String subject, String body) throws Exception
  {
    MailMessage msg = createMailMessage(db.getRiskEmails());
    msg.setSubject(subject);
    msg.setText(body);
    msg.send();
  }

  public String generateBaseOrgReportUrl(int dataType, int reportType, 
    String merchNum, Date startDate, Date endDate)
  {
    StringBuffer buf = new StringBuffer();

    buf.append("http://");
    buf.append(db.getWebHostName() + "/" + db.getOrgReportPath());
    buf.append("?dataType=" + dataType);
    buf.append("&reportType=" + reportType);
    buf.append("&nodeId=" + merchNum);
    buf.append("&com.mes.HierarchyNode=" + merchNum);

    Calendar cal = Calendar.getInstance();
    cal.setTime(startDate);
    buf.append( "&beginDate.month=" );
    buf.append( cal.get( Calendar.MONTH ) );
    buf.append( "&beginDate.day=" );
    buf.append( cal.get( Calendar.DAY_OF_MONTH ) );
    buf.append( "&beginDate.year=" );
    buf.append( cal.get( Calendar.YEAR ) );

    cal.setTime(endDate);
    buf.append( "&endDate.month=" );
    buf.append( cal.get( Calendar.MONTH ) );
    buf.append( "&endDate.day=" );
    buf.append( cal.get( Calendar.DAY_OF_MONTH ) );
    buf.append( "&endDate.year=" );
    buf.append( cal.get( Calendar.YEAR ) );

    return ""+buf;
  }

  public String generateReturnReportUrl(PachDb.CustReturnSummary sum)
  {
    return generateBaseOrgReportUrl(OrgSummaryDataBean.DT_ACHP_RETURNS,
      ReportSQLJBean.RT_DETAILS,sum.merchNum,sum.recvDate,sum.recvDate)
      + "&profileId=" + sum.profileId;
  }

  /**
   * Notify merchant of received customer return or NOC.
   */
  public void notifyMerchOfCustReturn(PachDb.CustReturnSummary sum)
  {
    try
    {
      StringBuffer buf = new StringBuffer();

      buf.append("DO NOT REPLY TO THIS MESSAGE - ");
      buf.append("This email has been automatically generated.\n\n");

      buf.append("MERCHANT E-SOLUTIONS - ");
      buf.append("ACH Payment System (ACHP)\n\n");

      buf.append("Merchant:   " + sum.merchNum + "\n");
      buf.append("Profile:    " + sum.profileId + "\n");
      buf.append("            " + sum.companyName + "\n\n");

      buf.append("This is to notify you that ACH payment Returns and/or ");
      buf.append("Notices of Change (NOCs) have been received on ");
      buf.append(db.formatDate(sum.recvDate,db.FMT_DATE2) + ".\n\n");
      

      buf.append("NOC Count:          " + sum.nocCount + "\n");
      buf.append("Return Count:       " + sum.returnCount + "\n");
      buf.append("Net Return Amount:  " + sum.netReturn + "\n\n");

      buf.append("To view details about each received item please ");
      buf.append("view the ACHP Return Report on the MES website:\n\n");

      buf.append(generateReturnReportUrl(sum) + "\n\n");

      buf.append("If you have any questions about this email, your ");
      buf.append("Merchant e-Solutions merchant account or processing ");
      buf.append("procedures, please contact our 24-hour Help Desk at ");
      buf.append("888-288-2692 or help@merchante-solutions.com.\n\n");

      buf.append("DO NOT REPLY TO THIS MESSAGE - ");
      buf.append("This email has been automatically generated.\n\n");

      notifyMerchant(sum.profileId,
        "MES ACHP Returns and NOCs Received",""+buf);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      notifyError(this,e,"notifyMerchOfCustReturn(pid=" + sum.profileId + ")");
    }
  }
  public static void notifyMerchOfCustReturn(PachDb.CustReturnSummary sum,
    boolean directFlag)
  {
    (new Notifier(directFlag)).notifyMerchOfCustReturn(sum);
  }

  /**
   * Notify risk department of received merchant return or NOC.
   */
  public void notifyRiskOfMerchReturn(PachDb.MerchReturnItem item)
  {
    try
    {
      boolean isReturn = "R".equals(item.ret.getReturnType());
      String itemDesc = isReturn ? "Return" : "NOC";
      StringBuffer buf = new StringBuffer();

      buf.append("ACHP Merchant Settlement " + itemDesc);
      buf.append(" Notification\n\n");

      buf.append("Merchant:   " + item.merchNum + "\n");
      buf.append("            " + item.merchName + "\n\n");

      buf.append("Profile:    " + item.profileId + "\n");
      buf.append("            " + item.companyName + "\n\n");


      buf.append("A settlement funding deposit or debit has generated a\n");
      buf.append(itemDesc + " on the above ACHP merchant profile.\n\n");

      if (isReturn)
      {
        buf.append("ACHP activity on this profile has been placed on hold\n");
        buf.append("pending investigation and resolution of the return.\n\n");
      }
      else
      {
        buf.append("The above merchant profile remains active but has\n");
        buf.append("received a Notice of Change that may require\n");
        buf.append("adjustments to the merchant profile.\n\n");
      }

      buf.append(itemDesc + " Details\n\n");
      buf.append("ID:         " + item.ret.getReturnId() + "\n");
      buf.append("Date:       " 
        + db.formatDate(item.ret.getCreateDate(),db.FMT_DATE2) + "\n");

      String retCode = item.ret.getReturnType() 
        + FileUtil.zeroPad(""+item.ret.getReasonCode(),2)
        + " - " + item.ret.getDescriptor();
      buf.append("Type:       " + retCode + "\n");
      buf.append("File Data:  " + item.ret.getReturnDescription() + "\n");

      notifyRisk("ACHP Merchant Return (" + item.merchNum + " - " 
        + item.merchName + ")",""+buf);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      notifyError(this,e,"notifyRiskOfMerchReturn(pid=" + item.profileId + ")");
    }
  }
  public static void notifyRiskOfMerchReturn(PachDb.MerchReturnItem item,
    boolean directFlag)
  {
    (new Notifier(directFlag)).notifyRiskOfMerchReturn(item);
  }

}