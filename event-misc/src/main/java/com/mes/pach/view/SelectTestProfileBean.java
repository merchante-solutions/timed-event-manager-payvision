package com.mes.pach.view;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchViewBean;
import com.mes.pach.MerchProfile;

public class SelectTestProfileBean extends AchViewBean
{
  static Logger log = Logger.getLogger(SelectTestProfileBean.class);

  public static final String FN_PROFILE_ID    = "profileId";
  public static final String FN_OP_TYPE       = "opType";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  public static final String OT_REQUEST = "request";
  public static final String OT_BATCH   = "batch";

  private MerchProfile profile;

  public SelectTestProfileBean(MesAction action)
  {
    super(action);
  }

  public MerchProfile getProfile()
  {
    return profile;
  }

  public boolean isRequestOp()
  {
    return OT_REQUEST.equals(getData(FN_OP_TYPE));
  }

  public boolean isBatchOp()
  {
    return OT_BATCH.equals(getData(FN_OP_TYPE));
  }

  public class ProfileIdValidation implements Validation
  {
    public String getErrorText()
    {
      return "Profile not found";
    }

    public boolean validate(String data)
    {
      if (data == null || data.length() == 0)
      {
        return true;
      }
      profile = db().getMerchProfile(data);
      return profile != null;
    }
  }
   
  public class OpTypeValidation implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      if (data != null)
      {
        if (OT_REQUEST.equals(data) || OT_BATCH.equals(data))
        {
          return true;
        }
        errorText = "Invalid op type '" + data + "'";
      }
      else
      {
        errorText = "No op type specified";
      }
      return false;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new Field(FN_PROFILE_ID,"Profile ID",20,20,false));
      fields.getField(FN_PROFILE_ID).addValidation(new ProfileIdValidation());
      fields.add(new HiddenField(FN_OP_TYPE));
      fields.getField(FN_OP_TYPE).addValidation(new OpTypeValidation());
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}