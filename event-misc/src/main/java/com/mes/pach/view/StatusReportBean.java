package com.mes.pach.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.mvc.Downloadable;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchViewBean;
import com.mes.pach.SecCode;
import com.mes.pach.StatusReportParms;
import com.mes.pach.StatusReportRow;
import com.mes.tools.DropDownTable;

public class StatusReportBean extends AchViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(StatusReportBean.class);

  public static final String FN_FROM_DATE   = "fromDate";
  public static final String FN_TO_DATE     = "toDate";
  public static final String FN_STATUS      = "status";
  public static final String FN_TRAN_TYPE   = "tranType";
  public static final String FN_SEC_CODE    = "secCode";
  public static final String FN_MERCH_NUM   = "merchNum";
  public static final String FN_TRAN_ID     = "tranId";
  public static final String FN_ACCOUNT_NUM = "accountNum";
  public static final String FN_CUST_NAME   = "custName";
  public static final String FN_CUST_ID     = "custId";
  public static final String FN_REF_NUM     = "refNum";
  public static final String FN_SUBMIT_BTN  = "submitBtn";

  private List rows;

  public StatusReportBean(MesAction action)
  {
    super(action);
  }

  public class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("","Any Status");
      addElements(db().getReportStatusDdItems());
    }
  }

  public class TypeTable extends DropDownTable
  {
    public TypeTable()
    {
      addElement("","All");
      addElement("SALE","Sales");
      addElement("CREDIT","Credits");
    }
  }

  public class SecTable extends DropDownTable
  {
    public SecTable()
    {
      addElement("","All");
      addElement(""+SecCode.CCD,""+SecCode.CCD);
      addElement(""+SecCode.PPD,""+SecCode.PPD);
      addElement(""+SecCode.WEB,""+SecCode.WEB);
      addElement(""+SecCode.TEL,""+SecCode.TEL);
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new NumberField(FN_TRAN_ID,"Transaction ID",12,12,true,0));
      fields.add(new Field(FN_MERCH_NUM,"Merchant Number",12,20,true));
      fields.add(new Field(FN_ACCOUNT_NUM,"Account Number",20,20,true));
      fields.add(new Field(FN_REF_NUM,"Reference Number",96,20,true));
      fields.add(new Field(FN_CUST_NAME,"Customer Name",22,20,true));
      fields.add(new Field(FN_CUST_ID,"Customer ID",15,20,true));
      fields.add(new DropDownField(FN_STATUS,new StatusTable(),true));
      fields.add(new DropDownField(FN_TRAN_TYPE,new TypeTable(),true));
      fields.add(new DropDownField(FN_SEC_CODE,new SecTable(),true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      StatusReportParms parms = new StatusReportParms(user);
      Field f = null;
      Date fromDate = 
        ((DateStringField)(getField(FN_FROM_DATE))).getUtilDate();
      parms.setFromDate(fromDate);
      if (!(f = getField(FN_TO_DATE)).isBlank())
      {
        parms.setToDate(((DateStringField)f).getUtilDate());
      }
      else
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        parms.setToDate(cal.getTime());
      }
      parms.setTranId(getField(FN_TRAN_ID).asLong());
      if (!(f = getField(FN_STATUS)).isBlank())
      {
        parms.addInStat(f.getData());
      }
      if (!(f = getField(FN_TRAN_TYPE)).isBlank())
      {
        parms.setTranType(f.getData());
      }
      if (!(f = getField(FN_MERCH_NUM)).isBlank())
      {
        parms.setMerchNum(f.getData());
      }
      if (!(f = getField(FN_ACCOUNT_NUM)).isBlank())
      {
        parms.setAccountNum(f.getData());
      }
      if (!(f = getField(FN_REF_NUM)).isBlank())
      {
        parms.setRefNum(f.getData());
      }
      if (!(f = getField(FN_SEC_CODE)).isBlank())
      {
        parms.setSecCode(f.getData());
      }
      if (!(f = getField(FN_CUST_NAME)).isBlank())
      {
        parms.setCustName(f.getData());
      }
      if (!(f = getField(FN_CUST_ID)).isBlank())
      {
        parms.setCustId(f.getData());
      }
      // NOTE: currently not fully itilizing in/not-in functionality
      // of parm object
      if (!(f = getField(FN_STATUS)).isBlank())
      {
        parms.addInStat(f.getData());
      }
      rows = db().getStatusReportRows(parms);
      return true;
    }
    catch (Exception e)
    {
      log.error("autoSubmit error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      // default to today if by default (from is today, leave to blank)
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
      if (fromField.isBlank() && toField.isBlank())
      {
        fromField.setUtilDate(Calendar.getInstance().getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("autoLoad error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */

  private Iterator dli;

  public void startDownload()
  {
    dli = getRows().iterator();
  }

  public String getDlHeader()
  {
    return "\"Tran ID\""
      + ",Batch"
      + ",\"Profile ID\""
      + ",Date"
      + ",Status"
      + ",\"SEC Type\""
      + ",\"Tran Type\""
      + ",\"Transit Routing Number\""
      + ",\"Account Number\""
      + ",\"Account Type\""
      + ",\"Reference Number\""
      + ",\"Customer ID\""
      + ",\"Customer Name\""
      + ",Recurring?"
      + ",Held?"
      + ",Amount";
  }

  public String getDlNext()
  {
    if (dli == null || !dli.hasNext())
    {
      return null;
    }

    StatusReportRow row = (StatusReportRow)dli.next();
    String tranDateStr = db().formatDate(row.getTranDate(),db().FMT_DATE2);

    String profileId = row.getProfileId();
    if (profileId != null && profileId.length() > 12)
    {
      profileId = profileId.substring(0,12) + "-" + profileId.substring(12);
    }

    return "\"" + row.getTranId() + "\","
      + "\"" + row.getBatchNum() + "\","
      + "\"" + profileId + "\","
      + "\"" + tranDateStr + "\","
      + "\"" + row.getReportStatus() + "\","
      + "\"" + row.getSecCode() + "\","
      + "\"" + row.getTranType() + "\","
      + "\"" + row.getTransitNum() + "\","
      + "\"" + row.getAccountNum() + "\","
      + "\"" + (row.isCheckingAccount() ? "Checking" : "Savings") + "\","
      + "\"" + row.getRefNum() + "\","
      + "\"" + row.getCustId() + "\","
      + "\"" + row.getCustName() + "\","
      + "\"" + (row.isRecurring() ? "Yes" : "No") + "\","
      + "\"" + (row.isHeld() ? "Yes" : "No") + "\","
      + "\"" + row.getAmount() + "\"";
  }

  public void finishDownload()
  {
    dli = null;
  }

  public String getDlFilename()
  {
    return "achp_status_report.csv";
  }
}