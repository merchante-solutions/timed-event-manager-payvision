package com.mes.pach.view;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Mvc;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchViewBean;

public class LookupProfilesBean extends AchViewBean
{
  static Logger log = Logger.getLogger(LookupProfilesBean.class);

  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List rows;

  public LookupProfilesBean(MesAction action)
  {
    super(action);
  }
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",64,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));

      // bean will be session stored, so have 
      // back links discard form field parameters
      fields.add(new HiddenField(Mvc.DISCARD_ARGS_FLAG)); 

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      String searchTerm = getData(FN_SEARCH_TERM);
      rows = db().lookupMerchProfileRefs(searchTerm);
      if (rows == null)
      {
        action.addFeedback("More than 200 matches found, narrow search"
          + " with more specific search term");
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}