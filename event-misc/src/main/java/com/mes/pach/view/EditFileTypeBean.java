package com.mes.pach.view;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchFileType;
import com.mes.pach.AchViewBean;

public class EditFileTypeBean extends AchViewBean
{
  static Logger log = Logger.getLogger(EditFileTypeBean.class);
  
  public static final String FN_FT_ID         = "ftId";
  public static final String FN_DESC          = "description";
  public static final String FN_DEST_ID       = "destinationId";
  public static final String FN_DEST_NAME     = "destinationName";
  public static final String FN_ORIG_ID       = "originId";
  public static final String FN_ORIG_NAME     = "originName";
  public static final String FN_COMPANY_ID    = "companyId";
  public static final String FN_COMPANY_NAME  = "companyName";
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private AchFileType ft;

  public EditFileTypeBean(MesAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_FT_ID));
      fields.addAlias(FN_FT_ID,"editId");
      fields.add(new Field(FN_DESC,"Description",32,20,false));
      fields.add(new Field(FN_DEST_ID,"Destination ID",10,20,false));
      fields.add(new Field(FN_DEST_NAME,"Destination Name",23,20,false));
      fields.add(new Field(FN_ORIG_ID,"Origin ID",10,20,false));
      fields.add(new Field(FN_ORIG_NAME,"Origin Name",23,20,false));
      fields.add(new Field(FN_COMPANY_ID,"Company ID",10,20,false));
      fields.add(new Field(FN_COMPANY_NAME,"Company Name",16,20,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  protected boolean autoLoad()
  {
    try
    {
      long ftId = getField(FN_FT_ID).asLong();
      if (ftId != 0L)
      {
        ft = db().getAchFileType(ftId);
        setData(FN_DESC,ft.getDescription());
        setData(FN_DEST_ID,ft.getDestinationId());
        setData(FN_DEST_NAME,ft.getDestinationName());
        setData(FN_ORIG_ID,ft.getOriginId());
        setData(FN_ORIG_NAME,ft.getOriginName());
        setData(FN_COMPANY_ID,ft.getCompanyId());
        setData(FN_COMPANY_NAME,ft.getCompanyName());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;    
  }
  
  protected boolean autoSubmit()
  {
    try
    {
      boolean isNew = false;
      long ftId = getField(FN_FT_ID).asLong();
      if (ftId == 0L)
      {
        ftId = db().getNewId();
        ft = new AchFileType();
        ft.setFtId(ftId);
        setData(FN_FT_ID,String.valueOf(ftId));
        isNew = true;
      }
      else
      {
        ft = db().getAchFileType(ftId);
      }
      
      ft.setDescription(getData(FN_DESC));
      ft.setDestinationId(getData(FN_DEST_ID));
      ft.setDestinationName(getData(FN_DEST_NAME));
      ft.setOriginId(getData(FN_ORIG_ID));
      ft.setOriginName(getData(FN_ORIG_NAME));
      ft.setCompanyId(getData(FN_COMPANY_ID));
      ft.setCompanyName(getData(FN_COMPANY_NAME));

      if (isNew)
      {
        db().insertAchFileType(ft);
      }
      else
      {
        db().updateAchFileType(ft);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}