package com.mes.pach.view;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchAccount;
import com.mes.pach.AchBase;
import com.mes.pach.AchViewBean;
import com.mes.pach.MerchProfile;
import com.mes.pach.MerchProfileRef;
import com.mes.pach.SecCode;
import com.mes.tools.DropDownTable;

public class EditProfileBean extends AchViewBean
{
  static Logger log = Logger.getLogger(EditProfileBean.class);

  public static final String FN_PROFILE_ID        = "profileId";
  public static final String FN_MERCH_NUM         = "merchNum";
  public static final String FN_MERCH_NAME        = "merchName";
  public static final String FN_STATUS_CODE       = "statusCode";
  public static final String FN_CO_NAME           = "companyName";
  public static final String FN_ACCOUNT_NUM       = "accountNum";
  public static final String FN_TRANSIT_NUM       = "transitNum";
  public static final String FN_ACCOUNT_TYPE      = "accountType";
  public static final String FN_SF_SEC_CODE       = "sfSecCode";
  public static final String FN_CCD_ENABLE_FLAG   = "ccdEnableFlag";
  public static final String FN_PPD_ENABLE_FLAG   = "ppdEnableFlag";
  public static final String FN_WEB_ENABLE_FLAG   = "webEnableFlag";
  public static final String FN_TEL_ENABLE_FLAG   = "telEnableFlag";
  public static final String FN_CCD_PEND_DAYS     = "ccdPendDays";
  public static final String FN_PPD_PEND_DAYS     = "ppdPendDays";
  public static final String FN_WEB_PEND_DAYS     = "webPendDays";
  public static final String FN_TEL_PEND_DAYS     = "telPendDays";
  public static final String FN_BATCH_NUM         = "batchNum";
  public static final String FN_TEST_BATCH_NUM    = "testBatchNum";
  public static final String FN_FT_ID             = "ftId";
  public static final String FN_HOLD_FLAG         = "holdFlag";
  public static final String FN_EMAIL_ADDR        = "emailAddr";
  public static final String FN_SUBMIT_BTN        = "submitBtn";

  // future
  public static final String FN_EMAIL             = "email";

  private MerchProfile profile;

  public EditProfileBean(MesAction action)
  {
    super(action);
  }

  public class AccountTypeTable extends DropDownTable
  {
    public AccountTypeTable()
    {
      addElement(AchAccount.AT_CHECKING,"Checking");
      addElement(AchAccount.AT_SAVINGS,"Savings");
    }
  }

  public class SecCodeTable extends DropDownTable
  {
    public SecCodeTable()
    {
      addElement(""+SecCode.CCD,"CCD - Business Account");
      addElement(""+SecCode.PPD,"PPD - Personal Account");
    }
  }

  public class PendDaysTable extends DropDownTable
  {
    public PendDaysTable()
    {
      addElement("0","Immediate Settlement");
      addElement("1","Pend 1 Day");
      addElement("2","Pend 2 Days");
      addElement("3","Pend 3 Days");
      addElement("4","Pend 4 Days");
      addElement("5","Pend 5 Days");
      addElement("6","Pend 6 Days");
      addElement("7","Pend 7 Days");
      addElement("8","Pend 8 Days");
      addElement("9","Pend 9 Days");
    }
  }

  public class FileTypeTable extends DropDownTable
  {
    public FileTypeTable()
    {
      addElement("","select one");
      addElements(db().getAchFileTypeDdItems());
    }
  }

  public class ProfileStatusTable extends DropDownTable
  {
    public ProfileStatusTable()
    {
      addElement("ACCEPT","Normal - incoming requests accepted, normal processing");
      addElement("HOLD","Holding - incoming requests accepted, processing on hold");
      addElement("REJECT","Rejecting - incoming requests rejected, processing on hold");
    }
  }

  public class BatchNumValidation implements Validation
  {
    String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      // not concerned with blank values here
      if (data == null || data.length() == 0)
      {
        return true;
      }
      try
      {
        int val = Integer.parseInt(data);
        if (val == 0)
        {
          errorText = "Minimum value allowed 1";
          return false;
        }
        if (val > 999999)
        {
          errorText = "Maximum value allowed 999999";
          return false;
        }
      }
      catch (Exception e)
      {
        log.error("Invalid batch number '" + data + "'");
        errorText = "Invalid batch number";
        return false;
      }
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_PROFILE_ID));
      fields.add(new HiddenField(FN_MERCH_NUM));
      fields.add(new HiddenField(FN_MERCH_NAME));
      fields.add(new DropDownField(FN_STATUS_CODE,new ProfileStatusTable(),true));
      fields.add(new Field(FN_CO_NAME,"Company Name",16,16,false));
      fields.add(new Field(FN_TRANSIT_NUM,"Transit Routing Number",9,16,false));
      fields.add(new Field(FN_ACCOUNT_NUM,"Account Number",32,16,false));
      fields.add(new DropDownField(FN_ACCOUNT_TYPE,new AccountTypeTable(),false));
      fields.add(new DropDownField(FN_SF_SEC_CODE,new SecCodeTable(),false));
      fields.add(new CheckField(FN_CCD_ENABLE_FLAG,"CCD Enabled",AchBase.FLAG_YES,true));
      fields.add(new CheckField(FN_PPD_ENABLE_FLAG,"PPD Enabled",AchBase.FLAG_YES,true));
      fields.add(new CheckField(FN_WEB_ENABLE_FLAG,"WEB Enabled",AchBase.FLAG_YES,true));
      fields.add(new CheckField(FN_TEL_ENABLE_FLAG,"TEL Enabled",AchBase.FLAG_YES,true));
      fields.add(new DropDownField(FN_CCD_PEND_DAYS,new PendDaysTable(),false));
      fields.add(new DropDownField(FN_PPD_PEND_DAYS,new PendDaysTable(),false));
      fields.add(new DropDownField(FN_WEB_PEND_DAYS,new PendDaysTable(),false));
      fields.add(new DropDownField(FN_TEL_PEND_DAYS,new PendDaysTable(),false));
      fields.add(new NumberField(FN_BATCH_NUM,"Production Batch Number",6,6,false,0));
      fields.add(new NumberField(FN_TEST_BATCH_NUM,"Test Batch Number",6,6,false,0));
      fields.getField(FN_BATCH_NUM).addValidation(new BatchNumValidation());
      fields.getField(FN_TEST_BATCH_NUM).addValidation(new BatchNumValidation());
      fields.add(new DropDownField(FN_FT_ID,new FileTypeTable(),false));
      fields.add(new CheckField(FN_HOLD_FLAG,"Account Activity On Hold",AchBase.FLAG_YES,false));
      fields.add(new EmailField(FN_EMAIL_ADDR,"Email Address",300,50,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      boolean isNew = false;       
      String profileId = getData(FN_PROFILE_ID);
      profile = db().getMerchProfile(profileId);
      if (profile == null)
      {
        profile = new MerchProfile();
        profile.setProfileId(profileId);
        profile.setMerchNum(getData(FN_MERCH_NUM));
        profile.setMerchName(getData(FN_MERCH_NAME));
        isNew = true;
      }
      profile.setCompanyName(getData(FN_CO_NAME));
      profile.setTransitNum(getData(FN_TRANSIT_NUM));
      profile.setAccountNum(getData(FN_ACCOUNT_NUM));
      profile.setAccountType(getData(FN_ACCOUNT_TYPE));
      profile.setSfSecCode(SecCode.codeFor(getData(FN_SF_SEC_CODE)));
      profile.setCcdPendDays(getField(FN_CCD_PEND_DAYS).asInteger());
      profile.setPpdPendDays(getField(FN_PPD_PEND_DAYS).asInteger());
      profile.setWebPendDays(getField(FN_WEB_PEND_DAYS).asInteger());
      profile.setTelPendDays(getField(FN_TEL_PEND_DAYS).asInteger());
      profile.setCcdEnableFlag(getData(FN_CCD_ENABLE_FLAG).toUpperCase());
      profile.setPpdEnableFlag(getData(FN_PPD_ENABLE_FLAG).toUpperCase());
      profile.setWebEnableFlag(getData(FN_WEB_ENABLE_FLAG).toUpperCase());
      profile.setTelEnableFlag(getData(FN_TEL_ENABLE_FLAG).toUpperCase());
      profile.setBatchNum(getField(FN_BATCH_NUM).asInteger());
      profile.setTestBatchNum(getField(FN_TEST_BATCH_NUM).asInteger());
      profile.setFtId(getField(FN_FT_ID).asLong());
      profile.setEmailAddress(getData(FN_EMAIL_ADDR));
      profile.setHoldFlag(getData(FN_HOLD_FLAG).toUpperCase());
      if (isNew)
      {
        db().insertMerchProfile(profile);
      }
      else
      {
        db().updateMerchProfile(profile);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  private void loadProfile(MerchProfile profile)
  {
    setData(FN_PROFILE_ID,profile.getProfileId());
    setData(FN_MERCH_NUM,profile.getMerchNum());
    setData(FN_MERCH_NAME,profile.getMerchName());
    setData(FN_CO_NAME,profile.getCompanyName());
    setData(FN_TRANSIT_NUM,profile.getTransitNum());
    setData(FN_ACCOUNT_NUM,profile.getAccount().getAccountNum());
    setData(FN_ACCOUNT_TYPE,profile.getAccountType());
    setData(FN_SF_SEC_CODE,""+profile.getSfSecCode());
    setData(FN_CCD_ENABLE_FLAG,profile.getCcdEnableFlag());
    setData(FN_PPD_ENABLE_FLAG,profile.getPpdEnableFlag());
    setData(FN_WEB_ENABLE_FLAG,profile.getWebEnableFlag());
    setData(FN_TEL_ENABLE_FLAG,profile.getTelEnableFlag());
    setData(FN_CCD_PEND_DAYS,""+profile.getCcdPendDays());
    setData(FN_PPD_PEND_DAYS,""+profile.getPpdPendDays());
    setData(FN_WEB_PEND_DAYS,""+profile.getWebPendDays());
    setData(FN_TEL_PEND_DAYS,""+profile.getTelPendDays());
    setData(FN_BATCH_NUM,""+profile.getBatchNum());
    setData(FN_TEST_BATCH_NUM,""+profile.getTestBatchNum());
    setData(FN_HOLD_FLAG,profile.getHoldFlag());
    setData(FN_EMAIL_ADDR,profile.getEmailAddress());
    setData(FN_FT_ID,""+profile.getFtId());
  }

  protected boolean autoLoad()
  {
    try
    {
      String profileId = getData(FN_PROFILE_ID);
      if (profileId != null && profileId.length() > 0)
      {
        // attempt to load existing profile from db
        profile = db().getMerchProfile(profileId);

        // if none found, set some defaults
        if (profile == null)
        {
          MerchProfileRef ref = db().getMerchProfileRef(profileId);
          profile = new MerchProfile();
          profile.setProfileId(profileId);
          profile.setMerchNum(ref.getMerchNum());
          profile.setMerchName(ref.getMerchName());
          profile.setTransitNum(ref.getMifTransitNum());
          profile.setAccountNum(ref.getMifDdaNum());
          profile.setCcdEnableFlag(AchBase.FLAG_YES);
          profile.setPpdEnableFlag(AchBase.FLAG_YES);
          profile.setWebEnableFlag(AchBase.FLAG_YES);
          profile.setTelEnableFlag(AchBase.FLAG_YES);
          profile.setCcdPendDays(3);
          profile.setPpdPendDays(3);
          profile.setWebPendDays(3);
          profile.setTelPendDays(3);
          profile.setBatchNum(1);
          profile.setTestBatchNum(1);
          profile.setFtId(db().getDefaultFtId());
          profile.setSfSecCode(SecCode.CCD);
          profile.setCompanyName(ref.getMerchName().length() > 16 ?
            ref.getMerchName().substring(0,16) : ref.getMerchName());
          profile.setAccountType(AchAccount.AT_CHECKING);
          profile.setHoldFlag(profile.FLAG_NO);
        }

        // load profile data into fields
        loadProfile(profile);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  public MerchProfile getProfile()
  {
    return profile;
  }
}