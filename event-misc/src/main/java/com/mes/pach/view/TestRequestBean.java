package com.mes.pach.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchAccount;
import com.mes.pach.AchBase;
import com.mes.pach.AchViewBean;
import com.mes.pach.MerchProfile;
import com.mes.pach.SecCode;
import com.mes.pach.StatusReportParms;
import com.mes.pach.TestRequest;
import com.mes.pach.TranType;
import com.mes.tools.DropDownTable;

public class TestRequestBean extends AchViewBean
{
  static Logger log = Logger.getLogger(TestRequestBean.class);

  public static final String FN_TRANSIT_NUM       = "transitNum";
  public static final String FN_ACCOUNT_NUM       = "accountNum";
  public static final String FN_ACCOUNT_TYPE      = "accountType";
  public static final String FN_SEC_CODE          = "secCode";
  public static final String FN_TRAN_TYPE         = "tranType";
  public static final String FN_AMOUNT            = "amount";
  public static final String FN_REF_NUM           = "refNum";
  public static final String FN_CUST_NAME         = "custName";
  public static final String FN_CUST_ID           = "custId";
  public static final String FN_IP_ADDRESS        = "ipAddress";
  public static final String FN_RECUR_FLAG        = "recurFlag";
  public static final String FN_SUBMIT_BTN        = "submitBtn";

  private List rows;
  private MerchProfile profile;

  public TestRequestBean(MesAction action)
  {
    super(action);
  }

  public MerchProfile getProfile()
  {
    return profile;
  }

  public class AccountTypeTable extends DropDownTable
  {
    public AccountTypeTable()
    {
      addElement(AchAccount.AT_CHECKING,"Checking");
      addElement(AchAccount.AT_SAVINGS,"Savings");
    }
  }

  public class SecCodeTable extends DropDownTable
  {
    public SecCodeTable()
    {
      addElement(""+SecCode.CCD,""+SecCode.CCD);
      addElement(""+SecCode.PPD,""+SecCode.PPD);
      addElement(""+SecCode.WEB,""+SecCode.WEB);
      addElement(""+SecCode.TEL,""+SecCode.TEL);
    }
  }

  public class TranTypeTable extends DropDownTable
  {
    public TranTypeTable()
    {
      addElement(""+TranType.SALE,"Sale");
      addElement(""+TranType.CREDIT,"Credit");
    }
  }

  // ip address validation pattern
  public static final String IP_ADDR_PATTERN =
		    "^(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])$";
  public static Pattern ipPattern = Pattern.compile(IP_ADDR_PATTERN);

  public class IpAddressValidation implements Validation
  {
    String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
   {
      if (SecCode.WEB.equals(getData(FN_SEC_CODE)) && isBlank(data))
      {
        errorText = "Required for WEB requests";
        return false;
      }
      if (!isBlank(data))
      {
        Matcher matcher = ipPattern.matcher(data);
        boolean isValid = matcher.matches();
        if (!isValid)
        {
          errorText = "Invalid IP address";
        }
        return isValid;
      }
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      profile = (MerchProfile)request.getAttribute("profile");
      fields.add(new Field(FN_TRANSIT_NUM,"Transit Routing Number",9,16,false));
      fields.add(new Field(FN_ACCOUNT_NUM,"Account Number",32,16,false));
      fields.add(new DropDownField(FN_TRAN_TYPE,new TranTypeTable(),false));
      fields.add(new DropDownField(FN_ACCOUNT_TYPE,new AccountTypeTable(),false));
      fields.add(new DropDownField(FN_SEC_CODE,new SecCodeTable(),false));
      fields.add(new CurrencyField(FN_AMOUNT,"Amount",10,10,false));
      fields.add(new Field(FN_REF_NUM,"Reference Number",96,16,true));
      fields.add(new Field(FN_CUST_NAME,"Customer Name",22,16,false));
      fields.add(new Field(FN_CUST_ID,"Customer ID",15,16,false));
      fields.add(new Field(FN_IP_ADDRESS,"IP Address",16,16,true));
      fields.getField(FN_IP_ADDRESS).addValidation(new IpAddressValidation());
      fields.add(new CheckField(FN_RECUR_FLAG,"Recurring Transaction",AchBase.FLAG_YES,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  private String createTridentTranId()
  {
    String str = "ACHPSYSTEM" 
      + System.currentTimeMillis() + user.getLoginName();
    UUID uuId = UUIDGenerator.getInstance().generateNameBasedUUID(null,str);
    return uuId.toString().replaceAll("-","");
  }

  private void loadRows()
  {
    StatusReportParms parms = new StatusReportParms(user);
    parms.setProfileId(profile.getProfileId());
    parms.setBatchNum(profile.getTestBatchNum());
    parms.setCurBatchFlag(true);
    rows = db().getStatusReportRows(parms);
  }

  protected boolean autoSubmit()
  {
    try
    {
      TestRequest request = new TestRequest();
      request.setProfileId(profile.getProfileId());
      request.setMerchNum(profile.getMerchNum());
      request.setTranType(TranType.typeFor(getData(FN_TRAN_TYPE)));
      request.setTransitNum(getData(FN_TRANSIT_NUM));
      request.setAccountType(getData(FN_ACCOUNT_TYPE));
      request.setAccountNum(getData(FN_ACCOUNT_NUM));
      request.setAmount(new BigDecimal(getData(FN_AMOUNT)).setScale(2));
      request.setRefNum(getData(FN_REF_NUM));
      request.setSecCode(SecCode.codeFor(getData(FN_SEC_CODE)));
      request.setCustName(getData(FN_CUST_NAME));
      request.setCustId(getData(FN_CUST_ID));
      request.setRecurFlag(getData(FN_RECUR_FLAG).toUpperCase());
      request.setTestFlag(AchBase.FLAG_YES);
      request.setTridentTranId(createTridentTranId());
      request.setIpAddress(getData(FN_IP_ADDRESS));
      db().insertTestRequest(request);
      loadRows();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      loadRows();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}