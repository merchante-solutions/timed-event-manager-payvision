package com.mes.pach.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Mvc;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.AchViewBean;
import com.mes.pach.MerchProfile;

public class SelectTestBatchBean extends AchViewBean
{
  static Logger log = Logger.getLogger(SelectTestBatchBean.class);

  public static final String FN_FROM_DATE   = "fromDate";
  public static final String FN_TO_DATE     = "toDate";
  public static final String FN_SUBMIT_BTN  = "submitBtn";

  private List rows;
  private MerchProfile profile;

  public SelectTestBatchBean(MesAction action)
  {
    super(action);
  }

  public void setProfile(MerchProfile profile)
  {
    this.profile = profile;
  }
  public MerchProfile getProfile()
  {
    return profile;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      profile = (MerchProfile)request.getAttribute("profile");
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));

      // bean will be session stored, so have 
      // back links discard form field parameters
      fields.add(new HiddenField(Mvc.DISCARD_ARGS_FLAG)); 

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  private void loadBatches() throws Exception
  {
    String profileId = profile.getProfileId();
    Date fromDate = 
      ((DateStringField)(getField(FN_FROM_DATE))).getUtilDate();
    Field f = null;
    Date toDate = null;
    if (!(f = getField(FN_TO_DATE)).isBlank())
    {
      toDate = ((DateStringField)f).getUtilDate();
    }
    else
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(fromDate);
      cal.add(cal.DATE,1);
      toDate = cal.getTime();
    }
    rows = db().getTranBatches(profileId,fromDate,toDate,true);
  }

  protected boolean autoSubmit()
  {
    try
    {
      loadBatches();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
      // default to prior 2 weeks
      if (fromField.isBlank())
      {
        // to date to today
        toField.setUtilDate(cal.getTime());
        // set from date to 2 months ago
        cal.add(cal.DATE,-14);
        fromField.setUtilDate(cal.getTime());
      }
      loadBatches();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}