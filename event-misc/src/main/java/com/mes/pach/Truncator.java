package com.mes.pach;

/**
 * Alternative account number truncation utility.
 */
public class Truncator
{
  private String  truncChar;
  private int     minLeftClear;
  private int     maxLeftClear;
  private int     minRightClear;
  private int     maxRightClear;
  private int     minTrunc;
  private int     minTruncVis;
  private int     maxTruncVis;

  public Truncator(String tc, int minlc, int maxlc, int minrc, int maxrc, 
    int mint, int mintv, int maxtv)
  {

    truncChar     = tc;
    minLeftClear  = minlc;
    maxLeftClear  = maxlc;
    minRightClear = minrc;
    maxRightClear = maxrc;
    minTrunc      = mint;
    minTruncVis   = mintv;
    maxTruncVis   = maxtv;
  }

  public String truncate(String data)
  {
    // don't truncate if less than minimum length:
    // min num digits on left, right and truncated
    int minLen = minLeftClear + minRightClear + minTrunc;
    if (data == null || data.length() < minLen) return data;
    int dataLen = data.length();

    // determine right clear
    int rightClear = dataLen - minTrunc - minLeftClear;
    rightClear = (rightClear > maxRightClear ? maxRightClear : rightClear);

    // determine left clear
    int leftClear = dataLen - minTrunc - rightClear;
    leftClear = (leftClear > maxLeftClear ? maxLeftClear : leftClear);

    // determine visible length of truncated chars
    int truncVis = dataLen - leftClear - rightClear;
    truncVis = (truncVis < minTruncVis ? minTruncVis : 
                (truncVis > maxTruncVis ? maxTruncVis : truncVis));

    // generate truncation string
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < truncVis; ++i) buf.append(truncChar);

    // add visible portions
    buf.insert(0,data.substring(0,leftClear));
    buf.append(data.substring(dataLen - rightClear));

    return ""+buf;
  }
}
  
