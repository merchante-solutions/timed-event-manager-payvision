package com.mes.pach;

import org.apache.log4j.Logger;

public class StatusReportRow extends AchTransaction
{
  static Logger log = Logger.getLogger(StatusReportRow.class);

  private String reportStatus;
  private int batchNum;

  public void setReportStatus(String reportStatus)
  {
    this.reportStatus = reportStatus;
  }
  public String getReportStatus()
  {
    return reportStatus;
  }

  public void setBatchNum(int batchNum)
  {
    this.batchNum = batchNum;
  }
  public int getBatchNum()
  {
    return batchNum;
  }

  public String toString()
  {
    return super.toString()
      + "\nStatusReportRow extensions:"
      + "\n  reportStatus: " + reportStatus
      + "\n  batchNum:     " + batchNum;
  }
}