package com.mes.pach;

import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

public class AchFile extends AchBase
{
  static Logger log = Logger.getLogger(AchFile.class);

  private long    fileId;
  private Date    createDate;
  private long    ftId;
  private Destination
                  destination;
  private String  destinationId;
  private String  destinationName;
  private String  originId;
  private String  originName;
  private String  testFlag;
  private Date    uploadDate;
  private int     fileSize;
  private String  fileName;
  private Date    transmitDate;

  public void setFileId(long fileId)
  {
    this.fileId = fileId;
  }
  public long getFileId()
  {
    return fileId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setUploadDate(Date uploadDate)
  {
    this.uploadDate = uploadDate;
  }
  public Date getUploadDate()
  {
    return uploadDate;
  }
  public void setUploadTs(Timestamp uploadTs)
  {
    uploadDate = toDate(uploadTs);
  }
  public Timestamp getUploadTs()
  {
    return toTimestamp(uploadDate);
  }

  public void setTransmitDate(Date transmitDate)
  {
    this.transmitDate = transmitDate;
  }
  public Date getTransmitDate()
  {
    return transmitDate;
  }
  public void setTransmitTs(Timestamp transmitTs)
  {
    transmitDate = toDate(transmitTs);
  }
  public Timestamp getTransmitTs()
  {
    return toTimestamp(transmitDate);
  }

  public void setDestination(Destination destination)
  {
    this.destination = destination;
  }
  public Destination getDestination()
  {
    return destination;
  }

  public void setDestinationId(String destinationId)
  {
    this.destinationId = destinationId;
  }
  public String getDestinationId()
  {
    return destinationId;
  }

  public void setDestinationName(String destinationName)
  {
    this.destinationName = destinationName;
  }
  public String getDestinationName()
  {
    return destinationName;
  }

  public void setOriginId(String originId)
  {
    this.originId = originId;
  }
  public String getOriginId()
  {
    return originId;
  }

  public void setOriginName(String originName)
  {
    this.originName = originName;
  }
  public String getOriginName()
  {
    return originName;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  public String getFileName()
  {
    return fileName;
  }

  public void setFileSize(int fileSize)
  {
    this.fileSize = fileSize;
  }
  public int getFileSize()
  {
    return fileSize;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }
  
  public String toString()
  {
    return "AchFile"
      + "\n  fileId:          " + fileId
      + "\n  createDate:      " + createDate
      + "\n  ftId:            " + ftId
      + "\n  destination:     " + destination
      + "\n  destinationId:   " + destinationId
      + "\n  destinationName: " + destinationName
      + "\n  originId:        " + originId
      + "\n  originName:      " + originName
      + "\n  testFlag:        " + isTest()
      + "\n  uploadDate:      " 
      + (uploadDate != null ? ""+uploadDate : "--")
      + "\n  fileName:        " + fileName
      + "\n  fileSize:        " + fileSize
      + "\n  transmitDate:    " 
      + (transmitDate != null ? ""+transmitDate : "--");
  }
}
