package com.mes.pach.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.mvc.Mvc;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.MerchProfile;
import com.mes.pach.PachDb;
import com.mes.pach.StatusReportParms;
import com.mes.pach.TranBatch;
import com.mes.pach.TranDetailData;
import com.mes.pach.proc.TestBatchRun;
import com.mes.pach.view.EditFileTypeBean;
import com.mes.pach.view.EditProfileBean;
import com.mes.pach.view.LookupProfilesBean;
import com.mes.pach.view.SelectTestBatchBean;
import com.mes.pach.view.SelectTestProfileBean;
import com.mes.pach.view.StatusReportBean;
import com.mes.pach.view.TestRequestBean;

public class PachAction extends MesAction
{
  static Logger log = Logger.getLogger(PachAction.class);

  protected PachDb db = new PachDb();

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma z'</nobr>'");

  private static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    if (date == null)
    {
      return "--";
    }
    return sdf.format(date);
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  public static final String SN_LOOKUP_PROFILES_BEAN = "lookupProfilesBean";

  private void setLookupProfilesBean()
  {
    LookupProfilesBean bean = (LookupProfilesBean)
      getSessionAttr(SN_LOOKUP_PROFILES_BEAN,null);
    if (bean == null)
    {
      bean = new LookupProfilesBean(this);
      setSessionAttr(SN_LOOKUP_PROFILES_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
  }

  public void doPachProfiles()
  {
    setLookupProfilesBean();
    doView("pachProfiles");
  }

  public void doPachAddProfile()
  {
    EditProfileBean bean = new EditProfileBean(this);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("ACH payment system profile created");
    }
    doView("pachAddProfile");
  }

  public void doPachEditProfile()
  {
    EditProfileBean bean = new EditProfileBean(this);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("ACH payment system profile updated");
    }
    doView("pachEditProfile");
  }

  public void doPachEditProfileFromCif()
  {
    handler.disableBackLink();
    doPachEditProfile();
  }

  public void doPachAddProfileFromCif()
  {
    handler.disableBackLink();
    doPachAddProfile();
  }

  public void doPachAddProfileFromTri()
  {
    handler.disableBackLink();
    doPachAddProfile();
  }

  public void doPachEditProfileFromTri()
  {
    handler.disableBackLink();
    doPachEditProfile();
  }

  public void doPachShowFileTypes()
  {
    request.setAttribute("showItems",db.getAchFileTypes());
    doView("pachShowFileTypes");
  }

  public void doPachEditFileType()
  {
    EditFileTypeBean bean = new EditFileTypeBean(this);
    boolean isEdit = name.equals("pachEditFileType");
        
    if (bean.isAutoSubmitOk())
    {
      String description = "File type " + (isEdit ? "updated" : "added");
      addFeedback(description);
      doActionRedirect("pachShowFileTypes");
    }
    else
    {
      doView(isEdit ? "pachEditFileType" : "pachAddFileType");
    }
  }

  public void doPachAddFileType()
  {
    doPachEditFileType();
  }

  public static final String SN_STATUS_REPORT_BEAN = "statusReportBean";

  private StatusReportBean getStatusReportBean()
  {
    StatusReportBean bean = (StatusReportBean)
      getSessionAttr(SN_STATUS_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new StatusReportBean(this);
      setSessionAttr(SN_STATUS_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doPachStatusReport()
  {
    StatusReportBean bean = getStatusReportBean();
    doView("pachStatusReport");
  }

  public void doPachDlStatusReport()
  {
    doCsvDownload(getStatusReportBean());
  }

  public void doPachTransactionDetail()
  {
    long tranId = getParmLong("tranId");
    TranDetailData dtl = db.getTranDetailData(tranId);
    request.setAttribute("logs",dtl.logs());
    request.setAttribute("detail",dtl);
    if (handler.backLinkEnabled())
    {
      handler.getBackLink().addArg(Mvc.DISCARD_ARGS_FLAG);
    }
    doView("pachTransactionDetail");
  }

  public void doPachTransactionDetailExtern()
  {
    handler.disableBackLink();
    doPachTransactionDetail();
  }

  public static final String SN_TEST_PROFILE = "testProfile";

  /**
   * Has user pick a profile to use for testing functions.  The session is 
   * stored in the session and the user is then redirected to their original 
   * destination.
   */
  public void doPachTestSelectProfile()
  {
    SelectTestProfileBean bean = new SelectTestProfileBean(this);
    if (bean.isAutoSubmitOk())
    {
      setSessionAttr(SN_TEST_PROFILE,bean.getProfile());
      if (bean.isRequestOp())
      {
        handler.doActionRedirect("pachTestRequest");
      }
      else
      {
        handler.doActionRedirect("pachTestSelectBatch");
      }
      return;
    }
    doView("pachTestSelectProfile");
  }

  /**
   * Look for a selected test profile in the session and place it in the request.
   * If no profile found then redirect to the profile selection screen to have
   * user specify a profile.  The op type is stored in the selection bean and used
   * to determine where to send the user after they've selected a profile (see
   * doPachTestSelectProfile()).  Profiles are automatically reloaded with
   * each request to catch profile changes that may have occurred.
   */
  private boolean setTestProfile(String opType)
  {
    MerchProfile profile = (MerchProfile)getSessionAttr(SN_TEST_PROFILE,null);
    if (profile == null)
    {
      handler.setBackLink("pachTestSelectProfile",handler.getBackLink());
      handler.doActionRedirect("pachTestSelectProfile",
        SelectTestProfileBean.FN_OP_TYPE + "=" + opType);
      return false;
    }
    else
    {
      profile = db.getMerchProfile(profile.getProfileId());
      setSessionAttr(SN_TEST_PROFILE,profile);
    }
    request.setAttribute("profile",profile);
    return true;
  }

  public void doPachTestRequest()
  {
    if (setTestProfile(SelectTestProfileBean.OT_REQUEST))
    {
      TestRequestBean bean = new TestRequestBean(this);
      if (bean.isAutoSubmitOk())
      {
        addFeedback("Test request submitted");
      }
      // manually place batch row list into request to force
      // batch table to always display (the table logic will
      // not try to display rows from a bean unless the bean
      // indicates a submit took place)
      request.setAttribute("batch",bean.getRows());
      doView("pachTestRequest");
    }
  }

  public void doPachTestProcessBatch()
  {
    //addFeedback("Under construction");
    //handler.doActionRedirect("pachTestRequest");
    String profileId = getParm("profileId");
    TranBatch batch = db.getTranBatch(profileId,"Y");
    TestBatchRun tbr 
      = new TestBatchRun(false,batch.getBatchId(),user.getLoginName());
    tbr.run();
    handler.setBackLink("pachTestViewBatch",handler.getBackLink());
    handler.getBackLink("pachTestViewBatch").addArg(Mvc.DISCARD_ARGS_FLAG);
    doActionRedirect("pachTestViewBatch","batchId=" + batch.getBatchId());
  }

  public static final String SN_SEL_TEST_BATCH_BEAN = "selTestBatchBean";

  private SelectTestBatchBean getSelectTestBatchBean()
  {
    SelectTestBatchBean bean = (SelectTestBatchBean)
      getSessionAttr(SN_SEL_TEST_BATCH_BEAN,null);
    if (bean == null)
    {
      bean = new SelectTestBatchBean(this);
      setSessionAttr(SN_SEL_TEST_BATCH_BEAN,bean);
    }
    else
    {
      // alwas set the profile in case new profile has been selected
      bean.setProfile((MerchProfile)request.getAttribute("profile"));
      bean.setAction(this);
    }
    return bean;
  }

  public void doPachTestSelectBatch()
  {
    if (setTestProfile(SelectTestProfileBean.OT_BATCH))
    {
      SelectTestBatchBean bean = getSelectTestBatchBean();
      request.setAttribute("batches",bean.getRows());
      doView("pachTestSelectBatch");
    }
  }

  public void doPachTestViewBatch()
  {
    if (setTestProfile(SelectTestProfileBean.OT_BATCH))
    {
      // fetch the batch
      long batchId = getParmLong("batchId");
      TranBatch batch = db.getTranBatch(batchId);
      request.setAttribute("batch",batch);

      // get processing details
      request.setAttribute("jobs",db.getBatchJobs(batchId));

      // get tran details
      StatusReportParms parms = new StatusReportParms(user);
      parms.setProfileId(batch.getProfileId());
      parms.setBatchId(batchId);
      request.setAttribute("trans",db.getStatusReportRows(parms));

      // get return records
      request.setAttribute("returns",db.getBatchReturns(batchId));

      // get ach details
      request.setAttribute("details",db.getBatchAchDetails(batchId));

      // get funding entries
      request.setAttribute("entries",db.getBatchFundingEntries(batchId));

      doView("pachTestViewBatch");
    }
  }
}
