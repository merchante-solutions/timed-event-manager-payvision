package com.mes.pach;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * Settlement funding summary.
 */

public class FundingSummary extends AchBase
{
  static Logger log = Logger.getLogger(FundingSummary.class);

  private long        summaryId;
  private Date        summaryDate;
  private Date        cutoffDate;
  private String      merchNum;
  private String      profileId;
  private long        ftId;
  private int         saleCount;
  private BigDecimal  saleTotal;
  private int         creditCount;
  private BigDecimal  creditTotal;
  private int         saleRetAdjCount;
  private BigDecimal  saleRetAdjTotal;
  private int         creditRetAdjCount;
  private BigDecimal  creditRetAdjTotal;
  private int         entryCount;
  private BigDecimal  netTotal;
  private String      testFlag;

  public void setSummaryId(long summaryId)
  {
    this.summaryId = summaryId;
  }
  public long getSummaryId()
  {
    return summaryId;
  }

  public void setSummaryDate(Date summaryDate)
  {
    this.summaryDate = summaryDate;
  }
  public Date getSummaryDate()
  {
    return summaryDate;
  }
  public void setSummaryTs(Timestamp recTs)
  {
    summaryDate = toDate(recTs);
  }
  public Timestamp getSummaryTs()
  {
    return toTimestamp(summaryDate);
  }

  public void setCutoffDate(Date cutoffDate)
  {
    this.cutoffDate = cutoffDate;
  }
  public Date getCutoffDate()
  {
    return cutoffDate;
  }
  public void setCutoffTs(Timestamp recTs)
  {
    cutoffDate = toDate(recTs);
  }
  public Timestamp getCutoffTs()
  {
    return toTimestamp(cutoffDate);
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setSaleCount(int saleCount)
  {
    this.saleCount = saleCount;
  }
  public int getSaleCount()
  {
    return saleCount;
  }

  public void setSaleTotal(BigDecimal saleTotal)
  {
    this.saleTotal = saleTotal;
  }
  public BigDecimal getSaleTotal()
  {
    return saleTotal;
  }

  public void setCreditCount(int creditCount)
  {
    this.creditCount = creditCount;
  }
  public int getCreditCount()
  {
    return creditCount;
  }

  public void setCreditTotal(BigDecimal creditTotal)
  {
    this.creditTotal = creditTotal;
  }
  public BigDecimal getCreditTotal()
  {
    return creditTotal;
  }

  public void setSaleRetAdjCount(int saleRetAdjCount)
  {
    this.saleRetAdjCount = saleRetAdjCount;
  }
  public int getSaleRetAdjCount()
  {
    return saleRetAdjCount;
  }

  public void setSaleRetAdjTotal(BigDecimal saleRetAdjTotal)
  {
    this.saleRetAdjTotal = saleRetAdjTotal;
  }
  public BigDecimal getSaleRetAdjTotal()
  {
    return saleRetAdjTotal;
  }

  public void setCreditRetAdjCount(int creditRetAdjCount)
  {
    this.creditRetAdjCount = creditRetAdjCount;
  }
  public int getCreditRetAdjCount()
  {
    return creditRetAdjCount;
  }

  public void setCreditRetAdjTotal(BigDecimal creditRetAdjTotal)
  {
    this.creditRetAdjTotal = creditRetAdjTotal;
  }
  public BigDecimal getCreditRetAdjTotal()
  {
    return creditRetAdjTotal;
  }

  public void setEntryCount(int entryCount)
  {
    this.entryCount = entryCount;
  }
  public int getEntryCount()
  {
    return entryCount;
  }

  public void setNetTotal(BigDecimal netTotal)
  {
    this.netTotal = netTotal;
  }
  public BigDecimal getNetTotal()
  {
    return netTotal;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public String toString()
  {
    return "FundingSummary"
      + "\n  summaryId:         " + summaryId
      + "\n  summaryDate:       " + summaryDate
      + "\n  cutoffDate:        " + cutoffDate
      + "\n  merchNum:          " + merchNum
      + "\n  profileId:         " + profileId
      + "\n  ftId:              " + ftId
      + "\n  saleCount:         " + saleCount
      + "\n  saleTotal:         " + saleTotal
      + "\n  creditCount:       " + creditCount
      + "\n  creditTotal:       " + creditTotal
      + "\n  saleRetAdjCount:   " + saleRetAdjCount
      + "\n  saleRetAdjTotal:   " + saleRetAdjTotal
      + "\n  creditRetAdjCount: " + creditRetAdjCount
      + "\n  creditRetAdjTotal: " + creditRetAdjTotal
      + "\n  entryCount:        " + entryCount
      + "\n  netTotal:          " + netTotal
      + "\n  testFlag:          " + isTest();
  }
}