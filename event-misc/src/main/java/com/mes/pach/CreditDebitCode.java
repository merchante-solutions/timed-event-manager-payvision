package com.mes.pach;

import java.util.HashMap;

public class CreditDebitCode
{
  // ach file detail credit debit codes, indicates if 
  // detail will credit or debit the target account
  public static final CreditDebitCode DEBIT   = new CreditDebitCode("D");
  public static final CreditDebitCode CREDIT  = new CreditDebitCode("C");

  private static HashMap codeHash = new HashMap();

  static
  {
    codeHash.put(""+DEBIT,DEBIT);
    codeHash.put(""+CREDIT,CREDIT);
  };

  private String codeStr;

  private CreditDebitCode(String codeStr)
  {
    this.codeStr = codeStr;
  }

  public static CreditDebitCode codeFor(String forStr, 
    CreditDebitCode dflt)
  {
    CreditDebitCode code 
      = (CreditDebitCode)codeHash.get(forStr);
    return code != null ? code : dflt;
  }
    
  public static CreditDebitCode codeFor(String forStr)
  {
    CreditDebitCode code = codeFor(forStr,null);
    if (code == null)
    {
      throw new NullPointerException("Invalid code string: '" 
        + forStr + "'");
    }
    return code;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof CreditDebitCode))
    {
      return false;
    }
    CreditDebitCode that = (CreditDebitCode)o;
    return that.codeStr.equals(codeStr);
  }

  public String toString()
  {
    return codeStr;
  }
}