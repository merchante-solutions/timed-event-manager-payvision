package com.mes.pach;

import org.apache.log4j.Logger;

/**
 * Temporary profile data container, should eventually be moved into 
 * trident profile data.
 */
public class MerchProfile extends AchBase
{
  static Logger log = Logger.getLogger(MerchProfile.class);

  public static final int DFLT_PEND_DAYS = 3;

  private String profileId;
  private String merchNum;
  private String merchName;
  private String companyName;
  private AchAccount account = new AchAccount();
  private SecCode sfSecCode = SecCode.CCD;
  private String enableFlag;
  private String ccdEnableFlag;
  private String ppdEnableFlag;
  private String webEnableFlag;
  private String telEnableFlag;
  private int pendDays      = DFLT_PEND_DAYS;
  private int ccdPendDays   = DFLT_PEND_DAYS;
  private int ppdPendDays   = DFLT_PEND_DAYS;
  private int webPendDays   = DFLT_PEND_DAYS;
  private int telPendDays   = DFLT_PEND_DAYS;
  private long ftId;
  private String holdFlag;
  private int batchNum;
  private int testBatchNum;
  private String emailAddress;

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setCompanyName(String companyName)
  {
    this.companyName = companyName;
  }
  public String getCompanyName()
  {
    return companyName;
  }

  public AchAccount getAccount()
  {
    return account;
  }

  public void setAccountNum(String accountNum)
  {
    account.setAccountNum(accountNum);
  }
  public String getAccountNum()
  {
    return account.truncated();
  }

  public void setAccountType(String accountType)
  {
    account.setAccountType(accountType);
  }
  public String getAccountType()
  {
    return account.getAccountType();
  }

  public void setTransitNum(String transitNum)
  {
    account.setTransitNum(transitNum);
  }
  public String getTransitNum()
  {
    return account.getTransitNum();
  }

  public boolean isCheckingAccount()
  {
    return account.isCheckingAccount();
  }
  public boolean isSavingsAccount()
  {
    return account.isSavingsAccount();
  }

  public void setSfSecCode(SecCode sfSecCode)
  {
    this.sfSecCode = sfSecCode;
  }
  public SecCode getSfSecCode()
  {
    return sfSecCode;
  }

  public void setFtId(long ftId)
  {
    this.ftId = ftId;
  }
  public long getFtId()
  {
    return ftId;
  }

  public void setEnableFlag(String enableFlag)
  {
    validateFlag(enableFlag,"enable");
    this.enableFlag = enableFlag;
  }
  public String getEnableFlag()
  {
    return enableFlag;
  }
  public boolean isEnabled()
  {
    return flagBoolean(enableFlag);
  }

  public void setCcdEnableFlag(String ccdEnableFlag)
  {
    validateFlag(ccdEnableFlag,"ccdEnable");
    this.ccdEnableFlag = ccdEnableFlag;
  }
  public String getCcdEnableFlag()
  {
    return ccdEnableFlag;
  }
  public boolean ccdEnabled()
  {
    return flagBoolean(ccdEnableFlag);
  }

  public void setPpdEnableFlag(String ppdEnableFlag)
  {
    validateFlag(ppdEnableFlag,"ppdEnable");
    this.ppdEnableFlag = ppdEnableFlag;
  }
  public String getPpdEnableFlag()
  {
    return ppdEnableFlag;
  }
  public boolean ppdEnabled()
  {
    return flagBoolean(ppdEnableFlag);
  }

  public void setWebEnableFlag(String webEnableFlag)
  {
    validateFlag(webEnableFlag,"webEnable");
    this.webEnableFlag = webEnableFlag;
  }
  public String getWebEnableFlag()
  {
    return webEnableFlag;
  }
  public boolean webEnabled()
  {
    return flagBoolean(webEnableFlag);
  }

  public void setTelEnableFlag(String telEnableFlag)
  {
    validateFlag(telEnableFlag,"telEnable");
    this.telEnableFlag = telEnableFlag;
  }
  public String getTelEnableFlag()
  {
    return telEnableFlag;
  }
  public boolean telEnabled()
  {
    return flagBoolean(telEnableFlag);
  }

  public void setPendDays(int pendDays)
  {
    this.pendDays = pendDays;
  }
  public int getPendDays()
  {
    return pendDays;
  }

  public void setCcdPendDays(int ccdPendDays)
  {
    this.ccdPendDays = ccdPendDays;
  }
  public int getCcdPendDays()
  {
    return ccdPendDays;
  }

  public void setPpdPendDays(int ppdPendDays)
  {
    this.ppdPendDays = ppdPendDays;
  }
  public int getPpdPendDays()
  {
    return ppdPendDays;
  }

  public void setWebPendDays(int webPendDays)
  {
    this.webPendDays = webPendDays;
  }
  public int getWebPendDays()
  {
    return webPendDays;
  }

  public void setTelPendDays(int telPendDays)
  {
    this.telPendDays = telPendDays;
  }
  public int getTelPendDays()
  {
    return telPendDays;
  }

  public void setHoldFlag(String holdFlag)
  {
    validateFlag(holdFlag,"hold");
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return holdFlag;
  }
  public boolean isHeld()
  {
    return flagBoolean(holdFlag);
  }

  public void setBatchNum(int batchNum)
  {
    this.batchNum = batchNum;
  }
  public int getBatchNum()
  {
    return batchNum;
  }

  public void setTestBatchNum(int testBatchNum)
  {
    this.testBatchNum = testBatchNum;
  }
  public int getTestBatchNum()
  {
    return testBatchNum;
  }

  public void setEmailAddress(String emailAddress)
  {
    this.emailAddress = emailAddress;
  }
  public String getEmailAddress()
  {
    return emailAddress;
  }

  public String toString()
  {
    return "MerchProfile"
      + "\n  merchNum:      " + merchNum
      + "\n  profileId:     " + profileId
      + "\n  transitNum:    " + getTransitNum()
      + "\n  accountNum:    " + getAccountNum()
      + "\n  accountType:   " + getAccountType()
      + "\n  sfSecCode:     " + sfSecCode
      + "\n  ftId:          " + ftId
      + "\n  enabled:       " + isEnabled()
      + "\n  ccdEnabled:    " + ccdEnabled()
      + "\n  ppdEnabled:    " + ppdEnabled()
      + "\n  webEnabled:    " + webEnabled()
      + "\n  telEnabled:    " + telEnabled()
      + "\n  pendDays:      " + pendDays
      + "\n  ppdPendDays:   " + ppdPendDays
      + "\n  ccdPendDays:   " + ccdPendDays
      + "\n  webPendDays:   " + webPendDays
      + "\n  telPendDays:   " + telPendDays
      + "\n  batchNum:      " + batchNum
      + "\n  testBatchNum:  " + testBatchNum
      + "\n  emailAddress:  " + emailAddress
      + "\n  holdFlag:      " + isHeld();
  }
}
