/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/vslistener/VirtualAppListener.java $

  Description:

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/06/04 1:02p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.vslistener;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.virtualapp.VirtualAppProcessor;

public class VirtualAppListener extends HttpServlet
{
  public static final int   MAX_READ    = 20000;
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    String              respMsg         = "";
    
    VirtualAppProcessor vaProcessor     = new VirtualAppProcessor();
    
    try
    {
      int             contentLength   = request.getContentLength();
      long            startMillis     = System.currentTimeMillis();
      BufferedReader  in              = request.getReader();
      String          xmlApp          = "";
      
      // show start time
      //System.out.println("XML APP start: " + DateTimeFormatter.getCurDateTimeString());
      
      if(contentLength > 0)
      {
        // get posted data based on contentLength
        char[] buf = new char[contentLength];
      
        int numRead = 0;
      
        while(numRead < contentLength)
        {
          if(contentLength - numRead > MAX_READ)
          {
            numRead += in.read(buf, numRead, MAX_READ);
          }
          else
          {
            numRead += in.read(buf, numRead, contentLength - numRead);
          }
        }
      
        // URL decode in case VeriSign decided to encode it
        xmlApp = URLDecoder.decode(String.valueOf(buf), StandardCharsets.UTF_8.name());
        
        // remove unnecessary preceding name if present
        if(xmlApp.substring(0,4).equals("xml="))
        {
          xmlApp = xmlApp.substring(4);
        }
        
        // pass xml app to validation engine
        respMsg = vaProcessor.getPreApprovalMessage(xmlApp, startMillis);

        // set content type
        response.setContentType("text/xml");

        response.setContentLength(respMsg.length());
        
        // send message back to listener immedately
        PrintWriter out = response.getWriter();
        
        out.println(respMsg);
        out.flush();
        response.flushBuffer();
        
        /*
        ** THIS IS NOW HANDLED BY A TIMED EVENT RUNNING ON VAPP_XML_PROCESS
        // now actually process the app (add it to the database)
        vaProcessor.process(request);
        */
      }
      else
      {
        respMsg="Missing parameter.";
      }
      
      // show end time
      //System.out.println("XML APP end:   " + DateTimeFormatter.getCurDateTimeString() + "\n\n");
    }
    catch(Exception e)
    {
      respMsg = e.toString();
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doGet()", e.toString());
    }
    
    try
    {
      // respond with message
      PrintWriter out   = response.getWriter();
      out.println(respMsg);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doGet() - 2", e.toString());
    }
  }
  
  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    doGet(request, response);
  }
}
