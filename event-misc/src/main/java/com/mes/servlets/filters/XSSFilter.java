/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/filters/XSSFilter.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.filters;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.log4j.Logger;
//import weblogic.servlet.security.Utils;

public class XSSFilter implements Filter
{
  static Logger log = Logger.getLogger(XSSFilter.class);
  
  private FilterConfig  filterConfig = null;
  
  public void init(FilterConfig config)
    throws ServletException
  {
    filterConfig = config;
  }
  
  protected FilterConfig getFilterConfig( )
  {
    return( filterConfig );
  }
  
  public void destroy( )
  {
    //filterConfig = null;
  }
  
  public void doFilter(ServletRequest request, ServletResponse response, 
    FilterChain chain)
  {
    try
    {
      if (request != null && request instanceof HttpServletRequest) 
      {
        HttpServletRequest req = (HttpServletRequest) request;
        String type = req.getHeader("Content-Type");

        // If this is not a multipart/form-data request then encode for XSS
        if (type == null || !type.startsWith("multipart/form-data")) 
        {
          chain.doFilter( new XSSFilterRequest((HttpServletRequest) request), response);
        }
        else 
        {
          // just pass it along
          chain.doFilter(request, response);
        }
      }
      else
      {
        chain.doFilter(request, response);
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.servlets.filters.XSSFilter::doFilter(): " + e.toString());
      e.printStackTrace();
    }
  }
  
  public class XSSFilterRequest extends HttpServletRequestWrapper
  {
    public XSSFilterRequest(HttpServletRequest request)
    {
      super(request);
    }
    
    public String[] getParameterValues(String parameter)
    {
      String[] results = super.getParameterValues(parameter);
      String[] xssResults = null;
      
      if(results != null)
      {
        int count = results.length;
        
        xssResults = new String[count];
        
        for(int i=0; i<count; ++i)
        {
          xssResults[i] = doEncodeXSS(results[i]);
        }
      }
      
      return (xssResults);
    }
    
    public String getParameter(String name)
    {
      String newVal = doEncodeXSS(super.getParameter(name));
      
      return( doEncodeXSS(super.getParameter(name)) );
    }
    
    public String doEncodeXSS(String val)
    {
      String retVal = null;
      
      if( val != null )
      {
        //retVal = Utils.encodeXSS(val);
      }
      
      return( retVal );
      
    }
  }
}