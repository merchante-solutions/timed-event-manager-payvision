/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/BCBLoginServlet.java $

  Description:
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-11-08 12:03:41 -0800 (Thu, 08 Nov 2007) $
  Version            : $Revision: 14308 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.user.ClientLoginCookie;

/*
** GenericLoginServlet routes users to the necessary Login page
*/
public class GenericLoginServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      // determine url for login page
      int client = HttpHelper.getInt(request, "client", ClientLoginCookie.CLIENT_DEFAULT);
      
      ClientLoginCookie clc = new ClientLoginCookie(client);
      response.addCookie(clc.getCookie());
      
      StringBuffer            forwardPage = new StringBuffer(HttpHelper.getServerURL(request));
      
      forwardPage.append(clc.getLoginPage());
      
      // send the browser to the new location
      response.sendRedirect(forwardPage.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
