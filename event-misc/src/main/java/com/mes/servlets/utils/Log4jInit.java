package com.mes.servlets.utils;

import java.net.URL;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.mes.support.HttpHelper;

public class Log4jInit implements ServletContextListener
{
  public static final String IP_DEV_CFG_FILE    = "log4j-init-file-development";
  public static final String IP_PROD_CFG_FILE   = "log4j-init-file-production";
  
  public void contextInitialized(ServletContextEvent contextEvent)
  {
    try
    {
      String parmName = 
        HttpHelper.isDevServer(null) ? IP_DEV_CFG_FILE : IP_PROD_CFG_FILE;
        
      //String filePath = getInitParameter(parmName);
      String filePath = contextEvent.getServletContext().getInitParameter(parmName);
      
      if (filePath == null)
      {
        throw new NullPointerException(this.getClass().getName() + " servlet"
          + " missing init-param " + parmName);
      }
      System.out.println("log4j initializing to settings in " + filePath + "...");

      URL configUrl = contextEvent.getServletContext().getResource(filePath);
      if (configUrl == null)
      {
        throw new NullPointerException("URL not found");
      }

      PropertyConfigurator.configure(configUrl);
      Logger log = Logger.getLogger(this.getClass());
      log.info("log4j successfully initialized by Log4jInit servlet");
    }
    catch (Exception e)
    {
      System.out.println("Error in Log4jInit servlet: " + e);
    }
  }
  
  public void contextDestroyed(ServletContextEvent arg0)
  {
  }
}



