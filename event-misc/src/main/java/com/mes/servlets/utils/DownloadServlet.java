/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/DownloadServlet.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import java.io.IOException;
import java.sql.ResultSet;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.mesConstants;
import com.mes.database.ResultSetEncoder;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueBase;
import com.mes.reports.ReportDataBean;
import com.mes.reports.ReportSQLJBean;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;

public class DownloadServlet extends MesServletBase
{
  private static int DS_UNKNOWN               = 0;
  private static int DS_REPORT_DATA_BEAN      = 1;
  private static int DS_STORED_PROCEDURE      = 2;
  private static int DS_REPORT_SQLJ_BEAN      = 3;
  private static int DS_REPORT_QUEUE_BEAN     = 4;
  private static int DS_REPORT_DATABASE_BEAN  = 5;
  private static int DS_REPORT_API            = 6;
  
  private void sendErrorMessage(HttpServletResponse response, String errorMsg)
    throws IOException
  {
    ServletOutputStream out = response.getOutputStream();
    out.print("\r\n      <html>\r\n      <head>\r\n      <title>Export Failed</title>\r\n      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n      </head>   \r\n      <body bgcolor=\"#FFFFFF\">\r\n      Failed to export data:  ");
    out.print("\r\n");
    out.print(errorMsg + "\r\n");
    out.print("\r\n      </body>\r\n      </html>\r\n");
  }
  
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    String                beanName        = null;
    int                   dsReportId      = -1;
    int                   encoding        = -1;
    QueueBase             queueBean       = null;
    ReportSQLJBean        reportBean      = null;
    SQLJConnectionBase    db              = null;
    boolean               timeoutExempt   = false;
    UserBean              userBean        = null;
    
    try
    {
      int         dataSource      = DS_UNKNOWN;
      String      errorMsg        = null;
      String      filenameBase    = "";
      ResultSet   resultSet       = null;
      
      long startt = System.currentTimeMillis();
      
      // try to load data from a report data bean
      try
      {
        beanName = request.getParameter("reportDataBean");
        ReportDataBean dataBean = (ReportDataBean)Class.forName(beanName).newInstance();
        dataSource      = DS_REPORT_DATA_BEAN;
        dataBean.setProperties(request);
        filenameBase    = dataBean.getDownloadFilenameBase();
        resultSet       = dataBean.executeQuery();
        if (resultSet == null)
        {
          throw new NullPointerException();
        }
      }
      catch (Exception e)
      {
        if (dataSource == DS_REPORT_DATA_BEAN)
        {
          errorMsg = "Failed to load data in " + request.getParameter("reportDataBean");
        }
      }
      
      encoding     = HttpHelper.getInt(request,"encoding",mesConstants.FF_CSV);
      timeoutExempt= HttpHelper.getBoolean(request,"te",false);
      errorMsg     = null;     // reset the error message to none
      beanName     = HttpHelper.getString(request,"reportSQLJBean",null);
      
      // dsReportType will be sent by the Reporting API.  When receiving
      // a request via an API call, the user is logged in for a single
      // request.  decode the numeric dsReportType to the bean name
      if ( (dsReportId = HttpHelper.getInt(request,"dsReportId",-1)) > 0 )
      {
        // if this server requires SSL to be used and the request
        // was not over SSL, reject the request.
        if( request.getServerPort() != 443 && 
           (HttpHelper.isProdServer(request) || HttpHelper.doesUseSSL(request)) )
        {
          errorMsg = "Invalid request protocol.  HTTPS is required by the server.";
        }
        else  // ssl present or not required
        {
          // validate the user credentials
          userBean = new UserBean();
          if ( !userBean.validate( HttpHelper.getString(request,"userId",""),
                                   HttpHelper.getString(request,"userPass",""),
                                   request ) )
          {
            // invalid user/password
            errorMsg = "Invalid user/password";
          }
          else if ( !userBean.hasRight(HttpHelper.getString(request,"dsUserRights","999999999")) )
          {
            errorMsg = "Insufficient rights";
          }
          
          // log details of this access even if it was denied
          userBean.logAccessDetails(request);
        }
        // setting the data source will preserve
        // any error messages that have been set
        dataSource = DS_REPORT_API;
        
        // error in user validation, invalidate bean name
        if ( errorMsg != null )
        {
          beanName = null;
        }        
      }
      else  // user expected to be logged in via web reporting system
      {
        userBean = (UserBean)request.getSession(false).getAttribute("UserLogin");
      }
      
      // check to see if this is a sqlj report bean
      try
      {
        reportBean   = (ReportSQLJBean)Class.forName(beanName).newInstance();
        dataSource   = DS_REPORT_SQLJ_BEAN;

        // connect to the database
        reportBean.connect(timeoutExempt);

        // store the report user bean in the bean before calling
        // initialize.  This allows the data bean to load user
        // specific default parameters from the database during
        // the call to initialize or setProperties.
        reportBean.setReportUserBean( userBean );

        // initialize the ReportSQLJBean object
        reportBean.initialize();

        // load the params from the request
        reportBean.setProperties(request);
        
        ServletOutputStream out = null;
        if( 
          reportBean.isEncodingSupported( encoding ) && 
          ! (reportBean instanceof com.mes.reports.CustomQueryDataBean) )
          // CustomQueryDataBean won't have the column names yet and so
          // creating the header is useless at this point in the process
        {
          out = reportBean.encodeDataPrep( encoding, response );
        }
        
        // load the data from the database
        reportBean.loadData();
        
        if ( reportBean.isEncodingSupported( encoding ) )
        {
          reportBean.encodeData( encoding, response, out );
        }
        else
        {
          errorMsg = "File format not supported.";
        }
      }
      catch( Exception e )
      {
        if ( dataSource == DS_REPORT_SQLJ_BEAN )
        {
          errorMsg = "Failed to load data in " + request.getParameter("reportSQLJBean");
        }
      }
      
      // check to see if this is a queue report bean
      try
      {
        beanName    = request.getParameter("queueBean");
        queueBean   = (QueueBase)(Class.forName(beanName).newInstance());
        dataSource  = DS_REPORT_QUEUE_BEAN;
        
        queueBean.setType(HttpHelper.getInt(request,"type",-1));    // set type first to allow beans to create fields if necessary
        queueBean.setFields(request);   // set fields with data from request
        
        queueBean.loadData(userBean);
        
        if ( queueBean.isEncodingSupported( encoding ) )
        {
          queueBean.encodeData( encoding, response );
        }
        else
        {
          errorMsg = "File format not supported.";
        }
      }
      catch( Exception e )
      {
        if ( dataSource == DS_REPORT_QUEUE_BEAN )
        {
          errorMsg = "Failed to load data in " + request.getParameter("queueBean");
        }
      }
      
      // check for generic SQLJConnectionBase bean
      try
      {
        beanName    = request.getParameter("dataBean");
        db          = (SQLJConnectionBase)(Class.forName(beanName).newInstance());
        dataSource  = DS_REPORT_DATABASE_BEAN;
        
        db.setProperties(request, userBean);
        
        db.loadData(userBean);
        
        if(db.isEncodingSupported(encoding))
        {
          db.encodeData(encoding, response);
        }
        else
        {
          errorMsg = "File format not supported.";
        }
      }
      catch(Exception e)
      {
        if(dataSource == DS_REPORT_DATABASE_BEAN)
        {
          errorMsg = "Failed to load data in " + request.getParameter("dataBean");
        }
      }
      
      // send out the result set at comma-delimitted text file
      if (resultSet != null)
      {
        String cd = "attachment; filename=" + filenameBase + ".csv";
        response.setContentType("application/csv");
        
        /* commented out temporarily because weblogic 6.1 sp3 chokes on this call
        response.setHeader("Content-Disposition",cd);
        */
        
        ResultSetEncoder encoder = new ResultSetEncoder(response.getOutputStream());
        encoder.encodeCSV(resultSet);
      }
      // failed to get result set, send an error message
      // NOTE:  ReportSQLJBean objects encode and transmit
      //        download files via the encodeData(..) method
      //        rather than returning a result set to be
      //        encoded.  This allows the abstraction necessary
      //        to allow the download servlet to be used to 
      //        download all types of data (rather than just 
      //        java.sql.ResultSet based data).
      else if ( errorMsg != null )
      {
        sendErrorMessage(response,errorMsg);
      }
      
      //System.out.println("Elapsed MS: " + (System.currentTimeMillis() - startt));
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
    finally
    {
      /**
      if (sqlBean != null)
      {
        sqlBean.close();
        sqlBean = null;
      }
      */
      try{ reportBean.cleanUp(); } catch( Exception e ) { }
    }
  }
     
  protected long orgIdQuery( HttpServletRequest request, long defaultOrgId )
  {
    long                orgId         = defaultOrgId;
    String              paramString   = null;
                                 
    paramString = request.getParameter("com.mes.OrgId");
    if ( paramString != null && ! paramString.equals("") )
    {
      try
      {
        orgId = Long.parseLong( paramString );
      }
      catch( NumberFormatException e )
      {
      }
    }
    return( orgId );
  }
}
