/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/BCBLoginServlet.java $

  Description:
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/10/04 2:05p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.user.ClientLoginCookie;
import com.mes.user.UserBean;

/*
** GenericLoginServlet routes users to the necessary Login page
*/
public class GenericLogoutServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      UserBean UserLogin = getUserLogin();
      
      // invalidate the user bean
      UserLogin.invalidate();
    
      // remove session beans
      Enumeration sessionBeans = session.getAttributeNames();
      while(sessionBeans.hasMoreElements())
      {
        String sessionBean = (String)sessionBeans.nextElement();
        session.removeAttribute(sessionBean);
      }
      
      // determine url for login page
      int client = HttpHelper.getInt(request, "client", ClientLoginCookie.CLIENT_DEFAULT);
      
      ClientLoginCookie clc = new ClientLoginCookie(client);
      
      StringBuffer            forwardPage = new StringBuffer(HttpHelper.getServerURL(request));
      
      forwardPage.append(clc.getLoginPage());
      
      // send the browser to the new location
      response.sendRedirect(forwardPage.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
