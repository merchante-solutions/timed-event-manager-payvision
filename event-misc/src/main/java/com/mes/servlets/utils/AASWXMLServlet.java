/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AASWXMLServlet.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 1/27/03 5:42p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jdom.output.XMLOutputter;
import com.mes.constants.MesUsers;
import com.mes.reports.AASWXMLDownload;
import com.mes.support.HttpHelper;
import com.mes.user.Permissions;
import com.mes.user.UserBean;

public class AASWXMLServlet extends MesServletBase
{
  UserBean    ub = null;
  
  
  private boolean userAuthenticate(HttpServletRequest request)
  {
    boolean result = false;
    try
    {
      String user     = HttpHelper.getString(request, "user", "");
      String password = HttpHelper.getString(request, "password", "");
      
      // create a user bean for this user
      ub = new UserBean();
      
      // try to authenticate user
      if(ub.validate(user, password, request))
      {
        // check to see if user has rights to see this data
        Permissions   perms = new Permissions();
        perms.allowRight(MesUsers.RIGHT_AASW_DATA);
        
        if(perms.hasPermissions(ub, request))
        {
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AASWXMLServlet::userAuthenticate()", e.toString());
    }
    
    return result;
  }
  
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    boolean success = false;
    try
    {
      // instantiate worker bean
      AASWXMLDownload   worker = new AASWXMLDownload();
      
      // validate user
      if(userAuthenticate(request))
      {
        // get date range
        String  fromDate  = HttpHelper.getString(request, "fromDate", "");
        String  toDate    = HttpHelper.getString(request, "toDate", "");
        String  franchise = HttpHelper.getString(request, "franchise", "");
        
        if(fromDate.equals("") || toDate.equals("") || franchise.equals(""))
        {
          worker.setErrorString("Invalid parameter(s)");
        }
        else
        {        
          success = worker.buildXML(fromDate, toDate, franchise);
        }
      }
      else
      {
        worker.setErrorString("User Authentication Failed");
      }
      
      if(!success)
      {
        // build error xml
        worker.buildErrorXML();
      }

      PrintWriter xmlout = new PrintWriter(response.getOutputStream(), true);
      
      XMLOutputter fmt = new XMLOutputter();
      
      fmt.output(worker.doc, xmlout);
      
      // make sure everything gets outputted
      xmlout.flush();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("AASWXMLServlet::doServlet()", e.toString());
    }
  }
}
