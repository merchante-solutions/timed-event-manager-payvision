/*************************************************************************

  FILE: $URL: /Java/servlets/com/mes/utils/HideAppServlet.java $

  Description:
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.MesUsers;
import com.mes.ops.HideApp;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

public class HideAppServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      if(userLogin.hasRight(MesUsers.RIGHT_HIDE_APP))
      {
        HideApp.hide(HttpHelper.getInt(request, "appSeqNum"), userLogin.getHierarchyNode());
      }
      
      getBackToWhereYouOnceBelonged(request, response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry("com.mes.utils.HideAppServlet::doServlet()", e.toString());
    }
  }
}
