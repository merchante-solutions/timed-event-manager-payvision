/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/SalesMenuServlet.java $

  Description:
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/10/03 11:48a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.tools.SalesMenuFinder;
import com.mes.user.UserBean;

public class SalesMenuServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      UserBean    userLogin = getUserLogin();
    
      // determine sales menu url from hierarchy node
      String salesURL = SalesMenuFinder.getSalesURL(userLogin.getHierarchyNode());
    
      // send browser to new url
      response.sendRedirect(salesURL);
      //forward(salesURL, request, response);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.utils.SalesMenuServlet::doServlet()", e.toString());
    }
  }
}
