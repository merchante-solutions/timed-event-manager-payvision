package com.mes.servlets.utils;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
// log4j
import org.apache.log4j.Category;
import com.mes.acr.ACR;
import com.mes.acr.ACRDAO;
import com.mes.acr.ACRDataBeanBase;
import com.mes.acr.ACRDataBeanBuilder;
import com.mes.acr.ACRDefinition;
import com.mes.acr.ACRPump;
import com.mes.acr.ACRQueueOps;
import com.mes.acr.ACRSession;
import com.mes.acr.ACRUserTools;
import com.mes.acr.MerchantInfo;
import com.mes.acr.RequestorBuilder;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.forms.Field;
import com.mes.queues.ChangeRequestQueue;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueSessionBean;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.support.HttpTextMessageManager;
import com.mes.support.StringUtilities;
import com.mes.user.UserBean;


/**
 * ACRController servlet class.
 *
 * - Performs ACR commands.
 * - Forwards to appropriate page.
 *
 * command
 * ------
 *
 * verr       View error page.  Issue this command when original command fails.
 *
 * vdefs      (Default) View "available" ACR definitions.
 *            --------------
 *            Expected input
 *            --------------
 *            mn              parent ID: Merchant Number
 *
 * racr       Re-identify the ACRs to submit.  (Selected from the available ACR definitions.)
 *            --------------
 *            Expected input
 *            --------------
 *            name:   "{ACR short name}"
 *            value:  ["y"]
 *
 * vacr       View ACR.
 *
 * facr       Fill ACR.  Set the current ACR with data retreived from http request input.
 *            --------------
 *            Expected input
 *            --------------
 *            { ACR form data specific to the ACR type (ACRDefinition). }
 *
 * vacrs      View ACR Summary. (Pre-Submit)
 *
 * sacr       Submit ACR(s).  Same command irregardless of which page user is on.
 *            The current state of the session dictates where the user is directed.
 *
 * pacr       Process ACR(s).  This happens when the final submit button is pressed on the summary page.
 *
 * dqo        Do Queue Op.  Perform a specified queue op for a given ACR.
 *            --------------
 *            Expected input
 *            --------------
 *            qo              Queue op (int).  (FORMAT: "{src q}:{dst q}"
 *            on              Op Notes
 *            { Current ACR must already be set. }
 *
 * acrd       ACR Detail.  Read Only ("Status") view of an existing ACR.
 *            --------------
 *            Expected input
 *            --------------
 *            acrid [id]      Current ACR ID
 *            [src]           Source
 *             - "ql"           Queue Listing
 *
 * uacr       Update ACR.    Updates the status ACR items with field data retreived from http request input.
 *
 * vacc       View Account.  (Relinquish the queue lock on the acr if present).
 *            --------------
 *            Expected input
 *            --------------
 *            [editable]      Status (true) or Submit (false) mode?  (Defaults to false)
 *
 * vah        View Action History
 *
 * vapp       View [associated] Application
 *            --------------
 *            Expected input
 *            --------------
 *            [editable]      Status (true) or Submit (false) mode?  (Defaults to false)
 *
 * vsrc       View Source Userid
 *            --------------
 *            Expected input
 *            --------------
 *            [editable]      Status (true) or Submit (false) mode?  (Defaults to false)
 *            ln              login name (mesdb.USERS.LOGIN_NAME)
 *
 * srch       View ACR Search page
 *            --------------
 *            Expected input
 *            --------------
 */
public final class ACRController extends MesServletBase
{
  // create class log category
  static Category log = Category.getInstance(ACRController.class.getName());

  // constants
  public static final String              RELPATH_ACRPRESENTATION   = "/jsp/acr/";
  public static final String              RELPATH_QUEUELISTING      = "/jsp/queues/queue.jsp?type=";
  public static final String              RELPATH_QUEUEROUTER       = "/srv/QueueRouter?itemType=10&";
  public static final String              RELPATH_SESSIONEXPIRED    = "/jsp/secure/sLogin.jsp?type=";
  public static final String              RELPATH_ACCOUNT           = "/jsp/maintenance/view_account.jsp?merchant=";
  public static final String              RELPATH_APPLICATION       = "/jsp/setup/merchinfo4.jsp?primaryKey=";
  public static final String              RELURL_INSUFACCESS        = "/jsp/secure/InsufficientAccess.jsp";
  public static final String              RELPATH_USERPROFILE       = "/jsp/credit/contact_info.jsp?userName=";
  public static final String              RELPATH_ACRSEARCHPAGE     = "/jsp/maintenance/lookup_change.jsp?action=1";


  // data members
  private HttpSession         session;
  private HttpServletRequest  request;
  private HttpServletResponse response;
  private ACRSession          acrs;
  private ACRDataBeanBase     bean;
  private ACRQueueOps         queueOps;
  private HttpTextMessageManager    tmm;
  private StringBuffer        forwardPage;
  private UserBean            loginUser;
  private StringBuffer        queryString;
  private QueueSessionBean    qsb;
  private ACRPump             acrPump;

  // class functions
  // (none)

  // object functions

  // construction
  public ACRController()
  {
    super();

    session=null;
    request=null;
    response=null;
    acrs=null;
    bean=null;
    queueOps=new ACRQueueOps();
    forwardPage = new StringBuffer(2048);  // presume 2k max len URL
    loginUser=null;
    queryString = new StringBuffer(512);
    qsb = null;
    acrPump = new ACRPump();
  }
    
  
  // methods

  /**
   * doCommand()
   *
   * Executes the given command.
   * Sets the page to forward to.
   * Recursive.
   */
  private void doCommand(String command)
    throws Exception
  {
    if(command==null || command.length()<1)
      throw new ServletException("Unable to perform command: Empty command specified.");
    if(acrs==null)
      throw new ServletException("Unable to perform command: Null ACRSession reference.");

    log.debug("doCommand("+command+") - START");

    String param=null;

    // verr: view error
    if(command.equals("verr")) {

      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_error.jsp");

    // vdefs: (default) View available ACR definitions.
    } else if(command.equals("vdefs")) {

      // get parent ID (MerchantNum) if specified
      param = request.getParameter("mn");
      if(param!=null && param.length()>0) {
        log.debug("Flag mn ('"+param+"') acquired.");
        acrs.setMerchantInfo(param);
      } else if(!acrs.isMerchantInfoLoaded() && acrs.getStatusBean()!=null)
        acrs.setMerchantInfo(acrs.getStatusBean().getMerchantNumber());

      if(!acrs.isMerchantInfoLoaded()) {
        log.error("View ACR Submission start screen failed: Unable to load merchant information.");
        tmm.err("View ACR Submission start screen failed: Unable to load merchant information.");
        doCommand("verr");
        return;
      }

      // add requestor info to acr session
      if(acrs.getRequestor()==null)
        acrs.setRequestor(RequestorBuilder.build(loginUser));

      // filter the acr definitions
      acrs.filterDefinitions();

      // bind data bean to target page w/ request scope
      if(bean==null)
        bean=ACRDataBeanBuilder.buildMain(acrs.getMerchantInfo(),acrs.getACRDefs(),acrs.getSubmitBeans(),loginUser);

      if(bean==null) {
        doCommand("verr");
        return;
      }

      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_main.jsp");

    // racr: Re-identify the ACRs to submit.
    } else if(command.equals("racr")) {

      // bind data bean to target page w/ request scope
      bean=ACRDataBeanBuilder.buildMain(acrs.getMerchantInfo(),acrs.getACRDefs(),acrs.getSubmitBeans(),loginUser);

      bean.setFields(request);
      log.debug("racr - bean.setFields() set.");
    
      ACRDefinition acrd=null;
      ACR acr=null;
      Field fld=null;

      // re-set the the desired ACRs in the ACRSession
      // (map the FieldBean data to ACRSession)
      //for(Enumeration e=acrs.getACRDefsEnumerator();e.hasMoreElements();) {
      for (Iterator i = acrs.getACRDefs().iterator(); i.hasNext();)
      {
        //acrd=(ACRDefinition)e.nextElement();
        acrd = (ACRDefinition)i.next();

        if(!acrd.isOn())
          continue;

        acr=acrs.getSubmitAcr(acrd.getShortName());
        fld=bean.getField(acrd.getShortName());

        if(fld==null) {
          log.error("Encountered NULL Field '"+acrd.getShortName()+"'.  Continuing.");
          continue;
        }

        if(fld.isBlank()) {
          log.debug("fld '"+fld.getName()+"' is UNchecked.");
          // un-selected
          if(acr!=null) {
            acr.setStatus(ACR.ACRSTATUS_ABORTED);
            log.debug("Aborting ACR '"+acrd.getShortName()+"'.");
          }
        } else {
          log.debug("fld '"+fld.getName()+"' is CHecked.");
          // selected
          if(acr==null) {
            log.debug("Creating Bean for def: '"+acrd.getShortName()+"'.");
            acr=acrs.getDAOFactory().getACRDAO().createACR();
            acr.setDefinition(acrd);
            if(!acrs.addSubmitBean(acr)) {
              log.error("Add submit bean failed.  Reverting to ACR Main page...");
              tmm.err("Currently, Change Request '"+acrd.getName()+"' is unavailable.");
              doCommand("vdefs");
              return;
            }
          }
          acr.setStatus(ACR.ACRSTATUS_INVALID);
        }

      }

      log.debug("About to validate Main form bean...");

      if(bean.isValid()) {
        // defer
        log.debug("racr - Now deferring command processing to 'sacr' command...");
        bean=null;  // IMPT: clear to ensure proper bean gets instantiated
        doCommand("sacr");
        return;
      } else {
        doCommand("vdefs");
        return;
      }

    // vacr: View ACR.
    } else if(command.equals("vacr")) {

      bean = acrs.getCurrentBean();

      // reqire a current ACR be set
      if(bean==null) {
        tmm.err("Unable to view ACR: No current ACR bean specified.");
        log.error("Unable to view ACR: No current ACR bean specified.");
        doCommand("vdefs");
        return;
      }

      // check user permissions on this acr type
      if (!bean.getACRDefinition()
            .hasPermissions(loginUser,ACRDefinition.FUNC_VIEW))
      {
        forwardPage.append(RELURL_INSUFACCESS);
        queryString.replace(0,queryString.length(),"");
          // kill the query string since redirecting to another servlet
        log.warn("User "+loginUser.getUserName()+" has insufficient rights for ACR type '"+bean.getACRDefinition().getName()+"'. Operation '"+command+"' aborted.");
        bean=null;
        return;
      }

      ACR acr = bean.getACR();

      // set requestor info w/ default info if present
      if(acr.getACRValue("Requestor's Name").length()<1 && acrs.getDfltRqstrName().length()>1)
        acr.setACRItem("Requestor's Name",acrs.getDfltRqstrName());
      if(acr.getACRValue("Requestor's Title").length()<1 && acrs.getDfltRqstrTitle().length()>1)
        acr.setACRItem("Requestor's Title",acrs.getDfltRqstrTitle());
      if(acr.getACRValue("Requestor's Phone #").length()<1 && acrs.getDfltRqstrPhone().length()>1)
        acr.setACRItem("Requestor's Phone #",acrs.getDfltRqstrPhone());

      // map the current ACR data to the detail data bean
      bean.doPush();

      log.debug("vacr - Forwarding to acr_detail.jsp.");
      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_detail.jsp");

    // facr: Fill ACR.
    } else if(command.equals("facr")) {

      bean = acrs.getCurrentBean();
      // reqire current ACR be set
      if(bean==null) {
        tmm.err("Unable to fill ACR: No current Bean specified.");
        log.error("Unable to fill ACR: No current Bean specified.");
        doCommand("vdefs");
        return;
      } else {

        // map back the form data to the current ACR
        bean.setFields(request);
        bean.doPull();

        ACR acr = bean.getACR();
        // set status according to the FieldBean.isValid()
        acr.setStatus(bean.isValid()? ACR.ACRSTATUS_VALID:ACR.ACRSTATUS_INVALID);
        log.debug("ACR '"+acr.getDefinition().getName()+"' "+((acr.getStatus()==ACR.ACRSTATUS_VALID)? "Valid":"INvalid"));

        // update the default requestor info
        acrs.setDfltRqstrName(acr.getACRValue("Requestor's Name"));
        acrs.setDfltRqstrTitle(acr.getACRValue("Requestor's Title"));
        acrs.setDfltRqstrPhone(acr.getACRValue("Requestor's Phone #"));

        //testing of field validation

        for(Iterator i = bean.getBaseGroup().iterator(); i.hasNext();)
        {
          Field f = (Field)i.next();
          if(f.isValid())
          {
            log.debug(f.getName()+" is valid");
          }
          else
          {
            log.debug(f.getName()+" is NOT valid");
          }
        }

        // iterate to next ACR if current one is valid
        if(acr.isValid())
          doCommand("sacr");
        else
          doCommand("vacr");  // current acr invalid so re-view
        return;
      }

    // uacr: Update ACR
    } else if(command.equals("uacr")) {

      // reqire current ACR be set
      if((bean=acrs.getStatusBean())==null) {
        tmm.err("Unable to update status ACR: No status Bean specified.");
        log.error("Unable to update status ACR: No status Bean specified.");
        doCommand("vdefs");
        return;
      }

      // map back the form data to the current ACR
      bean.setFields(request);
      bean.doPull();

      ACR acr = bean.getACR();

      // validate the ACR
      if(!bean.isValid()) {
        doCommand("acrd");
        return;
      }

      final ACRDAO acrDAO = acrs.getDAOFactory().getACRDAO();

      // re-persist acr
      acr.removeEmptyValuedItems();
      if(!acrDAO.persistACR(acr)) {
        acr.setStatus(ACR.ACRSTATUS_INVALID);
        tmm.err("Unable to process ACR '"+acr.getID()+"': Attempt to persist this ACR failed.");
        log.error("Unable to process ACR '"+acr.getID()+"': Attempt to persist this ACR failed.");
      } else {
        tmm.msg("ACR '"+acr.getID()+"' updated.");
        log.info("ACR '"+acr.getID()+"' updated.");
      }

      doCommand("acrd");

    // vacrs: View ACR Summary
    } else if(command.equals("vacrs")) {

      if(!acrs.isPostSubmit())
        bean=ACRDataBeanBuilder.buildPreSubmitSummary(acrs.getMerchantInfo(),acrs.getSubmitBeans());

      if(bean==null) {
        tmm.err("Unable to build ACR "+(acrs.isPostSubmit()? "POST":"PRE")+" summary page bean.");
        log.error("Unable to build ACR "+(acrs.isPostSubmit()? "POST":"PRE")+" summary page bean.");
        doCommand("vdefs");
        return;
      }

      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_summary.jsp");

    // sacr: Submit ACR(s).
    } else if(command.equals("sacr")) {

      // require all ACR(s) be in valid state.
      // target page: acr summary

      ACR acr;

      // forward to first found invalid acr detail else goto summary page
      for(Enumeration e=acrs.getSubmitBeansEnumerator();e.hasMoreElements();) {
        bean=(ACRDataBeanBase)e.nextElement();
        acr=bean.getACR();

        if(!acr.isValid() && acr.getStatus()!=ACR.ACRSTATUS_ABORTED) {
          acrs.setCurrentBean(bean);
          bean=null;
          // defer command
          doCommand("vacr");
          return;
        }
      }

      // all are valid so goto pre submission summary view
      bean=null;  // IMPT: to ensure summary bean is instantiated
      doCommand("vacrs");
      return;

    // pacr: Process ACR(s) - final submission
    } else if(command.equals("pacr")) {

      ACR acr;
      ACRDataBeanBase beantmp;
      final ACRDAO acrDAO = acrs.getDAOFactory().getACRDAO();

      // persist and unload the ACRs
      for(Enumeration e=acrs.getSubmitBeansEnumerator();e.hasMoreElements();) {
        beantmp=(ACRDataBeanBase)e.nextElement();
        acr=beantmp.getACR();

        if(acr.getStatus()==ACR.ACRSTATUS_ABORTED)
          continue;

        // save the ACR
        acr.removeEmptyValuedItems();
        if(!acrDAO.persistACR(acr)) {
          acr.setStatus(ACR.ACRSTATUS_INVALID);
          tmm.err("Unable to process ACR '"+acr.getDefinition().getName()+"' ("+acr.getID()+"): Attempt to persist this ACR failed.");
          log.error("Unable to process ACR '"+acr.getID()+"': Attempt to persist this ACR failed.");
          continue;
        } else
          log.info("ACR '"+acr.getID()+"' initiated for processing.");

        // add acr to appropriate startpoint queue(s)
        queueOps.reset(beantmp,loginUser);
        if(!queueOps.queueInitiate(null)) {
          acr.setStatus(ACR.ACRSTATUS_INVALID);
          tmm.err("Unable to initiate ACR '"+acr.getID()+"' into the startpoint queue(s).");
          log.error("Unable to initiate ACR '"+acr.getID()+"' into the startpoint queue(s).");
          continue; // don't save this ACR
        } else
          log.info("ACR '"+acr.getID()+"' inserted into the startpoint queue.");
        acrDAO.persistACR(acr); // to update the acr status based on the above queue op

        // pump any ensuing actions out to the appropriate handler
        log.debug("pumping...");
        ACRPump.AcrOp acrOp = new ACRPump.AcrOp(beantmp);
        acrPump.pump(acrOp);

        log.debug("pumped.  Getting results...");
        acrPump.transferResultsToMsgMngr(tmm);

      }

      // IMPT: build post submit summary bean (before unloading ACRs for valid summary data!)
      if((bean=ACRDataBeanBuilder.buildPostSubmitSummary(acrs.getMerchantInfo(),acrs.getSubmitBeans()))==null) {
        tmm.err("Unable to build POST ACR summary page bean.");
        log.error("Unable to build POST ACR summary page bean.");
        doCommand("vdefs");
        return;
      }

      // unload the submit info
      acrs.unloadSubmitBeans();

      // view POST submit summary view
      acrs.setIsPostSubmit(true);
      doCommand("vacrs");
      acrs.setIsPostSubmit(false);
      return;

    // dqo: Do Queue Op
    } else if(command.equals("dqo")) {

      // get the status acr
      if((bean = acrs.getStatusBean())==null) {
        tmm.err("Unable to do queue op: No status bean specified.");
        log.error("Unable to do queue op: No status bean specified.");
        doCommand("vdefs");
        return;
      }

      // get the queue ops performer
      queueOps.reset(bean,loginUser);

      boolean rval=false;
      
      // get the queue op
      String qo = request.getParameter("qo");
      log.debug("qo="+qo);
      if(qo!=null && qo.length()>0) {
        String opNotes = request.getParameter("on");
        log.debug("opNotes="+opNotes);
        int sq=MesQueues.Q_NONE,dq=MesQueues.Q_NONE;
        StringTokenizer st = new StringTokenizer(qo,":");
        if(st.hasMoreElements())
          sq=StringUtilities.stringToInt(st.nextToken(),MesQueues.Q_NONE);
        if(st.hasMoreElements())
          dq=StringUtilities.stringToInt(st.nextToken(),MesQueues.Q_NONE);
        
        int[] rq = QueueTools.getResidentQueues(bean.getACR().getID(),MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);
        int currentResidentQueue = rq.length > 0 ? rq[0] : MesQueues.Q_NONE;
               
        if( sq != currentResidentQueue )
        {
          // get the most updated resident queue
          sq = currentResidentQueue;      
          bean.setResidentQueue(currentResidentQueue);
        }
        
        // do the queue op
        bean.getACR().removeEmptyValuedItems();
        
        // only perform queue op if the ACR is not currently in destination queue  
        if( dq == sq )
        {
          rval = true;
        }
        else
        {
          rval=queueOps.doQueueOp(sq,dq,opNotes);
        }
        
        String statusMsg = queueOps.getOpStatusMsg();
        if(rval) {
          tmm.msg(statusMsg);
          log.info(statusMsg);
        } else {
          tmm.err(statusMsg);
          log.error(statusMsg);
        }

        // re-persist ACR (to update the acr status)
        if(!acrs.getDAOFactory().getACRDAO().persistACR(bean.getACR())) {
          log.error("Persist of ACR '"+bean.getACR().getID()+"' failed after queue op '"+sq+"-->"+dq+"' performed.");
        }

        // pump any ensuing actions out to the appropriate handler
        if(rval) {
          log.debug("pumping...");
          ACRPump.AcrOp acrOp = new ACRPump.AcrOp(bean,sq,dq);
          acrPump.pump(acrOp);

          log.debug("pumped.  Getting results...");
          acrPump.transferResultsToMsgMngr(tmm);
        }

      }

      if(rval && bean.isQueueBeanLoaded()) {
        // go to next item in same queue if exists

        // NOTE: Eliminate this functionality since it has been problematic in Spokane.
        /*
        if(bean.hasNextRow()) {
          bean.incrCrntRow();
          tmm.msg("Iterated to next item in queue.");
          forwardPage.append(RELPATH_QUEUEROUTER);
          forwardPage.append("&type=");
          forwardPage.append(bean.getResidentQueue());
          forwardPage.append("&id=");
          forwardPage.append(bean.getCrntRowID());
          queryString.replace(0,queryString.length(),"");
            // kill the query string since redirecting to another servlet
        } else {
        */

        // else re-route to the originating queue if a queue type was specified
        tmm.clear();  // since moving out of ACR framework domain
        forwardPage.append(RELPATH_QUEUELISTING);
        forwardPage.append(bean.getResidentQueue());
        queryString.replace(0,queryString.length(),"");
          // kill the query string since redirecting to another servlet

        //}
      } else {
        // default: maintain status view
        doCommand("acrd");
      }
      return;

    // acrd: ACR Detail ("Status" view)
    } else if(command.equals("acrd")) {

      // get the ACR ID
      param = request.getParameter("acrid");
      if(param == null)
      {
        param = request.getParameter("id");
      }
      if(param!=null && param.length()>0) {
        log.debug("Flag acrid '"+param+"' acquired.");
        final long acrID = StringUtilities.stringToLong(param,-1L);
        if(acrID>0) {
          bean=acrs.setStatusBean(acrID);
          if(bean==null) {
            tmm.err("Set Status Bean of ACR ID: '"+acrID+"' FAILED.");
            log.error("Set Status Bean of ACR ID: '"+acrID+"' FAILED.");
            doCommand("vdefs");
            return;
          }
        }
      } else
        bean=acrs.getStatusBean();

      if(bean==null) {
        tmm.err("View ACR Detail failed: Unable to obtain reference to ACR data bean.");
        log.error("View ACR Detail failed: Unable to obtain reference to ACR data bean.");
        doCommand("verr");
        return;
      }

      // check user permissions on this acr type
      if (!bean.getACRDefinition()
            .hasPermissions(loginUser,ACRDefinition.FUNC_VIEW))
      {
        forwardPage.append(RELURL_INSUFACCESS);
        queryString.replace(0,queryString.length(),"");
          // kill the query string since redirecting to another servlet
        log.warn("User "+loginUser.getUserName()+" has insufficient rights for ACR type '"+bean.getACRDefinition().getName()+"'. Operation '"+command+"' aborted.");
        bean=null;
        return;
      }

      // get the source of where request came from
      String source = HttpHelper.getString(request,"src","");
      boolean bFromQueueListingPage = (source.equals("ql"));

      // bind the queue data bean to the acr detail bean

      QueueBase queueBean = null;

      if(bFromQueueListingPage && qsb!=null) {
        int type = HttpHelper.getInt(request,"type",0);
        bean.setQueueBean(qsb.getQueueBean(type));
      }

      if(bean.getQueueBean()==null) {
        queueBean = new ChangeRequestQueue();
        queueBean.setType(QueueTools.getCurrentQueueType(bean.getACR().getID(),MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST));
        bean.setQueueBean(queueBean);
      }

      // map the status ACR data to the detail data bean
      bean.doPush();

      // post available acr queue ops to this data bean
      if(loginUser.hasRight(MesUsers.RIGHT_ACR_COMMAND) ||
         loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_COMMAND) ||
        (loginUser.hasRight(MesUsers.RIGHT_ACR_COMMAND_RESTRICT) &&
          ACRUserTools.userAllowedCommand(loginUser, bean)))
      {
        log.debug("post available acr queue ops to this data bean...");
        queueOps.reset(bean,loginUser);
        queueOps.QueueOpsToCommands();
        //bean.setShowActionHistoryLink(true);
      }
      else
      {
        bean.clearCommands();
        //bean.setShowActionHistoryLink(false);
      }

      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_detail.jsp");

    // View Account
    } else if(command.equals("vacc")) {

      String mn=null;

      final boolean editable = HttpHelper.getBoolean(request,"editable",false);

      if(editable) {
        MerchantInfo mi=acrs.getMerchantInfo();
        if(mi!=null)
          mn=mi.getMerchantNum();
      } else {
        ACR sacr = acrs.getStatusAcr();
        if(sacr!=null) {
          mn=sacr.getMerchantNum();
          // release lock on status acr if one exists
          if(sacr!=null)
            QueueTools.releaseLock(sacr.getID(),com.mes.constants.MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);
        }
      }

      if(mn==null || mn.length()<1) {
        tmm.err("View account failed: Unable to obtain merchant number.");
        log.error("View account failed: Unable to obtain merchant number.");
        doCommand(editable? "vdefs":"acrd");
        return;
      }

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_ACCOUNT);
      forwardPage.append(mn);
      queryString.replace(0,queryString.length(),"");

    // View Action History
    } else if(command.equals("vah")) {

      // get the status acr
      ACRDataBeanBase sbean=acrs.getStatusBean();
      ACR sacr = (sbean==null)? null:sbean.getACR();
      if(sacr==null) {
        log.error("Unable to view action history: Could not obtain ref to acr status bean.");
        doCommand("acrd");
        return;
      }

      // get the acr action history bean
      if((bean=ACRDataBeanBuilder.buildActionHistory(sbean))==null) {
        // unable to instantiate acr detail data bean so go back to main page
        tmm.wrn("Currently, the Action History for the request is unavailable.");
        log.warn("Reverting to acr status view: Unable to acquire ActionHistory instance.");
        doCommand("acrd");
        return;
      }

      // release lock on status acr if one exists
      QueueTools.releaseLock(sacr.getID(),com.mes.constants.MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);

      // redirect to acr action history presentation page
      forwardPage.append(RELPATH_ACRPRESENTATION);
      forwardPage.append("acr_action_history.jsp");

    // View Application
    } else if(command.equals("vapp")) {

      String asn=null;

      final boolean editable = HttpHelper.getBoolean(request,"editable",false);

      if(editable) {
        MerchantInfo mi=acrs.getMerchantInfo();
        if(mi!=null)
          asn=Long.toString(mi.getAppSeqNum());
      } else {
        ACR sacr = acrs.getStatusAcr();
        if(sacr!=null) {
          asn=Long.toString(sacr.getAppSeqNum());
          // release lock on status acr if one exists
          if(asn!=null && asn.length()>0)
            QueueTools.releaseLock(sacr.getID(),com.mes.constants.MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);
        }
      }

      if(asn==null || asn.length()<1) {
        tmm.err("View application failed: Unable to obtain application sequence number.");
        log.error("View application failed: Unable to obtain application sequence number.");
        doCommand(editable? "vdefs":"acrd");
        return;
      }

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_APPLICATION);
      forwardPage.append(asn);
      queryString.replace(0,queryString.length(),"");

    // View Source Userid
    } else if(command.equals("vsrc")) {

      String uname=null;

      final String ln = HttpHelper.getString(request,"ln");
      final boolean editable = HttpHelper.getBoolean(request,"editable",false);

      ACR sacr = acrs.getStatusAcr();

      // release lock on status acr if one exists
      if(sacr!=null)
        QueueTools.releaseLock(sacr.getID(),com.mes.constants.MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);

      if(ln==null || ln.length()<1) {
        tmm.err("View user profile failed: User login name not specified.");
        log.error("View user profile failed: User login name not specified.");
        doCommand(editable? "vdefs":"acrd");
        return;
      }

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_USERPROFILE);
      forwardPage.append(ln);
      queryString.replace(0,queryString.length(),"");

    // View ACR Search page
    } else if(command.equals("srch")) {

      String acn=null;

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_ACRSEARCHPAGE);
      queryString.replace(0,queryString.length(),"");

    // Unhandled request so defer to the concrete bean sub-class...
    } else {

      // instantiate the current data bean
      bean = acrs.getCurrentBean();
      if(bean==null) {
        tmm.err("Unable to defer command: No current ACR specified or unable to obtain ACR data bean reference.");
        log.error("Unable to defer command: No current ACR specified or unable to obtain ACR data bean reference.");
        doCommand("vdefs");
        return;
      }
      bean.setFields(request);

      log.info("Deferring command: '"+command+"' to the current ACR data bean...");
      if(!bean.doCommand(command))
        tmm.err("Command execution unsuccessful.");
      doCommand("vacr");
      return;
    }

  }

  private boolean userPermissionsCheck(String command)
  {
    boolean rval = true;  // default to allow op

    // acr system access?
    if(!loginUser.hasRight(MesUsers.RIGHT_ACR_SYSTEM))
    {
      rval=false;
    }
    // SUBMIT related
    else if(command.equals("vacr")  ||
            command.equals("vdefs") ||
            command.equals("racr")  ||
            command.equals("facr")  ||
            command.equals("vacrs") ||
            command.equals("sacr")  ||
            command.equals("pacr"))
    {
      rval=(loginUser.hasRight(MesUsers.RIGHT_ACR_CREATE)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_CREATE));
    }
    // STATUS related
    else if(command.equals("vah") ||
            command.equals("acrd"))
    {
      rval=(loginUser.hasRight(MesUsers.RIGHT_ACR_STATUS_VIEW)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_STATUS_VIEW));
    }
    // QUEUE OP related
    else if(command.equals("dqo"))
    {
      rval=(loginUser.hasRight(MesUsers.RIGHT_ACR_COMMAND)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_COMMAND_RESTRICT)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_COMMAND));
    }
    // links to the outside related
    else if(command.equals("vacc") ||
            command.equals("vapp") ||
            command.equals("vsrc"))
    {
      rval=(loginUser.hasRight(MesUsers.RIGHT_ACR_CREATE) ||
            loginUser.hasRight(MesUsers.RIGHT_ACR_STATUS_VIEW)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_CREATE)||
            loginUser.hasRight(MesUsers.RIGHT_ACR_BBT_STATUS_VIEW));
    }

    if(!rval) {
      forwardPage.append(RELURL_INSUFACCESS);
      queryString.replace(0,queryString.length(),"");
        // kill the query string since redirecting to another servlet
      log.warn("User "+loginUser.getUserName()+" has insufficient rights for operation command '"+command+"'. Operation aborted.");
    }

    return rval;
  }

  private void processRequest(HttpServletRequest request,HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    try {

      log.debug("ACR processRequest() - START");
      StringBuffer nameBuff         = new StringBuffer(); 
      String       acrSessionName   = null;
      String       requestId        = null;
      String       method           = null;
      
      // reset
      this.bean=null;
      this.request=request;
      this.response=response;
      forwardPage.replace(0,forwardPage.length(),"");
      log.debug("URL = " + HttpHelper.getServerURL(request));
      forwardPage.append(HttpHelper.getServerURL(request));
      queryString.replace(0,queryString.length(),"");

      // get ref to the current session
      if((session = request.getSession(false))==null) {        
        log.debug("Initializing HTTP session...");
        if((session = request.getSession(true))==null)
          throw new ServletException("Unable to obtain a reference to the HTTP session.");
        
        // redirect to logon...
        log.info("Session new or expired - redirecting to login page...");
        response.sendRedirect(RELPATH_SESSIONEXPIRED);
        return;
      }

      // get ref. to the UserLogin bean for user related session info
      if((loginUser=(UserBean)session.getAttribute("UserLogin"))==null) {
        log.error("Unable to obtain reference to the login user.");
        throw new ServletException("Unable to obtain reference to the login user.");
      }
      
      method = request.getParameter("method");
      acrSessionName = HttpHelper.getString(request, "sname", null);

      
      if ( (acrSessionName == null) || acrSessionName.equals("null") )
      {
        nameBuff.append("ACRSession");

        if ( method.equals("vdefs") || method.equals("acrd") )
        {
          requestId = String.valueOf(session.getAttribute("RequestId"));
          if ( requestId.equals("null") )
          {
            requestId = "0";
          }
          else
          {
            int       nextId     = 0;
            try 
            {
              nextId = Integer.parseInt(requestId.trim());
              nextId++;
            } 
            catch (NumberFormatException nfe) 
            {
              log.error("ACRController.processRequest() EXCEPTION: '"+nfe.getMessage()+"'.");
            }
            requestId = String.valueOf( nextId );
          }

          session.setAttribute("RequestId", requestId);
          nameBuff.append( requestId );
        }
        acrSessionName = nameBuff.toString();
      }

      if ( (acrs=(ACRSession)session.getAttribute(acrSessionName)) == null )
      {
        acrs=new ACRSession();
        if(acrs==null)
        {
          throw new ServletException("Unable to instantiate ACRSession object.");
        }
        session.setAttribute(acrSessionName,acrs);
      }

      // assign the user to the session object
      acrs.setLoginUser(loginUser);

      // make sure acr definitions are loaded in session
      if(!acrs.loadDefinitions())
      {
        throw new ServletException("Session ACR definitions failed to load");
      }

      // at this point acrs is presumed to be non-null and valid
      // get the ref. to the session-scope TextMessageManager
      if((tmm=(HttpTextMessageManager)session.getAttribute("HttpTextMessageManager"))==null) {
        tmm = new HttpTextMessageManager();
        session.setAttribute("HttpTextMessageManager",tmm);
        log.debug("HttpTextMessageManager session attribute set.");
      }
      tmm.setHttpRequest(request);

      // [re-]set ref to queue session bean
      qsb=(QueueSessionBean)session.getAttribute("queueSession");
      log.debug("QueueSessionBean session attribute gotten as "+(qsb==null? "NULL":"NON-NULL"));

      // get command
      String command=request.getParameter("method");

      // assign default command?
      if(command==null) {
        log.debug("Setting default ACR command to 'vdefs'.");
        command="vdefs";
      } else {
        // translate command?
        if(command.equals("Start Change Requests")) {
          command="racr";
        } else if(command.equals("Submit Request")) {
          command="facr";
        } else if(command.equals("Submit & Process Requests")) {
          command="pacr";
        } else if(command.equals("Do Action")) {
          command="dqo";
        } else if(command.equals("Update Request")) {
          command="uacr";
        }
      }

      // retain query string
      String qs = request.getQueryString();
      if(qs!=null && qs.length()>0 && !qs.equals("null"))
        queryString.append(request.getQueryString());
        
      if ( (command.equals("vdefs") || command.equals("acrd")) && (queryString.toString().indexOf("sname") == -1))
      { 
        queryString.append("&sname=");        
        queryString.append(acrSessionName);
      }
      // user permissions check
      if(userPermissionsCheck(command)) {

        // *** do command ***
        try {
          doCommand(command);
        }
        catch(Exception e) {
          log.error("Command Exception: "+e.getMessage()+" (Command: "+command+").");
          tmm.err("A command exception (command: "+command+") occurred: "+e.getMessage());
          doCommand("verr");
        }

      }

      // set the bean for the target page
      if(bean!=null) {
        bean.setTextMessageManager(tmm);
        bean.prepTextMessages();
        session.setAttribute("acrbean",bean);
        log.debug("Session ACR bean '"+bean.getClass().getName()+"' set.");
      }

      log.debug("Setting No cache in the header...");
      response.setHeader("Cache-Control","no-store");
      response.setHeader("Pragma","no-cache");
      response.setDateHeader("Expires",0);

      // send the browser to the new location
      log.debug("send the browser to the new location...");
      // IMPT: append the query string to tell the browser to force a refresh of the page!
      if(queryString.length()>0) {
        forwardPage.append("?");
        forwardPage.append(queryString);
      }

      log.debug("Redirecting to page '"+forwardPage.toString()+"'...");
      response.sendRedirect(forwardPage.toString());

    }
    catch(Exception e) {
      log.error("ACRController.processRequest() EXCEPTION: '"+e.getMessage()+"'.");
      e.printStackTrace();
    }
  }
  protected void doServlet( HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      processRequest(request,response);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }

  }

  public String getSignature()
  {
    return "Merchant e-Solutions - Account Change Request - Controller";
  }
  

} // ACRController



