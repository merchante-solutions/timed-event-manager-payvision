/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AuthNetRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-11 11:22:40 -0800 (Mon, 11 Feb 2008) $
  Version            : $Revision: 14576 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;

public class VirtualAppServlet extends MesServletBase
{
  protected String    userLoginName   = "";
  protected int       appType         = -1;
  protected boolean   skipLandingPage = false;
  
  protected void route(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      StringBuffer  forwardPage = new StringBuffer("/jsp/app/landing.jsp");
      
      // redirect to ssl if necessary
      if(request.getServerPort() != 443 &&
          (HttpHelper.isProdServer(request) || HttpHelper.doesUseSSL(request)))
      {
        StringBuffer sslPage = new StringBuffer(HttpHelper.getServerURL(request));
        sslPage.append(request.getServletPath());
        
        if(request.getQueryString() != null && !request.getQueryString().equals(""))
        {
          sslPage.append("?");
          sslPage.append(request.getQueryString());
        }
        
        response.sendRedirect(sslPage.toString());
        return;
      }
      else
      {
        // knows what pricing to display
        if(forceLogin(userLoginName))
        {
          // add app type as an attribute to the request so that the landing page
          request.setAttribute("appType", appType);
          
          if( true == skipLandingPage )
          {
            forwardPage.append("?accept=y&submitButton=Submit");
          }
      
          forward(forwardPage.toString(), request, response);
        }
        else
        {
          forward("/jsp/secure/navigate.jsp", request, response);
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
