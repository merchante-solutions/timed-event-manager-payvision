/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/PdfStatementServlet.java $

  Description:

  PdfStatementServlet

  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-15 10:56:38 -0700 (Fri, 15 Aug 2008) $
  Version            : $Revision: 15266 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.servlets.utils;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.mes.reports.Statement;
import com.mes.reports.StatementPage;
import com.mes.reports.StatementRecord;
import com.mes.support.HttpHelper;
import com.mes.tools.BlobHandler;
import com.mes.tools.Builder;
import com.mes.tools.Director;
import com.mes.tools.DocData;
import com.mes.tools.InvNotifyDocBuilder;
import com.mes.tools.ReasonCodeDocBuilder;
import com.mes.tools.StatementBlobHandler;

public class PdfStatementServlet extends MesServletBase
{
  static Logger log = Logger.getLogger(PdfStatementServlet.class);

  protected void doServlet( HttpServletRequest request, HttpServletResponse response)
  {
    byte[] docBuf = null;
    String pdfType = request.getParameter("pdfType");

    try
    {
      //pdfType is defined - NEW
      if(pdfType!=null)
      {
        //chargebacks
        if(pdfType.equals("cb"))
        {
          try
          {
            long docid = HttpHelper.getLong(request,"docid",0);
            BlobHandler bHandler = new BlobHandler();
            DocData bDoc = bHandler.getData(docid);
            docBuf = bDoc.getBody();
            System.out.println("doc = "+bDoc.getFileName());
            // ContentType
            response.setContentType(bDoc.getMimeType());
           }
           catch(Exception e)
           {
           }
         }
         else if(pdfType.equals("stmt"))
         {
           //this is for statement files coming via new 3942 processing
          try
          {
            String merchNum   = request.getParameter("m");
            long yearMonth    = Long.parseLong(request.getParameter("y"));
            long    recNum    = Long.parseLong(request.getParameter("r"));

            StatementBlobHandler bHandler = new StatementBlobHandler(merchNum,yearMonth, recNum);

            docBuf = bHandler.getDataBody();

            // ContentType
            response.setContentType("application/pdf");

           }
           catch(Exception e)
           {
           }

         }
         else
         {
           //these aren't being built by the DB, they're on the fly
           //and all (roughly) follow the builder pattern, and draw out
           //the necessary values per the URL string
           Builder b = null;

           if(pdfType.equals("rc"))
           {
             //reason code letters
             long cbid = HttpHelper.getLong(request,"cbid",0);
             int scope = HttpHelper.getInt(request,"scope",ReasonCodeDocBuilder.SCOPE_WARN);

             b = new ReasonCodeDocBuilder(cbid, scope);
           }
           else if(pdfType.equals("ctn"))
           {
             //calltag notification letters
             long ctid = HttpHelper.getLong(request,"ctid",0);
             String dhl = HttpHelper.getString(request,"dhl","");
             String ltr = HttpHelper.getString(request,"ltr","");

             b = new InvNotifyDocBuilder(ltr,ctid,dhl);
           }

           Director d = new Director(b);
           d.construct();
           docBuf = (byte[])b.getResult();
           response.setContentType("application/pdf");
        }
      }
      //legacy - NEVER remove
      else
      {

        StringBuffer    logoURL           = new StringBuffer("");
        StringBuffer    logoFilename           = new StringBuffer("");
        StatementRecord rec               = new StatementRecord();

        try
        {
          rec.connect();
          try
          {
            // load a statement record with the specified statement
            log.debug("***** Loading PDF servlet *****");

            String  merchNum  = request.getParameter("m");
            long    recNum    = Long.parseLong(request.getParameter("r"));
            long    yearMonth = Long.parseLong(request.getParameter("y"));

            log.debug("getting record from database (" + merchNum +","+recNum+","+yearMonth+")");
            rec.getData(merchNum,recNum,yearMonth);

            if(rec == null)
            {
              log.error("STATMENT RECORD IS NULL");

            }

            log.debug("setting Xml Schema");
            rec.setXmlSchema();

            int fontSize = (rec.getBankNum() == 1000 ? 8 : 9);
            int paraSpacing = (rec.getBankNum() == 1000 ? 12 : 11);

            // determine which logo to use
            String logoPath = rec.getLogoFilename(merchNum);

            logoURL.append("http://");
            logoURL.append(request.getHeader("HOST"));

            logoURL.append(logoPath);

            URL logourl = new URL(logoURL.toString());

            log.debug("retrieving image from " + logoURL.toString());
            Image logo = Image.getInstance( logourl );

            // create a pdf document in a buffer
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Document doc = new Document(PageSize.A4,18,18,18,18);
            PdfWriter.getInstance(doc,baos);
            doc.open();

            // scan through the statement lines, adding them into the pdf document
            log.debug("getting statement from record");

            if(rec == null)
            {
              log.error("*** REC IS NULL ***");
            }
            Statement stmt = rec.getStatement();

            log.debug("parsing through statement lines");
            for (int i = 0, pgCnt = stmt.getPageCount(); i < pgCnt; ++i)
            {
              doc.add(logo);
              StatementPage pg = stmt.getPage(i);
              for (int j = 0, lnCnt = pg.getLineCount(); j < lnCnt; ++j)
              {
                Paragraph para =
                  new Paragraph(paraSpacing,pg.getLine(j).toString(),
                                new Font(Font.COURIER,fontSize));
                para.setAlignment(Element.ALIGN_LEFT);
                doc.add(para);
              }

              doc.newPage();
            }
            doc.close();

            // make filename string for downloading
            log.debug("making filename string");
            StringBuffer filename = new StringBuffer();
            filename.append(yearMonth);
            filename.append("_");
            filename.append(rec.getDda());
            filename.append(".pdf");

            // place the document in a byte array
            docBuf = baos.toByteArray();

            // ContentType
            response.setContentType("application/x-pdf");
          }
          catch (RuntimeException e)
          {
            log.error("PdfStatementServlet.doServlet(): " + e.toString());
            throw e;
          }
        }
        catch (Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
          log.error("PdfStatementServlet.doServlet(): "+e.toString());
        }
        finally
        {
          rec.cleanUp();
        }
      }//end if (pdfType)

      if(docBuf!=null)
      {
        // ContentLength
        response.setContentLength(docBuf.length);
        response.getOutputStream().write(docBuf,0,docBuf.length);
        response.getOutputStream().flush();
        response.getOutputStream().close();
        return;
      }
      else
      {
        response.setContentType("text/html");
        response.getWriter().write("<html><header></header><body><div align='center' width='700'>Unable to generate requested file</div></body></html>");
        response.getWriter().flush();
        response.getWriter().close();
        return;
      }
    }
    catch(Exception e)
    {
      log.error(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
