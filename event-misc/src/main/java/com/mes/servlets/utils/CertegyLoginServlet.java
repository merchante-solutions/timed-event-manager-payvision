/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/CertegyLoginServlet.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/30/02 4:17p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

/*
** CertegyLoginServlet routes users to the current Certegy login page
*/
public class CertegyLoginServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      StringBuffer            forwardPage = new StringBuffer(HttpHelper.getServerURL(request));
      
      forwardPage.append("/jsp/certegy/login.jsp");
      
      // get the requested page from the request -- try attribute first
      String requestedPage = (String)request.getAttribute("RequestedPage");
      
      if(requestedPage == null || requestedPage.equals(""))
      {
        // try to find it in the request parameters
        requestedPage = HttpHelper.getString(request, "RequestedPage");
      }
      
      if(! requestedPage.equals(""))
      {
        forwardPage.append("?RequestedPage=");
        forwardPage.append(requestedPage);
      }
      
      // send the browser to the new location
      response.sendRedirect(forwardPage.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
