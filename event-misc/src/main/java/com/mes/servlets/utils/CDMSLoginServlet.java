/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/CDMSLoginServlet.java $

  Description:
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/08/04 5:11p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

/*
** BankServLoginServlet routes users to the current BankServ login page
*/
public class CDMSLoginServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      StringBuffer            forwardPage = new StringBuffer(HttpHelper.getServerURL(request));
      
      forwardPage.append("/jsp/custom/cdms_login.jsp");
      
      // send the browser to the new location
      response.sendRedirect(forwardPage.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
