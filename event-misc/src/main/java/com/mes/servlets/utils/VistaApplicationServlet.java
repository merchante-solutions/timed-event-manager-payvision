/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/VistaApplicationServlet.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/21/02 4:45p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.SyncLog;

public class VistaApplicationServlet extends MesServletBase
{
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    String    baseURL         = "/jsp/setup/verisign_instructions.jsp";
    
    try
    {
      // send the browser to the new location
      forward(baseURL, request, response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
  
}
