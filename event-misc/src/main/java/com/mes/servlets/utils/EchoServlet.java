package com.mes.servlets.utils;

import java.io.BufferedReader;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class EchoServlet extends MesServletBase
{
  Logger log = Logger.getLogger(EchoServlet.class);

  public static int MAX_CONTENT_LENGTH = 2048;

  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    BufferedReader in = null;
    PrintWriter out = null;
    try
    {
      int contentLength = 0;
      try
      {
        contentLength = Integer.parseInt(request.getHeader("Content-Length"));
      }
      catch (Exception e)
      {
        log.error("unable to get Content-Length header: " + e);
      }

      log.debug("Content-Length: " + contentLength);

      if (contentLength > 0) 
      {
        contentLength = (contentLength > MAX_CONTENT_LENGTH ? 
          MAX_CONTENT_LENGTH : contentLength);

        in = request.getReader();
        String postData  = in.readLine();

        log.debug("echoing postData: " + postData);

        out = response.getWriter();
        out.println(postData);

        /*
        char[] postData = new char[contentLength];
        in.read(postData,0,contentLength);

        log.debug("echoing postData: '" + postData + "'");

        out = response.getWriter();
        out.println(postData);
        */

        out.flush();
      }
    }
    catch (Exception e)
    {
      log.error("error: " + e);
      throw new RuntimeException(this.getClass().getName() + ".doServlet(): " + e);
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
      try { out.close(); } catch (Exception e) { }
    }
  }
}
