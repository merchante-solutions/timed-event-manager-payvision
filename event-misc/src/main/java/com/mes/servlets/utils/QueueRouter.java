/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/QueueRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/21/02 4:41p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mes.queues.QueueTools;
import com.mes.support.SyncLog;

public class QueueRouter extends MesServletBase
{
  static Logger log = Logger.getLogger(QueueRouter.class);
  
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    String    baseURL         = "/jsp/secure/RouteErr.jsp";
    int       queueType       = 0;
    int       itemType        = 0;
    long      queueId         = 0L;
    
    try
    {
      // get the queue type
      try
      {
        queueType = Integer.parseInt(request.getParameter("type"));
      }
      catch(Exception e)
      {
        log.error("getting queue type: " + e.toString());
      }
      
      // get the queue item id
      try
      {
        queueId = Long.parseLong(request.getParameter("id"));
      }
      catch(Exception e)
      {
        log.error("getting queue item id: " + e.toString());
      }
      
      // get the queue item type
      try
      {
        itemType = Integer.parseInt(request.getParameter("itemType"));
      }
      catch(Exception e)
      {
        log.error("getting item type: " + e.toString());
      }
      
      // try to lock this item
      if(QueueTools.lockQueueItem(queueId, queueType, userLogin))
      {
        // item is now locked
        baseURL = QueueTools.getQueueFunctionURL(queueType);

        //if no url provided then bring them back to the same queue...
        if(baseURL == null || baseURL.equals(""))
        {
          baseURL = "/jsp/queues/queue.jsp?type=" + queueType;
        }
      }
      else
      {
        // item is already locked by someone else
        baseURL = "/jsp/queues/lock_error.jsp";
      }
      
      // send the browser to the new location
      forward(baseURL, request, response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
