/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/TomDaileyReferral.java $

  Description:
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.mesConstants;

public class TomDaileyReferral extends VirtualAppServlet
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      appType = mesConstants.APP_TYPE_TOMD;
      userLoginName = "tomdaileyva";
      
      skipLandingPage = true;
      
      route(request, response);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
