/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/FannieMaeApplication.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/12/03 3:10p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.tools.UserInfoBean;

public class FannieMaeApplication extends MesServletBase
{
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    StringBuffer            fannieMaeURL    = new StringBuffer(HttpHelper.getServerURL(request));
    try
    {
      // build the URL for the FannieMae application (VeriSign, essentially)
      fannieMaeURL.append("/jsp/setup/merchinfo.jsp?verisign=");
      
      UserInfoBean uib = new UserInfoBean();
      
      fannieMaeURL.append(uib.getUserId("fanniemae"));
      
      //response.setContentType("text/html");
      //PrintWriter out = response.getWriter();
      // send the browser to the new location
      response.sendRedirect(fannieMaeURL.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
