package com.mes.servlets.utils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
// log4j
import org.apache.log4j.Category;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.ops.EquipProfileDataBean;
import com.mes.queues.ProgrammingQueue;
import com.mes.support.HttpHelper;
import com.mes.support.HttpTextMessageManager;
import com.mes.user.UserBean;
                 

/**
 * EquipProfileServlet servlet class.
 * 
 * - Performs Equip Profile related commands.
 * 
 * command
 * ------
 * 
 * vep            View Equipment Profile
 * vps            View Packing Slip
 * vops           View Old Packing Slip
 * vpq            View programming queue of specified type
 * vpm            View programming queue main menu
 * pp             Pend Profile
 * cp             Complete profile
 * rlfp           re-load from pend profile op
 * rlfc           re-load from complete profile op
 * 
 */
public final class EquipProfileServlet extends MesServletBase
{
  // create class log category
  static Category log = Category.getInstance(EquipProfileServlet.class.getName());
  
  // constants
  public static final String              SERVLET_PATH              = "/srv/eqprof";
  public static final String              RELPATH_SESSIONEXPIRED    = "/jsp/secure/sLogin.jsp?type=";
  public static final String              EQUIP_PROFILE_PAGE        = "/jsp/credit/equip_profile.jsp";
  public static final String              PACKING_SLIP_PAGE         = "/jsp/credit/newpackingslip.jsp";
  public static final String              OLD_PACKING_SLIP_PAGE     = "/jsp/credit/packingslip.jsp";
  public static final String              DEFAULT_COMMAND           = "vep";

  
  // data members
  private HttpSession                     session;
  private HttpServletRequest              request;
  private HttpServletResponse             response;
  private HttpTextMessageManager          tmm;
  private StringBuffer                    forwardPage;
  private UserBean                        loginUser;
  private StringBuffer                    queryString;
  private EquipProfileDataBean            bean;


  // class functions
  // (none)
  
  // object functions
  
  // construction
  public EquipProfileServlet()
  {
    super();
    
    request=null;
    response=null;
    tmm=null;
    forwardPage = new StringBuffer(2048);  // presume 2k max len URL
    loginUser=null;
    queryString = new StringBuffer(512);
    bean = null;
  }
  
  // methods
  
  /**
   * doCommand()
   * 
   * Executes the given command.
   * Sets the page to forward to.
   * Recursive.
   */
  private void doCommand(String command)
    throws Exception
  {
    log.debug("doCommand("+command+") - START");
    
    // vep            View Equipment Profile
    if(command.equals("vep")) {
      
      // get request id
      long requestId = HttpHelper.getLong(request,"rid",0L);
      if(requestId == 0L)
        requestId =  HttpHelper.getLong(request,"id",0L);

      // load bean
      if(requestId > 0L) {
        // get resident queue (qType)
        if(!bean.load(requestId,HttpHelper.getInt(request,"type",MesQueues.Q_NONE))) {
          tmm.err("Unable to successfully load equipment profile of request id '"+requestId+"'.");
        }
      }
      forwardPage.append(EQUIP_PROFILE_PAGE);
      log.debug("vep: forwardPage="+forwardPage);
    
    // vps            View Packing Slip
    } else if(command.equals("vps")) {
      forwardPage.append(PACKING_SLIP_PAGE);
      forwardPage.append("?primaryKey=");
      forwardPage.append(bean.getAppSeqNum());
      queryString.replace(0,queryString.length(),"");
    
    // vops           View Old Packing Slip
    } else if(command.equals("vops")) {
      forwardPage.append(OLD_PACKING_SLIP_PAGE);
      forwardPage.append("?primaryKey=");
      forwardPage.append(bean.getAppSeqNum());
      queryString.replace(0,queryString.length(),"");
      
    // vpq             View programming queue
    } else if(command.equals("vpq")) {
      
      int type = HttpHelper.getInt(request,"type",MesQueues.Q_NONE);
      if(type == MesQueues.Q_NONE)
        type = MesQueues.Q_PROGRAMMING_REGULAR;
      
      // goto new queue listing
      forwardPage.append("/jsp/queues/queue.jsp?type=");
      forwardPage.append(type);

    // vpm            View programming queue main menu
    } else if(command.equals("vpm")) {
      forwardPage.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
      forwardPage.append(MesMenus.MENU_ID_PROGRAMMING_QUEUES);
    
    // pp             Pend Profile
    } else if(command.equals("pp")) {
      // defer to the QueueTool servlet
      forwardPage.append("/srv/QueueTool?");
      forwardPage.append("action=");
      forwardPage.append(MesQueues.Q_ACTION_MOVE);
      forwardPage.append("&type=");
      forwardPage.append(bean.getResidentQueue());
      forwardPage.append("&dest=");
      forwardPage.append(ProgrammingQueue.getPendingQueue(bean.getResidentQueue()));
      forwardPage.append("&user=");
      forwardPage.append(loginUser.getLoginName());
      forwardPage.append("&fieldData=Completed");
      forwardPage.append("&id=");
      forwardPage.append(bean.getRequestId());
      forwardPage.append("&primaryKey=");
      forwardPage.append(bean.getAppSeqNum());
      forwardPage.append("&destination=");
      StringBuffer destination = new StringBuffer();
      destination.append(SERVLET_PATH);
      destination.append("?method=rlfp");
      forwardPage.append(URLEncoder.encode(destination.toString(), StandardCharsets.UTF_8.name()));
      log.debug("forwardPage="+forwardPage.toString());
      queryString.replace(0,queryString.length(),"");
      return;
      
    // cp            Complete profile
    } else if(command.equals("cp")) {
      // defer to the QueueTool servlet
      forwardPage.append("/srv/QueueTool?");
      forwardPage.append("action=");
      forwardPage.append(MesQueues.Q_ACTION_MOVE);
      forwardPage.append("&type=");
      forwardPage.append(bean.getResidentQueue());
      forwardPage.append("&dest=");
      forwardPage.append(ProgrammingQueue.getCompletedQueue(bean.getResidentQueue()));
      forwardPage.append("&user=");
      forwardPage.append(loginUser.getLoginName());
      forwardPage.append("&fieldData=Completed");
      forwardPage.append("&id=");
      forwardPage.append(bean.getRequestId());
      forwardPage.append("&primaryKey=");
      forwardPage.append(bean.getAppSeqNum());
      forwardPage.append("&destination=");
      StringBuffer destination = new StringBuffer();
      destination.append(SERVLET_PATH);
      destination.append("?method=rlfc");
      forwardPage.append(URLEncoder.encode(destination.toString(), StandardCharsets.UTF_8.name()));
      log.debug("forwardPage="+forwardPage.toString());
      queryString.replace(0,queryString.length(),"");
      return;
      
    // rlfp           Re-load from pend op
    } else if(command.equals("rlfp")) {
        bean.reLoad();
        if(ProgrammingQueue.getPendingQueue(bean.getResidentQueue())==bean.getResidentQueue()) {
          tmm.msg("Profile pended.");
        } else {
          tmm.err("Profile not pended due to an error.");
        }
        forwardPage.setLength(0);
        doCommand("vep");
    
    // rlfc           Re-load from completd op
    } else if(command.equals("rlfc")) {
        bean.reLoad();
        log.debug("bean reloaded.");
        if(ProgrammingQueue.isCompletedProgrammingQueue(bean.getResidentQueue())) {
          tmm.msg("Profile marked completed.");
        } else {
          tmm.err("Profile not marked completed due to an error.");
        }
        log.debug("now view ep...");
        forwardPage.setLength(0);
        doCommand("vep");
    
    // Unhandled request
    } else {
      forwardPage.append(request.getServerName());
    }
    
  }
  
  private boolean userPermissionsCheck(String command)
  {
    boolean rval = true;  // default to allow op
    
    // TODO: implement
    
    return rval;
  }

  private synchronized void processRequest(HttpServletRequest request,HttpServletResponse response)
  {
    try {
      
      log.debug("processRequest() - START");
      
      // reset
      this.request=request;
      this.response=response;
      forwardPage.replace(0,forwardPage.length(),"");
      forwardPage.append(HttpHelper.getServerURL(request));
      queryString.replace(0,queryString.length(),"");
      
      // get ref to the current session
      if((session = request.getSession(false))==null) {
        response.sendRedirect(RELPATH_SESSIONEXPIRED);
        return;
      }
        
      // get ref. to the UserLogin bean for user related session info
      if((loginUser=(UserBean)session.getAttribute("UserLogin"))==null) {
        log.error("Unable to obtain reference to the login user.");
        throw new ServletException("Unable to obtain reference to the login user.");
      }
      
      // get the ref. to the session-scope TextMessageManager
      if((tmm=(HttpTextMessageManager)session.getAttribute("HttpTextMessageManager"))==null) {
        tmm = new HttpTextMessageManager();
        session.setAttribute("HttpTextMessageManager",tmm);
        log.debug("HttpTextMessageManager session attribute set.");
      }
      tmm.setHttpRequest(request);
      
      // get ref to request scope equipment profile data bean
      if((bean = (EquipProfileDataBean)session.getAttribute("EquipProfBean"))==null) {
        log.debug("Instantiating 'EquipProfBean'...");
        bean = new EquipProfileDataBean();
        session.setAttribute("EquipProfBean",bean);
      }
     
      // get command
      String command=request.getParameter("method");
      if(command==null) {
        log.debug("Setting command to default...");
        command=DEFAULT_COMMAND;
      } else {
        // translate command?
        //if(command.equals("")) {
        //  command="";
        //}
      }
      
      // retain query string
      String qs = request.getQueryString();
      if(qs!=null && qs.length()>0 && !qs.equals("null"))
        queryString.append(request.getQueryString());
      
      // user permissions check
      if(userPermissionsCheck(command)) {
        // *** do command ***
        doCommand(command);
      }
      
      log.debug("Setting No cache in the header...");
      response.setHeader("Cache-Control","no-store");
      response.setHeader("Pragma","no-cache");
      response.setDateHeader("Expires",0);
      
      // send the browser to the new location
      log.debug("send the browser to the new location...");
      // IMPT: append the query string to tell the browser to force a refresh of the page!
      if(queryString.length()>0 && forwardPage.toString().indexOf('?')<0) {
        forwardPage.append("?");
        forwardPage.append(queryString);
      }
      
      // send the browser to the new location
      log.debug("Redirecting to page '"+forwardPage.toString()+"'...");
      //RequestDispatcher disp = getServletConfig().getServletContext().getRequestDispatcher(forwardPage.toString());
      //disp.forward(request,response);
      response.sendRedirect(forwardPage.toString());
      
    }
    catch(Exception e) {
      log.error("processRequest() EXCEPTION: '"+e.getMessage()+"'.");
      e.printStackTrace();
    }
  }

  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    processRequest(request,response);
  }

  public String getSignature()
  {
    return "Merchant e-Solutions - Equipment Profile - Controller";
  }

} // EquipProfileServlet