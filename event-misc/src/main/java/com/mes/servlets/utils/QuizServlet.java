package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesUsers;
import com.mes.reports.quiz.Quiz;
import com.mes.reports.quiz.QuizHandler;
import com.mes.support.SyncLog;

public class QuizServlet extends MesServletBase
{
  // create class log category
  static Logger log = Logger.getLogger(QuizServlet.class);

  public static final String RELPATH_QUIZ = "/jsp/reports/quiz/quiz.jsp";

  private Quiz quiz;

  protected void doServlet( HttpServletRequest request, HttpServletResponse response)
  {

    try
    {
      //session exist at higher level
      userLogin = getUserLogin();

      //check request info - get key
      String qTag         = getParam(request,Quiz.QUIZ_ID_TAG);
      String action       = getParam(request,Quiz.QUIZ_ACTION_TAG);
      String path         = RELPATH_QUIZ;

      if(userLogin==null)
      {
        log.warn("UserLogin is NULL");
        forward("/jsp/secure/navigate.jsp",request,response);
        //throw new Exception("UserLogin is NULL");
      }

      //check for existing quiz in session -
      quiz          = (Quiz)session.getAttribute(qTag);

      //build shell first in - will set into session at bottom
      //there should be a QUIZ_TYPE_TAG here only
      if( quiz == null)
      {
        log.debug("building quiz...");
        String qType        = getParam(request,Quiz.QUIZ_TYPE_TAG);
        quiz                = QuizHandler.build(qType, userLogin);
      }

      //rebuild query string for forward at bottom
      StringBuffer queryString = new StringBuffer();
      queryString.append(Quiz.QUIZ_ID_TAG).append("=").append(quiz.getIDTag());

      if(action.equals(Quiz.ACTION_SEARCH))
      {
        //do search
        //TODO - make this generic based on rights assoc with given quiz
        if(userLogin.hasRight(MesUsers.RIGHT_PCI_QUIZ_SEARCH))
        {
          //process the action
          //quiz.processAction(request,queryString);

          //go to the search page
          path = "/jsp/reports/quiz/search.jsp";
        }
        else
        {
          //move to error page
          quiz.moveToErrorPage();
        }
      }
      else if(action.equals(Quiz.ACTION_LOAD))
      {
        //process the action - just update the username to reflect the viewer
        //no validation here, handled by rights above this
        quiz.processAction(request,queryString);

        //ensure that the path is correct
        path = RELPATH_QUIZ +"?"+ queryString.toString();

      }
      else if(action.equals(Quiz.ACTION_COMPLETE))
      {
        //END
        session.removeAttribute(qTag);
        forward("/jsp/secure/navigate.jsp",request,response);
        return;
      }
      else
      {
        if(QuizHandler.validateUser(userLogin, quiz))
        {
          if(quiz.doOutsideProcess(request,queryString))
          {
            quiz.processAction(request,queryString);
          }
        }

        path = RELPATH_QUIZ +"?"+ queryString.toString();
      }

      //put quiz into session
      session.setAttribute(quiz.getIDTag(), quiz);

      //return to quiz -
      forward(path,request,response);

    }
    catch (Exception e)
    {
      //e.printStackTrace();
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet", e.toString());
      forward("/jsp/secure/navigate.jsp",request,response);
      //getBackToWhereYouOnceBelonged(request, response);
    }
  }

}

