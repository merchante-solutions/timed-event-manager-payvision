/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/RAPLoginServlet.java $

  Description:
  
  RAPLoginServlet
  
  Redirects the request to a login jsp.  Depending on mes.properties
  settings the request is redirected with ssl or not.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/21/02 4:45p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

/*
** RAPLoginServlet routes users to the current RAP login page
*/
public class RAPLoginServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      StringBuffer            forwardPage = new StringBuffer(HttpHelper.getServerURL(request));
      
      forwardPage.append("/rap/users/login.jsp");
      
      String queryString = request.getQueryString();
      if (queryString != null)
      {
        forwardPage.append("?" + queryString);
      }
      
      // send the browser to the new location
      response.sendRedirect(forwardPage.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
