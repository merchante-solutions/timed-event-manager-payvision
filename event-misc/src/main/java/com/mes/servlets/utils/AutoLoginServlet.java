/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AuthNetRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mes.support.HttpHelper;
import com.mes.user.AutoLoginBean;

public class AutoLoginServlet extends MesServletBase
{
  static Logger log = Logger.getLogger(AutoLoginServlet.class);
  
  protected String    userLoginName = "";
  protected int       appType       = -1;
  
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      // redirect to ssl if necessary
      if(request.getServerPort() != 443 &&
          (HttpHelper.isProdServer(request) || HttpHelper.doesUseSSL(request)))
      {
        StringBuffer sslPage = new StringBuffer(HttpHelper.getServerURL(request));
        sslPage.append(request.getServletPath());
        
        if(request.getQueryString() != null && !request.getQueryString().equals(""))
        {
          sslPage.append("?");
          sslPage.append(request.getQueryString());
        }
        
        response.sendRedirect(sslPage.toString());
        return;
      }
      else
      {
        AutoLoginBean alb = new AutoLoginBean(request);
        
        if(alb.canLogin())
        {                                        
          forceLogin(HttpHelper.getString(request, "login"));
        }
        
        StringBuffer landingPage = new StringBuffer(HttpHelper.getServerURL(request));
        landingPage.append("/jsp/secure/navigate.jsp");
        
        response.sendRedirect(landingPage.toString());
        return;
        
        //forward("/jsp/secure/navigate.jsp", request, response);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
