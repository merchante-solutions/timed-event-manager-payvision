/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AppServlet.java $

  Description:
  
  AppServlet
  
  Central online app routing servlet.  Routes requests for app data to
  appropriate app-related pages, handles app data submits, etc.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 5/05/03 3:39p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.setup.AppUtils;
import com.mes.support.SyncLog;

public class AppServlet extends MesServletBase
{
  /*
  ** private long getAppSeqNum(HttpServletRequest request)
  **
  ** Attempts to locate the app seq num in the request.
  **
  ** RETURNS: appSeqNum if found, else -1L.
  */
  private long getAppSeqNum(HttpServletRequest request)
  {
    long appSeqNum = -1L;
    try
    {
      String[] asnNames =
      {
        "appSeqNum",
        "primaryKey"
      };
    
      for (int i = 0; i < asnNames.length; ++i)
      {
        String parm = request.getParameter(asnNames[i]);
        if (parm != null)
        {
          try
          {
            appSeqNum = Long.parseLong(parm);
            break;
          }
          catch (Exception e)
          {
          }
        }
      }
    }
    catch (Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::getAppSeqNum()",e.toString());
    }
    return appSeqNum;
  }
  
  /*
  ** public void doServlet( HttpServletRequest request, HttpServletResponse response)
  **   throws java.io.IOException, javax.servlet.ServletException 
  **
  ** Handles the two basic app requests, starting a new application and viewing
  ** an existing application.  If an appSeqNum is not found in the request 
  ** then the request is handled as a new application, if one if found then the
  ** request is handled as a request for an existing app.
  */
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    try
    {
      String appPage;
      long seqType;
      
      // see if a app seq num for an existing app was given      
      long appSeqNum = getAppSeqNum(request);
      
      // if app seq num given, get the seq type of the app in question
      if (appSeqNum != -1L) // exists
      {
        seqType = AppUtils.getScreenSequenceType(appSeqNum);
      }
      // no seq num so must be new app, get the user's seq type
      else
      {
        seqType = getUserLogin().getSequenceType();
      }
        
      // get the first page of the app from the determined seq type
      appPage = AppUtils.getApplicationFirstPage(seqType);
      
      // work around for mesConstants.SEQUENCE_ID_APPLICATION first
      // page url which ends with 'id='
      if(appPage.indexOf("?id=") > 0)
      {
        appPage += "-1";
      }
      
      // forward the request to the determined sequence page
      forward(appPage,request,response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
