/*************************************************************************

  URL: $Archive: /Java/servlets/com/mes/utils/QueueTool.java $

  Description:
  
  SingletonAssassin
  
  Startup and shutdown servlet that keeps singletons down
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-06 11:02:28 -0700 (Mon, 06 Aug 2007) $
  Version            : $Revision: 13944 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import com.mes.tools.MESSingletons;

public class SingletonAssassin extends MesServletBase
{
  public void init()
  {
    System.out.println("*********************************");
    System.out.println("* SingletonAssassin Initialized *");
    System.out.println("*********************************");
  }
  
  public void destroy()
  {
    try
    {
      System.out.println("*******************************");
      System.out.println("* SingletonAssassin Activated *");
      System.out.println("*******************************");
      
      MESSingletons.destroyAll();
    }
    catch(Exception e)
    {
      System.out.println("SingletonAssassin: " + e.toString());
    }
  }
}