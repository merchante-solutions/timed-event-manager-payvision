/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AppServlet.java $

  Description:
  
  HideServiceCall
  
  Servlet that marks a specific service call as hidden

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 5/05/03 3:39p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.MesUsers;
import com.mes.maintenance.ServiceCallBean;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

public final class HideServiceCall extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      if(getUserLogin() != null && userLogin.hasRight(MesUsers.RIGHT_HIDE_SERVICE_CALLS))
      {
        long sequence = HttpHelper.getLong(request, "seq");
      
        ServiceCallBean.hideCall(sequence);
      }
      
      // return from whence you came
      getBackToWhereYouOnceBelonged(request, response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}