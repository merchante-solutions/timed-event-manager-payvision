/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AuthNetRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-01-11 14:23:48 -0800 (Fri, 11 Jan 2008) $
  Version            : $Revision: 14474 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.app.virtual.MiniMerchantApp;
import com.mes.support.HttpHelper;

public class MiniMerchantAppServlet extends MesServletBase
{
  protected String    userLoginName   = "";
  protected int       appType         = -1;
  
  protected void route(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
        // redirect to ssl if necessary
      if(request.getServerPort() != 443 &&
          (HttpHelper.isProdServer(request) || HttpHelper.doesUseSSL(request)))
      {
        StringBuffer sslPage = new StringBuffer(HttpHelper.getServerURL(request));
        sslPage.append(request.getServletPath());
      
        if(request.getQueryString() != null && !request.getQueryString().equals(""))
        {
          sslPage.append("?");
          sslPage.append(request.getQueryString());
        }
      
        response.sendRedirect(sslPage.toString());
        return;
      }
      else
      {
        MiniMerchantApp miniApp = new MiniMerchantApp(request);
        
        if( miniApp.initialized )
        {
          userLoginName = miniApp.getLoginName();
          appType       = miniApp.getAppType();
    
          StringBuffer  forwardPage = new StringBuffer(miniApp.getStartPage());
    
          // knows what pricing to display
          if( forceLogin(miniApp.getLoginName()) )
          {
            request.setAttribute("appType", appType);
        
            forward(forwardPage.toString(), request, response);
            return;
          }
        }
      }
      
      forward("/jsp/secure/navigate.jsp", request, response);
      return;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
  
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    route(request, response);
  }
}
