/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/MenuRoutingServlet.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mes.constants.MesMenus;
import com.mes.setup.AppUtils;
import com.mes.support.HttpHelper;
import com.mes.support.PropertiesFile;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;

public class MenuRoutingServlet extends MesServletBase
{
  public String buildURL(HttpServletRequest request, String baseURL)
  {
    PropertiesFile  propFile  = null;
    
    try
    {
      propFile = new PropertiesFile("mes.properties");
    }
    catch(Exception e)
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new PropertiesFile( inputStream );

      try { inputStream.close(); } catch(Exception re) {}
    }
    
    StringBuffer newURL = new StringBuffer("");
    
    // assign the protocol
    if(propFile.getBoolean("com.mes.useSSL", false))
    {
      newURL.append("https://");
    }
    else
    {
      newURL.append("http://");
    }
    
    // add the host name
    newURL.append(request.getHeader("HOST"));
    
    // add the base URL
    newURL.append(baseURL);
    
    return newURL.toString();
  }
  
  public UserBean getUserLogin(HttpServletRequest request)
  {
    UserBean  userLogin       = null;
    
    try
    {
      HttpSession session = request.getSession(false);
      
      if(session != null)
      {
        userLogin = (UserBean)session.getAttribute("UserLogin");
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::getUserLogin()", e.toString());
    }
    
    return userLogin;
  }
  
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    int       destCode        = MesMenus.MENU_DEST_INVALID;
    long      primaryKey      = 0L;
    String    baseURL         = "/jsp/secure/RouteErr.jsp";
    UserBean  userLogin       = getUserLogin(request);
    
    try
    {
      if(userLogin == null || ! userLogin.validated(request))
      {
        baseURL = "/jsp/secure/Login.jsp";
      }
      else
      {
        // get the requested destination code
        try
        {
          destCode    = Integer.parseInt(request.getParameter("dest"));
          primaryKey  = HttpHelper.getLong(request, "primaryKey", 0L);
        }
        catch(Exception parseException)
        {
          SyncLog.LogEntry(this.getClass().getName() + "::parseDestCode(" + request.getParameter("dest") + ")", parseException.toString());
          destCode = MesMenus.MENU_DEST_INVALID;
        }
      
        switch(destCode)
        {
          case MesMenus.MENU_DEST_SUBMIT_APP:
            
            //redirects to the first page based on a given sequence.. one link for both new and old apps.. 
            //also allowed primaryKey to be passed in.. so this could be the only router for all applications new or old.
            
            long seqType = userLogin.getSequenceType();

            //if there is a primary key.. then get the screen sequence id from the application table 
            //use that id.. and not the one associated with the user..
            if(primaryKey > 0L)
            {
              seqType = AppUtils.getScreenSequenceType(primaryKey);
            }

            baseURL = AppUtils.getApplicationFirstPage(seqType);

            //this is to fix the baseURL of the mesConstants.SEQUENCE_ID_APPLICATION sequence
            if(baseURL.indexOf("?id=") > 0)
            {
              baseURL += "-1";
              if(primaryKey > 0L)
              {
                baseURL += "&primaryKey=" + primaryKey;
              }
            }
            else
            {
              if(primaryKey > 0L)
              {
                if(baseURL.indexOf("?") == -1)
                {
                  baseURL += "?primaryKey=" + primaryKey;
                }
                else
                {
                  baseURL += "&primaryKey=" + primaryKey;
                }
              }
            }
            break;
            
          case MesMenus.MENU_DEST_BBT_APP_PAGE0:
            // redirects to the bb&t specific ola page 0
            baseURL = "/jsp/app/bbt/page0.jsp";
            break;
          
          case MesMenus.MENU_DEST_ACTIVITY_REPORTS:
            break;
          
          case MesMenus.MENU_DEST_INVALID:
            baseURL = "/jsp/secure/RouteErr.jsp";
            break;
        }
      }
      
      response.setContentType("text/html");
    
      response.setStatus(response.SC_MOVED_TEMPORARILY);
      response.setHeader("Location", buildURL(request, baseURL));
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
