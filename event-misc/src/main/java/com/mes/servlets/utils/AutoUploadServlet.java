/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AutoUploadServlet.java $

  Description:
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/10/03 11:48a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.ops.AutoUpload;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

public class AutoUploadServlet extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      // perform auto-upload of this application
      int appSeqNum = HttpHelper.getInt(request, "appSeqNum");
      
      if(appSeqNum > 0)
      {
        // see if this app can be auto uploaded
        if(AutoUpload.canAutoUpload(appSeqNum))
        {
          // check for expand only option
          if(HttpHelper.getString(request, "action").equals("expand"))
          {
            // only do data expansion
            AutoUpload.doAutoUpload(appSeqNum);
          }
          else
          {
            // perform the auto upload
            AutoUpload.doAutoUpload(appSeqNum);
          
            // submit to tape upload process
            AutoUpload.submitToTape(appSeqNum);
          
            // move to MMS queue
            AutoUpload.moveToMMS(appSeqNum, getUserLogin());
          
            // move setup queue item to completed queue
            AutoUpload.moveToSetupComplete(appSeqNum, getUserLogin());
          }
        }
      }
      else
      {
        SyncLog.LogEntry("com.mes.utils.AutoUploadServlet()", "appSeqNum was zero");
      }
    
      // send browser to requested url
      response.sendRedirect(HttpHelper.getString(request, "returnURL", "/jsp/secure/navigate.jsp"));
    }
    catch(Exception e)
    {
      SyncLog.LogEntry("com.mes.utils.AutoUploadServlet::doServlet()", e.toString());
    }
  }
}
