/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/TridentProfilePostEdit.java $

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.maintenance.TridentProfile;
import com.mes.queues.QueueTools;

public class TridentProfilePostEdit extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    int           action    = 0;
    int           actionId  = 0;
    int           requestId = 0;
    StringBuffer  baseUrl   = new StringBuffer("");
    
    try
    {
      action    = Integer.parseInt(request.getParameter("tppeact"));
      actionId  = Integer.parseInt(request.getParameter("actionId"));
      requestId = Integer.parseInt(request.getParameter("requestId"));
      
      switch(action)
      {
        case TridentProfile.TPPE_PROGRAMMING_QUEUE:
          baseUrl.append("/jsp/queues/queue.jsp?type=");
          switch(actionId)
          {
            case mesConstants.PROFGEN_VERICENTRE:
              baseUrl.append(Integer.toString(MesQueues.Q_PROGRAMMING_VC_NEW));
              break;
        
            case mesConstants.PROFGEN_TERMMASTER:
              baseUrl.append(Integer.toString(MesQueues.Q_PROGRAMMING_TM_NEW));
              break;
        
            case mesConstants.PROFGEN_TRIDENT:
              baseUrl.append(Integer.toString(MesQueues.Q_PROGRAMMING_REGULAR));
              break;
        
            default:
              baseUrl.setLength(0);
              baseUrl.append("/jsp/secure/navigate.jsp");
              break;
          }
          break;
          
        case TridentProfile.TPPE_MMS_NEW_QUEUE:
          baseUrl.append("/jsp/queues/queue.jsp?type=");
          baseUrl.append(Integer.toString(MesQueues.Q_MMS_NEW));
          break;
          
        default:
          baseUrl.append("/jsp/secure/navigate.jsp");
          break;
      }
      
      // move item to completed MMS queue
      QueueTools.moveQueueItem( actionId,
                                MesQueues.Q_MMS_NEW,
                                MesQueues.Q_MMS_COMPLETE,
                                getUserLogin(),
                                "");
      
      // send user to their final destination
      forward(baseUrl.toString(), request, response);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("TridentProfilePostEdit::doServlet()", e.toString());
    }
  }
}
