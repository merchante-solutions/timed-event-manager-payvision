/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AuthNetRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/24/04 2:30p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthNetRouter extends MesServletBase
{
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      if(forceLogin("authnetvirtualapp"))
      {
        forward("/jsp/app/authnet/business.jsp", request, response);
      }
      else
      {
        forward("/jsp/secure/navigate.jsp", request, response);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}