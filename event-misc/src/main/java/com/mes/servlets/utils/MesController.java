/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/MesController.java $

  Description:
  
  AppServlet
  
  Central online app routing servlet.  Routes requests for app data to
  appropriate app-related pages, handles app data submits, etc.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 1/02/04 11:03a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.SyncLog;

public class MesController extends MesServletBase
{
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    try
    {
      StringBuffer  newUrl = new StringBuffer("/jsp2");
      
      newUrl.append(request.getRequestURI().substring(4));
      
      System.out.println("Hit controller servlet - " + newUrl.toString());
      
      // forward the request to the determined sequence page
      forward(newUrl.toString(),request,response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
