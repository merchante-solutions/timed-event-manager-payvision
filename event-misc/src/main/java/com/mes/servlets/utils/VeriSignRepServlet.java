/*************************************************************************

  File: VeriSignRepServlet
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;

public class VeriSignRepServlet extends MesServletBase
{
  public static final String TEAM_1_URL = "/jsp/setup/merchinfo2.jsp?id=292622&promotionCode=";
  public static final String TEAM_2_URL = "/jsp/setup/merchinfo2.jsp?id=292623&promotionCode=";
  public static final String SENIOR_URL = "/jsp/setup/merchinfo2.jsp?id=308123&promotionCode=";
  
  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
    int           team = 0;
    String        promoCode = "";
    StringBuffer  appUrl = new StringBuffer("");
    
    try
    {
      // get team and promo code from url
      team = HttpHelper.getInt(request, "team", 1);
      promoCode = HttpHelper.getString(request, "promo");
      
      appUrl.append(HttpHelper.getServerURL(request));
      
      switch(team)
      {
        case 1:
        default:
          appUrl.append(TEAM_1_URL);
          break;
          
        case 2:
          appUrl.append(TEAM_2_URL);
          break;
          
        case 99:
          appUrl.append(SENIOR_URL);
          break;
      }
      
      appUrl.append(promoCode);
      
      response.sendRedirect(appUrl.toString());
      
      return;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
