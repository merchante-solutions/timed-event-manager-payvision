package com.mes.servlets.utils;

import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.mes.calltag.CallTag;
import com.mes.calltag.CallTagConstants;
import com.mes.calltag.CallTagDAO;
import com.mes.calltag.CallTagDAOFactory;
import com.mes.calltag.CallTagDetailBean;
import com.mes.calltag.CallTagDetailQueueBean;
import com.mes.calltag.CallTagItem;
import com.mes.calltag.CallTagNotification;
import com.mes.calltag.CallTagSession;
import com.mes.calltag.ConfirmationBean;
import com.mes.calltag.InventoryStatusBean;
import com.mes.calltag.ItemBean;
import com.mes.calltag.Note;
import com.mes.calltag.NoteManager;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.equipment.EquipmentDAO;
import com.mes.equipment.EquipmentDAOFactory;
import com.mes.equipment.EquipmentDataBuilder;
import com.mes.equipment.EquipmentData_EquipInv;
import com.mes.equipment.EquipmentHistory;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldError;
import com.mes.queues.CallTagQueue;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueSessionBean;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.support.HttpTextMessageManager;
import com.mes.support.StringUtilities;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentActionKey;
import com.mes.tools.EquipmentKey;
import com.mes.tools.Key;
import com.mes.tools.KeyBuilder;
import com.mes.user.UserBean;

/**
 * Calltag servlet class.
 *
 * - Performs Calltag commands.
 * - Forwards to appropriate page.
 *
 * command
 * -------
 *
 * vbdm             View by Display Mode
 *
 * vact             View Add Call Tag                      (default)
 * vect             View Edit Call Tag
 * vacti            View Add Call Tag Item
 * vecti            View Edit Call Tag Item
 * vactn            View Add Call Tag Notification
 * vectn            View Edit Call Tag Notification

 * vctd             View Call Tag detail
 * vctid            View Call Tag Item detail
 * vctnd            View Call Tag Notification detail

 * act              Add Call Tag
 * uct              Update Call Tag
 * acti             Add Call Tag Item
 * ucti             Update Call Tag Item
 * dcti             Delete Call Tag Item
 * actn             Add Call Tag Notification
 * uctn             Update Call Tag Notification
 * dctn             Delete Call Tag Notification

 * vcts             View Call Tag Search
 * dqo              Do queue op
 * vctq             View Call Tag Queues
 * sctp             [re-]Set Call Tag Parent (source)
 * vctp             View Call Tag parent (source)
 * vsrc             View Source Userid
 * sctie            [re-]Set Call Tag Item Equipment
 * vm               View Merchant
 * ved              View Equipment Detail
 * verr             View Error page

 * aeta             Assign equipment to account
 * ueta             Unassign equipment to account

 * aeti             Add equipment to MeS Inventory

 */


public final class CallTagServlet extends MesServletBase
{
  static Logger log = Logger.getLogger(CallTagServlet.class);

  public static CallTagConstants    cmds            = null;
  public static CallTagConstants    paths           = null;

  public static final String RELPATH_PRESENTATION   = "/jsp/calltag/";
  public static final String RELPATH_CALLTAGLOOKUP  = "/jsp/maintenance/lookup_calltag.jsp?action=9";
  public static final String RELPATH_QUEUELISTING   = "/jsp/queues/queue.jsp?type=";
  public static final String RELPATH_QUEUEROUTER    = "/srv/QueueRouter?itemType=10";
  public static final String RELPATH_SESSIONEXPIRED = "/jsp/secure/sLogin.jsp?type=";
  public static final String RELURL_INSUFACCESS     = "/jsp/secure/InsufficientAccess.jsp";
  public static final String RELPATH_USERPROFILE    = "/jsp/credit/contact_info.jsp?userName=";
  public static final String RELPATH_ACCOUNT        = "/jsp/maintenance/view_account.jsp?merchant=";

  // data members
  private HttpSession               session         = null;
  private HttpServletRequest        request         = null;
  private HttpServletResponse       response        = null;
  private UserBean                  loginUser       = null;
  private HttpTextMessageManager    tmm             = null;
  private StringBuffer              forwardPage     = null;
  private StringBuffer              queryString     = null;
  private CallTagSession            ctsession       = null;
  private CallTagDAO                ctdao           = null;
  private QueueSessionBean          qsb             = null;
  private CallTagDetailQueueBean    ctBean          = null;

  public CallTagServlet()
  {
    super();

    forwardPage = new StringBuffer();
    queryString = new StringBuffer();
    ctdao = CallTagDAOFactory.getInstance().getCallTagDAO();
  }

  /**
   * Adds the error message to the text message manager, sets the display mode
   * and executes the display command.
   */
  private void indicateError(String errorMsg, int displayMode) throws Exception
  {
    tmm.err(errorMsg);
    ctBean.setDisplayMode(displayMode);
    doCommand("vbdm");
  }

  /**
   * Adds the info message to the text message manager, sets the display mode
   * and executes the display command.
   */
  private void indicateInfo(String infoMsg, int displayMode) throws Exception
  {
    tmm.msg(infoMsg);
    ctBean.setDisplayMode(displayMode);
    doCommand("vbdm");
  }

  /**
   * Validation errors in the given bean are extracted and placed in the
   * text message manager.
   */
  private void extractErrorMessages(FieldBean bean)
  {
    if (!bean.hasErrors())
    {
      return;
    }

    for (Iterator i = bean.getErrors().iterator(); i.hasNext();)
    {
      Object error = i.next();

      // if error is a field error, turn it into an
      // error string based on the field error
      if (error instanceof FieldError)
      {
        FieldError fieldError = (FieldError)error;
        error = fieldError.getFieldLabel() + ": "
          + fieldError.getErrorText();
      }
      tmm.err(error.toString());
    }
  }

  /**
   * Looks for an call tag item id in the request and attempts to fetch
   * it from the call tag and return it.  The command that initiated the
   * fetch should be passed on so that error messages are more informative.
   */
  private CallTagItem getItemWithRequestedId(String command) throws Exception
  {
    // get the tag id
    String idParm = request.getParameter("itemId");
    long itemId = -1L;
    try
    {
      itemId = Long.parseLong(idParm);
    }
    catch (Exception e) { }
    if (itemId == -1L)
    {
      log.error(command + " - invalid or missing item id " + idParm);
      indicateError("Invalid or missing item id (" + itemId + ")",
        ctBean.DISPMODE_CT_EDIT);
      return null;
    }

    // retrieve the tag item using the id
    CallTag calltag = ctBean.getCallTag();
    CallTagItem tagItem = calltag.getItem(itemId);
    if (tagItem == null)
    {
      log.error(command + " attempted on non-existent item: " + itemId);
      indicateError("Item with id " + itemId + " not found",
        ctBean.DISPMODE_CT_EDIT);
      return null;
    }
    log.debug("found call tag item: " + tagItem.getEquipmentKey());

    return tagItem;
  }
  
  /**
   * Persist the given calltag to the database.
   */
  private boolean persistChanges(CallTag calltag)
  {
    boolean persistOk = false;
    
    try
    {
      calltag.dirty();
      persistOk = ctdao.persistCallTag(calltag);
    }
    catch (Exception e)
    {
      log.error("Calltag persist error: " + e);
    }
    
    log.debug("Calltag persist " + (persistOk ? " successful" : " FAILED"));
    return persistOk;
  }

  /**
   * Add a new call tag item with the given equipment key and status to the
   * calltag and persists the change to the db.  Returns the calltag item
   * that was added.
   */
  private CallTagItem addCalltagItem(EquipmentKey key, int tagStatus, 
    String received)
  {
    CallTagItem addedItem = null;
    
    try
    {
      // create new call tag item in db
      CallTagItem newItem = ctdao.createCallTagItem();

      // set equipment key in tag item
      newItem.setEquipmentKey(key);

      // set item status
      newItem.setStatusCode(tagStatus);
      
      // set the received indicator
      newItem.setReceived(received);

      // add the item to the bean
      CallTag calltag = ctBean.getCallTag();
      calltag.addItem(newItem);
      ctBean.setCurrentItem(newItem.getId());

      // persist the call tag data to db
      if (persistChanges(calltag))
      {
        addedItem = newItem;
        log.debug("Calltag item add for " + key + " successful");
      }
      else
      {
        log.error("Calltag item add for " + key + " FAILED");
      }
    }
    catch (Exception e)
    {
      log.error("error adding calltag item with key " + key + ": " + e);
    }
    
    return addedItem;
  }
  
  /**
   * Deletes the given calltag item from the Calltag object and persists
   * the change to the database.
   */
  private boolean deleteCalltagItem(CallTagItem delItem)
  {
    boolean deleteOk = false;
    
    try
    {
      // remove calltag item from bean
      CallTag calltag = ctBean.getCallTag();
      calltag.removeItem(delItem);
    
      // persist to db
      deleteOk = persistChanges(calltag);
      log.debug("calltag item delete for " + delItem.getEquipmentKey()
        + (deleteOk ? " successful" : " FAILED"));
    }
    catch (Exception e)
    {
      log.error("error deleting calltag item with key " 
        + delItem.getEquipmentKey() + ": " + e);
    }
    
    return deleteOk;
  }

  /**
   * CMD_ADD_TAG_ITEM
   *
   * Add an equipment item.  Uses ItemBean helper with the add/edit item
   * screen.  Creates a new incoming or outgoing equipment item and adds
   * it into the calltag.
   */
  private void addTagItem() throws Exception
  {
    // create helper bean, add to session
    ItemBean itemBean = new ItemBean(ctBean.getCallTag());
    ctsession.setHelperBean(itemBean);
    
    // process any request data
    itemBean.autoSetFields(request);

    // return without adding?
    if (itemBean.getData(itemBean.FN_RETURN).length() > 0)
    {
      // send user back to main call tag screen without changing anything
      ctBean.setDisplayMode(ctBean.DISPMODE_CT_EDIT);
      doCommand("vbdm");
      return;
    }

    // non- or unsuccessful submit: forward to add item page
    if (!itemBean.isAutoSubmitOk())
    {
      forwardPage.append(paths.PATH_ADD_TAG_ITEM);
      extractErrorMessages(itemBean);
      return;
    }

    // successful submit so try to add new call tag item
    EquipmentKey equipKey = itemBean.generateKey();
    int itemStatus = itemBean.getData(itemBean.FN_ITEM_TYPE).equals("in") 
      ? CallTagItem.STATUS_INCOMING : CallTagItem.STATUS_OUTGOING;
    String received = itemStatus == CallTagItem.STATUS_INCOMING 
      ? itemBean.getData(itemBean.FN_RECEIVED).toUpperCase() : "N";
    CallTagItem newItem = addCalltagItem(equipKey,itemStatus,received);
    if (newItem != null)
    {
      // add ok, return to main screen, inform user
      indicateInfo("New item " + equipKey + " added",ctBean.DISPMODE_CT_EDIT);
      return;
    }
    
    // indicate error, stay on add page
    log.error("Error adding item " + equipKey);
    tmm.err("Error adding item");
    forwardPage.append(paths.PATH_ADD_TAG_ITEM);
  }

  /**
   * CMD_DEL_TAG_ITEM
   *
   * Delete a call tag item.  User must confirm via a confirmation screen.
   * A tag id needs to be present in the query string.
   *
   * Parms wanted: itemId
   */
  private void deleteCalltagItem() throws Exception
  {
    // fetch the call tag item to delete
    CallTagItem delItem = getItemWithRequestedId(cmds.CMD_DEL_TAG_ITEM);
    if (delItem != null)
    {
      // make a confirmation screen helper bean
      ConfirmationBean confirmBean = new ConfirmationBean("Delete call tag item "
        + delItem.getEquipmentKey() + "?","method=" + cmds.CMD_DEL_TAG_ITEM
        + "&itemId=" + delItem.getId());
      confirmBean.autoSetFields(request);

      // did user cancel?
      String cancellation = request.getParameter("cancelBtn");
      if (cancellation != null)
      {
        // return to main screen without doing anything
        ctBean.setDisplayMode(ctBean.DISPMODE_CT_EDIT);
        doCommand("vbdm");
        return;
      }

      // did user confirm?
      String confirmation = request.getParameter("confirmBtn");
      if (confirmation == null)
      {
        // ask the user for confirmation using the confirmation screen
        ctsession.setHelperBean(confirmBean);
        forwardPage.append(paths.PATH_CONFIRMATION);
        return;
      }

      // successful submit so try to add new call tag item
      if (deleteCalltagItem(delItem))
      {
        // add ok, return to main screen, inform user
        indicateInfo("Item " + delItem.getEquipmentKey() + " deleted",
          ctBean.DISPMODE_CT_EDIT);
        return;
      }
    
      // trouble...
      indicateError("Failed to delete item",ctBean.DISPMODE_CT_EDIT);
    }
  }
  
  /**
   * CMD_EDIT_TAG_ITEM
   *
   * Allows editting of an equipment item.  User can change the serial number,
   * model type or incoming/outgoing status.  Uses item bean and edit_item.jsp.
   * Upon receiving valid input the existing tag item is deleted from the
   * calltag and a new one with the new info is created and added.
   */
  private void editTagItem() throws Exception
  {
    // create helper bean, add to session
    CallTagItem tagItem =
      getItemWithRequestedId(cmds.CMD_EDIT_TAG_ITEM);
    ItemBean itemBean = new ItemBean(ctBean.getCallTag(),tagItem);
    ctsession.setHelperBean(itemBean);
    
    // process any request data
    itemBean.autoSetFields(request);

    // return without update?
    if (itemBean.getData(itemBean.FN_RETURN).length() > 0)
    {
      // send user back to main call tag screen without changing anything
      ctBean.setDisplayMode(ctBean.DISPMODE_CT_EDIT);
      doCommand("vbdm");
      return;
    }

    // non- or unsuccessful submit: forward to edit item page
    if (!itemBean.isAutoSubmitOk())
    {
      forwardPage.append(paths.PATH_EDIT_TAG_ITEM);
      extractErrorMessages(itemBean);
      return;
    }

    // delete old item, add new item
    // (in that order to avoid duplicate serial nums)
    EquipmentKey equipKey = itemBean.generateKey();
    int itemStatus = itemBean.getData(itemBean.FN_ITEM_TYPE).equals("in") 
      ? CallTagItem.STATUS_INCOMING : CallTagItem.STATUS_OUTGOING;
    String received = itemStatus == CallTagItem.STATUS_INCOMING 
      ? itemBean.getData(itemBean.FN_RECEIVED).toUpperCase() : "N";
    if (deleteCalltagItem(tagItem))
    {
      CallTagItem newItem = addCalltagItem(equipKey,itemStatus,received);
      if (newItem != null)
      {
        // need to update the bean's hidden 
        // item id with the newly added item id
        itemBean.setData(itemBean.FN_ITEM_ID,String.valueOf(newItem.getId()));
        
        // replacement ok, inform user of successful item "update"
        log.debug("Item replaced with new item " + equipKey);
        tmm.msg("Item " + equipKey + " updated");
        forwardPage.append(paths.PATH_EDIT_TAG_ITEM);
        return;
      }
    }
    
    // trouble...
    log.error("Error updating item " + equipKey);
    tmm.err("Error updating item");
    forwardPage.append(paths.PATH_EDIT_TAG_ITEM);
  }

  /**
   * CMD_EDIT_INV_STATUS
   *
   * Edit a call tag equipment item's inventory status.  Intantiates an
   * inventory status helper bean, forwards to an inventory status view
   * screen that allows the equipment to be added into inventory if not
   * currently existing, or to modify the existing inventory status.
   *
   * Parms wanted: itemId
   */
  private void editInventoryStatus() throws Exception
  {
    // fetch the call tag item to edit status of
    CallTagItem tagItem = getItemWithRequestedId(cmds.CMD_EDIT_INV_STATUS);
    if (tagItem != null)
    {
      // get a helper bean
      InventoryStatusBean invBean = new InventoryStatusBean(tagItem);

      // process request data
      invBean.autoSetFields(request);

      // did user cancel?
      if (invBean.getData(invBean.FN_CANCEL).length() > 0)
      {
        // send user back to main call tag screen without changing anything
        ctBean.setDisplayMode(ctBean.DISPMODE_CT_EDIT);
        doCommand("vbdm");
        return;
      }

      // is user not done?
      if (!invBean.isAutoSubmitOk())
      {
        // either just coming to status page or an error in submitted
        // data, either way need to send user to status page, displaying
        // any error messages that may be present...
        ctsession.setHelperBean(invBean);
        forwardPage.append(paths.PATH_INVENTORY_STATUS);
        extractErrorMessages(invBean);
        return;
      }

      // user has successfully submitted new inventory status info,
      // the helper bean has pushed the submitted info into the call tag
      // item so just need to have the data persisted into the database
      EquipmentData_EquipInv equipData
        = (EquipmentData_EquipInv)tagItem.getEquipmentData();
      EquipmentDAO equipDb
        = EquipmentDAOFactory.getInstance().getEquipmentDAO();
      if (!equipDb.persistEquipmentData(equipData))
      {
        tmm.err("Failed to update database with equipment info");
        return;
      }

      // forward user to main call tag view/edit screen
      log.debug("Inventory status updated");
      indicateInfo("Inventory status updated",ctBean.DISPMODE_CT_EDIT);
    }
  }
  
  /**
   * Adds a note to calltag note table with the given note text.  Uses the
   * user and calltag id of the calltag bean.
   */
  private void addNote(String noteText)
  {
    try
    {
      // create a note and fill in pertinent fields
      CallTag calltag = ctBean.getCallTag();
      UserBean user = ctBean.getUser();
      Note note = new Note();
      note.setCalltagId(calltag.getId());
      note.setAuthor(user.getLoginName());
      note.setText(noteText);
    
      // use note manager to add note to db
      NoteManager manager = new NoteManager();
      manager.addNote(note);
      
      log.debug("added " + note);
    }
    catch (Exception e)
    {
      log.error("error adding note: " + e);
      throw new RuntimeException("Add note failed: " + e);
    }
  }
  
  /**
   * CMD_ADD_NOTE
   *
   * Adds the user submitted note text as a note to the calltag note table.
   */
  private void addNote() throws Exception
  {
    // get note text, ignore empty note text requests
    ctBean.autoSetFields(request);
    String text = ctBean.getData("newNote");
    if (text == null || text.length() == 0)
    {
      ctBean.setDisplayMode(ctBean.DISPMODE_CT_EDIT);
      doCommand("vbdm");
      return;
    }
    
    try
    {
      addNote(text);
      
      // clear out the note after adding it
      ctBean.setData("newNote","");
      
      indicateInfo("Note added",ctBean.DISPMODE_CT_EDIT);
    }
    catch (Exception e)
    {
      indicateError("Error adding note",ctBean.DISPMODE_CT_EDIT);
    }
  }

  
  
  
  

  /**
   * doCommand()
   *
   * Executes the given command.
   * Sets the page to forward to.
   * Recursive.
   */
  private void doCommand(String command)
    throws Exception
  {
    if(command==null || command.length()<1)
      throw new ServletException("Unable to perform command: Empty command specified.");

    log.debug("doCommand("+command+") - START");




    // new cmds.CMD_* commands


    // add new calltag item
    if (command.equals(cmds.CMD_ADD_TAG_ITEM))
    {
      addTagItem();
    }
    // edit calltag item
    else if (command.equals(cmds.CMD_EDIT_TAG_ITEM))
    {
      editTagItem();
    }
    // delete calltag item
    else if (command.equals(cmds.CMD_DEL_TAG_ITEM))
    {
      deleteCalltagItem();
    }
    // edit calltag item inventory status
    else if (command.equals(cmds.CMD_EDIT_INV_STATUS))
    {
      editInventoryStatus();
    }
    // add new note
    else if (command.equals(cmds.CMD_ADD_NOTE))
    {
      addNote();
    }




    // old commands


    // View Call Tag detail
    else if(command.equals("vctd")) {

      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());

      forwardPage.append(RELPATH_PRESENTATION);
      forwardPage.append("ct_detail.jsp");

    // View Call Tag Item detail
    } else if(command.equals("vctid")) {

      forwardPage.append(RELPATH_PRESENTATION);
      forwardPage.append("cti_detail.jsp");

    // View Call Tag Notification detail
    } else if(command.equals("vctnd")) {

      forwardPage.append(RELPATH_PRESENTATION);
      forwardPage.append("ctn_detail.jsp");

    // View by Display Mode
    } else if(command.equals("vbdm")) {

      switch(ctBean.getDisplayMode()) {
        case CallTagDetailBean.DISPMODE_CT_ADD:
        case CallTagDetailBean.DISPMODE_CT_EDIT:
          doCommand("vctd");
          break;
        case CallTagDetailBean.DISPMODE_CTI_ADD:
        case CallTagDetailBean.DISPMODE_CTI_EDIT:
          doCommand("vctid");
          break;
        case CallTagDetailBean.DISPMODE_CTN_ADD:
        case CallTagDetailBean.DISPMODE_CTN_EDIT:
          doCommand("vctnd");
          break;
        default:
          doCommand("verr");
          break;
      }

    // View Add Call Tag
    } else if(command.equals("vact")) {

      log.debug("vact - START");

      CallTag ct = ctdao.createCallTag();

      log.debug("vact: setting new call tag...");
      ctBean.setCallTag(ct);
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CT_ADD);

      // grab the merchant number if one specified
      String mn = HttpHelper.getString(request,"mn","");
      if(mn.length()>0) {
        ct.setBillMerchant(new AccountKey(mn,true));
        //log.debug("vact: ct.getBillingAccount().isSet()? "+(ct.getBillingAccount().isSet()? "Yes":"No"));
      }

      // load supplemental merchant info from account lookup bean
      ctBean.loadAccountData(ct.getBillMerchant(),loginUser);

      ctBean.setFields();

      doCommand("vctd");
      return;

    // View Edit Call Tag
    } else if(command.equals("vect")) {

      log.debug("vect - START");

      //get rid of any old messages
      if(tmm!=null){
        tmm.clear();
      }

      String msg;

      long id=StringUtilities.stringToLong(request.getParameter("id"),-1L);

      // find by acr id?
      if(id < 0) {
        long acrid=StringUtilities.stringToLong(request.getParameter("acrid"),-1L);
        if(acrid > 0)
          id = ctdao.getCallTagIdFromAcrId(acrid);
      }

      if(id > 0 && ctBean.getCallTagId() != id) {
        CallTag ct = ctdao.findCallTag(id);
        if(ct == null) {
          msg = "Unable to find Call Tag of ID: '"+id+"'.";
          tmm.wrn(msg);
        } else {
          ctBean.setCallTag(ct);
        }
      }

      if(id < 0 || ctBean.getCallTagId() < 1) {
        log.error("Unable to view Call Tag detail: No Call Tag Id specified.");
        doCommand("verr");
        return;
      }
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CT_EDIT);

      // get the source of where request came from
      String source = HttpHelper.getString(request,"src","");
      boolean bFromQueueListingPage = (source.equals("ql"));
      //log.debug("bFromQueueListingPage="+bFromQueueListingPage);

      // bind the queue data bean to the ct detail bean
      //log.debug("bind the queue data bean to the ct detail bean...");
      QueueBase queueBean = null;
      if(bFromQueueListingPage && qsb!=null) {
        int type = HttpHelper.getInt(request,"type",0);
        //log.debug("vect: type="+type+", qsb==null? "+(qsb==null? "Yes":"No"));
        ctBean.setQueueBean(qsb.getQueueBean(type));
        ctBean.loadQueueItem();
      }
      if(ctBean.getQueueBean()==null) {
        log.warn("Creating new call tag queue bean...");
        queueBean = new CallTagQueue();
        queueBean.setType(QueueTools.getCurrentQueueType(ctBean.getCallTagId(),MesQueues.Q_ITEM_TYPE_CALLTAG));
        ctBean.setQueueBean(queueBean);
      }
      if(ctBean.isQueueBeanSet()) {
        //log.debug("vect: loading queue item...");
        ctBean.loadQueueItem();
      }

      ///////

      // load supplemental merchant info from account lookup bean
      //log.debug("load supplemental merchant info from account lookup bean...");
      ctBean.loadAccountData(ctBean.getCallTag().getBillMerchant(),loginUser);

      //log.debug("vect: Push data to fields...");
      ctBean.setFields();

      //log.debug("vect: view detail...");
      doCommand("vctd");
      return;

    // View Add Call Tag Item
    } else if(command.equals("vacti")) {

      // require the parent call tag has a valid parent (source) first
      if(ctBean.getCallTag().getParentKey()==null) {
        tmm.wrn("A Call Tag Source must be specified before items may be added.");
        doCommand("vbdm");
        return;
      }

      ctBean.setCurrentItemToNew();
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CTI_ADD);
      ctBean.setEquipDisplayMode();
      ctBean.setFields();

      doCommand("vbdm");
      return;

    // View Edit Call Tag Item
    } else if(command.equals("vecti")) {

      long id=StringUtilities.stringToLong(request.getParameter("id"),-1L);

      if(id < 1) {
        doCommand("vbdm");
        return;
      }

      ctBean.setCurrentItem(id);
      ctBean.setEquipDisplayMode();
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CTI_EDIT);
      ctBean.setFields();

      doCommand("vctid");
      return;

    // View Add Call Tag Notification
    } else if(command.equals("vactn")) {

      // require the parent call tag has a valid parent (source) first
      if(ctBean.getCallTag().getParentKey()==null) {
        tmm.wrn("A Call Tag Source must be specified before notifications may be added.");
        doCommand("vbdm");
        return;
      }

      ctBean.setCurrentNotificationToNew();
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CTN_ADD);
      ctBean.setFields();

      doCommand("vctnd");
      return;

    // View Edit Call Tag Notification
    } else if(command.equals("vectn")) {

      long id=StringUtilities.stringToLong(request.getParameter("id"),-1L);

      if(id < 1) {
        doCommand("vbdm");
        return;
      }

      ctBean.setCurrentNotification(id);
      ctBean.setDisplayMode(CallTagDetailBean.DISPMODE_CTN_EDIT);
      ctBean.setFields();

      doCommand("vctnd");
      return;

    // Add Call Tag
    } else if(command.equals("act")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctd");
        return;
      }

      String msg;
      CallTag ct = ctBean.getCallTag();

      if(!ctdao.persistCallTag(ct)) {
        msg = "Add Call Tag of ID '"+ct.getId()+"' FAILED.";
        log.error(msg);
        tmm.err(msg);
      } else {
        msg = "Call Tag of ID '"+ct.getId()+"' added.";
        log.info(msg);
        tmm.msg(msg);
      }

      // add item to the queuing system
      String s;
      if(!ctBean.queueInitiate()) {
        s = "Unable to initiate Call Tag '"+ct.getId()+"' into the startpoint queue(s).";
        tmm.err(s);
        log.error(s);
      } else {
        s = "Call Tag '"+ct.getId()+"' inserted into the startpoint queue.";
        log.info(s);
      }

      // view added call tag via the QueueRouter
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      forwardPage.append(RELPATH_QUEUEROUTER);
      forwardPage.append("&type=");
      forwardPage.append(ctBean.getResidentQueue());
      forwardPage.append("&id=");
      forwardPage.append(ctBean.getCallTagId());
      queryString.setLength(0);
        // kill the query string since redirecting to another servlet

      return;

    // Update Call Tag
    } else if(command.equals("uct")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctd");
        return;
      }

      String msg;
      CallTag ct = ctBean.getCallTag();

      if(!ctdao.persistCallTag(ct)) {
        msg = "Update Call Tag of ID '"+ct.getId()+"' FAILED.";
        log.error(msg);
        tmm.err(msg);
      } else {
        msg = "Call Tag of ID '"+ct.getId()+"' updated.";
        log.info(msg);
        tmm.msg(msg);
      }

      doCommand("vbdm");
      return;

    // Add Call Tag Item
    } else if(command.equals("acti")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctid");
        return;
      }

      String msg;
      CallTagItem cti = ctBean.getCurrentCallTagItem();
      cti.setCallTagId(ctBean.getCallTagId());
      boolean bOk = false;

      if(ctBean.isCurrentCallTagPersisted()) {
        //log.debug("acti - persisting...");
        bOk = ctdao.persistCallTagItem(cti);
        if(!bOk) {
          msg = "Add Call Tag Item of ID '"+cti.getId()+"' FAILED.";
          log.error(msg);
          tmm.err(msg);
        }
      } else
        bOk=true;

      if(bOk) {
        // add newly created item to parent call tag
        ctBean.getCallTag().addItem(cti);
        msg = "Item of ID '"+cti.getId()+"' added to Call Tag.";
        log.info(msg);
        tmm.msg(msg);
      }

      // revert view
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      doCommand("vbdm");
      return;

    // Update Call Tag Item
    } else if(command.equals("ucti")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctid");
        return;
      }

      String msg;
      CallTagItem cti = ctBean.getCurrentCallTagItem();

      boolean bOk = false;
      if(ctBean.isCurrentCallTagPersisted()) {
        bOk = ctdao.persistCallTagItem(cti);
        if(!bOk) {
          msg = "Update Call Tag Item of ID '"+cti.getId()+"' FAILED.";
          log.error(msg);
          tmm.err(msg);
        }
      } else
        bOk = true;
      if(bOk) {
        msg = "Call Tag Item '"+cti.getId()+"' updated.";
        log.info(msg);
        tmm.msg(msg);
      }

      // maintain view
      doCommand("vbdm");
      return;

    // Delete Call Tag Item
    } else if(command.equals("dcti")) {

      long ctiid = HttpHelper.getLong(request,"ctiid");

      if(ctiid < 0L) {
        tmm.err("Unable to delete Call Tag Item: Invalid Call Tag Item Id specified.");
        doCommand("vbdm");
        return;
      }

      CallTagItem cti = ctBean.setCurrentItem(ctiid);
      String msg;

      if(ctBean.isCurrentCallTagPersisted())
        ctdao.deleteCallTagItem(cti);

      ctBean.getCallTag().removeItem(cti);
      msg = "Call Tag Item '"+cti.getId()+"' deleted.";
      log.info(msg);
      tmm.msg(msg);

      // revert view
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      doCommand("vbdm");
      return;

    // Add Call Tag Notification
    } else if(command.equals("actn")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctnd");
        return;
      }

      String msg;
      CallTagNotification ctn = ctBean.getCurrentCallTagNotification();
      ctn.setCallTagId(ctBean.getCallTagId());
      boolean bOk = false;

      if(ctBean.isCurrentCallTagPersisted()) {
        bOk = ctdao.persistCallTagNotification(ctn);
        if(!bOk) {
          msg = "Add Call Tag Notification of ID '"+ctn.getId()+"' FAILED.";
          log.error(msg);
          tmm.err(msg);
        }
      } else
        bOk=true;

      if(bOk) {
        // add newly created item to parent call tag
        ctBean.getCallTag().addNotification(ctn);
        msg = "Notification of ID '"+ctn.getId()+"' added to Call Tag.";
        log.info(msg);
        tmm.msg(msg);
      }

      // revert view
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      doCommand("vbdm");
      return;

    // Update Call Tag Notification
    } else if(command.equals("uctn")) {

      ctBean.setFields(request);

      if(!ctBean.isValid()) {
        ctBean.setFields();
        doCommand("vctnd");
        return;
      }

      String msg;
      CallTagNotification ctn = ctBean.getCurrentCallTagNotification();

      boolean bOk = false;
      if(ctBean.isCurrentCallTagPersisted()) {
        bOk = ctdao.persistCallTagNotification(ctn);
        if(!bOk) {
          msg = "Update Call Tag Notification of ID '"+ctn.getId()+"' FAILED.";
          log.error(msg);
          tmm.err(msg);
        }
      } else
        bOk = true;
      if(bOk) {
        msg = "Call Tag Notification '"+ctn.getId()+"' updated.";
        log.info(msg);
        tmm.msg(msg);
      }

      // revert view
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      doCommand("vbdm");
      return;

    // Delete Call Tag Notification
    } else if(command.equals("dctn")) {

      long ctnid = HttpHelper.getLong(request,"ctnid");

      if(ctnid < 0L) {
        tmm.err("Unable to delete Call Tag Notification: Invalid Call Tag Notification Id specified.");
        doCommand("vbdm");
        return;
      }

      CallTagNotification ctn = ctBean.setCurrentNotification(ctnid);
      String msg;

      if(ctBean.isCurrentCallTagPersisted())
        ctdao.deleteCallTagNotification(ctn);

      ctBean.getCallTag().removeNotification(ctn);
      msg = "Call Tag Notification '"+ctn.getId()+"' deleted.";
      log.info(msg);
      tmm.msg(msg);

      // revert view
      ctBean.setDisplayMode(ctBean.getPersistCallTagDisplayMode());
      doCommand("vbdm");
      return;

    // View Call Tag Search
    } else if(command.equals("vcts")) {

      forwardPage.append(RELPATH_CALLTAGLOOKUP);
      this.queryString.setLength(0);

    // Do queue op
    } else if(command.equals("dqo")) {

      /*
       *            --------------
       *            Expected input
       *            --------------
       *            qo              Queue op (int).  (FORMAT: "{src q}:{dst q}"
       *            on              Op Notes
       */

      boolean rval=false;

      // get the queue op
      String qo = request.getParameter("qo");
      //log.debug("qo="+qo);
      if(qo!=null && qo.length()>0) {
        String opNotes = request.getParameter("on");
        //log.debug("opNotes="+opNotes);
        int sq=MesQueues.Q_NONE,dq=MesQueues.Q_NONE;
        StringTokenizer st = new StringTokenizer(qo,":");
        if(st.hasMoreElements())
          sq=StringUtilities.stringToInt(st.nextToken(),MesQueues.Q_NONE);
        if(st.hasMoreElements())
          dq=StringUtilities.stringToInt(st.nextToken(),MesQueues.Q_NONE);

        // do the queue op
        rval=ctBean.doQueueOp(sq,dq,opNotes,ctBean.getQueueIsRush());
      }

      if(rval) {
        // go to next item in same queue if exists
        if(ctBean.hasNextRow()) {
          ctBean.incrCrntRow();
          tmm.msg("Iterated to next item in queue.");
          forwardPage.append(RELPATH_QUEUEROUTER);
          forwardPage.append("&type=");
          forwardPage.append(ctBean.getResidentQueue());
          forwardPage.append("&id=");
          forwardPage.append(ctBean.getCrntRowID());
          queryString.setLength(0);
            // kill the query string since redirecting to another servlet
        } else {
          // else re-route to the originating queue if a queue type was specified
          tmm.clear();  // since moving out of this servlet
          forwardPage.append(RELPATH_QUEUELISTING);
          forwardPage.append(ctBean.getResidentQueue());
          queryString.setLength(0);
            // kill the query string since redirecting to another servlet
        }
      } else {
        doCommand("vbdm");
      }
      return;

    // View Call Tag Queues
    } else if(command.equals("vctq")) {

      // IMPT: route here to go to call tag queue menu to ensure Call Tag session loaded

      forwardPage.append(RELPATH_PRESENTATION);
      forwardPage.append("calltagmenu.jsp?com.mes.CallTagMenuId=");
      forwardPage.append(MesMenus.MENU_ID_CALLTAG_QUEUES);
      queryString.setLength(0); // kill the query string

    // [re-]Set Call Tag Parent
    } else if(command.equals("sctp")) {

      ctBean.setFields(request);

      EquipmentActionKey key;
      try {
        key = KeyBuilder.getInstance().buildEquipmentActionKey(request);
      }
      catch(Exception e) {
        String s = "Unable to set Call Tag Source: Invalid lookup parameters specified.";
        tmm.err(s);
        log.error(s);
        doCommand("vbdm");
        return;
      }

      if(key.exists()) {
        ctBean.getCallTag().setParentKey(key);
        tmm.msg("Source set.");
      } else {
        //log.debug("sctp: key.exists()=false");
        tmm.err("Specified Source of key: "+key.toString()+" of given type does not exist.");
      }

      ctBean.setFields();

      //log.debug("sctp: deferring to vctd...");
      doCommand("vctd");
      return;

    // View Call Tag parent
    } else if(command.equals("vctp")) {

      // expected input
      //  - parent_type
      //  - [parent key]

      int parent_type = StringUtilities.stringToInt(request.getParameter("parent_type"),-1);

      EquipmentActionKey key;
      try {
        key = KeyBuilder.getInstance().buildEquipmentActionKey(request);
      }
      catch(Exception e) {
        log.error("doCommand() vctp: Unable to build equipment action key.");
        doCommand("vbdm");
        return;
      }

      switch(parent_type) {

        // ACR
        case mesConstants.ACTION_CODE_ACR:
          forwardPage.append("/srv/acr?method=acrd&id=");
          forwardPage.append(key.getId());
          break;

        // Call Tag
        case mesConstants.ACTION_CODE_CALLTAG:
          forwardPage.append("/srv/calltag?method=vect&id=");
          forwardPage.append(key.getId());
          break;

        // deployment
        case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
          // route to deployment report
          forwardPage.append("/jsp/inventory/equipmentdeployment.jsp?lookupValue=");
          forwardPage.append(key.getSerialNum());
          forwardPage.append("&fromYear=2000&fromDay=1&fromMonth=1");
          break;

        // swap & exchange
        case mesConstants.ACTION_CODE_SWAP:
        case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
        case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
        case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
          forwardPage.append("/jsp/inventory/merchantswap.jsp?swapnum=");
          forwardPage.append(key.getId());
          break;

        // repair
        case mesConstants.ACTION_CODE_REPAIR:
          forwardPage.append("/jsp/inventory/equiprepair.jsp?repairnum=");
          forwardPage.append(key.getId());
          break;

        default:
          log.error("View Call Tag Source failed: Unrecognized Call Tag parent type '"+parent_type+"'.");
          doCommand("vbdm");
          return;
      }

      queryString.setLength(0); // kill the query string

    // View Source Userid
    } else if(command.equals("vsrc")) {

      String uname=null;

      final String ln = HttpHelper.getString(request,"ln");
      final boolean editable = HttpHelper.getBoolean(request,"editable",false);

      CallTag ct = ctBean.getCallTag();

      // release lock on status item if one exists
      if(ct!=null)
        QueueTools.releaseLock(ct.getId(),CallTagDetailQueueBean.QUEUE_ITEM_TYPE);

      if(ln==null || ln.length()<1) {
        tmm.err("View user profile failed: User login name not specified.");
        log.error("View user profile failed: User login name not specified.");
        doCommand("vbdm");
        return;
      }

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_USERPROFILE);
      forwardPage.append(ln);
      queryString.setLength(0);

    // [re-]Set Call Tag Item Equipment
    } else if(command.equals("sctie")) {

      //ctBean.setFields(request);

      EquipmentKey key;
      try {
        key = KeyBuilder.getInstance().buildEquipmentKey(request);
      }
      catch(Exception e) {
        String s = "Unable to set Call Tag Item Equipment: Invalid lookup parameters specified.";
        tmm.err(s);
        log.error(s);
        doCommand("vctid");
        return;
      }

      boolean bOk = false;
      try {
        if(key.exists()) {
          ctBean.getCurrentCallTagItem().setEquipmentKey(key);
          bOk = true;
          tmm.msg("Equipment set.");

          if(ctBean.getDisplayMode() == CallTagDetailBean.DISPMODE_CTI_ADD)
            tmm.wrn("'Add Call Tag Item' button must be pressed to make this equipment selection persist.");
          else if(ctBean.getDisplayMode() == CallTagDetailBean.DISPMODE_CTI_ADD)
            tmm.wrn("'Update Call Tag Item' button must be pressed to make this equipment selection persist.");
        }
      }
      catch(Key.NonExistantKeyException neke) {}
      if(!bOk) {
        tmm.err("No equipment found for key: "+key.toString());
      }

      ctBean.setFields();

      doCommand("vctid");
      return;

    // View Merchant
    } else if(command.equals("vm")) {

      String mn=request.getParameter("mn");

      if(mn==null || mn.length() < 1) {
        tmm.err("Unable to view merchant: No merchant number specified.");
        doCommand("vbdm");
        return;
      }

      // release lock on status acr if one exists
      QueueTools.releaseLock(ctBean.getCallTagId(),CallTagDetailQueueBean.QUEUE_ITEM_TYPE);

      // redirect to outside world touchpoint
      forwardPage.append(RELPATH_ACCOUNT);
      forwardPage.append(mn);
      queryString.setLength(0);

    // View Equipment Detail
    } else if(command.equals("ved")) {

      // get the equipment key
      EquipmentKey ek = null;
      try {
        ek = KeyBuilder.getInstance().buildEquipmentKey(request);
      }
      catch(Exception e) {
        log.error("View Equipment Detail: Unable to build equipment key from request.  Aborted.");
      }

      // redirect to outside world touchpoint
      forwardPage.append("/jsp/maintenance/equipinfodtl.jsp?");
      forwardPage.append(ek.toQueryString());
      queryString.setLength(0);

    // aeta/ueta
    } else if(command.equals("aeta") || command.equals("ueta")) {

      ctBean.setFields(request);

      EquipmentData_EquipInv ei = null, eiCopy = null;
      boolean bOk = false;
      try {

        ei      = (EquipmentData_EquipInv)ctBean.getCurrentCallTagItem().getEquipmentData();
        eiCopy  = (EquipmentData_EquipInv)ei.getCopy(); // for reversion upon error

        EquipmentHistory eh = EquipmentDataBuilder.getInstance().buildEquipmentHistory(ei);
        eh.setUserId(loginUser.getUserId());
        eh.setUser(loginUser.getLoginName());
        eh.setNote("Inserted from Call Tag ID#: "+ctBean.getCallTag().getId()+".");

        int nedm = CallTagDetailBean.EQUIPMODE_NONE;

        // assign
        if(command.equals("aeta")) {
          // copy the merchant number over to equip inventory
          ei.setMerchantNumber(Long.toString(ctBean.getCallTag().getMerchNumber()));
          ei.setEiDbaName(ctBean.getMerchantInfo().getDbaName());
          eh.setActionPerformed(ei.getStatusName() + " to merchant #:" + ei.getMerchantNumber());

          nedm = CallTagDetailBean.EQUIPMODE_UNASSIGN;
        }

        // unassign
        else if(command.equals("ueta")) {
          eh.setActionPerformed("Returned from merchant #:" + ei.getMerchantNumber());
          // clear out the merchant number
          ei.setMerchantNumber("");
          ei.setEiDbaName("");
          // clear out the deployed date
          ei.setDeployedDate(null);

          nedm = CallTagDetailBean.EQUIPMODE_ASSIGN;
        }

        // validate
        //log.debug("u/aeta: validating...");
        if(!ctBean.isValid()) {
          throw new Exception();
        }

        // persist
        //log.debug("u/aeta: persisting...");
        if(!EquipmentDAOFactory.getInstance().getEquipmentDAO().persistEquipmentData(ei)) {
          throw new Exception("Unable to persist equipment (un)assignment.");
        }

        // add equip history entry
        //log.debug("u/aeta: add equip history entry...");
        if(!EquipmentDAOFactory.getInstance().getEquipmentDAO().addEquipmentHistoryRecord(eh)) {
          throw new Exception("Equipment history record not created:  An error occurred attempting to save it.");
        }

        //log.debug("u/aeta: bOk is true");
        bOk = true;

        if(command.equals("aeta"))
          tmm.msg("Equipment assigned.");
        else if(command.equals("ueta"))
          tmm.msg("Equipment unassigned.");

      }
      catch(Exception e) {
        log.error("aeta/ueta: EXCEPTION: "+e.toString());
        tmm.err(e.getMessage());
      }

      String nextStep="ucti";

      // revert to equip data copy if error occurred
      if(!bOk) {
        //log.debug("u/aeta: Reverting equipment data");
        ctBean.getCurrentCallTagItem().setEquipmentData(eiCopy);
        nextStep="vbdm";
      }

      ctBean.setEquipDisplayMode();
      ctBean.setFields();

      //log.debug("u/aeta: back to cti detail...");
      doCommand(nextStep);

    // Add equipment to MeS Inventory
    } else if(command.equals("aeti")) {

      // set field data and validate
      log.debug("aeti: validating field data...");
      ctBean.setFields(request);
      boolean isValid = ctBean.isValid();

      // invalid fields, refresh view
      if (!isValid)
      {
        tmm.err("Invalid data");
        log.debug("aeti: field data is invalid");
        ctBean.setFields();
        doCommand("vbdm");
        return;
      }
      log.debug("aeti: fields are valid");

      // persist any changes to the call tag item
      log.debug("aeti: persisting call tag item...");
      CallTagItem tagItem = ctBean.getCurrentCallTagItem();
      boolean tagPersistOk = ctdao.persistCallTagItem(tagItem);

      // tag item persist failed, refresh view
      if (!tagPersistOk)
      {
        tmm.err("Error saving call tag item data");
        log.error("aeti: tag item persist error");
        ctBean.setFields();
        doCommand("vbdm");
        return;
      }
      log.debug("aeti: tag item persisted ok");



      // DEBUG: check the status of equipment key in the equip data in the tag item
      EquipmentKey equipKey = tagItem.getEquipmentData().getKey();
      log.debug("aeti: tagItem equipData equipKey = '" + equipKey + "'");
      equipKey = tagItem.getEquipmentKey();
      log.debug("aeti: tagItem equipKey = '" + equipKey + "'");



      // persist equip data to equip_inventory
      log.debug("aeti: persisting equip data...");
      try
      {
        // get equipment data from the tag item
        EquipmentData_EquipInv equipData
          = (EquipmentData_EquipInv)tagItem.getEquipmentData();
        log.debug("aeti: equip data was " + (equipData == null ? "NOT " : "")
          + "found");

        // save it in equip_inventory
        EquipmentDAO equipDao
          = EquipmentDAOFactory.getInstance().getEquipmentDAO();
        if (!equipDao.persistEquipmentData(equipData))
        {
          throw new Exception("Persist call to equipment DAO failed");
        }
      }
      catch (Exception e)
      {
        tmm.err("Error adding to inventory: " + e);
        log.error("aeti: error persisting equipment - " + e);
        ctBean.setFields();
        doCommand("vbdm");
        return;
      }
      log.debug("aeti: equip data persisted ok");

      // persists are ok, switch to equip assign mode and refresh view
      log.debug("aeti: setting display mode to equip assign...");
      ctBean.setEquipDisplayMode(CallTagDetailBean.EQUIPMODE_ASSIGN);
      tmm.msg("Equipment added to inventory.");
      //ctBean.setFields();
      doCommand("vbdm");

    }
    // view error
    else if(command.equals("verr"))
    {
      forwardPage.append(RELPATH_PRESENTATION);
      forwardPage.append("error.jsp");
    }
    else
    {
      log.error("Unhandled command '"+command+"'.  No ops performed.");
    }
  }

  private boolean userPermissionsCheck(String command)
  {
    boolean rval = true;  // default to allow op

    /*
    // calltag system access?
    if(!loginUser.hasRight(MesUsers.RIGHT_CALLTAG_SYSTEM))
      rval=false;
    // submit related
    else if(command.equals("vetd") || command.equals("vctd"))
      rval=loginUser.hasRight(MesUsers.RIGHT_CALLTAG_TODO);
    */

    if(!rval) {
      forwardPage.append(RELURL_INSUFACCESS);
      queryString.setLength(0);
        // kill the query string since redirecting to another servlet
      log.warn("User "+ctBean.getUserLoginName()+" has insufficient rights for operation command '"+command+"'. Operation aborted.");
    }

    return rval;
  }

  private synchronized void processRequest(HttpServletRequest request,HttpServletResponse response)
  {
    try {

      log.debug("processRequest() - START");

      // reset
      this.request=request;
      this.response=response;
      forwardPage.setLength(0);
      forwardPage.append(HttpHelper.getServerURL(request));
      queryString.setLength(0);

      // get ref to the current session
      if((session = request.getSession(false))==null) {
        /*
        log.debug("Initializing HTTP session...");
        if((session = request.getSession(true))==null)
          throw new ServletException("Unable to obtain a reference to the HTTP session.");
        */
        // redirect to logon...
        log.info("Session new or expired - redirecting to login page...");
        response.sendRedirect(RELPATH_SESSIONEXPIRED);
        return;
      }

      // get ref. to the UserLogin bean for user related session info
      if((loginUser=(UserBean)session.getAttribute("UserLogin"))==null) {
        log.error("Unable to obtain reference to the login user.");
        throw new ServletException("Unable to obtain reference to the login user.");
      }

      // get the ref. to the session-scope TextMessageManager
      if((tmm=(HttpTextMessageManager)session.getAttribute("HttpTextMessageManager"))==null) {
        tmm = new HttpTextMessageManager();
        session.setAttribute("HttpTextMessageManager",tmm);
        //log.debug("HttpTextMessageManager session attribute set.");
      }
      tmm.setHttpRequest(request);

      // acquire the call tag session object
      if((ctsession=(CallTagSession)session.getAttribute("CallTagSession"))==null) {
        log.info("Initializing CallTag session...");
        ctsession = new CallTagSession();
        if(ctsession==null)
          throw new ServletException("Unable to instantiate CallTagSession object.");

        ctsession.setTextMessageManager(tmm);
        ctsession.setUser(loginUser);

        // add CallTagSession instance to session
        session.setAttribute("CallTagSession",ctsession);
        log.debug("CallTagSession object session attribute set.");
      }

      // [re-]set ref to queue session bean
      qsb=(QueueSessionBean)session.getAttribute("queueSession");

      // [re-]set ref to call tag detail queue bean
      if((ctBean = ctsession.getCallTagDetailQueueBean())==null) {
        throw new ServletException("Unable to obtain non-null Call Tag Bean reference (probably due to session expiration).");
      }

      // get command
      String command=request.getParameter("method");
      
      log.debug("command = '" + command + "'");

      // assign default command?
      if(command==null) {
        log.debug("Setting default command to 'vact'.");
        command="vact";
      } else {
        // translate command?
        if(command.equals("Add Call Tag"))
          command="act";
        else if(command.equals("Update Call Tag"))
          command="uct";
        else if(command.equals("Add Call Tag Item"))
          command="acti";
        else if(command.equals("Update Call Tag Item"))
          command="ucti";
        else if(command.equals("Delete Call Tag Item"))
          command="dcti";
        else if(command.equals("Add Call Tag Notification"))
          command="actn";
        else if(command.equals("Update Call Tag Notification"))
          command="uctn";
        else if(command.equals("Delete Call Tag Notification") ||
                command.equals(cmds.CMD_DEL_NOTIFICATION) )
          command="dctn";
        else if(command.equals("Set Equipment"))
          command="sctie";
        else if(command.equals("Set Source"))
          command="sctp";
        else if(command.indexOf("Assign Equipment") >= 0)
          command="aeta";
        else if(command.indexOf("Unassign Equipment") >= 0)
          command="ueta";
        else if(command.indexOf("Add to MeS Inventory") >= 0)
          command="aeti";
        else if(command.equals("Add Note"))
        {
          command = cmds.CMD_ADD_NOTE;
        }
      }

      // retain query string
      String qs = request.getQueryString();
      if(qs!=null && qs.length()>0 && !qs.equals("null"))
        queryString.append(request.getQueryString());
      log.debug("queryString="+queryString);

      // user permissions check
      if(userPermissionsCheck(command)) {
        // *** do command ***
        try {
          doCommand(command);
        }
        catch(Exception e) {
          log.error("Do command Exception: "+e.getMessage()+" (Command: "+command+").");
        }
      }

      log.debug("Setting No cache in the header...");
      response.setHeader("Cache-Control","no-store");
      response.setHeader("Pragma","no-cache");
      response.setDateHeader("Expires",0);

      // send the browser to the new location
      log.debug("send the browser to the new location...");
      // IMPT: append the query string to tell the browser to force a refresh of the page!
      if(queryString.length()>0) {
        forwardPage.append("?");
        forwardPage.append(queryString);
      }

      log.debug("Redirecting to page '"+forwardPage.toString()+"'...");
      response.sendRedirect(forwardPage.toString());
      //RequestDispatcher disp = getServletConfig().getServletContext().getRequestDispatcher(forwardPage.toString());
      //disp.forward(request, response);

    }
    catch(Exception e) {
      log.error("processRequest() EXCEPTION: '"+e.toString()+"'.");
      e.printStackTrace();
    }
  }

  protected void doServlet( HttpServletRequest request, HttpServletResponse response)
  {
    processRequest(request,response);
  }

  public String getSignature()
  {
    return "Merchant e-Solutions - CallTag - Controller";
  }

} // CallTagServlet