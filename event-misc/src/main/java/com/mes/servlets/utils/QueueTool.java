/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/QueueTool.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-23 16:33:00 -0700 (Thu, 23 Aug 2007) $
  Version            : $Revision: 14057 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.MesQueues;
import com.mes.ops.EquipProfileDataBean;
import com.mes.ops.QueueConstants;
import com.mes.queues.ProgrammingQueue;
import com.mes.queues.QueueTools;
import com.mes.setup.AppUtils;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.tmg.TmgDb;


public class QueueTool extends MesServletBase
{
  protected void doServlet( HttpServletRequest request, HttpServletResponse response) 
  {
    long    queueId       = 0;
    int     queueType     = 0;
    int     queueAction   = 0;
    int     queueDest     = 0;
    String  queueUser     = "";
    String  destination   = "";
    
    try
    {
      // get queueType, queueId, and queueAction
      try
      {
        queueType     = Integer.parseInt(request.getParameter("type"));
        queueId       = Long.parseLong(request.getParameter("id"));
        queueAction   = Integer.parseInt(request.getParameter("action"));
        queueDest     = HttpHelper.getInt(request, "dest", 0);
        queueUser     = HttpHelper.getString(request, "user", "SYSTEM");

        // take action
        switch(queueAction)
        {
          case MesQueues.Q_ACTION_UNLOCK:
            // unlock this item
            QueueTools.releaseLock(queueId, queueType);
            destination = "/jsp/queues/queue.jsp?type=" + queueType;
            break;

          case MesQueues.Q_ACTION_MOVE:
            QueueTools.moveQueueItem(queueId, queueType, queueDest, getUserLogin(), "");
            destination = HttpHelper.getString(request, "destination", "/jsp/queues/queue.jsp?type=" + queueType);
            
            // programming queue completed specific ops
            if(ProgrammingQueue.isCompletedProgrammingQueue(queueDest)) {
              // timestamp the profile completion date
              EquipProfileDataBean.markProfileCompleted(queueId);
              //sets the mms completed timestamp in the old app_tracking table..
              AppUtils.setCompletedTimeStamp(HttpHelper.getLong(request, "primaryKey"), QueueConstants.DEPARTMENT_MMS, queueDest);
            }

            // HACK: look for items move into deployment complete queue
            // and have them generate an ola order in the new inventory
            // order system
            if (queueDest == MesQueues.Q_DEPLOYMENT_COMPLETED)
            {
              TmgDb.makeOlaOrder(queueId,getUserLogin());
            }
            
            break;
          
          default:
            destination = "/jsp/secure/RouteErr.jsp";
            break;
        }
        
        response.sendRedirect(HttpHelper.getServerURL(request) + destination);
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::getting data", e.toString());
      }
      
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doServlet()", e.toString());
    }
  }
}
