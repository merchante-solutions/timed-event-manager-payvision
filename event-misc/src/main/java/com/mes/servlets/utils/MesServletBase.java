/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/MesServletBase.java $

  Description:

  PdfStatementServlet

  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.servlets.utils;

import java.io.InputStream;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.mes.support.PropertiesFile;
import com.mes.support.SyncLog;
import com.mes.user.MESB2FUser;
import com.mes.user.UserBean;

public class MesServletBase extends HttpServlet
{
  static Logger log = Logger.getLogger(MesServletBase.class);

  protected HttpSession         session     = null;
  protected HttpServletRequest  requestObj  = null;
  protected UserBean            userLogin   = null;
  protected MESB2FUser          b2fUser     = null;

  private   MesServletBase  trueServlet = null;

  protected String buildURL(HttpServletRequest request, String baseURL)
  {
    PropertiesFile  propFile  = null;
    
    try
    {
      propFile = new PropertiesFile("mes.properties");
    }
    catch(Exception e)
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new PropertiesFile( inputStream );
      
      try { inputStream.close(); } catch(Exception re) {}
    }

    StringBuffer newURL = new StringBuffer("");

    // assign the protocol
    if(propFile.getBoolean("com.mes.development", false))
    {
      newURL.append("http://");
    }
    else
    {
      newURL.append("https://");
    }

    // add the host name
    newURL.append(request.getHeader("HOST"));

    // add the base URL
    newURL.append(baseURL);

    return newURL.toString();
  }

  protected void getSession(HttpServletRequest request)
  {
    this.session = request.getSession();
  }
  
  protected void setRequestObj(HttpServletRequest request)
  {
    this.requestObj = request;
  }

  protected void getBackToWhereYouOnceBelonged(HttpServletRequest request,
                                               HttpServletResponse response)
  {
    try
    {
      String backURL = request.getHeader("Referer");

      if(backURL == null)
      {
        backURL = buildURL(request, "/jsp/secure/navigate.jsp");
      }

      response.sendRedirect(backURL);
    }
    catch(Exception e)
    {
      forward("/jsp/secure/navigate.jsp", request, response);
    }
  }

  protected void forward( String relativeURL,
                          HttpServletRequest request,
                          HttpServletResponse response)
  {
    try
    {
      RequestDispatcher disp    = null;

      disp = trueServlet.getServletConfig().getServletContext()
        .getRequestDispatcher(relativeURL);

      disp.forward(request, response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::forward()", e.toString());
    }
  }

  protected void include (String URL, HttpServletRequest request, HttpServletResponse response)
  {
    try
    {
      RequestDispatcher disp = null;

      disp = trueServlet.getServletConfig().getServletContext()
        .getRequestDispatcher(URL);

      disp.include(request, response);
    }
    catch(Exception e)
    {
      System.out.println("include failed: " + e.toString());
    }
  }

  protected UserBean getUserLogin()
  {
    try
    {
      if(session != null)
      {
        userLogin = (UserBean)session.getAttribute("UserLogin");
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::getUserLogin()", e.toString());
    }

    return userLogin;
  }
  
  protected MESB2FUser getB2fUser()
  {
    try
    {
      if( session != null )
      {
        b2fUser = (MESB2FUser)session.getAttribute("b2fUser");
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::getB2fUser()", e.toString());
    }
    
    return( b2fUser );
  }

  protected void logOut( )
  {
    try
    {
      getUserLogin().invalidate();
      getB2fUser().invalidate();
    
      Enumeration sessionBeans = session.getAttributeNames();
      while(sessionBeans.hasMoreElements())
      {
        String sessionBean = (String)sessionBeans.nextElement();
        session.removeAttribute(sessionBean);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::logOut()", e.toString());
    }
  }
  
  protected boolean forceLogin(String loginName)
  {
    boolean result = false;
    try
    {
      UserBean ub = null;

      ub = (UserBean)session.getAttribute("UserLogin");
      if(ub == null)
      {
        synchronized(session)
        {
          ub = (UserBean)session.getAttribute("UserLogin");
          if(ub == null)
          {
            ub = new UserBean();
          }
        }
      }
      
      MESB2FUser b2fu = null;
      
      b2fu = (MESB2FUser)session.getAttribute("b2fUser");
      if(b2fu == null)
      {
        synchronized(session)
        {
          b2fu = (MESB2FUser)session.getAttribute("b2fUser");
          if(b2fu == null)
          {
            b2fu = new MESB2FUser();
          }
        }
      }

      // force login
      synchronized(session)
      {
        ub.invalidate();
        result = ub.forceValidate(loginName, requestObj);

        // add to session
        if(result)
        {
          session.setAttribute("UserLogin", ub);
        }
        
        b2fu.setCurrentState(MESB2FUser.USER_STATE_AUTHENTICATED);
        b2fu.setLoginName(loginName);
        
        session.setAttribute("b2fUser", b2fu);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::forceLogin(" + loginName + ")", e.toString());
    }

    return result;
  }

  private void logAccess(HttpServletRequest request)
  {
    try
    {
      UserBean u = getUserLogin();
      
      if( null == u )
      {
        u = new UserBean();
        u.forceValidate("system");
        request.getSession().setAttribute("UserLogin",u);
      }
      
      if(u != null)
      {
        u.logAccessDetails(request);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::logAccess()", e.toString());
    }
  }

  private void setTrueServlet(MesServletBase trueServlet)
  {
    this.trueServlet = trueServlet;
  }

  protected String getParam(HttpServletRequest request, String paramName)
  {
    return getParam(request, paramName, "");
  }

  protected String getParam(HttpServletRequest request, String paramName, String defaultVal )
  {
    String paramVal = request.getParameter(paramName);
    if(paramVal == null)
    {
      paramVal = defaultVal;
    }
    return paramVal;
  }

  public final void doGet( HttpServletRequest request, HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    try
    {
      // spin off a new instance of the servlet class to handle the request
      MesServletBase servlet = (MesServletBase)getClass().newInstance();
      servlet.setTrueServlet(this);
      servlet.setRequestObj(request);
      servlet.getSession(request);
      servlet.logAccess(request);
      servlet.doServlet(request,response);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::doGet()", e.toString());
    }
  }

  public final void doPost( HttpServletRequest request, HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    doGet(request, response);
  }

  protected void doServlet(HttpServletRequest request, HttpServletResponse response)
  {
  }
}
