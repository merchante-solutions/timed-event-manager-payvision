/*@lineinfo:filename=PricingTool*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/PricingTool.sqlj $

  Description:
  
  PricingTool
  
  A bean used to calculate merchant pricing based on various parameters
  and the contents of the pt_* tables.


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/04/03 10:40a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class PricingTool extends FieldBean
{
  public class UserTypeTable extends DropDownTable
  {
    public UserTypeTable()
    {
      ResultSetIterator it = null;
      ResultSet rs         = null;
      try
      {
        addElement("","select one");
        
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:57^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pt_id,
//                    name
//            from    pt_types
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pt_id,\n                  name\n          from    pt_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.PricingTool",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.utils.PricingTool",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^9*/
        rs = it.getResultSet();
        while (rs.next())
        {
          addElement(rs);
        }
      }
      catch (Exception e)
      {
        log("UserTypeTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  private String[][] getMerchTypeList()
  {
    String[][]        typeList  = null;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    
    try
    {
      connect();
      int typeCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:91^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_type)
//          
//          from    pt_merch_types
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_type)\n         \n        from    pt_merch_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.PricingTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   typeCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^7*/
      
      if (typeCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:100^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_type,
//                    name
//            from    pt_merch_types
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_type,\n                  name\n          from    pt_merch_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.utils.PricingTool",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.utils.PricingTool",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:105^9*/
        rs = it.getResultSet();
      
        int idx = 0;
        typeList = new String[typeCount][2];
        while (rs.next())
        {
          typeList[idx][0] = rs.getString("name");
          typeList[idx][1] = rs.getString("merch_type");
          ++idx;
        }
      }
    }
    catch (Exception e)
    {
      log("getMerchTypeList()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return typeList;
  }
  
  String[][] adjustList =
  {
    { "Calculate pure discount rate, don't use a per transaction fee", "1" },
    { "Specify a discount rate, offset rate with a per transaction fee", "2" },
    { "Specify a per transaction fee, adjust pure discount rate down to offset fee", "3" }
  };
  
  public PricingTool()
  {
    try
    {
      fields.add(new DropDownField      ("userType",      new UserTypeTable(),false));
      fields.add(new Field              ("merchName",     40,30,false));
      fields.add(new CurrencyField      ("averageTicket", 12,12,false));
      fields.add(new CurrencyField      ("annualVolume",  12,12,false));
      fields.add(new RadioButtonField   ("merchType",     getMerchTypeList(),0,false,"Please specify a merchant type"));
      fields.add(new NumberField        ("profitMargin",  5,5,false,2));
      fields.add(new RadioButtonField   ("adjustType",    adjustList,0,false,"Please specify an adjustment type"));
      fields.add(new DiscountField      ("adjustToRate",  false));
      fields.add(new SmallCurrencyField ("adjustToFee",   4,6,false));
      fields.add(new ButtonField        ("calculate"));

      fields.getField("adjustToFee").setOptionalCondition(new FieldValueCondition(
          fields.getField("adjustType"),"3"));
      fields.getField("adjustToRate").setOptionalCondition(new FieldValueCondition(
          fields.getField("adjustType"),"2"));
          
      ((RadioButtonField)fields.getField("merchType")).setLayoutSpec(
        RadioButtonField.LAYOUT_SPEC_HORIZONTAL);

      fields.setGroupHtmlExtra("class=\"formFields\"");
    }
    catch (Exception e)
    {
      log("PricingTool()",e.toString());
    }
  }
  
  public void log(String method, String message)
  {
    System.out.println(this.getClass().getName() + "." + method + ": " + message);
    logEntry(method,message);
  }
  
  protected boolean autoSubmit()
  {
    boolean           submitOk  = false;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    try
    {
      connect();
      
      // extract parameter from field data
      int     userType      = fields.getField("userType")     .asInteger();
      int     merchType     = fields.getField("merchType")    .asInteger();
      double  averageTicket = fields.getField("averageTicket").asDouble();
      double  annualVolume  = fields.getField("annualVolume") .asDouble();
      double  profitMargin  = fields.getField("profitMargin") .asDouble() / 10000;
      double  adjustToRate  = fields.getField("adjustToRate") .asDouble() / 100;
      double  adjustToFee   = fields.getField("adjustToFee")  .asDouble();
      
      averageTicket = MesMath.roundUp(averageTicket,2);
      annualVolume  = MesMath.roundUp(annualVolume,2);
      profitMargin  = MesMath.roundUp(profitMargin,5);
      
      // load interchange assessment and risk using user and merch type
      double  iaPercent;
      double  iaPerTran;
      double  risk;
      /*@lineinfo:generated-code*//*@lineinfo:201^7*/

//  ************************************************************
//  #sql [Ctx] { select  (mt.percent / 100),
//                  mt.per_tran,
//                  (nvl(ut.risk,0) / 100)
//                  
//          
//                  
//          from    pt_merch_types  mt,
//                  pt_types        ut
//          
//          where   mt.merch_type = :merchType
//                  and ut.pt_id  = :userType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (mt.percent / 100),\n                mt.per_tran,\n                (nvl(ut.risk,0) / 100)\n                \n         \n                \n        from    pt_merch_types  mt,\n                pt_types        ut\n        \n        where   mt.merch_type =  :1 \n                and ut.pt_id  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.utils.PricingTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,merchType);
   __sJT_st.setInt(2,userType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   iaPercent = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   iaPerTran = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   risk = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/
      
      iaPercent = MesMath.roundUp(iaPercent,5);
      iaPerTran = MesMath.roundUp(iaPerTran,2);
      risk      = MesMath.roundUp(risk,5);

      // get tran cost based on sales volume and merch type
      /*@lineinfo:generated-code*//*@lineinfo:223^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cost
//          from    pt_costs
//          where   vol_min     <=  :annualVolume and
//                  vol_max     >=  :annualVolume and
//                  merch_type  =   :merchType and
//                  pt_id       =   :userType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cost\n        from    pt_costs\n        where   vol_min     <=   :1  and\n                vol_max     >=   :2  and\n                merch_type  =    :3  and\n                pt_id       =    :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.utils.PricingTool",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,annualVolume);
   __sJT_st.setDouble(2,annualVolume);
   __sJT_st.setInt(3,merchType);
   __sJT_st.setInt(4,userType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.utils.PricingTool",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:231^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        // calculate discount rate and per tran 
        double tranCost = rs.getDouble("cost");
        baseRate  = iaPercent + risk + profitMargin;
        baseFee   = MesMath.roundUp(iaPerTran + tranCost,2);
        
        // roll per tran into the discount rate
        pureRate  = MesMath.roundUp(baseRate + (baseFee / averageTicket),5);
        
        // adjust according to user choice
        adjustedProfit = profitMargin;
        switch (fields.getField("adjustType").asInteger())
        {
          // no adjustment, just using pure discount rate
          case 1:
          default:
            adjustedRate = pureRate;
            break;
            
          // adjust to use given rate
          case 2:
            // set adjusted rate to the specified rate
            adjustedRate = adjustToRate;
            
            // calculate the adjusted tran fee to offset the adjusted rate
            adjustedFee = MesMath.roundUp((pureRate - adjustedRate) * averageTicket,2);
            
            // if adjusted tran fee is negative then reset to zero
            // and set the adjusted profit to reflect the increase
            if (adjustedFee < 0)
            {
              adjustedProfit = adjustedRate - (pureRate - profitMargin);
              adjustedFee = 0;
            }
            break;
            
          // adjust to use given fee
          case 3:
            // set adjusted fee to specified fee
            adjustedFee = adjustToFee;
            
            // calculate the adjusted rate to offset the adjusted fee
            adjustedRate 
              = MesMath.roundUp(pureRate - (adjustedFee / averageTicket),5);
            
            // if adjusted rate is negative, reset it to 0 and 
            // set adjusted profit margin to reflect the increase
            if (adjustedRate < 0)
            {
              adjustedProfit = profitMargin + (adjustedFee / averageTicket - (pureRate - profitMargin));
              adjustedRate = 0;
            }
            break;
        }
        
        // calculate annual net discount profit
        netDiscount = MesMath.roundUp(annualVolume * adjustedProfit,2);
            
        submitOk = true;
      }
    }
    catch (Exception e)
    {
      log("autoSubmit()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return submitOk;
  }

  private double baseRate       = 0;
  private double baseFee        = 0;
  private double pureRate       = 0;
  private double adjustedRate   = 0;
  private double adjustedFee    = 0;
  private double adjustedProfit = 0;
  private double netDiscount    = 0;
  
  public double getBaseRate()
  {
    return baseRate;
  }
  public double getBaseFee()
  {
    return baseFee;
  }
  public double getPureRate()
  {
    return pureRate;
  }
  public double getAdjustedRate()
  {
    return adjustedRate;
  }
  public double getAdjustedFee()
  {
    return adjustedFee;
  }
  public double getAdjustedProfit()
  {
    return adjustedProfit;
  }
  public double getNetDiscount()
  {
    return netDiscount;
  }
  
  public double getProfitMargin()
  {
    return MesMath.roundUp(fields.getField("profitMargin").asDouble(),2);
  }
  
  private boolean isFixedUserType = false;
  
  public boolean getIsFixedUserType()
  {
    return isFixedUserType;
  }
  
  /*
  ** protected boolean postHandleRequest(HttpServletRequest request)
  **
  ** Determines pricing tool type from user bean.  If user has rights then type 
  ** selection is enabled.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      // find the pricing tool type that the user's hierarchy node falls under
      long userHierarchy = user.getHierarchyNode();
      /*@lineinfo:generated-code*//*@lineinfo:375^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pt.pt_id as user_type
//          from    pt_types pt,
//                  t_hierarchy h
//          where   pt.hierarchy_id = h.ancestor
//                  and h.descendent = :userHierarchy
//          order by h.relation
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pt.pt_id as user_type\n        from    pt_types pt,\n                t_hierarchy h\n        where   pt.hierarchy_id = h.ancestor\n                and h.descendent =  :1 \n        order by h.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.utils.PricingTool",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userHierarchy);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.utils.PricingTool",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^7*/
      
      // take the first row of result returned (it will 
      // be closest above the user's hierarchy node)
      // flag the user type as being fixed
      rs = it.getResultSet();
      if (rs.next())
      {
        fields.setData("userType",rs.getString("user_type"));
        isFixedUserType = true;
      }
      // no specific user type is known
      else
      {
        // for now, just let user pick a user type,
        // but in the future this should be only allowed
        // to users with appropriate rights assigned
        isFixedUserType = false;
      }
    }
    catch (Exception e)
    {
      log("postHandleRequest()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/