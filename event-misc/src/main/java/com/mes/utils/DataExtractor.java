package com.mes.utils;

public interface DataExtractor
{

  public boolean extract();
  public String getDataSource();
  public boolean validateFilename();
}