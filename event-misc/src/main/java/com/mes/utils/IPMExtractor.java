/*@lineinfo:filename=IPMExtractor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  /busplatform/branches/prod_tomcat/src/main/com/mes/utils/IPMExtractor.sqlj $

  Description:

  Last Modified By   : $Author: rmehta $
  Last Modified Date : $Date: 2015-05-05 09:24:25 -0700 (Tue, 05 May 2015) $
  Version            : $Revision: 23609 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

/*
cee = clearing emulation engine
ep = edit package

MC data flow
test transaction    => ep           => accepted ipm file
accepted ipm file   => cee          => life cycle file
life cycle file     => cee          => notification file with retr and cbs
notification file   => rob's loader => oracle
oracle              => mc cb event  => second presentments
second presentments => cee          => life cycle file 2
life cycle file 2   => cee          => notification file with arbitration cbs
notification file   => rob's loader => oracle
oracle              => retr billing event => 1740 msgs
1740 msgs           => cee
*/
package com.mes.utils;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.queues.QueueTools;
import com.mes.support.MesMath;
import com.mes.tools.ChargebackTools;
import com.mes.tools.FileUtils;
import masthead.mastercard.clearingiso.IpmOfflineFile;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.mastercard.clearingiso.MicroFocusNetExpressFile;
import sqlj.runtime.ResultSetIterator;

public class IPMExtractor extends SQLJConnectionBase implements DataExtractor
{
  static Logger log = Logger.getLogger(IPMExtractor.class);

  //main codes
  public final int PRESENTMENT    = 1240;
  public final int CHARGEBACK     = 1442;
  public final int FEE_COLLECTION = 1740;
  public final int ALL_OTHER      = 1644;
  /*
  1644 Sub types

  603 Retrieval Request 4
  640 Currency Update
  680 File Currency Summary
  685 Financial Position Detail
  688 Settlement Position Detail
  691 Message Exception
  693 Text Message
  695 File Trailer
  696 Financial Detail Addendum
  697 File Header
  699 File Reject
  */
  public final int RETRIEVAL      = 603;
  public final int MSG_EXCEPTION  = 691;
  public final int ADDENDUM       = 696;
  public final int FILE_FAILURE   = 699;
  public final int HEADER         = 697;
  public final int TRAILER        = 695;


  private boolean isTest          = false;
  private List    filenames       = new LinkedList();
  private String  filename        = null;
  private String  load_filename   = null;
  private String  filenameDateStr = "";
  private int     FileBankNumber  = 3941;

  //incoming 'raw' data
  private IpmOfflineMessage msg   = null;

  //central obj handled - can contain info from one or
  //several IpmOfflineMessage objs
  private MCItem item             = new MCItem();

  //indicator for rejects
  private boolean isPrimaryActive = false;

  //this is a class level object to handle reject information for the notifyMES call
  private StringBuffer messageSB   = null;
  private boolean hasRejects      = false;
  private boolean hasChargebacks  = false;
  private boolean hasRetrievals   = false;

  private int     cbCrCount         = 0;
  private double  cbCrTotal         = 0.0d;
  private int     cbDbCount         = 0;
  private double  cbDbTotal         = 0.0d;
  private int     unkCount          = 0;
  private double  unkTotal          = 0.0d;

  private final int NOTE_REJECT   = 1;
  private final int NOTE_CB       = 2;
  private final int NOTE_RETR     = 3;
  
  enum CurrencyConversionType{
		CLEARING(0,"C"), AUTHORIZATION(1,"A"), NOT_MATCHED(2,"N");
		private Integer ipmValue;
		private String dbValue;
		private CurrencyConversionType(Integer ipmValue,String dbValue) {
			this.ipmValue = ipmValue;
			this.dbValue = dbValue;
		}
		public static CurrencyConversionType getType(Integer ipmValue) {
			if ( AUTHORIZATION.ipmValue.equals(ipmValue)) return AUTHORIZATION;
			else if ( CLEARING.ipmValue.equals(ipmValue)) return CLEARING;
			return NOT_MATCHED;
		}
		public String getDbValue() {
			return this.dbValue;
		}
	}

  public IPMExtractor()
  {
  }

  //for testing purposes, handles the args[] in the handler
  public IPMExtractor(List filenames)
  {
    if(filenames!=null)
    {
      this.filenames = filenames;
    }
  }

  //main process of DataExtractor
  //SEE bottom of code for more info on this (# extract)
  public boolean extract()
  {
    boolean   result    = true;
    List      msgs      = null;
    
    try
    {
      connect();
      setAutoCommit(false);

      //build the file list
      //does nothing right now
      getFiles();

      byte[] data;

      //TODO
      //we may need to alter this to handle on-the-fly data (byte[])
      //rather than always reading from a persistent source
      for(Iterator itr = filenames.iterator(); itr.hasNext();)
      {
        filename = (String)itr.next();

        // reset the flags for this file
        hasRejects      = false;
        hasChargebacks  = false;
        hasRetrievals   = false;

        //ensure filename from list is a valid IPM file
        //allows ExtractorHandler to handle multiple file/Extractors
        if(validateFilename())
        {
          // validateFilename() will set load_filename
          // establish the load file id for this filename
          /*@lineinfo:generated-code*//*@lineinfo:178^11*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:load_filename)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,load_filename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^11*/

          IpmOfflineFile ipmFile;

          //generate byte data
          data = FileUtils.getBytesFromFile(filename);

          //due to variance in files coming in,
          //there may be a parse exception
          try
          {
            //try accessing the data from static method - REPR
            msgs = MicroFocusNetExpressFile.parseAsList(data);
            //ipmFile = IpmOfflineFile.parseMsgBlock(data);
            //msgs    = ipmFile.getMessages();
          }
          catch(Exception e)
          {
            //try this method if the above one fails
            //e.printStackTrace();
            ipmFile = IpmOfflineFile.parseMsgBlock(data);
            msgs    = ipmFile.getMessages();
           // msgs = MicroFocusNetExpressFile.parseAsList(data);
          }

          //if msgs found, run process
          if(msgs != null)
          {
            Iterator ipmMsgIter = msgs.iterator();

            while(ipmMsgIter.hasNext())
            {
              msg = (IpmOfflineMessage)ipmMsgIter.next();

              //activate this if you want the xml display of object
              //log.debug(msg.toXml());

              //if(Integer.parseInt(msg.getDataElement(0))== FEE_COLLECTION)
              //if("5178057279463728".equals(msg.getDataElement(2)))
              //{
                //log.debug(msg.toXml());
                processData();
              //}
            }
            //take care of anything left over - nature of ipm file

            processCleanUp();

            commit(); 

            //notifyMES();
          }
        }
      }//for
    }
    catch (Exception exn)
    {
      // I expect to see a couple errors here due to incorrect field definitions.
      // These errors can be painful to debug.
      //exn.printStackTrace();

      //ditch effort on this file to ensure integrity on reload
      rollback();

      //we're not going to throw an exception at this point,
      //simply log and move on
      //throw new RuntimeException(exn);

      //logEntry("execute( " + filename + " )",exn.toString());
      exn.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
      

    return result;

  }

  //only handle IPM files
  public boolean validateFilename()
  {
    if( FileUtils.validateExtension(filename, new String[]{"ipm","IPM"}) )
    {
      //need to build better mech to handle load_filename
      load_filename  = FileUtils.getNameFromFullPath(filename).toLowerCase();
      if(load_filename.length()>32)
      {
        load_filename = load_filename.substring(load_filename.length()-32);
      }
      return true;
    }
    else
    {
      return false;
    }
  }

  private void setFilenameDateString()
  {
    try
    {
      filenameDateStr = msg.getPrivateDataSubelement(105).substring(3,9);
    }
    catch(Exception e)
    {
      //need to default to sysdate from db
      String _date = "";
      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:294^9*/

//  ************************************************************
//  #sql [Ctx] { select to_char(sysdate,'yymmdd')
//            
//            from dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select to_char(sysdate,'yymmdd')\n           \n          from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   _date = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^9*/

        filenameDateStr = _date;
      }
      catch(Exception e1)
      {
        filenameDateStr = "";
      }
      finally
      {
        cleanUp();
      }
    }
  }


/*
MTI Function Code Clearing System Transaction Function

1240 200 First Presentment
1240 205 Second Presentment (Full)
1240 282 Second Presentment (Partial)

1442 450 First Chargeback (Full)
1442 451 Arbitration Chargeback (Full) 4
1442 453 First Chargeback (Partial)
1442 454 Arbitration Chargeback (Partial) 4

1644 603 Retrieval Request 4
1644 640 Currency Update
1644 680 File Currency Summary
1644 685 Financial Position Detail
1644 688 Settlement Position Detail
1644 691 Message Exception
1644 693 Text Message
1644 695 File Trailer
1644 696 Financial Detail Addendum
1644 697 File Header
1644 699 File Reject

1740 700 Fee Collection (Member-generated) 5
1740 780 Fee Collection Return
1740 781 Fee Collection Resubmission
1740 782 Fee Collection Arbitration Return
1740 783 Fee Collection (Clearing System-generated)
1740 790 Fee Collection (Funds Transfer) Apr 2008
1740 791 Fee Collection (Funds Transfer Backout) Apr 2008

4 Not supported for ATM transactions (DE 3 = 010000)
5 Fee Collection (Member-generated)/1740 messages includes manually submitted (non-Master-
  Com) retrieval fulfillments.
*/

  private void processData()
  throws Exception
  {
    boolean handled = false;

    //need to look at msg
    int type = Integer.parseInt(msg.getDataElement(0));

    //TESTING:: 3140630 and 3140631
    switch(type)
    {
      //1240
      case PRESENTMENT:

        //if this isn't in an error state
        //we need to process
        if(!item.hasErrors())
        {
          item.cleanAll();
          loadFields(PRESENTMENT);
          processPresentment();
          handled = true;
        }
        break;

      //1442
      case CHARGEBACK:

        //if this isn't in an error state
        //we need to process
        if(!item.hasErrors())
        {
          item.cleanAll();
          loadFields(CHARGEBACK);
          processChargeback();
          handled = true;
        }
        break;

      //1644 - includes retrievals, exceptions, headers, trailers...
      case ALL_OTHER:
        //don't check error count here, as the ALL_OTHER handles a
        //wide variety of scenarios
        processOtherBasedOnSubtype();
        //let the following process know we're good already
        handled = true;
        break;

      //1740
      case FEE_COLLECTION:
        //if this isn't in an error state
        //we need to process
        if(!item.hasErrors())
        {
          item.cleanAll();
          loadFields(FEE_COLLECTION);
          processFeeCollection();
          handled = true;
        }
        break;

      default:
        log.debug("Invalid message type found ::" + type);
        //throw new Exception("Invalid message type found.");
    };

    //indicates that we're in a reject scenario
    if(!handled)
    {
      //we've already passed the first primary
      //so process the reject of that item - trailing
      //main categories 1240, 1442 etc trigger this process
      if(isPrimaryActive)
      {
        processReject();
      }
      else
      {
        //need to set flag to start the reject process
        //in case there are messages following that
        //need to be added to the primary
        isPrimaryActive = true;
      }

      //load the fields after handling (potential) prior reject
      loadFields(type);

    }

  }

  private void processCleanUp()
  {
    //check to see if errors were found and use this as a flag
    //to tell us we need to handle the last item for rejects
    //boolean will never be true on valid, non-reject items
    log.debug("in processCleanUp...");
    if(isPrimaryActive)
    {
      try
      {
        processReject(true);
      }
      catch(Exception e)
      {
        String recId = item!=null?""+item.recId:"NULL";
        logEntry("processCleanUp():: Reject item="+recId , e.getMessage());
      }
    }

    try
    {
      // if there were chargebacks in the file, then
      // enter the filename into the risk_process table
      // to get it loaded into the proper places/queues
      if ( hasChargebacks )
      {

        /*@lineinfo:generated-code*//*@lineinfo:470^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//            (
//              process_type,
//              process_sequence,
//              load_filename
//            )
//            values
//            (
//              8,
//              0,
//              :load_filename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n          (\n            process_type,\n            process_sequence,\n            load_filename\n          )\n          values\n          (\n            8,\n            0,\n             :1  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,load_filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:484^9*/

      }
      else if ( hasRetrievals )
      {
        /*@lineinfo:generated-code*//*@lineinfo:489^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//            (
//              process_type,
//              process_sequence,
//              load_filename
//            )
//            values
//            (
//              1,
//              0,
//              :load_filename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n          (\n            process_type,\n            process_sequence,\n            load_filename\n          )\n          values\n          (\n            1,\n            0,\n             :1  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,load_filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("processCleanUp()::CB item",e.toString());
      MailMessage.sendSystemErrorEmail("IPM Extractor Error", "processCleanUp() failed to create risk_process table entries" );
    }
  }

/*
RETRIEVAL      = 603
MSG_EXCEPTION  = 691
ADDENDUM       = 696
FILE_FAILURE   = 699
HEADER         = 697
TRAILER        = 695
*/
  private void processOtherBasedOnSubtype()
  throws Exception
  {
    //need to look at funcCode
    int subType = Integer.parseInt(msg.getDataElement(24));

    switch(subType)
    {
      //Retrieval
      case RETRIEVAL:


        if(!item.hasErrors())
        {
          item.cleanAll();
          loadFields(RETRIEVAL);
          processRetrieval();
        }
        else
        {
          //this mimics the if(!handled) portion in processData()
          //we've already passed the first primary
          //so process the reject of that item
          if(isPrimaryActive)
          {
            processReject();
          }
          else
          {
            //need to set flag to start the reject process
            isPrimaryActive = true;
          }
          loadFields(RETRIEVAL);
        }
        break;

     //message exception - need to load relevant fields
      case MSG_EXCEPTION:
        loadFields(MSG_EXCEPTION);
        break;

      //addendum record - need to load reject reasons to item list
      case ADDENDUM:
        loadFields(ADDENDUM);
        break;

      //complete file failure
      case FILE_FAILURE:
        //only need to email notification on this
        log.debug("in processOtherBasedOnSubtype::FILE_FAILURE");
        loadFields(MSG_EXCEPTION);
        notifyFailure();
        break;

      case HEADER:
        //need to set file date string here, based on file header
        setFilenameDateString();
        log.debug("setFilenameDateString() = "+ filenameDateStr);
        break;

      case TRAILER:

        log.debug("Ignoring trailers...");
        break;

      default:
        log.debug("not currently handling 1644 subcode: "+ subType );
        try
        {
          //log.debug(msg.toXml());
          if(item.hasErrors())
          {
            //treat this like an exception to allow process to continue
            loadFields(MSG_EXCEPTION);
          }
        }
        catch(Exception e)
        {
          //let continue
          //e.printStackTrace();
        }
        break;
    };

  }

  //SEE bottom of page for DE details
  private void loadFields(int type)
  throws Exception
  {
    log.debug("in loadFields :: "+type);
    String dataElement    = null;

    if(msg==null)
    {
      throw new NullPointerException();
    }

    switch(type)
    {

      case PRESENTMENT:
      case CHARGEBACK:
      case RETRIEVAL:

        //load all fields here - primary
        item.msgType      = msg.getDataElement(0);
        item.acctNum      = msg.getDataElement(2) != null? Long.parseLong(msg.getDataElement(2)) : 0L ;
        item.procCode     = msg.getDataElement(3);
        item.DBCRInd      = item.procCode!=null?(item.procCode.substring(0,2).equals("20")?"C":"D"):"";
        item.transAmt     = msg.getDataElement(4) != null?(Double.parseDouble(msg.getDataElement(4))/100):0.0d;
        item.transDate    = msg.getDataElement(12)!= null?(msg.getDataElement(12).substring(0,6)):"";
        item.posCode      = msg.getDataElement(22);
        item.funcCode     = msg.getDataElement(24);
        item.decodeFuncCode();
        item.reasonCode   = msg.getDataElement(25);
        item.sic          = msg.getDataElement(26);
        item.origAmt      = msg.getDataElement(30)!=null?Double.parseDouble(msg.getDataElement(30).substring(0,12))/100:item.transAmt;
        item.refNum       = msg.getDataElement(31);
        //set acqBIN, bankNumber and acqRefNum, based on refNum
        item.setRefNumAttr();
        item.acqID        = msg.getDataElement(33);
        item.authNum      = msg.getDataElement(38);
        //validate merchant number here - check MIF to ensure it exists; if not, find it
        //item.merchNum     = msg.getDataElement(42)!=null?Long.parseLong(msg.getDataElement(42).trim()):0L;
        item.merchNum     = validateMID(item.bankNumber,msg.getDataElement(42));

        item.currCode     = msg.getDataElement(49);
        item.extractAddressData(msg.getDataElement(43));
        if ( (dataElement = msg.getDataElement(63)) != null )
        {
          item.bankNetRefNum  = dataElement.substring(1,10);
          item.bankNetDate    = dataElement.substring(10,14);
        }
        item.icaNum       = msg.getDataElement(93);
        item.issBIN       = msg.getDataElement(94);
        item.cbRefNum     = msg.getDataElement(95);
        //72??
        //93??
        //100??

        //private data subelements
        dataElement = msg.getPrivateDataSubelement(15);
		if ( StringUtils.isNotBlank(dataElement) && dataElement.trim().length() == 7 ) {
			item.currencyConversionDate = dataElement.substring(0, 6);
			item.currencyConversionIndicator = Integer.valueOf(dataElement.substring(6));
		}
        //23  = Terminal Type
        item.termType         = msg.getPrivateDataSubelement(23);
        
        //25  = Reversal Indicator
        if ( (dataElement = msg.getPrivateDataSubelement(25)) != null )
        {
          item.reversal = (dataElement.length() > 0 ? "R".equals(dataElement.substring(0,1)) : false);
        }          
        
        item.ecommSecLvlInd   = msg.getPrivateDataSubelement(52);
        //80?
        //148 = Currecny Exponents
        item.currExponents    = msg.getPrivateDataSubelement(148);

        //149 = Currency Codes, Amounts, Original
        item.currCodeOrig     = msg.getPrivateDataSubelement(149)!=null?msg.getPrivateDataSubelement(149).substring(0,3):"";

        //158 = Business Activity
        item.bizInd           = msg.getPrivateDataSubelement(158)!=null?msg.getPrivateDataSubelement(158).substring(0,12):"";


        //165 = Settlement Indicator
        item.settleInd        = msg.getPrivateDataSubelement(165);

        //173?
        //262 = Documentaton Indicator
        item.docInd           = msg.getPrivateDataSubelement(262);

        findRecId();

        break;

      case MSG_EXCEPTION:

        //in this case, we build the item errList
        item.addErrMsg(msg);

        break;


      case ADDENDUM:
        //this is not an error, but may contain error msgs needed
        //so for now we'll treat as one
        item.addErrMsg(msg);
        break;

      case FEE_COLLECTION:

        log.debug("found fee...");

        /*
        1740 700 Fee Collection (Member-generated) 5
        1740 780 Fee Collection Return
        1740 781 Fee Collection Resubmission
        1740 782 Fee Collection Arbitration Return
        1740 783 Fee Collection (Clearing System-generated)
        1740 790 Fee Collection (Funds Transfer) Apr 2008
        1740 791 Fee Collection (Funds Transfer Backout) Apr 2008

        <isomsg>
          <field id="000" value="1740"/>
          <field id="003" value="190000"/>
          <field id="004" value="000000007731"/>
          <field id="005" value="000000007731"/>
          <field id="009" value="61000000"/>
          <field id="024" value="783"/>
          <field id="025" value="7822"/>
          <field id="033" value="003001"/>
          <field id="048" value="{
            137='00030011015600373003',
            148='8402',
            158='DMC2010001  10061701    NNNNN',
            159='12093      30004543                    1US00000001N1006170110061701',
            165='M ',
            191='2'}"/>
          <field id="049" value="840"/>
          <field id="050" value="840"/>
          <field id="071" value="00000031"/>
          <field id="072" value="IC ADJ SR# LN-BWER-84XJFA &#0;&#0;&#0;&#0;&#0;&#0;&#0;&#0;&#0;ORG FEE=0000171668 CORR.FEE=0000179399  "/>
          <field id="093" value="012093"/>
          <field id="094" value="003001"/>
          <field id="100" value="011606"/>
        </isomsg>
        */

        item.msgType      = msg.getDataElement(0);
        item.acctNum      = msg.getDataElement(2) != null? Long.parseLong(msg.getDataElement(2)) : 0L ;
        item.procCode     = msg.getDataElement(3).substring(0,2);
        item.transAmt     = msg.getDataElement(4) != null?(Double.parseDouble(msg.getDataElement(4))/100):0.0d;
        item.reconAmt     = msg.getDataElement(5) != null?(Double.parseDouble(msg.getDataElement(5))/100):0.0d;; //recon amt
        item.setConvRate(msg.getDataElement(9)); //first number is decimal placing, remainder is conversion 61000000 = 1.000000
        item.funcCode     = msg.getDataElement(24);
        item.reasonCode   = msg.getDataElement(25);
        item.refNum       = msg.getDataElement(31);
        item.acqID        = msg.getDataElement(33);
        //subs under item 48:
//        = msg.getPrivateDataSubelement(137);//Fee Collection Control Number
//        = msg.getPrivateDataSubelement(148);//Currency Exponents
        item.setBizActivity(msg.getPrivateDataSubelement(158));//Business Activity
        item.setSettlementData(msg.getPrivateDataSubelement(159));//Settlement Data
        item.settleInd    = msg.getPrivateDataSubelement(165);//Settlement Indicator
//        = msg.getPrivateDataSubelement(191);//Originating Message Format
        item.currCode     = msg.getDataElement(49);
        item.reconCurrCode= msg.getDataElement(50);//currCode recon
        //= msg.getDataElement(71);//message number
        item.msgText      = msg.getDataElement(72);//record data
        item.transDate    = msg.getDataElement(73);//date, action
        item.icaNum       = msg.getDataElement(93);//Transaction Destination Institution ID Code
        item.issBIN       = msg.getDataElement(94);//Transaction Originator Institution ID Code
        dataElement = msg.getPrivateDataSubelement(15);
		if ( StringUtils.isNotBlank(dataElement) && dataElement.trim().length() == 7 ) {
			item.currencyConversionDate = dataElement.substring(0, 6);
			item.currencyConversionIndicator = Integer.valueOf(dataElement.substring(6));
		}
        //= msg.getDataElement(100);//Receiving Institution ID Code
        /*
        MERCHANT_NUMBER,                NUMBER (16),
        SETTLE_REC_ID,                  NUMBER (20),
        MESSAGE_TYPE,                   VARCHAR2 (4 Byte),
        FUNCTION_CODE,                  VARCHAR2 (3 Byte),
        PROCESSING_CODE,                VARCHAR2 (2 Byte),
        SOURCE_ICA,                     VARCHAR2 (6 Byte),
        DEST_ICA,                       VARCHAR2 (6 Byte),
        PROCESSING_ICA,                 VARCHAR2 (6 Byte),
        REASON_CODE,                    VARCHAR2 (4 Byte),
        SETTLEMENT_DATE,                DATE,
        SETTLEMENT_CYCLE,               NUMBER (2),
        RECON_DATE,                     DATE,
        RECON_CYCLE,                    NUMBER (2),
        TRANSACTION_DATE,               DATE,
        CARD_NUMBER,                    VARCHAR2 (16 Byte),
        DEBIT_CREDIT_INDICATOR,         CHAR (1 Byte),
        TRANSACTION_AMOUNT,             NUMBER (12,2),
        RECON_AMOUNT,                   NUMBER (12,2),
        CONVERSION_RATE,                NUMBER (15,7),
        FORWARDING_ID,                  VARCHAR2 (11 Byte),
        CURRENCY_CODE,                  VARCHAR2 (3 Byte),
        RECON_CURRENCY_CODE,            VARCHAR2 (3 Byte),
        MESSAGE_TEXT,                   VARCHAR2 (1000 Byte),

        //158
        CARD_PROGRAM_ID,                VARCHAR2 (3 Byte),
        BSA_TYPE,                       CHAR (1 Byte),
        BSA_ID_CODE,                    VARCHAR2 (6 Byte),
        IRD,                            VARCHAR2 (2 Byte),
        BUSINESS_DATE,                  DATE,
        BUSINESS_CYCLE,                 NUMBER (2),
        CARD_ACCEPTOR_OVERRIDE_IND,     CHAR (1 Byte),
        PRODUCT_CLASS_OVERRIDE_IND,     VARCHAR2 (3 Byte),

        //159
        SETTLEMENT_FLAG,                CHAR (1 Byte),
        SETTLE_AGREEMENT_INFO,          VARCHAR2 (30 Byte),
        SETTLE_SERVICE_ID,              VARCHAR2 (11 Byte),
        SETTLE_SERVICE_AGENT_ACCT,      VARCHAR2 (28 Byte),
        SETTLE_SERVICE_LEVEL_CODE,      NUMBER (1),
        SETTLE_SERVICE_ID_CODE,         VARCHAR2 (10 Byte),
        SETTLE_FX_RATE_CLASS_CODE,      CHAR (1 Byte),

        CARD_NUMBER_ENC,                VARCHAR2 (96 Byte),
        LOAD_FILENAME,                  VARCHAR2 (32 Byte),
        LOAD_FILE_ID                    NUMBER (20)

        */

        //log.debug(msg.toXml());
        break;

      default:
        //doing nothing here yet
    };
  }

  private long validateMID(int bankNumber, String data)
  {
    long    incomingMid   = 0L;
    long    mid           = 0L;

    try
    {
      // determine if the issuer provided a valid mid
      try { incomingMid = Long.parseLong(data.trim()); } catch( Exception ee ) { incomingMid = 0L; }
      if ( incomingMid != 0L && getMIDCount(bankNumber,incomingMid) > 0 )
      {
        mid = incomingMid;
      }
      // else allow the downstream lookup to resolve the mid
    }
    catch(Exception e)
    {
      logEntry("validateMID(" + bankNumber + "," + data + ")",e.toString());
      log.debug(e.getMessage());
    }
    return( mid );
  }

  private int getMIDCount(int bankNumber, long mid)
  {
    int count = 0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:860^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)
//          
//          from    mif     mf
//          where   mf.bank_number = :bankNumber
//                  and mf.merchant_number = :mid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)\n         \n        from    mif     mf\n        where   mf.bank_number =  :1  \n                and mf.merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,mid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:867^7*/
    }
    catch(Exception e)
    {
      logEntry("getMIDCount(" + mid + ")",e.toString());
    }
    return( count );
  }

  //need to access mc_settlement to find rec_id (-1, +6 was default batch_date range)
  private void findRecId()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:881^7*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//          
//          from    mc_settlement
//          where   acq_reference_number  = :item.acqRefNum
//                  and yddd_to_date(substr(:item.refNum,8,4)) between batch_date-1 and batch_date+6
//                  and :item.merchNum in (0,merchant_number)
//                  and nvl(test_flag,'Y') = 'N'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id \n         \n        from    mc_settlement\n        where   acq_reference_number  =  :1  \n                and yddd_to_date(substr( :2  ,8,4)) between batch_date-1 and batch_date+6\n                and  :3   in (0,merchant_number)\n                and nvl(test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.acqRefNum);
   __sJT_st.setString(2,item.refNum);
   __sJT_st.setLong(3,item.merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   item.recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:890^7*/
    }
    catch(Exception e)
    {
    	//e.printStackTrace();
    	log.info("in findRecId() Exception: "+load_filename+", " + item.refNum + ", " + item.bankNetRefNum + ", " + item.authNum + ", " + e.getMessage());
    	    	
    	// Bug Fix put in place for handling MC rejects not loaded due to rec id finding failures
    	if(load_filename.indexOf("mc_t113") >= 0 ||  load_filename.indexOf("mc_chgbk") >= 0 ) {
	    	findRecIdNew();
		} else {
    		logEntry("findRecId(" + item.acqRefNum + ", " + item.refNum + ")",e.toString());	    	
	    	item.recId = -1;
    	}
    }
    log.debug("in findRecId():"+item.recId);
  }

  private void findRecIdNew() {
    try
    {
    	log.debug("findRecIdNew(" + item.acqRefNum + ", " + item.refNum + ", " + item.bankNetRefNum + ", " + item.authNum + ")");
    	
    	if(item.bankNetRefNum != null && !"".equals(item.bankNetRefNum)
    		&& item.authNum != null && !"".equals(item.authNum)) {
	      /*@lineinfo:generated-code*//*@lineinfo:915^8*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//  		      
//  		      from    mc_settlement
//  		      where   acq_reference_number  = :item.acqRefNum
//  		              and yddd_to_date(substr(:item.refNum,8,4)) between batch_date-1 and batch_date+6
//  		              and :item.merchNum in (0,merchant_number)
//  		              and nvl(test_flag,'Y') = 'N'
//  		              and auth_banknet_ref_num = :item.bankNetRefNum
//  		              and auth_code = :item.authNum
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id \n\t\t       \n\t\t      from    mc_settlement\n\t\t      where   acq_reference_number  =  :1  \n\t\t              and yddd_to_date(substr( :2  ,8,4)) between batch_date-1 and batch_date+6\n\t\t              and  :3   in (0,merchant_number)\n\t\t              and nvl(test_flag,'Y') = 'N'\n\t\t              and auth_banknet_ref_num =  :4  \n\t\t              and auth_code =  :5 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.acqRefNum);
   __sJT_st.setString(2,item.refNum);
   __sJT_st.setLong(3,item.merchNum);
   __sJT_st.setString(4,item.bankNetRefNum);
   __sJT_st.setString(5,item.authNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   item.recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:926^8*/
    	} else if(item.bankNetRefNum != null && !"".equals(item.bankNetRefNum)) {
	      /*@lineinfo:generated-code*//*@lineinfo:928^8*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//  		      
//  		      from    mc_settlement
//  		      where   acq_reference_number  = :item.acqRefNum
//  		              and yddd_to_date(substr(:item.refNum,8,4)) between batch_date-1 and batch_date+6
//  		              and :item.merchNum in (0,merchant_number)
//  		              and nvl(test_flag,'Y') = 'N'
//  		              and auth_banknet_ref_num = :item.bankNetRefNum
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id \n\t\t       \n\t\t      from    mc_settlement\n\t\t      where   acq_reference_number  =  :1  \n\t\t              and yddd_to_date(substr( :2  ,8,4)) between batch_date-1 and batch_date+6\n\t\t              and  :3   in (0,merchant_number)\n\t\t              and nvl(test_flag,'Y') = 'N'\n\t\t              and auth_banknet_ref_num =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.acqRefNum);
   __sJT_st.setString(2,item.refNum);
   __sJT_st.setLong(3,item.merchNum);
   __sJT_st.setString(4,item.bankNetRefNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   item.recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:938^8*/
    	} else if(item.authNum != null && !"".equals(item.authNum)) {
	      /*@lineinfo:generated-code*//*@lineinfo:940^8*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//  		      
//  		      from    mc_settlement
//  		      where   acq_reference_number  = :item.acqRefNum
//  		              and yddd_to_date(substr(:item.refNum,8,4)) between batch_date-1 and batch_date+6
//  		              and :item.merchNum in (0,merchant_number)
//  		              and nvl(test_flag,'Y') = 'N'
//  		              and auth_code = :item.authNum
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id \n\t\t       \n\t\t      from    mc_settlement\n\t\t      where   acq_reference_number  =  :1  \n\t\t              and yddd_to_date(substr( :2  ,8,4)) between batch_date-1 and batch_date+6\n\t\t              and  :3   in (0,merchant_number)\n\t\t              and nvl(test_flag,'Y') = 'N'\n\t\t              and auth_code =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.acqRefNum);
   __sJT_st.setString(2,item.refNum);
   __sJT_st.setLong(3,item.merchNum);
   __sJT_st.setString(4,item.authNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   item.recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:950^8*/
    	}
    }
    catch(Exception e) {	    	
		logEntry("findRecIdNew(" + item.acqRefNum + ", " + item.refNum + ", " + item.bankNetRefNum + ", " + item.authNum + ")",e.toString());
		item.recId = -1;		
    }
    
    log.info("in findRecIdNew():"+item.recId);
  }
  
/*
The MasterCard reject handler was treating all rejects as though they are
first presentment (FP) rejects. updated to pay attention
to DE 24 (Function Code). When the function code is a second presentment (205 or 282)
and locate the cb_load_sec from the table network_chargeback_mc. The linking can
be done using the reference number from DE 31. If there are multiple matches,
then use the card number (DE 2) and the merchant dba name (subfield 1 of DE 43).
*/
  private void calibrateRecId()
  {
    log.debug("in calibrateRecId()... recId=="+item.recId);
    //check for second presentment only
    if(item.usageCode != null && item.usageCode.equals("2"))
    {
      long id = 0L;

      try
      {
        int count = 0;

        //first, look for matches
        /*@lineinfo:generated-code*//*@lineinfo:982^9*/

//  ************************************************************
//  #sql [Ctx] { select count(load_sec)
//            
//            from network_chargeback_mc
//            where reference_number = :item.refNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(load_sec)\n           \n          from network_chargeback_mc\n          where reference_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,item.refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^9*/

        log.debug("count = "+count);

        //only go forward if count is greater than 0
        if(count > 0)
        {
          if(count==1)
          {
            //use same query, but grab load_sec
            /*@lineinfo:generated-code*//*@lineinfo:998^13*/

//  ************************************************************
//  #sql [Ctx] { select load_sec
//                
//                from network_chargeback_mc
//                where reference_number = :item.refNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select load_sec\n               \n              from network_chargeback_mc\n              where reference_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,item.refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1004^13*/

            //change the rec_id to reflect the load_sec
            item.recId = id;
          }
          else
          {
            //found more than 1 entry, need to add more variables to
            //hone in on the correct transaction
            String searchString = "%"+item.aName+"%";

            ResultSetIterator itr = null;
            ResultSet rs          = null;

            /*@lineinfo:generated-code*//*@lineinfo:1018^13*/

//  ************************************************************
//  #sql [Ctx] itr = { select  load_sec
//                from    network_chargeback_mc
//                where   reference_number                          = :item.refNum
//                and     dukpt_decrypt_wrapper(card_number_enc)    = :item.acctNum
//                and     merchant_name                             like :searchString
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  load_sec\n              from    network_chargeback_mc\n              where   reference_number                          =  :1  \n              and     dukpt_decrypt_wrapper(card_number_enc)    =  :2  \n              and     merchant_name                             like  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,item.refNum);
   __sJT_st.setLong(2,item.acctNum);
   __sJT_st.setString(3,searchString);
   // execute query
   itr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.utils.IPMExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1025^13*/

            rs = itr.getResultSet();

            //assume that there's 1, or the first one is valid
            if(rs.next())
            {
              item.recId = rs.getLong("load_sec");
            }
          }

        }//count>0

      }
      catch(Exception e)
      {
        //e.printStackTrace();
        logEntry("calibrateRecId(" + item.acqRefNum + ")",e.toString());
      }
      log.debug("out calibrateRecId()... recId=="+item.recId);
      //log.debug(item.toString());
    }

  }



  private void processPresentment()
  throws Exception
  {
    log.debug("processPresentment() TBD");
    //get load sec??
    //item.loadSec      = ChargebackTools.getNewLoadSec();
  }

  private void processChargeback()
  throws Exception
  {

    //log.debug(item.toString());

    hasChargebacks = true;

    log.debug("running... processChargeback()");

    //check procCode
    String tranCode = item.procCode;
    if(tranCode != null && tranCode.length()>4)
    {
      tranCode = item.procCode.substring(0,4);
    }
    
    if ( item.reversal )    // reverse previous chargeback
    {
      long      loadSec   = 0L;
      int       recCount  = 0;
      
      try
      {
        double          reversalAmount  = (item.transAmt * ("C".equals(item.DBCRInd) ? -1 : 1));
        java.sql.Date   reversalDate    = null;
        
        // chargeback reference number is required to be
        // unique within the issuer bin
        /*@lineinfo:generated-code*//*@lineinfo:1089^9*/

//  ************************************************************
//  #sql [Ctx] { select  load_sec,
//                    nvl(to_date(:filenameDateStr,'yymmdd'), trunc(sysdate))
//            
//            from    network_chargeback_mc
//            where   issuer_bin      = :item.issBIN
//                    and cb_ref_num  = :item.cbRefNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_sec,\n                  nvl(to_date( :1  ,'yymmdd'), trunc(sysdate))\n           \n          from    network_chargeback_mc\n          where   issuer_bin      =  :2  \n                  and cb_ref_num  =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,filenameDateStr);
   __sJT_st.setString(2,item.issBIN);
   __sJT_st.setString(3,item.cbRefNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadSec = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   reversalDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1097^9*/
        ChargebackTools.reverseChargeback(Ctx,"MC",loadSec,reversalDate,reversalAmount);
      }
      catch( Exception e )
      {
        // issue system warning
        MailMessage.sendSystemErrorEmail("IPM Extractor Error", "Failed to reverse chargeback " + item.issBIN + "/" + item.cbRefNum + " in file '" + load_filename + "'" + "\n\n" + e.toString() );
      }        
    }
    else    // insert new chargeback
    {
      //get load sec
      item.loadSec      = ChargebackTools.getNewLoadSec();

      /*@lineinfo:generated-code*//*@lineinfo:1111^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_mc
//          (
//            load_sec,
//            cb_ref_num,
//            transaction_code,
//            processing_code,
//            pos_data_code,
//            account_number,
//            transaction_date,
//            debit_credit_ind,
//            transaction_amount,
//            original_amt,
//            original_currency_code,
//            function_code,
//            merchant_account_number,
//            merchant_name,
//            merchant_city,
//            merchant_state,
//            reference_number,
//            issuer_bin,
//            authorization_number,
//            banknet_ref_num,
//            banknet_date,
//            reason_code,
//            class_code,
//            merchant_country,
//            usage_code,
//            partial_amt_ind,
//            acquirer_institution_id,
//            mc_ica_num,
//            cardholder_act_term_ind,
//            ecomm_security_level_ind,
//            documentation_ind,
//            business_activity_ind,
//            central_processing_date,
//            bank_number,
//            actual_incoming_date,
//            load_filename    
//          )
//          values
//          (
//            :item.loadSec,
//            :item.cbRefNum,
//            :tranCode,
//            :item.procCode,
//            :item.posCode,
//            :item.acctNum,
//            to_date(:item.transDate,'yyMMdd'),
//            :item.DBCRInd,
//            ( :item.transAmt * decode(:item.DBCRInd,'C',-1,1) ),
//            ( :item.origAmt  * decode(:item.DBCRInd,'C',-1,1) ),
//            :item.currCode,
//            :item.funcCode,
//            :item.merchNum,
//            :item.aName,
//            :item.aCity,
//            :item.aState,
//            :item.refNum,
//            :item.issBIN,
//            :item.authNum,
//            :item.bankNetRefNum,
//            :item.bankNetDate,
//            :item.reasonCode,
//            :item.sic,
//            :item.aCountry,
//            :item.usageCode,
//            :item.partialInd,
//            :item.acqID,
//            :item.icaNum,
//            :item.termType,
//            :item.ecommSecLvlInd,
//            :item.docInd,
//            :item.bizInd,
//            nvl(to_date(:filenameDateStr,'yymmdd'), trunc(sysdate)),
//            :item.bankNumber,
//            nvl(to_date(:filenameDateStr,'yymmdd'), trunc(sysdate)),
//            :load_filename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
//@formatter:off
	 String theSqlTS = "insert into network_chargeback_mc "
	 		+ " ( "
	 		+ " load_sec, "
	 		+ " cb_ref_num, "
	 		+ " transaction_code, "
	 		+ " processing_code, "
	 		+ " pos_data_code, "
	 		+ " account_number, "
	 		+ " transaction_date, "
	 		+ " debit_credit_ind, "
	 		+ " transaction_amount, "
	 		+ " original_amt, "
	 		+ " original_currency_code, "
	 		+ " function_code, "
	 		+ " merchant_account_number, "
	 		+ " merchant_name, "
	 		+ " merchant_city, "
	 		+ " merchant_state, "
	 		+ " reference_number, "
	 		+ " issuer_bin, "
	 		+ " authorization_number, "
	 		+ " banknet_ref_num, "
	 		+ " banknet_date, "
	 		+ " reason_code, "
	 		+ " class_code, "
	 		+ " merchant_country, "
	 		+ " usage_code, "
	 		+ " partial_amt_ind, "
	 		+ " acquirer_institution_id, "
	 		+ " mc_ica_num, "
	 		+ " cardholder_act_term_ind, "
	 		+ " ecomm_security_level_ind, "
	 		+ " documentation_ind, "
	 		+ " business_activity_ind, "
	 		+ " central_processing_date, "
	 		+ " bank_number, "
	 		+ " actual_incoming_date, "
	 		+ " load_filename, "
	 		+ " fx_conv_date, "
	 		+ " fx_conv_ind) "
	 		+ " values ("
	 		+ " :1, "
	 		+ " :2, "
	 		+ " :3, "
	 		+ " :4, "
	 		+ " :5, "
	 		+ " :6, "
	 		+ " to_date( :7  ,'yyMMdd'), "
	 		+ " :8, "
	 		+ " ( :9   * decode( :10  ,'C',-1,1) ), "
	 		+ " ( :11    * decode( :12  ,'C',-1,1) ), "
	 		+ " :13, "
	 		+ " :14, "
	 		+ " :15, "
	 		+ " :16, "
	 		+ " :17, "
	 		+ " :18, "
	 		+ " :19, "
	 		+ " :20, "
	 		+ " :21, "
	 		+ " :22, "
	 		+ " :23, "
	 		+ " :24, "
	 		+ " :25, "
	 		+ " :26, "
	 		+ " :27, "
	 		+ " :28, "
	 		+ " :29, "
	 		+ " :30, "
	 		+ " :31, "
	 		+ " :32, "
	 		+ " :33, "
	 		+ " :34, "
	 		+ " nvl(to_date( :35  ,'yymmdd'), trunc(sysdate)), "
	 		+ " :36, "
	 		+ " nvl(to_date( :37  ,'yymmdd'), trunc(sysdate)), "
	 		+ " :38,  "
	 		+ " nvl(to_date( :39  ,'yymmdd'), trunc(sysdate)), "
	 		+ " :40"
	 		+ " ) ";
	//@formatter:on
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,item.loadSec);
   __sJT_st.setString(2,item.cbRefNum);
   __sJT_st.setString(3,tranCode);
   __sJT_st.setString(4,item.procCode);
   __sJT_st.setString(5,item.posCode);
   __sJT_st.setLong(6,item.acctNum);
   __sJT_st.setString(7,item.transDate);
   __sJT_st.setString(8,item.DBCRInd);
   __sJT_st.setDouble(9,item.transAmt);
   __sJT_st.setString(10,item.DBCRInd);
   __sJT_st.setDouble(11,item.origAmt);
   __sJT_st.setString(12,item.DBCRInd);
   __sJT_st.setString(13,item.currCode);
   __sJT_st.setString(14,item.funcCode);
   __sJT_st.setLong(15,item.merchNum);
   __sJT_st.setString(16,item.aName);
   __sJT_st.setString(17,item.aCity);
   __sJT_st.setString(18,item.aState);
   __sJT_st.setString(19,item.refNum);
   __sJT_st.setString(20,item.issBIN);
   __sJT_st.setString(21,item.authNum);
   __sJT_st.setString(22,item.bankNetRefNum);
   __sJT_st.setString(23,item.bankNetDate);
   __sJT_st.setString(24,item.reasonCode);
   __sJT_st.setString(25,item.sic);
   __sJT_st.setString(26,item.aCountry);
   __sJT_st.setString(27,item.usageCode);
   __sJT_st.setString(28,item.partialInd);
   __sJT_st.setString(29,item.acqID);
   __sJT_st.setString(30,item.icaNum);
   __sJT_st.setString(31,item.termType);
   __sJT_st.setString(32,item.ecommSecLvlInd);
   __sJT_st.setString(33,item.docInd);
   __sJT_st.setString(34,item.bizInd);
   __sJT_st.setString(35,filenameDateStr);
   __sJT_st.setInt(36,item.bankNumber);
   __sJT_st.setString(37,filenameDateStr);
   __sJT_st.setString(38,load_filename);
   __sJT_st.setString(39,item.currencyConversionDate);
   __sJT_st.setString(40,item.currencyConversionIndicator != null && item.currencyConversionIndicator != -1 ? CurrencyConversionType.getType(item.currencyConversionIndicator).getDbValue() : "");
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1191^7*/
    }      

    //use cumulative count for CBs now, not indy notes
    addTotalToCB();
  }

  private void addTotalToCB()
  throws Exception
  {
    //init the message
    if(messageSB == null)
    {
      messageSB = new StringBuffer();
    }

    if( "C".equals(item.DBCRInd) )
    {
      //increase CR item count
      cbCrCount++;

      //increase CR total
      cbCrTotal = cbCrTotal + item.transAmt;
    }
    else if("D".equals(item.DBCRInd))
    {
      //increase DB item count
      cbDbCount++;

      //increase DB total
      cbDbTotal = cbDbTotal + item.transAmt;
    }
    else
    {
      unkCount++;
      unkTotal = unkTotal + item.transAmt;
    }
  }

  private void processRetrieval()
  {
    try
    {
      log.debug("running... processRetrieval()");

      hasRetrievals = true;

      //get load sec
      item.loadSec      = ChargebackTools.getNewLoadSec();

      /*@lineinfo:generated-code*//*@lineinfo:1241^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrieval_mc
//          (
//            load_sec,
//            --transaction_code,
//            --processing_code,
//            --pos_data_code,
//            account_number,
//            transaction_date,
//            --debit_credit_ind,
//            transaction_amt,
//            --original_amt,
//            tran_currency_code,
//            --function_code,
//            merchant_account_number,
//            merchant_name,
//            merchant_city,
//            merchant_state,
//            merchant_country_code,
//            ref_num,
//            --issuer_bin,
//            authorization_code,
//            reason_code,
//            --class_code,
//            --usage_code,
//            --partial_amt_ind,
//            --acquirer_institution_id,
//            --documentation_ind,
//            business_activity_ind,
//            bank_number,
//            actual_incoming_date,
//            load_filename
//          )
//          values
//          (
//            :item.loadSec,
//            --:(item.procCode),
//            --:(item.procCode),
//            --:(item.posCode),
//            :item.acctNum,
//            to_date(:item.transDate,'yymmdd'),
//            --:(item.DBCRInd),
//            --:(item.transAmt), --leaving this...
//            :item.origAmt,    --using this for TRANSACTION_AMT
//            :item.currCodeOrig,
//            --:(item.funcCode),
//            :item.merchNum,
//            :item.aName,
//            :item.aCity,
//            decode( :item.aState,'PRI','PR',:item.aState ),
//            :item.aCountry,
//            :item.refNum,
//            --:(item.issBIN),
//            :item.authNum,
//            :item.reasonCode,
//            --:(item.sic),
//            --:(item.usageCode),
//            --:(item.partialInd),
//            --:(item.acqID),
//            --:(item.docInd),
//            :item.bizInd,
//            :item.bankNumber,
//            nvl(to_date(:filenameDateStr,'yymmdd'), trunc(sysdate)),
//            :load_filename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_retrieval_mc\n        (\n          load_sec,\n          --transaction_code,\n          --processing_code,\n          --pos_data_code,\n          account_number,\n          transaction_date,\n          --debit_credit_ind,\n          transaction_amt,\n          --original_amt,\n          tran_currency_code,\n          --function_code,\n          merchant_account_number,\n          merchant_name,\n          merchant_city,\n          merchant_state,\n          merchant_country_code,\n          ref_num,\n          --issuer_bin,\n          authorization_code,\n          reason_code,\n          --class_code,\n          --usage_code,\n          --partial_amt_ind,\n          --acquirer_institution_id,\n          --documentation_ind,\n          business_activity_ind,\n          bank_number,\n          actual_incoming_date,\n          load_filename\n        )\n        values\n        (\n           :1  ,\n          --:(item.procCode),\n          --:(item.procCode),\n          --:(item.posCode),\n           :2  ,\n          to_date( :3  ,'yymmdd'),\n          --:(item.DBCRInd),\n          --:(item.transAmt), --leaving this...\n           :4  ,    --using this for TRANSACTION_AMT\n           :5  ,\n          --:(item.funcCode),\n           :6  ,\n           :7  ,\n           :8  ,\n          decode(  :9  ,'PRI','PR', :10   ),\n           :11  ,\n           :12  ,\n          --:(item.issBIN),\n           :13  ,\n           :14  ,\n          --:(item.sic),\n          --:(item.usageCode),\n          --:(item.partialInd),\n          --:(item.acqID),\n          --:(item.docInd),\n           :15  ,\n           :16  ,\n          nvl(to_date( :17  ,'yymmdd'), trunc(sysdate)),\n           :18  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,item.loadSec);
   __sJT_st.setLong(2,item.acctNum);
   __sJT_st.setString(3,item.transDate);
   __sJT_st.setDouble(4,item.origAmt);
   __sJT_st.setString(5,item.currCodeOrig);
   __sJT_st.setLong(6,item.merchNum);
   __sJT_st.setString(7,item.aName);
   __sJT_st.setString(8,item.aCity);
   __sJT_st.setString(9,item.aState);
   __sJT_st.setString(10,item.aState);
   __sJT_st.setString(11,item.aCountry);
   __sJT_st.setString(12,item.refNum);
   __sJT_st.setString(13,item.authNum);
   __sJT_st.setString(14,item.reasonCode);
   __sJT_st.setString(15,item.bizInd);
   __sJT_st.setInt(16,item.bankNumber);
   __sJT_st.setString(17,filenameDateStr);
   __sJT_st.setString(18,load_filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1307^7*/

      addNoteToSB(NOTE_RETR);
    }
    catch( Exception e )
    {
      logEntry("processRetrieval()",e.toString());
    }
  }

  private void processFeeCollection()
  {
    java.sql.Date         batchDate       = null;
    String                cardNumber      = null;
    String                cardNumberEnc   = null;
    ResultSetIterator     it              = null;
    long                  merchantId      = 0L;
    ResultSet             resultSet       = null;
    String                settleRecId     = null;
    
    try
    {
      log.debug("processFeeCollection() insert");
      
      if ( item.refNum != null && item.refNum.trim().length() == 23 )
      {
        long  refNum = 0L;
        try { refNum = Long.parseLong(item.refNum.trim()); } catch(Exception e){}
        
        /*@lineinfo:generated-code*//*@lineinfo:1336^9*/

//  ************************************************************
//  #sql [Ctx] { select  yddd_to_date(substr(:refNum,8,4))
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  yddd_to_date(substr( :1  ,8,4))\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1341^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:1343^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number        as merchant_number,
//                    dt.cardholder_account_number      as card_number,
//                    dt.card_number_enc                as card_number_enc,
//                    mc.rec_id                         as rec_id
//            from    daily_detail_file_dt      dt,
//                    mc_settlement             mc
//            where   dt.merchant_account_number in 
//                    (
//                      select  mf.merchant_number 
//                      from    mbs_banks       mb,
//                              mif             mf
//                      where   substr(:refNum,2,6) in 
//                                ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,
//                                  mb.mc_bin_inet    ,mb.mc_bin_pin_debit )
//                              and mf.bank_number = mb.bank_number
//                    )
//                    and dt.batch_date between :batchDate-3 and :batchDate+1
//                    and dt.reference_number = :refNum
//                    and mc.batch_id(+) = dt.batch_number
//                    and mc.batch_record_id(+) = dt.ddf_dt_id
//                    and nvl(mc.test_flag,'Y') = 'N'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number        as merchant_number,\n                  dt.cardholder_account_number      as card_number,\n                  dt.card_number_enc                as card_number_enc,\n                  mc.rec_id                         as rec_id\n          from    daily_detail_file_dt      dt,\n                  mc_settlement             mc\n          where   dt.merchant_account_number in \n                  (\n                    select  mf.merchant_number \n                    from    mbs_banks       mb,\n                            mif             mf\n                    where   substr( :1  ,2,6) in \n                              ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,\n                                mb.mc_bin_inet    ,mb.mc_bin_pin_debit )\n                            and mf.bank_number = mb.bank_number\n                  )\n                  and dt.batch_date between  :2  -3 and  :3  +1\n                  and dt.reference_number =  :4  \n                  and mc.batch_id(+) = dt.batch_number\n                  and mc.batch_record_id(+) = dt.ddf_dt_id\n                  and nvl(mc.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,refNum);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setLong(4,refNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.utils.IPMExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1366^9*/
        resultSet = it.getResultSet();
      
        if ( resultSet.next() )
        {
          merchantId      = resultSet.getLong("merchant_number");
          cardNumber      = resultSet.getString("card_number");
          cardNumberEnc   = resultSet.getString("card_number_enc");
          settleRecId     = resultSet.getString("rec_id");
        }
        resultSet.close();
        it.close();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1380^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_settlement_fee_collect
//          (
//            merchant_number,
//            card_number,
//            card_number_enc,
//            settle_rec_id,
//            message_type,
//            function_code,
//            processing_code,
//            dest_ica,
//            reason_code,
//            settlement_date,
//            settlement_cycle,
//            recon_date,
//            recon_cycle,
//            transaction_date,
//            transaction_amount,
//            recon_amount,
//            conversion_rate,
//            currency_code,
//            recon_currency_code,
//            message_text,
//            card_program_id,
//            bsa_type,
//            bsa_id_code,
//            ird,
//            business_date,
//            business_cycle,
//            card_acceptor_override_ind,
//            product_class_override_ind,
//            settlement_flag,
//            settle_service_id,
//            settle_service_agent_acct,
//            settle_service_level_code,
//            settle_service_id_code,
//            settle_fx_rate_class_code,
//            reference_number,
//            load_file_id,
//            load_filename
//          )
//          values
//          (
//            :merchantId,
//            :cardNumber,
//            :cardNumberEnc,
//            :settleRecId,
//            :item.msgType,
//            :item.funcCode,
//            :item.procCode,
//            :item.icaNum,
//            :item.reasonCode,
//            parse_date(:item.settleDate,'yyMMdd'),
//            :item.settleCycle,
//            parse_date(:item.reconDate,'yyMMdd'),
//            :item.reconCycle,
//            parse_date(:item.transDate,'yyMMdd'),
//            :item.transAmt,
//            :item.reconAmt,
//            :item.convRate,
//            :item.currCode,
//            :item.reconCurrCode,
//            :item.msgText,
//            :item.cardProgramId,
//            :item.bsaType,
//            :item.bsaIdCode,
//            :item.ird,
//            parse_date(:item.businessDate,'yyMMdd'),
//            :item.businessCycle,
//            :item.cardAcceptorOverrideInd,
//            :item.productClassOverrideInd,
//            :item.settlementFlag,
//            :item.settleServiceId,
//            :item.settleServiceAgentAcct,
//            :item.settleServiceLevelCode,
//            :item.settleServiceIdCode,
//            :item.settleFxRateClassCode,
//            :item.refNum,
//            load_filename_to_load_file_id(:load_filename),
//            :load_filename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_settlement_fee_collect "
   					+ "(\n"
   					+ "  merchant_number,\n          card_number,\n          card_number_enc,\n"
   					+ "  settle_rec_id,\n          message_type,\n          function_code,\n          "
   					+ "  processing_code,\n          dest_ica,\n          reason_code,\n"
   					+ "  settlement_date,\n          settlement_cycle,\n          recon_date,\n "
   					+ "  recon_cycle,\n          transaction_date,\n          transaction_amount,\n "
   					+ "  recon_amount,\n          conversion_rate,\n          currency_code,\n "
   					+ "  recon_currency_code,\n          message_text,\n          card_program_id,\n "
   					+ "  bsa_type,\n          bsa_id_code,\n          ird,\n "
   					+ "  business_date,\n          business_cycle,\n          card_acceptor_override_ind,\n"
   					+ "  product_class_override_ind,\n          settlement_flag,\n          settle_service_id,\n "
   					+ "  settle_service_agent_acct,\n          settle_service_level_code,\n          settle_service_id_code,\n"
   					+ "  settle_fx_rate_class_code,\n          reference_number,\n          load_file_id,\n "
   					+ "  load_filename\n, fx_conv_date, fx_conv_ind ) "
   					+ "values"
   					+ "( "
   					+ "  :1  ,\n           :2  ,\n           :3  ,\n"
   					+ "  :4  ,\n           :5  ,\n           :6  ,\n  "
   					+ "  :7  ,\n           :8  ,\n           :9  ,\n  "
   					+ "  parse_date( :10  ,'yyMMdd'),\n           :11  ,\n          parse_date( :12  ,'yyMMdd'),\n "
   					+ "  :13  ,\n          parse_date( :14  ,'yyMMdd'),\n           :15  ,\n "
   					+ "  :16  ,\n           :17  ,\n           :18  ,\n   "
   					+ "  :19  ,\n           :20  ,\n           :21  ,\n   "
   					+ "  :22  ,\n           :23  ,\n           :24  ,\n  "
   					+ "  parse_date( :25  ,'yyMMdd'),\n           :26  ,\n           :27  ,\n   "
   					+ "  :28  ,\n           :29  ,\n           :30  ,\n  "
   					+ "  :31  ,\n           :32  ,\n           :33  ,\n   "
   					+ "  :34  ,\n           :35  ,\n          load_filename_to_load_file_id( :36  ),\n  "
   					+ "  :37  \n,  to_date(:38,'yymmdd'), :39      "
   					+ ")";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,cardNumber);
   __sJT_st.setString(3,cardNumberEnc);
   __sJT_st.setString(4,settleRecId);
   __sJT_st.setString(5,item.msgType);
   __sJT_st.setString(6,item.funcCode);
   __sJT_st.setString(7,item.procCode);
   __sJT_st.setString(8,item.icaNum);
   __sJT_st.setString(9,item.reasonCode);
   __sJT_st.setString(10,item.settleDate);
   __sJT_st.setInt(11,item.settleCycle);
   __sJT_st.setString(12,item.reconDate);
   __sJT_st.setInt(13,item.reconCycle);
   __sJT_st.setString(14,item.transDate);
   __sJT_st.setDouble(15,item.transAmt);
   __sJT_st.setDouble(16,item.reconAmt);
   __sJT_st.setDouble(17,item.convRate);
   __sJT_st.setString(18,item.currCode);
   __sJT_st.setString(19,item.reconCurrCode);
   __sJT_st.setString(20,item.msgText);
   __sJT_st.setString(21,item.cardProgramId);
   __sJT_st.setString(22,item.bsaType);
   __sJT_st.setString(23,item.bsaIdCode);
   __sJT_st.setString(24,item.ird);
   __sJT_st.setString(25,item.businessDate);
   __sJT_st.setInt(26,item.businessCycle);
   __sJT_st.setString(27,item.cardAcceptorOverrideInd);
   __sJT_st.setString(28,item.productClassOverrideInd);
   __sJT_st.setString(29,item.settlementFlag);
   __sJT_st.setString(30,item.settleServiceId);
   __sJT_st.setString(31,item.settleServiceAgentAcct);
   __sJT_st.setInt(32,item.settleServiceLevelCode);
   __sJT_st.setString(33,item.settleServiceIdCode);
   __sJT_st.setString(34,item.settleFxRateClassCode);
   __sJT_st.setString(35,item.refNum);
   __sJT_st.setString(36,load_filename);
   __sJT_st.setString(37,load_filename);
   __sJT_st.setString(38,item.currencyConversionDate);
   __sJT_st.setString(39,item.currencyConversionIndicator != null && item.currencyConversionIndicator != -1 ? CurrencyConversionType.getType(item.currencyConversionIndicator).getDbValue() : "");
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1462^7*/
    }
    catch( Exception e )
    {
      logEntry("processFeeCollection(" + load_filename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }

  //basically take some of the information and add it to the queue
  //need to add all reject codes into one item, so that it can be searched
  private void processReject(boolean useAll)
  throws Exception
  {

    //translate all existing error msgs
    item.processErrMsgs(useAll);

    //do reject queue placement
    insertRejectQueueItem();

    //clear item - this doesn't affect the errList
    //due to its transient nature
    item.cleanAll();
  }

  private void processReject()
  throws Exception
  {
    processReject(false);
  }

  private void insertRejectQueueItem()
  throws Exception
  {
    log.debug("running... insertRejectQueueItem()");

    Iterator rejects = item.getRejectIterator();
    log.debug("item isn't null...");

    //call this method to ensure we're using the proper recId
    //1st presentment - from mc_settlement
    //2nd presentment - from network_chargebacks_mc
    calibrateRecId();

    if(item.recId != -1)
    {

      hasRejects = true;

      String currReject;

      //add this item to the msg stringbuffer
      addNoteToSB(NOTE_REJECT);

      //since this is a possible 1:N relationship, the reject reasons are
      //held separately so that only one queue instance is needed regardless of
      //reject count
      int count = 0;

      //find our current count for this rec id
      /*@lineinfo:generated-code*//*@lineinfo:1526^7*/

//  ************************************************************
//  #sql [Ctx] { select count(distinct (round))
//          
//          from reject_record
//          where rec_id  = :item.recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(distinct (round))\n         \n        from reject_record\n        where rec_id  =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1532^7*/

      count++;

      while(rejects.hasNext())
      {
        log.debug("in rejects loop...");

        currReject = (String)rejects.next();

        //add detail to email
        messageSB.append(currReject).append(" ");

        //creation_date will default to sysdate
        /*@lineinfo:generated-code*//*@lineinfo:1546^9*/

//  ************************************************************
//  #sql [Ctx] { insert into REJECT_RECORD
//            (
//              REC_ID,
//              REJECT_ID,
//              REJECT_TYPE,
//              STATUS,
//              ROUND,
//              REJECT_DATE,
//              PRESENTMENT
//            )
//            values
//            (
//              :item.recId,
//              :currReject,
//              'MC',
//              -1,
//              :count,
//               nvl(to_date(:filenameDateStr,'yymmdd'), trunc(sysdate)),
//               :item.usageCode
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into REJECT_RECORD\n          (\n            REC_ID,\n            REJECT_ID,\n            REJECT_TYPE,\n            STATUS,\n            ROUND,\n            REJECT_DATE,\n            PRESENTMENT\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n            'MC',\n            -1,\n             :3  ,\n             nvl(to_date( :4  ,'yymmdd'), trunc(sysdate)),\n              :5  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.utils.IPMExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,item.recId);
   __sJT_st.setString(2,currReject);
   __sJT_st.setInt(3,count);
   __sJT_st.setString(4,filenameDateStr);
   __sJT_st.setString(5,item.usageCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1568^9*/

      }
      //finalize email data
      messageSB.append("\n");

      //then need to add item to q_data
      int toQueue = MesQueues.Q_REJECTS_UNWORKED;

      //usageCode == 2 means 2nd presentment
      if("2".equals(item.usageCode))
      {
        toQueue = MesQueues.Q_REJECTS_UNWORKED_2ND;
      }
      
      if ( QueueTools.hasItemEnteredQueue(item.recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
      {
        QueueTools.moveQueueItem(item.recId,
                                 QueueTools.getCurrentQueueType(item.recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                 toQueue,
                                 null,
                                 null);
      }
      else  //then need to add item to q_data
      {
        QueueTools.insertQueue(item.recId, toQueue);
      }
    }
    else
    {
      log.debug("Following item has recId = -1:\n"+item.toString());
    }
  }

  private void addNoteToSB(int type)
  {
    if(messageSB == null)
    {
      messageSB = new StringBuffer();
    }

    String nType = null;

    switch(type)
    {
      case NOTE_REJECT:

        if(messageSB.length()==0)
        {
          messageSB.append("The following rejects have just been added to the queues:\n");
        }

        String indicator = "C".equals(item.DBCRInd)?"CR":"DB";

        messageSB.append("\n");
        messageSB.append("\tMerch ID:  ").append(item.merchNum).append("\n");
        messageSB.append("\tRecord ID: ").append(item.recId).append("\n");
        messageSB.append("\tTrans Amt: ").append(MesMath.toCurrency(item.transAmt)).append(" (").append(indicator).append(")\n");
        messageSB.append("\tRej Codes: ");

        break;

      case NOTE_CB:
        nType = "chargeback";
      case NOTE_RETR:
        if(nType == null)
        {
          nType = "retrieval";
        }
        messageSB.append("The following ").append(nType).append(" has been added to the queue:\n");
        messageSB.append("\tMerch ID:  ").append(item.merchNum).append("\n");
        messageSB.append("\tCtrl Num:  ").append(item.loadSec).append("\n");
        messageSB.append("\tTrans Amt: ").append(MesMath.toCurrency(item.transAmt)).append("\n\n");

        break;

      default:
        //nothing to do
    };
  }


  private void notifyFailure()
  throws Exception
  {
    //first process the message
    item.processErrMsgs(true);

    try
    {

      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_MC_IPM_FILE_REJECT);
      msg.setSubject("Mastercard File Reject: " + load_filename);
      msg.setText("Complete File Failure");
      msg.send();

    }
    catch(Exception e)
    {
      logEntry("notifyFailure(" + load_filename + ")", e.toString());
      throw e;
    }
  }

  public void notifyMES()
  {
    try
    {

      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_MC_IPM_FILE_REJECT);
      StringBuffer subject = new StringBuffer("MBS MC File Notification: " + load_filename);

      if(messageSB != null)
      {
        if(hasRejects)
        {
          subject.append(" :: Rejects");
        }
        if(hasChargebacks)
        {
          subject.append(" :: Chargebacks");

          if(cbCrCount > 0)
          {
            messageSB.append("CR details for MC chargebacks:\n");
            messageSB.append("\tNumber:\t").append(cbCrCount).append("\n");
            messageSB.append("\tAmount:\t").append(MesMath.toCurrency(cbCrTotal)).append("\n");
          }
          if(cbDbCount > 0)
          {
            messageSB.append("DB details for MC chargebacks:\n");
            messageSB.append("\tNumber:\t").append(cbDbCount).append("\n");
            messageSB.append("\tAmount:\t").append(MesMath.toCurrency(cbDbTotal)).append("\n");
          }
          if(unkCount > 0)
          {
            messageSB.append("UNK details for MC chargebacks:\n");
            messageSB.append("\tNumber:\t").append(unkCount).append("\n");
            messageSB.append("\tAmount:\t").append(MesMath.toCurrency(unkTotal)).append("\n");
          }

        }
        if(hasRetrievals)
        {
          subject.append(" :: Retrievals");
        }
        msg.setSubject(subject.toString());
        msg.setText(messageSB.toString());

        msg.send();
      }

    }
    catch(Exception e)
    {
      logEntry("notify(" + load_filename + ")", e.toString());
    }
  }


  public void setTestFilename(String filename)
  {
    if(filename !=null)
    {
      this.filename = filename;
      isTest        = true;
    }
  }

  //Implementation method here
  public String getDataSource()
  {
    return filename;
  }

  public class MCItem
  {
    public Integer currencyConversionIndicator;

	public String currencyConversionDate;

	//holds reference to previous error msgs <IpmOfflineMessage>
    private LinkedList errList;

    //mes id
    public long   loadSec;
    public long   recId;
                            //Assoc Field
    public String msgType;     //00
    public long   acctNum;     //02
    public String procCode;    //03
    public String DBCRInd;     //--sub of 03
    public double transAmt;    //04
    public String transDate;   //12
    public String posCode;     //22
    public String funcCode;    //24
    public String reasonCode;  //25
    public String sic;         //26
    public double origAmt;     //30
    public String refNum;      //31
    public String acqBIN;      //-- sub of 31 1-7
    public long   acqRefNum;   //-- sub of 31 11-22
    public String acqID;       //33
    public String authNum;     //38
    public long   merchNum;    //42
    public String currCode;    //49
    public String bankNetRefNum; //-- sub of 63
    public String bankNetDate;   //-- sub of 63
    public String aName;       //-- sub of 43
    public String aAddr;       //-- sub of 43
    public String aCity;       //-- sub of 43
    public String aZip;        //-- sub of 43
    public String aState;      //-- sub of 43
    public String aCountry;    //-- sub of 43
    public String usageCode = null;   //-- sub of 24
    public String partialInd;  //-- sub of 24
    public String icaNum;      //93
    public String issBIN;      //94
    public String cbRefNum;    //95

    //FEE COLLECTION --
    public double reconAmt;     //05
    public double convRate;     //09
    public String reconCurrCode;//50
    public String msgText;      //72

    //PDS 158
    public String     cardProgramId;                //VARCHAR2 (3 Byte),
    public String     bsaType;                      //CHAR (1 Byte),
    public String     bsaIdCode;                    //VARCHAR2 (6 Byte),
    public String     ird;                          //VARCHAR2 (2 Byte),
    public String     businessDate;                 //DATE,
    public int        businessCycle;                //NUMBER (2),
    public String     cardAcceptorOverrideInd;      //CHAR (1 Byte),
    public String     productClassOverrideInd;      //VARCHAR2 (3 Byte),

    //PDS 159
    public String     settlementFlag;               //CHAR (1 Byte),
    public String     settleAgreementInfo;          //VARCHAR2 (30 Byte),
    public String     settleServiceId;              //VARCHAR2 (11 Byte),
    public String     settleServiceAgentAcct;       //VARCHAR2 (28 Byte),
    public int        settleServiceLevelCode;       //NUMBER (1),
    public String     settleServiceIdCode;          //VARCHAR2 (10 Byte),
    public String     settleFxRateClassCode;        //CHAR (1 Byte),
    public String     reconDate;
    public int        reconCycle;
    public String     settleDate;
    public int        settleCycle;
    //-- FEE COLLECTION end

    /**Private Data Subelements (PDS) (from DE048)**/
    //NAMED
    //23  = Terminal Type
    public String termType;
    //52  = Ecommerce Security Level Indicator
    public String ecommSecLvlInd;
    //148 = Currency Exponents
    public String currExponents;
    //149 = Currency Codes, Amounts, Original
    public String currCodeOrig;
    //158 = Business Activity
    public String bizInd;
    //165 = Settlement Indicator
    public String settleInd;
    //262 = Documentaton Indicator
    public String docInd;
    
    public boolean reversal     = false;    // pds 25
    
    public int     bankNumber   = 3941;     // default to MES

    //UNNAMED
    //2
    public String pds2;
    //3
    public String pds3;
    //43
    public String pds43;
    //80  =
    public String pds80;
    //173 =
    public String pds173;
    //191
    public String pds191;
    //192
    public String pds192;


    /**Private Data Subelements associated with 691 reject (from DE048) **/

    //5   = reject details
    public List rejectCodes    = null;
    //6   =
    public String pds6;
    //138 =
    public String pds138;
    //280 =
    public String pds280;
    
    public void cleanPrimary()
    {
      log.debug("running cleanPrimary()...");
      loadSec       = -1;
      recId         = -1;
      msgType       = "";
      acctNum       = 0;
      procCode      = "";
      DBCRInd       = "";
      transAmt      = 0d;
      transDate     = "";
      posCode       = "";
      funcCode      = "";
      reasonCode    = "";
      sic           = "";
      origAmt       = 0d;
      refNum        = "";
      authNum       = "";
      bankNetRefNum = "";
      bankNetDate   = "";
      merchNum      = 0;
      currCode      = "";
      aName         = "";
      aCity         = "";
      aState        = "";
      aCountry      = "";
      icaNum        = "";
      issBIN        = "";
      acqBIN        = "";
      bankNumber    = 3941;
      usageCode     = "0";
      partialInd    = null;
      acqID         = "";
      acqRefNum     = 0;
      cbRefNum      = "";
      termType      = "";
      ecommSecLvlInd= "";
      currExponents = "";
      currCodeOrig  = "";
      bizInd        = "";
      settleInd     = "";
      docInd        = "";
      pds2          = "";
      pds3          = "";
      pds43         = "";
      pds80         = "";
      pds173        = "";
      reversal      = false;    // pds 25
      currencyConversionIndicator = -1;
      currencyConversionDate = "";
    }

    //assuming that these fields show in 691
    public void cleanExceptions()
    {
      log.debug("running cleanExceptions()...");
      pds6            = "";
      pds138          = "";
      pds280          = "";

      //pds5
      if(rejectCodes!=null)
      {
        rejectCodes.clear();
      }

    }

    public void cleanAll()
    {
      cleanPrimary();
      cleanExceptions();
    }


    public void setRefNumAttr()
    {
      try
      {
        acqBIN      = refNum.substring(1,7);
        acqRefNum   = Long.parseLong(refNum.substring(11,22));
        bankNumber  = resolveBankNumber(acqBIN);
      }
      catch(Exception e)
      {
        acqBIN      = "";
        acqRefNum   = 0;
        bankNumber  = 3941;
      }
    }

    public void addErrMsg(IpmOfflineMessage tempErrMsg)
    {
      log.debug("in addErrMsg...");

      if(errList == null)
      {
        errList = new LinkedList();
      }
      errList.add(tempErrMsg);
    }

    public void processErrMsgs()
    {
      processErrMsgs(false);
    }

    public void processErrMsgs(boolean useAll)
    {
      log.debug("in processErrMsgs");

      IpmOfflineMessage held=null;

      if(errList!=null)
      {
        if(!useAll)
        {
          //need to grab and hold the last entry as this one applies
          //to the next item in the queue, not the one being processed
          held = (IpmOfflineMessage)errList.removeLast();
        }

        IpmOfflineMessage temp;

        for(Iterator itr = errList.iterator(); itr.hasNext();)
        {
          temp = (IpmOfflineMessage)itr.next();
          setRejectDetails(temp.getPrivateDataSubelement(5));
        }

        //empty the list
        errList.clear();

        if(!useAll && held!=null)
        {
          //add back in the last ipm msg
          log.debug("adding HELD item back in...");
          errList.add(held);
        }

      }

      /*Do we need these? If so, how to handle given multiple 691s...
      //6   =
      pds6           = tempErrMsg.getPrivateDataSubelement(6);
      //138 =
      pds138         = tempErrMsg.getPrivateDataSubelement(138);
      //280 =
      pds280         = tempErrMsg.getPrivateDataSubelement(280);
      */
    }

    public boolean hasErrors()
    {
      return errList!=null && errList.size()>0;
    }

   /*
    Breakdown of pds5 elements

    1-5   Data Element ID     an-5
    6-7   Error Severity Code n-2
    8-11  Error Message Code  an-4
    12-14 Subfield ID         n-3

    */
    private void setRejectDetails(String _pds5)
    {
      log.debug(_pds5);

      if(_pds5 != null)
      {
        //this is a 14-140 length field, in sections of 14
        //each 14 length string represents a reject detail
        int size = _pds5.length()/14;
        int multiplier;

        //new list
        if(rejectCodes==null)
        {
          rejectCodes = new LinkedList();
        }

        String rejectCode;
        for(int i = 0; i < size; i++)
        {
          multiplier = 14*i;
          //rejectCode is found at every (7x,11x) spot on the line
          rejectCode = _pds5.substring(7+multiplier,11+multiplier);
          rejectCodes.add(rejectCode);
        }
      }
    }

    public String getRejectReasonStr()
    {
      if(rejectCodes == null || rejectCodes.size()==0)
      {
        return null;
      }

      StringBuffer sb = new StringBuffer();
      for(Iterator itr = rejectCodes.iterator();itr.hasNext();)
      {
        sb.append((String)itr.next()).append(",");
      }
      //return the contents minus the trailing comma
      return sb.substring(0, sb.length()-1);
    }

    public Iterator getRejectIterator()
    {
      if(rejectCodes == null || rejectCodes.size()==0)
      {
        return null;
      }
      return rejectCodes.iterator();
    }

    public void decodeFuncCode()
    {
      if(item.funcCode==null)
      {
        return;
      }

      int intCode;

      try
      {
        intCode = Integer.parseInt(item.funcCode);

        switch(intCode)
        {
          //first presentment
          case 200:
            usageCode = "0";
            break;

          //second presentment, full
          case 205:
            usageCode = "2";
            break;

          //second presentment, partial
          case 282:
            usageCode = "2";
            partialInd = "I";
            break;

          //first time CB, full
          case 450:
            usageCode = "1";
            break;

          //arbitration CB, full
          case 451:
            usageCode = "3";
            break;

          //first time CB, partial
          case 453:
            usageCode = "1";
            partialInd = "I";
            break;

          //arbitration CB, partial
          case 454:
            usageCode = "3";
            partialInd = "I";
            break;

          default:
            break;
        };

      }
      catch(Exception e)
      {
        usageCode     = "0";
        partialInd    = null;
      }
    }

    public int getMsgType()
    {
      int type = 0;
      try
      {
        type = Integer.parseInt(msgType);
      }
      catch(NumberFormatException nfe)
      {}

      return type;
    }

    public int getMsgSubType()
    {
      int subType = 0;
      try
      {
        subType = Integer.parseInt(funcCode);
      }
      catch(NumberFormatException nfe)
      {}

      return subType;
    }

    public void extractAddressData(String rawData)
    throws Exception
    {
      if(rawData==null)
      {
        return;
      }

      //
      // first three fields are variable length separated by \ character
      //    field[0] = DBA Name
      //    field[1] = DBA Address
      //    field[2] = DBA City
      //
      // split on '\'
      String[] firstRun  = rawData.split("\\\\");

      item.aName        = firstRun[0];
      item.aAddr        = firstRun[1];
      item.aCity        = firstRun[2];
      
      // last three fields are fixed length
      item.aZip     = firstRun[3].substring(0,10).trim();
      item.aState   = firstRun[3].substring(10,13).trim();
      item.aCountry = firstRun[3].substring(13,16).trim();
    }

    //FEE COLLECTION
    public void setConvRate(String raw)
    {
      try
      {
        int multiplier = Integer.parseInt(raw.substring(0,1));
        long rawRate   = Long.parseLong(raw.substring(1));
        log.debug("M = "+multiplier+" || R = "+rawRate+" eq = "+(Math.pow(10,multiplier)));
        convRate       = rawRate/(Math.pow(10,multiplier));
      }
      catch(Exception e)
      {
        convRate = 1.00d;
      }
    }

    //158='DMC*2*010001*  *100617*01* *   *NNNNN' || Business Activity: * added to show separation
    public void setBizActivity(String raw)
    {
      if(raw != null)
      {
        cardProgramId             = raw.substring(0,3);
        bsaType                   = raw.substring(3,4);
        bsaIdCode                 = raw.substring(4,10);
        ird                       = raw.substring(10,12);
        businessDate              = raw.substring(12,18);
        try { businessCycle = Integer.parseInt(raw.substring(18,20)); }catch(Exception e){ businessCycle = 0; }
        cardAcceptorOverrideInd   = raw.substring(20,21);
        productClassOverrideInd   = raw.substring(21,24);
      }
    }

    //159='12093      30004543                    1US00000001N1006170110061701' || Settlement Data
    public void setSettlementData(String raw)
    {
      if(raw != null)
      {
        settlementFlag            = "Y";  //setting this to Y simply because the PDS data was found
        //public String     settleAgreementInfo;
        settleServiceId           = raw.substring(0,11).trim();
        settleServiceAgentAcct    = raw.substring(11,39).trim();
        try { settleServiceLevelCode = Integer.parseInt(raw.substring(39,40));}catch(Exception e){ settleServiceLevelCode = 0; }
        settleServiceIdCode       = raw.substring(40,50);
        settleFxRateClassCode     = raw.substring(50,51);
        reconDate                 = raw.substring(51,57);
        try { reconCycle = Integer.parseInt(raw.substring(57,59));}catch(Exception e){ reconCycle = 0; }
        settleDate                = raw.substring(59,65);
        try { settleCycle = Integer.parseInt(raw.substring(65,66));}catch(Exception e){ settleCycle = 0; }
      }
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("loadSec       = ").append(loadSec      ).append("\n");
      sb.append("recId         = ").append(recId        ).append("\n");
      sb.append("msgType       = ").append(msgType      ).append("\n");
      sb.append("acctNum       = ").append(acctNum      ).append("\n");
      sb.append("procCode      = ").append(procCode     ).append("\n");
      sb.append("DBCRInd       = ").append(DBCRInd      ).append("\n");
      sb.append("transAmt      = ").append(transAmt     ).append("\n");
      sb.append("transDate     = ").append(transDate    ).append("\n");
      sb.append("posCode       = ").append(posCode      ).append("\n");
      sb.append("funcCode      = ").append(funcCode     ).append("\n");
      sb.append("reasonCode    = ").append(reasonCode   ).append("\n");
      sb.append("sic           = ").append(sic          ).append("\n");
      sb.append("origAmt       = ").append(origAmt      ).append("\n");
      sb.append("refNum        = ").append(refNum       ).append("\n");
      sb.append("authNum       = ").append(authNum      ).append("\n");
      sb.append("merchNum      = ").append(merchNum     ).append("\n");
      sb.append("currCode      = ").append(currCode     ).append("\n");
      sb.append("aName         = ").append(aName        ).append("\n");
      sb.append("aCity         = ").append(aCity        ).append("\n");
      sb.append("aState        = ").append(aState       ).append("\n");
      sb.append("aCountry      = ").append(aCountry     ).append("\n");
      sb.append("acqBIN        = ").append(acqBIN       ).append("\n");
      sb.append("bankNumber    = ").append(bankNumber   ).append("\n");
      sb.append("usageCode     = ").append(usageCode    ).append("\n");
      sb.append("partialInd    = ").append(partialInd   ).append("\n");
      sb.append("acqID         = ").append(acqID        ).append("\n");
      sb.append("acqRefNum     = ").append(acqRefNum    ).append("\n");
      sb.append("cbRefNum      = ").append(cbRefNum     ).append("\n");
      sb.append("termType      = ").append(termType     ).append("\n");
      sb.append("currExponents = ").append(currExponents).append("\n");
      sb.append("ecommSecLvlInd = ").append(ecommSecLvlInd).append("\n");
      sb.append("bankNetRefNum = ").append(bankNetRefNum).append("\n");
      sb.append("bankNetDate   = ").append(bankNetDate).append("\n");
      sb.append("icaNum        = ").append(icaNum).append("\n");
      sb.append("currCodeOrig  = ").append(currCodeOrig ).append("\n");
      sb.append("bizInd        = ").append(bizInd       ).append("\n");
      sb.append("settleInd     = ").append(settleInd    ).append("\n");
      sb.append("docInd        = ").append(docInd       ).append("\n");
      sb.append("issBIN        = ").append(issBIN       ).append("\n");
      sb.append("pds2          = ").append(pds2         ).append("\n");
      sb.append("pds3          = ").append(pds3         ).append("\n");
      sb.append("pds43         = ").append(pds43        ).append("\n");
      sb.append("pds80         = ").append(pds80        ).append("\n");
      sb.append("pds173        = ").append(pds173       ).append("\n");
      if(rejectCodes != null)
      {
        for(int i=0;i<rejectCodes.size();i++)
        {
          sb.append("reject code ").append(i).append("    = ").append((String)rejectCodes.get(i)).append("\n");
        }
      }
      sb.append("pds6          = ").append(pds6         ).append("\n");
      sb.append("pds138        = ").append(pds138       ).append("\n");
      sb.append("pds280        = ").append(pds280       ).append("\n");
      return sb.toString();
    }
  }
  
  public void logEntry(String method, String message)
  {
    try
    {
      MailMessage.sendSystemErrorEmail("IPM Extractor Error - " + load_filename, method + "\n" + message);
    }
    catch(Exception e)
    {
    }
    super.logEntry(method, message);
  }
  
  public int resolveBankNumber( String binNumber )
  {
    int     bankNumber    = 3941;   // default to MES
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2330^7*/

//  ************************************************************
//  #sql [Ctx] { select  mb.bank_number
//          
//          from    mbs_banks     mb
//          where   :binNumber in
//                  ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,
//                    mb.mc_bin_inet    ,mb.mc_bin_pin_debit )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mb.bank_number\n         \n        from    mbs_banks     mb\n        where    :1   in\n                ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,\n                  mb.mc_bin_inet    ,mb.mc_bin_pin_debit )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.utils.IPMExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,binNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2338^7*/
    }
    catch( Exception e )
    {
      // issue system warning
      logEntry("resolveBankNumber(" + binNumber + "," + load_filename + ")",e.toString());
    }        
    return( bankNumber );
  }

  private void getFiles()
  {
    //TODO
    //once we know the mech by which to retrieve files
    //we'll use this to build list and ignore the List constructor
  }

}/*@lineinfo:generated-code*/