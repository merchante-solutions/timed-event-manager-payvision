/*@lineinfo:filename=CBTPricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/CBTPricing.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:39p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class CBTPricing extends PricingGridBean
{
  private int         gridType              = 0;
  private double      bundledRate           = 0.0;
  private long        assocNumber           = 0;
  private long        hierarchyNode         = 0L;
  private boolean     servicePointsFound    = false;
  private boolean     userHasServicePoints  = false;
  
  private double      serviceRate           = 0.0;
  private double      baseRate              = 0.0;
  private double      profitMargin          = 0.0;
  
  public boolean validate(long userNode)
  {
    if(gridType == 0)
    {
      addError("Please choose a pricing grid (Retail/MOTO/Lodging)");
    }
    
    if(annualSales == 0.0)
    {
      addError("Please provide the projected annual v/MC sales for this merchant");
    }
    
    if(avgTicket == 0.0)
    {
      addError("Please provide the projected average ticket amount for this merchant");
    }
    
    // see if this user has service points assigned
    int pointCount = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:69^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(basis_points) 
//          from    cbt_service_points
//          where   hierarchy_node = :userNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(basis_points)  \n        from    cbt_service_points\n        where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.CBTPricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pointCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      userHasServicePoints = (pointCount > 0);
    }
    catch(Exception e)
    {
      logEntry("validate() - userHasServicePoints", e.toString());
    }
    
    return(!hasErrors());
  }
  
  public void calculateBundledRate(long userNode)
  {
    double                totalRate;
    double                bankProfit;
    int                   servicePoints     = 0;
    
    ResultSetIterator     it                = null;
    
    try
    {
      // establish base rate
      baseRate = (double)getBasisPoints(gridType, avgTicket, annualSales);
      
      if(baseRate == -1)
      {
        // pricing outside grid
        this.bundledRate = -1;
      }
      else
      {
        // convert base rate to a percentage (from basis points)
        baseRate /= 100;
        
        // establish "service points"
        this.serviceRate = 0.0;
        
        if(this.hierarchyNode == 0L)
        {
          this.hierarchyNode = userNode;
        }
        
        // get service points from grid
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    basis_points
//            from      cbt_service_points
//            where     hierarchy_node = :hierarchyNode and
//                      annual_sales <= :annualSales
//            order by  annual_sales desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    basis_points\n          from      cbt_service_points\n          where     hierarchy_node =  :1  and\n                    annual_sales <=  :2 \n          order by  annual_sales desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.CBTPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setLong(2,annualSales);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.utils.CBTPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
        
        ResultSet rs = it.getResultSet();
        if(rs.next())
        {
          // take the first entry on the list
          servicePoints = rs.getInt("basis_points");
          servicePointsFound = true;
        }
        
        it.close();
        
        this.serviceRate = (double)servicePoints / 100.0;
        
        // add them all up to get the bundled rate
        bundledRate = baseRate + serviceRate + profitMargin;
      }
    }
    catch(Exception e)
    {
      addError(e.toString());
    }
  }
  
  public boolean userHasServicePoints()
  {
    return this.userHasServicePoints;
  }
  
  public void setGridType(int gridType)
  {
    this.gridType = gridType;
  }
  public void setGridType(String gridType)
  {
    try
    {
      setGridType(Integer.parseInt(gridType));
    }
    catch(Exception e)
    {
      logEntry("setGridType(" + gridType + ")", e.toString());
    }
  }
  public String getGridType(int gridType)
  {
    String result = "";
    if(gridType == this.gridType)
    {
      result = "checked";
    }
    
    return result;
  }
  
  public void setProfitMargin(String profitMargin)
  {
    this.profitMargin = getDigitsWithDecimal(profitMargin);
  }
  public String getProfitMargin()
  {
    String result = "";
    
    if(this.profitMargin > 0.0)
    {
      result = String.valueOf(this.profitMargin);
    }
    
    return result;
  }
  
  public double getBundledRate()
  {
    return this.bundledRate;
  }
  
  public double getBaseRate()
  {
    return this.baseRate;
  }
  
  public double getProfitMarginValue()
  {
    return this.profitMargin;
  }
  
  public double getServiceRate()
  {
    return this.serviceRate;
  }
  
  public boolean servicePointsFound()
  {
    return this.servicePointsFound;
  }
  
  public void setAssocNumber(String assocNumber)
  {
    this.assocNumber = getDigits(assocNumber);
    this.hierarchyNode = 3858000000L + this.assocNumber;
  }
  public long getAssocNumber()
  {
    return this.assocNumber;
  }
  public String getAssocNumberString()
  {
    String result = "";
    if(this.assocNumber > 0)
    {
      result = String.valueOf(assocNumber);
    }
    
    return result;
  }
  
}/*@lineinfo:generated-code*/