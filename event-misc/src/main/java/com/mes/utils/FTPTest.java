/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/FTPTest.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/11/01 3:20p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import org.apache.log4j.Logger;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.config.MesDefaults;

public class FTPTest
{
  
  static Logger log = Logger.getLogger(FTPTest.class);
  
  private String  host        = "";
  private String  user        = "";
  private String  password    = "";
  private String  folder      = "";
  
  private boolean submitted   = false;
  
  private String error = "FTP Transfer success";
  
  private static String testFile = "This is a test file for use in testing the ftp process";
  
  public FTPTest()
  {
    try {
      host        = MesDefaults.getString(MesDefaults.FTP_TEST_HOST);
      user        = MesDefaults.getString(MesDefaults.FTP_TEST_USER);
      password    = MesDefaults.getString(MesDefaults.FTP_TEST_PASSWORD);
      folder      = MesDefaults.getString(MesDefaults.FTP_TEST_PATH);
    } catch (Exception e) {
      log.error("FTPTest() constructor - Error in retrieving Credentials", e);
    }
  }
  
  public void submitData()
  {
    try
    {
      // instantiate the ftp client
      System.out.println("* Instantiating ftp client");
      FTPClient ftp = new FTPClient(host, 21);
      
      // set timeout
      ftp.setTimeout(4000);
      
      // turn on debugging
      ftp.debugResponses(true);
      
      // log in to server
      System.out.println("* Logging in to server");
      ftp.login(user, password);
      
      // set connect mode to active
      System.out.println("* Changing connect mode");
      ftp.setConnectMode(FTPConnectMode.ACTIVE);
      
      // set transfer type to ascii
      System.out.println("* Changing tranfer type");
      ftp.setType(FTPTransferType.BINARY);
      
      // change directory
      System.out.println("* Changing directory");
      ftp.chdir(folder);
      
      // send the file
      System.out.println("* Sending file");
      ftp.put(testFile.getBytes(), "test.txt");
      
      // close
      System.out.println("* Killing ftp session");
      ftp.quit();
    }
    catch(Exception e)
    {
      error = e.toString();
    }
  }
  
  public void setHost(String host)
  {
    this.host = host;
  }
  public String getHost()
  {
    return this.host;
  }
  public void setUser(String user)
  {
    this.user = user;
  }
  public String getUser()
  {
    return this.user;
  }
  public void setPassword(String password)
  {
    this.password = password;
  }
  public String getPassword()
  {
    return this.password;
  }
  public void setFolder(String folder)
  {
    this.folder = folder;
  }
  public String getFolder()
  {
    return this.folder;
  }
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public String getError()
  {
    return this.error;
  }
}
