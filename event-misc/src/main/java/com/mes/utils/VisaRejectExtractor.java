/*@lineinfo:filename=VisaRejectExtractor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/utils/VisaRejectExtractor.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-05-13 13:32:40 -0700 (Wed, 13 May 2015) $
  Version            : $Revision: 23625 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.queues.QueueTools;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.FileUtils;
import sqlj.runtime.ResultSetIterator;
public class VisaRejectExtractor extends SQLJConnectionBase implements DataExtractor
{
  static Logger log = Logger.getLogger(VisaRejectExtractor.class);

  private boolean       isTest          = false;
  private String        filename        = null;
  private List          filenames       = null;
  private List          processItems    = null;
  private String        filenameDateStr = "";
  private String        LoadFilename    = null;
  //private List          reportItems   = null;

  //this is a class level object to handle reject information for the notifyMES call
  private StringBuffer messageSB    = null;
  private int     cbCrCount         = 0;
  private double  cbCrTotal         = 0.0d;
  private int     cbDbCount         = 0;
  private double  cbDbTotal         = 0.0d;
  private int     unkCount          = 0;
  private double  unkTotal          = 0.0d;


  //test case constructor
  public VisaRejectExtractor()
  {
    filenames = new ArrayList();
    filenames.add("c:"+File.separator+"work"+File.separator+"REJDITEM1.dat");
  }

  //production
  public VisaRejectExtractor(List filenames)
  {
    this.filenames = filenames;
  }


  public boolean extract()
  {
    boolean result = false;
    boolean notify = false;

    System.out.println("running...");

    try
    {
      if(filenames != null && filenames.size()>0)
      {
        connect();

        setAutoCommit(false);

        for(Iterator itr = filenames.iterator(); itr.hasNext();)
        {
          filename = (String)itr.next();

          // retrieval requests use a different format of flat file record
          if( filename.indexOf("vs_retr") >= 0 )
          {
            processRetrievalRequests();
          }
          else if ( filename.indexOf("vs_fee") >= 0 )
          {
            processFeeCollectAndFundDisbursement();
          }
          else if ( filename.indexOf("vs_msg") >= 0 )
          {
            processMerchantProfileService();
          }
          else  // processing one of the files using the common flat file recs
          {
            setLoadFilename();

            //builds the  data from the file
            extractData();

            //DEPRECATED 12/09
            //builds the corresponding reject codes from the .txt
            //extractRejectCodes();

            //mashes the two together and
            if(LoadFilename.indexOf("vs_chgbk")>-1)
            {
              log.debug("found chargeback file...");

              //inserts the chargebacks
              processChargebacks();
            }
            else if( LoadFilename.indexOf("vs_reject") > -1 ||
                     LoadFilename.indexOf("vs_return") > -1 )
            {
              log.debug("found reject/return file...");

              //inserts the rejects
              processRejects();
            }
            notify = true;
          }

          if (notify)
          {
            log.debug("messaging...");
            notifyMES();
          }
        }
        commit();

        result = true;
      }

    }
    catch (Exception exn)
    {
        //log.debug("error while parsing file", exn);
        exn.printStackTrace();

        //rolling back the entire set
        rollback();

        throw new RuntimeException(exn);
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

/*
DEF_TYPE_VISA_TC05_TCR0             = 400;
DEF_TYPE_VISA_TC05_TCR1             = 401;
DEF_TYPE_VISA_TC05_TCR3_LODGING     = 403;
DEF_TYPE_VISA_TC05_TCR3_CAR_RENTAL  = 404;
DEF_TYPE_VISA_TC05_TCR5             = 405;
DEF_TYPE_VISA_TC05_TCR6             = 406;
DEF_TYPE_VISA_TC91_TCR0             = 411;
DEF_TYPE_VISA_TC92_TCR0             = 412;

0500 = DEF_TYPE_VISA_TC05_TCR0
0501 = DEF_TYPE_VISA_TC05_TCR1
0505 = DEF_TYPE_VISA_TC05_TCR5

0600 = ?
0601 = ?

050044322
0501
050528917
050048888
0501
050500917
910043588
060042642
0601
910043588
050042665
0501
050500917
910043588
920043588
*/

 private void extractData()
 throws Exception
 {

    String line;
    BufferedReader br     = getReader();

    //holds complete list of ff records to make a single reject
    FFProcessor ffp       = null;
    FlatFileRecord ffr    = null;

    while((line=br.readLine()) != null)
    {
      //log.debug(line);
      line = StringUtilities.leftJustify(line,168,' ');
      String procType   = line.substring(0,2);
      int type          = Integer.parseInt(line.substring(1,4));

      if ( "91".equals(procType) || "92".equals(procType) )
      {
        continue;   // skip trailer records
      }

      //build the record
      switch(type)
      {
        case 100:   // returned credit
        case 200:   // returned debit
        case 500:   // rejected sales draft
        case 600:   // rejected credit voucher
        case 700:   // rejected cash advance
          //assuming that 0500/0600 indicates the beginning of new reject record
          if(ffp != null)
          {
            if(processItems == null)
            {
              processItems = new LinkedList();
            }
            processItems.add(ffp);
          }

          ffp = new FFProcessor(procType);

          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0);
          break;

        case 101:
        case 201:
        case 501:
        case 601:
        case 701:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC05_TCR1);
          break;
          
        case 504:
        case 604:
        case 704:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC05_TCR4);  // DEF_TYPE_VISA_TC05_TCR4                   = 408; 
          break;
        case 105:
        case 205:
        case 505:
        case 605:
        case 705:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC05_TCR5);
          break;
        /*
        case 600:
        case 601:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0);
          break;
        */

        case 109:
        case 209:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_TC01_TCR9);
          break;

        case 509:
        case 609:
        case 709:
          ffr = new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_VISA_REJECT_TCR9);
          break;

        default:
          ffr = null;
      };

      if(ffr != null)
      {
        ffr.suck(StringUtilities.leftJustify(line, 168, ' '));
        //ffr.showData();
        ffp.setRecord(ffr);
      }

    }

    //add final one - you know it's there.
    if( ffp != null)
    {
      if(processItems == null)
      {
        processItems = new LinkedList();
      }

      processItems.add(ffp);
    }
  }
  
  private boolean isValidMid( int bankNumber, long merchantId )
  {
    int       recCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:326^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mif   mf
//          where   mf.bank_number = :bankNumber
//                  and mf.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mif   mf\n        where   mf.bank_number =  :1  \n                and mf.merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^7*/
    }
    catch( Exception e )
    {
      logEntry("isValidMid(" + bankNumber + "," + merchantId + ")",e.toString());
    }
    return( recCount != 0 );
  }

  private void processFeeCollectAndFundDisbursement()
    throws Exception
  {
    int               defType       = -1;
    FlatFileRecord[]  ffds          = new FlatFileRecord[3];
    int               idx           = 0;
    BufferedReader    in            = getReader();
    String            line          = null;
    FlatFileRecord[]  outputRecs    = new FlatFileRecord[3];
    String            tcr           = null;

    // create a id for this file
    /*@lineinfo:generated-code*//*@lineinfo:354^5*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:filename)
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n    \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^5*/

    for( int i = 0; i < ffds.length; ++i )
    {
      switch( i )
      {
        case 0:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC10_TCR0; break;
        case 1:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC10_TCR2; break;
        case 2:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC10_TCR4; break;
        default:  continue;
      }
      ffds[i]       = new FlatFileRecord(Ctx,defType);
      outputRecs[i] = null;
    }

    while( (line = in.readLine()) != null )
    {
      tcr = line.substring(2,4);

           if ( "00".equals(tcr) )  { idx = 0;  }
      else if ( "01".equals(tcr) )  { idx = 1;  }
      else if ( "04".equals(tcr) )  { idx = 2;  }
      else                          { idx = -1; }

      if( idx >= 0 )
      {
        if ( idx == 0 && outputRecs[0] != null )
        {
          storeFeeCollect(outputRecs);

          // reset the output record group
          for( int i = 0; i < outputRecs.length; ++i )
          {
            outputRecs[i] = null;
          }
        }

        // extract the data from this line
        ffds[idx].suck(StringUtilities.leftJustify(line, 168, ' '));
        outputRecs[idx] = ffds[idx];
      }
    }   // end while loop

    // boundary condition, store the last record group
    if ( outputRecs[0] != null )
    {
      storeFeeCollect(outputRecs);
    }
  }

  private String[] extractPotentialRefNums( String msgText )
  {
    String[]    retVal          = null;
    String      vsBinNumber     = null;

    try {
      /*@lineinfo:generated-code*//*@lineinfo:413^7*/

//  ************************************************************
//  #sql [Ctx] { select vs_bin_base_ii 
//          
//          from   mbs_banks
//          where  :msgText like '%' || vs_bin_base_ii || '%'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select vs_bin_base_ii \n         \n        from   mbs_banks\n        where   :1   like '%' || vs_bin_base_ii || '%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,msgText);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vsBinNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:419^7*/

      if( vsBinNumber != null )
      {
        retVal = new String[] { msgText.substring(msgText.indexOf(vsBinNumber)-1, msgText.indexOf(vsBinNumber)+22) };
      }
    }
    catch( Exception ee ) { } //ignore

    return( retVal );
  }

  private void storeFeeCollect( FlatFileRecord[] ffds )
    throws Exception
  {
    String                authCode        = null;
    ResultSetIterator     it              = null;
    long                  merchantId      = 0L;
    String                msgText         = null;
    long                  recId           = 0L;
    String                refNum          = null;
    String[]              refNums         = null;
    String                rolNum          = null;
    Date                  settlementDate  = getSettlementDate(ffds[0]);
  
    
    msgText = ffds[0].getFieldData("message_text");
     
     
    if ( msgText.startsWith("PARB") || msgText.startsWith("PCOMP") )
    {
      refNums = new String[] { msgText.substring(40,63) };
      rolNum  = msgText.substring(27,39);
    }
    else if ( msgText.startsWith("ROL") || msgText.startsWith("VCR") )
    {
      try { 
        rolNum = msgText.substring(4,14); 
        if( msgText.startsWith("VCR") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:459^11*/

//  ************************************************************
//  #sql [Ctx] it = { select ncb.reference_number
//              from   network_chargeback_visa ncb
//              where  ncb.actual_incoming_date >= sysdate - 180
//                     and ncb.merchant_account_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where processor_id = 1))
//                     and ncb.vrol_case_number = :rolNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ncb.reference_number\n            from   network_chargeback_visa ncb\n            where  ncb.actual_incoming_date >= sysdate - 180\n                   and ncb.merchant_account_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where processor_id = 1))\n                   and ncb.vrol_case_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rolNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.utils.VisaRejectExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^11*/

          ResultSet rs = it.getResultSet();

          if ( rs.next() )
          {
            refNums = new String[] { rs.getString("reference_number") };
          }
          rs.close();
          it.close();
        }
      } 
      catch( Exception ee ) 
      { 
        log.debug("Exception while trying to extract case number from message text");
      }
    }

    if( refNums == null )
    {
      // search for possible reference numbers in the free text
      refNums = extractPotentialRefNums(msgText);
    }

    if ( refNums != null )
    {
      for( int i = 0; i < refNums.length; ++i )
      {
        refNum = refNums[i];
        
        if (!isNumeric(refNum)) {
          refNum = null;
          continue;
        }

        // attempt to locate the original record
        try { 
          /*@lineinfo:generated-code*//*@lineinfo:503^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.*
//              from    visa_settlement   vs
//              where   vs.acq_reference_number  = substr(:refNum,12,11)
//                      and vs.bin_number = substr(:refNum,2,6)
//                      and yddd_to_date(substr(:refNum,8,4)) between vs.batch_date-2 and vs.batch_date+6
//                      and nvl(vs.test_flag,'Y') = 'N'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vs.*\n            from    visa_settlement   vs\n            where   vs.acq_reference_number  = substr( :1  ,12,11)\n                    and vs.bin_number = substr( :2  ,2,6)\n                    and yddd_to_date(substr( :3  ,8,4)) between vs.batch_date-2 and vs.batch_date+6\n                    and nvl(vs.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,refNum);
   __sJT_st.setString(2,refNum);
   __sJT_st.setString(3,refNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.utils.VisaRejectExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^11*/
          ResultSet rs = it.getResultSet();

          if ( rs.next() )
          {
            merchantId    = rs.getLong("merchant_number");
            recId         = rs.getLong("rec_id");
          }
          rs.close();
          it.close();
        } 
        catch( Exception ex ) 
        {
          log.debug("Exception while trying to match settlement record");
        }

        if ( merchantId != 0L )
        {
          break;
        }
      }
    }

    /*@lineinfo:generated-code*//*@lineinfo:534^5*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_settlement_fee_collect
//        (
//          merchant_number,
//          settle_rec_id,
//          tran_code,
//          dest_bin,
//          source_bin,
//          reason_code,
//          country_code,
//          settlement_date,
//          transaction_date,
//          card_number,
//          card_number_ext,
//          transaction_amount,
//          currency_code,
//          debit_credit_indicator,
//          message_text,
//          reference_number,
//          rol_number,
//          settlement_flag,
//          auth_tran_id,
//          reimbursement_attribute,
//          settlement_type,
//          national_reimbursement_fee,
//          installment_payment_count,
//          business_format_code,
//          promotion_type,
//          promotion_code,
//          card_number_enc,
//          load_filename,
//          load_file_id
//        )
//        values
//        (
//          :merchantId,
//          :recId,
//          :ffds[0].getFieldData("tran_code"),
//          :ffds[0].getFieldData("dest_bin"),
//          :ffds[0].getFieldData("source_bin"),
//          :ffds[0].getFieldData("reason_code"),
//          :ffds[0].getFieldData("country_code"),
//          :settlementDate,
//          to_date( :ffds[0].getFieldData("event_date"),'mmdd' ),
//          :ffds[0].getFieldData("card_number"),
//          :ffds[0].getFieldData("card_number_ext"),
//          :ffds[0].getFieldData("dest_amount") * 0.01,
//          :ffds[0].getFieldData("dest_currency_code"),
//          : "10".equals(ffds[0].getFieldData("tran_code")) ? "C" : "D",
//          :ffds[0].getFieldData("message_text"),
//          :refNum,
//          :rolNum,
//          :ffds[0].getFieldData("settlement_flag"),
//          :ffds[0].getFieldData("auth_tran_id"),
//          :ffds[0].getFieldData("reimbursement_attribute"),
//          :ffds[1] == null ? null : ffds[1].getFieldData("settlement_type"),
//          :ffds[1] == null ? null : ffds[1].getFieldData("national_reimbursement_fee"),
//          :ffds[1] == null ? null : ffds[1].getFieldData("installment_payment_count"),
//          :ffds[2] == null ? null : ffds[2].getFieldData("business_format_code"),
//          :ffds[2] == null ? null : ffds[2].getFieldData("promotion_type"),
//          :ffds[2] == null ? null : ffds[2].getFieldData("promotion_code"),
//          dukpt_encrypt_wrapper( :ffds[0].getFieldData("card_number") ),
//          :filename,
//          load_filename_to_load_file_id(:filename)
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_0 = ffds[0].getFieldData("tran_code");
 String __sJT_1 = ffds[0].getFieldData("dest_bin");
 String __sJT_2 = ffds[0].getFieldData("source_bin");
 String __sJT_3 = ffds[0].getFieldData("reason_code");
 String __sJT_4 = ffds[0].getFieldData("country_code");
 String __sJT_5 = ffds[0].getFieldData("event_date");
 String __sJT_6 = ffds[0].getFieldData("card_number");
 String __sJT_7 = ffds[0].getFieldData("card_number_ext");
 String __sJT_8 = ffds[0].getFieldData("dest_amount");
 String __sJT_9 = ffds[0].getFieldData("dest_currency_code");
 String __sJT_10 =  "10".equals(ffds[0].getFieldData("tran_code")) ? "C" : "D";
 String __sJT_11 = ffds[0].getFieldData("message_text");
 String __sJT_12 = ffds[0].getFieldData("settlement_flag");
 String __sJT_13 = ffds[0].getFieldData("auth_tran_id");
 String __sJT_14 = ffds[0].getFieldData("reimbursement_attribute");
 String __sJT_15 = ffds[1] == null ? null : ffds[1].getFieldData("settlement_type");
 String __sJT_16 = ffds[1] == null ? null : ffds[1].getFieldData("national_reimbursement_fee");
 String __sJT_17 = ffds[1] == null ? null : ffds[1].getFieldData("installment_payment_count");
 String __sJT_18 = ffds[2] == null ? null : ffds[2].getFieldData("business_format_code");
 String __sJT_19 = ffds[2] == null ? null : ffds[2].getFieldData("promotion_type");
 String __sJT_20 = ffds[2] == null ? null : ffds[2].getFieldData("promotion_code");
 String __sJT_21 = ffds[0].getFieldData("card_number");
   String theSqlTS = "insert into visa_settlement_fee_collect\n      (\n        merchant_number,\n        settle_rec_id,\n        tran_code,\n        dest_bin,\n        source_bin,\n        reason_code,\n        country_code,\n        settlement_date,\n        transaction_date,\n        card_number,\n        card_number_ext,\n        transaction_amount,\n        currency_code,\n        debit_credit_indicator,\n        message_text,\n        reference_number,\n        rol_number,\n        settlement_flag,\n        auth_tran_id,\n        reimbursement_attribute,\n        settlement_type,\n        national_reimbursement_fee,\n        installment_payment_count,\n        business_format_code,\n        promotion_type,\n        promotion_code,\n        card_number_enc,\n        load_filename,\n        load_file_id\n      )\n      values\n      (\n         :1  ,\n         :2  ,\n         :3  ,\n         :4  ,\n         :5  ,\n         :6  ,\n         :7  ,\n         :8  ,\n        to_date(  :9  ,'mmdd' ),\n         :10  ,\n         :11  ,\n         :12   * 0.01,\n         :13  ,\n         :14  ,\n         :15  ,\n         :16  ,\n         :17  ,\n         :18  ,\n         :19  ,\n         :20  ,\n         :21  ,\n         :22  ,\n         :23  ,\n         :24  ,\n         :25  ,\n         :26  ,\n        dukpt_encrypt_wrapper(  :27   ),\n         :28  ,\n        load_filename_to_load_file_id( :29  )\n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,recId);
   __sJT_st.setString(3,__sJT_0);
   __sJT_st.setString(4,__sJT_1);
   __sJT_st.setString(5,__sJT_2);
   __sJT_st.setString(6,__sJT_3);
   __sJT_st.setString(7,__sJT_4);
   __sJT_st.setDate(8,settlementDate);
   __sJT_st.setString(9,__sJT_5);
   __sJT_st.setString(10,__sJT_6);
   __sJT_st.setString(11,__sJT_7);
   __sJT_st.setString(12,__sJT_8);
   __sJT_st.setString(13,__sJT_9);
   __sJT_st.setString(14,__sJT_10);
   __sJT_st.setString(15,__sJT_11);
   __sJT_st.setString(16,refNum);
   __sJT_st.setString(17,rolNum);
   __sJT_st.setString(18,__sJT_12);
   __sJT_st.setString(19,__sJT_13);
   __sJT_st.setString(20,__sJT_14);
   __sJT_st.setString(21,__sJT_15);
   __sJT_st.setString(22,__sJT_16);
   __sJT_st.setString(23,__sJT_17);
   __sJT_st.setString(24,__sJT_18);
   __sJT_st.setString(25,__sJT_19);
   __sJT_st.setString(26,__sJT_20);
   __sJT_st.setString(27,__sJT_21);
   __sJT_st.setString(28,filename);
   __sJT_st.setString(29,filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:600^5*/
  }
  //  if central_proc_date is empty then get the date from the fileName
  public Date getSettlementDate(FlatFileRecord ffds)
  {
    Date retVal = null;
    String dateFromFilename=filename.substring(11,17);
    // System.out.println("filename is:>  "+dateFromFilename);
    try
    {
      if ((ffds.getFieldData("central_proc_date"))==null || "".equals((ffds.getFieldData("central_proc_date")).trim())) 
        {
          /*@lineinfo:generated-code*//*@lineinfo:612^11*/

//  ************************************************************
//  #sql [Ctx] { select  to_date( :dateFromFilename,'mm/dd/yy' ) 
//                      
//              from    dual                    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_date(  :1  ,'mm/dd/yy' ) \n                     \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,dateFromFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:617^11*/
        }
   else {
   /*@lineinfo:generated-code*//*@lineinfo:620^4*/

//  ************************************************************
//  #sql [Ctx] { select  yddd_to_date( :ffds.getFieldData("central_proc_date") )
//                      
//              from    dual                    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_22 = ffds.getFieldData("central_proc_date");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  yddd_to_date(  :1   )\n                     \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_22);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^11*/
       }
    }
    catch(Exception e)
    {
      logEntry("VisaRejectExtractor::getSettlementDate()",e.toString());
    }
    //System.out.println("retVal is "+retVal);
    return( retVal );
  }


  private void processMerchantProfileService()
    throws Exception
  {
    FlatFileRecord    ffd           = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_VISA_TC50_TCR0_MPS_IN);
    BufferedReader    in            = getReader();
    String            line          = null;
    String            loadFilename  = FileUtils.getNameFromFullPath(filename);

    try
    {
      // create a id for this file
      /*@lineinfo:generated-code*//*@lineinfo:648^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^7*/

      // remove any existing entries    
      /*@lineinfo:generated-code*//*@lineinfo:654^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    visa_merchant_profile_service     mps
//          where   mps.load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    visa_merchant_profile_service     mps\n        where   mps.load_file_id = load_filename_to_load_file_id( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:659^7*/

      while( (line = in.readLine()) != null )
      {
        ffd.suck(StringUtilities.leftJustify(line, 168, ' '));
      
        /*@lineinfo:generated-code*//*@lineinfo:665^9*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_merchant_profile_service
//            (
//              dest_bin,
//              source_bin,
//              service_identifier,
//              acquirer_bin,
//              dba_name,
//              dba_city,
//              dba_state,
//              country_code,
//              dba_zip,
//              sic_code,
//              merchant_number,
//              occurrence_number,
//              error_code_1,
//              error_code_2,
//              error_code_3,
//              error_code_4,
//              error_code_5,
//              error_code_6,
//              error_code_7,
//              error_code_8,
//              error_code_9,
//              mps_id_number,
//              reimbursement_attribute,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :ffd.getFieldData("dest_bin"),
//              :ffd.getFieldData("source_bin"),
//              :ffd.getFieldData("service_identifier"),
//              :ffd.getFieldData("acquirer_bin"),
//              :ffd.getFieldData("dba_name"),
//              :ffd.getFieldData("dba_city"),
//              :ffd.getFieldData("dba_state"),
//              :ffd.getFieldData("country_code"),
//              :ffd.getFieldData("dba_zip"),
//              :ffd.getFieldData("sic_code"),
//              :ffd.getFieldData("merchant_number"),
//              :ffd.getFieldData("occurrence_number"),
//              :ffd.getFieldData("error_code_1"),
//              :ffd.getFieldData("error_code_2"),
//              :ffd.getFieldData("error_code_3"),
//              :ffd.getFieldData("error_code_4"),
//              :ffd.getFieldData("error_code_5"),
//              :ffd.getFieldData("error_code_6"),
//              :ffd.getFieldData("error_code_7"),
//              :ffd.getFieldData("error_code_8"),
//              :ffd.getFieldData("error_code_9"),
//              :ffd.getFieldData("mps_id_number"),
//              :ffd.getFieldData("reimbursement_attribute"),
//              :loadFilename,
//              load_filename_to_load_file_id(:loadFilename)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_23 = ffd.getFieldData("dest_bin");
 String __sJT_24 = ffd.getFieldData("source_bin");
 String __sJT_25 = ffd.getFieldData("service_identifier");
 String __sJT_26 = ffd.getFieldData("acquirer_bin");
 String __sJT_27 = ffd.getFieldData("dba_name");
 String __sJT_28 = ffd.getFieldData("dba_city");
 String __sJT_29 = ffd.getFieldData("dba_state");
 String __sJT_30 = ffd.getFieldData("country_code");
 String __sJT_31 = ffd.getFieldData("dba_zip");
 String __sJT_32 = ffd.getFieldData("sic_code");
 String __sJT_33 = ffd.getFieldData("merchant_number");
 String __sJT_34 = ffd.getFieldData("occurrence_number");
 String __sJT_35 = ffd.getFieldData("error_code_1");
 String __sJT_36 = ffd.getFieldData("error_code_2");
 String __sJT_37 = ffd.getFieldData("error_code_3");
 String __sJT_38 = ffd.getFieldData("error_code_4");
 String __sJT_39 = ffd.getFieldData("error_code_5");
 String __sJT_40 = ffd.getFieldData("error_code_6");
 String __sJT_41 = ffd.getFieldData("error_code_7");
 String __sJT_42 = ffd.getFieldData("error_code_8");
 String __sJT_43 = ffd.getFieldData("error_code_9");
 String __sJT_44 = ffd.getFieldData("mps_id_number");
 String __sJT_45 = ffd.getFieldData("reimbursement_attribute");
   String theSqlTS = "insert into visa_merchant_profile_service\n          (\n            dest_bin,\n            source_bin,\n            service_identifier,\n            acquirer_bin,\n            dba_name,\n            dba_city,\n            dba_state,\n            country_code,\n            dba_zip,\n            sic_code,\n            merchant_number,\n            occurrence_number,\n            error_code_1,\n            error_code_2,\n            error_code_3,\n            error_code_4,\n            error_code_5,\n            error_code_6,\n            error_code_7,\n            error_code_8,\n            error_code_9,\n            mps_id_number,\n            reimbursement_attribute,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  ,\n             :6  ,\n             :7  ,\n             :8  ,\n             :9  ,\n             :10  ,\n             :11  ,\n             :12  ,\n             :13  ,\n             :14  ,\n             :15  ,\n             :16  ,\n             :17  ,\n             :18  ,\n             :19  ,\n             :20  ,\n             :21  ,\n             :22  ,\n             :23  ,\n             :24  ,\n            load_filename_to_load_file_id( :25  )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_23);
   __sJT_st.setString(2,__sJT_24);
   __sJT_st.setString(3,__sJT_25);
   __sJT_st.setString(4,__sJT_26);
   __sJT_st.setString(5,__sJT_27);
   __sJT_st.setString(6,__sJT_28);
   __sJT_st.setString(7,__sJT_29);
   __sJT_st.setString(8,__sJT_30);
   __sJT_st.setString(9,__sJT_31);
   __sJT_st.setString(10,__sJT_32);
   __sJT_st.setString(11,__sJT_33);
   __sJT_st.setString(12,__sJT_34);
   __sJT_st.setString(13,__sJT_35);
   __sJT_st.setString(14,__sJT_36);
   __sJT_st.setString(15,__sJT_37);
   __sJT_st.setString(16,__sJT_38);
   __sJT_st.setString(17,__sJT_39);
   __sJT_st.setString(18,__sJT_40);
   __sJT_st.setString(19,__sJT_41);
   __sJT_st.setString(20,__sJT_42);
   __sJT_st.setString(21,__sJT_43);
   __sJT_st.setString(22,__sJT_44);
   __sJT_st.setString(23,__sJT_45);
   __sJT_st.setString(24,loadFilename);
   __sJT_st.setString(25,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:723^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("processMerchantRegistrations(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
  }

  private void processRetrievalRequests()
    throws Exception
  {
    int               defType       = -1;
    FlatFileRecord[]  ffds          = new FlatFileRecord[3];
    int               idx           = 0;
    BufferedReader    in            = getReader();
    String            line          = null;
    FlatFileRecord[]  outputRecs    = new FlatFileRecord[3];
    String            tcr           = null;

    for( int i = 0; i < ffds.length; ++i )
    {
      switch( i )
      {
        case 0:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC52_TCR0; break;
        case 1:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC52_TCR1; break;
        case 2:   defType  = MesFlatFiles.DEF_TYPE_VISA_TC52_TCR4; break; 
        default:  continue;
      }
      ffds[i]       = new FlatFileRecord(Ctx,defType);
      outputRecs[i] = null;
    }

    while( (line = in.readLine()) != null )
    {
      tcr = line.substring(2,4);

           if ( "00".equals(tcr) )  { idx = 0;  }
      else if ( "01".equals(tcr) )  { idx = 1;  }
      else if ( "04".equals(tcr) )  { idx = 2;  }
      else                          { idx = -1; }

      if( idx >= 0 )
      {
        if ( idx == 0 && outputRecs[0] != null )
        {
          storeRetrievalRequest(outputRecs);

          // reset the output record group
          for( int i = 0; i < outputRecs.length; ++i )
          {
            outputRecs[i] = null;
            ffds[i].resetAllFields();
          }
        }

        // extract the data from this line
        ffds[idx].suck(StringUtilities.leftJustify(line, 168, ' '));
        outputRecs[idx] = ffds[idx];
      }
    }   // end while loop

    // boundary condition, store the last record group
    if ( outputRecs[0] != null )
    {
      storeRetrievalRequest(outputRecs);
    }

    // queue this file for processing by the risk loader
    /*@lineinfo:generated-code*//*@lineinfo:795^5*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//        (
//          process_type,
//          process_sequence,
//          load_filename
//        )
//        values
//        (
//          1,
//          0,
//          :filename
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n      (\n        process_type,\n        process_sequence,\n        load_filename\n      )\n      values\n      (\n        1,\n        0,\n         :1  \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:809^5*/
  }

  private void storeRetrievalRequest( FlatFileRecord[] ffds )
    throws Exception
  {
    String                authCode        = null;
    int                   bankNumber      = 0;
    String                cardNumber      = null;
    String                cc              = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    long                  merchantId      = 0L;
    String                motoEcommInd    = null;
    String                refNum          = null;
    String                rps             = null;
    java.sql.Date         tranDate        = null;

    refNum      = ffds[0].getFieldData("reference_number");
    cardNumber  = ffds[0].getFieldData("card_number");

    // attempt to locate the original record
    /*@lineinfo:generated-code*//*@lineinfo:831^5*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.*
//        from    visa_settlement   vs
//        where   vs.acq_reference_number  = substr(:refNum,12,11)
//                and vs.bin_number = substr(:refNum,2,6)
//                and yddd_to_date(substr(:refNum,8,4)) between vs.batch_date-2 and vs.batch_date+6
//                and nvl(vs.test_flag,'Y') = 'N'
//                and vs.card_number = substr(:cardNumber,1,6) || 'xxxxxx' || substr(:cardNumber,-4)
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vs.*\n      from    visa_settlement   vs\n      where   vs.acq_reference_number  = substr( :1  ,12,11)\n              and vs.bin_number = substr( :2  ,2,6)\n              and yddd_to_date(substr( :3  ,8,4)) between vs.batch_date-2 and vs.batch_date+6\n              and nvl(vs.test_flag,'Y') = 'N'\n              and vs.card_number = substr( :4  ,1,6) || 'xxxxxx' || substr( :5  ,-4)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,refNum);
   __sJT_st.setString(2,refNum);
   __sJT_st.setString(3,refNum);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setString(5,cardNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.utils.VisaRejectExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:840^5*/
    ResultSet rs = it.getResultSet();

    if ( rs.next() )
    {
      motoEcommInd  = rs.getString("moto_ecommerce_ind");
      merchantId    = rs.getLong("merchant_number");
      rps           = rs.getString("requested_payment_service");
      authCode      = rs.getString("auth_code");
      bankNumber    = rs.getInt("bank_number");
      tranDate      = rs.getDate("transaction_date");
      cc            = rs.getString("currency_code");
      
      // if visa did not provide the currency conversion for this
      // retrieval request, use the amount and currency code
      // from the original transaction.
      if ( !cc.equals(ffds[0].getFieldData("currency_code")) )
      {
        double originalAmount = rs.getDouble("transaction_amount");
        ffds[0].setFieldData("transaction_amount" ,MesMath.toFractionalAmount(originalAmount,2,2,false).replaceAll("\\.",""));
        ffds[0].setFieldData("currency_code"      ,cc);
      }
    }
    else
    {
      /*@lineinfo:generated-code*//*@lineinfo:865^7*/

//  ************************************************************
//  #sql [Ctx] { select  mb.bank_number,
//                  to_date(:ffds[0].getFieldData("transaction_date"),'mmdd')
//          
//          from    mbs_banks   mb
//          where   substr(:refNum,2,6) in
//                  ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,
//                    mb.mc_bin_inet    ,mb.mc_bin_pin_debit )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_46 = ffds[0].getFieldData("transaction_date");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mb.bank_number,\n                to_date( :1  ,'mmdd')\n         \n        from    mbs_banks   mb\n        where   substr( :2  ,2,6) in\n                ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,\n                  mb.mc_bin_inet    ,mb.mc_bin_pin_debit )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_46);
   __sJT_st.setString(2,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   tranDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:874^7*/
    }
    rs.close();
    it.close();

    loadSec = ChargebackTools.getNewLoadSec();

    /*@lineinfo:generated-code*//*@lineinfo:881^5*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrieval_visa
//        (
//          load_sec,
//          transaction_code,
//          account_number,
//          transaction_date,
//          mail_phone_order_ind,
//          transaction_amt,
//          ref_num,
//          merchant_name,
//          merchant_city,
//          merchant_state,
//          issuer_control_num,
//          tran_currency_code,
//          merchant_zip,
//          atm_acct_selection_id,
//          fax_number_for_retrieval,
//          acquirer_member_id,
//          merchant_country_code,
//          merchant_class_code,
//          card_acceptor_id,
//          trans_id,
//          exclude_trans_id_reason,
//          mutiple_clearing_seq_num,
//          request_payment_service,
//          retrieval_request_id_num,
//          issuer_rfc_bin,
//          issuer_rfc_subaddress,
//          authorization_code,
//          merchant_account_number,
//          actual_incoming_date,
//          central_processing_date,
//          est_fulfillment_method_ind,
//          --rfco_request_num,
//          --source_bin,
//          bank_number,
//          reason_code,
//          load_filename
//        )
//        values
//        (
//          :loadSec,
//          :ffds[0].getFieldData("tran_code"),
//          :ffds[0].getFieldData("card_number"),
//          :tranDate,
//          :motoEcommInd,
//          (:ffds[0].getFieldData("transaction_amount") * 0.01),
//          :refNum,
//          :ffds[0].getFieldData("dba_name"),
//          :ffds[0].getFieldData("dba_city"),
//          :ffds[0].getFieldData("dba_state").substring(0,2),
//          :ffds[0].getFieldData("issuer_control_number"),
//          :ffds[0].getFieldData("currency_code"),
//          :ffds[0].getFieldData("dba_zip"),
//          :ffds[0].getFieldData("account_selection"),
//          :ffds[1].getFieldData("fax_number"),
//          :ffds[0].getFieldData("acquirer_business_id"),
//          :ffds[0].getFieldData("country_code"),
//          :ffds[0].getFieldData("sic_code"),
//          :merchantId,
//          :ffds[1].getFieldData("transaction_identifier"),
//          :ffds[1].getFieldData("excluded_transaction_identifier_reason"),
//          :ffds[1].getFieldData("multiple_clearing_seq_num"),
//          :rps,
//          :ffds[0].getFieldData("retrieval_request_id"),
//          :ffds[1].getFieldData("issuer_rfc_bin"),
//          :ffds[1].getFieldData("issuer_rfc_sub_address"),
//          :authCode,
//          :merchantId,
//          nvl(get_file_date(:filename),trunc(sysdate)),
//          yddd_to_date( :ffds[0].getFieldData("central_proc_date") ),
//          :ffds[1].getFieldData("established_fulfillment_method"),
//          :bankNumber,
//          lpad(:ffds[0].getFieldData("request_reason_code"),4,'0'),
//          :filename
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_47 = ffds[0].getFieldData("tran_code");
 String __sJT_48 = ffds[0].getFieldData("card_number");
 String __sJT_49 = ffds[0].getFieldData("transaction_amount");
 String __sJT_50 = ffds[0].getFieldData("dba_name");
 String __sJT_51 = ffds[0].getFieldData("dba_city");
 String __sJT_52 = ffds[0].getFieldData("dba_state").substring(0,2);
 String __sJT_53 = ffds[0].getFieldData("issuer_control_number");
 String __sJT_54 = ffds[0].getFieldData("currency_code");
 String __sJT_55 = ffds[0].getFieldData("dba_zip");
 String __sJT_56 = ffds[0].getFieldData("account_selection");
 String __sJT_57 = ffds[1].getFieldData("fax_number");
 String __sJT_58 = ffds[0].getFieldData("acquirer_business_id");
 String __sJT_59 = ffds[0].getFieldData("country_code");
 String __sJT_60 = ffds[0].getFieldData("sic_code");
 String __sJT_61 = ffds[1].getFieldData("transaction_identifier");
 String __sJT_62 = ffds[1].getFieldData("excluded_transaction_identifier_reason");
 String __sJT_63 = ffds[1].getFieldData("multiple_clearing_seq_num");
 String __sJT_64 = ffds[0].getFieldData("retrieval_request_id");
 String __sJT_65 = ffds[1].getFieldData("issuer_rfc_bin");
 String __sJT_66 = ffds[1].getFieldData("issuer_rfc_sub_address");
 String __sJT_67 = ffds[0].getFieldData("central_proc_date");
 String __sJT_68 = ffds[1].getFieldData("established_fulfillment_method");
 String __sJT_69 = ffds[0].getFieldData("request_reason_code");
   String theSqlTS = "insert into network_retrieval_visa\n      (\n        load_sec,\n        transaction_code,\n        account_number,\n        transaction_date,\n        mail_phone_order_ind,\n        transaction_amt,\n        ref_num,\n        merchant_name,\n        merchant_city,\n        merchant_state,\n        issuer_control_num,\n        tran_currency_code,\n        merchant_zip,\n        atm_acct_selection_id,\n        fax_number_for_retrieval,\n        acquirer_member_id,\n        merchant_country_code,\n        merchant_class_code,\n        card_acceptor_id,\n        trans_id,\n        exclude_trans_id_reason,\n        mutiple_clearing_seq_num,\n        request_payment_service,\n        retrieval_request_id_num,\n        issuer_rfc_bin,\n        issuer_rfc_subaddress,\n        authorization_code,\n        merchant_account_number,\n        actual_incoming_date,\n        central_processing_date,\n        est_fulfillment_method_ind,\n        --rfco_request_num,\n        --source_bin,\n        bank_number,\n        reason_code,\n        load_filename\n      )\n      values\n      (\n         :1  ,\n         :2  ,\n         :3  ,\n         :4  ,\n         :5  ,\n        ( :6   * 0.01),\n         :7  ,\n         :8  ,\n         :9  ,\n         :10  ,\n         :11  ,\n         :12  ,\n         :13  ,\n         :14  ,\n         :15  ,\n         :16  ,\n         :17  ,\n         :18  ,\n         :19  ,\n         :20  ,\n         :21  ,\n         :22  ,\n         :23  ,\n         :24  ,\n         :25  ,\n         :26  ,\n         :27  ,\n         :28  ,\n        nvl(get_file_date( :29  ),trunc(sysdate)),\n        yddd_to_date(  :30   ),\n         :31  ,\n         :32  ,\n        lpad( :33  ,4,'0'),\n         :34  \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,__sJT_47);
   __sJT_st.setString(3,__sJT_48);
   __sJT_st.setDate(4,tranDate);
   __sJT_st.setString(5,motoEcommInd);
   __sJT_st.setString(6,__sJT_49);
   __sJT_st.setString(7,refNum);
   __sJT_st.setString(8,__sJT_50);
   __sJT_st.setString(9,__sJT_51);
   __sJT_st.setString(10,__sJT_52);
   __sJT_st.setString(11,__sJT_53);
   __sJT_st.setString(12,__sJT_54);
   __sJT_st.setString(13,__sJT_55);
   __sJT_st.setString(14,__sJT_56);
   __sJT_st.setString(15,__sJT_57);
   __sJT_st.setString(16,__sJT_58);
   __sJT_st.setString(17,__sJT_59);
   __sJT_st.setString(18,__sJT_60);
   __sJT_st.setLong(19,merchantId);
   __sJT_st.setString(20,__sJT_61);
   __sJT_st.setString(21,__sJT_62);
   __sJT_st.setString(22,__sJT_63);
   __sJT_st.setString(23,rps);
   __sJT_st.setString(24,__sJT_64);
   __sJT_st.setString(25,__sJT_65);
   __sJT_st.setString(26,__sJT_66);
   __sJT_st.setString(27,authCode);
   __sJT_st.setLong(28,merchantId);
   __sJT_st.setString(29,filename);
   __sJT_st.setString(30,__sJT_67);
   __sJT_st.setString(31,__sJT_68);
   __sJT_st.setInt(32,bankNumber);
   __sJT_st.setString(33,__sJT_69);
   __sJT_st.setString(34,filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:959^5*/
  }

  private void processRejects()
  throws Exception
  {
    List rejects;
    FFProcessor ffp;

    if(processItems == null )
    {
      log.debug("Something's wrong - or maybe only a trailer file?");
      throw new Exception("Null 'processItems' entities found.");
    }

    long recId;
    String rejectCode;

    try
    {
      for(int i=0; i < processItems.size(); i++)
      {

        ffp     = (FFProcessor)processItems.get(i);

        //load rec_id first time around
        recId = findRecId(ffp.getRefNum(), ffp.getMerchNum(), ffp.isSecondPresentment(), ffp.getAcctNum());

        log.debug(ffp.toString());

        //add this item to the msg stringbuffer
        if(messageSB == null)
        {
          messageSB = new StringBuffer("The following rejects have just been added to the queues:\n");
        }

        String  indicator     = "C".equals(ffp.getDBCRInd())?"CR":"DB";
        int     presentment   = ffp.isSecondPresentment() ? 2 : 0;

        messageSB.append("\n");
        messageSB.append("\tMerch ID:  ").append(ffp.getMerchNum()).append("\n");
        messageSB.append("\tRecord ID: ").append(recId).append("\n");
        messageSB.append("\tTrans Amt: ").append(MesMath.toCurrency(ffp.getTransAmt())).append(" (").append(indicator).append(")\n");
        messageSB.append("\tRej Codes: ");

        //get the list of rejects
        rejects = ffp.getRejects();

        int count = 0;

        //find our current count for this rec id
        /*@lineinfo:generated-code*//*@lineinfo:1010^9*/

//  ************************************************************
//  #sql [Ctx] { select count(distinct (round))
//            
//            from reject_record
//            where rec_id  = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(distinct (round))\n           \n          from reject_record\n          where rec_id  =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1016^9*/

        count++;

        for(int j = 0; j < rejects.size(); j++)
        {

          rejectCode = (String)rejects.get(j);

          log.debug("saving reject record " + recId +" :: "+rejectCode);

          //add detail to email
          messageSB.append(rejectCode).append(" ");

          /*@lineinfo:generated-code*//*@lineinfo:1030^11*/

//  ************************************************************
//  #sql [Ctx] { insert into REJECT_RECORD
//              (
//                rec_id,
//                reject_id,
//                reject_type,
//                status,
//                round,
//                reject_date,
//                presentment,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :recId,
//                :rejectCode,
//                'VS',
//                -1,
//                :count,
//                nvl(to_date(:filenameDateStr,'mmddrr'), trunc(sysdate)),
//                :presentment,
//                :LoadFilename,
//                load_filename_to_load_file_id(:LoadFilename)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into REJECT_RECORD\n            (\n              rec_id,\n              reject_id,\n              reject_type,\n              status,\n              round,\n              reject_date,\n              presentment,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n              'VS',\n              -1,\n               :3  ,\n              nvl(to_date( :4  ,'mmddrr'), trunc(sysdate)),\n               :5  ,\n               :6  ,\n              load_filename_to_load_file_id( :7  )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,rejectCode);
   __sJT_st.setInt(3,count);
   __sJT_st.setString(4,filenameDateStr);
   __sJT_st.setInt(5,presentment);
   __sJT_st.setString(6,LoadFilename);
   __sJT_st.setString(7,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1056^11*/

        }//end for j

        //finalize email data
        messageSB.append("\n");

        //then need to add item to q_data
        int toQueue = MesQueues.Q_REJECTS_UNWORKED;

        //presentment == 2 means 2nd presentment
        if(presentment == 2)
        {
          toQueue = MesQueues.Q_REJECTS_UNWORKED_2ND;
        }

        if ( QueueTools.hasItemEnteredQueue(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
        {
          QueueTools.moveQueueItem(recId,
                                   QueueTools.getCurrentQueueType(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                   toQueue,
                                   null,
                                   null);
        }
        else  //then need to add item to q_data
        {
          QueueTools.insertQueue(recId, toQueue);
        }


      }//end for i

    }
    catch(Exception e)
    {
      log.error(e.getMessage());
//      throw e;
      logEntry("processRejects(" + LoadFilename + ")",e.toString());
    }

  }

  //grab data from the flat file and add into V
  private void processChargebacks()
  throws Exception
  {
    int           bankNumber  = 0;
    FFProcessor   item        = null;
    String        tc          = null;
    
    if( processItems == null )
    {
      log.debug("Something's wrong - or maybe only a trailer file?");
      throw new Exception("Null 'processItems' entities found.");
    }

    try
    {
      String _filename = FileUtils.getNameFromFullPath(filename);

      for(int i=0; i < processItems.size(); i++)
      {
        item = (FFProcessor)processItems.get(i);
        tc   = item.getTranCode();
        
        /*@lineinfo:generated-code*//*@lineinfo:1121^9*/

//  ************************************************************
//  #sql [Ctx] { select  mb.bank_number
//            
//            from    mbs_banks     mb
//            where   :item.getBinNumber() in
//                    ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,
//                      mb.mc_bin_inet    ,mb.mc_bin_pin_debit )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_70 = item.getBinNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mb.bank_number\n           \n          from    mbs_banks     mb\n          where    :1   in\n                  ( mb.vs_bin_base_ii ,mb.vs_bin_pin_debit,\n                    mb.mc_bin_inet    ,mb.mc_bin_pin_debit )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_70);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1129^9*/
        
        if ( "35".equals(tc) ||   // Sales draft chargeback reversal
             "36".equals(tc) ||   // Credit voucher chargeback reversal
             "37".equals(tc) )    // Cash disbursement chargeback reversal 
        {
          String          exceptionMessage  = null;
          long            loadSec           = 0L;
          long            merchantId        = item.getMerchNum();
          double          reversalAmount    = (item.getDestAmt() * ("C".equals(item.getDBCRInd()) ? -1 : 1));
          java.sql.Date   reversalDate      = null;
          boolean         reversed          = false;
          
          if ( !isValidMid( bankNumber, merchantId ) ) { merchantId = 0L; }
          
          for( int attempt = 0; attempt < 2; ++attempt )
          {
            try
            {
              switch(attempt)
              {
                case 0:
                  // chargeback reference number is required to be
                  // unique within the issuer bin
                  /*@lineinfo:generated-code*//*@lineinfo:1153^19*/

//  ************************************************************
//  #sql [Ctx] { select  cb.cb_load_sec,
//                              nvl(get_file_date(:_filename), trunc(sysdate))
//                      
//                      from    network_chargebacks     cb
//                      where   :merchantId in ( 0, cb.merchant_number )
//                              and cb.incoming_date >= (sysdate-365)
//                              and cb.card_type = 'VS'
//                              and cb.reference_number = :item.getRefNum()
//                              and cb.cb_ref_num  = :item.getCBRefNum()
//              
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_71 = item.getRefNum();
 long __sJT_72 = item.getCBRefNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.cb_load_sec,\n                            nvl(get_file_date( :1  ), trunc(sysdate))\n                     \n                    from    network_chargebacks     cb\n                    where    :2   in ( 0, cb.merchant_number )\n                            and cb.incoming_date >= (sysdate-365)\n                            and cb.card_type = 'VS'\n                            and cb.reference_number =  :3  \n                            and cb.cb_ref_num  =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,_filename);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setString(3,__sJT_71);
   __sJT_st.setLong(4,__sJT_72);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadSec = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   reversalDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1165^19*/
                  break;
                
                case 1:
                  // unable to locate using the cb ref num (issuer sent slop)
                  // so attempt to lookup using the originating reference
                  // number data.
                  String refNum = item.getRefNum();
                  /*@lineinfo:generated-code*//*@lineinfo:1173^19*/

//  ************************************************************
//  #sql [Ctx] { select  cb.cb_load_sec,
//                              nvl(get_file_date(:_filename), trunc(sysdate))
//                      
//                      from    network_chargebacks     cb
//                      where   merchant_number = resolve_mid(:refNum)
//                              and incoming_date >= yddd_to_date(substr(:refNum,8,4))
//                              and reference_number = :refNum
//                              and cb.card_type = 'VS'
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.cb_load_sec,\n                            nvl(get_file_date( :1  ), trunc(sysdate))\n                     \n                    from    network_chargebacks     cb\n                    where   merchant_number = resolve_mid( :2  )\n                            and incoming_date >= yddd_to_date(substr( :3  ,8,4))\n                            and reference_number =  :4  \n                            and cb.card_type = 'VS'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,_filename);
   __sJT_st.setString(2,refNum);
   __sJT_st.setString(3,refNum);
   __sJT_st.setString(4,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadSec = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   reversalDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1183^19*/
                  break;
              }
              ChargebackTools.reverseChargeback(Ctx,"VS",loadSec,reversalDate,reversalAmount);
              reversed = true;
              break;
            }
            catch( Exception re )
            {
              exceptionMessage = re.toString();
            }
          }
          
          if ( !reversed )
          {
            // issue system warning
            MailMessage.sendErrorEmail("Visa Chargeback Loading Error","Failed to reverse chargeback " + item.getRefNum() + "/" + item.getCBRefNum() + "/" + reversalAmount + " in file '" + _filename + "'" + "\n\n" + exceptionMessage, MesEmails.MSG_ADDRS_VS_CHGBK_ERROR );
          }
        }
        else    // incoming chargeback
        {
          //log.debug(item.toString());

          //get load sec
          long loadSec      = ChargebackTools.getNewLoadSec();

          /*@lineinfo:generated-code*//*@lineinfo:1209^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_visa
//              (
//                load_sec,
//                chargeback_ref_num,
//                transaction_code,
//                account_number,
//                transaction_date,
//                debit_credit_ind,
//                transaction_amount,
//                currency_code,
//                original_amt,
//                original_currency_code,
//                merchant_account_number,
//                merchant_name,
//                merchant_city,
//                merchant_state,
//                reference_number,
//                issuer_bin,
//                authorization_number,
//                reason_code,
//                class_code,
//                merchant_country,
//                usage_code,
//                acquirer_member_id,
//                --cardholder_act_term_ind,
//                documentation_ind,
//                trans_id,
//                request_payment_service,
//                authorization_source,
//                pos_terminal_capablities,
//                terminal_entry_mode,
//                cardholder_id_method,
//                multiple_clearing_seq_num,
//                mail_phone_order_ind,
//                reimbursement_attr_code,
//                merchant_volume_ind,
//                cashback_amount,
//                special_chargeback_ind,
//                special_condition_ind,
//                fee_program_ind,
//                central_processing_date,
//                bank_number,
//                actual_incoming_date,
//                load_filename,
//                business_format_code, 
//                dispute_condition, 
//                vrol_case_number, 
//                dispute_status
//              )
//              values
//              (
//                :loadSec,
//                :item.getCBRefNum(),
//                :item.getTranCode(),
//                :item.getAcctNum(),
//                :item.getTransDate(),--//to_date(:(item.getTransDate()),'yyMMdd'),
//                :item.getDBCRInd(),
//                :item.getDestAmt() * decode(:item.getDBCRInd(),'C',-1,1),   -- for chargebacks, dest amount is settlement amount and is signed
//                :item.getDestCurrCode(),  -- for chargebacks, dest currency is settlement currency
//                :item.getTransAmt(),  -- origin amount is amount in cardholder currency
//                :item.getCurrCode(),  -- origin currency is cardholder currency
//                0,--//:(item.getMerchNum()),
//                :item.getAName(),
//                :item.getACity(),
//                :item.getAState(),
//                :item.getRefNum(),
//                :item.getIssBIN(),
//                :item.getAuthNum(),
//                :item.getReasonCode(),
//                :item.getSic(),
//                :item.getACountry(),
//                :item.getUsageCode(),
//                :item.getAcqID(),
//                --//:(item.getTermType()),
//                :item.getDocInd(),
//                :item.getTransId(),
//                :item.getReqPmtSer(),
//                :item.getAuthSource(),
//                :item.getPOSTerm(),
//                :item.getTermEMode(),
//                :item.getChIDMethod(),
//                :item.getMSeqNum(),
//                :item.getOrderInd(),
//                :item.getReimAttrCode(),
//                :item.getMerchVolInd(),
//                :item.getCashbackAmt(),
//                :item.getSpecialChargebackInd(),
//                :item.getSpecialConditionsInd(),
//                :item.getFeePgmInd(),
//                nvl(to_date(:filenameDateStr,'mmddyy'), trunc(sysdate)),
//                :bankNumber,
//                nvl(to_date(:filenameDateStr,'mmddyy'), trunc(sysdate)),
//                :_filename,
//                :item.getBusinessFormatCode(),
//                :item.getDisputeCondition(),
//                :item.getVrolCaseNumber(),
//                :item.getDisputeStatus()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_73 = item.getCBRefNum();
 String __sJT_74 = item.getTranCode();
 String __sJT_75 = item.getAcctNum();
 java.sql.Date __sJT_76 = item.getTransDate();
 String __sJT_77 = item.getDBCRInd();
 double __sJT_78 = item.getDestAmt();
 String __sJT_79 = item.getDBCRInd();
 String __sJT_80 = item.getDestCurrCode();
 double __sJT_81 = item.getTransAmt();
 String __sJT_82 = item.getCurrCode();
 String __sJT_83 = item.getAName();
 String __sJT_84 = item.getACity();
 String __sJT_85 = item.getAState();
 String __sJT_86 = item.getRefNum();
 String __sJT_87 = item.getIssBIN();
 String __sJT_88 = item.getAuthNum();
 String __sJT_89 = item.getReasonCode();
 String __sJT_90 = item.getSic();
 String __sJT_91 = item.getACountry();
 String __sJT_92 = item.getUsageCode();
 String __sJT_93 = item.getAcqID();
 String __sJT_94 = item.getDocInd();
 String __sJT_95 = item.getTransId();
 String __sJT_96 = item.getReqPmtSer();
 String __sJT_97 = item.getAuthSource();
 String __sJT_98 = item.getPOSTerm();
 String __sJT_99 = item.getTermEMode();
 String __sJT_100 = item.getChIDMethod();
 String __sJT_101 = item.getMSeqNum();
 String __sJT_102 = item.getOrderInd();
 String __sJT_103 = item.getReimAttrCode();
 String __sJT_104 = item.getMerchVolInd();
 double __sJT_105 = item.getCashbackAmt();
 String __sJT_106 = item.getSpecialChargebackInd();
 String __sJT_107 = item.getSpecialConditionsInd();
 String __sJT_108 = item.getFeePgmInd();
 String __sJT_109 = item.getBusinessFormatCode();
 String __sJT_110 = item.getDisputeCondition();
 String __sJT_111 = item.getVrolCaseNumber();
 String __sJT_112 = item.getDisputeStatus();
   String theSqlTS = "insert into network_chargeback_visa\n            (\n              load_sec,\n              chargeback_ref_num,\n              transaction_code,\n              account_number,\n              transaction_date,\n              debit_credit_ind,\n              transaction_amount,\n              currency_code,\n              original_amt,\n              original_currency_code,\n              merchant_account_number,\n              merchant_name,\n              merchant_city,\n              merchant_state,\n              reference_number,\n              issuer_bin,\n              authorization_number,\n              reason_code,\n              class_code,\n              merchant_country,\n              usage_code,\n              acquirer_member_id,\n              --cardholder_act_term_ind,\n              documentation_ind,\n              trans_id,\n              request_payment_service,\n              authorization_source,\n              pos_terminal_capablities,\n              terminal_entry_mode,\n              cardholder_id_method,\n              multiple_clearing_seq_num,\n              mail_phone_order_ind,\n              reimbursement_attr_code,\n              merchant_volume_ind,\n              cashback_amount,\n              special_chargeback_ind,\n              special_condition_ind,\n              fee_program_ind,\n              central_processing_date,\n              bank_number,\n              actual_incoming_date,\n              load_filename,\n              business_format_code, \n              dispute_condition, \n              vrol_case_number, \n              dispute_status\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n               :5  ,--//to_date(:(item.getTransDate()),'yyMMdd'),\n               :6  ,\n               :7   * decode( :8  ,'C',-1,1),   -- for chargebacks, dest amount is settlement amount and is signed\n               :9  ,  -- for chargebacks, dest currency is settlement currency\n               :10  ,  -- origin amount is amount in cardholder currency\n               :11  ,  -- origin currency is cardholder currency\n              0,--//:(item.getMerchNum()),\n               :12  ,\n               :13  ,\n               :14  ,\n               :15  ,\n               :16  ,\n               :17  ,\n               :18  ,\n               :19  ,\n               :20  ,\n               :21  ,\n               :22  ,\n              --//:(item.getTermType()),\n               :23  ,\n               :24  ,\n               :25  ,\n               :26  ,\n               :27  ,\n               :28  ,\n               :29  ,\n               :30  ,\n               :31  ,\n               :32  ,\n               :33  ,\n               :34  ,\n               :35  ,\n               :36  ,\n               :37  ,\n              nvl(to_date( :38  ,'mmddyy'), trunc(sysdate)),\n               :39  ,\n              nvl(to_date( :40  ,'mmddyy'), trunc(sysdate)),\n               :41  ,\n               :42  ,\n               :43  ,\n               :44  ,\n               :45  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,__sJT_73);
   __sJT_st.setString(3,__sJT_74);
   __sJT_st.setString(4,__sJT_75);
   __sJT_st.setDate(5,__sJT_76);
   __sJT_st.setString(6,__sJT_77);
   __sJT_st.setDouble(7,__sJT_78);
   __sJT_st.setString(8,__sJT_79);
   __sJT_st.setString(9,__sJT_80);
   __sJT_st.setDouble(10,__sJT_81);
   __sJT_st.setString(11,__sJT_82);
   __sJT_st.setString(12,__sJT_83);
   __sJT_st.setString(13,__sJT_84);
   __sJT_st.setString(14,__sJT_85);
   __sJT_st.setString(15,__sJT_86);
   __sJT_st.setString(16,__sJT_87);
   __sJT_st.setString(17,__sJT_88);
   __sJT_st.setString(18,__sJT_89);
   __sJT_st.setString(19,__sJT_90);
   __sJT_st.setString(20,__sJT_91);
   __sJT_st.setString(21,__sJT_92);
   __sJT_st.setString(22,__sJT_93);
   __sJT_st.setString(23,__sJT_94);
   __sJT_st.setString(24,__sJT_95);
   __sJT_st.setString(25,__sJT_96);
   __sJT_st.setString(26,__sJT_97);
   __sJT_st.setString(27,__sJT_98);
   __sJT_st.setString(28,__sJT_99);
   __sJT_st.setString(29,__sJT_100);
   __sJT_st.setString(30,__sJT_101);
   __sJT_st.setString(31,__sJT_102);
   __sJT_st.setString(32,__sJT_103);
   __sJT_st.setString(33,__sJT_104);
   __sJT_st.setDouble(34,__sJT_105);
   __sJT_st.setString(35,__sJT_106);
   __sJT_st.setString(36,__sJT_107);
   __sJT_st.setString(37,__sJT_108);
   __sJT_st.setString(38,filenameDateStr);
   __sJT_st.setInt(39,bankNumber);
   __sJT_st.setString(40,filenameDateStr);
   __sJT_st.setString(41,_filename);
   __sJT_st.setString(42,__sJT_109);
   __sJT_st.setString(43,__sJT_110);
   __sJT_st.setString(44,__sJT_111);
   __sJT_st.setString(45,__sJT_112);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1309^11*/
          addTotalToCB(item);
        }
      }//end for i

      //now enter it into the risk_process table to ensure it
      //gets loaded into the proper places/queues
      /*@lineinfo:generated-code*//*@lineinfo:1316^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//          (
//            process_type,
//            process_sequence,
//            load_filename
//          )
//          values
//          (
//            8,
//            0,
//            :_filename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n        (\n          process_type,\n          process_sequence,\n          load_filename\n        )\n        values\n        (\n          8,\n          0,\n           :1  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,_filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1330^7*/
    }
    catch(Exception e)
    {
      log.error(e.getMessage());
      throw e;
    }
  }

  private void addTotalToCB(FFProcessor item)
  throws Exception
  {
    //init the message
    /*
    //leave null to identify on notifyMES()
    if(messageSB == null)
    {
      messageSB = new StringBuffer();
    }
    */

    if( "C".equals(item.getDBCRInd()) )
    {
      //increase CR item count
      cbCrCount++;

      //increase CR total
      cbCrTotal = cbCrTotal + item.getDestAmt();
    }
    else if("D".equals(item.getDBCRInd()))
    {
      //increase DB item count
      cbDbCount++;

      //increase DB total
      cbDbTotal = cbDbTotal + item.getDestAmt();
    }
    else
    {
      unkCount++;
      unkTotal = unkTotal + item.getDestAmt();
    }
  }


  //need to access mc_settlement to find rec_id
  private long findRecId(String refNum, long merchNum, boolean secondPresentment, String acctNum)
  {
    java.sql.Date     cpd       = null;
    long              recId     = -1;
    String cardNum = encodeCardNumber(acctNum);

    log.debug("findRecId: "+refNum+"::"+merchNum);
    try
    {
      if ( secondPresentment )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1387^9*/

//  ************************************************************
//  #sql [Ctx] { select  yddd_to_date(substr(:refNum,8,4))
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  yddd_to_date(substr( :1  ,8,4))\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cpd = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1392^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1394^9*/

//  ************************************************************
//  #sql [Ctx] { select  cbv.load_sec
//            
//            from    network_chargeback_visa   cbv
//            where   cbv.merchant_account_number = :merchNum
//                    and (:cpd is null or cbv.actual_incoming_date between :cpd and least(trunc(sysdate),:cpd+275))
//                    and cbv.reference_number = :refNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cbv.load_sec\n           \n          from    network_chargeback_visa   cbv\n          where   cbv.merchant_account_number =  :1  \n                  and ( :2   is null or cbv.actual_incoming_date between  :3   and least(trunc(sysdate), :4  +275))\n                  and cbv.reference_number =  :5 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setDate(4,cpd);
   __sJT_st.setString(5,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1402^9*/
      }
      else
      {
          /*@lineinfo:generated-code*//*@lineinfo:1406^11*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//              from    visa_settlement
//              where   acq_reference_number  = substr(:refNum,12,11)
//                  and yddd_to_date(substr(:refNum,8,4)) between batch_date-2 and batch_date+6
//                  and merchant_number       = :merchNum
//                  and card_number           = :cardNum
//                  and nvl(test_flag,'Y')    = 'N'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id  \n            from    visa_settlement\n            where   acq_reference_number  = substr( :1  ,12,11)\n                and yddd_to_date(substr( :2  ,8,4)) between batch_date-2 and batch_date+6\n                and merchant_number       =  :3  \n                and card_number           =  :4  \n                and nvl(test_flag,'Y')    = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,refNum);
   __sJT_st.setString(2,refNum);
   __sJT_st.setLong(3,merchNum);
   __sJT_st.setString(4,cardNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1415^11*/
      }
    }
   
    catch(Exception e)
    {
      logEntry("findRecId(" + merchNum + "," + refNum + "," + secondPresentment + "," + cardNum + " )",e.toString());
    }
    return( recId );
  }
  
  private String encodeCardNumber( String input )
  {
    String          cardNumber  = input.trim();
    StringBuffer    ecard       = new StringBuffer();
    
    try
    {
      ecard.append( cardNumber.substring(0,6) );
      ecard.append( "xxxxxx" );
      ecard.append( cardNumber.substring( (cardNumber.length()-4) ) );
    }
    catch( Exception e )
    {
      return( cardNumber );
    }
    
    return( ecard.toString() );
  }


  private BufferedReader getReader()
  throws Exception
  {
    BufferedReader br = null;

    File f  = new File(filename);

    if(f.exists() && f.isFile())
    {
      br = new BufferedReader(new FileReader(filename));
    }
    else
    {
      throw new Exception("Unable to generate file reader.");
    }

    return br;
  }

  public void setFilename(String filename)
  {
    log.debug("Setting..."+filename);
    this.filename = filename;
  }

  //Implementation method here
  public String getDataSource()
  {
    return filename;
  }

  public boolean validateFilename()
  {return true;}


/*
For chargebacks
05 = Sales draft
06 = Credit voucher
07 = Cash disbursement
15 = Sales draft chargeback
16 = Credit voucher chargeback
17 = Cash disbursement chargeback
25 = Sales draft reversal
26 = Credit voucher reversal
27 = Cash disbursement reversal
35 = Sales draft chargeback reversal
36 = Credit voucher chargeback reversal
37 = Cash disbursement chargeback reversal
*/

  public static class FFProcessor
  {

    int procType       = 0;

    FlatFileRecord ff0 = null;
    FlatFileRecord ff1 = null;
    FlatFileRecord ff4 = null;
    FlatFileRecord ff5 = null;
    FlatFileRecord ff9 = null;
    FlatFileRecord ff9b = null;

    public FFProcessor(){}

    public FFProcessor(String procType)
    {
      try
      {
        this.procType = Integer.parseInt(procType);
      }
      catch(Exception e)
      {}

    }

    public void setRecord(FlatFileRecord record)
    {
      try
      {
        switch(record.getDefType())
        {
          case MesFlatFiles.DEF_TYPE_VISA_TC05_TCR0:
            ff0 = record;
            break;

          case MesFlatFiles.DEF_TYPE_VISA_TC05_TCR1:
            ff1 = record;
            break;
            
          case MesFlatFiles.DEF_TYPE_VISA_TC05_TCR4:
            ff4 = record;
              break;

          case MesFlatFiles.DEF_TYPE_VISA_TC05_TCR5:
            ff5 = record;
            break;

          case MesFlatFiles.DEF_TYPE_VISA_REJECT_TCR9:
            ff9 = record;
            break;

          case MesFlatFiles.DEF_TYPE_VISA_TC01_TCR9:
            ff9b = record;
            break;

          default:
            log.debug("unable to match FF record type");
        };
      }
      catch(Exception e)
      {
        //log?
      }
    }

    //400 = ff0
    //401 = ff1
    //405 = ff5

    //FF0
    public String getRefNum()
    {
      return ff0.getFieldData("reference_number");
    }
    
    public String getBinNumber()
    {
      String    binNumber = "";
      String    refNum    = getRefNum();
      
      if ( refNum != null && refNum.trim().length() == 23 )
      {
        binNumber = refNum.substring(1,7);
      }
      return( binNumber );
    }
    
    public String getTranCode()
    {
      return ff0.getFieldData("tran_code");
    }

    public String getAcctNum()
    {
      return ff0.getFieldData("card_number");
    }

    public java.sql.Date getTransDate()
    {
      String rawDate = ff0.getFieldData("transaction_date");

      if(rawDate.length() == 4)
      {
         return new java.sql.Date( (TridentTools.decodeVisakTranDateAsDate(rawDate)).getTime() );
      }
      else
      {
        return ff0.getFieldAsSqlDate("transaction_date");
      }
    }

    public String getAcqID()
    {
      return ff0.getFieldData("acquirer_business_id");
    }

    public double getTransAmt()
    {
      return ff0.getFieldAsDouble("transaction_amount");
    }

    public String getCurrCode()
    {
      return ff0.getFieldData("currency_code");
    }

    public double getDestAmt()
    {
      return ff0.getFieldAsDouble("dest_amount");
    }

    public String getDestCurrCode()
    {
      return ff0.getFieldData("dest_currency_code");
    }

    public String getAName()
    {
      return ff0.getFieldData("dba_name");
    }

    public String getACity()
    {
      return ff0.getFieldData("dba_city");
    }

    public String getAState()
    {
      return ff0.getFieldData("dba_state");
    }

    public String getACountry()
    {
      return ff0.getFieldData("country_code");
    }

    public String getUsageCode()
    {
      return ff0.getFieldData("usage_code");
    }

    public String getReasonCode()
    {
      return ff0.getFieldData("reason_code");
    }

    public String getSic()
    {
      return ff0.getFieldData("sic_code");
    }

    public String getReqPmtSer()
    {
      return ff0.getFieldData("requested_payment_service");
    }

    public String getChIDMethod()
    {
      return ff0.getFieldData("cardholder_id_method");
    }

    public String getReimAttrCode()
    {
      return ff0.getFieldData("reimbursement_attribute");
    }

    public String getCPD()
    {
      return ff0.getFieldData("central_proc_date");
    }

    public String getPOSTerm()
    {
      return ff0.getFieldData("pos_term_cap");
    }

    public String getAuthNum()
    {
      return ff0.getFieldData("auth_code");
    }

    //FF1
    public long getMerchNum()
    {
      return ff1.getFieldAsLong("merchant_number");
    }

    public String getIssBIN()
    {
      return ff1.getFieldData("issuer_workstation_bin");
    }

    public String getTermType()
    {
      return ff1.getFieldData("terminal_id");
    }

    public String getDocInd()
    {
      return ff1.getFieldData("documentation_ind");
    }

    public long getCBRefNum()
    {
      return ff1.getFieldAsLong("cb_ref_num");
    }

    public String getAuthSource()
    {
      return ff1.getFieldData("auth_source_code");
    }

    public double getCashbackAmt()
    {
      return ff1.getFieldAsDouble("cashback_amount");
    }

    public String getSpecialChargebackInd()
    {
      return ff1.getFieldData("special_chargeback_ind");
    }
    
    public String getSpecialConditionsInd()
    {
      return ff1.getFieldData("special_conditions_ind");
    }

    public String getFeePgmInd()
    {
      return ff1.getFieldData("fee_program_indicator");
    }

  //FF4
    public String getTransactionCode(){
      return ( (ff4 == null) ? null : ff4.getFieldData("transaction_code"));
    }
    
    public String getTransactionCodeQualifier(){
      return ( (ff4 == null) ? null : ff4.getFieldData("Transaction_code_qualifier"));
    }
    
    public String getTranComponentSeqNum(){
      return ( (ff4 == null) ? null : ff4.getFieldData("tran_component_seq_num"));
    }
    
    public String getAgentUniqueId(){
      return ( (ff4 == null) ? null : ff4.getFieldData("agent_unique_id"));
    }
    
    public String getBusinessFormatCode(){
      return ( (ff4 == null) ? null : ff4.getFieldData("business_format_code"));
    }
      
    public String getNetworkIdentificationCode(){
      return ( (ff4 == null) ? null : ff4.getFieldData("network_identification_code"));
    }
    
    public String getContactForInformation(){
      return ( (ff4 == null) ? null : ff4.getFieldData("contact_for_information"));
    }  
    
    public String getAdjustmentProcessingInd(){
      return ( (ff4 == null) ? null : ff4.getFieldData("adjustment_processing_indicator"));
    }    
    
    public String getMssageReasonCode(){
      return ( (ff4 == null) ? null : ff4.getFieldData("message_reason_code"));
    }   
    
    public String getDisputeCondition(){
      return ( (ff4 == null) ? null : ff4.getFieldData("dispute_condition"));
    }  
    
    public String getVrolFinancialId(){
      return ( (ff4 == null) ? null : ff4.getFieldData("vrol_financial_id"));
    }  
    
    public String getVrolCaseNumber(){
      return ( (ff4 == null) ? null : ff4.getFieldData("vrol_case_number"));
    }
    
    public String getVrolBundleCaseNumber(){
      return ( (ff4 == null) ? null : ff4.getFieldData("vrol_bundle_case_number"));
    } 
    
    public String getClientCaseNumber(){
      return ( (ff4 == null) ? null : ff4.getFieldData("client_case_number"));
    }
    
    public String getDisputeStatus(){
      return ( (ff4 == null) ? null : ff4.getFieldData("dispute_status"));
    } 
      
    public double getSurchargeAmount(){
      return ( (ff4 == null) ? 0 : ff4.getFieldAsDouble("surcharge_amount"));
    } 
    
    public String getSurchargCreditDebitInd(){
      return ( (ff4 == null) ? null : ff4.getFieldData("surcharge_credit_debit_indicator"));
    }   
   
    
    //FF5

    public String getTermEMode()
    {
      return "";
      //return( (ff5 == null) ? null : ff5.getFieldData("") );
    }

    public String getMSeqNum()
    {
      return( (ff5 == null) ? null : ff5.getFieldData("multiple_clearing_seq_num") );
    }

    public String getOrderInd()
    {
      return "";
      //return( (ff5 == null) ? null : ff5.getFieldData("") );
    }

    public String getMerchVolInd()
    {
      return( (ff5 == null) ? null : ff5.getFieldData("merchant_vol_indicator") );
    }

    public String getTransId()
    {
      return( (ff5 == null) ? null : ff5.getFieldData("auth_tran_id") );
    }

    //you need to set to D for 15 and 17 and C for 16
    public String getDBCRInd()
    {
      if (procType == 15 || procType == 17)
      {
        return "D";
      }
      else if (procType == 16)
      {
        return "C";
      }
      else
      {
        return "";
      }
      //return ff1.getFieldData("");
    }

    public double getOrigAmt()
    {
      return getTransAmt();
    }


    public List getRejects()
    {
      ArrayList rejects = new ArrayList();
      String base = "error_code_";
      String field;
      String err;

      if(ff9!=null)
      {
        //cycle through the record, looking for error_code_N
        //add values until you hit 0000, then done
        for(int i=1; i<11 ;i++)
        {
          field = base + i;

          err = ff9.getFieldData(field);

          if("0000".equals(err) || "    ".equals(err))
          {
            break;
          }

          rejects.add("V"+err);
        }
      }

      if(ff9b != null)
      {
        base = "return_reason_code_";

        for(int i=1; i<6 ;i++)
        {
          field = base + i;

          err = ff9b.getFieldData(field);

          if(null==err || "000".equals(err) || "   ".equals(err))
          {
            break;
          }
          rejects.add(err.trim());
        }
      }

      return rejects;
    }

    public boolean isSecondPresentment()
    {
      return( ff9b != null && "C".equals(ff9b.getFieldData("crs_return_flag")) );
    }
    
    public String toString()
    {

      StringBuffer sb = new StringBuffer("\n\n");

      sb.append("getRefNum = ").append(getRefNum()).append("\n");

      sb.append("TranCode = ").append(getTranCode()).append("\n");
      sb.append("AcctNum = ").append(getAcctNum()).append("\n");
      sb.append("TransDate = ").append(getTransDate()).append("\n");
      sb.append("DBCRInd = ").append(getDBCRInd()).append("\n");
      sb.append("TransAmt = ").append(getTransAmt()).append("\n");
      sb.append("OrigAmt = ").append(getOrigAmt()).append("\n");
      sb.append("CurrCode = ").append(getCurrCode()).append("\n");
      sb.append("MerchNum = ").append(getMerchNum()).append("\n");
      sb.append("AName = ").append(getAName()).append("\n");
      sb.append("ACity = ").append(getACity()).append("\n");
      sb.append("AState = ").append(getAState()).append("\n");
      sb.append("IssBIN = ").append(getIssBIN()).append("\n");
      sb.append("AuthNum = ").append(getAuthNum()).append("\n");
      sb.append("ReasonCode = ").append(getReasonCode()).append("\n");
      sb.append("Sic = ").append(getSic()).append("\n");
      sb.append("ACountry = ").append(getACountry()).append("\n");
      sb.append("UsageCode = ").append(getUsageCode()).append("\n");
      sb.append("AcqID = ").append(getAcqID()).append("\n");
      sb.append("TermType = ").append(getTermType()).append("\n");
      sb.append("DocInd = ").append(getDocInd()).append("\n");
      sb.append("TransId = ").append(getTransId()).append("\n");
      sb.append("ReqPmtSer = ").append(getReqPmtSer()).append("\n");
      sb.append("AuthSource = ").append(getAuthSource()).append("\n");
      sb.append("POSTerm = ").append(getPOSTerm()).append("\n");
      sb.append("TermEMode = ").append(getTermEMode()).append("\n");
      sb.append("ChIDMethod = ").append(getChIDMethod()).append("\n");
      sb.append("MSeqNum = ").append(getMSeqNum()).append("\n");
      sb.append("OrderInd = ").append(getOrderInd()).append("\n");
      sb.append("ReimAttrCode = ").append(getReimAttrCode()).append("\n");
      sb.append("MerchVolInd = ").append(getMerchVolInd()).append("\n");
      sb.append("CashbackAmt = ").append(getCashbackAmt()).append("\n");
      sb.append("SpecialChargebackInd = ").append(getSpecialChargebackInd()).append("\n");
      sb.append("SpecialConditionsInd = ").append(getSpecialConditionsInd()).append("\n");
      sb.append("FeePgmInd = ").append(getFeePgmInd()).append("\n");
      sb.append("CPD = ").append(getCPD()).append("\n");
      sb.append("TransactionCode  = ").append(getTransactionCode()).append("\n");
      sb.append("TransactionCodeQualifier  = ").append(getTransactionCodeQualifier()).append("\n");
      sb.append("TranComponentSeqNum  = ").append(getTranComponentSeqNum()).append("\n");      
      sb.append("AgentUniqueId = ").append(getAgentUniqueId()).append("\n");
      sb.append("BusinessFormatCode = ").append(getBusinessFormatCode()).append("\n");
      sb.append("NetworkIdentificationCode = ").append(getNetworkIdentificationCode()).append("\n");
      sb.append("ContactForInformation = ").append(getContactForInformation()).append("\n");
      sb.append("AdjustmentProcessingInd = ").append(getAdjustmentProcessingInd()).append("\n");
      sb.append("MssageReasonCode = ").append(getMssageReasonCode()).append("\n");
      sb.append("DisputeCondition = ").append(getDisputeCondition()).append("\n");
      sb.append("VrolFinancialId = ").append(getVrolFinancialId()).append("\n");
      sb.append("VrolCaseNumber = ").append(getVrolCaseNumber()).append("\n");
      sb.append("VrolBundleCaseNumber = ").append(getVrolBundleCaseNumber()).append("\n");
      sb.append("ClientCaseNumber = ").append(getClientCaseNumber()).append("\n");
      sb.append("DisputeStatus = ").append(getDisputeStatus()).append("\n");
      sb.append("SurchargeAmount = ").append(getSurchargeAmount()).append("\n");
      sb.append("SurchargCreditDebitInd = ").append(getSurchargCreditDebitInd()).append("\n");
      return sb.toString();
    }
  }

  public void notifyMES()
  {
    try
    {

      MailMessage msg = new MailMessage();

      msg.setAddresses(MesEmails.MSG_ADDRS_MC_IPM_FILE_REJECT);

      if(messageSB == null)
      {
        msg.setSubject("MBS Visa File Chargebacks: " + filename);

        messageSB = new StringBuffer();

        if(cbCrCount > 0)
        {
          messageSB.append("CR details for VS chargebacks:\n");
          messageSB.append("\tNumber:\t").append(cbCrCount).append("\n");
          messageSB.append("\tAmount:\t").append(MesMath.toCurrency(cbCrTotal)).append("\n");
        }
        if(cbDbCount > 0)
        {
          messageSB.append("DB details for VS chargebacks:\n");
          messageSB.append("\tNumber:\t").append(cbDbCount).append("\n");
          messageSB.append("\tAmount:\t").append(MesMath.toCurrency(cbDbTotal)).append("\n");
        }
        if(unkCount > 0)
        {
          messageSB.append("UNK details for VS chargebacks:\n");
          messageSB.append("\tNumber:\t").append(unkCount).append("\n");
          messageSB.append("\tAmount:\t").append(MesMath.toCurrency(unkTotal)).append("\n");
        }
      }
      else
      {
        msg.setSubject("MBS Visa File Rejects: " + filename);
        //messageSB taken care of via reject notes
      }

      msg.setText(messageSB.toString());

      msg.send();

    }
    catch(Exception e)
    {
      logEntry("notify(" + filename + ")", e.toString());
      //e.printStackTrace();
    }
  }

  private void setLoadFilename()
  {
    int       index           = -1;

    try
    {
      // strip off any path data so LoadFilename has just the filename
      if ( (index = filename.lastIndexOf(File.separator)) >= 0 )
      {
        LoadFilename = filename.substring(index+1);
      }
      else
      {
        LoadFilename = filename;
      }

      // create an entry in the table load_file_index
      /*@lineinfo:generated-code*//*@lineinfo:2056^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:LoadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.utils.VisaRejectExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2059^7*/

      // extract the file date for use later
      /*@lineinfo:generated-code*//*@lineinfo:2062^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(nvl(get_file_date(:LoadFilename),sysdate),'mmddyy')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(nvl(get_file_date( :1  ),sysdate),'mmddyy')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.utils.VisaRejectExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   filenameDateStr = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2067^7*/
    }
    catch(Exception e)
    {
      logEntry("setLoadFilename(" + LoadFilename + ")",e.toString());
    }
  }

  //DEPRECATED 12/09
  /*
  private void extractRejectCodes()
  throws Exception
  {
    setFilename("c:"+File.separator+"work"+File.separator+"EP100A.EPD");

    reportItems       = new LinkedList();
    List rejectCodes  = null;

    BufferedReader br = getReader();
    String line;
    int count = 0;

    while((line=br.readLine()) != null)
    {
      if(line.indexOf("FILE") == 0)
      {
        //if not first in, load the codes
        if(rejectCodes != null)
        {
          reportItems.add(rejectCodes);
        }
        rejectCodes = new ArrayList();
      }
      else
      {
        if(line.indexOf("V0")==0 || line.indexOf("V1")==0)
        {
          //add reject code to list
          rejectCodes.add(line.substring(0,5));
        }
      }

    }
    //add final one
    if(rejectCodes != null)
    {
      reportItems.add(rejectCodes);
    }

  }
  */
 
  protected boolean isNumeric(String str)
  {
    if (str != null)
    {
      for (int i = 0; i < str.length(); ++i)
      {
        if (!Character.isDigit(str.charAt(i)))
        {
          return false;
        }        
      }
      return true;
    }
    return true;
  }
   
  public static void main( String[] args )
  {
    VisaRejectExtractor     extractor   = null;

    try
    {
      //@SQLJConnectionBase.initStandalonePool("DEBUG");
      SQLJConnectionBase.initStandalone();

      extractor = new VisaRejectExtractor();
      extractor.connect(true);

      if ( "processRetrievalRequests".equals(args[0]) )
      {
        extractor.filename = args[1];
        extractor.processRetrievalRequests();
      }
      else if ( "processFeeCollect".equals(args[0]) )
      {
        extractor.filename = args[1];
        extractor.processFeeCollectAndFundDisbursement();
      }
      else if ( "processMerchantProfileService".equals(args[0]) )
      {
        extractor.filename = args[1];
        extractor.processMerchantProfileService();
      }
      else if ( "extract".equals(args[0]) )
      {
        extractor.filenames.clear();
        extractor.filenames.add(args[1]);
        extractor.extract();
      }
      else
      {
        System.out.println("Invalid command");
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ extractor.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/