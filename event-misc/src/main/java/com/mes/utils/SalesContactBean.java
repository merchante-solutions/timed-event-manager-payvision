/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/SalesContactBean.java $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesEmails;
import com.mes.forms.ButtonField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.TextareaField;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;

public class SalesContactBean extends FieldBean
{
  public boolean mustRedirect = false;
  public boolean hasErrors    = false;
  
  public SalesContactBean()
  {
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      createFields();
      
      setFields(request);
      
      if( getData("submit").equals("Send") )
      {
        if( isValid() )
        {
          submitData();
          mustRedirect = true;
        }
        else
        {
          hasErrors = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }
  
  private void createFields()
  {
    try
    {
      fields.add(new Field("businessName", "Name", 30, 30, false));
      fields.add(new Field("businessAddress", "Address", 30, 30, true));
      fields.add(new CityStateZipField("businessCsz", "City, State, Zip", 30, true));
      fields.add(new PhoneField("businessPhone", "Phone", false));
      fields.add(new PhoneField("businessFax", "Fax", true));
      fields.add(new EmailField("businessEmail", 75, 30, true));
      fields.add(new EmailField("ccEmail", 75, 30, true));
      fields.add(new NameField("contactName", 30, true, true));
      fields.add(new PhoneField("contactPhone", "Phone Number", true));
      fields.add(new Field("bestTime", 30, 30, true));
      fields.add(new CurrencyField("monthlySales", "Monthly business sales", 11, 9, true));
      fields.add(new CurrencyField("averageTicket", "Average sale amount", 11, 9, true));
      fields.add(new CurrencyField("monthlyVMC", "Monthly Visa/Mastercard Volume", 11, 9, true));
      fields.add(new Field("productType", "Type of product or service sold", 30, 30, true));
      fields.add(new NumberField("inPersonPercent", "In person", 3, 3, true, 0));
      fields.add(new NumberField("motoPercent", "Phone/Mail/Internet", 3, 3, true, 0));
      fields.add(new TextareaField("comments", 4000, 2, 80, true));
      
      fields.add(new ButtonField("submit", "Send"));
      
      addHtmlExtra("class=\"formFields\"");      
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }

  public void submitData()
  {
    try
    {
      StringBuffer  msg       = new StringBuffer("");
      StringBuffer  subject   = new StringBuffer("");
      int           eAddress  = MesEmails.MSG_ADDRS_SALES_CONTACT;
      String        curDate   = DateTimeFormatter.getCurDateTimeString();
      
      subject.append("MERCHANT LEAD - ");
      subject.append(curDate);

      msg.append("Date              : ");
      msg.append(curDate);
      msg.append("\n");
      msg.append("Business Name     : ");
      msg.append(getData("businessName"));
      msg.append("\n");
      msg.append("Address           : ");
      msg.append(getData("businessAddress"));
      msg.append("\n");
      msg.append("City, State, Zip  : ");
      msg.append(getData("businessCsz"));
      msg.append("\n");
      msg.append("Phone             : ");
      msg.append(getData("businessPhone"));
      msg.append("\n");
      msg.append("Fax               : ");
      msg.append(getData("businessFax"));
      msg.append("\n");
      msg.append("E-mail Address    : ");
      msg.append(getData("businessEmail"));
      msg.append("\n");
      msg.append("E-mail CC         : ");
      msg.append(getData("ccEmail"));
      msg.append("\n");
      msg.append("Contact Name      : ");
      msg.append(getData("contactName"));
      msg.append("\n");
      msg.append("Contact Phone     : ");
      msg.append(getData("contactPhone"));
      msg.append("\n");
      msg.append("Best time to call : ");
      msg.append(getData("bestTime"));
      msg.append("\n");
      msg.append("Monthly Business $: ");
      msg.append(getData("monthlySales"));
      msg.append("\n");
      msg.append("Average ticket    : ");
      msg.append(getData("averageTicket"));
      msg.append("\n");
      msg.append("Monthly V/MC Vol  : ");
      msg.append(getData("monthlyVMC"));
      msg.append("\n");
      msg.append("Product/Service   : ");
      msg.append(getData("productTYpe"));
      msg.append("\n");
      msg.append("In Person %       : ");
      msg.append(getData("inPersonPercent"));
      msg.append("\n");
      msg.append("Phone/Mail/Int %  : ");
      msg.append(getData("motoPercent"));
      msg.append("\n");
      msg.append("Comments          : ");
      msg.append(getData("comments"));
      msg.append("\n");
      
      MailMessage mail = new MailMessage();
      mail.setAddresses(eAddress);
      
      if( getData("businessEmail").equals("") )
      {
        mail.setFrom("webmaster@merchante-solutions.com");
      }
      else
      {
        mail.setFrom(getData("businessEmail"));
      }

      mail.setSubject(subject.toString());
      mail.setText(msg.toString());
      mail.send();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::submitData()", e.toString());
    }
  }
}
