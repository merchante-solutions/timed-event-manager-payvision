/*@lineinfo:filename=MerchAccountChange*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/MerchAccountChange.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:39p $
  Version            : $Revision: 14 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Time;
import java.text.DateFormat;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MerchAccountChange extends com.mes.database.SQLJDataBean
{
  public static final String DEFAULT_EMAIL_ADDRESS = "changes@merchante-solutions.com";
  
  private long    changeSequenceId        = 0L;
  private String  loginName               = "";
  private Date    changeRequestDate       = null;
  private Time    changeRequestTime       = null;

  // general information section
  private String  merchantName            = "";
  private String  merchantNumber          = "";
  private String  associationNumber       = "";
  private String  controlNumber           = "";
  private String  merchContactName        = "";
  private String  terminalId              = "";
  private String  bankName                = "";
  private String  bankRepName             = "";
  private String  bankRepPhone            = "";
  private String  changeDate              = "";

  // change types
  private String  discountFlag            = "N";
  private String  discountFrom            = "";
  private String  discountTo              = "";

  private String  otherFeeFlag            = "N";
  private String  otherFeeFrom            = "";
  private String  otherFeeTo              = "";

  private String  checkingAcctFlag        = "N";
  private String  checkingAcctFrom        = "";
  private String  checkingAcctTo          = "";

  private String  phoneFlag               = "N";
  private String  phoneFrom               = "";
  private String  phoneTo                 = "";

  private String  physicalAddressFlag     = "N";
  private String  physicalAddressFrom     = "";
  private String  physicalAddressTo       = "";

  private String  mailingAddressFlag      = "N";
  private String  mailingAddressFrom      = "";
  private String  mailingAddressTo        = "";

  private String  businessNameFlag        = "N";
  private String  businessNameFrom        = "";
  private String  businessNameTo          = "";

  private String  amexNumberFlag          = "N";
  private String  amexNumber              = "";
  private String  discoverNumberFlag      = "N";
  private String  discoverNumber          = "";
  private String  dinersNumberFlag        = "N";
  private String  dinersNumber            = "";

  private String  closeAccountFlag        = "N";
  private String  callTagAttn             = "";
  private String  callTagAddress          = "";
  private String  callTagCity             = "";
  private String  callTagState            = "";
  private String  callTagZip              = "";

  private String  comments                = "";
  
  // rush information
  private boolean rushRequest             = false;
  private String  rushDescription         = "";
  
  private String getRoutingAddress(int appType)
  {
    // get the email address for this type of application
    String result = DEFAULT_EMAIL_ADDRESS;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:125^7*/

//  ************************************************************
//  #sql [Ctx] { select  change_address 
//          from    org_app
//          where   app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  change_address  \n        from    org_app\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.MerchAccountChange",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
    }
    catch(Exception e)
    {
      logEntry("getRoutingAddress(" + appType + ") - " + result, e.toString());
    }
    
    return result;
  }

  public void getData(long changeSequenceId)
  {
    ResultSetIterator     it      = null;

    try
    {
      if(changeSequenceId > 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:148^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    account_change_request
//            where   change_sequence_id  = :changeSequenceId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    account_change_request\n          where   change_sequence_id  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.MerchAccountChange",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,changeSequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.utils.MerchAccountChange",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^9*/

        ResultSet rs = it.getResultSet();

        if(rs.next())
        {
          setMerchantNumber(rs.getString("merchant_number"));
          this.merchantName         = rs.getString("merchant_name");
          this.controlNumber        = rs.getString("control_number");
          this.merchContactName     = rs.getString("merch_contact_name");
          this.terminalId           = rs.getString("terminal_id");
          this.bankName             = rs.getString("bank_name");
          this.bankRepName          = rs.getString("bank_rep_name");
          this.bankRepPhone         = rs.getString("bank_rep_phone");
          this.changeDate           = rs.getString("change_date");
          this.discountFlag         = rs.getString("discount_flag");
          this.discountFrom         = rs.getString("discount_from");
          this.discountTo           = rs.getString("discount_to");
          this.otherFeeFlag         = rs.getString("other_fee_flag");
          this.otherFeeFrom         = rs.getString("other_fee_from");
          this.otherFeeTo           = rs.getString("other_fee_to");
          this.checkingAcctFlag     = rs.getString("checking_acct_flag");
          this.checkingAcctFrom     = rs.getString("checking_acct_from");
          this.checkingAcctTo       = rs.getString("checking_acct_to");
          this.phoneFlag            = rs.getString("phone_flag");
          this.phoneFrom            = rs.getString("phone_from");
          this.phoneTo              = rs.getString("phone_to");
          this.physicalAddressFlag  = rs.getString("physical_address_flag");
          this.physicalAddressFrom  = rs.getString("physical_address_from");
          this.physicalAddressTo    = rs.getString("physical_address_to");
          this.mailingAddressFlag   = rs.getString("mailing_address_flag");
          this.mailingAddressFrom   = rs.getString("mailing_address_from");
          this.mailingAddressTo     = rs.getString("mailing_address_to");
          this.businessNameFlag     = rs.getString("business_name_flag");
          this.businessNameFrom     = rs.getString("business_name_from");
          this.businessNameTo       = rs.getString("business_name_to");
          this.amexNumberFlag       = rs.getString("amex_number_flag");
          this.amexNumber           = rs.getString("amex_number");
          this.discoverNumberFlag   = rs.getString("discover_number_flag");
          this.discoverNumber       = rs.getString("discover_number");
          this.dinersNumberFlag     = rs.getString("diners_number_flag");
          this.dinersNumber         = rs.getString("diners_number");
          this.closeAccountFlag     = rs.getString("close_account_flag");
          this.callTagAttn          = rs.getString("call_tag_attn");
          this.callTagAddress       = rs.getString("call_tag_address");
          this.callTagCity          = rs.getString("call_tag_city");
          this.callTagState         = rs.getString("call_tag_state");
          this.callTagZip           = rs.getString("call_tag_zip");
          this.changeSequenceId     = rs.getLong  ("change_sequence_id");
          this.comments             = rs.getString("comments");
          this.loginName            = rs.getString("login_name");
          this.changeRequestDate    = rs.getDate  ("change_request_date");
          this.changeRequestTime    = rs.getTime  ("change_request_date");
          this.rushDescription      = rs.getString("rush_description");
        }
        
        it.close();
        
        if(this.rushDescription != null && ! this.rushDescription.equals(""))
        {
          this.rushRequest = true;
        }
      }
    }
    catch(Exception e)
    {
      addError("getData: " + e.toString());
      logEntry("getData(" + changeSequenceId + ")", e.toString());
    }
  }

  public void submitData(HttpServletRequest request, UserBean userLogin)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      // get rush description
      //System.out.println("getting rush description");
      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  description
//          from    rushes
//          where   type = :mesConstants.RUSH_ACCOUNT_CHANGE and
//                  hierarchy_node = :userLogin.getHierarchyNode()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1681 = userLogin.getHierarchyNode();
  try {
   String theSqlTS = "select  description\n        from    rushes\n        where   type =  :1  and\n                hierarchy_node =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.utils.MerchAccountChange",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mesConstants.RUSH_ACCOUNT_CHANGE);
   __sJT_st.setLong(2,__sJT_1681);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.utils.MerchAccountChange",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.rushDescription = rs.getString("description");
        this.rushRequest = true;
        //System.out.println(rushDescription);
      }
      
      it.close();
      
      // update record if it already exists
      //System.out.println("interfacing with database");
      if( ! recordExists("account_change_request", "change_sequence_id", changeSequenceId))
      {
        //System.out.println("  insert");
        /*@lineinfo:generated-code*//*@lineinfo:257^9*/

//  ************************************************************
//  #sql [Ctx] { insert into account_change_request
//            (
//              change_sequence_id,
//              change_request_date,
//              merchant_name,
//              merchant_number,
//              control_number,
//              merch_contact_name,
//              terminal_id,
//              bank_name,
//              bank_rep_name,
//              bank_rep_phone,
//              change_date,
//              discount_flag,
//              discount_from,
//              discount_to,
//              other_fee_flag,
//              other_fee_from,
//              other_fee_to,
//              checking_acct_flag,
//              checking_acct_from,
//              checking_acct_to,
//              phone_flag,
//              phone_from,
//              phone_to,
//              physical_address_flag,
//              physical_address_from,
//              physical_address_to,
//              mailing_address_flag,
//              mailing_address_from,
//              mailing_address_to,
//              business_name_flag,
//              business_name_from,
//              business_name_to,
//              amex_number_flag,
//              amex_number,
//              discover_number_flag,
//              discover_number,
//              diners_number_flag,
//              diners_number,
//              close_account_flag,
//              call_tag_attn,
//              call_tag_address,
//              call_tag_city,
//              call_tag_state,
//              call_tag_zip,
//              comments,
//              rush_description,
//              login_name
//            )
//            values
//            (
//              :changeSequenceId,
//              sysdate,
//              :merchantName,
//              :merchantNumber,
//              :controlNumber,
//              :merchContactName,
//              :terminalId,
//              :bankName,
//              :bankRepName,
//              :bankRepPhone,
//              :changeDate,
//              :discountFlag,
//              :discountFrom,
//              :discountTo,
//              :otherFeeFlag,
//              :otherFeeFrom,
//              :otherFeeTo,
//              :checkingAcctFlag,
//              :checkingAcctFrom,
//              :checkingAcctTo,
//              :phoneFlag,
//              :phoneFrom,
//              :phoneTo,
//              :physicalAddressFlag,
//              :physicalAddressFrom,
//              :physicalAddressTo,
//              :mailingAddressFlag,
//              :mailingAddressFrom,
//              :mailingAddressTo,
//              :businessNameFlag,
//              :businessNameFrom,
//              :businessNameTo,
//              :amexNumberFlag,
//              :amexNumber,
//              :discoverNumberFlag,
//              :discoverNumber,
//              :dinersNumberFlag,
//              :dinersNumber,
//              :closeAccountFlag,
//              :callTagAttn,
//              :callTagAddress,
//              :callTagCity,
//              :callTagState,
//              :callTagZip,
//              :comments,
//              :rushDescription,
//              :userLogin.getLoginName()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1682 = userLogin.getLoginName();
   String theSqlTS = "insert into account_change_request\n          (\n            change_sequence_id,\n            change_request_date,\n            merchant_name,\n            merchant_number,\n            control_number,\n            merch_contact_name,\n            terminal_id,\n            bank_name,\n            bank_rep_name,\n            bank_rep_phone,\n            change_date,\n            discount_flag,\n            discount_from,\n            discount_to,\n            other_fee_flag,\n            other_fee_from,\n            other_fee_to,\n            checking_acct_flag,\n            checking_acct_from,\n            checking_acct_to,\n            phone_flag,\n            phone_from,\n            phone_to,\n            physical_address_flag,\n            physical_address_from,\n            physical_address_to,\n            mailing_address_flag,\n            mailing_address_from,\n            mailing_address_to,\n            business_name_flag,\n            business_name_from,\n            business_name_to,\n            amex_number_flag,\n            amex_number,\n            discover_number_flag,\n            discover_number,\n            diners_number_flag,\n            diners_number,\n            close_account_flag,\n            call_tag_attn,\n            call_tag_address,\n            call_tag_city,\n            call_tag_state,\n            call_tag_zip,\n            comments,\n            rush_description,\n            login_name\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,\n             :42 ,\n             :43 ,\n             :44 ,\n             :45 ,\n             :46 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.utils.MerchAccountChange",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,changeSequenceId);
   __sJT_st.setString(2,merchantName);
   __sJT_st.setString(3,merchantNumber);
   __sJT_st.setString(4,controlNumber);
   __sJT_st.setString(5,merchContactName);
   __sJT_st.setString(6,terminalId);
   __sJT_st.setString(7,bankName);
   __sJT_st.setString(8,bankRepName);
   __sJT_st.setString(9,bankRepPhone);
   __sJT_st.setString(10,changeDate);
   __sJT_st.setString(11,discountFlag);
   __sJT_st.setString(12,discountFrom);
   __sJT_st.setString(13,discountTo);
   __sJT_st.setString(14,otherFeeFlag);
   __sJT_st.setString(15,otherFeeFrom);
   __sJT_st.setString(16,otherFeeTo);
   __sJT_st.setString(17,checkingAcctFlag);
   __sJT_st.setString(18,checkingAcctFrom);
   __sJT_st.setString(19,checkingAcctTo);
   __sJT_st.setString(20,phoneFlag);
   __sJT_st.setString(21,phoneFrom);
   __sJT_st.setString(22,phoneTo);
   __sJT_st.setString(23,physicalAddressFlag);
   __sJT_st.setString(24,physicalAddressFrom);
   __sJT_st.setString(25,physicalAddressTo);
   __sJT_st.setString(26,mailingAddressFlag);
   __sJT_st.setString(27,mailingAddressFrom);
   __sJT_st.setString(28,mailingAddressTo);
   __sJT_st.setString(29,businessNameFlag);
   __sJT_st.setString(30,businessNameFrom);
   __sJT_st.setString(31,businessNameTo);
   __sJT_st.setString(32,amexNumberFlag);
   __sJT_st.setString(33,amexNumber);
   __sJT_st.setString(34,discoverNumberFlag);
   __sJT_st.setString(35,discoverNumber);
   __sJT_st.setString(36,dinersNumberFlag);
   __sJT_st.setString(37,dinersNumber);
   __sJT_st.setString(38,closeAccountFlag);
   __sJT_st.setString(39,callTagAttn);
   __sJT_st.setString(40,callTagAddress);
   __sJT_st.setString(41,callTagCity);
   __sJT_st.setString(42,callTagState);
   __sJT_st.setString(43,callTagZip);
   __sJT_st.setString(44,comments);
   __sJT_st.setString(45,rushDescription);
   __sJT_st.setString(46,__sJT_1682);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^9*/
        
        // put this account change request into the queue
        QueueTools.insertQueue(changeSequenceId, MesQueues.Q_ACCT_CHANGE_NEW, userLogin);
        
        //System.out.println("getting date");
        /*@lineinfo:generated-code*//*@lineinfo:365^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  change_request_date
//            from    account_change_request
//            where   change_sequence_id = :changeSequenceId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  change_request_date\n          from    account_change_request\n          where   change_sequence_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.utils.MerchAccountChange",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,changeSequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.utils.MerchAccountChange",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^9*/
      
        rs = it.getResultSet();
      
        if(rs.next())
        {
          this.changeRequestDate = rs.getDate("change_request_date");
          this.changeRequestTime = rs.getTime("change_request_date");
        }
      
        it.close();
      
        //System.out.println("sending email");
        // now send email to anyone who cares
        StringBuffer msg = new StringBuffer("");
        StringBuffer subject = new StringBuffer("");
      
        if(rushRequest)
        {
          subject.append("RUSH (");
          subject.append(rushDescription);
          subject.append(") ");
        }
        subject.append("Change request: ");
      
        if(merchantName != null && ! merchantName.equals(""))
        {
          subject.append(merchantName);
          subject.append(" - ");
        }
      
        if(merchantNumber != null && ! merchantNumber.equals(""))
        {
          subject.append(merchantNumber);
          subject.append(" - ");
        }

        if(associationNumber != null && ! associationNumber.equals(""))
        {
          subject.append(associationNumber);
          subject.append(" - ");
        }
      
        if(controlNumber != null && ! controlNumber.equals(""))
        {
          subject.append(controlNumber);
        }
      
        msg.append(subject.toString());
        msg.append("\nRequested by ");
        msg.append(userLogin.getLoginName());
        msg.append(" (");
        msg.append(userLogin.getUserName());
        msg.append(")");
        msg.append(" on ");
        msg.append(getRequestDate());
        msg.append(" at ");
        msg.append(getRequestTime());
        msg.append("\n\n");
        msg.append("View this Change Request by following the link below:\n\n");
        msg.append("http://");
        msg.append(MesDefaults.getString(MesDefaults.DK_CHANGE_REQUEST_HOST));
//        msg.append(request.getHeader("HOST"));
        msg.append("/jsp/utils/account_change_request.jsp?action=review&changeSequenceId=");
        msg.append(changeSequenceId);
      
        MailMessage mail = new MailMessage();
        // this line is not needed because the email_adddresses table is not used
        // for change request emails any longer.
        //@mail.setAddresses(MesEmails.MSG_ADDRS_CHANGE_REQUEST);
      
        // route the application depending on the app_type of the user
        String  toAddress = getRoutingAddress(userLogin.getAppType());
        mail.addTo(toAddress);
      
        String fromEmail = userLogin.getEmail();
      
        if(fromEmail == null || fromEmail.equals(""))
        {
          fromEmail = userLogin.getLoginName() + "@merchante-solutions.com";
        }
        
        mail.setFrom(fromEmail);
        mail.setSubject(subject.toString());
        mail.setText(msg.toString());
        mail.send();
      }
      else
      {
        addError("Unable to overwrite existing change request");
      }
    }
    catch(Exception e)
    {
      addError("submitData: " + e.toString());
      logEntry("submitData(" + changeSequenceId + ")", e.toString());
    }
  }

  public boolean validate()
  {
    // make sure there is at least some information about the merchant
    if( isBlank(this.merchantName) &&
        isBlank(this.merchantNumber) &&
        isBlank(this.controlNumber) )
    {
      addError("Please provide a merchant name, number, or control number");
    }
    
    // make sure that the textarea fields are not too long
    
    if( this.physicalAddressFrom.length() > 200 ||
        this.physicalAddressTo.length() > 200 ||
        this.mailingAddressFrom.length() > 200 ||
        this.mailingAddressTo.length() > 200 )
    {
      addError("Entries in the address text boxes must be less than 200 characters");
    }
    
    if( this.comments.length() > 400)
    {
      addError("Please keep your comments to under 400 characters.");
    }

    return(!hasErrors());
  }

  public void setMerchantName(String merchantName)
  {
    if(this.merchantName == null || this.merchantName.equals(""))
    {
      this.merchantName = merchantName;
    }
  }
  public String getMerchantName()
  {
    return this.merchantName;
  }

  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
    
    // get the merchant name from the mif if it exists
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:518^7*/

//  ************************************************************
//  #sql [Ctx] { select  dba_name,
//                  dmagent
//           
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dba_name,\n                dmagent\n          \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.utils.MerchAccountChange",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.merchantName = (String)__sJT_rs.getString(1);
   this.associationNumber = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:526^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }

  public String getAssociationNumber()
  {
    return this.associationNumber;
  }

  public void setControlNumber(String controlNumber)
  {
    this.controlNumber = controlNumber;
  }
  public String getControlNumber()
  {
    return this.controlNumber;
  }

  public void setMerchContactName(String merchContactName)
  {
    this.merchContactName = merchContactName;
  }
  public String getMerchContactName()
  {
    return this.merchContactName;
  }

  public void setTerminalId(String terminalId)
  {
    this.terminalId = terminalId;
  }
  public String getTerminalId()
  {
    return this.terminalId;
  }

  public void setBankName(String bankName)
  {
    this.bankName = bankName;
  }
  public String getBankName()
  {
    return this.bankName;
  }

  public void setBankRepName(String bankRepName)
  {
    this.bankRepName = bankRepName;
  }
  public String getBankRepName()
  {
    return this.bankRepName;
  }

  public void setBankRepPhone(String bankRepPhone)
  {
    this.bankRepPhone = bankRepPhone;
  }
  public String getBankRepPhone()
  {
    return this.bankRepPhone;
  }

  public void setChangeDate(String changeDate)
  {
    this.changeDate = changeDate;
  }
  public String getChangeDate()
  {
    return this.changeDate;
  }

  public void setDiscountFrom(String discountFrom)
  {
    this.discountFrom = discountFrom;
  }
  public String getDiscountFrom()
  {
    return this.discountFrom;
  }

  public void setDiscountTo(String discountTo)
  {
    this.discountTo = discountTo;
  }
  public String getDiscountTo()
  {
    return this.discountTo;
  }

  public void setOtherFeeFrom(String otherFeeFrom)
  {
    this.otherFeeFrom = otherFeeFrom;
  }
  public String getOtherFeeFrom()
  {
    return this.otherFeeFrom;
  }

  public void setOtherFeeTo(String otherFeeTo)
  {
    this.otherFeeTo = otherFeeTo;
  }
  public String getOtherFeeTo()
  {
    return this.otherFeeTo;
  }

  public void setCheckingAcctFrom(String checkingAcctFrom)
  {
    this.checkingAcctFrom = checkingAcctFrom;
  }
  public String getCheckingAcctFrom()
  {
    return this.checkingAcctFrom;
  }

  public void setCheckingAcctTo(String checkingAcctTo)
  {
    this.checkingAcctTo = checkingAcctTo;
  }
  public String getCheckingAcctTo()
  {
    return this.checkingAcctTo;
  }

  public void setPhoneFrom(String phoneFrom)
  {
    this.phoneFrom = phoneFrom;
  }
  public String getPhoneFrom()
  {
    return this.phoneFrom;
  }

  public void setPhoneTo(String phoneTo)
  {
    this.phoneTo = phoneTo;
  }
  public String getPhoneTo()
  {
    return this.phoneTo;
  }

  public void setPhysicalAddressFrom(String physicalAddressFrom)
  {
    this.physicalAddressFrom = physicalAddressFrom;
  }
  public String getPhysicalAddressFrom()
  {
    return this.physicalAddressFrom;
  }

  public void setPhysicalAddressTo(String physicalAddressTo)
  {
    this.physicalAddressTo = physicalAddressTo;
  }
  public String getPhysicalAddressTo()
  {
    return this.physicalAddressTo;
  }

  public void setMailingAddressFrom(String mailingAddressFrom)
  {
    this.mailingAddressFrom = mailingAddressFrom;
  }
  public String getMailingAddressFrom()
  {
    return this.mailingAddressFrom;
  }

  public void setMailingAddressTo(String mailingAddressTo)
  {
    this.mailingAddressTo = mailingAddressTo;
  }
  public String getMailingAddressTo()
  {
    return this.mailingAddressTo;
  }

  public void setBusinessNameFrom(String businessNameFrom)
  {
    this.businessNameFrom = businessNameFrom;
  }
  public String getBusinessNameFrom()
  {
    return this.businessNameFrom;
  }

  public void setBusinessNameTo(String businessNameTo)
  {
    this.businessNameTo = businessNameTo;
  }
  public String getBusinessNameTo()
  {
    return this.businessNameTo;
  }

  public void setAmexNumber(String amexNumber)
  {
    this.amexNumber = amexNumber;
  }
  public String getAmexNumber()
  {
    return this.amexNumber;
  }

  public void setDiscoverNumber(String discoverNumber)
  {
    this.discoverNumber = discoverNumber;
  }
  public String getDiscoverNumber()
  {
    return this.discoverNumber;
  }

  public void setDinersNumber(String dinersNumber)
  {
    this.dinersNumber = dinersNumber;
  }
  public String getDinersNumber()
  {
    return this.dinersNumber;
  }

  public void setCallTagAttn(String callTagAttn)
  {
    this.callTagAttn = callTagAttn;
  }
  public String getCallTagAttn()
  {
    return this.callTagAttn;
  }

  public void setCallTagAddress(String callTagAddress)
  {
    this.callTagAddress = callTagAddress;
  }
  public String getCallTagAddress()
  {
    return this.callTagAddress;
  }

  public void setCallTagCity(String callTagCity)
  {
    this.callTagCity = callTagCity;
  }
  public String getCallTagCity()
  {
    return this.callTagCity;
  }

  public void setCallTagState(String callTagState)
  {
    this.callTagState = callTagState;
  }
  public String getCallTagState()
  {
    return this.callTagState;
  }

  public void setCallTagZip(String callTagZip)
  {
    this.callTagZip = callTagZip;
  }
  public String getCallTagZip()
  {
    return this.callTagZip;
  }

  public void setComments(String comments)
  {
    this.comments = comments;
  }
  public String getComments()
  {
    return this.comments;
  }

  public void setDiscountFlag(String discountFlag)
  {
    this.discountFlag = discountFlag;
  }
  public String getDiscountFlag()
  {
    return this.discountFlag;
  }
  public String getDiscountFlagChecked()
  {
    String result = "";

    if(this.discountFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setOtherFeeFlag(String otherFeeFlag)
  {
    this.otherFeeFlag = otherFeeFlag;
  }
  public String getOtherFeeFlag()
  {
    return this.otherFeeFlag;
  }
  public String getOtherFeeFlagChecked()
  {
    String result = "";

    if(this.otherFeeFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setCheckingAcctFlag(String checkingAcctFlag)
  {
    this.checkingAcctFlag = checkingAcctFlag;
  }
  public String getCheckingAcctFlag()
  {
    return this.checkingAcctFlag;
  }
  public String getCheckingAcctFlagChecked()
  {
    String result = "";

    if(this.checkingAcctFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setPhoneFlag(String phoneFlag)
  {
    this.phoneFlag = phoneFlag;
  }
  public String getPhoneFlag()
  {
    return this.phoneFlag;
  }
  public String getPhoneFlagChecked()
  {
    String result = "";

    if(this.phoneFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setPhysicalAddressFlag(String physicalAddressFlag)
  {
    this.physicalAddressFlag = physicalAddressFlag;
  }
  public String getPhysicalAddressFlag()
  {
    return this.physicalAddressFlag;
  }
  public String getPhysicalAddressFlagChecked()
  {
    String result = "";

    if(this.physicalAddressFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setMailingAddressFlag(String mailingAddressFlag)
  {
    this.mailingAddressFlag = mailingAddressFlag;
  }
  public String getMailingAddressFlag()
  {
    return this.mailingAddressFlag;
  }
  public String getMailingAddressFlagChecked()
  {
    String result = "";

    if(this.mailingAddressFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setBusinessNameFlag(String businessNameFlag)
  {
    this.businessNameFlag = businessNameFlag;
  }
  public String getBusinessNameFlag()
  {
    return this.businessNameFlag;
  }
  public String getBusinessNameFlagChecked()
  {
    String result = "";

    if(this.businessNameFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setAmexNumberFlag(String amexNumberFlag)
  {
    this.amexNumberFlag = amexNumberFlag;
  }
  public String getAmexNumberFlag()
  {
    return this.amexNumberFlag;
  }
  public String getAmexNumberFlagChecked()
  {
    String result = "";

    if(amexNumberFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setDiscoverNumberFlag(String discoverNumberFlag)
  {
    this.discoverNumberFlag = discoverNumberFlag;
  }
  public String getDiscoverNumberFlag()
  {
    return this.discoverNumberFlag;
  }
  public String getDiscoverNumberFlagChecked()
  {
    String result = "";

    if(discoverNumberFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setDinersNumberFlag(String dinersNumberFlag)
  {
    this.dinersNumberFlag = dinersNumberFlag;
  }
  public String getDinersNumberFlag()
  {
    return this.dinersNumberFlag;
  }
  public String getDinersNumberFlagChecked()
  {
    String result = "";

    if(dinersNumberFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }

  public void setCloseAccountFlag(String closeAccountFlag)
  {
    this.closeAccountFlag = closeAccountFlag;
  }
  public String getCloseAccountFlag()
  {
    return this.closeAccountFlag;
  }
  public String getCloseAccountFlagChecked()
  {
    String result = "";

    if(this.closeAccountFlag.equals("Y"))
    {
      result = "checked";
    }

    return result;
  }
  
  public String getLoginName()
  {
    return this.loginName;
  }

  public void setChangeSequenceId(String changeSequenceId)
  {
    try
    {
      setChangeSequenceId(Long.parseLong(changeSequenceId));
    }
    catch(Exception e)
    {
      logEntry("setChangeSequenceId(" + changeSequenceId + ")", e.toString());
    }
  }
  public void setChangeSequenceId(long changeSequenceId)
  {
    this.changeSequenceId = changeSequenceId;
  }
  public long getChangeSequenceId()
    throws Exception
  {
    ResultSetIterator     it        = null;
    boolean               wasStale  = false;
    // get a new change sequence id if necessary
    if(this.changeSequenceId == 0L)
    {
      try
      {
        if(isConnectionStale())
        {
          connect();
          wasStale = true;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1072^9*/

//  ************************************************************
//  #sql [Ctx] { select  account_change_sequence.nextval 
//            from    sys.dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  account_change_sequence.nextval  \n          from    sys.dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.utils.MerchAccountChange",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   changeSequenceId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1076^9*/
      }
      catch(Exception e)
      {
        addError("getChangeSequenceId: " + e.toString());
        logEntry("getChangeSequenceId()", e.toString());
        throw new Exception("Unable to retrieve new sequence Id: " + e.toString());
      }
      finally
      {
        if( wasStale )
        {
          cleanUp();
        }
      }
    }

    return this.changeSequenceId;
  }
  
  public String getRequestDate()
  {
    String result = "";
    
    try
    {
      if(this.changeRequestDate != null)
      {
        result = DateFormat.getDateInstance(DateFormat.FULL).format(this.changeRequestDate); 
      }
    }
    catch(Exception e)
    {
      logEntry("getRequestDate()", e.toString());
    }
    
    return result;
  }
  
  public String getRequestTime()
  {
    String result = "";
    
    try
    {
      if(this.changeRequestTime != null)
      {
        result = DateFormat.getTimeInstance(DateFormat.LONG).format(this.changeRequestTime);
      }
    }
    catch(Exception e)
    {
      logEntry("getRequestTime()", e.toString());
    }
    
    return result;
  }
  
  public boolean getRushRequest()
  {
    return this.rushRequest;
  }
  public String getRushDescription()
  {
    return this.rushDescription;
  }
}/*@lineinfo:generated-code*/