/*@lineinfo:filename=AmexRejectLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/utils/AmexRejectLoader.sqlj $

  Description:

  Last Modified By   : $Author: mduyck $
  Last Modified Date : $Date: 2012-05-01 14:30:55 -0700 (Tue, 01 May 2012) $
  Version            : $Revision: 20102 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionDirect;
import com.mes.net.MailMessage;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.TridentTools;

public class AmexRejectLoader extends SQLJConnectionDirect
{
  public void loadRejectFile( String loadFilename, boolean deleteFirst )
  {
    String            amexSeNum   = null;
    String            cardNumber  = null;
    BufferedReader    in          = null;
    String            line        = null;
    String            reasonCode  = null;
    int               recCount    = 0;
    long              recId       = 0L;
    String            recType     = null;
    String            refNum      = null;
    double            tranAmount  = 0.0;
    Date              tranDate    = null;
    
    try
    {
      in = new BufferedReader( new FileReader( loadFilename ) );
      
      /*@lineinfo:generated-code*//*@lineinfo:61^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.AmexRejectLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^61*/
      /*@lineinfo:generated-code*//*@lineinfo:62^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^27*/
      
      if ( deleteFirst )
      {
        /*@lineinfo:generated-code*//*@lineinfo:66^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    reject_record   rr
//            where   rr.load_file_id = load_filename_to_load_file_id(:loadFilename)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    reject_record   rr\n          where   rr.load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.AmexRejectLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^9*/
      }
      
      while( ( line = in.readLine() ) != null )
      {
        System.out.print("Loading Record " + (++recCount) + "                     \r");
        recType = line.substring(0,3).trim();
        
        if ("TAB".equals(recType) )
        {
          java.util.Date javaDate = DateTimeFormatter.parseDate(line.substring(71,79),"yyyyMMdd");
          tranDate   = new java.sql.Date( javaDate.getTime());
          amexSeNum  = line.substring(111,126).trim();
          refNum     = line.substring(219,249).trim();
          cardNumber = TridentTools.encodeCardNumber(line.substring(48,67).trim());
          tranAmount = Double.parseDouble(line.substring(88,98) + "." + line.substring(98,100));
          recId      = 0L;
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:91^13*/

//  ************************************************************
//  #sql [Ctx] { select  s.rec_id 
//                
//                from    amex_settlement   s
//                where   s.amex_se_number = :amexSeNum
//                        and s.transaction_date = :tranDate
//                        and s.card_number = :cardNumber
//                        and s.transaction_amount = :tranAmount
//                        and s.reference_number = :refNum
//                        and nvl(s.test_flag,'Y') = 'N'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  s.rec_id \n               \n              from    amex_settlement   s\n              where   s.amex_se_number =  :1 \n                      and s.transaction_date =  :2 \n                      and s.card_number =  :3 \n                      and s.transaction_amount =  :4 \n                      and s.reference_number =  :5 \n                      and nvl(s.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.utils.AmexRejectLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,amexSeNum);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setString(3,cardNumber);
   __sJT_st.setDouble(4,tranAmount);
   __sJT_st.setString(5,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^13*/
          }
          catch( java.sql.SQLException sqe )
          {
            MailMessage.sendSystemErrorEmail("Amex Reject Loading Error", 
                                              "Failed to resolve settlement record id.\n\n" 
                                              + "SE Number    : " + amexSeNum    + "\n"
                                              + "Tran Date    : " + tranDate     + "\n"
                                              + "Ref Num      : " + refNum       + "\n"
                                              + "Card Number  : " + refNum       + "\n"
                                              + "Tran Amount  : " + tranAmount   + "\n"
                                              + "Filename     : " + loadFilename + "\n"
                                              + "\n\n" + sqe.toString() );
            continue;   // move to the next line
          }
        }
        else if ( "ERR".equals(recType) )
        {
          if ( recId == 0L ) { continue; }  // skip missing recIds
          
          // get a round number for this reject
          int round = 0;
          /*@lineinfo:generated-code*//*@lineinfo:124^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(max(round),0)+1
//              
//              from    reject_record   rr
//              where   rr.rec_id = :recId
//                      and rr.reject_type = 'AM'
//                      and rr.presentment = 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(max(round),0)+1\n             \n            from    reject_record   rr\n            where   rr.rec_id =  :1 \n                    and rr.reject_type = 'AM'\n                    and rr.presentment = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.utils.AmexRejectLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   round = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^11*/
          
          // insert one row for each reject reason code
          for( int offset = 11; offset < line.length(); offset += 4 )
          {
            reasonCode = line.substring(offset,offset+4).trim();
            if ( "".equals(reasonCode) ) { break; }  // no more errors exit loop
            
            /*@lineinfo:generated-code*//*@lineinfo:140^13*/

//  ************************************************************
//  #sql [Ctx] { insert into reject_record
//                (
//                  rec_id,
//                  reject_id,
//                  reject_type,
//                  status,
//                  round,
//                  creation_date,
//                  reject_date,
//                  presentment,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :recId,
//                  :reasonCode,
//                  'AM',
//                  -1,
//                  :round,
//                  sysdate,
//                  get_file_date(:loadFilename),
//                  0,
//                  :loadFilename,
//                  load_filename_to_load_file_id(:loadFilename)
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into reject_record\n              (\n                rec_id,\n                reject_id,\n                reject_type,\n                status,\n                round,\n                creation_date,\n                reject_date,\n                presentment,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                'AM',\n                -1,\n                 :3 ,\n                sysdate,\n                get_file_date( :4 ),\n                0,\n                 :5 ,\n                load_filename_to_load_file_id( :6 )\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.utils.AmexRejectLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,reasonCode);
   __sJT_st.setInt(3,round);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setString(6,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^13*/
          }
          
          // if reject exists in queue system, move it to the unworked queue
          if ( QueueTools.hasItemEnteredQueue(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
          {
            QueueTools.moveQueueItem(recId,
                                     QueueTools.getCurrentQueueType(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                     MesQueues.Q_REJECTS_UNWORKED,
                                     null,
                                     null);
          }
          else  // reject does not exist, add recId to the unworked queue
          {
            QueueTools.insertQueue(recId, MesQueues.Q_REJECTS_UNWORKED);
          }
        }
      }
      System.out.println();
    }
    catch(Exception e)
    {
      logEntry("loadRejectFile(" + loadFilename + "," + recCount + ")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }
  
  public static void main(String[] args)
  {
    AmexRejectLoader         ldr     = null;
    
    try
    {
      ldr = new AmexRejectLoader();
      ldr.connect(true);
      
      if ( "loadRejectFile".equals(args[0]) )
      {
        ldr.loadRejectFile(args[1],false);
      }
      else if ( "cleanLoadRejectFile".equals(args[0]) )
      {
        ldr.loadRejectFile(args[1],true);
      }
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ ldr.cleanUp(); } catch(Exception e){}
    }
  }
}/*@lineinfo:generated-code*/