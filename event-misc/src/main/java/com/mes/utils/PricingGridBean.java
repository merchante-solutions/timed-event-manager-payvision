/*@lineinfo:filename=PricingGridBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/PricingGridBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/13/01 2:19p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;


public class PricingGridBean extends com.mes.database.SQLJDataBean
{
  public static final int     SOURCE_CBT          = 1;
  public static final int     SOURCE_MES          = 2;
  
  public static final int     GRID_CBT_RETAIL     = 1;
  public static final int     GRID_CBT_MOTO       = 2;
  public static final int     GRID_CBT_LODGING    = 3;
  
  public static final double  MIN_AVG_TICKET      = 5.00;
  
  protected double      avgTicket       = 0.0;
  protected long        annualSales     = 0L;
  protected long        tranCount       = 0;
  
  public int getBasisPoints(int gridType, double avgTicket, long annualSales)
  {
    ResultSetIterator       it              = null;
    ResultSet               rs              = null;
    int                     basisPoints     = -1;
    
    try
    {
      // set avg ticket
      double adjustedAvgTicket = (avgTicket < MIN_AVG_TICKET) ? MIN_AVG_TICKET : avgTicket;
      /*@lineinfo:generated-code*//*@lineinfo:61^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    basis_points
//          from      pricing_grids
//          where     grid_type = :gridType and
//                    avg_ticket_min <= :adjustedAvgTicket and
//                    annual_sales_min <= :annualSales
//          order by  avg_ticket_min desc, 
//                    annual_sales_min desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    basis_points\n        from      pricing_grids\n        where     grid_type =  :1  and\n                  avg_ticket_min <=  :2  and\n                  annual_sales_min <=  :3 \n        order by  avg_ticket_min desc, \n                  annual_sales_min desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.PricingGridBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,gridType);
   __sJT_st.setDouble(2,adjustedAvgTicket);
   __sJT_st.setLong(3,annualSales);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.utils.PricingGridBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:70^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        basisPoints = rs.getInt("basis_points");
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getBasisPoints()", e.toString());
    }
    
    return basisPoints;
  }
  
  public int getBasisPoints(int gridType, long tranCount, long annualSales)
  {
    int         basisPoints = -1;
    double      avgTkt      = 0.0;
    
    try
    {
      if(tranCount > 0)
      {
        // calculate average ticket
        avgTkt = (double)annualSales / (double)tranCount;
        
        // get basis points
        basisPoints = getBasisPoints(gridType, avgTkt, annualSales);
      }
    }
    catch(Exception e)
    {
      logEntry("getBasisPoints()", e.toString());
    }
    
    return basisPoints;
  }
  
  public void setAnnualSales(String annualSales)
  {
    this.annualSales = getDigitsNoDecimal(annualSales);
  }
  public String getAnnualSales()
  {
    String result = "";
    
    if(this.annualSales > 0L)
    {
      result = String.valueOf(this.annualSales);
    }
    
    return result;
  }
  
  public void setAvgTicket(String avgTicket)
  {
    this.avgTicket = getDigitsWithDecimal(avgTicket);
  }
  public void setAvgTicket(double avgTicket)
  {
    this.avgTicket = avgTicket;
  }
  public String getAvgTicket()
  {
    String result = "";
    
    if(this.avgTicket > 0.0)
    {
      result = String.valueOf(this.avgTicket);
    }
    
    return result;
  }
  
  public void setTranCount(String tranCount)
  {
    this.tranCount = getDigits(tranCount);
  }
  public String getTranCount()
  {
    String result = "";
    
    if(this.tranCount > 0)
    {
      result = String.valueOf(this.tranCount);
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/