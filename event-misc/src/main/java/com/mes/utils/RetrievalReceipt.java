/*@lineinfo:filename=RetrievalReceipt*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/utils/RetrievalReceipt.sqlj $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/05/03 5:35p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class RetrievalReceipt extends SQLJConnectionBase
{
  // members for searching by reference number
  private String    ref             = "";
  private String    card            = "";
  private String    tdate           = "";
  private String    merchant        = "";
  private Date      tranDate        = null;

  // members for searching by primary key
  private Date      batchDate       = null;
  private int       batchNumber     = 0;
  private int       recId           = 0;

  private boolean   isSingleSearch  = false;

  private Vector    details         = new Vector();

  public RetrievalReceipt()
  {
  }

  public void getData()
  {
    ResultSetIterator   it          = null;
    StringBuffer        hackedCard  = new StringBuffer(card);
    
    for( int i = 4; i < 6; ++i )
    {
      if ( Character.isDigit( hackedCard.charAt(i) ) )
      {
        hackedCard.setCharAt(i,'x');
      }
    }    
    
    try
    {
      connect();
      
      if(isSingleSearch)
      {
        /*@lineinfo:generated-code*//*@lineinfo:80^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_date                 batch_date,
//                    dt.batch_number               batch_number,
//                    dt.ddf_dt_id                  rec_id,
//                    dt.transaction_date           tran_date,
//                    dt.card_type                  card_type,
//                    dt.cardholder_account_number  card_number,
//                    dt.reference_number           ref_number,
//                    dt.authorization_num          auth_code,
//                    p.pos_entry_desc              pos_entry,
//                    dt.transaction_amount         tran_amount,
//                    m.merchant_number             merchant_number,
//                    m.name2_line_1                merchant_name,
//                    m.addr2_line_1                address_1,
//                    m.addr2_line_2                address_2,
//                    m.city2_line_4                city,
//                    m.state2_line_4               state,
//                    m.zip2_line_4                 zip,
//                    md.descriptor                 descriptor
//            from    daily_detail_file_dt          dt,
//                    pos_entry_mode_desc           p,
//                    mif                           m,
//                    merchant_descriptors          md
//            where   dt.batch_date     = :this.batchDate and
//                    dt.batch_number   = :this.batchNumber and
//                    dt.ddf_dt_id      = :this.recId and
//                    m.merchant_number = dt.merchant_account_number and
//                    dt.pos_entry_mode = p.pos_entry_code(+) and
//                    m.merchant_number = md.merchant_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.batch_date                 batch_date,\n                  dt.batch_number               batch_number,\n                  dt.ddf_dt_id                  rec_id,\n                  dt.transaction_date           tran_date,\n                  dt.card_type                  card_type,\n                  dt.cardholder_account_number  card_number,\n                  dt.reference_number           ref_number,\n                  dt.authorization_num          auth_code,\n                  p.pos_entry_desc              pos_entry,\n                  dt.transaction_amount         tran_amount,\n                  m.merchant_number             merchant_number,\n                  m.name2_line_1                merchant_name,\n                  m.addr2_line_1                address_1,\n                  m.addr2_line_2                address_2,\n                  m.city2_line_4                city,\n                  m.state2_line_4               state,\n                  m.zip2_line_4                 zip,\n                  md.descriptor                 descriptor\n          from    daily_detail_file_dt          dt,\n                  pos_entry_mode_desc           p,\n                  mif                           m,\n                  merchant_descriptors          md\n          where   dt.batch_date     =  :1  and\n                  dt.batch_number   =  :2  and\n                  dt.ddf_dt_id      =  :3  and\n                  m.merchant_number = dt.merchant_account_number and\n                  dt.pos_entry_mode = p.pos_entry_code(+) and\n                  m.merchant_number = md.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.RetrievalReceipt",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,this.batchDate);
   __sJT_st.setInt(2,this.batchNumber);
   __sJT_st.setInt(3,this.recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.utils.RetrievalReceipt",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:114^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_date                 batch_date,
//                    dt.batch_number               batch_number,
//                    dt.ddf_dt_id                  rec_id,
//                    dt.transaction_date           tran_date,
//                    dt.card_type                  card_type,
//                    dt.cardholder_account_number  card_number,
//                    dt.reference_number           ref_number,
//                    dt.authorization_num          auth_code,
//                    p.pos_entry_desc              pos_entry,
//                    dt.transaction_amount         tran_amount,
//                    m.merchant_number             merchant_number,
//                    m.name2_line_1                merchant_name,
//                    m.addr2_line_1                address_1,
//                    m.addr2_line_2                address_2,
//                    m.city2_line_4                city,
//                    m.state2_line_4               state,
//                    m.zip2_line_4                 zip,
//                    md.descriptor                 descriptor
//            from    daily_detail_file_dt          dt,
//                    pos_entry_mode_desc           p,
//                    mif                           m,
//                    merchant_descriptors          md
//            where   dt.merchant_account_number = :merchant and
//                    dt.batch_date between (:tranDate-7) and (:tranDate+7) and
//                    (
//                      ( 
//                        length(:ref) = 23 and
//                        dt.reference_number = :ref 
//                      ) or
//                      ( 
//                        length(:ref) <= 11 and
//                        lpad(:ref,11,'0') in 
//                          ( 
//                            substr(dt.reference_number,12,11),       -- vital
//                            '01' || substr(dt.reference_number,12,9) -- sabre
//                          )
//                      )
//                    ) and
//                    (
//                      dt.cardholder_account_number = :card or
//                      dt.cardholder_account_number = :hackedCard.toString()
//                    ) and
//                    dt.pos_entry_mode = p.pos_entry_code(+) and
//                    m.merchant_number = dt.merchant_account_number and
//                    m.merchant_number = md.merchant_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1683 = hackedCard.toString();
  try {
   String theSqlTS = "select  dt.batch_date                 batch_date,\n                  dt.batch_number               batch_number,\n                  dt.ddf_dt_id                  rec_id,\n                  dt.transaction_date           tran_date,\n                  dt.card_type                  card_type,\n                  dt.cardholder_account_number  card_number,\n                  dt.reference_number           ref_number,\n                  dt.authorization_num          auth_code,\n                  p.pos_entry_desc              pos_entry,\n                  dt.transaction_amount         tran_amount,\n                  m.merchant_number             merchant_number,\n                  m.name2_line_1                merchant_name,\n                  m.addr2_line_1                address_1,\n                  m.addr2_line_2                address_2,\n                  m.city2_line_4                city,\n                  m.state2_line_4               state,\n                  m.zip2_line_4                 zip,\n                  md.descriptor                 descriptor\n          from    daily_detail_file_dt          dt,\n                  pos_entry_mode_desc           p,\n                  mif                           m,\n                  merchant_descriptors          md\n          where   dt.merchant_account_number =  :1  and\n                  dt.batch_date between ( :2 -7) and ( :3 +7) and\n                  (\n                    ( \n                      length( :4 ) = 23 and\n                      dt.reference_number =  :5  \n                    ) or\n                    ( \n                      length( :6 ) <= 11 and\n                      lpad( :7 ,11,'0') in \n                        ( \n                          substr(dt.reference_number,12,11),       -- vital\n                          '01' || substr(dt.reference_number,12,9) -- sabre\n                        )\n                    )\n                  ) and\n                  (\n                    dt.cardholder_account_number =  :8  or\n                    dt.cardholder_account_number =  :9 \n                  ) and\n                  dt.pos_entry_mode = p.pos_entry_code(+) and\n                  m.merchant_number = dt.merchant_account_number and\n                  m.merchant_number = md.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.RetrievalReceipt",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchant);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setDate(3,tranDate);
   __sJT_st.setString(4,ref);
   __sJT_st.setString(5,ref);
   __sJT_st.setString(6,ref);
   __sJT_st.setString(7,ref);
   __sJT_st.setString(8,card);
   __sJT_st.setString(9,__sJT_1683);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.utils.RetrievalReceipt",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^9*/
      }

      ResultSet rs = it.getResultSet();

      while(rs.next())
      {
        String descriptor = (rs.getString("descriptor") == null) ? 
                              "Unknown" : 
                              rs.getString("descriptor");
                              
        RetrievalTranData rtd =
          new RetrievalTranData(rs.getDate("batch_date"),
                                rs.getInt("batch_number"),
                                rs.getInt("rec_id"),
                                rs.getDate("tran_date"),
                                rs.getString("card_type"),
                                rs.getString("card_number"),
                                rs.getString("ref_number"),
                                rs.getString("auth_code"),
                                rs.getString("pos_entry"),
                                rs.getDouble("tran_amount"),
                                rs.getString("merchant_number"),
                                rs.getString("merchant_name"),
                                rs.getString("address_1"),
                                rs.getString("address_2"),
                                rs.getString("city"),
                                rs.getString("state"),
                                rs.getString("zip"),
                                descriptor);

        details.add(rtd);
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public Vector getDetails()
  {
    return this.details;
  }

  public void setRef(String ref)
  {
    this.ref = ref;
  }
  public void setCard(String card)
  {
    this.card = card;
  }
  public void setMerchant(String merchant)
  {
    this.merchant = merchant;
  }
  public void setTdate(String tdate)
  {
    this.tdate = tdate;
    this.tranDate = DateTimeFormatter.parseSQLDate(tdate, "MMddyyyy");
  }
  public void setBatchDate(String batchDate)
  {
    this.batchDate = DateTimeFormatter.parseSQLDate(batchDate, "MMddyyyy");
  }
  public void setBatchNumber(String batchNumber)
  {
    try
    {
      this.batchNumber = Integer.parseInt(batchNumber);
    }
    catch(Exception e)
    {
      logEntry("setBatchNumber(" + batchNumber + ")", e.toString());
    }
  }
  public void setRecId(String recId)
  {
    try
    {
      this.isSingleSearch = true;
      this.recId = Integer.parseInt(recId);
    }
    catch(Exception e)
    {
      logEntry("setRecId(" + recId + ")", e.toString());
    }
  }

  public class RetrievalTranData
  {
    public String   batchDateParam  = "";
    public int      batchNumber     = 0;
    public int      recId           = 0;

    public String   batchDate       = "";
    public String   tranDate        = "";
    public String   cardType        = "";
    public String   cardNumber      = "";
    public String   refNumber       = "";
    public String   authCode        = "";
    public String   posEntry        = "";
    public String   tranAmount      = "";
    public String   merchantNumber  = "";
    public String   merchantName    = "";
    public String   address1        = "";
    public String   address2        = "";
    public String   city            = "";
    public String   state           = "";
    public String   zip             = "";
    public String   descriptor      = "";

    public RetrievalTranData()
    {
    }

    public RetrievalTranData( Date    batchDate,
                              int     batchNumber,
                              int     recId,
                              Date    tranDate,
                              String  cardType,
                              String  cardNumber,
                              String  refNumber,
                              String  authCode,
                              String  posEntry,
                              double  tranAmount,
                              String  merchantNumber,
                              String  merchantName,
                              String  address1,
                              String  address2,
                              String  city,
                              String  state,
                              String  zip,
                              String  descriptor )
    {
      setBatchDate(batchDate);
      setBatchNumber(batchNumber);
      setRecId(recId);
      setTranDate(tranDate);
      setCardType(cardType);
      setCardNumber(cardNumber);
      setRefNumber(refNumber);
      setAuthCode(authCode);
      setPosEntry(posEntry);
      setTranAmount(tranAmount);
      setMerchantNumber(merchantNumber);
      setMerchantName(merchantName);
      setAddress1(address1);
      setAddress2(address2);
      setCity(city);
      setState(state);
      setZip(zip);
      setDescriptor(descriptor);
    }

    public void setBatchDate(Date batchDate)
    {
      try
      {
        this.batchDateParam = DateTimeFormatter.getFormattedDate(batchDate, "MMddyyyy");
        this.batchDate      = DateTimeFormatter.getFormattedDate(batchDate, "MM/dd/yy");
      }
      catch(Exception e)
      {
        logEntry("setBatchDate()", e.toString());
      }
    }
    public void setBatchNumber(int batchNumber)
    {
      this.batchNumber = batchNumber;
    }
    public void setRecId(int recId)
    {
      this.recId = recId;
    }
    public void setTranDate(Date tranDate)
    {
      SimpleDateFormat  visualFmt   = new SimpleDateFormat("MM/dd/yy");

      try
      {
        this.tranDate = DateTimeFormatter.getFormattedDate(tranDate, "MM/dd/yy");
      }
      catch(Exception e)
      {
        logEntry("setTranDate()", e.toString());
      }
    }
    public void setCardType(String cardType)
    {
      this.cardType = cardType;
    }
    public void setCardNumber(String cardNumber)
    {
      this.cardNumber = cardNumber;
    }
    public void setRefNumber(String refNumber)
    {
      this.refNumber = refNumber;
    }
    public void setAuthCode(String authCode)
    {
      this.authCode = authCode;
    }
    public void setPosEntry(String posEntry)
    {
      this.posEntry = posEntry;
    }
    public void setTranAmount(double tranAmount)
    {
      try
      {
        this.tranAmount = MesMath.toCurrency(tranAmount);
      }
      catch(Exception e)
      {
        logEntry("setTranAmount()", e.toString());
      }
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;
    }
    public void setMerchantName(String merchantName)
    {
      this.merchantName = merchantName;
    }
    public void setAddress1(String address1)
    {
      this.address1 = address1;
    }
    public void setAddress2(String address2)
    {
      this.address2 = address2;
    }
    public void setCity(String city)
    {
      this.city = city;
    }
    public void setState(String state)
    {
      this.state = state;
    }
    public void setZip(String zip)
    {
      this.zip = zip;
    }
    public void setDescriptor(String descriptor)
    {
      this.descriptor = descriptor;
    }
  }
}/*@lineinfo:generated-code*/