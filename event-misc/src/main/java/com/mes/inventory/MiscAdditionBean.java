/*@lineinfo:filename=MiscAdditionBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/MiscAdditionBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:36p $
  Version            : $Revision: 13 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class MiscAdditionBean extends com.mes.ops.InventoryBase
{
  private int           numRecs                             = 0;
  private StringBuffer  QueryString                         = new StringBuffer("");
  public  Vector        models                              = new Vector();
  public  Vector        descriptions                        = new Vector();                           
  
  private boolean  firstpin                            = true;
  private boolean  firstterm                           = true;
  private boolean  firstperiph                         = true;
  private boolean  firstprinter                        = true;
  private boolean  firstimprinter                      = true;
  private boolean  firsttermprinter                    = true;
  private boolean  firsttermprintpin                   = true;
  private boolean  firstaccessory                      = true;

  private Vector   equipRecs                           = new Vector();

  public  Vector   classes                             = new Vector();
  public  Vector   classesCode                         = new Vector();

  public  Vector   source                              = new Vector();
  public  Vector   sourceCode                          = new Vector();

  public  Vector   status                              = new Vector();
  public  Vector   statusCode                          = new Vector();

  public  Vector   owner                               = new Vector();
  public  Vector   ownerCode                           = new Vector();

  private HashMap  descMap                             = new HashMap();
  private HashMap  validateMap                         = new HashMap();

  public MiscAdditionBean()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      // make sure we have a valid connection.  JSP should clean it up
      if(isConnectionStale())
      {
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                  equip_descriptor,
//                  equiptype_code
//          from    equipment
//          where   used_in_inventory = 'Y'
//          order by equiptype_code, equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                equip_descriptor,\n                equiptype_code\n        from    equipment\n        where   used_in_inventory = 'Y'\n        order by equiptype_code, equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.MiscAdditionBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.MiscAdditionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/  
      rs = it.getResultSet();
      
      while(rs.next())
      {
        descMap.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
        
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            if(firstterm)
            {
              descriptions.add("****TERMINALS****");
              models.add("*");
              firstterm = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            if(firstprinter)
            {
              descriptions.add("****PRINTERS****");
              models.add("*");
              firstprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            if(firstpin)
            {
              descriptions.add("****PINPADS****");
              models.add("*");
              firstpin = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            if(firstimprinter)
            {
              descriptions.add("****IMPRINTERS****");
              models.add("*");
              firstimprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            if(firsttermprinter)
            {
              descriptions.add("****TERMINAL/PRINTERS****");
              models.add("*");
              firsttermprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            if(firsttermprintpin)
            {
              descriptions.add("****TERMINAL/PRINTER/PINPADS****");
              models.add("*");
              firsttermprintpin = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            if(firstperiph)
            {
              descriptions.add("****PERIPHERALS****");
              models.add("*");
              firstperiph = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;

          case mesConstants.APP_EQUIP_TYPE_ACCESSORIES:
            if(firstaccessory)
            {
              descriptions.add("****ACCESSORIES****");
              models.add("*");
              firstaccessory = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
        }  
        
      }
      rs.close();
      it.close();
    
      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ec_class_name,
//                  ec_class_id
//          from    equip_class
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ec_class_name,\n                ec_class_id\n        from    equip_class";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.MiscAdditionBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.MiscAdditionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        classes.add(rs.getString("ec_class_name"));
        classesCode.add(rs.getString("ec_class_id"));  
      }
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_status_desc,
//                  equip_status_id
//          from    equip_status 
//          where   not equip_status_id in (1,2,5,7,20,26,27,28)
//          order by equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_status_desc,\n                equip_status_id\n        from    equip_status \n        where   not equip_status_id in (1,2,5,7,20,26,27,28)\n        order by equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.MiscAdditionBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.MiscAdditionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^7*/        
      rs = it.getResultSet();
      
      while(rs.next())
      {
        status.add(rs.getString("equip_status_desc"));
        statusCode.add(rs.getString("equip_status_id"));  
      }
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:226^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                  inventory_owner_name 
//          from    app_type 
//          where   SEPARATE_INVENTORY = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code,\n                inventory_owner_name \n        from    app_type \n        where   SEPARATE_INVENTORY = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.MiscAdditionBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.MiscAdditionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        owner.add(rs.getString("inventory_owner_name"));  
        ownerCode.add(rs.getString("app_type_code"));
      }
      rs.close();
      it.close();

      source.add("Bought From Merchant");
      sourceCode.add("1");
      source.add("Bought From supplier");
      sourceCode.add("2");
      source.add("Found");
      sourceCode.add("3");
      source.add("Other");
      sourceCode.add("4");
    }
    catch(Exception e)
    {
      logEntry("constructor--filling grids()", e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
  }
  
  public void updateData(HttpServletRequest aReq)
  {
    int     i           = 0;
    boolean addToVector = false;
    boolean cloneEquip  = false;

    while(i < this.numRecs)
    {
      EquipRec tempRec = new EquipRec();

      if(isBlank(aReq.getParameter("delete" + i)))
      {    
        addToVector = true;
      }
      else
      {
        addToVector = false;
      }

      if(isBlank(aReq.getParameter("clone" + i)))
      {    
        cloneEquip = false;
      }
      else
      {
        cloneEquip = true;
      }
      
      if(!isBlank(aReq.getParameter("partNum" + i)) && !aReq.getParameter("partNum" + i).equals("*"))
      {    
        tempRec.setPartNum(aReq.getParameter("partNum" + i));
      }
      
      if(!isBlank(aReq.getParameter("serialNum" + i)))
      {    
        tempRec.setSerialNum(aReq.getParameter("serialNum" + i));
      }

      if(!isBlank(aReq.getParameter("source" + i)))
      {    
        tempRec.setSource(aReq.getParameter("source" + i));
      }

      if(!isBlank(aReq.getParameter("status" + i)))
      {    
        tempRec.setStatus(aReq.getParameter("status" + i));
      }

      if(!isBlank(aReq.getParameter("note" + i)))
      {    
        tempRec.setNote(aReq.getParameter("note" + i));
      }

      
      if(!isBlank(aReq.getParameter("classType" + i)))
      {    
        tempRec.setClassType(aReq.getParameter("classType" + i));
      }
            
      if(!isBlank(aReq.getParameter("unitPrice" + i)))
      {    
        tempRec.setUnitPrice(aReq.getParameter("unitPrice" + i));
      }

      if(!isBlank(aReq.getParameter("shipPrice" + i)))
      {    
        tempRec.setShipPrice(aReq.getParameter("shipPrice" + i));
      }

      if(!isBlank(aReq.getParameter("owner" + i)))
      {    
        tempRec.setOwner(aReq.getParameter("owner" + i));
      }
      
      if(addToVector)
      {
        equipRecs.add(tempRec);
      }
      if(cloneEquip)
      {
        equipRecs.add(new EquipRec(tempRec.getPartNum(),tempRec.getClassType(),tempRec.getUnitPrice(),tempRec.getSource(),tempRec.getNote(),tempRec.getShipPrice(),tempRec.getOwner(),tempRec.getStatus()));
      }

      i++; //increment loop counter
    }

    if(!isBlank(aReq.getParameter("addNew")))
    {
      EquipRec tempRec = new EquipRec();
      equipRecs.add(tempRec);
    }

  }

  private String getProductDesc(String partNum)
  {
    String result = "";
    if(isBlank(partNum))
    {
      return "UNKNOWN";
    }
    try
    {
      result = (String)descMap.get(partNum);
    }
    catch(Exception e)
    {
    }
    
    if(isBlank(result))
    {
      return partNum;
    }
    return result;
  }

  public boolean validate()
  {
    int     i           = 0;

 //   if(this.poNotes.length() > 250)
   // {
     // addError("Notes must be limited to 250 characters.  Currently there are " + this.poNotes.length() + " characters");
   // }

    while(i < this.numRecs)
    {
      EquipRec tempRec =  (EquipRec)equipRecs.elementAt(i);


      if(isBlank(tempRec.getPartNum()))
      {    
        addError("Please select a product to add to inventory");
      }
      else
      {
        if(isBlank(tempRec.getSerialNum()))
        {    
          addError("Please provide the serial number of each " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }
        else if(doesExist(tempRec.getPartNum(), tempRec.getSerialNum()))
        {
          addError("Serial number " + tempRec.getSerialNum() + " for " +  getProductDesc(tempRec.getPartNum()) + " already exists in database");
        }
        else
        {
          String tempClass = (String)validateMap.get(tempRec.getSerialNum());
          if(isBlank(tempClass))
          {
            validateMap.put(tempRec.getSerialNum(),"added");
          }
          else
          {
            addError("Each item must have a unique serial number");
          }
        }
      
        if(tempRec.getClassType() == -1)
        {    
          addError("Please select the condition of the " + getProductDesc(tempRec.getPartNum()) + " you wish to add to the inventory");
        }
        if(tempRec.getSource() == -1)
        {    
          addError("Please select the source of the " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }
        if(tempRec.getStatus() == -1)
        {    
          addError("Please select the status of the " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }
        if(tempRec.getOwner() == -1)
        {    
          addError("Please select the owner of the " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }

        if(isBlank(tempRec.getUnitPrice()))
        {    
          addError("Please provide the unit price of each " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }
        else
        {
          if(validCurr(tempRec.getUnitPrice()))
          {
            tempRec.setUnitPrice(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getUnitPrice()))));
            try
            {
              double tempDoub = Double.parseDouble(tempRec.getUnitPrice());
              if(tempDoub > 9999.99)
              {
                addError("Unit cost for each " + getProductDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
              }
            }
            catch(Exception e)
            {}
          }
          else
          {
            addError("Please provide a valid unit cost for each " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
          }
        }

        if(isBlank(tempRec.getShipPrice()))
        {    
          addError("Please provide the shipping cost of each " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
        }
        else
        {
          if(validCurr(tempRec.getShipPrice()))
          {
            tempRec.setShipPrice(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getShipPrice()))));
            try
            {
              double tempDoub = Double.parseDouble(tempRec.getShipPrice());
              if(tempDoub > 9999.99)
              {
                addError("Shipping cost for each " + getProductDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
              }
            }
            catch(Exception e)
            {}
          }
          else
          {
            addError("Please provide a valid shipping cost for each " + getProductDesc(tempRec.getPartNum()) + " you wish to add to inventory");
          }
        }

      }
      i++;
    }
    
    return(!hasErrors());
  }
  public void submitData(long userLogin)
  {
    submitEquipment(userLogin);
  }
  
  private void submitEquipment(long userLogin)
  {
    EquipRec          tempRec = null;

    try
    {
      for(int x=0; x<this.numRecs; x++)
      {
        tempRec = (EquipRec)equipRecs.elementAt(x);
        
        
        /*@lineinfo:generated-code*//*@lineinfo:510^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_inventory 
//            (
//              ei_part_number,  
//              ei_serial_number, 
//              ei_unit_cost, 
//              ei_class, 
//              ei_original_class, 
//              ei_status, 
//              ei_lrb, 
//              ei_received_date, 
//              ei_transaction_id,
//              ei_shipping_cost,
//              ei_owner 
//            ) 
//            values
//            (
//              :tempRec.getPartNum(),
//              :tempRec.getSerialNum(),
//              :tempRec.getUnitPrice(),
//              :tempRec.getClassType(),
//              :tempRec.getClassType(), -- original class same as class in this situation
//              :tempRec.getStatus(),
//              :com.mes.constants.mesConstants.APP_EQUIP_IN_STOCK,
//              sysdate,
//              0,
//              :tempRec.getShipPrice(),
//              :tempRec.getOwner()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1793 = tempRec.getPartNum();
 String __sJT_1794 = tempRec.getSerialNum();
 String __sJT_1795 = tempRec.getUnitPrice();
 int __sJT_1796 = tempRec.getClassType();
 int __sJT_1797 = tempRec.getClassType();
 int __sJT_1798 = tempRec.getStatus();
 String __sJT_1799 = tempRec.getShipPrice();
 int __sJT_1800 = tempRec.getOwner();
   String theSqlTS = "insert into equip_inventory \n          (\n            ei_part_number,  \n            ei_serial_number, \n            ei_unit_cost, \n            ei_class, \n            ei_original_class, \n            ei_status, \n            ei_lrb, \n            ei_received_date, \n            ei_transaction_id,\n            ei_shipping_cost,\n            ei_owner \n          ) \n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 , -- original class same as class in this situation\n             :6 ,\n             :7 ,\n            sysdate,\n            0,\n             :8 ,\n             :9 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.inventory.MiscAdditionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1793);
   __sJT_st.setString(2,__sJT_1794);
   __sJT_st.setString(3,__sJT_1795);
   __sJT_st.setInt(4,__sJT_1796);
   __sJT_st.setInt(5,__sJT_1797);
   __sJT_st.setInt(6,__sJT_1798);
   __sJT_st.setInt(7,com.mes.constants.mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setString(8,__sJT_1799);
   __sJT_st.setInt(9,__sJT_1800);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^9*/
        
        // log this inventory movement
        addToHistory( tempRec.getPartNum(), tempRec.getSerialNum(), userLogin, 
                      tempRec.getStatus(),
                      "Item Added In Miscellaneous Addition Screen" );
                      
        // add this serial number to the list of serial numbers added                      
        QueryString.append( tempRec.getSerialNum() );
        QueryString.append( ":" );
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:552^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:555^7*/
    }  
    catch(Exception e)
    {
      logEntry("submitEquipment()", e.toString());
      addError("submitEquipment: " + e.toString());
    }
  }
  
  private String getDigitsAndDecimals(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("getDigits()", e.toString());
    }
    
    return digits.toString();

  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);   
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }
      
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {         
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }
      
      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  public String getQueryString()
  {
    return( QueryString.toString() );
  }
  public int getListSize()
  {
    return equipRecs.size();
  }
  
  public void setNumRecs(String numRecs)
  {
    try
    {
      this.numRecs = Integer.parseInt(numRecs);
    }
    catch(Exception e)
    {
      this.numRecs = 0;
    }
  }

  public int getClassType(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getClassType());
  }
  public int getSource(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getSource());
  }
  public int getStatus(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getStatus());
  }
  public int getOwner(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getOwner());
  }

  public String getUnitPrice(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getUnitPrice());
  }

  public String getShipPrice(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getShipPrice());
  }

  public String getPartNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getPartNum());
  }
  public String getSerialNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getSerialNum());
  }
  public String getNote(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getNote());
  }



  public class EquipRec
  {
    private int    classType      = -1;
    private int    source         = -1;
    private int    status         = -1;
    private int    owner          = -1;

    private String unitPrice      = "";
    private String shipPrice      = "";
    private String partNum        = "";
    private String serialNum      = "";
    private String note           = "";

    EquipRec()
    {}

    EquipRec(String partNum, int classType, String unitPrice, int source, String note, String shipPrice, int owner, int status)
    {
      this.partNum    = partNum;
      this.classType  = classType;
      this.unitPrice  = unitPrice;
      this.shipPrice  = shipPrice;
      this.source     = source;
      this.owner      = owner;
      this.status     = status;
      this.note       = note;
    }

    public void setClassType(String classType)
    {
      try
      {
        if(classType != null && !classType.equals(""))
        {
          this.classType = Integer.parseInt(classType);
        }
      }
      catch(Exception e)
      {}
    }
    public void setSource(String source)
    {
      try
      {
        if(source != null && !source.equals(""))
        {
          this.source = Integer.parseInt(source);
        }
      }
      catch(Exception e)
      {}
    }
    public void setStatus(String status)
    {
      try
      {
        if(status != null && !status.equals(""))
        {
          this.status = Integer.parseInt(status);
        }
      }
      catch(Exception e)
      {}
    }
    public void setOwner(String owner)
    {
      try
      {
        if(owner != null && !owner.equals(""))
        {
          this.owner = Integer.parseInt(owner);
        }
      }
      catch(Exception e)
      {}
    }


    public void setUnitPrice(String unitPrice)
    {
      this.unitPrice = unitPrice;
    }
    public void setShipPrice(String shipPrice)
    {
      this.shipPrice = shipPrice;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setSerialNum(String serialNum)
    {
      this.serialNum = serialNum;
    }
    public void setNote(String note)
    {
      this.note = note;
    }

    public int getClassType()
    {
      return this.classType;
    }
    public int getSource()
    {
      return this.source;
    }
    public int getStatus()
    {
      return this.status;
    }
    public int getOwner()
    {
      return this.owner;
    }

    public String getUnitPrice()
    {
      return this.unitPrice;
    }
    public String getShipPrice()
    {
      return this.shipPrice;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getSerialNum()
    {
      return this.serialNum;
    }
    public String getNote()
    {
      return this.note;
    }
  }

}/*@lineinfo:generated-code*/