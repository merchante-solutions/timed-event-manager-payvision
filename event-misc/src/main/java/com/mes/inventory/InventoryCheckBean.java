/*@lineinfo:filename=InventoryCheckBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/InventoryCheckBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/14/02 2:32p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class InventoryCheckBean extends SQLJConnectionBase
{
  private   Vector                scannedEquip        = null;
  private   Hashtable             dbInventory         = null;
  private   Vector                dbInventoryList     = null;

  private   int                   clientInventory     = -1;
  private   String                clientInventoryDesc = "";
  private   boolean               inventoryLoaded     = false;
  private   ScannedDataComparator sdc                 = new ScannedDataComparator();

  //result vectors
  public    Vector                scannedNotInDb        = null;
  public    Vector                scannedInDbNotInstock = null;
  public    Vector                notScannedInDbInstock = null;

  public InventoryCheckBean()
  {
  }

  public void resetInventory()
  {
    scannedEquip          = null;
    dbInventory           = null;
    clientInventory       = -1;
    clientInventoryDesc   = "";
    inventoryLoaded       = false;
  }
  
  public Iterator getScannedEquip()
  {
    if(this.scannedEquip == null)
    {
      return null;
    }

    TreeSet sortedResults = new TreeSet(sdc);
    sortedResults.addAll(this.scannedEquip);
    
    return sortedResults.iterator();
  }

  public boolean loadClientInventory(int client)
  {
    //reset everything and clear it all out...
    resetInventory(); 
  
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:102^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ei.ei_part_number,
//                 ei.ei_serial_number,
//                 ei.ei_unit_cost,
//                 ei.ei_lrb,
//                 ei.ei_received_date,
//                 ei.ei_deployed_date,
//                 ei.ei_transaction_id,
//                 ei.ei_merchant_number,                
//                 ei.ei_owner,
//                 status.equip_status_desc,                           
//                 equip.equip_descriptor,                             
//                 lt.equiplendtype_description,                       
//                 at.inventory_owner_name                             
//          from                                                
//                 equipment        equip,                             
//                 equiplendtype    lt,                                
//                 app_type         at,                                
//                 equip_inventory  ei,                                
//                 equip_status     status                             
//          where                                          
//                 ei.ei_owner              = :client                 and
//                 ei.ei_owner              = at.app_type_code        and
//                 ei.ei_part_number        = equip.equip_model       and
//                 ei.ei_lrb                = lt.equiplendtype_code   and
//                 ei.ei_status             = status.equip_status_id   
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ei.ei_part_number,\n               ei.ei_serial_number,\n               ei.ei_unit_cost,\n               ei.ei_lrb,\n               ei.ei_received_date,\n               ei.ei_deployed_date,\n               ei.ei_transaction_id,\n               ei.ei_merchant_number,                \n               ei.ei_owner,\n               status.equip_status_desc,                           \n               equip.equip_descriptor,                             \n               lt.equiplendtype_description,                       \n               at.inventory_owner_name                             \n        from                                                \n               equipment        equip,                             \n               equiplendtype    lt,                                \n               app_type         at,                                \n               equip_inventory  ei,                                \n               equip_status     status                             \n        where                                          \n               ei.ei_owner              =  :1                  and\n               ei.ei_owner              = at.app_type_code        and\n               ei.ei_part_number        = equip.equip_model       and\n               ei.ei_lrb                = lt.equiplendtype_code   and\n               ei.ei_status             = status.equip_status_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.InventoryCheckBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,client);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.InventoryCheckBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        EquipRec equipRec =  new EquipRec();
        
        equipRec.setReceivedDate( DateTimeFormatter.getFormattedDate(rs.getTimestamp("ei_received_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
        equipRec.setDeployedDate( DateTimeFormatter.getFormattedDate(rs.getTimestamp("ei_deployed_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
        equipRec.setPartNum     ( rs.getString("ei_part_number")                          );
        equipRec.setSerialNum   ( rs.getString("ei_serial_number")                        );
        equipRec.setUnitCost    ( MesMath.toCurrency(rs.getString("ei_unit_cost"))        );
        equipRec.setTransId     ( rs.getString("ei_transaction_id")                       );
        equipRec.setMerchantNum ( rs.getString("ei_merchant_number")                      );
        equipRec.setStatus      ( rs.getString("equip_status_desc")                       );
        equipRec.setEquipDesc   ( rs.getString("equip_descriptor")                        );
        equipRec.setLendType    ( rs.getString("equiplendtype_description")               );
        equipRec.setClientDesc  ( rs.getString("inventory_owner_name")                    );
        equipRec.setClient      ( rs.getString("ei_owner")                                );
        equipRec.setInstock     ( (rs.getInt("ei_lrb") == mesConstants.APP_EQUIP_IN_STOCK));

        addEquipRec(rs.getString("ei_serial_number"), equipRec);
        addDbInventoryList(rs.getString("ei_serial_number"));

        inventoryLoaded = true;

        this.clientInventory       = rs.getInt("ei_owner");
        this.clientInventoryDesc   = rs.getString("inventory_owner_name");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadClientInventory()", e.toString());
      inventoryLoaded = false;
      resetInventory(); 
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return inventoryLoaded;
  }

  public void addScannedEquip(String fieldItemName)
  {
    if(fieldItemName==null || fieldItemName.equals(""))
      return;
    
    if(scannedEquip==null)
    {
      scannedEquip = new Vector();
    }
    
    if(!scannedEquip.contains(fieldItemName.trim()))
    {
      scannedEquip.add(fieldItemName.trim());
    }
  }

  public void addDbInventoryList(String fieldItemName)
  {
    if(fieldItemName==null || fieldItemName.equals(""))
      return;
    
    if(dbInventoryList==null)
    {
      dbInventoryList = new Vector();
    }
    
    if(!dbInventoryList.contains(fieldItemName.trim()))
    {
      dbInventoryList.add(fieldItemName.trim());
    }
  }

  private void addEquipRec(String fieldItemName, EquipRec defaultValue)
  {
    if(fieldItemName==null || defaultValue==null)
      return;
    
    if(dbInventory==null)
    {
      dbInventory = new Hashtable();
    }

    dbInventory.put(fieldItemName.trim(), defaultValue);
  }

  //if not found.. returns new EquipRec
  private EquipRec getEquipRec(String fieldItemName)
  {
    return getEquipRec(fieldItemName, true);
  }

  private EquipRec getEquipRec(String fieldItemName, boolean returnNew)
  {
    EquipRec result = null;

    if(dbInventory==null || fieldItemName==null)
    {
      if(returnNew)
      {
        return new EquipRec();
      }
      else
      {
        return null;
      }
    }

    if(dbInventory.containsKey(fieldItemName.trim()))
    {
      result = (EquipRec)dbInventory.get(fieldItemName.trim());
    }
    else if(returnNew)
    {
      result = new EquipRec();
    }

    return result;
  }

  public boolean isInventoryLoaded()
  {
    return this.inventoryLoaded;
  }

  public int getClientInventory()
  {
    return this.clientInventory;
  }
  public String getClientInventoryDesc()
  {
    return this.clientInventoryDesc;
  }

  public void calculateReport()
  {
    this.scannedNotInDb        = new Vector(); //stores serial number strings
    this.scannedInDbNotInstock = new Vector(); //stores whole record
    this.notScannedInDbInstock = new Vector(); //stores whole record

    for(int x=0; x<this.scannedEquip.size(); ++x)
    {
      EquipRec equipRec = getEquipRec((String)this.scannedEquip.elementAt(x), false);

      //scanned but not found in database
      if(equipRec == null)
      {
        this.scannedNotInDb.add((String)this.scannedEquip.elementAt(x));
      }
      else
      {
        //check to see if it is instock or not.. if not.. then put in scanned but db says not in stock.
        if(!equipRec.isInstock())
        {
          this.scannedInDbNotInstock.add(equipRec);
        }
      }
    }

    for(int y=0; y<this.dbInventoryList.size(); y++)
    {
      EquipRec equipRec = getEquipRec((String)this.dbInventoryList.elementAt(y));

      //check to see if in stock in db.. but not scanned
      if(equipRec.isInstock()  &&  !this.scannedEquip.contains(this.dbInventoryList.elementAt(y)))
      {
        this.notScannedInDbInstock.add(equipRec);
      }
    }
  }
  
/******************************************************************
Class that holds information for each piece of equipment in inventory
*******************************************************************/  

  public class EquipRec
  {
    private boolean instock       = false;
    private String  receivedDate  = "";
    private String  deployedDate  = "";
    private String  partNum       = "";
    private String  serialNum     = "";
    private String  unitCost      = "";
    private String  transId       = "";
    private String  merchantNum   = "";
    private String  status        = "";
    private String  equipDesc     = "";
    private String  lendType      = "";
    private String  client        = "";
    private String  clientDesc    = "";

    EquipRec()
    {}

    public void setReceivedDate(String  data)
    {
      this.receivedDate = processData(data);
    }
    public void setDeployedDate(String  data)
    {
      this.deployedDate = processData(data);
    }
    public void setPartNum(String  data)
    {
      this.partNum = processData(data);
    }
    public void setSerialNum(String  data)
    {
      this.serialNum = processData(data);
    }
    public void setUnitCost(String  data)
    {
      this.unitCost = processData(data);
    }
    public void setTransId(String  data)
    {
      this.transId = processData(data);
    }
    public void setMerchantNum(String  data)
    {
      this.merchantNum = processData(data);
    }
    public void setStatus(String  data)
    {
      this.status = processData(data);
    }
    public void setEquipDesc(String  data)
    {
      this.equipDesc = processData(data);
    }
    public void setLendType(String  data)
    {
      this.lendType = processData(data);
    }
    public void setClient(String  data)
    {
      this.client = processData(data);
    }

    public void setClientDesc(String  data)
    {
      this.clientDesc = processData(data);
    }

    public void setInstock(boolean data)
    {
      this.instock = data;
    }


    private String processData(String data)
    {
      if(data == null)
        return "";
      else
        return data.trim();
    }

    public String getReceivedDate()
    {
      return this.receivedDate;
    }
    public String getDeployedDate()
    {
      return this.deployedDate;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getSerialNum()
    {
      return this.serialNum;
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }
    public String getTransId()
    {
      return this.transId;
    }
    public String getMerchantNum()
    {
      return this.merchantNum;
    }
    public String getStatus()
    {
      return this.status;
    }
    public String getEquipDesc()
    {
      return this.equipDesc;
    }
    public String getLendType()
    {
      return this.lendType;
    }
    public String getClient()
    {
      return this.client;
    }
    public String getClientDesc()
    {
      return this.clientDesc;
    }
    public boolean isInstock()
    {
      return this.instock;
    }
  }

  public class ScannedDataComparator implements Comparator
  {
    int compare(String o1, String o2)
    {
      int result    = 0;
      
      String compareString1 = o1;
      String compareString2 = o2;
      
      try
      {
        result = compareString1.compareTo(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(String o1, String o2)
    {
      boolean result    = false;
      
      String compareString1 = o1;
      String compareString2 = o2;
      
      try
      {
        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((String)o1, (String)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((String)o1, (String)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
}/*@lineinfo:generated-code*/