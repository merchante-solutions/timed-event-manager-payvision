/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PinpadExchangeSearchBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 2/25/02 4:59p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;

public class PinpadExchangeSearchBean extends com.mes.screens.SequenceDataBean
{

  public static final int EXCHANGE_REFERENCE_NUM      = 1;
  public static final int EXCHANGE_MERCHANT_NUM       = 2;
  public static final int EXCHANGE_SERIAL_NUM         = 3;
  public static final int EXCHANGE_CALLTAG_NUM        = 4;
  public static final int SEARCH_NOFILTER             = -1;

  public static final int SPECIFIC_DETAIL_EQUAL       = 1;
  public static final int SPECIFIC_DETAIL_GREAT       = 2;

  public static final int EXCHANGE_STATUS_ALL         = -1;
  public static final int EXCHANGE_STATUS_INCOMPLETE  = 0;
  public static final int EXCHANGE_STATUS_COMPLETE    = 1;

  // private data members
  private String        exchangeNumber      = "-1";
  private String        merchantNumber      = "-1";
  private String        serialNum           = "";
  private String        callTagNum          = "";
  private int           exchangeStatus      = EXCHANGE_STATUS_ALL;

  private int           specificDateDetail  = SPECIFIC_DETAIL_EQUAL;
  
  private Date          specificDate        = null;
  private Date          specificDateNext    = null;
  private String        specificDateStr     = "-1";
  
  private int           searchCriteria      = -1;
  private StringBuffer  qs                  = new StringBuffer("");
  private boolean       submit              = false;
  private boolean       hasErrors           = false;
  private Vector        errors              = new Vector();
  private ResultSet     rs                  = null;

  /*
  ** CONSTRUCTOR
  */
  public PinpadExchangeSearchBean()
  {
  }

  public ResultSet getResultSet()
  {
    PreparedStatement ps      = null;
    
    try
    {
      qs.setLength(0);
      
      qs.append("select DISTINCT * from equip_exchange ");


      switch(this.searchCriteria)
      {
        case EXCHANGE_REFERENCE_NUM:
          qs.append("where exchange_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.exchangeNumber);
          break;
        case EXCHANGE_MERCHANT_NUM:
          qs.append("where merchant_num = ? ");

          switch(this.exchangeStatus)
          {
            case EXCHANGE_STATUS_INCOMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_INCOMPLETE) + " ");
            break;
            case EXCHANGE_STATUS_COMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_COMPLETE) + " ");
            break;
          }

          qs.append("order by exchange_ref_num ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.merchantNumber);
          break;
/*
        case SEARCH_ORDER_DATE:
          if(this.orderSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_order_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_order_date between ? and ? ");
          }

          switch(this.exchangeStatus)
          {
            case EXCHANGE_STATUS_INCOMPLETE:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case EXCHANGE_STATUS_COMPLETE:
              qs.append("and ep.ep_transaction_date is null ");
            break;
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.orderDate.getTime()));
          
          if(this.orderSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.orderDateNext.getTime()));
          }
          break;
*/
        case EXCHANGE_SERIAL_NUM:
          qs.append("where (serial_num_in = ? or serial_num_out = ?) ");

          switch(this.exchangeStatus)
          {
            case EXCHANGE_STATUS_INCOMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_INCOMPLETE) + " ");
            break;
            case EXCHANGE_STATUS_COMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_COMPLETE) + " ");
            break;
          }

          qs.append("order by exchange_ref_num ");

          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.serialNum);
          ps.setString(2,this.serialNum);
          break;  
        case EXCHANGE_CALLTAG_NUM:
          qs.append("where call_tag_num = ? ");

          switch(this.exchangeStatus)
          {
            case EXCHANGE_STATUS_INCOMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_INCOMPLETE) + " ");
            break;
            case EXCHANGE_STATUS_COMPLETE:
              qs.append("and exchange_status = " + Integer.toString(EXCHANGE_STATUS_COMPLETE) + " ");
            break;
          }

          qs.append("order by exchange_ref_num ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.callTagNum);
          break;
        default:

          switch(this.exchangeStatus)
          {
            case EXCHANGE_STATUS_INCOMPLETE:
              qs.append("where exchange_status = " + Integer.toString(EXCHANGE_STATUS_INCOMPLETE) + " ");
            break;
            case EXCHANGE_STATUS_COMPLETE:
              qs.append("where exchange_status = " + Integer.toString(EXCHANGE_STATUS_COMPLETE) + " ");
            break;
          }

          qs.append("order by exchange_ref_num desc ");
          ps = getPreparedStatement(qs.toString());
          break;
      }
      
      this.rs = ps.executeQuery();
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return this.rs;
  }  

  public boolean validateData()
  {
    boolean result = false;
    String temp = "";
    switch(this.searchCriteria)
    {
      case EXCHANGE_REFERENCE_NUM:
        if(this.exchangeNumber.equals("-1"))
        {
          temp = "Please enter a valid exchange reference number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case EXCHANGE_MERCHANT_NUM:
        if(this.merchantNumber.equals("-1"))
        {
          temp = "Please enter a valid merchant number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
/*
      case SEARCH_ORDER_DATE:
        if(this.orderDateStr.equals("-1") || !validDate(this.orderDateStr))
        {
          temp = "Please enter a valid order date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.orderDateNext = getDateNext(this.orderDateStr);
        }
      break;
*/
      case EXCHANGE_SERIAL_NUM:
        if(isBlank(this.serialNum))
        {
          temp = "Please provide a Serial Number to see which exchanges it was involved in";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case EXCHANGE_CALLTAG_NUM:
        if(isBlank(this.callTagNum))
        {
          temp = "Please provide a Call Tag Number to see which exchange it was involved in";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      default:
        this.hasErrors = false;
        break;
    }
    
    if(!this.hasErrors)
    {
      result = true;
    }
    
    return result;
  }
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private Date getDateNext(String specificDate)
  {
    int index1  = specificDate.indexOf('/');
    int index2  = specificDate.lastIndexOf('/');
    String mon  = specificDate.substring(0,index1);
    String day  = specificDate.substring(index1+1,index2);
    String year = specificDate.substring(index2+1);
    Date result = null;
        
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day             = String.valueOf(nextDay);
      String date     = mon + "/" + day + "/" + year;
      DateFormat fmt  = DateFormat.getDateInstance(DateFormat.SHORT);
      result          = fmt.parse(date);
    }
    catch(Exception e)
    {
      result = null;
    }
    return result;
  }

  public void setSpecificDate(String specificDate)
  {
    this.specificDateStr = specificDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(specificDate.trim());
      try
      {
        specificDate          = specificDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
        this.specificDate     = aDate;
      }
      catch(Exception e)
      {
        this.specificDate    = null;
        this.specificDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.specificDate      = null;
      this.specificDateStr   = "-1";
    }
  }
 
  public String getSpecificDate()
  {
    String result = "";
    if(!this.specificDateStr.equals("-1"))
    {
      result = this.specificDateStr;
    }
    return result;
  }
 
  public void setSpecificDateDetail(String specificDateDetail)
  {
    try
    {
      this.specificDateDetail = Integer.parseInt(specificDateDetail); 
    }
    catch(Exception e)
    {
      this.specificDateDetail = -1;
    }
  }
  public int getSpecificDateDetail()
  {
    int result = 0;
    if(this.specificDateDetail != -1)
    {
      result = this.specificDateDetail;
    }
    return result;
  }


  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setExchangeStatus(String exchangeStatus)
  {
    try
    {
      this.exchangeStatus = Integer.parseInt(exchangeStatus); 
    }
    catch(Exception e)
    {
      this.exchangeStatus = -1;
    }
  }
  public int getExchangeStatus()
  {
    return this.exchangeStatus;
  }


  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      long temp = Long.parseLong(merchantNumber);
      this.merchantNumber = merchantNumber;
    }
    catch(Exception e)
    {
      this.merchantNumber = "-1";
    }
  }
  public String getMerchantNumber()
  {
    String result = "";
    if(!this.merchantNumber.equals("-1"))
    {
      result = this.merchantNumber;
    }
    return result;
  }

  public void setExchangeNum(String exchangeNumber)
  {
    try
    {
      long temp = Long.parseLong(exchangeNumber);
      this.exchangeNumber = exchangeNumber;
    }
    catch(Exception e)
    {
      this.exchangeNumber = "-1";
    }
  }
  public String getExchangeNum()
  {
    String result = "";
    if(!this.exchangeNumber.equals("-1"))
    {
      result = this.exchangeNumber;
    }
    return result;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum.trim();
  }
  public String getSerialNum()
  {
    return this.serialNum;
  }

  public void setCallTagNum(String callTagNum)
  {
    this.callTagNum = callTagNum.trim();
  }
  public String getCallTagNum()
  {
    return this.callTagNum;
  }

  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return this.hasErrors;
  }

  public Vector getErrors()
  {
    return errors;
  }
}
