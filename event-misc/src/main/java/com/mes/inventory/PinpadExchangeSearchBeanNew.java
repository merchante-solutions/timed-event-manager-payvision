/*@lineinfo:filename=PinpadExchangeSearchBeanNew*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PinpadExchangeSearchBeanNew.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:39p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class PinpadExchangeSearchBeanNew extends DateSQLJBean
{
  public class ExchangeData
  {
    public String exchangeRefNum;
    public String merchantNum;
    public String merchantDba;
    public String exchangeDate;
    public String exchangeDateSorted;
    public String callTagNum;
    public String exchangeStatus;

    public ExchangeData(ResultSet rs)
    {
      try
      {
        exchangeRefNum      = processStringField(rs.getString("exchange_ref_num"));
        merchantNum         = processStringField(rs.getString("merchant_num"));
        merchantDba         = processStringField(rs.getString("merchant_dba"));
        exchangeDate        = DateTimeFormatter.getFormattedDate(rs.getTimestamp("exchange_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        exchangeDateSorted  = DateTimeFormatter.getFormattedDate(rs.getTimestamp("exchange_date"), "yyyyMMddHHmmss");
        callTagNum          = processStringField(rs.getString("call_tag_num"));
        exchangeStatus      = processStringField(rs.getString("exchange_status"));
      }
      catch(Exception e)
      {
        System.out.println("exchangeDate Constructor: " + e.toString());
      }
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
  }

  public class ExchangeDataComparator
    implements Comparator
  {
    public final static int   SB_EXCHANGE_DATE        = 0;
    public final static int   SB_EXCHANGE_REF_NUM     = 1;
    public final static int   SB_MERCHANT_NUMBER      = 2;
    public final static int   SB_MERCHANT_DBA         = 3;
    public final static int   SB_CALL_TAG             = 4;
    public final static int   SB_EXCHANGE_STATUS      = 5;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public ExchangeDataComparator()
    {
      this.sortBy = SB_EXCHANGE_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_EXCHANGE_DATE && sortBy <= SB_EXCHANGE_STATUS)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(ExchangeData o1, ExchangeData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_EXCHANGE_DATE:
            compareString1 = o1.exchangeDateSorted + o1.exchangeRefNum + o1.exchangeRefNum;
            compareString2 = o2.exchangeDateSorted + o2.exchangeRefNum + o2.exchangeRefNum;
            break;
          
          case SB_EXCHANGE_REF_NUM:
            compareString1 = o1.exchangeRefNum + o1.exchangeDateSorted;
            compareString2 = o2.exchangeRefNum + o2.exchangeDateSorted;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.merchantNum + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.merchantNum + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
          
          case SB_MERCHANT_DBA:
            compareString1 = o1.merchantDba + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.merchantDba + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.callTagNum + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
            
          case SB_EXCHANGE_STATUS:
            compareString1 = o1.exchangeStatus + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.exchangeStatus + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(ExchangeData o1, ExchangeData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_EXCHANGE_DATE:
            compareString1 = o1.exchangeDateSorted + o1.exchangeRefNum + o1.exchangeRefNum;
            compareString2 = o2.exchangeDateSorted + o2.exchangeRefNum + o2.exchangeRefNum;
            break;
          
          case SB_EXCHANGE_REF_NUM:
            compareString1 = o1.exchangeRefNum + o1.exchangeDateSorted;
            compareString2 = o2.exchangeRefNum + o2.exchangeDateSorted;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.merchantNum + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.merchantNum + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
          
          case SB_MERCHANT_DBA:
            compareString1 = o1.merchantDba + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.merchantDba + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.callTagNum + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
            
          case SB_EXCHANGE_STATUS:
            compareString1 = o1.exchangeStatus + o1.exchangeDateSorted + o1.exchangeRefNum;
            compareString2 = o2.exchangeStatus + o2.exchangeDateSorted + o2.exchangeRefNum;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((ExchangeData)o1, (ExchangeData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((ExchangeData)o1, (ExchangeData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  public static final int EXCHANGE_STATUS_ALL         = -1;
  public static final int EXCHANGE_STATUS_INCOMPLETE  = 0;
  public static final int EXCHANGE_STATUS_COMPLETE    = 1;

  private String              lookupValue           = "";
  private String              lastLookupValue       = "";
  
  private int                 action                = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription     = "Invalid Action";
  
  private ExchangeDataComparator  edc               = new ExchangeDataComparator();
  private Vector              lookupResults         = new Vector();
  private TreeSet             sortedResults         = null;
  
  private boolean             submitted             = false;
  private boolean             refresh               = false;
  private boolean             edit                  = false;
  
  //search criteria
  private String              startNew              = "";
  private String              lastStartNew          = "";

  private int                 exchangeStatus            = -1;
  private int                 lastExchangeStatus        = -1;

  public PinpadExchangeSearchBeanNew()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        startNew        != lastStartNew       ||
        exchangeStatus  != lastExchangeStatus ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        /*@lineinfo:generated-code*//*@lineinfo:399^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ee.exchange_ref_num                                     exchange_ref_num,
//                    ee.merchant_num                                         merchant_num,
//                    mt.merchant_name                                        merchant_dba,
//                    decode(ee.exchange_status,1,'Complete','Incomplete')    exchange_status,
//                    ee.call_tag_num                                         call_tag_num,
//                    emt.action_date                                         exchange_date
//  
//            from    equip_exchange                  ee,
//                    equip_merchant_tracking         emt,
//                    merchant_types                  mt
//                    
//            where   ee.exchange_ref_num = emt.ref_num_serial_num  and 
//                    emt.action          = 6                       and
//                    ee.merchant_num     = mt.merchant_number      and
//                    (-1   = :exchangeStatus  or ee.exchange_status  = :exchangeStatus)  and
//                    (
//                      'passall'                 = :stringLookup     or
//                      upper(mt.merchant_name)   like :stringLookup  or
//                      upper(ee.call_tag_num)    = :stringLookup2    or
//                      ee.merchant_num           = :longLookup       or
//                      ee.exchange_ref_num       = :longLookup
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ee.exchange_ref_num                                     exchange_ref_num,\n                  ee.merchant_num                                         merchant_num,\n                  mt.merchant_name                                        merchant_dba,\n                  decode(ee.exchange_status,1,'Complete','Incomplete')    exchange_status,\n                  ee.call_tag_num                                         call_tag_num,\n                  emt.action_date                                         exchange_date\n\n          from    equip_exchange                  ee,\n                  equip_merchant_tracking         emt,\n                  merchant_types                  mt\n                  \n          where   ee.exchange_ref_num = emt.ref_num_serial_num  and \n                  emt.action          = 6                       and\n                  ee.merchant_num     = mt.merchant_number      and\n                  (-1   =  :1   or ee.exchange_status  =  :2 )  and\n                  (\n                    'passall'                 =  :3      or\n                    upper(mt.merchant_name)   like  :4   or\n                    upper(ee.call_tag_num)    =  :5     or\n                    ee.merchant_num           =  :6        or\n                    ee.exchange_ref_num       =  :7 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.PinpadExchangeSearchBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,exchangeStatus);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,stringLookup);
   __sJT_st.setString(4,stringLookup);
   __sJT_st.setString(5,stringLookup2);
   __sJT_st.setLong(6,longLookup);
   __sJT_st.setLong(7,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.PinpadExchangeSearchBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
        
        while(rs.next())
        {
          // add all results to the lookupResults
          lookupResults.add(new ExchangeData(rs));
        }
      
        rs.close();
        it.close();
        
        lastStartNew        = startNew; 
        lastExchangeStatus  = exchangeStatus;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Pinpad Exchange Search";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setExchangeStatus(String exchangeStatus)
  {
    try
    {
      this.exchangeStatus = Integer.parseInt(exchangeStatus);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getExchangeStatus()
  {
    return Integer.toString(this.exchangeStatus);
  }


  public synchronized String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
    }
    return body;
  }
 
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }

  public synchronized void setStartNew(String startNew)
  {
    this.startNew = startNew;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }

}/*@lineinfo:generated-code*/