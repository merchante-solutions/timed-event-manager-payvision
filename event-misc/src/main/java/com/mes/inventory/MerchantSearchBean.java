/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/MerchantSearchBean.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/19/03 5:19p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;
import com.mes.constants.mesConstants;

public class MerchantSearchBean extends com.mes.screens.SequenceDataBean
{
  public static final int MYAPPS           = 1;
  public static final int ALLUSERS         = 2;
  public static final int ALLTYPES         = -1;
  public static final int ALLSTATUS        = 4;

  public static final int SEARCH_CONTROL   = 1;
  public static final int SEARCH_DATE      = 2;
  public static final int SEARCH_DATERANGE = 3;
  public static final int SEARCH_MERCHANT  = 4;
  public static final int SEARCH_DBA       = 5;
  public static final int SEARCH_NOFILTER  = -1;


  // private data members
  private String        dba                 = "";
  private long          controlNumber       = -1;
  private long          merchantNumber      = -1;
  private Date          specificDate        = null;
  private Date          specificDateNext    = null;
  private String        specificDateStr     = "-1";
  private int           specificDateDetail  = 1;
  private Date          startDate           = null;
  private String        startDateStr        = "-1";
  private Date          endDate             = null;
  private Date          endDateNext         = null;
  private String        endDateStr          = "-1";
  
  private int           status              = com.mes.ops.QueueConstants.CREDIT_APPROVE;
  private int           owner               = MYAPPS;
  private int           appType             = -1;  //all apptypes default
  
  private int           searchCriteria      = -1;
  private StringBuffer  qs                  = new StringBuffer("");
  private boolean       submit              = false;
  private boolean       hasErrors           = false;
  private Vector        errors              = new Vector();
  private ResultSet     rs                  = null;
  
  public  Vector        appStatus           = new Vector();
  public  Vector        appUser             = new Vector();
  public  Vector        appTypes            = new Vector();
  
  public  Vector        appStatusDesc       = new Vector();
  public  Vector        appUserDesc         = new Vector();
  public  Vector        appTypeDesc         = new Vector();
    
  /*
  ** CONSTRUCTOR
  */
  public MerchantSearchBean()
  {
    appStatus.add(Integer.toString(com.mes.ops.QueueConstants.APP_NOT_FINISHED));
    appStatus.add(Integer.toString(com.mes.ops.QueueConstants.CREDIT_NEW));
    appStatus.add(Integer.toString(com.mes.ops.QueueConstants.CREDIT_APPROVE));
    appStatus.add(Integer.toString(com.mes.ops.QueueConstants.CREDIT_DECLINE));
    appStatus.add(Integer.toString(com.mes.ops.QueueConstants.CREDIT_PEND));
    appStatus.add(Integer.toString(ALLSTATUS));
    appStatusDesc.add("Not Finished");
    appStatusDesc.add("New Application");
    appStatusDesc.add("Approved");
    appStatusDesc.add("Declined");
    appStatusDesc.add("Pending");
    appStatusDesc.add("All Application");
  }

  public void setSuperDropDown()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try
    {
      ps = getPreparedStatement("select app_type, app_name from org_app order by app_type");
      rs = ps.executeQuery();

      while(rs.next())
      {
        appTypes.add(rs.getString("app_type"));
        appTypeDesc.add(rs.getString("app_name"));
      }
      appTypes.add(Integer.toString(ALLTYPES));
      appTypeDesc.add("All App Types");
    }
    catch(Exception e)
    {
    }
    finally
    {
      appUser.add(Integer.toString(MYAPPS));
      appUser.add(Integer.toString(ALLUSERS));
      appUserDesc.add("My Apps");
      appUserDesc.add("All User Apps");
    }
  }
  
  public void setManDropDown(String appType)
  {
    try
    {
      setManDropDown(Integer.parseInt(appType));
    }
    catch(Exception e)
    {
    }
  }
  public void setManDropDown(int appType)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try
    {
      ps = getPreparedStatement("select app_name from org_app where app_type = ?");
      ps.setInt(1, appType);

      rs = ps.executeQuery();

      if(rs.next())
      {
        appTypes.add(Integer.toString(appType));
        appTypeDesc.add(rs.getString("app_name"));
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      appUser.add(Integer.toString(MYAPPS));
      appUser.add(Integer.toString(ALLUSERS));
      appUserDesc.add("My Apps");
      appUserDesc.add("All User Apps");
    }

  }

  public void setSalesDropDown(String appType)
  {
    try
    {
      setSalesDropDown(Integer.parseInt(appType));
    }
    catch(Exception e)
    {
    }
  }
  public void setSalesDropDown(int appType)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try
    {
      ps = getPreparedStatement("select app_name from org_app where app_type = ?");
      ps.setInt(1, appType);

      rs = ps.executeQuery();

      if(rs.next())
      {
        appTypes.add(Integer.toString(appType));
        appTypeDesc.add(rs.getString("app_name"));
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      appUser.add(Integer.toString(MYAPPS));
      appUserDesc.add("My Apps");
    }
  }
  
  
  
  public ResultSet getResultSet(long userId)
  {
    PreparedStatement ps      = null;
    
    try
    {
      qs.setLength(0);
      
      qs.append("select vas.app_created_date, ");
      qs.append("       vas.app_seq_num, ");
      qs.append("       vas.app_type, ");
      qs.append("       vas.merc_cntrl_number, ");
      qs.append("       vas.merch_business_name, ");
      qs.append("       vas.merch_number, ");
      qs.append("       vas.merch_credit_status, ");
      qs.append("       vas.name, ");
      qs.append("       vas.vnumber ");
      qs.append("from   view_app_status vas, merchequipment me ");
      qs.append("where  vas.app_seq_num > 0 ");

      qs.append("and vas.app_seq_num = me.app_seq_num(+) ");

      qs.append("and me.equiptype_code(+) != " + mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE + " ");
      qs.append("and me.equiplendtype_code(+) != " + mesConstants.APP_EQUIP_OWNED + " ");

      if(this.owner == MYAPPS)
      {
        qs.append("and vas.app_user_id = ? ");
      }
      
      if(this.appType != ALLTYPES)
      {
        qs.append("and vas.app_type = " + this.appType + " ");
      }
      
      
      switch(this.searchCriteria)
      {
        case SEARCH_CONTROL:
          qs.append("and vas.merc_cntrl_number = ? ");
          qs.append("order by vas.app_created_date desc ");
        
          ps = getPreparedStatement(qs.toString());
        
          if(this.owner == MYAPPS)
          {
            ps.setLong(1, userId);
            ps.setLong(2, this.controlNumber);
          }
          else
          {
            ps.setLong(1, this.controlNumber);
          }
          break;
        case SEARCH_DATE:
          if(this.specificDateDetail == 2)
          {
            qs.append("and vas.app_created_date >= ? "); 
          }
          else
          {
            qs.append("and vas.app_created_date between ? and ? ");
          }
        
          if(this.status != ALLSTATUS)
          {
            qs.append("and vas.merch_credit_status = " + this.status + " ");
          }
          else
          {
            qs.append("and vas.merch_credit_status != " + com.mes.ops.QueueConstants.CREDIT_CANCEL + " ");
          }

          qs.append("order by vas.app_created_date desc ");
        
          ps = getPreparedStatement(qs.toString());
          if(this.owner == 1)
          {
            ps.setLong(1, userId);
            ps.setDate(2, new java.sql.Date(this.specificDate.getTime()));
            if(this.specificDateDetail != 2)
              ps.setDate(3, new java.sql.Date(this.specificDateNext.getTime()));
          }
          else
          {
            ps.setDate(1, new java.sql.Date(this.specificDate.getTime()));
            if(this.specificDateDetail != 2)
              ps.setDate(2, new java.sql.Date(this.specificDateNext.getTime()));
          }
          break;
        case SEARCH_DATERANGE:
          qs.append("and vas.app_created_date between ? and ? "); 
          
          if(this.status != ALLSTATUS)
          {
            qs.append("and vas.merch_credit_status = " + this.status + " ");
          }
          else
          {
            qs.append("and vas.merch_credit_status != " + com.mes.ops.QueueConstants.CREDIT_CANCEL + " ");
          }        
          

          qs.append("order by vas.app_created_date desc ");
        
          ps = getPreparedStatement(qs.toString());
          if(this.owner == 1)
          {
            ps.setLong(1, userId);
            ps.setDate(2, new java.sql.Date(this.startDate.getTime()));
            ps.setDate(3, new java.sql.Date(this.endDateNext.getTime()));
          }
          else
          {
            ps.setDate(1, new java.sql.Date(this.startDate.getTime()));
            ps.setDate(2, new java.sql.Date(this.endDateNext.getTime()));
          }
          break;
        case SEARCH_MERCHANT:
          qs.append("and vas.merch_number = ? ");
          qs.append("order by vas.app_created_date desc ");
        
          ps = getPreparedStatement(qs.toString());
        
          if(this.owner == MYAPPS)
          {
            ps.setLong(1, userId);
            ps.setLong(2, this.merchantNumber);
          }
          else
          {
            ps.setLong(1, this.merchantNumber);
          }
          break;
        case SEARCH_DBA:
          qs.append("and upper(vas.merch_business_name) like ? escape \'\\\' ");
          
          if(this.status != ALLSTATUS)
          {
            qs.append("and vas.merch_credit_status = " + this.status + " ");
          }
          else
          {
            qs.append("and vas.merch_credit_status != " + com.mes.ops.QueueConstants.CREDIT_CANCEL + " ");
          }
          
          qs.append("order by vas.app_created_date desc ");
        
          ps = getPreparedStatement(qs.toString());
        
          if(this.owner == MYAPPS)
          {
            ps.setLong(1, userId);
            ps.setString(2, "%" + this.dba.toUpperCase() + "%");
          }
          else
          {
            ps.setString(1, "%" + this.dba.toUpperCase() + "%");
          }
          break;
        default:
          
          if(this.status != ALLSTATUS)
          {
            qs.append("and vas.merch_credit_status = " + this.status + " ");
          }
          else
          {
            qs.append("and vas.merch_credit_status != " + com.mes.ops.QueueConstants.CREDIT_CANCEL + " ");
          }          

          qs.append("order by vas.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          if(this.owner == 1)
          {
            ps.setLong(1, userId);
          }
          break;
      }
      
      this.rs = ps.executeQuery();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return this.rs;
  }  


  public boolean validateData()
  {
    boolean result = false;
    String temp = "";
    switch(this.searchCriteria)
    {
      case SEARCH_CONTROL:
        if(this.controlNumber == -1)
        {
          temp = "Please enter a valid control number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_DATE:
        if(this.specificDateStr.equals("-1") || !validDate(this.specificDateStr))
        {
          temp = "Please enter a valid specific date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          getSpecificDateNext(this.specificDateStr);
        }
        break;
      case SEARCH_DATERANGE:
        if(this.startDateStr.equals("-1") || !validDate(this.startDateStr))
        {
          temp = "Please enter a valid start date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        if(!this.endDateStr.equals("-1")) 
        {
          if(this.endDateStr.equals("-2") || !validDate(this.endDateStr))
          {
            temp = "Please enter a valid end date in the following format:  mm/dd/yy";
            try
            {
              errors.add(temp);
            }
            catch(Exception e)
            {}
            this.hasErrors = true;
          }
          else
          {
            getEndDateNext(this.endDateStr);
          }
        }
        else
        {
          getEndDateNext("12/31/10");
        }
        break;
      case SEARCH_MERCHANT:
        if(this.merchantNumber == -1)
        {
          temp = "Please enter a valid merchant number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_DBA:
        if(isBlank(this.dba))
        {
          temp = "Please enter a Merchant DBA Name";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      default:
        this.hasErrors = false;
        break;
    }
    if(!this.hasErrors)
      result = true;
    return result;
  }
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private void getEndDateNext(String endDate)
  {
    int index1  = endDate.indexOf('/');
    int index2  = endDate.lastIndexOf('/');
    String mon  = endDate.substring(0,index1);
    String day  = endDate.substring(index1+1,index2);
    String year = endDate.substring(index2+1);
    
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day                   = String.valueOf(nextDay);
      String date           = mon + "/" + day + "/" + year;
      DateFormat fmt        = DateFormat.getDateInstance(DateFormat.SHORT);
      this.endDateNext = fmt.parse(date);
    }
    catch(Exception e)
    {
      this.endDateNext = null;
    }
  }

  private void getSpecificDateNext(String specificDate)
  {
    int index1  = specificDate.indexOf('/');
    int index2  = specificDate.lastIndexOf('/');
    String mon  = specificDate.substring(0,index1);
    String day  = specificDate.substring(index1+1,index2);
    String year = specificDate.substring(index2+1);
    
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day                   = String.valueOf(nextDay);
      String date           = mon + "/" + day + "/" + year;
      DateFormat fmt        = DateFormat.getDateInstance(DateFormat.SHORT);
      this.specificDateNext = fmt.parse(date);
    }
    catch(Exception e)
    {
      this.specificDateNext = null;
    }
  }
  public void setControlNumber(String controlNumber)
  {
    try
    {
      this.controlNumber = Long.parseLong(controlNumber.trim()); 
    }
    catch(Exception e)
    {
      this.controlNumber = -1;
    }
  }
  public String getControlNumber()
  {
    String result = "";
    if(this.controlNumber != -1)
    {
      result = String.valueOf(controlNumber);
    }
    return result;
  }

  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      this.merchantNumber = Long.parseLong(merchantNumber.trim()); 
    }
    catch(Exception e)
    {
      this.merchantNumber = -1;
    }
  }
  public String getMerchantNumber()
  {
    String result = "";
    if(this.merchantNumber != -1)
    {
      result = String.valueOf(merchantNumber);
    }
    return result;
  }

  public void setDba(String dba)
  {
    this.dba = dba; 
  }
  public String getDba()
  {
    return this.dba;
  }
 
  public void setSpecificDate(String specificDate)
  {
    this.specificDateStr = specificDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(specificDate.trim());
      try
      {
        specificDate          = specificDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
        this.specificDate     = aDate;
      }
      catch(Exception e)
      {
        this.specificDate    = null;
        this.specificDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.specificDate      = null;
      this.specificDateStr   = "-1";
    }
  }
 
  public String getSpecificDate()
  {
    String result = "";
    if(!this.specificDateStr.equals("-1"))
    {
      result = this.specificDateStr;
    }
    return result;
  }
 
 
  public void setStartDate(String startDate)
  {
    this.startDateStr = startDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(startDate.trim());
      try
      {
        startDate          = startDate.replace('/','0');
        long tempLong      = Long.parseLong(startDate.trim());
        this.startDate     = aDate;
      }
      catch(Exception e)
      {
        this.startDate    = null;
        this.startDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.startDate    = null;
      this.startDateStr = "-1";
    }
  }
 
  public String getStartDate()
  {
    String result = "";
    if(!this.startDateStr.equals("-1"))
    {
      result = this.startDateStr;
    }
    return result;
  }
  public void setEndDate(String endDate)
  {
    this.endDateStr = endDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(endDate.trim());
      try
      {
        endDate        = endDate.replace('/','0');
        long tempLong  = Long.parseLong(endDate.trim());
        this.endDate   = aDate;
      }
      catch(Exception e)
      {
        this.endDate    = null;
        this.endDateStr = "-2";
      }
    }
    catch(ParseException e)
    {
      this.endDate    = null;
      this.endDateStr = "-2";
    }
  }
 
  public String getEndDate()
  {
    String result = "";
    if(!this.endDateStr.equals("-1") && !this.endDateStr.equals("-2"))
    {
      result = this.endDateStr;
    }
    return result;
  }

  public void setSpecificDateDetail(String specificDateDetail)
  {
    try
    {
      this.specificDateDetail = Integer.parseInt(specificDateDetail); 
    }
    catch(Exception e)
    {
      this.specificDateDetail = -1;
    }
  }
  public int getSpecificDateDetail()
  {
    int result = 0;
    if(this.specificDateDetail != -1)
    {
      result = this.specificDateDetail;
    }
    return result;
  }

  public void setStatus(String status)
  {
    try
    {
      this.status = Integer.parseInt(status); 
    }
    catch(Exception e)
    {
      this.status = -1;
    }
  }
  public int getStatus()
  {
    int result = 4;
    if(this.status != -1)
    {
      result = this.status;
    }
    return result;
  }

  public void setOwner(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner); 
    }
    catch(Exception e)
    {
      this.owner = -1;
    }
  }

  public int getOwner()
  {
    int result = 1;
    if(this.owner != -1)
    {
      result = this.owner;
    }
    return result;
  }

  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType); 
    }
    catch(Exception e)
    {
      this.owner = -1;
    }
  }

  public int getAppType()
  {
    int result = 1;
    if(this.owner != -1)
    {
      result = this.appType;
    }
    return result;
  }

  
  
  
  
  
  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return this.hasErrors;
  }

  public Vector getErrors()
  {
    return errors;
  }

}
