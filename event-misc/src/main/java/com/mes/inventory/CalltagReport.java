/*@lineinfo:filename=CalltagReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/CalltagReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:37p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class CalltagReport extends DateSQLJBean
{
  public class CalltagData
  {
    private String      calltagNumber;
    private String      referenceNumber;
    private String      merchantNumber;
    private String      associationNumber;
    private String      dbaName;
    private String      client;

    private String      calltagSerialNum;
    private String      calltagDescription;
    private String      calltagUnitCost;

    private String      calltagDate;
    private Timestamp   calltagDateSorted;
    private String      calltagType;
    private String      calltagStatus;

    public CalltagData()
    {
      calltagNumber         = "";
      referenceNumber       = "";
      merchantNumber        = "";
      associationNumber     = "";
      dbaName               = "";
      client                = "";
      calltagSerialNum      = "";
      calltagDescription    = "";
      calltagUnitCost       = "";
      calltagDate           = "";
      calltagDateSorted     = null;
      calltagType           = "";
      calltagStatus         = "";
    }
    
    public CalltagData   (String     calltagNumber,    
                          String     referenceNumber,  
                          String     merchantNumber,   
                          String     associationNumber,
                          String     dbaName,          
                          String     client,           
                          Timestamp  calltagDateSorted,
                          String     calltagDate,      
                          String     calltagSerialNum,
                          String     calltagDescription,
                          String     calltagUnitCost,
                          String     calltagType,      
                          String     calltagStatus)
    {

    setCalltagNumber(calltagNumber);
    setReferenceNumber(referenceNumber);
    setMerchantNumber(merchantNumber);
    setAssociationNumber(associationNumber);
    setDbaName(dbaName);
    setClient(client);
    setCalltagDate(calltagDate);
    setCalltagDateSorted(calltagDateSorted);
    setCalltagType(calltagType);
    setCalltagStatus(calltagStatus);
    setCalltagSerialNum(calltagSerialNum);
    setCalltagDescription(calltagDescription);
    setCalltagUnitCost(calltagUnitCost);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }

    public void setCalltagSerialNum(String calltagSerialNum)
    {
      this.calltagSerialNum = processStringField(calltagSerialNum);
    }
    public String getCalltagSerialNum()
    {
      return this.calltagSerialNum;
    }

    public void setCalltagDescription(String calltagDescription)
    {
      this.calltagDescription = processStringField(calltagDescription);
    }
    public String getCalltagDescription()
    {
      return this.calltagDescription;
    }

    public void setCalltagUnitCost(String calltagUnitCost)
    {
      this.calltagUnitCost = processStringField(calltagUnitCost);
    }
    public String getCalltagUnitCost()
    {
      return this.calltagUnitCost;
    }

    public void setReferenceNumber(String referenceNumber)
    {
      this.referenceNumber = processStringField(referenceNumber);
    }
    public String getReferenceNumber()
    {
      return this.referenceNumber;
    }

    public void setCalltagNumber(String calltagNumber)
    {
      this.calltagNumber = processStringField(calltagNumber);
    }
    public String getCalltagNumber()
    {
      return this.calltagNumber;
    }


    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }

    public void setAssociationNumber(String associationNumber)
    {
      this.associationNumber = processStringField(associationNumber);
    }
    public String getAssociationNumber()
    {
      return this.associationNumber;
    }


    public void setCalltagDate(String calltagDate)
    {
      this.calltagDate = processStringField(calltagDate);
    }
    public String getCalltagDate()
    {
      return this.calltagDate;
    }

    public void setCalltagDateSorted(Timestamp calltagDateSorted)
    {
      this.calltagDateSorted = calltagDateSorted;
    }
    public String getCalltagDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(calltagDateSorted, "yyyyMMddHHmmss");
    }

    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }

    public void setClient(String client)
    {
      this.client = processStringField(client);
    }
    public String getClient()
    {
      return this.client;
    }

    public void setCalltagType(String calltagType)
    {
      this.calltagType = processStringField(calltagType);
    }
    public String getCalltagType()
    {
      return this.calltagType;
    }

    public void setCalltagStatus(String calltagStatus)
    {
      this.calltagStatus = processStringField(calltagStatus);
    }
    public String getCalltagStatus()
    {
      return this.calltagStatus;
    }

  }
  

  public class CallTagDataComparator
    implements Comparator
  {
    public final static int   SB_MERCHANT_NUMBER      = 0;
    public final static int   SB_CALLTAG_DATE         = 1;
    public final static int   SB_DBA_NAME             = 2;
    public final static int   SB_CLIENT               = 3;
    public final static int   SB_CALLTAG_NUMBER       = 4;
    public final static int   SB_CALLTAG_TYPE         = 5;
    public final static int   SB_REFERENCE_NUMBER     = 6;
    public final static int   SB_CALLTAG_STATUS       = 7;
    public final static int   SB_ASSO_NUMBER          = 8;
    public final static int   SB_CALLTAG_SERIAL_NUM   = 9;
    public final static int   SB_CALLTAG_DESCRIPTION  = 10;
    public final static int   SB_CALLTAG_UNIT_COST    = 11;

    private int               sortBy;
    private boolean           sortAscending           = false;
    
    public CallTagDataComparator()
    {
      this.sortBy = SB_CALLTAG_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_MERCHANT_NUMBER && sortBy <= SB_CALLTAG_UNIT_COST)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(CalltagData o1, CalltagData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getMerchantNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_SERIAL_NUM:
            compareString1 = o1.getCalltagSerialNum() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagSerialNum() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_DESCRIPTION:
            compareString1 = o1.getCalltagDescription() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagDescription() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_UNIT_COST:
            compareString1 = o1.getCalltagUnitCost() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagUnitCost() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;

          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getAssociationNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CALLTAG_DATE:
            compareString1 = o1.getCalltagDateSorted() + o1.getDbaName() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagDateSorted() + o2.getDbaName() + o2.getReferenceNumber();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getDbaName() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getClient() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_REFERENCE_NUMBER:
            compareString1 = o1.getReferenceNumber() + o1.getCalltagDateSorted();
            compareString2 = o2.getReferenceNumber() + o2.getCalltagDateSorted();
            break;

          case SB_CALLTAG_NUMBER:
            compareString1 = o1.getCalltagNumber() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CALLTAG_TYPE:
            compareString1 = o1.getCalltagType() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagType() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CALLTAG_STATUS:
            compareString1 = o1.getCalltagStatus() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagStatus() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(CalltagData o1, CalltagData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getCalltagDateSorted()+ o1.getReferenceNumber();
            compareString2 = o2.getMerchantNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_SERIAL_NUM:
            compareString1 = o1.getCalltagSerialNum() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagSerialNum() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_DESCRIPTION:
            compareString1 = o1.getCalltagDescription() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagDescription() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          case SB_CALLTAG_UNIT_COST:
            compareString1 = o1.getCalltagUnitCost() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagUnitCost() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;

          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getAssociationNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;                                                                 
            
          case SB_CALLTAG_DATE:
            compareString1 = o1.getCalltagDateSorted() + o1.getDbaName() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagDateSorted() + o2.getDbaName() + o2.getReferenceNumber();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getDbaName() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getClient() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_REFERENCE_NUMBER:
            compareString1 = o1.getReferenceNumber() + o1.getCalltagDateSorted();
            compareString2 = o2.getReferenceNumber() + o2.getCalltagDateSorted();
            break;

          case SB_CALLTAG_NUMBER:
            compareString1 = o1.getCalltagNumber() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagNumber() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CALLTAG_TYPE:
            compareString1 = o1.getCalltagType() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagType() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
            
          case SB_CALLTAG_STATUS:
            compareString1 = o1.getCalltagStatus() + o1.getCalltagDateSorted() + o1.getReferenceNumber();
            compareString2 = o2.getCalltagStatus() + o2.getCalltagDateSorted() + o2.getReferenceNumber();
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((CalltagData)o1, (CalltagData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((CalltagData)o1, (CalltagData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  private String                lookupValue         = "";
  private String                lastLookupValue     = "";
  
  private int                   action              = mesConstants.LU_ACTION_INVALID;
  private String                actionDescription   = "Invalid Action";
  
  private CallTagDataComparator cdc                 = new CallTagDataComparator();
  private Vector                lookupResults       = new Vector();
  private TreeSet               sortedResults       = null;
  
  private boolean               submitted           = false;
  private boolean               refresh             = false;
  
  //search criteria
  private int                   client              = -1;
  private int                   lastClient          = -1;

  private int                   calltagType         = -1;
  private int                   lastCalltagType     = -1;
  
  private int                   calltagStatus       = -1;
  private int                   lastCalltagStatus   = -1;

  private int                   lastFromMonth       = -1;
  private int                   lastFromDay         = -1;
  private int                   lastFromYear        = -1;
  private int                   lastToMonth         = -1;
  private int                   lastToDay           = -1;
  private int                   lastToYear          = -1;
  
  public  Vector                clients             = new Vector();
  public  Vector                clientValues        = new Vector();

  public  Vector                calltagTypes        = new Vector();
  public  Vector                calltagTypeValues   = new Vector();

  public  Vector                calltagStatuses     = new Vector();
  public  Vector                calltagStatusValues = new Vector();

  public CalltagReport()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        client          != lastClient         ||
        calltagType     != lastCalltagType    ||
        calltagStatus   != lastCalltagStatus  ||
        fromMonth       != lastFromMonth      ||
        fromDay         != lastFromDay        ||
        fromYear        != lastFromYear       ||
        toMonth         != lastToMonth        ||
        toDay           != lastToDay          ||
        toYear          != lastToYear         ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(clients.size() == 0)
      {
        clients.clear();
        clientValues.clear();
        calltagTypes.clear();
        calltagTypeValues.clear();
        calltagStatuses.clear();
        calltagStatusValues.clear();
      
        // clients
        clients.add("All Clients");
        clientValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:602^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name, app_type_code 
//            from    app_type
//            where   SEPARATE_INVENTORY = 'Y'
//            order by inventory_owner_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name, app_type_code \n          from    app_type\n          where   SEPARATE_INVENTORY = 'Y'\n          order by inventory_owner_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.CalltagReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.CalltagReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:608^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          clients.add(rs.getString("inventory_owner_name"));
          clientValues.add(rs.getString("app_type_code"));
        }
      
        rs.close();
        it.close();

        calltagTypes.add("All Types");
        calltagTypeValues.add("-1");
        calltagTypes.add("Repairs");
        calltagTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_REPAIR));
        calltagTypes.add("Swaps");
        calltagTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_SWAP));
        calltagTypes.add("Pinpad Exchanges");
        calltagTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_PINPAD_EXCHANGE));

        // status
        calltagStatuses.add("All Statuses");
        calltagStatusValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:632^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//            from    equip_calltag_status
//            order by calltag_code asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n          from    equip_calltag_status\n          order by calltag_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.CalltagReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.CalltagReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          calltagStatuses.add(rs.getString("calltag_description"));
          calltagStatusValues.add(rs.getString("calltag_code"));
        }
      
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    try
    {
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        lookupResults.clear();
        
        //calls to get data
        switch(calltagType)
        {
          case mesConstants.ACTION_CODE_REPAIR:
            getRepairData(longLookup, stringLookup, stringLookup2);
          break;

          case mesConstants.ACTION_CODE_SWAP:
            getSwapData(longLookup, stringLookup, stringLookup2);
          break;

          case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
            getExchangeData(longLookup, stringLookup, stringLookup2);
          break;
          
          //get both
          default:
            getSwapData(longLookup, stringLookup, stringLookup2);
            getRepairData(longLookup, stringLookup, stringLookup2);
            getExchangeData(longLookup, stringLookup, stringLookup2);
          break;
        }

        lastClient          = client;
        lastCalltagStatus   = calltagStatus;
        lastCalltagType     = calltagType;
        lastFromMonth       = fromMonth;
        lastFromDay         = fromDay;
        lastFromYear        = fromYear;
        lastToMonth         = toMonth;
        lastToDay           = toDay;
        lastToYear          = toYear;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(cdc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
    }
  }


  /*
  ** METHOD public void getSwapData()
  **
  */
  private void getSwapData(long longLookup, String stringLookup, String stringLookup2)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate(); 
      
      /*@lineinfo:generated-code*//*@lineinfo:792^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                  mt.merchant_type_desc           client_name,
//                  mt.merchant_number              merchant_number,
//                  mt.association_number           association_number,
//                  emt.action_date                 calltag_date,
//                  emta.action_description         calltag_type,
//                  ecs.calltag_description         calltag_status,
//                  es.SWAP_REF_NUM                 reference_number,
//                  es.call_tag_num                 calltag_number,
//                  es.serial_num_in                calltag_serial_num,
//                  equip.equip_descriptor          calltag_description,
//                  ei.ei_unit_cost                 calltag_unit_cost
//  
//          from    merchant_types                  mt,
//                  equip_merchant_tracking         emt,
//                  equip_merchant_tracking_action  emta,
//                  equip_calltag_status            ecs,
//                  equip_swap                      es,
//                  equip_inventory                 ei,
//                  equipment                       equip
//  
//          where   trunc(emt.action_date) between :fromDate and :toDate and
//                  emt.action = 2          and 
//                  emt.cancel_date is null and
//                  emt.merchant_number     = mt.merchant_number  and
//                  emt.action              = emta.action_code    and
//                  emt.ref_num_serial_num  = es.swap_ref_num     and
//                  es.swap_STATUS          = ecs.calltag_code    and
//                  (-1 = :client or mt.merchant_type = :client)  and
//                  (-1 = :calltagStatus or es.swap_STATUS = :calltagStatus) and
//                  (
//                    'passall'              = :stringLookup or
//                    mt.merchant_number     = :longLookup or
//                    mt.association_number  = :longLookup or
//                    emt.ref_num_serial_num = :stringLookup2 or 
//                    upper(es.call_tag_num) = :stringLookup2 or 
//                    upper(mt.merchant_name) like :stringLookup
//                  ) and
//                  es.serial_num_in  = ei.ei_serial_number(+)    and
//                  ei.ei_part_number = equip.equip_model(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                mt.merchant_type_desc           client_name,\n                mt.merchant_number              merchant_number,\n                mt.association_number           association_number,\n                emt.action_date                 calltag_date,\n                emta.action_description         calltag_type,\n                ecs.calltag_description         calltag_status,\n                es.SWAP_REF_NUM                 reference_number,\n                es.call_tag_num                 calltag_number,\n                es.serial_num_in                calltag_serial_num,\n                equip.equip_descriptor          calltag_description,\n                ei.ei_unit_cost                 calltag_unit_cost\n\n        from    merchant_types                  mt,\n                equip_merchant_tracking         emt,\n                equip_merchant_tracking_action  emta,\n                equip_calltag_status            ecs,\n                equip_swap                      es,\n                equip_inventory                 ei,\n                equipment                       equip\n\n        where   trunc(emt.action_date) between  :1  and  :2  and\n                emt.action = 2          and \n                emt.cancel_date is null and\n                emt.merchant_number     = mt.merchant_number  and\n                emt.action              = emta.action_code    and\n                emt.ref_num_serial_num  = es.swap_ref_num     and\n                es.swap_STATUS          = ecs.calltag_code    and\n                (-1 =  :3  or mt.merchant_type =  :4 )  and\n                (-1 =  :5  or es.swap_STATUS =  :6 ) and\n                (\n                  'passall'              =  :7  or\n                  mt.merchant_number     =  :8  or\n                  mt.association_number  =  :9  or\n                  emt.ref_num_serial_num =  :10  or \n                  upper(es.call_tag_num) =  :11  or \n                  upper(mt.merchant_name) like  :12 \n                ) and\n                es.serial_num_in  = ei.ei_serial_number(+)    and\n                ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.CalltagReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup2);
   __sJT_st.setString(11,stringLookup2);
   __sJT_st.setString(12,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.CalltagReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:834^7*/
      
      rs = it.getResultSet();
    
      while(rs.next())
      {
        lookupResults.add(new CalltagData   (rs.getString("calltag_number"),  
                                             rs.getString("reference_number"),
                                             rs.getString("merchant_number"), 
                                             rs.getString("association_number"),
                                             rs.getString("dba_name"), 
                                             rs.getString("client_name"), 
                                             rs.getTimestamp("calltag_date"), 
                                             DateTimeFormatter.getFormattedDate(rs.getTimestamp("calltag_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                             rs.getString("calltag_serial_Num"),
                                             rs.getString("calltag_description"),
                                             rs.getString("calltag_unit_cost"),
                                             rs.getString("calltag_type"), 
                                             rs.getString("calltag_status")));
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getSwapData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD public void getSwapData()
  **
  */
  private void getExchangeData(long longLookup, String stringLookup, String stringLookup2)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate(); 
      
      /*@lineinfo:generated-code*//*@lineinfo:886^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                  mt.merchant_type_desc           client_name,
//                  mt.merchant_number              merchant_number,
//                  mt.association_number           association_number,
//                  emt.action_date                 calltag_date,
//                  emta.action_description         calltag_type,
//                  ecs.calltag_description         calltag_status,
//                  ee.EXCHANGE_REF_NUM             reference_number,
//                  ee.call_tag_num                 calltag_number,
//                  ee.serial_num_in                calltag_serial_num,
//                  equip.equip_descriptor          calltag_description,
//                  ei.ei_unit_cost                 calltag_unit_cost
//  
//          from    merchant_types                  mt,
//                  equip_merchant_tracking         emt,
//                  equip_merchant_tracking_action  emta,
//                  equip_calltag_status            ecs,
//                  equip_exchange                  ee,
//                  equip_inventory                 ei,
//                  equipment                       equip
//  
//          where   trunc(emt.action_date) between :fromDate and :toDate and
//                  emt.action = 6          and 
//                  emt.cancel_date is null and
//                  emt.merchant_number     = mt.merchant_number  and
//                  emt.action              = emta.action_code    and
//                  emt.ref_num_serial_num  = ee.exchange_ref_num and
//                  ee.EXCHANGE_STATUS      = ecs.calltag_code    and
//                  (-1 = :client         or mt.merchant_type = :client)          and
//                  (-1 = :calltagStatus  or ee.EXCHANGE_STATUS = :calltagStatus) and
//                  (
//                    'passall'              = :stringLookup or
//                    mt.merchant_number     = :longLookup or
//                    mt.association_number  = :longLookup or
//                    emt.ref_num_serial_num = :stringLookup2 or 
//                    upper(ee.call_tag_num) = :stringLookup2 or 
//                    upper(mt.merchant_name) like :stringLookup
//                  ) and
//                  ee.serial_num_in  = ei.ei_serial_number(+)    and
//                  ei.ei_part_number = equip.equip_model(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                mt.merchant_type_desc           client_name,\n                mt.merchant_number              merchant_number,\n                mt.association_number           association_number,\n                emt.action_date                 calltag_date,\n                emta.action_description         calltag_type,\n                ecs.calltag_description         calltag_status,\n                ee.EXCHANGE_REF_NUM             reference_number,\n                ee.call_tag_num                 calltag_number,\n                ee.serial_num_in                calltag_serial_num,\n                equip.equip_descriptor          calltag_description,\n                ei.ei_unit_cost                 calltag_unit_cost\n\n        from    merchant_types                  mt,\n                equip_merchant_tracking         emt,\n                equip_merchant_tracking_action  emta,\n                equip_calltag_status            ecs,\n                equip_exchange                  ee,\n                equip_inventory                 ei,\n                equipment                       equip\n\n        where   trunc(emt.action_date) between  :1  and  :2  and\n                emt.action = 6          and \n                emt.cancel_date is null and\n                emt.merchant_number     = mt.merchant_number  and\n                emt.action              = emta.action_code    and\n                emt.ref_num_serial_num  = ee.exchange_ref_num and\n                ee.EXCHANGE_STATUS      = ecs.calltag_code    and\n                (-1 =  :3          or mt.merchant_type =  :4 )          and\n                (-1 =  :5   or ee.EXCHANGE_STATUS =  :6 ) and\n                (\n                  'passall'              =  :7  or\n                  mt.merchant_number     =  :8  or\n                  mt.association_number  =  :9  or\n                  emt.ref_num_serial_num =  :10  or \n                  upper(ee.call_tag_num) =  :11  or \n                  upper(mt.merchant_name) like  :12 \n                ) and\n                ee.serial_num_in  = ei.ei_serial_number(+)    and\n                ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.CalltagReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup2);
   __sJT_st.setString(11,stringLookup2);
   __sJT_st.setString(12,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.CalltagReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:928^7*/
      
      rs = it.getResultSet();
    
      while(rs.next())
      {
        lookupResults.add(new CalltagData   (rs.getString("calltag_number"),  
                                             rs.getString("reference_number"),
                                             rs.getString("merchant_number"), 
                                             rs.getString("association_number"),
                                             rs.getString("dba_name"), 
                                             rs.getString("client_name"), 
                                             rs.getTimestamp("calltag_date"), 
                                             DateTimeFormatter.getFormattedDate(rs.getTimestamp("calltag_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                             rs.getString("calltag_serial_Num"),
                                             rs.getString("calltag_description"),
                                             rs.getString("calltag_unit_cost"),
                                             rs.getString("calltag_type"), 
                                             rs.getString("calltag_status")));
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getExchangeData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD public void getRepairData()
  **
  */
  private void getRepairData(long longLookup, String stringLookup, String stringLookup2)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      connect();

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate(); 
      
      /*@lineinfo:generated-code*//*@lineinfo:979^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  get_dba_name(emt.merchant_number)     dba_name,
//                  get_association(emt.merchant_number)  association_number,
//                  emt.merchant_number                   merchant_number,
//                  emt.action_date                       calltag_date,
//                  emta.action_description               calltag_type,
//                  ecs.calltag_description               calltag_status,
//                  er.REPAIR_REF_NUM                     reference_number,
//                  er.call_tag_num                       calltag_number,
//                  app.inventory_owner_name              client_name,
//                  er.serial_num                         calltag_serial_num,
//                  equip.equip_descriptor                calltag_description,
//                  ei.ei_unit_cost                       calltag_unit_cost
//          from    equip_merchant_tracking               emt,
//                  equip_merchant_tracking_action        emta,
//                  equip_calltag_status                  ecs,
//                  equip_repair                          er,
//                  app_type                              app,
//                  equip_inventory                       ei,
//                  equipment                             equip
//          where   trunc(emt.action_date) between :fromDate and :toDate and
//                  emt.action = 3          and 
//                  emt.cancel_date is null and
//                  emt.owner               = app.app_type_code(+)  and
//                  emt.action              = emta.action_code      and
//                  emt.ref_num_serial_num  = er.repair_ref_num  and
//                  er.REPAIR_STATUS = ecs.calltag_code and
//                  (-1 = :client        or emt.owner = :client) and
//                  (-1 = :calltagStatus or er.REPAIR_STATUS = :calltagStatus) and
//                  (
//                    'passall'              = :stringLookup or
//                    emt.merchant_number    = :longLookup or
//                    emt.ref_num_serial_num = :stringLookup2 or 
//                    upper(er.call_tag_num) = :stringLookup2
//                  ) and
//                  er.serial_num     = ei.ei_serial_number(+)    and
//                  ei.ei_part_number = equip.equip_model(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  get_dba_name(emt.merchant_number)     dba_name,\n                get_association(emt.merchant_number)  association_number,\n                emt.merchant_number                   merchant_number,\n                emt.action_date                       calltag_date,\n                emta.action_description               calltag_type,\n                ecs.calltag_description               calltag_status,\n                er.REPAIR_REF_NUM                     reference_number,\n                er.call_tag_num                       calltag_number,\n                app.inventory_owner_name              client_name,\n                er.serial_num                         calltag_serial_num,\n                equip.equip_descriptor                calltag_description,\n                ei.ei_unit_cost                       calltag_unit_cost\n        from    equip_merchant_tracking               emt,\n                equip_merchant_tracking_action        emta,\n                equip_calltag_status                  ecs,\n                equip_repair                          er,\n                app_type                              app,\n                equip_inventory                       ei,\n                equipment                             equip\n        where   trunc(emt.action_date) between  :1  and  :2  and\n                emt.action = 3          and \n                emt.cancel_date is null and\n                emt.owner               = app.app_type_code(+)  and\n                emt.action              = emta.action_code      and\n                emt.ref_num_serial_num  = er.repair_ref_num  and\n                er.REPAIR_STATUS = ecs.calltag_code and\n                (-1 =  :3         or emt.owner =  :4 ) and\n                (-1 =  :5  or er.REPAIR_STATUS =  :6 ) and\n                (\n                  'passall'              =  :7  or\n                  emt.merchant_number    =  :8  or\n                  emt.ref_num_serial_num =  :9  or \n                  upper(er.call_tag_num) =  :10 \n                ) and\n                er.serial_num     = ei.ei_serial_number(+)    and\n                ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.CalltagReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setString(9,stringLookup2);
   __sJT_st.setString(10,stringLookup2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.inventory.CalltagReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1017^7*/


      rs = it.getResultSet();
    
      while(rs.next())
      {
        lookupResults.add(new CalltagData   (rs.getString("calltag_number"),  
                                             rs.getString("reference_number"),
                                             rs.getString("merchant_number"), 
                                             rs.getString("association_number"),
                                             rs.getString("dba_name"), 
                                             rs.getString("client_name"), 
                                             rs.getTimestamp("calltag_date"), 
                                             DateTimeFormatter.getFormattedDate(rs.getTimestamp("calltag_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                             rs.getString("calltag_serial_Num"),
                                             rs.getString("calltag_description"),
                                             rs.getString("calltag_unit_cost"),
                                             rs.getString("calltag_type"), 
                                             rs.getString("calltag_status")));
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRepairData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "CallTag Report";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      cdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setClient(String client)
  {
    try
    {
      this.client = Integer.parseInt(client);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getClient()
  {
    return Integer.toString(this.client);
  }
 
  public synchronized void setCalltagStatus(String calltagStatus)
  {
    try
    {
      this.calltagStatus = Integer.parseInt(calltagStatus);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getCalltagStatus()
  {
    return Integer.toString(this.calltagStatus);
  }

  public synchronized void setCalltagType(String calltagType)
  {
    try
    {
      this.calltagType = Integer.parseInt(calltagType);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getCalltagType()
  {
    return Integer.toString(this.calltagType);
  }

  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }

  public synchronized boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
}/*@lineinfo:generated-code*/