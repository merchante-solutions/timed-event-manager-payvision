/*@lineinfo:filename=PinpadExchangeBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PinpadExchangeBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:47p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.ops.InventoryBase;
import sqlj.runtime.ResultSetIterator;

public class PinpadExchangeBean extends InventoryBase
{
  public static final int RECEIVED_IN_STOCK       = 0;
  public static final int ENCRYPTED_IN_STOCK      = 3;

  public static final int EXCHANGE_INCOMPLETE     = mesConstants.CALLTAG_INCOMPLETE;
  public static final int EXCHANGE_COMPLETE       = mesConstants.CALLTAG_COMPLETE;

  public static final int EXCHANGE_IN_NOTE        = 0;
  public static final int EXCHANGE_OUT_NOTE       = 1;
 

  //always the same 
  private String   merchantNum                    = "";
  private String   exchangeNum                    = "";
  private int      exchangeStatus                 = 0;
  private String   callTag                        = "";

  private String   serialNumOutList               = "";
  private String   serialNumOutMan                = "";

  private String   serialNumInMan                 = "";
  private String   serialNumInList                = "";
  private String   partNum                        = "";
  private String   owner                          = "";
  private String   ownerDesc                      = "";
  
  //use once already submitted
  private String   serialNumOut                   = "";
  private String   descriptionOut                 = "";
  private String   serialNumIn                    = "";
  private String   descriptionIn                  = "";
  
  //holds current statues once submitted
  private String   statusIn                       = "";
  private String   statusOut                      = "";
  private String   statusOutDesc                  = "";
  
  private String   shippingCostIn                 = "";
  private String   shippingCostOut                = "";
  
  private String   notesIn                        = "";
  private String   notesOut                       = "";

  private boolean  inSubmitted                    = false;
  private boolean  outSubmitted                   = false;

  public  Vector   availableEquipDesc             = new Vector();
  public  Vector   availableEquipSerialNum        = new Vector();

  public  Vector   merchEquipDesc                 = new Vector();
  public  Vector   merchEquipSerialNum            = new Vector();

  public  Vector   equipDesc                      = new Vector();
  public  Vector   equipModel                     = new Vector();

  public  Vector   statusDescIn                   = new Vector();
  public  Vector   statusCodeIn                   = new Vector();

  public  Vector   statusDescOut                  = new Vector();
  public  Vector   statusCodeOut                  = new Vector();

  public  Vector   ownerIn                        = new Vector();
  public  Vector   ownerCodeIn                    = new Vector();

  public  Vector   inNote                         = new Vector();
  public  Vector   inDate                         = new Vector();
  public  Vector   inTime                         = new Vector();
  public  Vector   inUser                         = new Vector();

  public  Vector   outNote                        = new Vector();
  public  Vector   outDate                        = new Vector();
  public  Vector   outTime                        = new Vector();
  public  Vector   outUser                        = new Vector();


  public PinpadExchangeBean()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_descriptor,
//                  equip_model
//          from    equipment
//          where   equiptype_code = :mesConstants.APP_EQUIP_TYPE_PINPAD and
//                  used_in_inventory = 'Y'
//          order by equipmfgr_mfr_code, equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_descriptor,\n                equip_model\n        from    equipment\n        where   equiptype_code =  :1  and\n                used_in_inventory = 'Y'\n        order by equipmfgr_mfr_code, equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_TYPE_PINPAD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^7*/
      
      rs = it.getResultSet();

      equipDesc.add("select product desc");
      equipModel.add("");
      
      while(rs.next())
      {
        equipDesc.add(rs.getString("equip_descriptor"));
        equipModel.add(rs.getString("equip_model"));
      }

      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:151^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_status
//          where   equip_status_id not in
//                  (
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_PURCHASE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_RENT,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_LEASE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_LOAN,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_MERCHANT_OWNED,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_SOLD_AS_IS,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_MES_INTERNAL_LOAN
//                  )
//          order by equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_status\n        where   equip_status_id not in\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 \n                )\n        order by equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.EQUIP_STATUS_DEPLOYED_PURCHASE);
   __sJT_st.setInt(2,mesConstants.EQUIP_STATUS_DEPLOYED_RENT);
   __sJT_st.setInt(3,mesConstants.EQUIP_STATUS_DEPLOYED_LEASE);
   __sJT_st.setInt(4,mesConstants.EQUIP_STATUS_DEPLOYED_LOAN);
   __sJT_st.setInt(5,mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE);
   __sJT_st.setInt(6,mesConstants.EQUIP_STATUS_DEPLOYED_MERCHANT_OWNED);
   __sJT_st.setInt(7,mesConstants.EQUIP_STATUS_DEPLOYED_SOLD_AS_IS);
   __sJT_st.setInt(8,mesConstants.EQUIP_STATUS_DEPLOYED_MES_INTERNAL_LOAN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/

      rs = it.getResultSet();

      statusDescIn.add("select status");
      statusCodeIn.add("");
      
      while(rs.next())
      {
        statusDescIn.add(rs.getString("equip_status_desc"));
        statusCodeIn.add(rs.getString("equip_status_id"));
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_status
//          where   equip_status in
//                  (
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_PURCHASE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_RENT,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_LEASE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_LOAN,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_MERCHANT_OWNED,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_SOLD_AS_IS,
//                    :mesConstants.EQUIP_STATUS_DEPLOYED_MES_INTERNAL_LOAN
//                  )
//          order by equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_status\n        where   equip_status in\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 \n                )\n        order by equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.EQUIP_STATUS_DEPLOYED_PURCHASE);
   __sJT_st.setInt(2,mesConstants.EQUIP_STATUS_DEPLOYED_RENT);
   __sJT_st.setInt(3,mesConstants.EQUIP_STATUS_DEPLOYED_LEASE);
   __sJT_st.setInt(4,mesConstants.EQUIP_STATUS_DEPLOYED_LOAN);
   __sJT_st.setInt(5,mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE);
   __sJT_st.setInt(6,mesConstants.EQUIP_STATUS_DEPLOYED_MERCHANT_OWNED);
   __sJT_st.setInt(7,mesConstants.EQUIP_STATUS_DEPLOYED_SOLD_AS_IS);
   __sJT_st.setInt(8,mesConstants.EQUIP_STATUS_DEPLOYED_MES_INTERNAL_LOAN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:199^7*/
      
      rs = it.getResultSet();

      statusDescOut.add("select status");
      statusCodeOut.add("");
      
      while(rs.next())
      {
        statusDescOut.add(rs.getString("equip_status_desc"));
        statusCodeOut.add(rs.getString("equip_status_id"));
      }
      
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                  inventory_owner_name
//          from    app_type
//          where   SEPARATE_INVENTORY = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code,\n                inventory_owner_name\n        from    app_type\n        where   SEPARATE_INVENTORY = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/

      rs = it.getResultSet();

      ownerIn.add("select owner");
      ownerCodeIn.add("");
      
      while(rs.next())
      {
        ownerIn.add(rs.getString("inventory_owner_name"));  
        ownerCodeIn.add(rs.getString("app_type_code"));
      }

      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("constructor: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  

  public void situateOwner(String merchNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:262^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_type, 
//                    merchant_type_desc  
//            
//            from    merchant_types
//            where   merchant_number = :merchNum                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_type, \n                  merchant_type_desc  \n           \n          from    merchant_types\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   owner = (String)__sJT_rs.getString(1);
   ownerDesc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^9*/
      }
      catch( java.sql.SQLException sqle )   // no data found set default
      {
        ownerDesc = "MES";
        owner     = "0";
      }        
    
      /*@lineinfo:generated-code*//*@lineinfo:278^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number,
//                  equip.equip_descriptor,
//                  clas.ec_class_name
//          from    equipment equip,
//                  equip_inventory ei,
//                  equip_class clas
//          where   ei.ei_class = clas.ec_class_id
//                  and ei.ei_part_number = equip.equip_model
//                  and equip.equiptype_code = :mesConstants.APP_EQUIP_TYPE_PINPAD
//                  and ei.ei_lrb = 0
//                  and ei.ei_status in
//                    (
//                      :mesConstants.EQUIP_STATUS_RECEIVED_IN_STOCK,
//                      :mesConstants.EQUIP_STATUS_ENCRYPTED_IN_STOCK
//                    )
//                  and ei.ei_owner = :owner
//          order by  ei.ei_class,
//                    equip.equipmfgr_mfr_code,
//                    equip.equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number,\n                equip.equip_descriptor,\n                clas.ec_class_name\n        from    equipment equip,\n                equip_inventory ei,\n                equip_class clas\n        where   ei.ei_class = clas.ec_class_id\n                and ei.ei_part_number = equip.equip_model\n                and equip.equiptype_code =  :1 \n                and ei.ei_lrb = 0\n                and ei.ei_status in\n                  (\n                     :2 ,\n                     :3 \n                  )\n                and ei.ei_owner =  :4 \n        order by  ei.ei_class,\n                  equip.equipmfgr_mfr_code,\n                  equip.equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_TYPE_PINPAD);
   __sJT_st.setInt(2,mesConstants.EQUIP_STATUS_RECEIVED_IN_STOCK);
   __sJT_st.setInt(3,mesConstants.EQUIP_STATUS_ENCRYPTED_IN_STOCK);
   __sJT_st.setString(4,owner);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^7*/
      
      rs = it.getResultSet();

      availableEquipDesc.add("select one");
      availableEquipSerialNum.add("");

      while(rs.next())
      {
        availableEquipDesc.add(rs.getString("ec_class_name") + " - " + rs.getString("equip_descriptor") + " - " + rs.getString("ei_serial_number"));
        availableEquipSerialNum.add(rs.getString("ei_serial_number"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "situateOwner: " + e.toString());
      addError("situateOwner: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }


  public String startNewExchange(String merchNum)
  {
    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    String              result      = "";

    
    try
    {
      merchantNum = merchNum; //add merchant number to variable
      
      connect();
      
      if(isBlank(exchangeNum))
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:346^11*/

//  ************************************************************
//  #sql [Ctx] { select  swap_sequence.nextval
//              
//              from    dual;
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  swap_sequence.nextval\n             \n            from    dual;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKey = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:351^11*/
          
          // get a new control number
          Calendar      cal         = new GregorianCalendar();

          // add the year
          controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);

          // add the julian day
          controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);

          // add the primarykey
          controlSeq += primaryKey;

          exchangeNum = Long.toString(controlSeq);
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "createReferenceNum: " + e.toString());
          exchangeNum = "";
        }
      
        result = exchangeNum;
      }
    }
    catch(Exception e)
    {
      logEntry("startNewExchange(" + merchNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }


  public void getMerchantEquipment()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      connect();
      
      merchEquipDesc.add("select one");
      merchEquipSerialNum.add("");

      //only get pinpads that are deployed to merchant
      /*@lineinfo:generated-code*//*@lineinfo:403^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number,
//                  equip.equip_descriptor
//          from    equipment equip,
//                  equip_inventory ei
//          where   ei.ei_part_number = equip.equip_model and
//                  ei.ei_merchant_number = :merchantNum and
//                  equip.equiptype_code = :mesConstants.APP_EQUIP_TYPE_PINPAD
//          order by equip.equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number,\n                equip.equip_descriptor\n        from    equipment equip,\n                equip_inventory ei\n        where   ei.ei_part_number = equip.equip_model and\n                ei.ei_merchant_number =  :1  and\n                equip.equiptype_code =  :2 \n        order by equip.equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_PINPAD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:413^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        merchEquipDesc.add(rs.getString("equip_descriptor") + " - " + rs.getString("ei_serial_number"));
        merchEquipSerialNum.add(rs.getString("ei_serial_number"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantEquipment: " + e.toString());
      addError("getMerchantEquipment: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getNoteInfo(String exchange)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:448^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  esn.*,
//                  u.login_name
//          from    equip_swap_notes esn,
//                  users u
//          where   swap_ref_num = :exchange and
//                  esn.note_user = u.user_id
//          order by esn.note_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  esn.*,\n                u.login_name\n        from    equip_swap_notes esn,\n                users u\n        where   swap_ref_num =  :1  and\n                esn.note_user = u.user_id\n        order by esn.note_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchange);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:457^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("note_type"))
        {
          case EXCHANGE_IN_NOTE:
            inNote.add(replaceNewLine(rs.getString("note")));
            inDate.add(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            inTime.add(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            inUser.add(rs.getString("login_name"));
          break;
          case EXCHANGE_OUT_NOTE:
            outNote.add(replaceNewLine(rs.getString("note")));
            outDate.add(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            outTime.add(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            outUser.add(rs.getString("login_name"));
          break;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getNoteInfo: " + e.toString());
      addError("getNoteInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public String getExchangeInfo(String exchange)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:506^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_exchange
//          where   exchange_ref_num = :exchange
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchange);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("merchant_num");

        this.inSubmitted  = !isBlank(rs.getString("serial_num_in"));
        if(isInSubmitted())
        {
          this.serialNumIn = rs.getString("serial_num_in");
          getSubmittedInData();
        }

        this.outSubmitted = !isBlank(rs.getString("serial_num_out"));
        if(isOutSubmitted())
        {
          this.serialNumOut = rs.getString("serial_num_out");
          getSubmittedOutData();
        }

        this.shippingCostIn  = isBlank(rs.getString("shipping_cost_in")) ? "" : rs.getString("shipping_cost_in");
        this.shippingCostOut = isBlank(rs.getString("shipping_cost_out")) ? "" : rs.getString("shipping_cost_out");

        this.exchangeStatus  = rs.getInt("exchange_status");
        this.exchangeNum     = rs.getString("exchange_ref_num");
        this.merchantNum     = rs.getString("merchant_num");
        this.callTag         = isBlank(rs.getString("call_tag_num")) ? "" : rs.getString("call_tag_num");

      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getExchangeInfo: " + e.toString());
      addError("getExchangeInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }


  public void getSubmittedInData()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:570^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip.equip_descriptor,
//                  ei.ei_status
//          from    equipment equip,
//                  equip_inventory ei
//          where   equip.equip_model = ei.ei_part_number and
//                  ei.ei_serial_number = :serialNumIn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip.equip_descriptor,\n                ei.ei_status\n        from    equipment equip,\n                equip_inventory ei\n        where   equip.equip_model = ei.ei_part_number and\n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNumIn);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:578^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        descriptionIn = isBlank(rs.getString("equip_descriptor")) ? "" : rs.getString("equip_descriptor");
        statusIn      = isBlank(rs.getString("ei_status")) ? "" : rs.getString("ei_status");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSubmittedInData: " + e.toString());
      addError("getSubmittedInData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getSubmittedOutData()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:613^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip.equip_descriptor,
//                  es.equip_status_desc
//          from    equipment equip,
//                  equip_inventory ei,
//                  equip_status es
//          where   equip.equip_model = ei.ei_part_number and
//                  es.equip_status_id = ei.ei_status and
//                  ei.ei_serial_number = :serialNumOut
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip.equip_descriptor,\n                es.equip_status_desc\n        from    equipment equip,\n                equip_inventory ei,\n                equip_status es\n        where   equip.equip_model = ei.ei_part_number and\n                es.equip_status_id = ei.ei_status and\n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNumOut);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:623^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        descriptionOut = isBlank(rs.getString("equip_descriptor")) ? "" : rs.getString("equip_descriptor");
        statusOutDesc  = isBlank(rs.getString("equip_status_desc")) ? "" : rs.getString("equip_status_desc");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSubmittedOutData: " + e.toString());
      addError("getSubmittedOutData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private boolean doesExist()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;
    
    try
    {
      connect();
      if(!isBlank(this.merchantNum))
      {
        /*@lineinfo:generated-code*//*@lineinfo:660^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_number
//            from    merchant
//            where   merch_number = :merchantNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_number\n          from    merchant\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:665^9*/
        
        rs = it.getResultSet();

        if(rs.next())
        {
          result = true;
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "doesExist: " + e.toString());
      addError("doesExist: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  public boolean validate()
  {
    if(!doesExist())
    {
      addError("we got problems");
    }
    return(!hasErrors());
  }


  public boolean validateOutUpdateData()
  {

    if(!isBlank(this.shippingCostOut) && !isNumber(this.shippingCostOut))
    {
      addError("Please provide a valid shipping cost for deployed equipment");
    }

    return(!hasErrors());
  }

  public boolean validateOutData()
  {
    if(isBlank(this.serialNumOutMan) && isBlank(this.serialNumOutList))
    {
      addError("Either select an In Item from the list OR provide a serial number");
    }
    else if(!isBlank(this.serialNumOutMan) && !isBlank(this.serialNumOutList))
    {
      addError("Either select an In Item from the list OR provide a serial number");
    }

    if(isBlank(this.statusOut))
    {
      addError("Please select the status of the deployed equipment");
    }
    if(!isBlank(this.shippingCostOut) && !isNumber(this.shippingCostOut))
    {
      addError("Please provide a valid shipping cost for deployed equipment");
    }

    //makes sure it exists in inventory
    if(!hasErrors())
    {
      this.serialNumOut = isBlank(this.serialNumOutMan) ? this.serialNumOutList : this.serialNumOutMan;
      if(!equipmentExists(this.serialNumOut))
      {
        addError("Equipment with serial # " + this.serialNumOut + " does not exist in the inventory");
      }
    }

    //make sure equipment is not already deployed
    if(!hasErrors())
    {
      String tempMerch = getDeployedMerch(this.serialNumOut);

      if(!isBlank(tempMerch))
      {
        addError("Equipment with serial # " + this.serialNumOut + " is currently deployed to merchant # " + tempMerch);
      }
    }

    //make sure outgoing is a gosh darn pinpad since this is pinpad exchange...
    if(!hasErrors() && !isPinpad(this.serialNumOut))
    {
      addError("Equipment with serial # " + this.serialNumOut + " is not a Pinpad");
    }


    //make sure it deploys something with instock status
    if(!hasErrors())
    {
      String tempStatus = getStatusDesc(this.serialNumOut);

      if(!isBlank(tempStatus))
      {
        addError("Equipment with serial # " + this.serialNumOut + " currently has a status of " + tempStatus);
      }
    }

    //make sure it belongs to the same client
    if(!hasErrors())
    {
      String tempOwner = getEquipOwner(this.serialNumOut,this.owner);

      if(!isBlank(tempOwner))
      {
        addError("Equipment with serial # " + this.serialNumOut + " exists in inventory as being owned by " + tempOwner);
      }
    }


    return(!hasErrors());
  }

  private String getStatusDesc(String serNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:798^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_status,
//                  stat.equip_status_desc 
//          from    equip_inventory ei, 
//                  equip_status stat 
//          where   ei.ei_status = stat.equip_status_id and 
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_status,\n                stat.equip_status_desc \n        from    equip_inventory ei, \n                equip_status stat \n        where   ei.ei_status = stat.equip_status_id and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:806^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("ei_status") != RECEIVED_IN_STOCK && rs.getInt("ei_status") != ENCRYPTED_IN_STOCK)
        {
          result = isBlank(rs.getString("equip_status_desc")) ? "" : rs.getString("equip_status_desc");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getStatusDesc: " + e.toString());
      addError("getStatusDesc: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }

  private boolean equipmentExists(String serNum)
  {
    boolean           result  = true;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:853^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ei_serial_number)
//          
//          from    equip_inventory
//          where   ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ei_serial_number)\n         \n        from    equip_inventory\n        where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:859^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "equipmentExists: " + e.toString());
      addError("equipmentExists: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }


  public boolean validateNoteIn()
  {
    if(isBlank(this.notesIn))
    {
      addError("Please provide an In Item Note");
    }
    else if(this.notesIn.length() > 500)
    {
      addError("In Item Note must be less than 500 characters. Currently it's " + this.notesIn.length() + " characters long.");
    }
    return(!hasErrors());
  }

  public boolean validateNoteOut()
  {
    if(isBlank(this.notesOut))
    {
      addError("Please provide an Out Item Note");
    }
    else if(this.notesOut.length() > 500)
    {
      addError("Out Item Note must be less than 500 characters. Currently it's " + this.notesOut.length() + " characters long.");
    }
    return(!hasErrors());
  }

  public boolean validateInUpdateData()
  {

    if(isBlank(this.statusIn))
    {
      addError("Please select the status of the equipment upon return");
    }
    if(!isBlank(this.shippingCostIn) && !isNumber(this.shippingCostIn))
    {
      addError("Please provide a valid shipping cost for returned equipment");
    }

    return(!hasErrors());
  }

  public boolean validateInData()
  {
    if(isBlank(this.serialNumInMan) && isBlank(this.serialNumInList))
    {
      addError("Either select an In Item from the list OR provide a serial number and product description");
    }
    else if(!isBlank(this.serialNumInMan) && !isBlank(this.serialNumInList))
    {
      addError("Either select an In Item from the list OR provide a serial number and product description");
    }
    else if(!isBlank(this.serialNumInMan) && isBlank(partNum))
    {
      addError("If entering a serial #, also select the description of the item from the pull down list");
    }

    //else if(!isBlank(this.serialNumInMan) && isBlank(owner))
    //{
    //  addError("If entering a serial #, also select the owner of the item from the pull down list");
    //}

    if(isBlank(this.statusIn))
    {
      addError("Please select the status of the equipment upon return");
    }
    if(!isBlank(this.shippingCostIn) && !isNumber(this.shippingCostIn))
    {
      addError("Please provide a valid shipping cost for returned equipment");
    }


    //make sure its not deployed to a different merchant
    if(!hasErrors())
    {
      this.serialNumIn = isBlank(this.serialNumInMan) ? this.serialNumInList : this.serialNumInMan;
      String tempMerch = getDeployedMerch(this.serialNumIn);

      if(!isBlank(tempMerch) && !this.merchantNum.equals(tempMerch))
      {
        addError("Equipment with serial # " + this.serialNumIn + " is deployed to merchant # " + tempMerch);
      }
    }

    //make sure incoming is a gosh darn pinpad since this is pinpad exchange.... (same with outgoing)
    if(!hasErrors() && !isPinpad(this.serialNumIn))
    {
      addError("Equipment with serial # " + this.serialNumIn + " is not a Pinpad");
    }


    //make sure the equip desc is same as in database
    if(!hasErrors() && !isBlank(this.partNum))
    {
      String tempDesc = getEquipDesc(this.serialNumIn,this.partNum);

      if(!isBlank(tempDesc))
      {
        addError("Equipment with serial # " + this.serialNumIn + " exists in inventory as " + tempDesc);
      }
    }

    //makes sure incoming is a an item that exists in clients inventory..
    if(!hasErrors() && !isBlank(this.owner))
    {
      String tempOwner = getEquipOwner(this.serialNumIn,this.owner);

      if(!isBlank(tempOwner))
      {
        addError("Equipment with serial # " + this.serialNumIn + " exists in inventory as being owned by " + tempOwner);
      }
    }

    return(!hasErrors());
  }

  private boolean isPinpad(String serNum)
  {
    boolean           result  = true;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1002^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(equip.equip_type_code)
//          
//          from    equipment equip,
//                  equip_inventory ei
//          where   ei.ei_serial_number = :serNum and
//                  ei.ei_part_number = equip.equip_model and
//                  equip.equip_type_code = :mesConstants.APP_EQUIP_TYPE_PINPAD
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(equip.equip_type_code)\n         \n        from    equipment equip,\n                equip_inventory ei\n        where   ei.ei_serial_number =  :1  and\n                ei.ei_part_number = equip.equip_model and\n                equip.equip_type_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_PINPAD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1011^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isPinpad(): " + e.toString());
      addError("isPinpad(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }

  private String getEquipOwner(String serNum, String own)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1037^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_owner,
//                  at.inventory_owner_name 
//          from    equip_inventory ei, 
//                  app_type at 
//          where   ei.ei_owner = at.app_type_code and 
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_owner,\n                at.inventory_owner_name \n        from    equip_inventory ei, \n                app_type at \n        where   ei.ei_owner = at.app_type_code and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1045^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        String tempOwner = isBlank(rs.getString("ei_owner")) ? "" : rs.getString("ei_owner");

        if(!tempOwner.equals(own))
        {
          result = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipOwner: " + e.toString());
      addError("getEquipOwner: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }

  private String getEquipDesc(String serNum,String parNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1094^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_part_number,
//                  equip.equip_descriptor 
//          from    equip_inventory ei, 
//                  equipment equip 
//          where   ei.ei_part_number = equip.equip_model and 
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_part_number,\n                equip.equip_descriptor \n        from    equip_inventory ei, \n                equipment equip \n        where   ei.ei_part_number = equip.equip_model and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1102^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        String tempModel = isBlank(rs.getString("ei_part_number")) ? "" : rs.getString("ei_part_number");
        if(!tempModel.equals(parNum))
        {
          result = isBlank(rs.getString("equip_descriptor")) ? "" : rs.getString("equip_descriptor");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipDesc: " + e.toString());
      addError("getEquipDesc: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  private String getDeployedMerch(String serNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1151^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei_merchant_number 
//          from    equip_inventory 
//          where   ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei_merchant_number \n        from    equip_inventory \n        where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1156^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = isBlank(rs.getString("ei_merchant_number")) ? "" : rs.getString("ei_merchant_number");
      }
      else
      {
        result = "";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDeployedMerch: " + e.toString());
      addError("getDeployedMerch: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  private String decodeStatus(String status)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1197^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_status_desc 
//          from    equip_status 
//          where   equip_status_id = :status
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_status_desc \n        from    equip_status \n        where   equip_status_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,status);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1202^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        result = isBlank(rs.getString("equip_status_desc")) ? "" : rs.getString("equip_status_desc");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "decodeStatus: " + e.toString());
      addError("decodeStatus: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }

  private String decodeOwner(String own)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1238^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name 
//          from    app_type 
//          where   app_type_code = :own
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name \n        from    app_type \n        where   app_type_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,own);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1243^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        result = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "decodeOwner: " + e.toString());
      addError("decodeOwner: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }

  private void addToTracking()
  {
    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1277^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ref_num_serial_num)
//          
//          from    equip_merchant_tracking 
//          where   action = :mesConstants.ACTION_CODE_PINPAD_EXCHANGE and 
//                  ref_num_serial_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ref_num_serial_num)\n         \n        from    equip_merchant_tracking \n        where   action =  :1  and \n                ref_num_serial_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.ACTION_CODE_PINPAD_EXCHANGE);
   __sJT_st.setString(2,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1284^7*/

      if(recCount == 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1288^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_merchant_tracking 
//            (
//              merchant_number,
//              action_date,
//              action,
//              ref_num_serial_num 
//            )
//            values
//            (
//              :merchantNum,
//              sysdate,
//              :mesConstants.ACTION_CODE_PINPAD_EXCHANGE,
//              :exchangeNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_merchant_tracking \n          (\n            merchant_number,\n            action_date,\n            action,\n            ref_num_serial_num \n          )\n          values\n          (\n             :1 ,\n            sysdate,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setInt(2,mesConstants.ACTION_CODE_PINPAD_EXCHANGE);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1304^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addToTracking: " + e.toString());
      addError("addToTracking: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitInData(long userId)
  {
    String            action  = "";

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1328^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(stat.equip_status_desc)
//          
//          from    equip_inventory ei,
//                  equip_status stat
//          where   ei.ei_status = stat.equip_status_id and
//                  ei.ei_serial_number = :serialNumIn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(stat.equip_status_desc)\n         \n        from    equip_inventory ei,\n                equip_status stat\n        where   ei.ei_status = stat.equip_status_id and\n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNumIn);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1336^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1340^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory 
//            set     ei_status = :statusIn,
//                    ei_class  = 2,
//                    ei_lrb    = 0,
//                    ei_deployed_date    = null,
//                    ei_merchant_number  = null,
//                    ei_lrb_price        = null
//            where ei_serial_number = :serialNumIn
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory \n          set     ei_status =  :1 ,\n                  ei_class  = 2,\n                  ei_lrb    = 0,\n                  ei_deployed_date    = null,\n                  ei_merchant_number  = null,\n                  ei_lrb_price        = null\n          where ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,statusIn);
   __sJT_st.setString(2,serialNumIn);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1350^9*/
        
        action = "Item returned from merchant # " + this.merchantNum + " in exchange ref # " + this.exchangeNum;
        action += "\n Status of returning item - " + decodeStatus(this.statusIn);
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1357^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_inventory 
//            ( 
//              ei_status, 
//              ei_class,           --set to used, 2
//              ei_original_class,  --set to new, 1
//              ei_lrb,             --set to not deployed, 0
//              ei_received_date,   --set to today
//              ei_transaction_id,  --set to zero, 0
//              ei_part_number, 
//              ei_serial_number,ei_owner
//            ) 
//            values 
//            (
//              :statusIn,
//              2,
//              1,
//              0,
//              sysdate,
//              0,
//              :partNum,
//              :serialNumIn,
//              :owner
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_inventory \n          ( \n            ei_status, \n            ei_class,           --set to used, 2\n            ei_original_class,  --set to new, 1\n            ei_lrb,             --set to not deployed, 0\n            ei_received_date,   --set to today\n            ei_transaction_id,  --set to zero, 0\n            ei_part_number, \n            ei_serial_number,ei_owner\n          ) \n          values \n          (\n             :1 ,\n            2,\n            1,\n            0,\n            sysdate,\n            0,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,statusIn);
   __sJT_st.setString(2,partNum);
   __sJT_st.setString(3,serialNumIn);
   __sJT_st.setString(4,owner);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1382^9*/

        action = "Item added to inventory from merchant # " + this.merchantNum + " in exchange ref # " + this.exchangeNum;
        action += "\n Status of item upon entry - " + decodeStatus(this.statusIn);
        action += "\n Owner of item upon entry - " + decodeOwner(this.owner);
      }

      addToHistory(this.partNum, this.serialNumIn, userId, Integer.parseInt(this.statusIn), action);
      addToTracking();

      //add to swap table...
      recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1394^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1400^7*/

      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1404^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//            set     serial_num_in = :serialNumIn,
//                    exchange_status = :exchangeStatus,
//                    date_received = sysdate
//            where   exchange_ref_num = :exchangeNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n          set     serial_num_in =  :1 ,\n                  exchange_status =  :2 ,\n                  date_received = sysdate\n          where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNumIn);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1411^9*/
        
        // update call tag if necessary
        if(!isBlank(callTag))
        {
          /*@lineinfo:generated-code*//*@lineinfo:1416^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1421^11*/
        }
        
        // update shippingCost data if necessary
        if(!isBlank(shippingCostIn))
        {
          /*@lineinfo:generated-code*//*@lineinfo:1427^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     shipping_cost_in = :shippingCostIn
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     shipping_cost_in =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shippingCostIn);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1432^11*/
        }
      }
      else
      {
        // insert new record
        /*@lineinfo:generated-code*//*@lineinfo:1438^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//            (
//              exchange_ref_num,
//              merchant_num,
//              serial_num_in,
//              date_received,
//              shipping_cost_in,
//              call_tag_num,
//              exchange_status
//            )
//            values
//            (
//              :exchangeNum,
//              :merchantNum,
//              :serialNumIn,
//              sysdate,
//              :shippingCostIn,
//              :callTag,
//              :exchangeStatus
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n          (\n            exchange_ref_num,\n            merchant_num,\n            serial_num_in,\n            date_received,\n            shipping_cost_in,\n            call_tag_num,\n            exchange_status\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n             :4 ,\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setString(3,serialNumIn);
   __sJT_st.setString(4,shippingCostIn);
   __sJT_st.setString(5,callTag);
   __sJT_st.setInt(6,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1460^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitInData: " + e.toString());
      addError("submitInData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitUpdateInData(long userId)
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    String            action        = "";
    boolean           updateStatus  = false;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1487^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  stat.equip_status_desc,
//                  ei.ei_status 
//          from    equip_inventory ei, 
//                  equip_status stat 
//          where   ei.ei_status = stat.equip_status_id and 
//                  ei.ei_serial_number = :serialNumIn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  stat.equip_status_desc,\n                ei.ei_status \n        from    equip_inventory ei, \n                equip_status stat \n        where   ei.ei_status = stat.equip_status_id and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNumIn);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.inventory.PinpadExchangeBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1495^7*/
      
      rs = it.getResultSet();

      String curStatus = "";
      String curStatusDesc = "";
      
      if(rs.next())
      {
        curStatus = rs.getString("ei_status");
        curStatusDesc = rs.getString("equip_status_desc");
      }
      else
      {
        addError("In item Status update failed");
      }
      
      rs.close();
      it.close();

      if(!statusIn.equals(curStatus))  //if status changes add change to history
      {
        /*@lineinfo:generated-code*//*@lineinfo:1517^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory
//            set     ei_status = :statusIn
//            where   ei_serial_number = :serialNumIn
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory\n          set     ei_status =  :1 \n          where   ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,statusIn);
   __sJT_st.setString(2,serialNumIn);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1522^9*/
        
        action = "Status changed from " + curStatusDesc + " to " + decodeStatus(this.statusIn);

        addToHistory(this.partNum, this.serialNumIn, userId, Integer.parseInt(this.statusIn), action);
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1529^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1535^7*/

      //update in shipping in swap table... if not blank
      if(!isBlank(this.shippingCostIn))
      {
        updateStatus = true;

        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1544^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     shipping_cost_in = :shippingCostIn,
//                      exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     shipping_cost_in =  :1 ,\n                    exchange_status =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shippingCostIn);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1550^11*/
        }
        else
        {
          addError("In item shipping cost update failed");
        }
      }

      //update call tag... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;
        
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1565^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag,
//                      exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 ,\n                    exchange_status =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1571^11*/
        }
        else
        {
          addError("Call Tag update failed");
        }
      }

      //update swap status
      if(!updateStatus)
      {
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1584^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     exchange_status =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,exchangeStatus);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1589^11*/
        }
        else
        {
          addError("Exchange status update failed");
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitUpdateInData: " + e.toString());
      addError("submitUpdateInData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void addToTracking2() //this only shows that a deployment was made in a swap
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1616^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_merchant_tracking 
//          (
//            merchant_number,
//            action_date,
//            action,
//            action_desc,
//            ref_num_serial_num 
//          )
//          values
//          (
//            :merchantNum,
//            sysdate,
//            :mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE,
//            :statusOut,
//            :serialNumOut
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_merchant_tracking \n        (\n          merchant_number,\n          action_date,\n          action,\n          action_desc,\n          ref_num_serial_num \n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setInt(2,mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE);
   __sJT_st.setString(3,statusOut);
   __sJT_st.setString(4,serialNumOut);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1634^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addToTracking2: " + e.toString());
      addError("addToTracking2: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public void submitOutData(long userId)
  {
    String            action  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1656^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory
//          set     ei_status = :mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE,
//                  ei_lrb = :statusOut,
//                  ei_deployed_date = sysdate,
//                  ei_merchant_number = :merchantNum
//          where   ei_serial_number = :serialNumOut
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory\n        set     ei_status =  :1 ,\n                ei_lrb =  :2 ,\n                ei_deployed_date = sysdate,\n                ei_merchant_number =  :3 \n        where   ei_serial_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.EQUIP_STATUS_DEPLOYED_PP_EXCHANGE);
   __sJT_st.setString(2,statusOut);
   __sJT_st.setString(3,merchantNum);
   __sJT_st.setString(4,serialNumOut);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1664^7*/

      action = decodeStatus(this.statusOut) + " to merchant #: " + this.merchantNum + " in exchange ref # " + this.exchangeNum;
      addToHistory(this.partNum, this.serialNumOut, userId, Integer.parseInt(this.statusOut), action);
      addToTracking();
      addToTracking2();

      //add to swap table...
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1673^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1679^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1683^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//            set     serial_num_out = :serialNumOut,
//                    date_deployed = sysdate
//            where   exchange_ref_num = :exchangeNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n          set     serial_num_out =  :1 ,\n                  date_deployed = sysdate\n          where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNumOut);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1689^9*/

        if(!isBlank(this.callTag))
        {
          /*@lineinfo:generated-code*//*@lineinfo:1693^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"41com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1698^11*/
        }

        if(!isBlank(this.shippingCostOut))
        {
          /*@lineinfo:generated-code*//*@lineinfo:1703^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     shipping_cost_out = :shippingCostOut
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     shipping_cost_out =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"42com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shippingCostOut);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1708^11*/
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1713^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//            (
//              exchange_ref_num,
//              merchant_num,
//              serial_num_out,
//              date_deployed,
//              shipping_cost_out,
//              call_tag_num,
//              exchange_status
//            )
//            values
//            (
//              :exchangeNum,
//              :merchantNum,
//              :serialNumOut,
//              sysdate,
//              :shippingCostOut,
//              :callTag,
//              :exchangeStatus
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n          (\n            exchange_ref_num,\n            merchant_num,\n            serial_num_out,\n            date_deployed,\n            shipping_cost_out,\n            call_tag_num,\n            exchange_status\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n             :4 ,\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"43com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setString(3,serialNumOut);
   __sJT_st.setString(4,shippingCostOut);
   __sJT_st.setString(5,callTag);
   __sJT_st.setInt(6,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1735^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitOutData: " + e.toString());
      addError("submitOutData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitUpdateOutData(long userId)
  {
    boolean           updateStatus  = false;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1759^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1765^7*/

      //update out shipping in swap table... if not blank
      if(!isBlank(this.shippingCostOut))
      {
        updateStatus = true;

        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1774^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     shipping_cost_out = :shippingCostOut,
//                      exchangeStatus = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     shipping_cost_out =  :1 ,\n                    exchangeStatus =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"45com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shippingCostOut);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1780^11*/
        }
        else
        {
          addError("Out item shipping cost update failed");
        }
      }

      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1795^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag,
//                      exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 ,\n                    exchange_status =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"46com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1801^11*/
        }
        else
        {
          addError("Call Tag update failed");
        }
      }

      //update swap status
      if(!updateStatus)
      {
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1814^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     exchange_status =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"47com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,exchangeStatus);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1819^11*/
        }
        else
        {
          addError("Exchange status update failed");
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitUpdateOutData: " + e.toString());
      addError("submitUpdateOutData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitInNote(long userId)
  {
    boolean           updateStatus  = false;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1846^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_swap_notes
//          (
//            swap_ref_num,
//            note_date,
//            note_user,
//            note_type,
//            note
//          )
//          values
//          (
//            :exchangeNum,
//            sysdate,
//            :userId,
//            :EXCHANGE_IN_NOTE,
//            :notesIn
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_swap_notes\n        (\n          swap_ref_num,\n          note_date,\n          note_user,\n          note_type,\n          note\n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"48com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,EXCHANGE_IN_NOTE);
   __sJT_st.setString(4,notesIn);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1864^7*/
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1868^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1874^7*/
      
      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1881^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag,
//                      exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//                      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 ,\n                    exchange_status =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"50com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1888^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1892^11*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//              (
//                exchange_ref_num,
//                merchant_num,
//                call_tag_num,
//                exchange_status
//              )
//              values
//              (
//                :exchangeNum,
//                :merchantNum,
//                :callTag,
//                :exchangeStatus
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n            (\n              exchange_ref_num,\n              merchant_num,\n              call_tag_num,\n              exchange_status\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"51com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setString(3,callTag);
   __sJT_st.setInt(4,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1908^11*/

          //add to tracking table
          addToTracking();
        }
      }

      //update swap status
      if(!updateStatus)
      {
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1920^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     exchange_status =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"52com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,exchangeStatus);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1925^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1929^11*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//              (
//                exchange_ref_num,
//                merchant_num,
//                exchange_status
//              )
//              values
//              (
//                :exchangeNum,
//                :merchantNum,
//                :exchangeStatus
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n            (\n              exchange_ref_num,\n              merchant_num,\n              exchange_status\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"53com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setInt(3,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1943^11*/

          //add to tracking table
          addToTracking();
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitInNote: " + e.toString());
      addError("submitInNote: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitOutNote(long userId)
  {
    boolean           updateStatus  = false;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1969^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_swap_notes
//          (
//            swap_ref_num,
//            note_date,
//            note_user,
//            note_type,
//            note
//          )
//          values
//          (
//            :exchangeNum,
//            sysdate,
//            :userId,
//            :EXCHANGE_OUT_NOTE,
//            :notesOut
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_swap_notes\n        (\n          swap_ref_num,\n          note_date,\n          note_user,\n          note_type,\n          note\n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"54com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,EXCHANGE_OUT_NOTE);
   __sJT_st.setString(4,notesOut);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1987^7*/
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1991^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(exchange_ref_num)
//          
//          from    equip_exchange
//          where   exchange_ref_num = :exchangeNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(exchange_ref_num)\n         \n        from    equip_exchange\n        where   exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"55com.mes.inventory.PinpadExchangeBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1997^7*/
      
      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:2006^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     call_tag_num = :callTag,
//                      exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     call_tag_num =  :1 ,\n                    exchange_status =  :2 \n            where   exchange_ref_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"56com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callTag);
   __sJT_st.setInt(2,exchangeStatus);
   __sJT_st.setString(3,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2012^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2016^11*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//              (
//                exchange_ref_num,
//                merchant_num,
//                call_tag_num,
//                exchange_status
//              )
//              values
//              (
//                :exchangeNum,
//                :merchantNum,
//                :callTag,
//                :exchangeStatus
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n            (\n              exchange_ref_num,\n              merchant_num,\n              call_tag_num,\n              exchange_status\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"57com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setString(3,callTag);
   __sJT_st.setInt(4,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2032^11*/
          
          //add to tracking table
          addToTracking();
        }
      }

      //update swap status
      if(!updateStatus)
      {
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:2044^11*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//              set     exchange_status = :exchangeStatus
//              where   exchange_ref_num = :exchangeNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n            set     exchange_status =  :1 \n            where   exchange_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"58com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,exchangeStatus);
   __sJT_st.setString(2,exchangeNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2049^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2053^11*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_exchange
//              (
//                exchange_ref_num,
//                merchant_num,
//                exchange_status
//              )
//              values
//              (
//                :exchangeNum,
//                :merchantNum,
//                :exchangeStatus
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_exchange\n            (\n              exchange_ref_num,\n              merchant_num,\n              exchange_status\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"59com.mes.inventory.PinpadExchangeBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,exchangeNum);
   __sJT_st.setString(2,merchantNum);
   __sJT_st.setInt(3,exchangeStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2067^11*/

          //add to tracking table
          addToTracking();
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitInOut: " + e.toString());
      addError("submitInOut: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String replaceNewLine(String newLine)
  {
    if(isBlank(newLine))
    {
      return "";
    }

    String body = newLine;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;

      index = body.indexOf('\n');

      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"<BR>");
        body = bodyBuff.toString();
        index = body.indexOf('\n', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error");
    }
    return body;
  }


  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }

  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setCallTag(String callTag)
  {
    this.callTag = callTag;
  }

  public String getCallTag()
  {
    return this.callTag;
  }


  public void setExchangeStatus(String exchangeStatus)
  {
    try
    {
      this.exchangeStatus = Integer.parseInt(exchangeStatus);
    }
    catch(Exception e)
    {
      this.exchangeStatus = 0;
    }
  }

  public int getExchangeStatus()
  {
    return this.exchangeStatus;
  }


  public void setSerialNumIn(String serialNumIn)
  {
    this.serialNumIn = serialNumIn;
  }

  public void setSerialNumOut(String serialNumOut)
  {
    this.serialNumOut = serialNumOut;
  }

  public String getSerialNumOut()
  {
    return this.serialNumOut;
  }

  public String getSerialNumIn()
  {
    return this.serialNumIn;
  }

  public void setStatusIn(String statusIn)
  {
    this.statusIn = statusIn;
  }

  public String getStatusIn()
  {
    return this.statusIn;
  }

  public void setStatusOut(String statusOut)
  {
    this.statusOut = statusOut;
  }

  public String getStatusOut()
  {
    return this.statusOut;
  }

  public String getStatusOutDesc()
  {
    return this.statusOutDesc;
  }

  public void setPartNum(String partNum)
  {
    this.partNum = partNum;
  }

  public String getPartNum()
  {
    return this.partNum;
  }

  public void setOwner(String owner)
  {
    this.owner = owner;
  }

  public String getOwner()
  {
    return this.owner;
  }

  public void setOwnerDesc(String ownerDesc)
  {
    this.ownerDesc = ownerDesc;
  }

  public String getOwnerDesc()
  {
    return this.ownerDesc;
  }


  public void setNotesIn(String notesIn)
  {
    this.notesIn = notesIn;
  }

  public void setNotesOut(String notesOut)
  {
    this.notesOut = notesOut;
  }

  public String getDescriptionIn()
  {
    return this.descriptionIn;
  }

  public String getDescriptionOut()
  {
    return this.descriptionOut;
  }

  public void setSerialNumOutMan(String serialNumOutMan)
  {
    this.serialNumOutMan = serialNumOutMan;
  }

  public String getSerialNumOutMan()
  {
    return this.serialNumOutMan;
  }

  public void setSerialNumInMan(String serialNumInMan)
  {
    this.serialNumInMan = serialNumInMan;
  }

  public String getSerialNumInMan()
  {
    return this.serialNumInMan;
  }

  public void setSerialNumOutList(String serialNumOutList)
  {
    this.serialNumOutList = serialNumOutList;
  }

  public String getSerialNumOutList()
  {
    return this.serialNumOutList;
  }

  public void setSerialNumInList(String serialNumInList)
  {
    this.serialNumInList = serialNumInList;
  }

  public String getSerialNumInList()
  {
    return this.serialNumInList;
  }

  public boolean  isInSubmitted()
  {
    return this.inSubmitted;
  }
  public boolean  isOutSubmitted()
  {
    return this.outSubmitted;
  }

  public void setShippingCostIn(String shippingCostIn)
  {
    this.shippingCostIn = shippingCostIn;
  }

  public String getShippingCostIn()
  {
    return this.shippingCostIn;
  }

  public void setShippingCostOut(String shippingCostOut)
  {
    this.shippingCostOut = shippingCostOut;
  }

  public String getShippingCostOut()
  {
    return this.shippingCostOut;
  }

  public void setExchangeNum(String exchangeNum)
  {
    this.exchangeNum = exchangeNum;
  }

  public String getExchangeNum()
  {
    return this.exchangeNum;
  }

}/*@lineinfo:generated-code*/