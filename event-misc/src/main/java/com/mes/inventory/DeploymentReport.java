/*@lineinfo:filename=DeploymentReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/DeploymentReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:47p $
  Version            : $Revision: 16 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class DeploymentReport extends DateSQLJBean
{
  public class DeploymentData
  {
    private String      serialNum;
    private String      merchantNumber;
    private String      associationNumber;
    private String      dbaName;
    private String      client;
    private String      deployDate;
    private Timestamp   deployDateSorted;
    private String      receiveDate;
    private Timestamp   receiveDateSorted;
    private String      deployType;
    private String      lrb;
    private String      equipType;
    private String      equipDesc;
    private String      deployPrice;
    private String      cost;
    private String      condition;

    public DeploymentData()
    {
      serialNum         = "";
      merchantNumber    = "";
      associationNumber = "";
      dbaName           = "";
      client            = "";
      deployDate        = "";
      deployDateSorted  = null;
      receiveDate       = "";
      receiveDateSorted = null;
      deployType        = "";
      lrb               = "";
      equipType         = "";
      equipDesc         = "";
      cost              = "";
      deployPrice       = "";
      condition         = "";
    }
    
    public DeploymentData(String     serialNum,
                          String     merchantNumber,
                          String     associationNumber,
                          String     dbaName,
                          String     client,
                          Timestamp  deployDateSorted,
                          String     deployDate,
                          Timestamp  receiveDateSorted,
                          String     receiveDate,
                          String     deployType,
                          String     lrb,
                          String     equipType,
                          String     equipDesc,
                          String     cost,
                          String     deployPrice,
                          String     condition)
    {
      setSerialNum(serialNum);
      setMerchantNumber(merchantNumber);
      setAssociationNumber(associationNumber);
      setDbaName(dbaName);
      setClient(client);
      setDeployDate(deployDate);
      setDeployDateSorted(deployDateSorted);
      setReceiveDate(receiveDate);
      setReceiveDateSorted(receiveDateSorted);
      setDeployType(deployType);
      setLrb(lrb);
      setEquipType(equipType);
      setEquipDesc(equipDesc);
      setCost(cost);
      setDeployPrice(deployPrice);
      setCondition(condition);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setSerialNum(String serialNum)
    {
      this.serialNum = processStringField(serialNum);
    }
    public String getSerialNum()
    {
      return this.serialNum;
    }

    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }

    public void setAssociationNumber(String associationNumber)
    {
      this.associationNumber = processStringField(associationNumber);
    }
    public String getAssociationNumber()
    {
      return this.associationNumber;
    }

    public void setDeployDate(String deployDate)
    {
      this.deployDate = processStringField(deployDate);
    }
    public String getDeployDate()
    {
      return this.deployDate;
    }

    public void setDeployDateSorted(Timestamp deployDateSorted)
    {
      this.deployDateSorted = deployDateSorted;
    }
    public String getDeployDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(deployDateSorted, "yyyyMMddHHmmss");
    }

    public void setReceiveDate(String receiveDate)
    {
      this.receiveDate = processStringField(receiveDate);
    }
    public String getReceiveDate()
    {
      return this.receiveDate;
    }

    public void setReceiveDateSorted(Timestamp receiveDateSorted)
    {
      this.receiveDateSorted = receiveDateSorted;
    }
    public String getReceiveDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(receiveDateSorted, "yyyyMMddHHmmss");
    }

    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }

    public void setClient(String client)
    {
      this.client = processStringField(client);
    }
    public String getClient()
    {
      return this.client;
    }

    public void setDeployType(String deployType)
    {
      this.deployType = processStringField(deployType);
    }
    public String getDeployType()
    {
      return this.deployType;
    }

    public void setLrb(String lrb)
    {
      this.lrb = processStringField(lrb);
    }
    public String getLrb()
    {
      return this.lrb;
    }

    public void setEquipType(String equipType)
    {
      this.equipType = processStringField(equipType);
    }
    public String getEquipType()
    {
      return this.equipType;
    }

    public void setEquipDesc(String equipDesc)
    {
      this.equipDesc = processStringField(equipDesc);
    }
    public String getEquipDesc()
    {
      return this.equipDesc;
    }

    public void setCost(String cost)
    {
      this.cost = processStringField(cost);
    }
    public String getCost()
    {
      return this.cost;
    }

    public void setDeployPrice(String deployPrice)
    {
      this.deployPrice = processStringField(deployPrice);
    }
    public String getDeployPrice()
    {
      return this.deployPrice;
    }

    public void setCondition(String condition)
    {
      this.condition = processStringField(condition);
    }
    public String getCondition()
    {
      return this.condition;
    }
  }
  

  public class EquipDataComparator
    implements Comparator
  {
    public final static int   SB_MERCHANT_NUMBER  = 0;
    public final static int   SB_DEPLOY_DATE      = 1;
    public final static int   SB_DBA_NAME         = 2;
    public final static int   SB_CLIENT           = 3;
    public final static int   SB_SERIAL_NUMBER    = 4;

    public final static int   SB_DEPLOY_TYPE      = 5;
    public final static int   SB_LRB              = 6;
    public final static int   SB_EQUIP_TYPE       = 7;
    public final static int   SB_EQUIP_DESC       = 8;
    public final static int   SB_ASSO_NUMBER      = 9;
    public final static int   SB_COST             = 10;
    public final static int   SB_RECEIVE_DATE     = 11;
    public final static int   SB_CONDITION        = 12;
    public final static int   SB_DEPLOY_PRICE     = 13;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public EquipDataComparator()
    {
      this.sortBy = SB_DEPLOY_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_MERCHANT_NUMBER && sortBy <= SB_DEPLOY_PRICE)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(DeploymentData o1, DeploymentData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getMerchantNumber() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
          
          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getAssociationNumber() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_DATE:
            compareString1 = o1.getDeployDateSorted() + o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getDeployDateSorted() + o2.getDbaName() + o2.getSerialNum();
            break;
          
          case SB_RECEIVE_DATE:
            compareString1 = o1.getReceiveDateSorted() + o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getReceiveDateSorted() + o2.getDbaName() + o2.getSerialNum();
            break;

          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDbaName() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getClient() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNum() + o1.getDeployDateSorted();
            compareString2 = o2.getSerialNum() + o2.getDeployDateSorted();
            break;
            
          case SB_DEPLOY_TYPE:
            compareString1 = o1.getDeployType() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDeployType() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_LRB:
            compareString1 = o1.getLrb() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getLrb() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getEquipType() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getEquipDesc() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_COST:
            compareString1 = o1.getCost() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getCost() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_CONDITION:
            compareString1 = o1.getCondition() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getCondition() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_DEPLOY_PRICE:
            compareString1 = o1.getDeployPrice() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDeployPrice() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(DeploymentData o1, DeploymentData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getMerchantNumber() + o2.getDeployDate() + o2.getSerialNum();
            break;

          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getAssociationNumber() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_DATE:
            compareString1 = o1.getDeployDate() + o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getDeployDate() + o2.getDbaName() + o2.getSerialNum();
            break;

          case SB_RECEIVE_DATE:
            compareString1 = o1.getReceiveDateSorted() + o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getReceiveDateSorted() + o2.getDbaName() + o2.getSerialNum();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getDbaName() + o2.getDeployDate() + o2.getSerialNum();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getClient() + o2.getDeployDate() + o2.getSerialNum();
            break;
            
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNum() + o1.getDeployDate();
            compareString2 = o2.getSerialNum() + o2.getDeployDate();
            break;
            
          case SB_DEPLOY_TYPE:
            compareString1 = o1.getDeployType() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getDeployType() + o2.getDeployDate() + o2.getSerialNum();
            break;
            
          case SB_LRB:
            compareString1 = o1.getLrb() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getLrb() + o2.getDeployDate() + o2.getSerialNum();
            break;
            
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getEquipType() + o2.getDeployDate() + o2.getSerialNum();
            break;
            
          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc() + o1.getDeployDate() + o1.getSerialNum();
            compareString2 = o2.getEquipDesc() + o2.getDeployDate() + o2.getSerialNum();
            break;

          case SB_COST:
            compareString1 = o1.getCost() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getCost() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_CONDITION:
            compareString1 = o1.getCondition() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getCondition() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_DEPLOY_PRICE:
            compareString1 = o1.getDeployPrice() + o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDeployPrice() + o2.getDeployDateSorted() + o2.getSerialNum();
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((DeploymentData)o1, (DeploymentData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((DeploymentData)o1, (DeploymentData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  private String              lookupValue         = "";
  private String              lastLookupValue     = "";
  
  private int                 action              = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription   = "Invalid Action";
  
  private EquipDataComparator edc                 = new EquipDataComparator();

  private Vector              lookupResults       = new Vector();

  private TreeSet             sortedResults       = null;
  
  private boolean             submitted           = false;
  private boolean             refresh             = false;
  
  //search criteria
  private int                 client              = -1;
  private int                 lastClient          = -1;

  private int                 lrb                 = -1;
  private int                 lastLrb             = -1;
  
  private int                 deploymentType      = -1;
  private int                 lastDeploymentType  = -1;
  
  private String              equipType           = "-1";
  private String              lastEquipType       = "-1";

  private String              equipModel          = "-1";
  private String              lastEquipModel      = "-1";

  private int                 condition           = -1;
  private int                 lastCondition       = -1;


  private int                 lastFromMonth       = -1;
  private int                 lastFromDay         = -1;
  private int                 lastFromYear        = -1;
  private int                 lastToMonth         = -1;
  private int                 lastToDay           = -1;
  private int                 lastToYear          = -1;
  
  public  Vector              clients              = new Vector();
  public  Vector              clientValues         = new Vector();

  public  Vector              lrbTypes             = new Vector();
  public  Vector              lrbTypeValues        = new Vector();

  public  Vector              conditions           = new Vector();
  public  Vector              conditionValues      = new Vector();

  public  Vector              deploymentTypes      = new Vector();
  public  Vector              deploymentTypeValues = new Vector();

  public  Vector              equipTypes           = new Vector();
  public  Vector              equipTypeValues      = new Vector();

  public  Vector              equipModels          = new Vector();
  public  Vector              equipModelValues     = new Vector();

  public DeploymentReport()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        client          != lastClient         ||
        lrb             != lastLrb            ||
        deploymentType  != lastDeploymentType ||
        condition       != lastCondition      ||
        fromMonth       != lastFromMonth      ||
        fromDay         != lastFromDay        ||
        fromYear        != lastFromYear       ||
        toMonth         != lastToMonth        ||
        toDay           != lastToDay          ||
        toYear          != lastToYear         ||
        ! equipType.equals(lastEquipType)     ||
        ! equipModel.equals(lastEquipModel)   ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      System.out.println("filling drop downs...");
      connect();
      
      if(clients.size() == 0)
      {
        clients.clear();
        clientValues.clear();
        lrbTypes.clear();
        lrbTypeValues.clear();
        conditions.clear();
        conditionValues.clear();
        deploymentTypes.clear();
        deploymentTypeValues.clear();
        equipTypes.clear();
        equipTypeValues.clear();
        equipModels.clear();
        equipModelValues.clear();
      
        // clients
        clients.add("All Clients");
        clientValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:688^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name, app_type_code 
//            from    app_type
//            where   SEPARATE_INVENTORY = 'Y'
//            order by inventory_owner_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name, app_type_code \n          from    app_type\n          where   SEPARATE_INVENTORY = 'Y'\n          order by inventory_owner_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.DeploymentReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:694^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          clients.add(rs.getString("inventory_owner_name"));
          clientValues.add(rs.getString("app_type_code"));
        }
      
        rs.close();
        it.close();
      
        // lrb types
        lrbTypes.add("All Lend Types");
        lrbTypeValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:709^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiplendtype
//            where   equiplendtype_code in (1,2,5,7,20,26,27,28)
//            order by equiplendtype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiplendtype\n          where   equiplendtype_code in (1,2,5,7,20,26,27,28)\n          order by equiplendtype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.DeploymentReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:715^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          lrbTypes.add(rs.getString("equiplendtype_description"));
          lrbTypeValues.add(rs.getString("equiplendtype_code"));
        }
        
        rs.close();
        it.close();
        
        // condition
        conditions.add("All Conditions");
        conditionValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:731^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//            from    equip_class
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n          from    equip_class";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.DeploymentReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          conditions.add(rs.getString("ec_class_name"));
          conditionValues.add(rs.getString("ec_class_id"));
        }
      
        rs.close();
        it.close();

        // equip types
        equipTypes.add("All Equip Types");
        equipTypeValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:751^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiptype
//            where   equiptype_code not in (7,8,9)
//            order by equiptype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiptype\n          where   equiptype_code not in (7,8,9)\n          order by equiptype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.DeploymentReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:757^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipTypes.add(rs.getString("equiptype_description"));
          equipTypeValues.add(rs.getString("equiptype_code"));
        }
        
        rs.close();
        it.close();

        // equip models
        equipModels.add("All Equip Desc");
        equipModelValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:774^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                    equip_descriptor
//            from    equipment
//            where   equiptype_code not in (7,8,9) and
//                    used_in_inventory = 'Y'
//            order by equip_descriptor asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                  equip_descriptor\n          from    equipment\n          where   equiptype_code not in (7,8,9) and\n                  used_in_inventory = 'Y'\n          order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.DeploymentReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:782^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipModels.add(rs.getString("equip_descriptor"));
          equipModelValues.add(rs.getString("equip_model"));
        }
        
        rs.close();
        it.close();

        deploymentTypes.add("All Deployment Types");
        deploymentTypeValues.add("-1");
        deploymentTypes.add("New Accoun Setup");
        deploymentTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP));
        deploymentTypes.add("Additional Equipment");
        deploymentTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL));
        deploymentTypes.add("Swap");
        deploymentTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_DEPLOYMENT_SWAP));
        deploymentTypes.add("Pinpad Exchange");
        deploymentTypeValues.add(Integer.toString(mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE));
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        
        Date fromDate = getSqlFromDate();
        Date toDate   = getSqlToDate(); 
        
        /*@lineinfo:generated-code*//*@lineinfo:883^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                    mt.merchant_type_desc           client_name,
//                    mt.merchant_number              merchant_number,
//                    mt.association_number           association_number,
//                    emt.action_date                 deploy_date,
//                    emta.action_description         deploy_type,
//                    elt.equiplendtype_description   lrb,
//                    et.equiptype_description        equip_type,
//                    eq.equip_descriptor             equip_desc,
//                    ec.ec_class_name                condition,
//                    ei.EI_SERIAL_NUMBER             serial_number,
//                    ei.ei_unit_cost                 unit_cost,
//                    ei.ei_received_date             receive_date,
//                    ei.ei_lrb_price                 deploy_price
//            from    merchant_types                  mt,
//                    equip_merchant_tracking         emt,
//                    equip_merchant_tracking_action  emta,
//                    equipment                       eq,
//                    equiplendtype                   elt,
//                    equiptype                       et,
//                    equip_class                     ec,
//                    equip_inventory                 ei
//            where   trunc(emt.action_date) between :fromDate and :toDate and
//                    emt.action in (1,4,5,7) and emt.cancel_date is null and
//                    emt.merchant_number = mt.merchant_number and
//                    emt.action = emta.action_code and
//                    emt.action_desc = elt.equiplendtype_code and
//                    emt.ref_num_serial_num = ei.ei_serial_number and
//                    ei.ei_class = ec.ec_class_id and
//                    ei.EI_PART_NUMBER = eq.equip_model and
//                    eq.equiptype_code = et.equiptype_code and 
//                    (-1 = :client or mt.merchant_type = :client) and
//                    (-1 = :lrb or emt.action_desc = :lrb) and
//                    (-1 = :condition or ei.ei_class = :condition) and
//                    (-1 = :deploymentType or emt.action = :deploymentType) and
//                    ('-1' = :equipType or eq.equiptype_code = :equipType) and
//                    ('-1' = :equipModel or eq.equip_model = :equipModel) and
//                    (
//                      'passall'              = :stringLookup or
//                      mt.merchant_number     = :longLookup or
//                      mt.association_number  = :longLookup or
//                      upper(emt.ref_num_serial_num) = :stringLookup2 or 
//                      upper(mt.merchant_name) like :stringLookup
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                  mt.merchant_type_desc           client_name,\n                  mt.merchant_number              merchant_number,\n                  mt.association_number           association_number,\n                  emt.action_date                 deploy_date,\n                  emta.action_description         deploy_type,\n                  elt.equiplendtype_description   lrb,\n                  et.equiptype_description        equip_type,\n                  eq.equip_descriptor             equip_desc,\n                  ec.ec_class_name                condition,\n                  ei.EI_SERIAL_NUMBER             serial_number,\n                  ei.ei_unit_cost                 unit_cost,\n                  ei.ei_received_date             receive_date,\n                  ei.ei_lrb_price                 deploy_price\n          from    merchant_types                  mt,\n                  equip_merchant_tracking         emt,\n                  equip_merchant_tracking_action  emta,\n                  equipment                       eq,\n                  equiplendtype                   elt,\n                  equiptype                       et,\n                  equip_class                     ec,\n                  equip_inventory                 ei\n          where   trunc(emt.action_date) between  :1  and  :2  and\n                  emt.action in (1,4,5,7) and emt.cancel_date is null and\n                  emt.merchant_number = mt.merchant_number and\n                  emt.action = emta.action_code and\n                  emt.action_desc = elt.equiplendtype_code and\n                  emt.ref_num_serial_num = ei.ei_serial_number and\n                  ei.ei_class = ec.ec_class_id and\n                  ei.EI_PART_NUMBER = eq.equip_model and\n                  eq.equiptype_code = et.equiptype_code and \n                  (-1 =  :3  or mt.merchant_type =  :4 ) and\n                  (-1 =  :5  or emt.action_desc =  :6 ) and\n                  (-1 =  :7  or ei.ei_class =  :8 ) and\n                  (-1 =  :9  or emt.action =  :10 ) and\n                  ('-1' =  :11  or eq.equiptype_code =  :12 ) and\n                  ('-1' =  :13  or eq.equip_model =  :14 ) and\n                  (\n                    'passall'              =  :15  or\n                    mt.merchant_number     =  :16  or\n                    mt.association_number  =  :17  or\n                    upper(emt.ref_num_serial_num) =  :18  or \n                    upper(mt.merchant_name) like  :19 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.DeploymentReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,lrb);
   __sJT_st.setInt(6,lrb);
   __sJT_st.setInt(7,condition);
   __sJT_st.setInt(8,condition);
   __sJT_st.setInt(9,deploymentType);
   __sJT_st.setInt(10,deploymentType);
   __sJT_st.setString(11,equipType);
   __sJT_st.setString(12,equipType);
   __sJT_st.setString(13,equipModel);
   __sJT_st.setString(14,equipModel);
   __sJT_st.setString(15,stringLookup);
   __sJT_st.setLong(16,longLookup);
   __sJT_st.setLong(17,longLookup);
   __sJT_st.setString(18,stringLookup2);
   __sJT_st.setString(19,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.inventory.DeploymentReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:929^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          lookupResults.add(new DeploymentData(rs.getString("serial_number"),  
                                               rs.getString("merchant_number"), 
                                               rs.getString("association_number"),
                                               rs.getString("dba_name"), 
                                               rs.getString("client_name"), 
                                               rs.getTimestamp("deploy_date"), 
                                               DateTimeFormatter.getFormattedDate(rs.getTimestamp("deploy_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                               rs.getTimestamp("receive_date"), 
                                               DateTimeFormatter.getFormattedDate(rs.getTimestamp("receive_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                               rs.getString("deploy_type"), 
                                               rs.getString("lrb"), 
                                               rs.getString("equip_type"), 
                                               rs.getString("equip_desc"),
                                               rs.getString("unit_cost"),
                                               rs.getString("deploy_price"),
                                               rs.getString("condition")));
        }
      
        rs.close();
        it.close();
        
        lastClient          = client;
        lastLrb             = lrb;
        lastDeploymentType  = deploymentType;
        lastCondition       = condition;
        lastEquipType       = equipType;
        lastEquipModel      = equipModel;
        lastFromMonth       = fromMonth;
        lastFromDay         = fromDay;
        lastFromYear        = fromYear;
        lastToMonth         = toMonth;
        lastToDay           = toDay;
        lastToYear          = toYear;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Deployment Report";
  }
  public synchronized void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setEquipType(String equipType)
  {
    this.equipType = equipType;
  }
 
  public synchronized String getEquipType()
  {
    return this.equipType;
  }

  public synchronized void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
 
  public synchronized String getEquipModel()
  {
    return this.equipModel;
  }

  public synchronized void setCondition(String condition)
  {
    try
    {
      this.condition = Integer.parseInt(condition);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getCondition()
  {
    return Integer.toString(this.condition);
  }
  

  public synchronized void setClient(String client)
  {
    try
    {
      this.client = Integer.parseInt(client);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getClient()
  {
    return Integer.toString(this.client);
  }
 
  public synchronized void setLrb(String lrb)
  {
    try
    {
      this.lrb = Integer.parseInt(lrb);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getLrb()
  {
    return Integer.toString(this.lrb);
  }

  public synchronized void setDeploymentType(String deploymentType)
  {
    try
    {
      this.deploymentType = Integer.parseInt(deploymentType);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getDeploymentType()
  {
    return Integer.toString(this.deploymentType);
  }

 
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }
}/*@lineinfo:generated-code*/