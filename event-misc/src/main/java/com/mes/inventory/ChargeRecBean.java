/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/ChargeRecBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/06/01 10:03a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

public class ChargeRecBean extends com.mes.screens.SequenceDataBean
{

  private String   merchantNum     = "";
  private Vector   chargeRecs      = new Vector();
      
  public ChargeRecBean()
  {
  }

  public void getData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    ChargeRec         crec    = null;

    try
    {
      qs.append("select DISTINCT cg.cg_charge_amount, cg.cg_start_date, cg.cg_expiration_date, cg.cg_message_for_stmt from monthly_extract_cg cg, monthly_extract_gn gn ");
      qs.append("where cg.hh_load_sec = gn.hh_load_sec and gn.hh_merchant_number = ? and cg.cg_charge_record_type in ( 'POS','IMP','MEM' ) ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1,this.merchantNum);
      rs = ps.executeQuery();
  
      while(rs.next())
      {
         crec = new ChargeRec();

         crec.setChargeDesc(      rs.getString("cg_message_for_stmt"));
         crec.setChargeAmt(       rs.getString("cg_charge_amount"));
         crec.setChargeStartDate( rs.getString("cg_start_date"));
         crec.setChargeExpDate(   rs.getString("cg_expiration_date"));
         
         this.chargeRecs.add(crec);
      }    
      rs.close();  
      ps.close();

    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
  }  
  

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }


  public int getNumCharge()
  {
    return this.chargeRecs.size();
  }
  
  
  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }

 
  public String getChargeDesc(int idx)
  {
    ChargeRec crec = (ChargeRec)chargeRecs.elementAt(idx);
    return crec.getChargeDesc();
  }
  public String getChargeAmt(int idx)
  {
    ChargeRec crec = (ChargeRec)chargeRecs.elementAt(idx);
    return(formatCurr(crec.getChargeAmt()));
  }
  public String getChargeStartDate(int idx)
  {
    ChargeRec crec = (ChargeRec)chargeRecs.elementAt(idx);
    return(crec.getChargeStartDate());
  }
  public String getChargeExpDate(int idx)
  {
    ChargeRec crec = (ChargeRec)chargeRecs.elementAt(idx);
    return(crec.getChargeExpDate());
  }


  public class ChargeRec
  {
    private String chargeDesc         = "";
    private String chargeAmt          = "";
    private String chargeStartDate    = "";
    private String chargeExpDate      = "";

    public void setChargeDesc(String chargeDesc)
    {
      this.chargeDesc = chargeDesc;
    }
    public void setChargeAmt(String chargeAmt)
    {
      this.chargeAmt = chargeAmt;
    }
    public void setChargeStartDate(String chargeStartDate)
    {
      this.chargeStartDate = chargeStartDate;
    }
    public void setChargeExpDate(String chargeExpDate)
    {
      this.chargeExpDate = chargeExpDate;
    }

    public String getChargeDesc()
    {
      return this.chargeDesc;
    }
    public String getChargeAmt()
    {
      return this.chargeAmt;
    }
    public String getChargeStartDate()
    {
      return this.chargeStartDate;
    }
    public String getChargeExpDate()
    {
      return this.chargeExpDate;
    }

  }                                                                    
}