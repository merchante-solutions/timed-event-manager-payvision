/*@lineinfo:filename=EquipRepairBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EquipRepairBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:47p $
  Version            : $Revision: 17 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.ops.InventoryBase;

public class EquipRepairBean extends InventoryBase
{
  public static final int RECEIVED_IN_STOCK             = 0;
  public static final int ENCRYPTED_IN_STOCK            = 3;

  public static final int REPAIR_INCOMPLETE             = mesConstants.CALLTAG_INCOMPLETE;
  public static final int REPAIR_COMPLETE               = mesConstants.CALLTAG_COMPLETE;

  public static final int REPAIR_COMPANY_VMS            = 16;
  public static final int REPAIR_COMPANY_POSPORTAL      = 11;
  public static final int REPAIR_COMPANY_OTHER          = 8;
  public static final int REPAIR_COMPANY_INVALID        = 0;

  public static final int REPAIR_COMPANY_HYPERCOM       = 21;
  public static final int REPAIR_COMPANY_GCF            = 22;
  public static final int REPAIR_COMPANY_ALLIANCE       = 23;
  public static final int REPAIR_COMPANY_ENCRYPTION_VMS = 24;
  public static final int REPAIR_COMPANY_CDE_SERVICES   = 29;

  public static final int NOTE_TYPE_REPAIR              = 2;

  //equipment description
  private String   productName                    = "";
  private String   productDesc                    = "";
  private String   productType                    = "";
  private String   classDesc                      = "";
  private String   statusDesc                     = "";
  private boolean  inInventory                    = false;

  //for deployed products
  private String   merchantNum                    = "";
  private String   merchantDba                    = "";
  private String   owner                          = "";
  private String   ownerName                      = "";

  private Date     deployDate                     = null;
  private String   lrbDesc                        = "";

  //repair info 
  private String   repairNum                      = "";
  private int      repairStatus                   = REPAIR_INCOMPLETE;
  private int      repairCompany                  = REPAIR_COMPANY_INVALID;
  private String   callTag                        = "";

  private Date     dateSentTo                     = null;
  private Date     dateGotBack                    = null;
  private Date     dateOfOrder                    = null;

  //used on newrepair.jsp
  private String   serialNumInMan                 = "";
  private String   serialNumInList                = "";
  private String   partNum                        = "";
  
  private String   serialNum                      = "";

  //holds current statues once submitted
  private String   statusAfterRepair              = "";
  
  private String   shippingCostFromMerchant       = "";
  private String   shippingCostToRepair           = "";
  private String   shippingCostFromRepair         = "";
  private String   shippingCostToMerchant         = "";

  private String   repairCost                     = "";
  
  private String   repairNote                     = "";

  private boolean  inSubmitted                    = false;
  private boolean  outSubmitted                   = false;

  public  Vector   availableEquipDesc             = new Vector();
  public  Vector   availableEquipSerialNum        = new Vector();

  public  Vector   equipDesc                      = new Vector();
  public  Vector   equipModel                     = new Vector();

  public  Vector   statusDescIn                   = new Vector();
  public  Vector   statusCodeIn                   = new Vector();

  public  Vector   statusDescOut                  = new Vector();
  public  Vector   statusCodeOut                  = new Vector();

  public  Vector   repairNotes                    = new Vector();
  public  Vector   repairDates                    = new Vector();
  public  Vector   repairTimes                    = new Vector();
  public  Vector   repairUsers                    = new Vector();

  public EquipRepairBean()
  {
  }
  
  public void initialize( )
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      qs.setLength(0);
      qs.append("select equip_descriptor,equip_model ");
      qs.append("from equipment where equiptype_code not in (7,8,9) and used_in_inventory = 'Y' order by equip_descriptor ");

      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();

      equipDesc.add("select one");
      equipModel.add("");
      
      while(rs.next())
      {
        equipDesc.add(rs.getString("equip_descriptor"));
        equipModel.add(rs.getString("equip_model"));
      }

      ps.close();
      rs.close();

      qs.setLength(0);
      qs.append("select * from equip_status where equip_status_id not in (1,2,5,6,7,20,26,27,28) order by equip_status_order ");

      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();

      statusDescIn.add("select one");
      statusCodeIn.add("");
      
      while(rs.next())
      {
        statusDescIn.add(rs.getString("equip_status_desc"));
        statusCodeIn.add(rs.getString("equip_status_id"));
      }

      ps.close();
      rs.close();
    }  
    catch(Exception e)
    {
      logEntry("constructor()", e.toString());
      addError("constructor: " + e.toString());
    }
  }
  
  //gets repair info.. serialnum partnum and repairnum
  public void getRepairInfo(String repair)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();
    String            result  = "";

    try
    {

      //System.out.println("repair # " + repair);

      qs.append("select er.*                      ");
      qs.append("from   equip_repair        er    ");
      qs.append("where  er.repair_ref_num   = ?   ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1,repair);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.serialNum                = rs.getString("serial_num");
        this.partNum                  = rs.getString("part_num");
        this.repairNum                = rs.getString("repair_ref_num");
        this.shippingCostFromMerchant = isBlank(rs.getString("SHIPPING_COST_TO_MES"))       ? "" : rs.getString("SHIPPING_COST_TO_MES");
        this.shippingCostToRepair     = isBlank(rs.getString("SHIPPING_COST_TO_REPAIR"))    ? "" : rs.getString("SHIPPING_COST_TO_REPAIR");
        this.shippingCostFromRepair   = isBlank(rs.getString("SHIPPING_COST_FROM_REPAIR"))  ? "" : rs.getString("SHIPPING_COST_FROM_REPAIR");
        this.shippingCostToMerchant   = isBlank(rs.getString("SHIPPING_COST_TO_MERCHANT"))  ? "" : rs.getString("SHIPPING_COST_TO_MERCHANT");
        this.repairCost               = isBlank(rs.getString("repair_cost"))                ? "" : rs.getString("repair_cost");
        this.callTag                  = isBlank(rs.getString("call_tag_num"))               ? "" : rs.getString("call_tag_num");
        this.repairCompany            = rs.getInt("repair_company");
        this.repairStatus             = rs.getInt("repair_status");
        this.dateSentTo               = rs.getDate("date_sent_to_repair");
        this.dateGotBack              = rs.getDate("date_returned_from_repair");
        this.dateOfOrder              = rs.getDate("date_repair_order_initiated");
      }
      
      ps.close();
      rs.close();
    }  
    catch(Exception e)
    {
      logEntry("getRepairInfo()", e.toString());
      addError("getRepairInfo: " + e.toString());
    }
  }


  public void getData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select inv.*,                                                    ");
      qs.append("       stat.equip_status_desc,                                   ");
      qs.append("       class.ec_class_name,                                      ");
      qs.append("       equip.equip_descriptor,                                   ");
      qs.append("       type.equiptype_description,                               ");
      qs.append("       mfgr.equipmfgr_mfr_name,                                  ");
      qs.append("       lend.equiplendtype_description                            ");
      qs.append("from   equipment       equip,                                    ");
      qs.append("       equiptype       type,                                     ");
      qs.append("       equipmfgr       mfgr,                                     ");
      qs.append("       equip_inventory inv,                                      ");
      qs.append("       equip_class     class,                                    ");
      qs.append("       equiplendtype   lend,                                     ");
      qs.append("       equip_status    stat                                      ");
      qs.append("where  ei_serial_number         = ?                       and    ");
      qs.append("       ei_part_number           = ?                       and    ");
      qs.append("       ei_part_number           = equip.equip_model       and    ");
      qs.append("       equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and    ");
      qs.append("       equip.equiptype_code     = type.equiptype_code     and    ");
      qs.append("       lend.equiplendtype_code  = inv.ei_lrb              and    ");
      qs.append("       stat.equip_status_id     = inv.ei_status           and    ");
      qs.append("       inv.ei_class             = class.ec_class_id              ");

      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1,this.serialNum);
      ps.setString(2,this.partNum);

      rs = ps.executeQuery();
  
      if(rs.next())
      {
        this.inInventory     = true;
        this.classDesc       = isBlank(rs.getString("ec_class_name"))             ? "" : rs.getString("ec_class_name");
        this.statusDesc      = isBlank(rs.getString("equip_status_desc"))         ? "" : rs.getString("equip_status_desc");
        this.lrbDesc         = isBlank(rs.getString("equiplendtype_description")) ? "" : rs.getString("equiplendtype_description");
        this.productName     = isBlank(rs.getString("equipmfgr_mfr_name"))        ? "" : rs.getString("equipmfgr_mfr_name");
        this.productDesc     = isBlank(rs.getString("equip_descriptor"))          ? "" : rs.getString("equip_descriptor");
        this.productType     = isBlank(rs.getString("equiptype_description"))     ? "" : rs.getString("equiptype_description");

        if(this.repairStatus == REPAIR_COMPLETE)
        {
          this.statusAfterRepair = rs.getString("ei_status");
        }
        else
        {
          this.statusAfterRepair = "";
        }

        setStatusDropDown(rs.getInt("ei_lrb"));

        rs.close();  
        ps.close();

      }    

      else
      {
      
        qs.setLength(0);
        qs.append("select equip.equip_descriptor,                                 ");
        qs.append("       type.equiptype_description,                             ");
        qs.append("       mfgr.equipmfgr_mfr_name                                 ");
        qs.append("from   equipment equip,                                        ");
        qs.append("       equiptype type,                                         ");
        qs.append("       equipmfgr mfgr                                          ");
        qs.append("where  equip.equip_model         = ? and                       ");
        qs.append("       equip.equipmfgr_mfr_code  = mfgr.equipmfgr_mfr_code and ");
        qs.append("       equip.equiptype_code      = type.equiptype_code         ");

        ps = getPreparedStatement(qs.toString());
      
        ps.setString(1,this.partNum);

        rs = ps.executeQuery();
  
        if(rs.next())
        {
          //if dont exist in our database it will get this from first page or from going to back to existing repair
          this.classDesc       = "Used";
          this.statusDesc      = "Merchant Owned Equipment";
          this.lrbDesc         = "Purchased";
          this.productName     = isBlank(rs.getString("equipmfgr_mfr_name"))        ? "" : rs.getString("equipmfgr_mfr_name");
          this.productDesc     = isBlank(rs.getString("equip_descriptor"))          ? "" : rs.getString("equip_descriptor");
          this.productType     = isBlank(rs.getString("equiptype_description"))     ? "" : rs.getString("equiptype_description");
        }    

        rs.close();  
        ps.close();
      }
      
      /// get owner and/or merchant # info from equip_merchant_tracking table
      qs.setLength(0);
      qs.append("select merchant_number,        ");
      qs.append("       owner                   ");
      qs.append("from   equip_merchant_tracking ");
      qs.append("where  REF_NUM_SERIAL_NUM = ?  ");

      ps = getPreparedStatement(qs.toString());
    
      ps.setString(1,this.repairNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.merchantNum     = isBlank(rs.getString("merchant_number"))   ? "" : rs.getString("merchant_number");
        this.owner           = isBlank(rs.getString("owner"))             ? "" : rs.getString("owner");
      }    
      
      //System.out.println(" merchantNum =  " + this.merchantNum + " owner = " + this.owner);

      rs.close();  
      ps.close();
      
      if(!isBlank(this.merchantNum))
      {
        getMerchantName();
      }
      else if(!isBlank(this.owner))
      {
        getOwnerDesc();
      }

    }  
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
      addError("getData: " + e.toString());
    }
  }  

  public void getNoteInfo()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
 
      qs.append("select   esn.*,                      ");
      qs.append("         us.login_name               ");
      qs.append("from     equip_swap_notes  esn,      ");
      qs.append("         users             us        ");
      qs.append("where    swap_ref_num  = ? and       ");
      qs.append("         note_type     = ? and       ");
      qs.append("         esn.note_user = us.user_id  ");
      qs.append("order by esn.note_date DESC          ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, this.repairNum);
      ps.setInt(2, NOTE_TYPE_REPAIR);

      rs = ps.executeQuery();

      while(rs.next())
      {
        repairNotes.add(replaceNewLine(rs.getString("note"))); 
        repairDates.add(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date"))); 
        repairTimes.add(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
        repairUsers.add(rs.getString("login_name")); 
      }
      
      ps.close();
      rs.close();
    }  
    catch(Exception e)
    {
      logEntry("getNoteInfo()", e.toString());
      addError("getNoteInfo: " + e.toString());
    }
  }


  public boolean validateRepairData()
  {
    if(this.repairCompany == REPAIR_COMPANY_INVALID)
    {
      addError("Please select a repair company");
    }
    
    if(this.repairStatus == REPAIR_COMPLETE && (this.dateSentTo == null || this.dateGotBack == null))
    {
      addError("Valid Sent To Repair and Received From Repair dates must be provided before changing repair status to complete");
    }
    
    if(!isBlank(this.shippingCostFromMerchant) && !isNumber(this.shippingCostFromMerchant))
    {
      addError("Please provide a valid shipping cost From Merchant To MES");
    }
    if(!isBlank(this.shippingCostToRepair) && !isNumber(this.shippingCostToRepair))
    {
      addError("Please provide a valid shipping cost From MES to Repair Shop");
    }
    if(!isBlank(this.shippingCostFromRepair) && !isNumber(this.shippingCostFromRepair))
    {
      addError("Please provide a valid shipping cost From Repair Shop To MES");
    }
    if(!isBlank(this.shippingCostToMerchant) && !isNumber(this.shippingCostToMerchant))
    {
      addError("Please provide a valid shipping cost From MES Back TO Merchant");
    }

    if(!isBlank(this.repairCost) && !isNumber(this.repairCost))
    {
      addError("Please provide a valid Repair Cost");
    }

    if(this.repairStatus == REPAIR_COMPLETE && doesExist(this.partNum, this.serialNum) && isBlank(this.statusAfterRepair))
    {
      addError("Please select the status of equipment upon completetion of repair");
    }
    else if(this.repairStatus == REPAIR_INCOMPLETE && !isBlank(this.statusAfterRepair) && doesExist(this.partNum, this.serialNum))
    {
      addError("Must change status of repair to Complete before selecting status of equipment upon completion of repair");
    }

    if(!isBlank(this.repairNote) && this.repairNote.length() > 500)
    {
      addError("Note must be less than 500 characters. Currently it's " + this.repairNote.length() + " characters long.");
    }

    return(!hasErrors());
  }

  public void submitData(long userId)
  {
    String            action        = "";
    int               statusCode    = -1;
    String            statusDesc    = null;
    
    PreparedStatement ps            = null;
    StringBuffer      qs            = new StringBuffer();
    ResultSet         rs            = null;

    try
    {
      //System.out.println("partnum = " + this.partNum + " and serialnum = " + this.serialNum);

      if( doesExist( this.partNum, this.serialNum ) ) //then we try to update the status in the equip_inventory table
      {
        /*@lineinfo:generated-code*//*@lineinfo:487^9*/

//  ************************************************************
//  #sql [Ctx] { select  stat.equip_status_desc, 
//                    ei.ei_status  
//            
//            from    equip_inventory   ei, 
//                    equip_status      stat 
//            where   ei.ei_part_number   = :this.partNum and
//                    ei.ei_serial_number = :this.serialNum and
//                    stat.equip_status_id = ei.ei_status
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  stat.equip_status_desc, \n                  ei.ei_status  \n           \n          from    equip_inventory   ei, \n                  equip_status      stat \n          where   ei.ei_part_number   =  :1  and\n                  ei.ei_serial_number =  :2  and\n                  stat.equip_status_id = ei.ei_status";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.EquipRepairBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,this.partNum);
   __sJT_st.setString(2,this.serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   statusDesc = (String)__sJT_rs.getString(1);
   statusCode = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:498^9*/
      
        //System.out.println("after call partnum = " + this.partNum + " and serialnum = " + this.serialNum);

        //we automatically change status to repair when they select a repair company,, then we write action to history table
        if( (this.repairStatus == REPAIR_INCOMPLETE) && (statusCode != this.repairCompany) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:505^11*/

//  ************************************************************
//  #sql [Ctx] { update equip_inventory 
//              set    ei_status        = :this.repairCompany
//              where  ei_part_number   = :this.partNum and
//                     ei_serial_number = :this.serialNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_inventory \n            set    ei_status        =  :1 \n            where  ei_part_number   =  :2  and\n                   ei_serial_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.inventory.EquipRepairBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.repairCompany);
   __sJT_st.setString(2,this.partNum);
   __sJT_st.setString(3,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^11*/
        
          action = "Status automatically changed from " + statusDesc + " to " + getStatusDesc(this.repairCompany) + " in repair ref # " + this.repairNum;
          addToHistory(this.partNum, this.serialNum, userId, this.repairCompany, action );
        }

        //when repair complete, we allow them to chose a status of item after repair.. they do this then we update table
        //once a repair is marked as complete and status is picked.. they cant pick another status.. unless they change status back to incomplete
        //changing back to incomplete resets everthing...
        else if( (this.repairStatus == REPAIR_COMPLETE) && !this.statusAfterRepair.equals( Integer.toString(statusCode) ) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:522^11*/

//  ************************************************************
//  #sql [Ctx] { update equip_inventory 
//              set    ei_status        = :this.statusAfterRepair
//              where  ei_part_number   = :this.partNum and
//                     ei_serial_number = :this.serialNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_inventory \n            set    ei_status        =  :1 \n            where  ei_part_number   =  :2  and\n                   ei_serial_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.inventory.EquipRepairBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.statusAfterRepair);
   __sJT_st.setString(2,this.partNum);
   __sJT_st.setString(3,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:528^11*/
          action = "Status changed from " + statusDesc + " to " + getStatusDesc(Integer.parseInt(this.statusAfterRepair)) + " in repair ref # " + this.repairNum;
          addToHistory(this.partNum, this.serialNum, userId, Integer.parseInt(this.statusAfterRepair), action );
        }
      }

      qs.setLength(0);

      qs.append("update equip_repair set         ");
      qs.append("repair_status              = ?, ");
      qs.append("repair_company             = ?, ");
      qs.append("call_tag_num               = ?, "); 
      qs.append("repair_cost                = ?, ");
      qs.append("SHIPPING_COST_TO_MES       = ?, ");
      qs.append("SHIPPING_COST_TO_REPAIR    = ?, ");
      qs.append("SHIPPING_COST_FROM_REPAIR  = ?, ");
      qs.append("SHIPPING_COST_TO_MERCHANT  = ?, ");
      qs.append("DATE_SENT_TO_REPAIR        = ?, ");
      qs.append("DATE_RETURNED_FROM_REPAIR  = ?  ");
      qs.append("where repair_ref_num       = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setInt(1,     this.repairStatus);
      ps.setInt(2,     this.repairCompany);
      ps.setString(3,  this.callTag);
      ps.setString(4,  this.repairCost);
      ps.setString(5,  this.shippingCostFromMerchant);
      ps.setString(6,  this.shippingCostToRepair);
      ps.setString(7,  this.shippingCostFromRepair);
      ps.setString(8,  this.shippingCostToMerchant);
      
      if(this.dateSentTo != null)
      {
        ps.setDate(9,    new java.sql.Date(this.dateSentTo.getTime()));
      }
      else
      {
        ps.setString(9,"");
      }

      if(this.dateGotBack != null)
      {
        ps.setDate(10,   new java.sql.Date(this.dateGotBack.getTime()));
      }
      else
      {
        ps.setString(10,"");
      }

      ps.setString(11, this.repairNum);

      //System.out.println("repairNumber in the submit is " + this.repairNum);

      if(ps.executeUpdate() != 1)
      {
        addError("submitData: " + "Repair not found in database, submit not successful.");
      }

      ps.close();
    }  
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
      addError("submitData: " + e.toString());
    }
  }

  public void submitNote(long userId)
  {
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    StringBuffer      qs            = new StringBuffer();
    boolean           updateStatus  = false;

    try
    {
      //if they didnt supply a note no need to write it to the database
      if(isBlank(this.repairNote))
      {
        return;
      }
      
      qs.append("insert into equip_swap_notes ( ");
      qs.append("swap_ref_num, ");
      qs.append("note_date, ");
      qs.append("note_user, ");
      qs.append("note_type, ");
      qs.append("note) values (?,sysdate,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());
    
      ps.setString(1,this.repairNum);
      ps.setLong(2,userId);
      ps.setInt(3,NOTE_TYPE_REPAIR);
      ps.setString(4,this.repairNote);
      
      ps.executeUpdate();
      ps.close();
      
      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        ps = getPreparedStatement("select repair_ref_num from equip_repair where repair_ref_num = ? ");
        ps.setString(1, this.repairNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_repair set ");
          qs.append("call_tag_num = ?, "); 
          qs.append("repair_status = ? "); 
          qs.append("where repair_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.callTag);
          ps.setInt(2,this.repairStatus);
          ps.setString(3,this.repairNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_repair ( ");
          qs.append("repair_ref_num, ");
          qs.append("call_tag_num, repair_status) values (?,?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.repairNum);
          ps.setString(2,this.callTag);
          ps.setInt(3,this.repairStatus);
          ps.executeUpdate();
          ps.close();
        }
      }

      //update swap status
      if(!updateStatus)
      {
        ps = getPreparedStatement("select repair_ref_num from equip_repair where repair_ref_num = ? ");
        ps.setString(1, this.repairNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_repair set ");
          qs.append("repair_status = ? "); 
          qs.append("where repair_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setInt(1,this.repairStatus);
          ps.setString(2,this.repairNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_repair ( ");
          qs.append("repair_ref_num, ");
          qs.append("repair_status) values (?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.repairNum);
          ps.setInt(2,this.repairStatus);
          ps.executeUpdate();
          ps.close();
        }
      }
    }  
    catch(Exception e)
    {
      logEntry("submitNote()", e.toString());
      addError("submitNote: " + e.toString());
    }
 
    //once the note has been submitted we can erase it from the variable.. so it dont pop back up on
    //the text box on the form.. we only want it to be on the text box when an error occurs and a write is not performed
    this.repairNote = "";
  }

  private void setStatusDropDown(int lrb)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {

      qs.setLength(0);

      qs.append("select * from equip_status where equip_status_id not in ");
      
      switch(lrb)
      {
        //only display deploy purchase option.. since it its purchased by merchant
        case mesConstants.APP_EQUIP_PURCHASE:
         qs.append("(2,5,6,7,11,8,16,21,22,23,24) ");
        break;

        //only display deploy rent option.. since it its rented by merchant
        case mesConstants.APP_EQUIP_RENT:
         qs.append("(1,5,6,7,11,8,16,21,22,23,24) ");
        break;

        //only display deploy lease option.. since it its leased by merchant
        case mesConstants.APP_EQUIP_LEASE:
         qs.append("(1,2,6,7,11,8,16,21,22,23,24) ");
        break;

        //only display deploy unknown option.. since it its unknowned by merchant
        case mesConstants.APP_EQUIP_UNKNOWN:
         qs.append("(1,2,5,7,11,8,16,21,22,23,24) ");
        break;

        //only display deploy loan option.. since it its loaned to merchant
        case mesConstants.APP_EQUIP_LOAN:
         qs.append("(1,2,5,6,11,8,16,21,22,23,24) ");
        break;

        // its not deployed so dont show any deployed options.. they cant deploy from here.
        default:
         qs.append("(1,2,5,6,7,11,8,16,21,22,23,24) ");
        break;
      }

      qs.append("order by equip_status_order ");

      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();

      statusDescOut.add("select one");
      statusCodeOut.add("");
      
      while(rs.next())
      {
        statusDescOut.add(rs.getString("equip_status_desc"));
        statusCodeOut.add(rs.getString("equip_status_id"));
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("setStatusDropDown()", e.toString());
      addError("setStatusDropDown: " + e.toString());
    }
  }


  public void getOwnerDesc()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      qs.setLength(0);
      qs.append("select merchant_type_desc from merchant_types ");
      qs.append("where merchant_type = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1,this.owner);
      rs = ps.executeQuery();

      if(rs.next())
      {
        this.ownerName = isBlank(rs.getString("merchant_type_desc")) ? "Unknown Owner" : rs.getString("merchant_type_desc");
      }
      else
      {
        this.ownerName = "Unknown Owner";
      }
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      logEntry("getOwnerName()", e.toString());
      addError("getOwnerName: " + e.toString());
    }

  }


  public void getMerchantName()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      qs.setLength(0);
      qs.append("select merchant_name from merchant_types ");
      qs.append("where merchant_number = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1,this.merchantNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        this.merchantDba = isBlank(rs.getString("merchant_name")) ? "" : rs.getString("merchant_name");
      }
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      logEntry("getMerchantName()", e.toString());
      addError("getMerchantName: " + e.toString());
    }
  }


//*********************** THESE METHODS ONLY CALLED WHEN NEW REPAIR STARTED ON newrepair.jsp ***********************

  public boolean validateData()
  {
    
    if(isBlank(this.serialNumInMan))
    {
      addError("Provide a serial number and product description");
    }
    else 
    {
      this.serialNum = this.serialNumInMan.trim();
    }
    
    if(isBlank(partNum))
    {
      addError("Select the description of the item from the pull down list");
    }
    
    if(!hasErrors())
    {
      String actualPartNum = getEquipmentPartNumber(this.serialNum);

      if(!isBlank(actualPartNum) && !actualPartNum.equals(this.partNum))
      {
        String tempDesc = getEquipmentDesc( actualPartNum );
      
        if(!isBlank(tempDesc))
        {
          addError("Equipment with serial # " + this.serialNum + " exists in inventory as " + tempDesc);
        }
      }
    }

    if(!hasErrors())
    {
      try
      {
        
        if(!doesExist(this.partNum,this.serialNum))
        {
          if(isBlank(this.merchantNum))
          {
            addError("This item does not exist in inventory, if it is merchant owned equipment, please provide a valid merchant number, else, add it to the inventory");
          }
          else if(!isMerchant(Long.parseLong(this.merchantNum)))
          {
            addError("A Valid Merchant Number must be provided");
          }
        }
        else
        {
          //it exists in inventory so we dont need the merchant num.. so we clear it if one was provided... 
          //we use whats in the database.. not what they gave to us...
          this.merchantNum = "";
        }

      }
      catch(Exception e)
      {
        addError("A Valid Merchant Number is required");
      }
    }

    return(!hasErrors());
  }


  private void startNewRepair()
  {
    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    
    if(!isBlank(this.repairNum))
    {
      return;
    }

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:940^7*/

//  ************************************************************
//  #sql [Ctx] { select  swap_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  swap_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.EquipRepairBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKey = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:944^7*/

      Calendar      cal         = new GregorianCalendar();
      controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);
      controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);
      controlSeq += primaryKey;
     
      this.repairNum = Long.toString(controlSeq);
    }
    catch(Exception e)
    {
      logEntry( "startNewRepair()", e.toString());
      this.repairNum = "";
    }
  }

  //this only should be called once per repair
  public void submitNewRepairData(long userId)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();
    String            action  = "";

    try
    {
      //first we must get the new refnumber
      startNewRepair();

      //add to repair table...
      /*@lineinfo:generated-code*//*@lineinfo:974^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_repair 
//          (
//            repair_ref_num,
//            serial_num,
//            part_num,
//            date_repair_order_initiated,
//            repair_status
//          ) 
//          values 
//          (
//            :this.repairNum,
//            :this.serialNum,
//            :this.partNum,
//            sysdate,
//            :REPAIR_INCOMPLETE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_repair \n        (\n          repair_ref_num,\n          serial_num,\n          part_num,\n          date_repair_order_initiated,\n          repair_status\n        ) \n        values \n        (\n           :1 ,\n           :2 ,\n           :3 ,\n          sysdate,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.inventory.EquipRepairBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.repairNum);
   __sJT_st.setString(2,this.serialNum);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setInt(4,REPAIR_INCOMPLETE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:992^7*/
    
      //@ only add a history entry if the equipment was in inventory?
      if( doesExist( this.partNum, this.serialNum ) )
      {
        action = "Item sent for repair in repair ref # " + this.repairNum;
        
        //@ add to history, but use the current status?
        addToHistory(this.partNum, this.serialNum, userId, getEquipmentStatus( this.partNum, this.serialNum ), action );
      }
      
      addToTracking();
    }  
    catch(Exception e)
    {
      logEntry( "submitNewRepairData()", e.toString());
      addError("submitNewRepairData: " + e.toString());
    }
  }

  private void getMerchantNumAndOwner()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {


      if(isBlank(this.merchantNum))
      {
     
        qs.setLength(0);
        qs.append("select ei_merchant_number,       ");
        qs.append("       ei_owner                  ");
        qs.append("from   equip_inventory           ");
        qs.append("where  ei_serial_number = ? and  ");
        qs.append("       ei_part_number   = ?      ");

        ps = getPreparedStatement(qs.toString());
        ps.setString(1, this.serialNum);    
        ps.setString(2, this.partNum);

        rs = ps.executeQuery();

        if(rs.next())
        {
          this.merchantNum    =   isBlank(rs.getString("ei_merchant_number")) ? "" : rs.getString("ei_merchant_number");
          this.owner          =   isBlank(rs.getString("ei_owner"))           ? "" : rs.getString("ei_owner");
        }
      
        rs.close();
        ps.close();
      
      }
      else
      {
      
        qs.setLength(0);
        qs.append("select merchant_type       ");
        qs.append("from   merchant_types      ");
        qs.append("where  merchant_number = ? ");

        ps = getPreparedStatement(qs.toString());
        ps.setString(1, this.merchantNum);    

        rs = ps.executeQuery();

        if(rs.next())
        {
          this.owner          =   isBlank(rs.getString("merchant_type"))  ? "" : rs.getString("merchant_type");
        }
      
        rs.close();
        ps.close();
      
      }
    }  
    catch(Exception e)
    {
      logEntry("addToTracking()", e.toString());
      addError("addToTracking: " + e.toString());
    }
  }

  //add to tracking.. if its merchant owned stuff not in our database.. then we add the merchant number to the table.. and the owner
  //if its in our database... we just add the owner of the item to the table...
  private void addToTracking()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try
    {
      
      getMerchantNumAndOwner();

      ps = getPreparedStatement("select * from equip_merchant_tracking where action = ? and ref_num_serial_num = ? ");
      ps.setInt(1, mesConstants.ACTION_CODE_REPAIR);    
      ps.setString(2,this.repairNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        //if it already exists we dont need to put in another entry
      }
      else
      {
        rs.close();
        ps.close();

        qs.setLength(0);
        qs.append("insert into equip_merchant_tracking ( ");
        qs.append("merchant_number,         ");
        qs.append("owner,                   ");
        qs.append("action_date,             ");
        qs.append("action,                  ");
        qs.append("ref_num_serial_num )     ");
        qs.append("values(?,?,sysdate,?,?)  ");

        ps = getPreparedStatement(qs.toString());
      
        ps.setString(1, this.merchantNum);
        ps.setString(2, this.owner);
        ps.setInt(3,    mesConstants.ACTION_CODE_REPAIR); //code for repair action
        ps.setString(4, this.repairNum);//repair ref num

        ps.executeUpdate();
        ps.close();

      }
    }  
    catch(Exception e)
    {
      logEntry("addToTracking()", e.toString());
      addError("addToTracking: " + e.toString());
    }
  }
//***************************************************************************



  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }

  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setCallTag(String callTag)
  {
    this.callTag = callTag;
  }

  public String getCallTag()
  {
    return this.callTag;
  }

  public String getProductName()
  {
    return this.productName;
  }
  public String getProductDesc()
  {
    return this.productDesc;
  }
  public String getProductType()
  {
    return this.productType;
  }
  public String getClassDesc()
  {
    return this.classDesc;
  }
  public String getStatusDesc()
  {
    return this.statusDesc;
  }
  public String getLrbDesc()
  {
    return this.lrbDesc;
  }

  public String getMerchantDba()
  { 
    return this.merchantDba;
  }
  public String getOwnerName()
  {
    return this.ownerName;
  }

  public void setRepairStatus(String repairStatus)
  {
    try
    {
      this.repairStatus = Integer.parseInt(repairStatus);
    }
    catch(Exception e)
    {
      this.repairStatus = 0;
    }
  }

  public int getRepairStatus()
  {
    return this.repairStatus;
  }
 
  public void setRepairCompany(String repairCompany)
  {
    try
    {
      this.repairCompany = Integer.parseInt(repairCompany);
    }
    catch(Exception e)
    {
      this.repairCompany = 0;
    }
  }

  public int getRepairCompany()
  {
    return this.repairCompany;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }

  public String getSerialNum()
  {
    return this.serialNum;
  }


  public void setStatusAfterRepair(String statusAfterRepair)
  {
    this.statusAfterRepair = statusAfterRepair;
  }

  public String getStatusAfterRepair()
  {
    return this.statusAfterRepair;
  }

  public void setPartNum(String partNum)
  {
    this.partNum = partNum;
  }

  public String getPartNum()
  {
    return this.partNum;
  }

  public void setRepairNote(String repairNote)
  {
    this.repairNote = repairNote;
  }

  public String getRepairNote()
  {
    return this.repairNote;
  }

  public void setSerialNumInMan(String serialNumInMan)
  {
    this.serialNumInMan = serialNumInMan;
  }

  public String getSerialNumInMan()
  {
    return this.serialNumInMan;
  }


  public void setSerialNumInList(String serialNumInList)
  {
    this.serialNumInList = serialNumInList;
  }

  public String getSerialNumInList()
  {
    return this.serialNumInList;
  }

  public boolean  isInSubmitted()
  {
    return this.inSubmitted;
  }
  public boolean  isOutSubmitted()
  {
    return this.outSubmitted;
  }

  public void setShippingCostFromMerchant(String shippingCostFromMerchant)
  {
    this.shippingCostFromMerchant = shippingCostFromMerchant;
  }

  public String getShippingCostFromMerchant()
  {
    return this.shippingCostFromMerchant;
  }

  public void setShippingCostToRepair(String shippingCostToRepair)
  {
    this.shippingCostToRepair = shippingCostToRepair;
  }

  public String getShippingCostToRepair()
  {
    return this.shippingCostToRepair;
  }

  public void setShippingCostFromRepair(String shippingCostFromRepair)
  {
    this.shippingCostFromRepair = shippingCostFromRepair;
  }

  public String getShippingCostFromRepair()
  {
    return this.shippingCostFromRepair;
  }

  public void setShippingCostToMerchant(String shippingCostToMerchant)
  {
    this.shippingCostToMerchant = shippingCostToMerchant;
  }

  public String getShippingCostToMerchant()
  {
    return this.shippingCostToMerchant;
  }


  public void setRepairCost(String repairCost)
  {
    this.repairCost = repairCost;
  }

  public String getRepairCost()
  {
    return this.repairCost;
  }

  public void setRepairNum(String repairNum)
  {
    this.repairNum = repairNum;
  }

  public String getRepairNum()
  {
    return this.repairNum;
  }

  public String getDateSentTo()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "";
    
    if(this.dateSentTo != null)
    {
      result = df.format(this.dateSentTo);
    }
    
    return result;
  }  

  public void setDateSentTo(String dateSentTo)
  {
    String dateSentToStr = dateSentTo.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(dateSentTo.trim());
      try
      {
        dateSentTo            = dateSentTo.replace('/','0');
        long tempLong         = Long.parseLong(dateSentTo.trim());
        if(isDate(dateSentToStr.trim()))
        {
          this.dateSentTo = aDate;
        }
        else
        {
          this.dateSentTo = null;
        }
      }
      catch(Exception e)
      {
        this.dateSentTo    = null;
      }
    }
    catch(ParseException e)
    {
      this.dateSentTo      = null;
    }
  }


  public String getDateGotBack()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "";
    
    if(this.dateGotBack != null)
    {
      result = df.format(this.dateGotBack);
    }
    
    return result;
  }  

  public void setDateGotBack(String dateGotBack)
  {
    String dateGotBackStr = dateGotBack.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);

    try
    {
      aDate = fmt.parse(dateGotBack.trim());
      try
      {
        dateGotBack           = dateGotBack.replace('/','0');
        long tempLong         = Long.parseLong(dateGotBack.trim());
        if(isDate(dateGotBackStr.trim()))
        {
          this.dateGotBack = aDate;
        }
        else
        {
          this.dateGotBack = null;
        }
      }
      catch(Exception e)
      {
        this.dateGotBack    = null;
      }
    }
    catch(ParseException e)
    {
      this.dateGotBack      = null;
    }
  }

  public String getDateOfOrder()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "-----";
    
    if(this.dateOfOrder != null)
    {
      result = df.format(this.dateOfOrder);
    }
    
    return result;
  }  

  public boolean isInInventory()
  {
    return this.inInventory;
  }

  public void addPageError(String err)
  {
    addError(err);
  }

}/*@lineinfo:generated-code*/