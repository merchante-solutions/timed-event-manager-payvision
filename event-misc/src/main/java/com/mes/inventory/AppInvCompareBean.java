/*@lineinfo:filename=AppInvCompareBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/AppInvCompareBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:37p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class AppInvCompareBean extends DateSQLJBean
{
  public class EquipmentData
  {
    public String PortfolioNode   = "";
    public String PortfolioName   = "";
    public String MerchantNumber  = "";
    public String DbaName         = "";
    public String DateOpened      = "";
    public String ActivationDate  = "";
    public String PartNumber      = "";
    public String PartDesc        = "";
    public String AppCount        = "";
    public String InventoryCount  = "";

    public EquipmentData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      PortfolioNode     = processString(resultSet.getString("portfolio_node"));
      PortfolioName     = processString(resultSet.getString("portfolio_name"));
      MerchantNumber    = processString(resultSet.getString("merchant_number"));
      DbaName           = processString(resultSet.getString("dba_name"));
      DateOpened        = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("date_opened"), "MM/dd/yyyy"));
      ActivationDate    = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("activation_date"), "MM/dd/yyyy"));
      PartNumber        = processString(resultSet.getString("part_number"));
      PartDesc          = processString(resultSet.getString("part_desc"));
      AppCount          = processString(resultSet.getString("app_count"));
      InventoryCount    = processString(resultSet.getString("inventory_count"));
    }
  }
  
  public class EquipDataComparator
    implements Comparator
  {
    public final static int   SB_PORTFOLIO_NODE     = 0;
    public final static int   SB_PORTFOLIO_NAME     = 1;
    public final static int   SB_MERCHANT_NUMBER    = 2;
    public final static int   SB_DBA_NAME           = 3;
    public final static int   SB_DATE_OPENED        = 4;
    public final static int   SB_ACTIVATION_DATE    = 5;
    public final static int   SB_PART_NUMBER        = 6;
    public final static int   SB_PART_DESC          = 7;
    public final static int   SB_APP_COUNT          = 8;
    public final static int   SB_INVENTORY_COUNT    = 9;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public EquipDataComparator()
    {
      this.sortBy = SB_PORTFOLIO_NODE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_PORTFOLIO_NODE && sortBy <= SB_INVENTORY_COUNT)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(EquipmentData o1, EquipmentData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {

          case SB_PORTFOLIO_NODE:
            compareString1 = o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          case SB_PORTFOLIO_NAME:
            compareString1 = o1.PortfolioName + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.PortfolioName + o2.MerchantNumber + o2.PartNumber;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.MerchantNumber + o1.PortfolioNode + o1.PartNumber;
            compareString2 = o2.MerchantNumber + o2.PortfolioNode + o2.PartNumber;
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.DbaName + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.DbaName + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          case SB_DATE_OPENED:
            compareString1 = o1.DateOpened + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.DateOpened + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
            
          case SB_ACTIVATION_DATE:
            compareString1 = o1.ActivationDate + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.ActivationDate + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;

          case SB_PART_NUMBER:
            compareString1 = o1.PartNumber + o1.PortfolioNode + o1.MerchantNumber;
            compareString2 = o2.PartNumber + o2.PortfolioNode + o2.MerchantNumber;
            break;
          
          case SB_PART_DESC:
            compareString1 = o1.PartDesc + o1.PortfolioNode + o1.MerchantNumber;
            compareString2 = o2.PartDesc + o2.PortfolioNode + o2.MerchantNumber;
            break;
            
          case SB_APP_COUNT:
            compareString1 = o1.AppCount + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.AppCount + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;

          case SB_INVENTORY_COUNT:
            compareString1 = o1.InventoryCount + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.InventoryCount + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(EquipmentData o1, EquipmentData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_PORTFOLIO_NODE:
            compareString1 = o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          case SB_PORTFOLIO_NAME:
            compareString1 = o1.PortfolioName + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.PortfolioName + o2.MerchantNumber + o2.PartNumber;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.MerchantNumber + o1.PortfolioNode + o1.PartNumber;
            compareString2 = o2.MerchantNumber + o2.PortfolioNode + o2.PartNumber;
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.DbaName + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.DbaName + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          case SB_DATE_OPENED:
            compareString1 = o1.DateOpened + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.DateOpened + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
            
          case SB_ACTIVATION_DATE:
            compareString1 = o1.ActivationDate + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.ActivationDate + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;

          case SB_PART_NUMBER:
            compareString1 = o1.PartNumber + o1.PortfolioNode + o1.MerchantNumber;
            compareString2 = o2.PartNumber + o2.PortfolioNode + o2.MerchantNumber;
            break;
          
          case SB_PART_DESC:
            compareString1 = o1.PartDesc + o1.PortfolioNode + o1.MerchantNumber;
            compareString2 = o2.PartDesc + o2.PortfolioNode + o2.MerchantNumber;
            break;
            
          case SB_APP_COUNT:
            compareString1 = o1.AppCount + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.AppCount + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;

          case SB_INVENTORY_COUNT:
            compareString1 = o1.InventoryCount + o1.PortfolioNode + o1.MerchantNumber + o1.PartNumber;
            compareString2 = o2.InventoryCount + o2.PortfolioNode + o2.MerchantNumber + o2.PartNumber;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
  
  private int                 action                = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription     = "Invalid Action";
  
  private EquipDataComparator edc                   = new EquipDataComparator();
  private Vector              lookupResults         = new Vector();
  private TreeSet             sortedResults         = null;
  
  private boolean             submitted             = false;
  private boolean             refresh               = false;
  private boolean             edit                  = false;
  
  //search criteria (NONE)

  public void AccountLookup()
  {
    fillDropDowns();
  }
  
  public void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  public boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public void fillDropDowns()
  {
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public void getData()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      Date fromDate = getSqlFromDate();

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:391^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    po.org_group                          as portfolio_node,
//                    po.org_name                           as portfolio_name,
//                    mf.merchant_number                    as merchant_number,
//                    mf.dba_name                           as dba_name,
//                    mmddyy_to_date(mf.date_opened)        as date_opened,
//                    mf.activation_date                    as activation_date,
//                    me.equip_model                        as part_number,
//                    eq.EQUIP_DESCRIPTOR                   as part_desc,
//                    me.MERCHEQUIP_EQUIP_QUANTITY          as app_count,
//                    get_inventory_count( mr.merch_number, me.equip_model )      as inventory_count
//          from      organization      o,
//                    group_merchant    gm,
//                    mif               mf,
//                    merchant          mr,
//                    application       app,
//                    merchequipment    me,
//                    equipment         eq,
//                    groups            g,
//                    organization      po
//          where     o.org_group         = 394100000             and
//                    gm.org_num          = o.org_num             and
//                    mf.merchant_number  = gm.merchant_number    and
//                    mr.merch_number     = mf.merchant_number    and
//                    app.app_seq_num     = mr.app_seq_num        and
//                    app.APP_CREATED_DATE > :fromDate            and
//                    mmddyy_to_date( mf.date_opened ) < ( trunc(sysdate)-5 ) and
//                    me.app_seq_num = mr.app_seq_num and
//                    not me.equip_model in ('PCPS','IPPL','IP5') and
//                    me.EQUIPLENDTYPE_CODE in ( 1,2,4,5 ) and
//                    eq.EQUIP_MODEL = me.equip_model and
//                    g.assoc_number = (mf.bank_number || mf.dmagent) and
//                    po.org_group = g.group_2
//          order by  po.org_group, 
//                    mf.merchant_number, 
//                    mf.dba_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    po.org_group                          as portfolio_node,\n                  po.org_name                           as portfolio_name,\n                  mf.merchant_number                    as merchant_number,\n                  mf.dba_name                           as dba_name,\n                  mmddyy_to_date(mf.date_opened)        as date_opened,\n                  mf.activation_date                    as activation_date,\n                  me.equip_model                        as part_number,\n                  eq.EQUIP_DESCRIPTOR                   as part_desc,\n                  me.MERCHEQUIP_EQUIP_QUANTITY          as app_count,\n                  get_inventory_count( mr.merch_number, me.equip_model )      as inventory_count\n        from      organization      o,\n                  group_merchant    gm,\n                  mif               mf,\n                  merchant          mr,\n                  application       app,\n                  merchequipment    me,\n                  equipment         eq,\n                  groups            g,\n                  organization      po\n        where     o.org_group         = 394100000             and\n                  gm.org_num          = o.org_num             and\n                  mf.merchant_number  = gm.merchant_number    and\n                  mr.merch_number     = mf.merchant_number    and\n                  app.app_seq_num     = mr.app_seq_num        and\n                  app.APP_CREATED_DATE >  :1             and\n                  mmddyy_to_date( mf.date_opened ) < ( trunc(sysdate)-5 ) and\n                  me.app_seq_num = mr.app_seq_num and\n                  not me.equip_model in ('PCPS','IPPL','IP5') and\n                  me.EQUIPLENDTYPE_CODE in ( 1,2,4,5 ) and\n                  eq.EQUIP_MODEL = me.equip_model and\n                  g.assoc_number = (mf.bank_number || mf.dmagent) and\n                  po.org_group = g.group_2\n        order by  po.org_group, \n                  mf.merchant_number, \n                  mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.AppInvCompareBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.AppInvCompareBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^7*/
      
      lookupResults.clear();
    
      rs = it.getResultSet();
      
      while(rs.next())
      {
        // add all results to the lookupResults
        lookupResults.add(new EquipmentData( rs ));
      }
    
      rs.close();
      it.close();
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  
  public int getAction()
  {
    return this.action;
  }
  public void setAction(int action)
  {
    this.actionDescription = "App To Inventory Comparison Report";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error");
    }
    return body;
  }
 
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  public void setSubmitted(boolean sub)
  {
    this.submitted = sub;
  }
}/*@lineinfo:generated-code*/